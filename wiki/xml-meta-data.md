# Process Two - XML Metadata

This page also come with a instructional video: [Click to see instructions on video](https://play.hb.se/media/t/0_b1gybpoa) (right click, open in new tab)


* [Process Two - XML Metadata](#process-two---xml-metadata)
* [General issue data](#general-issue-data)
* [Galleys in each issue](#galleys-in-each-issue)
* [Authors meta data](#authors-meta-data)
* [Submission & Supplementary files](#submission--supplementary-files)
* [Article Galleys](#article-galleys)

In this process we register all the meta data for each document and its dependency files (images, pdf etc. that is attached and belong to this document). It is important that you from time to time test the URL for each file, to test if it is working and is available. Each issue has its own XML-file and cannot be copied between issues. We have stored a template XML file in the folder `XML Template`, copy this file and name it according to the issue you are working with currently.

`The XML template is structured as follows`

1. General issue data
2. Issue cover picture
3. Galleys in issue
4. Author metadata for each issue and paper
5. Submission files in this issue
6. Supplementary files
7. Galleys (files belonging to submission files)

## General issue data

This section of the XML document contains meta data on when issue was published, what section it belongs to in the CMS etc. `All information can be found in the file called `**`infresX.html`** in each issues folder.

The tag **`description`** is the general description for the current issue, this information is located in the `infresX.html` in connection with publication data.

`NOTE`
There is no need to edit or do anything with the cover picture as of now, just leave it as is.

## Galleys in each issue

Every issue in the journal consists of `galleys`, these are `a collection of files and meta data `<u>`for each paper and article`</u>` inside an issue`. The issue galleys all have **unique ID numbers** and follow a strict structure, so it is important that you adhere to this and folllow the set series and structure in order for our papers to be able to be imported correct with their images and files.

Each galley can have multiple articles, with multiple submission files that belong to each article. Each article consists of multiple authors, submission files and their corresponding supplementary files.

### Authors meta data

All authors meta data is located within each HTML documents `<head>`- section as a `<meta>`- tag.
In the earlier versions of Information Research Journal, all documents used XHTML 1.0 as standard. 
All authors are registered with the Dublin Core meta tag,
`<meta name="DC.Creator" content="Gareth Jones, Alexander M. Robertson">`

Authors meta data is also located within each of their papers, inside the `paperXX.html` `<head>`-section.
It is important to collect and register all authors meta data, such as full name, email, affiliation and country into the corresponding meta tag of the XML file. (If you are unsure about the meta data, refer to the earlier issues XML file)

### Submission & Supplementary files

The `<submission_file>`-tag is used to describe the files belonging to a specific article within OJS. This tag is used for importing the actual file into OJS, and it is therefore very important that you enter information and meta data in a correct way. All submission file must have the attributute *stage* set to submission.

`<supplementary>`-tag is used to describe those files that belong to a certain submission file. A paper written in HTML may have images linked in the code, these images are referred to as *supplementary files*.
All supplemenatry files must have the attribute *stage* set to proof.

These both element types follow each other in the XML structure, and is basically just a list of files that we want OJS to import to the current issue we are registering. We will later tie them together in the `<article_galley>`-element.

*Example code on submission file*:
```xml
<submission_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="submission" id="35" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
  <revision number="2" genre="Article Text" filename="PaperX.html" viewable="false" date_uploaded="2019-02-21" date_modified="2019-02-21" filetype="text/html" uploader="christerjohansson">
    <name locale="en_US">Information systems strategy formation in higher education institutions</name>
    <href src="https://bitbucket.org/christerjohansson/information-research/raw/master/2017-2019/issues/1-2/paperX.html"></href>
  </revision>        
</submission_file>
```

There are a couple of things that are important when registering files in this section. One is the ID attribute, this ID follows a series throughout the entire journal. Please refer to XML import file in the previous issue to the issue you are working with, the ID numbers follow each issue. Issue 1-1 submission and supplemenetary file ID's end at 44, meaning that issue 1-2 ID's start at 45 and so on.

Before you start entering submission and supplementary files for your current issue, refer to the previous issue XML-file for ID.

Another important piecve of data is the `<href src>`-element, this is used by OJS to download and import the actual file into its system. For this, we use our public Bitbucket repository direct URL to each file.
The **Base URL** to the issues directory is: `https://bitbucket.org/christerjohansson/information-research/raw/master/2017-2019/issues/`. You then add your issue number and file name accordingly to the URL. (refer to example code snippets).

*Example code on supplementary file*:

```xml
<supplementary_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="proof" id="36" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
 <revision number="1" genre="Other" filename="image.gif" viewable="true" date_uploaded="2019-02-21" date_modified="2019-02-21" filetype="image/gif" uploader="christerjohansson">
    <name locale="en_US">University of Bor&#xE5;s, tab1.gif</name>
    <href src="https://bitbucket.org/christerjohansson/information-research/raw/master/2017-2019/issues/1-2/image.gif"></href>
  </revision>
  <publisher locale="en_US">Tom Wilson</publisher>
  <date_created>2019-02-21</date_created>
  <source locale="en_US">University of Bor&#xE5;s</source>
</supplementary_file>
```

### Article Galleys

The `<article_galley>`-element is how OJS will coordinate what image belongs to what paper. This is the *"mapping"* feature of OJS import process. In this element we assign all the HTML documents attached images to it, ie. tell OJS that this PDF, images or file in general belongs to this particular submission file. This is where the submission file ID comes in handy.

**Please note**
`All article galley elements have their own internal ID that follows a series of their own. Refer to the earlier issue you are working with to find out what he last article galley ID was in order to set a correct starting ID in your article galley series.`

Make sure that the attribute *approved* is set to false for each galley.

*Example code for article galley*,

```xml
<article_galley xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" approved="false" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
  <id type="internal" advice="ignore">40</id>
  <name locale="en_US">image.gif</name>
  <seq>0</seq>
  <submission_file_ref id="35" revision="1"/>
</article_galley>
```

When you are done regsitering all your issue files meta data, save this xml-file with the issue number as filename, ie. `15-1.xml` in the
issues folder.