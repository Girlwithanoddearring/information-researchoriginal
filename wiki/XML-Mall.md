# Mall för import av utgåvor
All import av artiklar och användare till [Open Journal System](https://pkp.sfu.ca/ojs/) sker genom en XML fil utformad enligt [instruktioner](https://github.com/pkp/ojs/blob/master/tests/data/60-content/issue.xml).

Här följer en beskrivning av hur vår mall ser ut, och hur den används vid import.
Vår mall för import av utgåvor är skräddarsydd och anpassad för Information Research specifikt. Mallen utgår från att de filer som ska importeras finns tillgängliga på en webbserver, lokalt eller online.

Processen för att migrera artiklar är enligt följande.
1. Artikel i original (html).
2. Rensa HTML från oönskade element i HTML.
    - Här finns en konsol-applikation som stöd för att identifiera element genom XPATH.
3. Konvertering till [Markdown](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html).
4. Redigera Markdown i Visual Code, korrigering med mera.
5. Exportera Markdown-filen till HTML med VS Code extension Markdown PDF.
6. Kontrollera exporterad HTML & CSS.
7. Läs in data från HTML-fil till XML-mall.
    - Görs semi-manuellt med konsol-applikation som genererar XML-koden. Kontrollera data och redigera in det som saknas och säkerställ att rätt data angivits.
8. Överför alla filer som tillhör en utgåva till webbserver för import till OJS.
9. Ladda upp XML-fil till OJS genom traditionell import.
10. kontrollera att artiklar och utgåva importerats korrekt.

## XML Mallen
Den mall som används för import till OJS är specifikt utvecklad för detta projekt.
Varje enskild utgåva har en särskilt anpassad XML-fil som endast fungerar för den utgåvan.

### Utgåva - Issues
Varje utgåva registreras med elementet `<issue>` som har ett antal attribut. Elementet omfattar hela utgåvans import-data, och är rot-elementet.

```
<issue xmlns="http://pkp.sfu.ca" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" published="1" current="1" access_status="1" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
  <id type="internal" advice="ignore">18</id>
  <description locale="en_US">&lt;div&gt;&#xD;
&lt;p&gt;This is a trial issue of an electronic version of &lt;em&gt;Information Research News&lt;/em&gt; which publishes short papers by staff and students of the Department. We are now exploring the possibility of regular publication of &lt;em&gt;Information Research&lt;/em&gt; to take over that function and would welcome comments from present subscribers to the paper version, as well as from those who have not previously subscribed.&lt;/p&gt;&#xD;
&lt;p&gt;Copyright of papers accepted for &lt;em&gt;Information Research&lt;/em&gt; rests with the author(s) and publication in this form is not intended to limit revision and subsequent publication elsewhere.&lt;/p&gt;&#xD;
&amp;nbsp;&lt;/div&gt;</description>
  <issue_identification>
    <volume>1</volume>
    <number>1</number>
    <year>1995</year>
  </issue_identification>
  <date_published>1995-04-11</date_published>
  <last_modified>2019-02-22</last_modified>
  <sections>
    <section ref="ART" seq="1" editor_restricted="0" meta_indexed="1" meta_reviewed="1" abstracts_not_required="0" hide_title="0" hide_author="0" abstract_word_count="0">
      <id type="internal" advice="ignore">1</id>
      <abbrev locale="en_US">ART</abbrev>
      <title locale="en_US">Articles</title>
    </section>
  </sections>
    <issue_covers>
    <cover locale="sv_SE">
      <cover_image>2018-omslag.png</cover_image>
      <cover_image_alt_text/>
      <embed encoding="base64"> base64-kod </embed>
    </cover>
  </issue_covers>
  Här fortsätter sedan artiklar i utgåvan.
```  
### Författare 
Inom elementet `<authors>` lägger man in alla författare i ett eget `<author>`-element. 
```
<authors xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
    <author include_in_browse="true" user_group_ref="Author">
        <firstname>Förnamn</firstname>
        <lastname>Willett</lastname>
        <affiliation locale="en_US">Department of Information Studies, University of Sheffield</affiliation>
        <country>UK</country>
        <email>p.willett@shef.ac.uk</email>
    </author>
</authors>
```
### Filer tillhörande utgåvan
Alla artiklar importeras genom att OJS importerar de HTML-filer som utgör artikeln. Dessa HTML-filer måste importeras via extern källa, eller annan mapp på den webbserver där OJS är installerad. Varje fil registreras i ett `<submission_file>`-element. Utöver detta behöver alla filer även registreras i ett `<article_galley>`-element med ett id som referens.

### Article Galleys
Article galleys fungerar som index för Open Journal System, de länkar ihop supplementary_file med submission_file så att dessa länkas rätt till artiklar i utgåvan. Elementet har stöd för att ange olika språkkoder för att visa filnamn i olika språk (front end). 

```
  <article_galley xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" approved="false" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
    <id type="internal" advice="ignore">33</id>
    <name locale="en_US">Diagram</name>
    <seq>7</seq>
    <submission_file_ref id="43" revision="1"/>
  </article_galley>
```