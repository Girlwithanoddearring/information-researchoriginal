#### vol. 17 no. 1, March 2012

# Degree of Internet corporate reporting: a research framework

#### 
[Pak-Lok Poon](#authors)  
School of Accounting and Finance, The Hong Kong Polytechnic University, Hung Hom, Hong Kong  
[Yuen Tak Yu](#authors)  
Department of Computer Science, City University of Hong Kong, Kowloon Tong, Hong Kong

#### Abstract

> **Introduction.** This paper is concerned with the extent to which Internet corporate reporting has been practiced by firms which use this innovation, and whether it is related to certain characteristics of these firms.  
> **Method.** We surveyed a number of theories on technology innovation/adoption, and applied them to hypothesise the relationships between several characteristics of corporate firms and their degrees of Internet corporate reporting.  
> **Analysis.** Grounded on the diffusion of innovation theory, expectation-confirmation theory, corporate governance theories and learning/experience curve theories, we conjectured that the degree of adopting Internet corporate reporting is related to the firm’s corporate governance, system openness, size, revenue, stock trading activity and experience of adoption.  
> **Results.** A generic framework is developed to study the degree of Internet corporate reporting in corporate Websites. Together with the research questions and hypotheses presented in this paper, our work forms the essential theoretical basis on which empirical studies can be performed.  
> **Conclusions.** Our research framework and its associated research questions/hypotheses are generic and, hence, are useful to researchers for follow-up theoretical or empirical studies on the adoption of Internet corporate reporting in a variety of contexts.

## Introduction

Corporate Websites provide a major platform for firms to transmit information to customers and conduct transactions in business markets ([Dou _et al._ 2002](#dou02); [Huizingh 2000](#hui00); [Robbins and Stylianou 2003](#rob03); [Usunier _et al._ 2009-10](#usu0910)). In commonly known business-to-business and business-to-consumer e-commerce models, firms typically use the Internet to market their products to organisational and individual customers, respectively. To date, many studies have been performed on Website design and content in business-to-business ([Chakraborty _et al._ 2003](#cha03); [Lord and Collins 2002](#lor02); [Usunier _et al._ 2009-10](#usu0910)) and business-to-consumer ([Kim and Moon 1998](#kim98); [Loiacono _et al._ 2007](#loi07); [Palmer 2002](#pal02); [Poon and Lau 2006](#poo06)).

This study investigates a more recent e-commerce model which has emerged during the last decade. Many firms are using the Web to disseminate up-to-date corporate information to a global audience almost instantly ([Wong and Poon 2008](#won08)). We often witness that traditional press releases and company reports refer the reader to the corporate Websites for additional information. A practice known as _Internet corporate reporting_, the dissemination of corporate information via company Websites has now become a worldwide phenomenon ([Poon _et al._ 2003](#poo03)). The focus of our present study is the extent of current practice of disseminating corporate information through the Internet (instead of the hardcopy medium, such as publishing the firm’s annual report in paper form) in relation to certain characteristics of the firms.

Firms that are engaged in Internet corporate reporting attempt to make use of their corporate Websites not only to market their products to ordinary consumers ([Poon and Lau 2006](#poo06); [Su 2007](#su07); [Wang _et al._ 2009](#wan09)) or organisational customers (in the case of business-to-business) ([Chakraborty _et al._ 2003](#cha03); [Lord and Collins 2002](#lor02)), but also to market _the firms themselves_, that is, to raise the awareness and interests of shareholders and investors in the firms ([Ashbaugh _et al._ 1999](#ash99); [Poon _et al._ 2003](#poo03)).

Internet corporate reporting can offer tremendous benefits to commercial firms. For example, they may use such reporting to boost their corporate image by controlling the context in which data are presented, emphasising the positive and providing interpretation for potentially negative information. In so doing, firms can use their corporate Websites to shape information on their own terms and to improve and develop sustainable relationships with stakeholders ([Young and Benamati 2000](#you00)). In addition, corporate Websites often reflect the firms’ strategic intent to use the Internet to share information, facilitate transactions, communicate with different stakeholders and improve customer service ([Meroño-Cerdán and Soto-Acosta 2007](#mer07)).

Undoubtedly, a key component of corporate information is financial information such as profit and loss. By disseminating a variety of financial information over the corporate Websites, firms can ‘market’ themselves to _retail investors_ (individuals investing for their own accounts) by providing them with quality and timely information ([Wong and Poon 2008](#won08)). _Internet financial reporting_ is almost invariably an integral part of Internet corporate reporting ([Poon _et al._ 2003](#poo03)).

In view of the well known benefits that Internet corporate reporting may bring about, we expect that many firms have set up their own corporate Websites and, hence, are _Web-present_. Moreover, we postulate that many Web-present firms have engaged Internet corporate/financial reporting to their benefit, in which case we shall call them _Internet corporate reporting firms/Internet financial reporting firms_, respectively. To distinguish between firms which adopted this innovation minimally from those which have embraced this technology and utilised it to a much fuller extent, we define the _degree of Internet corporate reporting_ (which reflects the extent of adopting the practice of Internet corporate reporting) in terms of the _richness_ of corporate information disclosed on the firm’s Website. Our premise is that the richness of corporate information disclosed on a firm’s Website reflects the commitment, effort, resources and level of strategic significance attached to Internet corporate reporting by the company.

Since financial information is a key component of corporate information, and financial reporting is of particular interest to investors when they look into corporate reporting, our work also considers the degree of Internet financial reporting, where appropriate, in parallel with the degree of Internet corporate reporting. Hence, in this paper, even though we only refer to _Internet corporate reporting_ in section headings, research questions and hypotheses, our proposed research framework is also relevant to the investigation of Internet financial reporting whenever financial information is pertinent to the attribute under discussion.

Our work is grounded on the _diffusion of innovation theory_ ([Baskerville and Pries-Heje 2001](#bas01); [Carter _et al._ 2001](#car01); [Moore and Benbasat 1991](#moo91); [Rogers 2003](#rog03)), the _expectation-confirmation theory_ (also known as _expectancy-disconfirmation theory_ or _expectation-disconfirmation theory_) ([Bhattacherjee 2001](#bha01); [Chea and Luo 2008](#che08); [Hsu _et al._ 2004](#hsu04); [Lin _et al._ 2005](#lin05); [Oliver and Swan 1989](#oli89)), some well known _corporate governance theories_ ([Claessens 2006](#cla06); [Cohen _et al._ 2004](#coh04); [Karamanou and Vafeas 2005](#kar05)) and the _learning/experience curve theories_ ([Chang 1996](#cha96); [Henderson 1973](#hen73); [Lieberman 1987](#lie87)). In a nutshell, our review of these theories suggests the following specific research questions with respect to the degree of Internet corporate reporting:

*   **(RQ1)** How popular is Internet corporate reporting in corporate firms nowadays?
*   (**RQ2)** What are the degrees of Internet corporate reporting among the Web-present firms?
*   **(RQ3)** Is the degree of Internet corporate reporting related to the following characteristics of a firm: (a) corporate governance, (b) system openness, (c) size, (d) revenue, (e) stock trading activity and (f) previous experience of adopting this reporting practice?

We now outline the rationale of posing the above research questions and will leave the detailed elaboration of the theoretical underpinning to the next section.

Basically, the diffusion of innovation theory ([Baskerville and Pries-Heje 2001](#bas01); [Carter _et al._ 2001](#car01); [Moore and Benbasat 1991](#moo91); [Rogers 2003](#rog03)) argues that people (or organisations) have different degrees of willingness to adopt innovations (including new technologies), and that the rate of adoption depends on several determinants such as Relative Advantage and Compatibility. Considering Internet corporate reporting as an innovation, we observe that these determinants will contribute to a high degree of adopting such a reporting practice. Thus, there are reasons to expect that Internet corporate reporting is very popular in corporate firms nowadays (RQ1) and that the degree of such reporting should be generally high (RQ2).

Corporate governance theories ([Claessens 2006](#cla06); [Cohen _et al._ 2004](#coh04); [Karamanou and Vafeas 2005](#kar05)) state that there is a positive relationship between corporate governance and voluntary dissemination of company information, at both the country and individual firm levels. While these corporate governance theories are formulated primarily for company information dissemination in hardcopy format (such as a firm’s annual report), they do provide the rationale for anticipating a similar relationship between corporate governance and the degree of Internet corporate reporting (RQ3(a)).

Rogers ([2003](#rog03)) found that the openness of a firm’s reporting systems is positively correlated with its organisational innovativeness, which is a major factor contributing to a higher rate of adoption. Thus, we postulate that the openness of a firm’s systems is positively correlated with the degree of Internet corporate reporting (RQ3(b)).

Previous studies ([Martins and Oliveira 2008](#mar08); [Usunier _et al._ 2009-10](#usu0910)) found that larger firms have more financial resources and greater access to technical knowledge and expertise to adopt an innovation. Moreover, firms with larger revenues and higher stock trading activities normally attract more investors’ interests and, hence, are more inclined to disseminate corporate information (traditionally through hardcopy annual reports) ([Gompers and Metrick 2001](#gom01)). We therefore postulate that the size, revenue and stock trading activity of a firm will have a positive impact on its degree of Internet corporate reporting (RQ3(c)-(e)).

According to the expectation-confirmation theory ([Bhattacherjee 2001](#bha01); [Chea and Luo 2008](#che08); [Hsu _et al._ 2004](#hsu04); [Lin _et al._ 2005](#lin05); [Oliver and Swan 1989](#oli89)), if the product performs better than expected, positive disconfirmation would occur, resulting in the continual use of that product. Also, the learning/experience curve theories ([Chang 1996](#cha96); [Henderson 1973](#hen73); [Lieberman 1987](#lie87)) stipulate that the more often a task is performed, the lower will be the cost of doing it. Adapting these theories to our current study, we posit that previous experience of adopting Internet corporate reporting is positively correlated to the degree of such reporting (RQ3(f)).

From the next section onwards, we will first briefly summarise the essence of the relevant theories. Based on these theories, we elaborate how we derive a research model and a number of testable hypotheses from the three research questions we put forward above. We then conclude this paper with suggestion of further work to validate the proposed hypotheses.

The contribution of this paper is threefold. First, it provides an elaborate survey of theories relevant to the study of the adoption of Internet corporate reporting. Second, by virtue of these theories, the paper identifies several characteristics of firms that potentially affect their adoption of Internet corporate reporting. As a result, a set of hypotheses is put forward that can be empirically validated. Third, the paper proposes a generic framework to study the degree of Internet corporate reporting in corporate Websites. Our research framework and its associated research questions/hypotheses are generic and, hence, are useful to researchers for follow-up theoretical or empirical studies on the adoption of Internet corporate reporting in a variety of contexts.

## Theoretical background

Here we present the theoretical underpinning of our proposed research model; the diffusion of innovation theory ([Baskerville and Pries-Heje 2001](#bas01); [Carter _et al._ 2001](#car01); [Moore and Benbasat 1991](#moo91); [Rogers 2003](#rog03)), the expectation-confirmation theory ([Bhattacherjee 2001](#bha01); [Chea and Luo 2008](#che08); [Hsu _et al._ 2004](#hsu04); [Lin _et al._ 2005](#lin05); [Oliver and Swan 1989](#oli89)), corporate governance theories ([Claessens 2006](#cla06); [Cohen _et al._ 2004](#coh04); [Karamanou and Vafeas 2005](#kar05)) and the learning/experience curve theories ([Chang 1996](#cha96); [Henderson 1973](#hen73); [Lieberman 1987](#lie87)).

### Diffusion of innovation theory

#### Rogers's original theory

Originally put forward by Rogers ([2003](#rog03)), the diffusion of innovation theory argues that an _innovation_ (which refers to an idea, practice or object that is perceived as new by an individual or other unit of adoption) progresses through different communication channels over time among the members of a social system. _Diffusion_ is a special type of communication involving messages about an innovation. Individuals are observed as having different degrees of willingness to adopt innovations and the portion of the population adopting an innovation is approximately normally distributed over time ([Rogers 2003](#rog03)). Dividing this normal distribution into segments results in the segregation of individuals into five categories of individual innovativeness (from earliest to latest adopters): innovators, early adopters, early majority, late majority and laggards. When the adoption curve is converted to a cumulative percentage curve, a characteristic S-curve is generated that represents the rate of adoption of an innovation within the population.

Rogers ([2003](#rog03)) found that there are five determinants of the rate of adopting an innovation as follows.

1.  _Relative advantage:_ the degree to which an innovation is perceived to be better than its precursor.
2.  _Compatibility:_ the degree to which an innovation is perceived to be consistent with the existing values, needs and past experiences of potential adopters.
3.  _Trialability:_ the degree to which an innovation may be experimented with before adoption.
4.  _Observability:_ the degree to which the results of an innovation are visible to others.
5.  _Complexity:_ the degree to which an innovation is perceived to be difficult to use.

Determinants (a) to (d) above are generally positively correlated with the rate of adoption, while determinant (e) is generally negatively correlated with the rate of adoption ([Rogers 2003](#rog03)). Not all innovations have comparable rates of adoption. Whilst some consumer innovations, such as mobile phones, only took a few years to reach widespread adoption in the U.S., other ideas, such as the metric system or seat belts in vehicles, required decades to reach popular use ([Rogers 2003](#rog03)).

An individual’s decision about an innovation is not an instantaneous act. Rather, it is a _process_ that occurs over time and consists of a sequence of actions. The current model of the innovation-decision process, as presented in Rogers's classical book on diffusion of innovation ([2003](#rog03)), consists of five stages from the earliest to the latest: knowledge, persuasion, decision, implementation and confirmation.

#### Diffusion of innovation theory in information technology

Refining and expanding upon the five determinants proposed by Rogers ([2003](#rog03)), Moore and Benbasat ([1991](#moo91)) generated eight factors that impact on information technology adoption. The factors, called _perceived characteristics of innovation_ (or simply _perceived characteristics_), are: (1) _Relative advantage_, (2) _Compatibility_, (3) _Trialability_, (4) _Image_, (5) _Voluntariness_, (6) _Ease of use_, (7) _Visibility_ and (8) _Result demonstrability_.

Compared with Rogers's theory, we note the following.

*   The first three perceived characteristics (_Relative advantage_, _Compatibility_ and _Trialability_) are similar in meaning to the corresponding determinants of Rogers.
*   The two perceived characteristics, _Image_ and _Voluntariness_, are new: they did not appear among Rogers's five determinants. _Image_ refers to the degree to which the use of an innovation is perceived to enhance one’s image or status in one’s social system, while _Voluntariness_ refers to the degree to which the use of an innovation is perceived to be voluntary or of free will.
*   The perceived characteristic, _Ease of use_, replaces Complexity. _Ease of Use_ refers to the degree to which one perceives that adopting an innovation would be free of physical and mental effort.
*   The last two related perceived characteristics, _Visibility_ and _Result demonstrability_, replace _Observability_. _Visibility_ refers to the degree to which the _idea_ of the innovation itself is visible, while _Result demonstrability_ refers to the degree to which the _results_ of an innovation are visible and communicable to others.

Moore and Benbasat ([1991](#moo91)) observed that all these eight perceived characteristics are positively correlated with the rate of adopting an information technology innovation. For ease of discussion, from hereon, we will refer to both Rogers's determinants and the perceived characteristics proposed by Moore and Benbasat simply as ‘perceived characteristics’.

### Expectation-confirmation theory

#### Expectation-confirmation theory in marketing

The expectation-confirmation theory has been widely used in the consumer behaviour literature to study customer satisfaction, post-purchase behaviour and service marketing in general ([Chea and Luo 2008](#che08); [Hsu _et al._ 2004](#hsu04); [Lin _et al._ 2005](#lin05); [Oliver and Swan 1989](#oli89)). According to the theory, consumers form expectations of a product (or service) prior to purchasing it. Subsequent post-purchase usage then reveals to the consumer the actual performance of the product. The consumer then compares this post-purchase evaluation with his/her pre-purchase expectation. If the product performs better than expected (perceived actual performance > expected performance), _positive disconfirmation_ will occur. This in turn leads to consumer satisfaction and strengthens the consumer’s attitudes towards the product. If, however, the product performs less well than expected (perceived actual performance < expected performance), _negative disconfirmation_ occurs. In the latter case, future dispositions towards purchasing the product may be weakened and the consumer may search for other products.

#### Expectation-confirmation theory in information technology

Bhattacherjee ([2001](#bha01)) argued that users’ decision to continue using Web-based applications is similar to consumers’ decision to continue re-purchasing a product or service, because both decisions follow an initial (purchase or acceptance) decision, are influenced by the experience of initial use (of a product or a Web-based application), and can potentially result in _ex post_ reversal of the initial decision. Bhattacherjee ([2001](#bha01)) then integrated the technology acceptance model ([Gefen _et al._ 2003](#gef03); [Shih 2004](#shi04); [Vijayasarahty 2004](#vij04); [Wixom and Todd 2005](#wix05)) with the expectation-confirmation theory to offer an explanation for one’s intention to continue using online banking. He found that ‘users’ continuance intention is determined by perceived usefulness [a salient belief comprising the technology acceptance model] of IS [information system] continued use and their satisfaction with IS use’ ([Hsu _et al._ 2004: 768](#hsu04)). Note that user satisfaction is influenced by perceived usefulness and their confirmation of expectation from prior IT use.

### Corporate governance theories

_Corporate governance_ is concerned with assuring that the conduct of a corporation is carried out in accordance with corporate laws and regulations. More specifically, it refers to the set of processes, customs, policies, laws and institutions affecting the way a corporation is directed, administered, monitored or controlled. It has been reported that firms with good IT governance achieve up to 20% premium return on their IT investments ([Weill and Ross 2005](#wei05)). Inspired by a wave of widely-publicised corporate scandals including Enron, Global Crossing, Tyco and WorldCom, governance practices have been brought back under the spotlight since 2001\. In 2002 the U.S. Federal Government passed the Sarbanes-Oxley Act, intending to restore public confidence in corporate governance ([Velichety _et al._ 2007](#vel07)). More recently, following the collapse of Lehman Brothers, there has been a loud voice for the board of directors at every financial institution to exercise independent and better corporate governance, instead of being just an extension of the chief executive officer’s friendship network ([Williams 2010](#wil10)).

Several studies ([Claessens 2006](#cla06); [Cohen _et al._ 2004](#coh04); [Karamanou and Vafeas 2005](#kar05)) have been conducted to investigate the relationship between corporate governance and _voluntary_ dissemination of company information at both the country and individual firm levels. For example, when a country’s overall corporate governance is weak (such as in mainland China), voluntary dissemination of company information is found to be fairly limited ([Claessens 2006](#cla06)). At the firm’s level, it is found that the level of corporate governance is positively related to the level of company information dissemination (often in hardcopy format) ([Karamanou and Vafeas 2005](#kar05)) and financial reporting quality ([Cohen _et al._ 2004](#coh04)). The underlying rationale is that a firm with weak corporate governance is often less transparent in its business management and operations. Thus, such a firm is less inclined to disclose company information to outsiders on a voluntary basis.

### Learning/experience curve theories

The learning curve theory ([Lieberman 1987](#lie87)) and the closely related experience curve theory ([Chang 1996](#cha96); [Henderson 1973](#hen73)) express the relationship between experience and efficiency, or between efficiency gains and investment in the effort. The learning curve theory ([Lieberman 1987](#lie87)) states that the more often a task has been performed, the less time will be needed on each subsequent iteration. Furthermore, as the quantity of items produced doubles, labour costs decrease at a predictable rate.

Compared with the learning curve theory, the experience curve theory is wider in scope, encompassing more than just labour time and cost. According to the experience curve theory ([Chang 1996](#cha96); [Henderson 1973](#hen73)), the more often a task is performed, the lower will be the cost of doing it. Each time cumulative volume doubles, value added costs (including administration, marketing, distribution and manufacturing) fall by a constant and predictable percentage.

The strategists of the Boston Consulting Group examined the consequences of the learning/experience effects for businesses. They concluded that because relatively low cost of operations is a powerful strategic advantage, firms should capitalise on these learning/experience effects ([Henderson 1973](#hen73)). The underlying rationale is that increased activity contributes to increased learning, which leads to lower costs/prices, which can in turn result in increased market share, profitability and market dominance.

## Degree of Internet corporate reporting

The main theme of our study is to develop a research framework, which includes a theoretical model and concrete hypotheses, for the study of the degree of Internet corporate reporting. In doing so, while attempting to apply the diffusion of innovation and expectation-confirmation theories, we find it more useful to adapt the notion of adoption to our study context, as explained below.

First, as stated in Rogers's classical book on diffusion of innovation ([Rogers 2003: 21](#rog03)), the decision stage in the innovation-decision process has only two outcomes: (a) _adoption_, that is, ‘a decision to make _full_ use of an innovation as the best course of action available’, and (b) _rejection_, that is, ‘a decision _not_ to adopt an innovation’. In the context of our study, if a firm is not Web-present (that is, it has no corporate Website for information dissemination), then it is obvious that the firm has not adopted the practice of Internet corporate reporting. When a firm is Web-present, then it invariably disseminates at least some corporate information (such as company profile or product information) that becomes easily accessible by shareholders and investors to shape their impressions and potentially influence their investment decisions. That is, all Web-present firms can be considered to have adopted Internet corporate reporting to a certain extent. Thus, the simplest way for our investigation is to categorise firms into two types: Web-present and non-Web-present, corresponding to adoption and rejection of Internet corporate reporting, respectively, regardless of the richness of information disseminated on the firm’s Website.

However, we note that once a firm sets up its own corporate Website, it has the discretion of deciding what information to disseminate on it, or what strategies it uses when designing the contents of the Website. It may also decide to develop the site incrementally, starting with some basic information such as the firm’s contact information and profile, and later putting in more and richer information (such as its profit and loss account or even share price analysis) phase by phase or as the need arises. Thus, at any instant, some Web-present firms disseminate little corporate information online, while others make extensive use of the Web in disclosing a rich amount of corporate information. The reality is that different Web-present firms adopt Internet corporate reporting to a different extent and, hence, exhibit substantially different degrees of Internet corporate reporting. In other words, the simple dichotomy of firms into two categories (Web-present versus non-Web-present) cannot adequately capture the reality of their diversity in terms of the richness of online information dissemination, that is, the degree of Internet corporate reporting.

For these reasons, our study focuses on the _degree of Internet corporate reporting_ of firms, which reflects their _degree of adoption_ of such a reporting practice rather than merely distinguishing full adoption versus rejection. After all, there are currently no agreed and convincing objective criteria for determining, say, how much information disseminated through the Web would constitute the _full_ use of Internet corporate reporting by a Web-present firm.

Readers should carefully distinguish between our proposed notion of _degree of adoption_ and the concept of _rate of adoption_ developed in previous work, such as by Rogers ([2003](#rog03)) or Moore and Benbasat ([1991](#moo91)). Simply speaking, the _degree_ of adoption examines how _extensively_ an innovation is adopted by a unit of adoption, whereas the _rate_ of adoption addresses how _fast_ an innovation is adopted (regardless of its extent) once that innovation appears.

Furthermore, the expectation-confirmation theory addresses the question of whether a user will continue using a product, an application system or a technology. A direct application of the expectation-confirmation theory would be to verify whether the subject firms under study did continue their use of Internet corporate reporting after initial adoption. Instead, we extend this idea further to incorporate the notion of the degree of adopting Internet corporate reporting. Hence, our study examines the possible change in the degree of Internet corporate reporting of a firm over time after having adopted such a reporting practice.

From the literature review, the investigation of the degree of adopting Internet corporate reporting in firms, in the context of diffusion of innovation and expectation-confirmation, is a seldom-visited (or even yet-to-be explored) area. In this regard, our present work to adapt the diffusion of innovation and expectation-confirmation theories is novel and it potentially opens up a new means of developing an understanding of the present state of Internet corporate reporting of firms that adopt this innovation.

## Research model and hypothesis development

We formulate our hypotheses by drawing on theories of diffusion of innovation, expectation-confirmation, corporate governance and learning/experience curves, with necessary adaptations as described where appropriate.

### Degree of Internet corporate reporting

Considering that Web adoption is already very popular and it is believed that many firms are trying to take advantage of the opportunities offered by the Internet, we hypothesise that the degree of Internet corporate reporting should be high for most corporations.

> **_H1_ [_degree of Internet corporate reporting_]:** _The degree of Internet corporate reporting is generally high among Web-present firms._

H1 is also inspired by the diffusion of innovation theory: we observe that all the perceived characteristics have a positive impact on the rate of adopting Internet corporate reporting in commercial firms. Consider, for instance, Relative Advantage and Ease of Use. With regard to Relative Advantage, Internet corporate reporting is obviously much better than its precursor, that is, hardcopy-based annual reporting, in terms of various aspects such as distribution cost, global reach and timeliness of reported information. With regard to Ease of Use, using a firm’s Website for disseminating corporate information is no longer technically difficult nor prohibitively expensive. Although these perceived characteristics mainly concern the rate of adoption, we consider that they also have a positive impact on the degree of Internet corporate reporting in Web-present firms. Despite the strong reasons to believe that H1 should hold, from a scientific point of investigation, we still put forward the conjecture that H1 should be verified empirically, to avoid any potential bias.

### Corporate governance

The relationship between corporate governance and voluntary dissemination of company information as reported in the literature ([Claessens 2006](#cla06); [Cohen _et al._ 2004](#coh04); [Karamanou and Vafeas 2005](#kar05)) is mainly based on the ‘traditional’ reporting channel: the firm’s annual report, which is often in hardcopy format. Although not every country/region has imposed regulatory requirements demanding listed firms to disseminate their corporate information online, Internet corporate reporting does represent a cost-effective way of voluntary reporting. Thus, we posit that the positive association between corporate governance and voluntary reporting in the form of traditional annual reports should also apply to Internet corporate reporting, leading to the following hypotheses.

> **_H2a_ [_corporate governance - Web presence_]:** _Firms with strong corporate governance are more likely to be Web-present._  
> **_H2b_ [_corporate governance - degree of Internet corporate reporting_]:** _The level of corporate governance of a Web-present firm is positively associated with its degree of Internet corporate reporting._

### System openness

_System openness_ is defined as the degree to which the members of a system are linked to other individuals who are external to the system. It was found to be positively correlated with _organisational innovativeness_, defined as the degree to which a firm is relatively earlier in adopting new ideas than others and known to be a major factor contributing to a higher rate of adoption ([Rogers 2003](#rog03)). Monopoli ([2006](#mon06)) argued that the internal potential of a firm cannot be properly used without a clear understanding of the emerging trends and, in particular, the unmet customer needs and insights. In this regard, system openness measures how far ahead a firm can envision and whether it is capable of scanning the business periphery, exploiting relevant but weak signals in the environment.

Along the above reasoning, if we consider system openness to have a positive effect on the degree of Internet corporate reporting, then the two hypotheses below naturally follow.

> **_H3a_ [_system openness - Web presence_]:** _Firms with a higher level of system openness are more likely to be Web-present._  
> **_H3b_ [_system openness - degree of Internet corporate reporting_]:** _The level of system openness of a Web-present firm is positively associated with its degree of Internet corporate reporting._

### Firm’s size, revenue and stock trading activity

Some early diffusion of innovation studies ([Mahler and Rogers 1999](#mah99); [Rogers 2003](#rog03)) observed that larger organisations have a higher rate of adopting an innovation because they are usually more innovative. A common explanation for this observation is that larger organisations have more financial resources and greater access to technical knowledge and expertise to adopt an innovation ([Martins and Oliveira 2008](#mar08)). For example, Usunier _et al._ ([2009-10](#usu0910)) argue that firm size has an impact on resources and capabilities in the design of Websites and their contents and, hence, also affects the way a company uses its Website in its marketing efforts. Inspired by these observations, we put forward the following two hypotheses.

> **_H4a_ [_size - Web presence_]:** _Larger firms are more likely to be Web-present._  
> **_H4b_ [_size - degree of Internet corporate reporting_]:** _The size of a Web-present firm is positively associated with its degree of Internet corporate reporting._

It is obvious that informed investors are usually eager to know a listed firm’s revenue before making their decisions to invest in the firm (such as through buying its stocks). Similarly to size, larger revenues will provide firms with more resources to overcome financial constraints when they adopt an innovation. Moreover, Gompers and Metrick ([2001](#gom01)) observed that firms with larger revenues normally attract more investors’ interests and, hence, are more inclined to disseminate relevant, accurate and timely corporate information to investors. This kind of information is undoubtedly important to investors when they make their investment decisions. Thus, we state the following hypotheses.

> **_H5a_ [_revenue - Web presence_]:** _Firms with higher revenues are more likely to be Web-present._  
> **_H5b_ [_revenue - degree of Internet corporate reporting_]:** _The revenue of a Web-present firm is positively associated with its degree of Internet corporate reporting._

Let us turn to stock trading activity. Investors are among those more interested in getting detailed corporate information of listed firms, traditionally through hardcopy annual reports and now more easily from corporate Websites of Web-present firms. Thus, listed firms with higher levels of stock trading activity should be more inclined to disseminate corporate information online. This results in the following hypotheses.

> **_H6a_ [_stock trading activity - Web presence_]:** _Firms with higher stock trading activities are more likely to be Web-present._  
> **_H6b_ [_stock trading activity - degree of Internet corporate reporting_]:** _The level of stock trading activity of a Web-present firm is positively associated with its degree of Internet corporate reporting._

### Experience of adoption

Applying expectation-confirmation theory, if a firm found that the benefits derived from the initial adoption of Internet corporate reporting exceeded the expectation, positive disconfirmation would occur, resulting in the continual use of such reporting as an information dissemination channel. In addition, like the adoption of any other technology, Web-present firms progress through a learning/experience curve ([Chang 1996](#cha96); [Henderson 1973](#hen73); [Lieberman 1987](#lie87)) in adopting Internet corporate reporting. At an earlier stage of the adoption process, firms generally have little experience and technical knowledge of adopting Internet corporate reporting, such as designing the layout of Web pages and format of reported information, and building navigation hyperlinks across different Web pages. It is quite unsurprising that some firms may opt not to devote substantial effort in building their corporate Websites for an extensive use of online reporting. We reason that as the benefits of Internet corporate reporting are realised, and the experience and technical knowledge of firms are accumulated along with the adoption process, these firms will engage in more extensive and sophisticated uses of Internet corporate reporting, hopefully to further improve their competitive edge in this aspect. Our reasoning leads to the following hypothesis.

> **_H7_ [_experience of adoption - degree of Internet corporate reporting_]:** _Previous experience of adopting Internet corporate reporting of a Web-present firm is positively associated with the degree of such reporting._

Figure 1 summarises our research model that incorporates the major potential determinants affecting the degree of Internet corporate reporting.

<div align="center">![Figure 1\. Research model of degree of Internet corporate reporting](p509fig1.png)</div>

<div align="center">  
**Figure 1\. Research model of degree of Internet corporate reporting**</div>

## Related work

### Web adoption and content

Beatty _et al._ ([2001](#bea01)) conducted a survey of 286 medium-to-large Web-present firms in the U.S. They observed that the reasons why these firms decided to adopt Web technology varied significantly, depending on when the adoption decision was made. For example, early adopters placed significantly more emphasis on perceived benefits for having a Website than the laggards. Early adopters also viewed the use of the Web as being more compatible with their current organisational processes and systems, as well as their existing technological infrastructures.

Based on evidence from 10 firms, Mehrtens _et al._ ([2001](#meh01)) constructed a model of Internet adoption by small and medium enterprises. Their study concluded that three factors significantly affect Internet adoption by these firms: perceived benefits, organisational readiness and external pressure. Teo and Pian ([2004](#teo04)) introduced a model for Web adoption and examined the characteristics (features) of Websites of different adoption levels, from the lowest to the highest: Web presence, prospecting, business integration and business transformation. Note that the above studies actually investigated the issues of Web adoption and content in a wider context (not restricted to Internet corporate reporting) and, hence, it would be difficult to directly compare their studies with ours.

### Adoption of Internet corporate/financial reporting

Ashbaugh _et al._ ([1999](#ash99)) performed a survey of 290 listed firms in the U.S. Their survey showed that firms engaging in Internet financial reporting generally have reputations for excellent corporate reporting practices. In addition, whilst 70% of the firms engaged in Internet financial reporting, there was substantial variation in the quality of their Internet financial reporting practices. In our study, however, we have extended the research focus from Internet financial reporting to Internet corporate reporting and, hence, considered a broader perspective compared to previous work. In addition, Wong and Poon ([2008](#won08)) discussed in detail the related control issues that arise when a firm attempts to use its corporate Website to fulfil the requirement of Regulation Fair Disclosure. This initiative was specifically introduced by the U.S. Securities and Exchange Commission to prevent selective information dissemination or actions that might benefit one investor over another.

## Conclusion and further work

Our paper has established a research framework for studying the degree of Internet corporate reporting in corporate Websites. Based on this framework, we have also developed a set of research questions and hypotheses to investigate the relationship between some characteristics of firms and the richness of corporate information content disseminated at their Websites.

Our research framework is generic and, hence, should be applicable to a variety of contexts. Further work is needed to empirically validate our proposed research model and its associated hypotheses. To perform such a study by applying the framework, it will be necessary to define operational metrics or proxies for the variables that appear in the research model. For example, it is necessary to quantify the degree of Internet corporate reporting by a metric and identify operationally measurable proxies for the firm’s size, revenue and stock trading activity. We have performed such an empirical study on the listed firms in Hong Kong over a three-year period. Our results will be reported and discussed in a subsequent paper ([Poon and Yu 2012](#poo12)). We believe that similar studies can be replicated elsewhere for other types of firms or by reusing and adapting the generic framework which we have developed here in this paper.

## Acknowledgements

We would like to thank all those people who have helped review and edit this paper.

## About the authors

Pak-Lok Poon is an Associate Professor in the School of Accounting and Finance, The Hong Kong Polytechnic University, Hong Kong. He received his Master of Business (Information Technology) from Royal Melbourne Institute of Technology, Australia and his PhD in software engineering from The University of Melbourne, Australia. He can be contacted at: afplpoon@inet.polyu.edu.hk.

Yuen Tak Yu is an Associate Professor in the Department of Computer Science, City University of Hong Kong, Hong Kong. He received his Bachelor’s degree in Mathematics from The University of Hong Kong, Hong Kong and his PhD in software engineering from The University of Melbourne, Australia. He can be contacted at: csytyu@cityu.edu.hk.

#### References

*   Ashbaugh, H., Johnstone, K.M. & Warfield, T.D. (1999). Corporate reporting on the Internet. _Accounting Horizons_, **13**(3), 241-257.
*   Baskerville, R.L. & Pries-Heje, J. (2001). A multiple-theory analysis of a diffusion of information technology case. _Information Systems Journal_, **11**(3), 181-212.
*   Beatty, R.C., Shim, J.P. & Jones, M.C. (2001). Factors influencing corporate Web site adoption: a time-based assessment. _Information & Management_, **38**(6), 337-354.
*   Bhattacherjee, A. (2001). Understanding information systems continuance: an expectation-confirmation model. _MIS Quarterly_, **25**(3), 351-370.
*   Carter, F.J., Jr., Jambulingam, T., Gupta, V.K. & Melone, N. (2001). Technological innovations: a framework for communicating diffusion effects. _Information & Management_, **38**(5), 277-287.
*   Chakraborty, G., Lala, V. & Warren, D. (2003). What do customers consider important in B2B Web sites? _Journal of Advertising Research_, **43**(1), 50-61.
*   Chang, T-L. (1996). Cultivating global experience curve advantage on technology and marketing capabilities. _International Marketing Review_, **13**(6), 22-42.
*   Chea, S. & Luo, M.M. (2008). Post-adoption behaviors of e-service customers: the interplay of cognition and emotion. _International Journal of Electronic Commerce_, **12**(3), 29-56.
*   Claessens, S. (2006). Corporate governance and development. _World Bank Research Observer_, **21**(1), 91-122.
*   Cohen, J., Krishnamoorthy, G. & Wright, A. (2004). The corporate governance mosaic and financial reporting quality. _Journal of Accounting Literature_, **23**(1), 87-152.
*   Dou, W., Nielsen, U.O. & Tan, C.M. (2002). Using corporate Websites for export marketing. _Journal of Advertising Research_, **42**(5), 105-115.
*   Gefen, D., Karahanna, E. & Straub, D.W. (2003). Trust and TAM in online shopping: an integrated model. _MIS Quarterly_, **27**(1), 51-90.
*   Gompers, P.A. & Metrick, A. (2001). Institutional investors and equity prices. _Quarterly Journal of Economics_, **116**(1), 229-259.
*   Henderson, B. (1973). _[The experience curve - reviewed - IV. The growth share matrix.](http://www.webcitation.org/5x0Af1eu4)_ Retrieved 7 March, 2011 from http://www.bcg.com/documents/file13904.pdf (Archived by WebCite® at http://www.webcitation.org/5x0Af1eu4)
*   Hsu, M.H., Chiu, C.M. & Ju, T.L. (2004). Determinants of continued use of the WWW: an integration of two theoretical models. _Industrial Management & Data Systems_, **104**(8-9), 766-775.
*   Huizingh, E.K.R.E. (2000). The content and design of Web sites: an empirical study. _Information & Management_, **37**(3), 123-134.
*   Karamanou, I. & Vafeas, N. (2005). The association between corporate boards, audit committees, and management earnings forecasts: an empirical analysis. _Journal of Accounting Research_, **43**(3), 453-486.
*   Kim, J. & Moon, J.Y. (1998). Designing towards emotional usability in customer interfaces — trust-worthiness of cyber-banking system interfaces. _Interacting with Computers_, **10**(1), 1-29.
*   Lieberman, M.B. (1987). The learning curve, diffusion, and competitive strategy. _Strategic Management Journal_, **8**(5), 441-452.
*   Lin, C.S., Wu, S. & Tsai, R.J. (2005). Integrating perceived playfulness into expectation-confirmation model for web portal context. _Information & Management_, **42**(5), 683-693.
*   Loiacono, E.T., Watson, R.T. & Goodhue, D.L. (2007). WebQual: an instrument for consumer evaluation of Web sites. _International Journal of Electronic Commerce_, **11**(3), 51-87.
*   Lord, K.R. & Collins, A.F. (2002). Supplier Web-page design and organizational buyer preferences. _Journal of Business & Industrial Marketing_, **17**(2-3), 139-150.
*   Mahler, A. & Rogers, E.M. (1999). The diffusion of interactive communication innovations and the critical mass: the adoption of telecommunications services by German banks. _Telecommunications Policy_, **23**(10), 719-740.
*   Martins, F.O.M. & Oliveira, T. (2008). Determinants of information technology diffusion: a study at the firm level for Portugal. _Electronic Journal of Information Systems Evaluation_, **11**(1), 27-34.
*   Mehrtens, J., Cragg, P.B. & Mills, A.M. (2001). A model of Internet adoption by SMEs. _Information & Management_, **39**(3), 165-176.
*   Meroño-Cerdan, A.L. & Soto-Acosta, P. (2007). External Web content and its influence on organizational performance. _European Journal of Information Systems_, **16**(1), 66-80.
*   Monopoli, E. (2006). _[Evaluating and overcoming the resistors to innovation.](http://www.webcitation.org/5x0BOEp2k)_ Retrieved 7 March, 2011 from http://www.realinnovation.com/content/c061003a.asp (Archived by WebCite® at http://www.webcitation.org/5x0BOEp2k)
*   Moore, G.C. & Benbasat, I. (1991). Development of an instrument to measure the perceptions of adopting an information technology innovation. _Information Systems Research_, **2**(3), 192-222.
*   Oliver, R.L. & Swan, J.E. (1989). Equity and disconfirmation perceptions as influences on merchants and product satisfaction. _Journal of Consumer Research_, **16**(3), 372-383.
*   Palmer, J.W. (2002). Web site usability, design, and performance metrics. _Information Systems Research_, **13**(2), 151-167.
*   Poon, P-L. & Lau, A.H.L. (2006). The PRESENT B2C implementation framework. _Communications of the ACM_, **49**(2), 96-103.
*   Poon, P-L., Li, D. & Yu, Y.T. (2003). Internet financial reporting. _Information Systems Control Journal_, **1**, 42-45.
*   Poon, P-L. & Yu, Y.T. (2012). [Internet corporate reporting by listed firms in Hong Kong](http://www.webcitation.org/666dniZuz). _Information Research_, **17**(1), paper 510\. Retrieved 11 March, 2012 from http://informationr.net/ir/17-1/paper510.html (Archived by WebCite® at http://www.webcitation.org/666dniZuz).
*   Robbins, S.S. & Stylianou, A.C. (2003). Global corporate Web sites: an empirical investigation of content and design. _Information & Management_, **40**(3), 205-212.
*   Rogers, E.M. (2003). _Diffusion of innovations._ (5th ed.). New York, NY: Free Press.
*   Shih, H-P. (2004). Extended technology acceptance model of Internet utilization behavior. _Information & Management_, **41**(6), 719-729.
*   Su, B-C. (2007). Consumer e-tailer choice strategies at on-line shopping comparison sites. _International Journal of Electronic Commerce_, **11**(3), 135-159.
*   Teo, T.S.H. & Pian, Y. (2004). A model for Web adoption. _Information & Management_, **41**(4), 457-468.
*   Usunier, J-C., Roulin, N. & Ivens, B.S. (2009-10). Cultural, national, and industry-level differences in B2B Web site design and content. _International Journal of Electronic Commerce_, **14**(2), 41-87.
*   Velichety, S., Park, J., Jung, S., Lee, S. & Tanriverdi, H. (2007). Company perspectives on business value of IT investments in Sarbanes-Oxley compliance. _Information Systems Control Journal_, **1**, 42-45.
*   Vijayasarahty, L.R. (2004). Predicting consumer intentions to use on-line shopping: the case for an augmented technology acceptance model. _Information & Management_, **41**(6), 747-762.
*   Wang, K., Wang, E.T.G. & Farn, C-K. (2009). Influence of Web advertising strategies, consumer goal-directedness, and consumer involvement on Web advertisement effectiveness. _International Journal of Electronic Commerce_, **13**(4), 67-95.
*   Weill, P. & Ross, J. (2005). A matrixed approach to designing IT governance. _MIT Sloan Management Review_, **46**(2), 26-34.
*   Williams, M.T. (2010). _Uncontrolled risk: the lessons of Lehman Brothers and how systemic risk can still bring down the world financial system._ New York, NY: McGraw-Hill.
*   Wixom, B.H. & Todd, P.A. (2005). A theoretical integration of user satisfaction and technology acceptance. _Information Systems Research_, **16**(1), 85-102.
*   Wong, A. & Poon, P-L. (2008). Control issues of using corporate Web sites for public disclosure. _Information Systems Control Journal_, **5**(1), 38-40.
*   Young, D. & Benamati, J. (2000). Differences in public Web sites: the current state of large U.S. firms. _Journal of Electronic Commerce Research_, **1**(3), 94-105.