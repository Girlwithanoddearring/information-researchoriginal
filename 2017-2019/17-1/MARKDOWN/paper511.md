#### vol. 17 no. 1, March 2012

# Information resource selection of undergraduate students in academic search tasks

#### [Jee Yeon Lee](#authors)  
Department of Library and Information Science, Yonsei University, Seoul, Korea  
[Woojin Paik](#authors)  
**Department of Computer Science, Konkuk University, Chungju, Korea**  
[Soohyung Joo](#authors)  
School of Information Studies, University of Wisconsin-Milwaukee, WI, USA

#### Abstract

> **Introduction.** This study aims to investigate the selection of information sources and to identify factors associated with the resource selection of undergraduate students for academic search tasks. Also, user perceptions of some factors, such as credibility, usefulness, accessibility and familiarity, were examined to classify resources by their characteristics.  
> **Method.** A self-generated diary method was employed with 233 undergraduate students in achieving their academic search tasks.  
> **Analysis.** The collected diary data were analysed both qualitatively and quantitatively. A qualitative analysis was used to explore factors influencing resource selection, while a quantitative analysis was used for investigating the characteristics of resources.  
> **Results.** This study reaffirmed recent information usage trends that online sources are preferred by university students in their academic searches. This study identified twenty nine factors in four different dimensions, including information type, resource feature, search strategy, and interaction with others. Moreover, perceptions of users were investigated quantitatively focusing on usefulness, credibility, accessibility, familiarity with source, satisfaction, and intention of continuous use.  
> **Conclusions.** Multiple factors are influencing the resource selection of undergraduate students and those factors vary in different types of sources in academic search tasks. The findings of the study suggest some insights in how to guide undergraduates to choose better information resources for their academic search tasks.

## Introduction

Selection of information resources is one of most essential steps during the process of an information seeking task. The rapid growth of Web information technology has expanded the range of information sources available to end users. In particular, online information sources, such as search engines, Web pages and digital libraries, have increased. Recently, many researchers have addressed the substantial change in information selection, namely, the preference for online electronic sources over printed resources. Online sources are being utilised more often than traditional printed sources for academic tasks ([Tenopir _et al._ 2003](#ten03); [Barllan and Fink 2005](#bar05); [Lee _et al._ 2008](#lee08); [Xie and Joo 2009](#zie09)). A variety of factors influence the selection of information sources. These include the characteristics of the sources, search purposes, user preferences, user knowledge, information literacy and others. Research on these factors is important for understanding the breadth and depth of information source selection in various search situations and to help users choose adequate resources in line with their search objectives.

Undergraduate students, who have grown up in the digital age, have been exposed to the Internet environment since childhood, and accordingly, they are expected to exhibit unique information use patterns which engage more digital media and resources. Researchers have studied the information seeking behaviour of undergraduate students, focusing on what information sources they select and how they use the information they find in different search tasks ([Dilevko and Gottlieb 2002](#dil02); [Thompson 2003](#tho03); [Chang 2006](#cha06); [Kim and Sin 2007](#kim07); [Selwyn 2008](#sel08); [Mill 2008](#mil08); [Logan 2004](#log04)). Undergraduate students especially would have an increased need for academic information sources to complete their class assignments or research projects that usually require various types of sources and references. Understanding the undergraduate’s unique selection of information sources and the associated factors underlying their information use behaviour is necessary to support the undergraduate information search process.

This study aims to investigate the selection of information sources and to identify factors associated with the resource selection of undergraduate students for academic search tasks. Additionally, user perceptions of some important factors, such as credibility, usefulness, accessibility, and familiarity, were investigated to further examine the relationships between user perceptions and actual use. As to methodology, this is one of the few studies to employ self-generated diaries to investigate undergraduate information use in real settings. The present study not only investigated resource selection activities in real settings with self-generated tasks but also derived a range of factors behind resource selection, directly from the participants, in an unobtrusive manner.

## Selective literature review

In this literature review, previous research, that studied undergraduate information seeking behaviour and explored factors related to information source selection, was reviewed.

### **Undergraduate student information seeking behaviour**

The information seeking behaviour of undergraduate students has been a focus of research interest for decades. Previous studies investigated resource selection and preference, perceptions, search strategies, citation behaviour and others, in terms of undergraduates’ information seeking behaviour.

In the field of information science, researchers have focused more on user perceptions and search activities, than resource selection, while investigating college students. For example, Zhang _et al._ ([2005](#zha05)) attempted to examine how domain knowledge affected user search behaviour and search effectiveness. Their findings concluded that the level of domain knowledge had an influence on search behaviour but not on search effectiveness. Kim and Sin ([2007](#kim07)) studied undergraduate students’ perception and preference for different types of resources using a survey method. They found that undergraduates preferred information sources like search engines, Websites, books, online databases and journals. Also, students believed that books, encyclopedia, OPACs and librarians were trustworthy sources, while search engines, Websites, and friends/family were easy to use. Selwyn ([2008](#sel08)) focused on undergraduate Web information use behaviour for academic tasks based on a survey of 1,222 students. The study explored the different patterns of Internet resource selection considering a range of influences such as the students’ wider internet use, their access and expertise, their year of study, gender, age, ethnicity, and educational background. Mill ([2008](#mil08)) analysed citations in undergraduate research papers for intermediate and advanced courses. He found that journals were cited more frequently than books, in particular, students used electronic journals more than printed ones. In the online database search environment, Logan’s study([2004](#log04)) indicated that students had trouble in distinguishing between scholarly and non-scholarly sources of information. In addition, he reported that students had problems constructing and implementing effective search strategies.

Lubans ([1998](#lub98)) and Jones ([2002](#jon02)) surveyed the attitude of college students toward emerging Internet media and Web information. Most students believed that Web-based resources had a positive influence on college students’ academic experiences as the Web provided easily accessible information for their search tasks. Metzger _et al._ ([2003](#met03)) investigated college students’ perception of credibility in Web information. Although college students relied heavily on the Web for academic information, they were not likely to verify the credibility of the information from the Internet. Rieh and Hilligoss ([2007](#rie07)) also examined college students’ perception of credibility in using digital media and Web sources. Interviews with 24 college students revealed that they were aware of the potential problems of reliability in Web information and tried to employ several search strategies to deal with it.

Researchers have also been interested in information resource selection and use in undergraduate students. For example, Dilevko and Gottlieb ([2002](#dil02)) investigated the print source use of undergraduate students. According to their results, print resources were still regarded as vital owing to their nature of completeness, accuracy and in-depth content, although undergraduates typically relied more on online sources. Thompson’s study ([2003](#tho03)) discovered that the majority of students began a research assignment with the Internet, most often using a commercial search engine. More significantly, the study stressed the importance of information literacy skills in finding authoritative information. Burton and Chadwick ([2000](#bur00)) surveyed the Internet usage of students and found that Web documents were the main sources for students in writing research papers, while they still used library resources frequently. Ebersole ([2005](#ebe05)) reviewed research which examined student perceptions and uses of the World Wide Web for academic purposes and affirmed the positive attitude of students toward the Web. In her exploratory study of engineering and science students, Chang ([2006](#cha06)) administered a survey to investigate the use of e-books among undergraduate students and found that e-books were less frequently used than e-journals. Despite their increased popularity, e-books were not frequently selected for undergraduates’ academic tasks.

### **Factors related to resource selection**

Many researchers have identified factors and reasons for information source selection in different information seeking situations. Accessibility, availability, convenience and ease of use, which are closely related to search efficiency, are among the most frequently discussed factors in the field of information science.  
In their early study, Gerstberger and Allen ([1968](#ger68)) found a strong relationship between accessibility and frequency of resource use, based on the observations of nineteen engineers for fifteen weeks. O’Reilly ([1982](#ore82)) investigated the use frequency of four different information sources by decision makers, considering the impacts of quality and accessibility of information. For decision makers, accessibility was a more important factor than quality in selecting information sources. Fidel and Green ([2004](#fid04)) demonstrated the importance of accessibility in the selection of information sources. They examined different aspects of accessibility for engineer information source selection, including familiarity, suitable format, and different types of information. In particular, accessibility is the main concern for student users in selecting information sources. In their exploration of students’ Web source usage, Burton and Chadwick ([2000](#bur00)) found that students depended upon accessibility, easy to use and availability while choosing resources. Similarly, Pascoe _et al._ ([1996](#pas96)) also argued that ease of use, convenience and accessibility were major factors related to Internet use in academia.

Many researchers considered multiple factors comprehensively when investigating reasons underlying users’ information selecting behaviour. Chakrabarti _et al._ ([1983](#cha83)) reported that the frequency of use of information sources was related to utility, availability, and ease of use. In contrast, least frequently used information sources were perceived as being expensive and lower in ease of use, availability and utility of information. Quigley _et al._ ([2002](#qui02)) carried out a survey of 230 science faculty and researchers at the University of Michigan to investigate the importance of six factors, including speed, convenience, familiarity, currency, authoritativeness, and reliable availability, on their information resource preferences. They found that convenience was the most important factor in selecting information sources. Liu and Yang ([2004](#liu04)) identified important factors influencing selection of information resources in distance education students, such as timely information retrieval, easy access, comprehensive electronic resources, ease of use and high system performance. Tenopir and her colleagues have conducted a series of studies that investigate academic resource usage, in particular focusing on journal usage, in different fields. For example, in their survey of medical faculty and pediatricians, Tenopir _et al._ ([2004](#ten04); [2007](#ten04)) emphasized the contribution of currency, convenience, portability and reading patterns to the selection of journals. Also, in their research on scientists and engineers, Tenopir _et al._ ([2003](#ten03); [2005](#ten05)) and Allard _et al._ ([2009](#all09)) identified multiple attributes and factors related to the adoption of journals, such as convenient access, availability, links to full-text, format, least effort, timeliness, and authority. Vibert _et al._ ([2007](#vib07)) found that the time constraint of the task was another decisive factor in determining the usefulness of an online resource. Their study suggested that research scientists preferred information sources that could be efficiently accessed, as well as sources that were comprehensive. Kim and Sin ([2007](#kim07)) identified several factors associated with undergraduate information source selection, such as accessibility, ease of use, comprehensiveness, and efficiency. Their study revealed that the most important criteria in resource selection were accuracy and trustworthiness. Similarly, Rieh and Hilligoss ([2007](#rie07)) found that users regarded credibility as one of the critical resource elements when determining sources to use on the Web. Lee _et al._ ([2008](#lee08)) found that accuracy, recency, accessibility and reliability were considered most important when researchers search for information in research-oriented tasks. Xie and Joo ([2009](#xie09)) identified seventeen reasons related to information source selection in analysing sixty information-seeking episodes of 31 participants: accessibility, ease of use, comprehensiveness, interactivity, useful results, task type, familiarity with source, and many others, were identified. In addition, document format also acts as a factor influencing resource selection. Some researchers found that high school students usually preferred graphic and multimedia formats of Web documents in their information seeking ([Fidel _et al._ 1999](#fid99); [Agosto 2002](#ago02)).

Prior studies have greatly contributed to the understanding of the unique source selection behaviour of undergraduate students, as well as the identification of relevant factors in the process of information source selection. However, the existing research also has some limitations. While most of the previous studies surveyed the information seeking practices of undergraduate students, less research investigated what information sources they selected in the process of achieving their specific academic assignments in real settings. In addition, in most of the prior studies, related factors were not identified directly from the users’ actual information seeking processes. Instead, in most studies, subjects rated factors that were predefined by the researchers. Moreover, few researchers have examined the correlations between information source selection and associated factors. These limitations illustrate the need to investigate undergraduate resource selection in real settings and to identify factors affecting resource selection that are more related to achieving their own academic tasks.

## Research questions

This study intends to investigate selection of information sources and associated factors in undergraduate search tasks by addressing the following research questions:

1.  What information resources are selected by undergraduate students for academic tasks?
2.  What are the factors related to undergraduate selection of information resources for academic tasks?
3.  What are undergraduate students’ perceptions of usefulness, credibility, accessibility, familiarity, satisfaction, and intention for continued use, among different information resources? And what are the relationships between these perceptions and actual use?

## Methodology

### Data collection

To answer the three research questions, this study designed a semi-structured information seeking diary. The authors selected a diary method since it had some advantages over predominantly post-search questionnaires or interviews in this topic. It gave an in-depth description of an information-seeking episode compared to the questionnaire method. By analysing the words directly from participants, the authors were able to identify a range of factors qualitatively and extract relevant quotes to bolster the findings. Also, the diary enabled participants to keep records for the whole time of an information seeking episode, which would be difficult in post-search questionnaires or interviews. In addition, a self-generated diary approach better reflected the real situation, excluding obtrusive elements, as participants reported the information seeking tasks that occurred in real search tasks.

In this study, participants were instructed to keep an "information seeking diary" to record how they achieved a search task for academic purposes. These search tasks included class assignments, individual research, or project research. Each participant selected a search task to write in a diary as opposed to a task assigned by researchers. In this way, the authors intended to observe real field tasks from undergraduate participants. The semi-structured diary consisted mainly of the following: purpose of search task, topic, information sources used, factors in selecting the resources, and perceptions of selected resources (5-point scale measure). Participants recoded all the resources they used and justified why they selected the specific resources in the process of a search task. In addition, participants were asked to rate their perceptions of accessibility, usefulness, credibility, familiarity, satisfaction, and intention for continued use, for each selected source on a five-point Likert scale.

The participants of the diary study were undergraduate students at Yonsei University ([http://www.yonsei.ac.kr](http://www.yonsei.ac.kr)) in Seoul, Korea. In all, 233 subjects participated in the study. In order to recruit students taking account of proportions of majors in undergraduate members of Yonsei University, participants were invited from elective courses that were open to all majors and years in the spring semester 2010\. To facilitate their participation, the subjects were given extra credit incentives. Table 1 presents the proportions of participants by majors. Although the number of respondents in the humanities was relatively large, these proportions by major roughly reflect the proportions of Yonsei University students.

<table width="50%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Characteristics of participants (N=233)**  
</caption>

<tbody>

<tr>

<th>Major</th>

<th>Frequency</th>

<th>Percentage</th>

</tr>

<tr>

<td align="center">Humanities</td>

<td align="center">67</td>

<td align="center">28.8%</td>

</tr>

<tr>

<td align="center">Social sciences</td>

<td align="center">44</td>

<td align="center">18.9%</td>

</tr>

<tr>

<td align="center">Business</td>

<td align="center">6</td>

<td align="center">2.6%</td>

</tr>

<tr>

<td align="center">Sciences</td>

<td align="center">43</td>

<td align="center">18.5%</td>

</tr>

<tr>

<td align="center">Engineering</td>

<td align="center">48</td>

<td align="center">20.6%</td>

</tr>

<tr>

<td align="center">Medical sciences</td>

<td align="center">15</td>

<td align="center">6.4%</td>

</tr>

<tr>

<td align="center">Arts/ Music</td>

<td align="center">10</td>

<td align="center">4.3%</td>

</tr>

<tr>

<th>Total</th>

<th>233</th>

<th>100.0%</th>

</tr>

</tbody>

</table>

### Data analysis

Collected diaries were analysed qualitatively using content analysis based on an open coding method, which is the process of breaking down, examining, conceptualising, and categorising unstructured textual transcripts ([Glaser and Strauss 1967](#gla67); [Strauss and Corbin 1990](#str90)). A coding scheme of factors related to source selection (see Table 3 below) was created by the researchers. Since the diary data included the detailed description of reasons why participants selected a certain source, the authors were able to easily extract a multitude of factors affecting resource selection directly from users’ vocabulary. Then, the identified reasons were used to construct the coding scheme for resource selection factors, which is needed to answer the second research question. The coders were then instructed to identify, from the diary transcripts, factors of source selection using the predefined coding scheme. To ensure the reliability of the coding process, inter-coder reliability was examined using Holsti’s ([1969](#hol69)) inter-coder reliability index. To test the inter-coder reliability, two coders coded thirty-five randomly selected diaries, which comprised about 15% of total participants. The inter-coder reliability of this study was 0.91, according to Holsti's index.

Along with content analysis, the present study also applied some quantitative methods to investigate users’ perceptions. Users’ perceptions of usefulness, credibility, accessibility, familiarity, satisfaction and intention for continued use, were investigated for each selected source using a five-point scale. Then, resources observed in the diaries were compared with each other in terms of these six user perceptions. Additionally, the correlation among these six perceptions was examined. Furthermore, similarities among the resources were explored with respect to credibility, usefulness, and actual use. The relationships among sources were presented on a two dimensional multi-dimensional scaling (MDS) map to determine which resources had similar characteristics

## Results

The findings of this study were organized to answer the three proposed research questions: 1) types of information sources selected by undergraduates; 2) factors for the selection of information sources; and 3) perceptions of usefulness, credibility, accessibility, familiarity, satisfaction, and intention for continued use for different information resources.

### Resource selections of undergraduates in academic tasks

To investigate the types of resources used, participants were asked to record every information source they used while achieving their information seeking tasks. Table 2 presents frequency of uses of resources selected by participants in the process of search tasks. Online resources (67.1%) were more frequently selected by undergraduate students than the other types of sources, such as human resources (18.4%), printed resources (11.5%) and mass media broadcasting (3.0%). The results of the present study reaffirmed recent information usage trends, revealing that online sources are preferred in searching for information ([Tenopir _et al._ 2003](#ten03); [Barllan and Fink 2005](#ten03); [Kim and Sin 2007](#kim07); [Thomson 2007](#tho07); [Vibert _et al._ 2007](#vib07);  [Lee _et al._ 2008](#lee08); [Xie and Joo 2009](#xie09)).

Online sources consisted of about 67% of all selected resources. Search engines, such as Google, Naver ([http://www.naver.com](http://www.naver.com)), and Daum ([http://www.daum.net](http://www.daum.net)), were the most frequently used sources of information, accounting for 16.8%. Participants usually selected search engines as a source to begin a search task. Web pages, including individual and organizational Web pages, accounted for 16.1%. In particular, participants preferred individual Web pages to organizational ones (10.2% and 5.9%, respectively). Scholarly online databases, which provide electronic journal articles, proceedings, and theses (e.g. DBPIA ([http://www.dbpia.co.kr](http://www.dbpia.co.kr/)); EBSCOhost; KISTI-NDSL), were third in frequency of use among online sources, accounting for 8.3%. Wikipedia, social question & answer services (e.g. Naver Q&A, Yahoo Answers), online newspapers/magazines and digital libraries accounted for 5.5%, 5.2%, 5.2%, and 4.5%, respectively. College student report-sharing sites, which are commercial services for users uploading and downloading their individual papers to share mostly class assignment reports (e.g. Happycampus.com ([http://www.happycampus.com](http://www.happycampus.com)); Sooheng.com ([http://www.sooheng.com](http://www.sooheng.com/))), were also frequently used, accounting for 2.3%.

Human resources accounted for 18.4% of information source usage, which is higher than that for printed sources (11.5%). Among the human resources, colleagues and friends were the most frequently used sources (8.0%), followed by professors and lecturers (7.0%) and family members (1.8%). However, experts and professionals, and librarians were less frequently selected at 1.3% and 0.2%, respectively. In particular, of 233 participants, only three reported librarians as a source in their information seeking tasks.

Print materials were less frequently used than online and human resources. Only 11.5% of resources selected in all the diaries were print resources. Books were the most frequently used resource in the print-source category, accounting for 7.6%. Research reports, white papers and policy reports were second in this category. Since many periodicals, articles and news stories are available through online channels, participants were not likely to use print resources. Mass-media sources were the least frequently employed, and only 3.0% of participants reported using television, including internet-based television broadcasts, in their search processes.

<table width="80%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Resources selected by undergraduates in their academic tasks**  
</caption>

<tbody>

<tr>

<th width="19%">Dimensions</th>

<th width="43%">Resource type</th>

<th width="19%">Frequency</th>

<th width="19%">Percentage</th>

</tr>

<tr>

<th rowspan="6">Human resources</th>

<td>Experts and professionals</td>

<td align="center">17</td>

<td align="center">1.3%</td>

</tr>

<tr>

<td>Professors and lecturers</td>

<td align="center">92</td>

<td align="center">7.0%</td>

</tr>

<tr>

<td>Librarians</td>

<td align="center">3</td>

<td align="center">0.2%</td>

</tr>

<tr>

<td>Colleagues and friends</td>

<td align="center">105</td>

<td align="center">8.0%</td>

</tr>

<tr>

<td>Family</td>

<td align="center">24</td>

<td align="center">1.8%</td>

</tr>

<tr>

<th align="left">Sub-total</th>

<th>241</th>

<th>18.4%</th>

</tr>

<tr>

<th rowspan="6">Printed resources</th>

<td>Books (monographs)</td>

<td align="center">99</td>

<td align="center">7.6%</td>

</tr>

<tr>

<td>Magazines</td>

<td align="center">8</td>

<td align="center">0.6%</td>

</tr>

<tr>

<td>Research reports, white papers and policy reports</td>

<td align="center">31</td>

<td align="center">2.4%</td>

</tr>

<tr>

<td>Lecture notes</td>

<td align="center">4</td>

<td align="center">0.3%</td>

</tr>

<tr>

<td>Other printed resources (e.g., newspapers, brochures, maps, etc.)</td>

<td align="center">8</td>

<td align="center">0.6%</td>

</tr>

<tr>

<th align="left">Sub-total</th>

<th>150</th>

<th>11.5%</th>

</tr>

<tr>

<th rowspan="13">Online resources</th>

<td>Search engines</td>

<td align="center">220</td>

<td align="center">16.8%</td>

</tr>

<tr>

<td>Individual Web pages</td>

<td align="center">134</td>

<td align="center">10.2%</td>

</tr>

<tr>

<td>Organizational Web pages</td>

<td align="center">77</td>

<td align="center">5.9%</td>

</tr>

<tr>

<td>Institutional repository</td>

<td align="center">29</td>

<td align="center">2.2%</td>

</tr>

<tr>

<td>Digital libraries</td>

<td align="center">59</td>

<td align="center">4.5%</td>

</tr>

<tr>

<td>Wikipedia or online encyclopedia</td>

<td align="center">72</td>

<td align="center">5.5%</td>

</tr>

<tr>

<td>Online news and magazines</td>

<td align="center">68</td>

<td align="center">5.2%</td>

</tr>

<tr>

<td>Scholarly online databases</td>

<td align="center">109</td>

<td align="center">8.3%</td>

</tr>

<tr>

<td>Google scholar</td>

<td align="center">9</td>

<td align="center">0.7%</td>

</tr>

<tr>

<td>College student report sharing site</td>

<td align="center">30</td>

<td align="center">2.3%</td>

</tr>

<tr>

<td>Social Q&A services</td>

<td align="center">68</td>

<td align="center">5.2%</td>

</tr>

<tr>

<td>Other online resources (online lectures, etc.)</td>

<td align="center">3</td>

<td align="center">0.2%</td>

</tr>

<tr>

<th align="left">Sub-total</th>

<th>878</th>

<th>67.1%</th>

</tr>

<tr>

<th rowspan="2">Mass media</th>

<td>Television (including Internet-based television broadcasting)</td>

<td align="center">39</td>

<td align="center">3.0%</td>

</tr>

<tr>

<th align="left">Sub-total</th>

<th>39</th>

<th>3.0%</th>

</tr>

<tr>

<th colspan="2">Total</th>

<th>1308</th>

<th>100.0%</th>

</tr>

</tbody>

</table>

This study also examined how many different types of resources were selected simultaneously in one search task. The diaries show that participants utilised more than one source to achieve their single search task. Diaries revealed that each participant employed 5.28 different sources on average in his/her task (median=5, standard deviation=1.61). The range of number of resources selected was between two and ten. No participants finished their search task using only one single resource item.

<div align="center">![Figure 1: Number of sources selected in a single search task](p511fig1.jpg)</div>

<div align="center">  
**Figure 1: Number of sources selected in a single search task**</div>

### Factors of selection of information resources by undergraduates

Based on an open coding analysis of the diaries, twenty-eight factors were identified in relation to the selection of information sources. The factors identified herein are derived from the participants’ diary records in achieving their self-generated search tasks. These factors are associated with four dimensions, which included information types, resource features, search strategies, and interactions with others. Table 3 indicates dimensions, factors, definitions and corresponding quotes from the diaries. The findings validate many of the factors identified in the previous research mentioned in the literature review section. These factors were accessibility, ease of use, coverage, reliability, free access, in-depth nature, familiarity, recency and user experience, among others.

More importantly, the results suggested more factors that were not identified in previous research. For example, the present study specifies such factors of source selection as practical, scholarly, basic idea, historical, exemplary and summary information in the dimension of information types. Such knowledge was what the participants wanted to find. Resource features were the most widely reported factors in relation to source selection. This study identified 10 resource features including credibility, coverage, ease of access, recency and free access. In addition, this study explored the factors associated with search strategies. The authors attempted to find relevant factors while analysing the users’ unique search strategies, such as exploring further, narrowing the focus, and advanced searching. This study also paid attention to interactions between participants and human resources that occurred during the information search tasks.

<table width="80%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Factors of resource selection of undergraduates for their academic tasks**  
</caption>

<tbody>

<tr>

<th>Dimension</th>

<th>Factor</th>

<th>Definition/Objective</th>

<th>Example (quotes)</th>

</tr>

<tr>

<td rowspan="7" align="center">Information type</td>

<td>Practical information</td>

<td>The source provides practical information on a specific topic.</td>

<td>  [Expert] “he gave me practical information in the field”</td>

</tr>

<tr>

<td>In-depth professional or scholarly information</td>

<td>The source provides professional or scholarly information on a specific topic.</td>

<td>  [Online database] “I wanted to obtain in-depth professional materials in relation to the topic I am searching for”</td>

</tr>

<tr>

<td>Basic ideas or concepts</td>

<td>The source provides basic or introductory information on a specific topic.</td>

<td>  [Book] “I just tried to get some introductory knowledge about a gold alkaline nano stick”</td>

</tr>

<tr>

<td>Longitudinal information/ Historical reviews</td>

<td>The source provides longitudinal or historical information on a specific topic.</td>

<td>  [Book] “This book offers chronicles of elementary education in Korea”</td>

</tr>

<tr>

<td>Statistical data</td>

<td>The source provides statistics related to a specific topic.</td>

<td>  [organizational Website] “The site accumulates statistics data concerning aging society and population.”</td>

</tr>

<tr>

<td>Examples/ Specific cases</td>

<td>The source provides a specific example or case on a topic.</td>

<td>  [Newspaper] I reviewed several news articles to find examples of elderly offenders.”</td>

</tr>

<tr>

<td>Well organized summaries</td>

<td>The source contains a well-organized summary of a specific topic.</td>

<td>  [Lecture note] “This lecture note summarises overall information about stem cells comprehensively.”</td>

</tr>

<tr>

<td rowspan="10" align="center">Resource feature</td>

<td>Credibility</td>

<td>The information from the source is correct and reliable.</td>

<td>  [Research report] “(I selected the research report) to obtain most authoritative and accurate scholarly information related to that disease.”</td>

</tr>

<tr>

<td>Coverage</td>

<td>The source covers a broad area of materials that others might not cover.</td>

<td> [Book] “(This book) covers various viewpoints and comprehensive discussions in the topic.”</td>

</tr>

<tr>

<td>Ease of understanding</td>

<td>The source is easily understandable.</td>

<td>  [Colleague] “(my colleague) explained clearly and easily what I didn’t understand.”</td>

</tr>

<tr>

<td>Accessibility</td>

<td>The source is easily accessible.</td>

<td>  [Family] “As initial search, I wanted to ask my family, since they are living with me.”</td>

</tr>

<tr>

<td>Recency</td>

<td>The source covers recently updated information.</td>

<td>  [Newspaper] “I can grasp recent issues and trends in the research topic.”</td>

</tr>

<tr>

<td>Portability</td>

<td>The source can be easily taken to another location.</td>

<td> [Book] “I can read this book easily anywhere and anytime I want…”</td>

</tr>

<tr>

<td>Efficiency</td>

<td>The source is quick to find a relevant item.</td>

<td>  [Search engine] “(through the search engine) I can access the Web page I want to see promptly”</td>

</tr>

<tr>

<td>Ease of use</td>

<td>The source is easy to use for obtaining information.</td>

<td>  [Search engine] “I can search easily using Naver”</td>

</tr>

<tr>

<td>Language</td>

<td>Language of the source.</td>

<td>  [Google Scholar] “I used Google Scholar to look for relevant articles written in English.”</td>

</tr>

<tr>

<td>Free access</td>

<td>The source allows users to use it for free.</td>

<td>  [Online database] “I can download research articles for free (from this online database) on campus access.”</td>

</tr>

<tr>

<td rowspan="5" align="center">Search strategy</td>

<td>Exploring further information</td>

<td>A user uses the source to further explore related information on the selected topic.</td>

<td>  [Online database] “I attempted to further explore related articles based on scanning reference lists.”</td>

</tr>

<tr>

<td>Narrowing the focus</td>

<td>A user uses the source to narrow the coverage of information on a specific topic.</td>

<td>  [Search engine] “I selected the search engine to narrow the focus of research using more specific search terms.”</td>

</tr>

<tr>

<td>Access to multiple resources</td>

<td>A user uses the source to access multiple sources simultaneously.</td>

<td>  [Search engine] “Using Naver (search engine), I could find and access multiple relevant items at one time.”</td>

</tr>

<tr>

<td>Serendipity</td>

<td>A user finds unexpected information while using the source.</td>

<td>  [Wikipedia] “I found something new unexpectedly while clicking through related links in Wikipedia.</td>

</tr>

<tr>

<td>Advanced search</td>

<td>A user uses the advance search functions in the selected source.</td>

<td>  [Digital libraries] “Yonsei Digital Library is very useful to search what I wanted using advanced functions of search options.”</td>

</tr>

<tr>

<td rowspan="5" align="center">Interactions with other people</td>

<td>Advice on an information search process</td>

<td>A user uses the source to obtain advice on the information search process</td>

<td>  [Professor] “The professor told me where I can find related articles for the assignment.”</td>

</tr>

<tr>

<td>Advice on a task</td>

<td>A user solicits advice on the task from the source.</td>

<td>  [Professor] “The professor directed the research plan in our project.”</td>

</tr>

<tr>

<td>Feedback</td>

<td>A user uses the source to get some feedback.</td>

<td>  [Professor] “The professor confirmed if the articles that I found were relevant to the topic.”</td>

</tr>

<tr>

<td>Comparison</td>

<td>A user compares others’ findings or works with their own.</td>

<td>  [Report sharing site] “I used Happycampus (report sharing site) to compare what other students have done for similar class assignment.”</td>

</tr>

<tr>

<td>Recommended by others</td>

<td>A user selected the source because it was recommended by others.</td>

<td>  [Institutional repository] “I learned this site (institutional repository) from the teaching assistant in the class.”</td>

</tr>

</tbody>

</table>

<section style="margin-left:15%; margin-right:15%;">

Using content analysis, the factors mentioned by the participants in the diaries were tallied for each type of resource. Tables 4, 5, 6 and 7 present the frequencies of factors mentioned and the corresponding percentages of each resource. For less frequently selected sources, such as librarian, lecture notes, magazines, and Google Scholar, the percentages of related factors were not calculated due to the limited frequencies of the coding results. In many cases, multiple factors acted simultaneously in selecting a specific resource. For instance, one participant indicated his reasons for selecting an institutional repository as “I can obtain accurate and recent information about Korea’s aging population from this site (institutional repository), and also it provides some related statistical data.”  This example involved three different types of factors, credibility, recency, and statistical data, in selecting one specific source, according to the coding scheme in Table 3.

Table 4 shows the factors related to types of information in resource selection. While participants selected experts/professionals and individual Websites for practical information, they selected professors/lecturers, research reports, institutional repositories, digital libraries, and online databases for obtaining in-depth professional or scholarly information. Books and Wikipedia are usually selected because they provide basic ideas or concepts. organizational sites are chosen for statistical data, while newspapers are frequently selected to get examples or specific cases.

</section>

<table width="90%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Factors for selecting each resource type: information type**  
</caption>

<tbody>

<tr>

<th width="20%" rowspan="2"> </th>

<th colspan="7">Dimension: Information Type</th>

</tr>

<tr>

<th>Practical  
information</th>

<th>In-depth  
professional  
or scholarly  
information</th>

<th>Basic ideas  
or concepts</th>

<th>Longitudinal  
information</th>

<th>Statistical  
data</th>

<th>Examples or  
specific cases</th>

<th>Well  
organized  
summaries</th>

</tr>

<tr>

<td>Experts & professionals</td>

<td align="center">8 (33.3%)</td>

<td align="center">3 (12.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.2%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Professors & lecturers</td>

<td align="center">3 (2.9%)</td>

<td align="center">18 (17.4%)</td>

<td align="center">6 (5.8%)</td>

<td align="center">1 (1.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Librarians</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Colleagues & friends</td>

<td align="center">8 (6.5%)</td>

<td align="center">4 (3.3%)</td>

<td align="center">7 (5.7%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">6 (4.9%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Family</td>

<td align="center">1 (4.3%)</td>

<td align="center">1 (4.3%)</td>

<td align="center">1 (4.3%)</td>

<td align="center">1 (4.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.3%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Books</td>

<td align="center">5 (4.4%)</td>

<td align="center">12 (10.5%)</td>

<td align="center">20 (17.5%)</td>

<td align="center">7 (6.1%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">8 (7.0%)</td>

<td align="center">9 (7.9%)</td>

</tr>

<tr>

<td>Magazines</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">3</td>

<td align="center">0</td>

</tr>

<tr>

<td>Research reports</td>

<td align="center">0 (0.0%)</td>

<td align="center">8 (38.1%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.8%)</td>

<td align="center">1 (4.8%)</td>

<td align="center">1 (4.8%)</td>

</tr>

<tr>

<td>Lecture notes</td>

<td align="center">0</td>

<td align="center">2</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">2</td>

</tr>

<tr>

<td>Search engines</td>

<td align="center">1 (0.5%)</td>

<td align="center">1 (0.5%)</td>

<td align="center">11 (5.9%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">4 (2.2%)</td>

<td align="center">1 (0.5%)</td>

</tr>

<tr>

<td>Individual Web pages</td>

<td align="center">15 (16.5%)</td>

<td align="center">4 (4.4%)</td>

<td align="center">5 (5.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.1%)</td>

<td align="center">11 (12.1%)</td>

<td align="center">10 (11.0%)</td>

</tr>

<tr>

<td>Organizational Web pages</td>

<td align="center">8 (10.0%)</td>

<td align="center">4 (5.0%)</td>

<td align="center">3 (3.8%)</td>

<td align="center">5 (6.3%)</td>

<td align="center">15 (18.8%)</td>

<td align="center">6 (7.5%)</td>

<td align="center">2 (2.5%)</td>

</tr>

<tr>

<td>Institutional repository</td>

<td align="center">0 (0.0%)</td>

<td align="center">15 (37.5%)</td>

<td align="center">2 (5.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (2.5%)</td>

<td align="center">1 (2.5%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Digital libraries</td>

<td align="center">0 (0.0%)</td>

<td align="center">8 (20.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Wikipedia</td>

<td align="center">0 (0.0%)</td>

<td align="center">3 (4.9%)</td>

<td align="center">20 (32.8%)</td>

<td align="center">1 (1.6%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (3.3%)</td>

<td align="center">5 (8.2%)</td>

</tr>

<tr>

<td>Newspapers</td>

<td align="center">3 (5.8%)</td>

<td align="center">1 (1.9%)</td>

<td align="center">2 (3.8%)</td>

<td align="center">1 (1.9%)</td>

<td align="center">2 (3.8%)</td>

<td align="center">11 (21.2%)</td>

<td align="center">1 (1.9%)</td>

</tr>

<tr>

<td>Scholarly online database</td>

<td align="center">1 (1.2%)</td>

<td align="center">20 (24.7%)</td>

<td align="center">2 (2.5%)</td>

<td align="center">1 (1.2%)</td>

<td align="center">2 (2.5%)</td>

<td align="center">3 (3.7%)</td>

<td align="center">3 (3.7%)</td>

</tr>

<tr>

<td>Google Scholar</td>

<td align="center">0</td>

<td align="center">3</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Report sharing site</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.2%)</td>

<td align="center">2 (8.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">3 (12.5%)</td>

</tr>

<tr>

<td>Social Q&A</td>

<td align="center">2 (3.5%)</td>

<td align="center">1 (1.8%)</td>

<td align="center">3 (5.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">5 (8.8%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Television</td>

<td align="center">1 (4.2%)</td>

<td align="center">2 (8.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">4 (16.7%)</td>

<td align="center">0 (0.0%)</td>

</tr>

</tbody>

</table>

<section style="margin-left:15%; margin-right:15%;">

Table 5 reveals the relationships between resource features and resource selection. Experts/professionals, research reports, organizational Websites, and institutional repositories are considered credible while selecting resources. In contrast, few participants considered colleagues/friends, family, search engines, individual Websites, and social question and answer sites as credible. Use of search engines and digital libraries was related to resource coverage. The participants considered colleagues/friends, family, search engines and individual Websites as easily accessible sources. Newspapers were frequently selected for recency of information. In terms of efficiency, the participants thought they could obtain information easily and quickly using search engines and social question and answer sites.

</section>

<table width="100%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Selection factors for each resource type: resource feature**  
</caption>

<tbody>

<tr>

<th width="16%" rowspan="2"> </th>

<th colspan="10">Dimension: Resource feature</th>

</tr>

<tr>

<th>Credibility</th>

<th>Coverage</th>

<th>Ease of under-  
standing</th>

<th>Accessibility</th>

<th>Recency</th>

<th>Portability</th>

<th>Efficiency</th>

<th>Ease of use</th>

<th>Language</th>

<th>Free access</th>

</tr>

<tr>

<td>Experts & professionals</td>

<td align="center">5 (20.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.2%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Professors & lecturers</td>

<td align="center">3 (2.9%)</td>

<td align="center">2 (1.9%)</td>

<td align="center">2 (1.9%)</td>

<td align="center">4 (3.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Librarians</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Colleagues & friends</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (1.6%)</td>

<td align="center">4 (3.3%)</td>

<td align="center">14 (11.4%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">5 (4.1%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (0.8%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Family</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.3%)</td>

<td align="center">9 (39.1%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (8.7%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Books</td>

<td align="center">15 (13.2%)</td>

<td align="center">17 (14.9%)</td>

<td align="center">3 (2.6%)</td>

<td align="center">4 (3.5%)</td>

<td align="center">1 (0.9%)</td>

<td align="center">2 (1.8%)</td>

<td align="center">2 (1.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Magazines</td>

<td align="center">0</td>

<td align="center">2</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Research reports</td>

<td align="center">4 (19.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">3 (14.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Lecture notes</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">2</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Search engines</td>

<td align="center">0 (0.0%)</td>

<td align="center">32 (17.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">22 (11.9%)</td>

<td align="center">1 (0.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">26 (14.1%)</td>

<td align="center">33 (17.8%)</td>

<td align="center">4 (2.2%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Individual Web pages</td>

<td align="center">0 (0.0%)</td>

<td align="center">10 (11.0%)</td>

<td align="center">2 (2.2%)</td>

<td align="center">10 (11.0%)</td>

<td align="center">2 (2.2%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (2.2%)</td>

<td align="center">1 (1.1%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.1%)</td>

</tr>

<tr>

<td>Organizational Web pages</td>

<td align="center">26 (32.5%)</td>

<td align="center">2 (2.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">3 (3.8%)</td>

<td align="center">3 (3.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Institutional repository</td>

<td align="center">6 (15.0%)</td>

<td align="center">4 (10.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">6 (15.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (2.5%)</td>

<td align="center">1 (2.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Digital libraries</td>

<td align="center">3 (7.5%)</td>

<td align="center">11 (27.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">5 (12.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">3 (7.5%)</td>

<td align="center">3 (7.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (2.5%)</td>

</tr>

<tr>

<td>Wikipedia</td>

<td align="center">4 (6.6%)</td>

<td align="center">6 (9.8%)</td>

<td align="center">3 (4.9%)</td>

<td align="center">2 (3.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.6%)</td>

<td align="center">3 (4.9%)</td>

<td align="center">1 (1.6%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Newspapers</td>

<td align="center">3 (5.8%)</td>

<td align="center">4 (7.7%)</td>

<td align="center">1 (1.9%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">22 (42.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Scholarly online database</td>

<td align="center">6 (7.4%)</td>

<td align="center">9 (11.1%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">4 (4.9%)</td>

<td align="center">7 (8.6%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">6 (7.4%)</td>

<td align="center">3 (3.7%)</td>

<td align="center">1 (1.2%)</td>

<td align="center">3 (3.7%)</td>

</tr>

<tr>

<td align="center">Google Scholar</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

</tr>

<tr>

<td>Report sharing site</td>

<td align="center">3 (12.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Social Q&A</td>

<td align="center">1 (1.8%)</td>

<td align="center">5 (8.8%)</td>

<td align="center">2 (3.5%)</td>

<td align="center">5 (8.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">6 (10.5%)</td>

<td align="center">9 (15.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Television</td>

<td align="center">2 (8.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (8.3%)</td>

<td align="center">1 (4.2%)</td>

<td align="center">5 (20.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.2%)</td>

<td align="center">1 (4.2%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

</tbody>

</table>

<section style="margin-left:15%; margin-right:15%;">

Table 6 illustrates the factors related to search strategies in selecting resources. Search strategies are less related to resource selection than other dimensions. The participants used Wikipedia and online databases to find further information. In particular, some participants mentioned that they clicked through hyperlinks offered by Wikipedia to extend their search topics. Several participants tried to narrow the search focus based on query reformulation when using search engines. In addition, in three cases, professor resources helped the participants focus their topics in their search tasks. One interesting finding was that five participants encountered some useful information unexpectedly while watching TV and two found some related information serendipitously while randomly surfing Wikipedia. Advanced search function aids are also associated with resource selection in some information retrieval systems, such as search engines, digital libraries, and online databases.

</section>

<table width="80%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: Selection factors of for each resource type: search strategy**  
</caption>

<tbody>

<tr>

<td rowspan="2" align="center"> </td>

<th colspan="5">Dimension: search strategy</th>

</tr>

<tr>

<th>Exploring  
further  
information</th>

<th>Narrowing  
focus</th>

<th>Access to  
multiple  
resources</th>

<th>Serendipity</th>

<th>Advanced  
search</th>

</tr>

<tr>

<td>Experts & professionals</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Professors & lecturers</td>

<td align="center">3 (2.9%)</td>

<td align="center">3 (2.9%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Librarians</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Colleagues & friends</td>

<td align="center">2 (1.6%)</td>

<td align="center">2 (1.6%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Family</td>

<td align="center">1 (4.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Books</td>

<td align="center">1 (0.9%)</td>

<td align="center">3 (2.6%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Magazines</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Research reports</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Lecture notes</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Search engines</td>

<td align="center">7 (3.8%)</td>

<td align="center">7 (3.8%)</td>

<td align="center">34 (18.4%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (0.5%)</td>

</tr>

<tr>

<td>Individual Web pages</td>

<td align="center">1 (1.1%)</td>

<td align="center">2 (2.2%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Organizational Web pages</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (2.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Institutional repository</td>

<td align="center">1 (2.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Digital libraries</td>

<td align="center">2 (5.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (2.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (5.0%)</td>

</tr>

<tr>

<td>Wikipedia</td>

<td align="center">5 (8.2%)</td>

<td align="center">1 (1.6%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (3.3%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Newspapers</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Scholarly online database</td>

<td align="center">5 (6.2%)</td>

<td align="center">1 (1.2%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (2.5%)</td>

</tr>

<tr>

<td>Google Scholar</td>

<td align="center">2</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Report sharing site</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.2%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Social Q&A</td>

<td align="center">2 (3.5%)</td>

<td align="center">1 (1.8%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.8%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Television</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">5 (20.8%)</td>

<td align="center">0 (0.0%)</td>

</tr>

</tbody>

</table>

<section style="margin-left:15%; margin-right:15%;">

Table 7 shows the factors related to interactions with others. These factors are closely associated with human resources. Experts/professionals and professors/lecturers are involved in all three advice-related factors. Twenty-five participants asked professors/lecturers how to carry out information searches in fulfilling their academic tasks. In addition, professors/lecturers were the main source for students soliciting advice on how to conduct their academic tasks. This result implies that students have frequent interactions with professors and lecturers in their academic tasks. Colleagues and friends were also selected frequently as sources for guidance or advice.

In addition, the results showed that undergraduates were likely to select information sources to compare their findings or work with others. In particular, the participants frequently utilised report sharing sites to compare their class reports with similar ones by other students. Some resources, in particular books, were selected because of recommendations from others.  

</section>

<table width="80%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7: Factors of selection for each resource type: advice and interactions with others  
**  
</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="5">Dimension: Search strategy</th>

</tr>

<tr>

<th>Advice on  
search process</th>

<th>Advice on a task</th>

<th>Feedback</th>

<th>Comparison</th>

<th>Recommended  
by others</th>

</tr>

<tr>

<td>Experts & professionals</td>

<td align="center">4 (16.7%)</td>

<td align="center">3 (12.5%)</td>

<td align="center">2 (8.3%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Professors & lecturers</td>

<td align="center">25 (24.0%)</td>

<td align="center">20 (19.2%)</td>

<td align="center">12 (11.5%)</td>

<td align="center">1 (1.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Librarians</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Colleagues & friends</td>

<td align="center">15 (12.2%)</td>

<td align="center">16 (13.0%)</td>

<td align="center">1 (0.8%)</td>

<td align="center">34 (27.6%)</td>

<td align="center">2 (1.6%)</td>

</tr>

<tr>

<td>Family</td>

<td align="center">5 (21.7%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.3%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Books</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (0.9%)</td>

<td align="center">4 (3.5%)</td>

</tr>

<tr>

<td>Magazines</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

</tr>

<tr>

<td>Research reports</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (4.8%)</td>

<td align="center">1 (4.8%)</td>

</tr>

<tr>

<td>Lecture notes</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Search engines</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Individual Web pages</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">13 (14.3%)</td>

<td align="center">1 (1.1%)</td>

</tr>

<tr>

<td>organizational Web pages</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Institutional repository</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (2.5%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (2.5%)</td>

</tr>

<tr>

<td>Digital libraries</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (2.5%)</td>

</tr>

<tr>

<td>Wikipedia</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (3.3%)</td>

</tr>

<tr>

<td>Newspapers</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.9%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Scholarly online database</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">2 (2.5%)</td>

</tr>

<tr>

<td>Google Scholar</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Report sharing site</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">15 (62.5%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Social Q&A</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">1 (1.8%)</td>

<td align="center">13 (22.8%)</td>

<td align="center">0 (0.0%)</td>

</tr>

<tr>

<td>Television</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

<td align="center">0 (0.0%)</td>

</tr>

</tbody>

</table>

<section style="margin-left:15%; margin-right:15%;">

### Undergraduate users’ perceptions of information resources

Finally, perceptions of undergraduate users were analysed quantitatively in relation to six selected variables: usefulness, credibility, accessibility, familiarity, satisfaction and intention for continued use. The participants were requested to rate these six variables for each selected source using a five-point Likert scale (Table 8). As to usefulness of sources, experts (4.42), professors (4.32), librarians (4.50), research reports (4.33), institutional repositories (4.41) and online DBs (4.45) were perceived as useful by the subjects, who rated them at over 4.3\. On the contrary, colleagues/friends (3.36), family members (3.29), magazines (3.29), report sharing sites (3.47), social question & answer sites (3.42) and television broadcasts (3.41) were rated relatively low in terms of usefulness. Regarding credibility, professors (4.69), librarians (4.50), research reports (4.50), lecture notes (5.00), organizational Websites (4.74), institutional repositories (4.76) and online DBs (4.63) were highly scored. In contrast, colleagues/friends (3.17), family members (3.48), search engines (3.11), individual Websites (3.04), report sharing sites (3.37) and social question & answer sites (2.62) were regarded as less reliable sources. Accessibility showed different rating patterns from credibility. That is, credible sources were considered less accessible. Colleagues/friends (4.40), family (4.90), lecture notes (4.50), search engines (4.79), Wikipedia (4.40) and Google Scholar (4.33) were considered as highly accessible sources, whereas experts (2.25) and research reports (2.88) were least accessible. Familiarity showed a pattern similar to accessibility of sources. Colleagues/friends (4.48), family (4.95), search engines (4.65) and television broadcasts (4.09) were perceived as familiar to the participants; whereas experts (2.50), librarians (1.50), research reports (2.67), organizational Websites (2.60) and institutional repositories (2.55) were not.

</section>

<table width="80%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 8: Undergraduate student perceptions of usefulness, credibility, accessibility, familiarity, satisfaction and intention for continued use for different information resources**  
</caption>

<tbody>

<tr>

<th height="25"> </th>

<th colspan="2">Usefulness</th>

<th colspan="2">Credibility</th>

<th colspan="2">Accessibility</th>

<th colspan="2">Familiarity</th>

<th colspan="2">Satisfaction</th>

<th colspan="2">Intention for continued use</th>

</tr>

<tr>

<th> </th>

<th width="6%" align="center">M</th>

<th width="6%" align="center">SD</th>

<th width="6%" align="center">M</th>

<th width="6%" align="center">SD</th>

<th width="6%" align="center">M</th>

<th width="6%" align="center">SD</th>

<th width="6%" align="center">M</th>

<th width="6%" align="center">SD</th>

<th width="6%" align="center">M</th>

<th width="6%" align="center">SD</th>

<th width="6%" align="center">M</th>

<th width="6%" align="center">SD</th>

</tr>

<tr>

<td>Expert</td>

<td align="center">4.42</td>

<td align="center">1.16</td>

<td align="center">4.25</td>

<td align="right">1.22</td>

<td align="center">2.25</td>

<td align="center">1.22</td>

<td align="center">2.50</td>

<td align="center">1.24</td>

<td align="center">4.08</td>

<td align="center">1.24</td>

<td align="center">3.92</td>

<td align="center">1.31</td>

</tr>

<tr>

<td>Professor</td>

<td align="center">4.32</td>

<td align="center">0.80</td>

<td align="center">4.69</td>

<td align="center">0.71</td>

<td align="center">3.40</td>

<td align="center">1.10</td>

<td align="center">3.24</td>

<td align="center">1.10</td>

<td align="center">4.21</td>

<td align="center">0.80</td>

<td align="center">4.10</td>

<td align="center">0.94</td>

</tr>

<tr>

<td>Librarian</td>

<td align="center">4.50</td>

<td align="center">0.71</td>

<td align="center">4.50</td>

<td align="center">0.71</td>

<td align="center">3.00</td>

<td align="center">1.41</td>

<td align="center">1.50</td>

<td align="center">0.71</td>

<td align="center">4.00</td>

<td align="center">1.41</td>

<td align="center">4.50</td>

<td align="center">0.71</td>

</tr>

<tr>

<td>Colleague/Friend</td>

<td align="center">3.36</td>

<td align="center">0.94</td>

<td align="center">3.17</td>

<td align="center">0.89</td>

<td align="center">4.40</td>

<td align="center">0.97</td>

<td align="center">4.48</td>

<td align="center">0.84</td>

<td align="center">3.29</td>

<td align="center">0.99</td>

<td align="center">3.68</td>

<td align="center">1.11</td>

</tr>

<tr>

<td>Family</td>

<td align="center">3.29</td>

<td align="center">1.15</td>

<td align="center">3.48</td>

<td align="center">1.08</td>

<td align="center">4.90</td>

<td align="center">0.44</td>

<td align="center">4.95</td>

<td align="center">0.22</td>

<td align="center">3.43</td>

<td align="center">1.21</td>

<td align="center">3.57</td>

<td align="center">1.25</td>

</tr>

<tr>

<td>Books</td>

<td align="center">4.15</td>

<td align="center">1.03</td>

<td align="center">4.47</td>

<td align="center">0.78</td>

<td align="center">3.72</td>

<td align="center">1.06</td>

<td align="center">3.60</td>

<td align="center">1.06</td>

<td align="center">4.04</td>

<td align="center">0.92</td>

<td align="center">4.21</td>

<td align="center">0.91</td>

</tr>

<tr>

<td>Magazine</td>

<td align="center">3.29</td>

<td align="center">0.76</td>

<td align="center">4.29</td>

<td align="center">0.76</td>

<td align="center">3.71</td>

<td align="center">0.76</td>

<td align="center">3.86</td>

<td align="center">1.21</td>

<td align="center">3.57</td>

<td align="center">0.98</td>

<td align="center">3.57</td>

<td align="center">0.53</td>

</tr>

<tr>

<td>Research reports</td>

<td align="center">4.33</td>

<td align="center">0.87</td>

<td align="center">4.50</td>

<td align="center">0.88</td>

<td align="center">2.88</td>

<td align="center">1.03</td>

<td align="center">2.67</td>

<td align="center">0.92</td>

<td align="center">4.00</td>

<td align="center">0.72</td>

<td align="center">3.88</td>

<td align="center">0.74</td>

</tr>

<tr>

<td>Lecture notes</td>

<td align="center">3.75</td>

<td align="center">1.50</td>

<td align="center">5.00</td>

<td align="center">0.00</td>

<td align="center">4.50</td>

<td align="center">1.00</td>

<td align="center">3.75</td>

<td align="center">1.50</td>

<td align="center">3.50</td>

<td align="center">1.91</td>

<td align="center">3.25</td>

<td align="center">2.06</td>

</tr>

<tr>

<td>Search engine</td>

<td align="center">3.98</td>

<td align="center">0.87</td>

<td align="center">3.11</td>

<td align="center">0.83</td>

<td align="center">4.79</td>

<td align="center">0.58</td>

<td align="center">4.65</td>

<td align="center">0.71</td>

<td align="center">3.53</td>

<td align="center">0.81</td>

<td align="center">4.28</td>

<td align="center">0.81</td>

</tr>

<tr>

<td>Individual Website</td>

<td align="center">3.73</td>

<td align="center">0.77</td>

<td align="center">3.04</td>

<td align="center">0.89</td>

<td align="center">4.02</td>

<td align="center">0.81</td>

<td align="center">3.90</td>

<td align="center">0.92</td>

<td align="center">3.51</td>

<td align="center">0.84</td>

<td align="center">3.74</td>

<td align="center">0.94</td>

</tr>

<tr>

<td>organizational Website</td>

<td align="center">4.14</td>

<td align="center">0.95</td>

<td align="center">4.74</td>

<td align="center">0.67</td>

<td align="center">3.50</td>

<td align="center">1.09</td>

<td align="center">2.60</td>

<td align="center">0.87</td>

<td align="center">3.96</td>

<td align="center">1.08</td>

<td align="center">3.67</td>

<td align="center">1.13</td>

</tr>

<tr>

<td>Institutional repository</td>

<td align="center">4.41</td>

<td align="center">0.82</td>

<td align="center">4.76</td>

<td align="center">0.44</td>

<td align="center">3.34</td>

<td align="center">1.23</td>

<td align="center">2.55</td>

<td align="center">0.99</td>

<td align="center">4.21</td>

<td align="center">0.68</td>

<td align="center">3.97</td>

<td align="center">0.98</td>

</tr>

<tr>

<td>Digital libraries</td>

<td align="center">3.96</td>

<td align="center">0.93</td>

<td align="center">4.48</td>

<td align="center">0.71</td>

<td align="center">3.52</td>

<td align="center">1.06</td>

<td align="center">2.95</td>

<td align="center">0.86</td>

<td align="center">3.73</td>

<td align="center">1.07</td>

<td align="center">4.00</td>

<td align="center">1.06</td>

</tr>

<tr>

<td>Wikipedia</td>

<td align="center">3.86</td>

<td align="center">0.83</td>

<td align="center">3.79</td>

<td align="center">0.82</td>

<td align="center">4.40</td>

<td align="center">0.76</td>

<td align="center">3.74</td>

<td align="center">0.92</td>

<td align="center">3.60</td>

<td align="center">0.73</td>

<td align="center">3.88</td>

<td align="center">0.82</td>

</tr>

<tr>

<td>Newspapers</td>

<td align="center">3.72</td>

<td align="center">0.86</td>

<td align="center">3.92</td>

<td align="center">0.78</td>

<td align="center">3.96</td>

<td align="center">0.94</td>

<td align="center">3.92</td>

<td align="center">0.92</td>

<td align="center">3.66</td>

<td align="center">0.83</td>

<td align="center">3.83</td>

<td align="center">0.87</td>

</tr>

<tr>

<td>Online DB</td>

<td align="center">4.45</td>

<td align="center">0.72</td>

<td align="center">4.63</td>

<td align="center">0.56</td>

<td align="center">3.45</td>

<td align="center">1.00</td>

<td align="center">2.93</td>

<td align="center">0.93</td>

<td align="center">4.31</td>

<td align="center">0.75</td>

<td align="center">4.52</td>

<td align="center">0.71</td>

</tr>

<tr>

<td>Google Scholar</td>

<td align="center">4.22</td>

<td align="center">1.09</td>

<td align="center">4.33</td>

<td align="center">0.71</td>

<td align="center">4.33</td>

<td align="center">0.71</td>

<td align="center">3.44</td>

<td align="center">1.24</td>

<td align="center">4.33</td>

<td align="center">0.87</td>

<td align="center">4.44</td>

<td align="center">1.01</td>

</tr>

<tr>

<td>Report sharing site</td>

<td align="center">3.47</td>

<td align="center">0.86</td>

<td align="center">3.37</td>

<td align="center">0.96</td>

<td align="center">3.07</td>

<td align="center">1.01</td>

<td align="center">2.93</td>

<td align="center">0.69</td>

<td align="center">3.07</td>

<td align="center">0.98</td>

<td align="center">2.93</td>

<td align="center">0.91</td>

</tr>

<tr>

<td>Social Q&A</td>

<td align="center">3.42</td>

<td align="center">0.99</td>

<td align="center">2.62</td>

<td align="center">0.87</td>

<td align="center">4.26</td>

<td align="center">0.85</td>

<td align="center">4.08</td>

<td align="center">0.86</td>

<td align="center">3.05</td>

<td align="center">0.83</td>

<td align="center">3.53</td>

<td align="center">1.00</td>

</tr>

<tr>

<td>Television</td>

<td align="center">3.41</td>

<td align="center">0.86</td>

<td align="center">3.97</td>

<td align="center">0.73</td>

<td align="center">3.88</td>

<td align="center">1.15</td>

<td align="center">4.09</td>

<td align="center">0.90</td>

<td align="center">3.56</td>

<td align="center">0.82</td>

<td align="center">3.47</td>

<td align="center">1.02</td>

</tr>

<tr>

<td>Total</td>

<td align="center">3.92</td>

<td align="center">0.95</td>

<td align="center">3.80</td>

<td align="center">1.08</td>

<td align="center">3.98</td>

<td align="center">1.08</td>

<td align="center">3.73</td>

<td align="center">1.14</td>

<td align="center">3.70</td>

<td align="center">0.95</td>

<td align="center">3.95</td>

<td align="center">0.99</td>

</tr>

</tbody>

</table>

This study further examined the relationships between the four factors of usefulness, credibility, accessibility and familiarity, and satisfaction and intention for continued use. Table 9 presents correlation analysis results based on Pearson _r_ coefficients. The results reveal that satisfaction is moderately associated with usefulness and credibility (_r =_ 0.663 and 0.481, respectively at the 0.01 alpha level). As for intention for continued use, all four factors (usefulness, credibility, accessibility, and familiarity) are positively correlated at the 0.01 alpha level. In particular, usefulness is most closely correlated with the intention for continued use. The results reveal that users are likely to perceive satisfaction mainly based on the factors of usefulness and credibility, while intention for continued use correlated to accessibility and familiarity along with usefulness and credibility.

<table width="80%" border="1" align="center" cellpadding="3" cellspacing="0" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 9: Correlation analysis among usefulness, credibility, accessibility, familiarity, satisfaction, and intention for continued use (*p<0.05 ** p<0.01)**  
</caption>

<tbody>

<tr>

<th> </th>

<th align="center">Usefulness</th>

<th align="center">Credibility</th>

<th align="center">Accessibility</th>

<th align="center">Familiarity</th>

<th align="center">Satisfaction</th>

</tr>

<tr>

<td align="center">Credibility</td>

<td align="center">0.420**</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td align="center">Accessibility</td>

<td align="center">-0.030</td>

<td align="center">-0.313**</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td align="center">Familiarity</td>

<td align="center">-0.031</td>

<td align="center">-0.387**</td>

<td align="center">0.667**</td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td align="center">Satisfaction</td>

<td align="center">0.633**</td>

<td align="center">0.481**</td>

<td align="center">-0.057*</td>

<td align="center">-0.080**</td>

<td align="center"> </td>

</tr>

<tr>

<td align="center">Intention for continued use</td>

<td align="center">0.478**</td>

<td align="center">0.233**</td>

<td align="center">0.245**</td>

<td align="center">0.230**</td>

<td align="center">0.537**</td>

</tr>

</tbody>

</table>

A multi-dimentional scaling map was drawn incorporating frequency of use, usefulness and credibility, in an attempt to probe how frequently useful and reliable sources are likely to be used in undergraduate academic search tasks. Figure 2 visualises the result of the multi-dimensional scaling analysis based on the Euclidean distance method (stress value < 0.08). The selected sources were classified into four groups by the criteria of use frequency, usefulness, and credibility. These groups were 1) frequently used and useful and credible sources; 2) frequently used but less useful and credible; 3) useful and credible but less frequently used, and 4) less useful and credible and less frequently used. A notable finding was that most useful and credible sources, such as research reports, lecture notes, librarians, experts and institutional repositories, were not frequently selected. On the contrary, some resources were frequently used even though the participants perceived them as less useful and credible, such as search engines, individual Websites, Wikipedia, colleagues/friends and social question & answer services. Online databases, professors/lecturers, organizational Websites, and books were ideal sources because they were frequently selected as well as perceived as useful and credible.

<div align="center">![Figure 2: MDS analysis of selected resources considering frequency of use](p511fig2.jpg)</div>

<div align="center">  
**Figure 2: Mulit-dimensional scaling analysis of selected resources considering frequency of use**</div>

## Discussion and conclusions

This study is one of the few studies that investigated undergraduate student resource selection using self-generated diaries. The three research questions identified in the study were answered empirically based on qualitative and quantitative analyses of 233 diaries. The descriptive analysis of resource selection reaffirmed the dominant use of online sources in undergraduate academic tasks. Second, the present study explored factors influencing information selection from user wording in real settings. Twenty-eight factors in four dimensions, including information type, resource feature, search strategy and interaction with others, were identified. Third, perceptions of the participants were quantitatively investigated focusing on usefulness, credibility, accessibility, familiarity, satisfaction and intention for continued use. The study also classified four groups of resources based on their perception of usefulness, credibility and actual use. These groups were as follows: frequently used, and useful and credible sources; frequently used, but less useful and credible; useful and credible, but less frequently used; and less useful and credible, and less frequently used.

The findings of this study yield some insights into the strategies guiding undergraduates to choose better information resources in their academic search tasks. The results showed that several useful and credible sources were not frequently selected, such as experts/professionals, librarians, research reports and institutional repositories. Although they perceive these sources as credible and useful, undergraduates are less likely to use them due to their lack of accessibility and familiarity. To enhance accessibility and familiarity to these sources, information literacy education would be important. Information literacy courses can emphasise the advantages of using these reliable sources and teach students how to access and use them. In spite of the dominant use of online sources, the findings of the study highlighted the fact that interactions with human resources could lead to better choices of information, by providing advice, feedback, and comparisons. Professors, subject experts, or colleagues can assist undergraduates in their information seeking processes and help them find more adequate information sources or build better information search strategies. The results also found that fewer factors related to search strategies, such as exploring further information, narrowing the focus and advanced search, were associated with resource selection as compared to other factors. If students recognise their search strategies, they might be able to choose resources that are more aligned with their search strategies. To do this, students should know what search strategy would be most useful in achieving their search task. Through information literacy education, we could train undergraduates to develop effective search strategies and help them select information resources that fit those search strategies.

However, this study also has some limitations. First, the authors investigated only reasons why the participants selected a specific source but not why they failed to select others. That is, the study focused only on factors that contributed to selecting resources, while it did not explore factors that hindered selection. This fact limits our ability to identify factors associated with information source selection. Second, self-generated diaries might not show factors related to the users themselves. Past studies identified different types of user-side factors, such as subject knowledge, experience, search skills, gender and familiarity with topic ([Saito and Miwa 2001](#sai01); [Mitra _et al._ 2005](#mit05); [Xie and Joo 2009](#zie09)). However, self-claimed data collected by the diary method do not sufficiently include these user-side factors because the participants seemed to focus more on resources and their activities, and less on themselves as users. Third, this study investigated only the academic tasks of undergraduates in college environments. Undergraduate information search tasks are more various, not limited to academic tasks, such as entertainment, travel, shopping and so on. However, this study did not explore other types of search tasks. Fourth, although a search topic has been regarded as one of factors in terms of resource selection in previous studies ([Zhang _et al._ 2005](#zha05); [Xie and Joo 2009](#zie09)), this study did not investigate how students’ source selection would differ by search topic or area.

These limitations indicate a need for further research, which can investigate the factors that hinder the selection of a specific information source. Future research can also explore user-side factors in detail by applying complementary research methods such as in-depth interviews. In addition, the effects of different search tasks should be examined, and in particular, the difference in information resource use between academic and everyday tasks should be explored. Moreover, the authors plan a next study to explore how resource selection patterns would be different by assignment topics and subject.

## About the authors

<a id="authors"></a>Jee Yeon Lee is an associate professor of Library and Information Science at Yonsei University, Seoul, Korea.  She is also serving as a director of the Center for Human Computer Interaction. She has broad interests in information use behaviour, digital libraries, social computing, and human-computer interaction. She can be contacted at [jlee01@yonsei.ac.kr](mailto:jlee01@yonsei.ac.kr)

Woojin Paik, the corresponding author, is a professor of Computer Science at Konkuk University, Chungju, Korea. He is also working as a director of the Joongwon Library, which is the main library of the campus. His research interests include ICT use in education, human-computer interaction, and information extraction & retrieval. He can be contacted at [wjpaik@kku.ac.kr](mailto:wjpaik@kku.ac.kr)

Soohyung Joo is a PhD candidate in the School of Information Studies at the University of Wisconsin Milwaukee. His main research is focused on digital libraries, information use behaviour and interactive information retrieval. He can be contacted at [jsh8099@gmail.com](mailto:jsh8099@gmail.com)

#### References

*   Agosto, D.E. (2002). Bounded rationality and satisficing in young people’s Web-based decision making. _Journal of the American Society of Information Science and Technology_, **53**(1), 16-27.
*   Allard, S., Levine, K.J. & Tenopir, C. (2009). Design engineers and technical professionals at work: observing information usage in the workplace. _Journal of the American Society of Information Science and Technology_, **60**(3) 443-454.
*   Barllan, J. & Fink, N. (2005). Preference for electronic format of science journals – a case study of the Science Library users at the Hebrew University. _Library and Information Science Research_, **27**(3), 363-376.
*   Burton, V.T. & Chadwick, S.A. (2000). Investigating the practices of student researchers: patterns of use and criteria for use of Internet and library sources. _Computers and Composition_, **17**(3), 309-328.
*   Chakrabarti, A.K., Feineman, S. & Fuentevilla, W. (1983). Characteristics of sources, channels, and contents for scientific and technical information systems in industrial R and D. _IEEE Transactions on Engineering Management_, **EM-30**(2), 83-88.
*   Chang, H.R. (2006). The Use of Web-based electronic books among undergraduate students. _Journal of the Korean Society for Information Management_, **23**(4), 233-256
*   Dilevko, J. & Gottlieb, L. (2002). Print sources in an electronic age: a vital part of the research process for undergraduate students. _The Journal of Academic Librarianship_, **28**(6), 381-391.
*   Ebersole, S.E. (2005). On their own: students' academic use of the commercialized Web. _Library Trends_, **53**, 530-538.
*   Fidel, R., Davies, R.K., Douglass, M.H., Holder, J.K., Hopkins, C.J., Kushner, E.J., Miyagishima, B.K. & Toney. C.D. (1999). A visit to the information mall: Web searching behavior of high school students. _Journal of the American Society for Information Science_,**50**(1), 24-37.
*   Fidel, R. & Green, M. (2004). The many faces of accessibility: engineers’ perception of information sources. _Information Processing and Management_, **40**(3), 563-581.
*   Gerstberger, P.G. & Allen, T.J. (1968). Criteria used by research and development engineers in the selection of an information source. _Journal of Applied Psychology_, **52**(4), 272-279.
*   Glaser, B. & Strauss, A. (1967). _The discovery of grounded theory: strategies for qualitative research_, Chicago, IL: Transaction Publishers.
*   Holsti, O. (1969). _Content Analysis for the social sciences and humanities._ Reading, MA: Addison-Wesley.
*   Jones, S. (2002). _[The Internet goes to college: how students are living in the future with today’s technology: the Pew Internet and American Life Project](http://www.webcitation.org/65TRSF03X)_ Washington, DC: Pew internet and American Life Project. Retrieved 29 November, 2011, from http://www.pewinternet.org/~/media//Files/Reports/2002/PIP_College_Report.pdf.pdf (Archived by WebCite® at http://www.webcitation.org/65TRSF03X).
*   Kim, K. & Sin, S. J. (2007). Perception and selection of information sources by undergraduate students: effects of avoidant style, confidence, and personal control in problem-solving. _The Journal of Academic Librarianship_, **33**(6), 655-665.
*   Lee, J., Han, S. & Joo, S. (2008). The analysis of the information users' needs and information seeking behavior in the field of science and technology. _Journal of the Korean Society for Information Management_, **25**(2), 127-141.
*   Liu, Z. & Yang, Z. Y. (2004). Factors influencing distance-education graduate students' use of information sources: a user study. _The Journal of Academic Librarianship_, **30**(1), 24-35.
*   Logan, J. (2004). _[Using an online database searching tutorial to encourage reflection on the research process by undergraduate students.](http://www.webcitation.org/65TS9AyZH)_ Paper presented at the Online Teaching and Learning Conference: exploring Integrated Learning Environment. Brisbane, 3 November 2004\. Retrieved 29 November, 2011 from http://eprints.qut.edu.au/15693/1/15693.pdf (Archived by WebCite® at http://www.webcitation.org/65TS9AyZH)
*   Lubans, J. (1998). _How first year university students use and regard internet resources_. Unpublished manuscript, Duke University.
*   Metzger, M., Flanigan, A.J. & Zwarun, L. (2003). College student Web use, perceptions of information credibility, and verification behavior. _Computers and Education_, **41**(3), 271-290.
*   Mill, D.H. (2008). Undergraduate information resource choices. _College & Research Libraries_, **69**(4) 343-355.
*   Mitra, A., Willyard, J., Platt, C., & Parsons, M. (2005). [Exploring Web usage and selection criteria among male and female students.](http://www.webcitation.org/65TSgXkJ2) _Journal of Computer-Mediated Communication_, **10**(3), article 10\. Retrieved 15 February 2012 from http://jcmc.indiana.edu/vol10/issue3/mitra.html (Archived by WebCite® at http://www.webcitation.org/65TSgXkJ2)
*   O'Reilly, C. A. (1982). Variations in decision makers' use of information sources: the impact of quality and accessibility of information. _The Academy of Management Journal_, **25**(4), 756-771.
*   Pascoe, C., Applebee, A. & Clayton, P. (1996). Tidal wave or ripple? The impact of Internet on the academic. _Australian Library Review_, **13**(2), 147-153.
*   Quigley, J., Peck, D. R., Rutter, S. & Williams, E. M. (2002). [Making choices: factors in the selection of information resources among science faculty at the University of Michigan](http://www.webcitation.org/65TSurx7l). _Issues in Science and Technology Librarianship_, 34\. Retrieved 15 February 2012 from http://www.istl.org/02-spring/refereed.html (Archived by WebCite® at http://www.webcitation.org/65TSurx7l)
*   Rieh, S. Y. & Hilligoss, B. (2007). College students’ credibility judgments in the information seeking process. In M. Metzger & A. Flanigan (Eds.), _Digital media, youth, and credibility._ (pp. 49-72). Cambridge, MA: The MIT Press.
*   Saito, H., & Miwa, K. (2001). A cognitive study of information seeking processes in the WWW: the effects of searcher's knowledge and experience. In M.T. Özsu, H.J. Schek, K. Tanaka, Y. Zhang, and Y. Kambayashi (Eds.), _Proceedings of the 2nd International Conference on Web Information Systems Engineering, Kyoto, Japan, 2001._ (pp.321-333). Washington, DC: IEEE Computer Society.
*   Selwyn, N. (2008). An investigation of differences in undergraduates’ academic use of the internet. _Active Learning in Higher Education_, **9**(1), 11-22.
*   Strauss, A. & Corbin, J. (1990). Basics of qualitative research: grounded theory procedures and techniques. Newbury Park, CA: Sage Publications.
*   Tenopir, C., King, D.W., Boyce, P., Grayson, M., Zhang, Y. & Ebuen, M. (2003). [Patterns of journal use by scientists through three evolutionary phases](http://www.webcitation.org/65TT8dVOg). _D-Lib Magazine_, **9**(5). Retrieved 29 November, 2011 from http://dlib.org/dlib/may03/king/05king.html (Archived by WebCite® at http://www.webcitation.org/65TT8dVOg)
*   Tenopir, C., King, D.W. & Bush, A. (2004). Medical faculty's use of print and electronic journals: changes over time and in comparison with scientists. _Journal of the Medical Library Association_, **92**(2), 233-241.
*   Tenopir, C., King, D.W., Boyce, P., Grayson, M., & Paulson, K-L. (2005). Relying on electronic journals: reading patterns of astronomers. _Journal of the American Society for Information Science and Technology_, **56**(8), 786-802.
*   Tenopir, C., King, D.W., Clarke, M.T., Na, K. & Zhou, X. (2007). Journal reading patterns and preferences of pediatricians. _Medical Library Association_, **95**(1), 56-63.
*   Thompson, C. (2003). Information illiterate or lazy: how college students use the Web for research. _Portal: Libraries and the Academy_, **3**(2), 259-268.
*   Thomson, A.J. (2007). _Information seeking behavior of distance education students._ Unpublished Master's dissertation, University of North Carolina at Chapel Hill, North Carolina, USA.
*   Vibert, N., Rouet, J., Ros, C., Ramond, M. & Deshoullieres, B. (2007). The use of online electronic information resources in scientific research: the case of neuroscience. _Library & Information Science Research_, **29**(4), 508- 532.
*   Xie, I. & Joo, S. (2009). Selection of information sources: types of tasks, accessibility and familiarity of sources. _Proceedings of the American Society for Information Science and Technology,_ **46**(1), 1-18.
*   Zhang, X., Anghelescu, H. G. B. & Yuan, X. (2005). [Domain knowledge, search behaviour, and search effectiveness of engineering and science students: an exploratory study.](http://www.webcitation.org/65TUADCu4) _Information Research_, **10**(2), paper 2\. Retrieved 15 December 2010 from http://informationr.net/ir/10-2/paper217 (Archived by WebCite® at http://www.webcitation.org/65TUADCu4).