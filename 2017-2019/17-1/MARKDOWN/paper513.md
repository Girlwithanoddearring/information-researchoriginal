#### vol. 17 no. 1, March 2012

# What drives collective innovation? Exploring the system of drivers for motivations in open innovation, Web-based platforms  

#### [Cinzia Battistella](#authors)  
University of Udine, Department of Electric, Managerial and Mechanical Engineering, Udine, Italy  
[Fabio Nonino](#authors)  
Sapienza University of Rome, Department of Computer, Control, and Management Engineering “Antonio Ruberti”, Rome, Italy

#### Abstract

> **Introduction.** The paper deals with the topic of increasing integration of individuals and companies in innovation processes by means of Web-based software platforms. We consider why people participate and contribute in platforms and the characteristics of the platforms and the possible managerial actions (i.e., the drivers for motivations) to enhance these motivations?  
> **Method.** An empirical qualitative analysis of twenty important platforms, leading to a cause-effect map of drivers and motivations. Both primary data (from a Delphi study and interviews) and secondary data (documents, Website explanations, etc.) were used.  
> **Analysis.** Qualitative analysis was used to examine the interactions between specific drivers for enhancing specific motivations.  
> **Results.** Nine groups of drivers, classified into three groups, were found. Strategies for driving the motivations that could encourage users to play an active role in the platforms are proposed.  
> **Conclusions.** The findings could be useful for community managers and platform designers because they suggest choices for platform design that could attract potentially innovative participants and sustain a high level of collaboration for innovation-related contributions.

## Introduction

It is increasingly recommended that companies innovate using external resources ([Pisano and Verganti 2008](#pis08): 1). Knowledge sharing is a social activity that occurs within a system where knowledge represents a valuable resource ([Davenport and Prusak 1998](#dav98); [Fulk _et al._ 2004](#ful04)). Knowledge generation, use, application and exploitation can occur _within organizations_ at the individual, team and unit level and _outside_ the organization's boundaries ([Davenport and Prusak 1998](#dav98), [De Toni et _et al._ 2011](#det11)).

Focusing on knowledge-sharing for innovation, the open innovation paradigm ([Chesbrough 2003](#che03)) assumes that firms can use internal and external ideas and internal and external paths to markets and identifies that the processes of innovation increasingly involve a multitude of networks of knowledge-driven organizations. These can be industrial clusters ([Scheel 2002](#sch02); [Hutchings and Weir 2006](#hut06); [Brette and Chappoz 2007](#bre07)), science and technology parks ([Monck 1988](#mon88)), knowledge marketplaces ([Warkentin _et al._ 2001](#war01); [Geragthy and Desouza 2005](#ger05); [Bush and Tiwana 2005](#bus05); [Kafentzis _et al._ 2005](#kaf05); [Al-Shammari 2008](#als08)), communities of practice ([Lave and Wenger 1998](#lav98); [Chesbrough 2003](#che03)), and R&D consortia ([Aldrich 1995](#ald95)).

Traditional practices to enable knowledge sharing are training sessions, workshops, seminars, consulting, research and development activities, university courses, conferences, trade shows, government extension programmes and consortia. However, the World Wide Web has created new forms of enablers for collaborative innovation networks.

More and more companies recognize the potential of using their communities of customers, employees, partners and other interested parties as a source of information and innovation. _open innovation (Web-based) platforms_ (hereafter platforms) can be a means for companies to _accelerate_ the innovation process. For example, they can help client companies to find solutions and/or create communities that contribute to ideas and new products or services. In such platforms, the different parties contribute to and collaborate on ideas, propose new concepts and trends, present solutions to win contests and to answer companies' needs. When used by a firm, the platform can be based on contributions from:

*   internal communities, which consist of local and/or international units;
*   external communities, which may be formed of several sub-communities, most notably industry experts, suppliers, customers, competitors' advisors, and authorities.

Thus, these platforms enable _collective innovation_ (that is, open and collaborative innovation). The Web has changed how knowledge is created and shared, offering global accessibility and communication with low cost. Consequently, online communities and the platforms ([Dodgson _et al._ 2006](#dod06); [Huston and Sakkab 2006](#hus06)) represent a new approach to opening up company innovation processes ([Jeppesen and Frederiksen 2006](#jep06); [Fleming and Waguespack 2007](#fle07); [De Toni _et al._ 2012](#det12)). Recently, a large number of new innovative enterprises faced competitive markets by promoting innovation in products and services through platforms that offer free services and open hardware design and allow the collaboration of individuals and companies through so-called _crowdsourcing_ ([Howe 2006](#how06)). Traditional companies have also tried to foster their competitiveness and innovation potential by using platforms and new collaborative approaches, such as co-design, collective intelligence ([Brown and Lauder 2000](#bro00)), smart mobs ([Rheingold 2002](#rhe02)), lead-users innovation ([Von Hippel 2005](#von05)) and open-source communities ([Tapscott and Williams 2006](#tap06)).

The platforms are the nexus for the aggregation and integration of different members (individuals and companies) in an innovation community, permitting access to a large pool of experts and contributors, benefiting from proximity to customers and user innovations and avoiding a local search bias in innovation ([Füller _et al._ 2006](#ful06); [von Krogh and von Hippel 2006](#von06)).

New knowledge is often created and shared through interaction with the environment, as well as through social and collaborative processes ([Alavi and Leidner 2001](#ala01); [Bhatt 2001](#bha01); [Battistella _et al._ 2012](#bat12)). It is important, then, to consider the _motivations_ of individuals and companies to engage in knowledge-sharing and also the conditions and environments (_motivational drivers_) that facilitate and drive it. The economics and dynamics of the cooperative systems for innovation are critical and, although literature and empirical examples support us through an analysis of individuals' motivations, the research related to companies' motivations is still a big challenge. Moreover, while recent literature has begun to discuss the motivations for participation and knowledge sharing in platforms, it does not support us from a motivational drivers' point of view: the research identified the motivations but not the strategies (drivers) to push them. Finally, current research has explored very few of the drivers for open innovation and/or collective innovation, especially in a context where there is little, or uncertain, monetary return for the innovative efforts.

Consequently, a key issue is the identification of the most successful drivers for motivations that could encourage users (both individuals and companies) to share their knowledge, play an active role and collaborate in the platform. The objective of the research has been to understand how to stimulate the users' participation and active, reactive and proactive knowledge-sharing inside open innovation platforms for the company-centred crowdsourcing in the innovation process for new product or service development.

The first phase of our research has been to deepen the theoretical background on motivation for participation in the collective innovation process, grounded in the psychological and sociological literature. This permitted us to build the research framework that has been used for an in-depth qualitative analysis of twenty platforms. The main aim has been to further develop the state-of-the-art of motivational drivers for stimulating users' participation and collaboration and to evaluate their impact. The paper then proposes the results of the comparative analysis of the drivers and the impact of these drivers on motivations. Finally, these practices are synthesized in a framework useful for the analysis and discussion of the study's academic and managerial implications.

## Theoretical background

This section summarizes the conceptual basis of the motivations for participation in the collective innovation process. We categorize the main (intrinsic and extrinsic) motivations for collaboration proposed in papers and in the most recent literature reviews (see [Von Krogh _et al._ 2008](#von08); [Antikainen _et al._ 2010](#ant10); [Antikainen and Vaataja, 2010](#anv10)). This is used to build the research framework used in the analysis of the twenty open innovation platforms.

### A categorization of the motivations for collective innovation

'_To be motivated means to be moved to do something_' ([Ryan and Deci 2000: 54](#rya00)). Different people are motivated by different factors, which can be categorised into different groups or types of motivation. Self-determination theory ([Deci and Ryan 1980](#dec80); [1985](#dec85)) distinguishes different types of motivations based on different reasons or goals that lead to action. The main distinction is between intrinsic motivations (which refer to actions based on personal interest and pleasure) and extrinsic motivations (which concern actions that lead to a separable result).

The _intrinsic motivations_ are defined on the bais of satisfaction in the activity itself rather than external pressures or rewards ([Deci and Ryan 2000](#dec00)). So, intrinsically motivated behaviour is seen as the more spontaneous and self-interested. Intrinsic motivation can be separated into two dimensions: (1) the individual or enjoyment-based intrinsic motivation and (2) the social, or obligation or community-based intrinsic motivation ([Lindenberg 2001](#lin01); [Lakhani and Wolf 2005](#lak05); [Von Krogh _et al._ 2008](#von08)).

Literature describes _extrinsic motivation_ as when an activity is performed to achieve some separable results or consequences ([Deci and Ryan 1985](#dec85)), such as tangible or verbal rewards ([Deci and Ryan 1980](#dec80)). An _incentive_ is any (financial or non-financial) stimulus coming from the external environment, which affects the individuals' motivational processes, satisfying their need and then pushing them to a particular course of action, or counts as a reason for preferring one choice to the alternatives. Leimeister _et al._ ([2009](#lei09)) argue that extrinsic motivations are triggered by incentives such as monetary rewards (direct or indirect) or awards. According to this research, incentives are seen as potential to win a prize ascribable to extrinsic motivations. Thus, the close link between incentives and extrinsic motivations is evident; in literature, the incentives are broken down into two broad categories: _tangible_ and _intangible rewards_ ([Roberts _et al._ 2006](#rob06)). Tangible rewards are material rewards such as money, products and software tools, while intangible rewards include moral reward (for doing the right thing), social contacts and symbolic rewards. The _motivational drivers_ linked to extrinsic motivation are prizes, not necessarily cash-based. The use of the word _reward_ for incentives and _prize_ for extrinsic motivation highlights the fact that incentives are a means used by those who propose collaboration to urge those who must choose to cooperate; the goal of incentives is to generate in the potential contributors some extrinsic motivations that, added to the intrinsic ones, should lead to cooperation.

Bonaccorsi and Rossi ([2006](#bon06)) distinguished between _economic_ (tangible), _social_ and technological (_professional_) motivations. Von Krogh _et al._ ([2008](#von08)) suggest that economic motivation is similar to extrinsic motivation, and social motivation is close to intrinsic motivation. However, the authors also suggest that the third type of motivation, technological, includes benefits from learning and working with a bleeding-edge technology.

Summarizing the concepts described above we therefore propose the following framework of motivations for participation in the collective innovation process:

1) Intrinsic motivations

*   _Individual-driven motivations_ concern the psychological-emotional sphere of individuals who choose to enter the community and to collaborate or to contribute in a project.
*   _Social-driven motivations_ concern the collective sphere of the individual who joins a community of collaborative innovation.

2) Extrinsic motivations

*   _Economic motivations_ concern all the actions that lead, directly or indirectly, to economic advantages for the contributors (both individuals and companies).
*   _Professional motivations_ concern all the actions that lead to professional advantages for the contributor.
*   _Social motivations_ concern all the obligations and responsibilities arising from the social sphere of contributors and which have effects on community. Here the social dynamics are fundamental.

In particular:

*   **1a. Intrinsic individual-driven motivation**
    *   _Entrepreneurial mindset_. This motivation refers to the natural entrepreneurial propensity of the individual ([Tapscott and Williams 2006](#tap06)).
    *   _Opportunity to express individual creativity_. This motivation refers to the possibility given by the platform to express the individual creativity and artistic talent (often limited by routine working)([Amabile _et al._ 1994](#ama94); [Ryan and Deci 2000](#rya00)).
    *   _Care for community and attachment to the group, sense of membership_. This motivation rises from a sense of membership and companionship and the human natural tendency to join a group, to feel part of a community and to assume responsibilities towards other members ([Stallman 1999](#stal99); [Stewart and Gosain 2006](#ste06); [Nardi _et al._ 2004](#nar04)). It is also defined as _kinship_ ([Hars and Ou 2002](#har02); [Hemetsberger 2004](#hem04); [Hertel _et al._ 2003](#her03); [Lakhani and Wolf 2005](#lak05); [Zeityln 2003](#zei03)) and _altruism_ ([Aalbers 2004](#aal04); [Bitzer _et al._ 2007](#bit07); [Ghosh 2005](#gho05); [Hars and Ou 2002](#har02); [Hemetsberger 2004](#hem04); [Osterloh and Rota 2007](#ost07); [Haruvy _et al._ 2003](#har03); [Wu _et al._ 2007](#wug07); [Zeityln 2003](#zei03)).
    *   _Enjoyment, fun and entertainment_. Referring to the fun and personal pleasure that comes from doing what we like ([Wang and Fesenmaier 2003](#wan03); [Benkler 2002](#ben02); [Hemetsberger 2004](#hem04); [Herkel _et al._ 2003](#her03); [Lakhani and von Hippel 2003](#lak03); [Lakhani and Wolf 2005](#lak05); [Luthiger and Jungwith 2007](#lut07); [Roberts _et al._ 2006](#rob06); [Aalbers 2004](#aal04); [Nov 2007](#nov07); [Torvalds and Diamond 2001](#tor01); [von Hippel and von Krogh 2003](#von03)). It is also defined as _recreation_ ([Ridings and Gefen 2004](#rid04)).
    *   _Psychological compensation and sense of efficacy_. This motivation refers to the feelings that people may experience by participating and contributing to the achievement of shared projects, gaining awareness of their influence and personal revenge when individuals feel overlooked or underestimated (especially in the working environment) ([Nardi _et al._ 2004](#nar04)). It is also defined as _sense of efficacy_ ([Bandura 1995](#ban95); [Constant _et al._ 1994](#con94); [Kollock 1999](#kol99)).
*   **1b. Intrinsic social-driven motivation**
    *   _Sense of cooperation in the area of interest_. Individuals feel obliged to contribute and to cooperate for the development of their area of interest. ([Wang and Fesenmaier 2003](#wan03)). It is also defined as _interesting objectives and intellectual stimulations_ ([Hagel and Armstrong 1997](#hag97); [Ridings and Gefen 2004](#rid04); [McLure Wasko and Faraj 2000](#mcl00)).
    *   _Social Responsibility_. Individuals feel motivated by ideals linked to sustainability ([Benbya and Belbaly 2010](#ben10)). It is also defined as _ideology_ ([David _et al._ 2003](#dav03); [Ghosh 2005](#gho05); [Hemetsberger 2004](#hem04); [Herkel _et al._ 2003](#her03); [Lakhani and von Hippel 2003](#lak03); [Lakhani and Wolf 2005](#lak05); [Nov 2007](#nov07)).
*   **2a. Extrinsic economic motivation**
    *   _Monetary rewards_. Monetary rewards are the classic reason for which individuals offer performance, time and talent; there is a positive correlation between the intensity of contributors' participation in collaborative projects and amount of money received ([Anderson 2009](#and09); [Tapscott and Williams 2006](#tap06); [Benkler 2002](#ben02); [Ghosh 2005](#gho05); [Herkel _et al._ 2003](#her03); [Lakhani and Wolf 2005](#lak05); [Lattemann and Stieglitz 2005](#lat05); [Luthiger and Jungwith 2007](#lut07); [Roberts _et al._ 2006](#rob06); [Aalbers 2004](#aal04); [McLure Wasko and Faraj 2000](#mcl00)).
    *   _Free products (hardware and software)_. Free products are a surrogate for monetary rewards as they are an economic benefit for contributors; they are also physical products or software realized thanks to contributions received by the community ([Anderson 2009](#and09); [Tapscott and Williams 2006](#tap06)). This is also defined as _own use_ ([Bitzer _et al._ 2007](#bit07); [David _et al._ 2003](#dav03); [Ghosh 2005](#gho05); [Hemetsberger 2004](#hem04); [Herkel _et al._ 2003](#her03); [Lakhani and von Hippel 2003](#lak03); [Lakhani and Wolf 2005](#lak05); [Lattemann and Stieglitz 2005](#lat05); [Osterloh and Rota 2007](#ost07); [Roberts _et al._ 2006](#rob06); [Wu _et al._ 2007](#wug07)) and _need, software improvements and technical reasons_ ([Jeppesen and Frederiksen 2006](#jep06); [Kollock 1999](#kol99); [Ridings and Gefen 2004](#rid04)).
    *   _Free services_. Services are usually associated with hardware or software developed; but often additional services to the base package ([Anderson 2009](#and09); [Tapscott and Williams 2006](#tap06)). This has also been classified as _own use_ ([Bitzer _et al._ 2007](#bit07); [David _et al._ 2003](#dav03); [Ghosh 2005](#gho05); [Hemetsberger 2004](#hem04); [Herkel _et al._ 2003](#her03); [Lakhani and von Hippel 2003](#lak03); [Lakhani and Wolf 2005](#lak05); [Lattemann and Stieglitz 2005](#lat05); [Osterloh and Rota 2007](#ost07); [Roberts _et al._ 2006](#rob06); [Wu _et al._ 2007](#wug07)) and _need, software improvements and technical reasons_ ([Jeppesen and Frederiksen 2006](#jep06); [Kollock 1999](#kol99); [Ridings and Gefen 2004](#rid04)).
*   **2b. Extrinsic professional motivation**
    *   _Learning_. Refers to the individuals'goals to increase their competences and to acquire new skills ([Benbya and Belbaly 2010](#ben10); [Bowman and Willis 2003](#bow03); [David _et al._ 2003](#dav03); [Ghosh 2005](#gho05); [Hars and Ou 2002](#har02); [Hemetsberger 2004](#hem04); [Lakhani and Wolf 2005](#lak05); [Roberts _et al._ 2006](#rob06); [Spaeth _et al._ 2008](#spa08); [Wu _et al._ 2007](#wug07); [Ye and Kishida 2003](#yek03)).
    *   _Reputation_. Enhancing one's own reputation helps to improve and increase the perceived value of an individual's work and increase their professional standing, which could improve their economic condition ([Bowman and Willis 2003](#bow03); [Ghosh 2005](#gho05); [Hars and Ou 2002](#har02); [Hemetsberger 2004](#hem04); [Herkel _et al._ 2003](#her03); [Lakhani and von Hippel 2003](#lak03); [Lakhani and Wolf 2005](#lak05); [Lattemann and Stieglitz 2005](#lat05); [Lerner and Tirole 2002](#ler02); [Roberts _et al._ 2006](#rob06); [Spaeth _et al._ 2008](#spa08)). It is also defined as _peer recognition_ ([Hargadon and Bechky 2006](#har06); [Lerner and Tirole 2002](#ler02)).
    *   _Recognition from the company and growth of professional status or career benefits (for platforms used inside corporations)_. As above, individuals are motivated by enhancing their reputation and receiving special merit awards from the company; in an open-source community individuals gain respect, reputation and credibility, both in the eyes of other participants and of the company where they work. This status can be exploited in the workplace in order to improve their professional status ([Benbya and Belbaly 2010](#ben10)). It is also defined as _career_ ([Ghosh 2005](#gho05); [Hars and Ou 2002](#har02); [Hemetsberger 2004](#hem04); [Herkel _et al._ 2003](#her03); [Lakhani and Wolf 2005](#lak05); [Lerner and Tirole 2002](#ler02); [Roberts _et al._ 2006](#rob06); [Wu _et al._ 2007](#wug07)) and _reputation and enhancement of professional status_ ([Aalbers 2004](#aal04); [Bagozzi and Dholakia 2002](#bag02); [Dholakia _et al._ 2004](#dho04); [Hargadon and Bechky 2006](#har06); [Lerner and Tirole 2002](#ler02); [Lakhani and Wolf 2005](#lak05); [Rheingold 1993](#rhe93); [McLure Wasko and Faraj 2005](#mcl05)) and _firm recognition_ ([Jeppesen and Frederiksen 2006](#jep06)).
    *   _Reciprocity_. Reciprocity represents the possibility of establishing with a community a continuous and durable exchange relationship over time ([Raymond 1999](#ray99); [Benbya and Belbaly 2010](#ben10); [Bergquist and Ljungberg 2001](#ber01); [David _et al._ 2003](#dav03); [Hemetsberger 2004](#hem04); [Lakhani and von Hippel 2003](#lak03); [Lakhani and Wolf 2005](#lak05); [Aalbers 2004](#aal04); [Kollock 1999](#kol99); [McLure Wasko and Faraj 2000](#mcl00)).
*   **2c. Extrinsic social motivation**
    *   _Individual accountability_. Increasing the level of individual responsibility, the contributors feel compelled to succeed in a project; contributors feel an obligation towards those who respected and trusted them by giving them great responsibilities ([Benbya and Belbaly 2010](#ben10)). It is also defined as _sense of obligation to contribute_ ([Bryant _et al._ 2005](#bry05); [Lakhani and Wolf 2005](#lak05)).
    *   _Social capital_. This motivation refers to the set of interpersonal relationships, formal and informal, which are essential for the community functioning ([Bowman and Willis 2003](#bow03); [Wang and Fesenmaier 2003](#wan03), [De Toni and Nonino 2010](#det10)). It is also defined as _knowledge exchange, personal learning and social capital_ ([Gruen _et al._ 2005](#gru05); [Ridings and Gefen 2004](#rid04); [von Hippel and von Krogh 2003](#von03); [McLure Wasko and Faraj 2000](#mcl00); [Wiertz and Ruyter 2007](#wie07)).

This list shows a categorization of motivation for collective innovation proposed in literature under our proposed framework. We built it starting from the literature review proposed by Von Krogh _et al._ ([2008](#von08)), Antikainen _et al._ ([2010](#ant10)) and Antikainen and Vaataja ([2010](#anv10)) which are based on other studies about motivations in open source development ([Hars and Ou 2002](#har02); [Hertel _et al._ 2003](#her03); [Lakhani and Von Hippel 2003](#lak03); [Hemetsberger 2004](#hem04); [Lakhani and Wolf 2005](#lak05); [Roberts _at al._ 2006](#rob06); [Nov 2007](#nov07)). Moreover we added two individual intrinsic motivations (the entrepreneurial mindset and the opportunity to express personal creativity), which were not present in the three previous cited classifications but are present in the literature that discusses single motivations (in particular, [Tapscott and Williams 2006](#tap06); [Amabile _et al._ 1994](#ama94); [Ryan and Deci 2000](#rya00)). Finally, we modified the name _ideology_ to _social responsibility_.

The entrepreneurial mindset is a natural propensity of some individuals. A first fundamental aspect to motivate these possible contributors is to cultivate their entrepreneurial mindset. One of the proposed benefits of open innovation platforms is that they can stimulate entrepreneurship and the intention to share knowledge and ideas ([Tapscott and Williams 2006](#tap06)).

As regards opportunity to express individual creativity, intrinsic motivation is the key ingredient to help spark creativity ([Amabile 1998](#ama98)). Intrinsic motivations come from inside a person and are factors like passion and self-interest. The opportunity to express personal creativity motivates individuals in the accomplishment of complex tasks ([Amabile _et al._ 1994](#ama94), [1996](#ama96)) and engages them in their work for the challenge and enjoyment ([Amabile _et al._ 1986](#ama86)).

Another social motivation can be ideology ([Stallman 1999](#stal99); [Stewart and Gosain 2006](#ste06)). Nevertheless because of the business nature of open innovation platforms, we decided not toconsider ideology as defined by open source communities (e.g., the ideology could be the belief in software freedom as in the case of Linux vesrsus Microsoft) and in non-profit, open innovation platforms such as Wikipedia(e.g., 'I think information should be free' ([Nov 2007](#nov07))). Furthermore, as stated by Martin ([2000](#mar00), [2002](#mar02)), individuals can be pushed by moral concerns such as:

*   contributions to the greater good,
*   responsibility to observe ethical standards, and
*   obligations towards other practitioners and the wider society.

### The research gap: the drivers for motivations of platform users

Literature has focused on understanding the motivations for knowledge-related communication _within organizations_ (e.g., [Osterloh and Frey 2000](#ost00); [Bunderson and Sutcliffe 2002](#bun02); [Argote _et al._ 2003](#arg03); [Wittenbaum _et al._ 2004](#wit04); [Siemsen _et al._ 2008](#sie08); [Barachini 2009](#bar09); [Kaiser _et al._ 2010](#kai10)) and the drivers that lead people to actively engage in it (e.g., [Szulanski 1996](#szu96); [Reagans and McEvily 2003](#rea03); [Bock _et al._ 2005](#boc05); [Kankanhalli _et al._ 2005](#kan05); [Bordia _et al._ 2006](#bor06); [Quigley _et al._ 2007](#qui07)). Several enhancers and barriers for knowledge sharing have also been discussed from an _inter-organizational point of view_ (e.g., [Davenport _et al._ 1998](#dav98); [Davenport and Prusak 1998](#dap98); [Brown and Duguid 2000](#bro00); [McDermott and O'Dell 2001](#mcd01); [Dyer and Nobeoka 2000](#dye00); [Battistella and Nonino 2012](#bat12)).

Research on motivations has emphasized the importance of creating a favourable environment in order to elicit motivations. This line of research is full of contributions from a psychological point of view: for example the motivation for learning is curiosity, this is stimulated by the ability of the professor in not only giving concepts but also in storytelling, or it can be stimulated by a classroom full of cartoon posters on the walls. The ability of the learner is a motivational driver, as is the classroom full of cartoons. In a general definition, the _motivational drivers are environmental stimuli (objects, people's behaviours, events) pulling people's motivations_. In our case, the environment is the platform, therefore the motivational drivers are the design characteristics of the platforms (objects) and the managerial actions that the community manager can do inside the platform (people's activities) with the aim to enhance motivation.

The drivers that push collaboration and knowledge sharing inside platforms can be different from those discussed by the authors above. This is because the motivations themselves are different (for instance, some of the motivations are specific to an online context) and in the same way the drivers can be particular for knowledge sharing in an online context or for users motivated by the sense of community inspired by products or services such as Linux or Wikipedia.

Some works (e.g., [Hemetsberger 2002](#hem02); [Roberts _et al._ 2006](#rob06); [Antikainen _et al._ 2010](#ant10); [Antikainen and Vaataja 2010](#anv10)) try to explain the motivations that drive people to share their knowledge and to engage in collaborative innovation inside communities of practice. _However, knowledge remains incomplete on motivational drivers: how can a platform be designed in order to drive the right motivations?_

Our research aims to investigate the drivers that should be implemented inside a platform to push motivations and foster a collective innovation process. Scholars have previously studied motivations and their impact on participation without discussing platforms' drivers that stimulate users' contribution and collaboration.

## Research methodology

The purpose of the research is to identify the drivers that can develop and increase the motivations which encourage potential contributors to collaborate in platforms. Therefore, our research focused on the identification of the different drivers used inside platforms to promote innovation and on the study of how to design a system of incentives referring to this peculiar innovation process. Consequently we aimed to answer to the following research question: _Which motivational drivers can impact on motivations in open innovation platforms?_

The paper is a qualitative, empirical case study and Delphi analysis of the positioning of open innovation Web-based platforms. We decided to use case study research as an overall methodological approach for our empirical investigation. As suggested by a number of scholars, this is a very powerful method for building a rich understanding of complex phenomena ([Eisenhardt and Graebner 2007](#eis07)) that requires the capability to answer 'how' and 'why' questions ([Yin 2003](#yin03)). The qualitative methodology enables a contextual analysis of a number of events and situations and possible relationships that exist between them. In particular, we used a multiple case study approach because they typically yield more robust, generalizable and testable interpretations of a phenomenon than single case study research ([Eisenhardt and Graebner 2007](#eis07)). Given the complexity of the research aim and the need for an explorative approach, we considered that too small a sample would not give us a complete variety of possible drivers, while too big a sample would not give the right depth of analysis.

The empirical setting of this study is a set of twenty open innovation platforms, deemed particularly relevant for the purposes of the study among a [list of platforms](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.openinnovators.net%2Flist-open-innovation-crowdsourcing-examples%2F&date=2012-03-09). Here, every platform is seen as a case. For each platform we investigated the motivations and the motivational drivers and then conducted cross-case analysis to consider their influence.

We chose a differentiated sample in terms of the ultimate objective of the platforms, seniority and the Alexa ranking (an indicator that measures the performance of a Website in terms of popularity based on a set of indexes such as participation, number of users, sites linking in and linking out, etc.), in order to have a larger and comprehensive sample. Table 1 depicts the different platforms considered with their classification in terms of seniority and popularity of the site.

<table width="80%" cellspacing="0" cellpadding="3" border="1" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Classification of open innovation Web-based platforms**</caption>

<tbody>

<tr>

<th>No.</th>

<th>Platform name</th>

<th>Website</th>

<th>Seniority</th>

<th>Popularity (Alexa rank)</th>

</tr>

<tr>

<td align="center">1</td>

<td>crowdSPRING</td>

<td>http://www.crowdspring.com/</td>

<td align="center">2008</td>

<td align="center">8.974</td>

</tr>

<tr>

<td align="center">2</td>

<td>Quirky</td>

<td>http://www.quirky.com/</td>

<td align="center">2008</td>

<td align="center">33.545</td>

</tr>

<tr>

<td align="center">3</td>

<td>Shapeways</td>

<td>http://www.shapeways.com/</td>

<td align="center">2008</td>

<td align="center">56.294</td>

</tr>

<tr>

<td align="center">4</td>

<td>Guerra Creativa (choosa)</td>

<td>http://www.choosa.net/</td>

<td align="center">2009</td>

<td align="center">67.591</td>

</tr>

<tr>

<td align="center">5</td>

<td>clickworker</td>

<td>http://www.clickworker.com/</td>

<td align="center">2005</td>

<td align="center">72.869</td>

</tr>

<tr>

<td align="center">6</td>

<td>Ponoko</td>

<td>http://www.ponoko.com/</td>

<td align="center">2007</td>

<td align="center">84.666</td>

</tr>

<tr>

<td align="center">7</td>

<td>IdeaConnection</td>

<td>http://www.ideaconnection.com/</td>

<td align="center">2007</td>

<td align="center">87.481</td>

</tr>

<tr>

<td align="center">8</td>

<td>Innocentive</td>

<td>http://www.innocentive.com/</td>

<td align="center">2001</td>

<td align="center">87.638</td>

</tr>

<tr>

<td align="center">9</td>

<td>THINGIVERSE</td>

<td>http://www.thingiverse.com/</td>

<td align="center">2008</td>

<td align="center">90.999</td>

</tr>

<tr>

<td align="center">10</td>

<td>Enterprise Spigit</td>

<td>http://www.spigit.com/</td>

<td align="center">2006</td>

<td align="center">138.555</td>

</tr>

<tr>

<td align="center">11</td>

<td>Idea Bounty</td>

<td>http://www.ideabounty.com/</td>

<td align="center">2006</td>

<td align="center">149.199</td>

</tr>

<tr>

<td align="center">12</td>

<td>Presans</td>

<td>http://www.presans.com/</td>

<td align="center">2007</td>

<td align="center">209.091</td>

</tr>

<tr>

<td align="center">13</td>

<td>WhyNot?</td>

<td>http://www.whynot.net/</td>

<td align="center">2005</td>

<td align="center">241.459</td>

</tr>

<tr>

<td align="center">14</td>

<td>NineSigma</td>

<td>http://www.ninesigma.com</td>

<td align="center">2000</td>

<td align="center">317.370</td>

</tr>

<tr>

<td align="center">15</td>

<td>redesignme</td>

<td>http://www.redesignme.com/</td>

<td align="center">2007</td>

<td align="center">512.983</td>

</tr>

<tr>

<td align="center">16</td>

<td>Innovation Exchange</td>

<td>http://www.innovationexchange.com/</td>

<td align="center">2008</td>

<td align="center">650.994</td>

</tr>

<tr>

<td align="center">17</td>

<td>Hypios</td>

<td>https://www.hypios.com/</td>

<td align="center">2008</td>

<td align="center">1.035.496</td>

</tr>

<tr>

<td align="center">18</td>

<td>ideawicket.com</td>

<td>http://www.ideawicket.com/</td>

<td align="center">2006</td>

<td align="center">1.090.774</td>

</tr>

<tr>

<td align="center">19</td>

<td>Innoget</td>

<td>http://www.innoget.com/</td>

<td align="center">2009</td>

<td align="center">1.302.660</td>

</tr>

<tr>

<td align="center">20</td>

<td>Big Idea Group</td>

<td>http://www.bigideagroup.net/</td>

<td align="center">2008</td>

<td align="center">1.522.621</td>

</tr>

</tbody>

</table>

Our empirical inquiry investigated the phenomenon in-depth and within its real-life context ([Yin 2003](#yin03)): we used both an outsider or user perspective and an insider perspective through primary data (from a Delphi study and interviews) and secondary data (documents, Website explanations, etc.).

We gathered the data about motivations by a questionnaire for the platforms' users. We gathered the data about motivational drivers by observing the characteristics of the platforms (designed by the Website designer) and the actions of the community manager, by interviewing Website designers and community managers and then by confirming the data through a Delphi analysis.

In particular, some revealing elements were considered with the aim of expressing the quality and success (actual or potential) of the site, to identify the drivers on which to lever for a possible improvement and strengthening of motivations. Based on a comprehensive literature review a large number of motivations are listed and then identified in twenty platforms through a thorough analysis and systematization. Then, these platforms have been explored to find the drivers, i.e., the characteristics of the platforms and the managerial actions to enhance these motivations.

Moreover, we explicitly clarified the platform characteristics that determine each motivation. Once we analysed the site and understood how it functioned, its opportunities and its key success factors, we clarified the drivers that impact on discovered motivations, i.e., practical driversor levers that act to increase motivations or structural characteristics on which platforms should be designed and built. The next logical step has been to summarize all the motivations inferred from an analysis of individual sites and their drivers in a matrix.

In order to reduce the subjectivity of the analysis, in the first part of the research the two researchers autonomously analysed and evaluated the Websites, then compared their reduction and agreed on a common evaluation. In order to reinforce the analysis, we conducted a Delphi study with fourteen participants (three Web experts, four innovation experts, five community members and two Website designers) with high professional experience (an average of twelve years). They commented on the motivations and drivers of the platforms and also evaluated the platforms on a Likert scale from 1-5 (0 not present) for a set of indicators, such as Website popularity (number of participants, topics, posts etc.), level of competitiveness and collaboration, level of elaboration and efficacy of the graphic and audio-visual contents, partners or collaborators' and involved actors' popularity, successes and achieved goals. Finally, to study the reasons for the platforms' design elements and their impact on individual motivations and to deepen our comprehension of the reasons for the building, structure, modification and updating of the platforms, we interviewed Website designers, maintainers of the communities and community members.

The use of a qualitative empirical study methodology requires the definition of a research protocol selected in order to make a comparison and an aggregation of data collected. The protocol of the analysis was developed prior to data gathering and was based on the literature review of motivations and motivational drivers. The protocol of the analysis was constituted of the following parts:

*   General description: aim of the Website, objects, stakeholders, opportunities, etc.
*   Elements in the Website:
    *   Actors (individuals and companies belonging to the community or addressees of the invitation to collaborate);
    *   Typology of involvement (based on competition, collaboration, personal interest, etc.);
    *   Presence of sharing and collaborative environments (forums, blogs, sharable document repositories, Web-spaces for interventions and comments etc.);
    *   Presence of links to other sites and social networks;
    *   Visual choices (images, animation of the site etc.), site layout, explicit messages;
    *   Tools for the involvement of membership (mailing lists, newsletters etc.);
    *   Presence of elements to increase the credibility and the security of the site.
*   Motivations: starting from the framework previously proposed on the basis of theoretical considerations in open innovation platforms, we report all motivations identified by direct analysis of the Website.

## Results of the analysis

### Motivations in the open innovation platforms

For every platform we conducted an analysis of the specific motivations, the specific drivers for them and their relationships. Table 2 shows the key motivations that are levered by the platforms' designers and managers in order to push people to collaborate and innovate within the twenty platforms considered for our analysis.

<table width="100%" cellspacing="0" cellpadding="3" border="1" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Classification of the motivations in the platforms**</caption>

<tbody>

<tr>

<th colspan="3">Motivation</th>

<th>No.</th>

<th>Platform</th>

</tr>

<tr>

<th valign="middle" rowspan="10">Intrinsic</th>

</tr>

<tr>

<th valign="middle" rowspan="6">Individual</th>

</tr>

<tr>

<td align="center">Entrepreneurial mindset</td>

<td align="center">6</td>

<td align="left">Redesignme, shapeways, ponoko, quirky, thingverse, innoget</td>

</tr>

<tr>

<td align="center">Opportunity to express individual creativity</td>

<td align="center">12</td>

<td align="left">Ideaconnection, innovationexchange, redesignme, crowdspring, guerracreativa, ideabounty, shapeways, ponoko, quirky, thingverse, ideawicket, whynot</td>

</tr>

<tr>

<td align="center">Sense of membership</td>

<td align="center">20</td>

<td align="left">Innocentive, ideaconnection, ninesigma, innovationexchange, preasans, clickworker.com, redesignme, crowdspring, guerracreativa, ideabounty, enterprisespigit, bigideagroup, shapeways, ponoko, quirky, thingverse, ideawicket, whynot, innoget, hypios</td>

</tr>

<tr>

<td align="center">Enjoyment, fun and entertainment</td>

<td align="center">10</td>

<td align="left">Ideaconnection, redesignme, guerracreativa, enterprisespigit, bigideagroup, shapeways, ponoko, quirky, thingverse, ideawicket, whynot</td>

</tr>

<tr>

<td align="center">Psychological compensation, sense of efficacy</td>

<td align="center">20</td>

<td align="left">Innocentive, ideaconnection, ninesigma, innovationexchange, preasans, clickworker.com, redesignme, crowdspring, guerracreativa, ideabounty, enterprisespigit, bigideagroup, shapeways, ponoko, quirky, thingverse, ideawicket, whynot, innoget, hypios</td>

</tr>

<tr>

<th valign="middle" rowspan="3">Social</th>

</tr>

<tr>

<td align="center">Sense of cooperation</td>

<td align="center">7</td>

<td align="left">Ninesigma, innovationexchange, preasans, clickworker.com, crowdspring, quirky, thingverse</td>

</tr>

<tr>

<td align="center">Social responsibility</td>

<td align="center">10</td>

<td align="left">Innocentive, ideaconnection, ninesigma, innovationexchange, preasans, crowdspring, ideabounty, ponoko, thingverse, hypios</td>

</tr>

<tr>

<th valign="middle" rowspan="13">Extrinsic</th>

</tr>

<tr>

<th valign="middle" rowspan="4">Economic</th>

</tr>

<tr>

<td align="center">Monetary rewards</td>

<td align="center">17</td>

<td align="left">Innocentive, ideaconnection, innovationexchange, preasans, clickworker.com, redesignme, crowdspring, guerracreativa, ideabounty, enterprisespigit, bigideagroup, shapeways, ponoko, quirky, ideawicket, innoget, hypios</td>

</tr>

<tr>

<td align="center">Free products (hardware and software)</td>

<td align="center">3</td>

<td align="left">Redesignme, enterprisespigit, bigideagroup</td>

</tr>

<tr>

<td align="center">Free services</td>

<td align="center">6</td>

<td align="left">Ninesigma, shapeways, ponoko, quirky, thingverse, innoget</td>

</tr>

<tr>

<th valign="middle" rowspan="5">Professional</th>

</tr>

<tr>

<td align="center">Learning</td>

<td align="center">7</td>

<td align="left">Redesignme, crowdspring, big idea group, ponoko, quirky, thingverse, whynot</td>

</tr>

<tr>

<td align="center">Reputation</td>

<td align="center">12</td>

<td align="left">Innocentive, ideaconnection, ninesigma, innovationexchange, redesignme, crowdspring, guerracreativa, ideabounty, enterprisespigit, shapeways, quirky, innoget</td>

</tr>

<tr>

<td align="center">Career benefits</td>

<td align="center">9</td>

<td align="left">Innocentive, ideaconnection ninesigma, innovationexchange, crowdspring, idea bounty, enterprisespigit, big idea group, innoget</td>

</tr>

<tr>

<td align="center">Reciprocity</td>

<td align="center">14</td>

<td align="left">Innocentive, ideaconnection, ninesigma, innovationexchange, preasans, clickworker.com, guerracreativa, idea bounty, enterprisespigit, big dea roup, quirky, idea icket, innoget, hypios</td>

</tr>

<tr>

<th valign="middle" rowspan="3">Social</th>

</tr>

<tr>

<td align="center">Individual accountability</td>

<td align="center">5</td>

<td align="left">Ninesigma, innovationexchange, idea bounty, shapeways, quirky</td>

</tr>

<tr>

<td align="center">Social capital</td>

<td align="center">3</td>

<td align="left">Nine sigma, enterprise spigit, why not</td>

</tr>

</tbody>

</table>

### Motivational drivers in the open innovation platforms

We collected all the drivers (about fifty) that influenced users' motivations in participation and collaboration within platforms. They are listed in Table 3.

<table width="90%" cellspacing="0" cellpadding="3" border="1" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Motivational drivers of the platforms (together with the frequency in the platforms)**</caption>

<tbody>

<tr>

<th>Motivational drivers</th>

<th>#</th>

</tr>

<tr>

<td>Ideals of collaborative participation, crowdsourcing and open innovation</td>

<td align="center">4</td>

</tr>

<tr>

<td>Metaphor, as an incentive and stimulus to participation</td>

<td align="center">1</td>

</tr>

<tr>

<td>Objectives and philosophy of the site</td>

<td align="center">9</td>

</tr>

<tr>

<td>Possibility to collaborate and to negotiate a trade agreement with companies</td>

<td align="center">2</td>

</tr>

<tr>

<td>Possibility to open personal shops</td>

<td align="center">1</td>

</tr>

<tr>

<td>Possibility to purchase existing ideas and products in the shop within the site</td>

<td align="center">3</td>

</tr>

<tr>

<td>Possibility to realize personal ideas otherwise unfeasible</td>

<td align="center">3</td>

</tr>

<tr>

<td>Possibility to sell products designed (and/or manufactured) through the site</td>

<td align="center">2</td>

</tr>

<tr>

<td>Word of mouth: the members themselves suggest potential successful innovators</td>

<td align="center">1</td>

</tr>

<tr>

<td>Direct contacts with clients</td>

<td align="center">3</td>

</tr>

<tr>

<td>Ideas approved by client companies</td>

<td align="center">3</td>

</tr>

<tr>

<td>Problem solving: issues are proposed from outside and entrusted to the community members</td>

<td align="center">11</td>

</tr>

<tr>

<td>Satisfaction of specific requests from customers</td>

<td align="center">2</td>

</tr>

<tr>

<td>Creative structure of the site</td>

<td align="center">9</td>

</tr>

<tr>

<td>Evocative images referring explicitly to the concept of group</td>

<td align="center">1</td>

</tr>

<tr>

<td>Expressions present on the Website in the various pages accessed during the visit</td>

<td align="center">12</td>

</tr>

<tr>

<td>Games as stimulus for active participation</td>

<td align="center">1</td>

</tr>

<tr>

<td>List of winners and their explanatory interviews</td>

<td align="center">2</td>

</tr>

<tr>

<td>Messages inciting fun launched directly from/to potential contributors</td>

<td align="center">6</td>

</tr>

<tr>

<td>Need of a personal account in order to view the most important information inside the site</td>

<td align="center">5</td>

</tr>

<tr>

<td>Positive judgment on the network and on the site by external actors</td>

<td align="center">2</td>

</tr>

<tr>

<td>Recognition of best solvers between those involved in the resolution of proposed challenges</td>

<td align="center">7</td>

</tr>

<tr>

<td>Reward centre organized to incentivize the contributors' participation</td>

<td align="center">1</td>

</tr>

<tr>

<td>Opportunities of collaboration in relevant projects</td>

<td align="center">1</td>

</tr>

<tr>

<td>Opportunity to be promoted to a higher grade as recognition of a community member's effectiveness</td>

<td align="center">2</td>

</tr>

<tr>

<td>Opportunity to become a moderator</td>

<td align="center">2</td>

</tr>

<tr>

<td>Opportunity to become a leader within the subgroups of a community</td>

<td align="center">2</td>

</tr>

<tr>

<td>Possibility to create a descriptive personal account explicating details and information about education, competencies and interests</td>

<td align="center">4</td>

</tr>

<tr>

<td>Opportunity to enter into an elite community</td>

<td align="center">2</td>

</tr>

<tr>

<td>Collaboration between elite community and team of the site</td>

<td align="center">1</td>

</tr>

<tr>

<td>Direct contact between the site's team and participants (bidirectional)</td>

<td align="center">2</td>

</tr>

<tr>

<td>Messages with information, material, ideas sent by the site to the community members</td>

<td align="center">4</td>

</tr>

<tr>

<td>Collaboration between the community of consumers and the community of experts</td>

<td align="center">1</td>

</tr>

<tr>

<td>Possibility to create and nurture professional relations with other members of the community and with employees of companies with which participants come into contact</td>

<td align="center">3</td>

</tr>

<tr>

<td>Possibility to choose team members from those registered to the site</td>

<td align="center">1</td>

</tr>

<tr>

<td>Possibility to comment and to evaluate other posts</td>

<td align="center">4</td>

</tr>

<tr>

<td>Project feasibility evaluated by the community</td>

<td align="center">4</td>

</tr>

<tr>

<td>Choice of membership category based on interests and skills</td>

<td align="center">2</td>

</tr>

<tr>

<td>Exchange of information, ideas and knowledge inside special dedicated spaces of the site</td>

<td align="center">20</td>

</tr>

<tr>

<td>Organization of brainstorming</td>

<td align="center">1</td>

</tr>

<tr>

<td>Possibility to choose the categories of interest during the research of themes and topics to share</td>

<td align="center">5</td>

</tr>

<tr>

<td>Possibility to select materials to produce products</td>

<td align="center">1</td>

</tr>

<tr>

<td>Possibility to share information about projects and potential solutions for the physical realization</td>

<td align="center">2</td>

</tr>

<tr>

<td>Presence of data on the product and on the market (even if the idea is not carried out)</td>

<td align="center">1</td>

</tr>

<tr>

<td>Clients committed to social issues, as humanitarian aid agency or non-profit organizations</td>

<td align="center">2</td>

</tr>

<tr>

<td>Precautions for reducing waste and pollution</td>

<td align="center">1</td>

</tr>

<tr>

<td>Presence of social issues within the challenges</td>

<td align="center">7</td>

</tr>

<tr>

<td>Solutions for the common good regarding the social sphere</td>

<td align="center">1</td>

</tr>

<tr>

<td>Use of sustainable technologies and materials</td>

<td align="center">1</td>

</tr>

</tbody>

</table>

### The impact of platform drivers on users' motivations in collective innovation

We built for every platform a visual map of the relationships of drivers with users' motivation in that specific platform. All the causal relationships of impact between the motivational drivers and the intrinsic and extrinsic motivations are illustrated in Table 4\. These have been mapped in order to identify the most effective in promoting the users' participation and collaboration inside platforms. In the cells there are the specific platforms that have that combination of motivation and driver. Furthermore, after collecting all the platforms' drivers, we found similarities among them and therefore we categorized them in the following groups:

1.  _Website philosophy_: the drivers connected to the ideals and objectives of the platform;
2.  _Entrepreneurial and business opportunities_: the drivers connected to the possibility to do an entrepreneurial activity and to collaborate with companies;
3.  _Relationships with customers_: the drivers oriented to favour the interaction and collaboration with customers;
4.  _Website structure and visual aspects_: all the drivers connected to the design and style of the Website;
5.  _Personal profile development_: the drivers that favour the increase of the personal status;
6.  _Actions of the platform manager_: all the drivers that are connected with specific actions of the manager of the platform;
7.  _Features for creating and developing communities_: the features and drivers that favour the collaboration and sociality;
8.  _Features for accelerating innovation process_: all the features and tools that can promote users' creativity and accelerate the innovation process;
9.  _Social responsibility_: the drivers that stimulate social responsibility.

![Synthesis of drivers](Drivers.png) Table 4 ([click here for a pdf file](DRIVERS.pdf)) shows the groups of motivational drivers and their occurrence and can help in finding driver-motivation patterns in the specific context of a given platform. After identifying the most important motivations, the platform designer can look for patterns that help to identify the most important drivers for those motivations in the context of specific Website.

The _entrepreneurial mindset_ is, for example, favoured by the possibility to share information about projects and potential solutions for the physical realization of ideas (Thingiverse) or the possibility for the members to realize ideas which would otherwise be unfeasible, to open shops and to sell products designed (and/or manufactured) through the site (Shapeways, Ponoko).

In many platforms innovation and creativity are keywords. The _opportunity to express individual creativity_ derives clearly from messages on the Website (Ideaconnection, Innovationexchange). Creative people can propose ideas for the challenges posted by clients and win cash prizes (Ideabounty). The sites allow publishing and sharing creative ideas, even the most particular (Thingiverse).

The _sense of membership_ is often linked to the need to have an individual account in order to access the main sections of the site, for members to describe themselves, their areas of interest and their attitudes (Innovationexchange) and to be able to suggest other competent solvers (Innocentive). The physical feedback of the idea of being part of a group comes also from the list of problem solvers and their interviews: in the testimonies of the solver there is the explicit reference to the fact that the platform brings together and unites people with very different skills,knowledge and experiences (Ideaconnection), which is supported by messages that describe getting in touch with other members of the network to obtain and use all benefits and features offered by the platform (e.g., Preasans). Organizing brainstorming challenges is also significant. The sense of membership is developed by the fact that the community decides whether an idea is feasible (expressing votes) giving more importance to influencers (Quirky).

_Enjoyment, fun and entertainment_ are important motivations in the platforms. Community members are involved and interested in the topics of discussion and can post ideas and comment (Shapeways, Whynot). This can be linked to the organization of the reward centre aimed at engaging and pushing involvement and collaboration (Enterprisespigit). Thingiverse is founded on the ideals of open source and sharing projects and templates. Inventors are curious and interested to have a look and evaluate the ideas of others and to propose their own inventions and seeing them judged by others is enjoyable (Ideawicket).

_Psychological compensation_ and _sense of efficacy_ can be enhanced by images (Innocentive), by seeing the proposals for projects approved (Ninesigma), by acknowledgement of the success of their own work and ideas when choosing a winning team (Innovationexchange) or by success in the challenges (Preasans).

The _sense of cooperation_ stems when solvers join a group and when users enter in collaboration with other experts from the network, particularly when there is a responsibility to do well, rewarding the trust of community members of the team (Innovationexchange). This motivation stems from a possibility to share information about projects and potential solutions for their physical realization (Thingiverse).

Among the themes of problems brought by platforms, are those concerning the social sphere which motivates people who care about social themes. The _social responsibility_ motivation is recurrent, with themes concerning social problems, environmental problems and sustainability (Ideaconnection, Ninesigma). For example, among the clients of Ideabounty are associations and foundations such as the WWF, BreadLine Africa (charity organization) and the FNB (First National Bank - an ethical bank helping Africa) . The platforms ensure the materials used are sustainable and that carbon emissions are reduced by creating products on a site as close as possible to the point of consumption, minimizing the use of the traditional supply chain associated with the transportation and storage of products (e.g., Ponoko).

In many platforms _monetary rewards_ are one of the most important motivations (Innocentive). _Free products_ are offered for compensation when winning a challenge (Redesignme, Enterprisespigit) or alongside monetary remuneration (Big Idea Group). This is true also for free services. For example, Shapeways and Ponoko offer the possibility to upload templates, share them and open a personal online shop.

_Learning_ is stimulated by the exchange of information (Redesignme), the availability of data on products and markets (Thingiverse) and the possibility to share this and other information. Moreover, users are motivated by the opportunity to collaborate in relevant projects with consumers and the community of experts.

_Reputation_ can be linked to the fact that the solvers are expressly cited in the site (Ideaconnection) and promoted to the seekers (Innocentive). This increases the awareness of being part of the community of '_leaders in open innovation_' (Ninesigma). Users can increase their reputation by winning the challenges and going up a grade (Redesignme, Guerracreativa and Ideabounty). In some sites there is also a proper remuneration system (Enterprisespigit).

The motivation of _career benefits_ can stem from the chance to show off any future proposals and increase the professional curriculum (Ideaconnection), from the opportunity of collaboration in relevant projects (Enterprisespigit), from the opportunity to be promoted to a higher grade (Shapeways), from the opportunity to become a leader within the subgroups of a community (Innovationexchange), from direct contact with clients (Ideabounty, Ninesigma) and because ideas are approved directly by client companies (Innoget).

_Reciprocity_ is stimulated by direct contact with clients or the possibility to collaborate and negotiate trade agreements with companies. This is used in Innoget, where a mechanism called Ibox-out allows disclosing patents, innovative technologies and products to create collaborative projects or trade agreements with other organizations, with which they establish durable relations of reciprocity, or in Ninesigma, which clarifies that the participation represents a great opportunity for contributors: '_responding to a requests presents an opportunity for you to develop a mutually beneficial relationship with a large, globally-recognized partners with access to significant markets_'. Innovationexchange gives the possibility for users to choose their own team of innovators. This establishes a relationship of exchange and collaboration lasting over time. The site itself informs the innovators about the opportunity to develop relationships with other members, which can be useful even outside of the community to develop new business, investment research, recruiting, etc.

_Individual accountability_ can be enhanced by a set of very different drivers. For example, the personal profile development in Shapeways, where a system of _seniority_ can increase users' level of individual responsibility within the site, so contributors feel compelled to do well in realizing innovative ideas and products, especially when performing the role of moderator. This also happens with direct contact with clients as in Ideabounty or with the creation and community development, as in Quirky (where the influencers' community contributes to the decision to build a project from an external idea proposal by voting on projects). The site itself invites members to use the votes wisely and in a serious way. This gives more importance to influencers and automatically increases the level of individual responsibility inside the site and group: the influencers feel compelled to do well in judging the ideas to be implemented.

_Social capital_ is favoured by the possibility to create and nurture professional relationships: for example, in Whynot, within the community and the teams of players, interpersonal relations among members are increased, consolidated and enhanced by the popularity and the success of the site, in order to maintain the current members and enable the community grow.

## Discussion

Table 5 synthesizes the main findings of our research and highlights key success drivers that are important because they are:

*   the most frequent motivational drivers (high frequency),
*   the drivers impacting on the largest number of motivations (high impact),
*   the drivers impacting motivations usually not pushed by the majority of platforms (uncommon).

These drivers can be important to be implemented inside a platform to foster a collaborative innovation process by motivating users.

<table width="100%" cellspacing="0" cellpadding="3" border="1" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Classification of drivers based on frequency and impact**</caption>

<tbody>

<tr>

<th colspan="2">Motivational drivers</th>

<th>High frequency</th>

<th>High impact</th>

<th>Uncommon</th>

</tr>

<tr>

<th valign="middle" rowspan="3">Website philosophy</th>

</tr>

<tr>

<td>Ideals of collaborative participation, crowdsourcing and open innovation</td>

<td align="center"></td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>Objectives and philosophy of the site</td>

<td align="center">?</td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<th valign="middle" rowspan="3">Entrepreneurial and business opportunities</th>

</tr>

<tr>

<td>Possibility to purchase (but also sell) existing ideas and products in the shop within the site</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">?</td>

</tr>

<tr>

<td>Possibility to realize personal ideas otherwise unfeasible</td>

<td align="center"></td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<th valign="middle" rowspan="3">Relationships with customers</th>

</tr>

<tr>

<td>Ideas approved by client companies</td>

<td align="center">?</td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>Problem solving: issues and challenges are proposed from outside and entrusted to the members of the community</td>

<td align="center">?</td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<th valign="middle" rowspan="8">Website structure and visual</th>

</tr>

<tr>

<td>Creative structure of the site</td>

<td align="center">?</td>

<td align="center"></td>

<td align="center"></td>

</tr>

<tr>

<td>Expressions present on the Website in the various pages accessed during the visit</td>

<td align="center">?</td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>List of winners and their explanatory interviews</td>

<td align="center"></td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>Messages inciting fun launched directly from/to potential contributors</td>

<td align="center">?</td>

<td align="center"></td>

<td align="center"></td>

</tr>

<tr>

<td>Positive judgment on the network and on the site by external actors such as magazines, other sites, scientific journals, etc.</td>

<td align="center"></td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>Recognition of best solvers between those involved in the resolution of proposed challenges</td>

<td align="center"></td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>Reward centre organized to incentivize the contributors' participation</td>

<td align="center"></td>

<td align="center">?</td>

<td align="center">?</td>

</tr>

<tr>

<th valign="middle" rowspan="3">Personal profile development</th>

</tr>

<tr>

<td>Opportunity to be promoted to a higher grade, become a moderator, become leader, to enter into an elite community</td>

<td align="center"></td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>Possibility to create a descriptive personal account explicating details and information about education, competencies and interests</td>

<td align="center">?</td>

<td align="center"></td>

<td align="center"></td>

</tr>

<tr>

<th valign="middle" rowspan="2">Features for creating and developing communities</th>

</tr>

<tr>

<td>Possibility to create and nurture professional relations with other members of the community and with employees of companies with which solvers come into contact</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">?</td>

</tr>

<tr>

<th valign="middle" rowspan="3">Features for accelerating the innovation process</th>

</tr>

<tr>

<td>Exchange of information, ideas and knowledge inside special dedicated spaces of the site</td>

<td align="center">?</td>

<td align="center">?</td>

<td align="center"></td>

</tr>

<tr>

<td>Possibility to choose the categories of interest during the research of themes and topics to share and discuss</td>

<td align="center">?</td>

<td align="center"></td>

<td align="center"></td>

</tr>

<tr>

<th valign="middle" rowspan="2">Social responsibility</th>

</tr>

<tr>

<td>Presence of social issues within the challenges</td>

<td align="center">?</td>

<td align="center"></td>

<td align="center"></td>

</tr>

</tbody>

</table>

The _most frequent motivational drivers_ identified in the twenty platforms represent the drivers that a platform should have to be aligned with the most significant other platforms. Starting from the most frequent, they are in these groups:

*   drivers for accelerating the innovation process: exchange of information, ideas and knowledge inside special dedicated spaces of the site (18) and possibility to choose the categories of interest during the research of themes and topics to share and discuss (6);
*   Website structure and visual aspects: expressions present on the Website in the various pages accessed during the visit (13), creative structure of the site (9) and messages inciting fun launched directly from and to potential contributors (6);
*   relationships with customers: problem solving: issues and challenges are proposed from outside and entrusted to the members of the community (10) and idea approved by client companies (6);
*   Website philosophy: objectives and philosophy of the site (9);
*   social responsibility: presence of social issues within the challenges (7);
*   personal profile development: possibility to create a descriptive personal account, explicative details and information about education, competencies and interests (6).

The _drivers impacting on the largest number of motivations_ can be seen as the most efficient ones. Here we consider the efficiency as the ratio between the number of drivers (input) and the number of motivations (output). Clearly, if efficiency is viewed in terms of general performance of the platform, it is not certain that the drivers impacting on the largest number of motivations are the most efficient. For instance, user participation might be driven by only one specific type of motivation. In this case, the number of simultaneously influenced motivation types would not matter. A platform should have them to be competitive. The most important ones identified inside the twenty platforms analysed (together with the number of driven motivations) are in the following groups:

*   Website structure and visual aspects: expressions present on the Website in the various pages accessed during the visit (6), recognition of best solvers between those involved in the resolution of proposed challenges (6), list of winners and their explanatory interviews (5), reward centre organized to incentivize the contributors' participation (5) and positive judgment on the network and on the site by external actors such as magazines, other sites, scientific journals, etc. (4);
*   Website philosophy: objectives and philosophy of the site (6) and ideals of collaborative participation, crowdsourcing and open innovation (5);
*   personal profile development: opportunity to be promoted to a higher grade (6), become a moderator (6), become leader (6), to enter into an elite community (6);
*   entrepreneurial & business opportunities: possibility to realize personal ideas which would otherwise be unfeasible (5);
*   drivers for accelerating innovation: process exchange of information, ideas and knowledge inside special dedicated spaces of the site (4);
*   relationships with customers: ideas approved by client companies (4) and problem solving: issues and challenges are proposed from outside and entrusted to the members of the community (4).

The _drivers impacting motivations usually not pushed by the majority of platforms_ are those that can act as a competitive advantage because they are uncommon. A platform can focus on these drivers to be the most complete. Clearly these drivers can also be uncommon because they are irrelevant, but this can be evaluated only in the specific context of the platform, that is, if the motivation impacted is relevant for that platform. They are in these groups:

*   Website structure and visual aspects: reward centre organized to incentivize the contributors' participation (free product);
*   entrepreneurial and business opportunities: possibility to purchase (but also sell) existing ideas and products in the shop within the site (free product);
*   creation and community development: possibility to create and nurture professional relations with other members of the community and with employees of companies with which solvers come into contact (social capital).

## Conclusions

This paper is a first attempt to examine the knowledge sharing motivations and drivers in innovation platforms. It highlights the interaction between specific drivers for enhancing specific motivations to collaborate and innovate. The innovation achieved through mass collaboration or crowdsourcing is also acquired using the _knowledge about knowledge_ of contributors and of participants (from know-how to know who knows how) in a so-called open community. So this typology of innovation is intrinsically social. The (social) open innovation platforms are places where companies can find the collective intelligence of stakeholders' communities, capture outstanding ideas, and do crowdsourcing by fostering bottom-up innovation within or beyond organizational boundaries. The social aspect is fundamental; therefore in the platform it is important _to create an attractive environment to motivate people_. As with any open and social community, motivation to participate is one important key. As such the success or failure of an attempt at crowdsourced innovation lies with the platform managers' ability to drive the motivation of such participation. The challenge in enabling communities for innovation is to continuously support and enrich participation and contribution. The role of the community manager and Website designer is to create conditions for generation and dissemination of knowledge and to favour knowledge sharing. This is achieved by promoting conditions for an open exchange of ideas and information and by supporting innovative thinking.

Returning to the research question, the results provide suggestions of the drivers to consider if the aim is to increase motivation and foster a collaborative innovation process. We categorized the drivers into nine groups based on similarity. Some drivers are more efficient than others and some are rarer, we deem that they are the key drivers for success in terms of impact on motivations. The Website structure and the visual aspects are fundamental. The more advanced but infrequent characteristics are those related to selling and building professional communities and supply networks. Many platforms count on drivers such as creative structure of the site, messages inciting fun, possibility to create a descriptive account, possibility to choose the categories of interest or the presence of social issues, but they seem to not have a strong impact on motivations. Moreover, some sites forget very important drivers (the ones with high impact but not with high frequency), such as the objective and philosophy of the site, the possibility to realize personal ideas, the positive judgment of the network or the recognition of best solvers. Finally, the reward centre is an important driver both in terms of impact and differentiation: with high impact and also uncommon.

The findings can be useful from a practitioner's point of view, because they suggest to community managers and platform designers which kind of choices of design for the platform can impact on motivations in order to attract potentially innovative participants and sustain a high level of collaboration for innovation-related contributions. The findings are also valuable from a research point of view, because we built the first conceptual framework for evaluating the impact of motivational drivers inside platforms and provided an overview and a comprehensive analysis of twenty platforms. The researchers are working on further research, for example extending the number of the sample and relating the drivers to the projects' performance and to the success variables or to the peculiar business model of the platform, in order to identify the strategies (i.e. the target-oriented combination of motivational drivers through a particular platform). Moreover, another research direction can be a contingent approach, in order to look for platforms that fit best with particular circumstances. For example, taking into consideration the problem of communities, we can investigate if there are differences due to the composition of communities (that is, communities of experts, communities of users, etc.). Finally, we are working on a survey to test how these drivers affect the users, the real impact of the drivers.

## Acknowledgements

The research leading to these results has received funding from the European Community's Seventh Framework Programme managed by [REA-Research Executive Agency](http://ec.europa.eu/research/rea) ([FP 7 – SME 2008 – 2]) under grant agreement no. [243593], project [COLLECTIVE](http://www.collectiveproject.eu) – Emerging communities for collective innovation: ICT Operational tool and supporting methodologies for SME Associations.

We also wish to thank the editor and the anonymous referees for their useful suggestions on the first version of this paper. Thanks also to copy-editors of the journal.

## About the authors

Cinzia Battistella, Ph.D., is a post-doctoral researcher and lecturer in Innovation Management at the University of Udine. Her scientific interests are in the fields of innovation and strategic management, with primary focuses on the themes of foresight and open and collaborative innovation. She can be contacted at: [cinzia.battistella@uniud.it](mailto:cinzia.battistella@uniud.it)  
Fabio Nonino, Ph.D., is an Assistant Professor of Management Engineering at Sapienza University of Rome. His principal research interests concern informal networks, knowledge management, project management, operations and supply chain management. He can be contacted at: [fabio.nonino@uniroma1.it](mailto:fabio.nonino@uniroma1.it)

#### References

*   Aalbers, M. (2004). [Motivation for participating in an online open source software community](http://www.webcitation.org/668Tr33gJ). Retrieved 13 March, 2012 from http://download.blender.org/documentation/bc2004/Martine_Aalbers/results-summary.pdf (Archived by WebCite® at http://www.webcitation.org/668Tr33gJ).
*   Alavi, M. & Leidner, D.E. (2001). Review: knowledge management and knowledge management systems: conceptual foundations and research issues. _MIS Quarterly_, **25**(1), 107-136.
*   Albors, J., Ramos, J.C. & Hervas, J.L. (2008). New learning network paradigms: communities of objectives, crowdsourcing, wikis and open source. _International Journal of Information Management_, **28**(3), 194-202.
*   Aldrich, H.E. & Sasaki, T. (1995). R&D consortia in the United States and Japan. Research Policy, **24**(2), 301-316.
*   Al-Shammari, M. (2008). Toward a knowledge management strategic framework in the Arab region. _International Journal of Knowledge Management_, **4**(3), 44-63.
*   Amabile, T. M. (1996). _Creativity in Context_. Boulder, CO: Westview Press.
*   Amabile, T.M. (1998). How to kill creativity: keep doing what you're doing. Or, if you want to spark innovation, rethink how you motivate, reward, and assign work to people. _Harvard Business Review_, **76**(5), 77-87.
*   Amabile, T.M., Hennessey, B.A. & Grossman, B.S. (1986). Social influences on creativity: the effects of contracted-for reward. _Journal of Personality and Social Psychology_, **50**(1), 14-23.
*   Amabile, T.M., Hill, K.G., Hennessey, B.A. & Tighe, E.M. (1994). The work preference inventory: assessing intrinsic and extrinsic motivational orientations. _Journal of Personality and Social Psychology_, **66**(5), 950-967.
*   Anderson, C. (2009). _Free: the future of a radical price_. New York, NY: Hyperion.
*   Antikainen, M., Mäkipää, M. & Ahonen, M. (2010). Motivating and supporting collaboration in open innovation. _European Journal of Innovation Management_, **13**(1), 100-119.
*   Antikainen, M. & Vaataja, H.K. (2010). Rewarding in open innovation communities - how to motivate members. _International Journal of Entrepreneurship and Innovation Management_, **11**(4), 440-456.
*   Argote, L., McEvily, B. & Reagans, R. (2003). Managing knowledge in organizations: an integrative framework and review of emerging themes. _Management Science_, **49**(4), 571-582.
*   Bagozzi, R.P. & Dholakia, U.M. (2002). Intentional social action in virtual communities. _Journal of Interactive Marketing_, **16**(2), 2-21.
*   Bandura, A. (1995). _Self-efficacy in changing societies_. Cambridge: Cambridge University Press.
*   Barachini, F. (2009). Cultural and social issues for knowledge sharing. _Journal of Knowledge Management_, **13**(1), 98-110.
*   Battistella, C., Biotto, G. & De Toni, A.F. (2012). From design driven innovation to meaning strategy. _Management Decision_, **50**(4), In press.
*   Battistella, C. & Nonino, F. (2012). Exploring the impact of motivations on the attraction of innovation roles in open innovation Web-based platforms. _Production planning and control_, Forthcoming
*   Benbya, H. & Belbaly, N. (2010). Understanding developers' motives in open source projects: a multi-theoretical framework. _Communications of the association for information systems_, **27**(30), 589-610.
*   Benkler, Y. (2002). [Coase's penguin, or Linux and _The nature of the firm_](http://www.webcitation.org/663oAqqSv). _Yale Law Journal_, **112**(3), 369-447\. Retrieved 9 March, 2012 from http://www.yale.edu/yalelj/112/BenklerWEB.pdf (Archived by WebCite® at http://www.webcitation.org/663oAqqSv)
*   Bergquist, M. & Ljungberg, J. (2001). The power of gifts: organizing social relationships in open source communities. _Information Systems Journal_, **11**(4), 305-320.
*   Bhatt, G. (2001). Knowledge management in organizations: examining the interactions between technologies, techniques and people. _Journal of Knowledge Management_, **5**(1), 68-75.
*   Bitzer, J., Schrettl, W. & Schröder, P.J. (2007). Intrinsic motivation in open source software development. _Journal of Comparative Economics_, **35**(1), 160-169.
*   Bock, G.W., Zmud, R.W., Kim, Y.G. & Lee, J.N. (2005). Behavioral intention formation in knowledge sharing: examining the roles of extrinsic motivators, social-psychological forces, and organizational climate. _MIS Quarterly_, **29**(1), 87-111.
*   Bonaccorsi, A. & Rossi, C. (2006). Comparing motivations of individual programmers and firms to take part in the open source movement: from community to business. _Knowledge, Technology and Policy_, **18**(4), 40-64.
*   Bordia, P., Irmer, B.E. & Abusah, D. (2006). Differences in sharing knowledge interpersonally and via databases. The role of evaluation apprehension and perceived benefits. _European Journal of Work and Organizational Psychology_, **15**(2), 262-280.
*   Bowman, S. & Willis, C. (2003). _[We media: how audiences are shaping the future of news and information.](http://www.webcitation.org/663qWOl9O)_ Reston, VA: The Media Center at the American Press Institute. Retrieved 9 March, 2012 from http://www.mediacenter.org/mediacenter/research/wemedia/. (Archived by WebCite® at http://www.webcitation.org/663qWOl9O)
*   Brette, O. & Chappoz, Y. (2007). The French competitiveness clusters: toward a new public policy for innovation and research? _Journal of Economic Issues_, **41**(2), 391-398.
*   Brown, J.S. & Duguid, P. (2000). Organizational learning and communities of practice: toward a unified view of working, learning and innovation. In E.L. Lesser, M.A. Fontaine & J.A. Slusher (Eds.), _Knowledge and communities_, (pp. 99-121). Boston, MA: Butterworth Heinemann.
*   Brown, P. & Lauder, H. (2000). Human capital, social capital and collective intelligence. In S. Baron, J. Field & T. Schuller (Eds.), _Social capital: critical perspectives_ (pp.26-42). Oxford: Oxford University Press.
*   Bryant, S., Forte, A. & Bruckman, A. (2005). [Becoming Wikipedian: transformation of participation in a collaborative online encyclopedia](http://www.webcitation.org/663qrW4iS). In _GROUP05: International Conference on Supporting Group Work November 6-9, 2005, Sanibel Island, Florida_, (pp. 1-10). New York, NY: ACM Press. Retrieved 9 March, 2012 from http://www.butlercommonplace.org/thoughts/images/8/85/Bryantetal2005.pdf (Archived by WebCite® at http://www.webcitation.org/663qrW4iS)
*   Bunderson, J.S. & Sutcliffe, K.M. (2002). Comparing alternative conceptualizations of functional diversity in management teams: process and performance effects. _Academy of Management Journal_, **45**, 875-893.
*   Bush, A. & Tiwana, A. (2005). Designing sticky knowledge networks. _Communications of the ACM_, **48**(5), 67-71.
*   Chesbrough, H. (2003). _Open innovation: the new imperative for creating and profiting from technology_. Boston, MA: Harvard Business School Press.
*   Constant, D., Kiesler, S. & Sproull, L. (1994). What's mine is ours, or is it? A study of attitudes about information sharing. _Information Systems Research_, **5**(4), 400-21.
*   Davenport, T.H., De Long, D.W. & Beers, M.C. (1998). Successful knowledge management projects. _Sloan Management Review_, **39**(2), 43-57.
*   Davenport, T.H. & Prusak, L. (1998). _Working knowledge: how organizations manage what they know_. Boston, MA: Harvard Business School Press.
*   David, P.A., Waterman, A. & Arora, S. (2003). _[FLOSS-US: the free/libre/open source survey for 2003\. First Report](http://www.webcitation.org/663smFVMg)_. Stanford, CA: Stanford Institute for Economic Policy Research. (SIEPR Open Source Economics Project Working Paper). Retrieved 9 March, 2012 from http://www.stanford.edu/group/floss-us/report/FLOSS-US-Report.pdf. (Archived by WebCite® at http://www.webcitation.org/663smFVMg)
*   De Toni, A.F., Biotto, G. & Battistella, C. (2012). Organizational design drivers to enable emergent creativity in Web-based communities. _The Learning Organization_, **19**. Forthcoming.
*   De Toni, A.F. & Nonino, F. (2010). The key roles in the informal organization: a network analysis perspective. _The Learning Organization_, **17**(1), 86-103.
*   De Toni, A.F., Pivetta, M. & Nonino, F. (2011). A model for assessing the coherence of companies' knowledge strategy. _Knowledge Management Research and Practice_, **9**(4), 327-341.
*   Deci, E.L. & Ryan, RM (1980). The empirical exploration of intrinsic motivational processes. _Advances in Experimental Social Psychology_, **13**, 39-80.
*   Deci, E.L. & Ryan, R.M. (2000). The "What" and "Why" of goal pursuits: human needs and the self-determination of behavior. _Psychological Inquiry_, **11**(4), 227-268.
*   Deci, E.L. & Ryan, R.M. (2002). _Handbook of self-determination research_. Rochester, NY: University of Rochester Press.
*   Deci, E.L. & Ryan, R.M. (1985). _Intrinsic motivation and self-determination in human behavior_. New York, NY: Plenum Press.
*   Dholakia, U.M., Bagozzi, R. & Pearo, L.K. (2004). A social influence model of consumer participation in network- and small-group-based virtual communities. _International Journal of Research in Marketing_, **21**(3), 241-263.
*   Dodgson, M., Gann, D. & Salter, A. (2006). The role of technology in the shift towards open innovation: the case of Procter & Gamble. _R&D Management_, **36**(3), 333-346.
*   Dyer, J.H. & Nobeoka, K. (2000). Creating and managing a high performance knowledge-sharing network: the Toyota case. _Strategic Management Journal_, **21**, 345-367.
*   Eisenhardt, K.M. & Graebner, M.E. (2008). Theory building from cases: opportunities and challenges. _Academy of Management_, **50**(1), 25-32.
*   Fleming, L. & Waguespack, D.M. (2007). Brokerage, boundary spanning, and leadership in open innovation communities. _Organization Science_, **18**, 165-180.
*   Fulk, J., Heino, R., Flanagin, A.J., Monge, P.R. & Bar, F. (2004) A test of the individual action model for organizational information commons. _Organization Science_, **15**(5), 569-585.
*   Füller, J., Jawecki, G. & Mühlbacher, H. (2006). Innovation creation by online basketball communities. _Journal of Business Research_, **60**(1), 60-71.
*   Geragthy, K. & Desouza, K. (2005). Optimizing knowledge networks. _Industrial Management_, **47**(6), 25-32.
*   Ghosh, R.A. (2005). _Understanding free software developers: findings from the FLOSS study. Perspectives on free and open source software_. Boston, MA: MIT Press.
*   Gruen, T.W., Osmonbekov, T. & Czaplewski, A.J. (2005). How e-communities extend the concept of exchange in marketing: an application of the motivation, opportunity, ability (MOA) theory. _Marketing Theory_, **5**(1), 33-49.
*   Hagel, J. & Armstrong, A. (1997). _Net gain: expanding markets through virtual communities_. Boston, MA: McKinsey and Company.
*   Hargadon, A. & Bechky, B. (2006). When collections of creatives become creative collectives: a field study of problem solving at work. _Organization Science_, **17**(4), 484-500.
*   Hars, A. & Ou, S. (2002). Working for free? Motivations for participating in open-source projects. _International Journal of Electronic Commerce_, **6**(3), 25-39.
*   Haruvy, E., Prasad, A. & Sethi, S. (2003). Harvesting altruism in open-source software development. _Journal of Optimization Theory and Applications_, **118**(2), 381-416.
*   Hemetsberger, A. (2002). Fostering cooperation on the Internet: social exchange processes in innovative virtual consumer communities. _Advances in Consumer Research_, **29**, 354-356\. Retrieved 13 March, 2012 from http://flosshub.org/sites/flosshub.org/files/hemetsberger2.pdf (Archived by WebCite® at http://www.webcitation.org/668UYaFhx).
*   Hemetsberger, A. (2004). _[When consumers produce on the internet: the relationship between cognitive-affective, socially-based, and behavioral involvement of prosumers](http://www.webcitation.org/663tOaQI7)_. Working paper. Retrieved 9 March, 2012 from http://flosshub.org/system/files/hemetsberger1.pdf. (Archived by WebCite® at http://www.webcitation.org/663tOaQI7)
*   Hertel, G., Niedner, S. & Herrmann, S. (2003). Motivation of software developers in open source projects: an Internet-based survey of contributors to the Linux kernel. _Research Policy_, **32**(7), 1159-1177.
*   Howe, J. (2006). [The rise of crowdsourcing](http://www.webcitation.org/663teJjJK). _Wired_, **14**(6). Retrieved 9 March, 2012 from http://www.wired.com/wired/archive/14.06/crowds.html. (Archived by WebCite® at http://www.webcitation.org/663teJjJK)
*   Huston, L. & Sakkab, N. (2006). Connect and develop: inside Procter & Gamble's new model for innovation. _Harvard Business Review_, **84**(3), 58-66.
*   Hutchings, K. & Weir, D. (2006). Understanding networking in China and the Arab World: Lessons for international managers. _Journal of European Industrial Training_, **30**(4), 272-290.
*   Jeppesen, L. & Frederiksen, L. (2006). Why do users contribute to firm-hosted user communities? The case of computer-controlled music instruments. _Organization Science_, **17**(1), 45-63.
*   Kafentzis, K., Mentzas, G., Apostolou, D. & Georgolios, P. (2005). Knowledge marketplaces: strategic issues and business models. _Journal of Knowledge Management_, **8**(1), 130-46.
*   Kaiser, S., Kansy, S., Mueller-Seitz, G. & Ringlstetter, M. (2010). The motivation of bloggers for organizational knowledge sharing and creation: a comparative case study to identify contingency factors influencing motivation. _International Journal of Knowledge Management Studies_, **4**(1), 80-108.
*   Kankanhalli, A., Tan, B.C.Y. & Wei, K.K. (2005). Contributing knowledge to electronic knowledge repositories: an empirical investigation. _MIS Quarterly_, **29**(1), 113-143.
*   Kollock, P. (1999). The economies of online cooperation: gifts and public goods in cyberspace. In M. Smith & P. Kollock (Eds.), _Communities in cyberspace_ (pp. 220-239). London: Routledge.
*   Lakhani, K.R. & von Hippel, E. (2003). How open source software works: 'free' user-to-user assistance. _Research Policy_, **32**(6), 923-943.
*   Lakhani, K.R. & Wolf, R. (2005). Why hackers do what they do: understanding motivation and effort in free/open source software projects. In J. Feller, B. Fitzgerald, S. Hissam & K. Lakhani (Eds.), _Perspectives on free and open source software_. Cambridge: MIT Press.
*   Lattemann, C. & Stieglitz, S. (2005). Framework for governance in open source communities. In _Proceedings of the 38th Annual Hawaii International Conference on System Sciences (HICSS'05)_. (pp. 192.1) Washington, DC: IEEE Computer Society.
*   Lave, J. & Wenger, E. (1998). _Communities of practice: learning, meaning, and identity_. Cambridge: Cambridge University Press.
*   Leimeister, J.M., Huber, M.J., Bretschneider, U. & Krcmar, H. (2009). Leveraging crowdsourcing: activation-supporting components for IT-based idea competitions. _Journal of Management Information Systems_, **26**(1), 197-224.
*   Lerner, J. & Tirole, J. (2002). The simple economics of open source. _Journal of Industrial Economics_, **52**, 197–234.
*   Lindenberg, S. (2001). Intrinsic motivation in a new light. _Kyklos_, **54**(2/3), 317-342.
*   Luthiger, B. & Jungwirth, C. (2007). [Pervasive fun](http://www.webcitation.org/663umkUCm). _First Monday_, **12**(1). Retrieved 9 March, 2012 from http://www.firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1422/1340\. (Archived by WebCite® at http://www.webcitation.org/663umkUCm)
*   McDermott, R. & O’Dell, C. (2001). Overcoming cultural barriers to sharing knowledge. _Journal of Knowledge Management_, **5**(5), 76 - 85.
*   McLure Wasko, M. & Faraj, S. (2000). "It is what one does": why people participate and help others in electronic communities of practice. _Journal of Strategic Information Systems_, **9**(2-3), 155-173.
*   McLure Wasko, M. & Faraj, S. (2005). Why should I share? Examining social capital and knowledge contribution in electronic networks of practice _MIS Quarterly_, **29**(1), 35-57.
*   Martin, M.W. (2000). _Meaningful work: rethinking professional ethics_. Oxford: Oxford University Press.
*   Martin, M.W. (2002). Personal meaning and ethics in engineering. _Science and Engineering Ethics_, **8**(4), 545-560.
*   Monck, S. Porter, R., Quintas, P., Storey, D.J. & Wynarczyk, P. (1988). _Science parks and the growth of high technology firms_, London: McLintock.
*   Nardi, B.A., Schiano, D.J., Gumbrecht, M. & Swartz, L. (2004). Why we blog. _Communications of the ACM_, **47**(12), 41-46.
*   Nov, O. (2007). What motivates Wikipedians? _Communications of the ACM_, **24**, 50(11), 60-64.
*   Osterloh, M. & Frey, B.S. (2000). Motivation, knowledge transfer, and organizational forms. _Organization Science_, **11**(5), 538-550.
*   Osterloh, M. & Rota, S. (2007). Open source software development: just another case of collective invention? _Research Policy_, **36**(2), 157-171.
*   Quigley, N.R. Tesluk, P.E. Locke, E.A. & Bartol, K.M. (2007). A multilevel investigation of the motivational mechanisms. _Organization Science_, **18**(1), 71–88.
*   Pisano, G. & Verganti, R. (2008). Which kind of collaboration is right for you? _Harvard Business Review_, **86**(12), 78-86.
*   Raymond, E. (1999). The cathedral and the bazaar. _Knowledge echnology and Policy_, **12**(3), 23-49.
*   Reagans, R. & Bill McEvily, B. (2003). Network Structure and Knowledge Transfer: The Effects of Cohesion and Range. _Administrative Science Quarterly_, **48**(2), 240-267.
*   Rheingold, H. (1993). _The virtual community: homesteading on the electronic frontier_. New York, NY: Addison-Wesley.
*   Rheingold, R. (2002). _Smart mobs: the next social revolution_. Cambridge, MA: Perseus Publishing.
*   Ridings, C. & Gefen, D. (2004). [Virtual community attraction: why people hang out online](http://www.webcitation.org/663v1gPM6). _Journal of Computer-Mediated-Communication_, **10**(1). Retrieved 13 January, 2010 from http://jcmc.indiana.edu/vol10/issue1/ridings_gefen.html (Archived by WebCite® at http://www.webcitation.org/663v1gPM6)
*   Roberts, J.A., Hann, I.H. & Slaughter, S.A. (2006). Understanding the motivations, participation, and performance of open source software developers: a longitudinal study of the Apache projects. _Management Science_, **52**(7), 984-999.
*   Ryan, R. & Deci, E. (2000). Intrinsic and extrinsic motivations: classic definitions and new directions. _Contemporary Educational Psychology_, **25**(1), 54-67.
*   Scheel, C. (2002). Knowledge clusters of technological innovation systems. _Journal of Knowledge Management_, **6**(4), 356-367.
*   Siemsen, E., Roth, A.V. & Balasubramanian, S. (2008). How motivation, opportunity, and ability drive knowledge sharing: the constraining factor model. _Journal of Operations. Management_, **26**(3), 426-445.
*   Spaeth, S., Haefliger, S., von Krogh, G. & Birgit, R. (2008). [Communal resources in open source software development](http://www.webcitation.org/663vDma8V). _Information Research_, **13**(1), paper 332\. Retrieved 9 March, 2012 from http://informationr.net/ir/13-1/paper332.html (Archived by WebCite® at http://www.webcitation.org/663vDma8V).
*   Stallman, R. (1999). The GNU operating system and the free software movement. In C. DiBona, S. Ockman & M. Stone (Eds.), _Open sources: voices from the open source revolution_ (pp. 53-70). Sebastopol, CA: O'Reilly.
*   Stewart, K. & Gosain, S. (2006). The impact of ideology on effectiveness in open source software development teams. _MIS Quarterly_, **30**(2), 291-314.
*   Szulanski, G. (1996). Exploring internal stickiness: impediments to the transfer of best-practice within the firm. _Strategic Management Journal_, **17**27-43.
*   Tapscott, D. & Williams, A. (2006). _Wikinomics: how mass collaboration changes everything_. New York, NY: Portfolio.
*   Torvalds, L. & Diamond, D. (2001). _Just for fun: the story of an accidental revolutionary_. New York, NY: Harper Business.
*   Von Hippel, E. (2005). _Democratizing innovation_. Cambridge, MA: The MIT Press.
*   Von Hippel, E. & von Krogh, G. (2003). Open source software and the "private-collective" innovation model: issues for organization science. _Organization Science_, **14**(2), 209-223.
*   Von Krogh, G. & von Hippel, E. (2006). The high promise of research on open source software. _Management Science_, **52**(7), 975-983.
*   Von Krogh, G., Spaeth, S., Haefliger, S. & Wallin, M. (2008). _[Open source software: what we know (and do not know) about motives to contribute.](http://www.webcitation.org/663vTBGmz)_. Zurich, Switzerland: Eidgenössische Technische Hochschule. Retrieved 10 March, 2012 from http://www.dime-eu.org/files/active/0/WP38_vonKroghSpaethHaefligerWallin_IPROSS.pdf (Archived by WebCite® at http://www.webcitation.org/663vTBGmz).
*   Wang, Y. & Fesenmaier, D.R. (2003). Assessing motivation of contribution in online communities. _Electronic Markets_, **13**(1), 33-45.
*   Warkentin, M., Sugumaran, V. & Bapna, R. (2001). Intelligent agents for electronic commerce: trends and future impact on business models and markets. In Rahman, S.M. & Bignall R.J. (eds.). _Internet commerce and software agents: cases, technologies and opportunities_. (pp. 101-120). Hershey, PA: Idea Group Publishing.
*   Wiertz, C. & de Ruyter, K. (2007). Beyond the call of duty: why customers contribute to firm-hosted commercial online communities. _Organization Studies_, **28**(3), 347-76.
*   Wittenbaum, G.M., Hollingshead, A.B. & Botero, I.C. (2004). From cooperative to motivated information sharing in groups: moving beyond the hidden profile paradigm. _Communication Monographs_, **71**(3), 286-310.
*   Wu, C.G., Gerlach, J.H. & Young, C.E. (2007). Anempirical analysis of open source software developers' motivations and continuance intentions. _Information and Management_, **44**(3), 253-262.
*   Ye, Y. & Kishida, K. (2003). [Toward an understanding of the motivation of open source software developers](http://www.webcitation.org/663vlHgBi). In _Proceedings of 2003 International Conference on Software Engineering (ICSE2003), Portland, OR, May, 2003._, (pp. 419-429). Washington, DC: IEEE Computer Society. Retrieved 10 March, 2012 from http://l3d.cs.colorado.edu/~yunwen/papers/ICSE03.pdf (Archived by WebCite® at http://www.webcitation.org/663vlHgBi)
*   Yin, R.K. (2003). _Case study research design and methods_. Newbury Park, CA: Sage.
*   Zeitlyn, D. (2003). Gift economies in the development of open source software: anthropological reflections. _Research Policy_, **32**(7), 1287-1291.