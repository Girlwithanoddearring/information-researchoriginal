#### vol. 17 no. 1, March 2012

# Electronic books: content provision and adoption possibilities among users in Latvia

#### [Aiga Grenina](#aut)  
Department of Information and Library Studies, University of Latvia, Riga, Latvia

#### Abstract

> **Introduction.** This paper provides a brief summary of the most common advantages and disadvantages of electronic books (e-books), based on the diffusion of innovation theory. The first part of this research deals with the analysis of Latvian electronic book collections available on the Internet. The second part examines the usability of e-books. Finally, the author suggests ways for more successful adoption of electronic books.  
> **Method.** The research was based on content analysis and examines how three different user groups interact with e-books, using verbal protocol (as the main research method) and a questionnaire (as a secondary research method).  
> **Analysis.** The following categories were used to perform content analysis of Latvian e-book collections: general information, availability, contents provision and information retrieval possibilities. The verbal protocol data and notes taken during the observation process are transcribed and compared with answers obtained from questionnaires and content analysis.  
> **Results.** Content analysis shows there are at least fourteen collections of Latvian e-books available on the Internet, offering a total of 527 e-books. The results from verbal protocols and questionnaires clearly show different expectations of and requirements from e-books among different user groups. These differences are mainly based on the respondent's age and their different levels of computer skills.  
> **Conclusions.** The research shows that the analysed e-books are not suitable for long-term reading. Only successful utilisation of the many e-book advantages could make them more attractive than traditional books.

## Introduction

The print book has not fundamentally changed over centuries, despite transformations in other inventions and the development of new technologies. However, during the past decades, the electronic book (e-book) has been developed as an alternative to the traditional book. Eco is '_pretty sure that new technologies will render obsolete many kinds of books, like encyclopaedias and manuals_' ([1996: 299](#eco1996)). Eco identifies two types of books, serving two types of reading needs:

*   books for systematic or linear reading (fiction, and other literature '_for any circumstance in which one needs to read carefully, not only to receive information but also to speculate and to reflect about it_') ([1996: 300](#eco1996));
*   books for non-systematic or non-linear reading (reference literature) ([1996](#eco1996)).

Since the emergence of e-books, the concept of the traditional book has been under review. It is not completely understood how recent technological developments like the Internet, social media, and other developments in data delivery formats etc., might influence the book industry. A user's willingness to accept and use an e-book instead of a traditional book is another unclear and not thoroughly analysed issue.

Topics related to e-books have been examined from different perspectives. The use of e-books in libraries and in the academic environment are the most studied aspects. Rao ([2005](#rao2005)) considers the use, perspectives and integration of e-books in libraries and information centres. His colleague Pietilä ([2005](#pie2005)) conducted her Master's thesis on how to use and read e-books. E-books have also been studied by providers of e-books. For example, _eBrary_, a provider of e-content services and technology conducted worldwide surveys of students ([eBrary 2008](#ebr2008)), academics ([eBrary 2007a](#ebr2007a)) and librarians ([eBrary 2007b](#ebr2007b)) regarding their usage, needs, and perceptions of e-books. This author agrees with other researchers (e.g., [Hoffmann _et al._ 2009](#hof2009)) that, whilst many studies examine user opinions of e-books, only a few consider their usability. One such study analysed user experiences when accessing information through electronic and printed books. It concluded that the users' expectations were not met when using e-books for the first time ([Hoffmann _et al._ 2009](#hof2009)).

This paper deals with the current situation of e-book usage in Latvia, one of the Baltic region countries of Northern Europe with its own national language and a population of 2.25 million ([Central Statistical Bureau of Latvia 2010](#csb2010)).

The purpose of this study is to determine the current provision of collections of Latvian e-books available on the Internet by analysing the advantages and disadvantages of all Latvian e-books available in these collections and to come to a conclusion regarding the usability of e-books among three different user groups.

## Literature review

The adoption of e-books depends on different aspects. Availability, contents, information retrieval possibilities and users' willingness to accept e-books as a relevant source of information are considered the key factors for a successful adoption process. Characteristics of e-books and their current adoption rate within society are the main aspects analysed in this paper. The author has used the diffusion of innovation theory for the research.

### Diffusion of innovation theory

According to the diffusion of innovation theory the following questions must be taken into account when the adoption possibilities of an innovation are studied:

*   What is the innovation?
*   How does it work?
*   Why does it work?
*   What are its consequences?
*   What are its current advantages and disadvantages? ([Rogers 2003](#rog2003))

The theory has been widely used for researching the adoption of information and communication technologies and electronic publishing ([Hanh and Schoch 1997](#hah1997); [Lajoie-Paquette 2005](#laj2005)). According to the theory, it is often difficult to get an idea, practice or object adopted, even when it has obvious advantages ([Rogers 2003](#rog2003)). Usually it takes a long time before an innovation is widely adopted. Critics of the theory recognise it as a good descriptive tool, but consider it less strong in explanatory issues, and less useful in predicting outcomes, and providing guidance as to how to accelerate the rate of adoption ([Clarke 1999](#cla1999)). However the broad framework of the theory provides a platform for investigations of innovations in the library and information science. Despite its shortcomings, the theory is sufficiently robust in explaining the adoption and/or diffusion of information and communication technology innovations ([Minishi-Majanja and Kiplang 2005](#min2005)).

### The concept of e-book

Studies of e-books show that there is no generally accepted definition ([Armstrong 2008](#arm2008); [Vassiliou and Rowley 2008](#vas2008)). One of the simplest and most comprehensive explanations defines e-books as '_an online version of printed books, accessed via the Internet_' ([Gold Leaf 2003: 17](#gol2003)). Other definitions allow classification of a wide range of documents under the e-book label: '_any piece of electronic text regardless of size or composition (a digital object), but excluding journal publications, made available electronically (or optically) for any device (handheld or desk-bound) that includes a screen_' ([Armstrong _et al._ 2002: 217](#arm2002)). A recent study ([Vassiliou and Rowley 2008](#vas2008)) suggests considering the use of a two-part e-book definition that captures the persistent characteristics of e-books in one part, and changing characteristics in the other:

> 1.  An e-book is a digital object with textual and/or other content, which arises as a result of integrating the familiar concept of a book with features that can be provided in an electronic environment.
> 2.  E-books, typically have in-use features such search and cross reference functions, hypertext links, bookmarks, annotations, highlights, multimedia objects and interactive tools" ([Vassiliou and Rowley 2008: 363](#vas2008)).

Literature studies reveal that the diversity of e-books, their different access options and constant changes in technological development, are the main obstacles for universally accepted definitions of the term _e-book_. There are however some similarities in the definition of e-books. Definitions used by scholars, librarians, and e-book providers usually refer to an e-book as an interactive or digital document, and/or reading device. Diverse contents and different file formats of e-books as well as their analogy with traditional books are other often mentioned characteristics of e-books.

It is obvious that e-books try to address the limitations of traditional print books. Contemporary e-books, unlike printed books, are dynamic, include personalisation options, and are available immediately in different formats and devices.

### Typology of e-books

The publishing industry currently offers a wide range of e-books. Previous studies of the typology of e-books by Hawkins ([2000](#haw2000)), Schneider ([2006-2010](#sch2006)) and Vassiliou and Rowley ([2008](#vas2008)) included e-books that require a dedicated e-book reader. E-books in plain text format, and e-books designed for reading on standard computers were mentioned in two of three classifications. Only Vassiliou and Rowley distinguished between free and chargeable e-books ([2008](#vas2008)). Authors of the aforementioned studies differ when it comes to whether print-on-demand books and multimedia books should be classified as e-books. Access possibilities to contents of e-books, their formats, price, and downloading options were the main factors that were taken into consideration when classifying different types of e-books. All three typologies were used for sampling in this case.

### Advantages and disadvantages of e-books

The choice between a traditional book and an e-book is influenced by many things. The most important factors are the differences between the two book formats, and users' attitudes towards both those formats. This paper goes on to discuss the advantages and disadvantages of the e-book compared with the traditional book. Opinions expressed by academics, researchers, writers from the library and information science discipline and e-book publishing industry are summarised.

The identified advantages of e-books are:

*   _Availability._ E-books can be accessed by different types of devices: computers, e-book readers, mobile phones ,etc. They can be accessed anywhere the Internet is available. Open access e-books can be used simultaneously by unlimited numbers of users. Providers of e-books therefore do not have to worry about the number of copies of e-book titles ([Berglund _et al._ 2004](#ber2004); [eBrary 2007a](#ebr2007a); [eBrary 2008](#ebr2008); [Hawkins 2000](#haw2000); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Economic advantages._ E-books offer savings on costs of paper, ink, binding, packing, and delivery ([Armstrong and Lonsdale 2003](#arm2003); [Hawkins 2000](#haw2000); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Environmental advantages._ Increasing use of e-books can reduce the consumption of paper and save trees ([Hawkins 2000](#haw2000); [Rao 2003](#rao2003)).
*   _Flexibility._ It is possible to change the visual appearance of e-books, their font size or style, etc. Such personalisation options enable the same text to be available to different audiences in their preferred format. Print books, in contrast, would require different print runs in, for example, standard and large print ([Armstrong and Lonsdale 2003](#arm2003); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Multimedia._ E-books can be enhanced with multimedia features: interactive images, sound, and/or video material. Interactive features such as commentary options and chatting are also available ([Armstrong and Lonsdale 2003](#arm2003); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Space saving._ E-books do not require physical space. Unlike print books, there is no limit, in terms of storage space, on the number of e-books held in a collection ([Armstrong and Lonsdale 2003](#arm2003); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Portability._ E-books can be forwarded from one computer to another or even to another device ([Berglund _et al._ 2004](#ber2004); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Search possibilities._ While traditional books offer only a table of contents and different kinds of supplementary indexes, most e-books offer a variety of computerised search options such as unlimited keywords, combination of search results, search history, search within a book or the whole collection ([Armstrong and Lonsdale 2003](#arm2003); [eBrary 2007a](#ebr2007a); [eBrary 2008](#ebr2008); [Hawkins 2000](#haw2000); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Security._ It is harder to lose, steal or damage e-books, especially if they are available on the Internet, than print books ([Armstrong and Lonsdale 2003](#arm2003); [Berglund _et al._ 2004](#ber2004)).
*   _System of navigation._ Hyperlinked references in footnotes, titles, places, or people mentioned in the text facilitate access to related sources of information in e-books and save time ([Armstrong and Lonsdale 2003](#arm2003); [Hawkins 2000](#haw2000); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Text processing._ Many e-books provide features like bookmarks, text highlighting and underlining. Cutting, copying and pasting, printing and saving of text for later use are also possible ([Armstrong and Lonsdale 2003](#arm2003); [eBrary 2007a](#ebr2007a); [eBrary 2008](#ebr2008); [Hawkins 2000](#haw2000); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Timeliness._ The publishing process for e-books is much shorter than for traditional books. ([Armstrong and Lonsdale 2003](#arm2003); [eBrary 2008](#ebr2008); [Hawkins 2000](#haw2000); [Vassiliou and Rowley 2008](#vas2008)). The electronic environment also enables immediate updates and corrections of information already published ([Armstrong and Lonsdale 2003](#arm2003); [Hawkins 2000](#haw2000)).

Such advantages make e-books a potentially powerful competitor to traditional books. Most of the advantages, but especially the search possibilities, system of navigation, text processing and information updating, could be more useful to the books which Eco ([1996](#eco1996)) defined as books for non-linear reading purpose or reference literature. Thompson ([2005](#tho2005)) has a similar point of view. When describing the potential of e-books, he particularly stresses the advantages of electronic reference literature available online ([2005](#tho2005)). Thompson states that reference publications are not usually read from cover to cover. Readers just search for answers to their questions.

Despite the advantages of the electronic environment, there is still high demand for printed books and the specific disadvantages are.

*   _Availability._ E-book titles are limited and there is an unequal distribution of titles across subjects. Limited access to e-book readers at local stores is also an issue ([Armstrong and Lonsdale 2003](#arm2003); [Berglund _et al._ 2004](#ber2004); [eBrary 2007a](#ebr2007a); [Hawkins 2000](#haw2000); [Rao 2003](#rao2003); [Vassiliou and Rowley 2008](#vas2008)).
*   _Computer skills._ Insufficient computer skills, lack of experience of e-book use and technophobia create barriers to e-book use ([Armstrong and Lonsdale 2003](#arm2003)).
*   _Equipment._ E-book reading devices are comparatively expensive, and readers are unused to reading long texts on a small screen ([Armstrong and Lonsdale 2003](#arm2003); [eBrary 2007a](#ebr2007a); [Elgan 2007](#elg2007); [Hawkins 2000](#haw2000); [Thompson 2005](#tho2005)).
*   _Format._ There are many incompatible e-book formats. Therefore, users are forced to get specialised software or a particular hardware ([Berglund _et al._ 2004](#ber2004); [Herther 2005](#her2005); [Rao 2003](#rao2003); [Thompson 2005](#tho2005); [Vassiliou and Rowley 2008](#vas2008)).
*   _Lack of information._ Users do not know about the existence of e-books or their collections on the Internet or at the nearest libraries ([Bennett and Landoni 2005](#ben2005)).
*   _Language._ The majority of the released e-books are available in English, only a small proportion is in German and other languages ([Thompson 2005](#tho2005); [Vassiliou and Rowley 2008](#vas2008)).
*   _Piracy._ The balance between the interests of publishers and users regarding protection of e-book content is still an internationally unresolved issue ([Herther 2005](#her2005)).
*   _Price._ Prices of e-books are almost the same as those of traditional books, despite the savings on costs of paper, ink, binding, packing, and delivery ([eBrary 2007a](#ebr2007a); [Elgan 2007](#elg2007); [Herther 2005](#her2005); [Rao 2003](#rao2003); [Thompson 2005](#tho2005)).
*   _Rights._ A lot of e-books have limitations regarding their printing, sharing and saving options due to copyright issues and publishing models ([Herther 2005](#her2005); [Rao 2003](#rao2003); [Thompson 2005](#tho2005)).
*   _Unpredictability._ A new kind of e-book reading device or format can be released at any time. New inventions can make current technological solutions obsolete. Provision of e-books can change at any time. Providers of e-books have rights to forbid access to some titles or even to remove the whole collection ([eBrary 2007a](#ebr2007a); [Rao 2003](#rao2003)).

These disadvantages are the main reason why a new e-book era has not begun in the publishing industry. At the same time, there is no convincing evidence that the revolution is not possible or has not already started. Quarterly statistics on the e-book trade in the USA show a constant increase in revenues of e-book sales since the third quarter of 2003 ([International Digital Publishing Forum 2010](#int2010)).

Although it is hard to forecast the future of e-books, Brindley has predicted that

> by the year 2020, 40% of UK research monographs will be available in electronic format only, while a further 50% will be produced in both print and digital. A mere 10% of new titles will be available in print alone by 2020\. ([Sanderson 2005](#san2005)).

However e-book adoption differs between countries. This paper will now focus on Latvian e-books, their current availability and users' attitude towards them.

## Methods of data collection

The aim of this research is to study the provision and usability of e-book collections that are available in Latvian language and consists of two empirical studies: the first analyses the content of existing e-book collections; the second, carried out as a usability study, clarifies the attitudes of the users towards e-books.

The research is based on content analysis, which, roughly defined, is a type of document study where the analysed content could be anything: words, phrases, pictures, ideas, etc. ([Beck and Manuel 2008](#bec2008)). Content analysis has been widely used by such disciplines as psychology, sociology, and politics ([Krippendorff 2004](#kri2004)). Recent research trends show that this method has been regularly applied to library and information science research. As Koufogiannakis _et al._ ([2004](#kou2004)) found out, content analysis is one of the five most preferred methods for research carried out in library science.

A sample collection of e-books was identified by using the search engine Google, its hosted service Google Books and browsing Web pages of eight Latvian libraries of national significance. Because of the huge amount of information, the author analysed only those collections of e-books that consisted of four or more e-books. In order to be included in the study, a collection had to be available on the Internet and host e-books written in Latvian. This research also covers e-book collections that consist of e-books with separate chapters in Latvian or with text both in Latvian and foreign languages. Digital documents that lacked the generally accepted structure of a book: text, organizational, narrative, thematic and cultural structures ([Sun 2007](#sun2007)) were excluded from the research. Data for the research were collected between February 27th and March 10th 2009.

Collections of Latvian e-books were evaluated by applying categories and indicators listed in Table 1 (see [Appendix 1](#app1) for more detailed categories and indicators list). All categories and indicators were based on the literature review of the concept of e-books, typology, and advantages and disadvantages of e-books.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Categories and indicators of content analysis**</caption>

<tbody>

<tr>

<th>Categories</th>

<th>Indicators</th>

</tr>

<tr>

<td>General information</td>

<td>publication date, number of pages, analogy with the traditional book, typology</td>

</tr>

<tr>

<td>Availability</td>

<td>price, format, amount of information available free of charge, existence of identical content in traditional format</td>

</tr>

<tr>

<td>Content provision</td>

<td>reading purpose, publication type, subject</td>

</tr>

<tr>

<td>Information retrieval possibilities</td>

<td>searching scope, search type, multimedia, navigation, personalisation, communication possibilities, text processing features</td>

</tr>

</tbody>

</table>

E-books available in two or more collections were included in the analysis only once. All defined criteria were applied only to the e-books accessible free of charge. Chargeable e-books were analysed based on those categories where the information for analysis was freely available.

The main aim of the usability research was to clarify the attitude of different user groups towards e-books as an innovation into the market. Verbal protocol as The main research method was the verbal protocol and a questionnaire was used as a secondary research method.

The data gathered from verbal protocols provided the main information on users' skills and acquirements regarding the adoption of e-books. The researcher's observations during the data gathering process from verbal protocols and the questionnaires completed by the participants increased the accuracy of measurements of human behaviour. Data were gathered between 13th and 16th March, 2009.

Research shows that valid data in usability research through the use of the verbal protocol can be obtained from a relatively small number of users. Nielsen suggests including three users in each group when three or more different groups of users are studied. Such a number is enough to cover the diversity of behaviour within the group ([Nielsen 2000](#nie2000)). On this basis, the author of the paper analysed three different user groups: an expert group with experience of e-book usage; and two different age groups without experience of e-book usage. Participants of these groups were 19-25 or 46-55 years old. Age of the expert group participants was not taken into account during the participant selection process. The main factor for the selection of the expert group participants was their previous experience. Each group consisted of three people.

The three groups follow the diffusion of innovation theory. According to it, the distribution of innovation includes these elements: the innovation itself, the channel of communication and the individuals with and without previous knowledge or use experience of a particular innovation ([Rogers 2003](#rog2003)).

Observation and usability research are focused on human behaviour and actions. There are many different methodologies for examining an issue ([Beck and Manuel 2008](#bec2008)). The author of the research used a verbal protocol or think aloud protocol (also known as protocol analysis or thinking aloud technique) as the main research method for users' skills analysis, acquirements, cognitive processes, preferences and decision making processes during e-book use. '_Verbal protocols are rich data sources containing individuals' spoken thoughts that are associated with working on a task_' ([Bourg 2010](#bou2010)). Verbal reports usually consist of short, unfinished sentences which describe actual actions and thoughts occurring during the task.

Before starting the verbal protocol session, each participant was informed about the method and the existence of two task sheets which contained identical information. The sheets with six tasks in printed and electronic format were available to the participants during the whole process. The maximum time limit for all six tasks was 45 minutes. Each participant fulfilled the tasks separately and all spoken thoughts and navigation actions on the computer screen were recorded by means of screen capture programs: _ORipa Screen Recorder-recorder (Windows XP)_ or _Debut Video Capture Software (Windows Vista)_. All e-books used in the verbal protocol sessions had open access and were designed for reading on standard computers. Only e-books were available to the participants for completing the first four tasks. To complete the fifth and the sixth tasks, the participants could use printed and/or electronic versions with the same contents. The first and the second tasks were related to navigation activities, while the remaining four were information retrieval tasks from reference, scientific and fiction publications (see [Appendix 2](#app2)).

The researcher used unstructured observation of participants during the data collection process. This type of observation means that there are no predetermined categories of behaviour ([Powell and Connaway 2004](#pow2004)). The researcher followed the participants unobtrusively by taking notes of their actions during the observation process.

A questionnaire was used as the secondary research method. This method has been defined as '_a set of carefully designed questions given in exactly the same form to a group of people in order to collect data about some topic(s) in which the researcher is interested_' ([Jupp 2006: 252](#jup2006)). Every participant completed two different questionnaires: one before and one after the data collection process from verbal protocols. Before obtaining data from verbal protocols, each participant was asked to complete a questionnaire with general personal information: gender, age, education, self-assessment of computer skills and existence of previous experience of e-book use (see [Appendix 3](#app3)). After the verbal protocol session each participant completed another questionnaire (see [Appendix 4](#app4)), the main purpose of which was to examine users' attitude towards e-books after their actual use. Both questionnaires consisted of fixed-response questions. The second questionnaire used Likert scale questions which were based on four of Rogers's characteristics:

*   Relative advantage, or '_the degree to which an innovation is perceived as being better than the idea it supersedes_' (2003: 229