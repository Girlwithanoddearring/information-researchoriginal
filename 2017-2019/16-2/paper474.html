<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="style.css">
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
    <meta name="dc.title" content="Information behaviour of Australian men experiencing stressful life events: the role of social networks and confidants">
    <meta name="dc.creator" content="Peta Wellstead">
    <meta name="dc.subject" content="Australian men behaviour studies">
    <meta name="dc.description" content="This paper discusses the findings of a qualitative study into the information behaviour of a group of Australian men who had experienced a stressful life event. It examined sources of information together with the gaps and barriers experienced in the information search. The information value of social networks and confidants was explored. Sense-making interviews were conducted with fifteen men who had experienced a period of significant life stress. A self-report sheet was used to collect additional data from the help-seeking men. A semi-structured interview was held with six professionals who offer information and help to members of the community. Interview transcripts were examined for themes within the help-seeking episode. A thematic analysis was also undertaken in the interviews with the professionals. Results indicate Australian men use a variety of information behaviour strategies, including avoidance, during stressful life events. A key finding is the important role of women in assisting men.s information behaviour both in terms of facilitating help-seeking, and assisting men to stay on task during the help-seeking episode. These findings indicate the need for greater understanding of the information behaviour of Australian men experiencing stressful life events. The findings have implications for Australian government health policy, particularly the development of support services for men.">
    <meta name="dc.subject.keywords" content="information behaviour; information seeking; social network theory, men">
    <meta name="robots" content="all">
    <meta name="dc.publisher" content="Professor T.D. Wilson">
    <meta name="dc.coverage.placename" content="global">
    <meta name="dc.type" content="text">
    <meta name="dc.identifier" scheme="ISSN" content="1368-1613">
    <meta name="dc.identifier" scheme="URI" content="http://InformationR.net/ir/16-2/paper474.html">
    <meta name="dc.relation.IsPartOf" content="http://InformationR.net/ir/16-2/infres162.html">
    <meta name="dc.format" content="text/html">
    <meta name="dc.language" content="en">
    <meta name="dc.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
    <meta name="dc.date.available" content="2010-06-15">
</head>
<body>
<h4 id="vol-16-no-2-june-2011">vol. 16 no. 2, June, 2011</h4>
<h1 id="information-behaviour-of-australian-men-experiencing-stressful-life-events-the-role-of-social-networks-and-confidants">Information behaviour of Australian men experiencing stressful life events: the role of social networks and confidants</h1>
<h4 id="peta-wellstead"><a href="#author">Peta Wellstead</a></h4>
<p>School of Information and Social Sciences Open Polytechnic - Kuratini Tuwhera, 3 Cleary Street, Waterloo, Private Bag 31914, Lower Hutt, New Zealand 5040</p>
<h4>Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This paper discusses the findings of a qualitative study into the information behaviour of a group of Australian men who had experienced a stressful life event. It examined sources of information and the gaps and barriers experienced in the information search. The information value of social networks and confidants was explored.<br>
<strong>Method.</strong> Sense-making interviews were conducted with fifteen men who had experienced a period of significant life stress. A self-report sheet was used to collect additional data from the help-seeking men. A semi-structured interview was held with six professionals who offer information and help to members of the community.<br>
<strong>Analysis.</strong> Interview transcripts were examined for themes within the help-seeking episode. A thematic analysis was also undertaken in the interviews with the professionals.<br>
<strong>Results.</strong> Results indicate the use of a variety of information behaviour strategies, including avoidance, during stressful life events. A key finding is the important role of women in assisting men's information behaviour both in terms of facilitating help-seeking, and assisting men to stay on task during help-seeking.<br>
<strong>Conclusions.</strong> These findings indicate the need for greater understanding of the information behaviour of men experiencing stressful life events. The findings have implications for health policy, particularly the development of support services for men.</p>
</blockquote>
<h2 id="introduction">Introduction</h2>
<p>Men die in Australia, on average, at 77.8 years, six years younger than the 83.9 years for women (Australian Bureau of Statistics 2010b). Compared with women, men in most age groups have higher mortality rates for stroke, diabetes, cancers, ischaemic heart disease, bronchitis, emphysema, injury, poisoning, accidents and drug dependence (<a href="#ver00">Verrinder and Denner 2000</a>). Studies both in Australia and overseas have shown that men in countries with a dominant Anglo culture are also more likely than women to have unhealthy lifestyles, drink and smoke excessively, eat a less healthy diet and engage in risk-taking and/or aggressive activities that affect their health outcomes (<a href="#con99">Connell 1999</a>; <a href="#gri96">Griffiths 1996</a>).</p>
<p>While the statistics for general health outcomes for Australian men are poor, statistics for male suicide, compared to women, are also of considerable concern. The Australian Bureau of Statistics shows that throughout the period 1998 to 2007 the male age-standardised suicide death rate was approximately four times higher than the corresponding female rate: 13.9 per 100,000 standard population compared with 4.0 per 100,000 for women (<a href="#aus10a">Australian Bureau of Statistics 2010a</a>). Notwithstanding these obvious poor physical and mental heath outcomes, research also shows that Australian men are reluctant to seek help during stressful life events and face particular barriers in accessing information and help at such times, and across the lifespan more generally (<a href="#con99">Connell 1999</a>; <a href="#aus09">Australia. Department... 2009</a>).</p>
<p>The role of relationship breakdown as a factor in the high rates of suicide for Australian men has been suggested by a number of studies (<a href="#bau98">Baume <em>et al.</em> 1998</a>; <a href="#can95">Cantor and Slater 1995</a>). Cantor and Slater's study found that:</p>
<blockquote>
<p>separated (compared with married) [Australian] males were six times more likely to suicide, and this was greater in younger age groups. Separated female suicide rates were not significantly elevated. [The research suggests that] males may be particularly vulnerable to suicide associated with interpersonal conflict in the separation. (<a href="#can95">Cantor and Slater 1995</a>: 91)</p>
</blockquote>
<p>The risk of suicide in men post separation is of particular concern when the issue is considered in light of the occurrence of significantly higher female initiated separation in Australia (<a href="#hea06">Headley 2006</a>).</p>
<p>There is also considerable community anxiety related to the issue of separated and divorced men who engage in familicide, that is, the murder of children and then subsequent suicide of the father in the context of a dispute or distress over custody and access to children after a relationship has broken down (see, for example, <a href="#har05">Harris-Johnson 2005</a>).</p>
<h2 id="research-method-and-design">Research method and design</h2>
<p>This study took a multi-disciplinary study approach to the examination of the help-seeking behaviour of Australian men. An extensive multidisciplinary literature review was conducted which examined the possible historical, sociological, and psychological antecedents to poor health and harming behaviours by Australian men. In particular the impact of social norms (<a href="#cia99">Cialdini and Trost 1999</a>; <a href="#hat89">Hatch 1989</a>), and construction of masculinity (<a href="#cou00">Courtenay 2000</a>), were explored as contributors to levels of voluntary help-seeking. Likewise, the role of gender role strain (<a href="#ple95">Pleck 1995</a>) and the development of personal and cultural masculinity scripts (<a href="#mah03">Mahalik <em>et al.</em> 2003</a>) in the help-seeking experience were also explored.</p>
<p>Help-seeking is a subset of information behaviour. Information needs, seeking and use are a response to the world in which the subject finds himself or herself: there must be a perceived need for information to which there is a consequent response, either to seek out new information, or to ignore that need. By seeking help, individuals are using need as the framework for their information exchange. Using information and in turn increasing knowledge is a transitional process from '<em>distressing ignorance to becoming informed</em>' (<a href="#buc88">Buckland 1988</a>: 115)</p>
<p>There is a range of existing theories related to human information behaviour. These include sense-making (<a href="#der98">Dervin 1998</a>; <a href="#der03b">Dervin and Foreman-Wernet 2003</a>), small world theory (<a href="#cha91">Chatman 1991</a>, <a href="#cha96">1996</a>) and large group theory (<a href="#sch03">Schneider and Weinberg 2003</a>; <a href="#wil03">Wilke 2003</a>). There is also a range of graphic models (for example, <a href="#fos05">Foster 2005</a>; <a href="#spi01">Spink and Cole 2001</a>; <a href="#wil99">Wilson 1999</a>) to demonstrate how these theories operate in the information seeking arena in <em>real time</em>. These theories and models inform understanding of men's information seeking for personal decision making and personal change. Case has undertaken an extensive review of information seeking models showing that although sex is the primary focus of a number of information seeking studies '<em>typically the focus is on women</em>' (<a href="#cas07">Case 2007</a>: 314), and the few extant studies of the information behaviour of men focus on subgroups such as young homosexuals undertaking the process of coming out (see <a href="#cas07">Case 2007</a>: 307) rather than men dealing with everyday situations. The scholarship contained in this study provides much needed material for the enhanced understanding of the information behaviour of men in everyday settings. This new understanding may be able to be adapted and adopted in a range of contexts.</p>
<p>The multidisciplinary literature review undertaken for this study provided background to the development of a narrative style <em>sense-making interview</em> which was undertaken as the core of the research. Sense-making focuses on how humans make and unmake, develop, maintain, resist, destroy, and change order, structure, culture, organization, relationships, and the self. The sense-making theoretic assumptions are implemented through a core methodological metaphor that pictures the person as moving through time-space, bridging gaps and moving on (<a href="#der03">Dervin 2003</a>: 332).</p>
<p>During the sense-making interview the men were asked to reflect on their help-seeking episode as a serious of incremental steps and to consider their information behaviour in terms of:</p>
<ol>
<li>the type of information and support used (or avoided) during the help-seeking episode;</li>
<li>the value of this information and support; and</li>
<li>the type of information products and tools they might have used if these had been more readily available.</li>
</ol>
<p>After the sense-making interview the men were provided with a self-report sheet which could be returned by mail at a later date. This technique was an attempt to gather data that was more reflective and it was considered that the men may have preferred to respond in private. The information related to confidant availability and social network strength. Spencer and Pahl use the work of American epidemiologists (<a href="#coh97">Cohen <em>et al</em> 1997</a>) to suggest that '<em>the relative risk from mortality among those with less diverse networks is comparable in magnitude to the relationship between smoking and mortality of all causes</em>' (<a href="#spe06">Spencer and Pahl 2006</a>: 28). With this in mind examination of confidant availability and social network strength were considered important aspects of the study.</p>
<h2 id="participants">Participants</h2>
<p>The men in the study (n=15) were recruited using the e-mail contact database of a community agency which offers information and support to men. The men who agreed to take part in the study responded voluntarily to the e-mail broadcast. Initially twenty-eight men expressed interest in the research. When provided with further information seventeen agreed to take part. Two of these men cancelled their interview on the day it was to take place, citing pressure of work. Attempts to reinstate these interviews were not successful. Demographic data collected from the fifteen help-seeking men revealed a wide range backgrounds and life experience (see Table 1).</p>
<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: Verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Demographics of help-seeking men**</caption>
<tbody>
<tr valign="top">
<td>Age</td>
<td>The age range of the men was 32-63</td>
</tr>
<tr valign="top">
<td>Educational level</td>
<td>Varied across a range from completing Year 10 (leaving school age 15) to post-graduate university qualifications;</td>
</tr>
<tr valign="top">
<td>Place of residence</td>
<td>Varied across a range from outer urban/semi-rural to inner city.</td>
</tr>
<tr valign="top">
<td>Employment status</td>
<td>Varied across a range from casual work, home duties, public service, professional, to owning a business</td>
</tr>
<tr valign="top">
<td>Number of children</td>
<td>Only one of the participants did not have children</td>
</tr>
<tr valign="top">
<td>Employment</td>
<td>No men were currently working in blue collar industries, although a number had done so in the past</td>
</tr>
<tr valign="top">
<td>Relationships 1</td>
<td>Only three of the fifteen participants identified having had only one partner during his adult life; two of these relationships were no longer intact (so the men were single again) and the third had separated and reunited</td>
</tr>
<tr valign="top">
<td>Relationships 2</td>
<td>Four of the men had been married or partnered three times or more. [Note: the term partner was used to identify significant relationship bonds; casual 'dating type' relationships were excluded].</td>
</tr>
</tbody>
</table>
<p>The interviews took between 45 and 90 minutes. The interview collected data on the use of formal information and support services, as well as use of informal networks such as family, friends and colleagues during the help-seeking episode. It also sought data on use of information <em>things</em> such as books, pamphlets and websites and awareness of public information campaigns such as television advertising and billboards which seek to alert the community to sources of information and help.</p>
<p>To put the help-seeking behaviour of this group of men into context with help-seeking behaviour of Australian men more generally, the research also canvassed the views of a group of professionals (n=6), public and private, who offer information and help to members of the community facing stressful life events. These professionals were selected to take part because of the profile of the services they were offering, or the focus of their work role in supporting men. Approaches were made to eleven professionals but six of these declined due to pressure of work, a belief that they had nothing to offer to the project, and an unwillingness to take part in an interview process because they didn't feel comfortable in that setting. It was more difficult to recruit private practitioners than those working in community or government agencies.</p>
<p>These participants were recruited by word-of-mouth and through peer networks. The work role of the professionals who took part in the study was varied (see Table 2). Five male professionals were interviewed. Three female practitioners were approached to take part but they declined. The manager of the local government service was a woman.</p>
<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: Verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Work role of professionals**</caption>
<tbody>
<tr>
<th>Work role</th>
<th>No.</th>
</tr>
<tr>
<td>Private practice</td>
<td align="center">2</td>
</tr>
<tr>
<td>Private mentoring and support service</td>
<td align="center">2</td>
</tr>
<tr>
<td>Community-based (not for profit) agencies</td>
<td align="center">2</td>
</tr>
<tr>
<td>Local government agencies</td>
<td align="center">1</td>
</tr>
<tr>
<td>State government agencies</td>
<td align="center">1</td>
</tr>
<tr>
<td colspan="2">Note: The number of work roles is greater than the study participants (n=6) as the two private psychologists combined this role with work in a government agency or work in a community based (not-for-profit) agency.</td>
</tr>
</tbody>
</table>
<p>Data were collected by engaging the professionals in a semi-structured interview in which they were asked to consider:</p>
<ol>
<li>what services were currently being offered to men by them or their agency;</li>
<li>how they market these services; and</li>
<li>if resources (human and financial) were not restrained what type of service would they develop to assist men and how would they market it?</li>
</ol>
<p>The interviews also gave the professionals an opportunity to reflect on their information provision and support to men, and to discuss matters of general concern about engaging with men and supporting them in times of stress. The interviewers with the professionals took approximately 60 minutes.</p>
<h2 id="results">Results</h2>
<h3 id="phase-one-the-interview">Phase One: The interview.</h3>
<p>The first question in the interview was: <em>Can you give me an example of a time when you needed information to help you during a significant period of life stress?</em> The men indicated that they had previously needed help for to navigate a variety of life situations (see Table 3).</p>
<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: Verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Issues for which the men needed help**</caption>
<tbody>
<tr>
<td width="112">Participant 1</td>
<td width="445">Relationship breakdown, custody issues</td>
</tr>
<tr>
<td width="112">Participant 2</td>
<td width="445">Relationship breakdown, custody issues</td>
</tr>
<tr>
<td width="112">Participant 3</td>
<td width="445">Relationship breakdown</td>
</tr>
<tr>
<td width="112">Participant 4</td>
<td width="445">Relationship breakdown</td>
</tr>
<tr>
<td width="112">Participant 5</td>
<td width="445">Relationship breakdown, custody issues</td>
</tr>
<tr>
<td width="112">Participant 6</td>
<td width="445">Relationship breakdown, custody issues</td>
</tr>
<tr>
<td width="112">Participant 7</td>
<td width="445">Wife's alcohol addiction</td>
</tr>
<tr>
<td width="112">Participant 8</td>
<td width="445">Custody issues</td>
</tr>
<tr>
<td width="112">Participant 9</td>
<td width="445">Alcohol addiction</td>
</tr>
<tr>
<td width="112">Participant 10</td>
<td width="445">Death of young wife, support with care of 2-year old son</td>
</tr>
<tr>
<td width="112">Participant 11</td>
<td width="445">Relationship issues</td>
</tr>
<tr>
<td width="112">Participant 12</td>
<td width="445">Tried to kill himself twice</td>
</tr>
<tr>
<td width="112">Participant 13</td>
<td width="445">Life threatening illness</td>
</tr>
<tr>
<td width="112">Participant 14</td>
<td width="445">Relocation to Australia to join wife's family and enhance her career</td>
</tr>
<tr>
<td width="112">Participant 15</td>
<td width="445">Relationship breakdown</td>
</tr>
</tbody>
</table>
<p>The second and third questions in the interview asked: <em>What was the gap you were trying to fill?</em> <em>How did you think the information would help you?</em> Often, the responses were inter-changeable between these questions. Responses included:</p>
<blockquote>
<p>I didn't have a clue;<br>
I felt like a child crawling in the dark;<br>
I was deeply uncomfortable;<br>
I wanted more connection</p>
</blockquote>
<p>and, significantly:</p>
<blockquote>
<p>I needed to know I was normal.</p>
</blockquote>
<p>This sense of normalising the help-seeking experience was spoken of by all the men in the study in various ways.</p>
<p>Other men indicated that their health was the impetus for help-seeking. Responses with this theme included:</p>
<blockquote>
<p>sick of being sick;<br>
suicidal thoughts and depression;<br>
my wife knew I was ill.</p>
</blockquote>
<p>In response to the further question: <em>Was it difficult to think of places and people who might be able to help you?</em> only three of the fifteen men indicated that the task was relatively easy. Two of these seemed to be men with a clear articulation of their needs and with a strong support system, the third had a physical illness and his doctor was the obvious place for help, although he did admit to going to the doctor only after the protracted intervention by his wife.</p>
<p>As part of the interview men were asked to indicate their first major sources of information and help, and then secondary and subsequent sources in response to a question: <em>What did you do next?</em></p>
<p>The primary source of help, or conduits to other help such as doctors and counsellors, were family, friends and colleagues including their spouse (n=11).</p>
<p>Apart from the primacy of family, friends and colleagues in the help-seeking episode other early sources of help included <em>information things</em> such as pamphlets or leaflets, phone books, self-help books and mass media.</p>
<p>Secondary sources of help were often used concurrently with primary sources. For example, men who were talking to friends and family or reading books as a primary source of help did not stop doing those things when they sought help from a secondary source such as counselling.</p>
<h3 id="phase-two-self-completion-questionnaire">Phase Two: Self-completion questionnaire.</h3>
<p>The self completion response sheet covered two broad themes: confidant availability and social network strength.</p>
<p>The concepts of confidant availability and social network strength were overlapping and interdependent; the men often included a person named as an available confidant as a person within their social network. This confidant may also have been instrumental in many cases in facilitating a social network and often providing direction to <em>information things</em> such as books or Websites.</p>
<h4 id="confidant-availability">Confidant availability</h4>
<p>These questions asked the participants to identify people who were available to help and support them in times of need and to reportthose to whom they are important in return.</p>
<p>Nine men identified women as their primary confidants and people of emotional importance. In response to the question: <em>Is there a special person you feel close to?</em> all but one man answered <em>Yes</em>, with the other answering <em>Somewhat</em>. Eleven of these close people were women. Of those men who did not answer <em>partner or spouse</em> (not necessarily live-in) to the question, four identified other females as the person special to them namely, sister, friend or ex-wife.</p>
<p>In response to: <em>Are you special to someone?</em>, all but one man answered <em>Yes</em> and many men identified more than one person, with women and children being predominant.</p>
<p>As far as those who have been available to offer support in the last twelve months the primary source of support was women, with several identifying more than one woman (e.g. wife and mother-in-law, partner and sister etc). Other helpers in the last twelve months had been mates, former work colleague, brother and father.</p>
<h4 id="social-network-strength">Social network strength</h4>
<p>In response to the questions enquiring about the strength of their social network the men were asked to identify the number of close relationships they have through a range 1-2; 3-5; 6-10; or more than 10. The most common response was: 3-5 (n=10).</p>

</body>
</html>
