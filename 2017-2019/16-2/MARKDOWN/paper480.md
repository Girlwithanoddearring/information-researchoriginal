#### vol. 16 no. 2, June, 2011

#### Measuring information behaviour performance inside a company: a case study

#### [Yujong Hwang](#author)  
School of Accountancy & MIS, DePaul University in Chicago & College of International Studies, Kyung Hee University, Seoul, Korea

#### Abstract

> **Introduction**. Measuring information behaviour performance to provide practical guidance for knowledge workers is an important issue for the success of a company. Drawing upon the literature from psychology, marketing, management and information systems, this paper develops a practical model of information behaviour that provides fundamental determinants of knowledge workers' performance.  
> **Method**. This paper carried out a single company case study employing an online survey with a return of 352 employees from a large insurance company.  
> **Analysis**. Specific interpretations of the results at each level are provided with the quantitative analysis using average scores and gaps using Microsoft Excel statistical analysis functions.  
> **Results**. Results indicate that the capability to use information and information technology is moderately lower than the overall motivation to use information effectively in this company.  
> **Conclusions**. Employees are motivated to use information to make better decisions and share information to increase job productivity. A high analytic style of employees seems to be a clear advantage for the company. However, the capability to organize and maintain valuable information was found to be lower than the other scores and should be enhanced to improve overall job performance.

## Introduction

Measuring information behaviour performance to provide practical guidance for knowledge workers is an important issue for the success of a company. Effective use of information is a critical contributor to an individual knowledge worker's performance. A knowledge worker is a professional who uses information as the main input to their job and whose major products are distillations of that information. Knowledge workers now make up the largest portion and there is no dispute about the exponential growth of knowledge workers and their critical impact on business ([Acsente 2010](#acsente2010)). As industries become increasingly information intensive, and knowledge is perceived as the new basis for competing in the electronic-economy, information use and competence by the knowledge worker is regarded as the core asset of the organization ([Marchand _et al._ 2002; 2001; Grant 1996](#marchand1996)).

It has also been predicted that knowledge worker's information use and the corresponding performance impacts are bringing fundamental change in the basic structure of the global economic system ([Drucker 1988](#drucker1988); [1999](#drucker1999); [Davenport _et al._ 1998](#davenport1998); [H?glund 1998](#hoglund1998)). According to Jones ([2007](#jones07)) knowledge worker's information management should translate to better employee productivity, coordination of team work in the near-term, and be a key to leverage business success in the long term. However, this rise in the number of knowledge workers and importance of their performance has brought about special challenges for today's managers and executives; particularly the effective use of information by their employees. According to Drucker,

> Fifty years from now the leadership of the world economy will have moved to the countries and to the industries that have most systematically and most successfully raised knowledge worker productivity. ([Drucker 1999](#drucker1999): 158)

Towards that end, measuring effective use of information by an individual worker and understanding its effect on performance is a critical requirement.

Although information technology (defined as all forms of technology used to create, store, exchange, and use information) has been developed to support various activities related to information use, human knowledge workers are the ultimate agents who put information to use. As the knowledge economy grows, the way in which people share information and use intellectual assets of the organization is critical. For example, company success depends on the ability of employees to use information to act on customer feedback and to be more responsive in service delivery.

In articulating the _Knowledge base of the firm_, Grant ([1996](#grant1996)) argues that knowledge is viewed as residing within the individual and the primary role of the organization is knowledge application of individual members rather than knowledge creation at the organizational level. Information must be transformed by human cognition to be helpful for business performance ([Orna 1996](#orna1996)). It is the knowledge worker who brings in new ideas or information from the outside that improve performance ([Davenport _et al._ 1998)](#davenport1998). Interestingly, while the evidence mounts that we have entered an age where we must improve the effectiveness of information use of employees, much of the scholarly energy in management and information systems has been directed at the deployment of technology rather than improving the information use effectiveness of knowledge workers. Drucker ([1999](#drucker1999): 100) observes that '_top manager's frustration with IT has triggered a new Information Revolution_', focusing on effective use of information. He emphasizes that '_what is needed is to define information and new concepts of information use rather than more data, more technology, or more speed_

Drucker's arguments are encapsulated ;in his suggestion that the information systems function has tended to focus more on technology use than on the information use, emphasizing the technology in, and ignoring the information ([Drucker 1999](#drucker1999)). While Drucker's critique of the focus of information systems may be a bit severe, compared to the excellent research effort directed on identifying salient characteristics of information technology adoption and acceptance (e.g., [Davis 1989](#david1989); [Agarwal and Venkatesh 2002](#agarwal2002)) and their relationships to information system success factors (e.g., [DeLone and McLean 1992](#delone1992); [Seddon 1997](#seddon1997)), it is true that there has been comparatively little theoretical explanation or empirical study regarding the effective use of information at the individual level. This concept may be a fundamental driver of knowledge worker performance.

One recent exception to this information system research trend is the survey by Marchand, Kettinger and Rollins ([2000](#marchand2000), [2001](#marchand2001), [2002](#marchand2002)) of 1,009 senior managers in twenty-two countries and twenty-five industries. This survey measured the _information orientation_ of an organization by determining the degree to which a company possesses competence and synergy across three vital information capabilities: information technology practice, information management practice, and information behaviour and value. In this empirical investigation, they found that these three latent constructs are critical in determining the performance of organizations and that the information technology capability of an organization can only be fully beneficial when information behaviour and value and information management practice are also maximized. The findings of Marchand _et al._, ([2000](#marchand2000)) indicate that without a solid understanding of effective use of information and its relationship to performance, the organization could not readily identify productive information system implementation that maximizes information technology investment. However, while the information orientation model provides valuable insights in understanding information practices and their relationship to performance at the organization level, it does not provide the theoretical framework for individual knowledge workers.

At the individual knowledge worker level, Davenport _et al._ ([1998](#davenport1998)) argue that personal information effectiveness has become the limiting factor in business information management for most organizations. The influence of the individual worker's knowledge on job performance has been emphasized as the core resource of the company ([Grant 1996](#grant1996); [Davenport _et al._ 1998](#davenport1998); [Drucker 1988](#drucker1998)). The present study reconstructs the information orientation model to be applicable to the individual knowledge worker. The conceptual model in this study proposes that information motivation (the degree to which a person is motivated to use information effectively) and information capability (the degree to which a person manages his or her information well over the information lifecycle: sensing, collecting, organizing, processing, and maintaining), are fundamental determinants of an individual knowledge worker's performance.

While the present study conceptualizes information motivation and capability as fundamental determinants of an individual worker's performance based on the information orientation model, it also posits that _information technology capability_ (self-efficacy) and _cognitive style_ (analytical or intuitive) are important antecedents to job performance. Self-efficacy refers to an individuals' belief that they have the ability and resources to succeed at a specific task. This concept is based on the social cognitive theory ([Bandura 1986](#bandura1986)), which is a robust and well-accepted model of individual performance and the basis for many different types of performance domains. Self-efficacy beliefs contribute to effective performance by increasing motivational effort ([Bandura 1977](#bandura1977)) as well as information processing capability ([Brown and Ganesan 2001](#brown2001)). Cognitive style is also widely recognized as an important determinant of individual information behaviour. Witkin _et al._ ([1977](#witkin1977)) found cognitive style differences in the way people perceive, think, solve problems, or learn. Messick ([1984](#messick1984)) offers a clear distinction between capability and cognitive style. Capability refers to mastery of content and an individual's capacity to perform, whereas cognitive style is more concerned with what the individual will do in a given class of situation with the manner, form, and nature of performance. Sternberg ([1977](#sternberg1977)) also offers a model of intelligence which focuses on how intelligence is directed (cognitive style) rather than on how much intelligence a person has (capability), suggesting that people select situations that allow them to use their preferred cognitive styles and avoid those situations that repeatedly pose alternative demands.

The present research has two objectives: (1) develop a theoretical model of information behaviour that links information motivation and capability to the knowledge worker's job performance, and (2) provide the case evidence based on the survey.

This research offers theoretical, methodological, and practical contributions. First, the theoretical contribution of this research is to advance a model that describes the effects of information motivation and capability of the knowledge worker on job performance. This issue is important and relevant to information system literature because information use for performance is the ultimate goal of successful information technology implementation and information management. Secondly, the practical contribution of this research is to provide a tool that companies can use in work-place settings to diagnose or evaluate the effective use of information by an individual worker. The present study will demonstrate how to use the tool in a typical business organization.

This section has described the introduction of the study and general nature of the problem. The rest of the paper is organized as follows. The next section presents a review of previous research on the information orientation model, providing theoretical background and major findings relevant to the present research. This is followed by a presentation the research method with case study and data analysis. The final section discusses the summary of empirical findings and limitations and concludes by suggesting practical implementations and future research directions.

## Literature review

Marchand, Kettinger and Rollins ([2000](#marchand2000), [2001](#marchand2001), [2002](#marchand2002)) proposed the information orientation model to examine how the interaction of people, information and technology establishes an orientation towards the use of information and business performance at the organizational level. Marchand _et al._ ([2000](#marchand2000), [2002](#marchand2002)) used the perception of the senior managers to determine the degree to which a company possesses competence and synergy across three vital constructs: information behaviour and value, information management practice and information technology practice (see Figure 1). Using a powerful set of statistical techniques, they defined the key activities and practices associated with information orientation (Table 1 summarizes the detailed dimensions in the information orientation model). They demonstrated that, in the minds of senior managers, information orientation is a predictor of business performance and that being good at information behaviour and value, information management practice, or information technology practices alone does not guarantee increases in business performance. Rather, it is the comprehensive concept of information orientation using the three constructs together (and each of the constructs is necessary) that is needed to improve business performance. They found the empirical evidence to support the information orientation model and showed three separate dimensions of the information orientation construct.

The basic premise of the information orientation model is that a technology-centered viewpoint has not encouraged a more people-centered information management activity aimed at improving behaviours and values for more effective information use. Marchand _et al._ ([2000](#marchand2000); [2002](#marchand2002)) demonstrated in their empirical study that companies should do more than excel at investing in and deploying information technology and combine those practices with excellence in information management and with getting their people to embrace the right behaviour and values for working with information.

<div align="center">![figure1: Information orientation model](p480fig1.jpg)</div>

<div align="center">  
**Figure 1: Information orientation model ([Marchand, Kettinger, & Rollins 2000](#marchand2000))**</div>

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Information orientation dimensions** ([Marchand _et al._ 2000](#marchand2000))</caption>

<tbody>

<tr>

<th>Information behaviour and value</th>

<th>Information management practice</th>

<th>Information technology practice</th>

</tr>

<tr>

<td valign="top">To instill and promote behaviour and values in its people for effective use of information</td>

<td valign="top">To manage information effectively over its life cycle</td>

<td valign="top">To effectively manage appropriate information technology applications and infrastructure</td>

</tr>

<tr>

<td style="text-align: center; font-style: italic">Proactiveness</td>

<td style="text-align: center; font-style: italic">Sensing</td>

<td style="text-align: center; font-style: italic">Information technology for operations</td>

</tr>

<tr>

<td valign="top">To actively seek out and respond to changes in their competitive environment and think about how to use this information to enhance and create new products and services.</td>

<td valign="top">To detect and identify economic, social, and political changes, competitor's innovations that might impact the business, market shifts and customer demands for new products.</td>

<td valign="top">To ensure that lower-skilled workers perform their responsibilities consequently with the software, hardware, telecommunication networks, and technical expertise.</td>

</tr>

<tr>

<td style="text-align: center; font-style: italic">Sharing</td>

<td style="text-align: center; font-style: italic">Collecting</td>

<td style="text-align: center; font-style: italic">Information technology for business processes</td>

</tr>

<tr>

<td valign="top">To freely exchange sensitive and nonsensitive information between individuals in teams, across functional boundaries, and across organizational boundaries.</td>

<td valign="top">To provide systematic process of gathering relevant information by profiling information needs of employees, developing filter mechanisms to prevent information overload.</td>

<td valign="top">To focus on development of software, hardware, networks, and technical expertise to facilitate the management of business processes.</td>

</tr>

<tr>

<td style="text-align: center; font-style: italic">Transparency</td>

<td style="text-align: center; font-style: italic">Organizing</td>

<td style="text-align: center; font-style: italic">Information technology for innovation</td>

</tr>

<tr>

<td valign="top">To trust each other enough to talk about failures and mistakes in an open and constructive manner and without fear of unfair repercussions.</td>

<td valign="top">To index, classify and link information and databases to provide access within and across business units and functions.</td>

<td valign="top">To facilitate people's creativity by enabling the exploration, development, and sharing of new idea with software, hardware and technical expertise.</td>

</tr>

<tr>

<td style="text-align: center; font-style: italic">Formality</td>

<td style="text-align: center; font-style: italic">Processing</td>

<td style="text-align: center; font-style: italic">Information technology for management</td>

</tr>

<tr>

<td valign="top">To use and trust formal sources of information such as financial statements and formal performance reports.</td>

<td valign="top">To use and trust formal sources of information such as financial statements and formal performance reports.</td>

<td valign="top">To facilitate executive decision making with software, hardware, networks, and technical expertise.</td>

</tr>

<tr>

<td style="text-align: center; font-style: italic">Control</td>

<td style="text-align: center; font-style: italic">Maintaining</td>

<td style="text-align: center; font-style: italic"> </td>

</tr>

<tr>

<td valign="top">To disclose information about business performance to all employees to influence and direct individuals and company</td>

<td valign="top">To reuse existing information to avoid collecting the same information again, updating information databases so that they remain current</td>

<td align="center"> </td>

</tr>

<tr>

<td style="text-align: center; font-style: italic">Integrity</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td valign="top">To prevent from manipulating information for personal gains such as knowingly passing on inaccurate information, distributing information to justify decisions after the fact, or keeping information to oneself</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

The information orientation model provides the theoretical foundation for linking information use and job performance. While the research focused on senior manager's perception of information use in his or her organization, the direct link between information, as well as information technology behaviour, and job performance provides meaningful insights into individual information behaviour in the organization. While the information orientation model provides an important framework for understanding information use at the organizational level and its relationship to organizational performance, the present study focuses on an individual's information use and its relationship to performance. Specifically, information motivation and information capability aspects of information use (information behaviour/value and information management practice dimensions in information orientation model) and their relationships to job performance were investigated. Also, this study includes information technology self-efficacy (capability) as an information technology practice dimension as well as cognitive style, such as analytic style, as a motivational aspects of information behaviour.

## Research method and analysis.

### Case study

This section presents key findings of a case study conducted to understand how effectively employees use information at the CAROL Company (fictitious name). Data were collected by an online survey. The data relate to 352 employees from 11 Divisions out of 11,000 total employees. The average age of survey participants in this case study was 38 years, and the average years of experience in the current job was 3.39 (see [Appendix 1](#app1) for detailed sample demographics of 11 divisions). More female employees (83%) participated in the study because more than 80% of the company workers were female. Most of the participants had fewer than 10 years of job experience. Survey participants had various knowledge-based job positions including senior manager, financial analyst, information technology manager, credit reviewer and marketing researcher. To increase possible replication of the study, we surveyed employees with varied tasks, sizes, and histories. In essence, this report provides a snapshot of a new way of looking at effective information use in the company. Overall strengths and weaknesses of the information behaviour of the employees were examined.

The report contains detailed data analysis across divisions, experiences, and sexes. Specific interpretations of the results at each level are provided with the quantitative analysis using average scores and gaps to help understand this phenomenon more readily. The author used the services of the Marketing Research Department to administer the project. The survey Web site was developed by author and was revised three times based on feedback from managers at the CAROL Company. The final version of online survey was confirmed by the division managers. The employees who voluntarily participated in the online survey showed considerable interest and voiced the potential usefulness of the ideas in this project in comments to the researchers. For example, one of the participants wrote in the memo section of the survey: _'Great survey. Made me take a closer look at how I perform my job, areas to improve. Thank you for this opportunity'_.

### Data analysis

We show the overall information utilization and information technology capability scores in Figure 2\. We transformed the 11-point scale of the responses into percentage scores. Each score in the table is calculated by the average score of the responses in the survey. The self-reported overall job performance score reported by the employees at the Company who participated in this survey was 83%, which can be considered relatively high. The _information motivation_ and _information capability_ scores were 81% and 73%, which shows that overall information use motivation was higher than information use capability at the CAROL Company. The gap appears to indicate the particular need for _information capability_ enhancement training. _information capability_ (73%) was relatively lower than perceived job performance (83%), which demonstrates that employees felt that they performed well and provided high quality service, but their actual information use capability in their job was not as high it should be. _Information technology capability_ was relatively lower (73%) than _information motivation_ (81%). _Analytic style_ was relatively higher (80%) than _information capability_ or information technology capability.

<div align="center">![figure2](p480fig2.jpg)</div>

<div align="center">Figure 2: Overall information utilization, information technology capability, and performance of CAROL Company</div>

For more detailed interpretation of the survey results, we also examined the sub-dimensions of _information motivation_ as summarized in Figure 3\. The information proactiveness and information sharing scores of the employees at the Company were reasonably high (both 84%), whereas information transparency and information formality scores were relatively lower (77% and 78% each) than the other dimensions. Based on the results, employees had a high motivation to proactively use and share information. However, the other aspects of _information motivation_, such as transparently using negative information and voluntarily using formal sources of good information, were relatively lower than proactiveness and sharing. An individual's information transparency can be enhanced by a more open culture. Formality can be improved by increasing the quality of the institutional information systems so employees place a higher level of reliance on institutional information systems as opposed to using informal sources. Thus, these aspects should be enhanced at the overall company level to help achieve higher job performance.

<div align="center">![figure3: Detailed information motivation in the CAROL Company](p480fig3.jpg)</div>

<div align="center">Figure 3:. Detailed information motivation in the CAROL Company</div>

_Information capability_ is another aspect of personal information effectiveness and depicted in Figure 5\. Sensing information was moderately high at 79%, while organizing and maintaining information were relatively low at 71% and 72%. The relatively higher capability for sensing information is a positive sign for the Company because this perspective is the initial point of the information life-cycle leading to higher overall _information capability_. The company should continue to build on its capability of employees to sense information. Both collecting and processing information were measured reasonably high at 76%. The overall high analytic style (80%) in Figure 4 seems to influence information processing capability (76%) resulting in relatively higher scores than the other two dimensions (organizing and maintaining). It should be noted that since all of the _information capability_ scores are in the 70s, the respondants are all candidates for the company's efforts to improve effective information use. No dimension can be regarded lightly because the overall Information Capability is determined by all of these dimensions.

<div align="center">![figure4: Detailed information capability in the CAROL Company](p480fig4.jpg)</div>

<div align="center">Figure 4: Detailed information capability in the CAROL Company</div>

We conducted further analysis to check the detailed scores across divisions in the company in Table 2\. Based on the online survey results, we broke the work groups into eleven divisions. There were noticeable differences among divisions shaded darkly in the table. For example, the largest job performance score gap among divisions was 23% (98% for Blue Card Vs. 75% for CCD), and the largest gaps of _information motivation_ and _information capability_ were 16% (93% for Blue Card Vs. 77% for Quality Control) and 26% (86% for Benefit Files Vs. 60% for Quality Control). The largest information technology capability score gap across divisions was 34% (89% for Benefit Files Vs. 55% for Private Business), and the largest analytic style score gap across divisions was 23% (96% for Benefit Files Vs. 73% for Quality Control). The overall comparison shows that high performing divisions (i.e., Benefit Files and Blue Card) tend to show efficient use of information as examined through high _information motivation_ and _information capability_ scores. It should be noted that the number of respondents varied across divisions, which could affect results. In addition, it must be remembered that this study is self reported data. It should be noted that while information-technology-related knowledge tests or direct observation of individual's behaviour could be an alternative way to measure information use behaviour, this would be difficult to implement. Thus, we depended on the perceptual data based on the generalized survey-based methodology.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Overall comparison across divisions**</caption>

<tbody>

<tr>

<td> </td>

<th>Performance</th>

<th>Information motivation</th>

<th>Information capability</th>

<th>Information technology capability</th>

<th>Analytic style</th>

</tr>

<tr>

<td>1\. PSC</td>

<td align="center">82%</td>

<td align="center">81%</td>

<td align="center">71%</td>

<td align="center">71%</td>

<td align="center">80%</td>

</tr>

<tr>

<td>2\. CHC</td>

<td align="center">88%</td>

<td align="center">>83%</td>

<td align="center">77%</td>

<td align="center">73%</td>

<td align="center">89%</td>

</tr>

<tr>

<td>3\. Benefit files</td>

<td align="center">94%</td>

<td align="center">90%</td>

<td align="center">86%</td>

<td align="center">89%</td>

<td align="center">96%</td>

</tr>

<tr>

<td>4\. Blue card</td>

<td align="center">98%</td>

<td align="center">93%</td>

<td align="center">80%</td>

<td align="center">74%</td>

<td align="center">95%</td>

</tr>

<tr>

<td>5\. CBA</td>

<td align="center">84%</td>

<td align="center">84%</td>

<td align="center">75%</td>

<td align="center">71%</td>

<td align="center">93%</td>

</tr>

<tr>

<td>6\. CCD</td>

<td align="center">75%</td>

<td align="center">79%</td>

<td align="center">66%</td>

<td align="center">78%</td>

<td align="center">74%</td>

</tr>

<tr>

<td>7\. Economy research</td>

<td align="center">77%</td>

<td align="center">79%</td>

<td align="center">71%</td>

<td align="center">78%</td>

<td align="center">87%</td>

</tr>

<tr>

<td>8\. Major group</td>

<td align="center">87%></td>

<td align="center">85%></td>

<td align="center">77%</td>

<td align="center">73%</td>

<td align="center">82%</td>

</tr>

<tr>

<td>9\. Private business</td>

<td align="center">92%</td>

<td align="center">89%</td>

<td align="center">85%</td>

<td align="center">55%</td>

<td align="center">77%</td>

</tr>

<tr>

<td>10\. Quality control</td>

<td align="center">77%</td>

<td align="center">77%</td>

<td align="center">60%</td>

<td align="center">63%</td>

<td align="center">73%</td>

</tr>

<tr>

<td>11\. State</td>

<td align="center">87%</td>

<td align="center">83%</td>

<td align="center">74%</td>

<td align="center">85%</td>

<td align="center">74%</td>

</tr>

<tr>

<td>Average</td>

<td align="center">83%</td>

<td align="center">81%</td>

<td align="center">73%</td>

<td align="center">73%</td>

<td align="center">80%</td>

</tr>

</tbody>

</table>

Table 3 showed the detailed _information motivation_ scores across divisions. The largest proactiveness score gap was 13% (92% for Blue Card Vs. 79% for Quality Control), and the largest sharing score gap was 29% (98% for Blue Card Vs. 69% for Economy Research). The largest transparency score gap was 24% (94% for Benefit Files Vs. 70% for Quality Control), and the largest formality score gap was 20% (93% for Economy Research Vs. 73% for Quality Control). Blue Card Division was continuously rated to have high motivation to use information effectively. Interestingly, the Economy Research Division was high in formal information motivation (93%) but much lower in sharing that information (69%). This might imply that employees in the Division are highly motivated to use formal sources of information, such as financial statements or feedback information but reluctant to share specific information with co-workers in or outside the division.

<table width="67%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Detailed _information motivation_ cComparison across divisions**</caption>

<tbody>

<tr>

<td> </td>

<th>Proactiveness</th>

<th>Sharing</th>

<th>Transpanrency</th>

<th>Formality</th>

</tr>

<tr>

<td>1\. PSC</td>

<td align="center">84%</td>

<td align="center">84%</td>

<td align="center">76%</td>

<td align="center">79%</td>

</tr>

<tr>

<td>2\. CHC</td>

<td align="center">82%</td>

<td align="center">86%</td>

<td align="center">82%</td>

<td align="center">81%</td>

</tr>

<tr>

<td>3\. Benefit Files</td>

<td align="center">90%</td>

<td align="center">94%</td>

<td align="center">94%</td>

<td align="center">81%</td>

</tr>

<tr>

<td>4\. Blue Card</td>

<td align="center">92%</td>

<td align="center">98%</td>

<td align="center">87%</td>

<td align="center">93%</td>

</tr>

<tr>

<td>5\. CBA</td>

<td align="center">83%</td>

<td align="center">91%</td>

<td align="center">79%</td>

<td align="center">82%</td>

</tr>

<tr>

<td>6\. CCD</td>

<td align="center">82%</td>

<td align="center">80%</td>

<td align="center">76%</td>

<td align="center">77%</td>

</tr>

<tr>

<td>7\. Economy Research</td>

<td align="center">82%</td>

<td align="center">69%</td>

<td align="center">74%</td>

<td align="center">93%</td>

</tr>

<tr>

<td>8\. Major Group</td>

<td align="center">88%</td>

<td align="center">87%</td>

<td align="center">82%</td>

<td align="center">83%</td>

</tr>

<tr>

<td>9\. Private Business</td>

<td align="center">88%</td>

<td align="center">95%</td>

<td align="center">89%</td>

<td align="center">84%</td>

</tr>

<tr>

<td>10\. Quality Control</td>

<td align="center">79%</td>

<td align="center">84%</td>

<td align="center">70%</td>

<td align="center">73%</td>

</tr>

<tr>

<td>11\. State</td>

<td align="center">88%</td>

<td align="center">87%</td>

<td align="center">81%</td>

<td align="center">77%</td>

</tr>

<tr>

<td>Average</td>

<td align="center">84%</td>

<td align="center">84%</td>

<td align="center">77%</td>

<td align="center">78%</td>

</tr>

</tbody>

</table>

We showed the detailed _information capability_ scores across divisions in Table 4\. The largest gap of sensing capability was 41% (92% for Benefit Files and 51% for Quality Control). Sensing information is an important capability to monitor and act on changes in the business environment or product trends. Thus, high sensing capability of the employees in the Benefit Files Division is beneficial to the overall performance of the company. Collecting, organizing, and maintaining dimensions had a similar pattern to the sensing dimension, showing that the employees in Benefit Files Division were high (88%, 89%, and 79%) whereas the employees in Quality Control Division were relatively low (61%, 66%, and 55%). In the previous analysis, the Benefit Files Division was good at overall information behaviour resulting in high performance (94% in Table 6). Private Business Division had high processing capability (91%), while CCD had relatively lower processing capability (64%). The employees in Private Business Division reported to be relatively better at solving problems, making decision, and completing tasks with available information.

<table width="67%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: . Detailed _information capability_ comparison across divisions**</caption>

<tbody>

<tr>

<td> </td>

<th>Sensing</th>

<th>Collecting</th>

<th>Organizing</th>

<th>Processing</th>

<th>Maintaining</th>

</tr>

<tr>

<td>1\. PSC</td>

<td align="center">75%</td>

<td align="center">71%</td>

<td align="center">70%</td>

<td align="center">71%</td>

<td align="center">68%</td>

</tr>

<tr>

<td>2\. CHC</td>

<td align="center">79%</td>

<td align="center">80%</td>

<td align="center">74%</td>

<td align="center">80%</td>

<td align="center">73%</td>

</tr>

<tr>

<td>3\. Benefit Files</td>

<td align="center">92%</td>

<td align="center">88%</td>

<td align="center">89%</td>

<td align="center">82%</td>

<td align="center">79%</td>

</tr>

<tr>

<td>4\. Blue Card</td>

<td align="center">86%</td>

<td align="center">86%</td>

<td align="center">76%</td>

<td align="center">79%</td>

<td align="center">75%</td>

</tr>

<tr>

<td>5\. CBA</td>

<td align="center">77%</td>

<td align="center">76%</td>

<td align="center">80%</td>

<td align="center">71%</td>

<td align="center">73%</td>

</tr>

<tr>

<td>6\. CCD</td>

<td align="center">82%</td>

<td align="center">70%</td>

<td align="center">57%</td>

<td align="center">64%</td>

<td align="center">61%</td>

</tr>

<tr>

<td>7\. Economy research</td>

<td align="center">75%</td>

<td align="center">75%</td>

<td align="center">68%</td>

<td align="center">70%</td>

<td align="center">65%</td>

</tr>

<tr>

<td>8\. Major Group</td>

<td align="center">81%</td>

<td align="center">78%</td>

<td align="center">68%</td>

<td align="center">70%</td>

<td align="center">65%</td>

</tr>

<tr>

<td>9\. Private Business</td>

<td align="center">87%</td>

<td align="center">88%</td>

<td align="center">78%</td>

<td align="center">91%</td>

<td align="center">81%</td>

</tr>

<tr>

<td>10\. Quality Control</td>

<td align="center">51%</td>

<td align="center">61%</td>

<td align="center">66%</td>

<td align="center">68%</td>

<td align="center">55%</td>

</tr>

<tr>

<td>11\. State</td>

<td align="center">80%</td>

<td align="center">77%</td>

<td align="center">68%</td>

<td align="center">73%</td>

<td align="center">73%</td>

</tr>

<tr>

<td>Average</td>

<td align="center">79%</td>

<td align="center">76%</td>

<td align="center">71%</td>

<td align="center">76%</td>

<td align="center">72%</td>

</tr>

</tbody>

</table>

We also made comparisons across demographic characteristics such as the years that employee had been in the specific job position. If an employee had been in a market analyst position in the Company for two years and previously in the similar position at the other company for three years, his or her job experience was measured at five years. As the results showed in Table 5, the gaps between the highest score and the lowest score were not as high as the gaps among divisions. The gaps ranged from 8% to 17%. The employees who had more than twenty years of job experience performed better (95%) than those who have less than one year (78%). The _information capability_ gap between the employees with more than twenty years experience (81%) and less than one year (69%) seems to influence these performance differences. The employees who have five to six years of job experience had the highest _information motivation_ (88%) and information technology capability (79%). The employees with more than twenty years of experience had the highest analytic style (93%), but the employees with 16-20 years of experience had the lowest _information motivation_ (78%), _information technology capability_ (66%) and _analytic style_ (77%).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: .Overall comparison across job experiences**</caption>

<tbody>

<tr>

<td> </td>

<th>Performance</th>

<th>Information motivation</th>

<th>Information capability</th>

<th>Information technology capability</th>

<th>Analytic style</th>

</tr>

<tr>

<td>1\. Less than one year</td>

<td align="center">78%</td>

<td align="center">82%</td>

<td align="center">69%</td>

<td align="center">78%</td>

<td align="center">78%</td>

</tr>

<tr>

<td>2\. 1-2yrs</td>

<td align="center">85%</td>

<td align="center">81%</td>

<td align="center">76%</td>

<td align="center">72%</td>

<td align="center">81%</td>

</tr>

<tr>

<td>3\. 3-4yrs</td>

<td align="center">87%</td>

<td align="center">82%</td>

<td align="center">74%</td>

<td align="center">68%</td>

<td align="center">78%</td>

</tr>

<tr>

<td>4\. 5-6yrs</td>

<td align="center">87%</td>

<td align="center">88%</td>

<td align="center">77%</td>

<td align="center">79%</td>

<td align="center">81%</td>

</tr>

<tr>

<td>5\. 7-10yrs</td>

<td align="center">84%</td>

<td align="center">85%</td>

<td align="center">75%</td>

<td align="center">79%</td>

<td align="center">86%</td>

</tr>

<tr>

<td>6\. 11-15yrs</td>

<td align="center">91%</td>

<td align="center">86%</td>

<td align="center">80%</td>

<td align="center">67%</td>

<td align="center">83%</td>

</tr>

<tr>

<td>7\. 16-20yrs</td>

<td align="center">83%</td>

<td align="center">78%</td>

<td align="center">70%</td>

<td align="center">66%</td>

<td align="center">77%</td>

</tr>

<tr>

<td>8\. More than 20 years</td>

<td align="center">95%</td>

<td align="center">82%</td>

<td align="center">81%</td>

<td align="center">72%</td>

<td align="center">93%</td>

</tr>

<tr>

<td>Average</td>

<td align="center">83%</td>

<td align="center">81%</td>

<td align="center">73%</td>

<td align="center">73%</td>

<td align="center">80%</td>

</tr>

</tbody>

</table>

Table 6 showed that the gap of information formality scores between the 5-6 year group (90%) and the 16-20 years group (69%) was the highest (21%), and the other gaps were ranged from 7% to 18%. This result reflects that the employees who had 16-20 years of job experience were more reluctant to use formal sources of information in the work place and are more dependent on their own informal information sources. This activity caused lower overall _information motivation_ (78% in Table 8) and seems to negatively influence the overall job performance in this group.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: Detailed _information motivation_ comparison across job experiences**</caption>

<tbody>

<tr>

<td> </td>

<th>Proactiveness</th>

<th>Sharing</th>

<th>Transparency</th>

<th>Formality</th>

</tr>

<tr>

<td>1\. Less than 1yr</td>

<td align="center">87%</td>

<td align="center">84%></td>

<td align="center">80%</td>

<td align="center">78%</td>

</tr>

<tr>

<td>2\. 1-2yrs</td>

<td align="center">85%</td>

<td align="center">85%</td>

<td align="center">78%</td>

<td align="center">76%</td>

</tr>

<tr>

<td>3\. 3-4yrs</td>

<td align="center">84%</td>

<td align="center">86%</td>

<td align="center">76%</td>

<td align="center">83%</td>

</tr>

<tr>

<td>4\. 5-6yrs</td>

<td align="center">90%</td>

<td align="center">89%</td>

<td align="center">83%</td>

<td align="center">90%</td>

</tr>

<tr>

<td>5\. 7-10yrs</td>

<td align="center">85%</td>

<td align="center">88%</td>

<td align="center">84%</td>

<td align="center">82%</td>

</tr>

<tr>

<td>6\. 11-15yrs</td>

<td align="center">88%</td>

<td align="center">92%</td>

<td align="center">81%</td>

<td align="center">84%</td>

</tr>

<tr>

<td>7\. 16-20yrs</td>

<td align="center">83%</td>

<td align="center">82%</td>

<td align="center">76%</td>

<td align="center">66%</td>

</tr>

<tr>

<td>8\. More than twenty years</td>

<td align="center">95%</td>

<td align="center">82%</td>

<td align="center">81%</td>

<td align="center">72%</td>

</tr>

<tr>

<td>Average</td>

<td align="center">83%</td>

<td align="center">81%</td>

<td align="center">73%</td>

<td align="center">73%</td>

</tr>

</tbody>

</table>

Table 7 showed that the employees with 16-20 years and less than one year of job experience had relatively lower _information capability_ than employees with more than twenty years of job experience. The gaps ranged from 12% to 17%.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7: Detailed _information capability_ Ccomparison across years of job experience**</caption>

<tbody>

<tr>

<td width="23%"></td>

<th>Sensing</th>

<th>Collecting</th>

<th>Organizing</th>

<th>Processing</th>

<th>Maintaining</th>

</tr>

<tr>

<td>1\. Less than 1yr</td>

<td align="center">77%</td>

<td align="center">70%</td>

<td align="center">70%</td>

<td align="center">68%</td>

<td align="center">62%</td>

</tr>

<tr>

<td>2\. 1-2yrs</td>

<td align="center">77%</td>

<td align="center">77%</td>

<td align="center">74%</td>

<td align="center">77%</td>

<td align="center">73%</td>

</tr>

<tr>

<td>3\. 3-4yrs</td>

<td align="center">76%</td>

<td align="center">74%</td>

<td align="center">75%</td>

<td align="center">74%</td>

<td align="center">71%</td>

</tr>

<tr>

<td>4\. 5-6yrs</td>

<td align="center">81%</td>

<td align="center">82%</td>

<td align="center">71%</td>

<td align="center">79%</td>

<td align="center">74%</td>

</tr>

<tr>

<td>5\. 7-10yrs</td>

<td align="center">79%></td>

<td align="center">76%</td>

<td align="center">69%</td>

<td align="center">77%</td>

<td align="center">755</td>

</tr>

<tr>

<td>6\. 11-15yrs</td>

<td align="center">85%</td>

<td align="center">83%</td>

<td align="center">76%</td>

<td align="center">83%</td>

<td align="center">76%</td>

</tr>

<tr>

<td>7\. 16-20yrs</td>

<td align="center">74%</td>

<td align="center">72%</td>

<td align="center">63%</td>

<td align="center">73%</td>

<td align="center">68%</td>

</tr>

<tr>

<td>8\. More than twenty years</td>

<td align="center">87%</td>

<td align="center">86%</td>

<td align="center">72%</td>

<td align="center">85%</td>

<td align="center">74%</td>

</tr>

<tr>

<td>Average</td>

<td align="center">77%</td>

<td align="center">74%</td>

<td align="center">71%</td>

<td align="center">74%</td>

<td align="center">71%</td>

</tr>

</tbody>

</table>

Comparisons across the sexes did not find large gaps of scores. As shown in Table 8, Table 9, and Table 10, the largest gap was only 10% in organizing capability (73% of female and 63% of male). The overall pattern showed that female employees were somewhat better information users than male employees in the Company. Only collecting and processing information capabilities were higher in the male group than in the female group. Every dimension of _information motivation_ of the female group were higher than those of the male group. However, more females (n=163) participated in the online survey than male (n=35), and this unbalanced sample structure should be considered in the interpretation. Also, overall score comparison across sexes was difficult to interpret because most of the scores (both the highest and lowest) were over 70% within a 10% gap.

<table width="71%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 8: Overall comparison across sexes**</caption>

<tbody>

<tr>

<td width="23%"> </td>

<th>Performance</th>

<th>Information motivation</th>

<th>Information capability</th>

<th>Information technology capability</th>

<th>Analytic style</th>

</tr>

<tr>

<td>1\. Male</td>

<td align="center">84%</td>

<td align="center">79%</td>

<td align="center">74%</td>

<td align="center">70%</td>

<td align="center">77%</td>

</tr>

<tr>

<td>2\. Female</td>

<td align="center">86%</td>

<td align="center">84%</td>

<td align="center">75%</td>

<td align="center">74%</td>

<td align="center">82%</td>

</tr>

</tbody>

</table>

<table width="71%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 9:Detailed _information motivation_ Comparison across Sexes**</caption>

<tbody>

<tr>

<td width="23%"> </td>

<th>Proactiveness</th>

<th>Sharing</th>

<th>Transparency</th>

<th>Formality</th>

</tr>

<tr>

<td>1\. Male</td>

<td align="center">80%</td>

<td align="center">82%</td>

<td align="center">74%</td>

<td align="center">78%</td>

</tr>

<tr>

<td>2\. Female</td>

<td align="center">87%</td>

<td align="center">87%</td>

<td align="center">81%</td>

<td align="center">81%</td>

</tr>

</tbody>

</table>

<table width="71%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 10: Detailed _information capability_ comparison across sexes**</caption>

<tbody>

<tr>

<td> </td>

<th>Sensing</th>

<th>Collecting</th>

<th>Orgainzing</th>

<th>Processing</th>

<th>Maintaining</th>

</tr>

<tr>

<td>1\. Male</td>

<td align="center">79%</td>

<td align="center">79%</td>

<td align="center">63%</td>

<td align="center">79%</td>

<td align="center">70%</td>

</tr>

<tr>

<td>2.Female</td>

<td align="center">78%</td>

<td align="center">76%</td>

<td align="center">73%</td>

<td align="center">76%</td>

<td align="center">72%</td>

</tr>

</tbody>

</table>

Our analysis of employees at the CAROL Company shows that _information capability_ and _information technology capability_ should be emphasized and enhanced over the other factors discussed, given the comparative study results. While the employees felt that they performed well and were motivated to use information effectively, the _information technology capability_ of some divisions such as the Private Business Division were perceived to be relatively lower than the company as a whole. Company-wide information technology and information use training focusing on organizing and maintaining information should be beneficial to enhancing employee's information technology capability and information use.

The Quality Control Division showed relatively lower _information motivation_ and _information capability_ when compared to employees in other divisions of the Company. One of the possible reasons for this result could be that most of the workflow is pre-defined and automatic and less motivation to use information effectively in this division. Of course this needs to be investigated in much more detail to uncover the actual source of this relative gap.

The employees who had 16?20 years of job experience depended more on informal information rather than on formal information. This seemed to cause lower _information motivation_ and lower overall job performance. In particular, the _information formality_ of this group should be further enhanced because these employees usually have supervisory or managerial roles given their tenure and tend to wield considerable influence in the organization.

## Recommendations

*   Focus on enhancing the capability to use information effectively. The CAROL Company has high motivation and culture to use information effectively for performance and its employees possess a high analytic style, but their perceived capability to use information is not as high as it should be. Specifically, the capability to organize and reuse information warrants improvement. A training program or incentive system to support these activities would be useful to enhance these capabilities and the overall job performance.
*   The perceived individual information technology capability of the company should be improved. Company-wide information technology training to increase employees' belief about their ability to use new information technology would be beneficial because it would influence the information use capability and eventually increase knowledge worker's productivity in the company.
*   Encourage organizational culture changes that encourage more freely communicating problems and solutions with co-workers.
*   Emphasize formal information use by more senior employees who have 16-20 years of job experience. This might involve information technology training to migrate these employees more readily to formal company systems. Also, the implementation of incentives or a formal knowledge management system might be beneficial. Developing a training program targeting senior male employees seems to be specifically beneficial to the overall company's performance.

## Discussion

### Limitation

Some limitations of the present study should be noted when interpreting its findings. It is unknown how well the model and its findings will be able to be replicated beyond the specific conditions of this study. Although the final field survey used employees in multiple divisions that are substantially different in structure, role, and history, which might be conducive to the replication of the findings, future work is needed to understand the other types of knowledge workers, such as engineers, doctors, teachers etc., since anybody works with information can be considered a knowledge worker.

To help ensure study participation and maintain the anonymity of responses, this survey used a self-report measure of performance. From a methodological point of view, additional sources of performance data (such as actual measures of job output) could provide further evidence of the relationship between information management and job performance. However, given that the target sample in this study includes knowledge workers across multiple business units and divisions working with different product types and at different job levels, the use of such quantitative measures of performance was not possible. Future research could extend the model tested in this study to include additional measures of performance.

### Implications for practice

The practical contribution of this research is to identify what are the most important aspects of effective information management. Results indicate that the capability to use information and information technologyis moderately lower than the overall motivation to use information effectively at the company. Specifically, employees are motivated to use information proactively to make better decisions and share information to increase job productivity. A high analytic style of employees seems to be a clear advantage for the company. However, the capability to organize and maintain valuable information was found to be relatively lower than the other scores and should be enhanced to improve overall job performance. The company might also consider ways to enhance the overall information use motivation and information use capability of employees in the Quality Control Division as well as individuals with 16-20 years of job experience.

Davenport _et al._ ([1998](#davenport1988)) argued that personal information effectiveness is the most crucial factor in business information management, because personal manipulation of information is the main input of products in knowledge-based companies. In essence, the distillation of personal information behaviour and use is fundamental to a knowledge-based organization in order for it to stay competitive. Our research will be beneficial to a company's knowledge management in that it will provide validated measurements of individual-level information motivation and capability that are the essential component of a company's knowledge.

In addition to the theoretical explanation by information behaviour model, the present research actually provided the consulting tools to the company through information motivation and capability, which can have practical implications. The average scores of information motivation and capability of the employees of the company or the division were used for this analysis. The analysis of the information behaviour model of employees at this company showed that information capability should be emphasized and enhanced over the other factors. The practical guidance based on this analysis was to focus on enhancing the capability to use information effectively by implementing a training program or incentive system to support these activities. Specifically, the capability to organize and reuse information warrants improvement in this company.

In the analysis across divisions and demographic classifications, the detailed dimensions of information motivation and capability suggested some practical implications. For example, employees of one division in the company showed relatively lower sensing information capability when compared to employees in other divisions. One of the possible reasons for this result could be that most of the workflow is pre-defined and automatic and needed less ability to use information effectively in sensing new information in this division. The reactive and inflexible culture of workflow in this division could have influenced sensing capability based on the reciprocal nature of the environment and an individual's behaviour ([Marchand _et al._ 2000](#marchand2000); [2002](#marchand2002)).

Also, the employees who had 16?20 years of job experience had low information formality, which shows that employees with long job tenure depended more on informal information rather than on formal information. In particular, the information formality of this group should be further enhanced because these individuals usually have a supervisory or managerial role given their tenure and tend to wield considerable influence in the organization. A possible suggestion was to increase information technology training to integrate these employees more readily into formal company systems. Also, the implementation of incentives for the bulletin board system or a formal knowledge management system might be beneficial. In this way, the practical guidance offered for using information motivation and capability was successfully implemented and proved beneficial in this case.

This study would be helpful for practitioners in order to evaluate the successful implementation of knowledge management systems. Given that the knowledge worker systems are the priority of information technology investment in the future ([Kyle 2003](#kyle2003)) and information management is the most important strategic focus in the era of the Information Revolution ([Drucker 1988](#drucker1998); [1999](#drucker1999); [Davenport _et al._ 1998](#davenport1998); [Grant 1996](#grant1996)), the information behaviour model developed in this study should provide practical guidance for implementing these systems in order to achieve high information motivation and capability by the knowledge workers and enhance subsequent job performance. Without enhancing the information motivation and capability of the knowledge workers, the investment in knowledge management systems or recruiting high analytic workers/technicians would not be directly linked to the high performance of the organization.

While we did not apply information motivation and capability measures to the recruiting process, several participants of the survey wrote that this measurement would be a useful tool for those purposes. Given that knowledge workers are the most important intangible assets in a company, this application would be beneficial to the practitioners who try to implement knowledge management.

### Implications for research

This study applies the information orientation model [Marchand _et al._ 2002](#marchand2002)). This model was validated and generalized with a large number of organizational samples in order to examine the individual's psychological mechanism by the used of field-based survey and subsequent case analysis. These research and findings would add to the base of knowledge from which future research can continue making progress toward a better understanding of how a knowledge worker is motivated and able to use information for his or her job performance. Further research to improve the degree to which it approximates the personal information effectiveness and its relationship to job performance would be beneficial to making this understanding complete. The fundamental implication for information system researcher of this study would be the explanation of information use and knowledge application related to the job performance ([Grant 1996](#grant1996)). Individual-level information use, motivation, and capability were highly related to the job performance as suggested by information management researchers ([Drucker 1988](#drucker1998); [Davenport _et al._ 1998](#davenport1998); [Grant 1996](#grant1996)). The empirical findings in this study would help link psychology research and information management research in understanding one of the most important current issues: information management for performance.

### Conclusion

Recent research establishing the information effectiveness of knowledge workers and the information orientation model has advanced the practices and understanding of complex information behaviour. The present research contributes to this progress by formulating and performing a test of a model that explicitly applies information motivation and capability to job performance. The information behaviour model in this study introduced here may open the door to understanding the information use behaviour of knowledge workers that improves performance in the information age. Furthermore, this new model incorporates the fruitful knowledge of individual-level behaviour in the psychology literature into the complex information behaviour based on the comprehensive models. This result is beneficial to the information management literature by explaining the relationship between information effectiveness and performance. This study also provides both practical and theoretical contributions based on empirical testing involving relatively large number of field samples. Given that most jobs worldwide increasingly rely on information use and knowledge work, and most organizations depend on the performance of these knowledge workers, findings of this study should be beneficial to the overall success of information management, which is a fundamental issue of information system scholars in the information age.

## About the author

Yujong Hwang is Associate Professor of Management Information Systems in the School of Accountancy and MIS at DePaul University in Chicago. He is also Professor as International Scholar at Kyung Hee University in Korea. He received his Ph.D. in MIS from the University of South Carolina. His research focuses on e-commerce, knowledge management, and human-computer interaction. He Associate Editor for European Journal of Information Systems and Behaviour & IT.

#### References

*   Acsente, D. (2010). Literature review: a representation of how future knowledge worker is shaping the twenty-first century workplace, _On the Horizon_, **18**(3), 279-287
*   Agarwal, R., & Venkatesh, V. (2002). Assessing a firm's Web presence: a heuristic evaluation procedure for the measurement of usability. _Information Systems Research_, **13**(2), 168-188
*   Bandura, A., (1977). _Social learning theory._ Englewood Cliffs, NJ: Prentice-Hall
*   Bandura, A., (1986). _Social foundations of thought and action: a social cognitive theory_. Englewood Cliffs, NJ: Prentice-Hall.
*   Brown, S. & Ganesan, S. (2001). Self-efficacy as a moderator of information-seeking effectiveness, _Journal of Applied Psychology_, **86**(5), 1043-1051
*   Compueau, D. & Higgins, C. (1995). Computer self-efficacy: development of a measure and initial test. _MIS Quarterly_, **19**(2), 189-210
*   Davenport, T., De Long, D.W. & Beers, M.C., (1998). Successful knowledge management project, _Sloan Management Review_, **39**(2), 43-57
*   Davis, F. (1989). Perceived usefulness, perceived ease of use, and user acceptance of information technology, _MIS Quarterly_, **13**(4), 319-340
*   Davis, F., Bagozzi, R. & Warshaw, P. (1989). User acceptance of computer technology: a comparison of two theoretical models. _Management Science_, **35**(8), 982-1003
*   Drucker, P.F. (1988). The coming of the new organization, _Harvard Business Review_, **66**(1), 45-53
*   Drucker, P. F., (1999). _Management challenges for the 21st century_, New York, NY: Harper Collins Publishers, Inc.
*   Grant, R.M. (1996). Toward a knowledge?based theory of the firm, _Strategic Management Journal_, **17**(10), 109-122
*   H?glund, L. (1998). A case study of information culture and organizational climates. _Swedish Library Research/Svensk Biblioteksforskning/_ **3**(4), 73-86.
*   Jones, W., (2007). Personal information management. _Annual Review of Information Science and Technology_, **41**, 453?504.
*   Marchand, D.A., Kettinger, W.J. & Rollins J.D. (2000). Information orientation: people, technology and the bottom line. _Sloan Management Review_, **41**(4), 69-80
*   Marchand, D.A., Kettinger, W.J. & Rollins J.D. (2001). __Making the invisible visible: how companies win with the right information, people, and IT__, Chichester, UK: John Wiley and Sons.
*   Marchand, D.A., Kettinger, W.J. & Rollins J.D. (2002). _Information orientation: the link to business performance_, 2nd ed. Oxford: Oxford University Press.
*   Messick, S., (1984). The nature of cognitive styles: problems and promises in educational research, _Educational Psychology_, **19**(1), 59-74
*   Orna, E., (1996). Valuing information: problems and opportunities, In David Best, (Ed.). _The fourth resource: information and its management_, (pp. 18-40). Aldershot, UK: Aslib/Gower.
*   Sternberg, R. (1977). _Intelligence, information processing, and analogical reasoning. The componential analysis of human abilities_, Hillsdale, NJ: Erlbaum
*   Witkin, H., Goodenough, D. & Cox, P. (1977). Field dependent and field independent cognitive styles and their educational implications, _Review of Educational Research_, **47**(1), 1-64

## Appendix 1\. Sample demographics (n=352)

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th width="16%" valign="middle"></th>

<th width="19%" valign="middle">

<div align="left">Division(1-11)</div>

</th>

<th width="4%" valign="middle">1</th>

<th width="3%" valign="middle">2</th>

<th width="4%" valign="middle">3</th>

<th width="4%" valign="middle">4</th>

<th width="3%" valign="middle">5</th>

<th width="4%" valign="middle">6</th>

<th width="4%" valign="middle">7</th>

<th width="4%" valign="middle">8</th>

<th width="4%" valign="middle">9</th>

<th width="5%">10</th>

<th width="6%">11</th>

<th width="6%">Total</th>

<th width="14%">%</th>

</tr>

<tr>

<td>**Demographics**</td>

<td>**Employee Count**</td>

<td>38</td>

<td>25</td>

<td>31</td>

<td>35</td>

<td>53</td>

<td>10</td>

<td>31</td>

<td>29</td>

<td>36</td>

<td align="center">30</td>

<td align="center">34</td>

<td align="center">352</td>

<td align="center">100%</td>

</tr>

<tr>

<td rowspan="5">

<div align="center">Age</div>

</td>

<td>18-24 years</td>

<td>3</td>

<td>4</td>

<td>0</td>

<td>8</td>

<td>5</td>

<td>0</td>

<td>6</td>

<td>4</td>

<td>0</td>

<td align="center">8</td>

<td align="center">4</td>

<td align="center">42</td>

<td align="center">12%</td>

</tr>

<tr>

<td>25-34 years</td>

<td>8</td>

<td>7</td>

<td>15</td>

<td>7</td>

<td>20</td>

<td>4</td>

<td>7</td>

<td>13</td>

<td>15</td>

<td align="center">12</td>

<td align="center">15</td>

<td align="center">123</td>

<td align="center">35%</td>

</tr>

<tr>

<td>35-44 years</td>

<td>17</td>

<td>9</td>

<td>11</td>

<td>11</td>

<td>13</td>

<td>3</td>

<td>2</td>

<td>7</td>

<td>9</td>

<td align="center">8</td>

<td align="center">9</td>

<td align="center">99</td>

<td align="center">28%</td>

</tr>

<tr>

<td>45-54 years</td>

<td>6</td>

<td>0</td>

<td>5</td>

<td>7</td>

<td>11</td>

<td>0</td>

<td>10</td>

<td>5</td>

<td>10</td>

<td align="center">0</td>

<td align="center">2</td>

<td align="center">56</td>

<td align="center">16%</td>

</tr>

<tr>

<td>55-64 years</td>

<td>4</td>

<td>5</td>

<td>0</td>

<td>2</td>

<td>4</td>

<td>3</td>

<td>6</td>

<td>0</td>

<td>2</td>

<td align="center">2</td>

<td align="center">4</td>

<td align="center">32</td>

<td align="center">9%</td>

</tr>

<tr>

<td rowspan="2">Gender</td>

<td>Male</td>

<td>4</td>

<td>0</td>

<td>8</td>

<td>2</td>

<td>12</td>

<td>3</td>

<td>7</td>

<td>0</td>

<td>13</td>

<td align="center">9</td>

<td align="center">2</td>

<td align="center">60</td>

<td align="center">17%</td>

</tr>

<tr>

<td>Female</td>

<td>34</td>

<td>25</td>

<td>23</td>

<td>33</td>

<td>41</td>

<td>7</td>

<td>24</td>

<td>29</td>

<td>23</td>

<td align="center">21</td>

<td align="center">32</td>

<td align="center">292</td>

<td align="center">83%</td>

</tr>

<tr>

<td rowspan="4">Educational Backgrounds</td>

<td>Seondary or High School</td>

<td>9</td>

<td>8</td>

<td>7</td>

<td>5</td>

<td>15</td>

<td>3</td>

<td>15</td>

<td>14</td>

<td>10</td>

<td align="center">18</td>

<td align="center">9</td>

<td align="center">113</td>

<td align="center">32%</td>

</tr>

<tr>

<td>Associate/Technical Degree</td>

<td>7</td>

<td>5</td>

<td>9</td>

<td>8</td>

<td>10</td>

<td>5</td>

<td>5</td>

<td>7</td>

<td>12</td>

<td align="center">4</td>

<td align="center">9</td>

<td align="center">81</td>

<td align="center">23%</td>

</tr>

<tr>

<td>Undergraduate degree</td>

<td>11</td>

<td>9</td>

<td>14</td>

<td>14</td>

<td>28</td>

<td>1</td>

<td>11</td>

<td>6</td>

<td>14</td>

<td align="center">8</td>

<td align="center">14</td>

<td align="center">130</td>

<td align="center">37%</td>

</tr>

<tr>

<td>Graduate degree</td>

<td>11</td>

<td>3</td>

<td>1</td>

<td>8</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td align="center">0</td>

<td align="center">2</td>

<td align="center">28</td>

<td align="center">8%</td>

</tr>

<tr>

<td rowspan="7">Time in the Present Position</td>

<td height="30">Less than 1 year</td>

<td>7</td>

<td>9</td>

<td>5</td>

<td>10</td>

<td>18</td>

<td>5</td>

<td>6</td>

<td>5</td>

<td>5</td>

<td align="center">0</td>

<td align="center">7</td>

<td align="center">77</td>

<td align="center">22%</td>

</tr>

<tr>

<td>1-2 years</td>

<td>15</td>

<td>9</td>

<td>9</td>

<td>11</td>

<td>19</td>

<td>0</td>

<td>12</td>

<td>8</td>

<td>17</td>

<td align="center">15</td>

<td align="center">15</td>

<td align="center">130</td>

<td align="center">37%</td>

</tr>

<tr>

<td>3-4 years</td>

<td>4</td>

<td>5</td>

<td>4</td>

<td>5</td>

<td>1</td>

<td>2</td>

<td>7</td>

<td>9</td>

<td>10</td>

<td align="center">11</td>

<td align="center">8</td>

<td align="center">66</td>

<td align="center">19%</td>

</tr>

<tr>

<td>5-6 years</td>

<td>0</td>

<td>2</td>

<td>7</td>

<td>2</td>

<td>8</td>

<td>0</td>

<td>2</td>

<td>4</td>

<td>4</td>

<td align="center">2</td>

<td align="center">0</td>

<td align="center">31</td>

<td align="center">9%</td>

</tr>

<tr>

<td>7-10 years</td>

<td>7</td>

<td>0</td>

<td>2</td>

<td>7</td>

<td>7</td>

<td>2</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td align="center">0</td>

<td align="center">4</td>

<td align="center">31</td>

<td align="center">9%</td>

</tr>

<tr>

<td height="30">11-15 years</td>

<td>2</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>3</td>

<td>0</td>

<td align="center">2</td>

<td align="center">0</td>

<td align="center">10</td>

<td align="center">3%</td>

</tr>

<tr>

<td height="22">16-20 years</td>

<td>3</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">7</td>

<td align="center">2%</td>

</tr>

</tbody>

</table>

## Appendix 2: Online survey items

Respondents were asked to reply to each item on an 11-point scale, where 1 = Strongly disagree, and 11 = Strongly agree)

*   Proactiveness: I enjoy learning ways to improve the use of information with respect to my job.
*   Sharing: I always pass information to my co-workers to help them do better.
*   Transparency: I communicate my mistakes to other people because they can learn from my mistakes.
*   Formality: When the information provided by the organization is easily accessible, I will use it instead of my own informal information.
*   Sensing: I am good at recognizing potential problems and sensing information to address them.
*   Collecting: I am good at gathering the right information to prevent information overload.
*   Organizing: I frequently take time during my working day to classify new information for easy future retrievals.
*   Processing: I know how to translate information into specific knowledge that can be used by others.
*   Maintaining: I am good at determining the future value of information for later use.
*   Job Performance: Compared to my co-worker, I achieve the objectives of my job.
*   IT Capability: I am confident in using IT to do my job, when I can call someone for help if I get stuck.
*   Analytic Style: I would rather do something that challenges my thinking ability.