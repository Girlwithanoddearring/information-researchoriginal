﻿<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Windows version 5.2.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
      Methodological frameworks for developing a model of the public sphere in public libraries
    </title>
    <link href="../../IRstyle4.css" rel="stylesheet" media="screen" title="serif"><!--Enter appropriate data in the content fields-->
    <meta name="dc.title" content="Methodological frameworks for developing a model of the public sphere in public libraries">
    <meta name="dc.creator" content="Michael M. Widdersheim, Masanori Koizumi">
    <meta name="dc.subject" content="This paper identifies research gaps in the literature related to the public sphere and public libraries. The paper then proposes methodological frameworks and data collection techniques to extend research in this area.">
    <meta name="dc.description" content="This paper identifies research gaps in the literature related to the public sphere and public libraries. The paper then proposes methodological frameworks and data collection techniques to extend research in this area. A review of the literature related to the public sphere and public libraries is conducted to identify research gaps and develop promising research approaches. Previous literature is analyzed according to epistemological and ontological approaches. Existing literature related to the public sphere and public libraries exhibits epistemological and ontological shortcomings. Empirical studies are relatively few and existing studies remain tied to a limited public sphere concept. Two new methodological frameworks are proposed to guide future work in this area. One framework is the circulation of power model; the other framework is a custom-built model with multiple axes and dimensions.">
    <meta name="dc.subject.keywords" content="public libraries, public sphere, methodology, social theory"><!--leave the following to be completed by the Editor-->
    <meta name="robots" content="all">
    <meta name="dc.publisher" content="Professor T.D. Wilson">
    <meta name="dc.coverage.placename" content="global">
    <meta name="dc.type" content="text">
    <meta name="dc.identifier" content="ISSN 1368-1613">
    <meta name="dc.identifier" content="http://InformationR.net/ir/22-1/colis/colis1643.html">
    <meta name="dc.relation" content="http://InformationR.net/ir/22-1/infres221.html">
    <meta name="dc.format" content="text/html">
    <meta name="dc.language" content="en">
    <meta name="dc.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
    <meta name="dc.date.available" content="2017-03-15">
  </head>
  <body>
    <header>
      <h4>
        vol. 22 no. 1, March, 2017
      </h4>
    </header>
    <h2 style="text-align:center;">
      Proceedings of the Ninth International Conference on Conceptions of Library and Information Science, Uppsala, Sweden, June 27-29, 2016
    </h2>
    <article>
      <section>
        <h1 style="text-align:center;">
          Methodological frameworks for developing a model of the public sphere in public libraries
        </h1>
        <h2 class="author">
          <a href="#author">Michael M. Widdersheim</a> and <a href="#author">Masanori Koizumi</a>
        </h2><br>
        <blockquote>
          <strong>Introduction.</strong> This paper identifies research gaps in the literature related to the public sphere and public libraries. The paper then proposes methodological frameworks and data collection techniques to extend research in this area.<br>
          <strong>Method.</strong> A review of the literature related to the public sphere and public libraries is conducted to identify research gaps and develop promising research approaches.<br>
          <strong>Analysis.</strong> Previous literature is analyzed according to epistemological and ontological approaches<br>
          <strong>Results.</strong> Existing literature related to the public sphere and public libraries exhibits epistemological and ontological shortcomings. Empirical studies are relatively few and existing studies remain tied to a limited public sphere concept.<br>
          <strong>Conclusions.</strong> Two new methodological frameworks are proposed to guide future work in this area. One framework is the circulation of power model; the other framework is a custom-built model with multiple axes and dimensions
        </blockquote><br>
        <h2>
          Introduction
        </h2>
        <h3>
          Research purpose
        </h3>
        <p>
          Public libraries have been associated with the sociological concept of the public sphere, but these associations are still being developed. Attempts have been made to construct a more detailed and empirically-based model of the public sphere in public libraries, but such a project faces a number of challenges, including the development of appropriate research methods. The purpose of this preliminary research note is therefore to present methodological frameworks and data collection methods that might guide future work in this area. It is hoped that concrete research methods that are tied closely to sociological theory will lead to a more sophisticated description of the public sphere in public libraries
        </p>
        <h3>
          Definition of the public sphere
        </h3>
        <p>
          The phrase public sphere is the English translation of the German word Ö¦fentlichkeit, which refers to a type of transparent, undistorted communication that is open to anyone, oriented to common interests, and focused on argumentative give-and-take. Public spheres occur in face-to-face or mediated contexts, and they represent communicative interactions at various societal scales. While public sphere themes can be literary or cultural in nature, this study focuses on the political public sphere.
        </p>
        <p>
          In its most basic and generalised sense, the political public sphere expresses a reciprocal speaker-audience relationship (see Figure 1). On the one side of this interaction are networks of private people who form a public. As a public, these people deliberate to reach an understanding about a given situation. On the other side of the interaction are governing bodies, people such as legislators or engineers whose decisions affect the social lives of the public in question. Because they remain under scrutiny by a critical public, the decision-makers must justify their actions in a way that is more than just a display, a way that uses reasons and justification to account for their decisions. Publics in turn petition decision-makers for resolutions that reflect their concerns. The public can at any point question the truth, sincerity, or normative rightness of the actions of the decision-making body and demand further warrants or grounds from them. While it may be the case that publics or decision-makers can resort to strategic action, the public sphere relationship emphasizes a bond of reciprocal communicative power, not coercive power. Decision-making bodies wield authority because their decisions are valid; the public legitimates the decisions because they pass the tri-fold test of truth, sincerity, and rightness. The public sphere is a normative concept that emphasizes a deliberative process (<a href="#hab1992">Habermas, 1992</a>, p. 452).
        </p>
        <figure class="centre">
          <img src="colis1643fig1.png">
          <figcaption>
            <br>
            Figure 1: The political public sphere.
          </figcaption>
        </figure>
        <p>
          The public sphere concept is most commonly associated with German philosopher Jï¿½ï¿½n Habermas, whose work on the subject animated English-language research in a number of fields beginning in the late 1980s and early 1990s (Habermas, <a href="#hab1984">1984</a>, <a href="#hab1989a">1989a</a>, <a href="#hab1989b">1989b</a>), and whose more recent work has continued to resonate with media and political science scholars (Habermas, <a href="#hab1996">1996</a>, <a href="#hab2006">2006</a>). A central feature of the public sphere concept is it ambivalent nature-the question of whether the public sphere is "there" in an authentic sense or not, whether it has been overrun by power and money, and whether it might be reconstituted from a deteriorated state (Habermas, <a href="#hab1989a">1989a</a>). Though originally conceived as an intermediary between civil society groups and state bodies, globalizing economic tendencies and the formation of the European Union have resulted in a transnationalization of the public sphere concept (<a href="#hab1998">Habermas, 1998)</a>. The public sphere continues to be revisited and revised in secondary literature (e.g., <a href="#crossl2004">Crossley and Roberts, 2004</a>), a sign of its continued relevance.
        </p>
        <h3>
          History and development of the public sphere concept
        </h3>The concept of the public sphere has been developed extensively by Habermas since his first deployment of the term in 1962. The basic criteria of public sphere discourse-inclusiveness of participants, unrestricted exchange reasons for and against validity claims, orientation toward mutual understanding and consensus-have not changed since that time, but they represent only one dimension of the concept. The public sphere is a sociological category that has been utilized to describe social action-how and why people behave as they do in social contexts. Habermas takes a communicative approach to explaining action, one with a strong systems theory flavor. In the theory of society developed in Habermas (<a href="#hab1984">1984</a>, <a href="#hab1989b">1989b</a>), the public sphere is presented as an area of overlap between system and lifeworld (see Figure 2). In this model, the public sphere is situated in the societal component of the lifeworld along with the private sphere. The two other components of the lifeworld are culture and personality. In this application of the public sphere concept, the public sphere forms an area of interchange with the administrative system, or state, a subsystem on the system side of Habermas's action model. In the public sphere arena in this model, private actors fill the roles of citizens and clients.
        <figure class="centre">
          <img src="colis1643fig2.png">
          <figcaption>
            <br>
            Figure 2. The public sphere as an area of overlap between lifeworld and system.
          </figcaption>
        </figure>
        <p>
          As clients, private actors input taxes (money) and receive organizational accomplishments (power), and as citizens, they input mass loyalty (power) and receive political decisions (power) as outputs (<a href="#hab1989b">Habermas, 1989b, p. 320</a>). In the end, the model is supposed to demonstrate how the state and economic systems have come to dominate people's actions and have led to pathological consequences for culture, institutional orders, and identify formation. The public sphere is one channel through which this colonisation occurs.
        </p>
        <p>
          As Baxter (<a href="#baxter2011">2011</a>) points out, this model from Habermas (<a href="#hab1984">1984</a>, <a href="#hab1989b">1989b</a>) suffers from numerous problems, and the model has been revised in Habermas's more recent theories of society and politics. In Habermas (<a href="#hab1996">1996</a>, <a href="#hab2006">2006</a>), the lifeworld/system and public/private distinctions are still present, but they form continua rather than sharp contrasts. Baxter (<a href="#baxter2011">2011</a>) also points out that in more recent models, the "private sphere" from Habermas (<a href="#hab1984">1984</a>, <a href="#hab1989b">1989b</a>) has been replaced by "civil society" (Habermas, <a href="#hab1996">1996</a>, <a href="#hab2006">2006</a>) and "media system" has been added as a system (<a href="#hab2006">Habermas, 2006</a>). Habermas's recent societal theories situate the public sphere concept in a "circulation of power" model (<a href="#baxter2011">Baxter, 2011</a>). In this model from Habermas (<a href="#hab1996">1996</a>), the public sphere is an intermediary network located between civil society groups, economic groups, and media groups, on the one hand, and the decision-making bodies of the political system, on the other (see Figure 3). Using the public sphere, the various groups on the political system's periphery influence political decisions by transmitting "communicative power" to the political system's core. Their communicative power affects the "administrative power" wielded by decision-making bodies at the center. As Baxter (<a href="#baxter2011">2011</a>) notes, Habermas (<a href="#hab1996">1996</a>) equivocates on this point, but the public sphere appears to be situated outside the political system to form the system's environment. Various groups and associations that inform public sphere communication are also located beyond the political system on its outer periphery. The function of the public sphere is to receive and package various messages from the outer periphery and relay them to decision-making bodies. When functioning without distortion from money and power, the public sphere ensures that collective values and interests are translated into legislative, judicial, and executive decisions. As Habermas ( <a href="#hab2006">2006</a>) notes, however, the perennial danger is that public sphere discourses are influenced by economic, media, and social power in a way that marginalizes certain perspectives and privileges others. The challenge is to check these powers in a way that protects rights to free speech.
        </p>
        <figure class="centre">
          <img src="colis1643fig3.png">
          <figcaption>
            <br>
            Figure 3: Circulation of power model
          </figcaption>
        </figure>
        <h2>
          Library literature and the public sphere
        </h2>
        <h3>
          Overview
        </h3>
        <p>
          Lending libraries were first associated with the public sphere in Habermas (<a href="#hab1962">1962</a>; <a href="#hab1989a">1989a, p. 50</a>). The public sphere was first associated specifically with public libraries by Thauer and Vodosek (<a href="#thau1978">1978</a>) and Schuhböck (<a href="#schu1983">1983</a>, <a href="#schu1994">1994</a>) in their discussions of public libraries in Germany. Associations between the public sphere and public libraries began to appear in Britain by Greenhalgh, Landry, and Worpole (<a href="#green1993">1993</a>), Greenhalgh, Worpole, and Landry (<a href="#green1995">1995</a>), and Webster (<a href="#web1995">1995</a>). The public sphere concept became more widely used and associated with libraries in Scandinavia (<a href="#emerek1997">Emerek and Ørum, 1997</a>; Vestheim, <a href="#vest1997a">1997</a>, <a href="#vest1997b">1997b</a>), North America (<a href="#busch2003">Buschman, 2003</a>; <a href="#leckie2007">Leckie and Buschman, 2007</a>), and elsewhere.
        </p>
        <p>
          Recent research on the public sphere in public libraries finds two main dimensions: in one dimension, public libraries constitute public sphere commons; in the other dimension, public libraries are objects of public sphere discourse where they are governed and legitimated (<a href="#wid2016">Widdersheim and Koizumi, 2016</a>). With respect to their commons function, several authors have discussed the social value of the public sphere in public libraries (<a href="#mcc2004">McCook, 2004, pp. 188-189</a>). Social trust, social inclusion, solidarity, and social capital have been said to be positive spillovers of public sphere meetings in public libraries (<a href="#aabo2005">Aabø&nbsp;2005</a>; <a href="#aabo2012">Aabø&nbsp;and&nbsp;Audunson, 2012</a>; <a href="#aabo2010">Aabø, Audunson and Vårheim, 2010</a>; <a href="#aabo2007">Audunson, Vårheim, Aabø and Holm, 2007</a>). Public libraries are not passive or neutral agents in the constitution of public sphere commons (<a href="#andersen2006">Andersen and Skouvig, 2006</a>). Several authors question the quality of public sphere discourse in public libraries (<a href="#alstad2003">Alstad and Curry, 2003</a>;<a href="#leckie2002">Leckie and Hopkins, 2002</a>; <a href="#mcn2014">McNally, 2014/2015</a>). On the other side, where public libraries are objects of public sphere discourse, one theme of study is the meanings and justifications given by various stakeholders for public library creation, closings, and purpose (<a href="#evjen2015">Evjen, 2015</a>; <a href="#ing2015">Ingraham, 2015</a>; <a href="#newman2007">Newman, 2007</a>). Communicative power is generated both to legitimize and to govern public libraries (Widdersheim and Koizumi, <a href="#wid2015">2015</a>, <a href="#wid2016">2016</a>).
        </p>
        <h3>
          Epistemological limitations of previous research
        </h3>
        <p>
          Existing literature about the public sphere in public libraries suffers from several methodological problems. Within existing literature, relatively few studies present results based on empirical data. Existing data collection methods include interviews (<a href="#evjen2015">Evjen, 2015</a>; <a href="#green1993">Greenhalgh et al., 1993</a>; <a href="#newman2007">Newman, 2007</a>), surveys (Audunson <em>et al.</em>, 2007; <a href="#var">Vårheim, Steinmo and Ide, 2008</a>), ethnography (Aabø&nbsp;and&nbsp;Audunson, 2012; <a href="#leckie2002">Leckie and Hopkins, 2002</a>), histories (<a href="#emerek1997">Emerek and Ørum, 1997</a>; <a href="#vest1997a">Vestheim, 1997a</a>), and content analysis (Widdersheim and Koizumi, <a href="#wid2015">2015</a>, <a href="#wid2016">2016</a>). Still, most studies are conceptual in nature. More empirical studies are needed because research about the public sphere requires a mix of theory, conceptual analysis, and empirical evidence.
        </p>
        <h3>
          Ontological limitations of previous research
        </h3>
        <p>
          The second problem is ontological. An ontology specifies basic categories of experience and defines how they are organized. In a sociological theory, for example, the ontology is the set of basic concepts used to explain social action and social change. An ontological limitation in social research means that either the research does not define these basic concepts and use them to orient the research project or that the research does not contribute to theoretical development. In library research on the public sphere, for example, no studies articulate a cohesive sociological framework, such as the circulation of power model, as a framework to guide the research. Moreover, no studies about libraries and the public sphere contribute to existing theory about the public sphere concept. Using the circulation of power model as a methodological framework, for example, might present the following questions in a research study: Where does this communicative power travel? How is this public sphere discourse affected by other societal variables, such as the economy and state? Because the methodological frameworks of existing research are limited ontologically, the research can offer little to the development of sociological theory, especially regarding the public sphere concept.
        </p>
        <h2>
          Proposed methodological frameworks
        </h2>
        <h3>
          Framework 1: Circulation of power
        </h3>
        <p>
          In light of the epistemological and ontological limitations of existing research noted above, future research about the public sphere in public libraries must adopt more sophisticated methodological frameworks. A methodological framework establishes the guideposts for a research project. It is based on existing literature and theory about a topic, and it therefore incorporates the basic conceptual map-the ontology-that is central to investigating that topic. A methodological framework is important because it frames research problems and questions, and it suggests a general approach for collecting data to address the research questions. Data collection and analysis that is guided by existing literature about a topic produces results that are epistemologically sound.
        </p>
        <p>
          <a href="#hab1996">1996</a>) as a normative reconstruction of how political systems work, but it also serves as a useful methodological framework because 1) it specifies three main arenas that must be described: political center, inner periphery, and outer periphery; 2) it specifies two main types of power that might be observed: communicative and administrative; and 3) it identifies several key actors that might be studied in interaction: economic groups, civil society groups, media groups, quasi-state institutions, and decision-making bodies. A study of the public sphere in public libraries might use this framework to investigate how library systems operate along the center-periphery continuum, the mechanisms and practices through which communicative and administrative power circulate through libraries, and how libraries convert one type of power into another. This framework recommends a strongly qualitative approach to data collection, and it suggests that researchers focus on contexts where power exchanges occur between groups and across boundaries.
        </p>
        <h3>
          Framework 2: Societal quadrants with temporal and scalar dimensions
        </h3>
        <p>
          The circulation of power model above offers one potential methodological framework for future research about the public sphere in public libraries, but the model is limited to studies about the political system. A second, custom-built framework that includes the public sphere but that is not focused on the political system is therefore presented. Rather than a center/periphery axis, this second framework uses three other axes drawn both from early Habermas (<a href="#hab1984">1984</a>, <a href="#hab1989b">1989b</a>) and later Habermas (<a href="#hab1996">1996</a>, <a href="#hab2006">2006</a>): civil society/system, public/private, and time. The civil society/system and public/private axes form societal quadrants: the public sphere, private sphere, state, and economy (see Figure 4). At the intersection of all three axes is the object of study: public library systems. In other words, this framework supposes that public library systems contain aspects of all four quadrants simultaneously, that this mixed composition produces tensions and inconsistencies within the system, and that the dominant features of the system may change over time.
        </p>
        <p>
          The spheres in the middle of Figure 4 represent theoretical developments of the public sphere in public libraries that 1) account for each of the four societal dimensions; 2) account for changes in the dimensions over time; and 3) offer three scalar perspectives-micro, meso, and macro-to each of the three axes. These scales are necessary because public spheres occur in localized, inter-organizational, and society-wide contexts, and also because temporal patterns may be dynamic, durative, or cyclical. The attractiveness of this second framework is that it encourages a complex, multifaceted view of the public sphere in public library systems, and it avoids ahistorical and isolationist studies. Table 1 describes the objects of theoretical development for the three scalar dimensions in each societal quadrant.
        </p>
        <p>
          Like the methodological framework based on the circulation of power model, Framework 2 suggests a primarily qualitative approach to data collection. This framework emphasizes a historical account of the public sphere in public libraries, but also one that incorporates first-person or emic perspectives. Differentiating between public and private, civil society and system are hermeneutic and interpretive tasks that emphasize the role of the researcher and require qualitative data. Ethnographic methods with a strong historical focus therefore offer a promising approach. Ethnographic research could focus on numerous micro-level contexts in library systems of different sizes and locales in order to develop micro-, meso-, and macro-level theories. Ethnohistorical, anthrohistorical, and historical-anthropological methods may provide further insight into this area.
        </p>
        <figure class="centre">
          <img src="colis1643fig4.png" width="675" height="402">
          <figcaption>
            <br>
            Figure 4: Visualization of framework 2.
          </figcaption>
        </figure>
        <table class="center">
          <caption>
            <br>
            Table 1: Quadrants and theoretical scales for framework 2.
          </caption>
          <tbody>
            <tr>
              <th>
                Quadrant
              </th>
              <th>
                Quadrant characteristics
              </th>
              <th>
                Micro-level theory
              </th>
              <th>
                Meso-level theory
              </th>
              <th>
                Macro-level theory
              </th>
            </tr>
            <tr>
              <td>
                Public Sphere
              </td>
              <td>
                Openness<br>
                Common concern<br>
                Debate<br>
                Communicative power
              </td>
              <td>
                Person-level communication, e.g. hearings, forums
              </td>
              <td>
                Inter-organizational communication, networks
              </td>
              <td>
                Society-level communication
              </td>
            </tr>
            <tr>
              <td>
                Private Sphere
              </td>
              <td>
                Culture<br>
                Solidarity, norms, order<br>
                Personality, identity
              </td>
              <td>
                Personal interactions
              </td>
              <td>
                Informal associations and groups
              </td>
              <td>
                Society-wide movements
              </td>
            </tr>
            <tr>
              <td>
                State
              </td>
              <td>
                Executive bodies<br>
                Legislative bodies<br>
                Judicial bodies<br>
                Bureaucracy<br>
                Administrative power
              </td>
              <td>
                Local laws, local agencies, and local government bodies
              </td>
              <td>
                Regional laws and government bodies
              </td>
              <td>
                Federal and international laws and government bodies
              </td>
            </tr>
            <tr>
              <td>
                Economy
              </td>
              <td>
                Money<br>
                Media power<br>
                Economic power
              </td>
              <td>
                Person-level media exchanges
              </td>
              <td>
                Media groups and corporations
              </td>
              <td>
                National and international media systems
              </td>
            </tr>
          </tbody>
        </table>
        <h2>
          Conclusion
        </h2>
        <p>
          This research note proposed methodological frameworks that might guide future research related to the public sphere in public libraries. These frameworks are intended to overcome the epistemological and ontological limitations identified in previous work. Research about the public sphere in public libraries is significant because it contributes to an understanding of the public sphere concept and because it describes the social value of public libraries. Future studies about the public sphere and public libraries can both contribute to the public sphere concept and continue to explore how public libraries function in society.
        </p>
        <h2 id="author">
          About the author
        </h2>
        <p>
          <strong>Michael M. Widdersheim</strong> is a PhD candidate in the School of Information Sciences at the University of Pittsburgh, USA. He can be reached at <a href="mailto:mmw84@pitt.edu">mmw84@pitt.edu</a>.<br>
          <br>
          <strong>Masanori Koizumi</strong> is Assistant Professor in the Faculty of Library, Information and Media Science at the University of Tsukuba, Japan. He can be reached at <a href="mailto:koizumi@slis.tsukuba.ac.jp">koizumi@slis.tsukuba.ac.jp</a>.
        </p>
      </section>
      <section class="refs">
        <h2>
          References
        </h2>
        <ul class="refs">
          <li id="aabo2005">Aabø, S. (2005). The role and value of public libraries in the age of digital technologies. Journal of Librarianship and Information Science, 37(4), 205-211.
          </li>
          <li id="aabo2012">Aabø, S. &amp; Audunson, R. (2012). Use of library space and the library as place. Library &amp; Information Science Research, 34(2), 138-149.
          </li>
          <li id="aabo2010">Aabø, Audunson, R. &amp; Vårheim, A. (2010). How do public libraries function as meeting places? Library &amp; Information Science Research, 32(1), 16-26.
          </li>
          <li id="alstad2003">Alstad, C. &amp; Curry, A. (2003). Public space, public discourse, and public libraries. Libres, 13(1).
          </li>
          <li id="andersen2006">Andersen, J. &amp; Skouvig, L. (2006). Knowledge organization: A sociohistorical analysis and critique. Library Quarterly, 76(3), 300-322.
          </li>
          <li id="aabo2007">Audunson, R., Vårheim, A., Aabø, S. &amp; Holm, E. D. (2007). Public libraries, social capital, and low intensive meeting places. information research, 12(4).
          </li>
          <li id="baxter2011">Baxter, H. (2011). Habermas: The discourse theory of law and democracy. Stanford, CA: Stanford Law Books.
          </li>
          <li id="busch2003">Buschman, J. E. (2003). Dismantling the public sphere: Situating and sustaining librarianship in the age of the new public philosophy. Westport, CT: Libraries Unlimited.
          </li>
          <li id="crossl2004">Crossley, N. &amp; Roberts, J. M. (Eds.). (2004). After Habermas: New perspectives on the public sphere. Oxford, UK: Blackwell.
          </li>
          <li id="emerek1997">Emerek, L. &amp; Ørum, A. (1997). The conception of the bourgeois public sphere as a theoretical background for understanding the history of Danish public libraries. In N. W. Lund (Ed.), Nordic Yearbook of Library, Information, and Documentation Research (Vol. 1, pp. 27-57). Oslo, Norway: Novus.
          </li>
          <li id="evjen2015">Evjen, S. (2015). The image of an institution: Politicians and the urban library project. Library &amp; Information Science Research, 37(1), 28-35.
          </li>
          <li id="green1993">Greenhalgh, L., Landry, C. &amp; Worpole, K. (1993). Borrowed time? The future of public libraries in the UK. Gloucestershire, UK: Comedia.
          </li>
          <li id="green1995">Greenhalgh, L., Worpole, K. &amp; Landry, C. (1995). Libraries in a world of cultural change. London, UK: UCL Press.
          </li>
          <li id="hab1962">Habermas, J. (1962). Strukturwandel der Öffentlichkeit: Untersuchungen zu einer Kategorie der bügerlichen Gesellschaft. Frankfurt a. M.: Suhrkamp.
          </li>
          <li id="hab1984">Habermas, J. (1984). The theory of communicative action, volume 1: Reason and the rationalization of society (T. McCarthy, Trans.). Boston, MA: Beacon Press.
          </li>
          <li id="hab1989a">Habermas, J. (1989a). The structural transformation of the public sphere: An inquiry into a category of bourgeois society (T. Burger &amp; F. Lawrence, Trans.). Cambridge, MA: MIT Press.
          </li>
          <li id="hab1989b">Habermas, J. (1989b). The theory of communicative action, volume 2: Lifeworld and system: A critique of functionalist reason (T. McCarthy, Trans.). Boston, MA: Beacon Press.
          </li>
          <li id="hab1992">Habermas, J. (1992). Further reflections on the public sphere. In C. Calhoun (Ed.), Habermas and the public sphere. Cambridge, MA: MIT Press.
          </li>
          <li id="hab1996">Habermas, J. (1996). Between facts and norms: Contributions to a discourse theory of law and democracy (W. Rehg, Trans.). Cambridge, MA: MIT Press.
          </li>
          <li id="hab1998">Habermas, J. (1998). The inclusion of the other: Studies in political theory (C. Cronin &amp; P. D. Greif Eds.). Cambridge, MA: MIT Press.
          </li>
          <li id="hab2006">Habermas, J. (2006). Political communication in media society: Does democracy still enjoy an epistemic dimension? The impact of normative theory on empirical research. Communication Theory, 16(4), 411-426.
          </li>
          <li id="ing2015">Ingraham, C. (2015). Libraries and their publics: Rhetorics of the public library. Rhetoric Review, 34(2), 147-163.
          </li>
          <li id="leckie2007">Leckie, G. J. &amp; Buschman, J. E. (2007). Space, place, and libraries: An introduction. In J. E. Buschman &amp; G. J. Leckie (Eds.), The library as place: History, community, and culture (pp. 3-25). Westport, CT: Libraries Unlimited.
          </li>
          <li id="leckie2002">Leckie, G. J. &amp; Hopkins, J. (2002). The public place of central libraries: Findings from Toronto and Vancouver. Library Quarterly, 72(3), 326-372.
          </li>
          <li id="mcc2004">McCook, K. d. l. P. (2004). Introduction to public librarianship. New York, NY: Neal-Schuman.
          </li>
          <li id="mcn2014">McNally, M. B. (2014/2015). Response to Dr. Samuel E. Trosow's Keynote Address. Progressive Librarian, 43, 30-34.
          </li>
          <li id="newman2007">Newman, J. (2007). Re-mapping the public. Cultural Studies, 21(6), 887-909.
          </li>
          <li id="schu1983">Schuhböck, H. P. (1983). Die gesellschaftliche Funktion von Bibliotheken in der Bundesrepublik Deutschland: Zur neueren Diskussion nach 1945 [The societal function of libraries in the Federal Republic of Germany: The recent discussion since 1945]. Bibliothek: Forschung und Praxis, 7(3), 203-222.
          </li>
          <li id="schu1994">Schuhböck, H. P. (1994). Bibliothek und Ö¦fentlichkeit im Wandel [Libraries and the public sphere in transition]. Bibliothek Forschung und Praxis, 18(2), 217-229.
          </li>
          <li id="thau1978">Thauer, W. &amp; Vodosek, P. (1978). Geschichte der Ö¦fentlichen Bï¿½ï¿½rei in Deutschland [History of the public library in Germany]. Wiesbaden, West Germany: Otto Harrassowitz.
          </li>
          <li id="var08">Vårheim, A., Steinmo, S. &amp; Ide, E. (2008). Do libraries matter? Public libraries and the creation of social capital. Journal of Documentation, 64(6), 877-892.
          </li>
          <li id="vest1997a">Vestheim, G. (1997a). Fornuft, kultur og velferd: Ein historisk-sosiologisk studie av norsk folkebibliotekpolitikk [Reason, culture and welfare: A historical-sociological study of Norwegian public library policy]. (Fil.Dr.), Goteborgs Universitet, Sweden.
          </li>
          <li id="vest1997b">Vestheim, G. (1997b). Libraries as agents of the public sphere: Pragmatism contra social responsibility. In N. W. Lund (Ed.), Nordic Yearbook of Library, Information, and Documentation Research (Vol. 1, pp. 115-124). Oslo, Norway: Novus.
          </li>
          <li id="web1995">Webster, F. (1995). Theories of the information society. New York, NY: Routledge.
          </li>
          <li id="wid2015">Widdersheim, M. M. &amp; Koizumi, M. (2015). Signal architectures of US public libraries: Resolving legitimacy between public and private spheres. Paper presented at the ASIS&amp;T Annual Meeting, St. Louis, MO. https://www.asist.org/files/meetings/am15/proceedings/submissions/papers/50paper.pdf (Archived by WebCiteî&nbsp;¡t http://www.webcitation.org/6lJsSvzmO)
          </li>
          <li id="wid2016">Widdersheim, M. M. &amp; Koizumi, M. (2016). Conceptual modelling of the public sphere in public libraries. Journal of Documentation, 72(3).
          </li>
        </ul>
        <h3 style="text-align:center;">
          How to cite this paper
        </h3>
        <div class="citing">
          Widdersheim, M. M. &amp; Koizumi, M. (2016). Methodological frameworks for developing a model of the public sphere in public libraries. In <em>Proceedings of the Ninth International Conference on Conceptions of Library and Information Science, Uppsala, Sweden, June 27-29, 2016</em> <em>Information Research</em>, <strong>22</strong>(1) paper colis1643. Retrieved from http://InformationR.net/ir/22-1/colis/colis1643.html. (Archived by WebCite® at http://www.webcitation.org/6oJgY9u1d)
        </div>
      </section>
    </article>
    
    <br>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    
    <hr>
    
    <div style="text-align:center;">
      Check for citations, <a href="http://scholar.google.co.uk/scholar?hl=en&amp;q=http://informationr.net/ir/22-1/colis/colis1643.html&amp;btnG=Search&amp;as_sdt=2000">using Google Scholar</a>
    </div><br>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <div class="addthis_inline_share_toolbox" style="text-align:center;"></div>
    <hr>
    <table class="footer" style="padding:10;">
      <tr>
        <td style="text-align:center; vertical-align:top;">
          <br>
          <div>
            <a href="http://www.digits.net" target="_blank"><img src="http://counter.digits.net/?counter={e712c6ef-1ed8-3f84-9dc5-cf5fe14e3c88}&amp;template=simple" alt="Hit Counter by Digits"></a>
          </div>
        </td>
        <td class="footer" style="text-align:center; vertical-align:middle;">
          <div>
            © the author, 2017.<br>
            Last updated: 9 December, 2017
          </div>
        </td>
        <td style="text-align:center; vertical-align:middle;">
          &nbsp;
        </td>
      </tr>
    </table>
    <footer>
      <hr>
      
      <hr>
    </footer><!-- Go to www.addthis.com/dashboard to customize your tools -->
  </body>
</html>
