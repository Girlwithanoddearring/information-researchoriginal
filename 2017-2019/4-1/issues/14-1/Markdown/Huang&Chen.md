#### vol. 14 no. 1, March, 2009

* * *

# Information synergy as the catalyst between IT capability and innovativeness: empirical evidence from the financial service sector

#### [Yuan-Ho Huang](mailto:yuanho@lins.fju.edu.tw)  
Department of Library and Information Science, Fu Jen Catholic University, Hsinchuang , Taipei County, Taiwan

#### [Eldon Y. Li](mailto:eli@calpoly.edu)  
College of Commerce, National Chengchi University, Taipei, Taiwan

#### [Ja-Shen Chen](mailto:jchen@saturn.yzu.edu.tw)  
Department of Business Administration, Yuan Ze University, Chung Li, Taiwan

#### Abstract

> **Introduction.** Previous studies about the effect of information technology (IT) on firm performance have presented no conclusive evidence. In this study, we examine the effect of IT on a firm's innovativeness and introduce information synergy as the catalyst between them. Information synergy is the state of a company in which individuals pool their resources and collaborate across roles or boundaries through information technologies  
> **Method.** A research framework and the associated hypotheses are proposed. An empirical survey was conducted and questionnaires were mailed to 400 financial firms in Taiwan.  
> **Analysis.** A total of 76 valid observations was collected and analysed using structural equation modeling technique with partial least square analysis.  
> **Results.** The empirical result shows that IT capability does not have a direct effect on innovativeness; it has an indirect effect through information synergy and accounts for 32.3% of the variance in information synergy, while IT capability and information synergy account for 74% of the variance in innovativeness underlying the structural model.  
> **Conclusions.** Information synergy is the key to improving a firm's performance as represented by the level of innovativeness. Regardless of how large the size of IT investment is, what really matters is how well the IT is being used for sharing timely information and making the right business decisions.

## Introduction

In today's business environment, information technology (IT) is playing an active role in creating competitive advantage for financial service companies. For example, e-brokerage, such as [Schwab.com](https://www.schwab.com/public/schwab/home/welcomep.html) and [E-Trade.com](https://us.etrade.com/e/t/home), has become a new business model that makes IT services an important strategic business unit in companies. ([Rayport and Jaworski 2001](#ray01)). Recently, the emerging platforms of social networking sites, such as [Facebook](http://en-gb.facebook.com/) and [MySpace](http://www.myspace.com/), allow people to make friends, connect with each other to share music and photos, and build a global village in cyberspace. Because of the popularity of Facebook, Standard Chartered Bank is piloting the use of Facebook for internal use, attempting to improve productivity and communications ([Charman-Anderson 2008](#cha08)).

Studies of IT in services reveal that standardized performance measures are hard to establish and the relationship between IT investment and firm performance is not conclusive ([Brynjolfsson 1993](#bry93); [Harris and Katz 1989](#har89)). Many previous studies (e.g., [Dehning _et al._ 2005](#deh05); [Sircar _et al._ 2000](#sir00); [Thatcher and Oliver 2001](#tha01)) have tried to find the relationship between IT investment and firm performance and productivity. Another study ([Shu and Strassmann 2005](#shu05)) shows that IT investment yields the highest profit margin in the banking industry. These studies underestimate some complex issues, such as obsolescence and idleness of IT resources which consume much of the investment without contributing any value. Thus, rather than exploring the effect derived from IT investment, we focus on the value-creating intangible issues of IT capability, such as process effectiveness, IT experience and value innovation.

When studying firm performance in the service sector, it is difficult to find a perfect measure. For example, Subramanian and Nilakanta ([1996](#sub96)) define firm performance as the share of deposits and the return on assets. They reveal that a firm having higher innovativeness tends to have higher organizational performance, using 350 banks in the mid-west region of the United States. Bank of America defined customer satisfaction and revenues as the performance measures. It created an 'innovation market' within the bank's existing network and ran a series of formal experiments aimed at creating new service concepts for retail banking. The project was able to meet the final goal of improving the two performance measures ([Thomke 2003](#tho03)). Tuominen _et al._ ([2003](#tuo03)) proposed a contingency model assessing innovativeness through organizational adaptability. They regarded innovativeness as a pre-performance resource and an intermediate factor for financial performance. Recently, an empirical study conducted in east Germany has confirmed a positive relationship between the innovativeness and firm performance in high-technology manufacturers ([Eickelpasch _et al._ 2007](#eic07)). Based on these studies, we propose adopting the level of innovativeness as a plausible predictor of various performance outcomes, tangible or intangible.

Similar to the relationship between IT investment and firm performance, it is difficult to study the direct effect of IT capability on firm innovativeness in the service sector; for example, in financial service companies. The reason might be that there are other mediating effects between IT capability and firm innovativeness. One source of such mediating effects might come from information synergy in the company ([Li, Chen and Huang 2006](#li06)). By information synergy, we mean the state of a company in which individuals or subunits pool their resources and collaborate across roles or subunit boundaries through information technologies ([Dewett and Jones 2001](#dew01)). In this study, we shall investigate this mediating effect between IT capability and firm innovativeness.

In summary, the purpose of this study is to advance understanding of the relationship among IT capability, information synergy and innovativeness in the financial service sector and conduct the empirical data analysis to a verified partial framework proposed by Li _et al._ ([2006](#li06)). Specifically, the research has three purposes: 1) to identify and develop the constructs of IT capability, information synergy and innovativeness; 2) to explore possible relations among the three constructs; 3) to develop and test a model that depicts the effect of IT capability on innovativeness and the mediating effect of information synergy. In the remaining sections we first present literature reviews and the hypotheses based on the existing literature. Second, we propose a research model of this study. Third, we describe the development of the three constructs and validate them using data collected from financial service companies. Fourth, we test the model using structural equation modeling and discuss the results. Finally, we present implications, limitations and future research.

## Literature review and research model

In the following section, we review the pertinent literature and propose a research model with formative indicators. We also identify components for each construct in the research model and posit three hypotheses regarding the relationships among IT capability, information synergy and innovativeness.

### Information technology capability

Li, Chen and Huang ([2006](#li06)) have conducted a thorough discussion about the essence of information technology capability ([Bharadwaj 2000](#bha00); [Ross, Beath and Goodhue 1996](#ros96)), which is also known as 'IT competency' ([Tippins and Sohi 2003](#tip03)). In this study, we adopt the term 'IT capability'.

According to the model proposed by Peppard and Ward ([2004](#pep04)), three levels linking information systems capability with competencies and resources are: resource level, organizing level and enterprise level. The resource level indicates the main resource components that compose the competencies. The organizing level is about how these resources are organized to create competencies. The enterprise level is how the capability demonstrates itself and is conceived in the performance of the organization. In this study, we adopt the enterprise level to denote our information technology construct, therefore, we apply IT capability as our main concept.

Tippins and Sohi ([2003](#tip03)) proposed three dimensions of IT capability, which are knowledge, operations and objects. This classification not only encompasses the tangible and intangible elements (the objects) of IT capability, but also introduces the operations and knowledge work. Therefore, we adopt these three dimensions as our main components of IT capability. These three dimensions demonstrate co-specialized resources that firms cannot utilize the information technology objects effectively without sufficient knowledge and operations.

### Information Synergy

Li _et al._ ([2006](#li06)) have elucidated the terms 'synergy' and 'information synergy'. In this study, we define information synergy as 'an environment where corporate members tend to disseminate and share information willingly and collaborate with each other across subunit boundaries, forming a harmonious organizational climate to generate synergy through the power of unity'. According to this definition, we view information synergy as a formative construct with three sub-constructs: information dissemination, information responsiveness and shared interpretation. These sub-constructs were discussed in Li _et al._ ([2006](#li06)).

### Innovativeness

Although Li _et al._ ([2006](#li06)) have presented several definitions of innovation, different scholars have inconsistent viewpoints, making the operationalization of the term hard to proceed with. Therefore, instead of directly adopting the concept of innovation which emphasizes the tangible outcome, we use the concept of innovativeness in this study. In this study, we view innovativeness as 'the innovative climate existing in a firm and the tendency to develop new products or services'. We consider innovativeness as a pre-performance factor and regard it as the most important indicator of future performance, be it tangible or intangible. Instead of using financial firm performance indices, such as return on investments and return on assets, we believe that innovativeness can tell us more about a firm's potential success.

Based on the existing literature, we have derived a reflective construct of innovativeness with four sub-constructs: product, process, personnel and service. The definitions of these four sub-constructs are defined in Li _et al._ ([2006](#li06)). In contrast to that study, we did not adopt the sub-construct of technology innovativeness because there are very few technologies other than information technology adopted in financial service sector.

### Information technology capability and information synergy

Information technology platforms can be a good coordination mechanism; for example, the enterprise information portal, document management system, knowledge community, collaboration system, or e-learning system. The implementation and use of these mechanisms can promote values of coordination and partnering ([Li _et al._ 2006](#li06)). This, in turn, facilitates the creation of information synergies. For example, InsynQ Inc., a provider of online software applications and services to financial organizations, introduces online accounting systems that allow users to share files and screens over the Web. Using these systems, financial service companies can enhance their technology capability and gain the advantage to pass money, save time and improve services to clients, thereby attracting more business ([Camp 2005](#cam05)). Another example is the use of multiple integrated channels to ensure the success of e-banking adoption by a UK bank, The Woolwich. ([Sah and Siddiqui 2006](#sah06)). Tippins and Sohi ([2003](#tip03)) reveal that IT knowledge, operations and objects are significantly related to information dissemination and shared interpretation. All this evidence supports the relationship between IT capability and information synergy. Hence, we propose the following hypothesis which is consistent with the second proposition in Li _et al._ ([2006](#li06)).

> Hypothesis 1: IT capability is positively related to information synergy.

### Information synergy and innovativeness

The essential issue of synergy can be examined by the extent of integration between functional areas and multiple information channels ([Rowley 2002](#row02)). In this examination, coordination is a vital element of synergy between groups. The more the firms coordinate, the higher information synergy the firms can acquire, which, in turn, helps to promote innovativeness between external (customers, partners) and internal (departments, divisions) business units of an organization. Furthermore, Canada's best bankers demonstrated that synergy between banker and client, owner and employees is crucial ([Hatter 1995](#hat95)). Therefore, we propose the following hypothesis which is consistent with the fourth proposition in Li _et al._ ([2006](#li06)).

> Hypothesis 2: Information synergy has a positive effect on innovativeness.

### Information technology capability and innovativeness

As firms integrate IT in their operations by re-engineering their intra-organizational and inter-organizational business processes, a rich communication and synergy will develop between business partners ([Raymond and Blili 2000-2001](#ray00)). Only after the effect of information synergy is brought into these collaborative environments can one bring about creative thinking, efficiency and effectiveness in an innovation process. In his recent book, Friedman declared that 'the world is flat', because IT, as a powerful 'flattener', has made everything of value connected and 'created a flat world: a global, web-enabled platform for multiple forms of sharing knowledge and work, irrespective of time, distance, geography and increasingly, language' ([Friedman 2005](#fri05)). In this flattened world, common software platforms and open source code enable global collaboration and give rise to outsourcing, offshoring, supply chaining and insourcing. Therefore, China and India become the outposts of financial services innovation ([Sraeel 2006](#sra06)). This phenomenon is a metaphor of information synergy of a service company wherein every piece of valuable information is communicated and shared, as are the opinions and the decision process. This environment greatly facilitates stimulating and sharing innovations. Hence, we propose the following hypothesis:

> Hypothesis 3: The effect of IT capability on innovativeness is mediated by information synergy.

### Research model

Based on the literature reviewed in the previous section, we present a research model as shown in Figure 1\. The model shows the firm's innovativeness is affected by IT capability through information synergy. Furthermore, we posit the construct of information synergy be formative based on three criteria ([Jarvis, Mackenzie and Podsakoff 2003](#jar03)): (1) the direction of causality is from indicators to constructs; (2) the indicators need not be interchangeable; and (3) the covariation among indicators is not necessary.

<figure>

![Figure 1](../p394fig1.png)

<figcaption>

**Figure 1\. Research model**</figcaption>

</figure>

## Research methodology

### Scale items development

Considering the research model depicted in Figure 1, a three-part questionnaire was developed (See [Appendix A](#app)). Some of the scales were adapted from previous literature; others were developed for this study. While the measures of IT capability were adapted from Tippins and Sohi ([2003](#tip03)), the measures of innovativeness were adapted from Garcia and Calantone ([2002](#gar02)), Subramanian and Nilakanta ([1996](#sub96)) and Totterdell et al. ([2002](#tot02)).

Although the original concept of information synergy was introduced by Dewett and Jones ([2001](#dew01)), we proposed a new definition of information synergy which includes information dissemination, information responsiveness and shared interpretation. The concepts of these components were adapted from Tippins and Sohi ([2003](#tip03)), Gefen and Riding ([2002](#gef02)) and Ridings, Gefen and Arinze ([2002](#rid02)), respectively.

In order to improve the content validity of the questionnaire, we also did a preliminary interview with some experts consisting of three academics and four practitioners. Table 1 summarizes the sources of every measurement item. The subjects were asked to indicate the extent to which they agreed with the condition described by the questionnaire item on a 5-point Likert-type scale, ranging from 1 (strongly disagree) to 5 (strongly agree). Therefore, the data collected by this study are self-reported scores that represent the perception of the respondents toward the conditions of the questionnaire items.

<table><caption>

**Table 1: Sources of questionnaire items**</caption>

<tbody>

<tr>

<td colspan="2">

**Research variable**</td>

<td>

**No. of items**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>

ITcapability<sup>a</sup></td>

<td>IT knowledge IT operations IT objects</td>

<td>15</td>

<td>Tippins and Sohi 2003.</td>

</tr>

<tr>

<td rowspan="3">

Information synergy<sup>b</sup></td>

<td>Information dissemination</td>

<td>3</td>

<td>Tippins and Sohi 2003.</td>

</tr>

<tr>

<td>Information responsiveness</td>

<td>4</td>

<td>

Gefen and Ridings 2002;  
Ridings, Gefen and Arinze 2002.</td>

</tr>

<tr>

<td>Shared interpretation</td>

<td>2</td>

<td>Tippins and Sohi 2003.</td>

</tr>

<tr>

<td rowspan="4">Innovativeness<sup>b</sup></td>

<td>Product</td>

<td>3</td>

<td>

Garcia and Calantone 2002;  
Subramanian and Nilakanta 1996;  
Totterdell, et al. 2002</td>

</tr>

<tr>

<td>Process</td>

<td>5</td>

<td>

Garcia and Calantone 2002;  
Subramanian and Nilakanta 1996.</td>

</tr>

<tr>

<td>Personnel</td>

<td>5</td>

<td>

Totterdell, _et al_. 2002.</td>

</tr>

<tr>

<td>Service</td>

<td>3</td>

<td>Garcia and Calantone 2002.</td>

</tr>

<tr>

<td colspan="2">Total number of items</td>

<td>40</td>

<td></td>

</tr>

<tr>

<td colspan="4">

<sup>a</sup> Questionnaire items are mainly adopted from Tippins and Sohi (2003)  
<sup>b</sup> Components are newly constructed and questionnaire items are adapted from different sources.</td>

</tr>

</tbody>

</table>

### Data collection and sample profile

Data were collected from a sample of 400 financial service firms (including banks, investments, insurance and trading companies) randomly selected from a directory published in 2005 by the Joint Credit Information Center in Taiwan. The questionnaire was sent to the IT department manager in each sample company via regular postal mail. On the questionnaire, we also provided the URL of the Website from which the online version of the questionnaire is available. Therefore, the respondent could choose to reply by filling out either the printed questionnaire or the online questionnaire via the Internet. After eight weeks and several follow-up telephone calls urging recipients to complete their surveys, a total of 76 questionnaires were received, giving a 19% response rate. Of the 76 responses, 30 were received from our first wave of mailing while 46 were received after the follow-up calls. To check for the non-response bias, we conducted a series of t-tests to compare the first-wave and the second-wave respondents in terms of demographic characteristics and model variables. None of the t-tests of the means revealed any significant difference between these two groups of respondents, indicating insignificant non-response bias.

Table 2 provides a summary of the characteristics of the firms responding to the questionnaire. About a quarter of the sample companies are large companies which have over 300 million U.S. dollars in capital and were established over twenty-five years ago. The ratios of new services over company sales in most (73.7%) companies are below 20%. Besides, in most (82.9%) companies the value of IT investments is less than 10% of their budget. The gap in IT outsourcing ratios between different companies is wide. About one third (30.3%) of companies have their IT outsourcing ratios below 5%, while the ratios in some other companies (18.4%) are over 40%.

<table><caption>

**Table 2: Demographic profiles of the companies**</caption>

<tbody>

<tr>

<td colspan="6">(N=76)</td>

</tr>

<tr>

<td>

**Characteristics**</td>

<td>N</td>

<td>%</td>

<td>

**Characteristics**</td>

<td>N</td>

<td>%</td>

</tr>

<tr>

<td colspan="3">

_**Number of company employees**_</td>

<td colspan="3">

_**Capital of company (U.S. dollars)**_</td>

</tr>

<tr>

<td>100 or less</td>

<td>23</td>

<td>30.3</td>

<td>Less than 3 million</td>

<td>7</td>

<td>9.2</td>

</tr>

<tr>

<td>101 to 500</td>

<td>24</td>

<td>31.6</td>

<td>3 million to 15 million</td>

<td>24</td>

<td>31.6</td>

</tr>

<tr>

<td>501 to 1000</td>

<td>8</td>

<td>10.5</td>

<td>15 million to 30 million</td>

<td>8</td>

<td>10.5</td>

</tr>

<tr>

<td>1001 to 2000</td>

<td>6</td>

<td>7.9</td>

<td>30 million to 150 million</td>

<td>14</td>

<td>18.4</td>

</tr>

<tr>

<td>over 2000</td>

<td>13</td>

<td>17.1</td>

<td>150 million to 300 million</td>

<td>5</td>

<td>6.6</td>

</tr>

<tr>

<td>no response</td>

<td>2</td>

<td>2.6</td>

<td>over 300 million</td>

<td>17</td>

<td>22.4</td>

</tr>

<tr>

<td colspan="3">  
</td>

<td>no response</td>

<td>1</td>

<td>1.3</td>

</tr>

<tr>

<td colspan="3">

_**Years company has been established**_</td>

<td colspan="3">

_**The ratio of new product/service over company sales**_</td>

</tr>

<tr>

<td>Less than 5 years</td>

<td>2</td>

<td>1.3</td>

<td>Less than 5%</td>

<td>26</td>

<td>34.2</td>

</tr>

<tr>

<td>6 to 10 years</td>

<td>16</td>

<td>21.1</td>

<td>6 to 10%</td>

<td>16</td>

<td>21.1</td>

</tr>

<tr>

<td>11 to 15 years</td>

<td>27</td>

<td>35.5</td>

<td>11 to 19%</td>

<td>14</td>

<td>18.4</td>

</tr>

<tr>

<td>16 to 20 years</td>

<td>6</td>

<td>7</td>

<td>20 to 29%</td>

<td>6</td>

<td>7.9</td>

</tr>

<tr>

<td>21 to 25 years</td>

<td>2</td>

<td>2.6</td>

<td>30 to 39%</td>

<td>3</td>

<td>3.9</td>

</tr>

<tr>

<td>over 26 years</td>

<td>22</td>

<td>28.9</td>

<td>over 40%</td>

<td>3</td>

<td>3.9</td>

</tr>

<tr>

<td>no response</td>

<td>1</td>

<td>1.3</td>

<td>no response</td>

<td>8</td>

<td>10.5</td>

</tr>

<tr>

<td colspan="3">

_**The ratio of IT outsourcing**_</td>

<td colspan="3">

_**The ratio of IT investment**_</td>

</tr>

<tr>

<td>Below 5%</td>

<td>23</td>

<td>30.3</td>

<td>Below5%</td>

<td>39</td>

<td>51.3</td>

</tr>

<tr>

<td>6-10%</td>

<td>9</td>

<td>11.8</td>

<td>6-10%</td>

<td>24</td>

<td>31.6</td>

</tr>

<tr>

<td>11-19%</td>

<td>6</td>

<td>7.9</td>

<td>11-19%</td>

<td>3</td>

<td>3.9</td>

</tr>

<tr>

<td>20-29%</td>

<td>10</td>

<td>13.2</td>

<td>20-29%</td>

<td>5</td>

<td>6.6</td>

</tr>

<tr>

<td>30-39%</td>

<td>10</td>

<td>13.2</td>

<td>30-39%</td>

<td>2</td>

<td>2.6</td>

</tr>

<tr>

<td>over 40%</td>

<td>14</td>

<td>18.4</td>

<td>over 40%</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>no response</td>

<td>4</td>

<td>5.3</td>

<td>no response</td>

<td>3</td>

<td>3.9</td>

</tr>

</tbody>

</table>

## Data analysis and results

To measure the validity of our construct and test the proposed hypotheses, we conducted data analyses using Partial Least Squares (PLS) based on the research model in Figure 1\. PLS is a powerful equation modeling technique and can be tested with a sample size as small as five times the number of measurement items ([Chin 1998a](#chi98a); [Chin and Newsted 1999](#chi99)). In addition, the ability of PLS to model formative as well as reflective constructs makes it applicable for this study. In this model, there are several sub-constructs of each construct. Under this condition, Bollen ([1989](#bol89)) recommends the use of the two-step rule. The rule separates a structural equation model into two models: the measurement model and the structural model ([Herting and Costner 2001](#her01)). The measurement model defines the constructs (latent variables) that the model will use and assigns observed variables to each one. The structural model then defines the causal relationship among these latent variables ([Gefen, Straub and Boudreau 2000](#gef00)). Therefore, our analysis was conducted in two steps. First, we performed the testing and refining of our measurement model, which included confirmatory factor analysis, checking for cross loadings, reliability coefficients of the constructs and discriminant validity. In the second step, we regarded these latent variables which were validated at the first step, as the observable variables and then constructed the structural model based on our research model. Therefore, we used PLS to analyze the structural model consisting of ten composite indicators with 76 collected samples. This sample size is five times larger than the number of composite indicators, which meets the sample size requirement of PLS analysis.

In order to assess the psychometric properties of the measurement scales, the following sections present the tests of reliability, convergent validity, content validity and discriminant validity.

### Scale Reliability and Convergent Validity

The typical approach to reliability assessment is the Cronbach α coefficient, which ranges from 0 to 1\. As shown in Table 3, all the Cronbach α values are above 0.7 which are acceptable for capturing the dimensions ([Nunnally 1978](#nun78)).

<table><caption>

**Table 3: Assessment of consistency reliability**</caption>

<tbody>

<tr>

<td>

**Dimension**</td>

<td>

**No. of items**</td>

<td>

**Reliability coefficient**</td>

</tr>

<tr>

<td colspan="3">

_**IT capability**_</td>

</tr>

<tr>

<td>IT knowledge (ITKNO)</td>

<td>3</td>

<td>0.8952</td>

</tr>

<tr>

<td>IT operations (ITOPE)</td>

<td>4</td>

<td>0.8893</td>

</tr>

<tr>

<td>IT objects (ITOBJ)</td>

<td>5</td>

<td>0.9193</td>

</tr>

<tr>

<td colspan="3">

_**Information synergy**_</td>

</tr>

<tr>

<td>Information dissemination (I_DISS)</td>

<td>3</td>

<td>0.8516</td>

</tr>

<tr>

<td>Information responsiveness (I_RESP)</td>

<td>4</td>

<td>0.8215</td>

</tr>

<tr>

<td>Shared interpretation (SHR_I)</td>

<td>2</td>

<td>0.8704</td>

</tr>

<tr>

<td colspan="3">

_**Innovativeness**_</td>

</tr>

<tr>

<td>Product (PRODU_INN)</td>

<td>2</td>

<td>0.8763</td>

</tr>

<tr>

<td>Process (PROCE_INN)</td>

<td>4</td>

<td>0.9248</td>

</tr>

<tr>

<td>Personnel (PERSON_INN)</td>

<td>4</td>

<td>0.8936</td>

</tr>

<tr>

<td>Service (SERV_INN)</td>

<td>2</td>

<td>0.9046</td>

</tr>

</tbody>

</table>

The composite reliability of the three constructs as shown in Table 4 exceeded the threshold of 0.7 recommended by Nunnally ([1978](#nun78)). Furthermore, the average variance extracted (AVE) of each sub-construct exceeded the 0.5 threshold ([Fornell and Larcker 1981](#for81)), indicating that 50% or more variance of the indicators should be accounted for, which shows satisfactory convergent validity.

<table><caption>

**Table 4: Test of Reliability, Convergent and Discriminant Validity**  
(Note: Please refer to Table 3 for the meanings of acronyms.)</caption>

<tbody>

<tr>

<td>  
</td>

<td>

**Mean**</td>

<td>

**SD**</td>

<td>

**ITKNO**</td>

<td>

**ITOPE**</td>

<td>

**ITOBJ**</td>

<td>

**I_DISS**</td>

<td>

**I_RESP**</td>

<td>

**SHR_I**</td>

<td>

**PRODU_INN**</td>

<td>

**PROCE_INN**</td>

<td>

**PERSON_INN**</td>

<td>

**SERV_INN**</td>

<td>

**Composite<sup>a</sup> Reliability**</td>

</tr>

<tr>

<td>

**ITKNO**</td>

<td>3.55</td>

<td>0.84</td>

<td>

**0.91<sup>b</sup>**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>0.94</td>

</tr>

<tr>

<td>

**ITOPE**</td>

<td>3.73</td>

<td>0.80</td>

<td>0.67</td>

<td>

**0.87**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>0.93</td>

</tr>

<tr>

<td>

**ITOBJ**</td>

<td>3.92</td>

<td>0.89</td>

<td>0.61</td>

<td>0.71</td>

<td>

**0.90**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>0.95</td>

</tr>

<tr>

<td>

**I_DISS**</td>

<td>3.78</td>

<td>0.69</td>

<td>0.52</td>

<td>0.51</td>

<td>0.30</td>

<td>

**0.88**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>0.91</td>

</tr>

<tr>

<td>

**I_RESP**</td>

<td>3.63</td>

<td>0.82</td>

<td>0.37</td>

<td>0.28</td>

<td>0.14</td>

<td>0.73</td>

<td>

**0.83**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>0.90</td>

</tr>

<tr>

<td>

**SHR_I**</td>

<td>3.69</td>

<td>0.68</td>

<td>0.41</td>

<td>0.55</td>

<td>0.38</td>

<td>0.66</td>

<td>0.55</td>

<td>

**0.94**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>0.94</td>

</tr>

<tr>

<td>

**PRODU_INN**</td>

<td>3.15</td>

<td>0.89</td>

<td>0.52</td>

<td>0.55</td>

<td>0.36</td>

<td>0.68</td>

<td>0.54</td>

<td>0.60</td>

<td>

**0.88**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

<td>0.92</td>

</tr>

<tr>

<td>

**PROCE_INN**</td>

<td>3.21</td>

<td>0.77</td>

<td>0.50</td>

<td>0.60</td>

<td>0.41</td>

<td>0.75</td>

<td>0.57</td>

<td>0.70</td>

<td>0.82</td>

<td>

**0.89**</td>

<td>  
</td>

<td>  
</td>

<td>0.96</td>

</tr>

<tr>

<td>

**PERSON_INN**</td>

<td>3.58</td>

<td>0.75</td>

<td>0.53</td>

<td>0.54</td>

<td>0.39</td>

<td>0.80</td>

<td>0.68</td>

<td>0.72</td>

<td>0.74</td>

<td>0.81</td>

<td>

**0.82**</td>

<td>  
</td>

<td>0.91</td>

</tr>

<tr>

<td>

**SERV_INN**</td>

<td>3.36</td>

<td>0.86</td>

<td>0.48</td>

<td>0.46</td>

<td>0.34</td>

<td>0.70</td>

<td>0.55</td>

<td>0.67</td>

<td>0.71</td>

<td>0.76</td>

<td>0.83</td>

<td>

**0.92**</td>

<td>0.95</td>

</tr>

<tr>

<td colspan="14">

<sup>a</sup> Composite reliability is estimated using (Σλy<sub>i</sub>)<sup>2</sup>/&#91;(Σλy<sub>i</sub>)<sup>2</sup>+(Σε<sub>i</sub>)&#93;; where λy<sub>i</sub>is the standardized loading for scale item y<sub>i</sub>and ε<sub>i</sub> is the measurement error for thescale item y<sub>i</sub>.</td>

</tr>

<tr>

<td colspan="14">

<sup>b</sup> Average variance extracted is estimated using Σλy<sub>i</sub><sup>2</sup>/(Σλy<sub>i</sub><sup>2</sup>+Σε<sub>i</sub>) (Fornell and Larcker 1981.) The diagonal elements represent the square root of the AVEs of the construct, while the other matrix elements represent the correlations with other latent constructs.</td>

</tr>

</tbody>

</table>

### Content validity and discriminant validity

Content validity was established through several preliminary interviews with seven experts. As shown in Table 4, the correlations between any two constructs was lower than the corresponding square root of AVEs of the constructs, indicating that each scale was measuring a construct that was significantly different from all other constructs, thus establishing discriminant validity ([Fornell and Larcker 1981](#for81)).

### Summary statistics and correlation analysis

After confirming the validity of data and model, we began to perform data analyses for the three research variables using the composite average of the sub-construct scores as the construct's score. Similarly, the sub-construct score was the composite average of the item scores. The summary statistics and the correlations of the research variables, based on the composite average scores, are shown in Table 5\. The scores of the three variables are plotted in Figure 2, in sequence of the values of information-synergy scores. A scrutiny of Figure 2 reveals that most of the IT capability scores are higher than the information synergy scores, while most of the innovativeness scores are lower. The scores of IT capability and innovativeness are mostly far apart. Table 5 also shows that the correlation coefficient of information synergy and innovativeness is the highest (.823) among the three paired variables, followed by IT capability and innovativeness (.582) and information synergy and IT capability (.485). The three t-tests of paired variables indicate that the differences of all three pairs of means were significant at p<0.001. The highest mean difference is between IT capability and innovativeness (0.4091), followed by information synergy and innovativeness (0.3765) and then information synergy and IT capability (0.0326). All these findings confirm our argument about information synergy being a better indicator than IT capability for innovativeness.

<table><caption>

**Table 5: Summary statistics and correlations of research variables**</caption>

<tbody>

<tr>

<td>

**Variable**</td>

<td>

**Description**</td>

<td>

**Summary statistics**</td>

<td>

**IT capability**</td>

<td>

**Information synergy**</td>

<td>

**Innovativeness**</td>

</tr>

<tr>

<td rowspan="5">

**IT capability**</td>

<td>N</td>

<td>76</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

</tr>

<tr>

<td>Mean</td>

<td>3.7327</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Standard deviation</td>

<td>0.74411</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Pearson correlation  
</td>

<td> </td>

<td>1.000</td>

<td>0.485<sup>a</sup>  
</td>

<td>0.582<sup>a</sup>  
</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td>0.000</td>

<td>0.000</td>

</tr>

<tr>

<td rowspan="5">

**Information synergy**</td>

<td>N</td>

<td>76</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

</tr>

<tr>

<td>Mean</td>

<td>3.7000</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Standard deviation</td>

<td>0.6368</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Pearson correlation  
</td>

<td> </td>

<td>0.485<sup>a</sup>  
</td>

<td>1.000</td>

<td>0.823<sup>a</sup>  
</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td> </td>

<td>0.000</td>

<td> </td>

<td>0.000</td>

</tr>

<tr>

<td rowspan="5">

**Innovativeness**</td>

<td>N</td>

<td>76</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

</tr>

<tr>

<td>Mean</td>

<td>3.3236</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Standard deviation</td>

<td>0.7451</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Pearson correlation  
</td>

<td> </td>

<td>0.582<sup>a</sup>  
</td>

<td>0.823<sup>a</sup>  
</td>

<td>1.000</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td> </td>

<td>0.000</td>

<td>0.000</td>

<td> </td>

</tr>

<tr>

<td colspan="6">

_<sup>a</sup> Correlation is significant at the 0.01 level (2-tailed)._</td>

</tr>

</tbody>

</table>

<figure>

![Figure 2](../p394fig2.png)

<figcaption>

**Figure 2\. Information synergy as the mediator**</figcaption>

</figure>

### The structural model

According to Bollen ([1989](#bol89)), once the measurement parameters are identified during the first step, the whole model is identified if the latent variable model parameters can be identified. Therefore, we conducted the second step of the structural modeling based on our research model. Figure 3 shows the path loadings between constructs, the standardized loadings of scale items within two reflective constructs (IT capability and Innovativeness) and the weights of scale items within one formative construct (Information synergy). The reflective variables (also called effect indicators) are reflected by the latent variables and should be highly correlated with each other. The formative variables (called cause indicators) could determine the latent variables and should not be correlated ([Chin 1998b](#chi98b); [Blalock 1964](#bla64)). Based on the t-statistics, all the sub-constructs have either high loadings or high weights significant at p<0.001 level, except from information responsiveness to information synergy. Other results show that IT capability accounts for 32.3% of the variance in information synergy, while IT capability and information synergy account for 74% of the variance in innovativeness underlying the structural model. Since the PLS method makes no prior distributional assumptions, traditional significance tests and estimation of confidence intervals for the path coefficients cannot be done. However, the bootstrapping and jack-knifed estimates of mean, standard errors and t-statistic can be used to test the significance of the structural coefficients ([Chin 1998a](#chi98a)).

<figure>

![Figure 3](../p394fig3.png)

<figcaption>

**Figure 3\. Structural equation modeling analysis result**</figcaption>

</figure>

Based on Baron and Kenny ([1986](#bar86)), the following conditions must be satisfied for the mediation test and the related test result is shown in Table 6\. First, the direct effect of the independent variable (IT capability) on the dependent variable (innovativeness) must be significant. Second, both the path from the independent variable (IT capability) to the mediator (information synergy) and the path from the mediator (information synergy) to the dependent variable (innovativeness) should be significant. Third, the introduction of a mediator will lower the path loading between the independent variable (IT capability) and dependent variable (innovativeness). Furthermore, if the path loading is not significant, the mediator has a fully-mediated effect; otherwise the effect is partially mediated. Therefore, our proposed model is a fully mediated model. The finding indicates that information synergy is an essential mediator; IT capability would not have a significant effect on innovativeness without information synergy. The results of three possible models are shown in Table 6.

<table><caption>

**Table 6: Results of structural equations analyses for direct effect, full mediation and partial mediation models**</caption>

<tbody>

<tr>

<td colspan="3" rowspan="2">

**Path**</td>

<td>

**Direct effect**</td>

<td>

**Full mediation**</td>

<td>

**Partial mediation**</td>

</tr>

<tr>

<td>

**standard path coefficient  
(t-value)**</td>

<td>

**standard path coefficient  
(t-value)**</td>

<td>

**standard path coefficient  
(t-value)**</td>

</tr>

<tr>

<td colspan="3">

**_<u>Hypothesized paths</u>_**</td>

<td>  
</td>

<td>  
</td>

<td>  
</td>

</tr>

<tr>

<td>IT capability</td>

<td>→</td>

<td>Innovativeness (H3)</td>

<td>0.581  
(8.2794)<sup>a</sup></td>

<td>  
</td>

<td>0.173  
(1.91)</td>

</tr>

<tr>

<td>IT capability</td>

<td>→</td>

<td>Information synergy (H1)</td>

<td>  
</td>

<td>0.581  
(8.2794)<sup>a</sup></td>

<td>0.569  
(7.8296)<sup>a</sup></td>

</tr>

<tr>

<td>Information synergy</td>

<td>→</td>

<td>Innovativeness (H2)</td>

<td>  
</td>

<td>0.849  
(24.445)<sup>a</sup></td>

<td>0.750  
(10.8451)<sup>a</sup></td>

</tr>

<tr>

<td colspan="3">R<sup>2</sup> (Information synergy)</td>

<td>  
</td>

<td>0.325</td>

<td>0.323</td>

</tr>

<tr>

<td colspan="3">R<sup>2</sup> (Innovativeness)</td>

<td>0.337</td>

<td>0.72</td>

<td>0.74</td>

</tr>

<tr>

<td colspan="6">

_<sup>a</sup> p<0.001 (t value)_</td>

</tr>

</tbody>

</table>

## Conclusions and implications

Prior study has revealed that IT investment has positive impact on a firm's output and productivity in the manufacturing sector but not in the service sector ([Brynjolfsson 1993](#bry93)). This phenomenon might be due to the fact that the amount of IT investment cannot represent the substance of IT value and that the effect of IT investment is predicated by how well IT is utilized for strategic gains. Consequently, in this study we focus on exploring the effect of IT capability instead of IT investment. We further propose the role of information synergy as the catalyst between IT capability and a firm's innovativeness. The empirical evidence of Italian banks suggests that the development of IT capability, such as creating an Intranet to serve as a repository and communication tool, can support the redefinition of the overall strategy of the bank. Furthermore, cultural integration of the branch network and a life-long training process have been conducted to sustain the banks' large scale network ([Canato and Corrocher 2004](#can04)). Despite the fact that the financial service industry is one of the early adopters of new information technologies, the effect of IT capability on firm performance is inconclusive in the service sector in general, which is contrary to its manufacturing counterpart ([Brynjolfsson 1993](#bry93)). Therefore, we chose the financial service sector as our target sample to evaluate our proposed mediation model and to verify two propositions from Li, Chen and Huang ([2006](#li06)). The empirical results of this study provide the following conclusions.

First, we considered the importance of intangible elements in the service sector and introduced the concepts of IT capability, information synergy and innovativeness. Our data analysis confirmed the reliability and validity of the construct of IT capability. Moreover, we modeled IT capability and innovativeness as reflective constructs and information synergy as a formative construct. The entire research model is divided into and analyzed with a measurement model and structural model. Then, we analyzed the entire research model with PLS software once the measurement model was validated.

Second, after reviewing the literature, we argued for the effect of information synergy and proposed information dissemination, information responsiveness and shared interpretation as its three main components. The reliability and validity of the construct of information synergy were supported by the analysis. However, the result of the structural model analysis shows a low weight of information responsiveness, which means that the composite construct of information responsiveness is not a good indicator for information synergy. That is, we should not pay much attention to the speed for transmitting information in the organization.

Third, we adopted innovativeness as the pre-performance measure of firm performance, instead of traditional financial performance indices, such as ROI and ROA. In addition, we proposed four types of innovativeness: product, process, personnel and service innovativeness. We have verified that this construct of innovativeness possesses reliability and validity.

Fourth, we verified the validity of introducing information synergy as the catalyst between IT capability and innovativeness. According to our correlation analysis, IT capability has a significant correlation with innovativeness. However, the PLS analysis shows that IT capability does not have a significant direct effect on innovativeness once information synergy is introduced into the model as a mediator. This suggests that information synergy is a better indicator of innovativeness.

The aforementioned findings and conclusions offer us several managerial implications. First of all, information synergy is the key to improving a firm's performance as represented by the level of innovativeness. Regardless of how large the size of IT investment is, what really matters is how well the IT is being used for sharing timely information and making the right business decisions. This is the condition where information synergy is achieved in a company. Therefore, information synergy is the lifeblood of firm performance; overlooking information synergy is likely to reduce the utility of IT capability and lower the level of innovativeness, which may eventually weaken the competitive advantage of the firm. In order to outperform its competitors, a firm must provide an environment for creating information synergy, instead of merely reinforcing their IT capability. One possible step toward information synergy is to implement an organization-wide knowledge management system, which involves knowledge creating, gathering, storing, communicating, synthesizing and disseminating activities.

Furthermore, this study provides valid instruments for measuring the constructs of IT capability, information synergy and innovativeness. These instruments are easy to administer. Both IT managers and business executives could periodically (e.g., quarterly or annually) perform self evaluations using these instruments. A trend chart could be plotted to track the changes. An item-by-item analysis could be performed to identify large decrements in the individual item scores and formulate actions to correct specific problems and improve the scores. This analysis could also help managers to prioritize action items and allocate appropriate resources to support the actions.

## Limitations and future research

Though we tried to conduct our research analysis as thoroughly as possible, there are still some limitations. First, the respondents to our questionnaire were IT managers, which might not represent all employees in the sampled companies. In addition, although we have ensured the absence of common method bias, we could have avoided such a bias by collecting responses from multilevel sources in this study. Second, we only chose financial service firms as our samples from the service sector; this might overlook important information from other industry sectors. Future research should collect sample data from not only financial service firms but also other service or manufacturing sectors. This, in turn, could help us generalize our conceptual research model and the findings of our analyses. Third, since the sample data is a snapshot of the firms' conditions, we might have neglected the importance of the dynamic aspect. A longitudinal study is needed to trace the dynamics of business activities as time goes by. This will give us more clues to refine our research model. Finally, in addition to information synergy that we proposed as the mediator between IT capability and innovativeness, there might be some other factors worth considering, such as organizational culture and climate, top management support, education and training, etc. The mediating effects of these factors on innovativeness should be investigated in the future.

## Acknowledgements

The authors acknowledge Prof. Wynne W. Chin of the University of Houston for his support of the PLS software and thank Associate Editor, Dr. Diane H. Sonnenwald and anonymous reviewers for their careful reading and helpful feedback.

## About the authors

**Yuan-Ho Huang** is an associate professor of the Department of Library and Information Science, Fu Jen Catholic University, Taiwan. She received her Ph.D. degree in the Division of e-commerce within the School of Management at Yuan Ze University in Taiwan and Master of library and information science in the School of Information Science at University of Pittsburgh in USA.

**Eldon Y. Li** is University Chair Professor and Director of Center for Service Innovation and Innovation Incubation Center at National Chengchi University in Taiwan. He was Professor and Dean of College of Informatics at Yuan Ze University in Taiwan, as well as a Professor and the Coordinator of MIS Program at the College of Business, California Polytechnic State University, San Luis Obispo, California, USA.

**Ja-Shen Chen** is currently a Professor and Dean of the College of Management, Yuan Ze University, Taiwan. He holds MS and PhD degrees from Rensselaer Polytechnic Institute, N.Y.

## References

*   <a id="bar86"></a>Baron, R.M., & Kenny, D.A. (1986). The moderator-mediator variable distinction in social psychological research: conceptual, strategic and statistical considerations. _Journal of Personality and Social Psychology_, **51**, 1173-1182.
*   <a id="bha00"></a>Bharadwaj, A.S. (2000). A resource-based perspective on information technology capability and firm performance: an empirical investigation. _MIS Quarterly_, **24**(1), 169-196.
*   <a id="bla64"></a>Blalock, H. M. (1964). Scanning the business environment for information: a grounded theory approach. _Causal inferences in nonexperimental research._ Chapel Hill, NC: University of North Carolina Press.
*   <a id="bol89"></a>Bollen, K.A. (1989). _Structural equations with latent variables._ New York, NY: John Wiley and Sons.
*   <a id="bry93"></a>Brynjolfsson, E. (1993). The productivity paradox of information technology. _Communications of the ACM_, **36**, 66-77.
*   <a id="cam05"></a>Camp, M. (2005). Computing trends firms can expect. _Accounting Technology_, **21**, 28.
*   <a id="can04"></a>Canato, A., & Corrocher, N. (2004). Information and communication technology: organizational challenges for Italian banks. _Accounting, Business and Financial History_, **14**(3), 355-370.
*   <a id="cha08"></a>Charman-Anderson, S. (2008). [Social networking stands to benefit business.](http://www.cio.co.uk/industry/finance/features/index.cfm?articleid=706&pn=1) _CIO Magazine_, Retrieved 15 December, 2008 from http://www.cio.co.uk/industry/finance/features/index.cfm?articleid=706&pn=1\. (Archived by WebCite® at http://www.webcitation.org/5ePkVb5DN)
*   <a id="chi98a"></a>Chin, W.W. (1998a). The partial least squares approach for structural equation modeling. In G. A. Marcoulides (Ed.) _Modern methods for business research_, (pp.295-336). Mahwah, NJ: Lawrence Erlbaum Associates.
*   <a id="chi98b"></a>Chin, W. W. (1998b). Issues and opinion on structural equation modeling. _MIS Quarterly_, **22**(1), 7-16.
*   <a id="chi99"></a>Chin, W.W., & Newsted, P.R. (1999). Structural equation modeling analysis with small samples using partial least squares. In R. Hoyle (Ed.) _Statistical strategies for small sample research_, (pp.307-341). Thousand Oaks, CA: Sage Publications.
*   <a id="deh05"></a>Dehning, B., Richardson, V.J., & Stratopoulos, T. (2005). Information technology investments and firm value. _Information and Management_, **42**, 989-1008.
*   <a id="dew01"></a>Dewett, T.G., & Jones, R. (2001). The role of information technology in the organization: a review, model and assessment. _Journal of Management_, **27**, 313-346.
*   <a id="eic07"></a>Eickelpasch, A., Lejpras, A., & Stephan, A. (2007). _[Hard and soft locational factors, innovativeness and firm performance: an empirical test of Porter's diamond model at the micro-level.](http://www.infra.kth.se/cesis/documents/WP109.pdf)_ Stockholm: Royal Institute of Technology, Centre of Excellence for Science and Innovation Studies. (CESIS Electronic Working Paper No. 109). Retrieved 15 December, 2008 from http://www.infra.kth.se/cesis/documents/WP109.pdf. (Archived by WebCite® at http://www.webcitation.org/5ePkilo7c)
*   <a id="for81"></a>Fornell, C.R., & Larcker, D.F. (1981). Structural equation models with unobservable variables and measurement error. _Journal of Marketing Research_, **18**, 39-50.
*   <a id="fri05"></a>Friedman, T.L. (2005). _The world is flat: a brief history of the twenty-first century_. New York: Farrar, Straus and Giroux.
*   <a id="gar02"></a>Garcia, R., & Calantone, R. (2002). A critical look at technological innovation typology and innovativeness terminology: a literature review. _Journal of Product Innovation Management_, **19**, 110-132.
*   <a id="gef02"></a>Gefen, D., & Ridings, C.M. (2002). Implementation team responsiveness and user evaluation of customer relationship management: a quasi-experimental design study of social exchange theory. _Journal of Management Information Systems_, **19**(1), 47-69.
*   <a id="gef00"></a>Gefen, D., Straub, D., & Boudreau, M. (2000). Structural equation modeling and regression: guidelines for research practice. _Communications of the Association of Information Systems_, **4**(7), 1-79.
*   <a id="har89"></a>Harris, S.E., & Katz, J.L. (1989). Predicting organizational performance using information technology managerial control ratios. _Proceedings of the Twenty-Second Hawaiian International Conference on System Science : emerging technologies and applications track_, (pp.197-204). LA, CA: IEEE Computer Society Press.
*   <a id="hat95"></a>Hatter, D. (1995). Canada's best bankers: we asked four banks to name their best and brightest small-business bankers. _Profit_, **14**(1), 52.
*   <a id="her01"></a>Herting, J.R., & Costner, H.L. (2001). Another perspective on the proper number of factors and the appropriate number of steps. _Structural Equation Modeling_, **7**(1), 92-110.
*   <a id="jar03"></a>Jarvis, C.B., Mackenzie, S.B., & Podsakoff, P.M. (2003). A critical review of construct of indicators and measurement model mis-specification in marketing and consumer research. _Journal of Consumer Research_, **30**(2), 199-218.
*   <a id="li06"></a>Li, E.Y., Chen, J.S., & Huang, Y.H. (2006). A framework for investigating the impact of IT capability and organizational capability on firm performance in the late industrializing context. _International Journal of Technology Management_, **36** (1,2,3), 209-229.
*   <a id="nun78"></a>Nunnally, J.C. (1978). _Psychometric theory_. (2nd. ed.). New York: McGraw-Hill.
*   <a id="pep04"></a>Peppard, J., & Ward, J. (2004). Beyond strategic information systems: toward an IS capability. _Journal of Strategic Information Systems_, **13**, 167-194.
*   <a id="ray00"></a>Raymond, L., & Blili, S. (2000-2001). Organizational learning as a foundation of electronic commerce in the network organization. _International Journal of Electronic Commerce_, **5**(2), 29-45.
*   <a id="ray01"></a>Rayport, J.F., & Jaworski, B.J. (2001). _Electronic commerce_. Boston: McGraw-Hill.
*   <a id="rid02"></a>Ridings, C. M., Gefen, D., & Arinze, B. (2002). Some antecedents and effects of trust in virtual communities. _Journal of Strategic Information Systems_, **11**, 271-295.
*   <a id="ros96"></a>Ross, J.W., Beath, C.M., & Goodhue, D.L. (1996). Develop long-term competitiveness through IT assets. _Sloan Management Review_, **38**, 31-45.
*   <a id="row02"></a>Rowley, J. (2002). Synergy and strategy in e-business. _Marketing Intelligence and Planning_, **20(4)**, 215-222.
*   <a id="sah06"></a>Sah, M.H., & Siddiqui, F.A. (2006). Organizational critical success factors in adoption of e-banking at the Woolwich bank. _International Journal of Information Management_, **26**, 442-456.
*   <a id="shu05"></a>Shu, W., & Strassmann, P.A. (2005). Does information technology provide banks with profit? _Information and Management_, **42**, 781-787.
*   <a id="sir00"></a>Sircar, S., Turnbow, J. L., & Bordoloi, B. (2000). A framework for assessing the relationship between information technology investments and firm performance. _Journal of Management Information Systems_, **16**(4), 69-97.
*   <a id="sra06"></a>Sraeel, H. (2006). In a flat world, everything of value is connected. _Bank Technology News_, **19**(4), 8.
*   <a id="sub96"></a>Subramanian, A., & Nilakanta, S. (1996). Organizational innovativeness: exploring the relationship between organizational determinants of innovation, types of innovations and measures of organizational performance. _Omega_, **24**(6), 631-647.
*   <a id="tha01"></a>Thatcher, M.E., & Oliver, J.R. (2001). The impact of technology investments on a firm's production efficiency, product quality and productivity. _Journal of Management Information Systems_, **18**(2), 17-45.
*   <a id="tho03"></a>Thomke, S. (2003). R & D comes to services: bank of America's pathbreaking experiments. _Harvard Business Review_, **81**(4), 71-79.
*   <a id="tip03"></a>Tippins, M.J., & Sohi, R.S. (2003). IT competency and firm performance: is organizational learning a missing link? _Strategic Management Journal_, **24**, 745-761.
*   <a id="tot02"></a>Totterdell, P., Desmond, L., Birdi, C., & Wall, T. (2002). An investigation of the contents and consequences of major organizational innovations. _International Journal of Innovation Management_, **6**(4), 43-368.
*   <a id="tuo03"></a>Tuominen, M., Rajala, A., Moller, K., & Anttila, M. (2003). Assessing innovativeness through organizational adaptability: a contingency approach. _International Journal of Technology Management_, **25**(6/7), 643-658.

* * *

## <a id="app">Appendix A: Final survey items</a>

<table><caption>

**IT capability**</caption>

<tbody>

<tr>

<td colspan="2">

**IT knowledge**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td><sup>*</sup>IT1_1</td>

<td>Overall, our technical support staff are knowledgeable when it comes to computer-based systems</td>

<td rowspan="4">Tippins and Sohi, 2003</td>

</tr>

<tr>

<td>IT1_2</td>

<td>Our firm possesses a high degree of computer-based technical expertise.</td>

</tr>

<tr>

<td>IT1_3</td>

<td>We are very knowledgeable about new computer-based innovations.</td>

</tr>

<tr>

<td>IT1_4</td>

<td>We have the knowledge to develop and maintain computer-based communication links with our customers.</td>

</tr>

<tr>

<td colspan="2">

**IT operations**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td><sup>*</sup>IT2_1</td>

<td>Our firm is skilled at collecting and analyzing market information about our customers via computer-based systems.</td>

<td rowspan="6">Tippins and Sohi, 2003</td>

</tr>

<tr>

<td><sup>*</sup>IT2_2</td>

<td>We routinely utilize computer-based systems to access market information from outside databases</td>

</tr>

<tr>

<td>IT2_3</td>

<td>We have set procedures for collecting customer information from online sources.</td>

</tr>

<tr>

<td>IT2_4</td>

<td>We use computer-based systems to analyze customer and market information.</td>

</tr>

<tr>

<td>IT2_5</td>

<td>We utilize decision-support systems frequently when it comes to managing customer information</td>

</tr>

<tr>

<td>IT2_6</td>

<td>We rely on computer-based systems to acquire, store and process information about our customers.</td>

</tr>

<tr>

<td colspan="2">

**IT objects**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>IT3_1</td>

<td>Our company has a formal MIS department.</td>

<td rowspan="5">Tippins and Sohi, 2003</td>

</tr>

<tr>

<td>IT3_2</td>

<td>Our firm employs a manager whose main duties include the management of our information technology.</td>

</tr>

<tr>

<td>IT3_3</td>

<td>Every year we budget a significant amount of funds for new information technology hardware and software</td>

</tr>

<tr>

<td>IT3_4</td>

<td>Our firm creates customized software applications when the need arises.</td>

</tr>

<tr>

<td><sup>*</sup>IT3_5</td>

<td>Our firm’s members are linked by a computer network</td>

</tr>

</tbody>

</table>

<table><caption>

**Information synergy**</caption>

<tbody>

<tr>

<td colspan="2">

**Information dissemination**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>ID1</td>

<td>Within our firm, sharing customer information is the norm</td>

<td rowspan="3">Tippins and Sohi, 2003</td>

</tr>

<tr>

<td>ID2</td>

<td>Within our firm, information about our customers is easily accessible to those who need it most.</td>

</tr>

<tr>

<td>ID3</td>

<td>Representatives from different departments within our firm meet regularly to discuss the customers? needs.</td>

</tr>

<tr>

<td colspan="2">

**Information responsiveness**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>IR1</td>

<td>When one department obtains important information about our customers, it is circulated to other departments.</td>

<td rowspan="3">Ridings, Gefen and Arinze, 2002</td>

</tr>

<tr>

<td>IR2</td>

<td>The people on the bulletin board are very responsive to my posts.</td>

</tr>

<tr>

<td>IR3</td>

<td>I can always count on getting responses to my posting fairly quickly.</td>

</tr>

<tr>

<td>IR4</td>

<td>Most employees can respond quickly when receiving outside inquiries.</td>

<td>Gefen and Ridings, 2002</td>

</tr>

<tr>

<td colspan="2">

**Shared interpretation**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>Shri1</td>

<td>When managers have different opinions about an issue, they tend to resolve the differences eventually.</td>

<td rowspan="2">Tippins and Sohi, 2003</td>

</tr>

<tr>

<td>Shri2</td>

<td>Managers in our firm tend to agree on how to make the best decision.</td>

</tr>

</tbody>

</table>

<table><caption>

**Innovativeness**</caption>

<tbody>

<tr>

<td colspan="2">

**Product innovativeness**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td><sup>*</sup>Prod1</td>

<td>The company has different technical characteristics or specifications for different products.</td>

<td rowspan="3">Garcia and Calantone, 2002</td>

</tr>

<tr>

<td>Prod2</td>

<td>The company frequently introduces new classes of products.</td>

</tr>

<tr>

<td>Prod3</td>

<td>The company offers products that are more complex than others which were introduced into the same market.</td>

</tr>

<tr>

<td colspan="2">

**Process innovativeness**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>Proc1</td>

<td>The company adopts new production processes.</td>

<td rowspan="2">Garcia and Calantone, 2002</td>

</tr>

<tr>

<td><sup>*</sup>Proc2</td>

<td>The company’s product technology is new to the customer</td>

</tr>

<tr>

<td>Proc3</td>

<td>The company often adopts new policies and procedures.</td>

<td rowspan="3">Subramanian and Nilakanta, 1996</td>

</tr>

<tr>

<td>Proc4</td>

<td>Compared to other companies, this company usually adopts a new system earlier.</td>

</tr>

<tr>

<td>Proc5</td>

<td>Within the company, managers are consistently concerned about innovation issues.</td>

</tr>

<tr>

<td colspan="2">

**Personnel innovativeness**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>Pers1</td>

<td>Change in organizational structure occurs very often in the company.</td>

<td>Totterdell, etc., 2002</td>

</tr>

<tr>

<td>Pers2</td>

<td>The company encourages employees to apply innovative ways to improving work processes.</td>

<td rowspan="4">This study</td>

</tr>

<tr>

<td>Pers3</td>

<td>Employees can apply new concepts and technology to their jobs in the company.</td>

</tr>

<tr>

<td><sup>*</sup>Pers4</td>

<td>Laying off unsuitable employees is a routine job in the company.</td>

</tr>

<tr>

<td>Pers5</td>

<td>On-the-job training is a routine in the company.</td>

</tr>

<tr>

<td colspan="2">

**Service innovativeness**</td>

<td>

**Sources**</td>

</tr>

<tr>

<td>Serv1</td>

<td>The company often attracts new customers/clients.</td>

<td rowspan="3">Garcia and Calantone, 2002</td>

</tr>

<tr>

<td>Serv2</td>

<td>The company often adopts new marketing approaches (e.g., customer contacts, advertising promotions, etc.)</td>

</tr>

<tr>

<td><sup>*</sup>Serv3</td>

<td>The company clearly identifies and satisfies new customer/client needs.</td>

</tr>

<tr>

<td colspan="3">

<sup>*</sup>_These items have been deleted because their loadings were less than 0.6 after assessing the fit of each measurement model._</td>

</tr>

</tbody>

</table>