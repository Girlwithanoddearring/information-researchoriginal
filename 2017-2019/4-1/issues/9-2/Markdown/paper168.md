#### Vol. 9 No. 2, January 2004

# An analysis of the use of electronic journals and commercial journal article collections through the FinELib portal

#### [Terttu Kortelainen](mailto:terttu.kortelainen@oulu.fi)  
Department of Information Studies,  
University of Oulu,  
Oulu, Finland

#### **Abstract**

> The contents and characteristics of the services of a national electronic library, the FinELIb, were studied in relation to their use. The indicator of the use of the digital library material was the number of printed articles per journal in a year. The sites characterized as e-journals provide developed services utilizing the hypertext structure, 'outlinks', and the possibility of navigating between different materials, and, at least to a certain degree, maintain the structure of a traditional scientific journal. In services characterized as article files, the hypertext structure is utilised less and the unit of information delivered is an article. E-journals are used more heavily, but the article files outnumber them in the number of journals they offer. The services of e-journals and article files complement each other.

## Introduction

The evolving phase of digital libraries is bringing us close to the turning point where scientific publications, especially journals, predominantly only appear in an electronic format. The use of electronic journals has grown, although this innovation has not yet been fully adopted by scientists even in the developed countries. To be adopted, an electronic journal should perform the same functions as its predecessor, the print journal, such as building a collective knowledge base, communicating information, validating the quality of research and building scientific communities. Some authors see a scientific journal as an entity with its own profile, consisting of a combination of focus, community of readers and contributors and traditions, including articles in a traditional format. They find it important that such entities should also exist in electronic format ([Schaffner](#sch94), 1994), whereas other authors find the contents more important than the container (see [Roberts](#rob01), 2001).

The importance of studying journal collections comes from their central role in scientific communication, while their acquisition costs simultaneously account for a notable part of library funds. One way to cope with this problem is digital library solutions, where licenses for the journals are negotiated at a national level between the national digital library and the publishers. Such a solution. which gives large coverage, involves a sigfnificant investment, but provides services to a large audience. In this phase of change it is important to study the use and characteristics of digital library material. Digital libraries have been studied quite a lot from the technical viewpoint, as also the use of digital publications (for example, [Lempiäinen](#lem01), 2001; [Roberts](#rob01), 2001; and [Crawford](#cra96), _et al._, 1996). However, it is important also to study the contents of the digital libraries and their characteristics, related to their use, and to develop methodology for their needs for evaluation. The mere application of use statistics can be complemented by Webometric methods, which consist of informetric methods applied to Web publishing ([Almind & Ingwersen](#alm97), 1997).

The adoption of an innovation, such as a digital publication, is connected to its characteristics, such as the relative advantage it offers, its compatibility with users' needs, its complexity, its observability and its trialability ([Rogers](#rog95), 1995). In this study, these concepts are used to study the services offered by **FinELib**, the Finnish National Electronic Library, and the publication format of its digital journals. **FinELib** is uniquely comprehensive with 8400 licensed journals, 120 databases, dictionaries, etc., and its centralized, national-level service, which covers the universities and polytechnics of Finland, i.e. all together about 103 942 students, researchers and teachers. The service was launched in 1999, and since then, both its collections and the number of users have been steadily increasing. The research questions are:

What are the relative advantages, compatibility, complexity and observability of digital journals provided by **FinELib**, and how are they related to their use as measured by the number of printed articles per year?

## Background

From the user's point of view, implementing a digital library service requires the adoption of new working methods. Finding digital material and reading from the screen may, at least in the beginning, seem to be complicated and cause feelings of uncertainty. To be adopted, an innovation should have advantages that exceed its probable complexity, and compatibility with the needs and values of the user ([Rogers](#rog95), 1995).

Several characteristics of e-journals may promote their adoption by users. They may lower publishing costs and speed the publishing process, as manuscripts can be published only days after acceptance. They demand less space in libraries. A hypertext structure enables navigability between the article, its references, respective citations, and other related material. This, in turn, potentially, increases the breadth of access to new information and new kinds of data, such as video- or audio-clips, or original research data in the appendices of the articles. This may increase the applicability of the contents of the article, as the reader can view the basis of the conclusions made by the author. The electronic journal does not need to set the same limitations on the length of the article as its printed counterpart. Even if the article itself is not long, the appendices containing necessary information are also not subject to the same page limitations. ([Curran](#cur02), 2002; [Schaffner](#sch94), 1994).

The characteristics mentioned above can be described as a _relative advantage_ of the digital journals. _Relative advantage_ is the degree to which an innovation is perceived as being better than the idea it supersedes, and is positively related to its rate of adoption. The electronic publishing format can be regarded an innovation by users for whom it is new (see [Rogers](#rog95), 1995). Therefore, the adoption of e-journals can be studied from the viewpoint of diffusion of innovations. In addition to _relative advantage_, the attributes of an innovation include _complexity_, _observability_, _compatibility_ and _trialability_. _Observability_ is the degree to which the consequences of an innovation are perceived as observable by a potential adopter; _compatibility_, the degree to which an innovation is perceived as consistent with the existing values, past experiences, and needs of the potential adopter; and _trialability_, the degree to which an innovation may be experimented with on a limited basis. All of these attributes are positively related to the adoption of an innovation. _Complexity_, the degree to which an innovation is perceived as relatively difficult to understand and use, on the other hand, is negatively related to the rate of adoption of an innovation. Other factors that influence diffusion include attributes both of the potential adopter, and those of the social system to which the adopter belongs. The characteristics of the adopter that may promote the adoption of the innovation include technical expertise, in the case of a complex innovation, and in some cases, also, economic resources for the acquisition of the innovation and to overcome the possible disappointment with the decision to adopt the innovation. ([Rogers](#rog95), 1995.)

If the attributes of an innovation are compared with the attributes of a good digital object ([Cole](#col02), 2002), the _relative advantage_ of a digital library or an e-journal may include value-added services, such as search utilities, browser interfaces and specialized dissemination protocols. Such services make the use of digital libraries easier, facilitate the management of the digital contents and enhance the accessibility of information. Cole also requires a good digital object to be digitized in a format that supports current and likely future use, is exchangeable across platforms, and widely accessible. In the case of digital journals, _relative advantage_ implies factors that exceed the advantage offered by a printed journal: utilisation of a hypertext structure enabling navigation between an article, its references and other material, and new kinds of data linked to it.

With respect to the requirements set by Cole ([2002](#col02)) and Schaffner ([1994](#sch94)), the _compatibility_ of an e-journal could be defined as persistence; that is, that it remains accessible over time despite changing technologies. This also includes the requirement that it should perform the same functions as its predecessor, such as validating the quality of research, communicating information, building a collective knowledge base, and also being a source of credit. Compatibility could also include Cole's ([2002](#col02)) requirement that it should be authenticated in at least two senses: the user should be able to determine the object's origin, structure and developmental history, and a user should be able to determine that the object is what it purports to be.

The reader of a digital journal is required to adopt new ways to work: to read from the screen, browse electronically, and not to be able to take the journal into his own hands, which all can be considered as _complexities_ of the journal. Also electronic equipment and network connections enabling all this are necessary. Requirements like this may slow the adoption of electronic publications.

_Observability_ implies Cole's ([2002](#col02)) requirement that a digital object is named with a persistent, unique identifier, and not with a reference to its absolute filename or address (e.g. URL), which has a tendency to change. In the case of an e-journal this could mean that the title of the journal should be uniquely identified. Often, the reliability assessment or interest in an article is based on its publication channel, which also is of major importance for authors ([Luukkonen](#luu92), 1992).

_Trialability_, to some degree, has been more weakly connected to the adoption of an innovation. In the case of e-journals it implies their use through digital libraries, provided that the user has access to one, or through open access journals that do not require payment.

In this study the attributes of innovation are applied in the case of a digital library and its contents in order to describe its strengths and weaknesses, related to its use. Diffusion of innovation concepts have been used earlier in the field of informetrics by Crane ([1975](#cra75)) in her study of invisible colleges, Lancaster and Lee ([1985](#lan85)) in their study of the diffusion of the term 'acid rain', by Lindholm-Romantschuk ([1994](#lin94)) in her study of the diffusion of monographs representing the humanities and the social sciences, and Kajberg ([1996](#kaj96)) in his study of the diffusion of ideas from foreign countries into Danish library and information science. Oromaner ([1986](#oro86)) has studied the diffusion of publications representing a special field in American sociology into mainstream sociology and Kortelainen ([2001](#kor01)) the international diffusion of a national scientific journal.

## Research methods

The research corpus consists of the [**FinELib** Web pages](http://www.lib.helsinki.fi/FinELib/), which provide access to 8,400 licensed journals and 120 databases. The pages also include use statistics from the year 1999 onwards. Journals are arranged by publishers or brokers offering them, such as Ebsco Host, Elsevier Science Direct or SpringerLink.

The indicator of the use of this digital library material was the number of printed articles per journal in a year. The Web site also includes statistics of visits to each journal. However, printing an article implies more serious and less unintentional use. For seven **FinELib** services the statistics cover only one year or less. Therefore, this study focuses on ten services, for which statistics cover more than one year. The reason for the irregularity of the statistics is that licences of the journals have been granted at different times and access to them has been available for different periods.

The Web sites of the journals were studied qualitatively for their possible relative advantage, complexity, compatibility, observability and trialability. In the study of relative advantage, factors contributing to value-added services, such as search utilities, browser interfaces and linking were identified. Complexity was evaluated in relation to the existence of versatile search options. Compatibility was evaluated in relation to the maintenance of the functions of printed scientific journals. Special attention was paid to the possibility of browsing journals by volume. Observability was studied through the possibility to identify journal titles. All the digital publications included in the material are equally trialable and, therefore, their trialability was not specifically assessed. These characteristics were studied in relation to their use to find out whether and to what degree the attributes or the combination of services offered was related to their use.

This study deals with the **FinELib** services providing full-text journals through the Web pages of OuluUniversity Library, Finland. Databases, glossaries, and other electronic resources also offered by **FinELib** are outside this study.

## Results

The **FinELib** has licences for 8,400 electronic journals, from which 13,000,000 searches were made in 2002 and two million articles were printed. In 2001 the figures were, respectively, 7,200 journals and 1,400,000 articles.

The **FinELib** category 'electronic journals' includes seventeen services provided by publishers or brokers, which can be accessed without a password, and statistics concerning use in 2002 (and 2001) are availablefor ten of them. The use statistics concern 6,419 (4,544) journals from which 1,500,000 (1,200,000) articles were printed yearly, which means on the average 240 (280.5) printed articles per journal a year.

The shift to an electronic publishing format has affected the structure of the scientific journal and the page layout of its articles. The contents of the **FinELib** indicate that the modes of digital publishing vary notably and are currently far from standardization. There are services that have notably extended their scope from that of a printed journal. They offer links to related pages, which in Björneborg's and Ingwersen's terms ([2001](#bjo01)) can be called out-links. They also offer alerts through e-mail about relevant articles, information concerning the most frequently read and cited items, the possibility for a personal profile and login, and for publishing special material, such as audio or video clips. These services utilise thoroughly the possibilities of the hypertext structure and navigation between pages and different kinds of information. In this paper, they are called _e-journals_ and there are 1029 of them in this study (table 1). Table 1 does not include all the services offering _e-journals_ via the **FinELib**. There are also other equally developed services, such as Annual Reviews, Association for Computing Machinery, HighWire, etc. They are, however, excluded from this study because their use statistics do not cover a long enough period, and consequently are not comparable with the material mentioned above.

<table><caption>

**Table 1: Services offering e-journals in this study**</caption>

<tbody>

<tr>

<th> </th>

<th rowspan="2">Browsing by journal</th>

<th rowspan="2">Links provided to...</th>

<th colspan="2">Number of journals</th>

<th colspan="2">Printed articles a year</th>

<th colspan="2">Prints per journal</th>

</tr>

<tr>

<th>Service</th>

<td>2001</td>

<td>2002</td>

<td>2001</td>

<td>2002</td>

<td>2001</td>

<td>2002</td>

</tr>

<tr>

<td>Academic Press</td>

<td>X</td>

<td>Citing items</td>

<td>174</td>

<td>214</td>

<td>83,768</td>

<td>81,580</td>

<td>481.38</td>

<td>381.21</td>

</tr>

<tr>

<td>American Chemical Society</td>

<td>X</td>

<td>Society</td>

<td>25</td>

<td>34</td>

<td>95,800</td>

<td>29,901</td>

<td>3,832.00</td>

<td>879.44</td>

</tr>

<tr>

<td>Emerald: Emerald Library</td>

<td>X</td>

<td> </td>

<td>118</td>

<td>141</td>

<td>96,035</td>

<td>126,478</td>

<td>813.86</td>

<td>897.01</td>

</tr>

<tr>

<td>IEEE/IEE – IEL Online</td>

<td>X</td>

<td>Society</td>

<td>134</td>

<td>116</td>

<td>29,715</td>

<td>74,313</td>

<td>221.75</td>

<td>640.63</td>

</tr>

<tr>

<td>JSTOR</td>

<td>X</td>

<td> </td>

<td>117</td>

<td>132</td>

<td>20,160</td>

<td>34,463</td>

<td>172.31</td>

<td>261.10</td>

</tr>

<tr>

<td>Springer Verlag: LINK</td>

<td>X</td>

<td> </td>

<td>392</td>

<td>392</td>

<td>151,218</td>

<td>86,552</td>

<td>385.76</td>

<td>220.80</td>

</tr>

<tr>

<th>Total</th>

<td> </td>

<td> </td>

<td>960</td>

<td>1,029</td>

<td>476,696</td>

<td>433,287</td>

<td>496.56</td>

<td>421.10</td>

</tr>

</tbody>

</table>

There are also other services that provide journal articles in full-text form, without much utilizing the advantages of digital publishing. In these services the journal is not equally visible as an entity. Individual journals cannot be searched. The unit of publication is an article, not a journal. Lists of journals are available, but not linked to their respective contents. The digital publication is merely a representation of the printed one, and the advantages of a hypertext structure are not fully utilised. An important difference compared to e-journals is the absence of out-links: no links to related publications or other services are provided. These could be called full text databases rather than genuine e-journals. In this study services like this are called _article files_, four of which are studied here (see Table 2). There are also other services of the same kind, provided by the **FinELib**, but once again, their use statistics do not cover a long enough period to make the study meaningful.

<table><caption>

**Table 2: Services offering article files**</caption>

<tbody>

<tr>

<th rowspan="2">Service offering full text articles</th>

<th rowspan="2">Links provided to...</th>

<th colspan="2">Number of journals</th>

<th colspan="2">Printed articles a year</th>

<th colspan="2">Prints per journal</th>

</tr>

<tr>

<th>2001</th>

<th>2002</th>

<th>2001</th>

<th>2002</th>

<th>2001</th>

<th>2002</th>

</tr>

<tr>

<td>ABI Inform</td>

<td>Subject terms</td>

<td>750</td>

<td>1,000</td>

<td>205,251</td>

<td>221,122</td>

<td>273.67</td>

<td>221.12</td>

</tr>

<tr>

<td>EBSCO</td>

<td> </td>

<td>2,657</td>

<td>2,930</td>

<td>472,276</td>

<td>519,474</td>

<td>177.75</td>

<td>177.29</td>

</tr>

<tr>

<td>IIMPF</td>

<td> </td>

<td>30</td>

<td>53</td>

<td>2,637</td>

<td>1,448</td>

<td>87.90</td>

<td>27.32</td>

</tr>

<tr>

<td>OVID</td>

<td>Subject terms</td>

<td>98</td>

<td>98</td>

<td>107,973</td>

<td> </td>

<td>1101.77</td>

<td> </td>

</tr>

<tr>

<td>Total</td>

<td> </td>

<td>3,535</td>

<td>4,081</td>

<td>788,137</td>

<td>742,044</td>

<td>222.95</td>

<td>181.83</td>

</tr>

</tbody>

</table>

ABI Inform, EBSCO and OVID (Table 2) offer some services in which it is possible to browse journals by title and volumes and thereby to reach the full text of their articles. In OVID, _Journals@Ovid Full Text_ and _Your Journals@Ovid_ offer the Browse Journals search option, which is not available in the other services. The publication search of ABI Inform enables search by journal issue, but the results often only include an abstract or a limited full text. EBSCO, as provided at the **FinELib** offered by the University of Oulu, includes two bibliographic databases and one service in which journals can be browsed and the issues are linked to the respective full text articles.

The _e-journals_ offer several services that exceed those of the printed version, and can be said to offer more _relative advantage_, compared to the _article files_. However, the _article files_ have the advantage of magnitude. Their existence guarantees the user access to an ocean of articles, even though the search possibilities are not as versatile as those of the _e-journals_.

If _observability_ is defined by the naming of a digital object by a persistent, unique identifier, _e-journals_ and _article files_ are to some degree different. In _article files_, the form of the traditional journal has been to some degree eliminated, and the unit of the information delivered is the article. Information concerning the publication channel is of minor importance, sometimes even hard to find, and in some cases it is not easy to identify the author or publication year from the first page of the article. The journals cannot be browsed volume by volume.

In _e-journals_ provided by the **FinELib**, individual journals can be identified and searched both through searching words and browsing the volumes and issues of different years. The structure of the journal, as an entity, has been maintained, and the publication channel of an article is readily visible.

In the case of digital library material _observability_ comes close to _compatibility_, which was defined as the degree to which the digital journal maintains the tasks and functions of its printed predecessor. Schaffner ([1994](#sch94)) sees it necessary that the digital journal also guarantees the quality of the papers it publishes and builds scientific communities. Some _e-journal_ services of the **FinELib** even suggest the strengthening of this task by providing easy navigation to related items, most frequently read or cited publications, access to the pages of scientific societies (such as the American Chemical Society or the IEEE) and their services outside publishing. The reader may also be asked to actively participate, for example, by giving feedback, joining the society, or just registering to a certain service. This implies communication also through other, less formal means than merely authoring scientific articles and citing them, and makes this possible at least internationally, if not worldwide.

Both _e-journals_ and _article files_ can be said to be _trialable_. They are offered by a national service through both scientific and public libraries. However, each library may offer its own combination of the FinELib services. For example, universities offer different choices of journals and databases, according to the needs of their patrons. Nevertheless, if a user has access to FinELib s/he can experiment with it freely and the decision on using FinELib's material does not include any economic aspects. The trialability of an innovation has been found to be positively related to its adoption, but to some degree less than relative advantage, compatibility or complexity ([Rogers 1995](#rog95)). In any event, the lack of _trialability_ does not complicate the adoption of either _e-journals_ or _article files_. Accordingly, their _complexity_ probably has the same effect on the use of both these kinds of materials. A reader must have both the required equipment and access to **FinELib** in order to use its services. Another study should be made to be able to determine whether it is easier to find important information through browsing the volumes or by searching them with terms. However, these searching methods complement each other. Browsing the volumes and using the provided navigation to related or most cited items may enable the encountering of relevant, unexpected items in several different ways in addition to searching by terms or browsing alphabetically. Therefore, access to relevant papers in _e-journals_ may be more versatile, compared with that of _article files_. Therefore, _article files_ probably can be argued to be more complex than the _e-journals_.

The complexity of reading from the screen was reflected in Lempiäinen's study ([2001](#lem01)) by 59% of male and 53% of female students, who did not agree with the statement 'I read an article equally willingly from the screen as from a paper copy'. 74% of female and 80% of male students would have chosen the printed version of an article if both the electronic and printed versions had been available. Nevertheless, these were not the reason for not using digital journals. The reasons for not using them came from three directions: the students did not know about them, did not need them, or there were no electronic journals suitable for their subject of research.

When the use of the **FinELib** _e-journals_ and _article files_ was compared, it was found that on the average, the former were used more frequently than the latter in 2001-02\. The average number of articles printed from _e-journals_ was 421.10 per journal in 2002 (496.56 in 2001; see Table 1). The statistics concern six publishers and 1,029 journals (2002). The use concentrated most highly on _e-journal_ services that represented certain fields of research, such as the _American Chemical Society_, with 879.44 printed articles a year (2002). In _article files_ the maximum number of printed articles per journal in a year was 221.12 (for the ABI Inform collection) and the average 181.83 (2002; see Table 2), which is about half of that of the _e-journal files_.

Behind the use figures there are, naturally, also other factors than the characteristics of the publication format. One such factor is the allocation of research and educational resources in the country, which has an impact on the number of scientists in each field, and, consequently, the number of the users interested in different journals. With a few exceptions, however, most **FinELib** services cover several fields and, consequently, it is difficult to relate their use to the number of scientists in each field.

As a measure of use, it would be interesting to compare the number of articles printed to the impact factor of each journal to see the possible correlation. However, not all of the 6,864 journals in this study have an impact factor. In addition, in the **FinELib** such a comparison is complicated by the difficulty of getting lists of journals from article files.

To some degree the _article files_ and _e-journals_ complement each other: several publishers offer the latest items through e-mail alerts, for example, whereas there is one service, JSTOR, specifically offering only material older than one to five years, so as not to compete with the others. On the other hand there is duplication: numerous journals (for example, _Analytical Chemistry, Biochemistry, The Lancet, Nature, Science_, etc.) can be searched through at least two or three different **FinELib** services: through the publisher's or broker's own service and through article files.

## Conclusions

The reasons for the development of digital publishing and services, such as the **FinELib**, include factors such as lower publishing costs, speed of publication, reduced storage volume requirement, etc. These certainly are reasons for publishers to adopt the new publication format, as well as for libraries wishing to reduce their acquisition expenses and space requirements. However, these two factors do not necessarily affect the use of the **FinELib**, because the consortium has made the decision concerning the offering of the journals, and on the other hand, the user does not see the costs connected with the use of the journals. Therefore, more important factors affecting the use may be those connected with the contents, the structure, and the services offered by the journal.

The services called _e-journals_ in this study have maintained the structure of the journal and can be searched both by search terms, volumes and issues. In this respect they are compatible with the requirements of performing the same functions as their printed predecessors (e.g. [Schaffner](#sch94), 1994). The publication channel of articles is clearly observable, which may be important from both the author's and the reader's point of view. From the _e-journals_ information can be found through out-links to related material, to related citations or most frequently cited items. These versatile search possibilities, to some degree, can compensate for the complexity of a Web service, caused by the necessity to use an Internet connection and a computer to read an article. A lot of relative advantage, compared to printed journals, is gained through utilising the hypertext structure, navigability and the possibility, also, to link new kinds of material (audio, video) to the articles. An interesting feature of an article is the existence of out-links from it to related or cited material, provided by the publisher.

In many cases, _e-journals_ can be characterized as personalized and specialized. On average, they are more heavily used than the _article files_, which also offer full text journals in a digital form. The _e-journals_ among themselves, but also compared to _article files_, differ from each other greatly in composition, and it can be argued that they can learn from each other.

The number of printed articles per journal in a year as an indicator of use of the material differs in some respects from the impact factor, which measures citations received by a journal. The impact factor tells something about the use of the articles or at least about awareness of them, whereas the number of prints shows a user's interest, but not yet their utilization in further studies. The impact factor provides a worldwide picture, whereas the number of printed articles per journal is a national level figure. A journal publishing more articles will probably have more articles printed , as, according to Garfield ([1983](#gar83)) it also may be more cited. Therefore, it should be considered whether the figure should be divided by the number of published papers, as in the case of citations when counting impact factors. In this study, however, use was not studied at the level of journals, but, at the level of the services providing them.

The _article files_ of the **FinELib** offer a great magnitude of digital material, both scientific and popular. This great number of articles and journals is an important service for the users of Finnish libraries. The _article files_ studied here differ from the e-journals in their more basic service. The unit of the information delivered is an article. The structure of the journal is not equally visible, there are fewer search possibilities and the hypertext structure is not utilised to the same degree. Nevertheless, they offer a great deal of important information and the possibility to search it. _E-journals_ and _article files_ complement each other. However, it can be seen, that the former are used more heavily, at least according to the statistics provided for the first two years.

Three services include both e-journals and article files. This causes inaccuracy in the study of use scores, because the statistics of printed articles are provided for each **FinELib** service, and are not divided between separate article files or e-journals. The inclusion of these services into article files probably increases their use statistics. However, there was a clear difference between the use of the _e-journals_ and _article files_.

## Discussion

The **FinELib** was launched in 1999, and each year has brought both new material and users. Therefore, the statistics of the use of the **FinELib** cover a short time, are based on a varying number of users, and, consequently, do not provide a basis for analysing any trends. The attributes of the digital journals offered by a national digital library were studied qualitatively from the Web pages of the service in relation to the use of the service.

Digital publishing is still far from standardization and the services differ greatly from each other. Some publishers offer important and even innovative services in addition to the contents of scientific papers. Several journals continue to be identified as an entity also after digitization. Some of them even suggest a new way to build and support scientific communities and communication. Although the research material only represents 10 services, it, however, includes thousands of papers. The results based on this corpus suggest that more personalized and specialized services, in which the individual journals still exist, are more heavily used than those, in which the unit of information no longer is a journal, but, instead, an article.

There are interesting questions for further study, such as where do the links to related material lead? Do they lead to items published by the same or another publication, publisher or discipline; that is, do they act as transversal links (see [Björneborg and Ingwersen](#bjo01), 2001)? What consequences do they have for information use? In the article files studied here, all journals are equally present. Does this have consequences for the use of journals that in printed form probably would be less accessible?

It would also be interesting and important to study, whether the scientific e-journals really strengthen connections between scientists, by also offering less formal communication than authoring and citing scientific articles, and whether this communication has any impact on the quality and quantity of scientific production.

Even though the probable complexity of the electronic journals' Web sites does not prevent people from using them, as the growing user statistics demonstrate, a usability study would be welcome. In some cases the search page is several click's beyond the home page of the site. For an experienced user it is not any problem, but for the occasional browser it may be hard work to find the place, where to search.

In this study, quantitative methods focused on electronic publications, were used to determine the characteristics and use of the digital journals and articles. A qualitative feature, which seems to describe the utilization of the hypertext structure of electronic publications is the existence or absence of out-links from the articles, enabled by the publication channel. The number of printed articles was used as an indicator of use in this study. Compared to impact factor or Web-impact factor counts it gives a national level picture of the use of a Web service. However, in another study it would be interesting to see whether, and how, it correlates with the respective impact factors of the journals.

## References

*   <a id="alm97"></a>Almind, T.C. & Ingwersen, P. (1997). Informetric analysis on the World Wide Web: a methodological approach to "Webometrics". _Journal of Documentation_, **53**(4), 404-426.
*   <a id="bjo01"></a>Björneborg, L. & Ingwersen, P. (2001). Perspectives of webometrics. _Scientometrics_, **50**(1), 65-82.
*   <a id="col02"></a>Cole, T.W. (2002) Creating a framework of guidance for building good digital collections. _Firstmonday,_ **7**(5). Retrieved 23 August 2002 from http://firstmonday.org/issues/issues7_5/cole/index.html
*   <a id="cra75"></a>Crane, D. (1975). _Invisible colleges. Diffusion of knowledge in scientific communities._ Chicago, IL: The University of Chicago Press.
*   <a id="cra96"></a>Crawford, S., Hurd, J.M. & Weller, A.C. (1996). _From print to electronic. The transformation of scientific communication._ Medford, NJ: Information Today. (ASIS Monograph Series.)
*   <a id="cur02"></a>Curran, C. (2002). [Medical journal meets the Internet.](http://www.first monday.dk/issues/issue7_6/curran/) _Firstmonday,_ , **7**(6). Retrieved 28 August 2002 from http://www.first monday.dk/issues/issue7_6/curran/.
*   <a id="gar83"></a>Garfield, E. (1983). _Citation indexing - its theory and application in science, technology and humanities._ Philadelphia, PA: ISI Press.
*   <a id="kaj96"></a>Kajberg, L. (1996). A citation analysis of LIS serial literature published in Denmark 1957-1986\. _Journal of Documentation_, **52**(1) 69-85.
*   <a id="kor01"></a>Kortelainen, T. (2001). Studying the international diffusion of a national scientific journal. _Scientometrics_, **51**(1) 133-146.
*   <a id="lan85"></a>Lancaster, F.W. & Lee, J.-L. (1985). Bibliometric techniques applied to issues management: a case study. _Information Science_, **36**(1), 389-397.
*   <a id="lem01"></a>Lempiäinen, E. (2001). _Elektroniset lehdet opinnäyteteiden lähteinä. Tutkimus elektronisten tieteellisten lehtien käytöstä, merkityksestä sekä arvostuksesta tiede- ja ammattikorkeakouluissa. [Electronic journals as a source for theses: a study on the use, importance and appreciation of articles in online scientific journals at universities and polytechnics.]_, Unpublished licentiate dissertation. University of Tampere, Department of Information Studies.
*   <a id="lie00"></a>Liew, C.L., Foo, S. & Chennupati, K.R. (2000). A study of graduate student end-users' use and perception of electronic journals. _Online Information Review_, **24**(4), 302-315.
*   <a id="lin94"></a>Lindholm-Romantschuk, Y. (1994). _The flow of ideas within and among academic disciplines: scholarly book reviewing in the social sciences and humanities._ Unpublished doctoral dissertation. University of California at Berkeley, Graduate Division, Library and Information Studies
*   <a id="luu92"></a>Luukkonen, T. (1992). Is scientists' publishing behaviour reward-seeking? _Scientometrics_, **24**(2), 297-319.
*   <a id="oro86"></a>Oromaner, M. (1986). The diffusion of core publications in American sociology: A replication. _International Journal of Information Management_, **6**(1), 29-35.
*   <a id="rob01"></a>Roberts, S.A. (2001). Electronic journals in higher education: technology, decision making and economics. _Information Services & Use._ **21**(3/4), 223-234.
*   <a id="rog95"></a>Rogers, E.M. (1995). _Diffusion of innovations._ (4th ed.) New York, NY: The Free Press.
*   <a id="sch94"></a>Schaffner, A.C. (1994). The future of scientific journals: lessons from the past. _Information Technology and Libraries,_ **13**(4), 239-247.