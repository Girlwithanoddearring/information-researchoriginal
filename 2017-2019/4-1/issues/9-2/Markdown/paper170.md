#### Vol. 9 No. 2, January 2004

# Open access to scientific publications - an analysis of the barriers to change?

#### [Bo-Christer Björk](mailto:bo-christer.bjork@hanken.fi)  
Swedish School of Economics and Business Administration  
Helsinki, Finland

#### **Abstract**

> One of the effects of the Internet is that the dissemination of scientific publications in a few years has migrated to electronic formats. The basic business practices between libraries and publishers for selling and buying the content, however, have not changed much. In protest against the high subscription prices of mainstream publishers, scientists have started Open Access (OA) journals and e-print repositories, which distribute scientific information freely. Despite widespread agreement among academics that OA would be the optimal distribution mode for publicly financed research results, such channels still constitute only a marginal phenomenon in the global scholarly communication system. This paper discusses, in view of the experiences of the last ten years, the many barriers hindering a rapid proliferation of Open Access. The discussion is structured according to the main OA channels; peer-reviewed journals for primary publishing, subject-specific and institutional repositories for secondary parallel publishing. It also discusses the types of barriers, which can be classified as consisting of the legal framework, the information technology infrastructure, business models, indexing services and standards, the academic reward system, marketing, and critical mass.

## Introduction

Publication of scientific content has been one of the areas to benefit most from the emergence of the Internet. A scientific publication, as an information good, can easily be delivered electronically to the end user. Scientists already have the necessary equipment and skills to access the material in their normal work environment, and have been forerunners in the use of the Internet for communication. In the near future scientists worldwide will access most of the texts they read over the World Wide Web. In addition to traditional publications there are many other ways in which scientific communities can use information technology and the Internet to enhance their communication and collaboration processes, such as e-mail discussion lists, databases of observation data, the sharing of models and programming code etc., but these are not considered in this paper.

While the delivery technique for scientific publications has changed rapidly, the economic ramifications have hardly changed at all. The concentration of the publishing of journal titles in the hands of a few large players, in combination with electronic delivery, has made the strong players even stronger and there have been both an anti-trust investigation ([Competition Commission, 2001](#comp)) and an informal investigation ([Office of Fair Trading, 2002](#oft)) into some of the proposed mergers of the biggest publishers. The extremely low marginal costs of selling information over the Internet favour the use of sales and marketing strategies such as bundling and differential pricing ([Shapiro and Varian, 1999](#shapiro)). Consequently, publishers of scientific journals have rapidly started offering site licenses for electronic access to universities and university consortia. The key issue is that the there is very little competition in this industry and that the pricing schemes depend much more on each customer's willingness (and capacity) to pay, than on the production prices. Thus, it is today more or less as expensive for university libraries and individual subscribers to access this material over the Internet as before in paper format. The serials crisis, the long period of rises in subscription prices that started in the 1970s, continues ([Frazier, 2001](#frazier)).

In parallel to these developments pioneering scientists have jumped on the opportunity offered by the Internet for bypassing the costly intermediaries in the publishing process. During the 1990s several e-print archives as well as a few hundred peer-reviewed, electronic, scholarly journals emerged. The common denominator for these is that they offer free access to the electronic product. This has become known as "open access publishing".

The experiences of this first wave of pilots demonstrate that the barriers for changing the commercially oriented communication system were greatly underestimated. Because of such barriers the impact of open access on the total volume of scientific publishing is still negligible. This paper will take a closer look at some of these barriers, which broadly can be categorized into economic, legal, social and psychological.

## What is 'open access'?

'Open access' (OA) means that a reader of a scientific publication can read it over the Internet, print it out and even further distribute it for non-commercial purposes without any payments or restrictions. At the most the reader is in some cases required to register with the service in question, which for instance can be useful for the service providers in view of the production of readership statistics. The use of the content by third parties for commercial purposes is, however, as a rule prohibited. Thanks to the open availability the linking from reference lists to OA publications is substantially facilitated, since the reader does not encounter barriers such as use licenses, and each reference is only a mouse-click away. In general, the author keeps almost complete copyright and can also publish the material elsewhere.

The Internet is a superb channel for the free distribution of information in the public domain. Science as a social institution has also always had as an ideal the open sharing and critique of information. Thus. there is a much stronger case for open access to scientific content than, for instance, for the use of Open Source methods in the development of information technology applications ([Björk, 2001](#bjork2001)) not to mention free downloading of music files from subversive third parties, that is counter to the legitimate interest of the producing artists. A related development is also the posting of teaching resources for free on the Internet, a phenomenon for which the label 'OpenCourseWare' has been used ([MIT, 2001](#mit)).

The four most important OA channels are electronic, refereed, scientific periodicals, research-area-specific archive (e-print) servers (in this paper called subject-specific repositories), institutional repositories of individual universities, and self-posting on authors' home pages. OA scientific periodicals have been founded since the early 1990s, usually as individual efforts. Almost as a rule, such journals are electronic only, because of the need to minimise costs, but in some cases it may be possible to have a paid subscription to a paper version. OA journals are usually funded largely by the voluntary work of the involved editors and direct or implicit grants (the free usage of the host university's Web servers could be seen as a subsidy). In more recent years a number of efforts to publish OA journals on a larger scale have emerged. Their business plan is usually to finance the operations through author charges. A variety of OA journals are also traditional subscription journals, which become freely available in electronic form after a delay of, typically, six months to a year.

The best-known subject-specific repository, for high-energy physics ([Ginsparg, 1996](#ginsparg)), was founded in 1991\. Such repositories typically aim at the parallel publishing of material, which is being written for other outlets (such as conferences or traditional journals), allowing earlier and more efficient dissemination. Subject-specific repositories have emerged in research areas where traditions for the exchange of preprints have existed prior to the Internet and where the speed of publication is an essential factor ([Kling and McKim, 2000](#kling)). The guiding principle of such electronic archives is that researchers themselves upload article manuscripts, conference papers etc., into the repositories. Thus, very low maintenance costs can be achieved. The custodians of the repositories usually only check that totally irrelevant material is not deposited. Papers in a repository are available globally much earlier, than, for instance, the finally published versions of the manuscripts in paper-based journals. In areas like computer science this can be a significant benefit.

From the earliest days of the Web individual researchers have put copies of their own publications on their homepages. Although there are no published surveys of how common self-posting is, this author's experience is that this is the most common OA channel today. The increasing interest of the universities to start institutional repositories brings a more systematic and long-term commitment to this activity. Institutional repositories represent a third, important, OA-channel, and are relative newcomers compared to the journals and subject-specific repositories. Universities and their libraries are in a better position than individual academics to guarantee that the material is available even after decades and that the collection is systematically maintained, for instance, to take account of changing file formats and media. Institutional repositories represent an integral part of the long-term strategies of the universities in question, in particular as these have to redesign their publishing and library policies to take into account the totally new conditions created by the Internet. The university's own production of theses and working papers can easily be put up on such repositories, but in the long run the posting of the central production of the university's researchers, that is, their conference and, in particular, journal papers, is crucial. Although institutional repositories can be seen as useful marketing channels for individual universities their most significant impact on the global scale can only be achieved via co-operation via open access indexing services.

Today the primary channel for finding OA-material is through general-purpose Web search engines, unless the reader is a regular visitor to the journal or repository in question. Despite their deficiencies general Web search engines can be quite efficient, and a recent study from the domain of computer science showed that publications, of which copies had been freely available on the Web, received on the average three times as many citations as others ([Lawrence, 2001](#lawrence)). The problem with using general search engines is, however, how to distinguish the relevant publications from all the varied material which is available on the Internet and tends to clutter the search results from queries.

## Experiences of the pioneering years

The experiences from the first ten years of experimenting show that it is much harder to change the "system" than originally envisaged ([Guédon, 2001](#guedon)). In a few years hundreds of scientific journals adhering to the OA principles were launched, but of these roughly half have already disappeared and many only publish a few articles per year ([Wells, 1999](#wells), [Gustafsson, 2002](#gustafsson)). There are, nevertheless, some success stories, such as _First Monday_, which on the first Monday of every month publishes papers on Internet research ([First Monday](http://www.firstmonday.org/)). It is possible to maintain subject-specific repositories with minimal costs and in some domains these have acquired a prominent position. The best known of these is arXiv, in which researchers during its eleven year old history have deposited 225,000 publications from the fields of physics, mathematics, and information technology ([arXiv](http://arxiv.org/)). Despite such success stories it had become clear by the turn of the millennium, that the enthusiasm and collaborative spirit of researchers involved in OA efforts was not, alone, enough to offer viable competition to the traditional subscription based delivery channels.

The Swedish School of Economics and Business Administration (HANKEN) participates in the open, self-organising repository for scientific information exchange ([SciX](#scix)) project, financed through the fifth framework programme of the European Commission. SciX is a demonstration project, in which a fully operational subject-specific repository is built for one particular research community, and in which the experiences of the implementation and deployment are recorded and analysed ([SciX](http://www.scix.net/)). The role of HANKEN in the project is to study the scientific publishing process and the effects of different, alternative business models on the life-cycle costs of the process. One of the outputs so far has been a formal process model of the scientific publishing process ([Björk and Hedlund, 2003](#bjork2003)). The model currently contains twenty-two interconnect schemas with sixty-four separate activities. An example is shown in Figure 1.

<figure>

![Figure 1](../p170fig1.gif)

<figcaption>

**Figure 1\. A diagram from the Scientific Publishing Life-Cycle Model being developed in the SciX project. The modelling notation is [IDEFO](http://www.idef.com/idef0.html).**</figcaption>

</figure>

In addition, the barriers to the introduction of open access publishing models are being investigated in the SciX project. On the basis of the results, recommendations and guidelines for policymakers will be formulated.

There is disagreement as to how advantageous open access on a large scale would be. Many proponents have cited the very low running cost of some pioneering journals and repositories. On the other hand publishers and some researchers put forward that the costs of producing journals of high quality are mostly fixed costs and that the share of costs for printing and distribution is very low. One thing which is often overlooked in this debate is the cost repercussions downstream from the chosen business model. Open access would substantially lower a lot of the transaction costs throughout the process (both of publishers, libraries and readers). This is the central research topic in the SciX project, but is not discussed further in this paper. Rather, the hypothesis is that a scholarly communication system state in which a substantial part of all scientific publications are available through one or several OA channels would be advantageous for the academic community and society in general. Given that this hypothesis is shown to be true, the question is: What are the barriers for moving from the current system state to the envisaged OA state?

## Barriers to change

After a decade of experimenting there is now a lot of evidence about the possibilities and difficulties in making open access a real alternative. Quite a lot has been written about the subject, based mostly on the hands-on experience of creators of journals and repositories (e.g., [Odlyzko, 1998](#odlyzko), [Walker, 1998](#walker)). In addition to publications written by OA proponents there are also some more critical voices. For instance, Kling and McKim ([2000](#kling)) discuss the differences between scientific disciplines in adopting OA channels. A number of surveys have also been conducted where both authors and readers have been asked about their perceptions and choices. This author, for instance, has participated in a Web survey shedding light on the topic ([Björk and Turk, 2000](#bjork2000)). He also has first-hand experience of creating and running an OA-journal and in setting up a subject-specific repository. The discussion which follows is based both on personal involvement, use of secondary sources and data from some recent empirical studies.

Table 1 below can be used as a starting point for a discussion about the prerequisites and barriers for open access publishing. The three channels discussed in this paper are open access journals, which function as primary outlets, and subject-specific and institutional repositories, which mainly function as secondary outlets complementing the mainstream channels of journals and conference proceedings. Self-posting on the Web is left outside the discussion, even though it is a fairly important channel at present.

The barriers and means have been classified into six different categories: legal framework, information technology infrastructure, business models, indexing services and standards, academic reward system, marketing and critical mass. In the table the number of asterisks (from zero to three) denotes the importance of a particular item in hindering a rapid transition process. The importance is a subjective judgement made by the author. Thus, in the opinion of this author, there are no (or very small) legal obstacles to the proliferation of open access journals, whereas this is a very central issue to be solved if institutional repositories are to take a prominent position in the academic communication system.

<table><caption>

**Table 1: A classification of different types of barriers for increased open access publishing and their relative importance**</caption>

<tbody>

<tr>

<th></th>

<th>Open access Journals</th>

<th>Subject-specific repositories</th>

<th>Institutional repositories</th>

</tr>

<tr>

<td>Legal framework</td>

<td>-</td>

<td>*</td>

<td>**</td>

</tr>

<tr>

<td>IT-infrastructure</td>

<td>**</td>

<td>*</td>

<td>**</td>

</tr>

<tr>

<td>Business models</td>

<td>***</td>

<td>**</td>

<td>*</td>

</tr>

<tr>

<td>Indexing services and standards</td>

<td>**</td>

<td>-</td>

<td>***</td>

</tr>

<tr>

<td>Academic reward system</td>

<td>***</td>

<td>*</td>

<td>*</td>

</tr>

<tr>

<td>Marketing and Critical mass</td>

<td>***</td>

<td>**</td>

<td>***</td>

</tr>

</tbody>

</table>

In the following these barriers will be discussed one by one.

## Legal framework

### Open access journals

In the case of traditional journals, typically published by commercial publishers or learned societies, the author usually grants the publisher a rather exclusive copyright, in return for the services that the publisher renders the author. It must be stressed that contrary to what proponents of OA often state, authors do not give away the product for free. Instead, they trade their papers without specific payment in exchange for the services that the publisher renders them (peer review, quality labelling, marketing, and dissemination). The fact that some publishers have charged page charges to authors in addition to charging subscribers is one indication of this. The surrender of copyright is so total that, for instance in Finland, where it is rather common for a PhD thesis to consist of four or five previously published journal articles plus a summary, the author is usually forced to ask written permission from the publishers to publish copies of his own papers as part of his thesis (the thesis is usually published as a monograph by his own university and is mainly distributed for free). The author of this paper certainly had to do this for his own thesis. In the case of two articles it was even difficult to find out who owned the copyright at the time of printing of the thesis, since their publishers had been bought up in the meantime.

Many copyright forms grant the author the right to limited distribution of copies to colleagues. The emergence of the Internet has brought into light a particular problem, concerning the non-commercial distribution by posting copies on the Web. In many of the copyright forms which publishers ask authors to sign, this area is not properly addressed and constitutes a grey zone.

Open access journals, on the other hand, have, from the start, adopted a rather liberal approach reminiscent of the licensing schemes used by the open source programming community (often referred to as 'copyleft'). As a rule the author retains the copyright to the work. What the open access journals typically are interested in is that the paper, if made available elsewhere in the exact format of the journal, is attributed to primary publication in the journal, and also that no one (except the author) can resell the content. In conclusion, the copyright issue does not constitute an obstacle for the proliferation of open access journals. Currently used copyright agreements for OA journals are quite satisfactory from both the author's and the journal's viewpoint.

### Subject-specific repositories

A strong impulse for subject-specific repositories was the long lead-time between submission of a draft manuscript and the publication of the full paper. In some areas of science, such as high-energy physics, a tradition of scientists exchanging preprints on paper already existed and the new repositories just developed this mechanism further. The name preprint server describes this function.

One of the problems with low cost subject-specific repositories is that, because of the high number of papers in the successful ones, the managers of the service have no resources to check the legality of the papers posted. It is at the discretion of authors to remove papers once they have been accepted for publication, if they have signed copyright agreements, which prohibit keeping the copies on the server. On one hand the situation resembles that of the controversial music server Napster, on the other hand there are significant differences. The biggest difference is that, in the case of scientific publications, it is the author of the work who voluntarily puts up a copy on the server, not a third party. The legal problems resemble the situation with institutional repositories and, therefore, will be discussed below.

### Institutional repositories

In the early stages institutional repositories will get their initial content from works of the faculty for which the university itself or the authors retain the copyright, such as PhD theses and working paper series of departments. These entail no legal problems. In the longer run, however, the critical mass of institutional repositories depends on the inclusion of the best work of each university's faculty, that is the journal papers published elsewhere. From a legal viewpoint this constitutes a challenge, since university administrations will be very careful not to be break any copyright contracts, in contrast to individual researchers posting copies of their work on their home pages quite carelessly in spite of possible legal obstacles.

Many of the major publishers have recently, if the author asks for it, granted the authors permission to the parallel non-commercial electronic publishing on the Web pages of the university of the author. In a project conducted by the Loughborough University several leading publishers were asked about their official view concerning the publishing of the manuscript or the finalized paper in open access servers ([Romeo](#romeo)). Of these, 33 publishers agree in some form, whereas 49 gave a negative answer. Together the publishers who participated in the survey represented 7,169 journals. When the results were weighted according to the number of titles, 49% of the journals permitted the publishing of either or both versions.

Although many publishers currently are quite liberal in their attitudes towards parallel, non-commercial Web posting in subject-specific and institutional repositories, there is a lot of uncertainty in the longer run. The title of a conference recently organised by the International Association of Scientific, Technical & Medical Publishers, _Universal access to STM information: by evolution or revolution_, tells a lot about the ambivalent feelings publishers have on this issue. As long as the publishers' revenues are not seriously threatened, they advocate willingness to allow authors the right to parallel posting in institutional repositories. They even see this as additional advertisement for the primary publication. But if parallel OA-publishing gains momentum and starts to have a negative effect on subscription income it is possible that the copyright agreements will become tighter and, also, that compliance with existing agreements will be more actively monitored.

Thus, if and when subject-specific repositories and in particular institutional repositories start to have more substantial content and threaten the subscription income for the primary publishing channels that they complement, it is probable that the copyright issue will become a central battleground.

## Information technology infrastructure

### Open access journals

The information technology infrastructure of electronic peer-reviewed journals can include a wide spectrum of different features.

*   Storage mechanism for the papers and the metadata (static Web pages vs. database)
*   Format of the papers (HTML, PDF, XML etc.)
*   Treatment of graphics and hypermedia content
*   Management of drafts and the review process
*   Indexing and linking to external publications
*   Alerting and personalisation services for readers
*   Hyperlinked discussion threads
*   Statistics on readings, citations etc., for authors
*   Security back up, mirror sites, etc.

Most open access journals to date have been individual efforts created by single academics and groups of academics, often managing the journals on a part-time basis. Hence, the information technology infrastructure of these journals is quite varied, ranging from rather rudimentary, static HTML-versions to quite sophisticated database driven systems, depending on the skills and resources of the creators. The platforms have seldom been bought from outside companies or larger publishers. One of the drawbacks of these systems is that they are very vulnerable, in case the person in charge for some reason or other stops working with the journal.

The notable exceptions to this are provided by two major efforts utilising new business models for running portfolios of OA journals. The technical infrastructure of [Biomed Central](http://www.biomedcentral.com/) is on a par with the leading commercial publishers and includes coding of the papers in XML as well as workflow management of reviews. Biomed Central gets considerable economies of scale since they publish almost one hundred journals. The [Public Library of Science](http://www.publiclibraryofscience.org/) recently launched its first journal. Both publishers plan to finance the operations through author charges, but have invested considerable sums in the developing the infrastructure.

In the longer run the publishers of individual journals would benefit a lot from pooling resources, for instance by sharing software applications, or using collaborative Web hosting. Such discussions are under way in the Nordic countries for smaller national or Nordic peer-reviewed journals ([Kvaendrup, 2003](#kvaern)). Another possibility is to use open source applications for running such journals.

### Subject-specific repositories

Like OA journals most subject-specific repositories are the results of individual efforts and the corresponding systems have been made by the academics themselves. The best known of all these, the arXiv server, has for instance a rather simplistic Web interface on top of its database but has on the other hand been quite successful, because the service obtained critical mass rather early. One conclusion is that readers for this type of services care less about sophisticated technical features than about being assured that they can find most of the material of interest to them in one place.

Although there would also be benefits of sharing information technology resources for subject-specific repositories, this might be more difficult to achieve in practice, since such repositories are often bundled with other Web-services to the research community in question, thus necessitating quite a lot of customisation.

### Institutional Repositories

Institutional repositories present a rather different picture from current OA journals and subject-specific repositories. University libraries have considerable funds at their disposal and are used to outsourcing part of the work in building their information technology infrastructure. They also take a long-term perspective in the setting up of institutional repositories. For instance, already in the planning stages, they need to take into account the periodic necessity to upgrade the storage media and the storage formats. In addition to scientific publications universities also have a need to systematically organise the Web-based educational material produced by the faculty.

When universities start planning for such systems they are likely to use one of the following solutions or perhaps combinations of these:

*   Plan for joint national collaboration platforms
*   Use well-proven open source applications
*   Buy the software from outside information technology consultants
*   Outsource the whole service to commercial publishers

An example of the first option is the Dutch [DARE](http://www.surf.nl/themas/index2.php?oid=18) project. The currently best-known open source solution is the D-space system, originally developed by MIT for internal use but currently offered to other universities ([MIT, 2001](#mit)).

## Business models

### Open Access journals

Most open access journals have so far been established by individual pioneers or groups of academics. The main business model has been to minimise costs and to fund the operations as a form of open source project, where hardly any transfer of money is involved and all costs are absorbed by the employers of the individuals participating. A recent Web survey involving the editors of fifty-five open access journals carried out by Hanken confirmed this to be the predominant business model: only approximately ten percent of the journals had explicit budgets.

This business model is very vulnerable in respect of sustaining operations in the longer term and for scaling up from a few papers a year to larger publication volumes, since that might necessitate employing staff. It is also not well suited for journals where copy-editing and layout work for graphics etc., cannot be handled by the authors themselves.

Other possible business models, which would provide more funding for professional-level operations (such as the employment of staff) include advertisement, subsidies from learned societies or research funding agencies, or author charges, in order to keep the end product freely available on the Web, rather than take recourse to subscription fees. All of these have and are being tried out, in different combinations. The most controversial is the one involving author charges (for instance used by the BioMed Central journals) since this reverses the role of a publisher from a seller of a commodity to consumers to a provider of services to authors. Getting individual researchers to pay sums in the order of 500-1500 Euro for publication might be very difficult unless a journal already is regarded as a top-level journal in its field. A way around this dilemma which is being tried out by BioMed Central is for the publisher entering into 'umbrella agreements' with universities who pay a yearly fee covering all submissions from their own faculty.

Yet another model is to publish in a hybrid way, through a mixture of subscription only and open access. Each author decides whether his article will be open access, by paying an author charge. This business model is currently being pioneered by Oxford University Press who recently announced that they will start using this model for one of their most prestigious journals, Nucleic Acids Research ([Goode, 2003](#goode))

Advertisement can work in some limited fields of science such as medicine, where drug companies, for instance, may have an interest. A very important group of players is the learned societies, which, historically, were the ones to start scientific journals as we know them. They could see open access as an important service for their constituency and society in general. Unfortunately many learned societies see journal publishing as an internal profit centre generating finance for other activities or an activity, which at least should generate income enough to cover its cost. From this perspective open access through author charges would still be acceptable. A further problem, however, is that many offer journal subscriptions bundled with their membership fees and fear that going open access would threaten the income from such fees.

The business model issue is central to the further proliferation of open access journals. The currently dominating, volunteer work model does not easily scale up to large-scale and sustainable operations and the other business models need yet to demonstrate their strengths. Through co-operation or outsourcing of part of the work to commercial companies the publishers of individual journals could obtain the same economies of scale, branding etc, which large commercial publishers have today. This would however require changing the business model from the currently dominating open source model.

### Subject-specific repositories

Subject-specific repositories have evolved in a few select fields. The selection of fields has come about through a combination of existing behavioural infrastructure, individual entrepreneurship and pure chance. The best-known example of such a service is the arXiv service in high-energy physics. Despite its very low running costs, it still costs money to run and the whole service has recently been transferred to Cornell University. The main aim of the European SciX demonstration project is to set up such a disciplinary repository for the field of information technology in architecture and construction and fill it with initial content. This is done, primarily, by negotiating with the organisers of a number of conference series in the subject area to obtain their proceedings from the past few years and also by offering the repository as an easy channel to put new proceedings online.

Discipline-specific repositories are usually rather tightly aligned with pre-existing communities of researchers who communicate a lot with each other, meet at regular conferences and publish in a limited number of journals. It would be very difficult for such repositories to start either to charge subscription fees or to start to levy fees on authors uploading their papers (one aspect is that the payments would be rather small per upload and the transaction costs could consume most of the generated gross income). Thus, the main options left would be subsidies from hosting universities or advertisement. Probably the dependency on voluntary work will prevail.

### Institutional Repositories

Discipline-specific repositories will in the longer run be 'threatened' by institutional repositories, since both compete for the same material. If institutional repositories gain momentum and are indexed effectively through standards such as the [Archives Initiative - Protocol for Metadata Harvesting](#oai#), they will offer a parallel channel to the same content as subject-specific repositories, and have clear advantages in their business models.

From the business model viewpoint the development of institutional repositories will depend a lot on the political decisions universities have to make concerning the future roles in the electronic world of their libraries and publishing departments. Since the need for storing and handling paper copies of material from the outside decreases very rapidly the finance thus freed could be used to finance the institutional repositories instead.

## Indexing services and standards

### Open access journals

One of the major drawbacks of open access journals so far has been that they rarely have been indexed in the commercial indexing services for searching quality-assured publications, which universities provide to their researchers and students. Information about the publications in the journals has instead been spread through direct e-mail marketing among select communities of academics and through being indexed by general Web search engines. Partly this has been because of a view that existing scientific indexing services belong to the old establishment and that there is no need for their intermediation. Partly editors of relatively young and experimental journals have had a hard time getting their journals included in such services.

Indexing services fulfil in this connection a dual role in helping the marketing of the journal and its content. First, they help in attracting occasional readers who may not even be aware of the journal's existence. Secondly, the fact that a journal can claim being 'indexed in' lends prestige to the journal and thus helps in attracting more and better submissions. A particularly important one is the Science Citation Index (and the accompanying Social Sciences and Arts and Humanities indexes). This service regularly monitors a selection of a few thousand of the most important refereed journals and counts statistics of the citations in the articles that these journals publish. The more citations there are to a journal's articles in the other journals in this 'core selection', the higher is a journal's impact factor. Academic appointment and grant committees take these impact factors into consideration when ranking the output of academics and, thus, there are high rewards for publishing in such journals.

The use of SCI by university administrations as a decision support tool has become one of the strongest barriers to the success of open access journals, since it tends to strongly favour old established journals ([Guédon 2001](#guedon)). It is very difficult to get new journals accepted in SCI before they have established a reputation, and being outside the 'core literature' of SCI makes it very difficult to get good submissions and establish a reputation.

### Subject-specific repositories

Subject-specific repositories have usually not experienced any need to be indexed by third parties. First, it would be very difficult since most of the material in them is or will be published elsewhere, and thus the references should be to those primary sources. Secondly, subject-specific repositories strive to compete (in terms of coverage) with commercial indexing and full-text services rather than work in symbiosis with them.

The emergence of the OAI-Protocol for Metadata Harvesting (discussed below) may, however, change the situation in the near future.

### Institutional Repositories

Indexing services and the standards that underpin them are crucial to the success of institutional repositories. Since each university contributes only a very small fraction of the total publication output of the academic world, and since, in addition, this output is widely spread over a large number of disciplines, browsing through the user interface of a particular institutional repository is relatively uninteresting, unless for very targeted searches for the output of a particular researcher. A typical usage scenario would be to use the IR to get a free electronic copy of an article published in an expensive journal to which the reader has no access, or to get access to a conference paper published in proceedings which might be out of print.

In the same way as for subject-specific repositories it would be difficult for institutional repositories to be indexed in current established indexes for any of the content of which they are the secondary outlet.

The solution to this dilemma is, in general, Web search engines or in a new type of search engine dedicated to scientific Web content. If an author puts an electronic copy of his own publications on his Web pages the main channel to this is already today through general search engines such as Google. Dedicated open access search engines for scientific content which are tagged according to the OAI- Protocol for Metadata Harvesting ([OAI](#oai)) are currently under development.

## Academic Reward System

### Open access journals

The behaviour of academics as they choose to which journals and conferences they submit their papers is conditioned, to a very high degree, by the academic reward system. In most universities, publishing in the leading established journals in one's field is highly rewarded. Often, the systems are quite explicit and include shortlists of journals, numerical weighting schemes etc. Prestige counts much more than wide and rapid dissemination, and easy access. It has been pointed out that the growth in the number of journal titles and the emergence of strong commercial players in scientific journal publishing in the latter half of the 20th century was due more to a demand from authors for outlets for the papers they needed to have published in peer-reviewed serials, than for a need of readers of additional titles ([Cox, 2003](#cox)). The tenure systems in many countries and periodic comparisons of the scholarly output of university departments are strong motivating forces for this demand. This system naturally puts academics (and in particular the younger ones) in a situation where primary publishing of their best work in relatively unknown open access journals is a very low priority.

A system such as this places any new journals, whether subscription-based or open access, in a disadvantaged position. Only if the journal manages to get a sufficient influx of high-quality papers does it stand a chance of entering into the group of journals with high prestige, and even then after a delay of several years.

It is probably idealistic to expect the whole academic community to change its evaluation system, to take better account of the benefits offered by open access. The experiences of the past ten years show also that it is very difficult for new OA journals to become first rank journals in their fields. An obvious shortcut is if established journals would change their business models and become open access, but despite isolated examples, this is unlikely to happen on a larger scale as long as publishing is as profitable a business as it is today.

### Subject-specific repositories

The success or failure of subject-specific repositories has relatively little to do with the academic reward system, since few important items are published in the repositories alone. Authors upload their manuscripts to these in order to get more efficient and faster dissemination of their publications, which also appear elsewhere for reasons outlined above. If such dissemination leads to them being read, and, in particular, referenced more often, this could indirectly have a positive effect on their academic status and provide an incentive for uploading. It is difficult to envisage more direct rewarding mechanisms.

### Institutional repositories

Institutional repositories can function both as primary and secondary channels. As for the first function (for instance PhD theses and working paper series) filling them with content is unproblematic. The wide use of institutional repositories for secondary publication, however, will demand a number of measures. Scientists and their departments can be rewarded financially for posting electronic copies of their work to institutional repositories, or posting copies can be made mandatory, although the latter solution might be very difficult to implement. In the university of the author of this article, for instance, the department of each author currently receives a small sum of money for each published scientific paper provided that the metadata of that publication is registered in the research database of the university. This requirement could be extended to also include the uploading of a pdf copy of the article.

## Marketing and Critical Mass

### Open access journals

Since journal publishing is dependent on getting authors to submit their best papers to the journal in question, marketing and branding are very important for long-term success. The leading journals in many disciplines are brands as strong as Coca-Cola and Mercedes-Benz for other types of products. In addition to individual journals a publisher can also become a brand. In this respect the leading commercial publishers, learned societies and leading universities in particular from the US and UK have an enviable position. Libraries and authors alike find it much easier to accept a new journal from a well-established publisher.

Most OA journals have not yet been established as brands and on the whole the marketing of such journals has been very poor, partly due to lack of resources for marketing, partly because of a lack of understanding of the need for marketing. Many editors of OA journals have idealistically believed that the merits of Open Access and spreading the word by e-mail lists etc., are enough. The recent launch of BioMed Central, which houses around a hundred OA journals, is an exception and this hub might, in the near future, become a sort of brand in itself. Even more spectacular has been the start of the Public Library of Science journal of Biology in October 2003, which managed to become headline news in many media. PLoS has, however, used millions of dollars of its initial grant funding on marketing and includes several Nobel laureates on its editorial board.

There are many ways in which newly established journals can build their prestige. First, the reputation of the editor and the constitution of the editorial board are important. Secondly, attracting enough papers from leading academics early on is important. This can again lead to a positive chain reaction of citations in other articles and journals and eventually (in the long term) inclusion in the SCI.

In the summer of 2002 researchers at Hanken identified 317 active OA-journals ([Gustafsson, 2002](#gustafsson)). In the study three different sources were used, the most important of which was the [UlrichsWeb](http://www.ulrichsweb.com/ulrichsweb/) database. By comparing the number of journals with the total number of scientific peer review journals in UlrichsWeb, it was found that the share of OA-journals of the total number of journals was only 0.7 % and of electronically available titles 1.5 %. Of the new journals founded in the period 1996-99, about every tenth was, however, open access. In Table 2 below, the ten most popular topic areas for OA journals are listed.

<table><caption>

**Table 2: The most popular areas for Open Access journals**</caption>

<tbody>

<tr>

<th>Scientific domain</th>

<th>Number of journals</th>

</tr>

<tr>

<td>Medicine</td>

<td>36</td>

</tr>

<tr>

<td>Mathematics</td>

<td>36</td>

</tr>

<tr>

<td>Education</td>

<td>27</td>

</tr>

<tr>

<td>Law</td>

<td>20</td>

</tr>

<tr>

<td>Sociology</td>

<td>16</td>

</tr>

<tr>

<td>Economics</td>

<td>16</td>

</tr>

<tr>

<td>Computer Science</td>

<td>15</td>

</tr>

<tr>

<td>History</td>

<td>14</td>

</tr>

<tr>

<td>Biology</td>

<td>12</td>

</tr>

<tr>

<td>Information Science</td>

<td>11</td>

</tr>

</tbody>

</table>

The real number of open access peer-reviewed journals can be assumed to be substantially bigger than the numbers in HANKEN's study. It is very difficult to get information about smaller journals and journals publishing in other languages than English. Most of these journals are only known to interested readers in their respective communities, The recently launched [Directory of Open Access Journals](http://www.doaj.org/) tries to improve their marketing by providing university libraries (and scientists) world-wide with up-to date information about available journals. Currently the directory includes some 550 journals.

### Subject-specific repositories

Subject-specific repositories are also highly dependent on the behaviour of authors, but here the dilemma is slightly different. The papers uploaded are typically drafts intended for final publication elsewhere (as conference papers, journal articles, etc.). Thus, researchers are not really forced to make an either/or choice. Instead, they have to make the decision whether it is worth the extra effort to upload their papers to the server. This is dependent on his perception of how widely used the repository is in his research community.

From a marketing viewpoint it is thus very useful if subject-specific repositories can be bundled with other information, which is useful for researchers in the domain, such as conference announcements, e-mail discussion lists, directories of scientists, link lists to freely available educational resources.

No study has been done concerning the global coverage of discipline-specific repositories. There are some lists of such servers ([Open Citation Project](#open)), and in certain areas OA servers contain tens of thousands or even hundreds of thousands of papers. In a systematic study these volumes could be compared to the number of papers contained in the commercial services of some large publishers and aggregators.

### Institutional repositories

Institutional repositories will have much less of a problem with branding than the other two channels. When MIT for instance announces the development of its new institutional repository and that it is offering the software for free to other universities ([DSpace](http://libraries.mit.edu/dspace-mit/)), it is using its brand as a university as a marketing tool and, at the same time, is hoping to further strengthen its brand through this action. Thus, for most well known universities there should be no big problems in marketing the repositories.

In contrast to the individual open access journal and the subject-specific repository, the success of which is measured within a limited community of researchers, the success of an institutional repository is very much dependent on universities world-wide starting similar repositories, and on the contents of these being comprehensible and accessible to interested readers. Thus, these repositories follow the same sort of logic as mobile phones or e-mail addresses, where each new connection adds to the value of each already existing connection. Only when a critical mass on the global scale is achieved (both in terms of number and content) will they offer a competitive channel for scientific content.

The development of institutional repositories is just beginning and their impact is still at this moment extremely marginal.

## Conclusions

_Trying to get researchers to support the move towards open access, which most agree would be good for the advancement of science in principle, is like trying to get people to behave in a more ecological way. While most people recognise the need to save energy and recycle waste it takes much more than just awareness to get them to change their habits on a large scale. It takes a combination of measures of many different kinds, such as technical waste disposal infrastructure, legislation and taxation to get massive behavioural changes underway._

During the discussions at a recent conference a representative of a commercial publisher took a rather negative view of the possibility of the scientific community achieving the sort of collective action needed for a large scale migration to open access. In his view this will never be achieved since it will be impossible to co-ordinate the actions of the many parties involved in removing the barriers. Also Parks ([2002](#parks)) takes the pessimistic view that none of the important groups of actors involved have real incentives for changing the current system.

General awareness of the advantages of open access publishing is naturally a prerequisite for scientists choosing to use OA channels both for primary and secondary publishing and much remains to be done to achieve this. The recent rapid increase in the number of national and international conferences devoted to this theme is promising, but these conferences are mainly attended by the publishing and library community, not the content authors who are in the key position. Branding is extremely important from a marketing viewpoint, and in this respect it is interesting to follow the success or failure of attempts such as BioMed Central and the Public Library of Science. While critical mass on broad scale has not yet been achieved for any of the three channels discussed in this paper, there are examples of sub-communities of scientists where open access plays a significant role. A key issue for efficient indexing of open access material is the success of the OAI-Protocol for Metadata Harvesting ([OAI](#oai)). Its widespread adoption would enable the setting up and in particular filling up with content of OA harvesting services, which would provide good access points worldwide for OA material.

The enthusiasm and iconoclastic spirit of the early days of open access is now changing into a more realistic search for sustainable business models, and a better understanding of the formidable barriers to overcome. The marketplace, both on the supply side and the demand side, will decide the issue in the coming years.

## References:

*   <a id="bjork2003"></a>Björk, B.-C. & Hedlund, T. (2003). Scientific publication life cycle model (SPLC). In S.M. de S. Costa, J.Á Carvalho, A.A. Baptista, and A.C.S. Moreira (Eds.) _[From information to knowledge: Proceedings of the 7th ICCC/IFIP International Conference on Electronic Publishing held at the Universidade do Minho, Portugal 25-28 June 2003.](http://elpub.scix.net/data/works/att/0317.content.pdf)_ Retrieved 11 December, 2003 from http://elpub.scix.net/data/works/att/0317.content.pdf
*   <a id="bjork2000"></a>Björk, B.-C., Turk, Z. (2000). [How scientists retrieve publications: an empirical study of how the Internet is overtaking paper media](http://www.press.umich.edu/jep/06-02/bjork.html). _Journal of Electronic Publishing_, **6**(2). Retrieved 11 December 2003, from http://www.press.umich.edu/jep/06-02/bjork.html
*   <a id="bjork2001"></a>Björk, B.-C.(2001). [Open source, open science, open course ware.](http://www.hut.fi/events/ecaade/E2001presentations/01_01_bjork.pdf) In H. Penttilä (Ed.), _Architectural Information Management, eCAADe 19th Conference, Helsinki University of Technology, Department of Architecture, Espoo 2001_, (pp. 13-17). Helsinki, Helsinki University of Technology. Retrieved 11 December, 2003 from http://www.hut.fi/events/ecaade/E2001presentations/01_01_bjork.pdf
*   <a id="comp"></a>Competition Commission. (2001). [Reed Elsevier plc and Harcourt General, Inc: a report on the proposed merger](http://www.competition-commission.org.uk/rep_pub/reports/2001/457reed.htm). London: Competition Commission. (Cm5184) Retrieved 11 December, 2003 from http://www.competition-commission.org.uk/rep_pub/reports/2001/457reed.htm
*   <a id="cox"></a>Cox, J. (2003). [Value for money in electronic journals: a survey of the early evidence and some preliminary conclusions](http://dx.doi.org/10.1016/S0098-7913(03)00041-8). _Serials Review_ **29**(2), 83-88\. Retrieved 11 December, 2003 from http://dx.doi.org/10.1016/S0098-7913(03)00041-8 [Note - Science Direct site, subscription necessary.]
*   <a id="frazier"></a>Frazier, K. (2001). The librarians' dilemma - contemplating the costs of the 'Big Deal'. _D-Lib Magazine_ **7**(3). Retrieved 11 December, 2003 from http://dx.doi.org/10.1045/march2001-frazier
*   <a id="ginsparg"></a>Ginsparg, P. (1996). _[Electronic publishing in science.](http://arXiv.org/blurb/pg96unesco.html)_ Paper presented at a Conference held at UNESCO HQ, Paris, 19-23 February, 1996, during session _Scientist's View of Electronic Publishing and Issues Raised_, Wednesday, 21 February, 1996\. Retrieved 11 December, 2003 from http://arXiv.org/blurb/pg96unesco.html
*   <a id="goode"></a>Goode, R. (2003). _[Open access initiative from Oxford Journals.](http://www.eurekalert.org/pub_releases/2003-08/oup-oai080703.php)_ Oxford: Oxford University Press. (Press release.) Retrieved 11 December, 2003 from http://www.eurekalert.org/pub_releases/2003-08/oup-oai080703.php
*   <a id="guedon"></a>Guédon, J.C. (2001). [In Oldenburg's long shadow: librarians, research scientists, publishers, and the control of scientific publishing](http://www.arl.org/arl/proceedings/138/guedon.html), In _Creating the digital future. Association of Research Libraries. Proceedings of the 138th Annual Meeting, Toronto, Ontario May 23-25, 2001._ Washington, DC: Association of Research Libraries. Retrieved 5 January, 2003 from http://www.arl.org/arl/proceedings/138/guedon.html
*   <a id="gustafsson"></a>Gustafsson, T. (2002). _Open access - en empirisk undersökning om fritt tillgängliga vetenskapliga journaler på Internet_. Unpublished Master's thesis. Svenska Handelshögskolan, Informationsbehandling, Helsinki.
*   <a id="kling"></a>Kling, R. & McKim G. (2000). Not just a matter of time: field difference and the shaping of electronic media in supporting scientific communication, Paper accepted for publication in Journal of the American Society for Information Science.
*   <a id="kvaern"></a>Kvændrup, H.M.. (2003). _Fremtidens forskningspublicering - et nordisk samarbejde._ Copenhagen: Biblioteksstyrelsen.
*   <a id="lawrence"></a>Lawrence, S. (2001). [Online or invisible?](http://www.neci.nec.com/~lawrence/papers/online-nature01/) _Nature_, **411**(6837), 521\. Retrieved 11 December, 2003 from http://www.neci.nec.com/~lawrence/papers/online-nature01/
*   <a id="mit"></a>Massachusetts Institute of Technology. (2001). [MIT to make nearly all course materials available free on the World Wide Web.](http://Web.mit.edu/newsoffice/nr/2001/ocw.html) _MIT News_, 4.4.2001 Retrieved 11 December, 2003 from http://Web.mit.edu/newsoffice/nr/2001/ocw.html
*   <a id="odlyzko"></a>Odlyzko, A. (1998). [The economics of electronic journals](http://www.press.umich.edu/jep/04-01/odlyzko.html). _Journal of Electronic Publishing_, **4**(1). Retrieved 11 December, 2003 from http://www.press.umich.edu/jep/04-01/odlyzko.html
*   <a id="oft"></a>Office of Fair Trading (2003). [The market for scientific, technical and medical journals: a statement by the OFT, Sept. 2002.](http://www.oft.gov.uk/NR/rdonlyres/efvqwtvbz6bqcpm5vcfexjlnbl7ltb5anbnu7kuri5bhmfwx34kjdjni4qgpivcokrjdmugt4l6ic4kbokoji6a7xgg/oft396.pdf) London: Office of Fair Trading. Retrieved 11 December, 2003 from http://www.oft.gov.uk/NR/rdonlyres/ efvqwtvbz6bqcpm5vcfexjlnbl7ltb5anbnu7kuri5bhmfwx34kjdjni4qgpivcokr jdmugt4l6ic4kbokoji6a7xgg/oft396.pdf
*   <a id="oai"></a>Open Archives Initiative (2003). _[The Open Archives Initiative protocol for metadata harvesting. Version 2](http://www.openarchives.org/OAI/openarchivesprotocol.html)_. Open Archives Initiative. Retrieved 11 December, 2003 from http://www.openarchives.org/OAI/openarchivesprotocol.html
*   <a id="open"></a>Open Citation Project. (2003). [Core metalist of open access eprint archives.](http://opcit.eprints.org/explorearchives.shtml) Open Citation Project. Retrieved 11 December 2003 from http://opcit.eprints.org/explorearchives.shtml
*   <a id="parks"></a>Parks, R. (2002). The Faustian grip of academic publishing. _Journal of Economic Methodology_, **9**(1), 317-335
*   <a id="romeo"></a>RoMEO. Rights MEtadata for Open archiving Project. [http://www.lboro.ac.uk/departments/ls/disresearch/romeo/](http://www.lboro.ac.uk/departments/ls/disresearch/romeo/)
*   <a id="shapiro"></a>Shapiro, C and Varian, H. (1999). Information rules: a strategic guide to the networked economy. Boston, MA: Harvard Business School Press.
*   <a id="walker"></a>Walker, T.J. (1998). Free Internet access to traditional journals. _American Scientist_, **86**(5), 463-471.
*   <a id="wells"></a>Wells, A. (1999). _[Exploring the development of the independent, electronic, scholarly journal.](http://panizzi.shef.ac.uk/elecdiss/edl0001/index.html)_ Unpublished Master's thesis, University of Sheffield, Sheffield. Retrieved 11 December, 2003 from http://panizzi.shef.ac.uk/elecdiss/edl0001/index.html