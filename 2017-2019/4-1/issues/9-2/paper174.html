<!DOCTYPE html>
<html lang="en">

<head>
	<title>A longitudinal study of Web pages continued: a consideration of document persistence. Web documents,
		Half-life, Linkrot, Persistence, Web citations</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="dc.title"
		content="A longitudinal study of Web pages continued: a consideration of document persistence">
	<meta name="dc.creator" content="Wallace Koehler">
	<meta name="dc.subject" content="Page persistence and the stability of Web documents">
	<meta name="dc.description"
		content="It is well established that Web documents are ephemeral in nature. The literature now suggests that some Web objects are more ephemeral than others. Some authors describe this in terms of a Web document half-life, others use terms like 'linkrot' or persistence. It may be that certain 'classes' of Web documents are more or less likely to persist than are others. This article is based upon an evaluation of the existing literature as well as a continuing study of a set of URLs first identified in late 1996. It finds that a static collection of general Web pages tends to 'stabilize' somewhat after it has 'aged'. However 'stable' various collections may be, their instability nevertheless pose problems for various classes of users. Based on the literature, it also finds that the stability of more specialized Web document collections (legal, educational, scientific citations) vary according to specialization. This finding, in turn, may have implications both for those who employ Web citations and for those involved in Web document collection development.">
	<meta name="dc.subject.keywords" content="Web pages, persistence, half-life, Link rot, Web citations">
	<meta name="robots" content="all">
	<meta name="dc.publisher" content="Professor T.D. Wilson">
	<meta name="dc.coverage.placename" content="global">
	<meta name="dc.type" content="text">
	<meta name="dc.identifier" scheme="ISSN" content="1368-1613">
	<meta name="dc.relation.IsPartOf" content="http://InformationR.net/ir/9-2/infres92.html">
	<meta name="dc.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dc.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-9-no-2-january-2004">Vol. 9 No. 2, January 2004</h4>
	<h1 id="a-longitudinal-study-of-web-pages-continued-a-consideration-of-document-persistence">A longitudinal study of
		Web pages continued: a consideration of document persistence</h1>
	<h4 id="wallace-koehler"><a href="mailto:wkoehler@valdosta.edu">Wallace Koehler</a></h4>
	<p>Valdosta State University<br>
		Valdosta, Georgia, USA</p>
	<h4 id="abstract"><strong>Abstract</strong></h4>
	<blockquote>
		<p>It is well established that Web documents are ephemeral in nature. The literature now suggests that some Web
			objects are more ephemeral than others. Some authors describe this in terms of a Web document half-life,
			others use terms like 'linkrot' or persistence. It may be that certain 'classes' of Web documents are more
			or less likely to persist than are others. This article is based upon an evaluation of the existing
			literature as well as a continuing study of a set of URLs first identified in late 1996. It finds that a
			static collection of general Web pages tends to 'stabilize' somewhat after it has 'aged'. However 'stable'
			various collections may be, their instability nevertheless pose problems for various classes of users. Based
			on the literature, it also finds that the stability of more specialized Web document collections (legal,
			educational, scientific citations) vary according to specialization. This finding, in turn, may have
			implications both for those who employ Web citations and for those involved in Web document collection
			development.</p>
	</blockquote>
	<h2 id="introduction">Introduction</h2>
	<p><a href="http://informationr.net/ir/4-4/paper60.html">The World Wide Web still is not a library</a>. This paper
		offers a limited set of findings derived from the same set of URLs that have been monitored continuously since
		1996. These findings have been elaborated and reported in articles published in 1999 and 2002 (<a
			href="#koe99a">Koehler 1999a</a>, <a href="#koe99b">Koehler 1999b</a>, and <a href="#koe02">Koehler
			2002</a>). The first two articles reported the behaviour of 360 Web pages and 343 Web sites for a 53 week
		period from December 1996 to January 1998. The third paper follows the same Web pages from December 1996 to
		February 2001, or 214 weeks. This paper extends the analysis to May 2003.</p>
	<p>Despite our growing comfort and familiarity with the Web both as a medium to which to publish to as well as a
		conduit for and as a tool for the management of information maintained in other environments, there is still a
		tendency to confuse one function with the other. At the same time, initiatives like the <a
			href="http://www.archive.org/">Internet Archive</a> that seek to 'save' Web artifacts in a large,
		generalized digital library or search engine caches such as those generated by Google or Yahoo compete with and
		sometimes create potential confusion with the 'originals.'</p>
	<p>It is now well documented that Web pages and Web sites come and go; and that somewhat more rarely, they may come
		back again. This propensity for Web documents to be created then disappear has sometimes been labelled in the
		literature as the Web page or Web site half-life. As <a href="#half-life">we shall see below</a>, different
		types of material appear to have different half-lives. A half-life is that period of time required for half of a
		defined Web literature to disappear (or for one group of isotopes or atoms to decay into another).</p>
	<p>A second concern has less to do with half-lives and the continuing existence of any given URL, but rather with
		the stability of the content of persisting Web documents. The Internet Archive, for example, can allow us to
		compare Web page content at different points in time, just as one might compare different editions of the same
		title. There are a number of structural factors that appear to contribute to the prediction of content stability
		(see e.g. <a href="#cassbyrd">Casserly &amp; Byrd 2003</a> or <a href="#koe99a">Koehler 1999a</a>). In some very
		fundamental ways, content stability is of far greater importance to at least certain elements of the information
		professions than is document stability.</p>
	<p>This paper does not dwell on content stability issues. Its focus is Web page persistence and reports findings
		from a continuing longitudinal study extending more than 325 weeks from December 1996 to May 2003. It is, I
		believe, the longest continuous study of a single set of URLs. Most URL longevity studies are relatively short
		and are monitored over a period of days, weeks, or sometimes months. Furthermore, if the literature suggests
		interest, there is a greater interest in document stability than in content stability—there are far more papers
		published on the former than the latter.</p>
	<p>If we are to understand the dynamics of the Web as a repository of knowledge and culture, we must monitor the way
		in which that knowledge and culture is managed. We find that the Web in its 'native form' is a far too
		transitory medium and that the contributions of the Internet Archive and institutions like it are absolutely
		essential to the preservation process. Or we may find that the Web represents a more or less stable medium in
		some areas but is less so in others.</p>
	<h2 id="linkrot-half-lives-and-the-literature">Linkrot, half-lives, and the literature</h2>
	<p>It is now well established in the literature that Web pages are ephemeral. Kitchens and Mosley (<a
			href="#kitchens">2000</a>), for example question the utility of printed Internet guides since the Web
		references are far too ephemeral. Taylor and Hudson (<a href="#taylor">2000</a>) explore printed biographies of
		URLs and Web lists. They find variation among domain types and subject collections. Benbow (<a
			href="#benbow">1998</a>) found an attrition rate for Web resources of 20% and 50% over two and three year
		periods. Germain (<a href="#germain">2000</a>) questions URLs as citations for scholarly literature for the same
		reason. McMillan (<a href="#mcmillian">2000</a>) argues that content analysis tools can be brought to the Web,
		but there are problems unique to the method because of the ephemeral nature of the target. The phenomenon has
		been given a variety of names: 'broken links' (<a href="#markwell">Markwell &amp; Brooks 2002</a>; <a
			href="#kobayaski">Kobayashi &amp; Takeda 2002</a>), 'linkrot' (<a href="#denemark">Denemark 1996</a>; <a
			href="#taylor">Taylor and Hudson 2000</a>), 'link rot,' (<a href="#fichter">Fichter 1999</a>; <a
			href="#markwell">Markwell &amp; Brooks 2003</a>), or 'decay and failure' (<a href="#spinellis">Spinellis</a>
		2003). Rumsey (<a href="#rumsey">2002</a>) likens the use of the Web in legal research and its ephemeral nature
		to a 'runaway train'. Linkrot may have become the accepted spelling as it so appears in the online <a
			href="http://www.webopedia.com/TERM/L/linkrot.html">Webopedia</a> although other online sources also provide
		the variant spelling.</p>
	<p>Harter and Kim (<a href="#harter">1996</a>) are perhaps among the first of a handful researchers documenting the
		impact of the ephemeral nature of the Web on citations and citation systems. They examined scholarly e-journal
		articles published primarily in 1995 - but some as early as 1993 - and found that between the writing of the
		articles and their analysis (1995), a third of the URLs were no longer viable. Nelson and Allen (<a
			href="#nelson">2002</a>) surveyed digital library (DL) objects accessible via the Web. Placement in digital
		libraries, they hypothesize:</p>
	<blockquote>
		<p>...is indicative of someone's desire to increase the persistence and availability of an object, we expect DL
			objects to survive longer, change less, and be more available than general WWW content.</p>
	</blockquote>
	<p>Nelson and Allen report a 3% attrition rate over their sample year. Rumsey (<a href="#rumsey">2002</a>) addresses
		the legal literature and its citations. She notes that citations to electronic media have increased dramatically
		since 1995 from less than 5% of all citations to nearly 30% by 2001. She reports that citation viability
		declines rapidly. Of citations tested in mid 2001, 39% of electronic citations dated 2001 failed, 37% dated
		2000, 58% dated 1999, 66% dated 1998, and 70% dated 1997 (<a href="#rumsey">Rumsey 2002</a>: 35).</p>
	<p>Markwell and Brooks (<a href="#markwell">2002</a>) concerned themselves with the online literature employed by
		the scientific community and more specifically in biochemistry and molecular biology (<a
			href="#markwell">Markwell &amp; Brooks 2003</a>) for education purposes. They too find significant erosion
		in URL viability and estimate URL half-lives for these specific science education resources of some 4.6 years.
	</p>
	<p>Taylor and Hudson (<a href="#taylor">2000</a>), exploring reference issues, demonstrate wide variability across
		academic subject area and the top-level domains of URLs.</p>
	<table>
		<caption>
			<p><strong>Table 1: Resource half-lives by resource type</strong>
		</caption>
		</p>
		<tbody>
			<tr>
				<th>Study</th>
				<th>Resource type</th>
				<th>Resource half-life</th>
			</tr>
			<tr>
				<td>
					<p>Koehler (<a href="#1999a">1999</a> and <a href="#2002">2002</a>) 
				</td>
				</p>
				<td>
					<p>Random Web pages
				</td>
				</p>
				<td>
					<p>about 2.0 years
				</td>
				</p>
			</tr>
			<tr>
				<td>
					<p>Nelson and Allen (<a href="#nelson">2002</a>) 
				</td>
				</p>
				<td>
					<p>Digital Library Object
				</td>
				</p>
				<td>
					<p>about 24.5 years
				</td>
				</p>
			</tr>
			<tr>
				<td>
					<p>Harter and Kim (<a href="#harter">1996</a>) 
				</td>
				</p>
				<td>
					<p>Scholarly Article Citations 
				</td>
				</p>
				<td>
					<p>about 1.5 years
				</td>
				</p>
			</tr>
			<tr>
				<td>
					<p>Rumsey (<a href="#rumsey">2002</a>)
				</td>
				</p>
				<td>
					<p>Legal Citations 
				</td>
				</p>
				<td>
					<p>about 1.4 years
				</td>
				</p>
			</tr>
			<tr>
				<td>
					<p>Markwell and Brooks (<a href="#brooks">2002</a>)
				</td>
				</p>
				<td>
					<p>Biological Science Education Resources 
				</td>
				</p>
				<td>
					<p>about 4.6 years
				</td>
				</p>
			</tr>
			<tr>
				<td>
					<p>Spinellis (<a href="#spinellis">2003</a>)
				</td>
				</p>
				<td>
					<p>Computer Science Citations 
				</td>
				</p>
				<td>
					<p>about 4.0 years (p. 74)
				</td>
				</p>
			</tr>
		</tbody>
	</table>
	<p>The Nelson and Allen (<a href="#nelson">2002</a>) study offers perhaps a useful baseline against which to measure
		URL viabilities. They chose to measure object attrition in what they assumed a priori to be a stable
		environment. Their findings support their assumptions. It may be presumptuous for me to argue that my work could
		represent the 'right boundary' of longevity behaviour. The Harter and Kim (<a href="#harter">1996</a>) study
		suggests early URL 'instability'. However, the study methodology as reported is insufficiently precise to allow
		precise calculations of half-lives. Our data suggest one of two possibilities. Either the online scientific
		literature has grown more stable since sampled in 1995 by Harter and Kim, or the biological literature used for
		teaching and the computer science literature as reported by Markwell and Brooks (<a href="#markwell">2002</a>)
		and Spinellis (<a href="#spinellis">2003</a>) are exceptions to the general rule. We suspect the former, that
		the online scientific literature has become more 'stable'. We would also caution that an increase in the
		half-life of that online literature from about 1.5 years to about four years is no major gain.</p>
	<h2 id="methodology">Methodology</h2>
	<p>The original data set was collected between December 1996 and May 2003 to map Web page change over time. Data are
		collected weekly and include page size (in kilobytes) and link changes for Web pages. A number of attributes
		were examined to assess the growth, change, and death of those Web pages. From December 1996 until February
		2001, FlashSite 1.01, a software product of Incontext was employed throughout the study for data capture.
		Beginning in January 2002, <a href="http://www.link-checker-pro.com/">KyoSoft's Link Checker Pro</a> was
		employed for data capture. For a variety of reasons, including systems failures, the inability of more recent
		versions of Windows software to support Flashsite, and other factors, data were not collected for the majority
		of 2001.</p>
	<p>A sample of 361 URLs was collected in the last two weeks of December 1996 using the now defunct WebCrawler random
		URL generator. In late 1996, the Web contained an estimated 100 to 600 million pages (<a
			href="#1999a">Koehler1999a</a>). The sample was stratified to correspond to the reported distribution of Web
		pages by top-level domain at the time.</p>
	<p>This method relied on a single search engine's index to generate the sample and is similar to that similar to
		that reported by Bharat &amp; Broder (<a href="#bharat">1998</a>) in that the sample harvest was based on a
		single search engine. Other approaches to develop random samples includes a pool of URLs (Bray 1996), randomly
		generated IP numbers (Lawrence and Giles <a href="#lawrence">1998</a>, <a href="#giles">1999</a>) and a random
		walk of the Web using multiple search engines (<a href="#henzinger">Henzinger <em>et al.</em> 2000</a>).</p>
	<p>The 361 Web page sample was as random a representation of the Web as a whole as the WebCrawler index was
		representative of the Web as a whole in December 1996. The sample is not representative of the Web as a whole in
		2003 nor is it intended to be. It may reflect the status of Web pages that existed in late 1996 and continue to
		do so.</p>
	<p>Data were harvested employing two different commercial software packages. The use of FlashSite was discontinued
		when the program was no longer supported and updated by its producer. The product does not function or function
		well in Windows environments above Windows95. KyoSoft's Link Checker Pro has been adopted as the link check and
		data harvest software.</p>
	<p>Link Checker Pro provides a text or spreadsheet cumulative and individual report of the status of hypertext links
		embedded in a Website. In order to use Link Checker Pro, all URLs randomly collected in December 1996 for the
		original research project were encoded as part of a Web page and placed on the Web. That Webpage is then scanned
		weekly. The software provides an Excel spreadsheet with aggregate data as well as for each individual URL.</p>
	<p>Link Checker Pro provides the similar data to FlashSite as well as the date the site was last modified. In
		addition, Link Checker Pro reports the status of the missing URL: whether the request was forbidden or
		unauthorized (401 or 403 errors), the much more common a file not found error (the 404 error), and the no domain
		name server error (no DNS). The first two sets of errors indicate a restructuring or removal of content from the
		file structure. The latter error means either the disappearance or down status of a domain or Website.</p>
	<p>For a number of technical reasons, there are some substantial temporal gaps. These include the transition from
		one operating system to another and therefore one analytic program to another. It also represents the rigours of
		a move and research time lost in that transition. And finally, I suffered a systems crash. The data are
		nevertheless sufficiently continuous and contiguous to permit analysis.</p>
	<h2 id="an-assessment-of-research-findings">An assessment of research findings</h2>
	<p>It is suggested in the literature discussed above that the stability of Web site may be a function of URL form,
		domains, or of other factors. We suggest here that Web page persistence is somewhat more complex. Table 2 shows
		the distribution by level of the responding Web page collection when first collected in December 1996 and the
		remaining collection in mid-February 2001 and in May 2003.</p>
	<p>In December 1996 about half the sample consisted of navigation pages and half of content, after four years
		navigation pages represented more than 60% of the remaining sample. McDonnell <em>et al.</em> (<a
			href="#mcdonnell">2000</a>) have defined navigation pages as those pages that serve to guide the user
		through a Web site to the information the site was created to provide. They define content pages as those pages
		providing that information. They report that navigation pages are most often found at the server-level domain or
		one level removed (www.aaa.tld and www.aaa.tld/xxx).</p>
	<table>
		<caption>
			<p><strong>Table 2: Extant sample—distribution by level in percentages</strong><br>
				chi-square=18.95, df=2, p&lt;=.001
		</caption>
		</p>
		<tbody>
			<tr>
				<th> </th>
				<th>Dec 1996</th>
				<th>Feb 2001</th>
				<th>May 2003</th>
			</tr>
			<tr>
				<td>Navigation</td>
				<td>50.4</td>
				<td>61.3</td>
				<td>72.2</td>
			</tr>
			<tr>
				<td>Content</td>
				<td>49.6</td>
				<td>38.7</td>
				<td>27.8</td>
			</tr>
			<tr>
				<th>Total N</th>
				<td>361</td>
				<td>124</td>
				<td>122</td>
			</tr>
		</tbody>
	</table>
	<p>While we see very little change in the size of the remaining sample between February 2001 and May 2003, there has
		been a continued change in the distribution between navigation and content pages. As predicted by Koehler (<a
			href="#koe02">2002</a>), navigation pages have shown greater 'resilience' than have content pages over time.
		As already indicated, in December 1996, about half the sample consisted of navigation and content pages.
		However, in May 2003, two-thirds of the sample had gone. Of these, three-quarters of the sample that remained
		constituted navigation pages. We know from experience and <em>ad hoc</em> observation that not all content pages
		are 'gone'; they are, rather, re-addressed on the file structure. They are on occasion, 'intermittent', that is
		to say, they may, for a variety of reasons, 'come and go' (<a href="#koe02">Koehler 2002</a>). From a
		statistical perspective, the six year distribution (1996-2003) represents a significant difference in the
		distribution of content and navigation pages. Although the trend continues in the 2001-2003 period, these data
		do not represent a statistically significant difference (chi-square=3.3, df=1, p&lt;=.10).</p>
	<p>It is not unreasonable to ask, from a bibliographical perspective, whether a Web document, once moved from one
		address to another remains the same document or whether it is metamorphosed into something different. If at the
		same time that the document was moved its content was changed, we would probably have little problem defining it
		as <em>different</em>. How shall we treat it when content is either not changed or only very slightly changed?
		Is each modification a new edition?</p>
	<table>
		<caption>
			<p><strong>Table 3: Extant sample—distribution by implied TLD in percentages</strong><br>
				chi-square=17.3, df=12, p&lt;=.20
		</caption>
		</p>
		<tbody>
			<tr>
				<th>Top level domain</th>
				<th>Dec 1996</th>
				<th>Feb 2001 </th>
				<th>May 2003</th>
			</tr>
			<tr>
				<td>com</td>
				<td>32.1</td>
				<td>37.9</td>
				<td>26.4</td>
			</tr>
			<tr>
				<td>edu</td>
				<td>29.4</td>
				<td>28.2</td>
				<td>24.0</td>
			</tr>
			<tr>
				<td>gov</td>
				<td>5.0</td>
				<td>4.0</td>
				<td>9.1</td>
			</tr>
			<tr>
				<td>ccTLD (uncl)</td>
				<td>16.3</td>
				<td>9.8</td>
				<td>9.9</td>
			</tr>
			<tr>
				<td>mil</td>
				<td>3.3</td>
				<td>4.8</td>
				<td>1.7</td>
			</tr>
			<tr>
				<td>net</td>
				<td>10.5</td>
				<td>10.5</td>
				<td>24.0</td>
			</tr>
			<tr>
				<td>org</td>
				<td>3.3</td>
				<td>4.8</td>
				<td>5.0</td>
			</tr>
			<tr>
				<th>Total N</th>
				<td>361</td>
				<td>124</td>
				<td>122</td>
			</tr>
		</tbody>
	</table>
	<p>While there has been a change in the distribution of navigation and content pages, a similar conclusion cannot be
		reached for Web pages classed by top-level domain (TLD). Table 2 presents data for Web pages according to
		'implied' TLD. An 'implied TLD' is a country code TLD (ccTLD) 'reclassed' as a generic TLD (gTLD) for purposes
		of analysis based on the second level domain (2LD)—for example co.jp as commercial, ac.up as educational, or.cr
		as organizational, and gob.mx as governmental. Those that cannot be 'reclassed', are not. The ccTLDs are those
		that carry a country or regional abbreviation as the TLD. The gTLDs are those that carry the seven original and
		seven newer 'areas of competence or application' abbreviations - .com for commercial, .edu for educational,
		.museum for museums and so on.</p>
	<p>Perhaps there been shifts in the sample for some gTLDs, the net domain, for example. On balance (and
		statistically), it would appear that the distribution in the sample by gTLD and implied gTLD has remained
		relatively unchanged. This implies that the distribution of Web documents by TLD that existed at one point in
		time does not change over time for that same cohort of documents, the distribution of documents that existed in
		1996 and continue to exist in 2003, are similar. That does not mean, however, that the distribution of documents
		that existed in 1996 and that existed in May 2003 are the same. It has been well established that the size of
		the Web, the source of documents, and the number of documents have increased dramatically, perhaps geometrically
		over that period.</p>
	<p>These data suggest that as a static URL collection ages, it may become in time relatively stable. Figure 1,
		borrowed from Koehler (<a href="#2002">2002</a>) shows the rate at which the URL set eroded between December
		1996 and February 2001.</p>
	<figure>
		<img src="p174fig1.gif" alt="Figure 1: 'Comotose' Web pages">
		<figcaption>
			<strong>Figure 1: 'Comotose' Web pages</strong>
		</figcaption>
	</figure>
	<p>In December 1996, 100% of the sample was &quot;present,&quot; but by February 2001, it had eroded to 34.4 percent
		of its original size. By May 2003, it had been reduced to only 33.8 percent of the original sample size, in,
		however, as Tables 1 and 2 suggest, a somewhat restructured configuration. This restructuring can result, as
		Koehler (<a href="#2002">2002</a>) reports, from the periodic resurrection of Websites and Web pages, sometimes
		after protracted periods of time.</p>
	<figure>
		<img src="p174fig2.gif" alt="Figure 2: 'Comotose' Web pages, weeks 254 to 326">
		<figcaption>
			<strong>Figure 2: 'Comotose' Web pages, weeks 254 to 326</strong>
		</figcaption>
	</figure>
	<p>As is shown in Figure 3, 'missing', or 404 error continues to be the primary cause for Web page attrition in the
		sample. The legend in Figure 3 lists four 'causes' for URL failure: missing, restricted access, bad, and
		unknown. Missing signified the 'file not found or 404 error'; restricted access the '401 unauthorized and 403
		forbidden errors'; bad, the no domain name server fault; and other, all others. In recent months there appears
		to be a slight trend for a slight increase in DNS errors which may not be too surprising in a sample of Web
		sites that is over six years old.</p>
	<figure>
		<img src="p174fig3.gif" alt="Figure 3: Web page attrition">
		<figcaption>
			<strong>Figure 3: Web page attrition</strong>
		</figcaption>
	</figure>
	<h2 id="conclusions">Conclusions</h2>
	<p>Based on a relatively small number of studies, we can conclude that the Web documents are not particularly stable
		media for the publication of long-term information and the maintenance of individual objects or items. That
		said, we must distinguish between material published to the Web and material for which the Web serves as a
		conduit for access. For example, Nelson and Allen (<a href="#nelson">2002</a>) indicate long half-lives for
		online databases, where others suggest far shorter half-lives for Web published resources.</p>
	<p>There are two interesting trends that emerge from this analysis. First, once a collection has sufficiently aged,
		it may stabilize in the sense at least that its URLs may become more durable in time. We have shown, for
		example, that Koehler's collection of randomly collected URLs remained in a fairly 'steady-state' for two years
		after it lost approximately two-thirds of its population over a four year period. From a collection development
		perspective, this period of stability has been but of short duration. Additional monitoring is needed to
		establish resource lifetimes.</p>
	<p>Second, it is equally interesting to find that that the half-lives of Web resources in different disciplines,
		domains, and fields differ. First, not only are legal, scholarly, and educational electronic citations reported
		to have limited lifecycles not dissimilar to Web resources in general, but there is also variability among the
		disciplines.</p>
	<p>The discussion thus far has focused on URL sets monitored over specific periods of time to determine to
		probability of linkrot and other variations of decline in citation efficacy. There have been a number of
		initiatives to try to address these matters either through strategic design or by application of some form link
		checking technology. The <a href="http://www.scholarly-societies.org">Scholarly Societies Project</a> at the
		University of Waterloo is among the first to have adopted a collection strategy based on the format of the URL.
		They have observed that canonical URLs - those that took the form <em><strong>www.orgname.org</strong></em> and
		<em><strong>www.orgname.org.cc</strong></em> are more likely to persist than other non-canonical forms. Thus, as
		a strategic collection decision, one might preclude Web documents that do not meet some 'persistence test' such
		as the canonical URL in an effort to build more stable collections.</p>
	<p>Second, one might elect to link to the index page, or what we have labelled navigation pages here. It is not
		uncommon for any given domain to undergo wholesale restructuring. In recent months, for example, the American
		Library Association, owner and manager of www.ala.org completely changed the file tree with &quot;catastrophic
		consequences&quot; for many of us maintaining links to <strong>ala.org</strong>. Similarly, when management of
		the International Federation of Library Associations and Institutions (<a href="http://www.ifla.org">IFLA</a>)
		Web site migrated from Canada to France, the IFLA Committee on Free Access to Information and Freedom of
		Expression (<a href="http://ifla.org/faife/">FAIFE</a>) Web site was subsumed under the IFLA umbrella and
		converted from <strong>faife.dk</strong> to <strong>ifla.org/faife/</strong>. This change had particular
		consequences for the stability of one monitored collection I maintain.</p>
	<p>Links can be checked and corrected. I have adopted KyoSoft's Link Checker Pro to check the links periodically at
		my little web library and information professional association Web site and to prepare a report of its status.
		On a much larger scale, <a href="http://www.oclc.org/connexion/">OCLC Connexion</a> - subsuming the functions of
		the Cooperative Online Resource catalogue or CORC - provides a cataloguing system for electronic resources,
		including Web pages. It includes a periodic check for resource stability and catalogue maintenance.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="bharat"></a>Bharat, K. &amp; Broder, A. (1998). <a
				href="http://www7.scu.edu.au/programme/fullpapers/1937/com1937.htm">A technique for measuring the
				relative size and overlap of public Web search engines</a>. In: <em>Proceedings of the 7th International
				World Wide Web Conference</em>. (pp. 379-388). Amsterdam: Elsevier Science.</li>
		<li><a id="benbow"></a>Benbow, S.M.P. (1998). File Not Found: the problem of changing URLs for the World Wide
			Web. <em>Internet Research: Network Applications and Policy</em> <strong>8</strong>(3), 247-250.</li>
		<li><a id="bray"></a>Bray, T. (1996). <a
				href="http://www5conf.inria.fr/fich_html/papers/P9/Overview.html">Measuring the Web.</a> <em>World Wide
				Web Journal</em>, <strong>1</strong>(3). Retrieved 24 December, 2003 from
			http://www5conf.inria.fr/fich_html/papers/P9/Overview.html [Also found at http://www.w3j.com/3/s3.bray.html,
			but without images.]</li>
		<li><a id="cassbyrd"></a>Casserly, M. &amp; Byrd, J. (2003). Web citation availability: analysis and
			implications for scholarship. <em>College and Research Libraries</em>, <strong>64</strong>(4), 300-317.</li>
		<li><a id="denemark"></a>Denemark, H. (1996) The death of law reviews has been predicted: What might be lost
			when the last law review shuts down? <em>Seton Hall Law Review</em>, <strong>27</strong>(1), 1-32.</li>
		<li><a id="douglis"></a>Douglis, F, Feldmann, A., &amp; Krishnamurthy, B. (1997) <a
				href="http://www.usenix.org/publications/library/proceedings/usits97/full_papers/douglis_rate/douglis_rate_html/douglis_rate.html">Rate
				of change and other metrics: a live study of the World Wide Web.</a> In <em>USENIX Symposium on Internet
				Technologies and Systems, December 8-11, Monterey, CA.</em> (pp. 147-158). Berkeley, CA: USENIX.
			Retrieved 24 December, 2003 from
			http://www.usenix.org/publications/library/proceedings/usits97/full_papers/douglis_rate/douglis_rate_html/douglis_rate.html
		</li>
		<li><a id="fichter"></a>Fichter, F. (1999) Do I look like a maid? Strategies for preventing link rot.
			<em>Online</em>, <strong>23</strong>(5), 77-79.</li>
		<li><a id="germain"></a>Germain, C.A. (2000). URLs: uniform resource locators or unreliable reliable resource
			locators? <em>College and Research Libraries</em> <strong>61</strong>(4), 359-365.</li>
		<li><a id="harter"></a>Harter, S and Kim, H. (1996) <a
				href="http://informationr.net/ir/2-1/paper9.html">Electronic journals and scholarly communication: a
				citation and reference study.</a> <em>Information Research</em> <strong>2</strong> (1) paper 9.
			Retrieved May 1, 2003 from http://informationr.net/ir/2-1/paper9a.html</li>
		<li><a id="henzinger"></a>Henzinger, M., Heydon, A., Mitzenmacher, M. &amp; Najork, M. (2000). <a
				href="http://www9.org/w9cdrom/88/88.html#BB">On near-uniform URL sampling</a>. In, Proceedings of the
			Ninth International World Wide Web Conference (WWW9), May 15-19, 2000, Amsterdam. New York, NY: Elsevier
			Science B.V. Retrieved July 7, 2001 from http://www9.org/w9cdrom/88/88.html#BB</li>
		<li><a id="kitchens"></a>Kitchens, J.D. &amp; Mosley, P.A. (2000). Error 404: or, WWhat is the shelf-life of
			printed Internet guides? <em>Library Collections, Acquisitions &amp; Technical Services</em>,
			<strong>24</strong>(4), 467-478.</li>
		<li><a id="kobayashi"></a>Kobayashi, M &amp; Takeda, K. (2002) Information retrieval on the Web. <strong>ACM
				Computing Surveys</strong>, <strong>32</strong>(2), 144-173.</li>
		<li><a id="koe99a"></a>Koehler W. (1999a) An Analysis of Web page and Web site constancy and permanence.
			<em>Journal of the American Society for Information Science</em> <strong>50</strong>,(2), 162-180.</li>
		<li><a id="koe99b"></a>Koehler, W. (1999b)<a id="1999b"></a> <a
				href="http://informationr.net/ir/4-4/paper60.html">Digital libraries and World Wide Web sites and page
				persistence.</a> <em>Information Research</em>, <strong>4</strong>(4) Retrieved 24 December, 2003 from
			http://informationr.net/ir/4-4/paper60.html</li>
		<li><a id="koe02"></a>Koehler, W. (2002). Web page change and persistence - a four-year longitudinal study.
			<em>Journal of the American Society for Information Science and Technology</em>, <strong>53</strong>(2),
			162-171.</li>
		<li><a id="lawrence"></a>Lawrence, S. &amp; Giles, C.L. (1998). Searching the World Wide Web. <em>Science</em>,
			<strong>280</strong>(536), 98.</li>
		<li><a id="giles"></a>Lawrence, S. &amp; Giles, C.L. (1999). Accessibility of information on the Web.
			<em>Nature</em> <strong>400</strong>(8 July), 107-109.</li>
		<li><a id="mcdonnell"></a>McDonnell, J., Koehler, W. &amp; Carroll, B. (2000). Cataloging challenges in an Area
			Studies Virtual Library Catalog (ASVLC): results of a case study. <em>Journal of Internet Cataloging</em>
			<strong>2</strong>(2), 15-42.</li>
		<li><a id="brooks"></a>Markwell, J. &amp; Brooks, D.W. (2002) Broken links: the ephemeral nature of educational
			WWW hyperlinks. <em>Journal of Science Education and Technology</em>, <strong>11</strong>(2), 105-108.</li>
		<li><a id="markwell"></a>Markwell, J. &amp; Brooks, D.W. (2003) 'Link rot' limits the usefulness of Web-based
			educational materials in biochemistry and molecular biology. <em>Biochemistry and Molecular Biology
				Education</em> <strong>31</strong>(1), 69-72.</li>
		<li><a id="nelson"></a>Nelson, M &amp; Allen, B. (2002). <a
				href="http://www.dlib.org/dlib/january02/nelson/01nelson.html">Object persistence and availability in
				digital libraries.</a> <em>D-Lib Magazine</em> <strong>8</strong>(1). Retrieved May 1, 2003 from
			http://www.dlib.org/dlib/january02/nelson/01nelson.html</li>
		<li><a id="rumsey"></a>Rumsey, M. (2002). Runaway train: Problems of permanence, accessibility, and stability in
			the use of Web sources in law review citations. <em>Law Library Journal</em>, <strong>94</strong>(1), 27-39
		</li>
		<li><a id="spinellis"></a>Spinellis, D. (2003). The decay and failures of Web references. <em>Communications of
				the ACM</em> , <strong>46</strong>(1), 71-77.</li>
		<li><a id="taylor"></a>Taylor, M.K. &amp; Hudson, D. (2000). &quot;Linkrot&quot; and the usefulness of Web site
			bibliographies. <em>Reference &amp; User Services Quarterly</em>, <strong>39</strong>(3), 273-276.</li>
	</ul>

</body>

</html>