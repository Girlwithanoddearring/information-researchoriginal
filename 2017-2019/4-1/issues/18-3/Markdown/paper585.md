<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

# From coexistence to convergence: studying partnerships and collaboration among libraries, archives and museums

#### [Wendy M Duff](#author)  
Faculty of Information, University of Toronto, 140 St. George Street, Toronto, M5S 1A1, Canada  
#### [Jennifer Carter](#author)  
Département d'histoire de l'art L'Université du Québec à Montréal, 405 rue Ste-Catherine E, Montréal, H3C 3P8, Canada  
#### [Joan M. Cherry](#author), [Heather MacNeil](#author) and [Lynne C. Howarth](#author)  
Faculty of Information, University of Toronto, 140 St. George Street, Toronto, M5S 1A1, Canada

#### Abstract

> **Introduction.** The convergence of libraries, archives and museums is an evolving phenomenon that has garnered increased attention in the literature and professional practice over the past decade. To date, little research exists documenting the experiences of these institutions as they engage in different forms of collaboration and convergence.  
> **Method.** Using a series of on-site, semi-structured interviews of professionals conducted in 2010 and 2011, the study examined initiatives involving different forms of collaboration and convergence, and different stages of the process in two institutions in Canada and three in New Zealand.  
> **Analysis.** The interviews were audio recorded and a descriptive summary of each interview was prepared. We examined the summaries and identified themes within and across the institutions.  
> **Results.** Findings suggest the motivations that led to the various projects reflected the discourse, beliefs, and values of the professions at the time the projects took place, and occasionally, administrative expediency. Aspects that emerged from the interviews correlate broadly to six themes: to serve users better; to support scholarly activity; to take advantage of technological developments; to take into account the need for budgetary and administrative efficiencies; to adapt to an evolving understanding of digital surrogates as objects; and to obtain a holistic view of collections. Benefits have accrued in terms of new perspectives on collections and on ways of seeing the institution, its users and services, in enhanced staff learning, in finding different methods for accomplishing work activities, and in the pragmatic efficiencies of budget reallocation and cost-savings.  
> **Conclusion:** We conclude with an overview that summarizes the salient findings.

## Introduction

_From Coexistence to Convergence: Studying Partnerships and Collaboration Among Libraries, Archives and Museums_ is a project undertaken by a multi-disciplinary team of researchers from the Faculty of Information at the University of Toronto from 2009–2012 and funded by the Gladys Krieble Delmas Foundation. The study investigates the different motivations driving forms of collaboration and convergence among libraries, archives and museums, the processes used in the planning and implementation of these new partnerships, as well as the challenges experienced and the benefits accrued by the institutions involved.

Despite similar vocations as cultural heritage and memory institutions, libraries, archives and museums are, with some exceptions, three distinct types of institution. Indeed, these institutions were conceptually closer in the period of their emergence as public institutions throughout the eighteenth century than they are today, having evolved through different mandates, collections, and professional cultures. That they share common functions (such as collecting, conservation, research, and public service) belies the differences in professional practices, training, and organizational methods that largely constitute and differentiate these fields today. Yet while some cultural heritage organizations have departmental libraries, archives, and museums within one repository (for example, the Canadian Centre for Architecture in Montréal and the Smithsonian Institution in Washington, D.C.), not all such institutions have common professional practices and access systems. The advent of digital environments and the ideal of increased public accessibility are but two factors that have led to calls for greater collaboration among libraries, archives, and museums.

A number of initiatives since 2000, including journal articles, conference proceedings, invitational meetings, and administrative mergers, indicate a growing interest in exploring new forms of collaboration and convergence amongst libraries, archives and museums. The editors of three major journals (_Library Quarterly_, _Archival Science_ and _Museum Management and Curatorship_) coordinated their efforts to publish special issues exploring ideas concerning the convergence of these three institutions between 2008 and 2010\. Several library conferences have equally explored issues of convergence, notably the Research Library Group (RLG) Forum, 'Libraries, archives and museums: three-ring circus, one big show?' (2005) and the 2006 Rare Books and Manuscripts Section of Association of College and Research Libraries conference 'Libraries, archives and museums in the twenty-first century: intersecting missions, converging futures.' The Institute for Museums and Library Services (IMLS) sponsored an invitational meeting (2008) to investigate issues related to the education of,

> cultural heritage information professionals who can work across the boundaries of libraries, archives, and museums to meet the information needs of all users – including library patrons, museum visitors, the general public, and other professionals – in all types of cultural heritage organizations' ([Cultural Heritage Information Professionals Workshop 2008](#cul08))

and the Online Computer Library Center (OCLC) organized one-day workshops for libraries, archives and museums professionals facilitated by Diane Zorich. The workshops resulted in the insightful report _Beyond the silos of the LAMs: Collaboration among libraries, archives and museums_ published by OCLC Programs and Research in 2008.

The subject was more recently taken up by the discipline of information science, notably at the American Society for Information Science and Technology (ASIS&T) conference in 2009, which featured the panel Information Organization in Libraries, Archives and Museums: Converging Practices and Collaboration Opportunities. At its annual conferences in January 2011 and 2012, ALISE, the Association for Library and Information Science Education, presented sessions on convergence among libraries, archives and museums and implications for graduate professional education. These largely North American initiatives have, over the past few years, become more international in scope and even broader in their inclusion of still other forms of cultural heritage through the International Council on Monuments and Sites (ICOMOS) symposium in 2009 where a new acronym, LAMMS, was proposed: libraries, archives, museums, monuments and sites ([Gwinn 2009](#gwi09)).

This paper reports the findings of our research project, which included interviews with professionals working in five institutions undergoing a form of convergence within this sector in Canada and New Zealand in 2010 and 2011\. Four research questions guided our study:

1.  What motivates libraries, archives and museums to collaborate?
2.  What processes do libraries, archives and museums follow when planning and implementing collaborative projects?
3.  What are the challenges of collaborating across libraries, archives and museums?
4.  What are the benefits of collaboration among libraries, archives and museums?

## Literature review

There is a rich body of scholarly literature discussing convergence of various kinds including media convergence ([Jenkins 2006](#jen06)) as well as convergence and collaboration across a range of institutions ([Huxham and Vangen 2005](#hux05)), disciplines ([Moran 2002](#mor02)) and professions ([Meads _et al._ 2005](#mea05)). The literature review does not contain any discussion about collaboration, mergers and partnerships found in different contexts. For the purposes of this research project, we have limited our focus to the scholarly literature published between 2007 and 2011 that specifically discusses library, archive and museum convergence.

Collaboration and convergence of libraries, archives and museums, and of the affiliated disciplines of library and information science, archival studies, and museum studies, have been discussed intermittently in the literature of the three disciplines for the past several decades ([Tanackovic and Badurina 2009](#tan09)). Recently, those discussions have become more continuous and focused, partly in response to the creation of new governmental mechanisms for funding and managing cultural heritage resources in a number of jurisdictions (e.g., the United States, England, and Australia), which have encouraged collaboration among libraries, archives and museums ([Martin 2007](#mar07); [Birtley and Bullock 2008](#bir08); [Gibson _et al._ 2007)](#gib07); and partly in response to administrative realignments within the sector which have merged previously separate institutions ([Doucet 2007](#dou07)). A review of the scholarly literature on collaboration and convergence written over the past five years reveals four recurring themes: the limits and possibilities of collaboration and convergence; the need for institutional and disciplinary collaboration and convergence; the experience of building partnerships and collaborations; and the role of education.

### Limits and possibilities of collaboration and convergence among libraries, archives and museums

The limits to convergence among libraries, archives and museums are often couched in terms of perceived differences in how librarians, archivists, and museum professionals view their collections, their users, and their cultural mandates ([Beasley 2007](#bea07); [Martin 2007](#mar07); [Wythe 2007](#wyt07); [Trant 2009](#tra09)). Beasley and Martin observe, for example, that,

> there is a fundamental difference between “viewing” a collection, as is typical in a museum setting, and “using” or “reading” library and archival materials ([Martin 2007:84](#mar07)).

These differences are attributed, in turn, to the distinct historical traditions of the institutions and the distinct disciplinary identities of the professionals who work within those institutions ([Martin 2007](#mar07); [Trant 2009](#tra09)).

These differences and distinctions have been questioned by a number of authors who believe that the commonalities among these institutions are more meaningful than their differences ([Whiteman 2007](#whi07); [Bak and Armstrong 2009](#bak09)). Libraries, archives and museums '_share a common institutional ancestry_' ([Martin 2007:81](#mar07)) rooted in a common cultural endeavour ([Kirchhoff _et al._ 2008](#kir08); [Given and McTavish 2010](#giv10); [Paulus 2011](#pau11)), and the disciplinary divisions currently separating librarians, archivists, and museum professionals are a relatively recent historical development ([Given and McTavish 2010](#giv10)). For these authors, convergence in the digital age provides an opportunity for a _reconvergence_ of these institutions and their affiliated disciplines ([Given and McTavish 2010](#giv10); [Paulus 2011](#pau11)) and a potential realization of the vision of 'ubiquitous knowledge' outlined by Paul Otlet in his 1934 _Traité de documentation_ ([Kirchhoff _et al._ 2008](#kir08)). Successful collaboration depends, ultimately, on the ability of the collaborators to identify substantive commonalities as well as substantive differences in services and practices and to build partnerships that recognize and respect these commonalities and differences ([Martin 2007](#mar07); [Elings and Waibel 2007](#eli07)).

### The need for collaboration and convergence

User needs related to resources and services are at the heart of the '_imperative to collaborate_' ([Martin 2007: 85](#mar07)). According to Martin and others, '_the general public sees little significant difference between libraries, archives and museums_' ([Martin 2007:82](#mar07); [Doucet 2007](#dou07)) and the new users of the resources of these institutions want to have the ability to discover, download, use, and repurpose those resources in the same way they do other digital resources on the Web ([Michalko 2007](#mic07); [Conway 2010](#con10); [Given and McTavish 2010](#giv10)). Collaboration among institutions aimed at increasing online access to cultural heritage resources is an essential means of ensuring that such institutions '_stay relevant and engage their publics in the Information Age_' ([Ray 2009: 358](#ray09); [Given and McTavish 2010](#giv10)). For some authors, such engagement needs to extend beyond access to include collaborating with users themselves through the use of Web 2.0 tools ([Kalfatovic _et al._ 2008](#kal08); [Trant 2009](#tra09)).

### The experience of building partnerships and collaborations

A substantial number of articles written over the past five years take the form of case studies of specific partnerships and collaborations established for the purpose of enhancing access to the rich content of libraries, archives and museums and sharing it with a broader public ([Pijeaux 2007](#pij07); [Reed 2007](#ree07); [Kalfatovic _et al._ 2008](#kal08); [Waibel and Erway 2009](#wai09); [Chaffin Hunter _et al._ 2010](#cha10)). In these studies the need for consistent and open communication, flexibility, and respect for differences among collaborators is a common theme. There are also numerous case studies of convergence through the development and implementation of integrated online access systems ([Bak and Armstrong 2009](#bak09); [Kirchhoff _et al._ 2008](#kir08); [Timms 2009](#tim09)) and digital-imaging programmes ([Emery _et al._ 2009](#eme09); [France _et al._ 2010](#fra10)) within and across these institutions. The studies of technological convergence emphasize the critical importance of _'well-developed metadata, data management, storage, and access [systems that] are effectively integrated across institutions following broadly accepted international consensus standards and protocols_' as well as the need for collaboration among information, scientific and data professionals to ensure '_the integration of the technical, organizational, and social processes required to implement [these systems]_' ([France _et al._ 2010: 54, 55](#fra10)).

In virtually all of these case studies the authors were involved in the collaboration and convergence experiences on which they report. The number of independent research studies investigating library, archive and museum collaboration is small in comparison to these studies. Tanackovic and Badurina ([2009](#tan09)), for example, conducted a national study of the nature and scope of partnerships in the Croatian library, archive and museum sector. The investigators found that, while there was widespread agreement on the benefits of collaboration among these institutions, '_actual collaboration between them is sporadic, ad hoc, and not an integral part of regular operations of Croatian museums, libraries, or archives_' ([Tanackovic and Badurina 2009: 307](#tan09)). A study of collaborative partnerships between libraries and museums in England and the United States conducted by means of an email survey found '_that museum-library collaboration is happening successfully, with some significant similarities and differences between England and the USA_' ([Gibson _et al._ 2007: 63](#gib07)).

### The role of education

It is clear from the literature that, over the long term, effective and ongoing collaboration and convergence between and among libraries, archives and museums will require the professional and research skills of a new breed of information professional. The question that presents itself is, what kinds of educational programmes are needed to equip this new breed with those skills? According to Trant [(2009)](#tra09), existing programmes for educating these professionals continue to '_emphasize the differences rather than their emerging similarities_' ([Trant 2009: 376](#tra09)). Some authors consider the emergence of digital curation specializations within library and information science programmes as a promising step in the direction of creating _converged_ information professionals ([Ray 2009](#ray09)). Others view the evolution of some library and information science schools into iSchools as the way forward given the emphasis the iSchools place on interdisciplinarity among the information professions and their focus on the convergence of information, technology, and people ([Cox and Larsen 2008](#cox08); [Given and McTavish 2010](#giv10)).

Considered schematically, the scholarly literature on library, archive and museum convergence published over the last five years tends to fall into two broad categories. The first category consists of thought pieces in which the authors reflect and/or speculate on the challenges and opportunities of convergence, drawing on their own professional experience and/or the histories of the three professions. The second category consists of reports on collaborative activities within and across these institutions in which the authors themselves were participants. As noted above, there have been very few independent empirical research studies that have investigated library, archive and museum collaboration and convergence. It is that gap in the scholarly literature that this research project seeks to fill in some small way.

Considered in relation to the motivation for collaboration and convergence, the literature suggests that the factors driving it are:

1.  To serve users better ([Doucet 2007](#dou07); [Martin 2007](#mar07); [Michalko 2007](#mic07); [Conway 2010](#con10); [Given and McTavish 2010](#giv10));
2.  To support scholarly activity ([Hedstrom and King 2002](#hed02));
3.  To take advantage of technology developments ([Ray 2009](#ray09); [Given and McTavish 2010](#giv10));
4.  To achieve budgetary and administrative efficiencies ([Doucet 2007](#dou07)).

## Research methods

This qualitative research included a series of on-site, semi-structured interviews of professionals at selected cultural heritage institutions which had undergone, or were in the process of establishing, a form of collaboration or convergence. We selected two institutions in Canada and three in New Zealand as sites for our study: from Canada, the Canadian Centre for Architecture in Montréal, Québec, and the Taylor Family Digital Library at the University of Calgary in Alberta; and from New Zealand, the Museum of New Zealand Te Papa Tongarewa, The National Library of New Zealand, Te Puna Mātauranga o Aotearoa, and Archives New Zealand, Te Rua Mahara o te Kāwanatanga, all in Wellington. (New Zealand was chosen because the lead researcher was there on sabbatical.) Collectively, these five institutions represent a range of geographic, political, disciplinarian, and institutional contexts, that vary in scale, date of founding, and mission, and most importantly, where different forms of collaborative and convergence initiatives were undertaken, providing us with a number of distinct departure points and rationales for understanding these phenomena.

## Institutional contexts

The study involved five institutions, described below, that were involved in various types of collaborative initiatives.

### Canadian Centre for Architecture (CCA), Montréal, Québec, Canada

The Canadian Centre for Architecture in Montréal, Canada, is an international research centre and museum dedicated to the discipline of architecture. Founded in 1979, the institution is premised on the idea that architecture is a public concern, while the Centre's collections, exhibitions, research and programming platforms are designed to further this institutional mission and mandate. Four principal collections underpin the Centre: photographs, archives, prints and drawings, and volumes and periodicals.

We identified the Canadian Centre for Architecture as a site in our research for several reasons. One reason arises from the specific relationship of the institution's collection to the larger discipline of architecture. Traditionally, the study of architecture is dependent on material stored in libraries, archives, and museums, in addition to built heritage itself. The Centre is unique in combining all of these collections as an independent institution (as distinct from an architectural and fine arts library such as Avery, at Columbia University in New York City, or an architecture collection housed within a museum, such as the Architecture and Design collection at the Museum of Modern Art, also in New York City). We selected this institution because it provides an example of collections convergence, as well as examples of organizational and digital convergence.

### Taylor Family Digital Library, University of Calgary, Alberta, Canada

The idea of merging the library, archives, and museum resources at the University of Calgary was first articulated in 1998\. At that time, these services co-existed under a common administrative structure. The University's 2004/2005 strategic plan presented the idea of creating a new library that would include the resources of the museum, library and archives within a single entity. When we conducted the interviews in 2010, construction on the new building was virtually complete with plans to occupy the building in a few months. The institution included five outcome-based units: Collections, Research Services, Learning Services, Scholarly Communication, and Administration. The archivists and curators report to the Director of Arts and Culture, which is part of Research Services, although they also work with staff from other areas. The Collections Unit oversees metadata, acquisitions and cataloguing. Although the new building was not yet complete when we conducted our site visit, preparation for the move was well underway. At the time of the interviews, the library and archives resided in the same building, but the museum was in a separate building and had closed its exhibition space in preparation for the move. We selected this institution because the Taylor Family Digital Library houses the University's library, archives and museum collections.

### Museum of New Zealand Te Papa Tongarewa (Te Papa), Wellington, New Zealand

In 1990, the 150th anniversary of the signing of the Treaty of Waitangi fostered thoughts about nationhood and discussions concerning building three distinct national museums for, respectively, art, natural history, and the Māori and Pacific heritage in New Zealand. (The Treaty established a British Governor in New Zealand and recognized Māori ownership of their lands and other properties.) In 1992, however, the New Zealand government established a single institution, the Museum of New Zealand Te Papa Tongarewa in Wellington, as an autonomous Crown entity, which incorporated its predecessors, the Dominion Museum and the National Art Museum. Te Papa's collections are housed in two different buildings and include five areas: Art, History, Pacific, Māori, and Natural Environment. The main Te Papa building is situated on Wellington's waterfront, where the exhibitions and most of the museum's collections are housed, divided spatially by floors: the third floor focuses on natural history collections; the fourth floor contains the major Maori exhibits and the _marae_(or communal and spiritual meeting area); and the fifth floor exhibits the art collection. Although these collections are displayed on different floors, exhibitions may contain material from all three collections. Te Papa's online catalogue provides access to objects from all museum collections and the archives, but not the library. The library is located on the fourth floor of the main building, and the archives, as well as some research offices and research collections, reside in a building on Buckle Street, approximately one kilometre away. We selected this institution as an example of museological convergence within an institution that also houses library and archives collections. At the time of the interviews, Te Papa was embarking on a visioning exercise to develop a new strategic plan.

### The National Library of New Zealand, Te Puna Mātauranga o Aotearoa, Wellington New Zealand

In 1965, the National Library of New Zealand was formed, incorporating the Alexander Turnbull Library, the General Assembly Library, and the National Library Service. When we conducted the interview the National Librarian and a Leadership Group headed the Library. The Library contained four divisions: the Alexander Turnbull Library; Content Services; Literacy, Learning and Public Programmes; and the National Digital Library. In 2003, new National Library legislation gave the library the mandate to collect and preserve digital material. In 2004-05, the staff developed requirements for the digital library system and in 2007 signed the contract for this system, which manages all processes around the Library's digital object collection. In December 2009, the National Library moved from its permanent site to three temporary quarters to allow for a major redevelopment of its building. The Library moved some of its heritage collections, in addition to its manuscript and rare books collection to Archives New Zealand, its general collection to another site, and the remainder of its collections to storage. It established a shared reference service with Archives New Zealand described below. The National Library of New Zealand building was closed for renovations when we visited. The National Library and Archives New Zealand became part of the Department of Internal Affairs, Knowledge, Information, Research and Technology Branch, which led to greater collaboration across the institutions in 2011\. We selected this institution because it had temporarily merged its reference service with that of Archives New Zealand, and these two institutions were working on a joint digital preservation strategy.

### Archives New Zealand, Te Rua Mahara o te Kāwanatanga, Wellington, New Zealand

Archives New Zealand is the official guardian of the public records of New Zealand. The Archives holds very few personal papers, acquiring only records that enhance the understanding of the public records that are already held by the archives. The Archives' main location is in a modern office building in downtown Wellington, with regional offices in Auckland, Christchurch and Dunedin. The public services, _gateway_ (reference room) and reading room are on its ground floor. The gateway contains banners that carry the logos and names of the National Library and Archives New Zealand, and has two reference desks, one staffed by a reference librarian and the other by a reference archivist, as well as a number of desks with computers for public use. The shared reading room is in a separate location on the same floor. We selected this institution for the same reasons described for the National Library of New Zealand above.

Table 1 summarizes the characteristics of the institutions, the reasons for selecting each institution and the number of people interviewed in each institution.

<table><caption>

Table 1\. Institutions involved in the study</caption>

<tbody>

<tr>

<th>Institution</th>

<th>Characteristics</th>

<th>Reason for Selecting Institution</th>

<th>Number of People Interviewed</th>

</tr>

<tr>

<td>Canadian Centre for Architecture, Montréal, Canada</td>

<td>Research centre and museum dedicated to the study of architecture. Contains archives, photographs, prints and drawings, and monographs and periodicals</td>

<td>Research centre that combines library, archives and museum in one institution</td>

<td>3 (of whom 2 gave permission to use interview data)</td>

</tr>

<tr>

<td>Taylor Family Digital Library, Calgary, Canada</td>

<td>Houses the university libraries, archives and museum</td>

<td>Designed as converged entity that incorporates the existing library, archives and museum</td>

<td>7 (of whom 6 gave permission to use interview data)</td>

</tr>

<tr>

<td>Museum of New Zealand Te Papa Tongarewa, Wellington, New Zealand</td>

<td>Contains natural history collection, art collection and Maori collection as well as library and archives collections</td>

<td>An example of museological convergence (bringing together three different museum types) and institution that also houses library and archives collections</td>

<td>5</td>

</tr>

<tr>

<td>The National Library of New Zealand, Te Puna Mātauranga o Aotearoa, Wellington New Zealand</td>

<td>Contains collections of published and unpublished material related to New Zealand, and a digital library</td>

<td>Temporarily merged its reference service with Archives New Zealand, and was developing a joint digital preservation strategy with Archives New Zealand</td>

<td>2</td>

</tr>

<tr>

<td>Archives New Zealand, Te Rua Mahara o te Kāwanatanga, Wellington, New Zealand</td>

<td>Holds the records of the New Zealand Government</td>

<td>Temporarily merged its reference service with the National Library of New Zealand, and was developing a joint digital preservation strategy with the National Library of New Zealand</td>

<td>5 (of whom 4 gave permission to use interview data)</td>

</tr>

</tbody>

</table>

## Data collection and analysis

We asked the head of each institution to identify potential interviewees. In most cases, these individuals occupied middle and upper management positions and had experienced the planning, and effects of collaboration first-hand. In total, twenty-two individuals agreed to participate in the study. Two members of the research team travelled to each location and conducted face-to-face interviews onsite, which provided the researchers with an opportunity to tour the institutions as well as to meet each participant. Each interview lasted approximately one hour.

The interview script contained three sections: (1) Prior to convergence; (2) The experience and implementation; and (3) An assessment of the implementation. We did not define collaborative or convergence project but invited the participants to identify and discuss collaborative and convergence projects based on their own understanding of these. We audio-recorded each interview, after obtaining signed consent. We drafted summaries of each interview and sent these to interviewees for re-consent to participate in the study, and invited them to edit the summary if it did not accurately reflect their comments made in the interview. We also asked whether the interviewees wanted to be identified in any publication or presentation. In three instances, interviewees from three different institutions did not re-consent. We used the data from the remaining nineteen summaries to write this paper. Ten participants gave us permission to identify them in any publication describing this research. It is important to state that, although we have identified the interviewees through their institution, in many cases they provided their personal views rather than the views of the institution for which they worked.

## Findings

The interviews provided insights into a variety of types of initiatives involving collaboration and convergence amongst libraries, archives, and museums in Canada and New Zealand. The analysis that follows is organized according to the following four thematic headings: the motivations driving the collaborations and convergence; the planning and implementation process; the challenges experienced during convergence; and the benefits accrued.

### Motivations for collaboration and convergence

The motivations that led to the various convergence projects reflected the discourse, beliefs, and values of the professions at the time the convergence projects took place, and occasionally, administrative efficiency. Our findings in this area correlated broadly to six themes detailed below: to serve users better; to support scholarly activity; to take advantage of technological developments; to take into account the need for budgetary and administrative efficiencies; to adapt to an evolving understanding of digital surrogates as objects; and, to obtain a holistic view of collections.

#### a) To serve users better

The need to support users has led to the latest review of the various information resources held by Te Papa and the shared reference service at Archives New Zealand and the National Library of New Zealand. As one of the interviewees noted, Te Papa came together in the 1990s, but people working in the back office have continued to be siloed and they have continued to work as if they are working in the National Art Gallery or National Museum rather than in a newly configured cultural institution. Information and knowledge continue to reside in people's heads, databases, and spreadsheets, etc., and these need to be identified, and perhaps shared. The museum's new goal is to put systems in place that allow the museum's library to capture this information and knowledge and make it accessible, if appropriate; this goal is related to unified discovery and access. The shared reference services at Archives New Zealand and the National Library of New Zealand also envisioned '_a pretty seamless service so a customer coming in'_ would not care who they spoke to; '_they would get service across both collections'_. Responding to users' needs within the larger conceptual view has also driven the convergence at the University of Calgary, and is consistent with Martin's ([2007](#mar07)) 'imperative to collaborate'.

Imagining a broader constituency has catalysed the recent online developments at the Canadian Centre for Architecture, such that its Website has been conceived, in the words of one interviewee, '_as a place to go to find something interesting, like a magazine'._ Part of this thinking is market-driven, because a city the size of Montreal is not large enough to support such a specialized institution. Hence, the Centre's public is also outside, online, on the Web; and the Website, which is mainly content-based, necessarily addresses different communities. It is also, significantly, a Website that avoids by design a perpetuation of silo culture by including a _super-search_ function rather than specific _entrances_ to various divisions at the Centre.

#### b) To support scholarly activity

Scholarly demands have also fostered the need for convergence. The networked world enables the discovery of many different kinds of information by scholars in a highly integrated fashion. Tom Hickerson (Taylor Family Digital Library) suggested that

> The value to be realized through a closer integration of memory institutions, broadly writ, is something that I think is generally acknowledged, but I think that what really increasingly enhances the potential value of that convergence, is the educational and research environment that we live in.

To realize this value the Taylor Family Digital Library needs a unified access system. '_Number one on the list was unified discovery'_, that is, how do we find out about all the materials from all collections? John Wright, of the Library, underlines the importance of new scholarly needs. He suggests that there was an '_institutional imperative to re-focus or re-target scholarly communication'._

A shift in the conception of professional identities and attendant disciplinarian responsibilities is apparent in the new attitudes embraced by some institutions undergoing convergence that expect their professionals to perform with an expanded sense of their traditional job description and title. For example, the idea of a _transversal_ curatorial approach, that is, one that unites the different professional activities of the institution toward becoming more cohesive and research-oriented, was one of the goals of organizational convergence at the Canadian Centre for Architecture. This meant operating beyond the usual understanding of curation, one traditionally based on conservation and growth of the collections, and on planning large exhibitions related to these collections. As one interviewee described, the idea of a _transversal_ curatorial culture extends to other divisions at the Centre as well, such as Programs. Whereas the employees in Programs were once defined as exhibition coordinators and had managerial qualifications, now there is a greater curatorial focus to their division and work. Not only is there a curatorial coordinator, the Director of Programs is expected to have competence in both the curatorial and managerial domains.

Convergence involves going back to the collections, to the discovery of the collections, and to providing support in the way of partnerships. This demonstrates that collections are not static. John Wright, Taylor Family Digital Library, stated, '_It is about research support, that's the identified gap, and that's where convergence is, that's what convergence is targeting the most'_. This is a priority, if not _the_ priority, in the Arts and Culture Section at the Taylor Family Digital Library. Support for emerging networks of scholars, and for communities of knowledge, was also a goal articulated in the literature by Hedstrom and King ([2002](#hed02)).

#### c) To take advantage of technological developments

The advent and broad implementation of digital technologies across the library, archive and museum sector over the past decade has had an undeniable impact on collaboration and convergence initiatives amongst libraries, archives and museums. In many instances, digital technologies were seen as either the common platform for integrating media across the these institutions, or as a potential interface linking descriptions of museum objects to those in the broader collections of the converged institution as a whole. The collaborative projects at Archives New Zealand/National Library of New Zealand and the Taylor Family Digital Library provide examples of institutions influenced by the current digital paradigm.

The emergent field of digital preservation is another area that has influenced the move toward converged collections. Many of the concepts becoming central to digital preservation (such as provenance) derive from the archival context.

The digital realm has brought about a paradigm shift in the sense of how audiences and users expect to interface with institutions like museums, libraries and archives, an idea highlighted by Tracy Puklowski, from Te Papa. As users operate in an environment in which they are generating knowledge all the time, they are now questioning who the holder of the knowledge is, what the role of the curator is, and whether the museum is the voice of authority. Puklowski's observations align with those reported in the literature by Michalko ([2007](#mic07)), Conway ([2010](#con10)), and Given and McTavish ([2010](#giv10)).

Digital convergence has also occurred at the Canadian Centre for Architecture, which introduced the discursive creation of a single collection from what previously co-existed as distinct library, archives, photographic, and prints and drawings collections. In other words, the foundational collections of the institution are now viewed as one holistic collection rather than four separate collections. This is more than simply a technological convergence, although it has entailed aligning software where one system used to be in place for the library catalogue and another for all other services, a move to integrated online access systems also considered by Timms ([2009](#tim09)). Beyond merging cataloguing in one department and reference in another, this signals a cultural shift with deep implications for employee and professional identities.

#### d) Need for budgetary and administrative efficiencies

Budgetary constraints and need for administrative efficiencies also served as drivers for some of the convergence projects, and are consistent to what has been reported in the literature by Doucet ([2007](#dou07)). Martin Lewis (Te Papa) noted that, originally, New Zealand wanted to build three museums, but owing to pressures of budget administrators changed their idea from three museums to three floors within one museum. For Te Papa in general, the merging of the institutions was driven by space and economics according to Lewis. But, he noted, the museum grew into giving New Zealanders their money's worth and having an institution that could spark an economic benefit.

Moving the National Library and Archives New Zealand to the Department of Internal Affairs' Knowledge, Information, Research and Technology Branch and integrating the back office staff (information technology support, human resources, etc.) also had economic advantages. Steve Knight (National Library of New Zealand) suggests the decision for the Archives to use the digital repository of the National Library grew jointly out of their shared mandate and the imperative to save money: the National Library had a NZ\$24 (US\$18.7; £12.2) million project to build a digital library, and the Government Digital Archive Programme is a NZ$13 (US\$10; £6.6) million project, hence it would not have made sense for Archives New Zealand to develop or purchase their own system.

An interviewee from Archives New Zealand also pointed to financial advantages of collaboration, stating that a lot of money is invested in assets involved in the management of documentary heritage (e.g., archives' repositories, the National Library buildings, and the Government Digital Archive). Sharing assets is already happening: the Government Digital Archive is using hardware and software purchased through the National Library's contract. The same interviewee observed that when you talk about physical assets, this is not quite so straightforward since the buildings are a large part of organizational identity. The Department of Internal Affairs is fostering a different way of thinking: the government has assets for the delivery of these common purposes and, especially in today's financial environment, departments have to think far more flexibly and creatively about how to use these assets. An interviewee from the Archives suggested the government was looking at a shared use of the assets.

In another example, producing a more sustainable institution after an initial growth period meant downsizing the Canadian Centre for Architecture's workforce and pushing the four collections to work together, to the point that they are now referred to as _A collection_. In the early days of the Centre's development, a large conservation department, strong collecting policy, and ambitious exhibition agenda embraced by the museological and archival components of the institution predominated other possibilities for achieving the Centre's objectives of making architecture a public concern. '_Some of the convergence comes out of necessity, of an institution becoming more sustainable'_, observed one interviewee. The move toward sustainability included streamlining the workforce: cataloguers traditionally trained in subject-area specializations are now required to operate across the institution and to use other software programs such as the collection database known as The Museum System. Finally, a changed vision and budget have also impacted upon the nature of research conducted at the Canadian Centre for Architecture. If, before, a university model was adopted, suggesting that curators would conduct independent research within their own areas of interest and expertise, today's curator is far more focused on specific research projects identified by the institution.

#### e) Understanding of the digital surrogate as objects

A motivation for convergence that we observed in one institution relates to a changed understanding of the status of the object within a newly defined multidisciplinary context. The understanding of digital surrogates as objects in their own right advanced the convergence of some of the organizations. One interviewee pointed out that although the current discourse emphasizes a convergence of museums, libraries and archives, the

> real convergence that may be going on in the digital realm is not between our three structures, but is for all types of information. So the convergence isn't necessarily museums, archives, and libraries; the convergence is text, audio, image, data, moving image … That's the convergence.

Another interviewee discussed how the digital realm also helps to overcome the commitment to the original item and disdain for the digital surrogate. They noted that while the object is important, surrogates are also important. Surrogates, such as digital objects and their associated metadata, have many roles and are able to do things that the original cannot do. Steve Knight voiced a similar belief:

> My own feeling is that the divergences are historical and material-based and a lot of these differences disappear in the digital realm.

John Wright (Taylor Family Digital Library) noted that information comes in many forms and his own emphasis encompasses _'non-textual learning'_. He suggested that the University of Calgary's new library is about images, manipulation, and all sorts of things that are '_non-textual'_, but that continue to relate to '_knowledge production'_. He argues that the new library will move beyond the traditional provision of support to help knowledge building. This commitment might be viewed as consistent with Otlet's 1934 vision of 'ubiquitous knowledge' as described by Kirchhoff _et al._ ([2008](#kir08)).

#### f) To obtain an holistic view of collections

A fundamental change in the philosophy around the object's status underlies the decision for museological convergence at Te Papa. Traditionally, specific museum types frame objects in corresponding and disciplinarian ways: art museums house objects as works of art; ethnography museums frame objects ethnographically; and history museums display historical artefacts. At Te Papa, the indigenous Maori principle of _taonga_, which understands objects as a treasured or valued manifestation of living culture that transcend any single disciplinarian designation, has led to a different way of displaying Te Papa's three typological collections. This means that in exhibition spaces, the typological divide is broken down to enable convergence of collection types and, it hopes, a more holistic approach to displaying cultural heritage. In one of its first exhibitions, traditional disciplinarian divisions between objects were broken down by the display of a McCahon painting next to a Kelvinator refrigerator. Today, Te Papa routinely exhibits objects from its three collections together, for example, placing sculpture within the natural history collections, and natural history objects with the art collection.

Table 2 identifies factors that motivated each institution to undertake initiatives involving collaboration and convergence.

<table><caption>

Table 2\. Motivations for convergence</caption>

<tbody>

<tr>

<th>Motivation</th>

<th>Canadian Centre  
for Architecture</th>

<th>Taylor Family  
Digital Library</th>

<th>Te Papa</th>

<th>National Library of  
New Zealand</th>

<th>Archives New  
Zealand</th>

</tr>

<tr>

<td>To serve users better</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td>X</td>

<td>X</td>

</tr>

<tr>

<td>To support scholarly activity</td>

<td>X</td>

<td>X</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>To take advantage of  
technological developments</td>

<td> </td>

<td>X</td>

<td>X</td>

<td>X</td>

<td>X</td>

</tr>

<tr>

<td>To achieve budgetary  
and administrative efficiencies</td>

<td>X</td>

<td> </td>

<td>X</td>

<td>X</td>

<td>X</td>

</tr>

<tr>

<td>To understand digital surrogates as objects</td>

<td> </td>

<td>X</td>

<td> </td>

<td>X</td>

<td> </td>

</tr>

<tr>

<td>To obtain an holistic view of collection</td>

<td> </td>

<td> </td>

<td>X</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

As shown in Table 2, the projects were motivated by reasons related to serving users better, to supporting scholarly activity, to taking advantage of technological developments, to considering the need for budgetary and administrative efficiencies, to adapting to an understanding of the digital surrogate as object, and finally, to obtaining a holistic view of collections. Two of these factors, serving users better and taking advantage of technological developments respectively, have recently been at the forefront of institutional change, i.e., many institutions have been influenced by digital developments and an increasing emphasis on the user. We note that though digital opportunities influenced Te Papa's current convergence project, these were not a factor in the creation of the institution. Budgetary and administrative factors are likely involved in most collaborative projects.

### The planning and implementation process

The planning activities for the collaboration and convergence projects were similar across the institutions. In most cases, not surprisingly, teams developed plans and consulted with all stakeholders. For example, at the University of Calgary, the process involved broad strategic planning of the Taylor Family Digital Library, with six teams (including librarians, archivists, curators, managerial and support staff) organized according to functional outcome areas with a focus on collections, research support, outreach, media and technology, learning and staffing. There, to a large degree, Tom Hickerson, the Vice-provost (Libraries and Cultural Resources) and University Librarian, '_stayed at arm's length from the process'_. The groups developed a _'set of priorities_ that _incorporated convergence within the terms of reference'_. A period of organizational realignment to provide a structure to address those priorities followed the planning process.

The project to merge the reference services at Archives New Zealand also began with an inter-professional team. Staff from the National Library and Archives New Zealand worked for a year to develop a plan that provided a unified service so a customer would obtain the same service across both collections regardless of whom he or she interacted with at reference services. The team began by developing a set of principles that identified what it wanted to achieve, so that the staff would know _where the line in the sand was_. Some of the principles related to the different nature and types of collections held by the two institutions, while others related to the seamless service and ending the service when the Library returned to its permanent site. A team that involved staff from both the Archives and the Library was also developing a new, shared preservation strategy.

The planning process of convergence at the Canadian Centre for Architecture has been a lengthy one, ostensibly because it has involved a complete institutional reorganization and downsizing, but also owing to its instantiation as several different forms of convergence. Begun in 2003, the process is not yet complete; however, the Centre has made significant organizational changes. The implementation has been top-down, led by a director's vision, and supported by members of upper management.

At Te Papa, the current planning exercise to develop a new vision and strategy, however, is also team-based, with Level 3 managers meeting weekly. Value-based planning sessions utilized ideas from the book, _Tribal Leadership_, ([)Logan _et al._ 2008](#log08)). The method outlined by Logan examines what the staff is proud of, what they care about, their goals, and where they want to be in the future. It involves taking stock of assets, seeing whether they match up with goals and, if they do not, identifying the steps required to build the assets. The process results in the identification of a _noble cause_, which is effectively a mission, and also reflects the core values of the team.

Though many interviewees across the sites suggested that their planning processes were highly collaborative, elements of a top-down approach also emerged. For example, at the Taylor Family Digital Library, one interviewee noted that direction came down from above, and coordination came from the bottom up. She said that although the academic staff had expressed interest in having a very collaborative process, _'at times it does feel a little top down'_. This individual also observed _'you can't do everything from the bottom up'_. The project to create a seamless reference service in New Zealand was team-based as previously noted, but some issues had to be referred to upper management. Some decisions had policy and financial implications; therefore, the team had to involve various managers in the decision making process. The current planning process at Te Papa also appears collaborative with teams identifying their core values and developing the vision and strategy to move forward. One of the interviewees, however, noted that the library staff had not been given access to a copy of the library review though it had led to a major change in the focus of the library and a reduction in the number of staff.

While the interview script included questions related to the implementation of initiatives involving collaboration and convergence, we have little data on the implementation process. Some of the projects were still in the planning phase, e.g., the development of the digital strategy of the National Library of New Zealand and Archives New Zealand and the creation of the Taylor Family Digital Library. In other cases, though the projects were implemented, none of the interviewees who gave permission to use their data had been involved in the implementation process.

### Challenges

While many of the goals connected to, and benefits accrued from, collaboration and convergence relate to learning about other professional practices and improving access to a broader range of materials, a number of challenges arose during the initiatives. The challenges broadly categorized are institutional or professional in nature, and ranged from overcoming the existence of institutional silos (such as achieving access to collections), to respecting and/or adapting to changed professional identities and values, and overcoming language and communication barriers.

#### a) Institutional silos and access systems

Several interviewees perceived differences among various groups creating silos that needed to be broken down to increase the sharing of information across the various groups. For example, Claudia Orange suggested Te Papa still had to overcome the _'silo mentality'_ in which everyone wants to _'look after their own area, without… fully accepting the mandate of Te Papa that is cross-disciplinary, that we need to support each other in our work'_. Increasingly, research needs to be cross-disciplinary because it feeds into the exhibitions and related activities and products of Te Papa's converged museological collections. Another Te Papa interviewee also noted that convergence physically has happened _'in the sense that we have brought the collections, the visitors, and the staff together'_ but went on to say: _'I still think the culture is the stumbling block',_ as people are not always prepared to share information. A third Te Papa interviewee stated, _'I think it is the cultural experience of breaking down the silos and demonstrating benefits of sharing information. I think those for me are the challenges'_. Dave Brown, Integrated System Specialist, Taylor Family Digital Library, suggested even learning about the different cultures, and gaining the trust that goes with that, on both sides, can be challenging. Another Digital Library interviewee commented on the ongoing challenge of breaking down silos, noting further _'systems can break down those silos faster than people can'_. But he stated that the Digital Library does not want to end up with the lowest common denominator, where _'no one is an expert on anything anymore'_, and suggested the library needed to have access to expertise and, on this human level, we _'haven't got there yet'_.

To some degree, the persistence of silos among some of the institutions studied is consistent with the findings of Tanackovic and Badurina ([2009](#tan09)) in their study of Croatian heritage institutions. While there may be overall agreement as to the benefits of convergence, implementation may, itself, be sporadic and uneven.

The staff of the Taylor Family Digital Library viewed the process of converging different access systems and metadata schemes as challenging, an issue also addressed by France _et al._ ([2010](#fra10)) in their analysis of a library imaging programme. Tom Hickerson elaborated on the challenges of metadata issues. He noted that the different metadata standards, and collection discovery and management systems are a big problem. Canadian archival practice makes it more difficult because of the _'closer connection with the government records environment'_ than in the United States; Canadian standards are _'less flexible in terms of this kind of integration'_. He suggested the Museum, which is part of the Taylor Family Digital Library is also a challenge in a different way: they have an almost total absence of discovery for collections, which in a way is almost better. It is better to have nothing than to have _'standards that don't work for that purpose'_. Archivists, he thinks, _'assume that they should be providing that information'_. But the lack of a framework for integration presents _'a conceptual problem as well as a pragmatic difficulty'_. The problem is not lack of openness, but a lack of _'people broadly knowledgeable to train each other'_, an absence of a _discourse_ and leadership on those issues.

Other problems arose for the National Library of New Zealand staff working in the merged reading room; some library staff were concerned about the learning curve required to gain the necessary knowledge to use the Archives New Zealand's finding aid system to access the Archives collections. An interviewee indicated this was due partially to the staff members wanting to be perfectionists and wanting to ensure that the users received great service. Steve Knight also indicated that staff members, who wanted to resolve issues autonomously, could be a problem. He observed that _'it's difficult for people to think outside their own organization'_, and to think that other people may have some knowledge that can be applied. He suggested that there is a desire to be responsible, to gain the necessary knowledge and work through the issues and problems on their own. This is in some ways counterproductive. It has been difficult for teams to mesh together because,

> everybody wants to be seen to be fantastic and to be contributing to, and leading efforts, during a time when librarians and archivists are part of something more important than our usual work and when there is a lot at stake.

It is a question of _trying to strike a balance between engagement and being overenthusiastic_. He noted his unit has been pushing from a learning point of view, but they want to do the learning themselves in some instances.

#### b) Professional identities, values, languages and communications

Tom Hickerson (Taylor Family Digital Library) noted the greatest challenges of convergence are connected to professional identity, which in many ways connects to professional expertise. A person's expertise is what gives him or her a sense of value. It is seen as a failure if people do not respect one's expertise and one's ability to accomplish the precise mission for which one was trained. But that's not the point, notes Hickerson:

> It's not about worrying about what one knows or what everybody else knows, it's really about bringing knowledge sets together; not to mash them together, but in fact to align them towards a common practical application and then a broader mission.

John Wright (Taylor Family Digital Library) also highlighted the challenge of dealing with different professional identities. He suggested behaviour is negotiable but identities are non-negotiable, and that:

> when people identify in certain ways it's much more difficult to change than when people behave in certain ways. When people say 'That's not an archives,' or 'I'm a curator, that's not what I was taught'… What they're saying is not that that's not a good way to do it, but, if you're really listening, what you're hearing are identity statements.

As Martin ([2007](#mar07)) and Trant ([2009](#tra09)) have examined previously in the literature, professional values and philosophies inform professional identities and professional cultures. Negotiating among different philosophies around Internet access to develop a common set of principles challenged the development of the shared reference service of New Zealand's National Library and Archives. Archives traditionally are concerned with security, privacy and protection of material while libraries are committed to optimizing access. These two fundamentally different approaches were difficult to align. Archives New Zealand had never offered Internet access in their reference room; it completely locked down its computers and only provided access to the Archives' resources. The philosophy of the Library, on the other hand, included providing Internet access to everybody, so their researchers would have access to frequently used resources such as email and Facebook accounts. The issue took three to four months to resolve and the final compromise to provide limited access to external sites required the involvement of senior management.

Different values have also created challenges at Te Papa. Te Papa operates as a national museum for art and historical collections, but a few interviewees suggested some members of the art world believe it has not quite served the same role as the National Art Gallery once did. According to the interviewees, many art critics have suggested New Zealand still needs a stand-alone national art gallery; some feel that Te Papa's treatment of art, and the display of art in spaces that are non-traditional, compromise the art. Furthermore, problems with morale and considerable staff turnover in Te Papa's Art Department are linked to these same issues. For some, the professional values of the art curators are not being respected.

Interviewees from other institutions also suggested their expertise and professional cultures were not respected. As one interviewee explained:

> we [he and his professional colleagues] have really different working ethics, we come from very different cultures, our end products are very different ... The convergence is about establishing new working relationships for the future

Another interviewee also noted he felt misunderstood and other professionals in his institution did not realize the complexity of his work.

> They do not understand that an exhibition is a very long, complicated, and research-driven process. It's not just hanging pretty things on the wall. So there are misconceptions about what we [his department] do.

When both sides talk about teaching, they talk about different things, leading to _'disjunctures'_ and _'misunderstanding'_. He also indicated that convergence raised questions over whether his department would _'have control of our space'_. He suggested that convergence has meant coming under a new command and not feeling included in decision-making. These and other concerns articulated by interviewees relative to professional identity, values, cultures, and expertise, were consistent with the expressed need for regular and transparent communication, as well as a respect for differences among library, archive and museum collaborators, as reported in case studies cited earlier in the paper ([Pijeaux 2007](#pij07); [Reed, 2007](#ree07); [Kalfativic _et al._ 2008](#kal08); [Waibel and Erway 2009](#wai09); [Chaffin Hunter _et al._ 2010](#cha10)). They also lend support to the need identified in the literature for more integrated and interdisciplinary approaches to professional education ([Cox and Larsen 2008](#cox08); [Ray 2009](#ray09); [Given and McTavish 2010](#giv10)).

Two interviewees at the Taylor Family Digital Library also highlighted the difficulty posed by professional differences in terms of ways of thinking and language use. John Wright suggested that converging the thinking is more difficult than adopting similar practices. Each discipline historically develops semantic and linguistic tools (known as jargon to outsiders), to describe what they do. The question is not how one brings the people together or how one brings what they are doing together, _'but how you put a hundred years of tool building, language, real tools, together, and carry that over'_. He suggested that this was the real challenge. Marilyn Nasserden (Taylor Family Digital Library) also noted: _'I think the language is really important',_ suggesting that information professionals have to consider how various professions think about things differently. Finally, an interviewee from Archives New Zealand noted that communicating effectively with stakeholders and staff, and communicating enough with them, was a major challenge.

### Benefits

The interviewees discussed the benefits they had observed in terms of users, conceptualization of collections, staff learning, and improved working methods.

The interviewees involved in the shared reference service at Archives New Zealand and the National Library indicated that a number of users welcomed and benefited from the unified service. As one interviewee from the Archives noted:

> there has been huge benefit for readers as they can see relationships between the manuscript over there and the government record over here, and they can have them side by side to better appreciate that relationship.

This individual also noted that feedback from readers had been reasonably positive. The reading room is busier now but there is more staff to service their needs. Claudia Orange suggested the increase in visitor numbers was a major benefit of creating Te Papa; Martin Lewis, also from Te Papa, indicated that the potential benefit of connecting information resources to users was driving the current collaborative efforts.

Three interviewees at the Taylor Family Digital Library highlighted benefits related to changes in the way people view collections. John Wright posited that convergence work has resulted in the complete reconceptualization of the collections, how people work with them, and what staff needs to do in order to develop appropriate programmes and services to address the complexities of the digital world. Digital rights management and copyright are having a huge impact on the production, dissemination, and ownership of knowledge. Collaboration is also significant: _'It has freshened up everything'_, observed one interviewee. Another interviewee indicated that merging the University's library, archives and museums has led to a redistribution of the library's acquisition budget and an allocation of funds to acquire material and build the archives and museum collections. They suggested it was good to have a fixed acquisition budget, which the archives and museum did not have before being merged with the library. Yet another interviewee suggested, _'we have turned the corner on acknowledging the importance of small-s, small-c special collections'_. Unlike the 1980s, when special collections were cut because they appeared to be _'frivolous'_, now special collections have _'emerged as core to our mission'_. They also noted that the Digital Library has had a strong uptake of digital materials in different formats.

The collaboration and convergence projects have facilitated staff learning, an important benefit for many interviewees. Marilyn Nasserden (Taylor Family Digital Library) suggested that learning about what other people do is interesting. Also connected to education, an interviewee at the University of Calgary indicated it is nice to share what they do (for instance, through the lunch lecture series) with other professionals. In a similar vein, an interviewee from the Archives noted that the changes at Archives New Zealand were providing staff with a better understanding of how to influence government agencies, because the Archives are now working with a group of people that are used to influencing government at a senior level all the time. Moreover, being part of an organization that is groomed to play a much bigger part in the public service is rewarding.

A clear example of how convergence has been beneficial to the Canadian Centre for Architecture is that it has successfully implemented new working methods for a more sustainable and innovative institution. A second benefit has been the rethinking of the traditional role of curation to a more transversal and open model. The new figure of the curator is centred on generating and overseeing a discourse, and not only a material collection. Another example of a benefit, as one interviewee explained, is working with people from outside the institution who produce things from different contexts and places. This can destabilize an institution, but in a positive way, such that the institution is confronted with situations to which it is forced to react.

## Discussion

The study examined two institutions in Canada and three in New Zealand that were involved in various collaboration and convergence initiatives. While the literature has expanded on various motivations for such initiatives, the study presents a picture of five institutions at various points along a continuum of convergence.

The research addressed four research questions, each discussed below.

> What motivates libraries, archives and museums to collaborate?

Our research identified a number of the factors that influenced the collaboration and convergence projects; many had been identified in previous research including the desire to serve users better ([Michalko 2007](#mic07); [Conway 2010](#con10); [Martin 2007](#mar07)) including providing better support for scholarly activity ([Hedstrom and King 2002](#hed02)), a desire to take advantage of opportunities provided by developments in information and communication technologies ([Ray 2009](#ray09); [Given and McTavish 2010](#giv10)) and the need for budgetary and administrative efficiencies. Our study, however, also found that an institution's understanding of the status of the digital surrogates of the objects, as well as the holistic view of collections that convergence is capable of providing, were also important factors. As previously mentioned, the very creation of Te Papa as a museum that houses art, history, Maori, and natural environment collections arose from a Maori understanding of objects as living cultures and not divided along traditional disciplinary lines. On the other hand, staff at the Taylor Family Digital Library suggest that the digital environment changes our views of objects and breaks down traditional divisions that differentiate among library, museums, special collections and archival material, while the Canadian Centre for Architecture was motivated by a wish to eliminate silos and foster a more holistic view of collections.

> What processes do libraries, archives and museums follow when planning and implementing collaborative projects?

In all institutions we found that almost all the collaboration and convergence projects involved team-based planning, though a top-down approach to management intervened at times. The emphasis placed on involving teams that worked across disciplinary boundaries suggests that professionals working in converged environments need a good understanding of the different professional philosophies and values at play, as well as the ability to work in teams. This finding also reinforces the need identified by, for example, Chaffin Hunter _et al._ ([2010](#cha10)) and Kalfatovic _et al._ ([2008](#kal08)) for more open communication, flexibility and respect for differences among library, archive and museum professionals. As Cox and Larsen ([2008](#cox08)) and Given and McTavish [(2010)](#giv10) suggest, the interdisciplinary work promoted by iSchools could be an incubator for cultivating these qualities.

> What are the challenges of collaborating across libraries, archives and museums?

Almost all the collaboration and convergence projects we studied experienced challenges that staff had to overcome. Te Papa had previously physically converged into one building but both institutions continue to experience silo mentalities, also an issue for the Canadian Centre for Architecture. Issues of different professional identities and values were also noted in a number of cases. The need to work in interdisciplinary teams while still developing and respecting different types of expertise challenged many of the projects. Developing a culture of sharing information and communicating across and within teams also challenged the organizations. Again, the interdisciplinary education promoted by iSchools might help to educate professionals who will be able to work, share and communicate across disciplines. However, a tension remains for organizations trying to foster broad expertise that spans many different types of knowledge while still ensuring great depth of this expertise. Some participants remarked that they felt, at times, their expertise was not respected which, in the case of Te Papa, led to staff turnover and poor morale. Converging different access systems and metadata schemes developed for describing library, archives and museum objects, respectively, were also identified as problematic. A framework or model for integrating professional metadata schemes might help to alleviate problems of merging descriptive systems. While the resource description framework (RDF) has been developed to accommodate syntactic integration, semantic and conceptual integration has remained a greater challenge. The mapping of the [CIDOC conceptual reference model](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.cidoc-crm.org&date=2013-08-06) (CRM) and functional requirements for bibliographic records (FRBR) model may offer a promising example of a way forward.

> What are the benefits of collaboration among libraries, archives and museums?

The most important benefit of the collaboration and convergence project was improved services to users. Previous research ([Michalko 2007](#mic07); [Conway 2010](#con10); [Martin 2007](#mar07)) also corroborated by this study indicates that providing better services was an important goal for collaboration and we found that the staff involved in the collaboration projects believed that their projects achieved this goal. At the same time the convergence of the library, archives and museum at the University of Calgary (Taylor Family Digital Library) achieved a greater understanding of the value of different types of collections including digital surrogates. This resulted in better funding for the departments that have historically been underfunded, though some participants suggested that the expertise of the professionals who care for these types of collections were not always valued. Finally collaboration and convergence projects provided opportunities for library, archive and museum professionals to develop a better understanding of each individual profession's traditions, perspectives, and approaches.

## Limitations

There are inherent limitations to this exploratory research that should be highlighted. We conducted interviews at only five institutions, which were readily available to the researchers. Furthermore, the analysis is based on interviews with only nineteen participants.

Second, our interviews were conducted at different levels of management, and depended on the willingness of staff to become involved, and, in some instances, to remain involved for the duration of the project (some withdrew following their review of the interview summaries).

Third, as always, one of the consequences of relying on interviews as a technique is undoubtedly the level of self-censorship that occurs, even (or perhaps especially) when interviewees agree to be identified by name.

## Conclusions

This research demonstrates that the processes that have guided recent initiatives of collaboration and convergence amongst the institutions surveyed are distinct and do not adhere to any single model of convergence or collaboration. There are many reasons for this, ranging from the nature of the entities engaged in convergence or collaboration (the merging of two institutions or an inter-institutional convergence), to the nature of the services and activities to be modified by this initiative (for example, collections convergence, joint digital preservation strategies, merger of reference services, etc.). This diversity in itself demonstrates the complexity of the process referred to as convergence in libraries, archives and museums.

We identified a range of motivations underlying the desire to engage in some form of convergence and which broadly correlate to providing better forms of support for users (be these needs generated by the general public or more specialized scholarly use) and to responding to an evolving technological landscape (such as the proliferation of digital objects, and digital convergence in cataloguing systems). There are, to be certain, equally budgetary and administrative efficiencies that also come to the fore. For some institutions, such as the Museum of New Zealand Te Papa Tongarewa in Wellington, the net result is a more holistic understanding of the possibilities of the institution's collections and holdings, as previously distinct collections are now considered in terms of new relational perspectives. While the planning for convergence initiatives was found to be relatively similar amongst the case studies we analyzed (involving consultation with stakeholders, collaborative undertakings and often team-based approaches), so too do the challenges evoked by the interviewees demonstrate similar, and to a certain extent, unsurprising, themes. Notably, it is a question of adaptation, as historically distinct professional identities and cultures learn the languages, practices, and expertise of their colleagues. These challenges notwithstanding, important benefits were cited in terms of improvements to public use and staff learning, while ultimately leading toward a more sustainable institutional culture (such as at the Canadian Centre for Architecture).

As a whole, the case studies point to common concerns among collaborative and convergence projects. They suggest that collaborating across these institutions is not easy or simple. Across the five institutions we studied, providing unified access was viewed as necessary to meeting the needs of users. While some remained wedded to physical objects and the different needs of those objects (and acknowledging that the reading of a museum object is not the same as the reading of a library text, as suggested by Martin ([2007](#mar07)), improving the support of users and scholarship overall were seen as important tasks requiring the breaking down of silos among libraries, archives and museums.

Educational programmes, such as those provided by iSchools, may help to alleviate some of the focus on differences of library, archive and museum collections, while providing a greater perceived value for digital surrogates. Interestingly, it appeared that the institutions we studied are attempting to find the right balance between respecting professional expertise and merging systems. The success of convergence projects seems to require that one find this balance.

The greatest challenges for professionals working in institutions undergoing convergence or collaboration continue to exist in many of the same areas identified in the literature, including in professional cultures, values, and identities; in recognizing and respecting the expertise of a particular discipline; in sharing a common understanding of different work practices, processes and outputs; and in merging or reconciling variant standards. While each poses its own obstacles to full institutional convergence, each also presents opportunities for further examination and research. How do we foster a culture of respect for different professional knowledge and skills while still promoting closer integration? Do problems in common also suggest uniformly applicable solutions? Finally, what do findings from a study such as this imply about the role of education, or even a role _for_ education and training in the midst of transformational change within libraries, archives and museums? For example, what might a model curriculum for educating professionals look like? This study offers another piece in the convergence puzzle that remains very much a work-in-progress.

## Acknowledgements

We thank the Gladys Krieble Delmas foundation for funding this research. The Canadian Centre for Architecture, the University of Calgary, Taylor Family Digital Library, the Museum of New Zealand Te Papa Tongarewa, The National Library of New Zealand, Te Puna Mātauranga o Aotearoa, and Archives New Zealand, Te Rua Mahara o te Kawanatanga, for agreeing to participate in our study. We thank Juan Ilerbaig who helped with the drafting of the interview summaries. We also thank the referees, editor (Charles Cole) and copy-editor (Amanda Cossham) for their suggestions for improving the paper. Finally we thank Katrina Cohen-Palacios and Christina Rutherford who completed the HTML coding.

## <a id="author"></a>About the authors

**Jennifer Carter** is Professor of New Museology in the Département d''histoire de l'art L'Université du Québec à Montréal. Her research and publications consider the intersection between museums and representation in the forms of architecture and exhibition practice. A second area of investigation is human rights museology and its connections to social justice in an emergent number of museums around the world. She can be contacted at [carter.jennifer@uqam.ca](mailto:carter.jennifer@uqam.ca).  
**Joan Cherry** is Professor Emeritus in the Faculty of Information at the University of Toronto. Her research focuses on a variety of topics in the area of human-computer interaction. Some of her research has also focused on education for the information professions including a survey of student perceptions of masters programs in information schools in Canada. She can be contacted at [joan.cherry@utoronto.ca](mailto:joan.cherry@utoronto.ca).  
**Wendy Duff** is a Professor in the Faculty of Information at the University of Toronto. Her research and publications focus on archival users, evaluation and the impact of archives on social justice. She can be contacted at [wendy.duff@utoronto.ca](mailto:wendy.duff@utoronto.ca).  
**Lynne Howarth** is Professor and Associate Dean, Research, at the Faculty of Information, University of Toronto, Canada. Her research includes the study of artefacts as memory surrogates for individuals experiencing cognitive decline, while teaching is focused on metadata standards and applications across libraries, archives, and museums. She can be contacted at [lynne.howarth@utoronto.ca](mailto:lynne.howarth@utoronto.ca).  
**Heather MacNeil** is an Associate Professor in the Faculty of Information at the University of Toronto. Her research and publications focus on archival history and theory, the arrangement and description of archives, and the trustworthiness of records in analog and digital environments. She can be contacted at [heather.macneil@utoronto.ca](mailto:heather.macneil@utoronto.ca).

<section>

## References
 
<ul> 
<li id="bak09">Bak, G. &amp; Armstrong, P. (2009). Points of convergence: seamless long-term access to digital publications and archival records at
Library and Archives Canada. <em>Archival Science</em>, <strong>8</strong>(4), 279-293. </li>
<li id="bea07">Beasley, G. (2007). Curatorial crossover: building library, archives, and museum collections. <em>RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage</em>,<strong>8</strong>(1), 20-28. </li>
<li id="bir08">Birtley, M. &amp; Bullock, V. (2008). Will collections vanish in the urge to converge? Observations on convergent evolution in the collections
sector. <em>Archives &amp; Manuscripts</em>, <strong>32</strong>(2), 128-147.</li>
<li id="cha10">Chaffin Hunter, N., Lekk, K. &amp; Oehlerts, B. (2010). Two librarians, an archivist, and 13,000 images: collaborating to build a digital
collection. <em>Library Quarterly</em>, <strong>80</strong>(1), 81-103.</li>
<li id="con10">Conway, P. (2010). Preservation in the age of Google: digitization, digital preservation, and dilemmas. <em>Library Quarterly</em>,
<strong>80</strong>(1), 61-79.</li>
<li id="cox08">Cox, R. &amp; Larsen, R. (2008). iSchools and archival studies. <em>Archival Science</em>, <strong>8</strong>(4), 307-326.</li>
<li id="cul08">Cultural Heritage Information Professionals Workshop (2008). <em><a href="http://www.webcitation.org/6GlFHttSf ">Purpose</a></em>.
Retrieved from http://chips.ci.fsu.edu/purpose.html (Archived by WebCite at http://www.webcitation.org/6GlFHttSf) </li>
<li id="dou07">Doucet, M. (2007). Library and Archives Canada: a case study of a national library, archives, and museum merger.<em> RBM:
A Journal of Rare Books, Manuscripts, and Cultural Heritage</em>,<strong> 8</strong>(1), 61-66.</li>
<li id="eli07">Elings, M.W. &amp; Waibel, G. (2007). <a href="http://www.webcitation.org/6GlF3Y4sI">Metadata for all: descriptive standards
and metadata sharing across libraries, archives and museums</a>. <em>First Monday</em>, <strong>12</strong>(3). Retrieved from  http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1628/1543 (Archived by WebCite at http://www.webcitation.org/6GlF3Y4sI) </li>
<li id="eme09">Emery, D., Toth, M. &amp; Noel, W. (2009). The convergence of information technology and data management for digital imaging in museums. <em>Museum Management and Curatorship, </em><strong>24</strong>(4), 337-356.</li>
<li id="fra10">France, F., Emery, D. &amp; Toth, M. (2010). The convergence of information technology, data and management in a library imaging program. <em>Library Quarterly</em>, <strong>80</strong>(1), 33-59.</li>
<li id="gib07">Gibson, H., Morris, A. &amp; Cleeve, M. (2007). Links between libraries and museums: Investigating museum-library collaboration in England and
the USA. <em>Libri</em>, <strong>57</strong>(2), 53-64.</li>
<li id="giv10">Given, L. M. &amp; McTavish, L. (2010). What's old is new again: the reconvergence of libraries, archives, and museums in the digital age.
<em>Library Quarterly</em>, <strong>80</strong>(1), 7-32.</li>
<li id="gwi09">Gwinn, N. (2009). <em><a href="http://www.webcitation.org/6GlFLwr4F">LAMMs and international collaboration</a>.</em>
Paper presented at Scientific Symposium: Changing World, Changing Views of Heritage: the Impact of Global Change on Cultural Heritage:
Technological Change, 7 October 2009, Valletta, Malta. Retrieved from http://www.international.icomos.org/a
dcom/malta2009/pdf/ADCOM_200910_SYMP_1_Documentation_Nancy_Gwinn.pdf (Archived by WebCite at http://www.webcitation.org/6GlFLwr4F)</li>
<li id="hed02">Hedstrom, M.L. &amp; King, J.L. (2002). <em><a href="http://www.webcitation.org/6GlFQ4bkA">On the LAM: library, archive, and museum collections in the creation and maintenance of knowledge communities</a>.</em> Paris: Organization for Economic Cooperation and
Development. Retrieved from http://www.oecd.org/dataoecd/59/63/32126054.pdf (Archived by WebCite at http://www.webcitation.org/6GlFQ4bkA) </li>
<li id="hux05">Huxham, C. &amp; Vangen, S. (2005). <em>Managing to collaborate: the theory and practice of collaborative advantage.</em> London: Routledge.</li>
<li id="jen06">Jenkins, H. (2006). <em>Convergence culture: where old and new media collide</em>. New York, NY: New York University Press.</li>
<li id="kal08">Kalfatovic, M., Kapsalis, E., Spiess, K.P., Camp, A. &amp; Edson, M. (2008). Smithsonian Team Flickr: a library, archives, and museums collaboration in Web 2.0 space. <em>Archival Science</em>, <strong>8</strong>(4), 267-277.</li>
<li id="kir08">Kirchhoff, T., Schweibenz, W. &amp; Sieglerschmidt, J. (2009). Archives, libraries, museums and the spell of ubiquitous knowledge. <em>Archival
Science,</em><strong>8</strong>(4) 251-266.</li>
<li id="log08">Logan, D., King, J. &amp; Fischer-Wright, H. (2008). <em>Tribal leadership: leveraging natural groups to build a thriving organization</em>. New York, NY: HarperCollins</li>
<li id="mar07">Martin, R.S. (2007). Intersecting missions, converging practice. <em>RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage</em>,<strong> 8</strong>(1), 80-88.</li>
<li id="mea05">Meads, G., Ashcroft, J., Barr, H., Scott, R. &amp; Wild, A. (2005). <em>The case for interprofessional collaboration: in health and social care.</em> Oxford: Blackwell.</li>
<li id="mic07">Michalko, J. (2007). Libraries, archives, and museums: achieving scale and relevance in the digital age. <em>RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage,</em> <strong>8</strong>(1), 75-79.</li>
<li id="mor02">Moran, J. (2002). <em>Interdiciplinarity</em>. London: Routledge.</li>
<li id="pau11">Paulus, M.J. (2011). The converging histories and futures of libraries, archives, and museums as seen through the case of the curious collector Myron Eells. <em>Libraries &amp; the Cultural Record</em>, <strong>46</strong>(2), 185-205.</li>
<li id="pij07">Pijeaux, L. (2007). The Birmingham Civil Rights Institute: a case study in library, archives, and museum collaboration. <em>RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage,</em> <strong>8</strong>(1), 56-60.</li>
<li id="ray09">Ray, J. (2009). Sharks, digital curation, and the education of information professionals.<em>Museum Management and Curatorship,</em> <strong>24</strong>(4), 357-368.</li>
<li id="ree07">Reed, M (2007). Blurring the boundaries: collaborative library and museum exhibitions in Los Angeles. <em>RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage, </em> <strong>8</strong>(1), 45-50.</li>
<li id="tan09">Tanackovic, S.F. &amp; Badurina, B. (2009). Collaboration of Croatian heritage institutions: experiences from museums. <em>Museum
Management and Curatorship,</em> <strong>24</strong>(4), 299-321.</li>
<li id="tim09">Timms, K. (2009). New partnerships for old sibling rivals: the development of integrated access systems for the holdings of archives, libraries,
and museums. <em>Archivaria</em>, (No. 68), 67-95.</li>
<li id="tra09">Trant, J. (2009). Emerging convergence? Thoughts on museums, archives, libraries, and professional training. <em>Museum Management and Curatorship, </em> <strong>24</strong>(4), 369-387.</li>
<li id="wai09">Waibel, G. &amp; Erway, R. (2009). Think globally, act locally: library, archive, and museum collaboration. <em>Museum Management and Curatorship, </em><strong>24</strong>(4), 323-335.</li>
<li id="whi07">Whiteman, B. (2007). Cooperative collection building: a response to Gerald Beasley. <em>RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage,</em><strong> 8</strong>(1), 29-34. </li>
<li id="wyt07">Wythe, D. (2007). New technologies and the convergence of libraries, archives, and museums.  <em>RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage,</em> <strong>8</strong>(1), 51-55.</li>
<li id="zor08">Zorich, D., Waibel, G. &amp; Erway, R. (2008). <em><a href="http://www.webcitation.org/6GlFV550Z ">Beyond the silos of the LAMs. Collaboration among libraries, archives and museums</a>.</em> Dublin, OH: OCLC Programs and Research. Retrieved 1 November, 2012 from http://www.oclc.org/resources/research/publications/library/2008/2008-05.pdf (Archived by WebCite at http://www.webcitation.org/6GlFV550Z) </li>
</ul>

</section>

</article>