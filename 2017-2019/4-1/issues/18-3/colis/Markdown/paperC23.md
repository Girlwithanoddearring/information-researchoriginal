<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Cultural-historical activity theory and domain analysis: metatheoretical implications for information science.

#### [Lin Wang](mailto:wanglinpku@163.com)  
Tianjin NormalUniversity, School of Management, Binshuixidao 393 Xiqing Distinct, 300384 Tianjin, China, People's Republic of

#### Abstract

> **Background**. cultural-historical activity theory is an important theory in modern psychology. In recent years, it has drawn more attention from related disciplines including information science.  
> **Argument**. This paper argues that activity theory and domain analysis which uses the theory as one of its bases could bring about some important metatheoretical implications for information science.  
> **Elabouration**. After briefly reviewing the sociocultural historical theory and activity theory, this paper analyses the implications of these theories for information science, which includes reinterpretation of the goal of information science and a holistic view on user studies, on a metatheoretical level. It explains the lessons of _broad information outlook_ in China from the perspective of the domain analytic paradigm in information science and provides some enlightenment about the disciplinary construction of information science.  
> **Conclusion**. Metatheoretical implications of activity theory and domain analysis for information science discussed in this paper can provide guidelines for theory construction of information science. Domain analytic paradigm and its related theories in other disciplines are the most appropriate and fruitful orientation of information science at present and in the future.

<section>

## Introduction

Recently domain analysis, which was put forward by Hjørland and Albrechtsen in 1995, has become an important paradigm in information science. One of its main theoretical sources is the cultural-historical activity theory whose academic representatives are Vygotsky and Leontiev, among others ([Hjørland and Albrechtsen, 1995](#hjorland95); [Hjørland, 1997](#hjorland97)). The importance of Vygotsky’s sociocultural-historical theory and Leontiev’s activity theory is increasingly recognized by information science scholars (e.g., [Albrechtsen et al., 2001](#albrechtsen01); [Bødker, 1991](#bodker91); [Meyers, 2007](#meyers07); [Spasser, 1999](#spasser99), [2002](#spasser02); [Widén-Wulff and Davenport, 2007](#widen07); [Wilson, 2008](#wilson08)). It is sometimes put forward using other labels such as sociocultural theory (e.g., [Sundin and Johannisson, 2005](#sundin05)). It is my opinion that both actvity theory and domain analysis play crucial roles and have fundamental implications for information science at the metatheoretical level. This paper explores and elucidates some kinds of metatheoretical implications actvity theory and domain analysis bring about after briefly reviewing Vygotsky and Leontiev’s theories.

## Vygotsky’s sociocultural-historical theory

### Brief review

Vygotsky(1896-1934)was a famous psychologist in the former Soviet Union and also the founder of the sociocultural-historical school. As an outstanding psychology theorist in 20th century, Vygotsky transformed classical psychology. By using historism as the foundation of psychology, he founded the social-cultural origin theory of higher mental function, _sociocultural-historical theory_ and was one of the classics of modern psychology ([Gong, 1997](#gong97); [Wang, 2002](#wang02)). He has been also named _Mozart of psychology_ ([Toulmin, 1978](#toulmin78)).

According to Vygotsky, psychological functions can be divided into two types: lower mental functions and higher mental functions. The latter have characteristics that are totally different from the former and are the most essential forms of human psychology ([Du and Gao, 2004](#du04)).The origin of higher mental functions is not in an individual organism but outside of it, and can only be found in social life and interpersonal interaction. It is the product of social, historical development and is restricted by social rules. From the point of individual development, it is generated and developed in the process of social interaction activities ([Gong, 1985](#gong85)). That is to say, the human-specific psychological function is not generated internally but from people’s collabourative activities and interactions. Human’s structure of psychological process must be formed initially in people’s external activities and then transferred to the internal and becomes the internal structure of psychological process ([Gao, 1999](#gao99)). The primary characteristic of humanity is “sociality”, and all of the higher mental functions of humanity are derived from social interaction ([Shi, 2007).](#shi07)

[Vygotsky (](#shi07)[2004a: 114,130](#vygotsky04a)) discusses his view from the angle of children’s psychological development. Any higher mental function is a social function originally, which is the core issue of all internal and external activities. In the cultural development of children, every higher mental function appears twice at two sides. The first is social, interpersonal relationship between minds, and the second is the psychological category within children; all higher mental functions are the internalization of social relationships ([Vygostky, 2004b:388](#vygotsky04b)). The psychological development should be understood from historical points instead of abstract points, from its indivisible relations with the social environment instead of isolations from social environment ([Wang, 2009:19](#wang09)).

Vygotsky investigates human psychological development in the social-historical context and places socioculture and history prior to all other factors influencing the development of human’s mental functions; he highlights the decisive effect of social and culture factors to individual psychological development ([Huang, 2004](#huang04)). In a word. The nature of human psychology is the summation of social contexts, which transfers to the internal field of individual and becomes personal skills and the structural forms of the skills. Since individual psychological development is a transfer process of social sharing activities to internal psychological process, and the social relationships are internalized to form individual psychological structure, therefore, by studying the general rules of socioculture development, we can clearly elucidate the individual psychological development ([Gao and Ren 2004](#gao04); [Ma, 2004a](#ma04a), [2004b](#ma04b)).

The intermediary principle of higher mental functions is an important component of Vygotsky’s sociocultural-historical theory. According to Vygotsky, compared with lower mental functions, higher mental functions have an additional intermediary means in functional structure and have the property of indirectness. Its realization depends on a variety of supplementary means, like language and all kinds of symbol systems that can be called psychological or mental tools ([Wang, 2009](#wang09): 21, 108). Social activities in domains determine the forms and meanings of mental tools like concepts, which vary across social contexts and cultures ([Hjørland, 1998](#hjorland98)). With the help of mental tools as intermediary means, people can have mental production and psychological operation, and then change the psychological functions to higher levels qualitatively ([Gong, 1981](#gong81); [1985](#gong85)). That is to say, as practical activities use labour instruments as an intermediary means in material production, human’s higher mental functions also use sign systems as intermediary means ([Gong, 1981](#gong81); [Zhang, 1999](#zhang99)). Without the intermediary means like symbols, the formation and realization of higher mental functions are impossible.

The sign system is also the product of interpersonal relationships and cultural-historical development, being the carrier of social-cultural knowledge and pattern. It is the intermediary agent of between the external and the internal, social and individual, promoting the transformation of social behavior patterns to individual behavior patterns and the transformation of external psychological functions to internal psychological functions ([Gong, 1985](#gong85); [Gao, 1999](#gao99); [Zheng and Ye, 2004](#zheng04)). Vygotsky’s viewpoint ([Gong and Huang, 2004:12](#gong04)) that symbols system is originally a means with the social purpose, a means to influence others and then a means to influence oneself explains this transformation process vividly.

In the aforementioned transformation process, since the mental tools like symbols condense human sociocultural-historical knowledge, the higher mental functions are infused with the culture, institutional and historical contexts used to generate mental tools. Thus basically the development of higher mental functions is not restricted by the laws of biological evolution any more but generated in the development process of sociocultural-historical development and governed by the historical laws of sociocultural development ([Gao, 1999](#gao99); [An, 2004](#an04)). This is the essence of sociocultural-historical theory. This theory forms the basis of social-cognitive viewpoint in domain analysis ([Hjørland, 2004a](#hjorland04a)).

### The goal of information science from the perspective of sociocultural-historical theory

In general, mental tools refer to information resources and all things that have potential information. In the networked digital age, handling mental tools required by higher mental functions manifests itself as information literacy ([Ren, 2004](#ren04)). Basing on such view, our focus moves to information science. One goal of information science is to organize information resources, so that users will be able to access relevant information conveniently, satisfy their own needs and adequately utilize information. This information work process is actually a process of operating on knowledge representing signs. The objects of information science work—information resources, are actually the mental tools (signs) that work as a medium for the development of human’s higher mental function. Based on this theory, we can report the goal of information science as providing intermediary means as mental tools for the formation and development of information users’ higher mental functions. In other words, it is to provide value–added ordered sign system for them. One unique role of information science is its brokerage of transferring sociocultural and historical processes, social activities and social relationships into the internal mental structure of individuals, enhancing human information literacy as higher mental functions, while realizing that this is depending on collecting, organizing and managing information resource as signs. I name such a proposed opinion as the “new intermediary view” of information science.

In information science one of the features of the _new intermediary view_ is that it promotes information science to the level of casting critical influence on generation and maintenance of human society, which refers that it is an important form among mediums that works as a transmission method between socio historical, cultural contexts, and human’s internal psychological structures Without such form, sociocultural, historical traditions and social behavior patterns developed in human society will not be able to be efficiently transformed into individuals’ cognition and psychological activities easily, which will result in delay on individual development and then delay on the heritage of socioculture and history, as well as knowledge progress.

In addition, the _new intermediary view_ can also revitalize the social and cultural traditions of information science. The object of the information work—sign system, is produced by socioculture, it changes as socioculture and history change. Semantic relations of signs depend on specific socioculture and history ([Ma, 2006](#ma06)). According to Hjørland ([2000](#hjorland00), [2007](#hjorland07)), signs, being distinct from data and facts, evolve to take certain roles regarding standardized practices in communities. The functional values in relation to that practices and social interests that signs supposed to meet are the basic issues of semiotics.

In information science, for understanding and organizing sign system and adequately making use of its functions as the intermediary, it is necessary to study on cultural and historical context inherited by signs, and possess overall understandings of the origins of signs, i.e., social interactions and social contexts of activities. Only under such conditions the most perfect match between semantics of signs organization (i.e., knowledge organization like classification, indexing and tagging) and cultural, historical meanings, meanings of social knowledge and social behaviour patterns carried by signs may be realized. It is highly possible that automatic algorithms (auto-classification and clustering) that simply focus on statistical characters of signs, disconnect with practical social and cultural contexts can distort the inherent meanings laden by signs. This leads information science the intermediary role of serving users’ development cannot be realized in a good form.

Research on social, cultural and historical contexts and social activities has implied that information science should attach great importance to its features in aspects of humanities and social science instead of simple dependence on the method of taking technology development as the only support. It should integrate technology tradition with sociocultural tradition, instrumental rationality with value rationality in information science, inclining to the latter ones more.

## Activity theory

### Evolution of activity theory

Activity theory is closely connected with the sociocultural-historical theory. Activity theory is the psychological theory developed and systematized comprehensively by Leontiev (1903-1979). Being the mainstream principle of psychology in the former Soviet Union, it caused a great reaction after being introduced to western countries in the last 1970th. The Finland scholar Engeström has developed the model of the activity theory, which promotes its widespread application to many research fields like information management, information system engineering, and human-computer interaction, etc.

As the first-generation pioneer of activity theory, Vygotsky introduces the concept of _practice_ to research and points out that practice should become the foundation of modern psychology: the changes of the human psychological process are the same to the changes of his practical activities. What’s more, he realizes the important role of activity in the formation of psychological functions, with activities as the central concept of his theory ([Gong, 1997](#gong97); [Gao, 1999](#gao99)). The basic principle of sociocultural-historical theory ([Gong, 1981](#gong81), [1985](#gong85)) is that human’s psychology is developed in human activities, and evolves during interpersonal interaction, which is the most important form of activities. Higher mental functions are the continuous internalization of social activities and social interactions. The before-mentioned transformation of human’s psychological development from inter-psychological process to intra-psychological process is realized through the reflection and internalization of practical activities that are initially social and external to individual. Activity, culture, symbol, language and internalization altogether unify individual with the society organically ([Gong, 1985](#gong85); [Shi, 2007](#shi07)).

Leontiev was the student of Vygotsky. He inherited the main academic viewpoints of Vygotsky and had systematically studied the problems of activity in psychology for fifty years since the 1930s. It is he who officially put forward and perfected activity theory. His masterpiece _Activities, Consciousness, Personality_ is a classic of activity theory. He became the core leader of actvity theory after Vygotsky.

### Basic conceptions of activity theory

The basic assumptions of Leontiev’s activity theory ([Wang,2009:129-131;](#wang09) [Zhang, 1987](#zhang87)) include the following viewpoints: consciousness is the products of social labour and social relationships; external practical activities, which combine the subject with the reality of objects, generate individual internal psychological activities and so on. Leontiev ([Zhang, 1987](#zhang87)) believes that the binomial schema “stimulation—reaction” in the psychological methodology excludes the plentiful process of connecting the subject with the reality of the objective world from the research horizon, which is the source of difficulty in further progress. Trinomial schema should be accepted, which includes intermediate link—activity, its corresponding conditions, goals and means into the binomial schema. Activities, especially the practical activities play a critical role in mediating subject and object, this subject and other subject; it is activities that determine the cognitive ability of the subject ([Zhang, 1987](#zhang87)).

Based on such presuppositions, Leontiev thinks that activities generate psychology and psychology is the special form of activities and also the derivative of material life; external activities are transformed into internal and conscious activities in the developmental process of society and history ([Zhao, 1997](#zhao97)). Human activities constitute the substance of human consciousness. Therefore, the structure and internalization of activities become the central task of psychological study, and the key mechanism of human psychological development is to master all kinds of activities formed in the society and history and transform them into the mechanism of the internal psychological process ([Zhao, 1997](#zhao97); [Shi and Chen, 2003](#shi03)). In this way, in the beginning of human cognition and psychology is activity; and the source of mental development is summed as the interactions between subject and environment. By doing so, activity theory studies “activity” as the logical beginning and central concept to explain the problems concerned with the human psychological generation and development ([Yang, 2000](#Yang00)).

As for the classification of the activities, Leontiev ([1980:57](#leontiev80)) divides activities into external practical activities and internal psychological activities and believes that the former is the basic form of activities and the source of all latter forms.According to activity theory ([Wang, 2009](#wang09)), human psychology can only be given scientific explanations within the discourse of realistic practical activities.

The relationship between external practical activities and internal psychological activities can be classified as internalization and externalization. The process of generalizing, verbalizing and simplifying practical activities and transforming them to generate relatively independent internal intelligence is called internalization; the transformation from internal psychological activities to external activities is called externalization. The reason that these two transformations can take place is the external activities and the internal activities share the same structure ([Zhang, 1985](#zhang85); [Shi and Chen, 2004](#shi04)). Leontiev specially emphasizes that the study of external activities in psychology is not to separate the including psychological elements from it but analyse them with “activities” as one unit. The unit includes elements of cognitions and actions that are closely related and cannot be separated. Therefore, the object in this kind of psychological research is not isolated and pure psychological functions, but activity system ([Li, 1979](#li79); [Zhao, 1997](#zhao97)).

In activity theory ([Wikipedia, 2013](#wikipedia13)), tools are the intermediary means of external activities, and are also the means to accumulate and transmit social, historical and cultural knowledge. Through the external activities with tools as intermediary, individual can absorb human accumulated experience and the production methods of social, historical and cultural knowledge such that the structures of the psychological process are similar to socioculture structures. The absorption of this kind of knowledge and methods is carried out in the interaction with other people and can only be realized in the form of external activities. Therefore, higher mental process can only be generated in the interpersonal interaction; it is an inter-psychological process at first, then loses external forms gradually to transform into an intra psychological process ([Li, 1979](#li79); [Lu _et al._ 2007](#lu07)). This fully reflects Leontiev’s inheritance and development of Vygostky’s thought.

With activity theory as the basis, the three basic principles of psychological study are confirmed: the activity view, the social historism of human psychological development, and the unification of consciousness and activities ([Zhao, 1997](#zhao97)).

Since activity theory was introduced in western countries in the 1970s, it has stirred strong interest in academia. A group of scholars with Engeström as the representative have perfected activity theory with better maneuverability through the modeling and formulation, leading to good effect of application to many research areas on information science. However, we should remember what Wilson said, '_Activity theory was intended as way of arriving at contributions to understanding the nature of human consciousness, not as a tool for the investigation of information systems_' ([Wilson, 2006](#wilson06)). It is necessary to consider seriously the fundamental impact of activity theory on the core issues of information science such as cognition, relevance and information behaviour on the metatheoretical level. Hjørland ([1997](#hjorland97)) is a milestone in the application of activity theory to such issues metatheoretically.

### A holistic view of user studies with sociocultural and historical contexts as its foundation

Most present user studies in information science treat behaviour and psychology as two parallel development lines that can barely be connected. On the one side, Researches on user behaviour (such as information seeking and knowledge utilization) focus on analysis towards user actions like search strategy, query formulation and communication frequency, while explorations on mental mechanism hidden behind such behaviour are left with ignorance. On the other side, researches on user psychology usually focus on human’s internal mental mechanism, such as user knowledge structure, mental model, and need analysis, while deepened researches on relevant behaviour have seldom been conducted.

In general, there are no apparent overlaps between behaviour studies and psychology studies in information science. A few scholars, like Wilson, have already launched their work on trying to integrate these two main trends into one consistent model ([Wilson, 1999](#wilson99); [Bedny, _et al._ 2001](#bedny01)). However, they seldom offer explanations for doing so on a metatheoretical perspective. I take the view that activity theory can become the foundation for the holistic view of user studies that works on integrating behaviour with psychology from a meta-theoretical level. Activity theory upholds consistency between mind and behaviour. It is believed that study on activities can never be done alone, but shall be unified with study on mind. To conduct research on (external) activities requires to contain mental reactions in the unit of activity, treating psychological elements and behaviour elements as one tight connection that cannot be broken up ([Li, 1979](#li79)). Such view indicates that user’s information behaviour and cognition shall be aggregated into one analytical unit, i.e., unit of activity, for research.

From the perspective of activity theory, neither behaviourism that only focuses on user behaviour without any consideration about cognition, nor mentalism that only focuses on user cognition without any analysis on external behaviour is suitable to be the basis of user studies. We should take the holistic view that unifies behaviour and cognition as one research unit as the starting point and final target for user studies and user modeling. Developing information behaviour-cognition unified model is probably the most promising research direction.

There’s no doubt that there still exists a sequence issue of research in the horizon of the holistic view. In light of activity theory, which holds that practical activity is the origin of human cognition such that they share the same structure, it is appropriate to follow the research sequence of information behaviour at first, cognition secondly on user studies to track down the origin of user cognition to user behaviour.

Furthermore, the meaning of behaviour depends on context. Context ought to be the critical focus in the unified research model. As for the context of activity, Wilson ([2006](#wilson06)) authoritatively concludes that there are two kinds of contexts affecting information behaviour. The former is macro-context, including the cultural-historical setting and the relationship of between activity and external environment, and the latter is micro-context containing division of labour, rules, norms, artifacts as well as actors’ goals and motives.

On the ground of these arguments, it can thus be suggested the social, cultural and domain-specific aspects of behaviour and cognition should become the focuses of the unified research model. Leontiev points out human activities are the social and historical products, and sociability is the primary feature of activity. Because psychology is most complicated form of activity structure, so psychology or cognition is also produced by society and history when it comes down to it ([Wang, 2009: 130,133](#wang09); [Davydov, 2003](#davydov03)). Activity theory is also the domain-specific theory of noetics ([Hjørland,1997: 2](#hjorland97)). In a word, sociocultural, historical and domain-specific contexts govern and determine both the activity and cognition of individual or community. Hjørland’s socio-cognitive and sociological-epistemological view on information behaviour ([Hjørland, 2011](#hjorland11), [Hjørland and Nicolaisen, 2010](#hjorland10)) elucidates Leontiev’s points clearly and persuasively. During constructing the unified research model of information user, sociocultural and historical determinants and related epistemologies are the foremost _variables_ to be considered. Then on the basis of these we can construct the model by integrating user's information behaviour and cognition.

## Domain analysis: the best metatheory to explain the lessons of the _broad information outlook_

### _Information outlook_: from narrow to broad

It is still worthwhile for Chinese scholars to consider the _broad information outlook_ arisen in the 1980s in China deeply. Today the drawbacks negative consequence of the _broad information outlook_ Nevertheless, the “broad information outlook “it still has positive impact on the discipline construction of information science. Before the 1980s the mode of information science research and practice in China copied from the Soviet. Its features included: attaching great importance to scientific and technological documentation, exploring the principles of science and technology S&T information as the exclusive goal of information science. Information science research equalled to S&TIS research, which largely ignored various kinds of information in society. This kind of information view is termed as the _narrow information outlook_. Under such view, S&T information is the only research object in information science.

In order to cater to the needs of practical information work after Chinese opening and reforming in the 1980s, Lu ([1987](#lu87)) put forward the _broad information outlook_ against the narrow one. He contends we should extend the scope of research objects in information science, including every kind of social information. Since then the _broad information outlook_ became the mainstream of information science research in China ([Liu and Wang, 2001](#liu01)). It cannot be denied that the _broad information outlook_ played a positive role in the development of information science in the special historical period of China. One of the positive roles is to broaden the conception of information by pointing out that the content of information science research not only involves the S&T information phenomena, but also the phenomena of economic information, social information and other various kinds of information. This actually expands the conception of information from the discourse in a particular knowledge field (the field of science and technology) to the discourse shared by all fields of knowledge. Since then the information concept in China has been equaled with that in western countries (North America and Western Europe.)

Attributing to the _broad information outlook_,the consensus in information science at that time was the concept of information covered various fields of science and technology, social and economic production and life and in various types. In this way, the _broad information outlook_ made a sufficient theoretical preparation and laid solid groundwork for the subsequent information management oriented reform of information science research and education in the 1990s in China.

In China, however, what the _broad information outlook_ expected originally (namely, envision that the economic, social information science and other new branches of information science would develop prosperously, and an integrative information science unifies all branches would emerge) has not been realized. A systematic theory guiding actual work like the theory of S&TIS information science never established in the field of economic information science and social information science in China.

### What lessons of the _broad information outlook_ can be drawn?

From the metatheoretical perspective of domain analysis, the reason that the _broad information outlook_ fails information science that it does not consider the different characteristics of knowledge domains. If we consider the science and technology as a "large domain” consisting with a number of similar knowledge domains, then it is widely different from the domain of economy or social life in terms of domain features such as knowledge organizational structure, knowledge communication mode, forms of discourse, relevance judgment criteria and value of information, etc. These differences lead to the fact that the theories and models of information science supporting the knowledge activities in the domain of science and technology does not apply to the information problems in the economic and social field.

What the _broad information outlook_ does is to just mechanically copy the theoretical models of S&T IS in the economic and social domains, assuming those models are universal and suitable to au specific information science research in each knowledge domain. Under such a view, there is no comprehensive and accurate, analysis on the knowledge characteristics of the economic and social domains in advance, which should be regarded as the basis to design domain-specific solutions to practical information problems and construct the information science theory abstractly. This is also the case in the empirical users studies in western countries. From the perspective of domain analysis, many researches on users’ studies do not have high value because they do not take any domain-specific issues into consideration seriously ([Bawden and Robinson, 2012](#bawden12)).

The history of discipline development has proven that the S&T IS theories and models do not comply with knowledge structure, knowledge flow, and information ecology in the economic, social life and cultural domains. This results in the situation that the branches of information science develop slowly, while MIS and other related disciplines take advantage of developmental opportunities in China. Reviewing the business information management, MIS and other disciplines, we may find the secret of their successful rise lies in their research logic that is to proceed from knowledge phenomena in the economic and business domains and then base the work of theory construction and system design on the subject knowledge and information conventions relevant to the domains. This logic unconsciously stands on the angle of domain analysis to study, so the subject development makes a multiplier effect.

The _broad information outlook_ has a fault to regard ‘special’ as ‘general’. Today the revelation is valuable to us in information science research: not only practical work such as specific knowledge service and knowledge management, but also theoretical research like information philosophy should take knowledge domain and knowledge activities, knowledge structure and knowledge exchange within it as the starting points and the fundamental bases. Bawden and Robinson ([2012](#bawden12)) argue that concentrating on domain-specific resources, task and domain knowledge is the precondition to carry out worthwhile information users and use studies in that domain. Only by regarding knowledge domain as the basic unit of information science can powerful and specialized theories accumulate to lay the foundation for the development of general information science. I cannot agree with Hjørland's opinion anymore: it is necessary to distinguish the differences and similarities between knowledge domains for establishing a substantive general information science ([Hjørland, 2004b](#hjorland04b)). It is also a reflection of human understanding law from the particular to the general.

It is obviously dangerous to promote and apply information theories and models in the special knowledge domain for example, S&T domain as universal things in other domains without any change. On the contrary, when studying the environmental scanning behaviour of enterprise managers, Wang ([2004](#wang04)) recognizes that environmental scanning is the information behaviour occurring in the specific domain and explores it from the perspective of business operation and management domain. During studying the information behaviour such as scanning focus, information source selection and use, scanning mode of managers as a particular discourse community, the specificity and particularity of business domain have always been applied throughout the research. This work embodies the basic idea of domain analysis.

## Concluding remarks.

How to maintain the discipline identity and cultivate core competence of information science profession is a critical challenge in the digital age. Hjørland ([2002](#hjorland02)) argues that in order to improve status of information science and information science profession, it is vital for our is researchers to develop the knowledge and approaches of domain analysis. Holding the same position, I think domain analysis and its theoretical basis—actvity theory are the ideal knowledge for empowering information science researchers and professions to meet the challenge effectively, although there have been some debates on them until now. In this paper, I discuss some metatheoretical implications of actvity theory and domain analysis for information science, which can provide guidelines for theory construction of information science. It is my strong belief that domain analytic paradigm and its related theories in other disciplines are the most appropriate and fruitful orientation of information science at present and in the future.

## Acknowledgements

I really appreciate Professor Birger Hjørland for his warm academic discussions with me and valuable suggestions to my research during these years. This article is an achievement of youth project of Chinese National Social Science Fund “SME managers’ information seeking: behavioral patterns and influencing factors” [13CTQ040].

</section>

<section>

## References
<ul>
<li id="albrechtsen01">Albrechtsen, H., Andersen, H.H.K., Bødker, S. &amp; Pejtersen, A. M. (2001). Affordances in activity theory and cognitive systems engineering. Denmark. Risø: http://www.risoe.dk/rispubl/SYS/syspdf/ris-r-1287.pdf
</li>
<li id="an04">An, L.Y. (2004). Lev Vygotsky and constructivism (in Chinese) <em>Journal of Tianjin Academy of Educational Science</em>, (4), 51-55
</li>
<li id="bannon13">Bannon,L.(2013).What is activity theory? Retrieved June 1 2013. From:http://carbon.ucdenver.edu/~mryder/itc/act_dff.html
</li>
<li id="bawden12">Bawden, D. &amp; Robinson, L. (2012). An introduction to information science. <em>Facet Publishing</em>
</li>
<li id="bedny01">Bedny, G.Z., Karwowski,W., and Bedny, M. (2001). The principle of unity of cognition and behavior: Implications of activity theory for the study of human work. <em>International Journal of Cognitive Ergonomics</em>, 5(4), 401–420
</li>
<li id="bodker91">Bødker, S. (1991). Activity Theory as a challenge to systems design. In: H.-E. Nissen, H.K. Klein, and Hirschheim, R. (Eds.), <em>Information systems research:Contemporary approaches and emergent traditions</em> (pp. 551–564). Amsterdam: North-Holland
</li>
<li id="chen86">Chen,H.C. (1986). Recent discussions on the theory of activity in psychological circles in the Soviet Union (in Chinese) <em>Acta Psychologica Sinica</em>(2):215-223
</li>
<li id="davydov03">Davydov, V.V. (2003).Vygotsky and problems of education Psychology. In L.S. Vygotsky, <em>Educational Psychology</em>. [Translated by H.R. Gong, G.Y. Xu, S.D. Pan &amp; H.S. Liu] (pp. 18-55). Hangzhou, Chinea: Zhejiang Education Press.
</li>
<li id="du04">Du, D.K. &amp; Gao, W. (2004). Review on Vygostky’s educational thought. In L. Vygotsky, <em>Anthology on Lev Vygotsky’s education theory</em>. Translated by Z.Q. Yu. (pp. 1-23). Beijing: People’s Education Press. (In Chinese)
</li>
<li id="gao99">Gao, W. (1999). Vygotsky’s psychological development theory and social constructivism. (in Chinese) <em>Foreign Educational Files</em> (4), 10-14
</li>
<li id="gao04">Gao, W. &amp; Ren,Y.Q. (2004). Sociological analysis of knowledge production and acquisition. <em>Journal of East China Normal University. (Education Sciences)</em>, <strong>22</strong>(2), 7-13
</li>
<li id="gong81">Gong, H.R. (1981) Commentary on JT.C. Vygotsky’s culture-history development view (in Chinese) <em>Exploration of Psychology</em>, (3),46-54
</li>
<li id="gong85">Gong, H.R.(1985)On L. S.. Vygotsky's theory of higher-level mental functions (in Chinese) <em>Acta Psychologica Sinica</em> (1), 15-22
</li>
<li id="gong97">Gong, H.R. (1997). Lev Vygotsky and his contribution to modern psychology — starting from the international meeting for 100th birthday of Lev Vygotsky (in Chinese) <em>Psychology Development and Education</em>(4):61-64
</li>
<li id="gong04">Gong, H.R. &amp; Huang, X.L. (2004). Vygotsky’s contributions to psychology. In:Gong,H.R. and Huang,X.L.(eds) <em>Vygotsky’s Scientific Thought of Psychology in China</em> (1997).Haerbin: Heilongjiang Press:6-27
</li>
<li id="hjorland97">Hjørland, B. (1997). Information seeking and subject representation: An activity-theoretical approach to information science. <em>Westport, CT: Greenwood Press</em>
</li>
<li id="hjorland98">Hjørland, B. (1998). Theory and metatheory of information science. <em>Journal of Documentation</em>, <strong>54</strong>(5), 606- 621
</li>
<li id="Hjorland00">Hjørland, B. (2000). Documents, memory institutions and information science. <em>Journal of Documentation</em>, <strong>56</strong>(1), 27-41
</li>
<li id="hjorland02">Hjørland,B. (2002). The special competency of information specialists. <em>JASIST</em>, <strong>53</strong>(14), 1275-1276
</li>
<li id="hjorland04a">Hjørland, B. (2004a). Domain analysis: a socio-cognitive orientation for information science research. <em>Bulletin of ASIST</em> (February/ March), 17-21
</li>
<li id="hjorland04b">Hjørland, B. (2004b). Domain analysis in information science. IN: <em>Encyclopedia of Library and Information Science</em> pp. 1-7. New York: Marcel Dekker
</li>
<li id="hjorland07">Hjørland, B. (2007). Information: objective or subjective/situational? <em>JASIST</em> <strong>58</strong>(10), 1448-1456
</li>
<li id="hjorland11">Hjørland, B. (2011). The importance of theories of knowledge: browsing as an example. <em>Journal of American Society for Information Science and Technology</em>, <strong>62</strong>(3), 594-603.
</li>
<li id="hjorland95">Hjørland, B. &amp; Albrechtsen, H. (1995.) Toward a new horizon in information science: Domain Analysis. <em>JASIS</em>, <strong>46</strong>(6), 400-425
</li>
<li id="hjorland10">Hjørland, B. &amp; Nicolaisen, J. (2010). The social psychology of information use: seeking 'friends', avoiding 'enemies'. <em>Information Research</em>, <strong>15</strong>(3) colis706 Retrieved 22 August, 2013 from http://InformationR.net/ir/15-3/colis7/colis706.html.
</li>
<li id="huang04">Huang, X.L. (2004). The way Vygostky studied social origin of higher mental function. In H.E. Gong and X.L. Huang, (Eds.). Vygotsky’s scientific thought of psychology in China. (pp. 6-27). Haerbin, China: Heilongjiang Press.
</li>
<li id="leontiev80">Leontiev, A.H. (1980). Activity, consciousness, personality (Li,Y. trans) (in Chinese) <em>Shanghai: Shanghai Translation Press</em>
</li>
<li id="liu01">Liu, C.M. &amp; Wang, L. (2001) Network-oriented modern information outlook (in Chinese). <em>Library and Information Service</em> (1), 34-37
</li>
<li id="li79">Li ,Y. (1979) A.H.. Leontiev’s activity theory (in Chinese) <em>Acta Psychologica Sinica</em> 2), 233-241
</li>
<li id="lu07">Lu, J.J., Liu, M.F. &amp; Shi, L.F. (2007). An inquiry into evolutionary skeleton and application of activity theory (in Chinese) <em>Modern Education Technology</em> <strong>17</strong>(1), 8-14
</li>
<li id="lu87">Lu,T.H. (1987).The new conception and new methods in the changing information work (in Chinese) <em>Scientific and Technical Information Work</em> (3), 2-5
</li>
<li id="ma04a">Ma,Y.K. (2004a). The metastudy of the ideas of cognitive development of Piaget and Vygotsky (in Chinese) <em>Studies In Dialectics of Nature</em>, <strong>20</strong>(9), 41-44
</li>
<li id="ma04b">Ma, Y.K. (2004b). The application of Vygotsky’s social constructivism in contemporary instruction (in Chinese) <em>Studies in Foreign Education</em>, <strong>31</strong>(12), 6-9
</li>
<li id="ma06">Ma, Y.K. (2006) The inheritance and development of social constructionism psychology to Vygotsky’s ideas (in Chinese). <em>Advances in Psychological Science</em>, <strong>14</strong>(1), 154-160
</li>
<li id="meyers07">Meyers, E.M. (2007). From activity to learning: Using cultural historical activity theory to model school library programmes and practices. <em>Information Research</em>, <strong>12</strong>(3), paper 313
</li>
<li id="ren04">Ren, Y.Q. (2004). 21 century-oriented Vygotsky: constructivism, IT and Vygotsky’s thought (in Chinese) In: Gong, H. and Huang, X.L. (eds) <em>Vygotsky’s thoughts on scientific psychology in China</em>. Haerbin: Heilongjiang People Press: 302-313
</li>
<li id="shi03">Shi,W.S. &amp; Chen,J.L. (2003). Inspiration of culture-history school’s activity theory on mental health education researches (in Chinese). <em>Journal of East China Normal University (Educational Sciences)</em>, <strong>21</strong>(3), 83-96
</li>
<li id="shi04">Shi,W.S. and Chen,J.L. (2004). The definition of mental health by the social-cultural-historical school’s action theory (in Chinese), <em>Psychological Science</em>, <strong>27</strong>(5), 1168-1171
</li>
<li id="shi07">Shi ,X.Y. (2007). Comparative study of developmental concepts between humanistic psychological school and social and cultural historical school (in Chinese), <em>Shanghai Academy of Educational Sciences</em> (1), 35-38
</li>
<li id="sundin05">Sundin, O. &amp; Johannisson, J. (2005). Pragmatism, neo-pragmatism and sociocultural theory. <em>Journal of Documentation</em>, <strong>61</strong>(1), 23-43
</li>
<li id="spasser99">Spasser, M.A. (1999) Informing information science: the case for activity theory. <em>Journal of the American Society for Information Society</em>. <strong>50</strong>(12), 1136-1138
</li>
<li id="spasser02">Spasser, M.A. (2002). Realist activity theory for digital library evaluation: Conceptual framework and case study. <em>Computer Supported Cooperative Work</em>, <strong>11</strong>(1/2), 81-110
</li>
<li id="toulmin78">Toulmin, Stephen. (1978). The Mozart of Psychology. <em>New York Review of Books</em> <strong>28</strong>: 51–57
</li>
<li id="vygotsky04b">Vygotsky, L. (2004b). Problems of school-age children’s education and intelligence development . In: Vygotsky, L. <em>Anthology on Lev Vygotsky’s education theory</em> (YU,Z.Q.trans.) (in Chinese) Beijing: People’s Education Press
</li>
<li id="vygotsky04a">Vygotsky, L.S. (2004a). Thought and Language. In: Vygotsky, L. <em>Anthology on Lev Vygotsky’s education theory</em> (YU,Z.Q.trans.) (in Chinese) Beijing: People’s Education Press
</li>
<li id="wang02">Wang, G.R. (2002). The statement and evaluation about the Vygotsky’s psychological theories (in Chinese) <em>Exploration of Psychology</em>, <strong>22</strong> (4), 7-11
</li>
<li id="wang09">Wang, G.R. (2009). Culture interpretation—Vygotsky school of psychology. <em>Jinan: Shandong Education Press</em>
</li>
<li id="wang04">Wang, L. (2004) Managers’ information seeking behavior: a study of environmental scanning. (in Chinese) <em>Library and Information Service</em> <strong>48</strong>(4), 35-44
</li>
<li id="widen07">Widén-Wulff, G., &amp; Davenport, E. (2007). Activity systems, information sharing and the development of organizational knowledge in two Finnish firms: An exploratory study using Activity Theory. <em>Information Research,</em> <strong>12</strong>(3), paper 310
</li>
<li id="widjaja05">Widjaja, I. &amp; Balbo, S. (2005). Structuration of activity: A view on human activity. In:OZCHI '05: <em>Proceedings of the 17th Australia conference on Computer-Human Interaction: Citizens Online: Considerations for Today and the Future</em>:1-4
</li>
<li id="wikipedia13">Wikipedia (2013). Activity theory. Retrieved May15,2013.,from http://en.wikipedia.org/wiki/Activity_theory
</li>
<li id="wilson99">Wilson, T.D. (1999). Models in information behaviour research. <em>Journal of Documentation</em> <strong>55</strong>(3), 249-270
</li>
<li id="wilson06">Wilson T.D. (2006). Review of: Engeström. Y., Miettinen, R. and Punamäki, R-L (Eds.) Perspectives in activity theory Cambridge: Cambridge University Press, 1999. <em>Information Research</em>, <strong>11</strong>(3), review no. R214
</li>
<li id="wilson08">Wilson, T.D. (2008). Activity theory and information seeking behavior. <em>Annual Review of Information Science and Technology</em> <strong>42</strong>, 119-161
</li>
<li id="yang00">Yang, L.J. (2000). Learning view about activity theory and constructivism (in Chinese) <em>Educational Science Research</em> (4), 59-65
</li>
<li id="zhang87">Zhang, F.Q. (1987). Primary research on Soviet “activity theory” (in Chinese) <em>Philosophical Trends</em> (2), 7-11
</li>
<li id="zhang99">Zhang, J.W. (1999). Initial exploration on Lev Vygotsky’s developmental psychology research method system (in Chinese) <em>Journal of North-East Normal University</em> (5), 92-96
</li>
<li id="zhang85">Zhang, S.Y. (1985) On the historical formation and basic ideas of A. N. Leontiev's theory of activity (in Chinese) <em>Acta Psychologica Sinica</em> (1), 23-30
</li>
<li id="zhao97">Zhao,H.J. (1997). Generation, development and future of activity theory (in Chinese) <em>Journal North-East Normal University (Philosophy and Social Science)</em> (1), 87-93
</li>
<li id="zheng04">Zheng,F.X. &amp; Ye,H.S. (2004). Culture and mind: the current worth of studying Vygotsky’s cultural-historical theory (in Chinese). <em>Exploration of Psychology</em> <strong>24</strong>(1), 7-11
</li>
</ul>

</section>

</article>