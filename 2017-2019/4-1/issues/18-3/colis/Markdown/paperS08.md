<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

## Short papers

# Hermeneutics: a sketch of a metatheoretical framework for library and information science research

#### [Iulian Vamanu](#author)  
School of Communication and Information, Rutgers University,  
4 Huntington St., New Brunswick, NJ, USA 08901

#### Abstract

> **Introduction**. The present paper contributes to (meta)theoretical and methodological discussions in library and information science by exploring a conception of understanding developed within the tradition of hermeneutic philosophy and its virtues as a metatheoretical framework for information science.  
> **Approach**. The study clarifies key concepts of the hermeneutic philosophy of Hans-Georg Gadamer and sketches some of their methodological implications for research in information science.  
> **Results**. The study suggests that a hermeneutic framework emphasizes the interpretative nature of the domain of objects of information science and of the researcher’s access to it; the effects of the researcher’s prejudgments on her choice of research topics and on her approach to them; the importance of iteration in the process of data collection and of integration of ‘anomalous’ details into a coherent, meaningful whole at the level of data analysis; and the importance of repeating the study of the same phenomenon to acquire an ever richer understanding of it.  
> **Conclusion**. This study opens up new avenues for further exploration of the consequences of hermeneutic metatheory for information science research.

<section>

## Introduction

Hermeneutics has not enjoyed much attention in library and information science research, despite the fact that it has been employed successfully in literary theory, social science theory, historiography, theology, and law studies. Of the existing information science studies using it, only a few are concerned with foundational aspects (e.g., [Budd 2001](#budd01); [Capurro 2000](#capurro00); [Cornelius 1996](#cornelius96); [Hoel 1992](#hoel92)), while the rest consist of applications of hermeneutics to research situations. Benediktsson ([1989](#benediktsson89)), the most comprehensive study of hermeneutics in information science to date, distinguishes between hermeneutical theory, hermeneutical philosophy, critical hermeneutics, and phenomenological hermeneutics, to suggest that hermeneutical theory can be used for building an alternative research method for information science and, thus, for restoring a much needed ‘balance in our methodological approach to problem solving’ in information science ([Benediktsson 1989: 201](#benediktsson89)). Thus, while hermeneutical theory receives extensive attention, hermeneutical philosophy remains underemployed. To remedy this situation, the present paper proposes a more in-depth exploration of ‘hermeneutical philosophy’ (or hermeneutical interpretativism) as a metatheoretical framework for information science research. This exploration articulates the notion of understanding and its various aspects and offers suggestions in regard to how typical research procedures should unfold.

## Knowledge as understanding

A conception of knowledge (and its origins) is one of the key pillars of any metatheory ([Bates 2005: 2](#bates05)). Talja, Tuominen, and Savolainen ([2005](#talja05)) distinguish among three metatheories for information science by looking mainly at differences in how the origin of knowledge is understood within each of them. Thus, cognitive constructivism portrays knowledge as mediated through the ‘[i]ndividual creation of knowledge structures and mental models’ ([2005: 82](#talja05)); social constructivism (or collectivism) views knowledge as constructed in the interaction of the individual (and her knowledge structures) and her socio-cultural environment ([2005: 85](#talja05)); finally, constructionism regards knowledge as produced within specific boundary-setting discourses ([2005: 89-90](#talja05)).

Even though Gadamer’s hermeneutic philosophy focuses on understanding and interpretation (and seeks to understand only how they are possible), it does nevertheless refer to knowledge as well and distinguishes among three types of knowledge: scientific knowledge (the result of natural scientific research); practical knowledge, ‘the empirical knowledge of so-called practice that everyone accumulates in the midst of life;’ and cultural knowledge, which forms ‘human culture – poetry, the arts as a whole, philosophy, historiography and the other historical sciences’ ([Gadamer 1977: 529](#gadamer77)).

Whereas scientific knowledge presupposes verifiability and explanation, practical knowledge and especially cultural knowledge are ‘largely unverifiable and unstable’ ([1977: 529](#gadamer77)) and presuppose understanding (Verstehen). A hermeneutic metatheory will emphasize the importance of precisely this notion.

### Understanding and prejudgments

Even though, in Gadamer’s view, understanding contains a _cognitive_ dimension, it is not primarily an epistemological notion, namely one that emphasizes the intellectual grasp of phenomena based on following a rule-based procedure. Rather, understanding involves a stronger _practical_ dimension.

First, understanding includes a _competence-related_ or know-how dimension, which is captured by the English phrase ‘to know one’s way around,’ but is even more obvious in the German verb _sich verstehen_, in the French verb _s’y comprendre_, and in the older English usage of the verb _to understand_: ‘To be thoroughly acquainted or familiar with (an art, profession, etc.),’ ‘to be able to practise or deal with properly,’ and ‘to know one's place, or how to conduct oneself properly’ ([OED 2013](#oed13)).

Second, understanding is a _practical_ notion in yet another sense of the German verb _sich verstehen_, namely ‘to come to an agreement’ – a sense conveyed by the English phrase ‘we understand each other’ ([Grondin 2002: 39](#grondin02)). To make sense of this specific agreement-oriented dimension of understanding, it is important to first note that hermeneutic philosophy regards humans as already immersed in a world (culture, tradition) whose phenomena they experience as familiar, i.e., already endowed with a range of possible uses. These phenomena are of concern to them. A hermeneutic framework describes this fundamental belongingness of humans to their world by appealing to the notion of ‘fore-structure of understanding’ ([Heidegger 1996: 141](#heidegger96)) or of ‘prejudgments’ ([Gadamer 2004: 27](#gadamer04)). Gadamer introduces the notion of _Vorurteile_ (prejudgments) to describe the ‘often inexplicit set of practical and theoretical precommitments (Voreingenommenheiten), shaped in large part by cultural traditions, that determine how we experience what we experience’ ([Weberman 2000: 47](#weberman00)). According to Gadamer ([2004: 9](#gadamer04)), these prejudgments ‘constitute the initial directedness of our whole ability to experience. (…) They are simply conditions whereby we experience something — whereby what we encounter says something to us.’ Gadamer suggests that, since we belong fundamentally to our world, our experience is always already oriented. Thus, when we attempt to understand a text, we cannot possibly approach it with/from a neutral stance, i.e., one that does not reflect at least a minimal concern with the matter at hand. In this respect, understanding does not amount to a disinterested act of reconstruction of the meaning intended by its author, but rather to a process of coming to an agreement on the subject matter, i.e., on its _aboutness_ ([Grondin 2002: 40](#grodin02)). In this process, some of the prejudgments may be erroneous or unjustified (i.e., ‘prejudices’ or ‘biases’). This situation occurs, for instance, when we attribute to an author a certain perspective and attempt to read it, more or less programmatically, into any of her writings. Yet other prejudgments are ‘productive’ or legitimate and constitute inevitable initial orientations for understanding. Precisely because prejudgments are inevitable, the task of the researcher is not to set them aside, but rather ‘to recognize and work them out interpretatively’ ([Grondin 1991: 111](#grondin91)).

Since Gadamer emphasizes the fundamental ‘linguisticality’ of human experience, there is a second sense in which understanding takes the form of an agreement: it always involves the use of _language_ in the process of articulation of meanings. The ‘words that are always mine, but at the same time those of what I strive to understand’ ([Grondin 2002: 41](#grondin02)).

The three dimensions (cognitive, practical, and linguistic) of understanding are brought together in Gadamer’s notion of understanding as _application_> and _translation_. Grondin ([2002: 43](#grondin02)) summarizes this aspect by saying that what we seek to understand (i.e., apply, translate) appears to us as foreign, yet binding (‘I cannot say whatever I want’). Nevertheless, our understanding will depend on ‘terms that [we] can follow and hope to communicate.’ As researchers, we are constrained by the object of our research, yet the understanding of that object only succeeds if supported by the resources (cultural, intellectual, linguistic, etc.) we are able to mobilize and by our ability to translate its initial and fundamental _foreignness_. This act of making understanding explicit is what Gadamer ([2004: 306](#gadamer04)) calls ‘_interpretation_’.

### The circle of understanding

The _circle of understanding_ describes the form that the unending process of understanding takes. It occurs as an iterated reciprocal movement between (the meaning of) a part (e.g., sentence, text, etc.) and (the meaning of) a whole to which that part belongs (e.g., text, culture, tradition, etc.), under the assumption that a part only makes sense within a whole, yet the whole does not make sense except in terms of a coherent configuration of its parts ([Gadamer 2004](#gadamer04): 293). Let us consider an example. When approaching a text (e.g., fictional, scholarly, etc.), we start from specific precommitments which derive from our belongingness to a world (culture, tradition, discipline, etc.) and which made the text interesting for us in the first place. We approach the text by tentatively ‘investing’ it with projections of meaning on the basis of a set of (readily available) clues disseminated in the text and/or its paratext (the title, subtitles, blurb, a few concepts listed in the index, etc.). That is, we project an expectation of meaning unto the text as a whole. As we continue reading the text, this expectation may not be fulfilled completely (for instance, we may stumble upon passages that do not ‘make sense,’ i.e., do not seem to confirm our initial projection). In this case, we move from these parts back to the text as a whole and project another, modified anticipation of meaning. In this process, our ever renewed projections are progressively tested, confirmed and disconfirmed, adjusted and readjusted; our hope is to eventually see the clearest shape of the subject matter of that text.

A consequence of this hermeneutic circularity is the notion of the ‘anticipation of perfection’ of what is to be understood. This anticipation is readily fulfilled in unproblematic situations of reading. Yet, this anticipation may sometimes be frustrated: first, when we do not understand a text; second, when we do understand it, but cannot agree with its truth claim. In such situations of a break in understanding we may become aware of our prejudices (i.e., erroneous prejudgments). For understanding to unfold successfully, a temporary suspension or bracketing of these prejudices is required ([Connolly and Keutner 1988: 32-3](#connolly88)).

### Horizons of understanding and their fusion

We can be open and have expectations with regard to the subject matter of a text because we belong to a culture or tradition (familial, educational, cultural, disciplinary, professional, organizational, etc.) which equips us with prejudgments about anything that can become a matter of possible (research) interest. We can in principle understand only those phenomena with which we share a space of meaningfulness ([Connolly and Keutner 1988: 28](#connolly88); [Weberman 2000: 49](#weberman00)). For instance, growing up in a culture of honor equips us with a space of shared meaningfulness with Bushido, the samurai way of life. This sort of commonality positions us differently in regard to Bushido compared to a more utilitarian culture.

The configuration of prejudgments related to a domain of objects constitutes a ‘horizon of understanding,’ i.e., a boundary for all possible phenomena we can experience as familiar or as potentially cognizable; a horizon always constitutes for understanding a limit and a condition of possibility that is constantly shifting ([Gadamer 2004: 301ff](#gadamer04)).

If the hermeneutical circle describes the structure of understanding, the ‘fusion of horizons’ (Horizontverschmelzung) represents its mode ([Gadamer 2004: 305](#gadamer04)). This fusion stands for an event in which ‘the tension’ between the separate horizons of the researcher and of the object is ‘dissolved,’ as they ‘merge with each other’ ([Gadamer 1989: 41](#gadamer89)). Understanding emerges as an event out of this sort of fusion; the interpreter’s horizon has expanded and has been enriched as a result of this ‘merger.’ She has acquired a wider and more sophisticated view of the phenomenon.

### The situation of interpretation

Since any act of approaching a text involves the bringing into play of the prejudgments with which we happen to be equipped at that particular historical moment, the understanding of the subject matter of a text is inevitably related to our present situation: ‘the text poses a question to [the interpreter] in his particular historical situation and he approaches the text with given expectations’ (Hoy 1978: 67). In other words, interpretation takes the form of the application of the meaning anticipated in the text to one’s present situation ([Gadamer 2004: 323-4](#gadamer04)). However, this kind of application does not amount to a negation of the text (and its truth claim) as such in favor of concerns we may happen to entertain in the present; the text remains the litmus test for the meaning advanced by the interpreter in his or her present situation of interpretation. The text is familiar to us, yet ‘it is an address from a perspective other than our own,’ which makes us interested in it and willing to understand it in the first place ([Hoy 1978: 67](#hoy78)).

### History of effects and effects of history

If the hermeneutical circle describes the structure of understanding, the medium within which it occurs is what Gadamer ([2004: 299](#gadamer04)) calls ‘the history of effects’ (Wirkungsgeschichte), a notion which refers to the fact that the prejudgments the researcher brings to bear on a text are ‘effects of history.’ More precisely, these prejudgments reach us through the cultural tradition in which we are embedded and whose validity and authority (not authorianism) we usually taken for granted. Thus, when we deal with research objects (especially pertaining to the past), prejudgments about these objects derive from a ‘chain of past interpretations’ which have accumulate to become an integral part of them. They mediate our access and relation to these objects. In this respect, new questions, new concerns, and new contexts of research enable different understandings of the same object and transform understanding into an unending endeavor ([Quéré 1999: 45-8](#quere99)).

## Some consequences for information science research

This section sketches a few consequences of the hermeneutic framework discussed above for research in general and information science research in particular.

A hermeneutical metatheory encourages information science research aiming at understanding documents and documentation practices involving the three types of knowledge (scientific, practical, and cultural), e.g., the practical knowledge developed by information professionals and information users of any type (including scientists); the transmission of knowledge in institutions; the production and organization of scientific and cultural knowledge in various times and places, and so on.

An emphasis on understanding and prejudgments suggests that both the domain of objects in information science and the researcher’s approach to this domain are interpretative. In other words, research unfolds as an interpretative approach to self-interpreting agents and/or interpretable phenomena (practices and documents). The interpreter interacts with (rather than simply acting on) an object that is unstable and ultimately irreducible and uncontrollable.

The information science researcher should be aware of the fact that her interest in, and approach to, an object (e.g., information users, documents, documentation practices, and so on) is enabled by prejudgments about that object. Prejudgments usually originate from her disciplinary and cultural backgrounds. In other words, it is the socialization in the practices of a disciplinary framework and the broader ethno-cultural group that guide the choice of topics and research procedures.

The circle of understanding and the anticipation of perfection are meant to portray the way in which understanding unfolds. Yet they contain normative implications for research. Hermeneutic circularity may suggest, at the level of data collection, that we proceed by iterated sequences of data collection, striving to identify and keep for analysis all the objects or parts thereof (e.g., documents, passages in a text, patterns of action, practices, etc.) even though they may appear ‘anomalous.’ Further, at the level of data analysis, hermeneutic circularity implies that we do not really understand a phenomenon unless we manage to integrate, at least tentatively, its ‘anomalous’ details into a coherent, meaningful whole. The presence of peculiarities in the data is a good indicator that we have not understood the phenomenon completely.

A consequence of the notion of horizons of understanding for research is that the researcher should be able to articulate the configuration of prejudgments she brings to bear on the phenomenon, in awareness of the fact that complete self-transparency in this respect is utopian. If there is a methodical dimension to understanding, it can only refer to the researcher’s ability to make as explicit as possible and to possibly keep in check the erroneous prejudgments that block the researcher’s access to the subject matter. Such prejudices may originate in the researcher’s ethno-cultural background (especially when studying the documents and documentation practices of people with a different ethno-cultural background) or disciplinary background (a training in quantitative methods may prove insufficient in a research context which requires ethnographic sensitivity, and vice-versa).

A consequence of Gadamer’s views of the ‘situation of interpretation’ and of the ‘effects of history’ is that the unending character of understanding suggests that a repeated study of the same phenomenon is needed, as it may yield new insights.

## Conclusion

The present paper has discussed in depth a conception of _understanding_ proposed in the tradition of hermeneutic philosophy, in order to explore its virtues as a metatheoretical framework for information science. After discussing various aspects of the notion of understanding , the paper sketched some of the consequences of a hermeneutic metatheory for social science and Humanities research in general and for information science research in particular.

## <a id="author"></a>About the author

Iulian Vamanu is a PhD candidate in the School of Communication and Information at Rutgers University. His research interests include the cultural aspects of knowledge production and circulation, the discursive research of indigenous knowledge and curatorship, and the use of qualitative research methods in information science. He can be contacted at [ivamanu@rci.rutgers.edu](mailto:ivamanu@rci.rutgers.edu)

</section>

<section>

## References

<ul>
<li id="bates05">Bates, M.J. (2005). An introduction to metatheories, theories, and models. In K.E. Fisher, S. Erdelez, and E.F. McKechnie (Eds.), <em>Theories of information behavior</em> (pp. 1-24). Medford, NJ: Information Today
</li>
<li id="benediktsson89">Benediktsson, D. (1989). Hermeneutics: dimensions toward information science thinking. <em>Library and Information Science Research</em>, <strong>11</strong>, 201-34
</li>
<li id="bernstein83">Bernstein, R. (1983). Beyond objectivism and relativism. <em>Philadelphia, PA: University of Pennsylvania Press</em>
</li>
<li id="budd01">Budd, J. (2001). Knowledge and knowing in Library and Information Science: a philosophical framework. <em>Lanham, MD: Scarecrow Press.</em>
</li>
<li id="capurro92">Capurro, R. (1992). What is information science for? A philosophical reflection. In P. Vakkari, and B. Cronin (Eds.), <em>Metaphysics, epistemology, and technology. Research in philosophy and technology,</em> (pp. 82-96). London, UK: Taylor Graham
</li>
<li id="capurro00">Capurro, R. (2000). Hermeneutics and the phenomenon of information. In C. Mitcham (Ed.), <em>Conceptions of Library and Information Science</em> 19 (pp. 79-85). New York, NY: Elsevier
</li>
<li id="connolly88">Connolly, J. M. &amp; Keutner, Th. (1988). Hermeneutics versus science? Three German views. <em>Notre Dame, IN: Notre Dame University Press</em>
</li>
<li id="cornelius96">Cornelius, I. (1996). Meaning and method in information studies. <em>Norwood, NJ: Ablex</em>
</li>
<li id="gadamer77">Gadamer, H.-G. (1977). Theory, technology, practice: The task of the science of man. <em>Social Research</em>, <strong>44</strong>(3), 529-61
</li>
<li id="gadamer89">Gadamer, H.-G. (1989). Text and interpretation. In D. P. Michelfelder &amp; R. E. Palmer (Eds.), <em>Dialogue and deconstruction: the Gadamer-Derrida encounter</em> (pp. 21-51). Albany, NY: State University of New York Press
</li>
<li id="gadamer04">Gadamer, H.-G. (2004). Truth and method. (2nd rev. ed.) <em>London, UK: Continuum International Publishing Group</em>
</li>
<li id="grondin91">Grondin, J. (1991). Introduction to philosophical hermeneutics. <em>New Haven, CT: Yale University Press</em>
</li>
<li id="grondin02">Grondin, J. (2002). Gadamer’s basic understanding of understanding. In R. J. Dostal (Ed.), <em>The Cambridge companion to Gadamer</em> (pp. 36-51). Cambridge, UK: Cambridge University Press.
</li>
<li id="heelan88">Heelan, P. (1988). Space-perception and the philosophy of science. <em>Berkeley &amp; Los Angeles, CA: University of California Press</em>
</li>
<li id="heidegger96">Heidegger, M. (1996). Being and Time: A Translation of Sein und Zeit. Translated by Joan Stambaugh. <em>Albany, NY: State University of New York Press</em>
</li>
<li id="hoel92">Hoel, I. (1992). Information Science and hermeneutics - Should information science be interpreted as a historical and humanistic science? In P. Vakkari and B. Cronin (Eds.), <em>Conceptions of library and information science: historical, empirical and theoretical perspectives</em> (pp. 69-81). London, UK: Taylor Graham.
</li>
<li id="hoy78">Hoy, D. (1978). The critical circle: Literature, history, and philosophical hermeneutics. <em>Los Angeles, CA: University of California Press</em>
</li>
<li id="oed13">Oxford English Dictionary (2013). "Understand, v." <em>Oxford, UK: Oxford University Press</em>
</li>
<li id="ricoeur71">Ricoeur, P. (1971). The model of the text: Meaningful action considered as text. <em>Social Research</em>, <strong>L38</strong>(3), 529-62
</li>
<li id="quere99">Quéré, L. (1999). La sociologie à l'épreuve de l'herméneutique. <em>Paris, France &amp; Montréal, QC: L'Harmattan</em>
</li>
<li id="talja05">Talja, S., Tuominen, K. &amp; Savolainen, R. (2005) “Isms” in information science: constructivism, collectivism and constructionism. <em>Journal of Documentation</em>, <strong>61</strong> (1), 79-101
</li>
<li id="weberman00">Weberman, D. (2000). A new defense of Gadamer’s hermeneutics. <em>Philosophy and Phenomenological Research</em>, <strong>LX</strong>(1), 45-65
</li>
</ul>

</section>

</article>