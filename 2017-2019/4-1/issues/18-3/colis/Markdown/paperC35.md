<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Analytical implications of using practice theory in workplace information literacy research

#### [Camilla Moring](mailto:cm@iva.dk)  
Royal School of Library and Information Science, University of Copenhagen,  
Birketinget 6, 2300 Copenhagen, Denmark  
#### [Annemaree Lloyd](mailto:anlloyd@csu.edu.au)  
School of Information Studies, Charles Sturt University,  
Canberra, Australia

#### Abstract

> **Introduction**. This paper considers practice theory and the analytical implications of using this theoretical approach in information literacy research. More precisely the aim of the paper is to discuss the translation of practice theoretical assumptions into strategies that frame the analytical focus and interest when researching workplace information literacy. Two practice theoretical perspectives are selected, one by Theodore Schatzki and one by Etienne Wenger, and their general commonalities and differences are analysed and discussed.  
> **Analysis**. The two practice theories and their main ideas of what constitute practices, how practices frame social life and the central concepts used to explain this, are presented. Then the application of the theories within workplace information literacy research is briefly explored.  
> **Results and Conclusion.** The two theoretical perspectives share some commonalities as they both emphasise practice as a constellation of activities and share an interest in the constitution and conditioning of such activities in social life. However, researchers must be aware that there also exist differences between the two theories that impact on the analytical focus by emphasising some aspects of practice in favour of others.

<section>

## Introduction

In this paper the authors consider practice theory and the implication of using this approach in workplace information literacy research. The use of practice theory as a theoretical perspective for understanding information literacy is still not a very common approach ([Cox 2012a](#cox12a)) and can be seen as still emerging in the information studies field. However, practice approaches are reflected in the work of several information literacy researchers such as Eckerdal ([2011](#eckerdal11)), Limberg, Sundin and Talja ([2012](#limberg12)), Lipponen ([2010](#lipponen10)), Lloyd ([2009](#lloyd09), [2010](#lloyd10)), and Tuominen, Savolainen and Talja ([2005](#tuominen05)), where the focus is on information literacy as something that develops in social contexts and is specific to a particular community ([Talja and Lloyd 2010: xii](#talja10)).

Practice theory is not a coherent theory but consists of several theoretical perspectives. This understanding emphasises the need of discussing the analytical implications when choosing a particular practice perspective, as theories may differ in their epistemological and ontological orientation. Theories of practice are neither prescriptive, nor do they very seldom inform us about how empirical work should be carried out. Instead we are given a theoretical perspective or an interpretative lens ([Huizing and Cavanagh 2011](#huizing11)) from which we can observe the world as social. Therefore, using a certain practice theoretical perspective implies some specific methodological and analytical consequences for studying information literacy, because they ‘[…] offer [us] contingent systems of interpretation which enable us to make certain empirical statements (and exclude other forms of empirical statements)’ ([Reckwitz 2002: 257](#reckwitz02)). When choosing a certain theoretical perspective this choice implies that we turn our attention in a certain direction, and in doing so, acknowledge that we are emphasising some analytical aspects in favour of others. Researchers need to reflect on this by discussing the translation of practice theory in to analytical strategies (see also [Halkier and Jensen 2011](#halkier11)).

The aim of this paper is to more generally discuss the analytical implications of using practice theory to frame information literacy research in workplace settings. The present analysis will focus on comparing the general commonalities and differences between two selected practice theoretical perspectives; the first by Theodore Schatzki ([2001](#schatzki01), [2002](#schatzki02), [2010](#schatzki10)) and the other by Etienne Wenger ([1998](#wenger98), [2010](#wenger10)). These two theoretical perspectives have featured in practice oriented research in library and information science within the last decade in research areas such as information practice (e.g [Anderson 2007](#anderson07); [Cox 2012a](#cox12a), [2012b](#cox12b); [Lloyd 2010](#lloyd10); [Moring 2011](#moring11); [Savolainen 2008](#savolainen08); [Veinot 2007](#veinot07)), information sharing (e.g. [Hara 2009](#hara09); [Pilerot and Limberg 2011](#pilerot11); [Talja and Hansen 2006](#talja06)) and information literacy (e.g. [Lloyd 2009](#lloyd09), [2010](#lloyd10)).

In the following sections we will briefly introduce practice theory summarising the application of this theoretical approach within the information studies field and more specifically within information literacy research. Then we will introduce the two selected practice theoretical perspectives, discuss the general commonalities and differences between them, and consider more generally how they impact the analytical focus when researching information literacy.

## Introducing practice theory

Practice theory has its roots in philosophical work ([Wittgenstein 1958](#wittgenstein58); [Schatzki 2002](#schatzki02)) and in the work of social and cultural theorists ([Bourdieu 1976](#bourdieu76); [Giddens 1979](#); [Gherardi 2009](#gherardi09); [Lave and Wenger 1991](#lave91); [Rose 2010](#rose10)) in areas pertaining to education, organizational studies, working life and serious leisure. Practice theory is therefore not ‘a theory’ but consists of several theoretical perspectives, which differ in their theoretical framing. However, there are some features common to all practice theories that loosely bind these approaches together. Reckwitz ([2002: 244](#reckwitz02)) points out that ‘they do form a family of theories’, while Schatzki ([2012: 1](#schatzki12)) notes that several general concepts are shared by the divergent group who call themselves practice theorists. This includes the ideas that practice represents an organized constellation of different people’s activities, and that practice is understood intersubjectively as anchored in the activities of multiple people, and not as the action of an individual ([Schatzki, 2012](#schatzki12)).

In the information studies field the practice approach has been characterised by Talja ([2006: 123](#lloyd06)) as ‘a more sociologically and contextually oriented line of research’ where the focus is on the social and dialogic construction which underpins information seeking and use, as these activities are operationalized within a given setting, and according to the social conditions that inherently shape the setting. As Cox ([2012a](#cox12a)) points out, practice approaches which draw from sociological and anthropological traditions challenge traditional individualist/cognitive and psychologically influenced approaches by focusing on the activities of multiple people in co-participation within a practice ([Lloyd 2011](#lloyd11)), and on the construction and agreement of knowledge as an interactional relationship between people in practice ([Lloyd 2012](#lloyd12)) and across practices ([Moring 2011](#moring11)).

The concept of information practice has as mentioned previously been explored and described by a number of information science researchers. These researchers have drawn from a range of practice approaches to situate information practice as a social practice that is produced and reproduced within specific settings. However, the information practice research field has been characterized by an on-going discussion of what constitute an information practice which has led Savolainen ([2007](#savolainen07)) to describe information practice as an ‘umbrella-concept’ drawing upon different discourses. For instance Talja and Hansen identify information practices as ‘practices of information seeking, retrieval, filtering and synthesis’ ([2006: 113](#talja06)). Savolainen views everyday information practice as consisting of three main components ‘information seeking, information use and information sharing’ ([Savolainen 2008: 49-50](#savolainen08)), while Lloyd ([2010](#lloyd10)) views these elements as activities of practice rather than practices in themselves. Cox ([2012b: 61ff](#cox12b)) argues that the concept of information practice ‘seems to privilege those few practices that are primarily informational’ and instead he suggests using the phrase ‘information in social practice’ arguing that most social practices includes information activities that can be seen as involving information seeking, sharing, creation or management. While differences exist between approaches to information practice, there are also common elements such as a focus on social practices and people as they engage in activities and negotiations about information and ways of knowing in the on-going performance of their life.

Applying a practice perspective to information literacy allows for a broader analytical approach which aims to view the practice holistically focusing on the interaction between people, information and knowledge practices in a particular setting, with an emphasis on how information activities are socially constituted, conditioned and negotiated in practices. Practice theoretical perspective have been especially influential on information literacy studies in workplaces compared to educational settings were a prevailing amount of information literacy research origins. One major reason for this might be that practice theory applied in workplaces and organizational context relates the study of work to the study of organizing, and offer researchers the ability to observe and analyse the ‘fine details of how people use the resources available to them to accomplish intelligent actions, and how they give those actions sense and meaning’ ([Gherardi 2012: 2](#gherardi12)). This includes the use of information resources and also considers how information activities are valued and becomes meaningful within a working practice.

Given that there is no single practice perspective or single methodological approach to studying practices, the discussion of the analytical translation of practice theory can be approached in many ways. In the following section the analytical focus used in this paper will be described.

## Analytical focus

A common starting point for all practice theories is the questions: what constitutes a practice and how do practices frame social life? Schatzki ([2012: 13ff.](#schatzki12)) identifies three general commonalties that reflect this:

1.  The idea that a practice is an organized constellation of different people's activities;
2.  The idea that important features of human life (both social and psychological) must be understood as forms of or as rooted in human activity – not in the activity of individuals, but in practices, and
3.  That the basis of human activity is located in non-propositional bodily abilities.

The first commonalities points at practices as a social phenomenon. The idea that practices have bodily dimensions that emerge through material relations, and reflect knowledge or knowing how to go on is also highlighted. This is an increasingly important feature of practice theory that refutes the mind/body dualism and is inspired by the works of Merleau-Ponty ([1962](#merleau62)), the skills and expertise work by Dreyfus ([1991](#dreyfus91)) and by Bourdieu ([1976](#bourdieu76)) who sees the body in relation to the organizing of activities and as an expression of know-how knowledge ([Schatzki 2012](#schatzki12)). Further three related commonalties are proposed by Feldman and Orlikowski ([2011: 1241](#feldman11)). These are that 1) situated actions are consequential in the production of social life, 2) dualisms are rejected and 3) relations are mutually constitutive. The present analysis will focus on identifying these general commonalities but also the differences there may exist between the two selected theories. This is done by analysing the main ideas of what practices are and how they frame social life by looking at the central concepts used in the two theories to explain this.

Practices are invisible and the organization of practices are abstract phenomena, so when researching practices Schatzki ([2012](#schatzki23)) points out that they cannot be directly perceived. Therefore practices must be uncovered through analysis of e.g. knowledge, language, stories and activities underpinned by the narrative of how practice happens over time. The power of a practice theoretical approach lies in its ability to frame researcher awareness of the composition of a practice relation. Within the commonalities mentioned above there exists three central analytical dimensions that focuses on the relationship between 1) practices and activities, 2) the individual and the social and 3) the body and the mind. These dimensions will also be briefly touched upon in the following analysis.

## Two practice theoretical perspectives and their application within information literacy research in workplace settings

The first selected perspective draws from the ontological work of Schatzki (2002), while the second explores Wenger’s ([1998](#wenger98)) practice-based understanding of learning in communities of practice.

### A Schatzkian view of practice

According to Schatzki social life is inherent within a practice and is composed of ‘a materially mediated array of human activity centrally organized around shared practical understandings’ ([2002: 71](#schatzki02)). Engaging in the practices of a social site enables the sites members to understand how social life happens, because the engagement gives members insights into what forms of knowledge and ways of knowing are legitimized and sanctioned. Working from an ontological position, Schatzki suggests that life can be analysed through the social site as a place where coexistence transpires through a web of ongoing practice and arrangements ([Schatzki 2000: 26](#schatzki00); [Lloyd 2010](#lloyd10)).

The interest in this type of analysis lies in understanding how things happen and how this happening emerges through sayings and doings that entwine the social site. Where sayings represent what is spoken or gestured about, and doings reflect the actual performances associated with the execution of a practice. Schatzki suggests that ‘a practice always exhibits a set of ends that participants should or may pursue […] and a selection of tasks that they should or may perform’ ([2002: 80](#schatzki02)). This approach also emphasises the idea of actions that are bundled together to form an activity. Maintaining practice requires activity and skill development and a shared understanding of the embodied knowledge or know-how ([Schatzki 2002: 3](#schatzki02)). The concept of prefigurement is central to this theory of practice, whereby the social site qualifies the path of an activity, i.e. it is enabled or constrained through discourse. Practices are prefigured through a process of social interaction which over time creates layers of meanings ([Lloyd 2010: 250](#lloyd10)). Central to the idea of practice is the role of the body as a signifying body revealing the nature of the site and as an instrumental body demonstrating practical reasoning ([Lloyd 2010](#lloyd10)).

Applying a practice approach to information literacy research that draws from a Schatzkian perspective requires that researchers employ an analytical lens to practice as the site of the social ([Schatzki 2002](#schatzki02)). The nature and emergence of information literacy as a practice is the result of discursively produced agreements about what constitutes knowledge and ways of knowing ([Lloyd 2012](#lloyd12)). Consequently information literacy researchers must focus on understanding how the practice emerges within a site, and the social, corporeal dimensions that shape and reshape peoples’ interaction with others who are co-located and participating in the same practice. Analysis will also focus on the saying and doings that shape and maintain the practice, with the aim of understanding how it emerges in particular settings according to the types of knowledge that are legitimized and the ways of knowing that are sanctioned within the practice.

In the study of ambulance officers Lloyd ([2009](#lloyd09)) found that the transition of novices from training to work requires reconciling knowledge that connects novices to epistemic sources of canonical knowledge, e.g. rules and regulations and policies of work, with knowledge situated in the actual performance of work, e.g. novices moved from practice associated with learning to become a practitioner towards practices associated with being a practitioner. To effect this transition, novices engage with information literacy practice in order to recognize the different forms of knowledge (local, contingent, experiential, corporeal) and information sources that are legitimised and sanctioned as part of the discourses of the workplace. This shift requires novices to begin to think collectively, to recognize that knowledge is a collective possession ([Gherardi 2009](#gherardi09)) that is composed over time of the collective sayings and doings of experiential and embodied learning. Novice workers must develop strategies and ways of knowing to learn how sources are situated within the workplace setting, they must also engage with affordances furnished by the language which will allow them to learn how to become an ambulance officer. They must also identify the purpose of information related activities (e.g. information seeking, information sharing) in addition to the skills that are required to connect with the workplace landscape to understand the skilled performance of work and to engage with the shaping of recognisable workplace identity. A study of nurses ([Bonner and Lloyd 2011](#bonner11)), reported that participants emphasised the critical importance of other nurses in the development of occupational identity, solidarity and understanding about the professional performance of work. This information is referenced against epistemic sources of agreed upon knowledge ([Lloyd 2009: 402](#lloyd09)).

Practices also have corporeal dimensions and the body represents a central source of information in learning the skilled performances of work. The body was also acknowledged as a signifier of the sayings associated with professional practice and performance of work and with the social and material conditions which drive activity and workplace performance ([Lloyd 2010](#lloyd10)). In the previous studies ([Lloyd 2009](#lloyd09); [Bonner and Lloyd 2011](#bonner11)) bodies (re)present the institutional face of the organization and can be read and interpreted by outsiders as authoritative. Working bodies are central to learning and act as a source of embodied and experiential knowledge which can be drawn upon. In these examples, information literacy practice connects novices with information that is both structural (related to the norms of the group) and contextually associated with the sanctioned information literacy practices of the workplace.

### Wengers social theory of learning

Etienne Wenger’s theory ([1998](#wenger98)) is primarily about learning, but also about what constitutes and forms social practices. The theory is commonly known as the theory of communities of practice. However, the concept communities of practice were already introduced by Lave and Wenger in their previous book on situated learning ([Lave and Wenger 1991](#lave91)). Wenger emphasises that ‘the concept of community of practice [do] not exists by itself, [but] is part of a broader conceptual framework for thinking about learning in its social dimensions’ ([2010: 179](#wenger10)). So the concept should be viewed as analytical rather than instrumental and prescriptive. Also Lave states that ‘the conception; ‘communities of practice’ in a social-practice theory perspective is an analytical tool [...]’ ([Lave 2008: 292](#lave08)) and ‘a way to give scope in time/space to an understanding of practice’ […] (ibid: 284). Wenger finds that the analytical power of the concept lies in the integration of the four central components of his theory: community, practice, meaning and identity (Wenger 1998: 5-6). These components are interconnected and mutually defining, but at the same time they serve the purpose of being different entrances to the broader analytical perspective of communities of practice.

According to Wenger practice is always social, and the role of participation and negotiation of meaning in practice is a key point in his theory. A practice develops through long term practising and through meaning negotiations around everyday activities within that practice. Practice connotes doing, but ‘[…] in a historical and social context that gives structure and meaning to what we do’ ([Wenger 1998: 47](#wenger98)). Also he rejects dualism in his understanding of practice, as the process of practice always involves the whole person, both acting and knowing. Because manual activity is not thoughtless and mental activity is not disembodied (ibid: 48) the theory includes material and bodily aspects of practice. However, Wenger emphasises that practice is not just about coordinated bodies and brains in activity, but moreover that which gives meaning to us in activity. Practice is about meaning as an experience of everyday life (ibid: 52) and it is through the negotiations of meaning that we experiences our engagement in the world as meaningful (or sometimes the opposite). We all have our ideas about the world, but it is within communities of practices we share, negotiate and develop those ideas. We do not just make meanings up independently of the world – meaning is not pre-existing (ibid: 54) – and therefore we may understand the negotiation of meaning as a productive process were meanings are continuously produced and re-produced. In that sense the negotiations of meaning is not only about exchanging meanings but also about an on-going interpretation and re-negotiation of the context for meaning production. The negotiation constantly changes the situation to which it gives meaning, and offers new ways of interpreting the world. As a result practices are dynamic and transformative, and not only homogeneous, self-preserving or unchanging as some might argue ([Lave 2008: 290](#lave08)).

Closely related to the concept of meaning negotiation is the concept of participation. We all participate in different communities of practices and engage ourselves in various ways in these. Negotiation of meaning therefore also includes the negotiation of ways of being and belonging to a community of practice. This duality is exemplified in Moring’s study of sales assistants information practice where she notice that ‘information seeking contributes to the local negotiations of competent participation, but at the same time the ‘meaning’ attached to information seeking is created through participation in practice’ ([2011: 17](#moring11)). Negotiations of meaning frame and determines possible ways of participating in practice, and sometimes it opens up the possibility of changing paths and positions.

A community of practice can be seen as a locally negotiated regime of competence where ‘knowing can be defined as what would be recognised as competent participation in practice’ ([Wenger 1998: 137](#wenger98)). Information literacy, and what it means to be information literate, is in Wenger’s practice perspective very closely connected to negotiations on competent participation in practice, and to what it means to be a competent practitioner in a specific community of practice. Developing competence in practice is underpinned by the sharing of information about relevant sites of knowledge. As Lloyd ([2005](#lloyd05)) illustrates in the study of fire fighters, information literacy becomes a cultural practice where experienced practitioners facilitate novice access to information and to sites of agreed knowledge about practice and profession.

## Discussion: commonalities and differences between the two practice theoretical perspectives.

In this section some analytical implications of applying a practice theoretical perspective within information literacy research will be discussed by focusing on the general commonalities and differences between the two theoretical perspectives.

### What are the commonalities?

When looking at the two theories it becomes clear that they do share some commonalities. Both Schatzki and Wenger emphasise that practice is a constellation of activities e.g. doings, sayings or negotiations, and both focuses on the constitution and conditioning of such activities in social life. Dualism is also rejected by both authors who argue that practice creates cognitive and corporeal dimension represented as tacit, explicit or discursive elements as e.g. language, tools, symbols, values, perceptions, roles, regulations or rule of thumbs ([Wenger 1998](#wenger98)). Analytically this means that practice theory opposes an artificial divide between subjects and objects as ‘practices are seen as configurations of a number of theoretically equally important and interconnected dynamics [...]’ ([Halkier and Jensen 2011: 104](#halkier11)). For instance, when using Wenger’s theory, an analysis of meaning negotiations should not only focus on discourses and language (sayings) but also on participation, including the material, bodily as well as emotional aspects of meaning negotiation, as participation enables the social experience of meaning.

When information literacy is understood as a social practice and analysed through a practice lens, the focus of our research attention will be on what constitutes this particular information literacy practice. Applying a practice perspective makes information activities like any other activity in practice subject to participation and on-going negotiation of meaning in specific practices. As a social practice, information literacy is constituted as a suite of activities, knowledge and competencies that span people’s need to engage with epistemic, social and corporeal modalities in order to know the many landscapes that comprise a particular setting. For information literacy researchers the aim is to determine how a practice is shaped, how the social conditions and negotiations

### What are the differences?

Despite the commonalities, each of the two theories have variations in their theoretical approach to what constitutes a practice, and this influences the direction of analytical attention. In Wenger’s social theory of learning, practice is described by the idea of a community of practice, which he finds is the simplest system having all the properties of meaningful learning. A community of practice is a ‘unit’ were practice is the source of coherence. At the same time a community of practice has no fixed boundaries, but is connected to other practices into a landscape of practices and wider economies of meaning ([Wenger 1998: 198ff.](#wenger98)). So, analyses of negotiations of meaning in a community of practice always have to reflect on how meanings are produced and how they are valued both locally and within a broader community of meaning.

When using Wenger’s practice theory our analytical attention is turned towards what characterizes a certain community of practice and what is recognized as competent participation in that community. What it means be to information literate is continuously negotiated and renegotiated within the community and against the wider economy of meanings. Therefore analytically Wenger’s theory emphasises the negotiated nature of practice, which includes negotiations of meaning, knowledge, information, information activities, ways of participation and possible identities and positions. It will also point to the need of understanding the aspects of practice that frames the way that individuals can participate, and the possibilities and constraints there might be in a community of practice that influences their latitude in developing into competent practitioners in that specific practice.

In contrast Schatzki is not concerned with learning systems, but in understanding social life as it ‘happens’. In describing practices as consisting of a bundle of activities and material arrangements, Schatzki finds social life as stitched together in a mesh of intertwined practices that are connected and held together by common understandings and intentional relations about common project ends ([2002: 155](#schatzki02)). He reflects an ontological approach to practice, focusing on how practices emerge and ‘happen’ in relation to other practices. Practices can be understood as prefigured, relational, embodied and spatio-temporal ([Schatzki 2002](#schatzki02)). Employing Schatzki’s theory as an analytical lens requires researchers to focus on how sayings and doings enact people into practice, by locating them in relation to the specific knowledge and ways of knowing that are collectively negotiated, legitimized and sanctioned within a social site. Exploring information literacy from this perspective encourages researchers to attend to practices prefigurement, which situates activities in the context of established and emergent knowledge, ways of knowing, arrangements and teleological dimensions (e.g. emotions). The spatio-temporal aspect must also be considered because practices do not exist in isolation, but form part of a nexus of entwined practices which relate to each other and are linked through ‘understandings, rules and teleoaffective structure’ ([Schatzki 2002: 87](#schatzki02)). These aspects create the information landscape, and enable information literacy to emerge and be enacted in ways (through activities and skills) specific to the site.

## Conclusion

Within information literacy research there has been an increased interest in applying practice theoretical approaches, however this line of research is still emerging. In this paper we have emphasised that practice theory is not a coherent theory, but consist of several theoretical perspectives offering different analytical and interpretative ‘lenses’. Different practice theories imply different methodological and analytical consequences for studying information literacy. Therefore the aim of this paper has been to discuss the translation of practice theoretical assumptions when researching workplace information literacy. Two practice theoretical perspectives, one by Schatzki and one by Wenger were introduced and their general commonalities and differences were discussed. We conclude that the two theories share commonalities as they both emphasise practice as a constellation of activities, and share an interest in the constitution and conditioning of such activities in social life. Moreover they reject dualism and oppose an artificial divide between subjects and objects. For information literacy researchers who adopt a practice theoretical perspective the common analytical focus will be on how a practice emerges, what activities (both sayings and doings), different forms of knowledge and ways of knowing that compose the practice, and how social conditions as well as meaning negotiations influence its constitution.

Researchers must also be aware that there are differences between the two theoretical approaches even though this may not appear to be the case upon first reading. When applying Wenger’s theory our analytical attention is turned towards the negotiated nature of practice, towards analysis of what characterizes a certain community of practice and what is recognized as knowledge and competent participation within that community. From a Schatzkian perspective, the practice is the site of the social ([2002](#schatzki02)) and the interest lies in understanding how the practice emerges and ‘happens’ in relation to other practices. Practices from this perspective can be understood as relational, emergent, embodied, spatio-temporal and prefigured. The focus in this type of analysis lies in understanding how this ‘happening’ emerges through sayings and doings and how sayings and doings enact people into practice.

Viewing information literacy through a practice theoretical lens requires researches to pay attention to the analytical implications of the different practice theoretical perspectives acknowledging how they make certain aspects of a practice explicit while silencing other.

## Acknowledgements

The authors would like to thank the anonymous reviewers for their constructive comments and valuable suggestions to improve this paper.

</section>

<section>

<ul>
<li id="anderson07">Anderson, T. D. (2007). Settings, arenas and boundary objects: socio-material framings of information practices. <em>Information Research</em>, <strong>12</strong>(4), 1-16
</li>
<li id="bonner11">Bonner, A. &amp; Lloyd A. (2011). What information counts in the moment of practice: information practices of renal nurses. <em>Journal of Advanced Nursing</em>, <strong>67</strong>(6), 1213-1221
</li>
<li id="bourdieu76">Bourdieu, P. (1976). Outline of a theory of practice. <em>Cambridge: Cambridge University Press</em>
</li>
<li id="cox12a">Cox, A. M. (2012a). An exploration of the practice approach and its place in information science. <em>Journal of Information Science</em>, <strong>38</strong>(2), 176-188
</li>
<li id="cox12b">Cox, A. M. (2012b). Information in social practice: a practice approach to understanding information activities in personal photography. <em>Journal of Information Science</em>, <strong>39</strong>(1), 61-72
</li>
<li id="dreyfus91">Dreyfus, H. (1991). Being-in-the-world. A commentary on Heideggers being and time, division one. <em>Cambridge MA: MIT Press</em>
</li>
<li id="eckerdal11">Eckerdal, J. R. (2011).Young women choosing contraceptives: stories about information literacy practices related to evaluation and use of information sources. <em>Dansk Biblioteksforskning</em> [Danish Library Research], <strong>7</strong>(2/3), 17-31
</li>
<li id="feldmann11">Feldmann, M. S. &amp; Orlikowski, W. J. (2011). Theorising practice and practising theory. <em>organization Science</em>, <strong>22</strong>(5), 1240-1253
</li>
<li id="gherardi09">Gherardi, S. (2009). Introduction: the critical power of the `practice lens'. <em>Management Learning</em>, <strong>40</strong>(2), 115-129
</li>
<li id="gherardi12">Gherardi, S. (2012). How to conduct a practice-based study: problems and methods. <em>Cheltenham: Edward Elgar Publishing</em>
</li>
<li id="giddens79">Giddens, A. (1979). Central problems in social theory. Action, structure and contradiction in social analysis. <em>London: Macmillan</em>
</li>
<li id="halkier11">Halkier, B. &amp; Jensen, I. (2011). Methodological challenges in using practice theory in consumption research. Examples from a study on handling nutritional contestations of food consumption. <em>Journal of Consumer Culture</em>, <strong>11</strong>(1), 101-123
</li>
<li id="hara09">Hara, N. (2009). Communities of practice: fostering peer-to-peer learning and informal knowledge sharing in the work place. <em>Berlin: Springer</em>.
</li>
<li id="huizing11">Huizing, A. &amp; Cavanagh, M. (2011). Planting contemporary practice theory in the garden of information science. <em>Information Research</em>, <strong>16</strong>(4), 1-19
</li>
<li id="lave08">Lave, J. (2008). Epilogue: situated learning and changing practice. Amin, A. and Roberts, J. (Eds.). <em>Community, economic creativity and organization</em> (283-296). Oxford: Oxford University Press
</li>
<li id="lave91">Lave, J. &amp; Wenger, E. (1991). Situated learning. Legitimate peripheral participation. <em>Cambridge: Cambridge University Press</em>
</li>
<li id="limberg12">Limberg, L.; Sundin, O. &amp; Talja, S. (2012). Three theoretical perspectives on information literacy. <em>Human IT</em>, <strong>11</strong>(2), 93-130
</li>
<li id="lipponen10">Lipponen, L. (2010). Information literacy as situated and distributed activity. Lloyd, A. &amp; Talja. S. (Eds.). <em>Practising information literacy. Bringing theories of learning, practice and literacy together</em> (51-64). Charles Sturt University, Centre for Information Studies
</li>
<li id="lloyd05">Lloyd, A. (2005). No man (or woman) is an island: information literacy, affordances and communities of practice. <em>Australian Library Journal</em>, <strong>54</strong>(3), 230-237
</li>
<li id="lloyd09">Lloyd, A. (2009). Informing practice: information experiences of ambulance officers in training and on-road practice. <em>Journal of Documentation</em>, <strong>65</strong>(3), 396-419
</li>
<li id="lloyd10">Lloyd, A. (2010). Framing information literacy as information practice: site ontology and practice theory. <em>Journal of Documentation</em>, <strong>66</strong>(2), 245-258
</li>
<li id="lloyd11">Lloyd, A. (2011). Between a rock and a hard place: what counts as information literacy in the workplace and how is it conceptualized? <em>Library Trends</em>, <strong>60</strong>(2), 277-296
</li>
<li id="lloyd12">Lloyd, A. (2012). Information literacy as a socially enacted practice: sensitising themes for an emerging perspective of people-in-practice. <em>Journal of Documentation</em>, <strong>68</strong>(6), 772-783
</li>
<li id="merleau62">Merleau-Ponty, M. (1962). Phenomenology of perception. <em>Routledge &amp; Kegan Paul</em>
</li>
<li id="moring11">Moring, C. (2011). Newcomer information practices: negotiations on information seeking in and across communities of practice. <em>Human IT</em>, <strong>11</strong>(2), 1-20
</li>
<li id="pilerot11">Pilerot, O. &amp; Limberg, L. (2011). Information sharing as a means to reach collective understanding: a study of design scholars´ information practices. <em>Journal of Documentation</em>, <strong>67</strong>(2), 312-333
</li>
<li id="reckwitz02">Reckwitz, A. (2002). Toward a social theory of practices: a development in culturalist theorising. <em>European Journal of Social Theory</em>, <strong>5</strong>(2), 243-263
</li>
<li id="rose10">Rose, G. (2010). Doing family photography. Farnham: Ashgate
</li>
<li id="savolainen07">Savolainen, R. (2007). Information behaviour and information practice: reviewing the 'umbrella concepts' of information-seeking studies. <em>The Library Quarterly</em>, <strong>77</strong>(2), 109-132
</li>
<li id="savolainen08">Savolainen, R. (2008). Everyday information practices: a social phenomenological perspective. <em>Lanham, Maryland: The Scarecrow Press</em>
</li>
<li id="schatzki00">Schatzki, T. R. (2000). The social bearing of nature. <em>Inquiry</em>, <strong>43</strong>(1), 21-38
</li>
<li id="schatzki01">Schatzki, T. R. (2001). Introduction: practice theory. Schatzki, T. R., Knorr Cetina, K. &amp; von Savigny, E. (Eds.). <em>The practice turn in contemporary theory</em> (1-14). London: Routledge
</li>
<li id="schatzki02">Schatzki, T. R. (2002). The site of the social: a philosophical account of the constitution of everyday life. <em>Philadelphia: University of Pennsylvania Press</em>
</li>
<li id="schatzki10">Schatzki, T. R. (2010). The timespace of human activity: on performance, society, and history as indeterminate teleological events. <em>Maryland: Lexington Books</em>
</li>
<li id="schatzki12">Schatzki, T. R. (2012). A primer on practices. In Higgs. J. <em>et al.</em> (Eds). Practice-based education: perspectives and strategies (13-26). Rotterdam: Sense Publishers
</li>
<li id="talja06">Talja, S. (2006). The domain analytic approach to scholars information practices. Fisher, K.E., Erdelez, S. &amp; McKechnie, L. (Eds.). <em>Theories of information behaviour</em> (123-127). Medford, NJ: Information Today
</li>
<li>Talja, S. &amp; Hansen, P. (2006). Information sharing. Spink, A. &amp; Cole, C. (eds.). <em>New directions in human information behavior</em> (113-134). Dordrecht, The Netherlands: Springer
</li>
<li id="talja10">Talja, S. &amp; Lloyd, A. (2010). Integrating theories of learning, literacies and information practice. Lloyd, A. and Talja. S. (Eds.). <em>Practising information literacy. Bringing theories of learning, practice and literacy together</em> (viiii-xx). Charles Sturt University, Centre for Information Studies
</li>
<li id="tuominen05">Tuominen, K.; Savolainen, R. &amp; Talja, S. (2005). Information literacy as a sociotechnical practice. <em>Library Quarterly</em>, <strong>75</strong>(3), 329–345
</li>
<li id="veinot">Veinot, T. C. (2007). ‘The eyes of the power company’: workplace information practices of a vault inspector. <em>The Library Quarterly</em>, <strong>77</strong>(2), 157-179
</li>
<li id="wenger98">Wenger, E. (1998). Communities of practice: learning, meaning and identity. <em>Cambridge: Cambridge University Press</em>
</li>
<li id="wenger10">Wenger, E. (2010). Communities of practice and social learning systems: the career of a concept. Blackmore, C. (Ed.). <em>Social learning systems and communities of practices</em> (179-198). London: Springer
</li>
<li id="wittgenstein58">Wittgenstein, L. (1958). Philosophical investigations. Translated by G.E.M. Anscombe. <em>New York: MacMillian</em>
</li>
</ul>

</section>

</article>