<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Swapping settings: researching information literacy in workplace and in educational contexts

#### [Anna Hampson Lundh](mailto:anna.lundh@hb.se) and [Louise Limberg](mailto:louise.limberg@hb.se)  
The Swedish School of Library and Information Science/The Linnaeus Centre for Research on Learning,  
Interaction and Mediated Communication in Contemporary Society, Univeristy of Borås, Sweden

#### [Annemaree Lloyd](mailto:anlloyd@csu.edu.au)  
School of Information Studies, Charles Sturt University, Canberra, Australia

#### Abstract

> **Introduction**. Information literacy research is characterised by a multitude of interests, research approaches and theoretical starting-points. Challenges lie in the relevance of research to professional fields where information literacy is a concern, and the need to build a strong theoretical base for the research area. We aim to lay a foundation for a discussion on how researchers may approach issues of norms and values in information literacy, through a comparison between selected studies in educational and workplace settings respectively.  
> **Method**. Eight recent empirical studies on information literacy were selected for analysis; four of educational settings and four of workplace settings.  
> **Analysis**. A comparison between the eight studies was made with a focus on conceptualisations of information literacy and approaches to issues of norms and values.  
> **Results**. Two approaches to norms and values in relation to information literacy were identified in the eight studies. The studies conducted with students have a clear evaluative approach, while three of the workplace studies are characterised by an explorative approach. One workplace study has an intermediate position. The evaluative and explorative approaches are then swapped, to examine how relevant each approach is in the context of the other setting. In this way, we are able to discuss underlying perspectives that direct research.  
> **Conclusion**. Our analysis suggests that well-chosen theoretical perspectives are necessary if information literacy researchers wish to influence professional practice.

<section>

## Introduction

The area of information literacy research is characterised by a multitude of interests, research approaches, and theoretical starting-points. This can be seen as a sign of a strong development of the field (see [Sundin 2011](#sundin11)), but it also gives rise to certain challenges for designing, as well as for reading and understanding empirical studies focussing on information literacy.

Studies of information literacy might vary in several respects, for example in terms of aims, the object of study and the conceptualisation of the objects. With strong roots in educational librarianship, information literacy research is closely related to professional practices of library instruction and user education. One of the challenges for information literacy research is its relevance for the professional fields where information literacy is a concern, and at the same time to build a strong theoretical base for the research area.

In some instances (e.g. [Pilerot and Lindberg 2011](#pilerot11)), it has been suggested that there is a gap between professionally oriented literature where information literacy is viewed mainly as “a goal for educational activities” and research-based literature where information literacy is seen as “a study object” ([Pilerot and Lindberg 2011](#pilerot11): 341). It has also been suggested that information literacy research that is too strongly theoretically driven runs the risk of becoming irrelevant for professional practices (e.g. [Julien and Williamson 2011](#julien11)). Others, (e.g. [Limberg, Sundin and Talja 2012](#limberg12)) on the other hand have emphasised the importance of a sound theoretical base when studying information literacy, in order to be able to explicate how information literacy as a study object is understood.

This variation in terms of starting-points in the literature means that information literacy studies differ in their approaches to the value-laden issue of the power of defining information literacy. The study of information literacy inevitably means that an approach to norms and values on what information literacy should be is taken, either implicitly or explicitly. This highlights the importance of carefully scrutinising the different perspectives that are employed in the field when designing and reading information literacy research. In this paper, we will explore the question of perspectives on norms and values in information literacy research in relation to different empirical settings.

This exploration is, of course, influenced by our own research focussing on information literacy in both educational and workplace settings (e.g. [Alexandersson and Limberg 2012](#alexandersson12); [Lloyd 2010](#lloyd10); [Lundh 2011](#lundh11)). As researchers working in a sociocultural tradition, we are interested in how the range of understandings about information literacy, as a concept and as a study object, emerges and the conditions which influence this. It is from this sociocultural point of view that we conduct an in-depth literature review, where a small sample of studies are analysed in detail in order to illustrate research approaches in the field of information literacy. Thus, this literature review can be said to be discursively oriented, meaning that we are trying to understand what is taken for granted in the texts that we are analysing (see [Savolainen 2007](#savolainen07)).

The major part of information literacy studies focus on academic and educational settings. This implies certain issues regarding norms and values that are specific for these kinds of settings. One typical feature of educational settings is that learning is seen as a primary goal for most activities ([Säljö 2000](#saljo00)). Furthermore, learning in educational settings is the subject of assessment practices, often formalised in grading. Thus, information literacy in school settings is imbued with norms and values and researchers are often familiar with these norms and values which are inherent in the discourse of the setting ([Lloyd 2005](#Lloyd05)).

In recent years, however, we have seen the emergence of empirical information literacy studies in settings other than formal educational institutions, such as workplace settings. Among other things, these kinds of contexts offer possibilities of reflecting on how information literacy researchers may approach questions of norms and values related to information literacy in new ways. In workplace contexts, learning is usually not the primary goal for, but rather a necessary premise for, as well as a by-product of, workplace activities (see [Johansson 2009](#johansson09)). Assessment practices in workplace settings are usually based on productivity, quality measurements, and development of expertise, but these might not be as easily understood by an information literacy researcher entering these settings ([Lloyd 2010](#lloyd10)). The lack of familiarity with the workplace context enables researchers to enter this field without preconceived ideas about how information literacy should happen.

Thus, these different empirical contexts are likely to have implications for how information literacy researchers enter and analyse the practices studied. In the following we will explore this further.

## Aim and research questions

This article aims to lay a foundation for a discussion on how researchers may approach issues of norms and values in information literacy research. This will be done through a comparison between eight selected studies on information literacy in educational and workplace settings respectively. The comparison will be based on the following research questions:

*   How do researchers understand and approach information literacy as an object of study in educational and workplace settings respectively?
*   How do researchers understand and approach issues of norms and values in the educational and workplace settings respectively?
*   How are the approaches of information literacy research in educational settings applicable in studies of workplace settings, and vice versa?

## Selection of studies

Eight empirical studies focussing on information literacy have been selected for analysis; four studies derived from educational settings and four studies were conducted in workplace settings. An overview of the studies is provided in table 1.

The studies were chosen to provide varied, yet comparable examples of recent studies of information literacy in educational and workplace settings. Five of the studies (1, 2, 4, 5, 8) were found through keyword searches in the _Library & Information Science Abstracts_ database, whereas three of them (3, 6, 7) were brought to our attention through other means that can be used for monitoring a research field (e.g. personal communication; keeping updated on latest issues of specific journals; citation chaining; participation in workshops and conferences; etc.). Studies involving any of the three authors of the present paper were excluded in the selection process.

In this instance, recent means studies published in the years of 2010 (1 study) and 2011 (7 studies). We also sought a variation in terms of research traditions, reflected in choice of research methods, as well as in terms of geographical locations of the empirical settings. However, given the nature of the research field, where studies of information literacy in educational settings are common, and studies of workplace settings are still relatively few, the possibilities of variation were slightly limited in the latter category. Still, taken together, the eight studies involve empirical settings of six countries, and, as will be suggested in the following section, represent quite different research traditions.

<table><caption>

Table 1\. Overview of the eight studies</caption>

<tbody>

<tr>

<th>Study</th>

<th>Setting</th>

<th>Research methods</th>

<th>Country (of the empirical context)</th>

</tr>

<tr>

<td>

1\. Chu, S.K.W, Tse, S. K. and Chow, K. (2011). Using collaborative teaching and inquiry project-based learning to help primary school students develop information literacy and information skills. _Library & Information Science Research_, **33**(2), 132-143.</td>

<td>Educational: Primary school</td>

<td>Mixed methods</td>

<td>Hong Kong</td>

</tr>

<tr>

<td>

2\. Gross, M. and Latham, D. (2011). Experiences with and perceptions of information: a phenomenographic study of first-year college students. _The Library Quarterly_, **81**(2), 161-186.</td>

<td>Educational: College</td>

<td>Phenomenographic interviews</td>

<td>USA</td>

</tr>

<tr>

<td>

3\. Sormunen, E. and Lehtiö, L. (2011). [Authoring Wikipedia articles as an information literacy assignment: copy-pasting or expressing new understanding in one's own words?](http://informationr.net/ir/16-4/paper503.html) _Information Research_, **16**(4), paper 503.</td>

<td>Educational: Upper secondary school</td>

<td>Primarily text comparisons of student work</td>

<td>Finland</td>

</tr>

<tr>

<td>

4\. Walton, G. and Hepworth, M. (2011). A longitudinal study of changes in learners' cognitive states during and following an information literacy teaching intervention, _Journal of Documentation_, **67**(3), 449-479.</td>

<td>Educational: University (undergraduate)</td>

<td>Quasi-experimental study of two types of information literacy interventions</td>

<td>UK (England)</td>

</tr>

<tr>

<td>

5\. Conley, T.M. and Gil, E.L. (2011) Information literacy for undergraduate business students: examining value, relevancy, and implications for the new century. _Journal of Business & Finance Librarianship_, **16**(3), 213-228</td>

<td>Workplace: business</td>

<td>Survey</td>

<td>USA</td>

</tr>

<tr>

<td>

6\. Eckerdal, J.R. (2011). [To jointly negotiate a personal decision: a qualitative study on information literacy practices in midwifery counselling about contraceptives at youth centres in Southern Sweden.](http://informationr.net/ir/16-1/paper466.html) _Information Research_, **16**(1), paper 466.</td>

<td>Workplace: Maternity care youth centres</td>

<td>Thematic analysis of recorded conversations and qualitative interviews</td>

<td>Sweden</td>

</tr>

<tr>

<td>

7\. Moring, C. (2011). Newcomer information practice: negotiations on information seeking in and across communities of practice. _Human IT_, **11**(2), 1-20.</td>

<td>Workplace: Transport company</td>

<td>Ethnography</td>

<td>Denmark</td>

</tr>

<tr>

<td>

8\. O'Farrill, R. T. (2010). Information literacy and knowledge management at work: conceptions of effective information use at NHS24\. _Journal of Documentation_, **66**(5), 706-733.</td>

<td>Workplace: Medical call centre</td>

<td>Phenomenographic interviews and document analysis</td>

<td>UK (Scotland)</td>

</tr>

</tbody>

</table>

## The studies

Before comparing the eight studies, each of them will be introduced briefly, with a focus on the explicit aims of the studies, the empirical settings and how data were collected in the studies. After this introductory description, the studies will be discussed with the basis in the two first research questions.

The first study, by Chu _et al._ ([2011](#chu11)) was conducted at a primary school to address the need for the development of “an innovative instruction approach and examine how it may promote the learning of these skills [information literacy and information technology skills] among students.” ([Chu _et al._, 2011: 133](#chu11)). The study includes the development of a project-based teaching approach, and is designed to capture how students', parents' and staff's views on how this teaching approach helps the students' development of information literacy and information technology skills.

The second study, by Gross and Latham ([2011](#gross11)) concerns college students' levels of information literacy. While not explicit, one aim seems to be to understand students' views of what information literacy is through a phenomenographic analysis of 77 qualitative interviews. Another aim seems to be to compare and relate these conceptions to the students' results on an information literacy test.

The third study, by Sormunen and Lehtiö ([2011](#sormunen11)), focuses on one aspect of information literacy, namely that of information use in academic writing. The explicit aim of the study is: “to explore how writing for Wikipedia works as an information literacy exercise and as a group assignment in an upper secondary school” ([Sormunen and Lehtiö, 2011: no pagination](#sormunen11)). Thus, the context is upper secondary school where groups of students have been given the task of writing Wikipedia articles. The study is part of a larger ethnographical project and this specific study mainly analyses eleven written assignments in terms of the extent to which they are based on copy-and-paste practices.

The fourth study, by Walton and Hepworth ([2011](#walton11)) has, as we understand it, two aims: one is to try to comprehend how undergraduate students' cognition changes in the process of learning how to evaluate information sources. The other aim is to test different types of information literacy interventions. The study is designed in a type of experimental way, where different types of instructional designs are tested and evaluated through the use of several different data collection techniques.

The fifth study is a workplace study by Conley and Gil ([2011](#conley11)). It is conducted in a university setting, but the participants of the study are business professionals working in various types of enterprises and it is in these roles that they are participating in the study. It builds on a survey conducted during a Career Fair. The aim, although not clearly articulated, seems to be to understand what information literacy might be in business settings, and thereby be able to adjust information literacy instruction at the university to this understanding.

Study number six, by Eckerdal ([2011](#eckerdal11)), is conducted at maternity care centres where young women meet midwifes to discuss choices of contraceptives. The aim is double: firstly, the study is meant to enhance the understanding of information literacy practices in a health-care setting, and secondly, to explore the usefulness of positioning theory for analysing information literacy practices in such a setting. Data is collected through recordings of 10 counselling meetings between midwifes and young women, and interviews conducted with all participants after the meetings.

The seventh study, by Moring ([2011](#moring11)) has a focus on how newly employed sales assistants learn information seeking. The study has no explicit aim, but as we understand it, the aim seems to be to be to illustrate how the theoretical perspective of practice learning can be used to describe how the learning of information seeking within and between particular communities of practice takes place. To fulfil this aim, two empirical examples from a larger ethnographical study are presented and analysed.

Study number eight, by O'Farrill ([2010](#farrill10)) is conducted at a government health information and consultation call centre (NHS24), operated by specialised nurses. The aim of the study is to characterise information literacy at this workplace from the point of view of the staff and to examine the relevance and applicability of mainstream institutional information literacy frameworks. The study is conducted through phenomenographic interviews with 42 nurses, and an analysis of the organisation's documented knowledge management strategies.

## Conceptualisations of information literacy

The main concept of the field of information literacy is understood and operationalised in quite different ways in the eight studies. In our readings, we can identify three overall ways of defining the concept of information literacy: one is to draw on well-known information literacy standards and then apply these definitions, or parts of them, when approaching an empirical setting. Another approach is to revisit these types of standard definitions, through comparing them to the results of the empirical study conducted. A third strategy seems to be to avoid defining the concept at all.

The four studies conducted in educational settings all refer to and employ standardised, skill and ability based definitions of information literacy, such as the ACRL standards ([Association of College and Research Libraries 2000](#association00)). Characteristic of these definitions is the description of information literacy as an individual and generic competency, which once learnt, can be applied in all sorts of situations and contexts. Chu _et al._ ([2011: 134](#chu11)), for example, refer to UNESCO's Prague Declaration where information literacy is defined as the “knowledge of one's information concerns and needs, and the ability to identify, locate, evaluate, organize and effectively create, use and communicate information to address issues or problems at hand”. Sormunen and Lehtiö ([2011](#sormunen11)), state that the writing of Wikipedia entries offers “a framework for information literacy instruction which is similar to that of a scientific article and concurrent with the information literacy standards [the ACRL standards]” ([Sormunen and Lehtiö 2011: no pagination](#sormunen11)).

Another significant feature of these standardised kinds of definitions is that they portray information literacy as measurable on an individual level. This is particularly evident in the study of Gross and Latham ([2011](#gross11)). Although Gross and Latham acknowledge that '_the conversation about what information literacy is… continues_' ([2011](#gross11): 162), the study builds on the idea that information literacy can be measured by “an objective, standardized assessment of an individual's information literacy skill level” (ibid: 164). By using such a test, Gross and Latham suggest that their study participants can be divided into two distinctive groups: '_those who test as proficient and those who test as below-proficient in terms of their information literacy skills_' (ibid: 162). This division is then used as a basis for the phenomenographic analysis of students' perceptions of information and information literacy. Hence, the study is built on a predefined and binary (see [Johansson 2009](#johansson09): 246) information literacy definition. Even though it is argued that the study seeks to understand the students' perspectives, it is clear that the researchers are the ones who determine what counts as information literacy and not, based on the ACRL standards. The fact that the study participants don't concur with this view does not change the authors' belief in the accuracy of the standards:

Unless students come to understand that information literacy represents an objective skill set that is definable, achievable, and that can improve their experience in seeking information, there will be little reason for them to seek to develop or improve these skills. ([Gross and Latham 2011: 184](#gross11)).

While the study of Walton and Hepworth ([2011](#walton11)) is designed quite differently than the study of Gross and Latham ([2011](#gross11)), it draws on similar ideas of information literacy as a standardised, predefined, individual, and measurable set of skills. This study goes even so far as to suggest a set of formulae to be used '_for measuring the effectiveness of IL teaching and learning interventions_' ([Walton and Hepworth 2011](#walton11): 471). This quote also highlights another starting-point that the four studies conducted in educational settings have in common, that is, that information literacy is seen as an object of teaching and learning (see [Pilerot and Lindberg 2011](#pilerot11): 341; [Limberg and Alexandersson 2010](#limberg10)). Thus, underlying these studies, implicitly or explicitly, is the aim of improving students' levels of information literacy, as defined by a standard, by improving the design of information literacy education.

It should be noted, however, that all but one (Moring's study) of the studies concerning workplace information literacy also refer to information literacy standards. Especially the study by Conley and Gil ([2011](#conley11)) takes information literacy instruction as its starting-point and considers how it can be improved. But the approach of this study differs from the educational studies in that it opens up for conceptualisations of information literacy other than the standardised ones, in this case, by trying to understand their students' future employers' understandings of information literacy.

However, Conley and Gil ([2011](#conley11)) do not go as far as to contest their initial definition of information literacy. This more radical approach is presented by O'Farrill ([2010](#farrill10)) who explicitly states that '_rather than testing a particular definition of IL, the study aimed at exploring what the concept meant to the frontline staff of NHS24_' (p. 707). This means that in O'Farrill's study, the object for evaluation is the information literacy standards, and not the study participants. Thus, the approach is the opposite to the approaches of the educational studies:

> people who perform well (experts) in workplace environments are ordinarily information literate in that context. This leads to granting more importance to the exploration of what IL is, than to prescription of what it should be. ([O'Farrill 2010: 729](#farrill10)).

In this quote, O'Farrill suggests that information literacy cannot be seen as a generic skill, but instead needs to be conceptualised in relation to specific contexts and practices. In order to conduct the study, however, information literacy is broadly defined as “effective information use” ([O'Farrill 2010: 707](#farrill10)), then to be given more nuanced meanings through the empirical study.

The two other studies of workplace contexts, by Moring ([2011](#moring11)) and Eckerdal (2011), present approaches similar to that of O'Farrill ([2010](#farrill10)) in that the participants' views are favoured and that information literacy is not given an ultimate meaning. In fact, Moring does not present any definition at all, but works instead with the idea of '_information seeking as an integrated part of their [the study participants'] learning process_' ([Moring 2011](#moring11): 2). Eckerdal explicitly states that '_no definition of the concept is offered in this study_' ([2011: Using positioning theory...](#eckerdal11)), at the same time as it is stated that information literacy can only be studied '_as situated in a practice_' (ibid.). Thus, in these two studies, the concept of information literacy seems elusive and questions could be raised regarding the actual object of study.

Thus, in the studies the concept of information literacy is either predefined, post-defined or not defined at all. The first strategy is used in all of the studies of educational settings, and partly in one of the workplace studies ([Conley and Gil 2011](#conley11)); whereas the second and third strategies are employed in the workplace studies. These three strategies seem to interplay with the ways in which issues of norms are approached in the empirical settings; this topic will be explored in the next section.

## Approaches to norms and values

From our readings of the eight papers, we suggest that the question of norms and values can be understood in relation to the different conceptualisations of information literacy. In summary, the papers can be said to represent two opposite views of the central concept of information literacy. On the one hand, in the articles where the concept is predefined, information literacy is considered as something that exists as a set of skills and abilities that can be seen as _learning outcomes_ ([Chu _et al._ 2011](#chu11): 133); that are possible to measure through the use of '_an objective test_' ([Gross and Latham 2011: 161](#gross11)); that resides in people's '_cognitive states and transitions_' ([Walton and Hepworth 2011](#walton11): 452); or is visible in people's actions, such as '_students' use of information sources in authoring Wikipedia articles_' ([Sormunen and Lehtiö 2011](#sormunen11)). However, it is acknowledged in one of the studies that the term information literacy as such is not very well understood outside the university library sector ([Conley and Gil 2011](#conley11)).

On the other hand, in the studies where information literacy is post-defined, or not clearly defined, the concept of information literacy is approached as a theoretical construct; information literacy, or in Moring's ([2011](#moring11)) case information seeking as a part of a learning process, is not seen as a trait of individuals that can be measured and evaluated. In these three studies, information literacy or information seeking as a part of learning is instead sought to be understood through the empirical studies. Information literacy, or learning, is considered as already existing or taking place, and the researchers' task is to understand what it is in a specific setting or as it is '_practised in a context_' ([Eckerdal 2011: no pagination](#eckerdal11)).

These two approaches create quite different relations between the researchers and the settings studied, and by extension, how issues of norms and values in relation to information literacy are dealt with. In most of the studies, the relationship between the researchers and the settings studied is not commented on, and as a reader, you can only guess from which vantage point the studies are conducted. The two exceptions are O'Farrill's ([2010](#farrill10)) and Eckerdal's ([2011](#eckerdal11)) studies, where it is clearly stated that the two empirical settings, both in the health sector, were chosen because the authors wanted to highlight specific questions in relation to information literacy: in O'Farrill's ([2010](#farrill10)) case the relationships between information literacy and knowledge management, and in Eckerdal's case ([2011](#eckerdal11)) information literacy outside educational settings. Also in Moring's ([2011](#moring11)) study, it seems as if the researcher is new to the setting studied and has chosen it for theoretical reasons, even though this is not explicitly stated.

In these three workplace studies, questions of norms and values in relation to information literacy are, if discussed at all, treated analytically. The question of what information literacy is, is regarded as an empirical question; learning is understood as already happening in the settings, and the task for the researcher is to understand and reveal what is being learnt and/or how it happens. In the respective studies by Moring ([2011](#moring11)) and Eckerdal ([2011](#eckerdal11)), the processes studied are understood as value-laden and related to social norms within in the social practices where they take place. Moring describes that preferable ways of seeking information is seen as something that is socially, collectively, and locally negotiated at different workplaces ([Moring 2011: 16-18](#moring11)). Eckerdal ([2011: Using positioning theory...](#eckerdal11)) suggests that information literacy as a concept is ambiguous, since it implies empowerment for individuals, as well as societal and social expectations. One of her conclusions is that this contradiction is visible in the setting studied:

> The ambiguity of the concept information literacy is thus very much present in the storyline of the counselling meeting at a youth centre. The choice [of contraceptives] is constructed in a tension between an understanding of the individual's self-expression and the opportunities offered by plastic sexuality and expectations from the society upon the individual as a responsible and well-informed citizen. ([Eckerdal 2011: Discussion (para. 11)](#eckerdal11)).

In Moring's ([2011](#moring11)) and Eckerdal's ([2011](#eckerdal11)) studies information literacy practices are regarded as study objects characterised by negotiations of social norms and values. In the studies where information literacy is predefined, and related to educational settings, the approach is quite different. In these studies, information literacy is presented as something desirable and essentially positive. By presenting it this way, the researcher is put in a position from which it is considered possible to assess the setting, the activities, and the people studied.

Thus, the studies of educational settings are based on an approach implying that the settings and the individuals studied are compared to ideas of how these settings and individuals should ideally perform. This position entails a quantifiable view of information literacy; individuals are deemed more or less information literate (even though the term _information illiterate_ is not used). For example, students are asked to assess '_their level of information literacy_' ([Chu _et al._ 2011](#chu11): 135).; they are described as having '_below-proficient_' or '_proficient_' information literacy skills ([Gross and Latham 2011](#gross11)) or as showing '_a greater degree of information literacy_' ([Walton and Hepworth 2011](#walton11): 468); or their '_cognitive effort in transforming information_' ([Sormunen and Lehtiö 2011](#sormunen11): Copy pasting... (para. 2)) is measured.

A common characteristic is that each of these four studies builds on a deficit model where exceptions to the established norms of information literacy are regarded as problematic. In other words, the established norms are imposed from an outside researcher's perspective. The researchers' role of defining what information literacy should be is not questioned in these studies, and there is not any discussion of how this position is sanctioned or how it is understood from the participants' perspectives.

## Swapping settings

In summary, two overall approaches to norms and values in relation to information literacy can be identified in the eight studies (see table 2). Whereas the studies conducted with students have a clear evaluative approach, three of the workplace studies are characterised by an explorative approach. One of the workplace studies, by Conley and Gil ([2011](#conley11)), has an in-between position, as it illustrates some of the concerns that arise when predefined approaches to information literacy face settings outside education.

<table><caption>

Table 2\. Summary of differences between the two approaches</caption>

<tbody>

<tr>

<th></th>

<th>Evaluative approach</th>

<th>Explorative approach</th>

</tr>

<tr>

<td>

**Conceptualisation of information literacy**</td>

<td>Predefined</td>

<td>Post-defined or not defined</td>

</tr>

<tr>

<td>

**Basis for conceptualisation**</td>

<td>Externally formulated standards</td>

<td>The empirical study of a particular social practice</td>

</tr>

<tr>

<td>

**Role of the researcher**</td>

<td>To evaluate information literacy</td>

<td>To understand and illustrate how information literacy emerges and happens in a particular setting</td>

</tr>

<tr>

<td>

**Theoretical underpinnings**</td>

<td>Focus on the individual</td>

<td>Focus on social and political aspects</td>

</tr>

</tbody>

</table>

In addition to different empirical settings, the differences between the evaluative approach and the explorative approach can also be seen in the light of different theoretical underpinnings. Especially two of the explorative studies, the ones by Moring ([2011](#moring11)) and Eckerdal ([2011](#eckerdal11)) are theoretically driven, with a basis in different practice and discursively oriented theories. Also O'Farrill ([2010: 709](#farrill10)) applies ideas of _situated practice_ in his analysis. The four evaluative studies and the study by Conley and Gil ([2011](#conley11)) are not as explicitly and consistently theoretically based, but overall they are oriented towards individuals as units of analysis, rather than collective practices. Even though the relationships between the two identified approaches, the two types of empirical settings, and the different theoretical starting-points should not be described in terms of causality, the idea of swapping settings, that is, studying workplace settings with an evaluative approach and educational settings adopting an explorative approach might enable reflections on the underlying perspectives that direct contemporary information literacy research.

A feature of the evaluative approach is that findings often identify information activities as limited or insufficient in relation to certain information literacy standards, and discuss the implications of these findings for improving information literacy education. When studying educational settings, where pupils' and students' development of information literacy can be seen as a preferable outcome for the educators involved, it is relatively easy as a researcher to try and assess the information literacy of the students in relation to some norm of how information literacy should appear. One interpretation of this type of information literacy studies is to see them as a way of promoting librarianship and supporting the professional project of librarians. For example, this is a view put forward by Sundin ([2008](#sundin08)), who suggests that:

> the very act of establishing the concept of information literacy and of providing it with content, which in turn leads to the creation of a “novel” and distinct expert role for librarians, can equally be interpreted as a means of increasing the occupation's status. ([Sundin 2008](#sundin08): 26).

In this light, the idea that there exists standards for information literacy appears reasonable; by articulating such standards, librarians and information literacy researchers can also be put in the position of judging the levels of information literacy of individuals, and furthermore, if information literacy skills are found to be lacking, librarians and information literacy researchers can provide a remedy (see [Tuominen 1997](#tuominen97)).

However, when turned towards a workplace context, the evaluative approach poses certain difficulties. One reason for this is that information literacy in workplace settings cannot be seen as a goal in itself, but a means to achieve other goals. Furthermore, most research participants in workplace contexts could not be assumed to use the concept of information literacy when describing their work tasks and performance. Norms and values tied to information literacy are therefore not as easily accessible to researchers entering a workplace setting, since they are not systematically made explicit.

The idea of turning the evaluative approach towards workplace settings also raises questions regarding the professional project of librarians, in relation to other professions and occupational groups (see [Sundin 2008](#sundin08)). Even though librarians might be seen as experts on information seeking, their roles in this area are not, on a societal level, to generally prescribe certain ways of conducting information seeking. Therefore, when information literacy is studied in workplace settings, the difference between “real world” information literacy – or perhaps literacies – and standardised ideas on information literacy are possible to highlight. This kind of understanding can be used for improving educational efforts of librarians, such as user education and information literacy instruction, rather than suggesting improvements of the information seeking of professionals and workers.

In contrast, the idea of applying an exploratory approach towards information literacy in educational settings seems quite feasible. A workplace context can, as we have seen, be approached with the basis in a question of what constitutes information literacy in that specific context, in relation to the tasks and the expectations on one or several professional groups. The same question can be posed in relation to an educational context. Rather than comparing information activities in an educational setting to information literacy standards, the activities can be explored in the light of the goals of a specific lesson, unit or subject. Then, by analysing what it might mean to be information literate as a pupil or a student of a particular subject, in a particular educational setting, a foundation for improving the teaching for this particular setting can be created. This, we suggest, would also lead to quite different relationships between researchers and research participants.

However, a problem related to the explorative approach might be that power structures and contradictions within the settings studied are overlooked. Furthermore, the aim to study information activities in any setting, in order to improve information literacy teaching, is of course a reasonable one. What information literacy might be in a particular setting is, as we pointed out in the introduction, a highly value-laden question. One challenge for information literacy researchers, then, is to describe how information literacy is enacted in different settings with a non-evaluative approach, but at the same time to be able to critically discuss how this enactment might take place.

## Conclusion

In this paper, we have highlighted several challenges for information literacy research. We have introduced the question of conducting research that is professionally relevant. This challenge was related to the role of theory in information literacy research. Through the analysis conducted in the paper, we have also recognised the challenge of approaching empirical settings in an explorative way, and yet create the possibility of identifying problems in the setting studied.

We suggest that the ambition and the need for information literacy research to be professionally relevant should not be seen as excluding theorising. On the contrary, our analysis suggests that well-chosen theoretical perspectives are necessary for information literacy research. The question of whose perspective that is guiding studies of various groups and settings is critical and needs to be addressed. With careful consideration of the theoretical and methodological perspectives taken, it is possible to approach any empirical setting with the aim of trying to understand the rationales that underlie the activities studied. It is by creating this kind of understanding, which respects the local purposes of an activity, that information literacy research could be able to profoundly impact professional practice.

</section>

<section>

## References

<ul>
<li id="alexandersson12">Alexandersson, M. &amp; Limberg, L. (2012). Changing conditions for information ase and learning in Swedish schools: a synthesis of research. <em>Human IT</em>, <strong>11</strong>(2), 131–154
</li>
<li id="association00">Association of College and Research Libraries. (2000). Information literacy competency standards for higher education. Retrieved 15 July, 2013 from http://www.ala.org/ala/mgrps/divs/acrl/standards/informationliteracycompetency.cfm (Archived by WebCite® at http://www.webcitation.org/6INuvs8eW)
</li>
<li id="chu11">Chu, S.K.W, Tse, S. K. &amp; Chow, K. (2011). Using collaborative teaching and inquiry project-based learning to help primary school students develop information literacy and information skills. <em>Library &amp; Information Science Research</em>, <strong>33</strong>(2), 132-143
</li>
<li id="conley11">Conley, T.M. &amp; Gil, E.L. (2011). Information literacy for undergraduate business students: examining value, relevancy, and implications for the new century. <em>Journal of Business &amp; Finance Librarianship</em>, <strong>16</strong>(3), 213-228.
</li>
<li id="eckerdal11">Eckerdal, J.R. (2011). To jointly negotiate a personal decision: a qualitative study on information literacy practices in midwifery counselling about contraceptives at youth centres in Southern Sweden. <em>Information Research</em>, <strong>16</strong>(1)
</li>
<li id="gross11">Gross, M. &amp; Latham, D. (2011). Experiences with and perceptions of information: a phenomenographic study of first-year college students. <em>The Library Quarterly</em>, <strong>81</strong>(2), 161-186
</li>
<li id="johansson09">Johansson, V. (2009). Berättelser i gränssnittet: kritiska kompetenser och interaktiva informationsresurser. [Stories in the interface: critical competencies and interactive information resources]. In J. Hedman and A. Lundh (Eds.), <em>Informationskompetenser: om lärande i informationspraktiker och informationssökning i lärandepraktiker</em> [Information literacies: On learning in information practices and information seeking in learning practices] (pp. 235-267). Stockholm: Carlssons
</li>
<li id="julien11">Julien, H. &amp; Williamson, K. (2011). Discourse and practice in information literacy and information seeking: gaps and opportunities. <em>Information Research</em>, <strong>16</strong>(1).
</li>
<li id="limberg10">Limberg, L. &amp; Alexandersson, M. (2010). Learning and information seeking. <em>Encyclopedia of Library and Information Sciences</em>, Third Edition (pp. 3252-3262). Boca Raton, FL: CRC Press
</li>
<li id="limberg12">Limberg, L., Sundin, O. &amp; Talja, S. (2012). Three theoretical perspectives on information literacy. <em>Human IT</em>, <strong>11</strong>(2), 93–130
</li>
<li id="lloyd10">Lloyd, A. (2010). Information literacy landscapes: Information literacy in education, workplace and everyday contexts. <em>Chandos: Oxford UK</em>
</li>
<li id="lloyd05">Lloyd, A. (2005). Information literacy: different contexts, different concepts, different truths? <em>Journal of Librarianship and Information Science</em>, <strong>37</strong>(2), 82-88
</li>
<li id="lundh11">Lundh, A. (2011). Doing research in primary school: information activities in project-based learning. Borås: Valfrid. Retrieved 30 May 2013, from http://hdl.handle.net/2320/8610 (University of Gothenburg Ph.D. dissertation)
</li>
<li id="moring11">Moring, C. (2011). Newcomer information practice: negotiations on information seeking in and across communities of practice. <em>Human IT</em>, <strong>11</strong>(2), 1-20
</li>
<li id="farrill10">O'Farrill, R. T. (2010). Information literacy and knowledge management at work: conceptions of effective information use at NHS24. <em>Journal of Documentation</em>, <strong>66</strong>(5), 706-733
</li>
<li id="pilerot11">Pilerot, O. &amp; Lindberg, J. (2011). The concept of information literacy in policy-making texts: an imperialistic project? <em>Library Trends</em>, <strong>60</strong>(2), 338–360
</li>
<li id="savolainen07">Savolainen, R. (2007) Information behavior and information practice: reviewing the 'umbrella concepts' of information-seeking studies. <em>Library Quarterly</em>, <strong>77</strong>(2), 109-32
</li>
<li id="sormunen11">Sormunen, E. &amp; Lehtiö, L. (2011). Authoring Wikipedia articles as an information literacy assignment: copy-pasting or expressing new understanding in one's own words? <em>Information Research</em>, <strong>16</strong>(4).
</li>
<li id="sundin08">Sundin, O. (2008). Negotiations on information-seeking expertise: a study of web-based tutorials for information literacy. <em>Journal of Documentation</em>, <strong>64</strong>(1), 24 - 44
</li>
<li id="sundin11">Sundin, O. (2011). From the periphery to the centre: some aspects regarding the future of information literacy research. Position paper for the Social Media and Information Practices workshop: State of the art and future challenges for information literacy research, University of Borås, Sweden 10-11 November 2011. Retrieved 25 July 2013, from http://iilresearch.files.wordpress.com/2012/01/sundin_position-paper.pdf. (Archived by WebCite® at http://www.webcitation.org/6INvJQxZN)
</li>
<li id="saljo00">Säljö, R. (2000). Lärande i praktiken: att sociokulturellt perspektiv. [Learning in practice: a sociocultural perspective]. <em>Stockholm: Prisma</em>
</li>
<li id="tuominen97">Tuominen, K. (1997). User-centered discourse: an analysis of the subject positions of the user and the librarian. <em>Library Quarterly</em>, <strong>67</strong>(4), 350-371
</li>
<li id="walton11">Walton, G. &amp; Hepworth, M. (2011). A longitudinal study of changes in learners' cognitive states during and following an information literacy teaching intervention. <em>Journal of Documentation</em>, <strong>67</strong>(3), 449-479
</li>
</ul>

</section>

</article>