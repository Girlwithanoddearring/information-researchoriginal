<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Record as social action: understanding organizational records through the lens of genre theory

#### [Fiorella Foscarini](mailto:fiorella.foscarini@utoronto.ca)  
University of Toronto,  
Faculty of Information,  
140 St. George Street,  
Toronto, ON M5S 3G6,  
Canada

#### Abstract

> **Introduction**. Rhetorical genre studies offers a powerful lens to interpret the form and function of any texts (both written and oral) as typified social actions. This paper argues that incorporating a genre perspective into the archival domain may provide new insights, especially in relation to the understanding of the nature of a record and the moments of records creation and use.  
> **Method**. After reviewing some foundational principles of archival science, the paper considers the limitations of this approach. The main ideas of rhetorical genre studies are then discussed and used as a basis to reconceptualise record-related notions.  
> **Analysis**. By comparing the theoretical and methodological views of both disciplines, specific contributions of genre theory to records management are highlighted. The analysis also suggests that genre scholars may usefully draw on some archival concepts.  
> **Results**. The paper demonstrates that extending archival theory through genre ideas makes the approach characterizing records more relevant to the complex and evolving organizational systems that create and use records.  
> **Conclusions**. More research is needed to ensure that the dialogue between the record and the genre discipline continues and grows for the benefit of both areas of study.

<section>

## Introduction

The dialogue between the archives and records management discipline on the one hand and genre studies on the other is a very recent phenomenon, and probably most scholars of archival science (a term encompassing the study of both current and historical records) are still sceptical about the actual benefits their area of interest might gain by drawing on a discipline commonly associated with literary studies, rhetoric, and taxonomic classification of any kinds of cultural artefacts. While the library and information science domain seems to have embraced both traditional and contemporary interpretations of genre theory and to have produced innovative genre-related research (in connection with knowledge organization, web design, digital communication, information retrieval, etc. – see [Andersen 2008](#andersen08)), until a few months ago, the studies exploring the potential of genre ideas in relation to issues of concern to archivists and records managers could be counted on the fingers of one hand ([Oliver _et al._ 2008](#oliver08); [Foscarini _et al._ 2010](#foscarini10); [Oliver and Foscarini 2011](#oliver11)). The special issue of Archival Science published in December 2012 and entirely dedicated to genre studies in archives may be seen as an exceptional step forward. Is it a sign that a fruitful and lasting interdisciplinary exchange is about to happen?

The intention to nourish further this rising dialogue prompted the writing of this paper and its presentation at an international conference that promotes cross-fertilization of ideas. CoLIS 8 (the eighth international conference on Conceptions of Library and Information Science) involves a multidisciplinary audience, which includes, inter alia, genre experts who might be intrigued by the very special view of documentation that archivists and records managers share and by the synergies that are here suggested. The feedback received will help develop and shape these initial ideas, with the hope that genre theory may eventually become a central research approach for archival scholars.

This paper focuses particularly on current records, that is, the ‘genres of organizational communication’, as Yates and Orlikowski ([1992](#yates92)) would call them, which are, according to another genre researcher, ‘the very substance of organizations, their fact-making mechanisms’ ([Schryer 1993: 205](#schryer93)). The first part will summarize basic records-related notions and will highlight some of the limitations inherent in traditional archival understandings of the records and their contexts. Rhetorical Genre Studies will then be introduced as a complementary perspective that has the potential of enriching the archival one. The juxtaposition of recordkeeping and genre ideas is expected to contribute – by borrowing from the title of Miller’s ([1984](#miller84)) most cited article – to a reconceptualization of record as ‘social action’.

## Records as evidence of business activity

The nature of a record is one of the most debated issues in the archival literature ([Yeo 2007](#yeo07), [2008](#yeo08)). For the purposes of this paper, I will refer to a perspective that has its roots in major European archival traditions and that refers to records as by-products (residues, traces, outcomes) and instruments (tools, means) of the activities that individuals and organizations carry out in the usual and ordinary course of business. When set aside (filed, stabilized and kept in some meaningful way), such documentary evidence becomes a record (from the Latin word recordari, to remember), a tool that may be used to recall the activities that generated it for further action or reference ([Jenkinson 1966](#jenkinson66); [Cencetti 1970](#cencetti70); [Schellenberg 2003](#schellenberg03)). Because records are created ‘unselfconsciously’ and accumulate ‘naturally’ (in the sense that their creators are primarily concerned with the action they aim to accomplish, rather than the evidence they may leave of it, or the organization of the records for any future use), we trust them and use them as the foundation of our organizations’ accountability, especially when they are created and maintained in a controlled environment.

Records managers and archivists support records creators’ past, present and future accountability by ensuring an uninterrupted and uncorrupted transmission of their collective memory. The former are in particular responsible for establishing rules and best practices for the creation, organization, use, access, destruction, and maintenance of authentic and reliable records ([International Organization for Standardization 2001](#international01); [Shepherd and Yeo 2003](#shepherd03)). Such rules and best practices are embedded in corporate policies, procedures, training materials, available recordkeeping systems and technologies (including records classification systems, retention plans, electronic document and records management systems) and the records themselves (in the form of metadata). At the moment of records creation, the business needs and professional background of the creators and internal users of the records meet the records managers’ expertise and knowledge of the ‘record needs’. That is a critical moment, as the two parties have different purposes and most likely do not share the same culture. It is also crucial because it is the moment in which the contextual information that identifies the record as a record (i.e., its reliability and authenticity qualities), its relationships with the action it originated from and with the other records participating in the same action, and what the record means to those who created it must all be captured ([Duranti _et al._ 2002](#duranti02)).

Focussing on the contextual elements that surround the creation of records rather than their informational content is one of the hallmarks of the archival and records management theory and methodology. These contexts are often described as 1) the juridical-administrative framework in which the action and the documentation of the action take place; 2) the records’ provenance (i.e., its origin from a specific creator); 3) the procedure or business process determining how the actions involved are carried out; 4) the documentary context each record belongs to (i.e., its relationship with any other record constituting the whole archives of a single creator, and the structure itself of such archives and of every individual aggregation within it); 5) the technology in which the record is created (whether paper-based or electronic) ([Duranti and Preston 2008](#duranti08)). Such contextual understanding contributes to make each record unique and only existing and meaningful when considered in the web of its multiple relationships. Uniqueness and interrelatedness – together with impartiality, authenticity, and naturalness – are the properties ascribed to records by an uninterrupted archival tradition that started to consolidate in central Europe and England in the second half of the 19th century ([Duranti 1997b](#duranti97)).

## Limitations of traditional archival ideas

When considering the principles and methods of archival science, one has to be mindful that archives as a discipline was established at a time when the cultural milieu was characterized by positivism in science and philosophy, and when archival materials were almost exclusively textual documents produced by public administrative bodies. Furthermore, the origin of most of its fundamental concepts, such as the one of authenticity, may be found in diplomatics, a scientific approach to the analysis of the formal features of documents (diplomas) that was codified in France in the 17th century and that has been referred to as the theoretical basis of records management ([Duranti 1998, 2010](#duranti10)).

In the last few decades, several authors – whether explicitly or implicitly participating in ‘post-modern’ currents of archival thinking – have challenged some of the traditional tenets of their discipline and have suggested new ways of conceiving the record world and its relevant practices ([Bearman 1993](#bearman93); [Cook 1994](#cook94), [2001](#cook01); [Harris 1998](#harris98); [Brothman 2002](#brothman02), [2011](#brothman11); [McKemmish _et al._ 2005](#mcKemmish05); [Eastwood and MacNeil 2010](#eastwood10)). The new paradigm, or better, paradigms, have in many cases been invoked as a response to the drastic changes occurred to information objects and work practices with the advent of electronic communication technologies, and as Eastwood ([2010: 19](#eastwood10)) argued, should be seen as a ‘reorientation of the archival enterprise rather than wholesale abandonment of the traditional view of the nature of archives’ (emphasis added). Bringing genre theory into play is a way of contributing to this reorientation and of enhancing the explanatory power of existing new paradigms by leveraging on the various theoretical approaches employed by genre scholars. However, before delving into the details of genre studies, a closer look at some specific limitations of traditional archival ideas – mostly deriving from the positivist backbone of the discipline – seems to be appropriate, as a useful introduction to the new insights gleaned through the application of the genre lens.

The account of the nature of a record provided earlier clearly shows that to be accorded record status, information objects need to fulfil certain prerequisites. Despite recent openings to different kinds of media and formats (e.g., emails, blogs, tweets), the distinction between records and ‘non-records’ still exists. Information needs to be affixed to a medium, to have a stable content and a fixed form, to be relevant to some purposeful activity in order to be considered a record. Also, among the records, there are ‘good’, or ‘well-formed’, and ‘bad’ records. Final versions of official records created in the course of business activities are records par excellence, and their being natural and impartial by-products of those activities directly bears on their evidential value.

Records managers’ efforts are all directed to protect the qualities inherent in ‘good’ records, even when their actions might be in conflict with the creators/users’ needs. They are assisted in this by a plethora of standards, models, guidelines, laws, and manuals, which mostly reflect the assumptions shared by national or international archival communities. Such a literature tends to be rather prescriptive, abstract, principle-based, and in same cases, very technical. These are also the characteristics of the education and training usually received by records professionals, who are rarely confronted with real situations in the course of their studies. Empirical research is overall lacking in the archival and records management discipline, and especially the moment of records creation has not yet been investigated in depth. Only few existing studies examined the components of that crucial moment (structures, functions, human agents) and their interrelationships by applying innovative conceptual frameworks (see, for instance, the use of structuration theory in [Cook 1992](#cook92), and [Upward 1997](#upward97)). However, how the new ideas affect actual practices is not described anywhere, and therefore, practitioners tend to keep on referring to traditional conceptualizations. The three persons involved in the creation of every record (i.e., author, writer and addressee); the five, clearly demarcated contexts that, as seen earlier, wrap the record as a distinct object; the six procedural steps that ideally make up all possible transactions: these are just some examples of the type of abstraction and modelling orientation (mostly coming from diplomatics) that have been dominating the records management thinking.

## The genre approach

The stream of genre research and scholarship this paper refers to is the one known as Rhetorical Genre Studies, or New Rhetoric, that emerged in the 1980s, following the publication of the article Genre as Social Action by Carolyn Miller. In her article, Miller rejects the formalism of previous approaches to genre (which were mostly interested in classifying literary texts and classic forms of rhetorical discourse) and brings in a sociological perspective by describing every-day, non-literary genres as ‘situation-based fusion of form and substance’ ([1984: 153](#miller84)). The ‘pragmatics’ of communication, that is, the individual and social action that shape and is shaped by generic forms, becomes the focus of this new North American ‘genre school’, whose theoretical framework draws on several philosophical, linguistic, sociological, psychological, cognitive, rhetorical, and cultural-historical approaches (whose most illustrious representatives are, inter alia, Wittgenstein, Bakhtin, Bourdieu, Vygotsky, Giddens, Burke), all associated with a dynamic, interactive, constructivist, as well as situated, particular, and concrete understanding of human action as a social phenomenon.

Every instantiation of a genre (whether written, spoken, or acted) comes from a history of previous interactions and shapes any response to it by anticipating its audience’s expectations. When a situation is perceived as ‘recurrent’, the author/speaker/actor and the reader/listener/spectator involved in it refer to some available ‘constellation[s] of recognizable forms’ ([Campbell and Jamieson 1978: 21](#campbell78)) and appropriate ‘rules of the game’ ([Freadman 1994: 38](#freadman94)) – i.e., the ‘decorum’ dictated by the specific circumstances in which the exchange takes place and the goals participants want to achieve ([Bazerman 1988](#bazerman88)) – in order to produce ‘typified rhetorical actions’ ([Miller 1984: 159](#miller84)). The latter is to be understood as both the text and the context of the interaction, as they co-construct and re-produce each other.

Change and unexpected turns are part of this continuous process of reproduction, as the theory of structuration ([Giddens 1984](#giddens84)) teaches us, and transformation is an intrinsic property of all genres. Even the most institutionalized genre form is the provisional result of a chain of negotiations that occurred over time, and its centripetal force (directed towards normalization and stability) always contains its opposite, that is, a centrifugal impulse (involving idiosyncrasy and resistance) that is going to overturn any of the conventional features of that genre one day ([Bakhtin 1981](#bakhtin81)). Catherine Schryer ([1993: 200](#schryer93)) characterized very well the dynamic nature of genres by defining them as ‘stabilized-for-now or stabilized-enough sites of social and ideological action’. Amy Devitt ([2009: 39](#devitt09)) went even further by suggesting that ‘[g]enres are destabilized for now and forever’, meaning that ‘genres-in-use’ are always unique, despite the fact that each instantiation of a genre shares common formal, substantive, and situational elements.

The expressions ‘genre set’ ([Devitt 1991](#devitt91); [Bazerman 1994](#bazerman94)), ‘genre repertoire’ ([Orlikowski and Yates 1994](#orlikowski94)), ‘genre system’ ([Bazerman 1994](#bazerman94); [Yates and Orlikowski 2002](#yates02)), ‘genre chain’ ([Fairclough 2003](#fairclough03)), and ‘genre ecology’ ([Spinuzzi 2003](#spinuzzi03)) have been coined to account for the various kinds of interrelationships one may observe among genres, both diachronically and synchronically. They all involve the idea of intertextuality ([Devitt 1991](#devitt91)), or interdiscursivity ([Smart 2006](#smart06)), which not only is one of the enablers of genre evolution through the influence that texts exert one another, but it also explains the practical and intellectual work that genres accomplish when they are collectively employed to orchestrate some particular activity ([Bazerman 1988](#bazerman88); [Bazerman and Prior 2004](#bazerman04); [Yates and Orlikowski 2002](#yates02); [Smart 2006](#smart06)).

‘[G]enres organize activity, texts, knowledge, and people’ ([Andersen 2008: 341](#andersen08)), and every ‘discourse community’ – which can be as large as a continent or as small as a unit or workgroup within an organization, but in any case it always is a historically situated phenomenon – appropriates specific sets of genres and establishes certain shared conventions of discourse as ways of ‘acting together’ ([Burke 1969: 21](#burke69)). Learning ‘how to participate in the actions of a community’ ([Miller 1984: 165](#miller84)) is a central aspect of our daily and professional lives. Generic forms ‘guide’ our interactions ([Miller 1984: 159](#miller84); [Devitt 2009: 30](#devitt09)), are ‘means of orientation’ ([Andersen 2008: 349](#andersen08)); and by constructing our world, they reveal culture.

In order to become full, competent members of any community we participate in, we need to acquire ‘genre knowledge’ (i.e., awareness of how a particular genre or set of genres functions, including a sense of what forms and contents are appropriate to a particular purpose in a particular situation at a particular point in time) as well as ‘procedural’ and ‘social knowledge’ ([Berkenkotter and Huckin 1993: 487](#berkenkotter93)). This foray into the areas of genre learning and social cognition – which represent substantial avenues of research within Rhetorical Genre Studies (see most of the chapters included in [Artemeva and Freedman 2008](#artemeva08)) – aims at emphasizing that knowledge, whether related to a specific professional discipline or practice, a process, a tool, or a genre-in-use, is always ‘socially constructed in local sites’ ([Smart 2006: 21](#smart06)), that is, it is produced within specific discursive conditions, made (not found) by groups of people (not by individuals) in the course of their activities.

## Bringing record management and genre studies together

Both records management and genre studies have as their primary object of study the ‘text’ as an outcome and a means of practical activity. Both recognize that the latter enables and constrains the enactment of formal (language, style, etc.) and substantive (message, content articulation, etc.) textual features. Both suggest action-based principles for classification, knowledge organization, and meaning making. ‘Form reveals function’ is indeed one of the main assumptions of diplomatics ([Duranti 1998: 133](#duranti98)). An analysis of diplomatic concepts vis-à-vis genre concepts shows that the two disciplines share many constructs. Thanks to their distinct ways of interpreting the same phenomena, they may usefully be combined to provide richer insights into the generation and transmission of records/genres, as I tried to demonstrate in my contribution to the special issue of Archival Science dedicated to genre research in archives ([Foscarini 2012](#foscarini12)).

However, despite their commonalities, the records management and the genre discipline apply different conditions for a text to be recognized as a record and a genre respectively. As we have seen, the archival approach is quite selective and only those information objects that meet pre-established requirements in terms of intrinsic properties (e.g., fixed form, stable content) and external actions (e.g., among the latter, the act of ‘setting aside’) can ‘ris[e] at the level of a record’, as the title of an article by Yeo ([2011](#yeo11)) puts it. Quite oppositely, anything could be a genre. As suggested by Miller and Shepherd ([2004](#miller04)), ‘[w]hen a type of discourse or communicative action acquires a common name within a given context or community, that’s a good sign that it’s functioning as a genre’. Written and non-written texts, official and unofficial instantiations are equal members of the infinite and ever-changing genre family. In her seminal article, Miller had already warned us that ‘the set of genres is an open class, with new members evolving, old one decaying ([1984: 153](#miller84), emphasis added) and that, as a consequence, we should refrain from reducing genre classification to ‘tiresome and useless taxonomies’ ([Conley 1979: 53](#conley79) – cited in [Miller 1984: 151](#miller84)).

In the last decades, the emergence of new kinds of information objects on the one hand, and the cognizance of the instability and volatility of digital materials on the other have been pushing the record boundaries ([Duranti and Thibodeau 2006](#duranti06); [Yeo 2011](#yeo11)). The genre approach provides compelling arguments that may support current attempts to expand the scope of the archive and records management discipline and to redefine the nature of its object of study.

The notion of genre system as ‘interrelated genres that interact with each other in specific settings’ ([Bazerman 1994: 97](#bazerman94)) is, for instance, much more inclusive and dynamic than that of file, or dossier, as applied in archival science, that is, ‘the aggregation of all the records that participate in the same affair or relate to the same event, person, place, project, or other subject’ ([InterPARES 2 Terminology Database](#interpares)). While the file of a business meeting usually only comprises formal, significant, mostly written evidence of its preparation, execution, and outcome (e.g., invitations, notifications, agendas, copies of presented documentation, minutes, summary proceedings), taking a genre system perspective would involve looking at the meeting as a ‘complete communicative interaction’ ([Artemeva 2008: 27](#artemeva08)) as well as revealing its dialectical and rhetorical dimensions. This approach would include considering the specific ways in which facts/acts are presented (e.g., by investigating the explicit and implicit reasons for preferring PowerPoint to other presentation aids, as Yates and Orlikowsky ([2007](#yates07)) did) and exploring the ‘intersubjective relations’ ([Smart 2006](#smart06)) that individuals produce, reproduce, and challenge through the meeting.

On the other hand, the special kind of primary, necessary, and incremental link that archival science recognizes to all records that belong together because they originated during, and by virtue of, the same activity – known as ‘archival bond’ ([Duranti 1997a](#duranti97a)) – may contribute to explain why only ‘some typical sequence (or limited set of acceptable sequences)’ of genres is considered ‘appropriate’ within any genre system ([Yates and Orlikowski 2002: 15](#yates02)).

Being open to innovations is a message that is certainly not typical of records management, which rather tends to align itself with the centripetal forces that characterize all organizations. As genre scholar Clay Spinuzzi ([2003](#spinuzzi03)) exemplified through his study of the changes made to a traffic control database over time, when official systems are perceived as being rigid or inefficient, people, whether deliberately or unintentionally, start developing ‘micro-breakdowns’, or unauthorized work practices that reflect their unarticulated needs, and that in the long run, might alter the system features permanently. Paying attention to emerging, unofficial documentary forms, local innovations, apparently unacceptable deviations from the norm, especially those arising ‘from the bottom’, would be of enormous benefit to records managers, as it would sharpen their ‘antennas’ and make their interventions in the life of the organization more meaningful and effective ([Foscarini 2010](#foscarini10)).

Taking a socially and historically situated approach is another lesson that records management may learn from genre studies. Examining local practices of record creation and use, as well as observing the attitudes and values towards information – i.e., the ‘information cultures’ ([Oliver 2011](#oliver11)) – of those who are expert in areas of human endeavour other than the records management one, would once again support the creation of more appropriate and flexible tools, in line with the specific written and unwritten rules of each workplace. The notion of a ‘ceremony’, introduced by Anne Freadman ([1994: 40](#freadman94)) to describe all kinds of conventions for setting the place, time, mechanisms, participants, and any other aspects involved in a communicative exchange (which she compares to a tennis match), appears more encompassing and evocative than the mono-dimensional view of the ‘regulatory framework’ offered by the records management literature.

The exploration of ‘discourse communities’ and their actual ways of producing and disseminating knowledge would not only enable the adoption of more suitable practices but would also enhance records management from a theoretical and a methodological perspective. Genre research is often conducted through field studies that make use of ethnographic methods to unearth tacit practices and local innovations that would otherwise remain invisible ([Spinuzzi 2003](#spinuzzi03), [Smart 2006](#smart06)). An ‘ethnomethodology of records’ – as much as the ‘ethnomethodology of genres’ advocated by Miller and Shepherd ([2004](#miller04)) – may for instance allow for the development of multiple definitions of a record and of recordkeeping practices, based on the perceptions of different communities of records creators and users.

The genre lens may help illuminate ideological and political factors, conflicting motives, and the role of each participant in a records creating process, all aspects that have so far received scant attention by the archival community. With regard to participants, it has already been stressed elsewhere that the archival world is primarily concerned with ‘acts of authorship’ ([Brothman 2011: 298](#brothman11)) and that, following the diplomatic approach, the ‘persons’ involved in a documentation action (i.e., author, writer, and addressee) tend to be conceived as abstract entities, deprived of both agency and social dimension ([Foscarini 2012](#foscarini12)). Bakhtin’s ([1986](#bakhtin86)) idea of ‘addressivity’, that is, the relational quality characterizing any ‘concrete utterance’ or ‘speech genre’, may prove useful in this respect. Addressivity – not differently from the notion of ‘uptake’ introduced by subsequent genre scholars ([Freadman 1994](#freadman94), [2002](#freadman02); [Bawarshi 2000](#bawarshi00)) – explains the structuring of discourse and social systems as a dialogic relationship where the parts involved interact and construct one another, and where the recipient/listener/reader acquires a central role in the communicative action.

Placing a special emphasis on the ‘social context’ as an integral component of the activity generating the record – and of the record itself – does not simply mean adding another layer to the five contexts already identified by the archival discipline. The separation of the record from the situation that represents its raison d’être is an artificial one and should not prevent us from considering all contexts as mutually affecting one another and all made of the same social, cultural and political fabric that is inherent in any human action.

Records, not differently from genres, are ‘cultural artefacts’ ([Miller 1984: 163](#miller84), [1994: 57](#miller94)) and as such, they are sites of continuous social, cultural, and ideological negotiation. Ignoring or denying such fundamental aspect of the nature of a record – as the traditional archival doctrine has been doing by stressing the naturalness and impartiality of the process of records creation – de facto decontextualizes and idealizes the record, and does not allow us to see it as a particular, situated ‘form of life’ (an expression that Miller ([1984: 160](#miller84)) borrowed from Wittgenstein).

The model of recordkeeping depicted in the current literature neither exists in any actual manifestations of records-related activities nor helps reveal the pragmatics of communication. We may look at it as the Saussurian langue, ‘a self-sufficient system of signs’ ([Bourdieu 1991: 4](#bourdieu91)) that no real language community uses. Bourdieu brought the focus of linguistics back to the parole, that is, ‘the situated realization of the system by particular speakers’ ([1991, 5](#bourdieu91)); similarly, we should turn our attention to actual organizational practices and let the social and cultural patterns that give significance to those practices emerge.

## Conclusion

Rhetorical Genre Studies provides a powerful theoretical framework for investigating records as typified social action and uncovering the evolving cultural, ideological, and political circumstances that shape and are shaped by our acts of records creation and use. This stream of genre research and scholarship focuses on text-context interrelationships and builds upon a rich set of conceptualizations of the social world and the interactions occurring within it (from structuration and activity theory to distributed cognition and situated learning). As such, it appears particularly suitable to be connected to the ‘discipline of the record’. This paper has hopefully managed to demonstrate that opening the latter to genre ideas does not imply that the specific views of the record and the context characterizing archival science would be diluted or its foundational principles undermined, as some archival scholars might fear. Suggesting that records are not natural phenomena, ‘[t]heir objectified forms … [being] the artful constructions of text-based methodologies and the practices of formal organization’ ([Smith 1987: 151](#smith87), cited in [Schryer 1993: 205](#schryer93)) does not diminishes or cloud the evidential value of records and their ability to serve accountability purposes. By arguing for a more inclusive notion of a record, this paper wants to encourage the archival community to embrace an expanded idea of evidence as well. Rethinking the concepts of naturalness, impartiality, authenticity, uniqueness, and interrelatedness through the genre lens is expected to enhance our understanding of what records are, ‘what they do and how they do it’ – by paraphrasing the title of a book by Bazerman and Prior ([2004](#bazerman04)).

Records, as much as genres, organize our world. Their structuring function concerns not only documentary aggregations but also the actions, agents, and situations involved in them. At the same time, our world shapes and gives meaning to the records, as their form and substance reflect the socio-historical conditions in which we live, and those we come from. The evolutionary perspective taken by many studies in genre ([Bazerman 1988](#bazerman88), [1994](#bazerman94); [Yates and Orlikowski 1992](#yates92), [2007](#yates07); [Spinuzzi 2003](#spinuzzi03); [Miller and Shepherd 2004](#miller04), [2009](#miller09)) combined with their situated and dialogic (or polyphonic, as Bakhtin ([1981](#bakhtin81)) would say) understanding of social interactions appears to be particularly valuable to the area of records management, which may be seen as having lost touch with the organizational reality.

The lack of empirical research is a hindrance to the development of explanatory and exploratory, descriptive theory, one that is concerned with actual human action in the real world. Eastwood ([1992](#eastwood92), [1994](#eastwood94)), among other archival scholars, raised the issue of the limitations inherent in a discipline that appears to be dominated by normative, programmatic theory. One way of promoting field studies, including the application of interpretivist methods, in archives and records management would be through the establishment of collaborative research projects with the genre community. This paper had among its purposes that of inviting genre scholars to take notice of issues that are central to the archival discourse (e.g., accountability, authenticity, information as evidence, necessary links among records) and to try to imagine possible synergies. The understanding of our ‘acting together’ – much of which involves a process of ‘inscription’, that is, the ‘rendering of an event or object in documentary form’ ([Schryer 1993: 205](#schryer93)) – would certainly benefit from a constructive dialogue among all disciplines interested in our worlds of words.

</section>

<section>

## References

<ul>
<li id="andersen08">Andersen, J. (2008). The concept of genre in information studies. <em>Annual Review of Information Science and Technology</em> <strong>42</strong>(1), 339-367
</li>
<li id="artemeva">Artemeva, N. (2008). Approaches to learning genres: a bibliographical essay. In: N. Artemeva and A. Freedman (Eds.), <em>Rhetorical genre studies and beyond</em> (pp. 9-99). Winnipeg, MA: Inkshed Publications
</li>
<li id="artemeva08">Artemeva, N. and Freedman, A. (Eds.). (2008). Rhetorical genre studies and beyond. <em>Winnipeg, MA: Inkshed Publications</em>
</li>
<li id="bakhtin81">Bakhtin, M.M. (1981). The dialogic imagination: four essays. M. Holquist, (Ed.); C. Emerson and M. Holquist (Trans.). <em>Austin, TX: University of Texas Press</em>
</li>
<li id="bakhtin86">Bakhtin, M.M. (1986). The problem of speech genres. In C. Emerson and M. Holquist (Eds.), Speech genres and other late essays (pp. 60-102). <em>Austin, TX: University of Texas Press</em>
</li>
<li id="bawarshi00">Bawarshi, A. (2000). The genre function. <em>College English</em> 62(3): 335-360
</li>
<li id="bazerman88">Bazerman, C. (1988). Shaping written knowledge: the genre and activity of the experimental article in science. <em>Madison, WI: University of Wisconsin Press</em>
</li>
<li id="bazerman94">Bazerman, C. (1994). Systems of genres and the enactment of social intentions. In A. Freedman and P. Medway (Eds.), <em>Genre and the new rhetoric</em> (pp. 79-101). London: Taylor and Francis
</li>
<li id="bazerman04">Bazerman, C. and Prior, P. (2004). What writing does and how it does it. An introduction to analyzing texts and textual practices. <em>Mahwah, NJ, London: Laurence Erlbaum Associates Publishers</em>
</li>
<li id="bearman93">Bearman, D. (1993). Record-keeping systems. Archivaria 36, 16-36.
</li>
<li id="berkenkotter93">Berkenkotter, C. and Huckin, T.N. (1993). Rethinking genre from a sociocognitive perspective. <em>Written Communication</em> <strong>10</strong>(4), 475-509
</li>
<li id="bourdieu91">Bourdieu, P. (1991). Language and symbolic power. <em>Cambridge, UK: Polity Press</em>
</li>
<li id="brothman02">Brothman, B. (2002). Afterglow: conceptions of record and evidence in archival discourse. <em>Archival Science</em> <strong>2</strong> (3-4), 311-342
</li>
<li id="brothman11">Brothman, B. (2011). Designs for records and recordkeeping: visual presentation in diplomatics, the record continuum, and documentation strategy. In T. Cook (Ed.), <em>Documenting society and institutions. Essays in Honor of Helen Willa Samuels</em> (pp. 279-316). Chicago, IL: Society of American Archivists
</li>
<li id="burke69">Burke, K. (1969). A rhetoric of motives. <em>Berkeley, CA: University of California Press</em>
</li>
<li id="campbell78">Campbell, K.K. and Jamieson, K.H. (1978). Form and genre in rhetorical criticism: an introduction. In K.K. Campbell and K.H. Jamieson (Eds.) <em>Form and genre: shaping rhetorical action</em> (pp. 9-32). Falls Church, VA: Speech Communication Association
</li>
<li id="cencetti70">Cencetti, G. (1970). Sull’archivio come ‘universitas rerum’. In G. Cencetti, <em>Scritti archivistici</em> (pp. 47-55). Rome
</li>
<li id="conley79">Conley, T.M. (1979). Ancient rhetoric and modern genre criticism. <em>Communication Quarterly</em> <strong>27</strong>(4), 47-53
</li>
<li id="cook92">Cook, T. (1992). Mind over matter: towards a new theory of archival appraisal. In B.L. Craig (Ed.) <em>The archival imagination: essays in honour of Hugh A. Taylor</em> (pp. 38-70). Ottawa, ON: Association of Canadian Archivists
</li>
<li id="cook94">Cook, T. (1994). Electronic records, paper minds: the revolution in information management and archives in the post-custodial and post-modernist era. <em>Archives and Manuscripts</em> <strong>22</strong>(2), 300-328
</li>
<li id="cook01">Cook, T. (2001). Fashionable nonsense or professional rebirth: postmodernism and the practice of archives. <em>Archivaria</em> 51, 14-35
</li>
<li id="devitt91">Devitt, A.J. (1991). Intertextuality in tax accounting: generic, referential, and functional. In C. Bazerman and J. Paradis (Eds.), <em>Textual dynamic of the professions</em> (pp. 336-357). Madison, WI: University of Wisconsin Press
</li>
<li id="devitt09">Devitt, A.J. (2009). Re-fusing form in genre study. In G. Giltrow and D. Stein (Eds.), Genres in the Internet: issues in the theory of genre (pp. 27-47). Amsterdam, Philadelphia: John Benjamins Publishing Company
</li>
<li id="duranti97">Duranti, L. (1997a). The archival bond. <em>Archives and Museum Informatics</em> <strong>11</strong>, 213-218
</li>
<li>Duranti, L. (1997b). Archival science. In A. Kent (Ed.), <em>Encyclopedia of library and information science</em> <strong>59</strong>(22) (pp. 1-19). New York, NY: Marcel Dekker
</li>
<li id="duranti98">Duranti, L. (1998). Diplomatics. New uses for an old science. <em>Lanham, MD, London: Society of American Archivists and Association of Canadian Archivists in association with the Scarecrow Press Inc</em>
</li>
<li id="duranti10">Duranti, L. (2010). Concepts and principles for the management of electronic records, or records management theory is archival diplomatics. <em>Records Management Journal</em> <strong>20</strong>(1), 78-95
</li>
<li id="duranti02">Duranti, L., Eastwood, T. and MacNeil, H. (2002). Preservation of the integrity of electronic records. <em>Dordrecht, Boston, London: Kluwer Academic Publishers</em>
</li>
<li id="duranti08">Duranti, L. and Preston, R. (2008). InterPARES 2: interactive, dynamic and experiential records. <em>Padova, IT: Associazione Nazionale Archivistica Italiana</em>
</li>
<li id="duranti06">Duranti, L. and Thibodeau, K. (2006). The concept of record in interactive, experiential and dynamic environments. The view of InterPARES. <em>Archival Science</em> <strong>6</strong>(1), 13-68
</li>
<li id="eastwood92">Eastwood, T. (1992). Towards a social theory of appraisal. B.L. Craig (Ed.) <em>The archival imagination: essays in honour of Hugh A. Taylor</em> (pp. 71-89). Ottawa, ON: Association of Canadian Archivists
</li>
<li id="eastwood94">Eastwood, T. (1994). What is archival theory and why is it important? <em>Archivaria</em> <strong>37</strong>, 122-130
</li>
<li id="eastwood">Eastwood, T. (2010). A contested realm: the nature of archives and the orientation of archival science. In T. Eastwood and H. MacNeil (Eds.), <em>Currents of archival thinking</em> (pp. 3-21). Santa Barbara, CA: Libraries Unlimited
</li>
<li id="eastwood10">Eastwood, T. and MacNeil, H. (Eds.) (2010). Currents of archival thinking. <em>Santa Barbara, CA: Libraries Unlimited</em>
</li>
<li id="fairclough03">Fairclough, N. (2003). Analyzing discourse: textual analysis for social research. <em>London: Routledge</em>
</li>
<li id="foscarini">Foscarini, F. (2010). Understanding the context of records creation and use: ‘hard’ versus ‘soft’ approaches to records management. <em>Archival Science</em> <strong>10</strong>(4), 389-407
</li>
<li id="foscarini12">Foscarini, F. (2012). Diplomatics and genre theory as complementary approaches. <em>Archival Science</em> <strong>12</strong>(4), 389-409
</li>
<li id="foscarini10">Foscarini, F., Kim, Y., Lee, C.A., Mehler, A., Oliver, G. and Ross, S. (2010). On the notion of genre in digital preservation. In J.P. Chanod<em>et al.</em> (Eds.), <em>Proceedings of the Dagstuhl seminar 10291 on automation in digital preservation</em>
</li>
<li id="freadman94">Freadman, A. (1994). Anyone for tennis? In A. Freedman and P. Medway (Eds.) <em>Genre and the new rhetoric</em> (pp. 43-66). London: Taylor and Francis
</li>
<li id="freadman02">Freadman, A. (2002). Uptake. In R. Coe<em>et al.</em> (Eds.), <em>The rhetoric and ideology of genre: strategies for stability and change</em> (pp. 39-53). Cresskill: Hampton Press, Inc.
</li>
<li id="giddens84">Giddens, A. (1984). The constitution of society: outline of the theory of structuration. <em>Berkeley, CA: University of California Press</em>
</li>
<li id="harris98">Harris, V. (1998). Postmodernism and archival appraisal: seven theses. <em>South African Archives Journal</em> <strong>40</strong>, 48-50
</li>
<li id="international01">International Organization for Standardization (2001). ISO 15489-1:2001 Information and documentation – Records management. Part 1: general
</li>
<li id="interpares">InterPARES 2 Terminology Database (nd). Retrieved 25 February, 2013 from http://www.interpares.org/ip2/ip2_terminology_db.cfm
</li>
<li id="jenkinson66">Jenkinson, H. (1966). A manual of archive administration. (2nd ed.). <em>London: Percy Lund, Humphries and Co.</em>
</li>
<li id="mckemmish05">McKemmish, S., Piggott, M., Reed, B. and Upward, F. (Eds.). (2005). Archives: recordkeeping in society. <em>Wagga Wagga, NSW: Charles Sturt University, Centre for Information Studies</em>
</li>
<li id="miller84">Miller, C.R. (1984). Genre as social action. <em>Quarterly Journal of Speech</em> <strong>70</strong>(2), 151-167
</li>
<li id="miller94">Miller, C.R. (1994). Rhetorical community: the cultural basis of genre. In A. Freedman and P. Medway (Eds.), <em>Genre and the new rhetoric</em> (pp. 57-66)
</li>
<li id="miller04">Miller, C.R. and Shepherd, D. (2004). Blogging as social action: a genre analysis of the weblog. Into the blogosphere: rhetoric, community, and culture of weblogs. Retrieved 25 February, 2013 from http://blog.lib.umn.edu/blogosphere/blogging_as_social_action.html
</li>
<li id="miller09">Miller, C.R. and Shepherd, D. (2009). Questions for genre theory from the blogosphere. In G. Giltrow and D. Stein (Eds.), <em>Genres in the Internet: issues in the theory of genre</em> (pp. 263-290). Amsterdam, Philadelphia: John Benjamins Publishing Company
</li>
<li id="oliver">Oliver, G. (2011). Organisational culture for information managers. <em>Oxford: Chandos Publishing</em>
</li>
<li id="oliver11">Oliver, G. and Foscarini, F. (2011). Corporate recordkeeping: new challenges for digital preservation. <em>Proceedings of the 8th International Conference on Preservation of Digital Objects (iPRES) 2011</em>, Singapore, November 1-4, 2011, 260-261
</li>
<li id="oliver08">Oliver, G., Kim, Y. and Ross, S. (2008). Documentary genre and digital recordkeeping: red herring or a way forward? <em>Archival Science</em> <strong>8</strong>(4), 295-305.
</li>
<li id="orlikowski94">Orlikowski, W.J. and Yates, J. (1994). Genre repertoire: the structuring of communicative practices in organizations. <em>Administrative Science Quarterly</em> <strong>39</strong>(4), 541-574
</li>
<li id="schellenberg03">Schellenberg, T.R. (2003). Modern archives: principles and techniques. <em>Chicago, IL: The Society of American Archivists</em>
</li>
<li id="schryer93">Schryer, C.F. (1993). Records as genre. <em>Written Communication</em> <strong>10</strong>(2), 200-234
</li>
<li id="shepherd03">Shepherd, E. and Yeo, G. (2003). Managing records: a handbook of principles and practice. <em>London: Facet Publishing</em>
</li>
<li id="smart06">Smart, G. (2006). Writing the economy. Activity, genre and technology in the world of banking. <em>London, Oakville: Equinox</em>
</li>
<li id="smith87">Smith, D.E. (1987). The everyday world as problematic: a feminist sociology. <em>Toronto: University of Toronto Press</em>
</li>
<li id="spinuzzi03">Spinuzzi, C. (2003). Tracing genres through organizations: a sociocultural approach to information design. <em>Cambridge, MA: MIT Press</em>
</li>
<li id="upward97">Upward, F. (1997). Structuring the records continuum. Part two: structuration theory and recordkeeping. <em>Archives and Manuscripts</em> <strong>25</strong>(1), 10-35
</li>
<li id="yates92">Yates, J. and Orlikowski, W.J. (1992). Genres of organizational communication: a structuractional approach to studying communication and media. <em>Academy of Management Review</em> <strong>17</strong>(2), 299-326
</li>
<li id="yates02">Yates, J. and Orlikowski, W.J. (2002). Genre systems: structuring interaction through communicative norms. <em>Journal of Business Communication</em> <strong>39</strong>(1), 13-35
</li>
<li id="yates07">Yates, J. and Orlikowski, W.J. (2007). The PowerPoint presentation and its corollaries: how genres shape communicative action in organizations. In M. Zachry and C. Thralls (Eds.), <em>Communicative practices in workplaces and the professions. Cultural perspectives on the regulation of discourse and organizations</em> (pp. 67-91). Amityville: Baywood’s Publishing Company Inc.
</li>
<li id="yeo07">Yeo, G. (2007). Concepts of record (1): evidence, information, and persistent representations. <em>American Archivist</em> <strong>70</strong>(2), 315-343
</li>
<li id="yeo08">Yeo, G. (2008). Concepts of record (2): prototypes and boundary objects. <em>American Archivist</em> <strong>71</strong>(1), 118-143
</li>
<li id="yeo11">Yeo, G. (2011). Rising to the level of a record? Some thoughts on records and documents. <em>Records Management Journal</em> <strong>21</strong>(1), 8-27
</li>
</ul>

</section>

</article>