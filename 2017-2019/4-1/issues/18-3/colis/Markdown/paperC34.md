<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Different traditions in the study of disciplinarity in science – science and technology studies, library and information science and scientometrics

#### [Staša Milojevi&cacute;](mailto:smilojev@indiana.edu)  
Indiana University, 1320 E. 10th Street, LI Room 019, Bloomington, IN 47405-3907, USA

#### Abstract

> **Introduction**. Disciplinarity and other forms of differentiation in science have long been studied in the fields of science and technology studies, information science and scientometrics. However, it is not obvious whether these fields are building on each other’s findings.  
> **Methods**. An analysis is made of 609 articles on disciplinarity selected through a combination of automatic and manual methods published from 2000 to 2013.  
> **Analysis**. Scientometric methods are used to determine how the different fields approach the study of disciplinarity. It seeks to establish how cognitively similar the approaches are, by exploring their use of knowledge base (derived from reference lists) and of the cognitive concepts used in the titles of works.  
> **Results**. The three fields have very distinct communities of practitioners, but are similar cognitively. The three fields are using as their knowledge base similar authors, although not necessarily the same work by those authors. Both scientometrics and information science draw from the science and technology studies literature, which is not reciprocating. The similarity of terminology indicates that the three fields are studying the same objects, but other indicators suggest that they are actually interested in quite different aspects of those objects.

<section>

## Introduction

This study examines recent (2000-2013) writings on the disciplinarity and other forms of differentiation of science, published within the fields of science and technology studies, library and information science, and scientometrics, with the goal of determining the commonalities and the differences in the approaches between these three research traditions.

Studies of the various forms of differentiation in science in general and disciplines in particular have long been the focus of philosophy, history, and sociology of science. With the advent of a new, interdisciplinary field of science and technology studies, the emphasis shifted to the study of novel ways of differentiating science, such as the epistemic communities, communities of practice, etc. This change in focus was more in line with the newly found interest in ethnographic methods and the historical, sociological, and anthropological approaches to scientific practice.

Within library and information science the proponents of the domain analysis ([Hjørland, 2002](#hjorland02); [Hjørland and Albrechtsen, 1995](#)) have been the most vocal in expressing the need to conduct larger studies of scientific disciplines that would combine empirical with theoretical and philosophical analyses, drawing primarily from the fields of philosophy and history of science. They claim that such knowledge will both enhance our understanding of information and create better information systems.

Since its beginnings as the quantitative aspect of science and technology studies in the 1950s and 1960s ([Spiegel-Rósing and Price, 1977](#spiegel77)), the field of scientometrics has been making significant contributions, first to studying the invisible colleges and specializations, and then, with the advancement of technical capabilities, the study of larger units such as disciplines or groups of disciplines. Scientometrics warrants to be treated separately due to its ambiguous position. Despite its roots in science and technology studies ([Leydesdorff and Van den Besselaar 1997](#leydesdorff97); [Van den Besselaar 2001](#besselaar01)), the field became closer to library and information science over the years, which led a number of researchers to consider scientometrics part of it (e.g., [Åstróm 2002](#astrom02), [Van den Besselaar and Heimeriks 2006](#besselaar06)). More recently, however, a number of studies pointed out that scientometrics is a field in its own right, standing alongside and not within library and information science ([Janssens, _et al._ 2006](#janssens06); [Milojevi&cacute;, _et al._ 2011](#milojevic11)). However, scientometrics researchers use both scientometrics and library and information science venues (especially _Journal of the American Society for Information Science and Technology)_, to disseminate their research results, which makes using journals to delineate these fields difficult and more advanced methods are required to address this problem ([Milojevi&cacute; and Leydesdorff 2013](#milojevic13)).

Concepts introduced by various studies of science, such as paradigm, invisible college, specialty, discipline, multi-, inter- and trans-discipline, are the major forms of differentiation studied by all three communities. Below are the definitions of these major concepts.

### Paradigm

Kuhn ([1962/1996](#kuhn62)) introduced the concept of paradigm to the philosophy and history of science in his study of scientific revolutions. However, as pointed by Masterman ([1970](#masterman70)), Kuhn did not provide its definition, but used it in ways that led to twenty-one different meanings. In general, paradigm can be understood as a set of categories, theories, and procedures learnt in connection with concrete examples, accepted by the given scientific community and applied to deal with problems in concrete situations.

### Invisible college

Originally used by Robert Boyle in the 17th century to designate the precursor of what became the Royal Society, the term invisible college was reintroduced in the 1960s, when Price ([1963](#price63)) used it to refer to informal, close-knit groups of one hundred or fewer scientists who work on similar problems and who regularly share information with one another. Crane ([1972](#crane72)) considered invisible colleges to be the key units of the growth of science, emphasizing their role as communication networks. The more recent definitions (e.g., [Lievrouw 1990](#lievrouw90); [Zuccala 2006](#zuccala06)) emphasize the importance of both formal and informal communication and shared research interests and goals.

### Specialty

Ever since the two pioneering studies of Ben-David and Collins ([1966](#ben66)) and Price ([1963](#price63)), the problem of the development of new scientific specialties occupied the attention of sociologists and historians of science in the 1970s ([Chubin, 1976](#chubin76); [Edge and Mulkay, 1976](#edge76); [Law, 1976](#law76); [Mullins, 1972](#mullins72)), so much so that the sociological study of scientific specialization itself became a sociological specialty ([Zuckerman, 1988](#zuckerman88)). In general, specialty denotes a loose grouping of scientists, smaller than a discipline or a field, working on similar problems. Members of specialties are known to each other or know of one another’s work, more so than of research in their disciplines as a whole. They identify themselves, or are identified by others, as a socially and cognitively defined entity. Specialties have been variously characterized as _'microenvironments for research'_ ([Hagstrom 1970](#hagstrom70)), _'building blocks of science'_ ([Small and Griffith, 1974](#small74)), _'cognitive and social entities'_ ([Cole and Zuckerman, 1975](#cole75)), and _'smaller intellectual units nested within and between disciplines comprising the research domain'_ ([Chubin, 1976](#chubin76)).

### Discipline

A discipline can be considered _'the primary unit of internal differentiation of the modern system of science'_ ([Stichweh, 1992, p. 3](#stichweh92)). The term discipline is ambiguous since it refers _'both to a form of instruction to which one submits, and to a means of controlling behavior'_ ([Golinski, 2005: 69](#golinski05)). The work of Michel Foucault, whose influence is reflected in much of the recent work on disciplines, offers potentially illuminating theoretical perspectives as an alternative to the prevalent model of professionalization ([Goldstein, 1984](#goldstein84)). Foucault proposed that disciplines comprised one of the systems for the control and delimitations of a discourse setting ([Foucault, 1972](#foucault72); [Foucault, 1978/1995](#foucault78)). Bowker ([2005](#bowker05)) emphasized the importance of history in his definition of a discipline as _'a field of research that has a commonly accepted origin myth, ritually incanted in the first chapter of textbooks and the opening lectures of a survey course'_ (p. 92). Disciplines also have different socio-cognitive structures _'manifested in differing material practices, communication behaviors, and publishing regimes'_ ([Cronin, 2005, p. 3](#cronin05)).

### Multidiscipline, interdiscipline and transdiscipline

The terms multidiscipline, interdiscipline, and transdiscipline have been used to describe research activities, problems, institutions, teachings, or bodies of knowledge, each with an input from at least two scientific disciplines. Although there is still some confusion about the usage of the terms, there is an agreement that multidisciplinary describes a rather loose, additive, or preliminary relation between the involved disciplines, whereas interdisciplinary requires stronger ties, overlap, or integration. Multidisciplinarity often emerges as a spontaneous answer to problem-focused projects. In some models, multidisciplinarity is a preliminary step toward interdisciplinarity, which can go as far as a creation of a new hybrid discipline. Transdisciplinarity is a concept used to describe a state of research or knowledge that goes beyond disciplinary boundaries, with continuous input from various disciplines, but without any inclination to consolidate into a new hybrid discipline ([Klein, 1990](#klein90); [Palmer, 2001](#palmer01)).

## Methods

This paper uses scientometric methods to study how science and technology studies, library and information science, and scientometrics approach the study of science. I primarily focus on establishing how similar the approaches are from the cognitive standpoint, by exploring their use of knowledge base (as derived from the lists of references) and of the cognitive concepts (as used in the titles of works). I also explore the level of overlap between the contributors in the three traditions.

The data for the study were collected in a three-tiered procedure. The initial set of seed data consisted of articles from five relevant journals published between 2000 and 2007: _Social Studies of Science_ and _Social Science Information_ (as representative of science and technology studies), _Scientometrics_ (as representative of scientometrics), and _Journal of the American Society for Information Science and Technology_ and _Information Processing and Management_ (as representative of information science). Titles, keywords, abstracts, and full text of these articles were examined in order to identify 58 articles that primarily dealt with the nature, structure, or development of a field, specialty, discipline, domain, paradigm, or an invisible college. These articles were then analysed to identify the most common but relevant title words to be used in a large-scale (not restricted to just five journals) search of relevant articles using Thomson Reuters’ Web of Science database. The following keywords were used: _disciplin*, interdisciplin*, multidisciplin*, transdisciplin*, paradigm*, specialization, research front*, scientific field*, scientific research*, emerging field*, epistemic communit*, knowledge domain*, research field*, emerging specialt*, invisible college, research group*_ (* denotes wildcard endings). The Web of Science searches (covering the period from 2000 to 2013) were conducted on relevant subject categories: _history, history & philosophy of science, information science & library science, philosophy, social sciences interdisciplinary, and sociology._ These searches resulted in 3,488 articles. Each bibliographic record was examined for the appropriateness for the inclusion in the dataset. All articles that appeared in subject categories other than information science & library science were classified as science and technology studies. All of the articles published in journal Scientometrics, a specialized journal exclusively devoted to the quantitative studies of science, were classified as scientometrics. Following the procedure developed in Milojevi&cacute; and Leydesdorff ([2013](#milojevic13)) I divided all of the remaining articles that appeared in subject category information science & library science, into either library and information science or scientometrics. Namely, all of the articles that had at least one reference to journal Scientometrics and those that contained one of the following five frequent scientometrics-specific words or two prefixes in the title: _citation_, _bibliometric_, _scientometric_, _mapping_, or _cite_ as well as the prefixes: _h -_ or _co-_, were classified as scientometrics. All of the other articles from this group not meeting the above conditions were classified as library and information science. Furthermore, all of the articles that Web of Science classified as library and information science but actually belonged to the field of Management Information Systems were manually excluded from the lists. This resulted in 609 papers total, of which 394 were science and technology studies articles, 82 library and information science articles, and 133 scientometrics articles. science and technology studies articles came from 226 different journals. The top 20 are listed in Table 1 and they account for 31% of all science and technology studies articles. library and information science articles came from 40 different journals. The top 20 are listed in Table 1 and they account for 75% of all library and information science articles. scientometrics articles came from 20 different journals listed in Table 1\. Given the nature of these fields, it is not surprising that science and technology studies has the highest scatter of literature.

</section>

<table><caption>

Table 1\. The list of top 20 journals in science and technology studies, library and information science, and scientometrics and the number of articles published in each.</caption>

<tbody>

<tr>

<th>Rank</th>

<th>Science and technology studies journal titles</th>

<th>No. of articles</th>

<th>Library and information science journal titles</th>

<th>No. of articles</th>

<th>Scientometrics journal titles</th>

<th>No. of articles</th>

</tr>

<tr>

<td>1</td>

<td>ISIS</td>

<td>15</td>

<td>Journal of the American Society for Information Science and Technology</td>

<td>8</td>

<td>Scientometrics</td>

<td>66</td>

</tr>

<tr>

<td>2</td>

<td>Interdisciplinary Science Reviews</td>

<td>11</td>

<td>Information Research: an international electronic journal</td>

<td>6</td>

<td>Journal of the American Society for Information Science and Technology</td>

<td>25</td>

</tr>

<tr>

<td>3</td>

<td>Sotsiologicheskie Issledovaniya</td>

<td>9</td>

<td>Journal of Documentation</td>

<td>6</td>

<td>Research Evaluation</td>

<td>10</td>

</tr>

<tr>

<td>4</td>

<td>Social Studies of Science</td>

<td>8</td>

<td>Research Evaluation</td>

<td>5</td>

<td>Journal of Informetrics</td>

<td>7</td>

</tr>

<tr>

<td>5</td>

<td>Voprosy Filosofii</td>

<td>8</td>

<td>Information Society</td>

<td>5</td>

<td>Information Processing & Management</td>

<td>4</td>

</tr>

<tr>

<td>6</td>

<td>American Journal of Sociology</td>

<td>7</td>

<td>Journal of the American Medical Informatics Association</td>

<td>4</td>

<td>Journal of Information Science</td>

<td>3</td>

</tr>

<tr>

<td>7</td>

<td>Contemporary Sociology - a Journal of Reviews</td>

<td>7</td>

<td>Perspectivas em Ciencia da Informação</td>

<td>4</td>

<td>Annual Review of Information Science and Technology</td>

<td>2</td>

</tr>

<tr>

<td>8</td>

<td>Systems Research and Behavioral Science</td>

<td>6</td>

<td>Aslib Proceedings</td>

<td>3</td>

<td>Revista Espanola de Documentacion Cientifica</td>

<td>2</td>

</tr>

<tr>

<td>9</td>

<td>Historische Zeitschrift</td>

<td>6</td>

<td>Knowledge Organization</td>

<td>3</td>

<td>Journal of Documentation</td>

<td>2</td>

</tr>

<tr>

<td>10</td>

<td>Studies in History and Philosophy of Science</td>

<td>5</td>

<td>Canadian Journal of Information and Library Science-Revue Canadienne des Sciences de L'Information et de Bibliotheconomie</td>

<td>3</td>

<td>Journal of Knowledge Management</td>

<td>1</td>

</tr>

<tr>

<td>11</td>

<td>Social Science Information sur Les Sciences Sociales</td>

<td>5</td>

<td>Investigacion Bibliotecologica</td>

<td>2</td>

<td>Online Information Review</td>

<td>1</td>

</tr>

<tr>

<td>12</td>

<td>Innovation-The European Journal of Social Science Research</td>

<td>5</td>

<td>Libraries and Culture</td>

<td>2</td>

<td>Learned Publishing</td>

<td>1</td>

</tr>

<tr>

<td>13</td>

<td>British Journal for the History of Science</td>

<td>4</td>

<td>Law Library Journal</td>

<td>2</td>

<td>Knowledge Organization</td>

<td>1</td>

</tr>

<tr>

<td>14</td>

<td>Minerva</td>

<td>4</td>

<td>Journal of Librarianship and Information Science</td>

<td>2</td>

<td>Science and Engineering Ethics</td>

<td>1</td>

</tr>

<tr>

<td>15</td>

<td>Journal of the History of Biology</td>

<td>4</td>

<td>Advances in Library Administration and Organization, Vol. 25</td>

<td>1</td>

<td>Information Research: an international electronic journal</td>

<td>1</td>

</tr>

<tr>

<td>16</td>

<td>Canadian Journal of Sociology-Cahiers Canadiens de Sociologie</td>

<td>4</td>

<td>Iformação & Sociedade-Estudos</td>

<td>1</td>

<td>European Journal of Information Systems</td>

<td>1</td>

</tr>

<tr>

<td>17</td>

<td>Scientist</td>

<td>4</td>

<td>Transinformação</td>

<td>1</td>

<td>Advances in Library Administration and Organization, Vol. 24</td>

<td>1</td>

</tr>

<tr>

<td>18</td>

<td>Philosophy as a Humanistic Discipline</td>

<td>4</td>

<td>Portal-Libraries and the Academy</td>

<td>1</td>

<td>Journal of Academic Librarianship</td>

<td>1</td>

</tr>

<tr>

<td>19</td>

<td>Sociological Research Online</td>

<td>3</td>

<td>Impact of Electronic Publishing: the Future for Publishers and Librarians</td>

<td>1</td>

<td>Serials Librarian</td>

<td>1</td>

</tr>

<tr>

<td>20</td>

<td>Journal of the History of Economic Thought</td>

<td>3</td>

<td>Journal of Information Technology</td>

<td>1</td>

<td>College and Research Libraries</td>

<td>1</td>

</tr>

</tbody>

</table>

<section>

## Results and discussion

For each data set I examined the contributors (article authors), knowledge base (most commonly cited sources, documents, and authors), and concepts (from article titles). I used Jaccard and vector cosine measures to determine the statistical similarities of the three groups of articles on each of the above parameters.

### Contributors

I first examined the lists of major contributors in each tradition (lists not shown), counting all authors regardless of placement. While there were no overlaps between the top ten contributors to these three fields, for a more robust comparison, I used complete lists of authors and computed Jaccard similarities between author distributions of each pair of fields (Table 2). The similarities between all pairs of fields are very low (0 between science and technology studies and library and information science; 0.01 between science and technology studies and scientometrics; and 0.04 between library and information science and scientometrics). This is an indicator that the authors belong to three distinct communities.

</section>

<table><caption>

Table 2\. The _overlap_ between science and technology studies, library and information science, and scientometrics for different parameters measured using the cosine similarity, except for authors where Jaccard similarity was used. Higher values indicate greater similarity. Largest value in each measure is in bold.  
</caption>

<tbody>

<tr>

<th> </th>

<th>Science and technology studies vs. Library and information science</th>

<th>Science and technology studies vs. Scientometrics</th>

<th>Library and information science vs. Scientometrics</th>

</tr>

<tr>

<td>Knowledge base – cited sources</td>

<td>0.13</td>

<td>0.12</td>

<td>

**0.38**</td>

</tr>

<tr>

<td>Knowledge base – cited authors</td>

<td>0.13</td>

<td>0.09</td>

<td>

**0.26**</td>

</tr>

<tr>

<td>Knowledge base – cited works</td>

<td>0.03</td>

<td>0.03</td>

<td>

**0.11**</td>

</tr>

<tr>

<td>Authors*</td>

<td>0.00</td>

<td>0.01</td>

<td>

**0.04**</td>

</tr>

<tr>

<td>Title words (without search terms)</td>

<td>0.35</td>

<td>0.36</td>

<td>

**0.56**</td>

</tr>

</tbody>

</table>

<section>

### Knowledge base

Knowledge base can be defined as the body of literature on which a researcher builds his/her contributions as exemplified through references. References, in addition to providing a context for a study, serve as a device to persuade editors, referees, and ultimately readers of the study’s credibility as well as means to reinforce the common paradigms of scientific fields ([Milojevi&cacute; 2012](#milojevic12)). For each of the three fields I study the knowledge base in terms of most commonly cited sources, documents, and authors.

The list of ten most cited sources (Table 3) indicates that all three fields are heavily relying on their own journals. Apart from its own tradition science and technology studies seems to be using research coming from major general science journals (e.g., S_cience, Nature_). For scientometrics, the examination of top sources serves as the first indicator that this tradition is indeed at a cross between science and technology studies and library and information science. Apart from the very strong presence of its major venue for publication (_Scientometrics_), one can see a mix of library and information science and science and technology studies journals, as well as the presence of major general science journals (e.g., _Science_). library and information science draws primarily from its own sources. However, it also uses research coming from cognate field of MIS and some science and technology studies sources (e.g., Interdisciplinarity). Although the similarity between library and information science and scientometrics is the highest of the three when it comes to the sources used (Table 2), the list of the sources does not show this, probably because library and information science cites scientometrics research published in traditionally library and information science venues, rather than in the journal _Scientometrics_.

<table><caption>

Table 3\. The list of ten most frequently cited sources within science and technology studies, library and information science, and scientometrics and the number of times they were cited. Items appearing in more than one list (overlaps) are in bold.  
</caption>

<tbody>

<tr>

<th> </th>

<th>Science and technology studies</th>

<th>Library and information science</th>

<th>Scientometrics</th>

</tr>

<tr>

<td>1</td>

<td>

**Science**</td>

<td>104</td>

<td>

**JASIST**</td>

<td>78</td>

<td>Scientometrics</td>

<td>748</td>

</tr>

<tr>

<td>2</td>

<td>Am.J.Sociol.</td>

<td>95</td>

<td>

**J.Doc**</td>

<td>41</td>

<td>

**JASIST**</td>

<td>559</td>

</tr>

<tr>

<td>3</td>

<td>AmSociol.Rev.</td>

<td>77</td>

<td>Interdisciplinaridad</td>

<td>18</td>

<td>Res.Policy</td>

<td>111</td>

</tr>

<tr>

<td>4</td>

<td>

**Soc.Stud.Sci.**</td>

<td>58</td>

<td>MIS Q.</td>

<td>15</td>

<td>Res.Eval.</td>

<td>73</td>

</tr>

<tr>

<td>5</td>

<td>Isis</td>

<td>54</td>

<td>Intrerdisciplinarity</td>

<td>13</td>

<td>IP & M</td>

<td>66</td>

</tr>

<tr>

<td>6</td>

<td>Nature</td>

<td>49</td>

<td>J.Inform.Sci.</td>

<td>13</td>

<td>

**Science**</td>

<td>62</td>

</tr>

<tr>

<td>7</td>

<td>Ann.Rev.Sociol.</td>

<td>38</td>

<td>Information Research</td>

<td>13</td>

<td>

**MIS Q.**</td>

<td>57</td>

</tr>

<tr>

<td>8</td>

<td>Hist.Sci.</td>

<td>37</td>

<td>Database</td>

<td>12</td>

<td>

**Soc.Stud.Sci.**</td>

<td>55</td>

</tr>

<tr>

<td>9</td>

<td>Hist.Theory</td>

<td>32</td>

<td>

**ARIST**</td>

<td>12</td>

<td>

**ARIST**</td>

<td>54</td>

</tr>

<tr>

<td>10</td>

<td>Interdisciplinarity</td>

<td>32</td>

<td>J.Am.Med.Info.Ass.</td>

<td>11</td>

<td>

**J.Doc.**</td>

<td>53</td>

</tr>

</tbody>

</table>

The lists of the most cited authors and documents offer further insights (Tables 4 and 5). The most cited documents in the library and information science section are mostly related to theories and the nature of the field itself. The cited documents indicate that this field draws from a variety of theories, from the classics (Kuhn) to one of the most recent models of contemporary science (Gibbons _et al._). The interest in interdisciplinarity is reflected by the presence of two major books on the topic (Klein). The presence of Foucault indicates an interest in classification that is confirmed in the analysis of the most frequently used terms (Table 6). Interest in the institutional organization of knowledge is exemplified in Becher. The presence of Bates, Borko, and Saracevic indicates a strong interest in studying the field of library and information science itself. This is again confirmed by the analysis of major terms used (e.g., information science, library and information science, and informatics) (Table 6). Finally, Hjorland and Albrechtsen is an example of a study arguing for a particular approach, that of domain analysis. The list of authors reveals some names that did not appear in the list of most often used works, but who are obviously important for their summary contributions (e.g., Garfield and Bourdieu). As with the examination of most often cited sources (section above) it is again interesting that when one looks at the most often referred to authors and works, science and technology studies and library and information science seem to be more similar than each is to science and technology studies (Table 2).

<table><caption>

Table 4\. The list of the ten most frequently cited authors in science and technology studies, library and information science, and scientometrics and the number of times they were cited. Authors appearing in more than one list are in bold; those appearing in all three are underlined.  
</caption>

<tbody>

<tr>

<th> </th>

<th>Science and technology studies</th>

<th> </th>

<th>Library and information science</th>

<th> </th>

<th>Scientometrics</th>

<th> </th>

</tr>

<tr>

<td>1</td>

<td>Kuhn T</td>

<td>94</td>

<td>Hjørland B</td>

<td>18</td>

<td>Leydesdorff L</td>

<td>140</td>

</tr>

<tr>

<td>2</td>

<td>Foucault M</td>

<td>57</td>

<td>

**<u>Klein JT</u>**</td>

<td>18</td>

<td>Small H</td>

<td>113</td>

</tr>

<tr>

<td>3</td>

<td>

**Bourdieu P**</td>

<td>49</td>

<td>Bates M</td>

<td>14</td>

<td>White H</td>

<td>69</td>

</tr>

<tr>

<td>4</td>

<td>Abbott A</td>

<td>42</td>

<td>Morin E</td>

<td>13</td>

<td>

**Garfield E**</td>

<td>66</td>

</tr>

<tr>

<td>5</td>

<td>

**<u>Klein JT</u>**</td>

<td>35</td>

<td>Pinheiro L</td>

<td>12</td>

<td>Porter A</td>

<td>61</td>

</tr>

<tr>

<td>6</td>

<td>Athias M</td>

<td>33</td>

<td>Wilson TD</td>

<td>12</td>

<td>Price D</td>

<td>50</td>

</tr>

<tr>

<td>7</td>

<td>Butterfield H</td>

<td>29</td>

<td>

**Bourdieu P**</td>

<td>10</td>

<td>Chen C</td>

<td>48</td>

</tr>

<tr>

<td>8</td>

<td>Needham J</td>

<td>28</td>

<td>

**Garfield E**</td>

<td>10</td>

<td>Glanzel W</td>

<td>47</td>

</tr>

<tr>

<td>9</td>

<td>Kihara H</td>

<td>27</td>

<td>Tenopir C</td>

<td>9</td>

<td>

**<u>Klein JT</u>**</td>

<td>43</td>

</tr>

<tr>

<td>10</td>

<td>Collins R</td>

<td>27</td>

<td>Saracevic T</td>

<td>9</td>

<td>McCain K</td>

<td>39</td>

</tr>

</tbody>

</table>

<table><caption>

Table 5\. The list of ten most frequently cited documents in science and technology studies, library and information science, and scientometrics and the number of times they were cited. Authors appearing in more than one list are in bold; those appearing in all three are underlined.  
</caption>

<tbody>

<tr>

<th> </th>

<th colspan="2">Science and technology studies</th>

<th colspan="2">Library and information science</th>

<th colspan="2">Scientometrics</th>

</tr>

<tr>

<td>1</td>

<td>

**<u>Kuhn TS, 1970, Structure of scientific revolutions</u>**</td>

<td>31</td>

<td>

**<u>Kuhn TS, 1970, Structure of scientific revolutions</u>**</td>

<td>8</td>

<td>Small H, 1973, Co-citation in the scientific literature, JASIS</td>

<td>22</td>

</tr>

<tr>

<td>2</td>

<td>Abbott A, 2001, Chaos of disciplines</td>

<td>18</td>

<td>

**<u>Gibbons M., et. al., 1994, new production of knowledge</u>**</td>

<td>6</td>

<td>White HD, and McCain C. 1998, Visualizing a discipline, JASIS</td>

<td>17</td>

</tr>

<tr>

<td>3</td>

<td>

**<u>Gibbons M., et. al., 1994, New production of knowledge</u>**</td>

<td>15</td>

<td>

**Klein, JT, 1990, Interdisciplinarity**</td>

<td>6</td>

<td>Morillo F, Bordons M, and Gomez I, 2001, An approach to interdisciplinary bibliometric indicators, Scientometrics</td>

<td>17</td>

</tr>

<tr>

<td>4</td>

<td>

**Klein, Julie Thompson, 1990, Interdisciplinarity**</td>

<td>13</td>

<td>

**Klein, JT, 1996, Crossing boundaries**</td>

<td>5</td>

<td>Morillo F, Bordons M, and Gomez I, 2003, Interdisciplinarity in science, JASIST</td>

<td>16</td>

</tr>

<tr>

<td>5</td>

<td>Nowotny H, et. al. 2001, Rethinking scientific knowledge</td>

<td>12</td>

<td>Saracevic T, 1999, Information science, JASIS</td>

<td>4</td>

<td>Price Derek de Solla, 1963, Little science big science</td>

<td>16</td>

</tr>

<tr>

<td>6</td>

<td>Knorr Cetina, Karin, 1999, Epistemic cultures</td>

<td>11</td>

<td>Borko H, 1968, Information science: what is it? American Documentation</td>

<td>4</td>

<td>

**<u>Kuhn TS, 1970, Structure of scientific revolutions</u>**</td>

<td>16</td>

</tr>

<tr>

<td>7</td>

<td>Gieryn T. F., 1999, Cultural boundaries</td>

<td>10</td>

<td>Bates MJ, 1999, The invisible substrate of information science, JASIS</td>

<td>3</td>

<td>Porter AL and Chubin, DE, 1985, An indicator of cross-disciplinary research, Scientometrics</td>

<td>15</td>

</tr>

<tr>

<td>8</td>

<td>Abbot A, 1988, The system of professions: an essay on the division of expert labor</td>

<td>10</td>

<td>Hjorland B, Albrechtsen H, 1995, Toward a new horizon in information science: domain-analysis, JASIS</td>

<td>3</td>

<td>

**<u>Gibbons M., et. al., 1994, New production of knowledge</u>**</td>

<td>14</td>

</tr>

<tr>

<td>9</td>

<td>Galison Peter, 1997, Image and logic</td>

<td>9</td>

<td>Becher T, 2001, Academic tribes and territories</td>

<td>3</td>

<td>Boyack KW, Klavans R, Borner K, 2005, Mapping the backbone of science, Scientometrics</td>

<td>14</td>

</tr>

<tr>

<td>10</td>

<td>

**Klein J. T., 1996, Crossing boundaries**</td>

<td>8</td>

<td>Foucault M, The order of things: an archaeology of human sciences</td>

<td>3</td>

<td>Kessler MM, 1963, Bibliographic coupling between scientific papers, American Documentation</td>

<td>13</td>

</tr>

</tbody>

</table>

The most cited documents in the scientometrics group are divided somewhat differently. Two of the documents are related to traditional citation analysis methods (Small and Kessler). There are also documents dealing with visualization of disciplines and mapping of science (White and McCain and Boyack, Klavans and Borner). As with the other two fields, there are works related to inter-/cross-/multi- disciplinarity (Morillo, Bordons and Gomez and Porter and Chubin). There are also three documents related to theory, two classics (Kuhn and Price), and a more recent one on Mode 2 science (Gibbons _et al._). In addition, just like in library and information science, there are a few researchers, who made significant contributions to the knowledge base of the field, but did not appear in the list of most cited documents (e.g., Leydesdorff, Garfield, Chen and Glanzel).

The list of most cited documents in science and technology studies confirms the importance of books for this community, with ten most frequently cited resources being books. While Kuhn’s book is the most cited, it is highly probable that it has been criticized, rather than expanded on. However, in order to make those conclusions one would need to do citation context analysis, which is outside the scope of this paper. The most cited documents in the science and technology studies group reflect some of the more recent developments in this field, with an emphasis of studies of particular communities and their practices. This is reflected in the strong presence of works on less reductionist concepts, such as epistemic communities, boundary-work (e.g., Knorr-Cetina, Gieryn, Galison). The list of most cited authors provides further evidence of the prevalence of more critical approaches (e.g., Foucault, Bourdieu). Another major contribution to the knowledge base of science and technology studies are the books on new models of modern science (Gibbons _et al._; Nowotny _et al._; Abbott). Like scientometrics and library and information science, this field also shows interest in interdisciplinarity (Klein).

An examination of the resources indicates that works on interdisciplinarity have high presence in the knowledge bases of all three fields. This is not surprising given that since the 1990s there has been an increase in the number of policies and the amount of funding aimed at promoting interdisciplinary collaborations under the assumptions that interdisciplinary research generates a higher rate of breakthroughs, is more successful at dealing with societal problems, and fosters innovation and competitiveness. All three fields also refer to Kuhn, but probably for different reasons. Additionally, one of the more recent theories of the nature of knowledge production in modern science has also been used by all three fields.

The above analysis indicates that there is greater commonality in the cited authors than in the citing authors, i.e., the cognitive basis of the three approaches is much stronger than the social ties of their practitioners.

### Shared terminology

I examine the contribution of the three approaches to the study of different concepts, by exploring the relative frequency of keywords in titles (Figure 1). The interest in interdisciplinarity and multidisciplinarity in all three groups is confirmed by examining the contributions to this literature by all three groups. The higher contributions by scientometrics and science and technology studies may indicate that these two fields are making major methodological and theoretical contributions respectively, while library and information science is mostly applying these contributions. Keywords containing disciplinarity are present in literature of all three traditions, but mostly in science and technology studies. Of other main terms, epistemic community is mostly mentioned by science and technology studies but not at all by library and information science, while knowledge domain, research front, and invisible college are of interest to library and information science and scientometrics, but not to science and technology studies.

<figure>

![Figure 1\. The relative contribution of science and technology studies, library and information science, and scientometrics to the study of major concepts](../pC34fig1.png)

<figcaption>

Figure 1\. The relative contribution of science and technology studies, library and information science, and scientometrics to the study of major concepts.</figcaption>

</figure>

In terms of concepts used in titles (without the terms used in the selection of documents) the three fields show much higher degree of similarity than it was the case with references and the authors (Table 2). Similarity in terms used is again the highest between library and information science and scientometrics.

The lists of ten most common terms (Table 6) reflect both the methods and the objects of study. Top terms used in science and technology studies show a strong presence of history of science, especially one prior to the twentieth century science (as exemplified by the term century). The relatively strong focus on theory is confirmed by the presence of the word theory. Finally, the term cultural emphasizes the critical approaches to science. The list of terms most often used in library and information science confirms the conclusion made based on the documents and authors cited that this group is very much focused on studying itself. The group is also interested in classification. The top terms used in scientometrics confirm interest in mapping. scientometrics also shows an interest in networks. In addition, this group is interested in the application of the findings for the purposes of evaluation in the form of the term indicators.

<table><caption>

Table 6\. The list of ten most frequently used terms (words and word phrases) in the titles of articles in science and technology studies, library and information science, and scientometrics. Authors appearing in more than one list are in bold; those appearing in all three are underlined.</caption>

<tbody>

<tr>

<th> </th>

<th colspan="2">Science and technology studies</th>

<th colspan="2">Library and information science</th>

<th colspan="2">Scientometrics</th>

</tr>

<tr>

<td>1</td>

<td>History</td>

<td>86</td>

<td>Information Science</td>

<td>15</td>

<td>

**<u>Science</u>**</td>

<td>34</td>

</tr>

<tr>

<td>2</td>

<td>

**<u>Science</u>**</td>

<td>46</td>

<td>

**Information**</td>

<td>12</td>

<td>Citation</td>

<td>28</td>

</tr>

<tr>

<td>3</td>

<td>Sociology</td>

<td>33</td>

<td>Library</td>

<td>11</td>

<td>

**Information**</td>

<td>16</td>

</tr>

<tr>

<td>4</td>

<td>Philosophy</td>

<td>33</td>

<td>

**<u>Science</u>**</td>

<td>8</td>

<td>Journal</td>

<td>15</td>

</tr>

<tr>

<td>5</td>

<td>Social</td>

<td>20</td>

<td>Classification</td>

<td>8</td>

<td>Mapping</td>

<td>15</td>

</tr>

<tr>

<td>6</td>

<td>Century</td>

<td>19</td>

<td>Informatics</td>

<td>6</td>

<td>Bibliometric</td>

<td>15</td>

</tr>

<tr>

<td>7</td>

<td>Social science</td>

<td>16</td>

<td>Literature</td>

<td>4</td>

<td>Indicator</td>

<td>12</td>

</tr>

<tr>

<td>8</td>

<td>Theory</td>

<td>15</td>

<td>Model</td>

<td>4</td>

<td>Network</td>

<td>11</td>

</tr>

<tr>

<td>9</td>

<td>Cultural</td>

<td>14</td>

<td>Academic</td>

<td>4</td>

<td>Development</td>

<td>9</td>

</tr>

<tr>

<td>10</td>

<td>Comparative</td>

<td>14</td>

<td>Library and information science</td>

<td>4</td>

<td>Co-</td>

<td>9</td>

</tr>

</tbody>

</table>

## Conclusions

The study shows that socially the three approaches to studying disciplinarity in science are populated by distinct groups of contributors. The similarity between the three approaches is somewhat higher (though not high) when it comes to the knowledge base. There is some similarity in authors referenced, although not necessarily the same works by those authors, as exemplified by Foucault. In addition, both scientometrics and library and information science seem to be drawing from science and technology studies literature, but science and technology studies is not reciprocating. Remarkably, in cognitive sense the distinctions are much smaller. The three fields also seem to be studying the same objects, and even using similar terminology (as exemplified by similarity in terminology). At the finer scale we also see some specifics, which suggest that the three approaches are interested in different aspects of those objects: science and technology studies (cultural), library and information science (classification), and scientometrics (indicators and mapping). Therefore, the fact that they are drawing from different traditions is not surprising. While disciplinarity has been the most common topic in all three groups, there does not seem to have emerged a new body of theory that would synthesize and/or explain these new findings. It seems that when it comes to studying disciplines all three traditions have put much more effort in individual case studies than in synthesizing the findings into new theories and models. Kuhn’s Structure of Scientific Revolutions still seems to be the dominant piece of theory when it comes to talking about disciplines. All three fields were also highly influenced by the idea of Mode 2\. Interdisciplinarity has been one area where all three communities have contributed, and they also seem to be drawing from the same knowledge base. It is not clear how much the findings coming from all three fields are being integrated into new theories and models.

## Acknowledgements

I thank two anonymous referees for useful suggestions, and Colleen Martin for copy editing.

</section>

<section>

## References

<ul>
<li id="astrom02">Åstróm, F. (2002). Visualizing library and information science concept spaces through keyword and citation based maps and clusters. In H. Bruce, R. Fidel, P. Ingwarsen and P. Vakkari (Eds.), Emerging frameworks and methods: <em>Colibrary and information science4: Proceedings of the Fourth International Conference on Conceptions of Library and Information Science</em>, Seattle, WA, July 21-25, 2002 (pp. 185-197). Greenwood Village: Libraries Unlimited
</li>
<li id="bates10">Bates, M.J. (2010). Introduction to the Encyclopedia of Library and Information Sciences, third edition. In M. J. Bates and M. N. Maack (Eds.), <em>Encyclopedia of Library and Information Sciences</em> (pp. xi-xviii). New York: CRC Press
</li>
<li id="ben66">Ben-David, J. &amp; Collins, R. (1966). Social factors in the origins of a new science: the case of psychology. <em>American Sociological Review</em>, <strong>31</strong>(4), 451-465
</li>
<li id="bowker05">Bowker, G.C. (2005). Memory Practices in the Sciences. <em>Cambridge, Massachusetts: The MIT Press</em>
</li>
<li id="chubin76">Chubin, D.E. (1976). The conceptualization of scientific specialties. <em>Sociological Quarterly</em>, <strong>17</strong>, 448-476
</li>
<li id="cole75">Cole, J.R. &amp; Zuckerman, H. (1975). The emergence of a scientific specialty: The self-exemplifying case of the sociology of science. In L. A. Coser (Ed.), The Idea of Social Structure: Papers in Honor of Robert K. Merton (pp. 139-174). <em>New York: Harcourt Brace Jovanovich</em>
</li>
<li id="crane72">Crane, D. (1972). Invisible colleges: diffusion of knowledge in scientific communities. <em>Chicago: University of Chicago Press</em>
</li>
<li id="cronin05">Cronin, B. (2005). <em>The hand of science: academic writing and its rewards.</em> Lanham, MD: Scarecrow Press
</li>
<li id="edge76">Edge, D.O. &amp; Mulkay, M. J. (1976). <em>Astronomy transformed: the emergence of radio astronomy in Britain.</em> New York, NY: John Wiley &amp; Sons
</li>
<li id="foucault72">Foucault, M. (1972). The discourse on language. In <em>The archaeology of knowledge and the discourse on language</em> (pp. 215-237). New York: Pantheon Books
</li>
<li id="foucault78">Foucault, M. (1978/1995). <em>Discipline and punish: the birth of the prison</em>. New York, NY: Vintage Books
</li>
<li id="gibbons94">Gibbons, M., Limoges, C., Nowotny, H., Schwartzman, S., Scott, P. &amp; Trow, M. (1994). The new production of knowledge: the dynamics of science and research in contemporary societies. <em>London: Sage</em>
</li>
<li id="goldstein84">Goldstein, J. (1984). Foucault among the sociologists: the "disciplines" and the history of the professions. <em>History and Theory</em>, <strong>23</strong>(2), 170-192
</li>
<li id="golinski05">Golinski, J. (2005). Making natural knowledge: constructivism and the history of science. <em>Chicago: The University of Chicago Press</em>
</li>
<li id="hagstrom70">Hagstrom, W.O. (1970). Factors related to the use of different modes of publishing research in four scientific fields. In C.E. Nelson and D.K. Pollock (Eds.), <em>Communication among scientists and engineers</em> (pp. 85-124). Lexington: Lexington Books
</li>
<li id="hjorland02">Hjørland, B. (2002). Domain analysis in information science: Eleven approaches - traditional as well as innovative. <em>Journal of Documentation</em>, <strong>58</strong>(4), 422-462
</li>
<li id="hjorland95">Hjørland, B. &amp; Albrechtsen, H. (1995). Towards a new horizon in information science: domain analysis. <em>Journal of the American Society for Information Science</em>, <strong>46</strong>, 400-425
</li>
<li id="janssens06">Janssens, F., Leta, J., Glänzel, W. &amp; De Moor, B. (2006). Towards mapping library and information science. <em>Information Processing &amp; Management</em>, <strong>42</strong>(6), 1614-1642
</li>
<li id="klein90">Klein, J.T. (1990). Interdisciplinarity: history, theory, and practice. <em>Detroit: Wayne State University Press</em>
</li>
<li id="kuhn62">Kuhn, T.S. (1962/1996). The structure of scientific revolutions. <em>Chicago: University of Chicago Press</em>
</li>
<li id="law76">Law, J. (1976). The development of specialties in science: the case of x-ray protein crystallography. In G. Lemaine, R. Macleod, M. J. Mulkay and P. Weingart (Eds.), <em>Perspectives on the emergence of scientific disciplines</em> (pp. 123-152). Chicago: Aldine
</li>
<li id="leydesdorff97">Leydesdorff, L. &amp; Van den Besselaar, P. (1997). Scientometrics and Communication Theory: Towards Theoretically Informed Indicators. <em>Scientometrics</em>, <strong>38</strong>, 155-174
</li>
<li id="lievrouw90">Lievrouw, L.A. (1990). Reconciling structure and process in the study of scholarly communication. In C. L. Borgman (Ed.), <em>Scholarly communication and bibliometrics</em> (pp. 59-69). Newbury Park: Sage Publications
</li>
<li id="masterman70">Masterman, M. (1970). The nature of a paradigm. In I. Lakatos and A. Musgrave (Eds.), <em>Criticism and the growth of knowledge</em> (pp. 59-89). Cambridge: Cambridge University Press
</li>
<li id="milojevic12">Milojevi&amp;cacute;, S. (2012). How are academic age, productivity and collaboration related to citing behavior of researchers? <em>PLoS One</em>, <strong>7</strong>(11), e49176
</li>
<li id="milojevic13">Milojevi&amp;cacute;, S. &amp; Leydesdorff, L. (2013). Information metrics (iMetrics): A research specialty with a socio-cognitive identity? <em>Scientometrics</em>, <strong>95</strong>(1), 141-157
</li>
<li id="milojevic11">Milojevi&amp;cacute;, S., Sugimoto, C.R., Yan, E. &amp; Ding, Y. (2011). The cognitive structure of library and information science: Analysis of article title words. <em>Journal of the American Society for Information Science and Technology</em>, <strong>62</strong>(10), 1933-1953
</li>
<li id="mullins72">Mullins, N.C. (1972). The development of a scientific specialty: The Phage group and the origins of molecular biology. <em>Minerva</em>, <strong>10</strong>, 52-82
</li>
<li id="palmer01">Palmer, C.L. (2001). Work at the boundaries of science: information and the interdisciplinary research process. <em>Dordrecht: Kluwer Academic Publishers</em>
</li>
<li id="price63">Price, D.J.D.S. (1963). Little science, big science. <em>New York: Columbia University Press</em>
</li>
<li id="small74">Small, H. &amp; Griffith, B.C. (1974). The structure of scientific literatures I: Identifying and graphing specialties. <em>Science Studies</em>, <strong>4</strong>(1), 17-40
</li>
<li id="spiegel77">Spiegel-Rósing, I. &amp; Price, D.J.D.S. (Eds.). (1977). Science, Technology and Society. A Cross-Disciplinary Perspective. <em>London: Sage</em>
</li>
<li id="stichweh92">Stichweh, R. (1992). The sociology of scientific disciplines: On the genesis and stability of the disciplinary structure of modern science. <em>Science in Context</em>, <strong>5</strong>(1), 3-15
</li>
<li id="besselaar01">Van den Besselaar, P. (2001). The cognitive and the social structure of science and technology studies. <em>Scientometrics</em>, <strong>51</strong>(2), 441-460
</li>
<li id="besselaar06">Van den Besselaar, P. &amp; Heimeriks, G. (2006). Mapping research topics using word-reference co-occurrences: A method and an exploratory case study. <em>Scientometrics</em>, <strong>68</strong>(3), 377-393
</li>
<li id="zuccala06">Zuccala, A. (2006). Modeling the invisible college. <em>Journal of the American Society for Information Science and Technolog</em>y, <strong>57</strong>(2), 152-168
</li>
<li id="zuckerman88">Zuckerman, H. (1988). The sociology of science. In N. J. Smelser (Ed.), <em>Handbook of Sociology</em> (pp. 511-574). Newbury Park: Sage
</li>
</ul>

</section>

</article>