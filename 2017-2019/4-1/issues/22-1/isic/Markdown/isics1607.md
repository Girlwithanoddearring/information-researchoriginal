#### vol. 22 no. 1, March, 2017

<article>

# The information behaviour of Pacific Crest Trail thru-hikers: an autoethnographic pilot study

## [Ed Hyatt](#author)

> **Introduction.** This work-in-progress study considers if the models of Savolainen, Kuhlthau, and Wilson are congruent with the dynamics of information seeking and sharing behaviour amongst long-distance hikers as they undertake a journey along the Pacific Crest Trail, USA; this approach is largely unexplored in academic literature. Initial insights stemming from a pilot study are examined. Expected outcomes from the research include contributing to advancing knowledge in these areas and creating worth for the thru-hiking community with regard to their safety, time, navigation, efficiencies, and overall quality of experience.  
> **Method.** The study uses autoethnography as outlined by Ellis, Adams, and Bochner to investigate the information seeking behaviour associated with hikers.  
> **Analysis and results.** A preliminary pilot-study generated, amongst other materials, audio field notes that were informed by the works of Savolainen, Kuhlthau, and Wilson. Comments are made on the fieldwork process and difficulties in data collection are explored. The pilot-study confirmed that the autoethnographic method was suitable for this research and that a loose adherence to the elements that comprise models of information seeking behaviour gives some necessary structure to data collection.  
> **Initial insights and next steps.** The consideration of information behaviour during the pilot study was both interesting and informative, contributing to a cementation of ideas on the environment for fieldwork data collection, models of information seeking behaviour, and the autoethnographic method. The main data collection phase of the work is (literally) the next step in the research process: a three-month period whilst walking the Pacific Crest Trail.

<section>

## Introduction

Hoover Wilderness, California. July 2015.

> I’m at the edge of a rotten old glacier with a pass to cross at 11,600’. A late summer snow storm has changed my tactics and needs. The pre-planned route is no longer the way; now a shooting range as sun-warmed rocks released from thawing snow bounce across my intended path. Digital maps, GPS routes, reading web and picture blogs, studying Google Earth, and the rest of the protective web of everyday information I’ve spun around myself mean little; there’s not much maintenance of order here. Reality bites. Experience, unconscious in action comes into play and forty years of mountaineering practice take over. The information I am using now is different, as is my role in this theatre. Actions fuelled by fear drive feelings and thoughts; certainly salient yet hard to touch, categorise, and rationalise. This diverse and eclectic mixture is vested in personal experiences and beliefs, environments, competences, a tumult of emotional responses to situations, fed by old, new, and growing informational needs and experiences.  
>   
> It wasn’t meant to be like this.  
>   
> I hadn’t planned it this way. ([Hyatt, 2015](#hya15)).

This paper will discuss the milieu of recreational hiking prior to considering the models of information seeking behaviour that are most closely aligned with that activity, it then moves on to suggest that the autoethnographic method is a suitable means of exploring these processes. The narrative concludes with preliminary reflections and insights to lessons learned in a pilot study before outlining the next stage of the work.

The study’s distinction is formed around the application of and learning from the testing of information seeking behaviours in the context of hiking; a realisation of theory in practice. The work aspires to enhance information seeking behaviour research and methodologies while developing links between research and study populations. Worth will be created for the thru-hiking community by enhancing understandings and providing educational value with regard to their safety, time, navigation, food, efficiencies, and overall quality of experience.

This research uses extant models of information seeking behaviours as a framework to evaluate the informational dynamics and processes of long-distance thru-hikers as they undertake a five-month journey along the Pacific Crest Trail. Anecdotally referred to as ‘thru-hikers’ ([Luxenberg, 1994](#lux94); [Mueser, 1998](#mue98)) travellers on long distance paths comprise a discrete and closely-knit body of individuals who coalesce to form a collective with a notable camaraderie and the same motivation, to walk long distances in the wilderness regions of the USA. The fieldwork setting for this study is the Pacific Crest Trail, a 2,663 mile route that traverses the Western mountain ranges and deserts of the USA between the Mexican and Canadian borders.

This research concerns self, and self in relation to a series of complex environmental factors stemming from the need and requirement to sustain progress through various landscapes that call upon information, experience, feelings, and physicality. The realm of the long-distance walker and the various negotiations that revolve around the act of walking are central to this work. Personal experience is explored via authoethnography as a means and measure for reflection on the nature of information seeking, development, and sharing within a nomadic community.

There has been a degree of exploration of the experiences and artefacts that long distance walking generates, although comparatively few are found in academic literature, and fewer still that use an information seeking behaviour perspective. Much extant research is shaped around experiences and situations on The Appalachian Trail, the first officially sanctioned long-distance route in the USA ([Mueser, 1998](#mue98)). The earliest exploration of a society in motion (Forrester (1994, p. _ix_) in Luxenberg 1994) used individual characterisations to illustrate the collective behaviours of hikers on the trail. A more structured methodology was employed in Mueser’s ([1998](#mue98)) survey of Appalachian Trail walkers which considered a range of hiker habits and attitudes. The most pertinent work relating to information related themes is Siudzinski‘s ([2007](#siu07)) thesis on knowledge construction and sharing in Appalachian Trail hikers.

## Information seeking behaviour models germane to the study

The question central to this work is, are the models of Savolainen ([1995](#sav95)), Kuhlthau ([1991](#kuh91)), and Wilson ([1997](#wil97)) congruent with the dynamics of information seeking and sharing behaviour amongst long-distance hikers? The debate around what constitutes information is complex and seemingly endless; similarly the various contexts and labels applied to information seeking behaviour are equally as involved and perhaps as elusive from a point of definition. Wilson’s ([1999](#wil99)) review of information behaviour models is a particularly useful inception point in this discussion as it illustrates the divergent representations of what information seeking behaviour might encapsulate. Such models are further examined by Case ([2012](#cas12)) in a condensed and accessible comparative analysis of their respective approaches and applicability. Both Wilson ([1999](#wil99)) and Case ([2012](#cas12)) appear to flag an unsurprising evolution in models of information seeking behaviour; stated simply this manifests itself as a move away from resource focused and systemic representations of behaviour ([Wilson, 1981](#wil81); [Krikelas, 1983](#kri83); [Ellis, 1989](#ell89)) towards less precise and defined models that recognise the import of less-tangible characteristics such as feelings, thoughts, and actions ([Kuhlthau, 1991](#kuh91)), mastery of way of life, and maintenance of order ([Savolainen, 1995](#sav95)). These latter characteristics resonate within the context of long-distance hiking, where information is employed heavily in the planning and execution of long walks.

This outline of models of information seeking behaviour potentially suitable to this research leads toward an examination of the method that will be employed to collect data within the distinct thru-hiking environment of the work.

## Methodology

The study employs a combination of autoethnographic and ethnographic methods ([Mead, 1934](#mea34); [Malinowski, 1960](#mal60); [Ellis, Adams, and Bochner, 2011](#ell11); [Hammersley and Atkinson, 2007](#ham07); [Whyte, 1943](#why43)) to investigate the information behaviour associated with Pacific Crest Trail thru-hikers in 2016\. Constant comparative analysis ([Strauss & Corbin, 1990](#str90); [Glaser & Strauss, 1967](#gla67)) and discourse analysis ([Brown and Yule ,1983](#bro83)) will be used cast a lens upon this community. The much vaunted _thick descriptions_ ([Geertz, 1973, p.10](#gee73)) that characterise ethnographic studies are given a personal dimension with auto-ethnography, which various commentators ([Anderson, 2006](#and06); [Goodall, 2006](#goo06); [Bochner ,1997](#boc97)) perceive as aiding in the broader understanding of cultural practices. The opportunity for study is time-delimited by the seasonal nature of the long-distance walking calendar and a three-month data collection window is envisaged. A combination of the reflexive and layered approaches to auto-ethnography as detailed in Ellis, Adams, and Bochner ([2011](#ell11)) would support the intended participatory approach. Field notes fashioned around reflexivity (one of the central tenets of autoethnography) and observation will form the output of the work; notes will be taken in an audio format, a method that worked well in the pilot-study.

## Analysis of the pilot study

A pilot-study in 2015 adopted a grounded approach. This was spread over three instances and a range of situations.

1.  A nine-day solo walk along the Tahoe Rim Trail, California
2.  A month solo and largely off-trail (no paths) in the High Sierra, California
3.  An eight day off-trail (no paths) trip with friends in the Wind Rivers Mountains, Wyoming

The experience gained from the different situations and conditions of observation was very useful; not least as it exposed naiveté in expectations of ease and setting of data collection; lessons were learned that have informed the timings and locations of the principal fieldwork study.

Wilson ([1999](#wil99)) posits that some models of information seeking behaviour are more firmly established by subsequent research than others, namely those of Dervin ([2000](#der00)), Ellis ([1989](#ell89)), Kuhlthau ([1991](#kuh91)), and Wilson ([1981](#wil81) and [1997](#wil97)). Case ([2012, p. 137](#cas12)) casts his net more broadly, considering nine models in his discussion on the basis that they are _more fully developed and/or more widely used than others_. The models that pertain most closely to the projected aims and objectives of this work are those of Savolainen ([1995](#sav95)), Kuhlthau ([1991](#kuh91)), and Wilson ([1997](#wil97)).

As an illustration, if opening statements from unpublished pilot-study fieldnotes ([Hyatt, 2015](#hya15)) are interlaced with one’s own interpretation of points drawn from the models, initial alignments with some of the elements promulgated by those authors can be made. Everyday life information seeking as outlined by Savolainen ([1995](#sav95)), embraced socio-economic factors and accumulated personal experience (from the author’s research; _Experience, unconscious in action comes into play and forty years of mountaineering practice take over_) within a framework of mastering life (_the protective web of everyday information I’ve spun around myself mean little_) including the social side of life) while considering how individuals add order to their lives via their selection and employment of information sources (_It wasn’t meant to be like this. I hadn’t planned it this way_). The seemingly complementary (with regard to the focus of this work) and oft-tested model of Kuhlthau ([1991](#kuh91)) has been very widely cited and applied and has resonance with this study. Kuhlthau’s work has been largely examined in educational contexts and considers how gaps in knowledge arise and are fulfilled through an information seekers cognitive and affective behaviour (_information I am using now is different, as is my role in this theatre_) being driven by emotional needs such as feelings, thoughts, and actions (_Actions fuelled by fear drive thoughts and feelings_). Wilson’s ([1997](#wil97)) model is also of interest as it addresses behavioural forms in association with hurdles that must be transcended by the person in context (_personal experiences and beliefs, environments, competences, a tumult of emotional responses to situations, fed by old, new, and growing informational needs and experiences_). Further alignments with these models will be explored in the next phase of the fieldwork.

## Initial insights

To summarise, the fieldwork pilot confirmed that an autoethnographic method was most suitable for this work as the author’s experience, allied with the fieldwork contexts translate themselves readily to an autobiographical narrative. The examination of the author’s and others interplays was informative, contributing to a cementation of ideas on both the environment for fieldwork and a series of data collection methods. Initial analysis of the data appears to suggest a promising congruence with the models from Savolainen ([1995](#sav95)), Kuhlthau ([1991](#kuh91)), and Wilson ([1997](#wil97)); that said, the volume of data collected from the pilot study is not sufficient to enable a satisfactory analysis; subsequent data collection will address this.

## Outlook and next steps

The next phase of the fieldwork will be conducted in late March through August 2016 and this paper will be delivered immediately post the data-collection phase, by which time a great deal more will be _known_ and also _not known_ about the research.

## Acknowledgements

I wish to thank my colleagues Professor Julie Mcleod, Dr Geoff Walton, Dr Liz Sillence, and Dr Perla Innocenti from the University of Northumbria for their encouragement and insights throughout my investigation. I am indebted to my iSchool colleagues for their flexibility and support in enabling my fieldwork and am grateful to the reviewers for their helpful suggestions and observations. This research is being financially supported by the University of Northumbria and its iSchool.

## <a id="author"></a>About the author

**Edward Hyatt** is a Senior Lecturer in Information Sciences at the iSchool, University of Northumbria in Newcastle, UK. After a BSc in Geology at the University of Birmingham and an MPhil in Remote Sensing at Aston University, he was awarded an MSc in Information Sciences from Sheffield University. Formerly a telecommunications consultant, he worked for a range of companies before moving to a position at Newcastle Business School. A two-year stint at ESC Rouen Business School, France followed, after which he returned to Newcastle, moving into the information sciences arena at Northumbria University. His research and teaching interests include autoethnography, information seeking and sharing behaviour, social informatics, writing, consultancy practice, and ubiquitous devices. He likes walking up mountains; a lot.

</section>

<section>

## References

<ul>
<li id="and06">Anderson, L. (2006). Analytic autoethnography, <em>Journal of Contemporary Ethnography</em>, 35(4), 373-395.
</li>
<li id="boc97">Bochner, A. P. (1997). It's about time: Narrative and the divided self. <em>Qualitative Inquiry.</em> 3(4), 418-438.
</li>
<li id="bro83">Brown, G. &amp; Yule, G. (1983). <em>Discourse analysis</em>.&nbsp;Cambridge: Cambridge University Press.
</li>
<li id="cas12">Case, D. O. (2012). <em>Looking for information: a survey of research on information seeking, needs, and behavior</em> (3rd. ed.). Bingley, UK: Emerald.
</li>
<li id="der00">Dervin, B. (2000). Chaos, order and sense-making: A proposed theory for information design. In R. Jacobson (Ed.). <em>Information design</em>&nbsp;(pp. 35-57). Cambridge, MA: MIT Press.
</li>
<li id="ell11">Ellis, C., Adams, T. &amp; Bochner, A. (2011). Autoethnography: an overview, <em>Historical Social Research-Historische Sozialforschung</em>, 36(4), 273-290.
</li>
<li id="ell89">Ellis, D. (1989). A behavioural approach to information retrieval system design. <em>Journal of Documentation,</em> 45(3), 171-212.
</li>
<li id="gee73">Geertz, C. (1973). <em>The interpretation of cultures</em>: <em>selected essays</em>. New York, NY: Basic Books.
</li>
<li id="gla67">Glaser, B. G. &amp; Strauss, A. L. (1967). <em>The discovery of grounded theory: strategies for qualitative research</em>. New York, NY: Aldine Publishing.
</li>
<li id="goo06">Goodall, Bud H.L. (2006). <em>A need to know: The clandestine history of a CIA famil</em>y. Walnut Creek, CA: Left Coast Press.
</li>
<li id="ham07">Hammersley, M. &amp; Atkinson, P. (2007). <em>Ethnography: principles in practice</em>. Routledge: UK
</li>
<li id="hya15">Hyatt, E. (2015). The information behaviour of Pacific Crest Trail thru-hikers: an autoethnographic study. Unpublished fieldnotes. Newcastle upon Tyne, UK: iSchool.
</li>
<li id="kri83">Krikelas, J. (1983). Information-seeking behaviour: patterns and concepts. <em>Drexel Library Quarterly</em>, 19(2), 5-20.
</li>
<li id="kuh91">Kuhlthau, C. (1991). Inside the search process - information seeking from the users perspective. <em>Journal of the American Society for Information Science</em>, 42(5), 361-371.
</li>
<li id="lux94">Luxenberg, L. (1994). <em>Walking the Appalachian Trail</em>. Mechanicsburg, PA: Stackpole Books.
</li>
<li id="mal60">Malinowski, B. (1960). <em>A scientific theory of culture and other essays</em>. Oxford: Oxford University Press.
</li>
<li id="mea34">Mead, G. H. &amp; Morris, C. W. (1934). <em>Mind, self, and society: from the standpoint of a social behaviorist</em>. Chicago, IL: University of Chicago Press.
</li>
<li id="mue98">Mueser, R. (1998). <em>Long-distance hiking: Lessons from the Appalachian Trail</em>. Camden, ME: Ragged Mountain Press.
</li>
<li id="sav95">Savolainen, R. (1995). Everyday life information seeking: Approaching information seeking in the context of way of life. <em>Library and Information Science Research</em>, 17(3), 259-294.
</li>
<li id="siu07">Siudzinski, R. (2007). <em>Not all who wander are lost: an ethnographic study of individual knowledge construction within a community of practice</em>. Unpublished doctoral thesis, Virginia: Virginia Polytechnic Institute and State University.
</li>
<li id="str90">Strauss, A. L. &amp; Corbin, J. M. (1990). <em>Basics of qualitative research: Grounded theory procedures and techniques</em>. Newbury Park, CA: Sage.
</li>
<li id="why43">Whyte, W.F. (1943). Street corner society; the social structure of an Italian slum, Chicago, IL: University of Chicago Press.
</li>
<li id="wil81">Wilson, T. D. (1981). On user studies and information needs. <em>Journal of Documentation</em>, 37(1), 3-15.
</li>
<li id="wil99">Wilson, T. D. (1999). Models in information behaviour research. <em>Journal of Documentation</em>, 55(3), 249-270.
</li>
<li id="wil97">Wilson, T.D. (1997). Information behaviour: an interdisciplinary perspective. <em>Information Processing and Management</em>, 33(4), 551-572.
</li>
</ul>

</section>

</article>