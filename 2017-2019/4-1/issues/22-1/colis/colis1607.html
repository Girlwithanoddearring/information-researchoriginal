<!DOCTYPE html>
<html lang="en">

<head>
	<title>The records perspective: a neglected aspect of information literacy</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="dc.title" content="The records perspective: a neglected aspect of information literacy">
	<meta name="dc.creator" content="Gillian Oliver">
	<meta name="dc.subject"
		content="The power of records and record keeping is a perspective that is largely missing from information literacy research and practice.  As records are influential at all levels of human existence, the consequences of not addressing this perspective, particularly for the least empowered individuals in society (such as refugees), can be profound. The two purposes of this paper are to raise awareness of records and record keeping in the information literacy community, and to explore why this situation has come about.">
	<meta name="dc.description"
		content="The power of records and record keeping is a perspective that is largely missing from information literacy research and practice.  As records are influential at all levels of human existence, the consequences of not addressing this perspective, particularly for the least empowered individuals in society (such as refugees), can be profound. The two purposes of this paper are to raise awareness of records and record keeping in the information literacy community, and to explore why this situation has come about.  Although archival or primary source literacy has been considered by the archival community (largely in terms of user education) literacy from the point of view of interacting with everyday records and records systems has not.  Issues of professional identify and culture are likely to have influenced the lack of awareness of the need for records literacy in everyday life, in particular the division of responsibilities for record keeping to records managers and archivists in the Anglophone world. There is a need for a research and practice agenda to introduce and embed awareness of records and record keeping in information literacy. ">
	<meta name="dc.subject.keywords" content="records management, information literacy">
	<meta name="robots" content="all">
	<meta name="dc.publisher" content="Professor T.D. Wilson">
	<meta name="dc.coverage.placename" content="global">
	<meta name="dc.type" content="text">
	<meta name="dc.identifier" content="ISSN 1368-1613">
	<meta name="dc.identifier" content="http://informationr.net/ir/22-1/colis/colis1607.html">
	<meta name="dc.relation" content="http://informationr.net/ir/22-1/infres221.html">
	<meta name="dc.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dc.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dc.date.available" content="2017-03-15">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<article>
		<header>
			<h4 id="vol-22-no-1-march-2017">vol. 22 no. 1, March, 2017</h4>
		</header>
		<h2
			id="proceedings-of-the-ninth-international-conference-on-conceptions-of-library-and-information-science-uppsala-sweden-june-27-29-2016">
			Proceedings of the Ninth International Conference on Conceptions of Library and Information Science,
			Uppsala, Sweden, June 27-29, 2016</h2>
		<h1 id="the-records-perspective-a-neglected-aspect-of-information-literacy">The records perspective: a neglected
			aspect of information literacy</h1>
		<h2 id="gillian-oliver"><a href="#author">Gillian Oliver</a></h2>
		<blockquote>
			<p><strong>Introduction.</strong> The power of records and record keeping is a perspective that is largely
				missing from information literacy research and practice. As records are influential at all levels of
				human existence, the consequences of not addressing this perspective, particularly for the least
				empowered individuals in society (such as refugees), can be profound. The two purposes of this paper are
				to raise awareness of records and record keeping in the information literacy community, and to explore
				why this situation has come about.<br>
				<strong>Results.</strong> Although archival or primary source literacy has been considered by the
				archival community (largely in terms of user education) literacy from the point of view of interacting
				with everyday records and records systems has not. Issues of professional identify and culture are
				likely to have influenced the lack of awareness of the need for records literacy in everyday life, in
				particular the division of responsibilities for record keeping to records managers and archivists in the
				Anglophone world.<br>
				<strong>Conclusions.</strong> There is a need for a research and practice agenda to introduce and embed
				awareness of records and record keeping in information literacy.</p>
		</blockquote>
		<section>
			<h2 id="introduction">Introduction</h2>
			<p>From the late twentieth century onwards, a defining feature of the information and library science domain
				has been its concern with information literacy. The resulting body of literature is vast, and there are
				many examples of implementations of research into practice, as evidenced by the involvement of
				professional bodies and the development of standards and guidelines. In contrast, there has been
				relatively little interest or awareness of information literacy concepts from the archival and record
				keeping community. This absence of awareness means that existing conceptions of information literacy do
				not adequately reflect issues relating to records and record keeping.</p>
			<p>The purpose of this paper is twofold. Firstly, to raise awareness of records and record keeping issues in
				the information literacy research community and to emphasize the need for research and practice in this
				area. Secondly, to explore reasons why records and record keeping have been overlooked.</p>
			<p>The paper begins by explaining what records are, and what records have the power to do. It then briefly
				summarises the body of work relating to information literacy from the archival science community, which
				is predominately concerned with user education services. The absence of a broader awareness of
				information literacy from a records perspective is attributed to the fractured nature of the record
				keeping community into two occupational groups: records managers and archivists. The critical need to
				empower individuals to understand and negotiate record keeping systems and to have the ability to
				develop their own archives is argued using the example of refugees. The paper concludes by calling for a
				research and practice agenda to introduce and embed awareness of records and record keeping in
				information literacy.</p>
			<h2 id="what-do-we-mean-by-%E2%80%9Crecords%E2%80%9D">What do we mean by “records”?</h2>
			<p>Information or data in any format can be a record, it is what that information does or what it has the
				potential to do that is the defining feature of “records”. So records will include familiar documentary
				forms associated with bureaucracies such as official certificates, licenses, identity papers, as well as
				associated correspondence such as emails. These records will allow people to do things, to travel, to
				claim benefits. But the more mundane, everyday personal memorabilia such as photos of family and friends
				may also be considered records. These records will trigger memories, of situations, people and events.
				From this very brief list of exemplars it is not difficult to get a sense of the enormous and wide
				ranging power of records, encompassing every aspect of human existence. They may establish legal
				identities and rights to live, work and be protected by societies, and also form the bed-rock of our
				emotional well-being and understanding of our personal histories, identities and relationships.</p>
			<p>Records are inextricably linked to activities, and are contextually and culturally contingent. Records
				are evidence or traces of actions, and as such authenticity, integrity, reliability and useability are
				key concerns for recordkeepers. The imperative to ensure these characteristics predicates the need for
				specialist record keeping metadata, sometimes mistakenly categorized as administrative metadata.
				Referring to record keeping metadata as ‘administrative’ obscures and minimises the complexity and
				sophistication of the schema and protocols that need to be developed and implemented. Complexity and
				sophistication are necessary attributes because of the requirement to map and convey the spider’s web of
				relationships (between people, events, places and times as well as other records) which provide the
				essential contextual frame for records and record keeping.</p>
			<p>The international standard on records management provides the following rather dry definition of records:
				<em>‘information created, received &amp; maintained as evidence and information by an organization or
					person, in pursuance of legal obligations or in the transaction of business’</em> (<a
					href="#int2002">International Organisation for Standardization, 2002</a>)</p>
			<p>However, this definition is slightly less dry if it is read with the understanding that the term
				‘business’ is used as a holistic concept, encompassing all types of activities and not restricted to a
				very narrow interpretation of context relating only to private enterprises. Any information associated
				with activity can be a record, the key concern is whether it is or will be required to provide evidence
				of that activity either in the present time or at some specified or unspecified point in the future.
				Providing evidence should be similarly broadly interpreted, as situations could range from legal
				requirements to for example establish place of birth to social or family requirements to communicate to
				future generations cultural roots in different places and times. Significance motivates the need to
				manage information as records, but that significance may not be apparent to the persons concerned until
				it is highlighted by an institution or individual, whether an official or a family member. To further
				complicate matters, significance may only be recognized in a far distant, unimagined future, rather than
				in today’s time and space.</p>
			<h2 id="information-literacy-and-archival-science">Information literacy and archival science</h2>
			<p>Concern from the record keeping discipline and community to date with information literacy can be
				categorized as focusing on the skills perspective identified by Addison and Meyers (<a
					href="#add2013">2013</a>). There is a relatively small but robust body of literature (for instance
				<a href="#gill1998">Gilliland Swetland 1998</a>; <a href="#gill1999">Gilliland Swetland <em>et al.</em>
					1999</a>, <a href="#yak2002">Yakel, 2002</a>, <a href="#yaak2003">Yakel and Torres, 2003</a> among
				others) which is concerned with the need for archival literacy, the knowledge, skills and expertise
				required to research and use archival records and collections, often referred to as primary sources.</p>
			<p>In general, concerns expressed in this body of literature relate to user education, the ability to access
				and use records within the context of an institution, the archives. The concept of ‘archival
				intelligence’ has been formulated to refer to the specialist skills and knowledge required to locate,
				access, navigate and use archival records (<a href="#yaak2003">Yakel and Torres 2003</a>). Another
				recurrent theme in the literature is the need to incorporate archival records in the curriculum, and
				this has been explored from school age (K-12) students and beyond (<a href="#gill1998">Gilliland
					Swetland 1998</a>; <a href="#gill1999">Gilliland Swetland <em>et al.</em> 1999</a>, <a
					href="#mal2008">Malkmus, 2008</a>, <a href="#car2009">Carini, 2009</a>; <a
					href="#roc2011">Rockenbach, 2011</a>). The impact of such programmes is also the subject of research
				and study (<a href="#duff2008">Duff and Cherry, 2008</a>; <a href="#kra2010">Krause 2010</a>; <a
					href="#bah2012">Bahde and Smedberg, 2012</a>; <a href="#dan2013">Daniels and Yakel, 2013</a>).</p>
			<p>However, the literacy needed to interact with all types of records, from the official to the personal in
				the everyday lives of individuals, has not been addressed. One reason for this omission can be
				attributed to the development in North America and elsewhere in the English-speaking world of two
				distinct occupations concerned with record keeping – archivists and records managers. It should be noted
				that the characteristics associated with these two occupations discussed below are not universally
				applicable. Occupational characteristics and motivations associated with managing current records in
				other parts of the world (such as Scandinavia) can be quite different as a result of distinct record
				keeping traditions.</p>
			<h2 id="two-record-keeping-occupations">Two record keeping occupations</h2>
			<p>In the early years of the twentieth century and the emergence of the modern office environment in the
				United States concerns about office administration and efficiency first started to become apparent. The
				rise in documentation that accompanied the mass mobilization of troops in the 1930s and the 1940s and
				beyond provided the impetus for the development of a new occupation, records management. Thus in North
				America and the United Kingdom, and countries influenced by these record keeping traditions such as
				Australia and New Zealand two professional groups developed, both concerned with records and record
				keeping, but with very different missions. In the 1970s, the distinction was explained as follows:</p>
			<blockquote>
				<p>The archivist serves the needs of the scholar, the historian, and posterity, whereas, the records
					manager serves the needs of business which is usually profit motivated and which is interested only
					in information that contributes to or protects that profit or the goals of the organization. To put
					it another way, the records manager is basically a business administrator and the archivist is
					basically a historian. (<a id="#bro1971">Brown, 1971</a>, cited in <a href="#ath1985">Atherton
						1985-86</a>, p.43).</p>
			</blockquote>
			<p>The principal motivator for the emergence of records management was the reduction of business costs
				(Dollar, 1993). Despite repeated calls for a single minded (rather than a records management/archival
				duality) approach to record keeping (see, for instance, <a href="#ath1985">Atherton, 1985-6</a>, <a
					href="#upw2013">Upward <em>et al.</em></a>, 2013) this fractured and fragmented situation continues.
				Thus the occupation that is most concerned with developing systems and processes to manage everyday
				records which may profoundly influence people’s lives and wellbeing is principally concerned with
				records from the perspective of the workplace, and organisational motivations for record keeping.
				Potential to realise cost savings for employers continues to be one of the major aims of records
				management even if much of the evidence of this has traditionally been anecdotal rather than proven (<a
					href="#bai2011">Bailey, 2011</a>).</p>
			<p>This is a very different context to the library environment referred to by Addison and Meyers (<a
					href="#add2013">2013</a>), where teaching information literacy skills was strongly associated with
				professional identity, and different again from the archival world where information literacy has been
				considered from the perspective of understanding and using archival or primary sources in teaching and
				research.</p>
			<p>The consequence of this is that the power of records and record keeping and their impact on people’s
				lives is largely unaddressed in the information literacy discourse. Awareness of this specific
				perspective has quite simply been missing. In the English speaking world at least, one possible reason
				for this is that the focus of the occupational group (records management) most concerned with everyday
				records has been limited to a very narrow, corporatist perspective.</p>
			<h2 id="does-this-matter">Does this matter?</h2>
			<p>Anne Gilliland has argued, <em>‘archives and recordkeepers have social and ethical responsibilities
					toward those individuals who are least empowered to engage with official records and record keeping
					practices or to maintain their own records’</em> ( <a href="#gill2015">2015, p.2</a>). It is in
				thinking about those least empowered individuals that the imperative for records literacy can be seen
				most clearly.</p>
			<p>To focus on just one instance, refugees provide a prominent example of the least empowered. The
				information needs, practices and environment of refugees have rightly been the focus of recent research
				in the information literacy domain (<a href="#llo2013">Lloyd <em>et al.</em>, 2013</a>; <a
					href="#qay2014">Qayyim <em>et al.</em> 2014</a>; <a href="#llo2015">Lloyd 2015</a>). Not
				surprisingly though, records and record keeping have not been addressed in this research. However,
				considering record keeping issues in the context of refugees highlights further information-related
				areas that, if addressed, have the potential to make a major contribution to the health, prosperity and
				well-being of this group of individuals and help ensure that opportunities to resettle can be maximized.
			</p>
			<p>or instance, being able to establish and prove identity and residence rights in a country is a
				pre-requisite for gaining access to the services that provide the infrastructure for daily life, from
				educational opportunities and healthcare to utilities, communication channels and shopping. The refugee
				resettlement process (which varies from country to country) may ensure that individuals are registered
				to participate in some or all of the major infrastructure points, but leave individuals (whose lack of
				understanding of new and unfamiliar ways of doing may be compounded by understandable wariness of
				bureaucracies and officialdom) to negotiate others. Whether or not that negotiation is successful will
				largely be dependent on the assistance of others, perhaps other community members who may themselves
				only have partial understanding (or even misunderstanding) of the record keeping processes involved, and
				the implications of those processes.</p>
			<p>Then there are the precious records that are the memories of former homes and loved ones. Anne Gilliland
				refers to an interview broadcast on Croatian media with a Syrian journalist who identified mobile phones
				as being among the most important possessions someone fleeing their homeland may carry, not only because
				of their communication capabilities, but also because of the photographs they contain ( <a
					href="#gill2015">2015, p.8</a>). Thinking about the need to ensure that these photographs can be
				kept and shared, now and into the far distant future so that future generations may see and understand
				their past, illuminates an urgent need for information literacy skills, in this case relating to digital
				curation and archiving practices.</p>
			<h2 id="conclusion">Conclusion</h2>
			<p>From the first mention of information literacy in the 1970s ( <a href="#zur1974">Zurowski, 1974</a>) to
				the present day the interest and involvement of information and library science academics and
				practitioners in the concept has grown exponentially. A simple search for the term “information
				literacy” in Google Scholar at the time of writing resulted in a list of over 110,000 publications.
				Given this level of interest and involvement it is not surprising to find the meaning of the concept is
				subject to debate and multiple understandings abound. At the last COLIS conference, Colleen Addison and
				Eric Meyers identified three distinct discourses on information literacy, namely:</p>
			<ul>
				<li>The acquisition of information related skills</li>
				<li>The cultivation of habits of mind, and</li>
				<li>Engagement in information-rich social practices (2012)</li>
			</ul>
			<p>Of particular interest from the perspective of our consideration of the involvement of archivists and
				records managers in information literacy are the connections that Addison and Meyers make between these
				discourses and practice and the professional jurisdiction of librarians:</p>
			<blockquote>
				<p><em>These perspectives are furthermore intimately connected with professional identity. The skills
						perspective arguably reinforces the authority of traditional librarianship to an extent greater
						than the others, and thus the professional identity of librarians and other information
						professionals (as the skills highlighted are taught in an Mlibrary and information science or
						other library degree, such as database searching). The other perspectives arguably reinforce the
						identity of the supported professions , i.e. domain experts and non-library and information
						science professions that deal with information in their unique way.</em> (<a
						href="#add2013">Addison and Meyers, 2013</a>).</p>
			</blockquote>
			<p>Awareness of the power of records and record keeping in people’s lives is largely unaddressed and
				unacknowledged in information literacy research and practice. This absence can be seen as a moral
				failure on the part of record keeping practitioners and researchers, a possible consequence of the
				contested and fractured nature of the archival science (broadly conceived) community. Given the
				predominant motivations to serve employers and governing bodies on the part of those responsible for
				managing records in current environments in much of the English speaking world, developing records
				literacy skills and programmes for citizens of the broader society is unlikely to be a priority for
				records managers working in the Anglo record keeping traditions. However, this cannot be allowed to hide
				or negate the need for those records literacy skills and programmes. The lack of records literacy can
				have significant adverse consequences for individuals, which come into sharp focus with the displacement
				of vast numbers of people forced to flee their homelands and attempting to establish new lives
				elsewhere.</p>
			<p>It is therefore proposed to develop a research and practice agenda to identify and implement strategies
				to address information literacy from a records perspective. Collaboration between the established
				information literacy community and record keeping specialists is seen as essential to achieving positive
				outcomes. For that to be successful, understanding of each other’s domains and perspectives will be
				critical.</p>
			<h2><a id="author"></a>About the author</h2>
			<p><strong>Gillian Oliver</strong> received her PhD in 2006 from the School of Information Management at
				Monash University. She is Associate Professor in Information Management at Monash University, 900
				Dandenong Rd, Caulfield East VIC 3145, Australia. Her research interests are in record keeping
				informatics with a particular focus on information culture. She can be contacted at: <a
					href="mailto:gillian.oliver@monash.edu">Gillian.oliver@monash.edu</a></p>
		</section>
		<section>
			<h2>References</h2>

			<ul>
				<li id="add2013">Addison, C. &amp; Meyers, E. (2013). Perspectives on information literacy: a framework
					for conceptual understanding Information Research, 18(3) paper C27. Retrieved from
					http://InformationR.net/ir/18-3/colis/paperC27.html (Archived by WebCite® at
					http://www.webcitation.org/6lDCZoKZ4)</li>
				<li id="ath1985">Atherton, J. (1985). From life cycle to continuum: some thoughts on the records
					management–archives relationship. Archivaria, 1(21).</li>
				<li id="bah2012">Bahde, A., &amp; Smedberg, H. (2012). Measuring the magic: assessment in the special
					collections and archives classroom. RBM: A Journal of Rare Books, Manuscripts and Cultural Heritage,
					13(2), 152-174.</li>
				<li id="bai2011"> Bailey, S. (2011). Measuring the impact of records management: data and discussion
					from the UK higher education sector. Records Management Journal 21, no. 1: 46-68.</li>
				<li id="bro1971"> Brown, G. (1971). The archivist and the records manager: A records manager’s
					viewpoint. Records Management Quarterly 5, p. 21.</li>
				<li id="car2009"> Carini, P. (2009). Archivists as educators: Integrating primary sources into the
					curriculum. Journal of Archival Organization, 7(1-2), 41-50.</li>
				<li id="dan2013"> Daniels, M., &amp; Yakel, E. (2013). Uncovering impact: the influence of archives on
					student learning. The Journal of Academic Librarianship, 39(5), 414-422.</li>
				<li id="duff2008">Duff, W., &amp; Cherry, J. (2008). Archival orientation for undergraduate students: an
					exploratory study of impact. The American Archivist, 71(2), 499-529.</li>
				<li id="gill2015">Gilliland, A (2015). Permeable binaries, societal grand challenges, and the roles of
					the twenty-first century archival and record keeping profession. Retrieved from
					http://escholarship.org/uc/item/90q5538g (Archived by WebCite® at
					http://www.webcitation.org/6lDClFpVD)</li>
				<li id="gill1998">Gilliland-Swetland, A. J. (1998). An exploration of K-12 user needs for digital
					primary source materials. The American Archivist, 136-157.</li>
				<li id="gill1999">Gilliland-Swetland, A. J., Kafai, Y. B., &amp; Landis, W. E. (1999). Integrating
					primary sources into the elementary school classroom: A case study of teachers' perspectives.
					Archivaria, 89-116.</li>
				<li id="int2002">International Organization for Standardisation (2002). ISO15489.1</li>
				<li id="kra2010">Krause, M. (2010). Undergraduates in the archives: using an assessment rubric to
					measure learning. The American Archivist, 73(2), 507-534.</li>
				<li id="llo2015">Lloyd, A. (2015). Stranger in a strange land; enabling information resilience in
					resettlement landscapes. Journal of Documentation, 71(5), 1029-1042.</li>
				<li id="llo2013">Lloyd, A., Kennan, M., Thompson, K. M., &amp; Qayyum, A. (2013). Connecting with new
					information landscapes: information literacy practices of refugees. Journal of Documentation, 69(1),
					121-144.</li>
				<li id="mal2008">Malkmus, D. J. (2008). Primary source research and the undergraduate: a transforming
					landscape. Journal of Archival Organization, 6(1-2), 47-70.</li>
				<li id="qay2014">Qayyum, M. A., Thompson, K. M., Kennan, M. A., &amp; Lloyd, A. (2014). The provision
					and sharing of information between service providers and settling refugees. Information Research,
					19(2).</li>
				<li id="roc2011">Rockenbach, B. (2011). Archives, undergraduates, and inquiry-based learning: Case
					studies from Yale University Library. The American Archivist, 74(1), 297-311.</li>
				<li id="yak2002">Yakel, E. (2002). Listening to users. Archival Issues, 111-127.</li>
				<li id="yaak2003">Yakel, E., &amp; Torres, D. (2003). AI: archival intelligence and user expertise. The
					American Archivist, 66(1), 51-78.</li>
				<li id="upw2013">Upward, F, Reed, B., Oliver, G. and Evans, J. (2013). record keeping informatics:
					re-figuring a discipline in crisis with a single minded approach. Records Management Journal, 23
					(1), 37-50. </li>
				<li id="zur1974">Zurkowski, P.G. (1974). The information service environment relationships and
					priorities. Washington, D.C.: National Commission on Libraries and Information Sciences </li>
			</ul>
		</section>
	</article>
</body>

</html>