<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Social media activism in Maldives; information practices and civil society

## [Hilary Yerbury](#author) and [Ahmed Shahid](#author)

> **Introduction.** The study was designed to explore the information practices of a group of human rights activists in a campaign seeking to pressure the police service and government into investigating the disappearance of a journalist in the context of transnational advocacy networking.  
> **Method.** The social media associated with a campaign in Maldives, Find Moyameehaa, were the basis for the case study. Tweets and Facebook posts and comments from the first 100 days of the campaign and from the 500th day were downloaded; the website was analysed.  
> **Analysis.** Content analysis of tweets, posts and comments was carried out using a priori coding.  
> **Results.** The tactics of transnational advocacy networking proposed by Keck and Sikkink were apparent in the campaign, however the everyday focus of the posts showed this to be a campaign of local concern. A second potential purpose for the campaign emerged, the modelling of civil engagement in a fledgling democracy.  
> **Conclusions.** The information practices approach, emphasising continuity and habitualisation following Savolainen, brings additional perspectives to understanding social media activism, showing how it can represent the behaviour of civil society and create an archive of a campaign and emphasising the importance of social and cultural factors.

<section>

## Introduction

In recent times, there has been considerable interest in the use of social media by ordinary citizens to create pressure for change in attitudes and approaches to human rights by those in power. There is considerable enthusiasm for the possibilities of social media enhancing opportunities for civic engagement ([Bakardjieva, 2009](#bak09); [Harrelson-Stephens and Callaway, 2014](#har14); [Gerbaudo, 2016](#ger16)), although at the same time, it is recognised that there are limits and problems in using social media for civic engagement ([Fuchs, 2012](#fuc12)). This paper explores the information practices of people engaged in a social media campaign established in Maldives after a journalist disappeared in August 2014 to put pressure on the government to conduct an investigation. Studies of social media have tended to focus on the messages, the users or the technology ([Williams, Terras and Warwick, 2013](#wil13)), but this study is concerned with information practices in the context of human rights activism. Information practices assume that people are knowledgeable, engaged in a social context in actions arising in everyday life ([Mckenzie, 2003](#mck03)). Savolainen ([2007, p. 26](#sav07)) described information practices as the ‘_ways people deal with information_’ in a context of ‘_the continuity and habitualisation of activities affected and shaped by social and cultural factors_’. The social and cultural factors in the context of this study include concerns for human rights activism and for the development of a civil society. Thus, the study uses the information-based models of Keck and Sikkink ([1999](#kec99)) and Castells ([2012](#cas12)) to understand these information practices. For Keck and Sikkink, this may be achieved through transnational activism using the internet and for Castells, through the development of a globalised social movement using social media.

In the early days of internet-based activism, in a seminal work ([Magrath, 2015](#mag15)), Keck and Sikkink ([1999](#kec99)) highlighted the importance of information and its communication in movements for human rights change. Seeing the importance of the internet, with its websites and email, they were able to take a global perspective to activism and developed a typology of information-based tactics to represent the ways in which local and transnational actors could engage on human rights issues. They proposed four categories of tactics: (i) information politics, the ability to move information to where it will best support the campaign; (ii) symbolic politics, the ability to call on symbols, actions and stories to make sense of a situation; (iii) leverage politics, the ability to call on powerful actors to lend their support and influence; and (iv) accountability politics, the effort to compel powerful actors to uphold their commitments and responsibilities.

More recently, Castells ([2012](#cas12)) has stressed the importance of emotion as the catalyst for political action through the development of a social movement. He uses the phrase cognitive resonance to bring together the emotions of outrage with hope for change, and to express the idea that there is a point at which emotion transforms into action, shared feelings are identified and a sense of togetherness is created. He also emphasises the need for an effective communication channel for expressing concern which must be able to link people and convey both the actions and the emotions ([2012, p. 14](#cas12)). This linking process creates a networked space that enables ideas, information and proposals for change to flourish.

</section>

<section>

## Information and emotion in social media activism

Information as evidence is fundamental to effective activism ([Keck and Sikkink, 1999](#kec99)) and information politics can move through a multiplicity of channels, including those controlled by citizens ([Brysk, 2013, p.133](#bry13)). Social media brings political participation within the grasp of the everyday lives of ordinary people, who are able to engage in a controversy and who are able to act as a collective in opposition to problems of ‘_structural conditions_’ ([Bakardjieva, 2015, p. 985-986](#bak15)).

From a human rights practitioners’ perspective, the focus in a campaign is usually framed in a legal or quasi-legal way and is based on evidence, on facts as reported by witnesses. From this perspective, the work of Amnesty International in establishing practices for reporting alleged incidents of violations of human rights is well known ([McClintock, 2011](#mcc11)). These practices are based on principles which assume that there is some ‘_hard evidence_’ ([Guzman and Verstappen, 2003, p. 12](#guz03)), there is someone bearing witness to the violation of human and Verstappen, 2003, p. 12), there is someone bearing witness to the violation of human rights; and that the claim has been validated through the support or acceptance of a human rights organisation.

Sharing information is important in the forming of activist networks ([Keck and Sikkink, 1999](#kec99)). That information can be procedural information about meetings, rallies and protest actions but it can also include information on the support for a campaign. Kharroub and Bas ([2015](#kha15)), in a study of the Twitter images of the 2011 Egyptian revolution, found that images showing the number and type of people engaged in protest actions were prevalent. Zhou _et al_. ([2012](#zho11)) have argued that it may be that, in repressive regimes, individuals need to have a sense of who is already engaged in collective action in order to overcome the fear of threat to themselves if they take action. Badge-wearing has long been considered a symbol of collective concern, and similar approaches have been adopted into social media. In Twitter, for example, a twibbon or digital ribbon can be added to one’s profile picture or, in a more extreme example, one can replace one’s own profile picture to adopt the avatar of a cause.

Emotion is also significant in prompting social action. Papacharissi and de Fatima ([2012](#pap12)) showed the importance of affect in their study of the Twitter posts on #egypt, after the resignation of Hosni Mubarak in February 2011, where emotional content was prevalent, through the repetition and retweeting of statements expressing solidarity and optimism for social change. Treré ([2015](#tre15)) noted that humour and expressions of creativity such as poetry were an integral part of information communicated during political resistance. Humour can be particularly effective in situations where it may be necessary to avoid the scrutiny of censors, where democratic freedoms are not fully enjoyed. Xiao, for example, shows how the Chinese use satire and jokes to criticise the ruling party ([Diamond and Plattner, 2012, p. xviii](#dia12)).

</section>

<section>

## The case study context

Maldives, an island nation in the Indian Ocean, is a fledgling democracy, where human rights acculturation and change driven by concerted efforts of local and transnational actors stalled in the wake of political unrest in 2012 ([Shahid and Yerbury, 2014](#sha14)). On 13 August 2014, the online newspaper, _Minivan News_ (now _Maldives Independent News_) reported that Ahmed Rilwan Abdulla, a journalist well known by his social media nickname, Moyameehaa, and recognised as a human rights activist, a former staff of the Human Rights Commission of Maldives, had disappeared, not having been seen by his family and friends since 7 August. A few days earlier, on 4 August 2014, Rilwan had reported on death threats received by journalists reporting on gang-related activities.

As soon as his disappearance was reported, and in the face of inaction from the police in investigating his disappearance, a website ([http://www.findmoyameehaa.com](http://www.findmoyameehaa.com)) was established, to act as a source of record, containing material ‘_made available for and by friends and family of Ahmed Rilwan (@moyameehaa) to help find him urgently_’. Social media were also activated. A Twitter hashtag, #FindMoyameehaa began to be used from 14 August 2014\. On 17 August, a Facebook page, FindMoyameehaa was established ([https://www.facebook.com/findmoyameehaa](https://www.facebook.com/findmoyameehaa)) and on 24 August, a YouTube channel was set up ([http://www.youtube.com/channel/UCDkpcl8oRtEVn8PqT7FMRnQ](http://www.youtube.com/channel/UCDkpcl8oRtEVn8PqT7FMRnQ)). On 21 December 2015, 500 days since his disappearance, he was still missing and no action appeared to have been taken by the police, parliament or the government.

</section>

<section>

## Methodology

The exploration of the information practices of those involved in this social media campaign focuses on the postings from the inception of the Find Moyameehaa campaign to 16 November 2014, the one hundredth day after Rilwan’s disappearance. It also includes the Facebook posts and tweets, from 20-21 December 2015, the 500th day since his disappearance.

The content of posts identified by Twitter as the _top tweets_ using the hashtag #FindMoyameehaa for the first hundred days was downloaded using Twitter’s Archive tool, which allowed the authors to sort twitter texts and save them for further analysis. The time interval of the first 100 days was identified as key to understanding the evolution of the campaign. The 500th day of the campaign was chosen as a significant milestone in the campaign and as a clear vantage point to appraise the effect of time on the campaign. Twitter does not state the precise parameters it uses to determine _top tweets_, but notes that these include tweets posted by people who are active tweeters and those with significant retweets. The 1,180 tweets from the first 100 days and the 100 tweets marking the 500th day were in a mixture of English and Dhivehi, the local language. The Facebook posts and comments, similarly written in English and Dhivehi, using Roman script and Thaana, were also downloaded. The YouTube channel was not included in the analysis.

The information in the website and the YouTube was posted by family and friends of Rilwan. The Facebook posts were also generated by family and friends, but elicited many comments. The twitter hashtag could be used by anyone to join in the conversation. The Facebook posts and comments and the tweets were analysed using a priori coding developed from literature (e.g. [Castells, 2012](#cas12); [Keck and Sikkink, 1999](#kec99)). The coding frame contained two parts, the first related to Keck and Sikkink’s four categories of tactics: statements of fact; calls for support and arrangements for collective action; the use of symbols, including humour and cultural expressions to support the message; addresses to the police, government ministers or the president. The second part derived from Castells’s notions of expressions of outrage and hope; this included expressions of loss, sadness, frustration, anger and disappointment and expressions of optimism. Also, following Castells, it included examples of creating a sense of collective.

Coding of Facebook posts, comments and tweets cannot follow the conventions of content analysis with coding at the sentence or paragraph level. It is rare to find a sentence even in a Facebook post and therefore the posts, comments and tweets were coded as a whole, but with the recognition that even a 140 character expression can be complex and contain more than one significant idea. Further, the images contained in posts and comments and most were also coded. While content analysis software such as NVivo could have been useful in coding and structuring the analysis, the mixed language and scripts as well as challenges in sorting tweets made it more appropriate to use a low tech methodology of pen and paper.

</section>

<section>

## Findings

The findings present the information practices of participants in the campaign, within a social and cultural context.

</section>

<section>

### The people involved

The friends and family of the missing journalist are responsible for the campaign Find Moyameehaa and the use of the social media tools. The number of participants, although small in number, is relatively significant given the population of Maldives, and has been relatively stable; at 12 December 2014, the Facebook page had received 10, 472 likes; on 22 December 2015, it had received only an additional 40 likes, with a total of 10,712.

This study is not concerned with the identity of those taking part in the campaign. Most are taking part in their personal capacities, without identifying their professional affiliations and past roles, although a number do have public profiles as human rights activists and journalists. While some individuals do identify themselves by their current or past institutional affiliations, others use pseudonyms which provide them with a level of anonymity. These past and present affiliations give the individuals a discernible personal authority, especially in leading and guiding the campaign. As one participant notes: ‘_We are not a bunch of nobodies without voice or an audience_’ (Facebook post, 23 August 2014). A Twibbon has been adopted by 3,199 people. Further, the campaign has attracted attention and action from a number of significant activist organisations, as detailed in the next section.

</section>

<section>

### Information in the campaign – making something of nothing

The website acts as a source of record, a summary of the statements by NGOs and other organisations are posted on the website, with links to other key documents, such as newspaper articles, blog posts, statements from NGOs and responses from the Maldivian government. It is also in a mixture of languages and scripts. However, this is a campaign based on very little direct evidence. The banner of the website contains the one incontrovertible fact of the campaign, that Rilwan has disappeared, and records the number of days, hours, minutes and seconds since his disappearance. This record of the time since his disappearance is a refrain throughout the tweets, accounting for 315 unique tweets (27%) in the first 100 days and 53 of the 100 tweets on the 500th day. This information is also represented symbolically, through the fading of the missing person posters ‘_The old posters fade but we do not forget_’ 16 November 2014 and ‘_They thought he would fade away like the posters. No chance_’ 21 December 2015.

The publicness of this process of drawing attention to the disappearance of Rilwan and the lack of response from the Maldives Police Service acted as leverage (cf. [Keck and Sikkink, 1999](#kec99)) on other organisations within and outside of the local context of Maldives. Thus, local organisations such as the Maldivian Democracy Network and the Human Rights Commission of Maldives and overseas-based organisations, such as Reporters without Borders and the Committee to Protect Journalists, were made aware of the situation and attracted to the campaign. They in turn acted as multipliers of the message, similarly calling the Maldives Police Service (MPS) and the government to account, from the perspectives of human rights and democratic processes. Other organisations involved in spreading the message included: Maldivian news media, Al Jazeera, the International Federation of Journalists – Asia Pacific, the South East Asia Press Alliance, The Centre for Law and Democracy, and Freedom Watch Maldives. The Parliaments of Canada, the United Kingdom and Australia, following traditions of inter-governmental relationships and diplomacy in the British Commonwealth, called on the President of Maldives to ensure that the rule of law was followed and democratic traditions upheld.

Amnesty International, with a long tradition of working with Maldives to highlight human rights abuses, issued a statement ([2014](#amn14)) detailing a number of cases of violence and physical abuse, including the disappearance of Rilwan. This statement reminded the Maldivian government of its obligations under the international human rights instruments it has ratified and urged the police to carry out prompt and rigorous investigations and bring those responsible to justice. The Commonwealth Human Rights Initiative also called the government to account for their inaction and lack of communication through its presentation to the UN Human Rights Council’s Panel on the Safety of Journalists ([Asian Forum, 2014](#asi14)).

</section>

<section>

### Keeping people informed and doing things

Tweets and the Facebook page contain information about numerous plans for action. In the first 100 days, 293 out of 1,180 tweets suggest or support actions. These include the so-called Question March and demonstrations, signing petitions and praying; they also include reading the report commissioned by the Maldives Democracy Network (MDN), helping to make and distribute posters and flyers and showing solidarity across what might be considered social or political divides. On the 500th day, people were invited to join Rilwan’s friends at a café to engage in one of Rilwan’s favourite leisure time activities, origami, making 500 paper cranes to be delivered to the President.

Not all information-based actions were acknowledged as actions in the wider community. A petition with over 5,000 signatures was presented to the Maldivian Parliament, the Majlis; posts on Facebook and Twitter note that this number of signature represents more than the number of voters in any electorate in the country. Notwithstanding this significant expression of concern, the petition was rejected by the Parliament, ‘_quoting bogus bureaucratic crap_’ (Facebook post 28 October 2014), much to the dismay of many: ‘_Over 5000 of us Maldivians signed a petition asking @mvpeoplesmajlis to pressure authorities to #FindMoyameehaa. They threw it out. #100days_’ 15 November 2014.

</section>

<section>

### Addressing authority

According to media reports, during the first 100 days of the case, the Maldives Police Service gave very little information on the progress of the investigation. Twitter facilitates accountability politics, through the use of the @ function. More than 16% of the tweets in the first 100 days are addressed to the Maldives Police Service, @policemv, either noting that they should make greater effort to Find Moyameehaa, accusing them of not doing their jobs effectively or asking what they are doing. ‘_Day 31.@PoliceMv FAILS repeatedly n consistently to #FindMoyameehaa. Incompetence and negligence bleeds [sic] public trust_’ 8 September 2014\. Other messages are directed to the Commissioner of Police and to the Home Minister, calling them to account, often with messages that indicate that they have not carried out their role effectively. ‘_@UmarNaseerPPM where is @moyameehaa? we need answers!_’ 21 December 2015\. Inaction from the police and government led to a number of external bodies, including Amnesty International, calling the government to account, as the website shows.

</section>

<section>

### Using skills and talents

People used more than words to express their concerns both in Facebook and in Twitter. Apart from the posters, in the first hundred days cultural artefacts include sketches, poems, a wordle and a song. The song written and performed by Mezzomohamed and posted on YouTube and in Soundcloud (_Moyayas Hevey Dheyshey_) is a poignant reminder of the sense of loss that some Maldivians were feeling. It has been retweeted, reposted and downloaded hundreds of times.

Participants in the social media campaign also used humour to maintain their message. There is banter around the creation of a _gang_ with Rilwan as its _leader_. On 22 October 2014, a tweet recorded that it had only taken five days to find a criminal who is a gang member, leading the Home Minister to remark that it was hard to find Rilwan since he was not a gang member. Within a very short time, a tweet appeared announcing ‘_October 22 oh well, rilwan belongs to #SosalMediaGang too #Homeminister now #FindMoyameehaa #NoExcuses_’. The joking continued but was tinged with loss and sadness: ‘_October 23 Lonely coffee all alone with #SosalMediaGang thinking of how to #FindMoyameehaa_’; and ‘_November 5 90 days of failure. And @moyameehaa's only fault is that he isn't a gang member. #FindMoyameehaa_’.

</section>

<section>

### Expressing emotions

Rilwan’s family, friends and close associates are also instrumental in communicating the impact of lack of information by constantly bringing an emotional element of sadness and loss to the tweets: ‘_Shock, anger, sadness, confusion and holding on 2 all hope_’ 15 October 2014; ‘_100 days of incompetence and excuses by @PoliceMv. 100 days of pain for a family determined to find the truth. #FindMoyameehaa_’ 15 November 2014; ‘_500 days of sorrow_’ 21 December 2015\. Posts remind everyone that this protest is about a human life and justice for the family and friends: ‘_A mother torments [sic] not knowing whether her child is dead or alive_’16 September 2014; ‘_Seeing her [Rilwan’s mother] is always heartbreaking_’ 16 November 2014\. A recurring image in the tweets and in Facebook is a photo of Rilwan’s mother, with a look of grief and despair on her face, holding out her hand in a gesture of supplication and prayer. It encapsulates the sorrow of all mothers whose children disappear and who are left with no answers; further, it conveys societal outrage at her suffering and her treatment by the police and institutions of government.

</section>

<section>

### Doing the everyday

The tweets and Facebook posts show that family and friends of Rilwan are taking actions which are far from their everyday practices: organising marches, seeking meetings with politicians, bringing complaints against the police. Yet, participants in this campaign have maintained a strong link with the everyday reality of life in Maldives. The faded posters are an everyday sight for those living in Male’, and a constant reminder of the missing journalist. Minivan News, Rilwan’s employer, (now known as Maldives Independent News) carried his photo on the splash page of its website, maintaining the issue of safety for journalists as a news item for many months. Friends go to the same cafés and note his absence. Tweets link Rilwan’s case to events in local politics: Rilwan: ‘_#Maldives journalist missing for 500 days while Dunya @MDVForeign sings Rule of Law, Human Rights and Justice!_’ 21 December 2015.

This social media campaign and the information practices of the people engaged in it need to be understood in the context of Maldives. Challenges to democratic processes and attacks on freedom of speech had increased since early 2012 ([Harris, 2015](#har15)) and these were deemed serious enough to cause concerns among the governments of other countries ([European Parliament, 2015](#eur15)). This is the context of Rilwan’s disappearance and the subsequent campaign to draw attention to his disappearance and lack of action from the Maldives Police Service. This is, therefore, an important part of the context in which these information practices must be considered.

At one level, the information practices of people engaged in the campaign match the practices of those engaged in social media activism as reported in the literature: digital badge-wearing through the Twibbon, expressions of emotion, humour, the use of cultural forms such as poetry and song. The practices are clearly, as Bakardjieva describes ([2015, p. 985-986](#bak15)), an example of political participation by ordinary people in the context of their everyday lives. They also appear to be examples of the tactics of transnational advocacy networking identified by Keck and Sikkink ([1999](#kec99)). However, to understand them more fully, it is important to go beyond a simple checklist approach to identifying these practices as being of one type or another.

The key message, showing continuity over the period of five hundred days, is a local message, #FindMoyameehaa @PoliceMv. There is a certain sameness in the majority of the posts and tweets – they echo the number of days since Rilwan went missing, they call on the police to take action, they call for prayer or sympathise with the situation of his family, especially his mother. This consistency suggests that people contributing posts and tweets are not working entirely within a personal action frame. There are clearly close personal and professional relationships among many of the participants. There was a strong commitment to the purpose of the campaign, Find Moyameehaa, indicated through the likes, the adoption of the Twibbon and level of participation in activities. The consistency in the posts might also suggest a strong managing and monitoring role in the leadership team comprising Rilwan’s friends and relatives. Early in the campaign, a comment introduced a different but related issue, and an immediate response was a request to stick to the main point and not to be side-tracked.

The information practices used in this social media campaign are clear examples of the tactics described by Keck and Sikkink ([1999](#kec99)) as being fundamental to the establishment of a transnational advocacy network. The activities of participants in the social media campaign cover all four categories of tactics. One can argue that a transnational advocacy network formed – linking Maldivians concerned with a local matter to international NGOs and even to the governments of other countries – but within this network, each player, whether individual or organisation, is working towards his or her own objectives. On the one hand, this is an issue of international human rights importance. On the other hand, this is a local issue and local people are expressing _their_ concerns in personal and collective ways, and calling on _their_ police service and then on _their_ government to help them to manage an issue which has personal and societal dimensions.

This focus on the local means that the expressions of emotion, which are an integral part of the information practices of people involved in this campaign, are almost always personal, rather than concern for an abstract notion of injustice. Efforts are made by external groups to link this occurrence to other abuses of human rights; their contributions make them part of this group, but these contributions are of a completely different kind from all other Facebook comments and tweets, so that emotions expressed are mostly personal and the remedy is local.

All the posts and tweets are in the public eye, including the expressions of emotion, even though these might seem private in the first instance, being comments setting out the despair of an individual and not expecting a response from anyone else. In this way, they become part of a collective message, supporting Wellman _et al_.’s argument ([2001](#wel01)) that social media posts are personal and collective at the same time.

The message itself, Find Moyameehaa, calls on the police and politicians to respond but as posts and tweets continue to show (‘_Refusal to address issues – shunning respnsblty [sic] is poor governance … @PoliceMv_’ 20 December 2015), there was little response from the police, no engagement from the Parliament and therefore nothing from which to create a dialogue. Even if the spaces created by Facebook and Twitter are seen as creating a public sphere where citizens might be able to shape debate ([Castells, 2008, p. 86-87](#cas08)), there is no discussion, no need to clarify positions and with no response from the Maldivian Police Service or any other part of government, there is no scope for deliberation (cf. [Habermas, 1989](#hab89)) and no debate.

There has also been no trolling. There have been messages from time to time suggesting that the actions of the supporters of this campaign are futile and that the outcome should be left to the will of god: ‘_Better to make a DUA for the person missing. Prayers are only best weapon for anything …_’ (16 September 2014). This lack of opposition is surprising in a context where many people taking part in this campaign have received threats and been subjected to harassment and violence ([Find Moyameehaa Team, 2015](#fin15)). There are several potential explanations. Maldives is a small country and those trolling would be identified and then might be shamed; those who might oppose the calls on police and government to take action are following the lead of police and not responding; in Facebook, the page’s administrators may remove posts deemed inappropriate and in Twitter, individuals may have been blocked.

The key to understanding the information practices of the participants in this social media campaign in the context of Maldives may lie in two words that Savolainen ([2007](#sav07)) uses in his discussion of information practices: continuity and habitualisation. The themes and content of posts, comments and tweets have continuity – they have not varied from the establishment of the campaign. Participants in the campaign, members of government and the police are used to the messages – everyone is habituated to the content. Similarly, the form of the messages largely determined by the technologies does not vary. Finally, the tone in which the content of the messages is expressed does not vary.

What, then, are the consequences of people becoming habituated to this approach to expressing a significant human rights message? Risse and Sikkink ([1999](#ris99)) state that human rights can only be fully realised in a country when the practices have become part of the vernacular, habitualised into the everyday lives of citizens and they recognise that at times societal institutions and governments will not be receptive to appeals from citizens. In this campaign, one can see the modelling of a form not just of political engagement but of civil engagement. In this context, these can be seen as information practices which establish the norms of a democratic civil society. They raise an issue and call those in power to account; they keep that issue in the public eye, constantly reminding readers of the gaps and lacks in institutional processes. The Facebook posts and comments and, somewhat surprisingly, the tweets, maintain a civility of address, polite but firm, joking even satirical, but never offensive. Secondly, the use of the technologies has enabled a collection to emerge, bringing together all the posts and tweets, whether intended as private or public statements, and the links to the statements, speeches and questions made by external bodies. The website, in particular, presents a powerful record of an attempt to bring about the behaviour and practices on which a respect for democratic processes and the recognition of human rights rests.

</section>

<section>

## Conclusion

This study has explored the information practices of a group using social media to disseminate information about the unexplained disappearance of a young Maldivian journalist and to apply pressure to the Maldivian government to carry out a thorough investigation and keep the public informed. Unlike most other studies of social media activism, it has focussed not on the activism itself, nor on the logic of a particular technology, but on the practices of those engaged in the activist campaign. It has brought together three conceptual approaches, the notion of information practices, tactics for information-based human rights change and the importance of emotion in developing movements for social action. It has shown how continuity and habitualisation, elements of information practices, can bring an additional perspective to how social media activism may be understood. In particular it has suggested that the information practices in the context of these social and cultural factors are significant because they encapsulate the norms of democratic civil society.

The study has also shown how the use of social media adds additional dimensions to Keck and Sikkink’s typology of tactics for information-based human rights change ([1999](#kec99)). Significantly, the social media technologies and the interplay between them in this instance, have moved the campaign and its actions beyond the ephemeral communication of the moment, to create a collection of images and thoughts, photographs, sounds and statements which function as an archive, which represents action and emotion, the personal and the communal, the local and the international. Further research might explore the role of this and similar archives in the development of campaigns for social justice.

## <a id="author"></a>About the authors

**Hilary Yerbury** is an Adjunct Professor in the Faculty of Arts and Social Sciences at the University of Technology Sydney with a long-standing interest in issues of human rights. She can be contacted at [Hilary.Yerbury@uts.edu.au](mailto:hilary.yerbury@uts.edu.au).

**Ahmed Shahid recently** completed his Doctor of Philosophy degree at the University of Sydney in the area of human rights. He has years of engagement in the fields of human rights and public policy. He can be contacted at [ahmed.shahid@sydney.edu.au](mailto:ahmed.shahid@sydney.edu.au).

</section>

<section>

## References

<ul>
<li id="amn14">Amnesty International (2014). Public statement: Maldives: bring to justice those behind abductions, death threats and violence. (ASA 29/002/2014, 17 October 2014). Retrieved from <a href="https://www.amnesty.org/en/documents/asa29/002/2014/en/" target="_blank">https://www.amnesty.org/en/documents/asa29/002/2014/en/</a> (Archived by WebCite® at http://www.webcitation.org/6j8K0ROSg)
</li>
<li id="asi14">Asian Forum for Human Rights and Development (2014). Item 2 &amp; 3 General debate: introduction of OHCHR report on the Human Rights Council panel discussion on the safety of journalists. Retrieved from: <a href="https://www.forum-asia.org/?p=17665" target="_blank">https://www.forum-asia.org/?p=17665</a> (Archived by WebCite® at http://www.webcitation.org/6j8FaPbeM)
</li>
<li id="bak09">Bakardjieva, M. (2009). Subactivism: lifeworld and politics in the age of the internet. The Information Society, 25(2), 91-104.
</li>
<li id="bak15">Bakardjieva, M. (2015). Do clouds have politics? Collective actors in social media land. Information, Communication and Society, 18(8), 983-990.
</li>
<li id="bry13">Brysk, A. (2013). Speaking rights to power; constructing political will. Oxford: Oxford University Press.
</li>
<li id="cas12">Castells, M. (2012). Networks of outrage and hope: social movements in the Internet age. Cambridge: Polity Press.
</li>
<li id="cas08">Castells, M. (2008). The new public sphere: global civil society, communication networks and global governance. The Annals of the American Academy of Political and Social Science, 616, 78-93.
</li>
<li id="dia12">Diamond, L. &amp; Plattner, M. (2012). Liberation technology; social media and the struggle for democracy. Baltimore, MD: Johns Hopkins University Press.
</li>
<li id="eur15">European Parliament (2015) Situation in the Maldives. P8_TA-PROV(2015)0180. Retrieved from <a href="http://www.europarl.europa.eu/sides/getDoc.do?type=TA&amp;language=EN&amp;reference=P8-TA-2015-0180" target="_blank">http://www.europarl.europa.eu/sides/getDoc.do?type=TA&amp;language=EN&amp;reference=P8-TA-2015-0180</a> (Archived by WebCite® at http://www.webcitation.org/6j8GWddhl)
</li>
<li id="fin15">Find Moyameehaa Team (2015, August 8). A year ago today, our brother, friend and fellow reporter disappeared. Maldives Independent News. Retrieved from <a href="http://maldivesindependent.com/feature-comment/a-year-ago-today-our-brother-friend-and-fellow-reporter-disappeared-116219" target="_blank">http://maldivesindependent.com/feature-comment/a-year-ago-today-our-brother-friend-and-fellow-reporter-disappeared-116219</a> (Archived by WebCite® at http://www.webcitation.org/6j8GidX8r)
</li>
<li id="fuc12">Fuchs, C. (2012). Social media, riots and revolutions. Capital and Class, 36(3), 383-391.
</li>
<li id="ger16">Gerbaudo, P. (2016). Rousing the Facebook crowd: digital enthusiasm and emotional contagion in the 2011 protests in Egypt and Spain. International Journal of Communication, 10, 254–273
</li>
<li id="guz03">Guzman, M. &amp; Verstappen, B. (2003). What is documentation? Versoix: Huridocs. Retrieved from <a href="http://www.huridocs.org/wp-content/uploads/2010/08/whatisdocumentation-eng.pdf" target="_blank">http://www.huridocs.org/wp-content/uploads/2010/08/whatisdocumentation-eng.pdf</a> (Archived by WebCite® at http://www.webcitation.org/6j8HIZp7Z)
</li>
<li id="hab89">Habermas, J. (1989). The structural transformation of the public sphere: An enquiry into a category of bourgeois society. Cambridge: Polity Press.
</li>
<li id="har14">Harrelson-Stephens, J. &amp; Callaway, R. (2014). You say you want a revolution: the Arab Spring, norm diffusion, and the human rights regime. Human Rights Review, 15(4), 413-431.
</li>
<li id="har15">Harris, G. (2015, April 6) Maldives’ transition to democracy appears increasingly precarious. New York Times. Retrieved from <a href="http://www.nytimes.com/2015/04/07/world/asia/maldives-transition-to-democracy-appears-increasingly-precarious.html?_r=0" target="_blank">http://www.nytimes.com/2015/04/07/world/asia/maldives-transition-to-democracy-appears-increasingly-precarious.html?_r=0</a> (Archived by WebCite® at http://www.webcitation.org/6j8H63Mst)
</li>
<li id="kec99">Keck, M. &amp; Sikkink, K. (1999). Transnational advocacy networks in international and regional politics. International Social Science Journal, 51(159), 89-101.
</li>
<li id="kha15">Kharroub, T. &amp; Bas, O. (2015). Social media and protests: an examination of Twitter images of the 2011 Egyptian revolution. New Media and Society, February 23, 2015.
</li>
<li id="mag15">Magrath, B. (2015). Information politics, transnational advocacy and education for all. Comparative Education Review, 59(4), 666-692.
</li>
<li id="mcc11">McClintock, M. (2011). The standard approach to human rights research. Retrieved from <a href="http://humanrightshistory.umich.edu/files/2012/10/McClintock1.pdf" target="_blank">http://humanrightshistory.umich.edu/files/2012/10/McClintock1.pdf</a> (Archived by WebCite® at http://www.webcitation.org/6j8HYNVH8)
</li>
<li id="mck03">Mckenzie, P. (2003). A model of information practices in accounts of everyday-life information seeking. Journal of Documentation, 59(1) 19-40.
</li>
<li id="min14">Minivan News journalist reported missing. (2014, August 13). Minivan News. Retrieved from <a href="http://minivannewsarchive.com/politics/minivan-news-journalist-reported-missing-89591?utm_source=dlvr.it&amp;utm_medium=twitter#sthash.7Begxize.dpbs" target="_blank">http://minivannewsarchive.com/politics/minivan-news-journalist-reported-missing-89591?utm_source=dlvr.it&amp;utm_medium=twitter#sthash.7Begxize.dpbs</a> (Archived by WebCite® at http://www.webcitation.org/6j8HinOQI)
</li>
<li id="pap12">Papacharissi, Z. &amp; de Fatima Oliveira, M. (2012). Affective news and networked publics: the rhythms of news storytelling on #Egypt. Journal of Communication, 62(2), 266-282.
</li>
<li id="ril14">Rilwan, A. (2014, August 4). 15 journalists receive death threats over gang reporting. Minivan News, available at: <a href="http://minivannewsarchive.com/crime-2/15-journalists-receive-death-threats-over-gang-reporting-89404#sthash.cMDjDURA.dpbs" target="_blank">http://minivannewsarchive.com/crime-2/15-journalists-receive-death-threats-over-gang-reporting-89404#sthash.cMDjDURA.dpbs</a> (Archived by WebCite® at http://www.webcitation.org/6j8Hqv5xr)
</li>
<li id="ris99">Risse T. &amp; Sikkink, K. (1999). The socialization of international human rights norms into domestic practices. In T. Risse, S. Ropp and K Sikkink (Eds.). The power of human rights: international norms and domestic change (pp. 1-38). Cambridge: Cambridge University Press.
</li>
<li id="sav07">Savolainen, R. (2007). Information behavior and information practice: reviewing the “umbrella concepts” of information-seeking studies. Library Quarterly, 77(2), 109-132.
</li>
<li id="sha14">Shahid, A. &amp; Yerbury, H. (2014). A case study of the socialization of human rights language and norms in Maldives: process, impact and challenges. Journal of Human Rights Practice, 6(2), 281-305.
</li>
<li id="tre15">Treré, E. (2015). Reclaiming, proclaiming, and maintaining collective identity in the #YoSoy132 movement in Mexico: an examination of digital frontstage and backstage activism through social media and instant messaging platforms. Information, Communication and Society, 18(8), 901-915.
</li>
<li id="wel01">Wellman, B., Quan-Haase, A., Witte, J. &amp; Hampton, K. (2001). Does the internet increase, decrease or supplement social capital? Social networks, participation and community commitment. American Behavioral Scientist, 45(3) 437-456.
</li>
<li id="wil13">Williams, S.A., Terras, M. &amp; Warwick, C. (2013). What people study when they study Twitter: classifying Twitter related academic papers. Journal of Documentation, 69(3) 384-410.
</li>
<li id="zho11">Zhou, X., Wellman, B. &amp; Yu, J. (2011). Egypt: the first Internet revolt? Peace Magazine, July/September 2011, pp. 6-10, Retrieved from <a href="http://peacemagazine.org/archive/v27n3p06.htm" target="_blank">http://peacemagazine.org/archive/v27n3p06.htm</a> (Archived by WebCite® at http://www.webcitation.org/6j8KJdt0K).
</li>
</ul>

</section>

</article>