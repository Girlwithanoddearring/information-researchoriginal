<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Conceptions of school libraries and the role of school librarians: findings from case studies of primary schools in Lahore

## [Syeda Hina Batool](#author) and [Sheila Webber](#author).

> **Introduction**. This paper explores the conceptions of library as a place, conceived by primary school children and the role of school libraries in terms of meeting children's information needs. The article is based on a PhD study. This paper presents the selected findings related to the conceptions of libraries and the role of school libraries.  
> **Method**. The study used a multiple qualitative case study approach and data was primarily collected from children's focus groups, librarians' interviews, teachers' interviews, documents and observation.  
> **Analysis**. The present study used a combination of thematic and situational analysis approaches to analyse the data. The Nvivo 10 software was used to organize the data from coding to the final six broad themes. The themes were used in reporting results.  
> **Results**. It was found that primary school children conceive of the library as a place for reading, playing and a room to store books. In Pakistan, primary schools lack school libraries and offer limited services to children.  
> **Conclusion**. The majority of the children studying in state schools did not see libraries as physical places, they have different conceptions of the library. School libraries are playing a limited role in developing reading habits and making children independent learners.

<section>

## Introduction

The role of libraries in schools has been widely discussed in library and information science literature. It is established that the role of school libraries is changing due to digital environments and the need to prepare independent learners. Its role is transformed now from developing reading habits among children to build information (searching, organizing, using) and critical skills (thinking, evaluating) ([Easley and Yelvington, 2015](#eas15); [O'Connell et al., 2015](#oco15)). School libraries have long had the challenge to provide a hybrid (print and electronic information sources) learning environment for children. IASL (that:

> The school library provides a wide range of resources, both print and non-print, including electronic media, and access to data which promotes an awareness of the child's own cultural heritage, and provides the basis for an understanding of the diversity of other cultures.

Asselin ([2004](#ass04)) asserted that the meaning of literacy has changed dramatically from reading, writing to learn new information and communication technologies. However, this development creates a digital divide, which is a gap between those who can use information and communication tools effectively and those who cannot ([Singh, 2003](#sin03)). Multiple factors caused digital divide globally, it is a significant barrier in accessing and using information. Singh ([2003](#sin03)) claimed that school libraries can play important role by creating awareness, proving access to information and building necessary information skills of school community at early stage. Over years Bingham ([1989](#bin89)) declared school libraries as learning laboratories and librarians as consultants to teachers.

In Pakistan, the situation of school library services is disappointing as is highlighted in the literature ([Anwar, 1971](#anw71); [Khawaja, 1988](#kha88); [Haider, 2008](#hai08)). Non-availability of children's literature, lack of professional librarians and no regular provision of budget are the major issues of school libraries.

Previous studies ([Lynd, 2007](#lyn07); [UNESCO-IBE, 2007](#une07); [Khan, 2013](#kha13)) confirmed the missing facilities in primary schools, specifically non-availability of libraries and librarians in public (state) schools. Khan ([2013](#kha13)) reported that there are 454 public schools in Lahore (the site of the current research) and 300 schools do not have functional libraries. It is important here to discuss the public library availability and the services for school children in the city. The city of Lahore is fortunate to have many more public libraries than rural areas of Pakistan. However, the researcher asked five public library heads (personal communication, January 27, 2015; through personal reference, the researcher (first author) came to know that these libraries had children sections) and four heads confirmed that they have children sections in their libraries (Punjab Public Library, Model Town Public Library, Defence Public Library and Chughtai Public library). However, it needs to be studied that how functional they are and what services they are offering.

The researchers identified limited research based studies on the role of school libraries in Pakistan. Therefore, it was highly desirable to study the role of the school libraries and what schoolchildren understand about libraries. The present study included investigation of the role of primary school libraries in Lahore, Pakistan and the children's conceptions of the library. The reason to select primary schools rests on the statistics reported by Lynd ([2007](#lyn07)); he claimed that the Pakistani education system specifically focuses on primary schools and more children are attending this level of education than middle (6-8 grades), secondary (9-10 grades), high secondary (11-12 grades) and higher (13 grade or above) educational levels (i.e. many children do not attend school beyond the primary level).

Conceptions about school libraries has been less researched in literature, specifically, 5-7 years of school children's experiences. For the present study, the concept of conceptions has been defined as 'way of knowing something' ([Limberg, 1998](#lim98), p.117). It can be considered as a link between the subject and the phenomenon/situation/object. A number of studies ([Savolainen, 2009](#sav09); [Limberg, 1998](#lim98)) focused on conceptions of information use/seeking and investigated this phenomenon in different sectors. However, the present study will contribute by investigating conceptions of primary school children about library as a place and in terms of its services.

</section>

<section>

## Literature Review

The available literature identified a number of studies on the role and use of school libraries, status, development, challenges and its impact on students' learning ([McNichol, Ghelani and Nankivell, 2002](#mcn02); [Pors, 2008](#por08); [Bikos, Papadimitriou and Ginnakopoulos, 2014](#bik14); [Hughes, 2014](#hug14)). The studies mostly were conducted focused on secondary and higher educational levels ([Pors, 2008](#por08); [Bikos, Papadimitriou and Ginnakopoulos, 2014](#bik14)). The primary educational level was somewhat neglected and the present study attempted to fill this gap.

A number of studies identified the role of school libraries and their positive impact on students' academic performance and learning ([Hughes, 2014](#hug14); [Williams, Wavell and Morrison, 2014](#wil14)). Hughes ([2014](#hug14)) reviewed Australian and international studies and identifies that student outcomes are improved with the use of library and availability of learning material. Similarly a report on Scotland's school libraries found that school libraries impacted on students' higher academic scores and developed positive attitudes towards learning ([Williams, Wavell and Morrison, 2014](#wil14)). The role of school librarians as promoting reading and information literacy inquiry and collaborating with teachers to conduct learning activities is also significant factor in increasing students' academic performance ([Tilley, 2013](#til13); [Johnson, 2011](#joh11)).

Some studies ([Shilling and Cousins, 1990](#shi90); [Rafste, 2005](#raf05)) criticized the existing literature of school libraries that has been much focused on technical issues and neglected social use of libraries. However, several studies focused on high schools identified the understanding of students' about libraries and their use. The researchers, adopting a qualitative research approach, found that high school students used libraries for more social reasons (meeting place, chatting, waiting room) than academic reasons ([Shilling and Cousins, 1990](#shi90); [Rafste, 2005](#raf05); [Shenton, 2014](#she14)). Shenton ([2014](#she14)) conducted a study to investigate the purposes of school libraries as viewed by high school students in northern England. The majority of the students identified libraries as places suitable for independent learning. While examining the designs of physical school libraries conceived by secondary school children, Sheehan ([2012](#she12)) found that students' preferred both human (friends, teachers, librarians) and electronic resources in their ideal library.

The students come to libraries for more social as well as academic reasons (for example reading, studying, learning, relaxing, ICT activities, school work, leisure information, ambience, wireless network, study places, exhibitions, group works, computers, collections of music and films, socializing) ([Shilling and Cousins, 1990](#shi90); [Rafste, 2005](#raf05); [Pors, 2008](#por08); [Shenton, 2014](#she14)). Students considered the library as a 'third place' where they can gather, socialize, freely, not to behave like host, not to face any dominance and they want to go there as an escape from first (home) and second (work) places ([Wiegand, 2005](#wie05)).

However, in Pakistan the role of existing school libraries is very limited and the previous studies confirmed facilities were missing in many primary schools, specifically non-availability of libraries and librarians in public/state schools ([Lynd, 2007](#lyn07); [UNESCO-IBE, 2007](#une07); [Khan, 2013](#kha13)). Fatima ([1989](#fat89)) reported that government library plans and reports remained silent on the development of children's libraries and services. The existing literature on school libraries is inadequate and not up to date. As already noted, the literature ([Anwar, 1971](#anw71); [Khawaja, 1988](#kha88); [Haider, 2008](#hai08)) highlighted the basic problems of non-availability of children's literature, professional librarians and regular provision of budget as major issues of school libraries in Pakistan. This paper is therefore devoted to investigate the conceptions or understanding of primary school children and the role of libraries in their learning.

## Research Objectives

The aim of the overall study was to investigate information literacy practices in the primary schools of Lahore, Pakistan. However, this paper presents the selected findings related to conceptions of libraries and the role of school libraries, aiming to achieve the following:

*   To investigate the primary school children's understanding of libraries
*   To examine the basic reasons to visit the library
*   To highlight the role of librarian and library in primary schools' children's learning

## Methodology

This study applied a multiple qualitative case study approach to get the overall picture of the situation. Qualitative case studies allow the researcher to look at the phenomenon through multiple lenses ([Stake, 1995](#sta95); [Merriam, 2009](#mer09)). Primary schools of Lahore, Pakistan were the potential case or cases to support study objectives. In Pakistan, schools are run by both the public and the private sector. The public schools are established and operated by government. However, there are many types of private schools for example schools for elite economic class, schools for middle and lower middle class, community or trust schools and schools run by different organizations. Different types of schools were selected with the aim of gaining a complete picture of the situation, where access was granted. The six accessible schools from public (three schools: located in low, upper middle economic areas of the city), and private (three schools: one from elite economic class, one NGO and one un-registered school) sectors were selected as cases. Among six selected cases of schools, three schools had libraries. Out of those three libraries, one library was without librarian.

To know school children's conceptions of library 12 focus groups (3-9 children of grade one and two, 5-7 years of age) of length varying from 16-25 minutes were conducted from each school. From each school case, one focus group with grade one and one from grade two was conducted. Group interviews were selected as this technique allowed children to decrease their anxiety and soon they become familiar with the researcher during the process ([Spratling, Coke and Minick, 2012](#spr12)). The children were asked about their conceptions of libraries, and those who had school libraries were asked that why they do visit library. The focus of the study was 5-7 years children as it is less researched age group in IL literature nationally and internationally. But simultaneously, literature reported that librarians should reach the children early in their lives to influence IL skills to influence the kind of adults they may become ([Walter, 2010](#wal10)).

Also national statistics (Figure 1) shows that entrance in the basic education system in Pakistan at primary level is higher than other levels (62%, blue bar in the first column). The other columns show statistics of different provinces of Pakistan. The second column depicts the data (62% primary education) of province Punjab where present study research site is located. These facts directed the researcher to explore the phenomenon at early level of education as facts identifies at this level may help the decision makers at other educational levels.

<figure>

![Figure1: Net enrolment rates by level of education and province, 2006](../colis1606fig1.jpg)

<figcaption>

**Figure 1: Net enrolment rates by level of education and province, 2006\. Copied with permission from Lynd (2007, p. 16)**</figcaption>

</figure>

The semi-structured interviews were conducted with school librarians to know the role, services and operations of the libraries. Only two schools (case three and six) had librarians; interviews were conducted with librarians of length 20-25 minutes. For the overall PhD study, ten school teachers were also interviewed, however only their responses related to the library and its use are included in the following sections. Case study encourages use of multiple sources of evidence and data collection in a natural setting; therefore observation was made during interviews and focus interviews. The researcher (first author) observed school environment, learning places, library and overall infrastructure where the researcher was allowed.

</section>

<section>

## Analysis

There are a number of ways to analyze case studies' data; however Thomas ([2011](#tho11)) suggested adopting a method which gives holistic analysis. For the present study, the researcher preferred to use Braun and Clark's ([2006](#bra06)) approach to analyse data as it is flexible in terms of its application, and not theoretically bound like IPA (Interpretative phenomenological analysis) and grounded theory ([Braun and Clark, 2006](#bra06)). It can be used with different theoretical frameworks to report experiences, meanings and reality of participants.

All interviews of librarians, teachers and focus groups of children were transcribed. Through an iterative process, descriptions of important data were transformed into more specific implications (themes). The process of first and second level coding resulted in six main themes to report overall PhD study findings. However, this article will focus on only one main theme 'school libraries' out of six themes which emerged. The findings related to conceptions of libraries, library operations, library services and library problems will be the focus of present study (Figure 2). Nvivo 10 and MS Excel were used to manage and organize categories which emerged.

<figure>

![Figure 2: Emergence of theme 'school libraries'](../colis1606fig2.jpg)

<figcaption>

**Figure 2: Emergence of theme 'school libraries'.**</figcaption>

</figure>

</section>

<section>

## Findings

Figure 2 shows the emergence of the main theme, school libraries. This theme mainly emerged from librarians' interviews, teachers' interviews and children's focus groups' transcripts. The observational data, field notes and photographs (where school staff granted permission) provided additional evidence to understand the whole situation within context. The individual and cross case findings are discussed in the following sections:

</section>

<section>

### Case One (state school)

This case was a public school located in lower economic geographic area of the city (Lahore). It was mainly selected for its geographic location. This school was mainly boys' high school linked with primary school of mixed gender (up to grade X, 15 years of age). This school had no library, however the respondents were asked about their conceptions of libraries. The researcher observed only one book rack with multiple copies of old story books (Figure 3) in kids' room. This room was also utilized for data collection purpose. The class fellows of participant children were looking into the kids' room from outside, as they wanted to come in. When the researcher asked them to come, they said they were not allowed to come in this room. They also explained that this room used to be locked.

<figure>

![Figure3: Case one, rack holding multiple copies of old story books ](../colis1606fig3.jpg)

<figcaption>

**Figure 3: Case one, rack holding multiple copies of old story books.**</figcaption>

</figure>

Only two participant children had seen a physical library and according to them a library is a reading place. The other children conceived the library as a rack of books or a cabinet of books and a place with books. Some children considered that pictures of books they had seen somewhere were libraries. The following quotes describe their conceptions of the library: '_At my home, books of my elder sister_' (Case1C1FG). Another added: '_We read books there_' (Case1C1FG).

All the children expressed their wish to read storybooks if they had a library in their school. The children of grade two also desired to have a library in their school.

</section>

<section>

### Case Two (state school)

It was also public sector primary school. The reason to choose this school is that it was one of the oldest schools, established in 1883\. This school was granted autonomous status in 1990 and has its own body of board of governors. This is a boys' high school associated with mixed gender junior school (primary section).

This school had no library either but had a big multimedia room. The children never visited any library and they were not sure about how it looks. In response to a question 'have you ever seen a library in pictures or television', one child said: 'LCD' (Case2C1FG). On further explanation, the child became silent. One of the interesting findings in this case was that the children explained that there is a library in their school where they go daily. They mentioned that when they have time, their teacher took them to the library and they play there. They also explained that they take storybooks from library and their teacher issues books to them. In response to one question ('how do you take books from library'), one child replied

> first we say may I come in, ask for storybooks and then she give (Case2C2FG).

These children also expressed that they like to go to the library.

After focus groups the researcher asked the school head teacher for a library visit; she denied that the school had a library. The head teacher said

> we have a multimedia room, maybe these children thought that room as library.

It may be possible because one child mentioned 'LCD' when he was asked about school libraries. The researcher visited the multimedia room, it was a big room with colourful curtains, one computer was attached to a multimedia projector and benches were arranged for seating. However, it was observed that there were no books or storybooks in that room. The possible reason is that children may have heard that school libraries issue storybooks, so they mentioned this service but they never went to any physical library, therefore were not sure about library as place.

</section>

<section>

### Case Three (state school)

The case three belonged to public sector and was girls' high school with junior school. This school was selected as it was located in upper middle economic geographic area of the city. This school had a library run by professional librarian. The children of this school physically visited their school's library and one child mentioned that she saw another library near a hospital. The subthemes which emerged from case three are discussed below.

</section>

<section>

#### Librarian's role

The librarian was professional and had bachelors in library and information science. She mentioned that she is teaching, more than doing librarian's work. She was only involved in accessioning and purchasing of library material. In response to a question about purchase policy she said: '_I went to bookshop after checking duplication, I buy new books, mostly of science and maths_' (Case3ALib). The librarian told the researcher that she has nothing to do with budgets, she elaborated: _'I don't know, my principal knows about it, I just buy books'_ (Case3ALib).

#### Library visit frequency and services.

The schoolteachers mentioned that children visit the library with their class teacher and the librarian conducts storytelling sessions. However, none of the participant child confirmed this fact. In response to a question about frequency of visiting the school library, children explained that sometimes they go to the library. One child mentioned, '_Only younger children go there_' (Case3C2FG). In response to a question about library visits, the librarian explained:

> We have 58 sections from nursery to grade 10\. So we divide in this way, because government made compulsory library period from grade 6\. So the first week is for primary and there are six periods, but Friday is only for nursery classes. So now we have total five days, every class has its turn once in a week. This ratio increased for higher classes (Case3ALib).

It seems that younger children did not visit the library regularly. However, the teacher said while talking about library visit 'yes - once in a week' (Case3C1T). In response to questions about library hour and library services, teachers and children could not highlight very useful library hour practice and library services. It also emerged from the data that during library hour children do not always do book reading, one child said: 'If teacher says then we take books, otherwise we sit quiet' (Case3C1FG). The same issue highlighted by another child: '_We only went to visit library_' (Case3C2FG).

However, the librarian while talking about services, mentioned that for young children she conducts colouring activities and storytelling sessions. The librarian instructs children to note down difficult words and ask for meanings. While talking about storytelling, the librarian said: '_Yes they ask it to do every day, kids really love to come in library_' (Case3ALib). The librarian was of view that, '_Children should use library regularly, should not care about book damage_' (Case3ALib). However, participant children did not mention these activities, they said that they only visit school library.

The librarian mentioned that she tells students how to use the library. The library orientation session was about telling students what they were not allowed to do in library, the librarian said:

> Yes I don't allow them to bring pen and pencils, we don't allow them to tear books, and the teacher who comes with children looks after them completely (Case3ALib).

</section>

<section>

#### Library collection.

The researcher did not get a chance to visit the library, as school staff did not permit it. The librarian said that total library collection is 11,000 and there is a reference section in the library including dictionaries, maps and encyclopaedias. She mentioned that sometimes they also get books (story books, activity books and English books) from the government. ' _'For younger children we have separate cabinet of old storybooks of prince and princess because most of them like those'_ (Case3ALib).

The librarian said that the: '_most circulated material is novel, elder children usually demand novels specifically romantic novels'_ (Case3ALib).

The younger children like to read storybooks. It seems the library had very old collection of story books as one child mentioned: '_I read raja rani (prince, princess) story books_' (Case3C1FG). While talking about library hour, the teacher mentioned: '_Children know many story books as they read many times_' (Case3C1T).

</section>

<section>

#### Library users.

The school library was mostly used by school children. The teachers were also using the library but not on regular basis. To evidence this, the Librarian also mentioned that they issue books to teachers for one week, but she said: '_Only few teachers take interest, nobody loves books here_' (Case3ALib).

### Case Four (private trust school)

This school was private trust school located in economic lower or middle geographic area of the city of Lahore. It was a mixed sex high school.

This case is an example of a school where the school library was without a librarian. The description of the library is very interesting. The researcher (first author) asked the head to visit school library. The researcher visited a very small room on ground floor, where books were scattered on its shelves and on the floor. The room was so small that it was very hard to even stand in that room with scattered books on the floor. The school head mentioned that it is their library and they have to allocate one room as library to show government representatives during audit days. However, children explained that their library is upstairs, but the researcher was not given a chance to visit other floors.

During focus groups children also talked about their school library. One child said: '_There are books in library and registers_' (Case4C1FG). Another said: '_There are story books and school books_' (Case4C2FG). Some statements are extracted in response to a question, when they go and who take them library, they said:

> We read there (Case4C2FG)  
> We only go there to put books of our previous classes (Case4C1FG).  
> Sometimes we go in a group and sometimes we go with teacher and we read school books there (Case4C2FG).

It seems that maybe there was a storeroom of books upstairs like the one on the ground floor and children perceive it as a library. The room was to store old registers and books, therefore children said that they store their old books and holiday homework registers into that room. That room was also used as sick and a punishment room, one child noted:

> If somebody had headache then teacher asks them to go to library and if somebody did not learn the lesson then she asks them to go there and read (Case4C2FG).

There was nobody to look after that library, the teacher explained

> No librarian, we are familiar from books because during summer vacations we arrange the books (Case4C2T).

However, the children liked the library as a place because it was a quiet room.

### Case Five (private un-registered school)

It was a private un-registered school located in low economic geographic area of the city. This school was selected to see a wider picture of the situation in the private sector. The school was housed in a small rented building without library facility. The teachers expressed their wish to have library facility in their school. They responded that if they had a library, they would consult other books. The teacher expressed it this way: _Yes there is a lot of information, when I was child, I used to go library and consult other books_ (Case5C1T).

However, teachers were of the view that school books are enough to consult at this educational level. During focus groups children could not express their conceptions of library, they remained silent on questions about 'What is a library'?

</section>

<section>

### Case six (private elite class school)

This school was an elite class private school, located in a high economic geographic area of the city. The school had a proper library run by a library teacher who had no library training. There was a small but colourful library in the school. The books were properly organized using Dewey Decimal Classification system (Figure 4) and further by colours to separate reading levels. The library had the latest books (Figure 4), reference books and some audio-video collection as well.

<figure>

![Figure 4: Case six library book shelves showing English language novels ](../colis1606fig4.jpg)

<figcaption>

**Figure 4: Case six library book shelves showing English language novels.**</figcaption>

</figure>

#### Library teacher's role

The library teacher was lawyer by profession. She explained that the library was organized when she joined and she found this work interesting. She was also teaching English reading and conversation. Basically, she was running their novel reading program. She explained: '_Children bring their specified books in library and then we read, I read one or two pages and then they read one by one_' (Case6ALib).

She further explained that aim of this practice is to correct the English language accent of the school children. She also issued other library books to children according to their reading level. In response to a question about how she organized books, she became confused and said: '_No, no, they have-'evvv'.you can see they are different and every cupboard has different and books have stickers also, dark blue, orange, yellow..all classes have different colours on the stickers_' (Case6ALib).

The school administration bought books for the library. The library teacher was not aware of the different parts of books. In response to a question about table of contents, index, and glossary, she misunderstood and said: '_We have a file which we maintain in computer_' (Case6ALib). She also responded to a question about the use of other information sources such as encyclopaedias, atlases: '_No no for that I am not concerned 'what they are doing at home'. English teacher does the written work and all that_' (Case6ALib).

In response to the question 'Do you give them any information finding exercise?', she explained: '_I asked administration to involve children in some written work, but they said we don't want that_' (Case6ALib).

The library hour was basically a reading hour in which children came to the library once a week and read their specified books (novels) as part of their curriculum with the teacher. However, it seems that children wanted to read or consult other books during library hour, they said: '_There is no free period to go to the library_' (Case6C2FG). This is also evidenced from teachers' interviews: that library hour was a reading class.

</section>

<section>

#### Book records

The children had to maintain a record (Figure 5) after reading library books and had to get it signed by a teacher to get another book. The children explained:

> The library teacher gives us books and when we read, we go to Ms. X, she asked us about the read book and signed on it, then we return it to library to get a new book (Case6C2FG).

The book record shows (Figure 5) that children were issuing and reading other library books very regularly. After reading books, school teachers asked the children questions, to build their recall level. One child said:

> Ms. X asks questions, sometimes very difficult''sometime from the book and sometimes not from the book, if we gave right answer, then she knows that we read the book nicely (Case6C2FG).

This school engaged in this practice to build children's reading habits. The children also expressed the wish to have new books in their school library.

<figure>

![Figure 5: Case six children book record ](../colis1606fig5.jpg)

<figcaption>Figure 5: Case six children's book record.</figcaption>

</figure>

</section>

<section>

## Discussion

The discussion under this section will highlight the main connections among cases to produce new knowledge. This analysis will develop cross connections between similar factors and related concepts.

The understanding of the selected primary school children about school library is very interesting. A number of children from cases one, two and four knew that library is a reading place. They considered it as reading place, books or old books storage place, play area and a multimedia room. These findings are similar to the previous studies in which high school students considered library as an independent learning place and third place to socialize and gather ([Wiegand, 2005](#wie05); [Shenton, 2014](#she14)). The primary school children used the word 'play area' and for high schoolers it is a place to 'socialise'. However, none of the previous studies mentioned that primary school children considered the library as a storage place for books and multimedia room.

Figure 6 represents the use of library space in the selected schools. The results show that two schools (cases three and six) had proper libraries and one (case four) had a books' store room which they called library. The common factor among the libraries of case three (public school) and case four (un-registered private school) was that they were used as reading places and for quiet sitting. The libraries of case three (public school) and six (elite class private school) offered library hours to each grade level once a week. However, the objectives of conducting library hours were different in both cases. In case six, the purpose was to run a novel-reading program and in case three, during library hour children read and share story books and sometimes only sat quiet. In relation to these findings, previous studies ([Shilling and Cousins, 1990](#shi90); [Rafste, 2005](#raf05); [Pors, 2008](#por08); [Shenton, 2014](#she14)) reported that high school students used library for more social reasons than academic reasons: relaxing, quiet sitting and sometimes as classroom.

It was found that there is a contrast of physical library conceptions as found in literature and present study findings. Previous studies ([Herring, 2011](#her11); [Sheehan, 2012](#she12)) conceived library as conducive for learning, interactive and a place for pleasure learning. Herring ([2011](#her11)) was of view that library is a place which accommodates individual information seeking behaviours. Other studies ([Wiegand, 2005](#wie05); [Shenton, 2014](#she14)) reported that students conceived library as a third place where they can come to socialize and for independent learning. However, present study findings show that students conceived library as a reading place, place to store books or old books, place to sit quiet, sick or rest room and study alone place. These students thought about library as a more controlled environment where they can only perform certain tasks with reading books unlike the findings of above discussed studies. While investigating children's perceptions of place, Christensen, Mygind and Bentsen ([2015](#chr15)) found that main reason for children liking places is the 'act/activity' they perform at that place, for example, children liked park as they can play football there. Another study found that children prefer physical characteristics while selecting books, however, it also depends on age level and gender ([Reuter, 2007](#reu07)).

Wiegand ([2005](#wie05)) also argued that librarians should give importance to library as place, however, librarians only focus in terms of collections, information services and personal workspaces. He was of view that public spaces should provide ample reasons to visit it. Mardis ([2011](#mar11)) expressed that students enter the school library with three questions in mind, 'is this my kind of place, can I be successful here; and does this fit into the rest of my life?' (p.i). While highlighting the theme 'school library as place, school library as space', Mardis ([2011](#mar11)) pointed out that library environment should be welcoming and library as place should serve different from their homes and classrooms.

<figure>

![Figure 6: Cross case analysis of use of library place ](../colis1606fig6.jpg)

<figcaption>Figure 6: Cross case analysis of use of library place.</figcaption>

</figure>

Figure 7 shows the role of librarian/library teacher in case three (public school) and six (elite class school) and the connections between both. It was established from findings that, in both cases, the librarian/library teacher were performing dual duties. They were teaching as well as managing the school library. The role of the library teacher in case six (elite class private school) was to teach and run their novel reading program. She was also involved in correcting their English accent. Additionally, she issued library books according to the children's reading levels. It seems that the library teacher was an English language teacher instead of a media/information specialist. There are similar findings from case three but here the librarian was teaching subjects other than English language (Figure 7).

<figure>

![Figure 7: Role of the librarian/library teacher](../colis1606fig7.jpg)

<figcaption>Figure 7: Role of the librarian/library teacher</figcaption>

</figure>

The results revealed that in the public sector, the librarian had support of other teachers to manage the library and conduct the library hour. In the private sector, the library teacher was running a reading program and managing the library all alone. She also complained during the interview that she is overburdened.

The results revealed that in the public sector, the librarian had support of other teachers to manage the library and conduct the library hour. In the private sector, the library teacher was running a reading program and managing the library all alone. She also complained during the interview that she is overburdened.

Previous studies highlighted the positive impact of a professional school librarian's role on students' academic performance ([Hughes, 2014](#hug14)). The present findings show a very limited role for school librarians; more teaching of other subjects, managing the library and doing technical operations (issue/return, acquisition). The role of library management is consistent with previous studies, however other findings (teaching other subjects, running a novel class, improving vocabulary) are not consistent with other studies ([Tilley, 2013](#til13); [Johnson, 2011](#joh11)). Johnson ([2011](#joh11)) stressed the role of librarians as reading teachers; however, in the present study the library teacher of elite class private school was improving their vocabulary and accent instead of teaching reading strategies. While taking a broader approach, Subramaniam, Ahn, Fleischmann and Druin ([2012](#sub12)) proposed school librarian's role as information professional, instructional partner and technology specialist. It was found that conceptions of the role of school librarians discussed in previous studies is different as for present study the role was fitted into the school needs and was limited in terms of students' learning.

</section>

<section>

## Conclusion

The state/government, private trust and private un-registered primary schools in Pakistan do not have proper libraries. Some state schools in middle or upper economic areas of the city of Lahore have libraries but librarians are teaching other subjects than doing library work. Out of three selected public primary schools, only one school had library and librarian. Only a few participant children from these schools had seen a physical library. The elite economic class private school was trying to promote a reading culture among students, although the library teacher had no collaboration with teachers. The state school libraries are playing a limited role in developing reading habits and making children independent learners.

There are implications for the school librarians and libraries to offer interesting, interactive and innovative library services to attract students. Previous studies found that impact of school libraries cannot be separated from its' place and it is librarians' responsibility to develop welcoming environment for children. School library administration should provide financial and technical support in this regard. It is highly recommended that school librarians should collaborate with teachers to start meaningful library hours and should offer innovative and interesting library services. Another important implication of the present study is that the role of librarian should be more vibrant in active learning of students and in IL skills teaching than just running storytelling sessions, managing library and teaching other subjects. The teacher librarian role was found missing in the present study findings as reported by other studies (instructional partner, technology specialist, informational professional).

As far as the findings related to basic infrastructure concerned, the government should provide basic facilities (library, laboratory) to primary schools and should have strict evaluation criteria to judge the performance of private schools. There is a dire need to develop school libraries as inviting places where interesting and innovative information activities can take place to attract school children.

</section>

<section>

## <a id="author"></a>About the authors

**Syeda Hina Batool** is Assistant Professor in Department of Information Management, University of the Punjab, Lahore, Pakistan. She received her Ph.D. from the Information School, University of Sheffield, UK and her research interests are in information literacy specifically research conducted in school sector and information seeking. She can be contacted at [hina.im@pu.edu.pk](mailto:hina.im@pu.edu.pk)  
**Sheila Webber** is Senior Lecturer in the Information School, University of Sheffield, UK. Her key areas for research and teaching are information literacy (IL), information behaviour (IB) and educational informatics. She maintains the [Information Literacy Weblog](http://information-literacy.blogspot.com) and can be contacted at [s.webber@sheffield.ac.uk](mailto:s.webber@sheffield.ac.uk)

</section>

<section>

## References

<ul>
<li id="anw71">Anwar, M. A. (1971). Secondary school libraries in Pakistan. <em>International Library Review,3</em>, 349-352.
</li>
<li id="ass04">Asselin, M. (2004). New literacies: Towards a renewed role of school libraries. <em>Teacher Librarian, 31</em>(5), 52-53.
</li>
<li id="bik14">Bikos, G., Papadimitriou, P. &amp; A. Giannakopoulos, G. (2014). School libraries' impact on secondary education: a users' study. <em>Library Review, 63</em>(6/7), 519-530.
</li>
<li id="bin89">Bingham, R. T. (1989). The library: Integrating the library into the curriculum.<em>Educational Leadership, 46</em>(7), 87.
</li>
<li id="bra06">Braun, V. &amp; Clarke, V. (2006). Using thematic analysis in psychology. <em>Qualitative Research in Psychology, 3</em> (2), 77-101.
</li>
<li id="chr15">Christensen, J. H., Mygind, L. &amp; Bentsen, P. (2015). Conceptions of place: approaching space, children and physical activity. <em>Children's Geographies, 13</em>(5), 589-603
</li>
<li id="cla05">Clarke, A.D. (2005). <em>Situational analysis: grounded theory after the postmodern turn.</em>London, England: Sage.
</li>
<li id="fat89">Fatima, N. (1989). <em>Standard of children libraries in Pakistan.</em> Karachi, Pakistan: Library Promotion Bureau.
</li>
<li id="eas15">Easley, M. &amp; Yelvington, M. (2015). What's in a name: Reimagining the school library program. <em>Teacher Librarian, 42</em>(5), 21-23.
</li>
<li id="hai08">Haider, S. J. (2008). Fate of school libraries. <em>Pakistan Library &amp; Information Science Journal, 39</em>(3), 2-5.
</li>
<li id="her11">Herring, J. (2011). Year 7 students' information literacy, and transfer: A grounded theory. <em>School Library Research, 14.</em> Retrieved from http://www.ala.org/aasl/sites/ala.org.aasl/files/content/aaslpubsandjournals/slr/vol14/SLR_Year7Students_V14.pdf.
</li>
<li id="hug14">Hughes, H. (2014). School libraries, teacher-librarians and student outcomes: Presenting and using the evidence. <em>School Libraries Worldwide, 20</em>(1), 29-50.
</li>
<li id="int93">International Association of School Librarianship. [IASL] (1993). <em>IASL policy statement on school libraries.</em> Retrieved from http://www.iasl-online.org/about/organization/sl_policy.html
</li>
<li id="joh11">Johnson, M. J. (2011). Schools librarians as reading teachers? <em>School Library Monthly, 28</em>28(1), 28-30.
</li>
<li id="kha13">Khan, A-Q. (2013, February 9). School main libraries ki kami, talba main kutabbeni ka sauq madom ho gya. [Lack of libraries in schools: students lack reading habits]. <em>Roznama Dunya Lahore.</em>
</li>
<li id="kha88">Khawaja, I. (1988). Qualitative standards for school library budget. <em>Pakistan Library Bulletin, XIX</em>(4), 1-12.
</li>
<li id="lim98">Limberg, L. (1998). Three conceptions of information seeking and use. <em>Proceedings of the Second International Conference on Research in Information Needs, Seeking and Use in Different Contexts</em>(pp. 116-135). Retrieved from http://www.informationr.net/isic/ISIC1998/98_Limberg.pdf
</li>
<li id="lyn07">Lynd, D. (2007). <em>The education system in Pakistan.</em> Retrieved from http://unesco.org.pk/education/teachereducation/files/sa4.pdf
</li>
<li id="mar11">Mardis, M. A. (2011). Reflections on school libraries as space, school library as place. <em>School libraries worldwide, 17</em>(1). Retrieved from https://www.questia.com/library/journal/1P3-2251235731/reflections-on-school-library-as-space-school-library
</li>
<li id="mcn02">McNichol, S., Ghelani, T., &amp; Nankivell, C. (2002). The role of school libraries in resource-based learning. <em>Education Libraries Journal, 45</em>(3), 5-11.
</li>
<li id="mer09">Merriam, S. B. (2009). <em>Qualitative research: a guide to design and implementation.</em> San Francisco, CA: Jossey-Bass.
</li>
<li id="oco15">O'Connell, J., Bales, J. &amp; Mitchell, P. (2015). Evolution in reading cultures: 2020 vision for school libraries. <em>The Australian Library Journal, 64</em>64(3), 194-208.
</li>
<li id="por08">Pors, N-O. (2008). Traditional use patterns? <em>New Library World, 109</em>(9/10), 431-443.
</li>
<li id="raf05">Rafste, E.T. (2005). A place to learn or a place for leisure? Students' use of the school library in Norway. <em>School Libraries Worldwide, 11</em>(1), 1-16.
</li>
<li id="reu07">Reuter, K. (2007). Assessing aesthetic relevance: Children's book selection in a digital library. <em>Journal of the American Society for Information Science &amp; Technology, 58</em>(12), 1745-1763.
</li>
<li id="sav09">Savolainen, R. (2009). Epistemic work and knowing in practice as conceptualizations of information use. <em>Information Research, 14</em>(1) paper 392. Retrieved from http://www.informationr.net/ir/14-1/paper392.html
</li>
<li id="she14">Shenton, A.K. (2014). Just why do we need school libraries? Some ideas from students. <em>New Library World, 115</em>(3/4), 140-159.
</li>
<li id="she12">Sheehan, K. (2012).<em>Grand designs: Consider ways in which the physical school library motivates key stage three students to engage in information literacy.</em>(Unpublished Masters' thesis). University of Sheffield, England.
</li>
<li id="shi90">Shilling, C., &amp; Cousins, F. (1990). Social use of the school library: The colonisation and regulation of educational space. <em>British Journal of Sociology of Education, 11</em>(4), 411-430.
</li>
<li id="sin03">Singh, D.(2003). Reducing the Digital Divide?: The Role of School Libraries. <em>International Association of School Librarianship. Selected Papers from the Annual Conference</em>. (pp. 323-330).
</li>
<li id="spr12">Spratling, R. Coke, S. &amp; Minick, P. (2012). Qualitative data collection with children.<em>Applied Nursing Research,25</em>, 47-53.
</li>
<li id="sta95">Stake, R.E. (1995).<em>The art of case study research.</em> London, England: Sage.
</li>
<li id="sub12">Subramaniam, M. Ahn, J., Fleischmann, K.R. &amp; Druin, A. (2012). Reimagining the role of school libraries in STEM education: Creating hybrid spaces for exploration. <em>Library Quarterly, 82</em>(2), 161-182.
</li>
<li id="tho11">Thomas, G. (2011). <em>How to do your case study: A guide for students and researchers.</em> London, England: Sage.
</li>
<li id="til13">Tilley, C. (2013). Reading instruction and school librarians. <em>School Library Monthly, 30</em>(3), 5-8.
</li>
<li id="une07">UNESCO-IBE. (2007). <em>World data on education.</em> 6th ed. Retrieved from http://www.ibe.unesco.org/fileadmin/user_upload/archive/Countries/WDE/2006/ASIA_and_the_PACIFIC/Pakistan/Pakistan.pdf
</li>
<li id="wie05">Wiegand, W.A. (2005). Library as place. In <em>56th Biennial Conference North Carolina Library Association.</em> (pp.76-81). Retrieved from http://www.ncl.ecu.edu/index.php/NCL/article/view/70
</li>
<li id="wil14">Williams, D., Wavell, C. &amp; Morrison, K. (2014). Impact of school libraries on student learning. <em>Teacher Librarian, 41</em>(3), 32-33.
</li>
</ul>

</section>

</article>