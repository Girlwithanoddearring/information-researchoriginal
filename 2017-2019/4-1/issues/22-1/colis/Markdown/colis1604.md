<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# The front and backstage: pupils’ information activities in secondary school

## [Cecilia Andersson](#author)

> **Introduction.** This paper reports on a study of how information activities are legitimised in a school setting. Digital tools (e.g. computers, laptops, iPads) have been implemented in Swedish schools for some time now. Still many questions regarding how, why and when the tools should be used remains. The paper investigates how pupils describe and carry out information activities in school, as well as how these activities are legitimised.  
> **Method.** Focus groups and observations have been carried out with pupils in three secondary schools in Sweden. In total 39 pupils participated in the focus groups.  
> **Analysis.** The study takes off from a socio-cultural perspective and Goffman’s concepts of front and backstage acts as an analytical lens. A qualitative analysis which included colour coding was carried out to find patterns in the material.  
> **Results.** The findings were clustered into three themes; Legitimate and non-legitimate information sources, Googling: a front and backstage activity, and Technology creating a new backstage in the classroom.  
> **Conclusion.** The pupils adjust their performance of information activities towards their understanding of proper conduct in school. Meanwhile, the pupils also sometimes search for information in non-legitimate ways backstage but still aim towards providing the teacher with a school appropriate source. Through an investigation of the interplay between front- and backstage it is possible to identify uncertainties that exist and ongoing negotiations in the school setting.

<section>

## Introduction

For more than two decades schools in Sweden have worked towards the implementation of various digital tools (e.g. computers, iPads, and laptops). Still, many questions regarding how, when and why these tools should be used remains. Moreover, research has found there is discord in the current school system between new and traditional ways of teaching. The more traditional focus on setting tasks where pupils were expected to find one correct answer has given way to more inquiry-based learning goals ([Limberg, 2013](#lim13); [Livingstone, 2012](#liv12); [Collins and Halverson, 2010](#col10)). One of the overarching aims in the curriculum for Swedish compulsory schooling is that pupils should be able to use modern technology as a tool in ‘_the search for knowledge, communication, creativity and learning_’ ([Lgr11, p. 16](#lgr11)). However, exactly how this should be achieved is not outlined in detail. A report from the Swedish National Agency for Education ([2013](#sna13)) noted that while the access to computers in schools has increased, the amount of teaching carried out with the help of computers has not. Altogether, this raises questions regarding how information activities such as searching for information, the critical assessment of sources, and credibility judgements are taking place in schools.

The aim of this paper is to investigate how information activities are carried out and legitimised in a school setting. Drawing on Goffman ([1956](#gof56)), legitimate and non-legitimate can be viewed as reflected in frontstage and backstage activity, where legitimate and appropriate activities are brought to the front and others left backstage in the interaction with others. This paper reports on findings from focus groups and observations carried out in three secondary schools in Sweden with pupils ages 13-15\. The study is guided by three main research questions:

*   How do pupils describe and carry out information activities in school?
*   How are these information activities legitimised in a school setting?
*   In which ways does this help us elucidate the role of mobile technology in young peoples’ lives?

The use of various digital tools in schools has motivated research within information science on how young people make credibility assessments when using digital resources ([Subramaniam et al., 2015](#sub15); [Limberg, 2013](#lim13); [Sundin and Francke, 2009](#sun09); [Rieh and Hilligoss, 2008](#rie08)), the role of the teacher in the development of information literacy ([Lai, 2015](#lai15); [Lundh et al., 2011](#lun11); [Hongisto and Sormunen, 2010](#hon10)) , the role of school culture ([Gärdén, Francke, Lundh and Limberg, 2014](#gar14); [Rantala, 2010](#ran10)), how pupils reformulate their search queries ([Rutter et al., 2014](#rut15)), the relationship between learning and searching ([Rieh, et al., 2016](#rie16); [Alexandersson and Limberg, 2012](#ale12)) and formal and informal learning ([Ferguson et al., 2015](#fer15); [Crook, 2012](#cro12)). Notably, a socio-cultural approach has been successfully adopted within studies on information literacy ([Limberg, 2013](#lim13); [Addison and Meyers, 2013](#add13); [Lloyd, 2012](#llo12); [Sundin and Francke, 2009](#sun09)). The question of legitimisation of information activities in different settings has previously been studied, and the importance of pupil-teacher interactions in facilitating learning outcomes has been recognised (see [Lloyd, 2012](#llo12); [Lundh and Alexandersson, 2012](#lun12); [Gärdén et al., 2014](#gar14); [Limberg, Alexandersson, Lantz-Andersson, and Folkesson, 2008](#lim08); [Alexandersson and Limberg, 2012](#ale12)). This study contributes to that field of research with an investigation of information activities in a school setting from a wider perspective - beyond specific tasks - in order to shed light on how different ways of carrying out school tasks emerge and are legitimised.

</section>

<section>

## Theoretical and conceptual framework

This paper is underpinned by a socio-cultural approach and information activities are thereby viewed as situated and as developed in socio-cultural communities ([Lundh, 2011](#lun11); [Lloyd, 2012](#llo12); [Sundin and Francke, 2009](#sun09)). Information activities are the main empirical unit of analysis and are here viewed in broad terms as pupils’ ‘_doings with information_’ ([Lundh 2011, p. 20](#lun11)). Being able to participate in and perform information activities is viewed in terms of information literacies. Information literacies is used in its plural form as it is considered situational, thus, being information literate in one setting is different from being information literate in another ([Lundh, 2011](#lun11); [Lloyd, 2012](#llo12); [Sundin and Francke, 2009](#sun09)). This allows for an investigation of various forms of information literacies such as searching for information, credibility assessments and critical assessments of sources.

In order to grasp how information activities are legitimised in a school setting, Goffman’s dramaturgical concept of front- and backstage is the main theoretical lens. This concept fits well within a socio-cultural approach to information activities as this perspective also focuses on context. What is displayed frontstage can be understood as that which is legitimate behaviour in a certain setting ; ‘_the performance of an individual in a front region may be seen as an effort to give the appearance that his activity in the region maintains and embodies certain standards_’ ([Goffman, 1956, p. 67](#gof56)). In the school setting this can translate as wanting to appear to be active during class (frontstage) ([Alexandersson and Limberg, 2012](#ale12)). On the other hand, backstage represents the behaviour which one hopes to hide from the audience as it is not considered appropriate for the setting and situation. The main argument is that in all interactions certain things are displayed while others are hidden; things that we may consider situationally inappropriate or we do not want to be associated with are held back while other aspects are exhibited ([Goffman, 1956](#gof56)). An investigation of front- and backstage activity can thereby help reveal what pupils acknowledge as legitimate and non-legitimate ways of carrying out information activities in school.

In this study frontstage refers to those situations where the teacher acts as the audience, whether this is during face-to-face interaction in class or in focus group discussions. Backstage is defined as a situation where the teacher is not present as an audience. The discussions in the focus groups give insights into front- and backstage activities, although on a descriptive level. While the teacher is not present in the focus groups, the pupils still discuss their activities in relation to the teacher as audience. The pupils also discuss activities that they do not wish to display to the teacher. This theoretical approach enables an open-ended investigation on what is considered to be legitimate information activities in school without any predefined categories.

Goffman wrote in a time period before the internet. However, in an analysis of Goffman’s work Pinch ([2010](#pin10)) argued that technology plays a part in staging roles and ‘_is also crucial in terms of how the interaction is mediated_’ (p. 414). The link between materiality and performance becomes the most visible when the materiality has changed ([Pinch, 2010](#pin10)). In this case there have been several material changes with regards to the classroom such as the appearance of laptops, smartphones and smart boards. The way that pupils and teachers make use of these various technologies, such as looking up something on a smartphone, can be understood as a way of staging a performance using technological props.

Within library and information science Radford et al. ([2011](#rad11)) has shown how Goffman’s interaction analysis can be useful for analysing virtual interactions. Using Goffman’s theoretical framework, Chelton ([1997](#che97)) investigated the dynamics at a library service counter and revealed the role of power in interaction.

## Method and material

</section>

<section>

### Empirical setting

The Swedish compulsory school system consists of primary and secondary school, grade 1-9 ([www.skolverket.se](http://www.skolverket.se)). The participants in this study were attending secondary school (grade 7-9), and the pupils were 13 to 15 years old. Fieldwork was carried out in three different schools. Pupils in all of the schools had access to computers through different arrangements. It was interesting to carry out this research in Sweden due to a high prevalence of internet access as well as the use of several types of digital devices among young people ([The Swedish Media Council, 2015](#smc15); [Findahl, 2015](#fin15)).

Since the research participants were under the age of 15, consent forms were signed by the participants’ parents. All names have been anonymised for this article through using fictional names. The pupils chose to participate after I presented this project at their school.

### Focus groups and observations

Focus groups and observations were the two methods for data collection in this study. Focus groups provided participants’ descriptions of how they carry out activities while the observations gave insights into activities as they unfold in the class room ([Fangen, 2004](#fan04)).

The focus groups were divided according to which grade the pupils attend and thereby consisted of class mates. This was beneficial as the pupils share the experience of going to school together which fostered insight into their attitudes and shared understandings within the groups. The focus groups were semi-structured and all followed an interview guide ([Davies, 2008](#dav08)). The interview guide contained three themes that focused on searching for information on a general level, the use of digital tools in school and at home, and the performance of research for a task in school. During the last theme the pupils were given a scenario of a school task and a printout of an internet search in order to trigger discussion. Focus groups allowed for conversations to take place that are similar to those that occur in everyday life, but have a greater intensity. Therefore they are a suitable method for investigating participants’ attitudes. This method also decentres the role of the researcher, giving the participants more ownership of the process ([Kamberelis and Dimitriadis, 2011](#kam11)).

I acted as moderator for the groups. I recorded and transcribed all material from the focus groups. Six focus groups were carried out with a total of 39 participants. Each focus group lasted between 32 -73 minutes with 4-10 participants each. During one focus group there was a problem with the recording and the discussion has not been transcribed but instead notes were taken during and straight after the discussion.

Four full days of school observations were carried out. The observations were a combination of classroom observations and walk-alongs with the participants. A walk-along entails following a person while they are carrying out their daily activities ([Kusenbuch, 2003](#kus03)). I did this with three different participants (twice with one of the participants). During the observations I took field notes which are used in the analysis ([Wolfinger, 2002](#wol02); [Tjora, 2006](#tjo06)).

The focus groups were carried out between December and March in 2014-2015\. Observations were carried out during May and October 2015.

</section>

<section>

### Analysing the material

The material for the analysis consisted of transcripts from the focus groups and field notes from the observations. The focus group material was analysed through colour-coding. Each focus group discussion was assigned a colour and then excerpts from the transcripts were extracted and pasted into a new document. The colour coding made it possible to get an overview of how often and how widely topics were discussed; this enabled me to find patterns in the material ([Morgan, 1997](#mor97)). The observations were analysed in relation to the topics in the focus groups. Analysing the observations gave context to some of the discussions in the focus groups and also added another layer to the analysis with the presence of the teacher ([Davies, 2008](#dav08); [Flick, Kardorff and Steinke, 2004](#fli04)).

The analysis has been clustered into three themes: _Legitimate and non-legitimate information sources; Googling: a front- and backstage activity_; and _Technology as a new backstage in the classroom_. The themes are informed by the empirical findings which have been analysed in light of the chosen theoretical approach. The research questions are reported throughout the themes rather than each theme representing a research question.

## Analysis and discussion

### Legitimate and non-legitimate information sources

</section>

<section>

When the pupils discussed how they looked for information when starting a new research project in school, many explained that they would turn to Google and then proceed to Wikipedia. Wikipedia was frequently mentioned in all focus groups as a very important source of information for the pupils. However, the use of Wikipedia was surrounded by uncertainty and not considered a fully worthwhile source in an educational setting:

> _**Eric**: Do you write out sources?  
> **Sara**: It depends if it’s sources that I’m proud of or not.  
> **Moderator**: Which sources are you proud of?  
> **Eric**: The ones that sounds professional.  
> **Johan**: Like NE (a Swedish Encyclopaedia).  
> **Eric**: Yes, NE, that makes the teachers happy.  
> **Sara**: Or the encyclopaedias from the library or something like that_

In the above quote, Sara explained that she would rather put sources in a written assignment if they are sources she is proud of. Previously in the discussion the pupils had all explained that they would use Wikipedia when looking for information for a school task. The discussion was based around search hits on Google and deciding which ones they would follow-up. When reflecting on their different choices they commented how they strived for professional-sounding sources, and, in this instance, Wikipedia is not considered professional. Here, frontstage performances come into play as the pupils explicitly wanted to perform in a way that would make their teacher happy. Their aim, in this regard, is not only (or necessarily) to find a source that satisfies them but something that makes the teacher happy. As such, Wikipedia can be understood as a backstage information activity because, although they do use it, they would rather showcase another information source to the teachers. The pupils’ discussions indicated that they felt their teachers were rather conservative: they believed that using an encyclopaedia from the library would make the teacher happier than using an online version. The discussion also suggested that in certain instances the pupils were more concerned with what the teacher would think of their use of a certain information source rather than with whether or not they found what they needed from a website.

Although the pupils used Wikipedia extensively, they do not do so without reflection. Their need to be critical of sources came through in the discussions. In several groups they explained that they knew they should not use Wikipedia. Using Wikipedia was constructed as something that everyone does but no one should do; the use of Wikipedia is better left backstage. The reasons for not using Wikipedia are mostly centred on their teachers’ approval:

> _**Moderator**: But you mentioned that Wikipedia is not-  
> **Carl**: The teacher doesn’t like it.  
> **Marie**: And we don’t care.  
> [Several people laugh]_

In the quote and the discussion that followed it was evident that the fact that the teachers do not approve of Wikipedia does not deter the pupils from using it but instead they might hide the fact (leave it backstage). In most of the groups the pupils explained that they used Wikipedia but backed it up with findings from other sites. The pupils explained that they should double-check what is written in Wikipedia with what is written elsewhere, but rarely did the pupils mention different ways of assessing an article on Wikipedia other than just comparing it to other websites. In only one group they reflected on what the teachers actually said regarding Wikipedia. While they claimed that they should not use Wikipedia at all, as they discussed this topic they realised that they never actually heard the teacher say not to use it, but rather: ‘_don’t use only Wikipedia_’. This might help explain why they were focused on finding other material to support what they read on Wikipedia. Still, the pupils’ perceptions on the use of Wikipedia indicated a uncertainty about whether they were allowed to use that website. Here it becomes evident how tacit understandings can contribute to maintaining standards. This uncertainty could also be related to a discord between old and new ways of finding information and carrying out school tasks ([Limberg, 2013](#lim13); [Collins and Halverson, 2010](#col10)). Alexandersson and Limberg ([2012](#ale12)) found that pupils’ performance of tasks is related to their understanding of school practice. Thereby, pupils define school tasks ‘_in ways that make them meaningful in relation to the discursive practice of school_’ (p. 147). The pupils carried out the task in relation to what they believe is expected of them. As such their ‘_cultural competence_’ is important when they carry out complex tasks, such as carrying out their own research ([Alexandersson and Limberg, 2012, p. 147](#ale12)). This also comes through in this study. Meanwhile, the discussions highlighted the pupils’ insecurity when it came to evaluating articles on Wikipedia. The focus groups can be seen as a way of accentuating these taken for granted attitudes and it also enabled the pupils to question their own and each other’s ideas.

</section>

<section>

### Googling: a front- and backstage activity

Throughout the school day pupils can be faced with various instructions when it comes to whether or not they should use a search engine. In the focus groups, the pupils explained that not all teachers have the same approach towards the use of digital tools in general, and search engines specifically, in the class room. In the curriculum for compulsory school, the focus on information searching is more pronounced in certain subjects ([Sundin, 2015](#sun15); [Lgr 11](#lgr11)). However, although there are differences between subjects, instead the pupils in this study spoke about different practices among teachers. ‘_Depends on what teacher I have_’ explains one pupil in relation to writing out references in a written assignment. During the observations, the teachers encouraged and discouraged the use of search engines throughout the school day, for example in math class:

> _The lesson begins with the math teacher handing out a piece of paper to the pupils. The paper is a map of the area in which their school is located. Today’s task is to calculate the scale of the map. Immediately one of the pupils ask: ‘can’t we just Google it’? He then justifies his question by explaining that on the map it says where the map was retrieved from so why can’t they just go to that website to find out the scale? ‘That is not the point of the class’ explains the teacher; he wants them to solve the problem mathematically._ (Field note, 5/10-2015)

During math class, finding the answer to the problem with the help of Google was considered a hindrance for the learning outcome of the class. Meanwhile, the ability to look things up online can make it more difficult to explain why pupils should not do so. As stated by one group:

> _**Patrick**: Actually you don’t need to have anything memorised besides reading and writing.  
> **Christina**: Because, like, technology has developed so much. Before the teacher would say, ‘You have to learn the multiplication table because you won’t always have access to a calculator’. Now everyone has a calculator on their phone.  
> **Emma**: But some teachers are still like, ‘I should be able to wake you in the middle of the night and you could recite the multiplication table...’_

In the excerpt, it is evident that the pupils do not find the argument for memorising the multiplication table sufficient. They pupils do not consider the lack of a calculator to be a realistic scenario. Erstad ([2011](#ers11)) notes that the introduction of new technologies can trigger discussions on the fundamentals of established school subjects. The calculator in itself created discussions and disputes on the changes for the subject ([Erstad, 2011](#ers11)). Similar discussions seem to be present in the class room in relation to smartphones.

Whereas googling is not legitimised during math class, and better left backstage, the opposite is true for art class. During art the same pupil is encouraged to use Google:

> _During arts class the task is to recreate a painting. The pupils start to look at paintings and look up information about the painters. The pupil that I am observing walks up to the teacher to ask about the chosen painting.. The teacher responds and the pupil continues working. Then the pupil comes up with a new question to ask the teacher, but this time the response from the teacher is: ‘there is just one of me but many of you; you’ll have to help each other out. Or you can google it and find some useful facts’_ (Field note, 22/10-2015).

In math class using Google was considered an unacceptable shortcut, yet in art it enabled the teacher to do her work more efficiently. In art class googling is not conveyed as problematic but rather instrumental and the teacher is legitimising the use of Google in order to find answers.

The examples from math and art class highlight the importance of context and the pupil-teacher interaction for the legitimisation of different ways of using information technology. The math class focused on how the pupils should learn things for themselves rather than rely on Google. Learning things on their own was something that was discussed in the focus groups. In one group, the pupils came to the agreement that for school, tests and such, you need to learn things on your own. However, they also remarked that not even the teacher knows everything:

> _**Sofia**: [… ]for tests - but then our teacher often googles during class so…  
> **Helena**: (imitates the teacher) ‘oh I can’t remember’ and then the teacher picks up her phone._

In this quote, according to the pupils, the teacher is not living up to the expectations which are set for the pupils. Mullany ([2011](#mul11)) suggests viewing front- and backstage as a continuum rather than a dichotomy because slippage occurs. Slippage is defined as when backstage behaviour is revealed in the front. What the teacher does could be understood as slippage since she does not uphold the same rules that she set for the pupils. Consistency is an important part of a credible performance, and when consistency is lost then its credibility is put into question since the divide between front and back is not maintained ([Goffman 1956](#gof56)). While the teacher may view her online search as instrumental to her teaching at that moment, the pupils may decide it is proof that memorising information is not important. As such, different ways of carrying out information activities are legitimised during teaching as well as through smaller actions performed by the teacher in the class room.

In his study, Crook ([2012](#cro12)) identified tensions when digital tools are imported into the school setting. In addition, he noted that the pupils in his study showed a sophisticated awareness of these tensions. The same can be said of the pupils in this study who noticed how the teachers’ search for information.

### Technology creating a new backstage in the classroom

During discussions it became apparent that the pupils aimed to use legitimate information sources when in school. Meanwhile, the pupils did not always know what sources the teachers used. In a way, the use of technology opened up for a new backstage within the classroom - for teachers as well as pupils. For example, during English class the teacher used information resources but did not reveal them to the pupils:

> _English class begins with the teacher showing the pupils two videos with Jamie Oliver. Afterwards they discuss different expressions that he used while cooking, and then the pupils are told to write a recipe using certain English words. The most frequent question in the classroom is: ‘How do I translate this word’? The teacher and a teaching assistant walk around helping the pupils. When the teacher does not know the word that a pupil asks for, she picks up her smartphone and says, ‘I’m just going to check in my dictionary’. This is done repeatedly throughout the English class_ (Field note, 19/5- 2015).

In the excerpt from English class both the pupils and teacher perform their roles, in Goffman’s terminology. Many of the pupils have their smartphones, some even listen to music with their smartphone, yet they do not use them to look up English words themselves. Rather than accessing their own phones the pupils acknowledge the teacher’s role and ask for help. Only the teacher knew whether she had a dictionary app - we could only see the front stage. Through using the word dictionary rather than saying she will google it, she gives legitimacy to the use of dictionaries. However, the teacher uses the word dictionary without denoting any specific dictionary, therefore giving limited information to the pupils. The searching in the dictionary remains backstage even though it happens in the class room. The pupils only see and hear what the teacher wants them to.

In this example technology plays an important part in the teacher’s performance ([Pinch, 2010](#pin10)). The teacher used her smartphone as an educational tool in the form of a dictionary. The performance is credible because there is nothing to disrupt the image. If anyone had been able to peek at the phone and see the website then it might have been different. Furthermore, the teacher needs to tell the pupils that she is looking words up in her dictionary because that is not evident. If she had been using an actual dictionary then she would not have had to make it explicit. This illustrates a link between technology and performance. Furthermore, the smartphone hides the backstage from the frontstage ([Goffman, 1956](#gof56)). However, this means that the pupils do not know what the teacher is actually doing: is she googling? Is she looking up words in a dictionary? The pupils could gain information by asking the teacher but they choose not to, at least during this lesson. This is similar to Rantala’s ([2010](#ran10)) argument that schooled digital literacies are framed by general school practices, the most important aspects of which are ‘_playing the role of a pupil and doing a school task_’ (p. 134).

While technology can hide backstage activities, this can easily change. This was apparent during one classroom observation where a pupil was working on an assignment and the teacher came and checked. As the teacher approached, the pupil shut down his internet windows and left only his Word document open. The teacher noticed complex words in the text, and that it was written about a movie when the assignment was about a book. As the conversation unfolded it became evident that the pupil had taken the text straight from Wikipedia and pasted it into his Word document. What was perhaps intended for the backstage came out in the front. This example also illustrates the dynamics of the classroom where teachers can pose questions or invade the space of the pupils, but not the other way around. Goffman ([1956](#gof56)) argued that one way of showing respect is through various rituals, and avoidance and respect of space is one. As one pupil explained, ‘_A teacher is the only person who will start talking to me when I have my headphones on_’. In this example it can also be argued that the pupil is taking an explorative task and turning it into a fact-finding task ([Limberg, 2013](#lim13)). Rather than writing down reflections and different perspective on the book, which was the assignment, he is looking for a clear cut answer which is readily available online.

</section>

<section>

## Conclusion

This paper has investigated how pupils describe and carry out information activities as well as how these are legitimised in a school setting. An overarching research question has been in which ways this helps us elucidate the role of mobile technology in young peoples’ lives. The analysis has been informed by Goffman’s notion of front- and backstage performances. Through that lens it has been possible to sketch which activities that the pupils understand as legitimate, and appropriate for the front stage, and which activities that are considered less or non-legitimate. The findings were clustered around three themes: _Legitimate and non-legitimate information source, Googling: a front- and backstage activity, Technology creating a new backstage in the classroom_.

In regards to information sources, much of the pupils’ discussions revolved around the use of Wikipedia. The use of Wikipedia was surrounded by uncertainty. While all of the pupils used Wikipedia they did not consider it to be a fully legitimate information source for a school assignment, and thus better left backstage. The uncertainty was linked to both tacit and explicit expectations in the school setting. The pupils incorporated teachers’ frontstage performance into their understanding of how to perform activities. However, these performances are not always in line with each other which open up for various interpretations. Slippage, on the part of the teacher, further opens up for questions and negotiations around proper conduct. This highlights the important role that consistency plays in order to uphold credibility and legitimacy to an activity in an educational setting. The pupils were quick to note when a teacher performed activities that contradicted instructions given to them.

The use of search engines, particularly Google, moved between front and backstage during different classes. As such, legitimate ways of carrying out information activities were context bound. What is appropriate during arts class might not be appropriate during math class. Furthermore, in the classroom technology can hide how an activity is being carried out, both for pupils and teachers. The pupils might not be able to see how a teacher carries out a certain task and vice versa. At the same time, the pupils aim for a frontstage performance in line with their understanding of how things should be done in school which can then pose a challenge.

The important role of mobile technology in young peoples’ lives comes through in several ways in this study. The pupils in many ways assume that they can rely on mobile technology, and through that internet access. The recurring question ‘_can’t we just Google it_?’ highlights the way that mobile technology is taken for granted. When the teachers talk about knowing things on their own because they will not always be able to look things up, the pupils do not find this a credible scenario. On the occasions that the teachers themselves make use of their smart phones during class it further strengthens the pupils’ argument. While the starting point for the pupils might be that they always have the possibility to search for answers online, they are also aware of school practice and aim at carrying out information activities in line with this awareness.

</section>

<section>

## Acknowledgements

The work was funded by the Swedish Research Council through the framework grant ‘Knowledge in a Digital World. Trust, Credibility and Relevance on the Web’. I am grateful for those who have participated in the study as well as those schools that have allowed me access to carry out my research. I would also like to thank the two anonymous reviewers for valuable comments as well as those present during the presentation and discussion of the paper at CoLIS 9 in Uppsala.

</section>

<section>

## <a id="author"></a>About the author

**Cecilia Andersson** is a PhD student in Information studies at Lund University, Department of Arts and Cultural Science, Sweden. She can be contacted at: cecilia.andersson@kultur.lu.se

</section>

<section>

## References

<ul>
<li id="add13">Addison, C. &amp; Meyers, E. (2013). <a href="http://www.webcitation.org/6jwo9AhoV" target="_blank">Perspectives on information literacy: a framework for conceptual understanding.</a> <em>Information Research, 18</em>(3) paper C27. Retrieved from http://InformationR.net/ir/18-3/colis/paperC27.html (Archived by WebCite® at http://www.webcitation.org/6jwo9AhoV)
</li>
<li id="ale12">Alexandersson, M. &amp; Limberg, L. (2012). <a href="http://www.webcitation.org/6cT55Q2Y7" target="_blank">Changing conditions for information use and learning in Swedish schools: a synthesis of research.</a> <em>Human IT, 11</em>(2), 131-154. Retrieved from http://etjanst.hb.se/bhs/ith/2-11/mall.pdf (Archived by WebCite® at http://www.webcitation.org/6cT55Q2Y7)
</li>
<li id="che97">Chelton, M. K. (1997). The 'overdue kid'. A face-to-face library service encounter as ritual interaction. <em>Library &amp; Information Science Research, 19</em>(4), 387-399.
</li>
<li id="col10">Collins, A., &amp; Halverson, R. (2010). The second educational revolution: rethinking education in the age of technology. <em>Journal of Computer Assisted Learning, 26</em>(1), 18-27
</li>
<li id="cro12">Crook, C. (2012). The "Digital Native" in Context: Tensions Associated with Importing Web 2.0 Practices into the School Setting. <em>Oxford Review of Education, 38</em>(1), 63-80.
</li>
<li id="dav08">Davies, C. (2008). <em>Reflexive Ethnography</em>. New York: Routledge.
</li>
<li id="ers11">Erstad, O. (2011). Citizens navigating in literate worlds: the case of digital literacy. In M. Thomas (ed.). <em>Deconstructing digital natives: young people, technology, and the new literacies</em>. New York: Routledge.
</li>
<li id="fan04">Fangen, K. (2004). <em>Deltagende observasjon</em>. Bergen: Fagbokforlag.
</li>
<li id="fer15">Ferguson, R., Faulkner, D., Whitelock, D., &amp; Sheehy, K. (2015). Pre-teens’ informal learning with ICT and Web 2.0. Technology, <em>Pedagogy &amp; Education, 24</em>(2), 247-265.
</li>
<li id="fin15">Findahl, O. (2015). <em>Svenskarna och internet 2015</em>. Stockholm: .SE (Stiftelsen för internetinfrastruktur).
</li>
<li id="fli04">Flick, U., Kardorff, E.V. &amp; Steinke, I. (red.) (2004). <em>A companion to qualitative research.</em> London: SAGE.
</li>
<li id="fra11">Francke, H., Sundin, O., Limberg, L. (2011). Debating credibility: The shaping of information literacies in upper secondary schools. <em>Journal of Documentation</em>, 675.
</li>
<li id="gof56">Goffman, E. (1956). <em>The presentation of self in everyday life</em>. Edinburgh: University of Edinburgh Social Sciences Research Centre.
</li>
<li id="gar14">Gärdén, C., Francke, H., Lundh, A. H. &amp; Limberg, L. (2014). <a href="http://www.webcitation.org/6cT5bcAFn" target="_blank">A matter of facts? Linguistic tools in the context of information seeking and use in schools.</a> Information Research, 19(4), paper isic07. Retrieved from http://InformationR.net/ir/19-4/isic/isic07.html (Archived by WebCite® at http://www.webcitation.org/6cT5bcAFn).
</li>
<li id="hon10">Hongisto, H. &amp; Sormunen, E. (2010). The challenges of the first research paper: Observing students and the teacher in the secondary school classroom. In A. Lloyd &amp; S. Talja (eds.), <em>Practising information literacy: bringing theories of learning, practice and information literacy together</em>, Centre for Information Studies, Wagga Wagga, N.S.W., 2010.
</li>
<li id="kam11">Kamberelis, G &amp; Dimitriadis, G. (2011). Focus groups- contingent articulations of pedagogy, politics, and inquiry. In N.K Denzin. &amp; Y.S. Lincoln (eds.). <em>The SAGE handbook of qualitative research</em>. (4., ed.) Thousand Oaks: SAGE.
</li>
<li id="kus03">Kusenbach, M. (2003). Street phenomenology: The go-along as ethnographic research tool. <em>Ethnography</em>, (3). 455.
</li>
<li id="lai15">Lai, C. (2015). Modeling teachers' influence on learners' self-directed use of technology for language learning outside the classroom. <em>Computers &amp; Education</em>, 8274-83.
</li>
<li id="lgr11">Lgr11 (2011). <em>Curriculum for the compulsory school, preschool class and the recreation centre.</em> Stockholm: Skolverket.
</li>
<li id="lim08">Limberg, L., Alexandersson, M., Lantz-Andersson, A., &amp; Folkesson, L. (2008). What matters? Shaping meaningful learning through teaching information literacy. <em>Libri, 58</em>(2), 82-91.
</li>
<li id="lim13">Limberg, L. (2013). <em>Informationskompetens i undervisningspraktiker. In U. Carlsson (ed.) (2013). Medie- och informationskunnighet i nätverkssamhället: skolan och demokratin.</em> Göteborg: Nordicom.
</li>
<li id="liv12">Livingstone, S. (2012). Critical reflections on the benefits of ICT in education. <em>Oxford Review Of Education, 38</em>(1), 9-24.
</li>
<li id="llo12">Lloyd, A. (2012). Information literacy as a socially enacted practice: Sensitising themes for an emerging perspective of people-in-practice. <em>Journal of Documentation, 68</em>(6), 772-783.
</li>
<li id="lun10">Lundh, A. H. (2011). <em>Doing research in primary school. Information activities in project-based learning</em>. Borås, Sweden : Valfrid.
</li>
<li id="lun12">Lundh, A., &amp; Alexandersson, M. (2012). Collecting and compiling: the activity of seeking pictures in primary school. <em>Journal of Documentation, 68</em>(2), 238-253.
</li>
<li id="lun11">Lundh, A. Davidsson, B. &amp; Limberg, L. (2011) Talking About the good Childhood: An Analysis of Educators’ Approaches to School Children’s Use of ICT. <em>Human IT. 11</em>.2: 22-46.
</li>
<li id="mor97">Morgan, D. L. (1997). <em>Focus groups as qualitative research</em>. London : SAGE.
</li>
<li id="mul11">Mullany, L.J., 2011. Frontstage and backstage: Gordon Brown, the “bigoted woman” and im/politeness in the 2010 UK General Election. Linguistic Politeness Research Group (2011). Discursive approaches to politeness. Berlin: De Gruyter Mouton.
</li>
<li id="pin10">Pinch, T. J. (2010). The Invisible Technologies of Goffman's Sociology From the Merry-Go-Round to the Internet. <em>Technology And Culture</em>, (2), 409.
</li>
<li id="rad11">Radford, M. L., Radford, G. P., Connaway, L. S., &amp; DeAngelis, J. A. (2011). On virtual face-work: an ethnography of communication approach to live chat reference interaction. <em>Library Quarterly, 81</em>(4), 431-453.
</li>
<li id="ran10">Rantala, L (2010). Digital Literacies as School Practices. In A. Lloyd &amp; S. Talja (eds.), <em>Practising information literacy: bringing theories of learning, practice and information literacy together</em>. Centre for Information Studies, Wagga Wagga, N.S.W.,
</li>
<li id="rie04">Rieh, S. Y. (2004). On the Web at Home: Information Seeking and Web Searching in the Home Environment. <em>Journal of the American Society for Information Science &amp; Technology, 55</em>(8), 743-753.
</li>
<li id="rie08">Rieh, S. Y. &amp; Hilligoss, B. (2008). College Students’ Credibility Judgments in the Information-Seeking Process. In M.J. Metzger &amp; A.J. Flanagin (eds.) (2008). <em>Digital media, youth, and credibility</em>. Cambridge, Mass.: MIT Press.
</li>
<li id="rie16">Rieh, S. Y., Collins-Thompson, K., Hansen, P., &amp; Lee, H. J. (2016). Towards searching as a learning process: A review of current perspectives and future directions. <em>Journal of Information Science, 42</em>(1), 19-34.
</li>
<li id="rut15">Rutter, S., Ford, N., &amp; Clough, P. (2015). How Do Children Reformulate Their Search Queries?. <em>Information Research, 20</em>(1).
</li>
<li id="sub15">Subramaniam, M., Taylor, N. G., St. Jean, B., Follman, R., Kodama, C., &amp; Casciotti, D. (2015). As simple as that?: tween credibility assessment in a complex online world. <em>Journal of Documentation, 71</em>(3), 550-571.
</li>
<li id="sun09">Sundin, O. &amp; Francke, H. (2009). <a href="http://www.webcitation.org/6jwrfTi0p" target="_blank">In search of credibility: pupils' information practices in learning environments.</a> <em>Information Research, 14</em>(4) paper 418. Retrieved from http://InformationR.net/ir/14-4/paper418.html (Archived by WebCite® at http://www.webcitation.org/6jwrfTi0p).
</li>
<li id="sun15">Sundin, O. (2015). Invisible Search : Information Literacy in the Swedish curriculum for compulsory schools. <em>Nordic Journal Of Digital Literacy</em>, 193.
</li>
<li id="smc15">The Swedish Media Council (2015). <em>Ungar och Medier</em>. Stockholm: Medierådet.
</li>
<li id="sna13">The Swedish National Agency for Education (2013). <em>It-användning och it-kompetens i skolan.</em> Stockholm: Skolverket.
</li>
<li id="tjo06">Tjora, A. (2006). Writing small discoveries: an exploration of fresh observers' observations. <em>Qualitative Research, 6</em>(4), 429-451.
</li>
<li id="wol02">Wolfinger, N.H.,. (2002). On writing fieldnotes: collection strategies and background expectancies. <em>Qualitative Research, 2</em>(1), 85-95.
</li>
</ul>

</section>

</article>