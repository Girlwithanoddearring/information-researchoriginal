<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Stories of storytime: the discursive shaping of professional identity among Swedish children’s librarians

## [Åse Hedemark](#author) and [Jenny Lindberg](#author)

> **Introduction.** This paper reports an empirical study on Swedish children’s librarians’ shaping of professional identity, as it emerges from the participants’ statements about their work with storytime activities and the competences required.  
> **Method.** The empirical material consists of a transcript from one focus group interview with eight children’s librarians.  
> **Analysis.** A discursively oriented text analysis was conducted. The transcript was initially examined in terms of the explicit contents of the participants’ statements. It was then interpreted further with an analytical focus on the concepts of professional identity and key narratives.  
> **Results.** The analysis indicates both shared and conflicting ideas concerning work methods and competencies that define and legitimise librarians’ storytime sessions. In discussing competences, the librarians’ point out inter-professional relations, especially to teachers, as important aspects of identity shaping. A key narrative unfolds, with the library presented as an enclosed, magical space.  
> **Conclusions.** Storytime is a debated object of professional expertise. There is a lack of a specialist terminology for storytime activities, which may counteract the professional knowledge claims of this female-intensive group. The professional identity of children’s librarians appears multifaceted and somewhat vague and thus calls for further investigation.

<section>

## Introduction

In Swedish library policy-making and legislation children are frequently cited out as a prioritised group (e.g. [_Bibliotekslag_ 2013, 8 §](#bib13)). Accordingly, children’s librarians may be regarded as an important occupational group, both as professional guardians of children’s interests in the public library field and as relevant subjects of library and information science research. However, to date few library or information science studies have focused on this professional group, its activities and identity. The aim of the empirical study reported here is to extend knowledge of the professional identity of children’s librarians by investigating how they view and talk about a frequent activity in their daily work, and how they describe the knowledge and competences it requires. The activity in focus is that of _storytime_.

The tradition of storytime in Swedish public libraries dates back at least a century. Owing to recent reports (e.g. [Carlsson and Johannisson 2012](#car12)) and debates on young people’s declining reading ability, the pedagogical potential of storytime, as well as of activities like book talks, book clubs and baby rhyme, has been highlighted. Today, this kind of (potentially) literacy-promoting activities arguably, to a large extent, constitute the _raison d´être_ of children’s librarians. This, in turn, makes storytime a good starting point for our investigation of children’s librarians’ professional identity.

The analysis focuses on certain aspects of _professional identity_, such as librarians’ varying ways of describing and defining the activity of storytime, as well as on their different views regarding what are perceived as appropriate work methods and professional _competences_ needed for the job. The analysis rests upon the assumption that an occupational group’s specific knowledge claims are a constituent of the profession (cf. [Abbott 1988](#abb88)). As a means of capturing important aspects on the micro level of children’s librarians’ professional identity, the concept of _key narratives_ ([Wackerhausen 2004](#wac04); [2009](#wac09)) is applied. In order for this aim to be achieved, the following research questions have guided the study:

Q1\. In what ways do children’s librarians describe their professional work with storytime activities, and according to what lines of reasoning do they claim relevance to their work?

Q2\. What competences do children’s librarians describe as crucial for professional storytime activities?

Q3\. What key narratives can be identified in the children’s librarians’ statements, and what do they imply?

The paper is structured as follows: The section below contains a selective review of related research. This is followed by an account for the theoretical framework. Before presenting the results, we describe our method. The paper ends with a concluding discussion.

## Previous research

Research on professions was long carried out primarily within the field of sociology. Over the past decades new approaches have prevailed: occupational groups and professionalisation processes have been studied in their own disciplinary contexts ([Nolin 2008](#nol08)). Theory of professions has also been used in studies analysing the development of librarianship as a profession (e.g. [Tuominen 1997](#tuo97); [Abbott 1998](#abb98); [Sundin 2008](#sun08); [Dilevkov 2009](#dil09); [Huvila et al. 2013](#huv13); [Lindberg 2015](#lin15); [Kann-Rasmussen and Balling 2015](#kan15)).

Studies on the profession of children’s librarianship are scarce, and consist mostly of master’s theses (e.g. [Andersson and Ekberg 2007](#and07)) and applied research reports (e.g. [Rydsjö and Elf 1997](#ryd07); [Rydsjö, Hultgren and Limberg 2010](#ryd10); [Borrman and Hedemark 2015](#bor15)). Fraser’s dissertation from 1997 is noteworthy, examining the professional role of children’s librarians and finding that it is usually considered somewhat unclear and often associated with low status. These findings correspond well with those on other professions involving work with children ([Fraser 1997](#fra97)). Fraser’s study showed that many children’s librarians have trouble articulating the importance of their work and services. Members of an occupational group who are unsure of their own significance, will find it difficult to convince colleagues, decision-makers and cooperating professionals of the value and legitimacy of their work. Children’s librarians often collaborate with other professionals who share their interest in doing what is best for the child in terms of stimulating their interest in books and reading, developing literacies and encouraging them to participate in cultural activities etc.

Research has shown that professions that work together at some level, such as children’s librarians, preschool teachers, teachers and children’s nurses, often have different views on how to achieve their common goals (e.g. [Lundh 2011](#lun11); [Borrman and Hedemark 2015](#bor15)). This indicates colliding intra-professional approaches, which in turn may impede collaboration (cf. [Wackerhausen 2009](#wac09)). Another fact that should not be disregarded is the overall _female intensity_ prevailing in librarianship (cf. [Harris 1992](#har92)), and among children’s librarians in particular. Christine Williams’ ([1992](#wil92)) study on men’s careers in female-intensive occupations shows that male librarians must sometimes defend their active choice to work on the library floor instead of striving for more prominent positions in the organisation. One participant in Williams’ study was, for example, although commended by his supervisor for his good work in storytelling and related activities, criticised for ‘_not shooting high enough_’ ([Williams 1992, p. 257](#wil92)). Williams states that ‘_throughout his ten-year career, he has had to struggle to remain in children’s collections_’ (ibid., p. 257).

## Theoretical framework

The theoretical framework guiding this study takes its starting point in a social constructionist perspective, emphasising the collectivist aspects of the formation of professional identities. Drawing on the work of Wackerhausen ([2004](#wac04), [2009](#wac09)) we acknowledge the importance of exploring professional identities on both a macro and a micro level. The macro-level may be described as _the public face_ of a profession, in the sense that it concerns identity as defined not only by the professionals themselves but also, for example, by other professions and the public opinion ([Wackerhausen 2009](#wac09)).

Since the empirical study primarily captures the micro level of professional identity, we deal only with the macro-level of children’s librarians’ identities with reference to previous research. Wackerhausen ([2009](#wac09)) describes professional identity on a micro level as ‘_a joint designation of the qualities (whatever they might be) that a person must possess to be a fully acknowledged member of a given profession_’ (p. 459). This includes following an array of norms concerning professional conduct and ways of dealing with work tasks. In this study the focus on norms (or _rules_, with Wackerhausen) is narrowed down to three of the five identity-constituting elements of professional practice presented by Wackerhausen ([2009, p. 460](#wac09)). These elements are: _asking the types of questions we do, understanding and explaining things the way we do_ and the _key-narratives_ of the professional group. These elements are no doubt intertwined in practice, but for analytical reasons we will elaborate on the concepts separately.

### Asking the types of questions we do

According to Wackerhausen ([2009](#wac09)), the first-mentioned element, i.e. _asking the types of questions we do_, is an essential part of professional identity, which entails the process of socialisation into a profession. Children’s librarians would, for instance, be expected to conduct themselves in specific ways according to the norms of the professional group. Conduct hereby implies both physical behavior and ways of defining and advocating solutions to professional problems in the workplace. Regarding norms, it is important to note that there is not always a consensus within the professional group, neither on the groups’ prioritised objectives and problems, nor on their preferred solutions. Such matters may rather be subject to negotiation and debate. These negotiations may take place in both intra-professional and inter-professional interaction, as well as with library users. They involve asking the ‘right’ type of questions when confronted with a professionally tricky situation, approaching the library user in the ‘right’ way, and, overall, behave in accordance with accepted norms.

The element of _understanding and explaining things the way we do_ is closely connected to the above, but also includes the professionals’ use of metaphors and certain specialist terms, concepts and phrases that typically prevail among librarians. This element denotes a certain way of talking about occupational matters adopted through education and professional practice.

### Key narratives

The concept of _key narratives_ is an analytical tool applied to trace stories or myths that are shaped and reproduced within a specific community. These narratives are intended to represent what is perceived as essential and of interest not only to the individual, but to the whole group ([Wackerhausen 2004](#wac04)). A key narrative, as we understand it, typically presents an example with a symbolic meaning beyond the actual statements. Such stories or narratives are also characterised by a certain quality of recognition that may stem from common experience. In such cases a key narrative brings a sense of collective belonging to the members of a group (ibid.). At the same time, however, the narrative may exclude some members, such as newcomers who do not share the background of more established participants.

## Method

The empirical material was produced through an interview with a focus group of eight children’s librarians, from the same number of libraries, discussing storytime. The interview, in Swedish, took place at the region’s central library and lasted approximately two hours. It was recorded and transcribed in a 31-page document. The participants were all women with varied professional experience of up to several decades’ duration as children’s librarians. Excerpts from the empirical material are frequently used to illustrate and support the analysis. To ensure anonymity, the participants were assigned pseudonyms. All quotations are reported verbatim after being translated into English by the authors. During the interview, the specific activities of storytime sessions were discussed vividly.

In the analysis, the transcripts from the interview were read repeatedly by both authors and examined with regard to both explicit and implicit content. Initially the statements were analysed in terms of what the participants actually said that they do at work. Thereafter, the analytical process moved from the descriptive to the more theoretical level, which contributed to the unfolding of the potential meanings of what was being said (cf. Kvale and Brinkmann 2009). This final step also included the discursively oriented analysis focusing on understanding the librarians’ attitudes and approaches as part of the shaping of professional identity.

In summary, the analysis may be described as based on recurrent close reading of the empirical material with a focus on the elements introduced in the theoretical framework, i.e. the elements of _asking the types of questions we do, understanding and explaining things the way we do and the key-narratives_ of the professional group.

## Results

The first step of the analysis is to look into the participants’ ways of describing and reasoning about their work in storytime sessions at their workplaces. The presentation of the results is structured according to the three research questions.

### Professional storytime activities

Although almost all public libraries offer more or less organised storytime sessions, normally directed at children aged three to five, there are rarely any articulate or documented objectives guiding the activity. When the librarians relate what they do, many strongly emphasise their role as promoters of literature. ‘_I’m not a storyteller, but a reading-promoter. I want to provide the children with **the book**! I’m not [merely] a narrator of fairytales_’ (Katarina).

Other librarians (such as Nelly, below) state that telling the story is the most important thing they do:

> I’ve worked a lot with storytelling. It’s a kind of performance that needs an enclosed space. [...] You should be properly prepared, because I hear this from others [librarians], that sometimes ‘[y]ou just take a book and start reading aloud – it doesn’t even matter if you have read the book.’ But I wouldn’t call that a storytime session, not according to my standards. You have to have read the story beforehand, and it [the story time session] should be coherent. Ideally you should be able to **tell** the story, not just read it aloud. (Nelly)

The characteristics of both standpoints – emphasising, respectively, promoting reading and telling stories – are both well anchored in the empirical material, not least in its frequent references to their respective sets of material attributes. For example, the reading-promoters speak primarily about books while the storytellers refer to tools like finger puppets and _kamishibai_, (a narrating method of Japanese origin, using sets of picture cards as points of departure for storytelling).

Nelly’s statement also reflects a view on the professional practice of storytime as closely connected to the virtues of bringing the story to life by telling it by heart, and the importance of being well prepared. From the above quotation on how other librarians do not prepare properly for storytime sessions, she appears to consider merely reading aloud from a book almost as cheating. In advocating the distinctive features of narration, Nelly also refers to her own extensive experience and her advanced professional competence.

However, other participants stress that planning for a storytime session does _not_ necessarily take much time. The activity could well, they say, be approached more spontaneously and flexibly. In these statements, storytime sessions and the librarians’ work in them are defined differently:

> _[S]imply pick up a picture book and start reading when a group of pre-school children are visiting and say: ‘Come over here and listen!’ This [way of conducting storytime] is equally valuable. It doesn’t have to be thoroughly prepared and scheduled in the calendar or anything like that._ (Pernilla)

On a similar note another participant adds:

> _You may start off with very high ambitions, for example that it [the story time session] should be staged in a closed room, there should be a certain number of children, everything should be very well prepared, and you should know the story and preferably be able to **tell** it – not **read** it. Many librarians actually get panic attacks from thinking this is expected of them [...] I think you ought to get the opportunity to practise._ (Jonna)

The overall impression given by the empirical material is that the very definition of a storytime session, as well as its aim, is closely related to the participants’ varying professional experience. The least experienced children’s librarians argue that storytime may – at least early on in a librarian's career – consist of reading books aloud to children. However, to be a fully recognised member of the profession the librarian should be able to do more, such as staging well-prepared storytime sessions in which stories are dramatised. It is sometimes implied that inexperienced librarians need to adjust their behaviour according to the rules of ‘good conduct’ and direct their work efforts as responses to ‘the types of questions children’s librarians ask’ (cf. [Wackerhausen, 2009](#wac09)). As Jonna puts it: a librarian ‘_ought to get the opportunity to practise_’ and learn.

Some of the librarians express what may be interpreted as an attempt to combine the opposing positions. Paula says, for example:

> _Well, if you, as a professional, want to define storytime as an activity that includes many ingredients and as something really professionally advanced, I think it’s perfectly alright. But that doesn’t mean that you can’t have other standards for reading activities, more spontaneous or whatever_… (Paula)

On the one hand, this statement represents the ambition to merge the two points of view, but on the other hand, Paula emphasises the differing status accorded to the respective ways of conducting storytime. Being thoroughly prepared and mastering a repertoire of narrative techniques confer higher status than a straightforward, spontaneous reading session. Nevertheless, Paula suggests that both ways should be accepted.

As a librarian, being confronted with new practitioners who actually question established ways of doing things may, as the excerpts indicate, be somewhat traumatic (cf. [Wackerhausen 2009](#wac09)). It may prompt individuals’ doubt about the direction of their work, which in turn risks destabilising their professional identity (ibid.). Such doubt may be at play when Paula subsequently states that, at least for her, the time-consuming preparations for storytime, choosing the right story etc., bring true pride. During the interview she keeps reverting to this and the fact that it has taken her a long time to come up with a storytime concept that works.

Summing up, the empirical material has provided us with varoius descriptions of what storytime is and how it should be _conducted_. The two different positions identified are characterised by their focus on _reading books_ and _telling stories_. These positions indicate conflicting ideas about _the way we do and explain the things we do_.

### Competences needed for storytime sessions

From the empirical material, the lack of a specific nomenclature for describing librarians’ professional activities and work methods is noticeable. At one point, Jonna explicitly calls for more advanced terminology: ‘_[W]e should praise the practice of storytime to the skies, we should write reports about it, give it the status it deserves, and we should speak ‘gobbledygook’_’ (Jonna). This statement illustrates an awareness of the fact that doing research about a phenomenon (as in the reference to reports) and developing more academic language with specialist terminology, also raises the status of the phenomenon and the profession concerned.

It is also striking that the children’s librarians’ ways of understanding and reasoning about their professional activities seem to be influenced by activities in the private or domestic sphere and by the way children express themselves. The everyday language is probably intentional: in order to attract and welcome very young users, children’s librarians may choose wordings to describe their practice in a way intended to appeal to children, as in storytime, book-talk or a fairytale-play. However, there is a risk of other professional groups not acknowledging storytime as an activity at professional level.

Previous research has shown that librarians often perceive that they are underestimated or misunderstood by other groups (e.g. [Limberg and Folkesson 2006](#lim06); [Lindberg 2015](#lin15)). In this study such opinions are indeed expressed by the children’s librarians interviewed. One says: ‘_[W]e offer this amazing service, and then you notice that the teachers have neither the time nor the energy to take part in it_’ (Jonna). Nolin ([2008, p. 9](#nol08)) argues that to increase the quality of their work, professionals need to develop concepts and theories to describe what they do. Statements in the empirical material reflect an awareness of the importance of this. For example: ‘_We should perhaps get better at describing (to others) what we do, why we do it and with what methods_’ (Paula).

One way of defining the competences needed by children’s librarians is to compare their knowledge and skills with those of other professionals, such as preschool and school teachers. Malin asserts that preschool teachers ‘_are really good at telling stories_’, while librarians are better at finding literature. According to the participants, other professionals generally acknowledge that children’s librarians know about children’s literature, and that librarians are better equipped to select books. Recurrent claims are ‘_I know books_’ and ‘_I have knowledge about literature_’.

Apart from knowing about children’s literature, it is noticeable that some librarians in the focus group more implicitly claim to have certain knowledge about children and how to interact with them. Nelly relates that there is a kind of ‘_magical distance_’ in storytime sessions – that by moving physically closer to the children, librarians promote communication and the opportunity to connect. We interpret this as a kind of didactic knowledge, not clearly articulated as such, but nonetheless perceived as a valuable competence for children’s librarians.

Other skills mentioned are the flexibility and the ability to adjust the sessions to the groups of children attending. The trait of humour is also referred to as a useful tool for interacting with children.

To conclude, a lack of a unique professional terminology is discernible. This, in turn, may adversely affect the professional interest and status of children’s librarians negatively. Conflicting views on what professional storytime activities involve are also reflected in the discussion about which competences are important.

### A key narrative and its implications

One salient key narrative emerges from the empirical material. It emanates from a metaphor of the library as a source of magic, accessible for dedicated children’s librarians. Participants draw on this narrative when they describe successful storytime sessions as ‘magical moments’ and state that the purpose of these activities is to ‘create a little bit of magic’ or ‘serve a magical experience to the children’. Within this narrative, the participants speak of storytime sessions as achievable escapism, implying that children are in need of distraction from sometimes unpleasant realities.

As mentioned above, librarians may propose different sets of tools for their storytime sessions, but the virtues ascribed to the library unite them:

> _I often think that through the stories and through the storytelling, the library becomes charged; the children look at it with completely new eyes on their next visit. [...] One mother told me that it’s like when you get in the vicinity and they [the children] see the ‘LIBRARY’ in big letters and suddenly burst out: There it is! There’s the library!_ (Nelly)

The librarians’ shared testimonies from particularly successful sessions in which interaction with the children was dynamic and rewarding. These seemed to bring the focus group participants to a common understanding, if not of their work methods, at least of their motivation and view of the library as a powerful resource.

The narrative of the _library as a source of magic_ also reflects a certain image of what children are or ought to be like, which includes appreciating magic and fantasy. In the statements, children are implicitly categorised as a somewhat different and exotic group of people, concerned with fantasy and magic. The intentions of providing children with ‘_magical_’ encounters with books and fairytales are no doubt genuine, but when the librarians base storytime activities on these assumptions about children and childhood they also risk reproducing stereotypes. Children who do not match the expectations of being curious and imaginative may be excluded since full participation is often conditioned by the specific cultural context of storytime defined by the librarians as ‘a magical moment’.

Storytime as situated in the library imposes certain conditions on the librarians’ interaction not only with children, but also with their teachers and parents. One participant states firmly:

> _We don’t allow for the teachers. They must be somewhere else. [...] We only want the children to come. They should be old enough to look after themselves without mum or dad or the teachers. That works perfectly well._ (Kerstin)

The fact that the teachers, who are supposedly responsible for the children during library visits, are not always allowed to attend the storytime sessions has its reasons, according to the librarians. Occasionally there have been incidents when children’s librarians have found teachers to be a hindrance to fruitful interaction with the children, since they seek to communicate with the librarian on an adult level during storytime. Parents too may impede successful storytime sessions by becoming the prime object of their child’s attention. This may be particularly disturbing since physical closeness and attentive communication between librarians and children are important constituents of this key narrative.

Summing up, the key narrative of _the library as a source of magic_ seems to unite children’s librarians in this study insofar as it brings a sense of collective belonging to the members of the group and to children who relate to and appreciate fantasy and magic. However, children who do not relate as expected to such virtues risk being excluded. Accompanying adults, such as preschool teachers and parents, play no legitimate part in this key narrative either.

## Concluding discussion

How can the professional identity of children’s librarians be understood through the activity of storytime? The participants in this study discuss storytime as a frequently occurring task at public libraries. However, the analysis indicates that storytime is an elusive object of professional expertise. We have identified a divide between reading promotion, which focuses on books, and reading aloud, on the one hand, and storytelling, with a focus on organised sessions in which stories are dramatised or retold by heart, on the other. These two approaches also call for differing competences. Regarding competence and knowledge claims, an overall lack professional terminology for storytime activities has been identified. This, by extension, may serve to counteract the interests of children’s librarianship as a female-intensive profession.

Through the discursively oriented analysis a key narrative, portraying _the library as a potential source of magic_, emerged. Several of the participating librarians draw on this metaphorical narrative to describe what is actually at play during a successful storytime session. According to this key narrative storytime should take place in the library (or possibly in a mobile library), preferably in an enclosed space. The latter is referred to as a prerequisite for engaging the inherent power of the venue. Furthermore, this narrative indicates that the magic is reserved for librarians and children, while for example parents or preschool teachers are preferably left out.

The identities associated with the two views on storytime described above seem closely connected to the librarians’ own personal identity (cf. [Kann-Rasmussen and Balling 2015](#kan15)). In cases when professional identity is intertwined with personal identity, it is harder for librarians to reflect on their professional practice and actually question ways of conducting the work. Wackerhausen ([2009](#wac09)) asserts that professional reflection with a critical stance to how things are being done may be hard, and even perceived as threatening, when people’s personal identity has become entangled with their professional identity. Librarians initiating such critical reflection, or otherwise departing from professional norms and standards, thus run the risk of getting into trouble. It is a matter of ‘_[t]he friendly side of professional socialization and membership [being] inclusion and rewards, but the unfriendly side is punishment and exclusion_’ ([Wackerhausen 2009, p. 467](#wac09)).

Although practitioners of related professions work together and share basic goals, such as in this case doing what is best for the children, they may also compete with one another to some extent ([Wackerhausen 2009](#wac09)). Results from this analysis, as well as from previous research (e.g. [Borrman and Hedemark 2015](#bor15)) indicate that being the entertaining, storytelling children’s librarian is seen as somewhat burdensome. Jonna, for example, claims that ‘_many librarians get panic attacks thinking this [i.e. telling the story, not reading it] is expected of them_’. According to the same line of reasoning, the study by Borrman and Hedemark ([2015](#bor15)) shows that library managers find it difficult getting librarians to conduct storytime sessions, because the librarians believe they have to put on a show and perform. This has resulted in libraries recruiting ‘storytellers’ in addition to regular staff. On the one hand, this may imply that this task is regarded as something many librarians would rather avoid; on the other hand, it may suggest that storytime is an activity for which others are better equipped.

The present study, being based on a single focus-group interview, is limited. However, the results fit in well with previous research on female-intensive professions such as nursing, teaching and social work ([Williams 1995](#wil95)). Such professional groups are generally undervalued and/or underpaid since they are considered to be ‘women's professions’. In the mid- to late 19th century, librarianship was transformed by a process of feminisation ([Williams 1995](#wil95)).The underlying logic of this transformation was the ideology of domesticity, which helped to legitimise librarianship as a suitable occupation for women at the time. The attributes of this ideology alluded to women’s innate ability to take care of others and patience in performing tedious work accurately. It was also claimed that women’s maternal instincts made them suitable candidates for work in children’s libraries ([Williams 1995, p. 28](#wil95)).

It was stated in the early 1990s that public opinion often assumes that anyone could perform ‘women's work’ such as librarianship, because it requires no professional skills ([Harris 1992, p. 28](#har92)). The historical background of librarianship, and the characteristics librarians seem to share with the counterparts in other female-intensive professions, must be taken into consideration in investigating children’s librarians’ professional identity. These dimensions form part of the macro level of professional identity and, as Wackerhausen ([2009](#wac09)) claims, this level too shapes and gives content to a profession.

The results presented here contribute to an understanding of how professional norms affect children’s activities at libraries. Furthermore, they shed light on collaboration between various professional groups with an interest in work promoting children’s literacy. Given the limited scope of this small-scale empirical study, the tentative results call for further investigation. Future research could, for example, explore the multifaceted but somewhat vague professional identity of children’s librarians in the light of the growing body of media and information literacy studies. The advent of new media and literacies is indeed likely to bring new challenges to the practice of children’s librarianship.

## <a id="author"></a>About the authors

**Åse Hedemark** is Senior Lecturer in Library and Information Science, at the Department of Archival Science, Library & Information Science, and Museum & Heritage Studies, Uppsala University. Hedemark completed her PhD in Library and Information Science in 2009\. Her PhD project investigated the public image of public libraries using discourse analysis. Her current research interest includes children’s literacy practices and information literacies in public libraries and other cultural institutions. She can be contacted at [ase.hedemark@abm.uu.se](mailto:ase.hedemark@abm.uu.se)  
**Jenny Lindberg** is a lecturer at the Swedish School of Library and Information Science, University of Borås. In 2015 she defended her doctoral thesis, where the information seeking and the shaping of professional identities among library and information science students and novice academic librarians were investigated. Lindberg’s current research activities focus on professional information practices and on research use as a means of professionalisation. She can be contacted at [jenny.lindberg@hb.se](mailto:jenny.lindberg@hb.se)

</section>

<section>

## References

<ul>
<li id="abb88">Abbott, A. (1988). The system of professions: an essay on the division of expert labor. Chicago: Univ. of Chicago Press.
</li>
<li id="abb98">Abbott, A. (1998). Professionalism and the future of librarianship. Library Trends, 46(3), 430–444.
</li>
<li id="and07">Andersson, E. &amp; Ekberg, T. (2007). En profession med mission: barnbibliotekariers yrkesidentitet [A profession with a mission: the professional identity of children’s librarians]. Lund: Lunds University.
</li>
<li id="bib13">Bibliotekslag (2013:801) [the Swedish Library Act 2013:801]. Stockholm: Kulturdepartementet.
</li>
<li id="bor15">Borrman, P. &amp; Hedemark, Å. (2015). Leonards plåster. Om syfte, barnsyn och kvalitet i bibliotekets sagostund [Leonard’s plaster: on purpose, perceptions of children, and quality in library storytime]. Stockholm: Regional Library of Stockholm.
</li>
<li id="car12">Carlsson, U. &amp; Johannisson, J. (Eds.) (2012). Läsarnas marknad, marknadens läsare: en forskningsantologi [The readers’ market, the market’s readers: a research anthology]. Stockholm: Offentliga förlaget, Publit.
</li>
<li id="dil09">Dilevkov, J. (2009). The politics of professionalism: a retro-progressive proposal for librarianship. Duluth, MN: Library Juice Press.
</li>
<li id="fra97">Fraser, M. (1997). Evaluating the role of the professional children’s librarian: an analysis of functions and development. London: University of London.
</li>
<li id="har92">Harris, R. (1992). Librarianship. The erosion of a woman’s profession. Norwood: Ablex publishing.
</li>
<li id="huv13">Huvila, I., Holmberg, K., Kronqvist-Berg, M., Nivakoski, O. &amp; Widén, G. (2013). What is librarian 2.0 – new competences or interactive relations? A library professional viewpoint. Journal of Librarianship and Information Science, 45(3), 198–205.
</li>
<li id="kan15">Kann-Rasmussen, N. &amp; Balling, G. (2015). Every reader his book – every book its reader? Notions on readers’ advisory and audience development in Danish public libraries. Journal of Librarianship and Information Science, 47(3), 242–253.
</li>
<li id="lim06">Limberg, L. &amp; Folkesson, L. (2006). Undervisning i informationssökning. Slutrapport från projektet Informationssökning, didaktik och lärande (IDOL) [Teaching in information seeking. Final report from the ‘Information seeking, didactics, and learning’ project (IDOL)]. Borås: Valfrid.
</li>
<li id="lin15">Lindberg, J. (2015). Att bli bibliotekarie. Informationssökning och yrkesidentiteter hos B&amp;I-studenter och nyanställda högskolebibliotekarier [Learning librarianship: information seeking and professional identities among LIS students and novice academic librarians]. Borås: Valfrid.
</li>
<li id="lun11">Lundh, A. (2011). Doing research in primary school. Information activities in project-based learning. Gothenburg: Valfrid.
</li>
<li id="nol08">Nolin, J. (2008). In search of a new theory of professions. Borås: University of Borås.
</li>
<li id="ryd07">Rydsjö, K. &amp; Elf, AC. (2007). Studier av barn- och ungdomsbibliotek. En kunskapsöversikt [Studies on children’s and young adults’ libraries: a knowledge review]. Stockholm: Regionbibliotek Stockholm.
</li>
<li id="ryd10">Rydsjö, K., Hultgren, F. &amp; Limberg, L. (2010). Barnet, platsen, tiden. Teorier och forskning i barnbibliotekets omvärld [The child, the place, the time: theories and research in the world of the children’s library]. Stockholm: Regional Library of Stockholm.
</li>
<li id="sun08">Sundin, O. (2008). Negotiations on information-seeking expertise: a study of web-based tutorials for information literacy. Journal of Documentation, 64(1), 24–44.
</li>
<li id="tuo97">Tuominen, K. (1997). User-centered discourse: an analysis of the subject positions of the user and the librarian. Library Quarterly, 67(4), 350–371.
</li>
<li id="wac04">Wackerhausen, S. (2004). Professionsidentitet, sædvane og akademiske dyder [Professional identity, custom and academic virtues]. In N.B. Hansen &amp; J. Gleerup (Ed.), Videnteori, professionsuddannelse og professionsforskning (pp. 13–29). Odense: University Press of Southern Denmark.
</li>
<li id="wac09">Wackerhausen, S. (2009). Collaboration, professional identity and reflection across boundaries. Journal of Interprofessional Care, 23(5), 455–473.
</li>
<li id="wil92">Williams, Ch. (1992). The glass escalator: hidden advantages for men in the “female” professions. Social problems, 39(3), 253–267.
</li>
<li id="wil95">Williams, Ch. (1995). Still a man’s world. Men who do women’s work. Berkeley: University of California Press.
</li>
</ul>

</section>

</article>