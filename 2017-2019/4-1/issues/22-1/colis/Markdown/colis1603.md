<header>

#### vol. 22 no. 1, March, 2017

## Proceedings of the Ninth International Conference on Conceptions of Library and Information Science, Uppsala, Sweden, June 27-29, 2016

</header>

<article>

# Information through the lens: information research and the dynamics of practice

## [Sally Irvine-Smith](#author)

> **Introduction.** Information researchers have been urged to move beyond the individual behaviourist paradigm to broaden our view of information activities. Increasingly information scholars suggest we treat information activities as practices, and to re-position information research within the bounds of practice theory. But what exactly is practice theory, how does it relate to information, and what does it offer to information research?  
> **Aim.** This paper takes as its starting point the arguments of those who suggest an information practice approach can better identify and analyse the full range of information activities. It briefly examines the practice approach in the social sciences and identifies seven essential characteristics of practices: action-based, temporal, situated, continuous and recurrent, embodied, mediated and social. Using these characteristics as categories of activity, the researcher can develop a conceptual framework of practice which may be profitably utilised in an empirical examination of human information.  
> **Conclusion.** The paper concludes that an examination of information activities in accordance with this framework of practice could allow researchers to gain a wider understanding of all that goes into a person becoming informed.

<section>

## Introduction

The field of information research has latterly seen a number of scholars professing a level of dissatisfaction with the information behaviour approach. The case for broadening the scope of information research beyond that of information behaviour is argued on two fronts, both resulting from the strictures that the individualism implicit in the approach are perceived to place upon research.

Firstly, if information behaviour is defined as those activities engaged in when a person identifies his or her own needs for information, actively or passively searches for it, thence ultimately uses it ([Wilson, 1981, p. 249](#wil81); [2000, p. 49](#wil00)) then information behaviour research is too strongly focussed on directed seeking and use. This focus on the individual as ‘user’ and ‘seeker’ operates to such an extent that all other elements of information behaviour are largely ignored ([Vakkari, 2008](#vak08)).

Many models of information behaviour are limited in their ability to describe every day information activities and the attendant research focusses on active information seeking, to the neglect of less-directed activities ([McKenzie, 2003, p. 19](#mck03)). These models start from a posited lack of information on the part of the individual and proceed on the assumption that the individual thence seeks out information in response to this acknowledged and articulated need or problem. However lived experience - and research - suggest that problem-solving actually occupies very little space in the totality of human experience. Despite this, problem solving and the concept of the problematic situation have been given a greatly exaggerated role in information research ([Talja, 2010](#tal10); [Talja & Nyce, 2015](#tal15)).

Thus, it is argued, the concept of information behaviour privileges the activities of directed searching and use over all other activities, which are either neglected or attributed a less prominent role. Information behaviour research is accordingly perceived as skewed towards investigating those self-aware individuals who are actively problem-solving or decision-making. Examining certain activities to the exclusion of others in information research must necessarily lead to an incomplete understanding of how humans become informed.

Secondly, even if information behaviour researchers extricate themselves from the strictures of information seeking and broaden their examination of information activities to others such as serendipitous discovery or wilful omission, emphasis is still very much concentrated on the requirements and particular situation of the individual. This ‘needs and motives’ focus of information behaviour does not adequately take into account the social context, or the communicative aspects of information ([Savolainen, 2007, p. 126](#sav07)). Consequently, much information research pays scant attention to the social aspects of information processes, either in terms of the socio-cultural context of the users or the socio-cultural context of the information system ([Talja, 1996, p. 81](#tal96)).

Many information scholars believe that all knowledge is social in origin, the individual exists always within a world that is socially and subjectively constructed and that the knowledge structure of every individual is mutually constituted within this socio-cultural environment ([Talja, Tuominen, & Savolainen, 2005, p. 82](#talTS05)). For information to make a difference, if it is to have any meaning, ‘then it must be situated and made intelligible through the contextual lens of social life’ ([Lloyd, 2010b, p. 247](#llo10b)). Thus, an investigation of how the individual becomes informed must necessarily be investigated in terms of how they are sited within the social.

Emphasis on the individual ‘user’ of information discounts the communicative and social aspects of human information activities, placing less emphasis on the non-directed, on-going nature of human understanding developing, always in conjunction with others, over time.

## The call to practice

Many researchers who perceive limitations in the study of individual information behaviour advocate a practice approach in an endeavour to more fully explain and analyse the full gamut of information activities. ‘A focus on practices rather than on behaviour shifts the analysis from cognitive to social and is consistent with the study of information seekers within their social context’ ([McKenzie, 2003, p. 24](#mck03)). Studies of information practices have always constituted ‘a more sociologically and contextually oriented line of research’ ([Talja, 2005, p. 123](#tal05)).

According to its proponents, the practice approach to information research is superior to research concentrating on the behaviours of the individual, because it ‘assumes that the processes of information seeking and use are constituted socially and dialogically, rather than based on the ideas and motives of individual actors’ ([Tuominen, Talja, & Savolainen, 2005, p. 328](#tuo05)). It conveys the view that since all human practices are social, information activities - including perceiving needs, seeking and use - are socially constituted ([Talja & McKenzie, 2007](#tal07)). It allows for the examination of the continuous nature and habitualisation of information activities, all of which are affected and shaped by social and cultural factors (Savolainen, 2007, p. 126). It is therefore unsurprising that information researchers have looked at the communicative, shared and social nature of information activities, noted they are part of the routine accomplishments of everyday life, and seen them as dimensions of social practice ([Talja & Hansen, 2006, p. 125](#tal06)).

Savolainen defines information practices as sets of socially and culturally established ways to identify, seek, use, and share the information, often habitual and embedded in everyday contexts. But he further contends that their self-evident nature make these practices "invisible" and difficult to see in greater detail ([Savolainen, 2008, pp. 2-3](#sav08)). How then are we to study information practices? The emphasis on the behaviour of the individual information seeker may well have been, in part, because this is the aspect of information activity most easy to identify, examine and analyse.

Practice theory itself can provide us with a framework which will allow us to reveal and investigate the established, routine and unexceptional information practices of individuals as they go about their everyday lives.

## Practice theory: what people do

Practice theorists are not, in fact, united by theory. As all commentators are quick to point out, there is not a theory of practice theory: practice theories are as abundant as the theorists who propound them. Practice theorists are, nevertheless, united by a common purpose. They are those philosophers and social theorists who believe that practices are the basic unit of analysis of the social. In the practice approach the ‘basic domain of study of the social sciences…is neither the experience of the individual actor, nor the existence of any form of societal totality, but social practices ordered across space and time.’([Giddens, 1984, p. 2](#gid84)).

While practice theory can trace its philosophic antecedents to Marx and Wittgenstein, the two preeminent practice theorists of the C20th are sociologists Pierre Bourdieu and Anthony Giddens. While their respective theoretical approaches are very different, both strive to resolve the structure/agency dichotomy of the social sciences and to provide a middle ground between subjectivist/objectivist polarities ([Rouse, 2007, pp. 644-651](#rou07)). For Bourdieu, practices are interwoven activities carried out in a specific ‘field’. They are produced by dispositions – habitus – which are acquired according to the objective conditions of the field, thus becoming self-perpetuating ([Schatzki, 1997, p. 287](#sch97)). For Giddens, social structures are constituted by generative rules and resources which are both means by which social life is produced and reproduced as on-going activity, and also simultaneously produced and reproduced by this activity. Giddens calls this duality structuration: a means by which a social system can be examined in terms of its production and reproduction through social interaction ([Giddens, 1977, p. 118](#gid77)).

Despite their conceptual differences, both Bourdieu and Giddens acknowledge that human behaviour is not wholly determined by objective external structures beyond the individual’s control - societal and cultural structures do have a reality and an influence. Human society is more than a sum of the actions of all the individuals they comprise, yet is nothing more than a complex weaving of human actions, beliefs, wants and tools made meaningful by practice.

The theories of Bourdieu and Giddens are by no means uncontested, and more recent scholars such as Theodore Schatzki have attempted to address perceived deficiencies in their accounts while remaining faithful to the concept that practice is the mechanism by which social life is constituted and transformed. He sees social life as a nexus of practices, which are in turn made up of doings and sayings. For Schatzki there are four mechanisms of practice: practical understanding, general understandings, rules, and teleoaffective structures. Teleoaffectivity is central to Schatzki’s conception of practice because it explains why and when we perform an action, based on the 'oughtness' we are conditioned to feeling by socialisation ([Schatzki, 1996, pp. 89-90, 165-167, 211](#sch96)).

Schatzki, along with Giddens and Bourdieu, has a conception of practice that can be characterised ‘as what people do’. They have each developed a philosophical framework which aims to explicate the workings and interactions of human society. Latterly, however, a more epistemological conception of practice has come to the fore: practice as an interpretive ‘lens’ through which research may be construed ([Corradi, Gherardi, & Verzelloni, 2010, pp. 271-273](#cor10); [Gherardi, 2009](#ghe09). These scholars make a distinction between practice as something people do and practice as a way to study what they do.

Many researchers use the practice approach to describe situations in terms of the activities of actors, the tools they use, and how they interact – listing and enumerating the practices people do. However, some researchers advocate a stronger programme of practice research, one which seeks to explain and analyse phenomena in terms of practices or practical accomplishment; one with a strong theoretical orientation with clearly articulated assumptions ([Nicolini, 2009](#nic09); [2012, p. 13](#nic12)).

Conceiving practice as an interpretive lens rather than a method of making sense of real-world phenomena allows it to be utilised as a theoretical concept, as opposed to being the empirical object itself ([Huizing & Cavanagh, 2011](#hui11)). In accordance with the prevailing view of practice as an interpretive lens, the theoretical underpinnings of a number of practice theories have been examined and a framework of practice has been devised with which to investigate information practices.

## Through the lens: examining practice

Despite the singularity of every practice theorist’s premise, there a number of points of conceptual commonality to their world views which it is possible to identify, describe and categorise. Practice theory can be said to comprise seven basic assumptions about human social life:

*   It is based totally in _action_ and _activity_
*   Human activity is _temporal_, always existing in time
*   Human activity exists in space and is always _situated_
*   Human activities are _continuous_ and _recurrent_
*   Human activities are necessarily _embodied_
*   _Mediation_ plays a pivotal role in human actions
*   Above all, humans exist always in the _social_

These basic assumptions, essential to the concept of practice in a theoretical sense, can be used as categories through which to empirically examine social life.

#### Action and activity:

_To know the world, according to Bourdieu, one must situate oneself within real activity: ‘the preoccupied, active presence in the world through which the world imposes its presence, with its urgencies, its things to be done and said, things made to be said, which directly govern words and deed without ever unfolding as a spectacle.’_ ([Bourdieu, 1990, p. 52](#bou90))

As can be expected of theories which relate to human ‘doing’, action and activity are fundamental concepts in practice theory. The extent to which actions are causally linked in practice is up for debate([Schatzki, 1996, pp. 89-90](#sch96)), as are questions about intention and motivation ([Giddens, 1984, pp. 3-5](#gid84)) but action is central to practice. While there may be no unified practice approach, most thinkers who theorise practices conceive of them, at the basic level, as arrays of human activity ([Schatzki, Knorr Cetina, & von Savigny, 2001, p. 11](#knorr01))

As Lave notes, the ‘commitment that separates the broad family of practice theories, with their emphasis on relations and process, from other social theories is a reversal of the usual separation and priority of thought over action in the production of social life' ([Lave, 2011, p. 161](#lav11)). Practice is human being as activity.

#### Temporality:

Time – and the passage of time - is central to any conception of practice. Practices are not only situated in time but are time-dependent. Time provides meaning to situations and activities, as Bourdieu’s detailed example of gift giving shows. If a gift is reciprocated too quickly it could be deemed an insult – an unattractive eagerness to discharge an obligation, or a message that the gift given made no impression and was scarcely worthy of thought. The interval between gift and counter-gift is redolent with meaning; the time lapse is integral to the practice itself. It is not an inert gap between one action and another as objectivist accounts might have it, but a calculated interval which imbues meaning in the practice ([Bourdieu, 1977, pp. 5-7](#bou77)). In accordance with the temporal nature of practices are their historicity: Bourdieu’s habitus is a product of history, producing individual and collective practices which turn into more history ‘ensuring the active presence of past experience’ ([Bourdieu, 1990, p. 54](#bou90)).

Giddens similarly illustrates the importance of time by using the example of a school: here time is the method whereby students’ activities are routinized; its regulation, along with space, is the origin of the school’s disciplinary authority; and the ordering of school activities in time saturate them with significance ([Giddens, 1984, pp. 135-137](#gid84)).

#### Situatedness:

Practices exist in space as well as time, and the physical space – the site – in which the activity occurs cannot be conceptually separated or discounted from any analysis of the activity. There is no activity that is not situated and ‘agent, activity, and the world mutually constitute each other.’ ([Lave & Wenger, 1991, p. 33](#lav91)). Thus a bed has meaning when it is understood to be a place to sleep, or a bus stop when it is understood to be a place where one can catch a bus ([Schatzki, 1996, p. 115](#sch96)).

But situatedness can mean more than placement in a physical place: the theories of Giddens and Bourdieu both posit an essential relational positioning – the social position of the actor for Giddens ([Giddens, 1984, pp. 83-86](#gid84)) and the social space with its concomitant economic and social capital for Bourdieu ([Crossley, 2008](#cro08)).

#### Continuity and recurrence

Implicit in all practice theories is the ‘on-goingness’ of human activities. Bourdieu’s habitus is made by the past experiences of the actor which ‘at every moment’ are modified by new experiences which are themselves produced only to the extent allowable by the structures built by those past experiences (c.f.[Bourdieu, 1990, p. 60](#bou90)).

Similarly, the very basis of Giddens’ structuration theory is its recursiveness: ‘there is no structure, in human social life, apart from the continuity of processes of structuration’ ([GGiddens, 1977, p. 118](#gid77)). Analysing social systems means no more than studying the modes in which the ‘knowledgeable activities of situated actors’ are produced and reproduced in interaction across time and space ([Giddens, 1984, p. 25](#gid84)).

#### Embodiment

Since philosopher Gilbert Ryle persuasively argued that the distinction between mind and body was a category-mistake of the highest magnitude ([Ryle, 1949](#ryl49)) many social theorists have worked to disestablish thought from the privileged position it holds in the Western positivist tradition. Practice theorists are no exception and embodied action is a core element of practice theories. As Giddens says, ‘the body is the ‘locus’ of the active self’ ([Giddens, 1984, p. 36](#gid84)).

While there is disagreement about the level to which practice is inscribed on the body in the form of routines, habits and social normalisation ([Rouse, 2007, pp. 652-654](#rou07)) – and in 2001 Schatzki felt compelled to argue at length that mind was the mechanism by which practices were ordered and causally linked ([Schatzki, 2001](#sch01))- all practice theories recognise that there is no mind without body and that the agent can only ever engage with the world via her body.

Indeed, it has been suggested that scholars who do not take into consideration the active role of bodies and materiality cannot be considered practice theorists ([Nicolini, 2012, p. 170](#nic12)).

#### Mediation

Practices are mediated in the first instance by language, but also by tools and artefacts. Most contemporary practice theorists acknowledge that nonhuman entities help constitute human sociality. Practices ‘are generally construed as materially mediated nexuses of activity’ ([Schatzki et al., 2001, p. 20](#sch01)).

Pickering uses the practices of experimental physicists to demonstrate that scientific discovery is made possible only by an entanglement of material and human agency (Pickering, [1993](#pic93), [1995](#pic95)). He argues for a return to ‘the thematics of the visible’ and suggests the history of agency can only be written with recourse to ‘the visible sites of encounter of human and non-human agency’ ([Pickering, 2001, p. 174](#pic01)).

Mediation makes practices historically situated and profoundly social phenomena - even those carried out in isolation. Artefacts, both material and symbolic, are a way of conveying the past into what we do and in doing so expand our practice ([Nicolini, 2012, p. 107](#nic12)).

#### Sociality

Above all else, practices are the realm of the social. It is the placement of the social in practices that distinguishes practice theory from other types of cultural theories which conceptually site the social in mental qualities, discourse or in interaction ([Reckwitz, 2002, p. 249](#rec02)). Nicolini exhorts us to refrain from using the phrase ‘social practice’ as it is, in his view, a tautology ([Nicolini, 2012, p. 94](#nic12)).

Practice theories seek to explicate, describe and interpret society by envisioning social life as an interrelated web of practice. Practices ‘are the medium in which lives interrelate’ and all forms of coexistence are established in practice ([Schatzki, 1996, pp. 14, 186](#sch96)). Indeed, the social is accessible only via understanding practice: ‘most of what people do is done as part of some practice(s) or other, and such social phenomena as institutions and power are to be understood via the structures of and relations among practices’ ([Schatzki, 1997, p. 284](#sch97)).

This is by no means an exhaustive list of all concepts found in all practice theories, nor is it a complete summation of any particular theorist’s position. However, these seven characteristics distil those elements central to all practice theories and illustrate the distinctive flavour of the practice approach. Without falling prey to the urge to build simplifying diagrammatic models of social life (c.f. [Schatzki, 2002, p. xii](#sch02)), these seven elements can nevertheless form a framework for research which might constructively be used to reveal and analyse human practices. It is argued herein that this schema could profitably be used in information research to overcome some of the limitations identified in models of individual behaviour.

### Applying the practice lens to information research

The efficacy of this sevenfold framework of practice in unearthing routine, customary and on-going information practices has yet to be tested in the field. Nevertheless, indications from the existing literature suggest that it could indeed be a fruitful method of investigation and analysis.

#### Action and activity

To treat the process of becoming informed - information - as a network of actions and activities is not problematic. Any list of mechanisms by which a person might come to ‘know’ demonstrates the activity-based nature of information: searching, using, sharing, browsing, monitoring, viewing, listening, watching, seeing – the list goes on. The first principle use of the word ‘information’ is to describe the action of informing, of telling, or of being told ([Buckland, 1991, p. 3](#buc91)).

For information as action, we need look no further than Dervin for whom information is a verb; instead of finding facts, we make them: ‘..factizing as a verb suggests that among the many ways in which people make their worlds is a proceduring, a designing called making facts’ ([Dervin, 1999, p. 6](#der99)).

#### Temporality

That information activities exist in time, as do all human activities, is of course self-evident. But time has long been seen as a significant factor in information research.

Time is one of the main contextual factors of information seeking and Savolainen identifies around 100 articles and books addressing temporal factors in information seeking ([Savolainen, 2006](#sav06)). One of the main dimensions by which collaborative information behaviours can be classified and analysed is the synchronicity or asynchronicity of the activities ([Talja & Hansen, 2006, p. 122](#tal06)).

In their research on documents as boundary objects in a professional theatre and a midwifery clinic, Davies and McKenzie demonstrate how information and information activities, must be managed across multiple timelines when any complex work is performed ([Davies & McKenzie, 2004](#dav04)). Lloyd demonstrates how the relationship with information over time turns the novice firefighter into the professional ([Lloyd, 2007](#llo07)); and how a vital ‘compliance knowledge’ is built over time to enable refugees navigate the rules and regulations of their adopted country ([Lloyd, Kennan, Thompson, & Qayyum, 2013](#llo13)).

#### Situatedness

The work of Elfreda Chatman pays testament to information research in situation and context. Chatman’s small world theories of information share, as Savolainen notes, many characteristics of the practice approach, particularly her theory of normative information behaviour. ‘Chatman provides a compelling case of the ways in which social and cultural factors such as norms determine daily information seeking, use, and sharing in the specific context of “small world.”’ ([Savolainen, 2008, pp. 6, 204-205](#sav08)). Chatman’s research into university janitors (Chatman, [1990](#cha90), [1991](#cha91)), female prisoners ([Chatman, 1999](#cha99); [Pendleton & Chatman, 1998](#oen98)) and women in a retirement home([Chatman, 1992](#cha92)) was ground-breaking not only because it applied theories from other disciplines to information research, and because she studied impoverished and marginalised populations, but because the physical situations of her subjects provided such a wealth of data about their information activities.

From a 1986 study of patients in four medical practices ([Dervin, Harlock, Atwood, & Garzona, 1986](#der86)) to recent studies of information literacy among refugees in regional Australia (Lloyd et al., 2013) and the small worlds of Rwandan student activists ([Yerbury, 2015](#yer15)) countless information research projects have been profitably conducted from a situational perspective. Since the success of the first Information Seeking in Context Conference in 1996, the significance of situatedness has surely become acknowledged and uncontested in information research.

#### Embodiment

‘Bodies play a central role….as a source of sensory and sentient information, as an instrument of non-verbal communication….bodies reflect the discourse in which the person is situated socially, politically and historically.’ ([Lloyd, 2010c, p. 18](#llo10c)). Lloyd’s study of information literacy in the world of firefighters provides a rich example of the necessity of bodily knowledge as an advancement of and adjunct to textual sources in their training:

In the context of firefighting, the body is used as a source of sensory information, which enables the development of fire sense. The body becomes the communicator of practice and a symbol of community and professionalism that reflects the discourse in which the body is situated socially, politically, and historically. ([Lloyd, 2007, p. 188](#llo07)).

‘Listening’ to one’s own body and observing the bodies of co-participants are integral for Lloyd’s emergency workers – ‘observation and demonstration are important activities for information work’ and this corporeality is ‘critical for the transformation from novice to expert’ ([Lloyd, 2010a](#llo10a)).

Veinot shows how the practice of electrical vault inspection is fully embodied: from climbing a ladder to inspect a transformer, through physically separating wires in an amp meter, to physically gauging the temperature of equipment, the inspector’s body becomes a vital tool for assessing transformers ([Veinot, 2007, p. 167](#vei07)).

#### Mediation

In 2000 Brown and Duguid proposed that documents and other information artefacts are active social entities that can bring diverse groups of people together in their pivotal roles as boundary objects (Brown & Duguid, 2000). This idea has been used to good effect by McKenzie in her analysis of document use in a midwifery clinic and Davies in her analysis of a theatre production. Here texts are seen as simultaneously constructing - and being constructed by - a social setting, and revealing the organisation of that setting ([Davies & McKenzie](#dav04), 2004; [McKenzie, 2006](#mck06)). Others have noted that work practice is organised, in every case, to some material, technological base ([Talja & Hansen, 2006, p. 126](#tal06)) and few would dispute that information technologies and artefacts play a major role in all non-work aspects of life as well.

Informational objects need no longer be confined to the technological, textual or documentary: objects like cars, light bulbs, compost heaps and recycling bins have become carriers of environmental information by virtue of the practices associated with them ([Haider, 2011, p. 836](#hai11)). The informational aspects of costumes and sets in the theatre is well demonstrated ([Olsson, 2010](#ols10)), and even the body of a pregnant woman is an integral information object in the midwifery clinic ([Davies & McKenzie, 2004](#dav04)).

A practice approach generally deproblematises the role information and knowledge artefacts play in information activities. ‘Information’ is neither the object itself nor the cognitive ‘change’ that interaction with the object occasions, but a complex interplay of both of these with other factors. Interpreting information objects as mediators can fully account for the construction of their meaning by the actor, while also acknowledging that this meaning is greatly influenced by an external shared experience.

#### Continuity and recurrence

In her work on everyday life information seeking McKenzie found that information practices form a continuum, as opposed to the two dimensional stop/start process implied by traditional formulations of information seeking ([McKenzie, 2003](#mck03)). Johannisson & Sundin posit that while information practices are always purposeful, they should not be understood ‘as isolated steps in easily discernible decision-making processes’ ([Johannisson & Sundin, 2007, p. 203](#joh07)).

Conceptualising information as continuous, recurrent, constitutive practices that are constantly re-shaping the actor’s movements and actions will help deliver it from the constraining ‘needs and motives’ paradigm so despaired of by Savolainen.

#### Sociality

If information is an activity it cannot be assumed to be a solitary one, carried out in perfect isolation. Information is not an entity with fixed boundaries or a commodity that is transferred through communication channels; it is, rather, a communicative construct which is produced in a social context ([TTuominen & Savolainen, 1996, p. 89](#tuo96)). We construct meaning based on what we have heard, seen and experienced in our social setting. If people with similar practices are more easily bound together to form social networks, so are people with different practices divided from them. This is not a difference in the information they can access – they may have much information in common – it is a difference in their attitude and disposition to that information as shaped by their social context (Brown & Duguid, 2000, p. 141).

For Lloyd, the social is one of four modalities of central to her concept of information practice: the experience of information can only occur in relation to other people and its dimensions can only be fully explored by examining the interactions of a setting’s members ([Lloyd, 2010c, p. 168](#llo10c); [2010d, pp. 41-42](#llo10d)).

Studies of scholarly communication conducted in the 1960s and 70s demonstrated that their social ties profoundly affected their information activities, and many more recent information studies demonstrate the decisive role social networks play ([Talja & Hansen, 2006, pp. 119-122](#tal06)). Surveying a number of information research projects, Williamson contends that only by examining the social do the complexities of the real world have some chance of emerging ([Williamson, 2006, p. 98](#will06)). A review of research on information sharing indicates that it must necessarily take account of the social, particularly in relation to social capital and social networks ([Savolainen, 2008, pp. 184-188](#sav08)).

Analysing the ways that information practices are embedded in social groups not only shows how information is sought, gathered and selectively shared, but also the ways in which it is withheld, hidden, forgotten or simply not noticed ([Dourish & Anderson, 2006](#dou06)). It encompasses a wider investigation of information activities than can be accommodated by looking at individual behaviours.

## Conclusion

Latterly certain researchers have identified two significant short-comings of empirical research which follows a behaviourist approach in information studies. Firstly, this research has tended to focus on a user’s purposive search for information in response to a perceived lack or problem without due regard for other, less-directed, more habitual and routinised activities. Secondly, a focus on the individual does not adequately take into account the social aspects of knowledge creation and knowledge sharing which are essentially communicative in nature – surely a person does not become informed in isolation. While there is nothing wrong with a research emphasis on the individual, if this is our only focus we may not uncover or examine all that goes into the human experience of becoming informed. We are, in short, in danger of being uninformed about information itself.

The proponents of information practice assert that only by adopting a practice approach will the essentially social nature of information be revealed – that practice is the lens through which it is possible to identify and explore the full gamut of actions, activities, behaviours, situations and context that result in an individual becoming informed. An examination of the work of a number of prominent practice theorists reveals that practice revolves around seven essential concepts:

*   action/activity
*   temporality
*   situatedness
*   continuity and recurrence
*   embodiment
*   mediation
*   sociality

It is argued that these characteristics can form a framework for research suitable for the empirical examination of a broad range of information activities, bringing to light those habitual and often hidden practices which are otherwise difficult to identify. This assertion is yet to be tested: the schema has not yet been used in its entirety in a single research project. Nevertheless, indications from existing literature are promising.

The framework of practice suggested herein will allow the investigator to overcome the research limitation of treating information as something that ‘happens’ to the individual. It will reveal the essentially non-linear, chaotic, iterative, two-way web of activities and processes that result in a person becoming informed. It accommodates all information activities, purposive and non-purposive, needs-based and serendipitous. And, perhaps most importantly of all, it situates the manifold activities and processes by which a person ‘comes to know’ firmly in the social.

## Acknowledgements

I would like to give my very great thanks the anonymous reviewer who made a number of pertinent and timely comments about this paper, including but not limited to the suggestion that the structure of practice I advance should more properly be viewed as a framework than a model. This saved me from a major methodological, and philosophic, inconsistency. I have also appropriated your phrase ‘dynamics of practice’.

## <a id="author"></a>About the author

**Sally Irvine-Smith** is a Doctoral student in information studies at the University of Technology Sydney, 15 Broadway, Ultimo NSW 2007, Australia. She is interested in the real-life processes and activities by which people generate and share knowledge. She uses practice theory as a means by which these processes and activities can be analysed and interpreted. Her Doctoral project examines the information practices of decision makers in the local sphere. She can be contacted at sally.b.irvine-smith@student.uts.edu.au.

</section>

<section>

## References

<ul>
<li id="add13">Addison, C. &amp; Meyers, E. (2013). Perspectives on information literacy: a framework for conceptual understanding Information Research, 18(3) paper C27. Retrieved from http://InformationR.net/ir/18-3/colis/paperC27.html (Archived by WebCite® at http://www.webcitation.org/6lDCZoKZ4)</li>
<li id="bou77">Bourdieu, P. (1977). Outline of a Theory of Practice (R. Nice, Trans.). Cambridge: Cambridge University Press.</li>
<li id="bou90">Bourdieu, P. (1990). The Logic of Practice (R. Nice, Trans.). Cambridge: Polity Press.</li>
<li id="buc91">Buckland, M. (1991). Information and Information Systems. Westport, CT: Greenwood Press.</li>
<li id="cha90">Chatman, E. A. (1990). Alienation theory: application of a conceptual framework to a study of information among janitors. Reference Quarterly (RQ), 29(3), 355-369. </li>
<li id="cha91">Chatman, E. A. (1991). Life in a Small World: applicability of Gratification Theory to Information-Seeking Behavior. Journal of American Society for Information Science, 42(6), 438-449. </li>
<li id="cha92">Chatman, E. A. (1992). The Information World of Retired Women. Westport, CT: Greenwood Press.</li>
<li id="cha99">Chatman, E. A. (1999). A theory of life in the round. Journal of the American Society for Information Science, 50(3), 207-217. </li>
<li id="cor10">Corradi, G., Gherardi, S., &amp; Verzelloni, L. (2010). Through the practice lens: where is the bandwagon of practice-based studies heading? Management Learning, 41(3), 265-283. </li>
<li id="cro08">Crossley, N. (2008). Social Class. In M. Grenfell (Ed.), Pierre Bourdieu: Key Concepts (pp. 87-99). Durham, UK: Acumen Publishing Ltd.</li>
<li id="dav04">Davies, E., &amp; McKenzie, P. J. (2004). Preparing for opening night: temporal boundary objects in textually-mediated professional practice. Information Research, 10(1). Retrieved from http://informationr.net/ir/10-1/paper211.html. (Archived by WebCite® at http://webcitation.org/5vMu2P3Ik)</li>
<li id="der99">Dervin, B. (1999). Chaos, order and sense-making: a proposed theory for information design. In R. Jacobson (Ed.), Information Design (pp. 35-57). Cambridge, MA: MIT Press.</li>
<li id="der86">Dervin, B., Harlock, S., Atwood, R., &amp; Garzona, C. (1986). The human side of information: an exploration in a health communication context. Communication Yearbook, 4, 591-608. </li>
<li id="dou06">Dourish, P., &amp;Anderson, K. (2006). Collective information practice: exploring privacy and security as social and cultural phenomena. Human-Computer Interaction, 21, 319-342. </li>
<li id="ghe09">Gherardi, S. (2009). Introduction: The Critical Power of the 'Practice Lens'. Management Learning, 40(2), 115-128. </li>
<li id="gid77">Giddens, A. (1977). Studies in Social and Political Theory. London: Hutchinson &amp; Co.</li>
<li id="gid84">Giddens, A. (1984). The Constitution of Society: Outline of the Theory of Structuration. Cambridge: Polity Press.</li>
<li id="hai11">Haider, J. (2011). The environment on holidays or how a recycling bin informs us on the environment. Journal of Documentation, 67(5), 823-839. </li>
<li id="hui11">Huizing, A., &amp; Cavanagh, M. (2011). Planting contemporary practice theory in the garden of information science. Information Research, 16(4) paper 497. Retrieved from http://InformationR.net/ir/16-4/paper497.html. (Archived by WebCite® at http://www.webcitation.org/6BGRzQGMN).</li>
<li id="joh07">Johannisson, J., &amp;Sundin, O. (2007). Putting discourse to work: Information practices and the professional project of nurses. The Library Quarterly, 77(2), 199-218. </li>
<li id="lav11">Lave, J. (2011). Apprenticeship in Critical Ethnographic Practice. Chicago, IL: University of Chicago Press.</li>
<li id="lav91">Lave, J., &amp; Wenger, E. (1991). Situated Learning: Legitimate Peripheral Participation. Cambridge: Cambridge University Press.</li>
<li id="llo07">Lloyd, A. (2007). Learning to put out the red stuff: becoming information literate through discursive practice. The Library Quarterly, 77(2), 181-198. </li>
<li id="llo10a">Lloyd, A. (2010a). Corporeality and practice theory: exploring emerging research agendas for information literacy. Information Research, 15(3). Retrieved from http://www.informationr.net/ir/15-3/colis7/colis704.html (Archived by WebCite® at http://www.webcitation.org/6jEGH48aE)</li>
<li id="llo10b">Lloyd, A. (2010b). Framing information literacy as information practice: site ontology and practice theory. Journal of Documentation, 66(2), 245-258. </li>
<li id="llo10c">Lloyd, A. (2010c). Information literacy landscapes: information literacy in education, workplace and everyday contexts. Oxford: Chandos Publishing.</li>
<li id="llo10d">Lloyd, A. (2010d). Lessons from the workplace: understanding information literacy. In A. Lloyd &amp; S. Talja (Eds.), Practising information literacy: bringing theories of learning, practice and information literacy together (pp. 29-49). Wagga Wagga, NSW: Centre for Information Studies.</li>
<li id="llo13">Lloyd, A., Kennan, M. A., Thompson, K. M., &amp; Qayyum, A. (2013). Connecting with new information landscapes: information literacy practices of refugees. Journal of Documentation, 69(1), 121-144. </li>
<li id="mck03">McKenzie, P. J. (2003). A model of information practices in accounts of every-day life information seeking. Journal of Documentation, 59(1), 19-40. </li>
<li id="mck06">McKenzie, P. J. (2006). Mapping Textually Mediated Information Practice in Clinical Midwifery Care. In A. Spink &amp; C. Cole (Eds.), New Directions in Human Information Behaviour (pp. 73-92). Dordrecht, NED: Springer.</li>
<li id="nic09">Nicolini, D. (2009). Zooming in and out: Studying practices by switching theoretical lenses and trailing connections. Organization Studies, 30(12), 1391-1418. </li>
<li id="nic12">Nicolini, D. (2012). Practice Theory, Work, and Organization: An Introduction. Oxford: Oxford University Press.</li>
<li id="ols10">Olsson, M. (2010). All the World’s a Stage – the Information Practices and Sense-Making of Theatre Professionals. Libri, 60, 241-252. </li>
<li id="oen98">Pendleton, V. E. M., &amp; Chatman, E. A. (1998). Small world lives: Implications for the public library. Library Trends, 46(4). </li>
<li id="pic93">Pickering, A. (1993). The mangle of practice: Agency and emergence in the sociology of science. American Journal of Sociology, 99(3), 559-589. </li>
<li id="pic95">Pickering, A. (1995). The Mangle of Practice: Time, Agency, and Science. Chicago, IL: University of Chicago Press.</li>
<li id="pic01">Pickering, A. (2001). Practice and posthumanism: social theory and a history of agency. In T. R. Schatzki, K. Knorr Cetina, &amp; E. Von Savigny (Eds.), The Return of Practice in Contemporary Theory (<a href="#knorr01">pp. 172-183</a>). London: Routledge.</li>
<li id="rec02">Reckwitz, A. (2002). Toward a theory of social practices: A development in culturalist theorizing. European Journal of Social Theory, 5(2), 243-263. </li>
<li id="rou07">Rouse, J. (2007). Practice Theory. In S. P. Turner &amp; M. W. Risjord (Eds.), Philosophy of Anthropology and Sociology (pp. 639-681). Amsterdam, NL: Elsevier.</li>
<li id="ryl49">Ryle, G. (1949). The Concept of Mind. Harmondsworth, UK: Penguin Books.</li>
<li id="sav06">Savolainen, R. (2006). Time as a context of information seeking. Library &amp; Information Science Research, 28, 110-127. </li>
<li id="sav07">Savolainen, R. (2007). Information behaviour and information practice: Reviewing the “Umbrella Concepts” of information-seeking studies. Library Quarterly, 77(2), 109-132. </li>
<li id="sav08">Savolainen, R. (2008). Everyday Information Practices: a Social Phenomenological Perspective. Lanham, MD: The Scarecrow Press.</li>
<li id="sch96">Schatzki, T. R. (1996). Social Practices: A Wittgensteinian Approach to Human Activity and the Social. Cambridge: Cambridge University Press.</li>
<li id="sch97">Schatzki, T. R. (1997). Practices and Actions: A Wittgensteinian Critique of Bourdieu and Giddens. Philosophy of the Social Sciences, 27(3), 283-308. </li>
<li id="sch01">Schatzki, T. R. (2001). Practice mind-ed orders. In T. R. Schatzki, K. Knorr Cetina, &amp; E. Von Savigny (Eds.), The Practice Turn in Contemporary Theory (pp. 50-63). London: Routledge.</li>
<li id="sch02">Schatzki, T. R. (2002). The Site of the Social: a philosophical account of the constitution of social life and change. University Park, PA: The Pennsylvania State University Press.</li>
<li id="knorr01">Schatzki, T. R., Knorr Cetina, K., &amp; von Savigny, E. (Eds.). (2001). The Practice Turn in Contemporary Theory. London: Routledge.</li>
<li id="tal96">Talja, S. (1996). Constituting "Information" and "User" as research objects. In P. Vakkari, R. Savolainen, &amp; B. Dervin (Eds.), Information Seeking in Context: Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Context 14-16 August, 1996, Tampere, Finland (pp. 67-80). London: Taylor Graham.</li>
<li id="tal05">Talja, S. (2005). The domain analytic approach to scholars' information practices. In K. E. Fisher, S. Erdelez, &amp; L. McKechnie (Eds.), Theories of Information Behaviour (pp. 123-127). Medford, NJ: Information Today Inc.</li>
<li id="tal10">Talja, S. (2010). Jean Lave's Practice Theory. In G. J. Leckie, L. M. Given, &amp; J. E. Buschman (Eds.), Critical Theory for Library and Information Science (pp. 205-220). Santa Barbara, CA: Libraries Unlimited.</li>
<li id="tal06">Talja, S., &amp; Hansen, P. (2006). Information Sharing. In A. Spink &amp; C. Cole (Eds.), New Directions in Human Information Behaviour (pp. 113-134). Dordrecht, NL: Springer.</li>
<li id="tal07">Talja, S., &amp; McKenzie, P. J. (2007). Editors' introduction: Special issue on discursive approaches to information seeking in context. The Library Quarterly, 77(2), 97-108. </li>
<li id="tal15">Talja, S., &amp; Nyce, J. M. (2015). The problem with problematic situations: Differences between practices, tasks, and situations as units of analysis. Library &amp; Information Science Research, 37(1), 61-67. </li>
<li id="talTS05">Talja, S., Tuominen, K., &amp; Savolainen, R. (2005). “Isms” in information science: constructivism, collectivism and constructionism. Journal of Documentation, 61(1), 79-101. </li>
<li id="tuo96">Tuominen, K., &amp; Savolainen, R. (1996). A social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen, &amp; B. Dervin (Eds.), Information Seeking in Context: Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts, 14-16 August 1996, Tampere, Finland (pp. 81-96). London: Taylor Graham.</li>
<li id="tuo05">Tuominen, K., Talja, S., &amp; Savolainen, R. (2005). The social constructionist viewpoint on information practices. In K. E. Fisher, S. Erdelez, &amp; L. McKechnie (Eds.), Theories of Information Behaviour (pp. 328-337). Medford, NJ: Information Today Inc.</li>
<li id="vak08">Vakkari, P. (2008). Trends and approaches in information behaviour research. Information Research, 13(4) paper 361. Retrieved from http://InformationR.net/ir/13-4/paper361.html (Archived by WebCite® at http://www.webcitation.org/6jEEW2QG2)</li>
<li id="vei07">Veinot, T. C. (2007). "The eyes of the power company": Workplace information practices of a vault inspector. The Library Quarterly, 77(2), 157-179. </li>
<li id="will06">Williamson, K. (2006). Research in constructivist frameworks using ethnographic techniques. Library Trends, 55(1), 83-101. </li>
<li id="wil81">Wilson, T.D. (1981). On user studies and information needs. Journal of Documentation, 37(1), 3-15. </li>
<li id="wil00">Wilson, T.D. (2000). Human information behaviour. Informing Science, 3(2), 49-55. </li>
<li id="yer15">Yerbury, H. (2015). Information practices of young activists in Rwanda. Information Research, 20(1). Retrieved from http://informationr.net/ir/20-1/paper656.html (Archived by WebCite® at http://www.webcitation.org/6jCuCXKil)</li>
</ul>

</section>

</article>