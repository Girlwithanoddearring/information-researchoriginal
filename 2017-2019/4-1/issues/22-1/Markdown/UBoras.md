<header>

#### vol. 22 no. 1, March, 2017

* * *

</header>

<article>

<section>

# The Swedish School of Library and Information Science, University of Borås. An introduction to the new publisher of Information Research

The Swedish School of Library and Information Science is the new publisher of _Information Research_, and this brief document is intended to serve as an introduction to the School for readers of the journal. It was originally prepared for a different purpose and has been slightly edited for publication here.

The Swedish School of Library and Information Science (SSLIS) at the University of Borås is one of the leading institutes of its type in terms of size and contribution to the field of library and information science, both nationally and internationally. Although evaluation and ranking measures are not common in the field, the government agency for higher education in Sweden evaluated library and information science and neighbouring fields in 2004 and ranked the School as No. 1 in Sweden. It is a member of the iSchools group.

</section>

<section>

## GENERAL

*   SSLIS currently has 9 professors, 20 senior lecturers/ researchers, 30 lecturers, 18 PhD students, and other staff active at the school.
*   Research is primarily focused on:
    *   [Libraries, Culture and Society](http://www.hb.se/en/Research/Research-Groups/Libraries-Culture-and-Society/)
    *   [Digital Resources and Services: Culture and Information](Digital Resources and Services: Culture and Information)
    *   [Information Practices](http://www.hb.se/en/Research/Research-Groups/Information-Practices/)
    *   [Social Media Studies](http://www.hb.se/en/Research/Research-Groups/SMS---Social-Media-Studies/)
    *   [Strategic Research Programme in Data Science](http://www.hb.se/en/Research/Research-Groups/Strategic-Research-Program-in-Data-Science/)
*   SSLIS hosts The [Centre for Cultural Policy Research](http://www.hb.se/en/Research/Centres1/The-Centre-for-Cultural-Policy-Research-KPC/) (KPC)
*   SSLIS is now the publisher of the international, open access, peer-reviewed scholarly journal _Information Research_
*   SSLIS founded and hosts the journal [HUMAN IT](https://humanit.hb.se/index), which has been running since 1997\. It invites contributions from the humanities, the social, behavioural, and natural sciences, as well as technology. As it allows publications both in English and all the Nordic languages it has been an important journal for the development of the field-related concepts in the Nordic languages.
*   SSLIS hosts Valfrid Publishing which has published more than 60 monographs, of which 39 are PhD theses produced within SSLIS.
*   SSLIS offers multiple educational programmes at Bachelor’s, Master’s, and PhD levels and enjoys a strong and nurturing knowledge environment (including both education and research) with regular research seminars and participation in many research projects, striving for excellence in competitive topics.
*   Doctoral education, with the largest impact on the field in Sweden (and beyond), has close connections with both undergraduate education and research groups of the department.

</section>

<section>

## COLLABORATIONS

Generally, research at SSLIS is conducted in close collaboration with the business community (e.g., partnership with AstraZeneca, Recorded Future, Findwise, Axiell) culture and public sector (e.g., the National Library of Sweden; the Swedish National Agency for Education, Swedish Agency for Cultural Policy Analysis, Swedish Agency for Accessible Media, Swedish National Data Services, Swedish Arts Council, and more), professionals, and researchers within the field, locally, nationally and internationally. The results and impacts of these collaborations are too many to count.

SSLIS has had collaborations with organisations such as Nordic-Baltic Research Institute ([NordForsk](https://www.nordforsk.org/en/)), Nordic Research School in Information Studies ([NORSLIS](http://www2.abm.uu.se/norslis/)), Graduate School of Language Technology at the University of Gothenburg ([GSLT](http://www.gslt.hum.gu.se/sv/?languageId=100000&disableRedirect=true&returnUrl=http%3A%2F%2Fwww.gslt.hum.gu.se%2F%3FlanguageId%3D100000)) and is a member of the Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society ([LinCS](http://lincs.gu.se/?languageId=100001&disableRedirect=true&returnUrl=http%3A%2F%2Flincs.gu.se%2Fsvenska%2F)). Not to mention the collaboration with colleagues within [the iSchools group](http://ischools.org).

</section>

<section>

## RESEARCH PROJECTS

SSLIS faculty, research staff and Ph.D. students regularly participate in national, international and EU projects. Some of these involve temporary placement at partner organisations, or creation or participation in courses and more.

As examples of projects funded by EU one can mention:

[PERICLES](http://pericles-project.eu/main) (2013-2017) – Within this large EU-funded project (with budget of over €10 million) we have been collaborating with ten other academic and non-academic partners: 1- Kings College London; 2- Information Technologies Institute – CERTH (Greece); 3- DotSoft (a software development company – Greece); 4- Georg-August Universität, Göttingen (Germany); 5- The University of Liverpool; 6- Space Applications Services NV (Belgium); 7- Xerox SAS (France); 8- The University of Edingburgh; 9- Tate; 10- Belgian User Support and Operations Cernter - B.USOC.

This collaboration has resulted in a very large collection of [scholarly publications](http://pericles-project.eu/publications), and [deliverables](http://pericles-project.eu/deliverables) as well as many seminars, workshops, a three-day fully-booked conference, introduction of research results in multiple master’s courses (e.g., at Edinburgh and SSLIS), and creation of a PhD course ([Dynamics of Knowledge Organization](http://www.hb.se/Global/HB%20-%20student/utbildningsområden/Forskarutbildning/F%20blanketter/Kursplan_FBIDKO1_Dynamics%20of%20Knowledge%20Organization.pdf) – course plan). We have also created an [online modular training package](http://pericles-project.eu/training-module/) with (so far) 10 different modules (in various stages of completion) and are currently producing a MOOC, which will become available via [FutureLearn](https://www.futurelearn.com).

[SHAMAN](https://ercim-news.ercim.eu/en80/special/shaman) – Another EU funded project and a precursor to PERICLES.

[DiXiT](http://dixit.uni-koeln.de) (2014-2017) – Another Example of an EU funded projects is a Marie Sklodowska-Curie project called Digital Scholarly Editions Initial Training Network (with a budget of over €382,000). In this project 10 Full Partners and 18 Associated Partners are involved, supporting 12 Early Stage Researchers and 3 Experienced Researchers.

Of nationally funded projects, one can name:

[LÄST](http://www.hb.se/Forskning/Projekt/Lasning-tradition-och-forhandling-LAST-Lasaktiviteter-i-svenska-klassrum-1967-1969/) – Läsning, tradition och förhandling (funded by the Swedish Research Council, with a budget of around SEK 4.5 million)

[E-books](http://www.hb.se/en/Research/Projects/The-case-of-the-e-book-in-a-small-language-culture-Media-technology-and-effects-in-the-digital-society/) (2013-2017) – The case of the e-book in a _small language_ culture: media, technology and effects in the digital society (funded by the Swedish Research Council, with a budget of around SEK 12 million)

[PET](http://www.his.se/en/Research/informatics/Skovde-Artificial-Intelligence-Lab/Utvalda-projekt/Picking-the-Winners-Forecasting-Emergent-Technology/) (2016-2018) – in another project co-funded by the Knowledge Foundation and industrial partners, we collaborate with the University of Skövde, the pharmaceutical company AstraZeneca, and a big-data company called Recorded Future. In this project, for example, parts of the project funds have been used to employ a full time post-doc as a member of the project team.

As examples of collaborative initiatives on regional level one could mention multiple projects (e.g., [INCITE](http://www.hb.se/Forskning/Projekt/INCITE---Informatin-Fusion-as-an-E-SErvice-in-Scholarly-Information-Use/), [SAREK](http://www.hb.se/Forskning/Projekt/Ambulanstraningscenter/#presentation), TACIT [as part of which a PhD studentship has been created] and more) funded by the local region, Region Västra Götaland in two stages of 2013-2015 and 2016-2018 ([TIKT-1](http://www.hb.se/Forskning/Aktuellt/Forskningssamarbete-Boras---Skovde/Tema-Informations--och-kommunikationsteknik-TIKT/) and [TIKT-2](http://www.hb.se/Forskning/Projekt/TIKT-2/)) with a combined budget of SEK 34 million.

As examples of broader international collaborations where meaningful educational and development support is being offered by SSLIS, one could name projects funded by the [Swedish International Development Cooperation Agency](http://www.sida.se/English/) (SIDA), e.g.,, in a couple of projects in Rwanda and Uganda (with a budget of over SEK 13 million).

</section>

<section>

## COMMISSIONED COLLABORATIONS

Perhaps the best measures of collaboration outcomes are not the amount of money or the size of the project: the competencies of colleagues at SSLIS are much appreciated by the field as the colleagues are commonly invited to take part in various smaller initiatives, offering support to the professionals and interacting and impacting the field. Below are a few recent examples of such collaborations.

Commissioned by The Royal Library (the National Library of Sweden)  
Assignment National Library Strategy – The importance of organizational placement in various library type’s parent organization.  
Implementation Spring of 2016.  
Cost 1.5 month's work = SEK 230 000.

Commissioned by The Royal Library  
Assignment 1 National Library Strategy – From an international perspective to summarize the work that in the past five years has been raised in studies and articles about school libraries role in student learning. Knowledge and research review.  
Producing a knowledge and research overview of school libraries' role in student learning. The overview should include national and international texts from the past five years.  
Implementation 2016  
Cost corresponding two months of work = SEK 320 000.

Assignment 2 To write a skills profile for school librarians to complement teacher training so that the various competencies complement each other.  
To perform the task requires a number of interviews with the teacher and the review of a large amount of training plans and curricula. The focus is on education, not on professional practice, which is an advantage. Thus, one can eventually have the opportunity to influence the design of teacher education; or rather contribute to teachers' increased knowledge about the school library's role in information literacy, language development, and reading promotion activities. The mission may therefore be about hammering out a skills profile for school librarians through interviews and document studies.  
Implementation Fall 2016  
Cost corresponding two months of work = 320 000.

Commissioned by Region Gävle, Västra Götaland, Kronoberg/Blekinge, Stockholm  
Assignment Digital services in municipal libraries: local, regional and national perspective.  
The report’s aim is to discuss challenges and opportunities for e-resources at the municipal library on three levels - local, regional and national.  
Implementation Spring 2016  
Cost 100 000 SEK

Commissioned by The City of Malmö  
Assignment Extended mission for follow-up research regarding the project “The Little Castle” at Malmö City Library. The extended mission involves, in brief, more hours but no change in the content.  
Implementation Completed 2016-12-31  
Cost about SEK 100 000 (for both researchers)

Commissioned by Borås municipality, Adult Education and Almås (upper secondary) School.  
Assignment Lectures on information literacy in adults.  
Implementation April and May 2016  
Cost SEK 10 000

Commissioned by Swedish Agency for Cultural Policy Analysis  
Assignment A study of the development of the concept diversity in the Swedish cultural policy on behalf of the agency.  
Implementation April 15 to August 8 2016  
Cost SEK 100 000 excl. VAT.

Commissioned by Swedish National Agency for Education  
Assignment The researchers at SSLIS will develop research articles with accompanying discussion questions, for a module for primary and secondary school, in the digital school improvement programme. Two of these research papers will be developed by the SSLIS participants alone and one article will be developed by them in collaboration with Professor Olof Sundin, at Lund University.  
Implementation April 1, 2016 - October 15, 2016  
Cost 205 000 SEK

Commissioned by Kultur i Väst  
Assignment To deliver a speech at a conference in Gothenburg on the changed view of reading with a historical perspective and about the changing reading and reading habits.  
Implementation On September 6, 2016  
Cost 8000 excl. VAT

Commissioned by Praktiska gymnasiet  
Assignment Lecture and workshop on school libraries.  
Implementation October 5, 2016  
Cost SEK 8000

Commissioned by The County Library Kalmar, Blekinge / Kronoberg, Jönköping, Halland and Skåne  
Assignment Lecture on the theme analyze and discuss regional role of libraries  
Implementation October 10, 2016  
Cost SEK 16000

Commissioned by The County Library Uppsala  
Assignment Lecture and workshop Regional library operation and formalized reflection.  
Implementation November 7, 2016  
Cost SEK 8000

Commissioned by County Libraries Uppsala and Dalarna  
Assignment The project is about public libraries’ work with newcomers and refugees.  
Implementation To be completed at the latest March 31, 2017.  
Cost SEK 400 000

Commissioned by Swedish Agency for Accessible Media  
Assignment Overview of users of accessible media  
Implementation April 30, 2017  
Cost SEK 110 000

There are also other upcoming assignments which are commissioned by: Regional Library Uppsala, Swedish National Data Services, Swedish Arts Council, the Region Blekinge/Kronoberg, Svea Bredal, and more.

</section>

<section>

## CONFERENCES

SSLIS has initiated and organizes a number of recurring conferences.

For example Mötesplats inför framtiden is an annual conference in which researchers and professionals in the field of library and information science come together (since 2000). Mötesplats Sociala Medier is another conference in which dialogue has become possible between social media researchers and politicians. Both national and international politicians and researchers have been invited speakers and guest. Another event has been SMS-S (Social Media Studies - Symposium) offering researchers in the field a forum for presenting their recent research with the possibility of publication of peer-reviewed papers emerging from the symposium in a special supplement of the _Information Research_ journal.

* * *

Dr. Nasrine Olson  
University of Borås  
January 2017

</section>

</article>