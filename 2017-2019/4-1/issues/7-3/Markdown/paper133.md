#### Information Research, Vol. 7 No. 3, April 2002,

# The development of the information management research area

#### [Elena Macevičiūtė](mailto:elena.maceviciute@hb.se) & [T.D. Wilson](mailto:t.d.wilson@shef.ac.uk) (Visiting Professor)  
Högskolan i Borås  
Borås, Sweden

#### **Abstract**

> In the 1980s information management was emergent and perceived by some to be simply a re-write of traditional librarianship. However, it has continued to thrive and much of what is now included is far removed even from modern information science, although information management draws upon ideas from both librarianship and information science. In one form or another it is likely to persist in the future, since information problems are likely to persist in organisations. The means for resolving the problems may change, but the need to understand those problems and develop solutions will remain.

## Introduction

The ideas of the information society and of information as the key resource for the development of new industries emerged in the 1970s and 80s. In the late 1970s, the US Paperwork Reduction Act ([Commission on Federal Paperwork, 1977](#com77)) led to a recognition that the production of information by bidders for government contracts was an unbearable cost, and the cost of handling that information at the Federal government level was also onerous.

Out of this emerged the idea that _information is a resource_. That is, it has potential value for an organisation and, as a result, what information resources exist should be known to the organisation (information mapping became a buzz-phrase of the time), that the costs of acquiring, storing, manipulating and using information needed to be known, and that organisational budgets should recognise these costs.

The consequences of the emergence of Information Management for research were significant: the economics of information, which, until then, had been a relatively insignificant part of economics, attained a new importance. Previously, economics had factored information out of its equations - in the market, everyone was assumed to have perfect information about that market. That is manifestly untrue, and a new field - asymmetric information - was introduced into economics.

Secondly, the information content of information systems became important: previously, data bases were just that - they consisted largely of the numbers management needs to control an organisation, such as production figures in a manufacturing organisation. The recognition that _most_ information produced in an organisation is _text_ rather than numbers led to work on building systems that could handle text, and a greater interest in the information systems community of the role of information retrieval.

Thirdly, it was recognised that effective systems need to be built according to the needs of the information user, rather than the convenience of the information producer, if the information is to be presented in the most appropriate way and organised in the most accessible fashion.

Finally information policy and information strategy became key terms and the need to have such policies and strategies, at national, local and organisational level was recognised.

Four decades have passed since the emergence of information management as a research field. During this period it has been developed by scholars and practitioners all over the world. In addition, a tremendous change in information handling and communication technology has occurred. Empirical research in the areas of economics, management, organisational theory, information systems, library and information science served as a basis for further theoretical development in these fields. All this had a significant influence on information management work and research. The last decade was especially interesting as many changes that have been accumulated throughout years have manifested themselves explicitly and were rooted in the academic environment.

The aim of our article is to reveal the change of the information management research area during this past decade (from 1989 to 2000).

The article consists of the background overview of the discussions about information management and information management research contents, description of the method of research article analysis, presentation and discussion of the results, and conclusion.

## Background

The content and scope of information management has been under close scrutiny by researchers and practitioners from several fields (business and management, organisation research, information systems, information and communication technology, public administration, communication, information and librarianship) for a long time. Lately, authors trying to draw a line between information management and knowledge management renewed the discussions ([Rowley, 1998](#row98); [Kirk, 1999](#kir99); [Davenport & Prusak,](#dav00) 2000). The change of the profession under the impact of new technology, globalisation of markets, and increasing social and economic pressures is evident in the writings of library and information science (LIS) professionals, but it is expressed practically in the same words by the representatives of business and computer fields. The LIS representatives advocate stronger orientation towards the perspective of management in new flexible organisations and use of technology in them ([Dressang & Robbins, 1999](#dre99)). In the business field information management is seen as a higher management level function, especially when it is labelled as knowledge management. [Mintzberg](#min80) (1980) has described information roles of managers and sees management as an information intensive job. There is a growing understanding of information management significance for all kinds of senior executives in a vast range of business and management related literature. Information management programmes are found in business and management schools as well as in schools and departments of librarianship and information science.. Moreover, computer professionals, information systems designers, and (IT) specialists for businesses are now more concerned about the "necessity to study how managers utilise... information" ([Bruns _et al._, 1993](#bru93)), "thinking about information" content, "organising and utilising data" ([Lundberg, 1996](#lun96)), and adapting to the knowledge world by incorporating new responsibilities for strategic thinking" ([Korn/Ferry Int., 2000](#kor00)).

There are numerous attempts to define the framework for information management. The concepts largely depend on the contents put into the words "Information Management". It is not only the concepts of "information" as such, but the multiple meanings of the phrase, emphasis of its elements, or the word order as well as the scientific perspective. The phrase is also used to mean something other than what the LIS field considers to be the management of information resources. For example, it is used as an abbreviation for: the management of IT, information systems management, management information systems, etc. The meaning of the phrase is even more clouded by the emergence of new, related terms, such as "knowledge management", which in many cases has an identical meaning to information management ([Rowley, 1998](#row98)) or sometimes means just library and information studies in general (see, [Martin, 1999](#mar99)). Many authors have noticed this. Reviewing the newest literature we still can find more than enough concepts of information management, but in fact some coherence can be traced through the variety of sources and authors' opinions, especially those who work outside IT and computer sciences and have LIS or managerial backgrounds.

[Rowley](#row98) (1998) proposes four different levels of information management: information retrieval, information systems, information contexts, and information environments. Effective information management needs to address issues at all of these levels. [Choo](#cho98) (1998) defines "information management as a cycle of processes that support the organisation's learning activities: identifying information needs, acquiring information, organising and storing information, developing information products and services, distributing information, and using information". [Kirk](#kir99) (1999) summarises the main information management roles based on Braman's concepts of information developed in the area of information policy studies.

The summary of the concepts of Braman is as follows:

*   Information as a resource. "Information, its creators, processors and users are viewed as discrete and isolated entities. Information comes in pieces unrelated to bodies of knowledge or information flows into which it might be organised" ([Braman, 1989: 236](#bra89)).
*   Information as a commodity. Complementary to definitions of information as a commodity is the concept of an information production chain through which information gains in economic value. The notion of information as a commodity incorporates "the exchange of information among people and related activities as well as its use" ([Braman, 1989: 238](#bra89)) and implies buyers, sellers and a market. In contrast to the absence of power of information as a resource, information as a commodity has economic power.
*   Information as perception of pattern. Here the concept of information is broadened by the addition of context. Information "has a past and a future, is affected by motive and other environmental and casual factors, and itself has effects" ([Braman, 1989: 238](#bra89)). The concept of information and its processes is broadened so much that information in this sense can be applied to a highly articulated social structure. Information has a power of its own although its effects are isolated. The example given is of information reducing uncertainty but only in regard to a single specific question.
*   Information as a constitutive force in society. Information has a role in shaping context. "Information is not just affected by its environment, but is itself an actor affecting other elements in the environment" ([Braman, 1989: 239](#bra89)). The definitions in this category "apply to the entire range of phenomena and processes in which information is involved, can be applied to a social structure of any degree of articulation and complexity, and grant information, its flow and use an enormous power in constructing our social (and ultimately physical) reality". ([Kirk, 1999](#kir99)).

From this, Kirk derives a hierarchy of the definitions of information management as: IT systems, information resource management, information management as aligning information strategy and business strategy and integrating strategy formation and information ([Kirk, 1999](#kir99)).

There are only a few authors who have discussed information management in the field of research and education. [Andertone](#and86) (1986), [Lytle](#lyt88) (1988), and [Wilson](#wil97) (1997) present first reflections on the subject and note the confusion over the educational base of information management as well as the distinct differences from the traditional LIS and information science education. [Fairer-Wessels](#fai97) (1997) has done an extended review of literature on the educational issues and comes to the conclusion that the present paradigm of information management "over-specialisation" has resulted in a fragmented approach with various focuses on one or another component: information processing, management information systems, or managing IT. She argues for a more holistic paradigm in information management studies to reflect the inter-disciplinarity of the field.

Looking into the field of information systems research one will see many more attempts of self-reflection starting with [Kreiber and van Horn](#kre71) in 1971 and finishing with [Claver _et al_](#cla00). in 2000\. The authors of the latter study analysed the contents of two journals _Information & Management_ and _Management Information Systems Quarterly_ between 1981 and 1977\.

The definition of _information management_ chosen for this study is that proposed by [Wilson](#wil97) (1997):

> The application of management principles to the acquisition, organization, control, dissemination and use of information relevant to the effective operation of organizations of all kinds. 'Information' here refers to all types of information of value, whether having their origin inside or outside the organization, including data resources, such as production data; records and files related, for example, to the personnel function; market research data; and competitive intelligence from a wide range of sources. Information management deals with the value, quality, ownership, use and security of information in the context of organizational performance.

## Data collection

In 1989 Wilson made an overview of three main journals publishing articles on information management. At that time the core journals in the field were:

*   _International Journal of Information management_
*   _Management Information Systems Quarterly_
*   _Information Management Review._

This work was the origin of the idea for a study that would reveal what changes have occurred in the field during the last decade. The easiest way to establish the changes would be to compare the categories that emerged in 1989 with the categories that emerge in a new study. Consequently, a survey of the contents of the core journals was repeated in 2000\. However the group of journals has changed considerably. First, _Information Management Review_ ceased publication. Therefore, other publications were included in the core group. The main feature for selection was the profile of research articles. It had to match more or less the profile of the three journals of 1989, i.e., most of the articles should deal with information activity in organisations. Therefore, journals such as _Information Management and Processing_ were not included as the bulk of its articles are on information retrieval issues and only 5% deal with other information-related topics.

Six journals published in 2000 were selected:

*   _Information Economics and Policy_ (IEP) (vol. 12, issues 1-4)
*   _Information & Management_ (IM) (vol. 37, issues 1-6, vol. 38, issues 1-4)
*   _Information and Organisation_ (IO)(before 2001 _Accounting, Management and Information Technologies_) (vol. 10, issues 1-4)
*   _International Journal of Information Management_ (IJIM) (vol. 20, issues 1-6)
*   _Journal of Strategic Information Systems_ (JSIS) (vol. 9, issues 1-4)
*   _MIS Quarterly_ (MQ) (vol. 24, issues 1-4).

The journal _Information & Management_ displays an irregular pattern of publication, a volume can consist of 4 or 8 issues, cover a part of the year or spread over two years. A full volume of _Information & Management_ in 2000 consists of 6 issues, but two issues of volume 38 were also published in 2000, the articles received in 1999 and the beginning of 2000 appear in the next two issues (3 and 4). Therefore an exception was made for this journal and the articles from 10 issues (vol. 37, issue 1-6, vol. 38, issue 1-4) were included. The _International Journal of Information Management_ appears bi-monthly, the rest are quarterlies.

The contents of the research articles were analysed using the headings that emerged in 1989\. Literature reviews and editorials were omitted. New headings or sub-headings were added when necessary. The headings indicate the main subjects of the article. Not more than three headings were assigned to an article if the subject was complex. In addition, the research methods and the institution of the authors were studied paying attention to the affiliation, country, and number of authors of the article.

## Results and discussion

The overview of the research field in the 1980s revealed the following categories:

<table>

<caption>

**Table 1 : Categories found by Wilson in 1989**</caption>

<tbody>

<tr>

<th scope="col">

Application areas</th>

<th scope="col">

Artificial intelligence</th>

<th scope="col">

Economics of information</th>

<th scope="col">

Education for information management</th>

<th scope="col">

Information management functions</th>

<th scope="col">

Information policy</th>

<th scope="col">

Information use and users</th>

<th scope="col">

Information systems</th>

<th scope="col">

Information technology</th>

<th scope="col">

Systems theory</th>

</tr>

<tr>

<td>

*   banking
*   business
*   government
*   health services
*   industry
*   manufacturing

</td>

<td>

*   expert systems
*   knowledge-based systems

</td>

<td>

*   cost-benefit analysis
*   fee-based services
*   information and productivity
*   information economy
*   value-added processes
*   information industry

</td>

<td> </td>

<td>

*   aiding business strategy
*   chief information officers
*   computer-based records management
*   corporate information resources
*   information mapping
*   information resource management
*   manpower aspects
*   online information systems
*   organisational aspects
*   strategic monitoring

</td>

<td>

*   national information policies

</td>

<td>

*   end-user computing
*   information needs

</td>

<td>

*   database systems
*   data administration
*   decision support systems
*   group decision support
*   design
*   implementation
*   legal aspects
*   management information systems
*   organisational impact
*   strategies
*   user involvement

</td>

<td>

*   management aspects

    *   competitive advantage
    *   computer disaster strategies
    *   decisions on choices
    *   economic implications
    *   information centres
    *   legal implications
    *   marketing and sales potential
    *   office automation
    *   transborder data flow

*   technology aspects

    *   electronic publishing
    *   laser optics
    *   office automation
    *   optical character recognition
    *   personal computers
    *   telecommunications and IT
    *   videotex

</td>

<td> </td>

</tr>

</tbody>

</table>

Summarising the previous table five main categories can be derived:

*   economics of information
*   information management practice

*   application areas

*   information systems and technology

*   artificial intelligence
*   systems theory

*   information policy and strategy
*   information use and users

A total of 150 articles was chosen for analysis in 2000\. Almost one third of the articles was published in ten issues of _Information & Management_, the lowest number was published in _Information and Organisation_, which publishes two to three articles an issue. The rest usually have four to five articles an issue, and the difference in the number of articles depends on the frequency of the journal (IJIM - six annual issues, JSIS - a double issue (no. 2-3) in 2000).

<table>

<tbody>

<tr>

<th>Title</th>

<th>Number of articles</th>

</tr>

<tr>

<td>

_Information Economics and Policy (IEP)_</td>

<td>20</td>

</tr>

<tr>

<td>

_Information & Management (IM)_</td>

<td>48</td>

</tr>

<tr>

<td>

_Information and Organisation (IO)_</td>

<td>11</td>

</tr>

<tr>

<td>

_International Journal of Information Management (IJIM)_</td>

<td>33</td>

</tr>

<tr>

<td>

_Journal of Strategic Information Systems (JSIS)_</td>

<td>15</td>

</tr>

<tr>

<td>

_MIS Quarterly (MQ_)</td>

<td>23</td>

</tr>

<tr>

<td>

**TOTAL**</td>

<td>

**150**</td>

</tr>

</tbody>

<caption>

**Table 2 : The number of articles in selected journals**</caption></table>

### Contents

The comparison of subject categories found in 2000 (see Appendix 1) with those from the study of 1989 reveals new or expanded areas. However, most of the research areas present in 1980s remain - except that there is a significant reduction in attention to systems theory, although a systemic approach is often applied, and the interest in AI applications declines considerably (only two articles were found).

The main changes can be summarised as follows:

*   In the economics of information a shift to market economics is evident. The greatest attention is paid to the ways of increasing competitive advantage, aiding business strategies through IT, commercial and corporate usage of information networks, and the economic implications of information systems. Though purely economic articles are not numerous (5), the economic aspect is present in at least one third (34%) of all articles. General information management functions are mainly the concern of IJIM, but, as a secondary category, papers on this topic are found in all of the journals.
*   There is an increasing concern with organisational culture, environment and human issues. Different aspects of information users' behaviour gain priority against previously dominating information needs research (no articles with the primary category of Information needs were found in 2000). Organisational environment and learning are investigated as the factors influencing IT usage (5 articles). Sense making of IT and the ways IT and information systems are adopted by organisations, accepted by users or change organisations, are common in MQ, IM and especially IO. An entirely new category has emerged: information professionals. Researchers have realised that the professional behaviour of information systems and software developers, IT officers, information managers, librarians, etc. can influence the quality of the products and services and has to be taken into account.
*   The information systems area is changing in the same direction: human factors, the impact of organisational environment and culture, users' involvement and satisfaction gain more attention than previously. IS evaluation and quality issues, users' satisfaction or information usage interest the researchers of database systems, decision support systems, group support systems and other software (especially groupware, which emerges as a new category). The conclusions of [Claver _et al_](#cla00). (2000: 186) that "there is decreasing interest in subjects focusing on systems development in favour of managerial topics, which have received the most attention since 1996" is also supported.
*   Information networking, the Internet and intranets gains attention on various levels (from individual user's behaviour to macroeconomic issues). This category naturally did not exist in 1989 and is a single one among new categories that has expanded to almost the same extent as the categories of Information systems or Information management functions.
*   Telecommunication policies are the main concern of information policy research with a strong emphasis on market regulation policies in the telecommunications market and services. An issue of IEP (3) that published material from a _Symposium on Universal Service Obligation and Competition_ has introduced this topic into the analysis.
*   Application areas have proliferated significantly. In 1989 only six application areas were listed. Most of them remain, although government applications appear to have declined in interest, and banking with electronic payment systems has become the area of greatest interest (eight articles). Health care and medicine still appears in general information management periodicals, though many specialised journals are published for health information management. In addition, the book trade, commerce, education, engineering and construction, environmental policy, heritage preservation, manufacturing, port and post office applications, share trading and e-commerce have emerged or strengthened.
*   Knowledge management has emerged as new term rather than a new field - wherever it is mentioned, the study is conducted into the influence of human factors on information transfer, etc., or organisational cultures. There were two issues of the journals specifically devoted to knowledge management research:

1.  JSIS, 2000, vol. 9 issue 2-3
2.  Special issue of MQ spread over the issues 1 of 1999, 1 and 3 in 2000, the aim of which "has been to publish exemplary reports of intensive research, giving particular emphasis to studies that deal with knowledge" ([Markus and Lee, 2000](#mar00)) can be labelled as such.

Each of the selected journals has, of course, its area of interest. The dominating subjects are listed in Table 3\. However, in many cases the articles deal with complicated subjects that cannot be characterised by one category. The secondary categories applied to define the contents cover all the main categories for every journal.

<table>

<tbody>

<tr>

<th>Title</th>

<th>Dominating subjects</th>

</tr>

<tr>

<td>

_Information Economics and Policy_</td>

<td>Information policy: telecommunications (13 articles)  
Economics of information (4 articles)</td>

</tr>

<tr>

<td>

_Information & Management_</td>

<td>Information systems (22)  
Information networks (7)  
Information management functions (5)  
Information use and users (3)</td>

</tr>

<tr>

<td>

_Information and Organisation_</td>

<td>Organisational learning and culture (7)  
Information systems (3)</td>

</tr>

<tr>

<td>

_International Journal of Information Management_</td>

<td>Information management functions (9)  
Information systems (8)  
Information networks (4)  
Information use and users (4)  
Education for information (3)</td>

</tr>

<tr>

<td>

_Journal of Strategic Information Systems_</td>

<td>Information systems (6)  
Information technology (4)  
Organisational culture (2)</td>

</tr>

<tr>

<td>

_MIS Quarterly_</td>

<td>Information systems (9 articles)  
Information technology (7)  
Information professionals (4)</td>

</tr>

</tbody>

<caption>

**Table 3: Profiles of analysed journals (only one main subject category of those found in the year 2000 journals is used)**</caption></table>

### Research methods

*   Methodology has shifted from positivist to qualitative methods. Case studies pervade the areas of information systems, IT and information management functions research with major application of unstructured and semi-structured interviews, document analysis, ethnographic observation, and other qualitative methods of data collection. Quantitative methods and mathematic modeling do not lose ground. Quantitative methods are applied for research of various aspects and sometimes in combination with qualitative methods. The above mentioned special issue of _MQ_ in fact bore the title "Special issue on intensive research in information systems: using qualitative, interpretive, and case methods to study information technology". This marks a recognition of the trend and need to adopt new ways of thinking. However, it is interesting to observe that interpretative research and even the grounded theory approach are still used within the modernist paradigm which sees the world as predictable and controllable.
*   In connection with the increasing interest in organisational cultures more cross-cultural studies appear. These are influenced by globalisation and internationalisation processes and understanding that one model may not be suitable for the whole world. A cross cultural study of behaviour in software projects (Keil _et al._, 2000) and study of Lotus Notes implementation in Singapore ([Tung _et al._](#tun00), 2000) would be good examples of this type of studies.

### Authors

The investigated articles were written by 315 authors. 39 articles (26%) were published by a single author, the rest by two or more authors. The highest proportion of the articles written by more than one author is in _JSIS_ (13 of 15 - 87%), _IM_ (41 of 48 - 85%) and _MQ_ (18 of 23 - 78%) followed by _IJIM_ (23 of 33 - 70%), _IO_ (7 of 11 - 63%) and _IEP_ (10 of 20 - 50%).

The majority of the authors were affiliated with universities. 15 came from business-based research units and 7 from other research institutions. Most of those affiliated to universities come from Business, Management, Finance, and Economic departments (163 -55%) and from Computing and Information Systems departments (95 - 32%), approximately 20 authors (7%) belong to Information Management or LIS departments. In fact, there is no big difference in the journals as far as the subject affiliation of the authors is concerned. The only exception may be _IJIM_, which publishes more articles written by staff from Information Management and LIS departments.

Regarding the country of origin of the authors there are three American dominated journals: _MQ_ (48 of 62 authors come from USA - 77%), _IM_ (58 of 102 - 57%), _JSIS_ (17 of 32 - 53). Less dominated is _IO_ (8 of 21 - 38) and _IEP_ (7 of 33 - 21%). Authors from the United Kingdom dominate _IJIM_ (32 of 66 - 48%). However the range of the representatives from different countries is increasing.

<table>

<tbody>

<tr>

<th> </th>

<th colspan="2">Authors</th>

</tr>

<tr>

<th>Country</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>USA</td>

<td>143</td>

<td>45.4</td>

</tr>

<tr>

<td>UK</td>

<td>46</td>

<td>14.6</td>

</tr>

<tr>

<td>Australia</td>

<td>16</td>

<td>5.1</td>

</tr>

<tr>

<td>South Korea</td>

<td>15</td>

<td>4.8</td>

</tr>

<tr>

<td>Singapore</td>

<td>14</td>

<td>4.4</td>

</tr>

<tr>

<td>Hong Kong</td>

<td>13</td>

<td>4.1</td>

</tr>

<tr>

<td>Canada</td>

<td>12</td>

<td>3.8</td>

</tr>

<tr>

<td>Netherlands</td>

<td>10</td>

<td>3.2</td>

</tr>

<tr>

<td>Finland</td>

<td>10</td>

<td>3.2</td>

</tr>

<tr>

<td>Taiwan</td>

<td>7</td>

<td>2.2</td>

</tr>

<tr>

<td>France</td>

<td>6</td>

<td>1.9</td>

</tr>

<tr>

<td>Spain</td>

<td>5</td>

<td>1.6</td>

</tr>

<tr>

<td>Austria</td>

<td>3</td>

<td>0.9</td>

</tr>

<tr>

<td>Brunei</td>

<td>3</td>

<td>0.9</td>

</tr>

<tr>

<td>India</td>

<td>3</td>

<td>0.9</td>

</tr>

<tr>

<td>Italy</td>

<td>2</td>

<td>0.6</td>

</tr>

<tr>

<td>Sweden</td>

<td>2</td>

<td>0.6</td>

</tr>

<tr>

<td>Germany</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td>Malaysia</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td>New Zealand</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td>Norway</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td>Saudi Arabia</td>

<td>1</td>

<td>0.3</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**315**</td>

<td>

**99.7\***</td>

</tr>

</tbody>

<caption>

**Table 4 : Country of origin of authors (\* Note: rounding error)**</caption>

</table>

_IM_ has published articles written by the authors from 13 countries, _IJIM_ and _IEP_ from 11 countries each, _MQ_ from 8, _IJSIS_ and _IO_ from 7 countries each.

Thus, we see that the authors are coming not only from more diverse disciplines (e.g., management, computer science, information retrieval, information systems, library and information science, consultancy sectors, economics, etc.) but from wider geographical spread (Australia, Taiwan, Singapore, South Korea, Hong Kong, India, Brunei, Malaysia, Saudi Arabia) but US and UK researchers continue to dominate.

## Conclusion

In the 1980s information management was emergent and perceived by some to be simply a re-write of traditional librarianship. However, it has continued to thrive and much of what is now included is far removed even from modern information science, although information management draws upon ideas from both librarianship and information science. In one form or another it is likely to persist in the future, since information problems are likely to persist in organizations. The means for resolving the problems may change, but the need to understand those problems and develop solutions will remain.

There are strong pressures at the moment, from policy organizations such as the European Commission, from the consultancy companies such as Price Waterhouse Coopers (PwC), from hardware manufacturers (mainly IBM), and, sadly, from the research opportunists in every field, to subsume information management within 'knowledge management'. We believe, however, that information management has a stronger theoretical base than knowledge management and that the latter is simply a label, designed, like other labels, for presentational purposes, to impress the consumers of consultancy companies by giving the impression of something new and serious.

Perhaps we shall re-visit this topic in a few years' time to discover whether we are right.

## References

*   <a id="and86"></a>Andertone, R. (1986) "Short reports". _International Journal of Information Management_, **4**, 247-258.
*   <a id="bra89"></a>Braman, S. (1989) "Defining information: an approach for policymakers", _Telecommunication Policy_, **13**, 233-242.
*   <a id="bru93"></a>Bruns, Jr. et al. (1993) "Information and managers: a field study", Journal of Management Accounting Research, 5, 84-109.
*   <a id="cho98"></a>Choo, C.W. (1998) _Information management for the intelligent organizations: the art of scanning the environment_. Medford, NJ: Information Today, Inc.
*   <a id="cla00"></a>Claver E., González, R. & Llopis, C. (2000) An analysis of research in information systems (1981-1997), _Information & Management_, **37** (4), 181-195.
*   <a id="com77"></a>Commission on Federal Paperwork (1977) _Information resources management_. Washington DC: US Government Printing Office.
*   <a id="dav00"></a>Davenport, T. H. & Prusak, L. (2000) _Working knowledge: how organizations manage what they know_. Boston: Harvard Business School Press.
*   <a id="dre99"></a>Dresang, E.T. & Robbins, J.B. (1999) "Preparing students for information organizations in the twenty-first century: Web-based management and practice of field experience". _Journal of Education for Library and Information Science_, **40** (4), 218-230.
*   <a id="fai97"></a>Fairer-Wessels, F.A. (1997) "Information management education: towards a holistic perspective". _South African Journal of Library and Information Science_, **65** (2), 93-103.
*   <a id="kei00"></a>Keil M. et al. (2000) "A cross-cultural study of escalation of commitment behaviour in software projects" _Management Information Systems Quarterly_, **24** (2), 299-325.
*   <a id="kir99"></a>Kirk, J. (1999) "[Information in organisations: directions for information management](http://InformationR.net/ir/4-3/paper57.html)", _Information Research_, **4** (3). Available at http://InformationR.net/ir/4-3/paper57.html
*   <a id="kor00"></a>Korn/Ferry Int. (2000) "[The changing role of the Chief Information Officer](http://www.cio.com/executive/edit/kornferry.html)". _CIO Magazine_, April. Available at http://www.cio.com/executive/edit/kornferry.html [Site visited 6th April 2002]
*   <a id="kre71"></a>Kreiber C.H. & van Horn R.L. (1971). _Management information systems research_.. Pittsburg: Carnegie-Mellon University. (Report no. 225)
*   <a id="lun96"></a>Lundberg, A. (1996) "[How much do you think about information?](http://www.cio.com/CIO/060196_editor.html)", _CIO Magazine_, January. Available at http://www.cio.com/CIO/060196_editor.html [Site visited 6th April 2002]
*   <a id="lyt88"></a>Lytle, R.H. (1988) "Information resource management: a five-year perspective". _Information Management Review_, **3** (3), 9-16.
*   <a id="mar00"></a>Markus M.L. & Lee A.S. (2000) "Special issue on intensive research in information systems: using qualitative, interpretive, and case methods to study information technology – second installment; foreword" _Management Information Systems Quarterly_, **24** (1), 1.
*   <a id="mar99"></a>Martin, W. (1999) "New directions in education for LIS: knowledge management programs at RMIT." _Journal for Education for Library and Information Science_, **40** (3), 142-150
*   <a id="min80"></a>Mintzberg, H. (1980) _The nature of managerial work_. Englewood Cliffs, NJ: Prentice Hall.
*   <a id="row98"></a>Rowley, J. (1998) "Towards a framework for information management". _International Journal of Information Management_, **18** (5), 359-369.
*   <a id="tun00"></a>Tung L.L. et al. (2000). "Adoption, implementation and use of Lotus Notes in Singapore." _International Journal of Information Management_, **20** (5), 369-382.
*   <a id="wil89"></a>Wilson T.D. (1989) "Towards an information management curriculum." _Journal of Information Science_, **15** (4-5), 203-209.
*   <a id="wil97"></a>Wilson, T.D (1997) "Information management", in _International Encyclopedia of Information and Library Science_, pp. 187-196\. London: Routledge.

## Appendix 1

Categories characterising publications in information management area (2000)

**Application area**

*   Application area: banking - 6
*   Application area: book trade - 1
*   Application area: broking - 1
*   Application area: education - 1
*   Application area: engineering and construction - 1
*   Application area: environmental policy - 1
*   Application area: grocery - 1
*   Application area: health care and medicine - 2
*   Application area: heritage - 1
*   Application area: insurance - 1
*   Application area: litigation - 1
*   Application area: manufacturing - 2
*   Application area: negotiations - 1
*   Application area: pharmaceutical - 1
*   Application area: ports - 1
*   Application area: post office - 1
*   Application area: share trading - 1

**Artificial intelligence**

*   Artificial intelligence: neural networks - 1
*   Artificial intelligence: knowledge based systems - 1

**Economics of information**

*   Economics of information: economy - 1
*   Economics of information: information and productivity - 1
*   Economics of information: investment - 3

**Education for Information Management**

*   Education for Information Management - 3
*   Education for Information Management: Information literacy - 1

**Information management**

*   Information management: aiding business strategy - 10
*   Information management: aiding business strategies: environmental scanning - 1
*   Information management: corporate information resources - 5
*   Information management: manpower aspects - 3
*   Information management: manpower aspects: information overload - 1
*   Information management: organisational aspects - 2
*   Information management: security - 2
*   Information management: services - 2
*   Information management: strategic monitoring - 1
*   Information management: strategy - 7

**Information networks**

*   Information networks - 3
*   Information networks: economics - 1
*   Information networks: internet
*   Information networks: internet: economics -1
*   Information networks: internet: consumer behaviour - 1
*   Information networks: internet: e-commerce - 7
*   Information networks: internet: electronic market - 3
*   Information networks: internet: multimedia database - 1
*   Information networks: internet: usage - 1
*   Information networks: internet: usage: virtual communities - 2
*   Information networks: internet: web sites: design - 5
*   Information networks: internet: web sites: usage - 1
*   Information networks: intranet: quality issues - 2
*   Information networks: intranets - 1

**Information professionals**

*   Information professionals - 10

**Information systems**

*   Information systems: bibliometrics - 1
*   Information systems: database systems - 6
*   Information systems: decision support systems - 3
*   Information systems: design - 1
*   Information systems: design & implementation - 1
*   Information systems: design: human factors (designer, user) - 4
*   Information systems: design: human factors (designers) - 1
*   Information systems: development - 6
*   Information systems: evaluation - 1
*   Information systems: expert systems - 1
*   Information systems: globalisation - 2
*   Information systems: group support systems - 5
*   Information systems: groupware - 3
*   Information systems: implementation - 1
*   Information systems: implementation: human factors - 3
*   Information systems: interorganisational systems - 2
*   Information systems: life cycle - 1
*   Information systems: management - 5
*   Information systems: management: human resources - 2
*   Information systems: multimedia - 1
*   Information systems: organisational impact - 4
*   Information systems: philosophy - 1
*   Information systems: research (strategies) - 1
*   Information systems: research policy - 1
*   Information systems: software - 6
*   Information systems: strategies - 4
*   Information systems: user involvement - 5

**Information technology**

*   Information technology: adoption - 3
*   Information technology: competitive advantage - 4
*   Information technology: economic implications - 6
*   Information technology: marketing and sales - 2
*   Information technology: organisational impact - 6
*   Information technology: sense making - 1
*   Information technology: social aspects - 4
*   Information technology: strategy - 5
*   Information technology: strategy: investment - 2
*   Information technology: user acceptance - 5

**Information use and users**

*   Information use and users: behaviour - 12
*   Information use and users: behaviour: information overload - 3

**Knowledge management**

*   Knowledge management - 9

**Organisation**

*   Organisation culture - 2
*   Organisation culture: information sharing - 5
*   Organisation theory - 2
*   Organisation theory: sense making - 1
*   Organisational environment: impact on information technology usage - 5
*   Organisational learning - 5

**Telecommunication industry**

*   Telecommunications industry: economics: pricing - 1
*   Telecommunications industry: globalisation - 1
*   Telecommunications industry: developing countries - 1
*   Telecommunications industry: infrastructure development - 1
*   Telecommunications industry: market - 2
*   Telecommunications industry: market: competition - 5
*   Telecommunications industry: market: consumers: marketing - 1
*   Telecommunications industry: market: pricing - 2
*   Telecommunications industry: mobile telephone: consumers - 1
*   Telecommunications industry: policy: economy - 1
*   Telecommunications industry: performance - 2
*   Telecommunications industry: productivity - 1
*   Telecommunications industry: universal service - 3
*   Telecommunications industry: universal service: competition - 1

**Theory and research methods**

*   Theory and research methods - 4