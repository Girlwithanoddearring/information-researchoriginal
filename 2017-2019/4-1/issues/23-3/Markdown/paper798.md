<header>

#### vol. 23 no. 3, September, 2018

</header>

<article>

# Community disaster resilience and social solidarity on social media: a semantic network analysis of the Sewol ferry disaster

## [Jasmine Yoo Jung Hong, Narae Kim, Sangwon Lee](#author) and [Jang Hyun Kim](#author)

> **Introduction**: The present study examines how community disaster resilience and social solidarity concerning the Sewol ferry disaster, one of South Korea's most tragic maritime disasters that occurred on April 16, 2014, were portrayed on and manifested by social media during a 1-year span at three time periods.  
> **Method**: Data was collected from Korean social media platforms Naver blog and Naver café regarding the keyword 'Sewol ferry' during three time periods: April 16, 2014 to May 16, 2014, October 1, 2014 to October 31, 2014 and March 16, 2015 to April 16, 2015.  
> **Analysis**: A semantic network analysis was conducted on the data collected to show the most significant words pertaining to the Sewol ferry disaster.  
> **Results**: During the first period, people used social media to share news information and factual information, including some words indicating sympathy, positive emotions and hopeful sentiments. For the second period, the central words were similar to those of the first period, but more optimistic and emotional. In the last period, a noticeable change in the central words was found for the community's reminiscence and commemoration of the victims and their families' grief.  
> **Conclusions**: Words relevant to community disaster resilience and social solidarity were revealed on social media. Furthermore, there was a significant change in the sentiments of the community, as well as the way of forming resilience and solidarity over time.

<section>

## Introduction

On April 16, 2014, the Sewol ferry en route from Incheon to Jeju capsized and sank in the Jindo area ([Kim and Cho, 2014](#kim14)). A total of 476 passengers and crew members were on board, of which 339 were students and teachers of Danwon High School ([Kim and Cho, 2014](#kim14)). The sinking of the ferry resulted in 304 casualties, and 9 bodies remain missing as of December 2015, making it one of South Korea's most tragic maritime disasters ([Borowiec, 2015](#bor15)). As of 2018, family members of the victims are still experiencing severe post-traumatic stress disorder (PTSD), according to a study ([Lee _et al._, 2018](#lee18)). Even city-wide prescription of antidepressants has increased in Ansan city where the Danwon High School is located ([Han _et al._, 2017a](#han17a)).

A disaster is defined as

> a potentially traumatic event that is collectively experienced, has an acute onset, and is time-delimited; disasters may be attributed to natural, technological, or human causes

([McFarlane and Norris, 2006](#mcf06), p. 4). However, the term disaster has a plethora of definitions, which have transformed over the years, experiencing a shift from physical referents towards a social phenomenon ([Quarantelli, 1985](#qua85)). In other words, an incident is considered 'a disaster not because of the physical impact per se but because of the assumed social consequences of the physical happenings' ([Quarantelli, 1985](#qua85), p. 46-47). In this sense, a disaster can entail an 'unexpected collective action' ([Quarantelli, 1985](#qua85), p. 48), which can also be referred to as community resilience ([Paton, 2006](#pat06)) or solidarity ([Durkheim, 1912/2008](#dur12)).

The importance of social media's role in emergency management has been significantly increasing, both during and after the disaster. Social media, such as blogs, discussion forums, Facebook and Twitter, foster communication and dissemination of information among people ([Dufty, 2012](#duf12)). Recently, where the newspaper industry is in crisis and time and place are limited for newsgathering, social media can be a convenient tool for journalism. Unlike state and commercial actors, social media enable rich, quick, and easy-to access to a large range of interesting and otherwise difficult to approach information and sources, which can be used to voice the vox populi ([Broersma and Graham, 2012, 2013](#bro12)). Social media itself has some limitations. It is vulnerable to strategic posts from the state, political parties, commercial actors such as corporations, and small pressure groups. However, it is also true that Korean people are highly dependent on Naver as a search engine and Naver blog and café as a forum for sharing ideas. Thus, the authors of this research assume that in spite of some limitations Naver blogs and cafés are still an importance medium for public exchanges. Social media is especially effective in disaster communication, as it provides access to timely information from various sources and connects people within the community ([Taylor, Wells, Howell and Raphael, 2012](#tay12)). This was proven during the prominent 2011 natural disasters that occurred in Australia, New Zealand and Japan ([Taylor _et al._, 2012](#tay12)). As for the Sewol ferry disaster, the yellow ribbon campaign which arose from it rapidly spread throughout social media. The yellow ribbon symbolized the solidarity with the victims' families and hope for the return of the missing passengers ([Mullen, 2014](#mul14)). Many people have posted the photo of a yellow background and a black ribbon on social media, such as Facebook and Naver blogs ([Mullen, 2014](#mul14)). The date April 16, 2015 marked the first anniversary of the disaster, thus revitalizing active participation on social media. Han et. al. ([2017b](#han17b)) compared Korean online portal with traditional media, suggesting that the Korean online portal website Naver in terms of agenda setting might be relatively weaker than traditional news media, as it is just a news aggregator. In addition, the online portal website is structured mainly to display the most viewed and recommended articles. Accordingly, the possibility of delivering diverse issues in online coverage might be limited, in comparison to the traditional media. However, the study did not deal with social media and their role in resilience.

</section>

<section>

## Literature review

### Community disaster resilience

In the aftermath of a disaster or a crisis, either resistance or transient dysfunction occurs ([Norris, Stevens, Pfefferbaum, Wyche and Pfefferbaum, 2008](#nor08)). Resistance is about immediately counteracting through adequate resources, such as economic development, social capital, information and communication, and community competence, thus resulting in no dysfunction ([Norris _et al._, 2008](#nor08)). On the other hand, transient dysfunction may lead to resilience or vulnerability ([Norris _et al._, 2008](#nor08)). The former helps the relevant publics recover from the disaster, while the latter may lead to persistent dysfunction. In another sense, resilience is when there is latency in counteracting disaster through adequate resources, whereas vulnerability is when there are not sufficient resources to form resistance or resilience, thus leading to a persistent and ongoing dysfunction ([Norris _et al._, 2008](#nor08)). Even for US federal agencies, there is a change in the rhetoric from disaster vulnerability to resilience pertaining to natural disasters, since resilience is considered ‘a more proactive and positive expression of community engagement’ ([Cutter _et al._, 2008](#cut08), p. 598).

The term resilience is originally rooted in such fields as physics and mathematics, which defines it as ‘the capacity of a material or system to return to equilibrium after a displacement’ ([Norris _et al._, 2008](#nor08), p. 127). Hence, in applying the term to people and their environments, it is considered a metaphor ([Norris _et al._, 2008](#nor08)). When referring to resilience from a theoretical perspective, it is ‘a process linking a set of adaptive capacities to a positive trajectory of functioning and adaptation after a disturbance’ ([Norris _et al._, 2008](#nor08), p. 130), which involves ‘nurturing and sustaining the capacity of people, communities and societal institutions to adapt to and experience benefit from disaster’ ([Paton, 2006](#pat06), p. 315). Disaster resilience occurs at different levels, which include individual, community (collective), institutional and environmental ([Paton, 2006](#pat06)). When a disaster occurs, people feel a sense of ‘shared responsibility’ ([Dufty, 2012](#duf12)). Thus, the present study focuses on the community level, in which a community is defined as ‘an entity that has geographic boundaries ([Korea in the current paper]) and shared fate’ ([Norris _et al._, 2008](#nor08), p. 128).

There have been numerous studies on community disaster resilience from various fields and aspects, such as public health ([Plough _et al._, 2013](#plo13)), urban building ([Godschalk, 2003](#god03)), terrorism ([Bonanno, Galea, Bucciarelli and Vlahov, 2006](#bon06)) and natural disasters ([Taylor _et al._, 2012](#tay12); [Middleton, Middleton and Modafferi, 2014](#mid14)). Yoon, Kang and Brody ([2015](#yoo15)), on user task performance and their eye movements as they use the system. Following the completion of the VICS and extended CSA-WA tests, participants performed developed a community disaster resilience index (CDRI) based on six dimensions: ‘the human, social, economic, institutional, physical, and environmental dimensions’ (p. 443). Applying the CDRI to natural disaster cases in Korea, the researchers found that the selected dimensions have a significant effect on reducing aspects such as economic damage, implying that a stronger disaster resilience leads to less damages and loss ([Yoon, Kang and Brody, 2015](#yoo15)).

However, the semantic traits related to community disaster resilience have rarely been studied.

### Social solidarity theory

Social solidarity, which was developed systematically by ([Durkheim, 1912/2008](#dur12)), is an important sociological concept that is also closely related to community disaster resilience. Durkheim ([1912/2008](#dur12)) attempted to examine how the constituents of a society are able to stay cohesive, despite the increasingly individualistic nature of society. Social solidarity is also an important concept in the study of disasters. Researchers have consistently found that social solidarity serves as a protective factor for community members, which makes them come together to recover from tragic incidents.

For Durkheim ([1912/2008](#dur12)), the integrated symbolic action comes from the collective recognition that ‘the group periodically renews the sentiment which it has of itself and its unity; at the same time, individuals are strengthened in their social natures’ (p. 375). Thus, social solidarity is at its peak through symbolic acts of collective reaction ([Durkheim, 1912/2008](#dur12)). Governments and other organizations may benefit from this social solidarity. Considering the complementary roles of social media to those of state and commercial actors ([Broersma and Graham, 2012, 2013](#bro12)), governments and public organizations need to understand this phenomenon and to leverage it.

Parsons ([2013](#par13)) further developed collective solidarity by using a social system of interaction based on Durkheim’s ([1912/2008](#dur12)) work. According to recent studies on social solidarity in disaster, there was an increase in perceived solidarity and social solidarity after a large-scale tragic disaster ([Barnshaw, Letukas and Oloffson, 2008](#bar08)). However, there is a lack of adequate research on how this solidarity affects victims or community members, as well as the recovery of a disaster.

In another study, Hawdon and Ryan ([2011](#haw11)) argued that

> ‘[T]hese displays of solidarity provide emotional support for survivors, reaffirm that the group remains intact, and stimulate collective action that strengthen existing social networks’

(p. 1364), which implies that social solidarity frequently benefits survivors and victims.

Social solidarity increases the collective responses to disasters because community members pay attention to victims and their symbolic actions after a tragedy occurs ([Collins, 2004](#col04)). Moreover, communal bereavement increases social solidarity, which leads to a collective action such as memorial rituals ([Hawdon and Ryan, 2011](#haw11)). Hawdon and Ryan ([2011](#haw11)) insisted that the collective action intensifies community resilience and emotions, thus leading to solidarity-producing actions, which in turn increases collectivism. In addition, after a year of disaster, the effect of collective actions significantly remains in the community ([Hawdon and Ryan, 2011](#haw11)).

### Community disaster resilience and solidarity revealed on social media

In order for effective community disaster resilience and social solidarity to occur, communication is an essential element in mitigating disaster impacts ([Houston _et al._, 2015](#hou15). People exchange information through social media, which offer easy access to a large range of meaningful and otherwise difficult to approach sources ([Lindsay, 2011](#lin11); [Broersma and Graham, 2013](#bro13)). Thus, due to such nature of social media, it is an important channel and platform for us to understand collective disaster resilience and how social solidarity is formed. According to Lindsay ([2011](#lin11)), the two main uses of social media for emergencies and disasters are the exchange of information and systematic use, such as issuing warnings or monitoring activities.

There have been studies on the relationship between social media and disaster responses. Taylor _et al._ ([2012](#tay12)) conducted a survey to examine the use of social media in establishing community disaster resilience during natural disasters in Australia and New Zealand. Key findings from the study indicated that social media were used to share government-sourced information deemed useful for the community by re-posting (sharing) or re-tweeting, thus being able to share timely information and create a sense of connectedness ([Taylor _et al._, 2012](#tay12)). In that social media are largely a ‘top-down’ and ‘across’ phenomenon, they function as a ‘psychological first aid’ for the public accordingly ([Taylor _et al._, 2012](#tay12), p. 26). Additionally, in social media as an information- and reaction-exchange network for actors and individuals, the sentiments of those affected by tragic events can be represented and understood. In another study, Houston _et al._ ([2015](#hou15)) constructed a functional framework pertaining to disaster social media, which categorized 15 different uses, such as obtaining and creating content and connecting community members in the aftermath of a disaster. The participants of disaster social media consist of communities, government, media outlets, and others ([Houston _et al._, 2015](#hou15)).

The Sewol ferry disaster and its aftermath effects have been analysed in various perspectives and fields, such as, but not limited to, the linguistic analysis of headlines of two Korean newspapers (Chosun ilbo and Hankyoreh shinmun) regarding the Sewol ferry incident [Ju and Yeon, 2015](#juy15)), maritime safety management ([Kim, 2015](#kim15)) and behavioral safety [Zhang and Wang, 2015](#zha15)). In another study, Woo, Cho, Shim, Lee and Song ([2015](#woo15)) examined the public mood in Korea in response to the Sewol ferry disaster by looking at Twitter data and analyzing keywords pertaining to human-made disaster and suicide. Main findings from the study indicate that in the aftermath of the incident, emotional reactions, such as anger and suicide-related keywords were prevalent on Twitter ([Woo _et al._, 2015](#woo15)).

Despite a number of studies on the relationship between community resilience and collective action and social media, as well as the Sewol ferry disaster, there is a lack of studies focusing on the semantic traits and how they have changed over time.

</section>

<section>

## Research questions

Based on the abovementioned theoretical discussions, two main research questions have been formulated.

> RQ1\. How are community disaster resilience and solidarity portrayed on and manifested by social media regarding the Sewol ferry disaster?

As aforementioned, social media has increasingly become an effective media channel for emergency management. In past studies, the relationship between media and disaster responses has not been thoroughly examined through a semantic network analysis. In addition, there is yet to be a study on the analysis of community disaster resilience and solidarity formed on and by social media that is specifically pertaining to the Sewol ferry disaster. By analyzing the most significant and frequently occurring words pertaining to the Sewol ferry disaster on social media, the present study provides insights on the types of words that are related to the theories.

Moreover, there is a lack of adequate studies on the relationship between time and the level of community disaster resilience and solidarity on social media. Consequently, it is uncertain whether the strength of disaster resilience and solidarity increases, stagnates or weakens over time in an online environment. By examining the changes in the word usage over different time periods, the present study would be able to provide further insights on the overall transformation of significant words and public sentiments revealed on social media. Thus, the following second research question has been derived:

> RQ2\. Does the level of community disaster resilience and solidarity change over time on social media?

</section>

<section>

## Methodology of research

The present study used the key term Sewol ferry (세월호) on Korean social media platforms Naver blog and Naver café. Naver café is an online community platform provided by Naver, the largest Korean search engine company. Although an exact number of Naver blog and cafe (daily) users has not been released, the Naver search results indicate that there are more than 9.9 million cafes in Naver cafe space. As most Koreans are highly dependent on Naver, it is estimated that a majority of Koreans are using Naver cafe and/or blog service. Data was collected and analysed in three different time periods: April 16, 2014 to May 16, 2014, the one-month period right after the disaster occurred; October 1, 2014 to October 31, 2014, the one-month period that forms the midpoint between the occurrence of the disaster and the first anniversary; March 16, 2015 to April 16, 2015, the one-month period leading up to the first anniversary of the disaster. [Textom](http://www.textom.co.kr/home/main/main.php), a semantic network analysis tool, was used to collect data on the most frequently appearing and co-occurring words on the aforementioned channels and during the specified time periods. The top 50 to 55 words that had a frequency of 60 or higher were selected. For the visual representation of the network, NetDraw from UCINET ([Borgatti, Everett and Freeman, 2002](#bor02)) network analysis tool was used.

Words from the Naver blog and café were analyzed by the UCINET software and human coders. The former was used to list words of each time period by frequency descending order and present inter-word networks based on word distance and co-occurrence. Betweenness centrality was indicated as the size of each node in each figure. Betweenness centrality is defined as “Let bjk be the proportion of all geodesics linking vertex j and vertex k which pass through vertex i. The betweenness of vertex i is the sum of all bjk where i, j and k are distinct. Betweenness is therefore a measure of the number of times a vertex occurs on a geodesic” ([Borgatti _et al._, 2002](#bor02), help section, based on Freeman, 1979). The latter was used to tell facts, solidarity and resilience-related words. For each word included in the high frequency word list, the three coders read the original blog or café posts and evaluated whether each word usage belong to solidarity and resilience-related group. After coding was done, the authors calculated inter-coder reliability Cronbach’s alpha (=.91), which show high level of reliability.

</section>

<section>

## Results

### Frequency analysis of word usage over time. Period 1: April 16, 2014 to May 16, 2014

The most frequently appearing nodes in the network are such words as Sewol (세월), sink (침몰), disaster (참사), victim (희생자), nature of event (사고), Sewol ferry (세월호) and ferry(여객선), in the frequency descending order. The Freeman’s betweenness values of the nodes are as the following: Sewol (세월): 2.549; sink (침몰): 2.549; disaster (참사): 2.549; victim (희생자): 2.549; nature of event (사고): 2.549; Sewol ferry (세월호): 2.549; ferry (여객선): 2.229.

In terms of RQ1, words that portray community disaster resilience and solidarity include Yellow ribbon (노란리본), condolences (애도), citizens (국민), South Korea (대한민국), hope (희망), people’s wish (명복), news (소식), miracle (기적), group memorial altar (합동분향소) and memorial (추모). Refer to Figure 1 below for the diagram of the semantic network of the April 16, 2014 to May 16, 2014 period.

<figure>

![Figure 1: Semantic Network Analysis](../p798fig1.png)

<figcaption>Figure 1: Semantic network analysis: April 16, 2014 to May 16, 2014</figcaption>

</figure>

### Period 2: October 1, 2014 to October 31, 2014

The most frequent words in the data include Sewol (세월), disaster (참사), bereaved family (유가족), missing people (실종자), nature of event (사고), Sewol ferry (세월호) and captain (선장), in the frequency descending order. The Freeman’s betweenness values of the nodes are as follows: Sewol (세월): 9.358; disaster (참사): 9.358; bereaved family (유가족): 8.728; missing people (실종자): 7.169; nature of event (사고): 9.358; Sewol ferry (세월호): 6.415; captain (선장): 4.129.

RQ1\. Words that portray community disaster resilience and solidarity include Gwanghwamun (광화문, the location of memorial events and bereaved family rallies), truth ascertainment (진상규명), Sewol ferry special law (세월호특별법, a law requested by bereaved family and their supporters), ruling and opposition parties (여야), citizens (국민) and truth (진실). Refer to Figure 2 below for the diagram of the semantic network of the October 1, 2014 to October 31, 2014 period.

<figure>

![Figure 2: Semantic network analysis](../p798fig2.png)

<figcaption>Figure 2: Semantic network analysis: October 1, 2014 to October 31, 2014</figcaption>

</figure>

### Period 3: March 16, 2015 to April 16, 2015

The most frequently appearing nodes in the network are such words as Sewol (세월), 1 (numeric one), disaster (참사), heart (마음), 1st anniversary (1주기), news (뉴스) and bereaved family (유가족), in the frequency descending order. The Freeman’s betweenness values of the nodes are as follows: Sewol (세월): 1.874; 1: 1.874; disaster (참사): 1.874; heart (마음): 1.331; 1st anniversary (1주기): 1.874; news (뉴스): 1.003; bereaved family (유가족): 1.191.

RQ1\. Words that portray community disaster resilience and solidarity include memorial (추모), yellow ribbon (노란리본), South Korea (대한민국), citizens (국민), family (가족), truth (진실), people’s wish (명복), news (소식) and never forget (잊지않겠습니다). Refer to Figure 3 below for the diagram of the semantic network of the March 16, 2015 to April 16, 2015 period.

<figure>

![Figure 3: Semantic Network Analysis](../p798fig3.png)

<figcaption>Figure 3: Semantic Network Analysis: March 16, 2015 to April 16, 2015</figcaption>

</figure>

Tables 1, 2 and 3 below show the frequency of the top twenty most frequently appearing words for the three time periods (see the [appendix](#app) for a complete list).

<table><caption>Table 1: Frequency Table (April 16, 2014 to May 16, 2014)</caption>

<tbody>

<tr>

<th>Ranking</th>

<th>Word</th>

<th>English translation</th>

<th>Frequency</th>

</tr>

<tr>

<td>1</td>

<td>세월</td>

<td>Sewol</td>

<td>4829</td>

</tr>

<tr>

<td>2</td>

<td>침몰</td>

<td>Sink</td>

<td>900</td>

</tr>

<tr>

<td>3</td>

<td>참사</td>

<td>Disaster</td>

<td>612</td>

</tr>

<tr>

<td>4</td>

<td>사고</td>

<td>Nature of event</td>

<td>484</td>

</tr>

<tr>

<td>5</td>

<td>희생자</td>

<td>Casualty</td>

<td>428</td>

</tr>

<tr>

<td>6</td>

<td>사건</td>

<td>Incident</td>

<td>410</td>

</tr>

<tr>

<td>7</td>

<td>세월호</td>

<td>Sewol ferry</td>

<td>294</td>

</tr>

<tr>

<td>8</td>

<td>여객선</td>

<td>Ferry</td>

<td>249</td>

</tr>

<tr>

<td>9</td>

<td>선장</td>

<td>Captain</td>

<td>242</td>

</tr>

<tr>

<td>10</td>

<td>이번</td>

<td>This time</td>

<td>206</td>

</tr>

<tr>

<td>11</td>

<td>관련</td>

<td>Related</td>

<td>204</td>

</tr>

<tr>

<td>12</td>

<td>실종자</td>

<td>Missing people</td>

<td>155</td>

</tr>

<tr>

<td>13</td>

<td>마음</td>

<td>Heart</td>

<td>154</td>

</tr>

<tr>

<td>14</td>

<td>추모</td>

<td>Memorial</td>

<td>154</td>

</tr>

<tr>

<td>15</td>

<td>구조</td>

<td>Rescue</td>

<td>144</td>

</tr>

<tr>

<td>16</td>

<td>사람</td>

<td>Person</td>

<td>139</td>

</tr>

<tr>

<td>17</td>

<td>국민</td>

<td>Citizens</td>

<td>138</td>

</tr>

<tr>

<td>18</td>

<td>침몰사</td>

<td>Sinking accident</td>

<td>130</td>

</tr>

<tr>

<td>19</td>

<td>4월</td>

<td>April</td>

<td>128</td>

</tr>

<tr>

<td>20</td>

<td>배</td>

<td>Ship</td>

<td>127</td>

</tr>

</tbody>

</table>

<table><caption>Table 2: Frequency Table (October 1, 2014 to October 31, 2014)</caption>

<tbody>

<tr>

<th>Ranking</th>

<th>Word</th>

<th>English translation</th>

<th>Frequency</th>

</tr>

<tr>

<td>1</td>

<td>세월</td>

<td>Sewol</td>

<td>3623</td>

</tr>

<tr>

<td>2</td>

<td>참사</td>

<td>Disaster</td>

<td>506</td>

</tr>

<tr>

<td>3</td>

<td>유가족</td>

<td>Bereaved family</td>

<td>550</td>

</tr>

<tr>

<td>4</td>

<td>특별법</td>

<td>Special law</td>

<td>285</td>

</tr>

<tr>

<td>5</td>

<td>사건</td>

<td>Incident</td>

<td>253</td>

</tr>

<tr>

<td>6</td>

<td>실종자</td>

<td>Missing people</td>

<td>252</td>

</tr>

<tr>

<td>7</td>

<td>사고</td>

<td>Nature of event</td>

<td>225</td>

</tr>

<tr>

<td>8</td>

<td>선장</td>

<td>Captain</td>

<td>205</td>

</tr>

<tr>

<td>9</td>

<td>침몰</td>

<td>Sink</td>

<td>175</td>

</tr>

<tr>

<td>10</td>

<td>세월호</td>

<td>Sewol ferry</td>

<td>174</td>

</tr>

<tr>

<td>11</td>

<td>시신</td>

<td>Dead body</td>

<td>163</td>

</tr>

<tr>

<td>12</td>

<td>사람</td>

<td>Person</td>

<td>132</td>

</tr>

<tr>

<td>13</td>

<td>발견</td>

<td>Discover</td>

<td>124</td>

</tr>

<tr>

<td>14</td>

<td>사형</td>

<td>Capital punishment</td>

<td>123</td>

</tr>

<tr>

<td>15</td>

<td>가족</td>

<td>Family</td>

<td>118</td>

</tr>

<tr>

<td>16</td>

<td>희생자</td>

<td>Casualty</td>

<td>115</td>

</tr>

<tr>

<td>17</td>

<td>이준석</td>

<td>Joon-Seok Lee</td>

<td>112</td>

</tr>

<tr>

<td>18</td>

<td>생각</td>

<td>Think</td>

<td>111</td>

</tr>

<tr>

<td>19</td>

<td>오늘</td>

<td>Today</td>

<td>111</td>

</tr>

<tr>

<td>20</td>

<td>이후</td>

<td>After</td>

<td>107</td>

</tr>

</tbody>

</table>

<table><caption>Table 3: Frequency Table (March 16, 2015 to April 16, 2015)</caption>

<tbody>

<tr>

<th>Ranking</th>

<th>Word</th>

<th>English translation</th>

<th>Frequency</th>

</tr>

<tr>

<td>1</td>

<td>세월</td>

<td>Sewol</td>

<td>4814</td>

</tr>

<tr>

<td>2</td>

<td>1</td>

<td>1</td>

<td>952</td>

</tr>

<tr>

<td>3</td>

<td>참사</td>

<td>Disaster</td>

<td>857</td>

</tr>

<tr>

<td>4</td>

<td>오늘</td>

<td>Today</td>

<td>684</td>

</tr>

<tr>

<td>5</td>

<td>1주</td>

<td>1st anniversary</td>

<td>544</td>

</tr>

<tr>

<td>6</td>

<td>추모</td>

<td>Memorial</td>

<td>459</td>

</tr>

<tr>

<td>7</td>

<td>사건</td>

<td>Incident</td>

<td>398</td>

</tr>

<tr>

<td>8</td>

<td>1년</td>

<td>One year</td>

<td>349</td>

</tr>

<tr>

<td>9</td>

<td>4월</td>

<td>April</td>

<td>280</td>

</tr>

<tr>

<td>10</td>

<td>1주기</td>

<td>1st anniversary</td>

<td>270</td>

</tr>

<tr>

<td>11</td>

<td>세월호</td>

<td>Sewol ferry</td>

<td>257</td>

</tr>

<tr>

<td>12</td>

<td>사고</td>

<td>Nature of event</td>

<td>246</td>

</tr>

<tr>

<td>13</td>

<td>기억</td>

<td>Remember</td>

<td>217</td>

</tr>

<tr>

<td>14</td>

<td>날</td>

<td>Day</td>

<td>216</td>

</tr>

<tr>

<td>15</td>

<td>희생자</td>

<td>Casualty</td>

<td>216</td>

</tr>

<tr>

<td>16</td>

<td>16</td>

<td>16</td>

<td>212</td>

</tr>

<tr>

<td>17</td>

<td>침몰</td>

<td>Sink</td>

<td>211</td>

</tr>

<tr>

<td>18</td>

<td>1주</td>

<td>1st anniversary</td>

<td>205</td>

</tr>

<tr>

<td>19</td>

<td>인양</td>

<td>Salvage</td>

<td>203</td>

</tr>

<tr>

<td>20</td>

<td>생각</td>

<td>Think</td>

<td>169</td>

</tr>

</tbody>

</table>

RQ2.Changes over time. The significant words that express community disaster resilience and social solidarity have changed over time. Although the level of participation on community resilience and solidarity cannot be specifically measured, it is evident that there has been a shift in the type of words that most frequently appear on social media. The first period (April 16, 2014 to May 16, 2014) contained disheartening yet considerably positive words, such as condolences (애도), hope (희망), miracle (기적) and memorial (추모). This then changed to truth ascertainment (진상규명), Sewol ferry special law (세월호특별법), ruling and opposition parties (여야) and truth (진실). Then in the last period, nodes such as memorial (추모), yellow ribbon (노란리본), truth (진실) and never forget (잊지않겠습니다) appeared. Table 2 below shows the overview of community resilience and solidarity-related terms that appear in the three time periods.

<table><caption>Table 4: Overview of community resilience- and solidarity-related words *The highlighted words signify recurring community resilience- and solidarity-related terms</caption>

<tbody>

<tr>

<th>Period 1(April 16, 2014 to May 16, 2014)</th>

<th>Period 2(October 1 to 31, 2014)</th>

<th>Period 3(March 16, 2015 to April 16, 2015)</th>

</tr>

<tr>

<td>yellow ribbon (노란리본)</td>

<td>Gwanghwamun (광화문)</td>

<td>memorial (추모)</td>

</tr>

<tr>

<td>condolences (애도)</td>

<td>truth ascertainment (진상규명)</td>

<td>yellow ribbon (노란리본)</td>

</tr>

<tr>

<td>hope (희망)</td>

<td>Sewol ferry special law (세월호특별법)</td>

<td>South Korea (대한민국)</td>

</tr>

<tr>

<td>people’s wish (명복)</td>

<td>ruling and opposition parties (여야)</td>

<td>citizens (국민)</td>

</tr>

<tr>

<td>news (소식)</td>

<td>citizens (국민)</td>

<td>family (가족)</td>

</tr>

<tr>

<td>miracle (기적)</td>

<td>truth (진실)</td>

<td>truth (진실)</td>

</tr>

<tr>

<td>group memorial altar (합동분향소)</td>

<td> </td>

<td>people’s wish (명복)</td>

</tr>

<tr>

<td>memorial (추모)</td>

<td> </td>

<td>news (소식)</td>

</tr>

<tr>

<td>citizens (국민)</td>

<td> </td>

<td>never forget (잊지않겠습니다)</td>

</tr>

<tr>

<td>South Korea (대한민국)</td>

<td> </td>

<td>remember (기억)</td>

</tr>

</tbody>

</table>

</section>

<section>

## Discussion

The results of the present study suggest that during the first period (April 16, 2014 to May 16, 2014), people used social media to share news information and factual information. Additionally, other words indicated sympathy, positive emotions and hopeful sentiments: e.g., condolences (애도), hope (희망), miracle (기적), heart (가슴), group memorial altar (합동분향소), memorial (추모) and yellow ribbon (노란리본). These words signify that the Korean citizens united as a community to provide both emotional and physical support for the victims’ families. Furthermore, the words support the proposition that social media are used for social solidarity.

For the second period (October 1, 2014 to October 31, 2014), the central words are similar to the previous network, but optimistic and emotional words, such as hope (희망) and heart (마음), moved towards the center of the network. Also, the overall tone of the words became more objective than the previous period, suggesting that the community is searching for solutions to this tragic disaster rather than simply emotionally and passively accepting the disaster. Thus, words pertaining to politics, such as government (정부), ruling and opposition parties (여야), president (대통령) and Gwanghwamun (광화문) have appeared in the network. Moreover, other nodes such as truth ascertainment (진상규명), Sewol ferry special law (세월호특별법), truth (진실) and captain (선장) denote the community’s state of public doubt and requesting a legal initiative.

In the last period (March 16, 2015 to April 16, 2015), there is a noticeable change in the words that are central, when compared to the previous two networks. Significant nodes, such as 1, 1st anniversary (1주기), heart (마음), memorial (추모), yellow ribbon (노란리본) and never forget (잊지않겠습니다) express 1) the community’s reminiscence and commemoration of the victims, and 2) their families’ grief. There are similarities between this network and the first, as shown in the word usage of memorial (추모), yellow ribbon (노란리본), citizens (국민) and South Korea (대한민국). The overlapping usage demonstrates the Korean community’s collective and national sentiments.

### Theoretical implications

There are several theoretical implications that can be drawn from the present study’s findings. First, regarding the Sewol ferry incident, community disaster resilience and social solidarity are indeed revealed on social media. Words such as, but not limited to, yellow ribbon (노란리본), memorial (추모), South Korea (대한민국) and citizens (국민) signify a sense of community. Second, there has been a shift in the sentiment of collective resilience and solidarity on social media. Initially, the tone of the words was emotional and hopeful, which then changed to rational, as words calling for truth appeared. In the third period, the sentiment drifted away from hopeful and rational towards emotions and commemoration. It is not only the sentiments and emotions, but also the way of forming resilience and solidarity that has shifted over time. During the first period, the community 1) called for participation in the group memorial altar and 2) expressed condolences and hope to the bereaved families. However, in the second period, the community initiated more active participation such as demonstrations in Gwanghwamun, and requested further investigation. In the last period, the community still seeks truth, but in a more passive way through commemoration than the previous periods. Through the findings, it is revealed that time is a significant element that should be considered while looking at disaster resilience and solidarity. Such implications of the present study provide insights on how social media was used for community disaster resilience and social solidarity pertaining to the Sewol ferry disaster, as well as how they have transformed over a one-year period.

### Practical implications

There are several practical implications. Based on the present study’s findings that people used social media to share factual information regarding the Sewol ferry disaster, government-affiliated organizations or public institutions can use online communication channels to provide both timely and accurate news information and announcements during emergency situations to the public. In addition, considering that time is a factor to be considered for disaster resilience and solidarity, such organizations can use this knowledge to effectively plan out a timeline of communication activities.

</section>

<section>

## Limitations and suggestions for future research

There were several limitations to the study. First, the present study only examined Naver blog and café, thus limiting other prominent social media platforms in Korea. Naver blog and café are still one of the leading services in each domain, but it may be difficult to generalize the findings of the present study to the entire social media space in Korea. According to Kim ([2016](#kim16))(2016) there are approximately 42 million users with a Naver ID, out of about 51.25 million Koreans. Second, data was collected only during a total of a three-month period. It may not fully capture the semantics regarding community disaster resilience and solidarity on social media. However, the three selected time periods – the one-month period right after the disaster, the one-month period that forms the midpoint between the disaster and its first anniversary and the one-month period leading up to the first anniversary – may have been when activities on social media were most likely to be active.

For future studies, it is recommended that researchers scrutinize other Korean social media platforms, such as (but not limited to) Daum café and blog, for a more extensive period of time than the present study. Moreover, since the Sewol ferry disaster received worldwide attention, it is recommended that future research be conducted on other international social media platforms, such as Facebook and Twitter, to gauge a more comprehensive research on community resilience and social solidarity on social media.

</section>

<section>

## About the authors

**Jasmine Yoo Jung Hong** is a PhD student in Interaction Science at Sungkyunkwan University, South Korea. Before entering her doctoral programme, she obtained her Master’s degree in Management (specialisation in Sales & Marketing) from IE Business School in Madrid, Spain and her Bachelor’s degree in Public Relations with a minor in Information Management and Technology from Syracuse University in Syracuse, NY, USA. Her main research interests lie in social media, crisis communication, social and semantic network analyses, and reputation management. She can be contacted at[yjasmine@skku.edu](mailto: yjasmine@skku.edu)  
**Narae Kim** is a Master’s degree holder from the Department of Interaction Science at Sungkyunkwan University, South Korea. She received her Bachelor's degree in English literature and language from Yonsei University. Her research interests include Internet of Things (IoT) service, user-centered service, user experience, and customer experience. She can be contacted at [narae0113@naver.com](mailto:narae0113@naver.com)  
**Sangwon Lee** is an Associate Professor in the Department of Interaction Science, Sungkyunkwan University, Seoul, South Korea. He received his Bachelor’s degree in Industrial Engineering from Korea University, South Korea, and his PhD from Pennsylvania State University, USA. He can be contacted at [upcircle@skku.edu](mailto: upcircle@skku.edu)  
**Jang Hyun Kim** (corresponding author) is an Associate Professor in the Department of Interaction Science, Sungkyunkwan University, Seoul, South Korea. He received his Bachelor’s degree in Economics and Master’s degree in Communication from Yonsei Universiy, South Korea, and his PhD from State University of New York at Buffalo, New York, USA. He can be contacted at [alohakim@skku.edu](mailto:alohakim@skku.edu)

</section>

<section>

## <a id="author"></a>Acknowledgement

This research was supported by the MIST(Ministry of Science and ICT), Korea, under the National Program for Excellence in SW supervised by the Institute for Information & communications Technology Promotion (2015-0-00914)

</section>

<section>

## References

<ul>

<li id="bar08">Barnshaw, J., Letukas, L. &amp; Oloffson, A. (2008). <em><a href="http://www.webcitation.org/72Ttd8CAl">Solidarity trumps catastrophe? An empirical and theoretical analysis of post-tsunami media in two western nations.</a></em> Paper presented at the annual meeting of the American Sociological Association Annual Meeting, Boston, MA, July, 2018. Retrieved from http://udspace.udel.edu/bitstream/handle/19716/4055/PP%20363.pdf?sequence=1&isAllowed=y.  (Archived by WebCite® at http://www.webcitation.org/72Ttd8CAl)</li>

<li id="bon06">Bonanno, G. A., Galea, S., Bucciarelli, A. &amp; Vlahov, D. (2006). Psychological resilience after disaster: New York city in the aftermath of the September 11th Terrorist Attack. <em>Psychological Science, 17</em>(3), 181-186. </li>

<li id="bor15">Borowiec, S. (2015, April 15). Victims of South Korea’s Sewol ferry disaster remembered pne year on. <em>Time</em>. Retrieved from http://time.com/3824576/south-korea-sewol-anniversary-paengmok/ </li>

<li id="bro12">Broersma, M.  &amp; Graham, T. (2012). Social media as beat: Tweets as a news source during the 2010 British and Dutch elections. <em>Journalism Practice, 6</em>(3), 403-319. </li>

<li id="bro13">Broersma, M.  &amp; Graham, T. (2013). Twitter as a news source: how Dutch and British newspapers used tweets in their news coverage, <em>Journalism practice, 7</em>(4), 446-464.</li>

<li id="col04">Collins, R. (2004). Rituals of solidarity and security in the wake of terrorist attack. <em>Sociological Theory, 22</em>(1), 53-87.</li>

<li id="cut08">Cutter, S. L., Barnes, L., Berry, M., Burton, C., Evans, E., Tate, E. &amp; Webb, J. (2008). A place-based model for understanding community resilience to natural disasters. <em>Global environmental change, 18</em> (4), 598-606.</li>

<li id="duf12">Dufty, N. (2012). Using social media to build community disaster resilience. <em>Australian Journal of Emergency Management, 27</em>(1), 40-45.</li>

<li id="dur12">Durkheim, E. (2008). <em>The elementary forms of the religious life</em>. London: George Allen and Unwin</li>

<li id="fre79">Freeman, L. C. (1979).  Centrality in social networks: conceptual clarification.  <em>Social Networks, 1</em>, 215-239.</li>

<li id="god03">Godschalk, D. R. (2003). Urban hazard mitigation: creating resilient cities. <em>Natural Hazards Review, 4</em>(3), 136-143.</li>

<li id="han17a">Han, K. M., Kim, K. H., Lee, M., Lee, S. M., Ko, Y. H. &amp; Paik, J. W. (2017a). Increase in the prescription rate of antidepressants after the Sewol Ferry disaster in Ansan, South Korea. <em>Journal of Affective Disorders</em>, 219, 31-36.</li> 

<li id="han17b">Han, J., Lee, S. &amp; McCombs, M. (2017b). The attribute agenda-setting influence of online community on online newscast: investigating the South Korean Sewol ferry tragedy. <em>Asian Journal of Communication, 27</em>(6), 601-615.</li>

<li id="haw11">Hawdon, J. &amp; Ryan, J. (2011). Social relations that generate and sustain solidarity after a mass tragedy. <em>Social Gorces, 89</em>(4), 1363-1384.</li>

<li id="hou15">Houston, J. B., Hawthorne, J., Perreault, M. F., Park, E. H., Goldstein Hode, M., Halliwell, M. R. &amp; Griffith, S. A. (2015). Social media and disasters: a functional framework for social media use in disaster planning, response, and research. <em>Disasters, 39</em>(1), 1-22.</li>

<li id="juy15">Ju, Y. &amp; Yeon, J. (2015). <a href="http://www.webcitation.org/72B0osa1o">Linguistic representation of Sewol Ferry Disaster: a corpus-based analysis of headlines from two Korean newspapers.</a> <em>SOAS-AKS Working Papers</em>, (46), 1-12. Retrieved from https://eprints.soas.ac.uk/20815/1/SOAS-AKS%20Working%20Paper%20(final-proofread)%20(Ju%20&amp;%20Yeon).pdf (Archived by WebCite® at http://www.webcitation.org/72B0osa1o)</li> 

<li id="kim14">Kim, N. &amp; Cho, M. (2014, April 22). <a href="http://www.webcitation.org/72EDcL0NP">First sign of South Korea ferry disaster was call from a frightened boy</a>. 	<em>Reuters</em>. Retrieved from http://uk.reuters.com/article/2014/04/22/us-korea-ship-idUKBREA3F01Y20140422  (Archived by WebCite® at http://www.webcitation.org/72EDcL0NP)</li> 

<li id="kim15">Kim, S. K. (2015). The Sewol Ferry Disaster in Korea and Maritime Safety Management. <em>Ocean Development &amp; International Law, 46</em>(4), 345-358.</li>

<li id="kim16">Kim, Y. S. (2016). <a href="http://www.webcitation.org/72B1sjhgP">42 million Naver users are advantages... for Naverpay</a>. <em>Edaily</em> (in Korean). May 31, 2016. Retrieved from http://www.edaily.co.kr/news/NewsRead.edy?SCD=JE41&amp;newsid=01275926612654496&DCD=A00504&amp;OutLnkChk=Y (Archived by WebCite® at http://www.webcitation.org/72B1sjhgP)</li> 

<li id="lee18">Lee, S. M., Han, H., Jang, K. I., Huh, S., Huh, H. J., Joo, J. Y. &amp; Chae, J. H. (2018). Heart rate variability associated with posttraumatic stress disorder in victims’ families of sewol ferry disaster. <em>Psychiatry Research, 259</em>, 277-282.</li>

<li id="lin11">Lindsay, B. R. (2011). <em>Social media and disasters: current uses, future options, and policy considerations</em>. Washington, DC: Congressional Research Service.</li> 

<li id="mcf06">McFarlane, A. C. &amp; Norris, F. (2006). Definitions and concepts in disaster research. In F. Norris, S. Galea, M. Friedman &amp; P. Watson (Eds.), <em>Methods for disaster mental health research</em> (pp. 3–19). New York: Guilford Press.</li> 

<li id="mid14">Middleton, S. E., Middleton, L. &amp; Modafferi, S. (2014). Real-time crisis mapping of natural disasters using social media. <em>IEEE Intelligent Systems, 29</em>(2), 9-17.</li>

<li id="mul14">Mullen, J. (2014, April 24). <em>Ferry disaster: yellow ribbons become symbol of hope, solidarity</em>. <em>CNN</em>. Retrieved from http://edition.cnn.com/2014/04/24/world/asia/south-korea-yellow-ribbons/ (Archived by WebCite® at http://http://www.webcitation.org/72B3UlVH0)</li>

<li id="neu14">Neubaum, G., Rösner, L., Rosenthal-von der Pütten, A. M. &amp; Krämer, N. C. (2014). Psychosocial functions of social media usage in a disaster situation:  multi-methodological approach. <em>Computers in Human Behavior, 34</em>, 28-38.</li>

<li id="nor08">Norris, F. H., Stevens, S. P., Pfefferbaum, B., Wyche, K. F. &amp; Pfefferbaum, R. L. (2008). Community resilience as a metaphor, theory, set of capacities, and strategy for disaster readiness. <em>American Journal of Community Psychology, 41</em>(1-2), 127-150.</li>

<li id="par13">Parsons, T. (2013). <em>The social system</em>. New York, NY: Free Press.</li> 

<li id="pat06">Paton, D. (2006). Disaster resilience: integrating individual, community, institutional and environmental perspectives. In D. Paton &amp; D. Johnston (Eds.), <em>Disaster resilience: an integrated approach</em> (pp. 305–318). Springfield, IL: Charles C. Thomas..</li>

<li id="plo13">Plough, A., Fielding, J. E., Chandra, A., Williams, M., Eisenman, D., Wells, K. B., ... &amp; Magaña, A. (2013). Building community disaster resilience: perspectives from a large urban county department of public health. <em>American Journal of Public Health, 103</em>(7), 1190-1197.</li>

<li id="qua85">Quarantelli, E. L. (1985). What is disaster? <em>The need for clarification in definition and conceptualization in research</em>. Rockville, MD: National Institute of Mental Health.</li> 

<li id="tay12">Taylor, M., Wells, G., Howell, G. &amp; Raphael, B. (2012). The role of social media as psychological first aid as a support to community resilience building. <em>The Australian Journal of Emergency Management, 27</em>(1), 20-26.</li> 

<li id="woo15">Woo, H., Cho, Y., Shim, E., Lee, K. &amp; Song, G. (2015). Public trauma after the Sewol Ferry disaster: the role of social media in understanding the public mood. <em>International Journal of Environmental Research and Public Health, 12</em>(9), 10974-10983</li>

<li id="yoo15">Yoon, D. K., Kang, J. E. &amp; Brody, S. D. (2015). A measurement of community disaster resilience in Korea. 	<em>Journal of Environmental Planning and Management, 59</em>(3), 436-460.</li>

<li id="zha15">Zhang, S. &amp; Wang, J. (2015). Analysis of South Korea Sewol sunken ferry accident based on behavioral safety. <em>Journal of Coastal Research</em>, (Special issue 73), 611-613.</li>
</ul>

</section>

<section>

## Appendix: Frequency distribution tables

<table><caption>Table 1: Frequency Table (April 16, 2014 to May 16, 2014)</caption>

<tbody>

<tr>

<th>Ranking</th>

<th>Word</th>

<th>English translation</th>

<th>Frequency</th>

</tr>

<tr>

<td>1</td>

<td>세월</td>

<td>Sewol</td>

<td>4829</td>

</tr>

<tr>

<td>2</td>

<td>침몰</td>

<td>Sink</td>

<td>900</td>

</tr>

<tr>

<td>3</td>

<td>참사</td>

<td>Disaster</td>

<td>612</td>

</tr>

<tr>

<td>4</td>

<td>사고</td>

<td>Nature of event</td>

<td>484</td>

</tr>

<tr>

<td>5</td>

<td>희생자</td>

<td>Casualty</td>

<td>428</td>

</tr>

<tr>

<td>6</td>

<td>사건</td>

<td>Incident</td>

<td>410</td>

</tr>

<tr>

<td>7</td>

<td>세월호</td>

<td>Sewol ferry</td>

<td>294</td>

</tr>

<tr>

<td>8</td>

<td>여객선</td>

<td>Ferry</td>

<td>249</td>

</tr>

<tr>

<td>9</td>

<td>선장</td>

<td>Captain</td>

<td>242</td>

</tr>

<tr>

<td>10</td>

<td>이번</td>

<td>This time</td>

<td>206</td>

</tr>

<tr>

<td>11</td>

<td>관련</td>

<td>Related</td>

<td>204</td>

</tr>

<tr>

<td>12</td>

<td>실종자</td>

<td>Missing people</td>

<td>155</td>

</tr>

<tr>

<td>13</td>

<td>마음</td>

<td>Heart</td>

<td>154</td>

</tr>

<tr>

<td>14</td>

<td>추모</td>

<td>Memorial</td>

<td>154</td>

</tr>

<tr>

<td>15</td>

<td>구조</td>

<td>Rescue</td>

<td>144</td>

</tr>

<tr>

<td>16</td>

<td>사람</td>

<td>Person</td>

<td>139</td>

</tr>

<tr>

<td>17</td>

<td>국민</td>

<td>Citizens</td>

<td>138</td>

</tr>

<tr>

<td>18</td>

<td>침몰사</td>

<td>Sinking accident</td>

<td>130</td>

</tr>

<tr>

<td>19</td>

<td>4월</td>

<td>April</td>

<td>128</td>

</tr>

<tr>

<td>20</td>

<td>배</td>

<td>Ship</td>

<td>127</td>

</tr>

<tr>

<td>21</td>

<td>학생</td>

<td>Student</td>

<td>125</td>

</tr>

<tr>

<td>22</td>

<td>가족</td>

<td>Family</td>

<td>125</td>

</tr>

<tr>

<td>23</td>

<td>뉴스</td>

<td>News</td>

<td>124</td>

</tr>

<tr>

<td>24</td>

<td>진도</td>

<td>Jin-do</td>

<td>118</td>

</tr>

<tr>

<td>25</td>

<td>생각</td>

<td>Think</td>

<td>118</td>

</tr>

<tr>

<td>26</td>

<td>아이</td>

<td>Child</td>

<td>117</td>

</tr>

<tr>

<td>27</td>

<td>일</td>

<td>Work</td>

<td>115</td>

</tr>

<tr>

<td>28</td>

<td>애도</td>

<td>Condolences</td>

<td>114</td>

</tr>

<tr>

<td>29</td>

<td>오늘</td>

<td>Today</td>

<td>111</td>

</tr>

<tr>

<td>30</td>

<td>말</td>

<td>Words</td>

<td>108</td>

</tr>

<tr>

<td>31</td>

<td>정부</td>

<td>Government</td>

<td>108</td>

</tr>

<tr>

<td>32</td>

<td>16</td>

<td>16</td>

<td>103</td>

</tr>

<tr>

<td>33</td>

<td>대한민국</td>

<td>South Korea</td>

<td>101</td>

</tr>

<tr>

<td>34</td>

<td>명복</td>

<td>People's wish</td>

<td>98</td>

</tr>

<tr>

<td>35</td>

<td>소식</td>

<td>News</td>

<td>98</td>

</tr>

<tr>

<td>36</td>

<td>유가족</td>

<td>Bereaved family</td>

<td>80</td>

</tr>

<tr>

<td>37</td>

<td>기적</td>

<td>Miracle</td>

<td>79</td>

</tr>

<tr>

<td>38</td>

<td>글</td>

<td>Writing</td>

<td>79</td>

</tr>

<tr>

<td>39</td>

<td>이야기</td>

<td>Story</td>

<td>78</td>

</tr>

<tr>

<td>40</td>

<td>전</td>

<td>Past</td>

<td>74</td>

</tr>

<tr>

<td>41</td>

<td>합동분향소</td>

<td>Group memorial altar</td>

<td>73</td>

</tr>

<tr>

<td>42</td>

<td>가슴</td>

<td>Heart</td>

<td>72</td>

</tr>

<tr>

<td>43</td>

<td>분향</td>

<td>Burning of incense</td>

<td>70</td>

</tr>

<tr>

<td>44</td>

<td>시간</td>

<td>Time</td>

<td>70</td>

</tr>

<tr>

<td>45</td>

<td>현재</td>

<td>Present</td>

<td>70</td>

</tr>

<tr>

<td>46</td>

<td>슬픔</td>

<td>Sadness</td>

<td>69</td>

</tr>

<tr>

<td>47</td>

<td>생존자</td>

<td>Survivor</td>

<td>67</td>

</tr>

<tr>

<td>48</td>

<td>발생</td>

<td>Occur</td>

<td>66</td>

</tr>

<tr>

<td>49</td>

<td>단원</td>

<td>Danwon</td>

<td>66</td>

</tr>

<tr>

<td>50</td>

<td>노란리본</td>

<td>Yellow ribbon</td>

<td>66</td>

</tr>

<tr>

<td>51</td>

<td>기부</td>

<td>Donation</td>

<td>64</td>

</tr>

<tr>

<td>52</td>

<td>침몰사고</td>

<td>Sinking accident</td>

<td>64</td>

</tr>

<tr>

<td>53</td>

<td>승객</td>

<td>Passenger</td>

<td>63</td>

</tr>

<tr>

<td>54</td>

<td>희망</td>

<td>Hope</td>

<td>60</td>

</tr>

</tbody>

</table>

<table><caption>Table 2: Frequency Table (October 1, 2014 to October 31, 2014)</caption>

<tbody>

<tr>

<th>Ranking</th>

<th>Word</th>

<th>English translation</th>

<th>Frequency</th>

</tr>

<tr>

<td>1</td>

<td>세월</td>

<td>Sewol</td>

<td>3623</td>

</tr>

<tr>

<td>2</td>

<td>참사</td>

<td>Disaster</td>

<td>506</td>

</tr>

<tr>

<td>3</td>

<td>유가족</td>

<td>Bereaved family</td>

<td>550</td>

</tr>

<tr>

<td>4</td>

<td>특별법</td>

<td>Special law</td>

<td>285</td>

</tr>

<tr>

<td>5</td>

<td>사건</td>

<td>Incident</td>

<td>253</td>

</tr>

<tr>

<td>6</td>

<td>실종자</td>

<td>Missing people</td>

<td>252</td>

</tr>

<tr>

<td>7</td>

<td>사고</td>

<td>Nature of event</td>

<td>225</td>

</tr>

<tr>

<td>8</td>

<td>선장</td>

<td>Captain</td>

<td>205</td>

</tr>

<tr>

<td>9</td>

<td>침몰</td>

<td>Sink</td>

<td>175</td>

</tr>

<tr>

<td>10</td>

<td>세월호</td>

<td>Sewol ferry</td>

<td>174</td>

</tr>

<tr>

<td>11</td>

<td>시신</td>

<td>Dead body</td>

<td>163</td>

</tr>

<tr>

<td>12</td>

<td>사람</td>

<td>Person</td>

<td>132</td>

</tr>

<tr>

<td>13</td>

<td>발견</td>

<td>Discover</td>

<td>124</td>

</tr>

<tr>

<td>14</td>

<td>사형</td>

<td>Capital punishment</td>

<td>123</td>

</tr>

<tr>

<td>15</td>

<td>가족</td>

<td>Family</td>

<td>118</td>

</tr>

<tr>

<td>16</td>

<td>희생자</td>

<td>Casualty</td>

<td>115</td>

</tr>

<tr>

<td>17</td>

<td>이준석</td>

<td>Joon-Seok Lee</td>

<td>112</td>

</tr>

<tr>

<td>18</td>

<td>생각</td>

<td>Think</td>

<td>111</td>

</tr>

<tr>

<td>19</td>

<td>오늘</td>

<td>Today</td>

<td>111</td>

</tr>

<tr>

<td>20</td>

<td>이후</td>

<td>After</td>

<td>107</td>

</tr>

<tr>

<td>21</td>

<td>관련</td>

<td>Related</td>

<td>104</td>

</tr>

<tr>

<td>22</td>

<td>국민</td>

<td>Citizens</td>

<td>100</td>

</tr>

<tr>

<td>23</td>

<td>말</td>

<td>Words</td>

<td>89</td>

</tr>

<tr>

<td>24</td>

<td>진상규명</td>

<td>Truth Ascertainment</td>

<td>89</td>

</tr>

<tr>

<td>25</td>

<td>10월</td>

<td>October</td>

<td>87</td>

</tr>

<tr>

<td>26</td>

<td>아이</td>

<td>Child</td>

<td>85</td>

</tr>

<tr>

<td>27</td>

<td>구형</td>

<td>Demand</td>

<td>83</td>

</tr>

<tr>

<td>28</td>

<td>제정</td>

<td>Enact</td>

<td>83</td>

</tr>

<tr>

<td>29</td>

<td>인양</td>

<td>Salvage</td>

<td>81</td>

</tr>

<tr>

<td>30</td>

<td>4월</td>

<td>April</td>

<td>79</td>

</tr>

<tr>

<td>31</td>

<td>유족</td>

<td>Bereaved family</td>

<td>78</td>

</tr>

<tr>

<td>32</td>

<td>일</td>

<td>Work</td>

<td>77</td>

</tr>

<tr>

<td>33</td>

<td>마음</td>

<td>Heart</td>

<td>77</td>

</tr>

<tr>

<td>34</td>

<td>진실</td>

<td>Truth</td>

<td>75</td>

</tr>

<tr>

<td>35</td>

<td>전</td>

<td>Before</td>

<td>75</td>

</tr>

<tr>

<td>36</td>

<td>광화문</td>

<td>Gwanghwamun</td>

<td>74</td>

</tr>

<tr>

<td>37</td>

<td>2014</td>

<td>2014</td>

<td>74</td>

</tr>

<tr>

<td>38</td>

<td>대통령</td>

<td>President</td>

<td>71</td>

</tr>

<tr>

<td>39</td>

<td>합의</td>

<td>Agree</td>

<td>70</td>

</tr>

<tr>

<td>40</td>

<td>102일</td>

<td>102 days</td>

<td>69</td>

</tr>

<tr>

<td>41</td>

<td>6개월</td>

<td>Six months</td>

<td>69</td>

</tr>

<tr>

<td>42</td>

<td>단원</td>

<td>Danwon</td>

<td>68</td>

</tr>

<tr>

<td>43</td>

<td>세월호특별법</td>

<td>Sewol ferry special law</td>

<td>68</td>

</tr>

<tr>

<td>44</td>

<td>세월호법</td>

<td>Sewol ferry law</td>

<td>68</td>

</tr>

<tr>

<td>45</td>

<td>여야</td>

<td>ruling and the opposition parties</td>

<td>67</td>

</tr>

<tr>

<td>46</td>

<td>정부</td>

<td>Government</td>

<td>66</td>

</tr>

<tr>

<td>47</td>

<td>수습</td>

<td>Collect</td>

<td>65</td>

</tr>

<tr>

<td>48</td>

<td>앞</td>

<td>Front</td>

<td>64</td>

</tr>

<tr>

<td>49</td>

<td>이야기</td>

<td>Story</td>

<td>64</td>

</tr>

<tr>

<td>50</td>

<td>시간</td>

<td>Time</td>

<td>63</td>

</tr>

<tr>

<td>51</td>

<td>타결</td>

<td>Settlement</td>

<td>60</td>

</tr>

<tr>

<td>52</td>

<td>이번</td>

<td>This time</td>

<td>60</td>

</tr>

</tbody>

</table>

<table><caption>Table 3: Frequency Table (March 16, 2015 to April 16, 2015)</caption>

<tbody>

<tr>

<th>Ranking</th>

<th>Word</th>

<th>English translation</th>

<th>Frequency</th>

</tr>

<tr>

<td>1</td>

<td>세월</td>

<td>Sewol</td>

<td>4814</td>

</tr>

<tr>

<td>2</td>

<td>1</td>

<td>1</td>

<td>952</td>

</tr>

<tr>

<td>3</td>

<td>참사</td>

<td>Disaster</td>

<td>857</td>

</tr>

<tr>

<td>4</td>

<td>오늘</td>

<td>Today</td>

<td>684</td>

</tr>

<tr>

<td>5</td>

<td>1주</td>

<td>1st anniversary</td>

<td>544</td>

</tr>

<tr>

<td>6</td>

<td>추모</td>

<td>Memorial</td>

<td>459</td>

</tr>

<tr>

<td>7</td>

<td>사건</td>

<td>Incident</td>

<td>398</td>

</tr>

<tr>

<td>8</td>

<td>1년</td>

<td>One year</td>

<td>349</td>

</tr>

<tr>

<td>9</td>

<td>4월</td>

<td>April</td>

<td>280</td>

</tr>

<tr>

<td>10</td>

<td>1주기</td>

<td>1st anniversary</td>

<td>270</td>

</tr>

<tr>

<td>11</td>

<td>세월호</td>

<td>Sewol ferry</td>

<td>257</td>

</tr>

<tr>

<td>12</td>

<td>사고</td>

<td>Nature of event</td>

<td>246</td>

</tr>

<tr>

<td>13</td>

<td>기억</td>

<td>Remember</td>

<td>217</td>

</tr>

<tr>

<td>14</td>

<td>날</td>

<td>Day</td>

<td>216</td>

</tr>

<tr>

<td>15</td>

<td>희생자</td>

<td>Casualty</td>

<td>216</td>

</tr>

<tr>

<td>16</td>

<td>16</td>

<td>16</td>

<td>212</td>

</tr>

<tr>

<td>17</td>

<td>침몰</td>

<td>Sink</td>

<td>211</td>

</tr>

<tr>

<td>18</td>

<td>1주</td>

<td>1st anniversary</td>

<td>205</td>

</tr>

<tr>

<td>19</td>

<td>인양</td>

<td>Salvage</td>

<td>203</td>

</tr>

<tr>

<td>20</td>

<td>생각</td>

<td>Think</td>

<td>169</td>

</tr>

<tr>

<td>21</td>

<td>사람</td>

<td>Person</td>

<td>169</td>

</tr>

<tr>

<td>22</td>

<td>유가족</td>

<td>Bereaved family</td>

<td>162</td>

</tr>

<tr>

<td>23</td>

<td>마음</td>

<td>Heart</td>

<td>161</td>

</tr>

<tr>

<td>24</td>

<td>일</td>

<td>Work</td>

<td>140</td>

</tr>

<tr>

<td>25</td>

<td>시간</td>

<td>Time</td>

<td>140</td>

</tr>

<tr>

<td>26</td>

<td>아이</td>

<td>Child</td>

<td>133</td>

</tr>

<tr>

<td>27</td>

<td>작년</td>

<td>Last year</td>

<td>126</td>

</tr>

<tr>

<td>28</td>

<td>학생</td>

<td>Student</td>

<td>113</td>

</tr>

<tr>

<td>29</td>

<td>2014</td>

<td>2014</td>

<td>108</td>

</tr>

<tr>

<td>30</td>

<td>명복</td>

<td>People's wish</td>

<td>105</td>

</tr>

<tr>

<td>31</td>

<td>말</td>

<td>Words</td>

<td>101</td>

</tr>

<tr>

<td>32</td>

<td>가족</td>

<td>Family</td>

<td>98</td>

</tr>

<tr>

<td>33</td>

<td>전</td>

<td>Before</td>

<td>97</td>

</tr>

<tr>

<td>34</td>

<td>국민</td>

<td>Citizens</td>

<td>87</td>

</tr>

<tr>

<td>35</td>

<td>학교</td>

<td>School</td>

<td>85</td>

</tr>

<tr>

<td>36</td>

<td>정부</td>

<td>Government</td>

<td>84</td>

</tr>

<tr>

<td>37</td>

<td>관련</td>

<td>Related</td>

<td>84</td>

</tr>

<tr>

<td>38</td>

<td>단원</td>

<td>Danwon</td>

<td>82</td>

</tr>

<tr>

<td>39</td>

<td>뉴스</td>

<td>News</td>

<td>79</td>

</tr>

<tr>

<td>40</td>

<td>배</td>

<td>Ship</td>

<td>78</td>

</tr>

<tr>

<td>41</td>

<td>잊지않겠습니</td>

<td>Never forget</td>

<td>77</td>

</tr>

<tr>

<td>42</td>

<td>글</td>

<td>Writing</td>

<td>74</td>

</tr>

<tr>

<td>43</td>

<td>노란리본</td>

<td>Yellow ribbon</td>

<td>74</td>

</tr>

<tr>

<td>44</td>

<td>세월호사건</td>

<td>Sewol ferry accident</td>

<td>74</td>

</tr>

<tr>

<td>45</td>

<td>세월호참사</td>

<td>Sewol ferry disaster</td>

<td>73</td>

</tr>

<tr>

<td>46</td>

<td>때</td>

<td>The moment</td>

<td>72</td>

</tr>

<tr>

<td>47</td>

<td>가슴</td>

<td>Heart</td>

<td>71</td>

</tr>

<tr>

<td>48</td>

<td>리본</td>

<td>Ribbon</td>

<td>70</td>

</tr>

<tr>

<td>49</td>

<td>진실</td>

<td>Truth</td>

<td>66</td>

</tr>

<tr>

<td>50</td>

<td>대한민국</td>

<td>South Korea</td>

<td>63</td>

</tr>

<tr>

<td>51</td>

<td>하늘</td>

<td>Sky</td>

<td>63</td>

</tr>

<tr>

<td>52</td>

<td>앞</td>

<td>Front</td>

<td>61</td>

</tr>

<tr>

<td>53</td>

<td>이야기</td>

<td>Story</td>

<td>60</td>

</tr>

</tbody>

</table>

</section>

</article>