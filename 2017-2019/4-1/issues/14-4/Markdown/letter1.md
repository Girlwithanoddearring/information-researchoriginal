#### vol. 14 no. 4, December, 2009

* * *

# Letter to the Editor

## The publication activity of Region Västra Götaland: a critical comment.

#### [Peder Olesen Larsen](#author)  
Marievej 10A,  
DK-2900 Hellerup,  
Denmark

The publication activity in a Swedish region 1998-2006 has been analysed in a recent publication ([Jarneving 2009](#jar09)). [The article](http://InformationR.net/ir/14-2/paper397.html) provides a clear description of the methods used. However, whole counting has been used for publications and citations without due regard to the properties of this method. The publication also contains additional smaller methodological problems. Therefore, some of the results and conclusions in the publications are incorrect. This criticism is based on the following analysis.

## Counting schemes

Under the heading _Counting Schemes_ it is stated that there are three standard counting methods.

1.  first (author) counting,
2.  fractional counting, and
3.  whole counting

with reference to Gauffriau _et al._ ([2007](#gau07)). However, this is not in agreement with the results described in this reference. There are five, not three fundamental counting methods. In addition for each method the object of study and the basic unit of analysis must be stated. Both can for example be persons, institutions, regions and countries.

Jarneving adds: '_Fractional counting implies a difficulty as there is no clear correspondence between authors and corporate addresses_'. This is correct but is only relevant if fractionation based on author numbers is considered. Fractional counting based on numbers of institutions or countries presents no difficulties.

Whole counting is the method used in the publication. Both the objects of study and the basic units of analysis are institutions, regions and countries (all the basic units are derived from the addresses in the publications). The choice of whole counting is supported by a reference to the European Commission ([2003](#eur03)). However, OECD from [2001](#oec01) has been using fractional counting in its R&D statistics. The OECD statistics depend on data from National Science Foundation ([2000](#nsf00)), also based on fractional counting.

It is stated, '_Furthermore, empirical experiences have shown that at the macro level, all three methods may yield satisfactory results, but a lower levels of aggregation, the whole counting scheme is more appropriate_', with reference to Glänzel ([1996](#gla96)). This is a distortion of the reference. The reference contains no empirical evidence whatsoever and, on counting methods, states: '_Whilst at the macro-level all three methods may yield satisfactory results, at lower levels of aggregation first-address counts proved to be inappropriate and especially at the micro-level full [whole] counts should be preferred to fractional counts_'. The choice of counting methods must be based on the questions addressed and different methods can provide widely different results (Gauffriau _et al._ [2008](#gau08)).

Of special importance is the fact that results obtained by whole counting are non-additive. This fact was shown already by Anderson _et al._ in [1988](#and88) (compare Gauffriau _et al._ [2007](#gau07)). This means that the counting numbers for the institutions in a region cannot be added to provide the value for the region and that the counting numbers for the regions in a country cannot be added to provide the value for the country. Because of multi authorship and cooperation between institutions and regions the sums will always be larger than the values for the corresponding regions or countries.

## The activity index

The activity index defined by Jarneving (the country's share in world's publication output in the given field divided by the country's share in world's publication output in all science fields) is problematic with the counting method chosen. Only if the region studied has exactly the same extent of cooperation with scientists outside the region in all fields will the values be meaningful. However, in the section _Research Collaboration_ this is shown not to be the case.

## Publication numbers and growth rates

In the section Data sources and data processing 24,823 records from 1998-2006 are given for the region studied from 1998-2008 whereas the number for Sweden is 131,544 records.

In the section _The regional article production_ it is stated that during the observation there was a linear growth for both the region and the nation. This statement is surprising and not documented. Exponential growth rate might be expected as already shown is Price's pioneering work ([1961](#pri61), [1963](#pri63)).

## The regional producers

In the section The regional article production it is stated that the region contributes approximately 19% yearly (to Swedish scientific publication) (24,823 divided by 131,823 and multiplied by 100). However, with the counting method used the sum for all Swedish regions must exceed 100%. On the other hand it would be correct to state that the region contributed to or participated in 19% of all scientific publications with Swedish participation.

Figure 1 records the national shares in 11 broad fields and for the national total. It is written that 'The shares are based on the cumulated number of articles. Multiple counting was applied for the different broad fields while a single count was applied when computing shares of the national total'. This must mean that if a paper was counted in two fields it was only counted once for the total. In any event, again, the shares for the Swedish regions counted with the method used must add up to more than 100%.

Figure 1 shows that the share for the Region Västra Götaland of the national total is increasing slightly during the period studied. This may be caused only by increased cooperation with other regions and countries. In fact the counting method used makes it possible for all regions to increase their shares even if the total number of publications is constant ([Anderson _et al._ 1988](#and88)).

In Table 1 the percentages of production in different fields for the different institutions in the region are recorded. The problem about non-additivity is clearly displayed by the values for Physics and Astronomy and for Mathematics and Statistics. In Physics and Astronomy two institutions are indicated to have percentages of 85 and 39%, together 114%. For Mathematics and Statistics the values are 82 and 35%, together 117%. The values are used for rankings. Again, it will be possible for all institutions to increase their shares over time by increasing cooperation, even if the total output of publications is constant.

Support from the Carlsberg Foundation is gratefully acknowledged.

## References

*   <a id="and88"></a>Anderson, J., Collins, P.M.D., Irvine, J., Isard, P.A., Martin, B.R., Narin, F. & Stevens, K. (1988). On-line approaches to measuring national scientific output: a cautionary tale. _Science and Public Policy_ **15**(3), 153-161.
*   <a id="eur03"></a>European Commission. _Directorate-General for Research_ (2003). __Third European report on science & technology indicators 2003\. Towards a knowledge-based economy. Bibliometric analysis: methodological annex to chapter 5__. Brussels: European Commission. Retrieved 4 November, 2009 from ftp://ftp.cordis.europa.eu/pub/indicators/docs/3rd_report.pdf
*   <a id="gau07"></a>Gauffriau, M., Larsen, P.O., Maye, I., Roulin-Perriard, A. & von Ins, M. (2007). Publication, cooperation and productivity measures in scientific research. _Scientometrics_ **73**(2),175-214\.
*   <a id="gau08"></a>Gauffriau, M., Larsen, P.O., Maye, I., Roulin-Perriard, A. & von Ins, M. (2008). Comparisons of results of publication counting methods using different methods. _Scientometrics_ **77**(1), 147-176.
*   <a id="gla96"></a>Glänzel, W. (1996). The need for standards in bibliometric research and technology. _Scientometrics_, **35**(2), 167-276.
*   <a id="jar09"></a>Jarneving, B. (2009). [The publication activity of Region Västra Götaland: a bibliometric study of an administrative and political Swedish region during the period 1998-2006.](http://InformationR.net/ir/14-2/paper397.html) _Information Research_, **14**(2), paper 397\. Retrieved 4 November, 2009 from http://InformationR.net/ir/14-2/paper397.html (Archived by WebCite® at http://www.webcitation.org/5l2gD3Y7o)
*   <a id="nsf00"></a>National Science Foundation (2000). _[Science and engineering indicators 2000.](http://www.webcitation.org/5l2iqRcXp)_ Washington, DC: National Science Foundation. Retrieved 4 November, 2009 from http://www.nsf.gov/statistics/seind00/ (Archived by WebCite® at http://www.webcitation.org/5l2iqRcXp)
*   <a id="oec01"></a>Organisation for Economic Co-Operation and Development. (2001). _Science, technology and industry scoreboard. Towards a knowledge-based economy._ Paris: OECD.
*   <a id="pri61"></a>Price, D.J.de Solla (1961). _Science since Babylon_. New Haven, CT: Yale University Press.
*   <a id="pri63"></a>Price, D.J.de Solla (1963). _Little science, big science._ New York, NY: Columbia University Press.