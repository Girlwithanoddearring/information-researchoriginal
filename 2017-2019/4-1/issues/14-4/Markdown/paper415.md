#### vol. 14 no. 4, December, 2009

* * *

# The situation of open access institutional repositories in Spain: 2009 report

#### [Remedios Melero](#authors)  
Instituto de Agroquímica y Tecnología de Alimentos,  
CSIC, PO Box 73 46100 Burjasot,  
Valencia,  
Spain

#### [Ernest Abadal](#authors)  
Departament de Biblioteconomia i Documentació,  
Universitat de Barcelona,  
Melcior de Palau 140,  
08014 Barcelona,  
Spain

#### [Francisca Abad](#authors)  
Departamento de Historia de la Ciencia y Documentación,  
Facultad de Medicina,  
Blasco Ibáñez 15,  
4610 Valencia,  
Spain

#### [Josep Manel Rodríguez-Gairín](#authors)  
Departament de Biblioteconomia i Documentació,  
Universitat de Barcelona,  
Melcior de Palau 140,  
08014 Barcelona,  
Spain

#### Abstract

> **Introduction.** The DRIVER I project drew up a detailed report of European repositories based on data gathered in a survey in which Spain's participation was very low. This created a highly distorted image of the implementation of repositories in Spain. This study aims to analyse the current state of Spanish open-access institutional repositories and to describe their characteristics.  
> **Method.** The data were gathered through a Web survey. The questionnaire was based on that used by DRIVER I: coverage; technical infrastructure and technical issues; institutional policies; services created; and stimulators and inhibitors for establishing, filling and maintaining their digital institutional repositories.  
> **Analysis.** Data were tabulated and analysed systematically according responses obtained from the questionnaire and grouped by coverage.  
> **Results.** Responses were obtained from 38 of the 104 institutions contacted, which had 29 institutional repositories. This represents 78.3% of the Spanish repositories according to the _BuscaRepositorios_ directory. Spanish repositories contained mainly full-text materials (journal articles and doctoral theses) together with metadata. The software most used was DSpace, followed by EPrints. The metadata standard most used was Dublin Core. Spanish repositories offered more usage statistics and fewer author-oriented services than the European average. The priorities for the future development of the repositories are the need for clear policies on access to scientific production based on public funding and the need for quality control indicators.  
> **Conclusions.** This is the first detailed study of Spanish institutional repositories. The key stimulants for establishing, filling and maintaining were, in order of importance, the increase of visibility and citation, the interest of decision-makers, simplicity of use and search services. On the other hand the main inhibitors identified were the absence of policies, the lack of integration with other national and international systems and the lack of awareness efforts among academia.

## Introduction

In recent years institutional repositories world-wide have grown in number and in the volume of materials deposited (Figure 1). This has also been the case in Spain. According to the [_BuscaRepositorios_](http://www.webcitation.org/5k01Jx5p0) directory, since the first Spanish repository of doctoral theses ([Tesis Doctorals en Xarxa](http://www.tesisenxarxa.net/) - known as TDX) was created in 2001, forty-four more repositories have been implemented and most of them are only two or three years old.

<figure>

![Figure 1\. Growth of Spanish open-access repositories in comparison with the whole world (source: OpenDOAR)](../p415fig1.jpg)

<figcaption>

**Figure 1\. Growth of Spanish open-access repositories in comparison with the whole world (source: OpenDOAR)**</figcaption>

</figure>

The number of digital objects deposited has also shown a large increase as a result of the greater visibility offered by repositories, the guarantee of data preservation and institutional policies that require or encourage self-archiving. The libraries or documentation services of academic and research institutions have made a major contribution to this growth in digital objects by digitizing documents through mediated archiving or through capturing, gathering and extracting files published on the Internet ([Association of Research Libraries 2009](#arl-2009)). This contrasts, however, with the still low level of participation by scientists and authors depositing their own works ([Foster and Gibbons 2005](#Foster-and-Gibbons-2005); [Davis and Connolly 2007](#davis-and-connolly-2007)). According to [DRIVER](#driver) ([van Eijndhoven and van der Graaf 2007](#van-eijndhoven-and-van-der-graaf-2007)), the European average of self-archived items was only 38% in 2005 (although the response rate was low).

Few studies have been made on the situation of institutional repositories in a given country or geographic area, mainly because the low response rate of survey-based studies makes it difficult to obtain data. In recent years work has been done in the United States to identify and characterize American institutional repositories ([Lynch 2003](#lynch-2003); [Lynch and Lippincott 2005](#lynch-and-lippincott-2005); [Bailey 2006](#bailey-2006); [Markey _et al_ 2007](#markey-2007); [McDowell 2007](#mcdowell-2007)). In a report on institutional repositories based on a survey of American academic library directors (Markey et al. 2007), with a response rate of 20.8%, only 10.8% of respondents had implemented an institutional repository and 52.9% had no plans to do so. The survey showed that, as in Spain, most institutions involved in the development of repositories were universities. Rather than establishing recommendations or requirements, the institutions' policies tended to refer to the materials that could be deposited and the authorization to do so.

A smaller survey was carried out among the participants at the SPARC Digital Repositories Meeting held in Baltimore on 17-18 November 2008 ([Bankier 2008](#bankier-2008)). The questions referred to current trends and perceptions of repositories, including usage for disseminating non-academic materials such as newsletters and student reports. The most important success factors were value-added services such as mediated deposits, copyright checking and personal publication pages.

A detailed study of open access in the Nordic countries (Sweden, Denmark, Norway, Finland and Iceland) identified the institutional repositories and open-access publications in each country ([Hedlund and Rabow 2007](#hed07)). The results showed that institutional repositories contained predominantly doctoral theses, articles and conference papers.

Recently, ([Kennan and Kingsley 2009](#Ken09)) published a report on the current situation of repositories in Australia. They obtained a 97.4% response rate corresponding to thirty-seven of the thirty-eight universities. Of these, thirty-two (84.2%) already had a repository, so their level of implementation at a national level was very high. The authors also stressed the great increase observed since 2006 in the number of digital objects deposited. Depositing research output was mandatory in five universities and implementation of a mandate was being planned in a further eight.

In Europe, under the auspices of the European Union's [DRIVER I project](http://www.driver-repository.eu/) of the Sixth Framework Programme, a study was carried out to determine the situation of institutional repositories in Europe ([van Eijndhoven and van der Graaf 2007](#van-eijndhoven-and-van-der-graaf-2007)). The study was based on a Web survey sent to 230 European institutions with one or more repositories. The data were gathered between June 2006 and February 2007 and the participation by institutions was approximately 50%. Of the twenty-seven current members of the Union, only fifteen had implemented an appreciable number of repositories, whereas the rest had either few or none. In Spain, only three of the twelve repositories existing at that time responded to the survey.

In Spain, the number, type and patterns of repositories have been closely monitored in the last three years ([Melero 2006](#melero-2006), [2007](#melero-2007), [2008](#melero-2008); [Melero _et al_ 2008](#melero-et-al-2008)). The high rate of growth observed is related to the adhesion of institutions to the Berlin Declaration ([2003](#berlin)) and the offer of public grants to fund digitizing and archiving of materials. The expansion of the open access movement has led to blogs and discussion lists and the subject has been dealt with at many national and international events held in Spain. Furthermore, in DRIVER II Spain acts as a tester of the infrastructure for European institutional repositories that meet its guidelines ([DRIVER 2008](#driver-2008)).

## Objectives

The DRIVER I project drew up a detailed report of European repositories based on data gathered in a survey in which Spain's participation was very low. Of the twelve institutional repositories registered in the [Directory of Open Access Repositories](http://www.webcitation.org/5k03TpcaX) (OpenDOAR) in the sample period (June 2006 to February 2007), only three responded. This created a highly distorted image of the implementation of repositories in Spain. To remedy this situation, it was decided to carry out a detailed national study aiming for a high response rate offering comparable data to those obtained from the DRIVER I project. Following the model of this project, the present report wishes to show the current situation of repositories created by Spanish institutions and to fill the gap left by earlier studies.

## <a id="method"></a>Method

The data were gathered through a Web survey sent to a total of 104 institutions, including academic, research and cultural organizations. The link was communicated by e-mail to directors of libraries belonging to the Network of University Libraries (REBIUN), directors of information and documentation services of research centres and directors of national and regional governmental institutions. The first messages were sent in July 2008\. Reminders were sent by e-mail or telephone and the gathering of data concluded in November 2008.

The survey contained the same questions as those used in the DRIVER I study, translated into Spanish. Two changes were made in the response option; the list of service providers linking to the repositories and the system for scoring and choosing answers in the section on stimulants and inhibitors for maintaining digital repositories.

The questionnaire was divided into six sections:

1.  Information on the documents deposited in the repositories.
2.  Technical infrastructure and technical issues.
3.  Institutional policies regarding the digital repositories.
4.  Services created on top of the digital repositories.
5.  Stimulants and inhibitors for establishing, filling and maintaining repositories.
6.  The institution and its digital repository.

Although the questionnaire was based on the assumption that the respondents already had a repository, an alternative form was provided for institutions that had not yet set one up, allowing them to state whether they had plans to do so in the future.

The final stage of the study included a comparison of the results obtained in this study and the results of the DRIVER I study published in 2007.

## Results

### Response and participation rates of Spanish repositories

Of the total of 104 questionnaires sent, twenty-five responses were received from institutions that already had a repository and thirteen from institutions that did not. This represented a response rate of 36.5%, this low-medium response rate is explained by the wide range of type of institutions we addressed, some of which were approached with the aim of promoting the idea of repositories, rather than in expectation of obtaining data.

If the institution had more than one repository, it filled in a single survey summarizing the overall situation. In fact, the twenty-five responses referred to twenty-nine repositories because the Consortium of Academic Libraries of Catalonia (CBUC) responded for three repositories (TDX, [RECERCAT](http://www.recercat.net/) and [RACO](http://www.raco.cat/) - Revistas Catalanes amb Acces Obert) and the Universitat Politècnica de Catalunya responded for four ([E-prints UPC](http://upcommons.upc.edu/e-prints/), [Revistes i Congressos UPC](https://upcommons.upc.edu/revistes/), [Treballs acadèmics UPC](http://upcommons.upc.edu/pfc/) and [Videoteca Digital UPC](http://upcommons.upc.edu/video/)) under the name UPCommons. One respondent was excluded from the results because their repository is included in the CBUC.

This report therefore has the figures and characteristics of twenty-nine repositories, representing 65.9%, 78.3% or 93.5% of the Spanish repositories according to the source used: the [Registry of Open Access Repositories](http://www.webcitation.org/5k03iRQ85) (ROAR), BuscaRepositorios or OpenDoar, at the time of this study: 44, 31, 37, respectively). These considerable differences in the census of Spanish repositories have already been reported in an earlier study ([Melero _et al_ 2008](#melero-et-al-2008)) and are due to some duplications and the inclusion of journals in the OpenDOAR and ROAR databases. At present the most reliable census is undoubtedly BuscaRepositorios.

### Types of institution participating in the study

Grouped by sectors, the respondents included twenty universities, the Consorci de Biblioteques Universitaries de Catalunya (Consortium of Catalonian University Libraries), the Consejo Superior de Investigaciones Científicas (Spanish National Research Council), two central government bodies and one regional government body (see [Appendix](#appendix) for the names of these institutions and their repositories).

Of the thirteen responses received from institutions that had not yet set up a repository, ten stated that they were planning to do so in the near future, two stated that they had no such plans and one responded "don't know". Universities were the institutions that most frequently expressed their intention to create repositories.

### Questionnaire and responses

In the following sections we analyse the responses of the institutions that already had an institutional repository. Information is presented in terms of the six sections outlined in the [Method section](#method), above.

#### Information on the documents deposited in the repositories

This group of questions was aimed at determining the type of materials contained in the repositories, the version of published articles deposited, the disciplines to which they belonged, the type of access, the process followed for depositing materials and the persons who carried it out. The materials varied greatly, but full-text research articles and doctoral theses were the most frequently deposited. The vast majority of repositories contained both metadata of the text documents and the full-text documents, as can be seen in Figure 2 and Table 1\. These results contrast favourably with the DRIVER report, in which only 32% of the text objects had metadata and full-text documents.

<figure>

![Figure 2\. Types of material and percentage of repositories that contain them](../p415fig2.jpg)

<figcaption>

**Figure 2: Types of material and percentage of repositories that contain them**</figcaption>

</figure>

<table><caption>

**Table 1: Number and types of digital objects in Spanish repositories in 2007 and 2008**</caption>

<tbody>

<tr>

<th rowspan="2">Digital objects</th>

<th colspan="2">Total digital objects</th>

</tr>

<tr>

<th colspan="1">2007</th>

<th colspan="1">2008</th>

</tr>

<tr>

<td>Articles (full text + metadata)</td>

<td>17538</td>

<td>68357</td>

</tr>

<tr>

<td>Articles (only metadata)</td>

<td>100</td>

<td>235</td>

</tr>

<tr>

<td>Books/book chapters (full text + metadata)</td>

<td>429</td>

<td>1145</td>

</tr>

<tr>

<td>Books/book chapters (only metadata)</td>

<td>2005</td>

<td>3053</td>

</tr>

<tr>

<td>Theses (full text + metadata)</td>

<td>3155</td>

<td>8532</td>

</tr>

<tr>

<td>Theses (only metadata)</td>

<td>260</td>

<td>800</td>

</tr>

<tr>

<td>Proceedings (full text + metadata)</td>

<td>444</td>

<td>2080</td>

</tr>

<tr>

<td>Proceedings (only metadata)</td>

<td>0</td>

<td>332</td>

</tr>

<tr>

<td>Working papers (full text + metadata)</td>

<td>1306</td>

<td>3729</td>

</tr>

<tr>

<td>Working papers (only metadata)</td>

<td>0</td>

<td>270</td>

</tr>

<tr>

<td>Learning objects</td>

<td>1725</td>

<td>5889</td>

</tr>

<tr>

<td>Primary data sets</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Images</td>

<td>1129457</td>

<td>4814493</td>

</tr>

<tr>

<td>Vídeos</td>

<td>283</td>

<td>2789</td>

</tr>

<tr>

<td>Music</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Other</td>

<td>2153</td>

<td>9439</td>

</tr>

</tbody>

</table>

The version of journal articles deposited most frequently was the published version (40%), followed by the post-print version (33%) and finally the preprint version (27%). These results are similar to the response obtained by DRIVER I in 2007.

With regard to the type of availability, most of the materials (64%) were available in open access from the moment they were deposited and only 19%, such as articles with publisher restrictions, were subject to some type of embargo.

The thematic areas or disciplines represented in the Spanish repositories were, in order of importance: humanities and social sciences (46%), engineering, life sciences, natural sciences and, finally, fine and performing arts. This order does not coincide with the European averages obtained by DRIVER in 2007, which were, in order of importance: humanities and social sciences, life sciences, natural sciences, engineering. This difference is mainly due to the digital objects of the UPCommons repositories, a pioneering project in Spain with a great deal of content, which includes many engineering subjects.

The materials were self-deposited by the authors in 32% of the repositories, they were delivered by the authors but deposited by others in 24%, they were collected from other sources or online databases in 32% and they were deposited by other, unspecified means in 12% (Table 2). This means that the materials were mainly deposited by specialized staff (mediated archiving). These figures are similar to the European averages obtained by DRIVER I.

<table><caption>

**Table 2\. Work processes of depositing materials**</caption>

<tbody>

<tr>

<th rowspan="1">Which statement best describes the work processes of depositing of materials in the repository?</th>

<th>Answers</th>

<th>%</th>

</tr>

<tr>

<td>Self-depositing by academics, quality control by specialised staff members</td>

<td>8</td>

<td>32</td>

</tr>

<tr>

<td>Delivery by academics, depositing by specialised staff members</td>

<td>6</td>

<td>24</td>

</tr>

<tr>

<td>Collection by staff members independent of the academics</td>

<td>8</td>

<td>32</td>

</tr>

<tr>

<td>Other</td>

<td>3</td>

<td>12</td>

</tr>

</tbody>

</table>

The respondents were also asked to explain briefly the process of depositing materials in the repositories. The responses were used to draw up diagrams, which were pooled to create the flow chart shown in Figure 3\. In general, the pattern followed depended on the type of material deposited. For example, digitized material was archived by the library or by a specialist service (1). Theses were deposited by the Ph.D. students, with the approval of the repository manager (2). Papers and learning objects were normally also archived by their author (3) or by mediated deposit (4). Finally (particularly in the initial period of operation), the repository managers collected materials which were self-archived in subject repositories by staff of the institution or published in open-access journals that allow self-archiving (5).

<figure>

![Figure 3\. Flow chart indicating the process of archiving digital objects in the surveyed Spanish repositories](../p415fig3.jpg)

<figcaption>

**Figure 3\. Flow chart indicating the process of archiving digital objects in the surveyed Spanish repositories**</figcaption>

</figure>

#### Technical infrastructure and technical issues

In this section we deal with the infrastructure and the technical aspects of repository applications and services. We first asked about the software used to create the repository. Most used non-proprietary platforms with a variety of free software licenses. Far above the rest, the software most used was DSpace (66%), followed by EPrints and DigiBib (7% each), as can be seen in Figure 4.

<figure>

![Figure 4\. Distribution of the software used to set up the repositories](../p415fig4.jpg)

<figcaption>

**Figure 4\. Distribution of the software used to set up the repositories**</figcaption>

</figure>

A persistent identifier was assigned to each digital object by 90% of the respondents, but only 30% used a unique author identifier.

The most widely used standard was qualified (68%) and unqualified Dublin Core (28%). With the new DRIVER 2.0 guidelines, the standard may be enriched in the not-too-distant future.

An important aspect to bear in mind in setting up a repository is the need to guarantee the preservation of the materials it contains. A high proportion of the respondents (72%) stated that preservation is guaranteed and taken into account in the management of the repositories at an individual level. There is currently no initiative to deal with the subject at a national level, as happens in the Netherlands, where the National Library is responsible for preservation.

The descriptors or keywords assigned to each document are included in the metadata of digital objects, one of the questions referred to the use of controlled vocabularies or subject heading lists for indexing (Figure 5).

<figure>

![Figure 5\. Use of controlled vocabularies or lists of subjects for indexing digital objects in the repositories](../p415fig5.jpg)

<figcaption>

**Figure 5\. Use of controlled vocabularies or lists of subjects for indexing digital objects in the repositories**</figcaption>

</figure>

Most repositories use a controlled vocabulary to assign keywords and/or classification in the language of the country. However, there is no common process. This situation may change in the near future if the repositories adopt the DRIVER 2.0 guidelines, which recommend using controlled vocabularies to assign keywords and providing them in the language of the country and in English.

It is a common practice among repository managers to analyse accesses and downloads of documents in order to draw up statistics for quantitative (how much) and qualitative (who accesses) purposes. As many as 84% of the respondents stated that they had statistics on access and consultation of the materials contained in their repositories. However, there has been insufficient consensus on criteria and standardization to compare results between different repositories. This is another subject dealt with in the DRIVER 2.0 guidelines, which will probably help to achieve a common procedure.

#### Institutional policies regarding the digital repositories

Few repositories were supported by institutional policies that required the depositing of documents created by academics. The respondents stated that depositing was voluntary in 44% of the repositories, that there was no policy on depositing in 24%, that it was encouraged in 24% and that it was mandatory in 8% (4% for all materials and 4% for theses only).

<figure>

![Figure 6\. Statements that correspond to the policies of your institution's repository](../p415fig6.jpg)

<figcaption>

**Figure 6\. Statements that correspond to the policies of your institution's repository**</figcaption>

</figure>

The second question in this section referred to the characteristics associated with the institution's policies: integration of repositories in other systems, institutional support and the use made of the content for institutional purposes. The respondents were asked to choose from a list the statement that best identified each repository (see Figure 6). The ones that obtained the greatest number of responses were awareness-raising campaigns to promote open access and integration in larger platforms. The aspects that were considered the least important were policies on depositing documents and their use for measuring the research output of the members of the institutions.

#### Services created on top of the digital repositories

This section analysed the presence of repositories in internal or external catalogues, directories or OAI-PMH service providers and the added services that have been created with the repositories.

The presence of the repositories in internal and external catalogues was 64% and 44%, respectively. This means that some institutions have yet to integrate the repositories in their _information ecology_. With regard to their presence in directories and harvesters, the majority are included in RECOLECTA and OpenDOAR (72% each). Also, as shown in Figure 7, the Spanish repositories are present in the main directories and harvesters accepting the _Open Archives Initiative Protocol for Metadata Harvesting_ ([2008](#OAI08)).

<figure>

![Figure 7\. Directories and harvesters that include the Spanish repositories](../p415fig7.jpg)

<figcaption>

**Figure 7\. Directories and harvesters that include the Spanish repositories**</figcaption>

</figure>

Few services are as yet included in the repositories. The most frequent ones are usage and access statistics, which are offered by at least 52% of the repositories (Table 3).

<table><caption>

**Table 3\. Services available in digital repositories**</caption>

<tbody>

<tr>

<th colspan="1"> </th>

<th colspan="2">Printing on-demand services</th>

<th colspan="2">Usage statistics per digital item</th>

<th colspan="2">Personal services</th>

</tr>

<tr>

<th> </th>

<th>No. answers</th>

<th>%</th>

<th>No. answers</th>

<th>%</th>

<th>No. answers</th>

<th>%</th>

</tr>

<tr>

<td>No</td>

<td>23</td>

<td>92</td>

<td>4</td>

<td>8</td>

<td>4</td>

<td>16</td>

</tr>

<tr>

<td>Yes</td>

<td>1</td>

<td>4</td>

<td>13</td>

<td>52</td>

<td>12</td>

<td>48</td>

</tr>

<tr>

<td>Planned</td>

<td>1</td>

<td>4</td>

<td>8</td>

<td>32</td>

<td>9</td>

<td>36</td>

</tr>

</tbody>

</table>

The following question was aimed at determining the types of service associated with institutional repositories that had the highest priority. The question offered a series of alternatives to be scored from 1 to 10\. The accumulated scores were fairly similar for the high-priority services: advisory services (promotion of open access), citation index services, personalized services for authors, preservation services, research assessment and evaluation services and usage statistics services. Three services clearly emerged as being of lower priority: repository hosting, publication services and document copying on-demand services, in decreasing order of priority (Table 4).

<table><caption>

**Table 4\. Accumulated score of options on service priority at a European level**</caption>

<tbody>

<tr>

<th rowspan="1">List of services on top of repositories</th>

<th colspan="1">Accumulated score</th>

</tr>

<tr>

<td>Research assessment and/or evaluation service</td>

<td>224</td>

</tr>

<tr>

<td>Usage statistics services</td>

<td>219</td>

</tr>

<tr>

<td>Preservation</td>

<td>214</td>

</tr>

<tr>

<td>Citation index services</td>

<td>213</td>

</tr>

<tr>

<td>Advisory services (open-access advocacy)</td>

<td>208</td>

</tr>

<tr>

<td>Personalized services for researchers</td>

<td>205</td>

</tr>

<tr>

<td>Disciplinary and/or thematic search engines, gateways, portals, repositories</td>

<td>197</td>

</tr>

<tr>

<td>General search engines, gateways, portals</td>

<td>191</td>

</tr>

<tr>

<td>Cataloguing or metadata creation and/or enhancement services</td>

<td>191</td>

</tr>

<tr>

<td>Advisory services (technical aspects)</td>

<td>191</td>

</tr>

<tr>

<td>Repository hosting services</td>

<td>147</td>

</tr>

<tr>

<td>Publishing services</td>

<td>128</td>

</tr>

<tr>

<td>Printing-on-demand services</td>

<td>119</td>

</tr>

</tbody>

</table>

#### Stimulants and inhibitors for establishing, filling and maintaining repositories

This section is perhaps one of the most interesting in this study because it identifies the strong points and the critical points found during the creation and maintenance of the repositories ([Sale 2006](#sale-2006); [Palmer _et al_. 2008](#palmer-et-al.-2008)). To evaluate the first question in this section, on the stimulants considered to be most important for the development of the repositories, in this study we used a different scale to that of DRIVER. We decided to maintain the list of options but to ask the respondents to score them all from 1 to 10 instead of choosing a maximum of three of them. The scores received for each option were summed to give a total score for each one (Table 5). Of the fifteen options, the most highly valued ones are represented by red bars. Again, the key aspects for the development of repositories were, in order of importance, the increase in visibility and citation, the interest of decision-makers, simplicity of use and search services.

<table><caption>

**Table 5\. Stimulants for supporting existing repositories**</caption>

<tbody>

<tr>

<th rowspan="1">Stimulant option</th>

<th colspan="1">Accumulated score</th>

</tr>

<tr>

<td>Increased visibility and citations for the publications of the academics in our institute</td>

<td>178</td>

</tr>

<tr>

<td>Interest of the decision makers within our institute</td>

<td>168</td>

</tr>

<tr>

<td>Our simple and user-friendly depositing process</td>

<td>164</td>

</tr>

<tr>

<td>Search services as provided by national and international gateways</td>

<td>152</td>

</tr>

<tr>

<td>Integration/linking of the digital repository with other systems in our institute</td>

<td>148</td>

</tr>

<tr>

<td>Our policy to safeguard the long-term preservation of the deposited material</td>

<td>143</td>

</tr>

<tr>

<td>Our clear guidelines for selection of material for inclusion</td>

<td>142</td>

</tr>

<tr>

<td>Situation with regard to copyright of (to be) published materials and the knowledge about this among academics in our institute</td>

<td>132</td>

</tr>

<tr>

<td>Awareness-raising efforts among the academics in our institute</td>

<td>128</td>

</tr>

<tr>

<td>National open access policies</td>

<td>124</td>

</tr>

<tr>

<td>Financial support from a national funding programme for the digital repository in our institute</td>

<td>118</td>

</tr>

<tr>

<td>Coordination of a national body for digital repositories</td>

<td>118</td>

</tr>

<tr>

<td>The requirements of research-funding organizations in our country about depositing research output in Open Access repositories</td>

<td>107</td>

</tr>

<tr>

<td>Our institutional policy of accountability</td>

<td>94</td>

</tr>

<tr>

<td>Our institutional policy of mandatory depositing</td>

<td>89</td>

</tr>

</tbody>

</table>

The main inhibitors identified were the absence of policies, the lack of integration with other national and international systems and the lack of awareness-raising efforts among academics (Table 6), although the differences here were not as great as for the stimulants.

<table><caption>

**Table 6\. Inhibitors for the development of a repository**</caption>

<tbody>

<tr>

<th rowspan="1">Inhibitor option</th>

<th colspan="1">Accumulated score</th>

</tr>

<tr>

<td>Lack of requirements of research funding organizations in our country about depositing research output in open-access repositories</td>

<td>205</td>

</tr>

<tr>

<td>Lack of institutional policies or mandates</td>

<td>183</td>

</tr>

<tr>

<td>Lack of an institutional accountability policy</td>

<td>170</td>

</tr>

<tr>

<td>Lack of awareness-raising efforts among the academics in our institute</td>

<td>149</td>

</tr>

<tr>

<td>Situation with regard to copyright of (to be) published materials and the knowledge about this among academics in our institute</td>

<td>149</td>

</tr>

<tr>

<td>Lack of coordination of a national body for digital repositories</td>

<td>145</td>

</tr>

<tr>

<td>Lack of financial support from a national funding programme for the digital repository in our institute</td>

<td>141</td>

</tr>

<tr>

<td>Lack of support for increased visibility and citations for the publications of the academics in our institute</td>

<td>132</td>

</tr>

<tr>

<td>Lack of integration/linking of the digital repository with other systems in our institute</td>

<td>131</td>

</tr>

<tr>

<td>Lack of interest from the decision-makers within our institute</td>

<td>128</td>

</tr>

<tr>

<td>Lack of search services as provided by national and international gateways</td>

<td>113</td>

</tr>

<tr>

<td>Lack of a policy to safeguard the long-term preservation of the deposited material</td>

<td>109</td>

</tr>

<tr>

<td>Lack of a simple and user-friendly depositing process</td>

<td>101</td>

</tr>

<tr>

<td>Lack of clear guidelines for selection of material for inclusion</td>

<td>87</td>

</tr>

</tbody>

</table>

The last question in this section referred to the priorities for the further development of repositories in Spain and Europe. In this case the responses were free text and they were grouped according to their content. The results were the categories shown in Figure 8.

<figure>

![Figure 8\. Priority aspects for the future development of digital repositories in Spain and Europe](../p415fig8.jpg)

<figcaption>

**Figure 8\. Priority aspects for the future development of digital repositories in Spain and Europe**</figcaption>

</figure>

The priorities established at a national and European level were similar: the existence of an open-access mandate, quality control and standards, clear institutional policies on open access and funding. These priorities were also stated in the studies carried out in the United States and the Nordic countries ([Hedlund and Rabow 2007](#hed07)).

## Conclusion

This is the first detailed study of Spanish institutional repositories. The results represent the situation of 78% of the repositories existing on the survey date (end of 2008 beginning of 2009). The results show that Spanish institutional repositories contained mostly metadata and full text research articles and doctoral theses. Nevertheless, the materials deposited were far from including the whole scientific production of the institutions. It must be noted that in most cases the materials were deposited by specialized staff (mediated deposit). Institutional policies regarding self-archiving and services created on top repositories were the most relevant priorities stated by repository managers.

One of the commitments of our research group is to follow the development of open access environment in Spain, and one of the tasks for the future is to monitor the existing institutional repositories and their evolution: in this sense this work has served as the basis for future endeavours.

## Acknowledgements

This research was funded by the Spanish _Plan Nacional de I+D_, in the context of the project _Open Access to scholarly production in Spain: analysis of the current state and presentation of policies and strategies for their development_ (CSO2008-05525-C02-01/SOCI).

## <a id="authors"></a>About the authors

Remedios Melero is researcher in Instituto de Agroquímica y Tecnología de Alimentos (CSIC). She can be contacted at [rmelero@iata.csic.es](mailto:rmelero@iata.csic.es)

Ernest Abadal is Senior Lecturer in the Department of Library and Information Science, University of Barcelona. He can be contacted at [abadal@ub.edu](mailto:abadal@ub.edu)

Francisca Abad is Senior Lecturer in the Departamento de Historia de la Ciencia y Documentación, Facultad de Medicina, University of Valencia. She can be contacted at [Maria.F.Abad@uv.es](mailto:maria.f.abad@uv.es)

Josep Manel Rodríguez-Gairín is Senior Lecturer in the Department of Library and Information Science, University of Barcelona. He can be contacted at: [rodriguez.gairin@ub.edu](mailto:rodriguez.gairin@ub.edu)

They are members of the Research Group [_Acceso Abierto a la ciencia_](http://www.accesoabierto.net).

## References

*   <a id="arl-2009"></a>Association of Research Libraries. _Digital Repositories Task Force._ (2009). _[The research library's role in digital repository services: final report of the ARL Digital Repositories Task Force](http://www.webcitation.org/5jhiBmrPB)_. Washington, DC: Association of Research Libraries. Retrieved 23 September, 2009 from http://www.arl.org/bm~doc/repository-services-report.pdf (Archived by WebCite® at http://www.webcitation.org/5jhiBmrPB)
*   <a id="bailey-2006"></a>Bailey, C.W. (2006). _[SPEC Kit 292: institutional repositories](http://www.webcitation.org/5k00M1Gjd)_. Washington, DC: Association of Research Libraries. Retrieved 23 September, 2009 from http://www.arl.org/bm~doc/spec292web.pdf (Archived by WebCite® at http://www.webcitation.org/5k00M1Gjd)
*   <a id="bankier-2008"></a>Bankier, J-G. (2008). _[Perceptions of developing trends in repositories: survey results for the SPARC digital repositories meeting 2008, Baltimore, MD November 17th-18th, 2008.](http://www.webcitation.org/5k01Ggul2)_. Berkeley, CA: The Berkeley Electronic Press. Retrieved 23 September, 2009 from http://works.bepress.com/cgi/viewcontent.cgi?article=1018&context=jean_gabriel_bankier (Archived by WebCite® at http://www.webcitation.org/5k01Ggul2)
*   <a id="berlin"></a>[Berlin declaration on open access to knowledge in the sciences and humanities.](http://www.webcitation.org/5lYYizAQZ) (2003). Retrieved 25 November, 2009 from http://oa.mpg.de/openaccess-berlin/berlindeclaration.html (Archived by WebCite® at http://www.webcitation.org/5lYYizAQZ)
*   <a id="davis-and-connolly-2007"></a>Davis, P.M. & Connolly, M.J.L. (2007). [Institutional repositories: evaluating the reasons for non-use of Cornell University's installation of DSpace.](http://www.webcitation.org/5k01UJ8qK) _D-Lib Magazine_, **13**(3/4). Retrieved 23 September, 2009 from http://www.dlib.org/dlib/march07/davis/03davis.html (Archived by WebCite® at http://www.webcitation.org/5k01UJ8qK)
*   <a id="driver"></a>[DRIVER. Digital Repository Infrastructure Vision for European Research](http://www.webcitation.org/5k01WlvVA). Retrieved 23 September, 2009 from http://www.driver-community.eu/ (Archived by WebCite® at http://www.webcitation.org/5k01WlvVA)
*   <a id="driver-2008"></a>DRIVER (2008). [DRIVER Guidelines 2.0.](http://www.webcitation.org/5k01c0CND) Retrieved 23 September, 2009 from http://www.driver-support.eu/documents/DRIVER_Guidelines_v2_Final_2008-11-13.pdf (Archived by WebCite® at http://www.webcitation.org/5k01c0CND)
*   <a id="foster-and-gibbons-2005"></a>Foster, N.F. & Gibbons, S. (2005). [Understanding faculty to improve content recruitment for institutional repositories.](http://www.webcitation.org/5k01f1ZgV) _D-Lib Magazine_, **11** (1). Retrieved 23 September, 2009 from http://www.dlib.org/dlib/january05/foster/01foster.html (Archived by WebCite® at http://www.webcitation.org/5k01f1ZgV)
*   <a id="hed07"></a>Hedlund, T. & Rabow, I. (2007). _[Open Access in the Nordic Countries: a state of the art report](http://www.webcitation.org/5k03rHEvq)_. Retrieved 23 September, 2009 from http://www.nordforsk.org/_img/oa_report_020707.pdf (Archived by WebCite® at http://www.webcitation.org/5k03rHEvq)
*   <a id="ken09"></a>Kennan, M. A.& Kingsley, D. (2009). [The state of the nation: a snapshot of Australian institutional repositories](http://www.webcitation.org/5k01hP3yX). _First Monday_, **14**(2). Retrieved 23 September, 2009 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/viewArticle/2282/2092 (Archived by WebCite® at http://www.webcitation.org/5k01hP3yX)
*   <a id="lynch-2003"></a>Lynch, C. (2003). [Institutional repositories: essential infrastructure for scholarship in the digital age](http://www.webcitation.org/5k01nG1Uv). _ARL: A Bimonthly Report_, No. 226\. Retrieved 23 September, 2009 from http://www.arl.org/resources/pubs/br/br226/br226ir.shtml (Archived by WebCite® at http://www.webcitation.org/5k01nG1Uv)
*   <a id="lynch-and-lippincott-2005"></a>Lynch, C.A. & Lippincott, J.K. (2005). [Institutional repository deployment in the United States as of early 2005](http://www.webcitation.org/5k01pgsZD). _D-Lib Magazine_, **11**(9). Retrieved 23 September, 2009 from http://www.dlib.org/dlib/september05/lynch/09lynch.html (Archived by WebCite® at http://www.webcitation.org/5k01pgsZD)
*   <a id="markey-2007"></a>Markey, K., Young, R. S., Jean, B., Kim, J. & Yakel, E. (2007). _[Census of institutional repositories in the United States: MIRACLE Project research findings](http://www.webcitation.org/5k01tMozm)_. Washington, DC: Council on Library and Information Resources. Retrieved 23 September, 2009 from http://www.clir.org/pubs/reports/pub140/pub140.pdf (Archived by WebCite® at http://www.webcitation.org/5k01tMozm)
*   <a id="mcdowell-2007"></a>McDowell, C. S. (2005). [Evaluating institutional repository deployment in American academe since early 2005: repositories by the numbers, Part 2](http://www.webcitation.org/5k01y6GB3). _D-Lib Magazine_, **13**(9/10). Retrieved 23 September, 2009 from http://www.dlib.org/dlib/september07/mcdowell/09mcdowell.html (Archived by WebCite® at http://www.webcitation.org/5k01y6GB3)
*   <a id="melero-2006"></a>Melero, R. (2006). [_Open access environment in Spain: how the 'movement' has evolved and current emerging initiatives_](http://www.webcitation.org/5k020d5DD). Paper presented at the Workshop [on] Open Access and Information Management, Oslo (Norway), 10th May, 2006\. Retrieved 23 September, 2009 from http://digital.csic.es/bitstream/10261/1489/1/OA3rm.pdf (Archived by WebCite® at http://www.webcitation.org/5k020d5DD)
*   <a name="Melero-2007" id="Melero-2007"></a>Melero, R. (2007). [Open access institutional repositories: the case study of Spain](http://www.webcitation.org/5k024MAcx). In Paula Goossens, (Ed.). _Proceedings of the 31st Library System Seminar: ELAG 2007: Library 2.0_. University of Barcelona, 9-11 May 2007\. European Library Automation Group. Retrieved 23 September, 2009 from http://elag2007.upf.edu/papers/melero_2.pdf (Archived by WebCite® at http://www.webcitation.org/5k024MAcx)
*   <a id="melero-2008"></a>Melero, R. (2008). [El paisaje de los repositorios institucionales open access en España](http://www.webcitation.org/5k02BEpdW) [Institutional open access repositories in Spain]. _BiD: textos universitaris de biblioteconomia i documentació_, No. 20\. Retrieved 23 September, 2009 from http://www2.ub.edu/bid/consulta_articulos.php?fichero=20meler4.htm (Archived by WebCite® at http://www.webcitation.org/5k02BEpdW)
*   <a id="melero-et-al-2008"></a>Melero, R., López Medina, A. & Prats, J. (2008). [Landscape of open access institutional repositories in Spain](http://www.webcitation.org/5k028gkLJ). In _Third International Conference on Open Repositories 2008, 1-4 April 2008, Southampton, United Kingdom._ Retrieved 23 September, 2009 from http://pubs.or08.ecs.soton.ac.uk/56/ (Archived by WebCite® at http://www.webcitation.org/5k028gkLJ)
*   <a id="oai08"></a>[The Open Archives Initiative Protocol for Metadata Harvesting](http://www.webcitation.org/5lZqql0OE). (2008). (Protocol Version 2.0 of 2002-06-14). Retrieved 26 November, 2009 from http://www.openarchives.org/OAI/openarchivesprotocol.html (Archived by WebCite® at http://www.webcitation.org/5lZqql0OE)
*   <a id="palmer-et-al.-2008"></a>Palmer, C.L., Teffeau, L.C. & Newton, M.P. (2008). _[Identifying factors of success in CIC institutional repository development: final report](http://www.webcitation.org/5k03dZjZO)_. New York, NY: The Andrew Mellon Foundation. Retrieved 23 September, 2009 from http://www.cic.net/Libraries/Reports/PalmerEtAlMellonReport.sflb (Archived by WebCite® at http://www.webcitation.org/5k03dZjZO)
*   <a id="sale-2006"></a>Sale, A. (2006). _[Generic risk analysis: open access for your institution](http://www.webcitation.org/5k03p0IDg)_. (Unpublished technical report.) Retrieved 23 September, 2009 from http://eprints.utas.edu.au/266/1/Risk_Analysis-v1.0.pdf (Archived by WebCite® at http://www.webcitation.org/5k03p0IDg)
*   <a id="van-eijndhoven-and-van-der-graaf-2007"></a>van Eijndhoven, K & van der Graaf, M. (2007). _[Inventory study into the present type and level of OAI compliant Digital Repository activities in the EU](http://www.webcitation.org/5k03vM4tB)_. Retrieved 23 September, 2009 from http://www.driver-support.eu/documents/DRIVER%20Inventory%20study%202007.pdf (Archived by WebCite® at http://www.webcitation.org/5k03vM4tB)

* * *

## <a id="appendix"></a>Appendix: Institutions participating in the study and links to their institutional repositories

### 1\. Institutions with an established institutional repository

Consejo Superior de Investigaciones Científicas, CSIC (http://digital.csic.es/)  
Consorci de Biblioteques Universitàries de Catalunya, CBUC (http://www.cbuc.es/)  
Gobierno del Principado de Asturias (http://www.princast.es)  
Ministerio de Cultura Dirección General del Libro, Archivos y Bibliotecas Subdirección General de Coordinación Bibliotecaria ( http://bvpb.mcu.es )  
Ministerio de Cultura Dirección General del Libro, Archivos y Bibliotecas Subdirección General de Coordinación Bibliotecaria ( http://prensahistorica.mcu.es )  
Universidad Carlos III de Madrid (http://e-archivo.uc3m.es)  
Universidad Complutense de Madrid (http://eprints.ucm.es)  
Universidad de Alcalá (http://dspace.uah.es/dspace)  
Universidad de Alicante (http://rua.ua.es)  
Universidad de Burgos (http://dspace.ubu.es:8080/tesis/)  
Universidad de Murcia (http://digitum.um.es/)  
Universidad de Navarra (http://dspace.unav.es)  
Universidad Nacional de Educación a Distancia UNED (http://e-spacio.uned.es)  
Universidad Politécnica de Cartagena (http://repositorio.bib.upct.es)  
Universidad Politécnica de Madrid (http://oa.upm.es)  
Universidad Politécnica de Valencia (http://riunet.upv.es)  
Universidad Pontificia de Salamanca (http://www.upsa.es)  
Universidad Rey Juan Carlos (http://eciencia.urjc.es/dspace)  
Universidade da Coruña (http://dspace.udc.es)  
Universitat Autònoma de Barcelona (http://www.uab.cat/bib)  
Universitat de Barcelona (http://diposit.ub.edu)  
Universitat de Girona (http://dugi.udg.edu/portal/)  
Universitat de les Illes Balears (http://ibdigital.uib.es/)  
Universitat de Lleida. Servei de Biblioteca i documentació Repositorios de CBUC  
Universitat Politècnica de Catalunya, UPC (http://upcommons.upc.edu/)

### 2\. Institutions without repository at that time the survey was conducted

Universidad Europea Miguel de Cervantes  
Universidad Antonio de Nebrija  
Agencia Laín Entralgo para La Formación, Investigación y Estudios Sanitarios. Consejeria de Sanidad. Madrid.  
Universidad de Extremadura  
Universitat de Vic  
Universidad de Córdoba  
Universidad CEU Cardenal Herrera  
Universidad de Huelva  
Universidade de Santiago de Compostela  
Instituto de Salud Carlos III  
Centro de Estudios y Experimentación de Obras Públicas  
Universitat Pompeu Fabra  
Biblioteca de la Universidad de Zaragoza