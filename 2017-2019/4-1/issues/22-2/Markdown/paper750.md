<header>

#### vol. 22 no. 2, June, 2017

</header>

<article>

# Credibility assessments of everyday life information on Facebook: a sociocultural investigation of a group of mothers

## [Ameera Mansour](#author) and [Helena Francke](#author).

> **Introduction.** The article explores whether members of a Facebook group view the group as a source of credible information and how they evaluate the credibility of information provided in the group.  
> **Method.** The data for this study were collected using semi-structured interviews with 19 members of a closed Facebook group for mothers.  
> **Analysis.** The constant comparison technique was used to analyse the interview transcripts which were interpreted from a sociocultural perspective using the concepts of cultural tools and cognitive authority.  
> **Results.** The findings show that although the participants used the Facebook group to seek information, they did not consider it a credible source of information. The study contributes the insight that assessments depended on the domain of the information and that participants distinguished between information offered in a professional or a personal capacity. A number of cultural tools were employed to negotiate credibility assessments, including language use and writing style, expertise, life experience, educational background, and similar lifestyles, parenting values and worldviews.  
> **Conclusions.** The Facebook group was characterised by a combination of familiar and unfamiliar others, of the sharing and seeking of information from different domains and of first- and second-hand knowledge. The participants employed various cultural tools to assess credibility in this mixture of knowledge domains and information sources.

<section>

## Introduction

Social media have become integrated in many people’s everyday lives. These rich and diverse social networking platforms attract people from different age and social groups, and they are widely used to seek and share information for various parts of people’s everyday lives ([Duggan, Lenhart, Lampe and Ellison, 2015](#Dug15)). Previous studies have shed some light on social media platforms and their perceived role as potential and useful sources of information, and found that such platforms are often a preferred source for information seeking, as opposed to formal information channels and search engines ([Duggan _et al._, 2015](#Dug15); [Lampe, Vitak, Gray and Ellison, 2012](#Lam12); [Morris, Teevan and Panovich, 2010](#Mor10)). This is because the information is often perceived to be more tailored to people’s information needs and they tend to trust its credibility since it often comes from known and close social networks ([Johnson and Kaye, 2015](#Joh15); [Lampe _et al._, 2012](#Lam12); [Morris _et al._, 2010](#Mor10)).

Parents, especially mothers, have been found to be among the largest and fastest growing demographic group on social media ([Duggan _et al._, 2015](#Dug15); [Morris, 2014](#Mor14); [Schoenebeck, 2013](#Sch13)). Social media are widely considered as an important information resource by parents who report encountering useful information on social media ([Duggan _et al._, 2015](#Dug15); [Morris, 2015](#Mor14); [Schoenebeck, 2013](#Sch13)). Facebook is one of the most popular social media platforms with 1.59 billion users accessing the site ([Zuckerberg, 2016](#Zuc16)). One of the most active user groups on Facebook is parents, with close to 75 % of active American parents logging onto Facebook on a daily basis ([Duggan _et al._, 2015](#Dug15)). Facebook allows its users to create groups around specific topics of interest, a feature that is very popular among the 1 billion Facebook users accessing Facebook groups on a monthly basis ([Zuckerberg, 2016](#Zuc16)). Groups that are centered on parenting issues are widely spread on Facebook. Such groups provide parents with common spaces to share their parenting experiences with other parents. Social media sources such as Facebook groups may thus be seen as important sources of information through which parents can both seek and share information and experiences about childcare issues, and lend social and emotional support to other parents ([Duggan _et al._, 2015](#Dug15); [Lampe _et al._, 2012](#Lam12)).

Despite the affordances of social media, and their mediating role in information seeking and sharing, there have been some concerns about information credibility in online settings in general and on social media platforms in particular. This is mostly because social media platforms lack some of the control over content quality built into traditional media and genres ([Johnson and Kaye, 2013](#Joh13); [Metzger, Flanagin and Medders, 2010](#Met10); [Rieh and Danielson, 2007](#Rie07); [Rieh, Kim, Yang and St. Jean, 2010](#Rie10)). These concerns over credibility have resulted in renewed attention among media and information science scholars to understanding information credibility and how it is evaluated in social media environments. However, while the research on credibility in social media environments has grown rapidly in the past few years focusing on different social media platforms, there is still a lack of scholarly knowledge about how credibility assessments are carried out on Facebook, one of the largest and most dominant social media platforms.

Consequently, there is a need for more focused research in order to understand how people arrive at credibility decisions in social media environments, where information is frequently sought, created and shared on a large scale by multiple people and communities (see [Metzger and Flanagin, 2013](#Met13)). Mothers’ frequent and active use of Facebook, particularly Facebook groups, for seeking and finding information motivates us to investigate the ways through which they make credibility assessments of information received through Facebook groups. A mothers’ group is a particularly interesting setting for studying credibility assessments as part of everyday-life information seeking and use, since the groups are used to address a variety of information needs and uses.

Our aim is to explore if and why Facebook group members (belonging to a mothers’ group) perceive the group as containing credible information, and to describe the ways in which they assess the credibility of information provided in the group. Theoretically, we apply a sociocultural lens which leads us to look for the cultural tools ([Wertsch, 1998](#Wer98)) used by the participants in their credibility assessments and their attribution of cognitive authority ([Wilson, 1983](#Wil83)) to sources. The study is based on semi-structured interviews with nineteen mothers living in Sweden who are members of a Facebook group. We address the following questions:

> 1) What perceptions do the members of the Facebook group have of the group as a credible information source?  
> 2) How do members of a Facebook group assess information credibility in the group and what cultural tools do they rely on in this process?

</section>

<section>

## Related work

A number of theoretical and empirical contributions have been made to explicate the different strategies that people follow in making credibility assessments in online environments (see e.g.[Fogg, 2003](#Fog03); [Hilligoss and Rieh, 2008](#Hil08); [Metzger, 2007](#Met07); [Metzger and Flanagin, 2013](#Met13); [Rieh and Danielson, 2007](#Rie07)). An influential example is Metzger and Flanagin ([2013](#Met13)). Metzger _et al._ ([2010](#Met10)) identified six cognitive heuristics that online information seekers use as quick tools in order to judge credibility, particularly on new media platforms. The _**reputation heuristic**_ describes how people believe sources that are familiar to them rather than unfamiliar sources. People employ the _**endorsement heuristic**_ when they trust information or sources if others also have done so. The _**consistency heuristic**_ describes that people check if the information is consistent across different online and offline sources. When using the _**self-confirmation heuristic**_, people choose to believe information as credible only if the information aligns with their pre-existing beliefs. Similarly, the _**expectancy violation heuristic**_ is employed when people immediately judge information from a source as not credible if that source failed to meet their expectations in any way. Finally, the _**persuasive intent heuristic**_ is used when people dismiss information if it came from a possibly biased source.

Empirical findings from research on credibility uncovered a number of factors affecting people’s credibility assessments on social media, including topic, information source or platform consulted, and the user’s familiarity with a topic or an information source.

An increasing number of empirical studies have suggested that evaluating information on different topics leads to different credibility assessments online ([Fogg, 2003](#Fog03); [Francke, Sundin and Limberg, 2011](#Fra11); [Jeon and Rieh, 2013](#Jeo13); [Jeon and Rieh, 2014](#Jeo14); [Metzger _et al._, 2010](#Met10); [Sundin and Francke, 2009](#Sun09); [Rieh and Danielson, 2007](#Rie07)). Some studies reported that people tend to distinguish between factual information and subjective information, which in turn influences their credibility judgments. For instance, in a study by Metzger _et al._ ([2010](#Met10)), the participants were more concerned with the credibility of what they considered as subjective information (e.g., an opinion on a product) that needed assessment, as opposed to objective information (e.g., the dimensions of a product) that did not require as much scrutiny. Similarly, Sundin and Francke ([2009](#Sun09)) noted that students in their study made a distinction between facts and opinions when selecting online sources. When writing school reports, for instance, these students perceived information on Wikipedia to be trustworthy to some extent since they related it to traditional encyclopaedias that provide factual information. In contrast, they considered information on blogs to be opinions and thus they were more hesitant to use it in their school reports.

In a similar vein, findings by Rieh _et al._ ([2010](#Rie10)) demonstrated how credibility assessments can be conditional based on the topic of information sought and also the information source consulted. On the one hand, the authors found that for health and news information the participants tended to trust traditional and official media sources, but they were more sceptical about information on the same topics available via user-generated content sources. Conversely, when assessing the credibility of information concerning certain products or services, the participants tended to view user-generated content (e.g., customer reviews) as more likely to be credible and unbiased than information available via official sources (e.g., a manufacturer’s or a travel agency’s website). Syn and Kim ([2013](#Syn13)) examined undergraduate students’ perceptions of the credibility of health information with regard to different information sources available on Facebook. They found that the expertise of the information source was very important when making credibility assessments, and thus official information sources (e.g., health organisations and governmental agencies) were considered to be the most credible information sources on Facebook.

Further, familiarity with a topic or an information source, as well as users’ Internet skills, have been found to influence the heuristics invoked when assessing credibility. When people are not familiar with, or are new to, an information source they tend to rely on traditional and superficial cues (e.g., writing style, website design) to aid their assessments ( [Flanagin and Metzger, 2007](#Fla07); [Fogg, 2003](#Fog03); [Rieh and Danielson, 2007](#Rie07)), but they tend to invoke source specific cues when they are more familiar with the information source. Gao, Tian and Tu ([2015](#Gao15)) also found that when the users of the Chinese microblogging system Weibo had previous knowledge on a topic (e.g., food safety) they relied on their knowledge to inform their judgements of the information’s credibility. But when they had no prior knowledge of the topic they relied on social cues (e.g., negative/positive feedback from friends) and platform specific cues (e.g., popularity of the message) to assess credibility.

Van Der Heide and Lim ([2016](#Van16)) and Lim and Van Der Heide ([2015](#Lim15)) found that participants who were familiar with a certain platform relied on, for instance, previous experience or skills in using the platform to help them assess credibility (see also [Francke and Sundin, 2010](#Fra10)). If they lacked previous experience, users would rely on traditional heuristic cues (e.g., writing style) to form their judgments. This is in line with findings by Hocevar, Flanagin and Metzger ([2014](#Hoc14)) about experienced social media users. They found that these users tend to consider information on social media to be more credible compared to less experienced users. The ability and skill of such users make them better at locating sources of information which are relevant to them, and that they consider to be credible ([Johnson and Kaye, 2015](#Joh15); [Stavrositu and Sundar, 2008](#Sta08)).

In addition, research has shown the importance of source specific features in a social media platform such as Twitter or Facebook (e.g., user name, user image) when users make credibility assessments ([Morris, Counts, Roseway, Hoff and Schwarz, 2012](#Mor12)). Similarly, Kim ([2010](#Kim10)) found that users employed source specific strategies (e.g., checking answerer’s profiles) in _Yahoo! Answers_ to assess credibility. This was also observed by Jeon and Rieh ([2014](#Jeo14)) who found that when assessing the credibility of questions on Q&A platforms, the attitude of the answerer (e.g., how much involvement and effort did they put into answering the question), trustworthiness (e.g., way of answering, wording, punctuating and formatting the answer), and expertise (e.g., perceived knowledge or experience of the answerer) were important factors when making the assessment.

To summarise, previous studies provide insight into how credibility assessments are made in social media environments by highlighting the role of the topic, the type of information source/platform used, and user’s familiarity with a topic or an information source (e.g. user’s skills and experience with platform) in the assessments.

## Theoretical framing

To develop a theoretical account of credibility assessment and how it is negotiated, we draw on sociocultural theory as a theoretical lens. A sociocultural approach situates human action, particularly learning, within a broader cultural, institutional, and historical context (see [Wertsch, 1998](#Wer98)).

From a sociocultural perspective, interaction is viewed as mediated by various types of tools. People rely on or appropriate tools from the _‘cultural toolkits’_ available to them (e.g., their cultural, conceptual, and material heritage) to help coordinate their actions with each other and with the world around them ([Cole and Wertsch, 1996, p. 252](#Col96); [Wertsch, 1998](#Wer98)). These cultural tools, or what is often referred to as mediational means, can be _material or immaterial_. For instance, Wertsch ([1998, p. 30](#Wer98)) suggests that some cultural tools:

> [H]ave a clear-cut materiality in that they are physical objects that can be touched and manipulated. Furthermore, they can continue to exist across time and space, and they can continue to exist as physical objects even when not incorporated into the flow of action.

Other mediational means or tools can be immaterial or conceptual (e.g., spoken language, cultural customs, norms, values and beliefs); the materiality of such cultural tools is very difficult to hold or grasp.

Wertsch ([1998, p. 142](#Wer98)) goes on to argue that cultural tools can have great power in shaping people’s understanding of the world, their consciousness, and their actions. However, drawing on Certeau, Wertsch points out that this is not to say that people are passive consumers of cultural tools. Rather he emphasises that by using or renting the tools, _‘individuals and groups always shape and transform these cultural tools in particular ways’_ ([Wertsch, 1998, p. 142](#Wer98)), for instance by adding their imprint to them. Thus, the tools available for people enable them to master their actions in ways that are appropriate and accepted to their cultures, but also to refine these tools as they use them in specific communities ([Kozulin, 1998](#Koz98)).

Credibility assessment is understood here as something that is constructed and negotiated in relation to a specific social setting and mediated through cultural tools (cf.[Francke _et al._, 2011](#Fra11); [Limberg, Sundin and Talja, 2012](#Lim12); [Sundin and Francke, 2009](#Sun09)). Thus, how credibility is negotiated differs from one social setting to another and from one context to the other. Each social setting has its own and unique cultural, institutional, and historical context that shapes and is shaped by how people make credibility assessments.

To understand people’s credibility assessments, we also rely on the concept of cognitive authority proposed by Patrick Wilson ([1983](#Wil83)). Wilson states that beyond people’s own experiences, everything they know about the world is mediated to them by others. Wilson suggests that in interpreting the world, people rely on what they have learned first-hand from their own experience or what they have learned second-hand from others. In the second case, the need to ascertain that the second-hand knowledge is credible arises, and one cultural tool used in such assessments is if the information originates from a cognitive authority. Cognitive authorities are those whose influence on one’s knowledge one considers to be justified, and hence worthy of belief. Wilson points out that people tend to perceive someone as a cognitive authority if they are honest, careful in what they say and are disinclined to deceive. People’s cognitive authorities are thus credible sources to whom people tend to turn when they need to expand their knowledge of the world. Cognitive authority is always an attributed property; one is always a cognitive authority _to_ someone. Nonetheless, Wilson points out that someone’s cognitive authority is limited. For instance, within a sphere someone may speak with full authority, but in other spheres they may be attributed no authority at all.

Wilson ([1983, p. 24](#Wil83)) further explains that cognitive authority _‘can be justified on the ground that one finds the views of an individual intrinsically plausible, convincing, or persuasive’_. He claims that people’s prior held beliefs often set limits to what they can accept as new beliefs and thus, of course, determine who or what they trust as cognitive authorities to rely on. On the one hand, if someone provides information that may _‘ring true’_ ([Wilson, 1983, p. 24](#Wil83)), people start listening to that person as a source who provides trustworthy information and he or she becomes a new cognitive authority to whom they turn for credible information. On the other hand, when relying on prior held beliefs to test the intrinsic plausibility of claims made, if a person fails to provide information in accordance with those beliefs, then information from that person will be dismissed outright. To put it in other words, people _‘cannot take seriously’_ ([Wilson, 1983, p. 24](#Wil83)) any information provided by someone if the person is speaking _‘nonsense’_ or failing to tell them what they want to hear ([Wilson, 1983, p. 169](#Wil83)).

Wilson’s concept of cognitive authority is in line with the sociocultural view that people rely on cultural tools and others to help them understand the world, for instance to evaluate the quality of sources. Therefore the concept of cognitive authority fits very well with a sociocultural understanding of human action as it helps in conceptualizing how people and cultural tools shape and are shaped by credibility assessments. Thus, this theoretical framework provides a lens to interpret what and why people do what they do.

</section>

<section>

## Methods, material, and participants

The data for this study were generated through semi-structured interviews with 19 mothers that were recruited from a mother’s Facebook group. Interviewing proved to be a suitable method to generate data in our study since it allowed for private discussions with the participants about their _‘behind-the-scene’_ motivations and justifications ([Boellstorff, Nardi, Pearce and Taylor, 2012, p. 93](#Boe12)) of information credibility assessments in relation to the group and in a specific setting.

</section>

<section>

### The setting

The participants of the study were recruited from a mothers’ Facebook group that was created by and for foreign mums who are living in Sweden. The group had a little more than 1,500 members (at the time of the data collection), who were all mothers or mothers-to-be living in Sweden, or mothers planning to move to Sweden. The group accommodates mothers from different nationalities and with different spoken languages, but English is the only language used to communicate within the group. The main purpose for creating the group was to have a common place for non-native mothers to seek and share information and advice related to their children, as well as general information about living in Sweden. The group is set to be closed, which means that no one other than its members can post, view or comment on posts in the group.

</section>

<section>

### Negotiating access and recruiting participants

Before the group described above was selected, the group administrators of four mother’s Facebook groups were contacted via a Facebook personal message to get permission for posting in the groups an invitation to participate in the study. When permission was granted, the invitation was posted in the four groups. We received responses from all groups, but decided to continue work with participants in the group from which we received most responses, which was also the group that was most active in terms of posts. The participants were recruited between May and November 2014.

Announcements and reminders were posted several times in the group to recruit more participants and invitations were sent out through private Facebook messages to some active members who were thought to be likely to contribute insights of relevance to the study. When a point of saturation was reached ([Silverman and Marvasti, 2008](#Sil08)), that is, no further or new insights were added through the interviews, we stopped recruiting more participants. In the end, a total of 19 members participated in the interview study. The interviews took place from May 2014 through early spring of 2015.

</section>

<section>

### Participants and data collection

The first author conducted semi-structured interviews with the 19 mothers, whose ages ranged between 25 and 45 years. The participants came from 10 different countries, but the majority were from English-speaking countries. The mothers had different educational backgrounds, but almost everyone who agreed to participate was highly educated, ranging from bachelor degrees to doctorate degrees. The number of children varied between one and three children, and the children’s ages varied from new-borns up to 8-year-olds.

The interviews were conducted by whatever means was most convenient for the participant, including face-to-face, Skype, FaceTime, phone, and chat. All interviews were recorded and a log of the chat interview was saved. The interviews lasted between 45 minutes and 90 minutes. Each interview began with asking the participant general questions about her information and social media activities, which social media platforms she used most often, and the main sources she relied on for information. After this, questions focused specifically on her information activities within the group and her main strategies for filtering information obtained from the group. Credibility was one of the main themes to be covered in each interview, but in many cases the quality and credibility of information in the group was brought up by the participant at the beginning or early in the interview, before being explicitly asked about it.

</section>

<section>

### Ethical considerations

Upon agreeing to participate in the study, each participant was sent an information sheet and a consent form to read and sign. The participant was informed of the purpose of the study, her rights and role as a participant in the study, and what kinds of data were to be collected. The participant was informed that her participation in the study was completely voluntary, and that she had the right to ask to change or refuse to answer any question or to withdraw from the study at any time without any consequences.

In each interview, the participant was advised not to give any information that she would prefer not be made public. Participants’ privacy was protected by removing all information that could be used to identify a participant (e.g., name, child’s name, city name) from the interview transcripts. In order to protect the participants’ identities, the participants will be referred to throughout the study by pseudonyms (P1, P2, and so on).

</section>

<section>

### Data analysis

The analysis of the data was ongoing throughout the data collection period. Thus reflections and findings from previous interviews were used to inform and refine the questions for the subsequent interviews, so we could follow up to confirm initial findings or to ask additional questions.

The software package Atlas.Ti was used to organise the interview transcripts and to help facilitate the data analysis. The constant comparison method was used to analyse the data ([Lincoln and Guba, 1985](#Lin85)). Each transcript was read multiple times and the key ideas in each transcript were coded. For instance, we started by looking for instances where information credibility was brought up in the discussion by the researcher or the participants and started to identify and code patterns that emerged in those instances. The codes were constantly compared and refined across the different transcripts. Afterwards, the codes were grouped under different themes. We decided to narrow our focus by choosing a few themes for closer and more detailed analysis. In the end, focus was placed on credibility assessments made in relation to the group and group members rather than with regard to information sources external to the group.

</section>

<section>

## Empirical findings

The main aim of this study was to explore if and why mothers perceived their Facebook group as containing credible information, and the approaches used to make credibility assessments of information provided by other members in the group. We present our findings first by discussing the participants’ views on the group as an information source and the types of information found there. Secondly, we illustrate the cultural tools the participants relied on to negotiate their credibility assessments within the group.

</section>

<section>

### Perceptions of the Facebook group as an information source

The interviews show that although the participants used the Facebook group to seek information, almost everyone said they did not consider the group as a credible information source. This view was formed both based on who was posting in the group (source) and on the information posted (message). Some participants were concerned about what they thought was a tendency among some other group members to consume information from the group without any critical assessment and to consult it as their only source. P4 noted: _‘it feels like that some people, you know, they only go there for information, which is not me. And I think it’s a bit dangerous just to put all your eggs in one basket’._

Contributing to a sense of mistrust among group members was the difficulty of verifying members’ backgrounds as expressed by P13: _‘[…] some of them I don’t [trust their advice]…but it’s just because they are strangers and I don’t know them and I don’t know where they are coming from’_.

Several participants voiced their concerns over the quality of some information shared in the group, especially concerning vaccinations. They pointed out the dangers of spreading false and misleading information which some members claimed was _‘researched and scientific’_, as P8 pointed out. P12 stated similar concerns over false information that she believed was being circulated in the group:

> My fear, and I know other people [pause] there are some microbiologists and there are some other people who were part of the group, and their fear that somebody who doesn’t know better will look at these websites and make their decision based on misinformation... the danger, I feel, is if somebody is easily influenced and can’t pick out the good information from the bad information.

Most, if not all, participants indicated that the Facebook group is not a credible information source. One explanation offered by participants for why they use the group even if they do not consider the information to be credible was that the main aim for being a member of the group was to connect and socialise with other mothers who share a similar situation of taking care of same-age children or even live in a nearby neighbourhood. P3 stated:

> I spend a lot of my time on social media since my husband travels all the time... So quite often I am alone in the evenings and I find it as, like, a way of semi-socializing, because otherwise I wouldn’t meet anyone in those evenings during the week when I am alone and I have to take care of the kids. So I use it as a social mean.

She further explained that when she had an information need which was not practical (e.g., sales or shops to buy toys), she would look for that information somewhere else and would not rely on the group members to provide her with that type of information:

> I see there are some people who go there on social media [the Facebook group] … and they ‘oh I am worried about this and what should I do?’ You know, I am more of someone who would do the research myself if I was worried about that. (P3)

Further, an observation that would explain why these participants actually use information from the Facebook group, despite their scepticism about its credibility as an information source, may be related to their experiences as social media users. All the participants in the study indicated that they had been using social media for three years and more, and they had been members of the group for a minimum of a year, and frequently visited and actively participated in group activities on a daily basis. So it is reasonable to assume that their association or acquaintance with the group over some time could be the reason why they continued to use the group for certain information. How experience on a platform may influence perceptions of credibility has also been observed in previous studies by Van Der Heide and Lim ([2016](#Van16)) and Johnson and Kaye ([2015](#Joh15)).

</section>

<section>

### Assessments of information on various topics

In the discussions with the participants about why and on what topics they sought or encountered information, the participants expressed that they tended to employ different ways to assess the credibility of information depending on the topic and their purposes for using the information. For instance, several participants stressed their lack of trust in information shared within the group when they sought health and medically related information. Information on these topics was talked about as too _‘serious’_ to seek from group members via Facebook since many of the participants did not believe that the other members had proper medical knowledge or expertise and thus deemed the information not to be credible. P8 explained:

> I feel like some of the support is really nice, but many times I don’t think that, like, they’re medically credible to be answering some questions. So, if I have, like, some really serious questions, I usually talk to my physician or my daughter’s physician about it or I have a few close friends, who are actually in the medical field that, I mean, I can discuss with.

Several participants explained that they would look to or recommend to check with other online (e.g., official government websites) and offline sources (e.g., physicians, family members, and friends) to search for this type of information. P17 gives an example of when an offline traditional source should have been consulted, instead of asking in the group:

> I remember something happened to the child... the child got hurt or something and she was asking what should I do, should I go to the doctor, or should I read this and that, and everything. Rather than using the Facebook at this moment, you should go to the doctor.

For the most part, the participants explained that despite their perception that information shared in the group was not credible, they continued to be members and tended to be interested in experiences by others for the purpose of reassurance. Their interest was thus primarily prompted by a wish to see if other people were experiencing similar situations with their children and how they dealt with them:

> You know, when you have a small child who is ill you look up things all the time... if it’s normal? What should we do?... so I would read a lot about what people do in that kind of situation, but it’s more just to see that 'ok, others have gone through this, so it’s fine'. But then when it comes to the treatment or what would I do? I would definitely only trust the official health care site. (P7)

Interestingly, the participants also discussed information that they could only seek or get via the group. This information was often context-specific, that is, participants would seek information from group members who might have been in similar situations and shared similar characteristics as mothers (e.g., got same-age children, live in Sweden). This information was described by the participants as personalised and they often sought this type of information in the group because it was either difficult to find information from formal sources because they used the local language or because family members from whom they could get advice lived in distant places. For instance, the participants sought information in the group regarding parenting practices (e.g., sleep training, disciplining children, children’s nutrition), recommendations for local family activities, help in applying and finding suitable and local day care, restaurant and product recommendations and reviews, and information about how to find and navigate different Swedish systems (e.g., the social insurance system). P11 in our chat interview expressed what motivated her reliance on the group although she is originally from Sweden:

> There is so much I don't know about life in Sweden, especially as a parent, and I had so many questions on how to get started. I have a couple of Swedish friends who have children, but they don't seem to be able to get their head around how little I really know about so many things. I have asked questions and they give answers that require background information I don't have. I don't for example know (more no[w] than two months ago) what roles different authorities have, heck I don't even know which ones exist! I've lived in this society, but as a school child never really had to deal with it.

Under such circumstances, when the participants failed to find information via traditional information channels, often perceived as credible sources, the Facebook group was considered by the participants, to some extent, as a credible information source. This was similar to how educators approached Wikipedia on certain topics in Francke _et al.’s_ ([2011](#Fra11)) study.

</section>

<section>

### Cultural tools used in credibility assessments

In this section, we present findings about five cultural tools used by participants in their credibility assessments. These include language use and writing style, expertise, life experience, educational background, and similar lifestyles, parenting values and worldviews.

</section>

<section>

#### Language use and writing style

One of the cultural tools that the participants used when making credibility assessments of other members’ advice was the language and writing style used in Facebook posts. Language or writing style was regarded as a tool used to make credibility assessments when requesting or reading advice from unknown members in the group. In most situations, language use provided cues of the educational level and background of other members, which for several of the participants was considered to be an indicator of the quality of the information. Text characteristics, such as writing style, grammar, and spelling, were examples of cues relied upon to make such assessments. One participant, who was not very active in the group, reflected on how difficult it was for her to know the background of others on social media, especially in the group. However, she explained that the writing style used in posts often attracted her attention:

> For the social media stuff you don’t know who these people who are posting [are]. So I would probably, in terms of evaluating how much I take them seriously, or what information I know about them, is how they write. It’s like the only thing I know.(P1)

The ability to write and communicate with others through well-articulated text in the group was perceived by P1 as a sign of an individual’s knowledge, which motivated her belief that the information shared was more likely to be credible. She added:

> If you read the kind of things people are posting and then they write in like not that clear way, like I don’t know, like I wouldn’t believe her as a trusted source of information. I wouldn’t take her advice [laughs]. But like someone who is, you know, they seem to know what they are doing and they could communicate clearly, I would be, like, more [likely] to believe what they say about what you are supposed to do if your dagis [kindergarten] is serving that food or something.

Participant 8 shared a similar sentiment regarding the use of language: _‘I think the easiest way for me to filter most things is... I judge on people’s ability to like use proper grammar and spelling’_.

Both participants in the examples above relied on language use as a culturally valued tool to inform their assessment of information credibility. The culturally valued notion ([Gee, 2014](#Gee14)) of _‘being able to write’_ and _‘to use proper grammar’_ was considered an indication of a certain level of education which, in turn, led the participant to regard the information as more credible.

</section>

<section>

#### Expertise

Another cultural tool frequently invoked by the participants to aid their credibility assessments was the perceived expertise of the person providing information. The participants tended to consider some members as cognitive authorities based on their professional training and credentials. Such cognitive authorities were considered to possess expertise and competence that qualified them to give advice on topics on which others had no expertise. P13 describes below how she trusted the information provided by another member in the group whom she considers a cognitive authority. She valued her advice on matters related to vaccination since she believed the member held proper expertise on the subject:

> I think she does vaccine for a living. I think she is like a research scientist or something, so I kind of trust her opinion. I mean I still would not base a life decision on her but I trust her more because I know she has the educational background... and then the link she posted was from a reputable journal... and the study seemed legitimate.

P8 looked to her cognitive authorities (e.g., biologists, physicians) in order to assess the credibility of documents (e.g., a medical journal article) linked to in the group:

> I mean, like, if it’s [links posted in the group] in a proper medical journal and you run it by, like, other biologists and things like [pause] and [you have] friends who work, you know, in that field, I am gonna trust it. You know especially after talking with my physician or my daughter’s physician, then I am like 'ok, this is trustworthy'.

In the example above, when the participant made her assessment of the credibility of a document (the study posted through a link in the group) she relied on the endorsement of the document made by her cognitive authority in the form of familiar professionals in the field (see [Metzger _et al._, 2010](#Met10)).

</section>

<section>

#### Life experience

In other cases, other members’ first-hand experiences on specific topics were valued by the participants when assessing credibility within the group. The participants assumed that if a member has older children or more children, for example, she is presumably more knowledgeable about specific issues than those with fewer or younger children. Experienced members were often perceived as cognitive authorities who influenced and shaped the participant’s decisions of what to trust and believe when filtering through information given by others within the group. This is clearly illustrated by P19:

> They already have quite a lot of children but they seem to have two or three, and I have one kid. Then I think ‘yeah, I don’t have that much experience, I’ve only had one child and I already know that this is completely wasted money'. So if you have three kids then you probably know even better … and then I read a post … I ask the question that mum is commenting and then I remember that she has three kids and then she probably knows about that in that field.

In the examples above, the participants pointed to expertise and experience as something that influenced their view of people or documents as cognitive authorities. Nevertheless, in some cases, expertise and experience came into conflict in the participants’ stories. For instance, whereas the expertise of qualified others was valued by P8 in an earlier account, in the following quotation she explained why she valued other mothers’ first-hand experiences over the expertise of a nurse.

> I already got an opinion from my BVC [child healthcare centre] nurse, which I didn’t really appreciate so I wanted to get like another perspective... so just like that our daughter is skinny and that we really need to push her to eat solid foods and everything.... The nurse is trying to … she only works with Swedish children and she doesn’t know [that] other children are skinny and completely healthy.... I wasn’t, like, to ask another Swede because I am sure they would say my child is skinny so I wanted to ask mums about their children because I think it is better to get their perspective.

The participant believed that the expertise and skills of the nurse, in Wilson’s words, are only limited to her domain of knowledge, that is, caring for Swedish children. So P8 tended to trust the advice provided by group members since she believed that they could relate to her situation and have a first-hand experience relevant to her problem.

</section>

<section>

#### Educational background

The participants constructed their credibility assessments based on their previous knowledge, especially developed from education or training, and on what was valued within their community (e.g., profession), when they judged the credibility of information provided by others. This can be seen in the description provided by P1, who motivated her focus on correct language (see above) by pointing to her own educational background and how it influenced her assessment of credibility:

> The quality of someone’s writing is something that I took my entire education [pause] and my parents are like English professors and, like, everything is, you know, in that direction... I think I have a prejudice about language because it is my profession and it is something that I really notice.

P19 also described how her education and profession influenced how she assessed the advice shared in the group:

> There have been discussions about sleep training and I read those comments, and that my child psychiatry background pops in, that I never sleep train my baby like that, like cry it out, but that’s my point of view, I would not do that.... I was working in that field, and I will never let my six–month old baby lie alone in his bed... from a child psychological background, like, the attachment of the baby and mother, I think it is a bad idea.

Such statements show how the participants emphasise that immaterial cultural tools such as knowledge from their educational background shape their assessments of credibility.

</section>

<section>

#### Similar lifestyles, parenting values and worldviews

Additional cultural tools that were employed by the participants to mediate their credibility assessments included drawing on similar lifestyles, parenting values and worldviews. Our analysis shows that the participants thus assessed if the information given by others corresponded to their prior held beliefs or values and used this in order to determine whose advice to trust in the group. If advice given by a group member was convincing and considered intrinsically plausible ([Wilson, 1983](#Wil83)), the participant would consider this advice to be credible. Some participants mentioned that when they sought or read information in the group, they were not really looking for new information but rather seeking confirmation from others that supported what they already believed. Some participants valued the advice from others who were from the same socioeconomic or cultural background, as in this quote from P12: _‘I guess frequently they would be sociologically similar to me, so they would be middle class, come from a middle class home, and probably come from Western Europe or US or Canada’._

She continued to describe that she most appreciated advice that told her what she wanted to hear, while at the time she ignored advice from people that she did not view as being _‘on the same page’_:

> There are certain people whose opinions I would respect more than others... because I’ve seen their postings and know that basically we are on the same page. And there are the other people of course who I know are not like me at all and we don’t hold the same opinions on a lot of things, and I would read their answers with interest but probably [...] I would not pay as much attention to them because we are really not that similar. (P12)

Similarly, P3 described the process through which she formed her knowledge about other members’ advice and their beliefs within the group, which in turn influenced her decision of whose advice to take or dismiss. She noted:

> I know which people every time will say 'I would take the medication and listen to my doctor' for those questions when they are asked. I know who they are, it is predictable for me because I know the opinions that they have because I have been on there for long enough to know that two or three people will always say 'Oh, nutrition will never work, that’s useless!'... So that’s how I make my judgments when I am reading posts as well.

P13 dismisses information and advice outright from other members who she already knows do not make the same parenting decisions as her:

> I take more weight in some people’s replies than others. Because I know there is like anti-vaccine people on there and people that are against feeding your children greens and stuff like that, that I really don’t pay any attention to what they say.

Further, P19 explained how her non-religious worldview influenced her evaluation:

> I think one thing that I have a hard time for and I wouldn’t listen to them, but that’s just me again, I am really an atheist, all those people who pray for the lord to get healthy or something worked out well, I don’t get them seriously so I think those ones I would not really listen to.

To summarise, an important tool for finding trustworthy sources within the group is to identify and learn about similar others who share the participant’s values, lifestyles and worldviews. This confirms previous findings from for instance Metzger _et al._ ([2010](#Met10)), who discovered that finding like-mined others was one of the main strategies of credibility assessment online employed by their participants.

</section>

<section>

## Discussion

In this section, we revisit the observations made in the previous section on empirical findings in order to further discuss how these findings may be understood from a theoretical perspective on credibility.

</section>

<section>

### The role of the Facebook group as a credible information source

A key barrier for trusting information in the Facebook group was that information was shared by people unfamiliar to and/or unidentified by the participants. This lack of familiarity meant there was little information that could be used to assess a person’s potential credibility. Similar distrust of unfamiliar or unidentifiable sources has been observed more generally in digital, networked environments (e.g., [Metzger and Flanagin, 2013](#Met13); [Metzger _et al._, 2010](#Met10)). Rieh _et al._ ([2010](#Rie10)) found that social processes are important components of credibility assessment in sources with user-generated content. However, even when identities were known, the participants in our study still expressed concern over the credibility and quality of information shared in the group, especially when it concerned a topic where the consequences of choices made was thought to be serious, such as vaccine-related information.

The Facebook group was not primarily seen as a source of credible information. A majority of the participants suggested that they treated the Facebook group rather as a space to connect with others and to meet new people for a variety of purposes. However, what made the users of the group continue to go to the group to seek certain information, despite many participants’ expressed view that it lacked credible information, was that they had developed strategies for using it for particular purposes, for which there was a lack of other suitable sources. The interviews showed that the participants had developed skills that helped them navigate the group and know its group dynamics. This helped them determine what kinds of questions to ask, what types of advice were offered by other members, what the other members’ backgrounds and expertise were, whom to turn to for information and advice in the group, and so on. This shows that they drew on familiarity with and skills in using the online platform. Similar use, for credibility assessments, of knowledge of a platform and of the people who participate regularly in communities on the platform, has also been discussed in previous studies (e.g.[Francke and Sundin, 2010](#Fra10); [Johnson and Kaye, 2015](#Joh15); [Hocevar _et al._, 2014](#Hoc14); [Stavrositu and Sundar, 2008](#Sta08); [Van Der Heide and Lim, 2016](#Van16)). These studies suggest that familiarity and experience with an online platform help people better locate information and make well-informed credibility assessments.

</section>

<section>

### Credibility of knowledge claims in different domains

As discussed above, the Facebook group was not considered to be a very credible information source by the participants, but they did turn to it for information in certain situations and on certain topics. Such differences in how people assess the credibility of different types of information is consistent with previous research, which has shown that for instance factual or subjective information, what topic one seeks information about, which platform is used, and whether or not one will be examined or assessed, influence how and where people turn for information and what information they consider to be credible ([Francke _et al._, 2011](#Fra11); [Gao _et al._, 2015](#Gao15); [Jeon and Rieh, 2013](#Jeo13); [Jeon and Rieh, 2014](#Jeo14); [Metzger _et al._, 2010](#Met10); [Rieh _et al._, 2010](#Rie10); [Sundin and Francke, 2009](#Sun09)).

Some patterns emerged regarding the topics and purposes for which the participants, to a greater or lesser extent, were reluctant or willing to turn to the group for information and which concerned how credibility was perceived. We will discuss how the participants related to knowledge claims in two different domains in terms of knowledge gained by a source in a _professional_ or _personal capacity_.

When it was a matter of information that concerned topics on which people are quite aware that there is an established corpus of formalised knowledge, the participants often expressed that they would turn to sources of what we will here refer to as _professional knowledge_. By professional knowledge, we mean _knowledge gained and transmitted predominantly in a person’s or organisation’s professional capacity or as an extension of that capacity_. Such topics included information about medical and health issues. There are limitations to talking about this domain of knowledge in terms of professional knowledge, as the term _professional_ may indicate a strong codification of particular types of expertise. However, it has the advantage of including both second-hand knowledge in the form of _‘[f]ormal propositional knowledge’_ ([Lloyd, 2014, p. 101](#Llo14)) gained through for instance education and literature and first-hand knowledge gained from professional experience. The latter is not captured by the alternative terms formal or formalised, even though formality is one aspect of professional knowledge. This category draws on such established ways of motivating cognitive authority as occupational specialisation, formal education, and reputation among equals ([Wilson, 1983, pp. 21-23](#Wil83)). It is primarily exercised and attributed within a particular domain or _‘sphere of authority’_ ([Wilson, 1983, p. 19](#Wil83)).

There were also examples where participants described how they turned to sources they trust outside of the Facebook group on matters of health and medicine, sources such as family members or close friends that were not explicitly identified as possessing professional knowledge. This indicates that choices made by the participants regarding whom they perceive to be a cognitive authority within this sphere could have to do with aspects other than professional knowledge. In this case, the close relationship a participant has with her cognitive authority, an authority who is presumably expected to have the participant’s best interests at heart, seems to be a basis for attributing a higher value to trustworthiness than to competence (two components of credibility identified by [Wilson, 1983, p. 15](#Wil83)).

The participants described how they were more likely to turn to the Facebook group for information connected to what we will here term _personal knowledge_, that is, _knowledge gained through personal experience in a personal (rather than professional) capacity_. Such personal knowledge is largely experiential. It concerns, for instance, parents’ experience of raising children; knowledge about local situations that is of importance for example when choosing a day care institution, a product or activity; and knowledge of how to negotiate situations such as interactions with authorities. In these cases, first-hand knowledge or informal retelling of others’ first-hand knowledge contribute heavily to the authority of the information source. The knowledge domain is generally non-structured, situation-specific and anecdotal. Canagarajah’s ([2002, p. 244](#Can02)) characterising of local knowledge as _‘context bound, community specific, and nonsystematic because it is generated ground up through social practice in everyday life’_ is relevant here (see also [Lloyd, 2014](#Llo14)), although the situations described by the participants mainly lack the negotiation of dominant discourses that Canagarajah is concerned with. However, in this particular group, information seeking and credibility assessments are associated with negotiating life in an unfamiliar society in which the participants are placed in situations where they sometimes lack the knowledge and resources required to handle everyday life situations successfully. In such situations, sources that can offer the information needed, or communicate it in ways that are understandable, become important and what is perceived as successful accomplishment ([Wilson, 1983, p. 23](#Wil83)) with regard to, for example, raising children or dealing with authorities in the new society become reasons for attributing cognitive authority. For this group of participants, the Facebook group became a place in which such cognitive authorities could be found and consulted.

</section>

<section>

### Expertise, experience and intrinsic plausibility

In assessing the credibility of information in the Facebook group, the participants drew on various cultural tools. These were described above in terms of language use and writing style, expertise, life experience, educational background, and similar lifestyles, parenting values and worldviews. The tools were used primarily when assessing the credibility of information or information sources on topics concerning personal knowledge, for which the participants turned to the group for help or inspiration, but they were also in some cases used for assessing credibility on topics concerning professional knowledge.

In a few cases, where professional knowledge on for example medical topics was assessed appreciatively, the participants indicated that they considered the formal expertise of the information source. Professional training and competence, for instance, was perceived as a merit that qualified an individual to be considered a cognitive authority. The potentially non-professional skills associated with writing correct and well-articulated English were also used as a tool for evaluating credibility. Although not necessarily an expression of professional knowledge, good writing was treated as an indicator of the information provider’s knowledge. It is worth noting in this context that English is not the native language of all group members. The use of language skills such as format, punctuation, spelling and grammar have also been pointed out in previous research as influencing the way people assess the credibility of information (e.g., [Jeon and Rieh, 2013](#Jeo13); [Jeon and Rieh, 2014](#Jeo14); [Kim, 2010](#Kim10); [Metzger _et al._, 2010](#Met10)).

Personal knowledge gained through first-hand experience was also something that participants considered indicative of credibility. For instance, for a mother who had just given birth to her first child, mothers with more than one child, who were considered experienced mothers, were appreciated sources of information. These mothers were often perceived as cognitive authorities despite the fact that they were not trained as, for instance, caregivers or nurses specialising in child care. There was even an instance where the local and experiential knowledge of mothers with the same background as the participant was considered more credible when determining the potential health risks for a skinny child than a nurse’s professional knowledge which was thought to lack the ethnic and cultural understanding which influenced the knowledge of other mothers.

The previous example is an expression of how participants on a large number of occasions in the interviews referred to how they trusted personal knowledge from people who were similar to themselves. These Facebook users often identified other members who had similar lifestyles and values as themselves and used similarity as a strategy to evaluate credibility and filter out information. They were selective in using information from others depending on whether these others shared their lifestyle or had mutual values and worldviews.

We interpret this reliance on similar values and experiences as an expression of the importance played by intrinsic plausibility ([Wilson, 1983](#Wil83)) in the credibility assessments made in the Facebook group. Similar findings have been discussed by Metzger and Flanagin ([2013](#Met13); see also [Metzger _et al._, 2010](#Met10)) as expressions of a self-confirmation heuristic and an expectancy violation heuristic. The values and experiences that the participants expected the information to confirm in order to be plausible were in some cases epistemic; they had to do with which knowledge claims the participant sided with on such issues as vaccination or sleep training. The participant’s education was sometimes closely associated with such assessments, which were based on professional knowledge gained through education. Thus, the plausibility of the information content was assessed in relation to professional knowledge.

In other cases the participants based plausibility or lack thereof on shared (religious) beliefs, social class, and culture of origin. The emphasis placed on the correct use of language can also be viewed as an expression of intrinsic plausibility. A post on the Facebook wall that was not grammatically correct did not, in these cases, meet with the expectations the reader had on a credible message and thus failed the test of intrinsic plausibility ([Wilson, 1983, p. 25](#Wil83)).

Information that was considered intrinsically plausible was thus also often information that was appreciated and that confirmed the participant’s expectations. As such, the information served not only, and often not primarily, to fulfil an information need, but to confirm the participants’ current practices and beliefs by providing assurance that others were doing and believing the same things.

</section>

<section>

## Conclusion

The main purpose of this study was to understand how members of a Facebook group perform credibility assessments of information within the group and whether they regard the Facebook group as a credible source of information. Through the lens of a sociocultural understanding of credibility assessments we analysed how credibility of online and user-generated content is perceived and how credibility assessments of the information is mediated through cultural tools.

The study shows that the participants expressed that the Facebook group was generally not a credible source of information to them but rather they used it as a space that provided social support and an opportunity to meet like-minded individuals. At the same time, when speaking about what they did in the group, they explained that they turned to the group as a source for locating credible information within particular domains. The domains for which the group was considered an acceptable source were primarily domains where the participants wanted to learn from other participants’ personal knowledge gained from their experiences as a parent or as a parent in the local community. For issues within domains where professional knowledge was valued, the participants often turned to other sources.

The Facebook group was characterised by a combination of familiar and unfamiliar others, of the sharing and seeking of information from both professional and personal domains of knowledge and from first- and second-hand knowledge. This mixture of knowledge domains and of information sources meant that the participants employed a number of cultural tools in assessing credibility. They drew on the formal expertise and lived experience of others but also on what they found intrinsically plausible in the various knowledge claims and in how these were expressed. An important tool in the latter assessments was to identify others who were similar to oneself with regard to values, lifestyle and worldviews. It is likely that this is a tool used broadly in assessments in Facebook groups, as well as in other social media environments. Similar findings have been made in studies with other groups and platforms (e.g., [Metzger _et al._, 2010](#Met10)). However, it should be acknowledged that the group of participants in the present study was fairly homogeneous, despite our attempts to recruit a more heterogeneous group. Future studies with participants who represent a broader variety of educational and cultural backgrounds, age groups as well as different genders, could provide further insights into the findings presented here. We conclude by noting that the information and credibility assessment activities of mothers (and quite likely also of fathers), as a group of active social media users with complex and varied information and credibility needs, merits further study.

</section>

<section>

## Acknowledgements

We wish to thank the study participants for sharing their time and their information experience on Facebook with us. We would also like to thank Annemaree Lloyd and other colleagues at the University of Borås for their constructive feedback on a previous version of this article. We appreciate the constructive feedback on the text from the two anonymous reviewers. This study was funded by the Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS) at the University of Gothenburg and the University of Borås, Sweden (registration number: 349-2006-146).

## <a id="author"></a>About the authors

**Ameera Mansour** is a PhD candidate at the Swedish School of Library and Information Science, University of Borås, Sweden. She is also a member of the Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS) at the University of Gothenburg and the University of Borås, Sweden. She is particularly interested in information literacy, credibility assessments, information activities, and mothers' practices on social media. She can be contacted at: [ameera.shanaah@gmail.com](mailto:ameera.shanaah@gmail.com).  
**Helena Francke** is Associate Professor at the Swedish School of Library and Information Science, University of Borås, Sweden and on the board of the Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS) at the University of Gothenburg and the University of Borås, Sweden. Her research interests concern the credibility and authority of sources, information literacy, scholarly communication, and document studies. She can be contacted at [helena.francke@hb.se](mailto:helena.francke@hb.se).

</section>

<section>

## References

<ul>
<li id="Boe12">Boellstorff, T., Nardi, B., Pearce, C. &amp; Taylor, T. L. (2012).<em>Ethnography and virtual worlds: a handbook of method</em>. Princeton, NJ: Princeton University Press.</li>
<li id="Can02">Canagarajah, S. (2002). Reconstructing local knowledge.<em>Journal of Language, Identity &amp; Education, 1</em>(4), 243-259.</li>
<li id="Col96">Cole, M. &amp; Wertsch, J. V. (1996). Beyond the individual-social antinomy in discussions of Piaget and Vygotsky. <em>Human development, 39</em>(5), 250-256.</li>
<li id="Dug15">Duggan, M., Lenhart, A., Lampe, C. &amp; Ellison, N. B. (2015). <em><a href="http://www.webcitation.org/6gZA77izV">Parents and social media</a></em>. Pew Research Centre. Retrieved from http://www.pewinternet.org/2015/07/16/parents-and-social-media/ (Archived by WebCite&reg; at http://www.webcitation.org/6gZA77izV) </li>
<li id="Fla14">Flanagin, A. J., Hocevar, K. P. &amp; Samahito, S. N. (2014). Connecting with the user-generated web: how group identification impacts online information sharing and evaluation.<em>Information, Communication &amp; Society, 17</em>(6), 683-694.</li>
<li id="Fla07">Flanagin, A. J. &amp; Metzger, M. J. (2007). The role of site features, user attributes, and information verification behaviors on the perceived credibility of web-based information. <em>New Media &amp; Society, 9</em>(2), 319-342.</li>
<li id="fog03">Fogg, B. J. (2003). Prominence-interpretation theory: explaining how people assess credibility online. In <em>CHI'03 extended abstracts on human factors in computing systems</em> (pp. 722-723). New York, NY: ACM Press.</li>
<li id="Fra10">Francke, H. &amp; Sundin, O. (2010). <a href="http://www.webcitation.org/6gZ9THGTi "> An inside view: credibility in Wikipedia from the perspective of editors</a>. <em>Information Research: Special supplement: Proceedings of the Seventh International Conference on Conceptions of Library and Information Science, 15</em>(3), paper colis702. Retrieved from http://www.informationr.net/ir/15-3/colis7/colis702.html (Archived by WebCite&reg; at http://www.webcitation.org/6gZ9THGTi)</li>
<li id="Fra11">Francke, H., Sundin, O. &amp; Limberg, L. (2011). Debating credibility: the shaping of information literacies in upper secondary schools. <em>Journal of Documentation, 67</em>(4), 675-694.</li>
<li id="Gao15">Gao, Q., Tian, Y. &amp; Tu, M. (2015). Exploring factors influencing Chinese user’s perceived credibility of health and safety information on Weibo. <em>Computers in Human Behavior</em>, 45, 21-31.</li>
<li id="Gee14">Gee, J. P. (2014).<em>An introduction to discourse analysis: theory and method</em>. Abingdon, UK: Routledge.</li>
<li id="Hil08">Hilligoss, B. &amp; Rieh, S. Y. (2008). Developing a unifying framework of credibility assessment: construct, heuristics, and interaction in context. <em>Information Processing &amp; Management, 44</em>(4), 1467-1484.</li>
<li id="Hoc14">Hocevar, K. P., Flanagin, A. J. &amp; Metzger, M. J. (2014). Social media self-efficacy and information evaluation online. <em>Computers in Human Behavior,</em> <em>39</em>, 254-262.</li>
<li id="Jeo13"> Jeon, G. Y. &amp; Rieh, S. Y. (2013).<a href="http://www.webcitation.org/6gmiVgprn "><em>Do you trust answers? Credibility judgments in social search using social Q&amp;A sites</em></a>. Paper presented at Computer Supported Cooperative Work and Social Computing (CSCW 2013) Workshops on Social Media Question Asking, San Antonio, TX. Retrieved from  http://research.microsoft.com/en-us/events/cscw2013smqaworkshop/credibility_social_qanda.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6gmiVgprn) </li>
<li id="Jeo14">Jeon, G. Y. &amp; Rieh, S. Y. (2014). <a href="http://www.webcitation.org/6gmamp4VV ">Answers from the crowd: how credible are strangers in social Q&amp;A?</a> In <em>Proceedings of iConference 2014</em> (pp. 663–668). Urbana-Champaign, IL: iSchools.  Retrieved from http://hdl.handle.net/2027.42/106410 (Archived by WebCite&reg; at http://www.webcitation.org/6gmamp4VV) </li>
<li id="Joh13">Johnson, T. J. &amp; Kaye, B. K. (2013). The dark side of the boon? Credibility, selective exposure and the proliferation of online sources of political information. <em>Computers in Human Behavior, 29</em>(4), 1862-1871.</li>
<li id="Joh15">Johnson, T. J. &amp; Kaye, B. K. (2015). Reasons to believe: influence of credibility on motivations for using social networks. <em>Computers in Human Behavior</em>, <em>50</em>, 544-555.</li>
<li id="Kim10">Kim, S. (2010).<a href="http://www.webcitation.org/6gZBqjrZ9">Questioners’ credibility judgments of answers in a social question and answer site.</a> <em>Information Research, 15</em>(2), paper 432. Retrieved from http://www.informationr.net/ir/15-2/paper432.html (Archived by WebCite&reg; at http://www.webcitation.org/6gZBqjrZ9)</li>
<li id="Koz98">Kozulin, A. (1998).<em>Psychological tools: a sociocultural approach to education</em>. Cambridge, MA: Harvard University Press.</li>
<li id="Lam12">Lampe, C., Vitak, J., Gray, R. &amp; Ellison, N. (2012). Perceptions of Facebook’s value as an information source. In J. Konstan, E. H. Chi &amp; K. Höök, <em>Proceedings of the SIGCHI Conference on Human Factors in Computing Systems</em> (pp. 3195-3204). Austin, TX: ACM Press.</li>
<li id="Lim15">Lim, Y. &amp; Van Der Heide, B. (2015). Evaluating the wisdom of strangers: the perceived credibility of online consumer reviews on Yelp. <em>Journal of Computer-Mediated Communication, 20</em>(1), 67-82.</li>
<li id="Lim12">Limberg, L., Sundin, O. &amp; Talja, S. (2012).<a href="http://www.webcitation.org/6gmZwOIwB">Three theoretical perspectives on information literacy.</a> <em>Human IT, 11</em>(2), 93-130. Retrieved from https://humanit.hb.se/article/view/69 (Archived by WebCite&reg; at http://www.webcitation.org/6gmZwOIwB)</li>
<li id="Lin85">Lincoln, Y. &amp; Guba, E. (1985). <em>Naturalistic inquiry</em>. Beverly Hills, CA: Sage Publications.</li>
<li id="Llo14">Lloyd, A. (2014). Following the red thread of information in information literacy research: recovering local knowledge through interview to the double. <em>Library &amp; Information Science Research, 36</em>(2), 99-105.</li>
<li id="Met07">Metzger, M. (2007). Making sense of credibility on the web: models for evaluating online information and recommendations for future research. <em>Journal of the American Society for Information Science and Technology, 58</em>(13), 2078–2091.</li>
<li id="Met13">Metzger, M. J. &amp; Flanagin, A. J. (2013). Credibility and trust of information in online environments: the use of cognitive heuristics. <em>Journal of Pragmatics</em>, <em>59</em>, 210-220.</li>
<li id="Met10">Metzger, M. J., Flanagin, A. J. &amp; Medders, R. B. (2010). Social and heuristic approaches to credibility evaluation online. <em>Journal of Communication, 60</em>(3), 413-439.</li>
<li id="Mor14">Morris, M. R. (2014). Social networking site use by mothers of young children. In S. Fussell &amp; W. Lutters (Eds.),   <em>Proceedings of the 17th ACM Conference on Computer Supported Cooperative Work &amp; Social Computing</em> (pp. 1272-1282). Baltimore, MD: ACM Press.</li>
<li id="Mor12">Morris, M. R., Counts, S., Roseway, A., Hoff, A. &amp; Schwarz, J. (2012). Tweeting is believing? Understanding microblog credibility perceptions. In S. Poltrock &amp; C. Simone (Eds.), <em>Proceedings of the ACM 2012 Conference on Computer Supported Cooperative Work</em> (pp. 441-450). Seattle, WA: ACM Press.</li>
<li id="Mor10">Morris, M. R., Teevan, J. &amp; Panovich, K. (2010). What do people ask their social networks, and why? A survey study of status message Q&amp;A behavior. In E. Mynatt, G. Fitzpatrick, S. Hudson, K. Edwards &amp; T. Rodden (Eds.), <em>Proceedings of the CHI '10 Conference on Human Factors in Computing Systems</em> (pp. 1739-1748). Atlanta, GA: ACM Press.</li>
<li id="Rie02">Rieh, S. Y. (2002). Judgment of information quality and cognitive authority in the Web.<em>Journal of the American Society for Information Science and Technology, 53</em>(2), 145-161.</li>
<li id="Rie07">Rieh, S. Y. &amp; Danielson, D. R. (2007). Credibility: a multidisciplinary framework. <em>Annual Review of Information Science and Technology, 41</em>(1), 307-364.</li>
<li id="Rie10">Rieh, S. Y., Kim, Y. M., Yang, J. Y. &amp; St. Jean, B. (2010).<a href="http://www.webcitation.org/6gmmrkc2q ">A diary study of credibility assessment in everyday life information activities on the web: preliminary findings.</a> In A. Grove (Ed.), <em>Proceedings of the ASIS&amp;T Annual Meeting</em> (pp. 1-10). Silver Spring, MD: ASIS&amp;T. Retrieved from https://www.asis.org/asist2010/proceedings/proceedings/ASIST_AM10/submissions/182_Final_Submission.pdf (Archived by WebCite&reg; at  http://www.webcitation.org/6gmmrkc2q)</li>
<li id="Sch13">Schoenebeck, S. Y. (2013). The secret life of online moms: anonymity and disinhibition on YouBeMom.Com. In <em>Proccedings of Seventh International Conference on Weblogs and Social Media (ICWSM 2013)</em> (pp. 555-562). Cambridge, MA: AAAI Press.</li>
<li id="Sil08">Silverman, D. &amp; Marvasti, A. (2008). <em>Doing qualitative research: a compartive guide</em>. Chicago, IL: Sage.</li>
<li id="Sta08">Stavrositu, C. &amp; Sundar, S. S. (2008). If Internet credibility is so iffy, why the heavy use? The relationship between medium use and credibility. <em>Cyberpsychology &amp; Behavior, 11</em>(1), 65-68.</li>
<li id="Sun09">Sundin, O. &amp; Francke, H. (2009). <a href="http://www.webcitation.org/6gZJTtICu ">In search of credibility: pupils’ information practices in learning environments.</a> <em>Information Research, 14</em>(4), paper 418. Retrieved from http://www.informationr.net/ir/14-4/paper418.html (Archived by WebCite&reg; at http://www.webcitation.org/6gZJTtICu) </li>
<li id="Syn13">Syn, S. Y. &amp; Kim, S. U. (2013). <a href="http://www.webcitation.org/6oG0pFTVB ">The impact of source credibility on young adults’ health information activities on Facebook: preliminary findings</a>. In A. Grove (Ed.), <em>Proceedings of the ASIS&amp;T Annual Meeting</em> (pp. 1-4). Silver Spring, MD: ASIS&amp;T. Retrivevd from http://onlinelibrary.wiley.com/doi/10.1002/meet.14505001122/epdf (Archived by WebCite&reg; at http://www.webcitation.org/6oG0pFTVB)</li>
<li id="Van16">Van Der Heide, B. &amp; Lim, Y. S. (2016). On the conditional cueing of credibility heuristics: the case of online influence. <em>Communication Research, 43</em>(5), 672-693.</li>
<li id="Wer98">Wertsch, J. V. (1998). <em>Mind as action</em>. New York, NY: Oxford University Press.</li>
<li id="Wil83">Wilson, P. (1983). <em>Second-hand knowledge: an inquiry into cognitive authority.</em> Westport, CT: Greenwood Press.</li>
<li id="Zuc16">Zuckerberg, M. (2016, January 27). <a href="https://www.facebook.com/photo.php?fbid=10102621100061261&set=a.529237706231.2034669.4&type=3&theater">We just announced our quarterly results and shared an update on our community’s progress to connect the world </a>[Facebook status update]. Retrieved from https://www.facebook.com/photo.php?fbid=10102621100061261&amp;set=a.529237706231.2034669.4&amp;type=3&amp;theater.</li>
</ul>

</section>

</article>