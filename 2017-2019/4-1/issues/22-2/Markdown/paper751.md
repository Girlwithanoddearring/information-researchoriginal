<header>

#### vol. 22 no. 2, June, 2017

</header>

<article>

# E-health literacy and the vaccination dilemma: an Israeli perspective

## [Noa Aharony](#author) and [Romina Goldman](#author).

> **Introduction**. Parents' hesitancy to use vaccines is becoming more prevalent in Israel. The aim of the current research is to examine whether there is a relationship between the increase in parents’ level of involvement in the decision to vaccinate their children, and the increase in searching for and filtering information from online electronic sources. The current research is based on Hochbaum and Rosenstock's health belief model from 1952 and the Lily model, which relates to e-health literacy.  
> **Method**. The research population consists of 200 parents of children born from 2000 forward. Researchers surveyed five aspects to gather data: demographic characteristics, vaccination intentions, level of online information sources use, knowledge on the subject of vaccination and perceived e-health literacy.  
> **Results**. Findings show that most of the parents who hesitated over whether to vaccinate their children decided not to vaccinate them. These parents believe they can search for information on the Internet and they surf different kinds of sites (government, social network sites, forums and alternatives).  
> **Conclusion**. This study expanded the theoretical literature that deals with e-health and vaccination and may help directors of medical institutions to find solutions of how to lower the number of individuals who refuse to vaccinate their children.

<section>

## Introduction

Parents are usually asked to vaccinate their children against chickenpox, measles, diphtheria, hepatitis a and b, polio, tetanus and many other diseases. However, parents' refusal to use vaccines is becoming more prevalent in Israel. Various studies have suggested that vaccination refusers are older, and have higher levels of education than average ([Salmon _et al._, 2005](#sal05); [Smith _et al._, 2011](#smi11)). Further, Mikulak ([2011](#mik11)) suggests that vaccination refusers are characterised by a rational decision resulting from an intensive process of information research. Brunson ([2013](#bru13)) maintains that conformer (non-hesitant) parents usually vaccinate without being aware of the diseases against which they are vaccinating and do not question medical authorities. However, active (hesitant) parents tend not to rely on the information supplied to them by others and carry out independent research. After evaluating the information, these parents agree to the vaccination, postpone it, or refuse it. This process may repeat itself several times ([Brunson, 2013](#bru13)).

## Theoretical background

### Vaccination refusers

Individual or organised resistance to vaccination has been known since the end of the 18th century. In 1998, the controversy over vaccination made headlines after the publication of Andrew Wakefield’s paper in the British medical journal, _The Lancet_. Wakefield pointed to a causal connection between the triple vaccine against measles, mumps and rubella, and autism in children and babies. This paper led to extensive media coverage, a drastic reduction in the number of children who were vaccinated ([Middleton and Wolfe, 2013](#mid13)), and a renewed outbreak of these diseases in England ([Burgess, Burgess and Leask, 2006](#bur06); [Wolfe and Sharp, 2002](#wol02)).

In February 2004, the _Sunday Times_ newspaper published an investigation that cast doubts on Wakefield’s research, discovering that the research had been based on a conflict of interest. Twelve years after Wakefield's publication, _The Lancet_ retracted the paper ([Deer, 2011](#dee11)). Even though the scientific community considered Wakefield’s findings invalid, the anti-vaccination movement still uses them to advance its agenda by maintaining that the researcher was a victim of the scientific community and pharmaceutical companies ([Leask, Chapman, Hawe and Burgess, 2006](#lea06); [Middleton and Wolfe, 2013](#mid13); [Poland and Jacobson, 2012](#pol12)).

Unlike resistance to vaccination in the 18th Century, which was based on religious beliefs, the present-day phenomenon of vaccination refusal is associated with a rational decision based on a thorough process of information research ([Bazin, 2009](#baz09); [Betsch and Sachse, 2012](#betsac12); [Mikulak, 2011](#mik11)). This phenomenon is part of a post-modern medical paradigm, in which patients are actively engaged in searching for information before making any decision on medical matters ([Kata, 2011](#kat11)). Kata maintains that parents who struggle with vaccination decisions are more likely to use a wider variety of resources before making a decision on whether to vaccinate their children. A study conducted in Israel ([Aharon, Nehama, Shmuel, and Baron-Epel, 2012](#aha12)) examined the demographic data of children who were not fully vaccinated, revealing a difference between those who were not vaccinated as a consequence of their parents’ decision and those who were not vaccinated because of organisational barriers. Organizational barriers refer to technical and financial difficulties that prevent the population from reaching family health centres ([Aharon, 2011](#aha11)). Findings reveal that there is a greater likelihood that a child will not be vaccinated because of a parental decision if the mother is Jewish, has an academic degree, and is over 39 years old. However, the likelihood that a child will not be vaccinated because of organisational barriers is higher among Muslims, having parents who do not have an academic degree, and having a young mother ([Aharon _et al._, 2012](#aha12)). In a study that was carried out in the United States, Smith, Chu and Barker ([2004](#smi04)) found that children who were vaccinated tended to be black, had a younger mother who was not married and did not have a college degree, and lived in a household near the poverty level. The unvaccinated children tended to be white, had a married mother who had a college degree and with an annual income exceeding $75,000\.

### The health belief model

The health belief model was developed at the beginning of the 1950s to predict conditions under which people may act to prevent, screen for, or control illness ([Champion and Skinner, 2008](#cha08)). More recent studies that were based on the health belief model tried to predict in which conditions the individual will decide if to vaccinate or not ([Coe, Gatewood and Moczygemba, 2012](#coe12); [Smith _et al._, 2011](#smi11)). The model can be applied to parents to demonstrate the process used by non-conformer or hesitant parents who decide to refuse vaccinations. The characteristics of the model, applied to the process of vaccination refusal, are:

#### Perceived susceptibility

The individual’s subjective perception of his or her chances of becoming ill with a particular disease ([Champion and Skinner, 2008](#cha08); [Strecher and Rosenstock, 1997](#str97)). Extensive vaccination programmes have caused the eradication of many diseases. Young parents have usually not experienced these diseases, thus their subjective perception may be that the chance of their child becoming ill with the disease is very low ([Bond and Nolan, 2011](#bon11)). Another study ([Brewer _et al._, 2007](#bre07)) proposed that those who perceived themselves to be more susceptible to an illness were more likely to be vaccinated.

#### Perceived severity

The individual’s subjective perception of the severity of the disease and its medical consequences (death, disability, or pain) or its social consequences (work, family life and social relations) ([Champion and Skinner, 2008](#cha08); [Strecher and Rosenstock, 1997](#str97)). Vaccination refusers believe that the diseases are not as dangerous as medical authorities describe, and it is possible to heal them through natural processes without external (specifically, normative medical) intervention ([Kasmaei _et al._, 2014](#kas14); [Luthy, Beckstrand, Callister and Cahoon, 2012](#lut12)).

#### Perceived benefits

The individual’s perceived benefits of the various available actions for reducing the disease threat. Parents who do not believe in the disease’s severity do not see any benefit in taking preventative action ([Champion and Skinner, 2008](#cha08); [Strecher and Rosenstock, 1997](#str97)). Vaccination refusers do not perceive the disease is as dangerous as commonly believed and therefore any preventative measure, such as vaccination, will not be effective, and will even be perceived as harmful ([Luthy _et al._, 2012](#lut12)).

#### Perceived barriers

These are factors that prevent the individual from taking preventative measures, such as severe side effects of the treatment ([Champion and Skinner, 2008](#cha08); [Strecher and Rosenstock, 1997](#str97)). Side effects that may appear as a consequence of vaccination (such as autism, allergies, paralysis, etc.) are perceived as more dangerous than the disease itself ([Betsch and Sachse, 2012](#betsac12)).

### The Internet as a tool for seeking information on medical topics

A study that examined information-seeking behaviour on medical topics on the Internet in the United States discovered that young people, especially white adults earning more than $75,000 a year, and those with bachelor’s degrees and higher, are more likely to search the Internet to solve medical problems. When the participants in the study were asked where they searched for information, 77% answered that initially they used search engines such as Google, Bing or Yahoo; 13% began their search on professional Websites such as WebMD; 2% used Websites such as Wikipedia, and only 1% used social media such as Facebook ([Fox and Duggan, 2013](#fox13)). Moreover, 52% believe that ‘nearly all’ or ‘most’ of the medical information on the Internet is reliable and 70% agreed that the information they find on the Internet influences their choice of medical treatment ([Kata, 2010](#kat10)).

The ability to use reliable sources of information and discern between high and low quality health information is very important ([Austin, Pinkleton, Austin and Van de Vord, 2012](#aus12)). Studies show that people who search for health information frequently base their impression of the reliability of a Website according to its design and ease of use, not on the quality of the information it contains ([Austin _et al._, 2012](#aus12)). The individual can interpret the information they retrieve in various ways, without any of these interpretations considered invalid. Therefore, Websites that create the most coherent and concise content will have a greater influence on information consumers ([Betsch and Sachse, 2012](#betsac12)).

In light of studies showing that most people who look for online information do not carry out an in-depth investigation of the sources of that information, it can be assumed that the likelihood of reaching unreliable information by this method (and also receiving messages against vaccination) will increase ([Bean, 2011](#bea11); [Kata, 2011](#kat11)). These messages may influence the decision to vaccinate. In addition, the transition to Web 2.0 has made the issue of the quality of health information appearing in different Websites more problematic. The fact that anyone can contribute easily to health discourse, frequently in an anonymous way, is an increasing source of concern regarding the accuracy and quality of the information ([Tafuri _et al._, 2014](#taf14); [Witteman and Zikmund-Fisher, 2012](#wit12)).

Previous studies maintained that the Internet had little influence on the process of deciding whether to vaccinate ([Fredrickson _et al._, 2004](#fre04); [Salmon _et al._, 2005](#sal05)). However, more recent studies present a change of this trend. A study which explored the associations between parents’ Internet information-seeking and their knowledge, attitudes and beliefs about human HPV vaccination, found that those who searched for information on the Internet had higher knowledge, and positive attitudes and beliefs about HPV vaccination ([Mcree, Reiter and Brewer, 2012](#mcr12)). Further, Harmsen _et al._ ([2013](#har13)) propose that although parents perceived health care workers as the most reliable source of information about vaccination, they also searched the Internet as they feel that the information that was given to them did not present the vaccination side effects.

A study showed that though there was no change in the intention to vaccinate within the group that received information in favour of vaccination but those who surfed anti-vaccination Websites increased their perceived risk of vaccination by 28%: according to the findings, viewing typical vaccine-critical Websites for only 5-10 minutes increases the perception of risk regarding vaccinations ([Betsch, Renkewitz, Betsch and Ulshöfer, 2010](#bet10)). A recent study supports Betsch _et al._'s findings ([Dunn, Leask, Zhou, Mandl and Coiera, 2015](#dun15)). In accordance with this, Ruiz and Barnett ([2015](#rui15)), who conducted a semantic analysis of the words retrieved on Google when searching for information about the HPV virus, found that there were more negative than positive words concerning vaccinations and their side effects and other aspects.

Betsch and Sachse ([2012](#betsac12)) identified characteristics that can increase the likelihood that users who conduct Internet research will acquire unreliable information on vaccination. These are: lower socioeconomic status, lower cognitive ability, lower health literacy, and less knowledge about vaccination. These findings contrast with previous ones showing that people who oppose vaccination were parents with a higher socioeconomic status and higher cognitive ability ([Smith _et al._, 2011](#smi11)).

Other studies that examined the characteristics of Websites identified with the anti-vaccination movement ([Bean, 2011](#bea11); [Davies, Chapman, and Leask, 2002](#dav02); ; [Zimmerman _et al._, 2005](#zim05)) suggested that behind these Websites were groups that promoted an anti-vaccination agenda by presenting themselves as legitimate authorities with scientific credibility. Most of the Websites propounded the scientific validity of their claims by referencing literature dominated by self-published works and the alternative medical press ([Davies, Chapman, and Leask, 2002](#dav02)). Betsch’s ([2011](#bet11)) research found that people with less knowledge on vaccination, who are more likely to conduct searches, will do so using less complex search terms which lead to more anti-vaccination Websites.

### Health literacy in the era of the Internet

Health literacy relates to the individual’s abilities to cope with the complex demands necessary in order to deal with the modern health system. The health literate person has to know how to place in the right context their own health, the health of their family, and the health of the community, in order to understand factors which affect them, and know how to handle them. Researchers have shown that health literacy can lead to increased health knowledge, improved individual health, reduced government expenses, fewer hospitalisations and, finally, contributed to reduced use of these services ([Sørensen _et al._, 2012](#sor12)).

The growing influence of the Internet as an initial source of information on medical topics has given rise to a new concept: e-health literacy. According to Norman and Skinner ([2006a](#nor06a), p. 1), this type of literacy relates to the ‘_ability to seek, find, understand, and appraise health information from electronic sources and apply the knowledge gained to addressing or solving a health problem_’. Norman and Skinner ([2006a](#nor06a)) translated their definition of e-health literacy to the Lily model, which is based on a combination of six different kinds of literacies, and aims to ensure an optimal health decision-making process. The six kinds of literacies can be divided into two types: analytic, which includes traditional literacy, media literacy and information literacy, and the content type that includes computer literacy, scientific literacy and health literacy.

Schulz and Nakamoto ([2011](#sch11)) maintained that the flexibility afforded by the Internet as a tool for making medical decisions can sometimes be dangerous rather than beneficial. Instead of encouraging the information consumer to apply a methodical, thorough approach, search engines are liable to encourage users to take shortcuts, and thus the individual can ignore critical considerations. Further, studies show that searching anti-vaccination sites increases the individual's level of insecurity regarding vaccination ([Betsch, _et al._, 2010](#bet10); [Dunn _et al._, 2015](#dun15); [Guidry, Carlyle, Messner and Jin, 2015](#gui15)).

## Problem statement

The Pew Research Center ([2015](#pew15)) reveals that about 83% of the public in the US say that vaccination for diseases such as measles, mumps and rubella (MMR) is safe for healthy children, 9% think that such vaccinations are not safe and 7% report that they do not know. Thus, due to the growth in the number of vaccination refusers, the question arises as to whether those who are non-conformers and hesitant about vaccination make their decision after a sophisticated and comprehensive search. Do they search for information in medically reliable sites? Are they impressed by the ease of use of the site and its design? Are they influenced by videos and pictures presented or the quality of information it contains? Do they know how to distinguish between reliable and invalid information? This question is important because researchers assume that as people are exposed to the enormous amount of information on the Internet, they may be affected by irrelevant, false information before making their medical decisions ([Schulz and Nakamoto, 2011](#sch11)).

The current study focuses on hesitant and non-hesitant parents and its aim is to examine whether there is a difference between these two groups concerning the relationship between the increase in parents’ level of involvement in the decision to vaccinate their children, and the increase in searching for and filtering information from online electronic sources. The study will focus on parents’ perceived level of online literacy (e-health), their level of electronic information sources use and types of information sources, and their actual level of knowledge on the subject of vaccination.

The research may expand the theoretical literature that deals with e-health and vaccination ([Betsch and Wicker, 2012](#betwic12)). In addition, the study results may help directors of medical institutions to find solutions of how to lower the number of individuals who refuse to vaccinate their children.

The objectives of this study are to investigate: (a) What kind of sites do hesitant and non-hesitant parents use before they make their decisions concerning vaccination?; (b) Are there differences between hesitant and non-hesitant parents concerning their perceived e-health literacy?; (c) Are there differences between hesitant and non-hesitant parents concerning their level of knowledge about vaccinations?

## Hypotheses

Assuming that parents’ level of searching for and filtering information from different online electronic sources, their level of e-health literacy, and their actual knowledge on the subject of vaccination, may affect their decision whether to vaccinate or not, the following hypotheses can be made:

> H1\. Hesitant parents will have broader knowledge concerning vaccination than non-hesitant parents.  
> H2\. No significant difference concerning perceived level of e-health literacy will be found between hesitant and non-hesitant parents.  
> H3\. Hesitant parents will have greater likelihood to refuse vaccination than non-hesitant parents.  
> H4\. Hesitant parents’ use of online electronic resources will be greater than non-hesitant parents.  
> H5\. Parents who refuse to vaccinate their children (refuser parents) will have higher perceived level of e-health literacy and more limited knowledge about vaccination than non-refuser and hesitant parents.  
> H6\. Parents who hesitated in the past to vaccinate and, after searching for information, decided to vaccinate, have higher perceived e-health literacy, and higher knowledge concerning vaccination than non-refuser and non-hesitant parents.  
> H7\. Refuser parents will use more information resources than non-refuser parents.  
> H8\. Parents who received information on Web 2.0 sites have lower knowledge about vaccination than parents who received information on other sites

## Method

### Data collection

The study took place in December 2014 and January 2015\. The researchers wanted to reach parents who use electronic information sources, thus, they addressed Facebook groups, and forums whose main concern was parents’ discourse on raising children, focusing on their education and health. In the first stage they asked Facebook groups and forums’ managers for permission to disseminate questionnaires to their groups. After getting their consent, they sent questionnaires to two forums and to ten Facebook groups. The researchers received 210 questionnaires. The questionnaire was distributed to a convenience sample; therefore it does not represent the entire population of parents in Israel.

### Data analysis

Quantitative data analysis consisted of Pearson correlations in order to examine the relationships between the research variables. MANOVA and a chi-squared test analyses were performed to compare the various groups.

The research sample consisted of 210 parents of children born between 2000 and 2014\. Of these, 174 (82.9%) were females and 36 (17.1%) were males. 125 (59.5%) were hesitant, and 85 (40.4%) were non-hesitant. As for their level of education, 49 (22.3%) were high-school graduates, 91 (43.4%) had a bachelor degree, and 70 (34.3%) had a masters degree.

### Measures

Researchers surveyed five aspects to gather data: demographic characteristics, vaccination intentions, level of online information sources use, knowledge on the subject of vaccination and perceived e-health literacy (see Appendix for copy of questionnaire).

The demographic section consists of six questions.

The intentions section consists of three questions that make it possible to determine whether the parent is a refuser, non-hesitant or hesitant. A refuser parent is one who does not vaccinate their children, a non-hesitant is a parent that either vaccinates their children or refuses to vaccinate them, and a hesitant one is a parent that is uncertain as to whether to vaccinate or not.

The section on level of online information sources use has four statements rated on a 5-point Likert scale (1= no knowledge; 5= a great amount of knowledge). Respondents were asked to mark the degree to which they used online information sources in order to receive information on the subject of vaccination. Internal reliability was examined by Cronbach’s Alpha and found to be 0.79 ([Dunsky-Baranga, 2007](#dun07)).

The section on knowledge of the subject of vaccination consists of nine statements. The aim was to examine the parents’ level of scientific knowledge on the subject of vaccination. Respondents were asked to mark: ‘_True_’, ‘_False_’, or ‘_I don’t know_’. The tool was developed by Zingg and Siegrist ([2012](#zin12)) and has a reliability of ρ = 0.79\. The questionnaire’s reliability was tested using the Mokken scale (equivalent to the Cronbach’s Alpha scale) ([Zingg and Siegrist, 2012](#zin12)). For the purpose of analysing the data, the participant received one point for each correct answer, and no points for each incorrect or unknown answer.

The perceived e-health literacy section consists of nine questions rated on a five-point Likert scale (1= strongest disagreement; 5= strongest agreement). Its aim was to examine how parents perceive their level of electronic medical literacy. The tool is taken from the model developed by Norman and Skinner ([2006b](#nor06b)). The internal reliability was examined by Cronbach’s Alpha and found to be 0.88 ([Jordan, Osborne, and Buchbinder, 2011](#jor11); [Norman and Skinner, 2006b](#nor06b)).

## Results

A one-way MANOVA was performed to examine whether there are differences between hesitant and non-hesitant parents concerning knowledge about vaccination and perceived e-health literacy. The analysis revealed a significant difference concerning parents' knowledge, F (2.207) = 20.16, p &lt; 0.001, η<sup>2</sup>= 1.16. Means, standard deviations and the MANOVA analysis for each group are presented in Table 1.

<table><caption>

**Table 1: Means and standard deviations of parents' perceived e-health literacy and knowledge**</caption>

<tbody>

<tr>

<th rowspan="2">Measures</th>

<th colspan="2">Hesitant parents</th>

<th colspan="2">Non-hesitant parents</th>

<th rowspan="2">F(2.207)</th>

<th rowspan="2">η<sup>2</sup></th>

</tr>

<tr>

<th>Mean</th>

<th>StdDev</th>

<th>Mean</th>

<th>StdDev</th>

</tr>

<tr>

<td>Perceived e-health literacy</td>

<td>3.59</td>

<td>0.85</td>

<td>3.52</td>

<td>0.90</td>

<td>0.32</td>

<td>0.00</td>

</tr>

<tr>

<td>Knowledge</td>

<td>3.20</td>

<td>2.63</td>

<td>5.48</td>

<td>2.73</td>

<td>36.76*</td>

<td>0.15</td>

</tr>

<tr>

<td colspan="7"

>\* p &lt; 0.001</td>

</tr>

</tbody>

</table>

Table 1 shows that non-hesitant parents have a greater knowledge about vaccination than hesitant parents. This result contrasts with H1\. H2 was supported, revealing no significant difference between perceived e-health literacy of hesitant parents and non-hesitant parents. In order to examine differences between hesitant and non-hesitant parents concerning postponing vaccination, a chi square test was conducted. Significant differences between hesitant and non-hesitant were found concerning delaying vaccination, χ<sup>2</sup>=65.23, p &lt; 0.001, and concerning future vaccination, χ<sup>2</sup>=96.33, p &lt; 0.001\. Figure 1 presents hesitant and non-hesitant parents' opinions concerning vaccination postponement.

<figure>

![Hesitant and non-hesitant parents' opinions concerning vaccination postponement](../p751fig1.jpg)

<figcaption>Figure 1: Hesitant and non-hesitant parents' opinions concerning vaccination postponement</figcaption>

</figure>

Table 2 presents hesitant and non-hesitant parents' opinions concerning future vaccination.

<table><caption>

**Table 2: Hesitant and non-hesitant parents' opinions concerning future vaccination**</caption>

<tbody>

<tr>

<th rowspan="2">Category</th>

<th colspan="4">Future vaccination</th>

<th colspan="2">Total</th>

</tr>

<tr>

<th>Yes</th>

<th>%</th>

<th>No</th>

<th>%</th>

<th>Number</th>

<th>%</th>

</tr>

<tr>

<td>Hesitant</td>

<td>30</td>

<td>24.0</td>

<td>95</td>

<td>76.0</td>

<td>125</td>

<td>100</td>

</tr>

<tr>

<td>Non-hesitant</td>

<td>79</td>

<td>92.9</td>

<td>6</td>

<td>7.1</td>

<td>85</td>

<td>100</td>

</tr>

<tr>

<td>Total</td>

<td>109</td>

<td>51.9</td>

<td>101</td>

<td>48.1</td>

<td>210</td>

<td>100</td>

</tr>

</tbody>

</table>

Results support H3, confirming that hesitant parents have a greater likelihood of refusing vaccination than non-hesitant parents. In order to examine if there are differences concerning Internet searching habits between hesitant and non-hesitant parents, researchers conducted a chi square test. Table 3 presents the analysis.

<table><caption>

**Table 3: Hesitant and non-hesitant parents' Internet searching habits**</caption>

<tbody>

<tr>

<th rowspan="2">Type of Website  
used and %</th>

<th colspan="2">Non-hesitant</th>

<th colspan="2">Hesitant</th>

<th rowspan="2">χ<sup>2</sup></th>

</tr>

<tr>

<th>Yes</th>

<th>No</th>

<th>Yes</th>

<th>No</th>

</tr>

<tr>

<td>Government</td>

<td>58</td>

<td>67</td>

<td>41</td>

<td>43</td>

<td>0.18</td>

</tr>

<tr>

<td>%</td>

<td>46.4</td>

<td>53.6</td>

<td>49.4</td>

<td>50.6</td>

<td> </td>

</tr>

<tr>

<td>Alternative</td>

<td>47</td>

<td>78</td>

<td>10</td>

<td>75</td>

<td>17.08*</td>

</tr>

<tr>

<td>%</td>

<td>37.6</td>

<td>62.4</td>

<td>11.8</td>

<td>88.2</td>

<td> </td>

</tr>

<tr>

<td>Forum</td>

<td>44</td>

<td>81</td>

<td>11</td>

<td>74</td>

<td>12.97*</td>

</tr>

<tr>

<td>%</td>

<td>35.2</td>

<td>64.8</td>

<td>12.9</td>

<td>87.1</td>

<td> </td>

</tr>

<tr>

<td>Social network</td>

<td>54</td>

<td>71</td>

<td>21</td>

<td>64</td>

<td>7.54*</td>

</tr>

<tr>

<td>%</td>

<td>43.2</td>

<td>56.8</td>

<td>24.7</td>

<td>75.3</td>

<td> </td>

</tr>

</tbody>

</table>

Table 3 shows that hesitant parents search more in alternative sites, forums, and social network sites than non-hesitant parents. This result confirms H4, which proposed that hesitant parents' Internet use is greater than that of non-hesitant parents.

When participants were asked if they intend to vaccinate their children in the future, 51.9% answered that they will vaccinate them, and 48.1% that they will not vaccinate them.

In order to examine if there is a change between parents' intentions before and after their information search concerning future vaccination, researchers conducted a chi square test that showed a significant difference between the two dates, χ<sup>2</sup>=96.33, p &lt; 0.001\. Figure 2 presents hesitant and non-hesitant parents' intentions concerning future vaccination after their information search.

<figure>

![Hesitant and non-hesitant parents' intentions concerning future vaccination after their information search](../p751fig2.jpg)

<figcaption>Figure 2: Hesitant and non-hesitant parents' intentions concerning future vaccination after their information search</figcaption>

</figure>

Figure 2 shows that 92.9% of non-hesitant parents reported that after receiving the information, they intend to vaccinate their children. Among hesitant parents, 24.0% reported that after receiving the information, they intend to vaccinate their children, 49.6% decided not to vaccinate their children, and 26.4% continued hesitating.

In order to examine H5, that suggested that refuser parents will have higher perceived e-health literacy and lower knowledge about vaccination, a one-way MANOVA comparing three groups (non-refuser, refuser and hesitant parents) was conducted. A significant difference was found among the three groups, _F_(4.412)=34.72, p &lt; 0.001, η<sup>2</sup>=0.25\. Means, standard deviations, and the MANOVA analysis for each group are presented in Table 4.

<table><caption>

**Table 4: Means and standard deviations of parents' perceived e-health literacy and knowledge about vaccination**</caption>

<tbody>

<tr>

<th rowspan="2">Measures</th>

<th colspan="2">Non-refuser parents</th>

<th colspan="2">Refuser parents</th>

<th colspan="2">Hesitant parents</th>

<th rowspan="2">F(4.412)</th>

<th rowspan="2">η<sup>2</sup></th>

</tr>

<tr>

<th>Mean</th>

<th>StdDev</th>

<th>Mean</th>

<th>StdDev</th>

<th>Mean</th>

<th>StdDev</th>

</tr>

<tr>

<td>Perceived e-health literacy</td>

<td>3.58</td>

<td>0.90</td>

<td>3.78</td>

<td>0.79</td>

<td>3.06</td>

<td>0.75</td>

<td>8.53*</td>

<td>0.07</td>

</tr>

<tr>

<td>Knowledge about vaccination</td>

<td>5.78</td>

<td>2.49</td>

<td>1.96</td>

<td>1.96</td>

<td>3.00</td>

<td>2.27</td>

<td>61.29**</td>

<td>0.37</td>

</tr>

<tr>

<td colspan="9">

\* p &lt; 0.01; ** p &lt; 0.0001</td>

</tr>

</tbody>

</table>

Table 4 shows significant differences within the two measures. However, the differences concerning knowledge about vaccination among the three groups were greater than differences concerning perceived e-health literacy. Table 4 shows that non-refuser parents have the greatest knowledge about vaccination, followed by hesitant parents and refuser parents have the least knowledge. Further, table 4 presents that the refuser parents have the highest level of perceived e-health literacy, followed by non-refuser parents, then by hesitant parents. Researchers performed post-hoc tests, using the Bonferroni correction and found significant differences among the three groups. Means show that hesitant parents estimate they have the lowest level of perceived e-health literacy in comparison with two other groups.

H6, that assumed that parents who hesitated in the past to vaccinate and, after searching for information, decided to vaccinate, will have higher level of perceived e-health literacy, and greater knowledge about vaccination, was rejected. In order to see whether there are differences between refuser, and non-refuser parents, and between hesitant and non-hesitant parents a two way MANOVA (refuser non-refuser X hesitant non-hesitant) was conducted. No significant difference was found between refuser and non-refuser parents concerning their decision to vaccinate or not, F (2.203)=1.97, p &gt; 0.05, and no significant difference was found between vaccination or non-vaccination and the hesitation process, F (4.408)=1.47, p &gt; 0.05.

H7 and H8 suggested that refuser parents will use more information resources than other parents, and that parents who received information on Web 2.0 sites, have lower knowledge about vaccination than parents who received information on other sites. In order to examine whether there are differences among the three groups (non-refuser, refuser parents, and hesitant parents) concerning their information search in different sites (government, alternative, forums, and social network sites) a one way MANOVA was conducted. It found a significant difference among the three groups, F(8.410)=11.7, p &lt; 0.001, η<sup>2</sup>=0.19\. Means, standard deviations, and the MANOVA analysis for each group are presented in Table 5\.

<table><caption>

**Table 5: Means and standard deviations of parents' information search**</caption>

<tbody>

<tr>

<th rowspan="2">Websites</th>

<th colspan="2">Non-refuser parents</th>

<th colspan="2">Refuser parents</th>

<th colspan="2">Hesitant parents</th>

<th rowspan="2">F(8.410)</th>

<th rowspan="2">η<sup>2</sup></th>

</tr>

<tr>

<th>Mean</th>

<th>StdDev</th>

<th>Mean</th>

<th>StdDev</th>

<th>Mean</th>

<th>StdDev</th>

</tr>

<tr>

<td>Government</td>

<td>2.71</td>

<td>1.17</td>

<td>2.75</td>

<td>1.24</td>

<td>2.40</td>

<td>0.84</td>

<td>1.23</td>

<td>0.01</td>

</tr>

<tr>

<td>Alternative</td>

<td>1.77</td>

<td>1.05</td>

<td>3.28</td>

<td>1.24</td>

<td>2.65</td>

<td>1.25</td>

<td>36.74*</td>

<td>0.26</td>

</tr>

<tr>

<td>Forum</td>

<td>1.87</td>

<td>1.13</td>

<td>3.21</td>

<td>1.10</td>

<td>2.94</td>

<td>1.39</td>

<td>30.20*</td>

<td>0.22</td>

</tr>

<tr>

<td>Social network</td>

<td>2.21</td>

<td>1.32</td>

<td>2.65</td>

<td>1.25</td>

<td>3.16</td>

<td>1.23</td>

<td>11.38*</td>

<td>0.09</td>

</tr>

<tr>

<td colspan="9">

\* p &lt; 0.0001</td>

</tr>

</tbody>

</table>

Table 5 shows that refuser parents search more than other parents in government sites, alternative sites, and forums, and that hesitant parents search more in social network sites. Table 5 presents significant differences among the three groups concerning searching in alternative sites, forums, and social network sites. The greatest differences were found regarding searching in alternative sites and in forums.

In order to examine the relationship between perceived e-health literacy, knowledge about vaccination and searching in different sites, researchers performed Pearson correlations, which are presented in Table 6.

<table><caption>

**Table 6: Pearson correlations between perceived e-health literacy, knowledge about vaccination and searching in different sites (N = 210)**</caption>

<tbody>

<tr>

<th>Measures</th>

<th>Government  
sites</th>

<th>Alternative  
sites</th>

<th>Forums</th>

<th>Social network  
sites</th>

</tr>

<tr>

<td>Perceived e-health literacy</td>

<td>0.30**</td>

<td>0.07</td>

<td>0.12</td>

<td>0.21**</td>

</tr>

<tr>

<td>Knowledge about vaccination</td>

<td>0.15*</td>

<td>-0.51***</td>

<td>-9.40***</td>

<td>-0.15*</td>

</tr>

<tr>

<td colspan="5">

\*p &lt; 0.05 **p &lt; 0.01 \***p &lt; 0.0001</td>

</tr>

</tbody>

</table>

Table 6 presents a significant, positive correlation between perceived e-health literacy and government sites and social network sites. Hence, the higher the perceived e-health literacy, the more people search for information in government sites and social network sites. Further, significant negative correlations were found between knowledge about vaccination and alternative sites, forums, and social network sites, and a positive correlation was found between knowledge about vaccination and government sites. It should be noted that the correlations between knowledge about vaccination and government sites and social network sites is lower in comparison with alternative sites and forums. It seems that the more knowledge parents have about vaccination the less they search in alternative sites, forums, social network sites, and the more in government sites.

### Demographic variables

In order to examine whether there are differences between hesitant and non-hesitant parents concerning demographic variables (age, child's age, education, and income), a one way MANOVA was performed, that found a significant difference between the two groups, F(4.205)=3.22, p &lt; 0.05, η<sup>2</sup>=0.06\. Means, standard deviations, and the MANOVA analysis for each group are presented in Table 7\.

<table><caption>

**Table 7: Means and standard deviations of parents' demographic variables and hesitancy**</caption>

<tbody>

<tr>

<th rowspan="2">Measures</th>

<th colspan="2">Hesitant parents</th>

<th colspan="2">Non-hesitant parents</th>

<th rowspan="2">F(4.205)</th>

<th rowspan="2">η<sup>2</sup></th>

</tr>

<tr>

<th>Mean</th>

<th>StdDev</th>

<th>Mean</th>

<th>StdDev</th>

</tr>

<tr>

<td>Age</td>

<td>33.92</td>

<td>5.68</td>

<td>36.21</td>

<td>5.15</td>

<td>8.86*</td>

<td>0.04</td>

</tr>

<tr>

<td>Child's age</td>

<td>2.86</td>

<td>2.69</td>

<td>4.12</td>

<td>3.16</td>

<td>9.66*</td>

<td>0.04</td>

</tr>

<tr>

<td>Education</td>

<td>2.52</td>

<td>1.04</td>

<td>2.70</td>

<td>1.24</td>

<td>1.26</td>

<td>0.01</td>

</tr>

<tr>

<td>Income</td>

<td>3.29</td>

<td>1.29</td>

<td>3.35</td>

<td>1.24</td>

<td>0.10</td>

<td>0.00</td>

</tr>

<tr>

<td colspan="7">

\*p &lt; 0.01</td>

</tr>

</tbody>

</table>

Table 7 presents significant differences concerning parents' age and child's age. It seems that the non-hesitant parents are older and that the child's age is higher in comparison with hesitant parents.

In order to see whether there are differences between refuser and non-refuser parents concerning demographic variables, (age, child's age, education, and income), a one way MANOVA was performed, that found a significant difference between the two groups, F (4.205)=2.67, p &lt; 0.05, η<sup>2</sup>. Means, standard deviations, and the MANOVA analysis for each group are presented in Table 8.

<table><caption>

**Table 8: Means and standard deviations of parents' demographic variables and refuser status**</caption>

<tbody>

<tr>

<th rowspan="2">Measures</th>

<th colspan="2">Non-refuser parents</th>

<th colspan="2">Refuser parents</th>

<th rowspan="2">F(4.205)</th>

<th rowspan="2">η<sup>2</sup></th>

</tr>

<tr>

<th>Mean</th>

<th>StdDev</th>

<th>Mean</th>

<th>StdDev</th>

</tr>

<tr>

<td>Age</td>

<td>35.85</td>

<td>5.72</td>

<td>33.51</td>

<td>5.11</td>

<td>9.40*</td>

<td>0.04</td>

</tr>

<tr>

<td>Child's age</td>

<td>4.05</td>

<td>3.43</td>

<td>2.46</td>

<td>1.80</td>

<td>15.99**</td>

<td>0.07</td>

</tr>

<tr>

<td>Education</td>

<td>2.56</td>

<td>1.20</td>

<td>2.64</td>

<td>1.03</td>

<td>24.0</td>

<td>0.00</td>

</tr>

<tr>

<td>Income</td>

<td>3.24</td>

<td>1.28</td>

<td>3.42</td>

<td>1.25</td>

<td>1.04</td>

<td>0.01</td>

</tr>

<tr>

<td colspan="7">

\*p &lt; 0.01 **p &lt; 0.001</td>

</tr>

</tbody>

</table>

Table 8 presents similar findings to the ones found with hesitant and non-hesitant parents, meaning there are significant differences concerning parents' age and child's age. It seems that the non-refuser parents are older and that the child's age is higher in comparison with refuser parents.

## Discussion

The research reported here focuses on hesitant and non-hesitant parents and its aim is to examine whether there is a difference between these two groups concerning the relationship between the increase in parents’ level of involvement in the decision to vaccinate their children, and the increase in searching for and filtering information from online electronic sources. Further, the study explored parents’ perceived level of online literacy (e-health), their level of electronic information sources use and their actual level of knowledge on the subject of vaccination.

H1 suggested that hesitant parents will have broader knowledge concerning vaccination than non-hesitant parents. The Betsch ([2011](#bet11)) psychological model suggests that hesitant parents will search for information through various channels, will evaluate the vaccination benefits and risks, and then will decide whether to vaccinate or not. In contrast, non-hesitant parents will not search for information and therefore will not have broad knowledge about the vaccination. However, this hypothesis was rejected, and the findings reveal that hesitant parents had lower knowledge concerning vaccination than non-hesitant parents. We may assume that non-hesitant parents are not a tabula rasa and probably have prior knowledge about vaccination. We should add that the current research investigated only parents' use of online resources and we may speculate that non-hesitant parents received information from other traditional, authorised resources such as doctors, nurses etc. This finding was already presented in Jones _et al._ ([2012](#jon12)), who revealed that 91.7% of non-hesitant parents receive most of their knowledge from the family doctor. The lower knowledge of hesitant parents may be associated with another study ([Fox and Duggan, 2013](#fox13)) that showed that around 70% of hesitant parents used electronic resources to retrieve medical information. However, it is well known that a lot of the information found on the Internet is not reliable or valid ([Bean, 2011](#bea11)). Thus, we may conclude that perhaps parents who searched for information on the Internet did not get precise information, and non-hesitant parents got information from other, authorised medical resources.

H2 suggested that no significant difference concerning perceived level of e-health literacy will be found between hesitant and non-hesitant parents, and was accepted. We assume that because the questionnaires were delivered via the Internet, only people who know how to use the Internet and have electronic literacy answered it. Therefore there was no difference concerning their perceived level of e-health literacy. Perhaps if the questionnaire was delivered via traditional ways, the results would have been different, as in this case it might involve people who possess and do not possess e-health literacy skills.

H3, that assumed that hesitant parents will have a greater likelihood of refusing vaccination than non-hesitant parents, was accepted, and shows that most parents who hesitated over whether or not to vaccinate, decided against it. This finding can be associated with the health belief model ([Champion and Skinner, 2008](#cha08)) and supports the growth of the anti-vaccination movement. Various researchers have claimed that while parents are considering vaccination, they estimate the risks of both the disease and the side effects of the vaccination, and many prefer not to vaccinate ([Bond and Nolan, 2011](#bon11); [Luthy _et al._, 2012](#lut12)). In addition, young parents—who probably have not experienced these diseases—base their decision on the fact that various diseases have been eradicated. Thus, they assume these diseases are not dangerous anymore, and that it is possible to heal them through natural processes, without external intervention ([Bond and Nolan, 2011](#bon11); [Luthy _et al._, 2012](#lut12)). Further, they maintain that the vaccination's side effects may be more dangerous than the disease itself ([Betsch and Sachse, 2012](#betsac12)).

H4 suggested that hesitant parents’ use of online electronic resources will be greater than non-hesitant parents and was accepted. Findings reveal that hesitant parents surf more alternative and social network sites, and forums than non-hesitant parents. These findings are consistent with Wilson, Barakat, Vohra, Ritvo and Boon ([2008](#wil08)), showing that non-hesitant parents tend to rely on authorised information from medical services and do not expand their search to other information sources. In contrast, hesitant parents used a variety of information sources, including the Internet. Hesitant parents also used more social network sites. Various studies suggested that Web 2.0 sites such as YouTube, Myspace, Facebook and others support the anti-vaccination movement and express anti-vaccination opinions ([Keelan, Pavri, Balakrishnan, and Wilson, 2010](#kee10); [Seeman, Ing and Rizo, 2010](#see10)). Thus, we may understand that hesitant parents are more exposed to anti-vaccination messages.

H5 suggested that parents who refuse vaccination will have higher perceived levels of e-health literacy and more limited knowledge about vaccination, and was confirmed. Findings reveal that refuser parents have the highest level of perceived e-health literacy, and that non-refuser parents have the largest knowledge about vaccination. These findings can be associated with Schulz and Nakamoto's ([2011](#sch11)) theory that maintained that individuals think they are capable to search and evaluate medical information (because they have a high level of perceived e-health literacy). However, they actually lack crucial information that may help them make the right medical decision.

H6 proposed that parents who hesitated in the past to vaccinate and, after searching for information, decided to do so, have higher perceived e-health literacy and higher knowledge concerning vaccination than non-refuser and non-hesitant parents. This hypothesis is based on Betsch’s model, assuming that when parents face the problem of whether to vaccinate or not, they will look for information, evaluate it and then decide whether to vaccinate or not. However, this assumption was rejected. We can associate these findings with Luthy, Beckstrand, Asay, and Hewett ([2013](#lut13)), who explored parents' experience concerning vaccination. The hesitant parents reported that they did not actually seek information, but rather relied on their feelings, explaining that they did not search for information because they were either overloaded with information, lacked information, or did not know where to locate reliable information about vaccination.

H7 suggested that refuser parents will use more information resources than other parents. This hypothesis was confirmed, echoing Kata's ([2011](#kat11)) finding that refuser parents, who explored the possibility of whether to vaccinate or not, had a higher likelihood of using more information resources in comparison with non-refuser parents.

H8, that investigated whether parents who received information on Web 2.0 sites have lower knowledge about vaccination than parents who received information on other sites, was also confirmed. Findings reveal that the more knowledge parents have about vaccination the less they search in alternative sites, forums, social network sites, and the more they search in government sites. This can be associated with previous research ([Betsch and Sachse, 2012](#betsac12); [Kata, 2011](#kat11); [Venkatraman, Garg and Kumar, 2015](#ven15); [Witteman and Zikmund-Fisher, 2012](#wit12)) showing that most of the fallacious information concerning vaccination can be found in Web 2.0 sites. Thus, parents who use Web 2.0 sites have less knowledge about vaccination.

The researchers also examined the relationships between demographic variables and between hesitant and non-hesitant parents, and between refuser and non-refuser parents. The interesting finding was that hesitant and refuser parents tend to be younger. These findings contrast with previous studies ([Aharon _et al._, 2012](#aha12); [Salmon _et al._, 2005](#sal05)) that showed refuser and hesitant parents to be older. We may assume that because the current study concentrated only on parents who search for information on the Internet, they are probably young people who grew up in the Internet era. Apparently, older parents look for information from other traditional, authorised sources such as books, doctors, nurses, etc.

Concerning the study's limitations, the current study examined only the relationship between parents' Internet searching and their decision on whether to vaccinate their children. It should be mentioned that the researchers did not come tabula rasa to the study and that their opinion is that vaccination is vital and essential. In addition, the study population was recruited via the snowball strategy using online channels. Perhaps using this method has limited the population in advance, as it did not include the population who hesitated over whether to vaccinate but used traditional channels of information.

The researchers recommend that a future study could also use qualitative methods such as open questions or interviews to supplement the quantitative analysis, and thereby enrich the findings by adding other dimensions to the process. Moreover, a further study may use an Internet site supporting vaccination, where the messages are conveyed via social network sites, videos and pictures, and examine its effect on individuals' decisions to vaccinate.

## Conclusion

Theoretically, this study focuses on hesitant and non-hesitant parents concerning the vaccination dilemma, and on their perceived level of online literacy (e-health), level of electronic information sources use and their actual level of knowledge on the subject of vaccination. The research findings show that most of the parents who hesitated over whether to vaccinate their children decided not to vaccinate them. These parents believe they can search for information on the Internet and they surf different kinds of sites (government, social network sites, forums and alternatives). Practically, we suggest that in order to increase parents' willingness to vaccinate their children and their e-health literacy, medical institutions should change their strategy. They should adopt the strategy used by the anti-vaccination movement that conveys its messages via different online channels. Researchers assume that information presented in government sites, that usually include statistical data and verbal explanations, do not influence users ([Betsch _et al._, 2010](#bet10)). However, the anti-vaccination movement uses videos, pictures and social network sites. This strategy was found to be more effective. Therefore, it would be beneficial if medical institutions use these channels to illustrate the dangers of non-vaccination in order to increase parents' e-health literacy. Moreover, we recommend that mainstream medical institutions direct parents to recommended health sites, where they can retrieve reliable information, and thus reduce the likelihood they will search sites offered by the anti-vaccination movement, or sites that present misleading, inaccurate information.

## <a id="author"></a>About the authors

**Noa Aharony** received her Ph.D. in 2003 from the School of Education at Bar-Ilan University, Israel. She is the head of the Information Science Department at Bar-Ilan University. Her research interests are in education for library and information science, information literacy, technological innovations and the librarianship and information science community, and Web 2.0\. Prof. Aharony is a member of the editorial boards of Journal of Librarianship and Information Science, Online Information Review, and IJELL. Prof. Aharony has published in refereed information science and education journals. She can be contacted at Noa.Aharony@biu.ac.il ORCID No. orcid.org/0000-0003-1440-3305

**Romina Goldman** is a graduate student at the Department of Information Science Department at Bar-Ilan University.

</section>

<section>

## References

<ul>
<li id="aha11">Aharon, A. (2011). Parents’ decisions not to immunize their child: past and present,
characteristics of the phenomenon and its causes. <em>Israeli Journal of Health Education and Promotion,
1</em>(4), 32-40.</li>
<li id="aha12">Aharon, A. A., Nehama, H., Shmuel, R. &amp; Baron-Epel, O. (2012). <a
href="http://www.webcitation.org/6pTmpaHgl">Distribution of reasons for not completing vaccines.</a>
[PowerPoint presentation]. Retrieved from http://video.new-app.com/customers/NIHP/parallel/1A2.pdf (Archived
by WebCite&reg; at http://www.webcitation.org/6pTmpaHgl)</li>
<li id="aus12">Austin, E. W., Pinkleton, B. E., Austin, B. W. &amp; Van de Vord, R. (2012). The relationships of
information efficacy and media literacy skills to knowledge and self-efficacy for health-related decision
making. <em>Journal of American College Health, 60</em>(8), 548-554.</li>
<li id="baz09">Bazin, H. (2009). History of vaccine refusal. <em>Bulletin de l'Academie nationale de medecine,
194</em>(4-5), 705-718. </li>
<li id="bea11">Bean, S. J. (2011). Emerging and continuing trends in vaccine opposition website content.
<em>Vaccine, 29</em>(10), 1874-1880.</li>
<li id="bet11">Betsch, C. (2011). Innovations in communication: the internet and the psychology of vaccination
decisions. <em>Euro Surveillance, 16</em>(17), 4.</li>
<li id="bet10">Betsch, C., Renkewitz, F., Betsch, T. &amp; Ulshöfer, C. (2010). The influence of
vaccine-critical websites on perceiving vaccination risks. <em>Journal of Health Psychology, 15</em>(3),
446-455.</li>
<li id="betsac12">Betsch, C. &amp; Sachse, K. (2012). Dr. Jekyll or Mr. Hyde? (How) the internet influences
vaccination decisions: recent evidence and tentative guidelines for online vaccine communication. <em>Vaccine,
30</em>(25), 3723-3726.</li>
<li id="betwic12">Betsch, C. and Wicker, S. (2012). E-health use, vaccination knowledge and perception of own
risk: drivers of vaccination uptake in medical students. Vaccine, 30(6), 1143-1148. </li>
<li id="bon11">Bond, L. &amp; Nolan, T. (2011). Making sense of perceptions of risk of diseases and
vaccinations: a qualitative study combining models of health beliefs, decision-making and risk perception.
<em>BMC Public Health, 11</em>(1), 943.</li>
<li id="bre07">Brewer, N.T., Chapman, G.B., Gibbons, F.X., Gerrard, M., McCaul, K.D. &amp; Weinstein, N. D.
(2007). Meta-analysis of the relationship between risk perception and health behavior: the example of
vaccination. <em>Health Psychology, 26</em>(2), 136-145.</li>
<li id="bru13">Brunson, E.K. (2013). How parents make decisions about their children's vaccinations.
<em>Vaccine, 31</em>(46), 5466-5470.</li>
<li id="bur06">Burgess, D.C., Burgess, M.A. &amp; Leask, J. (2006). The MMR vaccination and autism controversy
in United Kingdom 1998–2005: inevitable community outrage or a failure of risk communication? <em>Vaccine,
24</em>(18), 3921-3928.</li>
<li id="cha08">Champion, V.L. &amp; Skinner, C.S. (2008). The health belief model. In K. Glanz, B. K. Rimer
&amp; K. Viswanath (Eds.), <em>Health behaviour and health education: theory, research, and practice</em> (4th
ed.) (pp. 45-65). San Francisco, CA: Jossey-Bass.</li>
<li id="coe12">Coe, A. B., Gatewood, S. B. &amp; Moczygemba, L. R. (2012). The use of the health belief model to
assess predictors of intent to receive the novel (2009) H1N1 influenza vaccine. <em>Innovations in Pharmacy,
3</em>(2), 1. </li>
<li id="dav02">Davies, P., Chapman, S. &amp; Leask, J. (2002). Antivaccination activists on the world wide web.
<em>Archives of Disease in Childhood, 87</em>(1), 22-25.</li>
<li id="dee11">Deer, B. (2011). How the case against the MMR vaccine was fixed. <em>British Medicql Journal,
342</em>(7788), 77-82.</li>
<li id="dun15">Dunn, A. G., Leask, J., Zhou, X., Mandl, K. D. &amp; Coiera, E. (2015). Associations between
exposure to and expression of negative opinions about human papillomavirus vaccines on social media: an
observational study. <em>Journal of Medical Internet Research, 17</em>(6), e144. </li>
<li id="dun07">Dunsky-Baranga, L. (2007). <em>A progressive model for explaining mothers’ objection to the
vaccination of their infants.</em> Unpublished doctoral dissertation, Bar-Ilan University, Ramat Gan,
Israel.</li>
<li id="fox13">Fox, S. &amp; Duggan, M. (2013). <a href="http://www.webcitation.org/6pMsZcRwt">Health online
2013</a>. Washington, DC: Pew Research Center. Retrieved from
http://pewinternet.org/Reports/2013/Health-online.aspx (Archived by WebCite&reg; at
http://www.webcitation.org/6pMsZcRwt)</li>
<li id="fre04">Fredrickson, D. D., Davis, T. C., Arnould, C. L., Kennen, E. M., Humiston, S. G., Cross, J. T.
&amp; Bocchini, J. A. (2004). <a
href="http://www.stfm.org/Portals/49/Documents/FMPDF/FamilyMedicineVol36Issue6Fredrickson431.pdf">Childhood
immunization refusal: provider and parent perceptions.</a> <em>Family Medicine, 36</em>(6), 431-439.
Retrieved from http://www.stfm.org/Portals/49/Documents/FMPDF/FamilyMedicineVol36Issue6Fredrickson431.pdf
[Unable to archive]</li>
<li id="gui15">Guidry, J. P., Carlyle, K., Messner, M. &amp; Jin, Y. (2015). On pins and needles: how vaccines
are portrayed on Pinterest. <em>Vaccine, 33</em>(39), 5051-5056.</li>
<li id="har13">Harmsen, I. A., Doorman, G. G., Mollema, L., Ruiter, R. A., Kok, G. &amp; de Melker, H. E.
(2013). <a href="https://bmcpublichealth.biomedcentral.com/articles/10.1186/1471-2458-13-1219">Parental
information-seeking behaviour in childhood vaccinations.</a> <em>BMC Public Health, 13</em>(1), 1219.
Retrieved from https://bmcpublichealth.biomedcentral.com/articles/10.1186/1471-2458-13-1219 [Unable to
archive]</li>
<li id="jon12">Jones, A. M., Omer, S. B., Bednarczyk, R. A., Halsey, N. A., Moulton, L. H. &amp; Salmon, D. A.
(2012). <a href="https://www.hindawi.com/journals/apm/2012/932741/">Parents’ source of vaccine information and
impact on vaccine attitudes, beliefs, and nonmedical exemptions. </a><em>Advances in Preventive Medicine,
2012</em>, article ID 932741. Retrieved from https://www.hindawi.com/journals/apm/2012/932741/ [Unable to
archive]</li>
<li id="jor11">Jordan, J. E., Osborne, R. H. &amp; Buchbinder, R. (2011). Critical appraisal of health literacy
indices revealed variable underlying constructs, narrow content and psychometric weaknesses. <em>Journal of
Clinical Epidemiology, 64</em>(4), 366-379.</li>
<li id="kas14">Kasmaei, P., Shokravi, F., Hidarnia, A., Hajizadeh, E., Atrkar-Roushan, Z., Shirazi, K. &amp;
Montazeri, A. (2014). Brushing behavior among young adolescents: does perceived severity matter. <em>BMC
Public Health, 14</em>(8), 2-6. Retrieved from
https://bmcpublichealth.biomedcentral.com/articles/10.1186/1471-2458-14-8 [Unable to archive]</li>
<li id="kat10">Kata, A. (2010). A postmodern Pandora's box: anti-vaccination misinformation on the Internet.
<em>Vaccine, 28</em>(7), 1709-1716. </li>
<li id="kat11">Kata, A. (2011). Anti-vaccine activists, Web 2.0, and the postmodern paradigm–an overview of
tactics and tropes used online by the anti-vaccination movement. <em>Vaccine, 30</em>(25), 3778-3789.</li>
<li id="kee10">Keelan, J., Pavri, V., Balakrishnan, R. &amp; Wilson, K. (2010). An analysis of the Human
Papilloma Virus vaccine debate on MySpace blogs. <em>Vaccine, 28</em>(6), 1535-1540.</li>
<li id="lea06">Leask, J., Chapman, S., Hawe, P. &amp; Burgess, M. (2006). What maintains parental support for
vaccination when challenged by anti-vaccination messages? A qualitative study. <em>Vaccine, 24</em>(49),
7238-7245.</li>
<li id="lut12">Luthy, K. E., Beckstrand, R. L., Callister, L. C. &amp; Cahoon, S. (2012). Reasons parents exempt
children from receiving immunizations. <em>Journal of School Nursing, 28</em>(2), 153-160‏</li>
<li id="lut13">Luthy, K. E., Beckstrand, R. L., Asay, W. &amp; Hewett, C. (2013). Vaccinating parents experience
vaccine anxiety too. <em>Journal of the American Association of Nurse Practitioners, 25</em>(12), 667-673.
</li>
<li id="mcr12">Mcree, A., Reiter, P. &amp; Brewer, N. (2012). Parents’ Internet use for information about HPV
vaccine. <em>Vaccine, 30(</em>25), 3757-3762. </li>
<li id="mid13">Middleton, D. B. &amp; Wolfe, R. M. (2013). The vaccine misinformation landscape in family
medicine. In Archana Chatterjee, (Ed.). <em>Vaccinophobia and vaccine controversies of the 21st century</em>
(pp. 147-164). New York, NY: Springer.</li>
<li id="mik11">Mikulak, A. K. (2011). Parents’ vaccination concerns are about more than risk and benefit.
<em>Human Vaccines, 7</em>(6), 597-599.</li>
<li id="nor06a">Norman, C. D. &amp; Skinner, H. A. (2006a). <a
href="http://www.webcitation.org/6qHLi91S">E-health literacy: essential skills for consumer health in a
networked world</a>. <em>Journal of Medical Internet Research, 8</em>(2), e9. Retrieved from
https://www.jmir.org/2006/2/e9/ (Archived by WebCite&reg; at http://www.webcitation.org/6qHLi91SV)</li>
<li id="nor06b">Norman, C. D. &amp; Skinner, H. A. (2006b). <a
href="http://www.webcitation.org/6qHLUPFBA">eHEALS: the eHealth literacy scale</a>. <em>Journal of Medical
Internet Research, 8</em>(4), e27. Retrieved from https://www.jmir.org/2006/4/e27/ (Archived by WebCite&reg;
at http://www.webcitation.org/6qHLUPFBA)</li>
<li id="pew15">Pew Research Center. (2015). <a href="http://www.webcitation.org/6pMtiI1u3">83% say measles
vaccine is safe for healthy children</a>. Washington, DC: Pew Research Center. Retrieved from
http://www.people-press.org/2015/02/09/83-percent-say-measles-vaccine-is-safe-for-healthy-children/ (Archived
by WebCite&reg; at http://www.webcitation.org/6pMtiI1u3)</li>
<li id="pol12">Poland, G. A. &amp; Jacobson, R. M. (2012). The clinician’s guide to the anti-vaccinationists’
galaxy. <em>Human Immunology, 73</em>(8), 859-866.</li>
<li id="rui15">Ruiz, J. B. &amp; Barnett, G. A. (2015). Exploring the presentation of HPV information online: a
semantic network analysis of websites. <em>Vaccine, 33</em>(29), 3354-3359.</li>
<li id="sal05">Salmon, D. A., Moulton, L. H., Omer, S. B., DeHart, M. P., Stokley, S. &amp; Halsey, N. A.
(2005). Factors associated with refusal of childhood vaccines among parents of school-aged children: a
case-control study. <em>Archives of Pediatrics &amp; Adolescent Medicine, 159</em>(5), 470-476.</li>
<li id="sch11">Schulz, P. J. &amp; Nakamoto, K. (2011). <a
href="http://www.aaai.org/ocs/index.php/SSS/SSS11/paper/download/2430/2853"> &quot;Bad&quot; literacy, the
internet, and the limits of patient empowerment.</a> In Nancy Green, Sara Rubinelli and Donia Scott, (Eds.)
<em>AAAI Spring Symposium: AI and Health Communication.</em> Palo Alto, CA: AAAI. Retrieved from
http://www.aaai.org/ocs/index.php/SSS/SSS11/paper/download/2430/2853 [Unable to archive]</li>
<li id="see10">Seeman, N., Ing, A. &amp; Rizo, C. (2010). <a
href="http://www.longwoods.com/content/21923">Assessing and responding in real time to online anti-vaccine
sentiment during a flu pandemic.</a> <em>Health Q, 13</em>(Spec. No.), 8-15. Retrieved from
http://www.longwoods.com/content/21923 [Unable to archive]</li>
<li id="smi04">Smith, P. J., Chu, S. Y. &amp; Barker, L. E. (2004). Children who have received no vaccines: who
are they and where do they live? <em>Pediatrics, 114</em>(1), 187-195.</li>
<li id="smi11">Smith, P. J., Humiston, S. G., Marcuse, E. K., Zhao, Z., Dorell, C. G., Howes, C. &amp; Hibbs, B.
(2011). Parental delay or refusal of vaccine doses, childhood vaccination coverage at 24 months of age, and
the Health Belief Model. <em>Public Health Reports, 126</em>(Suppl. 2), 135-146.</li>
<li id="sor12">Sørensen, K., Van den Broucke, S., Fullam, J., Doyle, G., Pelikan, J., Slonska, Z. &amp; Brand,
H. (2012). <a href="https://bmcpublichealth.biomedcentral.com/articles/10.1186/1471-2458-12-80">Health
literacy and public health: a systematic review and integration of definitions and models.</a> <em>BMC
Public Health, 12</em>(1), 80. Retrieved from
https://bmcpublichealth.biomedcentral.com/articles/10.1186/1471-2458-12-80 [Unable to archive].</li>
<li id="str97">Strecher, V. J. &amp; Rosenstock, I. M. (1997). The health belief model. In A. Baum, S. Newman,
J. Weinman, R. West &amp; C. MacManus (Eds.), <em>Cambridge handbook of psychology, health and medicine</em>
(pp. 113-117). Cambridge: Cambridge University Press.</li>
<li id="taf14">Tafuri, S., Gallone, M. S., Cappelli, M. G., Martinelli, D., Prato, R. &amp; Germinario, C.
(2014). Addressing the anti-vaccination movement and the role of HCWs. <em>Vaccine, 32</em>(38), 4860-4865.
</li>
<li id="ven15">Venkatraman, A., Garg, N. &amp; Kumar, N. (2015). Greater freedom of speech on web 2.0 correlates
with dominance of views linking vaccines to autism. <em>Vaccine, 33</em>(12), 1422-1425. </li>
<li id="wil08">Wilson, K., Barakat, M., Vohra, S., Ritvo, P. &amp; Boon, H. (2008). Parental views on pediatric
vaccination: the impact of competing advocacy coalitions. <em>Public Understanding of Science, 17</em>(2),
231-243.</li>
<li id="wit12">Witteman, H. O. &amp; Zikmund-Fisher, B. J. (2012). The defining characteristics of web 2.0 and
their potential influence in the online vaccination debate. <em>Vaccine, 30</em>(25), 3734-3740.</li>
<li id="wol02">Wolfe, R. M. &amp; Sharp, L. K. (2002). Anti-vaccinationists past and present. <em>British
Medical Journal, 325</em>(7361), 430-432.</li>
<li id="zim05">Zimmerman, R. K., Wolfe, R. M., Fox, D. E., Fox, J. R., Nowalk, M. P., Troy, J. A. &amp; Sharp,
L. K. (2005). <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1550643/">Vaccine criticism on the world
wide web.</a> <em>Journal of Medical Internet Research, 7</em>(2), e17. Retrieved from
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1550643/ [Unable to archive] </li>
<li id="zin12">Zingg, A. &amp; Siegrist, M. (2012). Measuring people's knowledge about vaccination: developing a
one-dimensional scale. <em>Vaccine, 30</em>(25), 3771-3777</li>
</ul>

</section>

</article>

<section>

## Appendix: Questionnaire

Gender  
o Female  
o Male  
Year of birth __________

Number of children  
o 1  
o 2  
o 3  
o 4  
o 5 or more  
Year of birth of the youngest child __________

Education  
o High school graduate  
o College graduate  
o Graduate degree  
o Ph.D. degree  
o Technical training

The average family gross monthly income is about 14,622 NIS. Is your family income?  
o Much lower  
o Slightly lower  
o Something like  
o Slightly higher  
o Much higher

Here are some questions about vaccination:  
The ministry of health recommends several vaccinations for children. Have you ever hesitated if to vaccinate your child?  
o Yes  
o No

Have you ever postponed a vaccination for any other reason apart from child illness?  
o Yes  
o No  
o Hesitate

If you would have another baby, would you follow the ministry of health recommendations and vaccinate him/her?  
o Yes  
o No  
o Hesitate

Please mark how much information you have acquired from the following electronic sources:

<table>

<tbody>

<tr>

<th>Websites</th>

<th>Any knowledge</th>

<th>Little knowledge</th>

<th>Not much, not little</th>

<th>Much knowledge</th>

<th>Very much knowledge</th>

</tr>

<tr>

<td>Government  
(like HMOs sites and medical centers)</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Alternative sites  

</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Forum  

</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Social networks  
(like Facebook, Twitter, etc.)</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

</tbody>

</table>

Statements related to vaccines:

<table>

<tbody>

<tr>

<th> </th>

<th>True</th>

<th>False</th>

<th>Do not  
know</th>

</tr>

<tr>

<td>Vaccines are superfluous, as diseases can be treated (e.g. with antibiotics).</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Without broadly applied vaccine programs, smallpox would still exist.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>The efficacy of vaccines has been proven.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Children would be more resistant if they were not always vaccinated against all diseases.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Diseases like autism, multiple sclerosis and diabetes might be triggered through vaccinations.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>The immune system of children is not overloaded through many vaccinations.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Many vaccinations are administered too early, so the body's own immune system has no possibility to develop.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>The doses of the chemicals used in vaccines are not dangerous for humans.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Vaccinations increase the occurrence of allergies.</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

We would like your position on and experience of using the Internet for getting health information. For each sentence, please select an answer which reflects your position and your present experience:

<table>

<tbody>

<tr>

<th></th>

<th>Strongly disagree</th>

<th>Disagree</th>

<th>Neither agree nor disagree</th>

<th>Agree</th>

<th>Strongly agree</th>

</tr>

<tr>

<td>I know how to find helpful health resources on the Internet</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>I know how to use the Internet to answer my health questions</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>I know what health resources are available on the Internet</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>I know where to find helpful health resources on the Internet</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>I know how to use the health information I find on the Internet to help me</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>I have the skills I need to evaluate the health resources I find on the Internet</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>I can tell high quality from low quality health resources on the Internet</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>I feel confident in using information from the Internet to make health decisions</td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

</tbody>

</table>

</section>