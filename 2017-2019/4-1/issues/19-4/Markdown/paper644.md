<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# Information seeking behaviour of mathematicians: scientists and students

#### [Remigiusz Sapa, Monika Krakowska](#author) and [Małgorzata Janiak](#author)  
Institute of Information and Library Science of the Jagiellonian University, ul. prof. Stanis&lstrok;awa &Lstrok;ojasiewicza 4, 30-348 Kraków, Poland

#### Abstract

> **Introduction.** The paper presents original research designed to explore and compare selected aspects of the information seeking behaviour of mathematicians (scientists and students) on the Internet.  
> **Method.** The data were gathered through a questionnaire distributed at the end of 2011 and in January 2012\. Twenty-nine professional mathematicians and 153 students of mathematics from the Institute of Mathematics of the Jagiellonian University in Kraków, Poland, were surveyed.  
> **Analysis.** The gathered data were analysed in a quantitative manner and then interpreted comparatively to find similarities and differences between the behaviour of professional mathematicians and students.  
> **Results.** Students, as opposed to scientists, often declared searching for reference works and multimedia objects and comparatively rarely for journal papers and information about sources unavailable on the Web. They more willingly use social networking sites while scientists more often search discipline-oriented portals or library Websites. Scientists use, first of all, the author's name or the publication titles to formulate queries, students prefer keyword searching. While scientists trust their own ability to determine the scientific character of information or treat journals as determinants of the scientific quality, students do not.  
> **Conclusions.** The research revealed some significant differences between the information seeking behaviour of those two groups of mathematicians. It could be the result of different levels of experience in scientific work, distinct tasks undertaken within the academic environment, and the change in the general paradigm of information searching.

<section>

## Introduction

Human information behaviour has been one of the central areas of interest of information science for at least thirty years. Marcia Bates placed the question concerning the way people seek and use information among three 'big questions' of information science ([Bates, 1999](#bat99)). This strong interest in information behaviour has been proved not only by empirical research but also by developments in information behaviour theory (e.g., [Fisher, Erdelez, and McKechnie, 2005](#fis05)). On the other hand, information science has always been closely related to problems of scholarly communication, both in its pure (fundamental) and applied research. These two fields of interest (information behaviour and scholarly communication) have resulted in much published research on how scholars behave while seeking information and what can be done to make the process more effective and efficient. Some of them are theoretical (e.g., [Meho and Tibbo, 2003](#meh03)) while others are focused on very practical aspects of improving information services and systems designed for scholars and students (e.g., [Haines, Light, O'Malley, and Delwiche, 2010](#hai10)). Additionally, one could generally classify the main issues explored by the researchers of scholarly information seeking behaviour, both within fundamental and applied research, into several groups regarding the scope and the main aspect of information seeking.

The first group covers the research focused on the exploration of the patterns of information seeking behaviour within selected academic disciplines or research fields. Some of them are limited to one specific discipline, some are designed to compare information seeking behaviour in various scholarly domains, and others aim at the exploration of how information is searched for in several interdisciplinary areas of scholarly activities (e.g., [Ellis, Cox, and Hall, 1993](#ell93); [Davis, 2004](#dav04); [Jamali and Nicholas, 2008](#jam08); [Jamali and Nicholas, 2010](#jam10b)).

The next group (which could also be partly viewed as a sub-division of the group defined above) contains such publications which focus on the information seeking behaviour of specific groups of users selected in accordance with their role in scholarly communication or position within academia, among them being: students, university professors, novice scholars etc. (e.g., [Whitmire, 2002](#whi02); [Jankowska, 2004](#jan04); [Barjak, 2004](#bar04) and [2006](#bar06); [Hemminger, Vaughan, and Adams, 2007](#hem07); [Junni, 2007](#jun07); [Haglund and Olsson, 2008](#hag08); [Jaskowska, Koryci&nacute;ska-Huras, and Próchnick, 2009](#jas09); [Tramullas and Sánchez-Casabón, 2010](#tra10)).

Another area of research focuses on information seeking in the local context defined primarily by the country where the scholars live and work (e.g., [Niu _et al._, 2010](#niu10)). Additionally, such works are quite often limited to the representatives of specific disciplines (e.g., [Brindesi and Kapidakis, 2011](#bri11))

Furthermore, there are also publications aimed at exploring the way scholars and students search for information within specific information resources or systems, including digital libraries, institutional online collections, library catalogues, etc. (e.g., [Sapa, 1997](#sap97); [Buchanan, Cunningham, Blandford, Rimmer, and Warwick, 2005](#buc05); [Ross and Terras, 2011](#ros11)).

The next group is composed of the research focused on the use of modern information technology, particularly search engines, by scientists and students searching for scholarly information. It also covers some wider problems of information seeking behaviour in the digital (or virtual) environment (e.g., [Rowlands _et al._, 2008](#row08); [Jamali and Asadi, 2010](#jam10); [Tahir, Mahmood, and Shafique, 2010](#tah10)). Such research is often closely related to the one focused on the scholars' information literacy and the way such competences influence their information seeking behaviour (e.g., [Joshi and Nikose, 2010](#jos10)).

One can also find publications on selected methodological aspects of the research on the information seeking behaviour of modern scholars (e.g., [Nicholas, 2008](#nic08)) as well as on many theoretical considerations and models of information behaviour in general, which can be used as frames or reference points for the formulation of conclusions from the empirical research on specific aspects of the information seeking behaviour of scientists and students (some of them are further discussed in the following parts of this text).

Worth emphasizing is the fact that frequently a few issues are addressed within one research and, as a result, within one publication. This is not a wholly disjunctive classification of publications concerning the problem of scholarly information seeking behaviour but simply an arrangement of issues addressed most often in the literature, compiled to comprehend the scale and the scope of the background of the research presented in this paper.

## Main goals, value and structure of this paper

The main goal of the research presented here was to explore selected aspects of the information seeking behaviour of mathematicians on the Internet. Another goal was to reveal the differences and similarities between the behaviour of scientists and students of mathematics and to interpret them within the frame provided by selected information behaviour models to formulate some hypotheses on their reasons. Although the findings could be useful for practitioners providing or designing information services for scientists and students, the research was aimed at improving our understanding of how scholarly information is searched for on the Internet by the community of mathematicians. While the main problem under consideration belongs to the first group of publications defined in the introductory part of this paper (the information seeking behaviour of scholars representing a specific discipline), the text also touches other aspects of information seeking behaviour present in library and information science literature, including information competencies, differences in behaviour of various groups of information seekers as well as the way people use modern information technology.

The information seeking behaviour of mathematicians has not been sufficiently explored yet. It was taken into consideration by some authors writing about a wider spectrum of scientists (e.g., [Brown, 1999](#bro99)) or by those who were interested in this problem while conducting the research on other aspects of scholarly communication, particularly on various information services produced by mathematicians and offered to that group of scientists and students (e.g., [Newby, 2005](#new05); [Sapa, 2007](#sap07); [Zhao, Kan, and Theng, 2008](#zha08)). Generally, we lack empirically proven information about the way they seek new knowledge and how they use the information sources and tools available on the Internet.

This paper is based on original research and, while it has not been designed to provide a complete picture of the issue, it should help to enrich our knowledge of the information seeking behaviour of mathematicians. Although the research regards only a local group of respondents (see the Methodology section below), its results can be viewed as a starting point to compare the information seeking behaviour of mathematicians in different settings and contexts (institutional, cultural, national) and furthermore, for example, to formulate more general conclusions on the importance of various factors influencing the information seeking behaviour of mathematicians and the level of 'globalization' of their information seeking behaviour. The research presented in the paper provides not only facts about the information behaviour of that group of academics but also makes it possible to formulate a few new hypotheses (in the Conclusion section) to inspire future research on exact reasons of the behaviour identified here.

The authors assert that the idea of _one size fits all_ is not ideal for the improvements of the scholarly communication system. The modern information environment offers more flexibility and more possibilities than the traditional one. But to effectively use the opportunities offered by this environment to develop the information services and resources (for example offered by academic libraries) tailored for specific needs of various groups of participants, more focused research is needed.

The paper is divided into four parts. Part one is devoted to the theoretical issues of information seeking behaviour. Its main goal is to draw the readers' attention to a few models of such behaviour proposed in the information science literature which seem to be the most adequate to formulate a conceptual frame for the research presented here. The second part centres on the method and scope of the research and the third part is devoted to the presentation of its findings. Finally, the last part of the paper contains discussion and conclusions.

## Information seeking behaviour models

An interdisciplinary perspective on human information behaviour requires an understanding of the heterogeneous context in which this behaviour occurs ([Johnson, 2003](#joh03)). For example, the model proposed by Wilson ([2000](#wil00)) encompasses not only the actions undertaken by humans to gain, process and use information, but also the key factors influencing their behaviour, including the affective components (motivations, emotions) as well as cultural, environmental, social (role) or psychological contexts of individual behaviour.

Furthermore, any holistic model of human information behaviour should reflect various ways of gaining information including, for example, incidental or opportunistic encountering of information as well as intuitive or routine collecting of information. Within such a holistic model of information behaviour there should also be a place for the reflection of all the actions concerning information sharing, disseminating and using. Furthermore, to wholly understand information processes one should also take into account such phenomena as: avoiding, ignoring, accepting, rejecting or even hiding and destroying information. All these actions and decisions, together with their context and factors (see also [Niu and Hemminger, 2012](#niu12)) affecting them, constitute a wide frame for the exploration and interpretation of the information behaviour of any individuals and allow us to research the human information interactions with the surrounding reality.

From a user's perspective, the broad issue of human information behaviour encompasses various aspects of human needs as well as the problems of searching, disseminating, using and managing information from a variety of perspectives ([Savolainen, 2007](#sav07)). Therefore it also includes the concept of information literacy ([Shenton and Hay-Gibson, 2011](#she11)), the problems of using information in accordance with individual interpretations as well as the relations between the effectiveness of information processes and, for example, information users' experiences. Those issues have already been explored as a part of considerations regarding models of learning (e.g., [Bandura, 1969](#ban69); [Bandura, 1977](#ban77); [Miwa, 2005](#miw05)). Learning can also occur during the process of sense-making interpreted within Dervin's situation-gap-use model ([Dervin, 1998](#der98); [Zhu, Chen, Chen, and Chern, 2011](#zhu11)).

An intellectual and cognitive approach to the study of information behaviour is contained within Kuhlthau's model explaining a psychological way to access ideas, concepts and information at different stages of the information process: initiating, selecting, exploring, formulating, collecting and presenting information ([Kuhlthau, 2005](#kuh05)). Nied&zacute;wiedzka in her model, based largely on Wilson's research and achievements, indicates the role of formal or informal information intermediaries and their power to act as determinants or stimuli of human information behaviour. In her model, the information sources (information-searching systems, databases, information centres, libraries, reference materials or individual knowledge) affect the information processes of analysis, selection and production of information ([Nied&zacute;wiedzka, 2003](#nie03)).

Bearing in mind this broad picture of human information behaviour, we can look closer at the subject of this paper – information seeking. Marchionini characterises information seeking as a process that is generated by information needs of humans situated in specific contexts, so that they are able to interact with other components and the environment ([Marchionini, 2008](#mar08)). As already mentioned, the context of information seeking behaviour is defined by numerous various elements including individual emotional and intellectual capabilities, specific tasks and professional roles, available sources of information, widely seen information competencies etc. Here, considering the main goal of the research, two perspectives of information seeking behaviour seem to be crucial: the professional perspective (mathematicians as professional scientists within the academia) and the learning one (students, but also scientists gaining new knowledge and skills).

An interesting model of the information behaviour of professionals was presented by Leckie and Pettigrew. The information seeking process in this model features the roles professionals play in the environmental, social and cultural context of their professions. According to this model the actual tasks (such as: assessing, supervising, speaking, researching, writing reports, etc.) performed by professionals have considerable influence on their information behaviour. The main components of the proposed model are: needs (specific and shaped by variables like demographic and situational factors as well as frequency and predictability), sources (which are important for encountering, searching, creating information, developing individual knowledge and experience), awareness (based on information literacy, knowledge about different, useful information resources, perception of the information processes determining the paths of information seeking) and outcomes (which could be work-related, tasks-related or role-related and which are optimal when they meet the information needs of the users) ([Leckie and Pettigrew, 1997](#lec97)).

Worth noting is also the improvement of Ellis' model, which was the outcome of research on the information seeking behaviour of scholars – representatives of various disciplines - and which was used to analyse behavioural patterns in the information processes ([Ellis, 1989](#ell89); [Ellis, 2005](#ell05)). The improvement was proposed by Makri and his colleagues. They specified nine generic features which are composed of different stages.

Stage 1: Starting: this initiates the search by identifying references, previously used sources, relevant information and activities of asking, consulting, reviewing, using online catalogues, indexes, abstracts, etc.;

Stage 2: Chaining: occurs when connections are made between citations and reference information or other pieces of information used in the first stage resources;

Stage 3: Browsing: occurs when information is looked for within the domains of user's possible interests;

Stage 4: Differentiating: based on filtering and using distinctions of quality and the hierarchy of information;

Stage 5: Monitoring: in which users regularly use particular and well known resources that constitute the core of the professional/academic development (journals, magazines, databases, catalogues, books, etc.);

Stage 6: Extracting: this is associated with selecting an adequate amount of knowledge from information resources or identifying relevant expertise in various information sources – journals, bibliographies, collections of abstracts, databases or repositories;

Stage 7: Verifying: choosing the relevant, exact and right information, avoiding errors;

Stage 8: Information managing: involves the processes of information gathering, organising, archiving and collecting;

Stage 9: Ending: the process of information dissemination, including the preparation of various _information products_ (papers, books, presentations, theses, etc.) ([Ellis, Wilson, Ford, Foster, Lam, Burton, and Spink, 2002](#ell02); [Makri, Blandford, and Cox, 2008](#mak08); [Makri and Warwick, 2010](#mak10)). Some of those processes (starting, browsing and searching, monitoring and differentiating/verifying) were partly investigated within the research presented here in the subsequent parts of the paper.

Another model which should not be overlooked is the model proposed by Urquhart and Rowley ([2007](#urq07)). It relates to the students' information seeking behaviour in an electronic environment and places the searching processes in a wide context of macro (e.g., information and learning technology infrastructure, policies and funding) and micro (e.g., discipline and curriculum, academics’ information behaviour, search strategies) factors specific to the academic environment and at least partly different in various disciplines.

According to this model, any processes connected with the scientific knowledge creation, teaching and learning activities, as well as information qualifications and competencies, personal experiences and knowledge or even various programs of study may have a significant impact on information processes and influence the information behaviour of scientists and students. Furthermore, the information seeking behaviour of such users takes place within a broader context of macro factors including the education and science policies at the local, regional and national levels, the culture of education, availability of information resources and tools as well as the information and technology infrastructure and academic organizational culture. Urquhart and Rowley also pointed to other factors influencing information seeking behaviour in general: the design, structure and level of information resources, learning need expectations and response of information resources’ publishers, organisers, creators, etc. ([Urquhart and Rowley, 2007](#urq07)).

Another model of the students' information behaviour was expanded by the elements taken into account by Wilson and presented in the study on the information behaviour of graduates who had completed the higher level of their education. It was used by Al-Muomen, Morris and Maynard in the research on graduates of the Kuwait University. The revised model includes additional components present in Wilson's previous model, taking into account for example psychological and cultural factors influencing information behaviour. This behaviour is also placed within the demographic context which provides us with an opportunity to relate any research findings to various demographic conditions in different countries ([Al-Muomen, Morris, and Maynard, 2012](#alm12)).

## Method and scope of the research

As it has already been mentioned, the research presented here had been designed to explore selected aspects of the information seeking behaviour of mathematicians on the Web and to gain knowledge about differences in this behaviour between professional scientists and students. It was decided to focus the research on the following components of information seeking behaviour (derived from the improvement of Ellis's model): starting (what tools or starting points are used?), searching and monitoring (what, where, how and what tools are used?), differentiating/selecting (criteria). The research was also limited to seeking information related to scientific and educational activities.

To reach the goal of the research two versions of one questionnaire were prepared: one for academics and another for students (see: one general version in the [Appendix](#app)). The versions differ slightly in the way the questions are formulated (but not the suggested answers to choose from) but are identical in what they ask for. The differences reflect various goals of information seeking of these two groups within the academic contexts (scientists - for research and teaching, students, first of all, for learning) and (although irrelevant for research goals) some aspects of the communication culture in Polish universities.

In particular, the questions had been designed to discover what kind of content and what sort of information sources are searched for on the Internet by mathematicians; which resources are used most often, which Internet searching tools are known and preferred; how mathematicians formulate queries, where their starting points are and how they distinguish scientific from non-scientific information.

The questionnaires were prepared and disseminated (printed out and handed individually) in November/December 2011 and in January 2012\. All together twenty-nine professional mathematicians (out of the total number of seventy-seven) and 153 students of mathematics (out of the total number of 330) from the Institute of Mathematics, Faculty of Mathematics and Computer Science of the Jagiellonian University in Kraków, Poland were surveyed.

The fact that all the respondents come from the same institution has vital consequences. Firstly, the results can not be generalised and applied to the global population of mathematicians. According to the models discussed earlier, their behaviour could be strongly influenced by many local factors, for example: the broader cultural and social context, organizational culture, demographic context, local and national education and science policies, information and technology infrastructure, programs of study, etc. However, they can be useful as a reference point for comparisons and in drawing some deeper conclusions on the role of such local contexts. On the other hand, all such factors influencing information seeking behaviour could be excluded from the considerations when analysing differences between scientists' and students' behaviour, because they are the same (or almost the same) for both groups of respondents. It significantly narrows down the scope of possible reasons for detected differences or similarities to those selected factors defined in the models mentioned before which do not influence both groups at the same time, for example: tasks, experiences, competencies or attitudes.

The gathered data were first of all analysed in a quantitative manner and then interpreted in a comparative way to find and determine similarities and differences between those two groups of respondents.

## Research findings

As mentioned, the research had been designed to identify the selected aspects of the information seeking behaviour of mathematicians and to compare the behaviour of scientists and students of mathematics within those aspects. The following research questions (relating to the aspects taken here into consideration) were formulated:

*   Where do they look for information?
*   How do they search?
*   How do they decide on the academic value of the information found on the Web?
*   What kind of knowledge are they looking for?

These general questions where then transformed into the questionnaires (see the [Appendix](#app) and the explanations in the 'Methodology and scope of the research' section of this paper) in such a way that the first one is represented by the questionnaire questions number 1 and 2, the second by the questions number 3, 4, and 5, the third by the question number 6 and the last one by the question number 7.

Altogether, both the academics and the students were asked to answer 7 specific questions related to their behaviour while searching the Internet for information resources used in their teaching, research and learning activities (see the [Appendix](#app)). The common information resources, places and tools were identified and also the information seeking approaches and preferences were recognised.

In the first question both groups of respondents were asked to indicate what kind of information resources (knowledge containers) relating to their scientific and educational activities they look for on the Internet most frequently. They were asked to tick a maximum of three answers.

The results show (Figure 1) that the academics look for scholarly papers (full texts) and this kind of information resource is sought by 93.10% of surveyed scientists. Scientific books (full texts, including textbooks and course books) are searched for by 72.41% of the staff while the information about scientific papers and books (bibliographic descriptions, availability) is looked for by 37.93%. Abstracts are searched for by 24.14% of the academics and the same percentage of the surveyed scholars look for reference works (encyclopaedias, dictionaries, lexicons, atlases, etc.). Fact databases are sought only by 10.34% of the academics, multimedia objects by 3.45% and other sources by 6.90% (Wikipedia and some assignments and materials needed for the process of teaching). No one chose the answer: _I don't search for such information sources on the Internet._

On the other hand, the students seek mostly scientific books, including textbooks or course books – 81.05%. 62.75% of the surveyed students look for scientific papers (full texts) and 41.83% for reference works (encyclopaedias, dictionaries, lexicons, atlases, etc.). Multimedia objects are searched for by 33.33% of the students, abstracts by 22.88%, information about scientific papers and books by 17.65%, fact databases by 10.46%, and other sources by 3.92% (lectures, definitions, maths forums, homework assignments, thematic portals and _the good sources of knowledge_). Only two respondents (1.31%) declared that they did not search the Internet for such information, but still one of them indicated in the subsequent questions that he or she uses multimedia search engines for finding information objects.

<figure>

![Figure 1: Kinds of information sources searched for on the Internet most often](../p644fig1.png)

<figcaption>Figure 1: Kinds of information sources searched for on the Internet most often</figcaption>

</figure>

Both the academics and the students search mostly for papers and books. But while the scientists prefer papers, students are more interested in finding books. The fact worth underlining is that 72.41% of the academics and only 53.59% of the students indicated these two answers simultaneously. Other differences between those two groups of respondents can be seen while comparing searching for information about papers and books (bibliographic descriptions, availability information) - the academics do it quite often while the students rarely. In the case of searching for reference works the situation is opposite. Also multimedia objects are rarely looked for by the scientists and are sought quite often by the students.

Question two (_Where exactly do you look for information?_) was designed to identify the most popular types of sites on the Web which mathematicians search through for information (Figure 2). They were asked to indicate three most frequently used sites. The survey revealed that the academics most often look for the information on disciplinary oriented scientific and educational portals (51,72%), open access repositories and archives of scientific outputs and on the websites of individual scientists (in both cases 44,83% of the surveyed scientists), and then on library Websites (41,38%). Publishers' websites are searched through for information by 34.48% and digital libraries by 27.59% of the academics. They also look for information on various websites provided by scientific institutions and organizations (20,69%), on scholarly oriented social networking sites (e.g., discussion forums, bookmark and publication sharing systems, data sharing systems, blogs, wikis, etc.) (13.79%), and in other sources (6.90%).

Within the group of students a significant majority of the respondents indicated scholarly oriented social networking sites as the most often used resources of scientific information and learning materials on the Internet (73.86%). Furthermore, the students look for such information on institutional websites (42.48%), on individual researchers' websites (41.83%), in open access repositories and archives of scientific publications (32.68%) or on disciplinary oriented scientific and educational portals (24.84%). They also search for information on publishers' websites, in digital libraries (16.99% in both cases), and on library websites (what seems to be a little bit surprising - only 15.03%). The answer _in other resources_ was chosen by 4.58% of the surveyed students.

<figure>

![Figure 2: The sites which mathematicians search through for scientific information and learning resources on the Internet](../p644fig2.png)

<figcaption>Figure 2: The sites which mathematicians search through for scientific information and learning resources on the Internet</figcaption>

</figure>

The differences between both groups of respondents are mostly visible in the usage of Web 2.0 sites, which are very popular among the students and less so among the academics. On the other hand, while the scientists still rely on library and publishers' websites, the students do not. The same could be said about disciplinary oriented portals, which are quite often used by the academics and seldom by the students.

The following facts cast more light on the comparison of the information seeking behaviour of those two groups of mathematicians. The Jagiellonian Library (which is the main library of the Jagiellonian University) Website was indicated by 27.59% of the scientists and only by 7,84% of the students, and the Central Mathematical Library of the Institute of Mathematics, Polish Academy of Sciences, by 17.24% of the academics and only by less than 1% of the students (in fact only one student admitted using this portal). What is more, two students (2.61%) indicated Wikipedia and one student the Science Direct portal as library websites. Disturbing for librarians might be the fact that also a few scientists indicated other portals as library websites.

20.69% of the surveyed scientists declared MathSciNet (American Mathematical Society, www.ams.org/mathscinet) to be their preferred scientific portal. Only one scholar mentioned Matematyka.pl (a Polish portal for mathematicians) and another one - arXiv.org (sic!). What is more, MathSciNet and arXiv.org were also declared as _other sources_ by some scholars, together with Amazon and Google Scholar, which indicates some problems with the understanding of the specificity of various sources of information. Among the students, MathSciNet was not mentioned at all, and Polish Matematyka.pl turned out to be the most popular portal (5.88%). Wolfram MathWorld (http://mathworld.wolfram.com/), Wolfram Alpha (http://www.wolframalpha.com) and Wikipedia (sic!) were mentioned by one student each (less than 1%). Other sources listed by the students were: Google twice (1.31%), Rapidshare (http://www.rapidshare.com), Wikipedia, _ebooks_ and _books_ once each.

In the third question the respondents were asked to indicate the search tools used when looking on the Internet for the information related to scientific and educational activities (Figure 3). The universal search engines (e.g., Google, Bing, Yahoo) are used by 96.55% of the scientists, while scholarly oriented search engines are used only by 65.52% of the respondents from this group. The academics also use search engines specialised in finding specific objects (e.g., graphics, audio files) but not very often – only 24.14% selected this answer. 6.90% of the scientists declared that they use other tools. Google was mentioned 10 times (34,48%), MathSciNet 6 times (20,69%) - 4 times as a scientific search engine and 2 times as _another search tool_, Google Books 4 times (13,79%), Google Scholar only 3 times (10.34%). Two other scientists (6.90%) declared that they use Zentralblat (www.zentralblatt-math.org/zmath/). Scirus and ISI Web of Knowledge were indicated once each.

Also among the students the universal search engines are the most often used tools for searching the Internet for information related to scientific and educational activities (98.04%). 52.29% of the students declared that they use search engines designed to find specific objects (e.g., graphics, audio files), 14.38% of them chose the answer _scholarly oriented search engines (e.g., Google Scholar, Scirus)_, and 3.27 pointed to the answer _others_. Google was mentioned by 13.73% of the surveyed students, Google Books by 3.92%, and Google Scholar by 1,31%. Furthermore, also other 'search engines' were indicated by the students: Wikipedia (as a universal search engine!), MathSciNet (as a scholarly oriented search engine), and Wolphram Alpha (as a universal search engine and as _others_) twice (1,31%) each, Vonito.pl (such a website probably does not exist at all), arXiv.org (as a scholarly oriented search engine!) and Google Maps once (0.65%) each.

<figure>

![Figure 3: The tools used for searching information necessary for scientific and educational activities on the Internet](../p644fig3.png)

<figcaption>Figure 3: The tools used for searching information necessary for scientific and educational activities on the Internet</figcaption>

</figure>

As shown above, both groups use universal search engines intensively but they differ significantly in using scholarly search engines and search engines designed to find specific objects. While the scientists prefer the first group of tools mentioned above, the students use them very rarely. On the other hand, the scientists seem uninterested in using any tools for searching graphics or audio files, while the students claim to use them more willingly (the question is whether they really use them to find the information objects related to scientific and educational activities as they had declared).

In the next question the mathematicians were asked about their search terms used in search engines, library catalogues, databases etc. The results show (Figure 4) that the majority of the academics insert a name of the author of requested information objects (papers, books, presentations, etc.) (72,41% ) or a title or another name of what they are looking for (68,97%). Keywords representing the topic of the information searched for are typed into the search boxes by 48.28% of the surveyed academics and formal subject headings are used only by 6.90% of the scientists.

Within the group of students, the most popular way to conduct searching in the search engines or other search tools is typing the keywords defining the subject of information needed (69.93%). 50.98% of the surveyed students insert titles or other names of requested information objects and 40.52% type the author’s name. According to the results of this survey the formal subject headings defined in a given information system are used by 26.80% of the students.

<figure>

![Figure 4: Information usually typed into search boxes of search engines, library catalogues, databases etc.](../p644fig4.png)

<figcaption>Figure 4: Information usually typed into search boxes of search engines, library catalogues, databases etc.</figcaption>

</figure>

The differences between the students and the academic staff are presented by the Figure 4\. While the most frequently used way of searching declared by the scientists is the author searching, the students most often perform the keyword searching. What seems to be surprising is the fact that, at least in the light of the results of this survey, the students quite often rely on the subject heading searching (while only two scientists declared that they use subject headings). It appears there is a need to design and conduct another research to explain this phenomenon – it is necessary to find out what they exactly mean by 'subject headings' and whether they really type any formal subject headings into search boxes.

In question 5 the respondents were asked to indicate their most usual behaviour when they start seeking information on the Internet (Figure 5). They could choose only one of two answers. The goal of this question was to find out if the mathematicians gather and use URL addresses known before (e.g., of library websites, scientific portals, subject gateways, publisher websites, etc.) as starting points for searching or rather rely on what search engines can find for them.

79.31% of the academics and 83.01% of the students start the process of searching for scientific information or learning/teaching materials on the Internet from the search engines. Only a relatively small proportion of the scientists (20.69%) and the students (16.99%) begin searching from previously known other starting points. What is not surprising is that 68.67 % of the scientists and 69.93% of the students point to Google (including Google Scholar) as their starting point. Only two (6,90%) scientists identified MathSciNet.

<figure>

![Figure 5: The usual way of starting searching for information on the Internet](../p644fig5.png)

<figcaption>Figure 5: The usual way of starting searching for information on the Internet</figcaption>

</figure>

As it was clearly proven, there are no differences in this aspect of information seeking behaviour between the academics and the students. Both groups often use search engines to start searching than type an URL of any other starting points known before.

The next question (number 6) was posed to get the knowledge on the criteria employed by the mathematicians to decide whether an information object found on the Internet contains scientific knowledge (information, data) or not (Figure 6). Respondents were allowed to select up to three answers each. The most common criterion taken into consideration by the representatives of both surveyed groups when deciding on the character of information found on the Internet is its authorship. This answer was indicated by 68.67% of the academics and 74.51 % of the students.

The next popular criteria used by the academics for this purpose are: the journal in which the text was published (62.07%), the publisher (51.72%) and the organization responsible for the creation of the considered piece of information (37.93%). The fact that a given information object was found using a scientific search engine was important only for 17.24% of the academic staff. Worth highlighting is the fact that 41.38% of the scientists declared that they use other criteria to decide whether a piece of information is scientific or not. Among them, on six occasions (20.68%), the content was declared to be the main criterion for distinguishing between what is scientific and what is not. Two scientists (6.89%) stated that they base their recognition on the individually conducted evaluation without giving any further explanations. Two others informed that they verify the value of information in other sources. Another academic classified information as scientific because of its specific theme, and one pointed to the number of citations as a criterion for verifying the scientific character of a given information object.

On the other hand, the second popular criterion declared by the students was the institution (or organization) responsible for the creation and publication the information on the Internet (72.55%). The students also willingly take into consideration the publisher when they try to decide on the character of the information found on the Internet (54.90%). The title of the journal was indicated as a criterion for determining the scientific character of information significantly more rarely (33.33%) and the fact that the object was found using a scientific search engine was the least common criterion within this group of respondents (25.49%). The answer others was marked by 7.19 % of the students. What seems to be quite interesting, 2.61% of the students declared that they based their opinion on the scientific character of information on their own analysis of its content (one student wrote, for example: '_I check if the resource contains the truth_', apparently thinking that 'true' equals 'scientific'). Two students (1.31%) indicated the usefulness and pertinence of information found on the Internet (for scientific and educational purposes) as criteria for classifying it as scientific. Individual students also expressed other opinions whose meaning is not always clear: _good category of references_, _comparison with other sources_, _'wiki' in the title_ and _the pdf file format_.

<figure>

![Figure 6: Criteria used by the mathematicians to determine the scientific character of information found on the Internet](../p644fig6.png)

<figcaption>Figure 6: Criteria used by the mathematicians to determine the scientific character of information found on the Internet</figcaption>

</figure>

The data gathered with the use of the questionnaire lead us to the conclusion that the scientists rely more heavily on their own judgement about the content of information and on the journal reputation than the students. On the contrary, the students base their judgement more willingly than the scientists on the character of the organization responsible for creating considered piece of information. What is more, the authorship is seen by both groups of the respondents as a decisive criterion for classifying information found on the Internet as scientific. Scientific search engines are generally not seen as tools which could 'certify' the scientific character of the indexed information objects (although the students see them as such a little bit more willingly than the academics).

The last question's aim was to reveal what kind of scientific knowledge (information) is mainly searched for by these two groups of mathematicians on the Internet (Figure 7). The respondents were allowed to select up to two answers each.

All the surveyed academics (100%) indicated the subject knowledge (research findings, theoretical considerations, research sources, etc.), while only 13.79% of them marked the answer _methodological knowledge_. 68.97% of the scientists declared that they searched for information about information sources unavailable on the Internet (reference information). Nobody from the group of academics pointed to the pragmatic knowledge on how to operate within the academia.

For the surveyed students also the subject knowledge seem to be the most important goal when searching the Internet to find the information related to scientific and educational activities (81.05%). 44.44% of the students declared that they seek information about information sources unavailable on the Internet and 39.22% of them informed that they search through the Internet for pragmatic information useful for their education and career in the academia. Generally, the methodological knowledge seldom becomes the goal of seeking information on the Internet. Only 19.61% of the students marked this answer in the questionnaire.

<figure>

![Figure 7: Types of scholarly knowledge searched for on the Internet by the mathematicians](../p644fig7.png)

<figcaption>Figure 7: Types of scholarly knowledge searched for on the Internet by the mathematicians</figcaption>

</figure>

For both groups, the subject knowledge concerning the discipline, including research findings, theoretical considerations, research sources, educational materials for university students, etc., is the type of knowledge which is most often searched for on the Internet. Also both groups declared very little interest in searching the Internet for the methodological knowledge. The difference between the scientists and the students is in searching for the pragmatic knowledge, which is quite popular among the students and totally ignored by the academics. Further research could verify if these preferences are still true when considering the information seeking behaviour of mathematicians also outside the Internet.

## Discussion

Before discussing the research results and drawing any conclusions, it is necessary to make some general remarks indispensable for the proper interpretation of the gathered data.

Firstly, as it was presented in the section of this paper devoted to the information behaviour models, information seeking is always performed in many contexts and is conditioned by various factors. This research was limited to one very specific population of mathematicians (students and scientists from one university unit operating in one city) and only to seeking information related to scientific and educational activities on the Internet. While interpreting the findings, one should keep these restrictions in mind.

Secondly, there could be some doubts about the respondents' proper understanding of some terms used in the questionnaire and about their knowledge on how various tools and information resources operate, which could lead in some cases to mistakes in choosing wrong options presented in the questionnaire (for example Wikipedia was classified as a search engine; there were problems with classifying arXiv.org). In consequence it could have slightly distorted the results.

Some doubts may also arise owing to the relatively big number of respondents who declared that they searched for information in digital libraries, while Polish digital libraries, although quite numerous and rich in various information objects, do not offer many materials for mathematicians. Probably the term 'digital libraries' should have been defined in the questionnaire more precisely to eliminate such inaccuracies.

As the research has proven, in spite of many developments in scholarly communication on the Web (e.g., Web 2.0 tools and resources, new forms of information/knowledge containers or information objects), mathematicians search the Internet mainly to find journal papers and books, which has not changed since 15 years ago when Brown conducted her research to explore the way in which scientists (including mathematicians) sought information to support their teaching and research ([Brown, 1999](#bro99)). Mathematicians have apparently preserved some patterns of information seeking behaviour even after the transition to the modern digital information environment.

Brown found also that they generally preferred browsing full texts to using indexing or abstracting tools and quite willingly tried to get publications directly from their authors. Mathematicians still are not great adherents of abstracting services and quite often search individual scientists' websites to find publications, which may also partly result from another specificity of the information behaviour of mathematicians identified in previous research – their inclination to publish complete lists of their publications with links to the full texts (where available) on their individual websites ([Sapa, 2007](#sap07)).

The findings of the research presented here generally reiterate the results of previous studies on the way scientists start searching the Internet. The surveyed mathematicians, seen as one group, initially use search engines as starting points while seeking information related to their scientific and learning or teaching activities on the Internet. What is not surprising is that the significant majority of the surveyed mathematicians (both the scientists and the students) indicated Google as their starting point. When they use search engines, they generally prefer the universal ones (not only to start searching as it was mentioned above). These quantitative results confirm the findings of the qualitative research conducted by Zhao and others ([Zhao _et al._, 2008](#zha08)). According to those authors, mathematicians use general search engines for any searches, even to find specific mathematical terminology, also when they are conscious about the serious limitations and weaknesses of such tools for domain-specific information searching. As the main reasons for this behaviour they identified the quick response of search engines and the variety of information found. Also another research on information behaviour of students (including students of mathematics) regarding the period 1985-2003 ([Junni, 2007](#jun07)) confirms the general tendency to gradually replace the variety of tools and resources used for finding information by one prevailing model based on general search engines. What is more, the approach to information seeking on the Internet based on using general search engines to start and complete searching is common also among the representatives of other scientific disciplines (e.g., [Haglund and Olsson, 2008](#hag08)), including for example physicists and astronomers ([Jamali and Asadi, 2010](#jam10)). Even if they also consult other resources (like the mathematicians surveyed here - see the graph 2 above), Google dominates their information seeking behaviour.

On the other hand, the dominant role of Google was less obvious a few years ago and was not confirmed by the research conducted by Hemminger and his colleagues ([Hemminger _et al._, 2007](#hem07)) who surveyed mainly doctoral students and found that they used bibliographic/citation databases more often (47%) than general search engines (30%). This dissimilarity may result from various reasons: the significant expansion of search engines in scholarly communication since that time, the different specificity of scientific disciplines represented by the research subjects (only few respondents in that research were mathematicians), the different roles of the research subjects in academic environment (PhD students versus professional scientists and undergraduate and graduate students) or the different character of the questions asked - the research presented here was focused on information seeking behaviour on the Internet while the research conducted by Hemminger and his colleagues was aimed at revealing any information resources used by the respondents. Furthermore, also in contradiction to some previous research results regarding information seeking behaviour of scientists representing other disciplines (e.g., [Haglund and Olsson, 2008](#hag08); [Jamali and Nicholas, 2008](#jam08)), we found that scholarly oriented search engines are being used quite often, even if not as often as general search engines. Growing interests in using such tools by scientists confirms the process noticed already a few years ago by Hemminger and his colleagues ([Hemminger _et al._, 2007](#hem07)).

The domination of Google does not mean the total elimination of other tools and resources from mathematicians' strategies. As proven in previous researches (e.g., [Zhao _et al._, 2008](#zha08)), although mathematicians willingly start from simple Google searching, they also use browsing techniques on subsequent stages of information seeking and use scientific oriented portals (for example MathSciNet). To find information needed they monitor, search or browse a wide spectrum of information resources available on the Web from library websites to social networking sites. What seems of interest is the fact that the research presented here has revealed the relatively high rate of searching for multimedia objects and using search engines designed to find specific objects (e.g., graphics, audio files) by the surveyed students.

Formulating queries (not only in search engines but also in other searching tools offered in library catalogues, databases, scholarly oriented portals, etc.) the surveyed mathematicians type the authors' names, titles of publications or uncontrolled keywords rather than formal subject headings. Surprisingly a big number of students declared using the formal subject heading. It requires further research to be fully explained. It seems that the gathered data are insufficient to explain this phenomenon, which is in contrast with other research results concerning the role of the formal subject headings in information seeking processes (e.g., [Sapa, 1997](#sap97); [Yu and Young, 2004](#yu04); [Antell and Huang, 2008](#ant08); [Grey, 2012](#gre12)).

The respondents were mostly interested in searching the Internet for the subject knowledge contained in scientific journal papers and books. They also quite often look for the information about sources unavailable on the Internet. What may seem to be surprising, only few of them declared looking for methodological and practical information regarding the academic profession or studies. Although some previous research also showed that generally scientists searched the Internet not only for subject knowledge but also for other kinds of knowledge indispensable for their research and educational activities within the academia (e.g., [Jamali and Asadi, 2010](#jam10)), there have not been any quantitative results regarding mathematicians' goals of information seeking on the Internet, so the results received here cast new light on this problem.

The research on the information seeking behaviour of mathematicians conducted so far has not answered the question about the criteria used by this group of scientists and students to determine the scientific character of information found on the Internet. The results presented here give a new insight into these aspects of their information behaviour, stressing the role of authorship, affiliation and intermediaries (publishers, journals). The surveyed mathematicians base their judgement of the character of information found on the Internet on various 'reputation' criteria, generally not treating scholarly search engines as trustworthy tools to filter out what is not scientific.

## Conclusions

The first goal of the research presented here was to explore selected aspects of the information seeking behaviour of mathematicians on the Internet. Another goal was to reveal the differences and similarities between the behaviour of scientists and students of mathematics. There have not yet been any studies comparing the information seeking behaviour on the Internet (in its present form and with its present functionalities) of professional mathematicians and students, so the findings received here can be seen as pioneering in this field.

Any models of information behaviour do not offer deterministic, ready to use _reason-result relations_. They show possible sources of impact and influences instead. The opinions on the reasons of the differences identified during the research presented below were formulated mainly within the frames provided by the model of information behaviour of professionals presented by Leckie and Pettigrew, the model based on students' information seeking behaviour in electronic environment proposed by Urquhart and Rowley and similar model by Al-Muomen, Morris and Maynard (all of them have been briefly described above).

Some of them could result from different levels of self-confidence connected with various levels of experience in scientific work. The students are likely to rely more on objective and well known symptoms of the scientific character of information (e.g., character of the institution responsible for the creation of a considered piece of information or the name of the publisher) – which confirms some findings of earlier research (e.g., [Liu, 2004](#liu04); [Rieh, 2002](#rie02)) - while the scientists more often believe in their own abilities to differentiate between what is scientific and what is not; as well as in the role of journals as determinants of the scientific character of published texts (probably thanks to their deeper knowledge about scientific journals). The students also search through scientific institutions' websites more often than the scientists do. Furthermore, while the scientists do not look for pragmatic information at all, students willingly search for such knowledge.

Other differences may be based on distinct tasks to be undertaken within the academic environment by both groups of the respondents. The students search more often for reference works (encyclopaedias, dictionaries, etc.) and use more often search engines designed to find specific objects (e.g., graphics, audio files, multimedia objects). Having in mind the risk mentioned above, it may also result from their learning activities and homework assignments.

There are also some differences, which could result from the change in the general paradigm of information searching. The students, as undoubtedly the younger group, grew up with the Internet. They use keyword searching and social networking sites more willingly and, at the same time, look for information on library websites or publishers' websites significantly less often than the scientists. Furthermore, the students also search for bibliographic descriptions or availability information of publications less often than the scientists.

It is necessary to stress that the research presented in this paper was designed to reveal differences in the information behaviour but not to identify their reasons in a definite way. The suggestions regarding the reasons of detected differences in behaviour between the two surveyed groups of mathematicians are based on the research findings and on their interpretations in the light of some information seeking behaviour models mentioned in this paper and should be seen as hypotheses that need to be proven or refuted in further research.

## Acknowledgements

The authors would like to thank the reviewers and the copy editor for their valuable input to the text.

## <a id="author"></a>About the authors

**Remigiusz Sapa** is an Associate Professor in the Institute of Library and Information Science of the Jagiellonian University in Kraków, Poland. He received his PhD in Book Science from the University of Warsaw, Poland, and his habilitation in Book and Information Sciences, from the University of Silesia in Katowice, Poland. He can be contacted at: [remigiusz.sapa@uj.edu.pl](mailto:remigiusz.sapa@uj.edu.pl)  
**Monika Krakowska** is an Associate Professor in the Institute of Library and Information Science of the Jagiellonian University in Kraków, Poland. She received her PhD in Book Science from the University of Silesia in Katowice, Poland. She can be contacted at: [monika.krakowska@uj.edu.pl](mailto:monika.krakowska@uj.edu.pl)  
**Małgorzata Janiak** is an Associate Professor in the Institute of Library and Information Science of the Jagiellonian University in Kraków, Poland. She received her PhD in Book Science from the University of Silesia in Katowice, Poland. She can be contacted at: [malgorzata.janiak@uj.edu.pl](mailto:malgorzata.janiak@uj.edu.pl)

</section>

<section>

## References

*   Al-Muomen, N., Morris, A. & Maynard, S. (2012). Modelling information-seeking behaviour of graduate students at Kuwait University. _Journal of Documentation, 68_(4), 430–459.
*   Antell, K. & Huang, J. (2008). Subject searching success transaction logs, patron perceptions, and implications for library instruction. _Reference & User Services Quarterly, 48_(1), 68–76.
*   Bandura, A. (1969). Social-learning theory of identificatory process. In D.A. Goslin (Ed.), _Handbook of socialization theory and research_ (pp. 213-262). Chicago: Rand McNally & Company.
*   Bandura, A. (1977). Self-efficacy: toward a unifying theory of behavioral change. _Psychological Review, 84_(2), 191-215.
*   Barjak, F. (2004). [From the "analogue divide" to the "hybrid divide": no equalisation of information access in science through the internet.](http://www.webcitation.org/6SFwmw383) In _Proceedings of the AoIR-ASIST 2004 Workshop on Web Science Research Methods_, Brighton, UK. Retrieved 12 December, 2012 from http://cybermetrics.wlv.ac.uk/AoIRASIST/Barjak_hybrid_divide.pdf (Archived by WebCite® at http://www.webcitation.org/6SFwmw383)
*   Barjak, F. (2006). The role of the Internet in informal scholarly communication. _Journal of the American Society for Information Science and Technology, 57_(10), 1350-1367.
*   Bates, M. J. (1999). The invisible substrate of information science. _Journal of the American Society for Information Science, 50_(12), 1043-1050.
*   Brindesi, H. & Kapidakis, S. (2011). [_Information seeking behavior of Greek astronomers._](http://www.webcitation.org/6SFxXewWQ) Retrieved 27 January, 2013 from http://eprints.rclis.org/15852/1/09.Brindesi.pdf (Archived by WebCite® at http://www.webcitation.org/6SFxXewWQ)
*   Brown, C.M. (1999). Information seeking behavior of scientists in the electronic information age: astronomers, chemists, mathematicians, and physicists. _Journal of the American Society for Information Science, 50_(10), 929-943.
*   Buchanan, G., Cunningham, S.J., Blandford, A., Rimmer, J. & Warwick, C. (2005). Information seeking by humanities scholars. _Lecture Notes in Computer Science, 3652_, 218-229.
*   Davis, P.M. (2004). Information-seeking behavior of chemists: a transaction log analysis of referral URLs. _Journal of the American Society for Information Science and Technology, 55_(4), 326-332.
*   Dervin, B. (1998). Sense-making theory and practice: an overview of user interests in knowledge seeking and use. _Journal of Knowledge Management, 2_(2), 36-46.
*   Ellis, D. (1989). A behavioural approach to information retrieval system design. _Journal of Documentation, 45_(3), 171-212.
*   Ellis, D. (2005). Ellis's model of information-seeking behaviour. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behaviour_ (pp. 38-142). Medford, NJ: Information Today, Inc.
*   Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information-seeking patterns of researchers in the physical and social sciences. _Journal of Documentation, 49_(4), 356-369.
*   Ellis, D., Wilson, T.D., Ford, N., Foster, A, Lam, H.M., Burton, R. & Spink, A. (2002). Information seeking and mediated searching. Part 5: User-intermediary interaction. _Journal of the American Society for Information Science and Technology, 53_(11), 883-893.
*   Fisher, K.E., Erdelez, S. & McKechnie, L. (Eds.). (2005). _Theories of information behaviour_. Medford, New York: Information Today, Inc.
*   Grey, A. (2012). So you think you're an expert: keyword searching vs. controlled subject headings. _Codex: The Journal of the Louisiana Chapter of the ACRL, 1_(4), 15-26.
*   Haines, L.L., Light, J., O'Malley, D. & Delwiche, F.A. (2010). Information-seeking behavior of basic science researchers: implications for library services. _Journal of the Medical Library Association, 98_(1), 73-81.
*   Haglund, L. & Olsson, P. (2008). The impact on university libraries of changes in information behavior among academic researchers: a multiple case study. _Journal of Academic Librarianship, 34_(1), 52-59.
*   Hemminger, B.M., Lu, D., Vaughan, K.T.L. & Adams, S.J. (2007). Information seeking behavior of academic scientists. _Journal of the American Society for Information Science and Technology, 58_(14), 2205-2225.
*   Jamali, H.R. & Asadi, S. (2010). Google and the scholar: the role of Google in scientists' information-seeking behavior. _Online Information Review, 34_(2), 282-294.
*   Jamali, H.R. & Nicholas, D. (2008). Information-seeking behaviour of physicists and astronomers. _Aslib Proceedings, 60_(5), 444-462.
*   Jamali, H.R. & Nicholas, D. (2010). Interdisciplinarity and the information-seeking behavior of scientists. _Information Processing and Management, 46_(2), 233-243.
*   Jankowska, M.A. (2004). Identifying university professors' information needs in the challenging environment of information and communication technologies. _Journal of Academic Librarianship, 30_(1), 51-66.
*   Jaskowska, M., Koryci&nacute;ska-Huras, A. & Próchnicka, M. (2009). Zachowania informacyjne autorów prac licencjackich z zakresu informacji naukowej i bibliotekoznawstwa. Tendencje w wykorzystaniu &zacute;róde&lstrok; elektronicznych i drukowanych. In K. Migo&nacute; & M. Skalska-Zlat (Eds.), _Uniwersum pi&sacute;miennictwa wobec komunikacji elektronicznej_ (pp. 291-311). Wroc&lstrok;aw, Poland: Wydaw. UWr.
*   Johnson, D. I. (2003). On contexts of information seeking. _Information Processing and Management, 39_(5), 735-760.
*   Joshi, P.A. & Nikose, S.M. (2010). [_Mapping Internet information literacy among faculty members: a case study of Rajiv Gandhi College of Engineering, Chandrapur._](http://www.webcitation.org/6SFzVOuYF) Retrieved 11 December, 2012 from http://eprints.rclis.org/bitstream/10760/14375/1/Mapping_Internet_Information_Literacy_among_Faculty_Members.pdf (Archived by WebCite® at http://www.webcitation.org/6SFzVOuYF)
*   Junni, P. (2007). [Students seeking information for their Master's theses: the effect of the Internet.](http://www.webcitation.org/6SFzeaxvF) _Information Research, 12_, paper 2\. Retrieved 25 January, 2013 from http://informationr.net/ir/12-2/paper305.html (Archived by WebCite® at http://www.webcitation.org/6SFzeaxvF)
*   Kuhlthau, C.C. (2005). Kuhlthau's information search process. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behaviour_ (pp. 230-234). Medford, NJ: Information Today, Inc.
*   Leckie, G.I. & Pettigrew, K.E. (1997). A general model of information seeking of professionals: role theory in the back door. In P. Vakkari, R. Savolainen & B. Dervin, (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts 14-16 August, 1996, Tampere, Finland_ (pp. 99-110). London: Taylor Graham Publishing.
*   Liu, Z. (2004). Perceptions of credibility of scholarly information on the Web. _Information Processing and Management, 40_(6), 1027-1038.
*   Makri, S., Blandford, A. & Cox, A.L. (2008). Investigating the information-seeking behaviour of academic lawyers: from Ellis's model to design. _Information Processing and Management, 44_(2), 613-634.
*   Makri, S. & Warwick, C. (2010). Information for inspiration: understanding architects' information seeking and use behaviors to inform design. _Journal of the American Society for Information Science and Technology, 61_(9), 1745-1770.
*   Marchionini, G. (2008). Human–information interaction research and development. _Library & Information Science Research, 30_(3), 165-174.
*   Meho, L.I. & Tibbo, H.R. (2003). Modelling the information-seeking behavior of social scientists: Ellis's study revisited. _Journal of the American Society for Information Science and Technology, 54_(6), 570-587.
*   Miwa, M. (2005). Bandura's social cognition. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behaviour_ (pp. 54-57). Medford, NJ: Information Today, Inc.
*   Newby, J. (2005). An emerging picture of mathematicians' use of electronic resources: the effect of withdrawal of older print volumes. _Science & Technology Libraries, 25_(4), 65-85.
*   Nicholas, D. (2008). The information-seeking behaviour of the virtual scholar: from use to users. _Serials: The Journal for the Serials Community, 21_(2), 89-92.
*   Nied&zacute;wiedzka, B. (2003). [A proposed general model of information behaviour.](http://www.webcitation.org/63UQ9trxT) _Information Research, 9_(1), paper 164\. Retrieved from http://informationr.net/ir/9-1/paper164.html (Archived by WebCite® at http://www.webcitation.org/63UQ9trxT)
*   Niu, X. & Hemminger, B.M. (2012). A study of factors that affect the information-seeking behavior of academic scientists. _Journal of the American Society for Information Science and Technology, 63_(2), 336-353.
*   Niu, X., Hemminger, B.M., Lown, C., Adams, S., Brown, C., Level... & Cataldo, T. (2010). National study of information seeking behavior of academic researchers in the United States. _Journal of the American Society for Information Science and Technology, 61_(5), 869-890.
*   Rieh, S. Y. (2002). Judgment of information quality and cognitive authority in the web. _Journal of the American Society for Information Science and Technology, 53_(2), 145-161.
*   Ross, C. & Terras, M. (2011). [Scholarly information-seeking behaviour in the British Museum online collection.](http://www.webcitation.org/6SG0729jy) In _Museums and the Web 2011_, Retrieved 26 November, 2012 from http://www.museumsandtheweb.com/mw2011/papers/scholarly_information_seeking_behaviour_in_the.html (Archived by WebCite® at http://www.webcitation.org/6SG0729jy)
*   Rowlands, I., Nicholas, D., Williams, P., Huntington, P., Fieldhouse, M., Gunter... & Tenopir, C. (2008). The Google generation: the information behaviour of the researcher of the future. _Aslib Proceedings, 60_(4), 290-310.
*   Sapa, R. (1997). Zachowania informacyjne u&zdot;ytkownika OPAC w Bibliotece Jagiello&nacute;skiej. [User information behaviour in the OPAC of the Jagiellonian Library.]. _Zagadnienia Informacji Naukowej, 70_(2), 70-79.
*   Sapa, R. (2007). Indywidualne strony WWW historyków i matematyków we wspó&lstrok;czesnej komunikacji naukowej. [Individual WWW pages of historians and mathematicians in modern scientific communication]. In M. Kocójowa (Ed.), _Mi&eogon;dzy przesz&lstrok;o%sacute;ci&aogon;. a przysz&lstrok;o&sacute;ci&aogon;. Ksi&aogon;&zdot;ka, biblioteka, informacja naukowa - funkcje spo&lstrok;eczne na przestrzeni wieków_ (pp. 272-278). Kraków, Poland: Wyd. UJ.
*   Savolainen, R. (2007). Information behavior and information practice: reviewing the "umbrella concepts" of information-seeking studies. _Library Quarterly, 77_(2), 109-132.
*   Shenton, A.K. & Hay-Gibson, N.V. (2011). Information behaviour and information literacy: the ultimate in transdisciplinary phenomena? _Journal of Librarianship and Information Science, 43_(3), 166-175.
*   Tahir, M., Mahmood, K. & Shafique, F. (2010). Use of electronic information resources and facilities by humanities scholars. _The Electronic Library, 28_(1), 122-136.
*   Tramullas, J. & Sánchez-Casabón, A.I. (2010). Scientific information retrieval behavior: a case study in students of philosophy. In D.E. Losada, P. Castells & J.M. Fernández-Luna, (Eds.), [_Actas del 1er Congreso Español de Recuperación de Información CERI 2010 = Proceedings of the 1st Spanish Conference on Information Retrieval_](http://www.webcitation.org/6SG0QzQem) (pp. 251-258). Madrid: Univ. Autónoma de Madrid. Retrieved 18 January, 2103 from http://eprints.rclis.org/14776/ (Archived by WebCite® at http://www.webcitation.org/6SG0QzQem)
*   Urquhart, C. & Rowley J. (2007). Understanding student information behavior in relation to electronic information services: lessons from longitudinal monitoring and evaluation. Part 2\. _Journal of the American Society for Information Science and Technology, 58_(8), 1188-1197.
*   Whitmire, E. (2002). Disciplinary differences and undergraduates' information-seeking behavior. _Journal of the American Society for Information Science and Technology, 53_(8), 631–638.
*   Wilson, T.D. (2000). [Human information behaviour.](http://www.webcitation.org/6SG2MrLa4) _Informing Science:, 3_(2), 49-55\. Retrieved 6 November, 2012 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf (Archived by WebCite® at http://www.webcitation.org/6SG2MrLa4)
*   Yu, H. & Young, M. (2004). The impact of Web search engines on subject searching in OPAC. _Information Technology & Libraries, 23_(4), 168-180.
*   Zhao, J., Kan, M-Y. & Theng, Y.L. (2008). [Math information retrieval: user requirements and prototype implementation.](http://www.webcitation.org/6SG18aoFD) In R. Larsen, A. Paepcke, J. Borbinha and M. Naaman, (Eds.), _Proceedings of the 8th ACM/IEEE-CS joint conference on digital libraries_ (pp. 187-196). New York: ACM. Retrieved 11 October, 2012 from https://www0.comp.nus.edu.sg/~kanmy/dossier/papers/fp096-zhao.pdf (Archived by WebCite® at http://www.webcitation.org/6SG18aoFD)
*   Zhu, Y-Q., Chen, L-Y., Chen, H-G. & Chern, C-C. (2011). How does Internet information seeking help academic performance? The moderating and mediating roles of academic self-efficacy. _Computers & Education, 57_(4), 2476-2484.

</section>

</article>

* * *

## <a id="app"></a>Appendix. Questionnaire – general version

_Dear Sir or Madam_

_We would like to ask you to fill in this short questionnaire on the way you search for scientific information. To indicate the appropriate answer (answers) please cross the preceding box (X) (or boxes)._

<form>

<fieldset><legend>1. What kind of information sources useful for scientific and educational activities do you search on the Internet most often (what would you like to find)? [please indicate 3]:</legend>  
<input type="checkbox"> scientific articles (full texts)  
<input type="checkbox"> scientific books (full texts, including textbooks, course books, etc.)  
<input type="checkbox"> abstracts of scientific texts  
<input type="checkbox"> information about scientific articles and books (bibliographic descriptions, availability)  
<input type="checkbox"> reference works (encyclopaedias, dictionaries, lexicons, atlases, etc.)  
<input type="checkbox"> fact databases  
<input type="checkbox"> multimedia objects  
<input type="checkbox"> others: (which?.......................................................................................................)  
<input type="checkbox"> I don't search for such information sources on the Internet</fieldset>

<fieldset><legend>2. Where exactly do you look for information? [please indicate 3 the most frequently used sites]:</legend>  
<input type="checkbox"> on library websites (which library?..................................................................)  
<input type="checkbox"> on disciplinary oriented scientific and educational portals (which portals?.......................................................................................)  
<input type="checkbox"> on publishers' websites  
<input type="checkbox"> in open access repositories and archives of scientific outputs  
<input type="checkbox"> on individual scientists' websites  
<input type="checkbox"> on websites provided by scientific institutions and organizations  
<input type="checkbox"> on scholarly oriented social networking sites (e.g., discussion forums, bookmark and publication sharing systems, data sharing systems, blogs, wikis, etc.)  
<input type="checkbox"> in digital libraries  
<input type="checkbox">  in other resources (.................................................................................)</fieldset>

<fieldset><legend>3. Which searching tools do you use while seeking information useful for scientific and educational activities? [please indicate 2 the most relevant answers]:</legend>  
<input type="checkbox"> universal search engines (e.g., Google, Yahoo) (others:..................................................................)  
<input type="checkbox"> scholarly oriented search engines (e.g., Google Scholar, Scirus) (others:..................................................................)  
<input type="checkbox"> search engines designed to find specific objects (e.g., graphics, audio files) (which?:..................................................................)  
<input type="checkbox"> others (........................................................................................)</fieldset>

<fieldset><legend>4. What do you usually type into the search boxes of search engines, library catalogues, databases etc.? [please indicate maximum 2 answers]:</legend>  
<input type="checkbox"> author's family name  
<input type="checkbox"> titles/other names of requested information objects  
<input type="checkbox"> keywords representing the topic of the information searched for  
<input type="checkbox"> formal subject headings used in a given information system</fieldset>

<fieldset><legend>5. How do you usually start searching for information on the Internet? [please indicate 1 answer]:</legend>  
<input type="checkbox"> a search engine (which one? ....................................................................)  
<input type="checkbox"> typing an URL known before into your Web browser (e.g., of a library Website, scientific portal, subject gateway, publisher etc.)</fieldset>

<fieldset><legend>6.What do you take into consideration when deciding whether an information object found on the Internet contains scientific information or not? [please indicate 3 most relevant answers]:</legend>  
<input type="checkbox"> institution or organization responsible for its creation  
<input type="checkbox"> author  
<input type="checkbox"> publisher  
<input type="checkbox"> journal title  
<input type="checkbox"> whether the object was found with the help of a scientific search engine  
<input type="checkbox"> others (specify:......................................................................................)</fieldset>

<fieldset><legend>7. What kind of scientific knowledge do you seek on the Internet? [please indicate 2 answers]:</legend>  
<input type="checkbox"> subject knowledge (research findings, theoretical considerations, research sources, etc.)  
<input type="checkbox"> methodological knowledge  
<input type="checkbox"> pragmatic knowledge on how to operate within the academia (as a scientist or student)  
<input type="checkbox"> information about the information sources unavailable on the Internet</fieldset>

</form>