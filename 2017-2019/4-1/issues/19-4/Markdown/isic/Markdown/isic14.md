<header>

#### vol. 19 no. 4, December, 2014

##### Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# Crisis-based information seeking: monitoring versus blunting in the information seeking behaviour of working students during the Southeast Asian Haze Crisis

#### [Natalie Pang](#author)  
Wee Kim Wee School of Communication and Information, Nanyang Technological University, 04-39, 31 Nanyang Link, Singapore 637718

#### Abstract

> **Introduction.** Context often provides the basis for studying information behaviour, and much of it has been studied in terms of particular occupations, roles, or socioeconomic status. A less studied, but as important basis is crisis coping as shaping information seeking. This paper focuses on a haze crisis as one such situation, and examines the information seeking behaviour of concerned working students coping with the crisis using Miller's coping styles and Johnson's comprehensive model of information seeking as the main theoretical framework.  
> **Method.** A survey utilising proportionate quota sampling was administered to 339 students who hold a job in Singapore during the Southeast Asian Haze Crisis in 2012\.  
> **Analysis.** T-tests and analysis of variance were used to determine the effect of coping styles on each of the four antecedents in Johnson's model, and these factors were also studied for their influences in shaping information seeking on four information sources.  
> **Results.** Coping style was found to be a significant variable in influencing three of the four antecedents in the comprehensive model: demographics (age), experience, and salience. Significant effects are also found for age, experience, salience, beliefs, and coping styles on information seeking in mainstream media, new media, personal networks, and healthcare professionals.  
> **Conclusions.** The study demonstrated the importance of incorporating coping style as a contingent factor to be incorporated into CMIS to better understand information seeking behaviour in a crisis.

## Introduction

There has been much work investigating the information behaviour of individuals in everyday life. Recent studies have mainly been carried out using the basis of occupations, roles, demographic and socioeconomic groups; an observation by Case ([2012](#Cas12)), during his extensive synthesis of the studies carried out post-1990s.

An increasingly important but less investigated context is that of situation, in which situational characteristics may present unique circumstances for information seeking or behaviour. Savolainen ([1993](#Sav93)) described situations as '_the time-space context in which sense is constructed_' (p. 17). In the framework of sense making, this also means that situations are not objective attributes, but reflect individuals' interpretations of a given situation. Vakkari ([1997](#Vak97)), in noting the importance of studying the influence of society, also called for greater attention to be paid to '_the actions, tasks and situations_' that provides underlying forces shaping information behaviour. Because interpretations of situations depends on individual sense making, it implies that the study of situational elements can be ambiguous, since it can lie anywhere between a spectrum of passive or reflexive monitoring of a situation to seeking solutions to a specific problem ([Waldron and Dervin, 1988](#Wal88)).

Although several information seeking behaviour models (such as Dervin's sense-making approach) recognise the relevance and importance context and time in shaping information seeking behaviour, not many have formally conceptualised it. Savolainen ([2006](#Sav06)) attempted to conceptualise it by identifying three aspects of temporality: time as bounded with social and cultural factors to produce individual information behaviour, time as a constraint reducing or shaping individual information behaviour, or time as the implicit link for linear information behaviour activities. Allen ([2011](#All11)) on the other hand, provides empirical illustrations on how time-constrained information behaviour can look like. Such empirical work is important and more still needs to be done, and time is but one aspect of context.

The gap in considering situational elements as context in information behaviour research may also be explained by Johnson ([2003](#Joh03)) when attempting to highlight the difficulties in studying context. Given that much work in the library and information science discipline are cross-sectional research which usually happen in a controlled time-space frame, there is little motivation for researchers to study the effects of and on changing situational contexts. Additionally, context itself is a concept that can have multiple levels of analysis when explicated, and it is incredibly challenging to account for multiple levels of analysis within the scope of one study. There is also the related problem of the researcher's theoretical orientation and paradigm. For instance, Johnson ([2003](#Joh03)) observed that a researcher may be satisfied with his treatment of context at a dyadic level or describing context as an interaction of a finite number of observable factors, other researchers may completely refute such explanations as being too simplistic and prefer discourse analysis which could account for context as something that reflects overall structural complexities.

To address these issues Johnson ([2003](#Joh03)) clarified three approaches by which contexts have been studied: situation, contingency and frameworks. Research that looks at contexts as specific situations tend to have a positivist orientation, which view elements within an environment which can shape individual information behaviour. Contingency approaches however, move beyond understanding context as a list of influential factors and seeks to understand links between factors in a situation to how they shape outcomes and processes in information behaviour. As Johnson ([2003](#Joh03)) puts it, '_a contingent approach to context is concerned with specifying key situational factors which produce predictable states of information seeking_' (p. 740). The third approach utilises frameworks - a postmodern oriented concept stipulating the use of frames as a way of viewing context and of interpreting it.

The study described in this paper adopted the contingency approach to account for context, with the situation being a major haze crisis that happened in the Southeast Asia in 2013\. I will first describe the context with the goal to identify the contextual elements that are relevant to the crisis. With these factors, I will discuss the theoretical framework - CMIS (Comprehensive Model of Information Seeking) by Johnson ([2003](#Joh03)) and the relevance of coping styles contextualised to this particular crisis.

#### The Southeast Asian Haze Crisis

The practice of deforestation by fire has been a usual practice particularly in Indonesia. Deforestation alternatives were costly and farmers simply cannot afford them, the fragmented flow of information coupled with disjointed and remote farming areas lead to the lack of sharing practices and technology amongst farmers, and political struggles in the country in the last three decades have contributed to on-going challenges with enforcing laws against practices by the farming community.

These are factors arising from the structuring of Indonesia's society. These factors however, do mean that its neighbours such as Singapore has to deal with the haze as an annual affair that comes up during the dry season, a period that can lie anywhere between March to August of each year. At the macro level, information is religiously monitored and disseminated by the [National Environmental Agency (NEA)](#Nat) of Singapore in various forms: Pollutant Standards Index (PSI) readings are released on its website, social media, press releases and reported by mainstream media. Since it is a regular issue for Singapore, residents typically deal with the haze as part of everyday life.

However, in June 2013, Singapore encountered the worst haze outbreak in its history when the PSI value hit 401, way above the 'hazardous level', the highest level of health alert ([Yong, 2013](#Yon13)). This broke Singapore's record of the previous high of 226 encountered in September 1997\. Residents were also concerned as the Particulate Matter (PM) 2.5 value reached a high of 300 micrograms per cubic meter on 22 June 2013 (National Environment Agency [NEA], n.d.). PM 2.5 refers to fine particles less than 2.5 micrometres in diameter and is believed to pose the greatest health risks.

Although the public were cautioned to avoid outdoor activity and physical exertions, no action was taken to prohibit all outdoor activities, including commuting to work. This move provided the main rationale for the study to be based on working students, as they have high risks of exposure given the amount of time they have to spend commuting to and from home, work, and study.

Information on the haze and articles on how it affects one's health were updated and transmitted daily to the public via various media platforms in Singapore. Mainstream newspapers, radio and television channels reported on the haze daily. The Internet was also an extremely popular information source as people could obtain the latest status on the PSI reading, information on health risks and immediately exchange information in social media on the measures to protect their health. At one point during the crisis, accessibility to the latest PSI reading at NEA's website was disrupted due to heavy web traffic. The rush to buy protective masks and the spread of rumours about the shortage of masks provided further evidence of widespread panic and concern. For those who work in many organisations, their working environments were a key source of information field where individuals acquire useful information in order to deal with the crisis.

These circumstances gave rise to a number of interesting questions. Although the highly developed infrastructure of the Internet and widespread connectivity within the republic provided ubiquitous accessibility to timely information, the amount of misinformation on the Internet were observed to cause anxiety to the public. For example, irresponsible Internet users giving false health advice on the haze and the rumours that the PSI readings provided by NEA were untrue could contribute to the erosion of trust in the information released by authorities ([Cheng, 2013](#Che13)).

What was the role of information seeking in coping with the crisis? How do coping styles in a crisis shape the antecedents of information seeking? How do coping styles, together with antecedents of information seeking, shape information seeking on different information sources? These three questions guide our investigation in this study.

#### Related work

Various scholars have established that individuals will seek out health information if they are facing health challenges ([Weaver III _et al_., 2010](#Wea10)), are generally interested in the topic of health, or when faced with situations that raise uncertainty about their health ([Rosen and Knäuper, 2009](#Ros09)).

In a study by Watson, Riffe, Smithson-Stanley and Ogilvie ([2013](#Wat13)), it was found that individual's perceived risks or self-efficacy are influenced primarily by their media use rather than their demographics and previous exposure to environmental risks. As such, characteristics of the channels through which information is conveyed to the public should be considered. For instance, informing health conscious individuals about the different types of prevention and treatment is effective through Internet given its accessibility and diversity of sources available ([Dutta-Bergman, 2004](#Dut04)).

The most common sources of health related information are television news, newspaper and online sources ([Crighton _et al_., 2013](#Cri13); [Griffin and Dunwoody, 2000](#Gri00); [Semenza _et al_., 2008](#Sem08); [Watson _et al_., 2013](#Wat13)). Anaman ([2001](#Ana01)) suggests that radio and television broadcasts were perceived as the most important carriers of haze-related information. In another study about the influenza pandemic that occurred in Germany in 2009/10, mass media such as television, radio, and print media were the main sources used to gather information about the disease ([Walter, Böhmer, Reiter, Krause and Wichmann, 2012](#Wal12)). Respondents in the study also saw friends, relatives, and physicians as important sources of information.

Credibility, trustworthiness, and comprehensiveness are some characteristics of information sources mentioned in the study by Johnson, Donohue, Atkin and Johnson ([1995](#Joh95)). In Marton and Choo ([2012](#Mar12)), a review of studies on theoretical models of health information seeking on the web indicated that characteristics of information source such as accessibility, reliability, usefulness and quality are commonly used as factors that influence preference of online information sources. Cutilli ([2010](#Cut10)) found that healthcare professionals are often cited as the most common and trusted source. Yet, these source characteristics are seldom explored in information seeking behaviour during crises such as haze situations. Moreover, in a haze situation, it is also possible that most individuals may not seek healthcare professionals' advice if they feel that their health problem is attributed to haze and is temporary.

Studies involving similar health crises where there is a widespread health concern consistently found proactive information use associated with information seeking ([De Zwart, Veldhuijzen, Richardus, and Brug, 2010](#Dez10)). Other studies also found that individuals who received H1N1-related information had been associated with intentions to engage in infection avoidance and prevention behaviors during the influenza pandemic ([Wong, 2010](#Won10); [van der Weerd, Timmermans](#van11)). During environmental pollution, most of the 14 new mothers interviewed expressed a willingness to use the information gathered to prevent health risks caused by environmental pollution ([Crighton _et al_., 2013](#Cri13)). Bickerstaff and Walker ([1999](#Bic99)) found that individuals do take protective actions during periods of poor air quality but such actions are more intense when they have a health concern. Thus, this is attributed to their own sense of state of health rather than active information seeking. This is interesting as it may suggest that perceived salience of related health information may be part of an individual's preventive actions.

There is consistent evidence that individuals seek information when faced with crisis or uncertain situations in order to mitigate tensions and anxiety, and to come up with informed responses to the threats ([Seeger, Sellnow and Ulmer, 2003](#See03)). That said, people may not always engage in information seeking logically and rationally. For instance, the need to reduce anxiety may be so overwhelming that they stop monitoring the situation for information the moment they get immediate assurance about the situation. Kruglanski and Mayseless ([1987](#Kru87)) argued that this may be due to differences in individuals' need for closure. Those with high need for closure make conclusions about the threatening situation quickly, even if information is limited ([Heaton and Kruglanski, 1991](#Hea91)) or rely on past experiences and knowledge ([Ford and Kruglanski, 1995](#For95)). Such behaviour may also be likened to blunting styles of coping, where individuals choose to avoid information the moment they feel they know enough.

#### Theoretical framework

The questions asked in the study bring to mind concepts in Johnson's Comprehensive Model of Information Seeking (CMIS). Although the model was empirically developed in the context of cancer-related information seeking ([Johnson, 1993](#Joh93)), it was intended, and has been applied to other contexts ([Johnson, Donohue, Atkin and Johnson, 1995](#Joh95)). Whilst the variables in the model consistently comes up as significant, specific relationships can differ, which suggests that there may be contingent effects presented by the context ([Fry and Smith, 1987](#Fry87)).

CMIS consists of three categories of variables (see Figure 1). Antecedents (demographics, experience, salience and beliefs) provide underlying imperatives to seek information.

Demographics can refer to the individual's age, education, gender, income status, and ethnicity depending on the context of the study. Together with _experience_, they make up the _background factors_ to information seeking in CMIS. The concept of experience can be of contention: it can involve related concepts of memory and recall, as well as resources a person may have on hand to be able to tap on knowledge regarding the topic of interest. For instance, when it comes to knowing what to do in a haze crisis, an individual who had prior experience from living in countries that have similarly been affected by air pollution, or know someone who has may have greater experience compared to others.

<figure>

![Figure 1: The Comprehensive Model of Information Seeking](../isic14fig1.jpg)

<figcaption>

Figure 1: The Comprehensive Model of Information Seeking ([Johnson _et al._, 1995](#Joh95), p. 226)</figcaption>

</figure>

The other two antecedents, salience and beliefs, form the second group of antecedents known as '_personal relevance_'. An information need arises when a gap is perceived between what one should know and what he actually knows. In other words, people become aware of their ignorance. Information needs do not always result in information seeking, however. As Case _et al._ ([2005](#Cas05)) pointed out, '_people are only motivated to seek information when they both know that they are ignorant and the missing information becomes salient_' (p. 358). Salience is one of the key concepts in CMIS, providing a key motivation to seek information. It captures the concept of how the missing information is perceived to be significant to an individual, and is closely associated with how individuals perceive that they are at risk and under threat. Beliefs on the other hand, can constrain or empower individuals to seek information. For instance, if individuals do not believe that knowing more is important in helping them change their situation or cope with a crisis, they are less likely to seek information.

In addition to the original set of antecedents, coping style was considered in order to investigate the effects of individual differences in coping with the crisis. As van Zuuren and Wolfs ([1991](#van91)) argued, 'dealing with information… can be problematic particularly in threatening situations' (p. 141). The ways by which individuals cope with crises is then of particular interest to studying their information behaviour in such contexts.

For the purpose of the study Miller's ([1980](#Mil80)) self-reporting instrument, the Miller Behavioural Style Scale was used to measure the coping styles of participants in this study. Miller ([1980](#Mil80)) found two broad coping styles in stressful situations: monitoring and blunting. Monitors are vigilant, and orientate themselves towards the threatening information in order to process them as much as they could. Their tolerance for uncertainty in stressful situations such as the haze crisis is low, they prefer to confront rather than avoid information. On the other hand, blunters tend to avoid information perceived as threatening, and have a low tolerance for emotional arousal. These styles are not mutually exclusive; in reality, individuals have a mix of both, within each style individuals can also differ but there should be a dominant style which characterises most of their behaviours during crises.

Savolainen ([1995](#Sav95)) may have alluded to the notion of crisis coping in his Everyday Life Information Seeking (ELIS) model. In his model, the exploration of how individuals operate when it comes to everyday issues and decisions - comes from repeated behaviour, norms, values, and attitudes that have been reinforced over time and space. Even though Savolainen's model was focused on everyday life which is quite different from the crisis facing participants in the study, the series of concepts in his model provides important in-depth knowledge to how individual coping styles are formed, and how they navigate a crisis through their information behaviour. Savolainen's model would later form the basis for a series of in-depth interviews with selected participants in the study described here, but the results are beyond the scope of this paper.

Perceptions of information carriers and their influence on seeking actions are not within the scope of this analysis. The concept of information carrier factors however, is of relevance. It consists of two factors - the characteristics of information carriers shape information seeking intentions. These factors are explicated later as information seeking in four categories of information sources.

## Method

For the study working students above 21 years old and above form the main selection criteria for our target participants. Because no announcement was made to suspend or minimise work, working students were presumed to be most concerned about the effects of the haze, and would display prominent information seeking behaviour as part of coping with the crisis.

#### Sampling and data collection

With a target number of at least 300 participants, the study employed a proportionate quota sampling approach, using age group (Table 1) based on the proportion indicated in the most recent data of employed persons published by Singapore's Ministry of Manpower (2013) as of June 2012.

<table><caption>Table 1: Stratified sample based on age</caption>

<tbody>

<tr>

<th>Age range</th>

<td>21-29</td>

<td>30-39</td>

<td>40-49</td>

<td>50-59</td>

<td>60 and above</td>

<td>Total</td>

</tr>

<tr>

<th>Proportion (%)</th>

<td>17.6</td>

<td>24.7</td>

<td>25.9</td>

<td>21.5</td>

<td>10.3</td>

<td>100.0</td>

</tr>

<tr>

<th>Number</th>

<td>53</td>

<td>74</td>

<td>78</td>

<td>65</td>

<td>30</td>

<td>300</td>

</tr>

</tbody>

</table>

Participants were recruited by mature-age students enrolled in a part-time postgraduate coursework programme in Nanyang Technological University, Singapore. Three students employed to administer the survey went to all the classes that working students were enrolled in to recruit participants. Although this was not purely random and the population from which the sample was drawn may not be completely representative of the population of working students in Singapore, the eventual sample is still significant as its purposive intention to sample certain age groups (e.g. 60 years old and above) which are considered minorities in the general population of working students can eliminate biases in the results of the study.

The survey was made available to respondents either as a paper or web-based questionnaire, and participants were able to indicate their preferences after agreeing to participate in the study. It was first pilot tested with six volunteers, and after their feedback, the questionnaire was improved in terms of its design and usability of the questions. The survey yielded a total of 339 responses, out of which 298 (87.9%) were valid.

#### Measures

The questionnaire was designed using the Comprehensive Model of Information Seeking (CMIS) ([Johnson _et al_., 1995](#Joh95)). According to the model, demographics, direct experience, salience and beliefs functioned as antecedents for seeking information. In terms of demographics, age, education, gender, employment status (whether working full-time or part-time) and dwelling type (acting as a proxy for income status, as questions about income are usually regarded as sensitive) are regarded as relevant contextual factors for the haze crisis.

The concept of experience is operationalised as 4 statements with regards to their previous experience in seeking information from mainstream, new media, personal and health professional sources, and 1 statement on whether or not they have previous experience in dealing with similar crisis.

Salience in this context, therefore, is not just about the relevance of information to the haze crisis, but a person has greater salience if he also has tangible concerns that can make the presence (or absence) of information directly influence his wellbeing. Participants were asked to indicate their agreement on a scale of 1 to 5 on three statements to measure their salience. The three statements asked for their opinions on whether or not the haze affects their ability to go on with everyday life, the extent to which information they seek determines how well they cope with the haze, and whether or not they generally actively seek information for the concerns they have about their health.

Beliefs in this context were operationalised as beliefs in the ability of their own health to cope with the haze, the importance of information to cope with the haze, and their own abilities in seeking information from mainstream, new media, personal and health professional sources cope with the crisis. Participants were asked to indicate their agreement with each statement on a scale of 1 to 5\. The Miller Behavioral Style Scale ([1980](#Joh80)) was also included to determine dominant coping styles of participants as a possible antecedent of information seeking

The types of information sources were adapted from Griffin and Dunwoody ([2000](#Gri00)), Cotten and Gupta ([2004](#Cot04)) and Dutta-Bergman ([2004](#Dut04)). The four categories of information sources used in this study were: (a) mainstream sources, (b) family, friends and colleagues, (c) new media online sources, and (d) healthcare professionals. Mainstream sources include traditional media sources such as newspapers, magazines, television, radio, and online sources such as government websites, online editions of newspapers and radio channels, and pharmaceutical companies. Family, friends and colleagues with whom participants have a relationship with provide interpersonal and social information sources. New media online sources refer to sources that are mostly online and accessible via any digital device, with information that are put together by non-traditional content producers (e.g. Wikipedia, blogs, Facebook, and Yahoo!Answers). The last category, healthcare professionals, refers to information sources coming from trained specialists in the healthcare profession, such as pharmacists, general practitioners, and medical specialists.

## Findings and discussion

The sample of participants in our study aged between 21 and 70 years old, with 56.4% being females (n=168) and the remaining 40.6% males (n=121), and 45.6% who are married (n=136) and 54.4% (n=162) who are single (including those who are divorced, widowed, or separated). 94.5% of participants are engaged in office-based work, and 53.6% are University graduates. This poses certain limitations to the sample, as the findings may not apply to individuals who are engaged in work that involves being outdoors, and the sample is slightly skewed towards those who are higher educated.

#### More intense information seeking during the haze

Consistent with other related work on information seeking during crises and disasters, participants reported an overall increase in the frequency of seeking health information during the haze compared to everyday life. The intensity of seeking information during the crisis also goes to show that information, or the act of information seeking, have a role to play in crisis response and alleviating anxiety.

This was further analysed across sources (see figure 2). In the current media environment where there is an abundance of information and media sources, the use of mainstream media may have declined as users may prefer to use alternative online sources (this does not include mainstream media sources which are online) instead largely due to their convenience and timeliness. During the haze crisis however, participants in our study reported that they sought out information from mainstream media sources most often, followed by personal information sources such as family, friends and colleagues.

<figure>

![Figure 2\. Seeking health information during the haze and non-haze periods (by frequency and source)](../isic14fig2.jpg)

<figcaption>

Figure 2\. Seeking health information during the haze and non-haze periods (by frequency and source)</figcaption>

</figure>

The percentage of respondents seeking information frequently from mainstream sources increased from 34.9% in everyday life to 72.2% during the haze. This was followed by an increase from 39.8% to 61.7% in seeking information from new media online sources, 39.4% to 64.8% from family, friends and colleagues, and 18.7% to 23.5% from healthcare professionals. One plausible reason why the intensity in seeking information from healthcare professionals appears to be least evident may be due to the fact that such information seeking behaviour is mostly driven by medical needs - one simply does not go to see a doctor unless he is sick.

Still, the increases in information seeking across all four media sources are significant, as revealed by ANOVA tests. The increase in seeking information during the crisis was significant for mainstream sources [F(1, 288) = 283.028, p < .001], new media online sources [F(1, 288) = 185.468, p < .001], personal networks (family, friends and colleagues) [F(1, 288) = 85.153, p < .001], and healthcare professionals [F(1, 288) = 131.956, p < .001].

#### Coping style and antecedents of information seeking

Overall, 45.1% (n=129) respondents considered themselves as dominantly blunters and the remaining 54.9% (n=157) considered themselves as dominantly monitors during a crisis.

Coping styles were influenced by age, as revealed by an independent samples t-test. Monitors were more likely to be older (M = 41.29; SD = 12.28) and blunters more likely to be younger (M = 34.77; SD = 10.23), t = -4.902, p < .001\. Other demographic factors such as gender, dwelling type, education, and employment status, were not significant in shaping coping styles.

The five items to measure experience in seeking information to cope with the crisis generated a Cronbach alpha of 0.87, and an overall experience index was further computed to compare participants in terms of their background experiences in coping with the crisis. The results suggest that experience may have to do with participants' dominant coping styles, with monitors having greater background experience (M = 2.88; SD = 0.99) than blunters (M = 2.51; SD = 0.90), t = -3.293, p < .001\. This also implies that in everyday life, monitors would also actively seek information and contribute to the background experiences of seeking information.

The three items on salience generated a Cronbach alpha of 0.65, and an overall salience index was computed for participants. An independent samples t-test showed that monitors perceived greater salience (M = 4.05; SD = 0.51) of information compared to blunters (M = 3.26; SD = 0.61), t = -11.757, p < .001\. In other words, monitors considered information about the haze to be salient, and if they are also aware that information is missing, they will have greater motivations to seek out relevant information.

The concept of beliefs was measured by three items: the extent to which participants believed they have conditions that could be a problem with the haze, how important information was to cope with the haze, and their own efficacies in seeking information from mainstream, new media, personal and health professional sources (with eight sub-items to explicate each information source). The items generated a Cronbach alpha of 0.75, and used to compute an index to compare beliefs held by participants to seek information. Interestingly, coping style was not significant in shaping beliefs, which suggests that individuals can have different levels of self-efficacies about the situation which are independent of their coping styles. Monitors are as likely as blunters to have high or low degrees of self-efficacies (e.g. beliefs in how much they can do to change the situation) to seek information.

Although implicit, the results suggest that coping style, which is influenced by age, and in turn, mediating the antecedents of information seeking, provides a significant social context shaping individual information behaviour.

#### Information seeking across four sources

Participants were studied in terms of where they sought information (mainstream media, new media, personal networks (family, friends and/or colleagues), and healthcare professionals), and the effects of age, experience, salience, beliefs and coping style in seeking information using ANOVA.

Age was only significant in seeking information from healthcare professionals [F(46, 242) = 1.489, p < .05], with older working students preferring to seek information from professionals than younger participants. For all other sources however, age was not a significant antecedent. Whilst it was not surprising for mainstream media sources and personal networks (as the results imply that younger participants sought information from these sources as much as older participants), the finding was interesting for new media sources. To date, the current literature has reported that those who use new media sources tend to be younger - yet the finding implies that when it comes to a crisis, and perhaps when individuals felt that they could not trust information coming from only one or two sources, they were as compelled to seek information from new media sources.

Experience was significant for all four sources [F<sub>Mainstream</sub>(33,255) = 5.083, p < .001; F<sub>NewMedia</sub>(33,255) = 3.240, p < .001; F<sub>PersonalNetworks</sub>(33,255) = 3.101, p < .001; F<sub>HealthcareProfs</sub>(33,255) = 6.334, p < .05], with greater experience associated with greater likelihood of using each of the four sources to seek information. The finding reinforces the contingent effects of experience on shaping information seeking in the CMIS model.

Salience was significant in shaping information seeking from personal networks [F(12, 274) = 2.148, p < .05] and health professionals [F(12, 274) = 1.793, p < .05], but not from mainstream media or new media. In other words, there was no difference in terms of perceived salience of information when seeking information from mainstream or new media, but those with greater perceived salience of information were more motivated to seek information from personal networks or health professionals.

Like experience, beliefs was significant for all four sources [F<sub>Mainstream</sub>(30,258) = 1.928, p < .05; F<sub>NewMedia</sub>(30,258) = 2.986, p < .001; F<sub>PersonalNetworks</sub>(30,258) = 2.104, p < .001; F<sub>HealthcareProfs</sub>(30,258) = 3.188, p < .001], with greater beliefs motivating information seeking on each of the four sources.

Coping style was significant for seeking information from all sources [F<sub>Mainstream</sub>(1,284) = 13.452, p < .001; F<sub>PersonalNetworks</sub>(1,284) = 11.808, p < .001; F<sub>HealthcareProfs</sub>(1,284) = 9.012, p < .05] except for new media. Monitors sought significantly more information from mainstream, personal and professional sources than blunters. The fact that styles of coping is not significant for seeking information on new media is interesting - it reinforces the universally accessible characteristic of new media.

## Conclusion

The study support the hypothesis that coping style, which is highly relevant in understanding the ways people cope with a crisis is significant in shaping all antecedents in the CMIS model except for beliefs held about information seeking. Additionally, it also has a direct influence on information seeking from mainstream media sources, personal networks, and healthcare professionals. In the context of crisis-based information seeking behaviour, the findings suggest the importance of including the possible effects of coping behaviour.

The findings about new media in the study contravene current findings about information seeking behaviour utilising new media sources. In a crisis, regardless of age, perceived salience or coping style, individuals are equally likely to seek information from new media sources.

There are however some limitations to the study. For the purpose of the study, Miller's Behavioral Style Scale ([1980](#Mil80)) was used to derive dominant coping styles but there are other instruments that have been developed after Miller used to measure crisis coping. The original scale was selected as this was the first study in this context, but future work should triangulate the findings using other scales. In addition there may be differences in the information environments of participants: some participants may have been exposed to richer and more information and others, and the effects of such information environments simply cannot be examined via a survey. Future work should also investigate this using an experimental design. The final limitation lies with the context of the study - a haze crisis brings up health-related information seeking and its associated challenges, and other contexts of crises, such as dealing with natural disasters or terrorism, may yield very different results. Again, this cannot be known until the study can be replicated in other contexts.

## Acknowledgements

I would like to thank my students Lui Eei Yin, Teng Ai Wei, and Poh Lay Chin for their assistance with data collection for the project

## <a id="author"></a>About the author

**Natalie Pang** is an Assistant Professor in the Wee Kim Wee School of Communication and Information, Nanyang Technological University, Singapore. She received her PhD from Monash University in Australia, where her PhD work also won two awards. Her primary research interest is in participatory forms of engagement using new media, and she investigates this theme in the domains of civic and political participation and information behaviour. She practices both basic and applied research. She can be contacted at [nlspang@ntu.edu.sg](mailto:nlspang@ntu.edu.sg)

<section>

## References

<ul>
<li id="All11">Allen, D. K. (2011). Information Behaviour and Decision Making in Time Constrained Practice: A Dual-Processing Perspective. <em>Journal of the American Society for Information Science and Technology, 62</em>(11), 2165-2181.</li> 
<li id="Ana01">Anaman, K. A. (2001). Urban householders' assessment of the causes, responses, and economic impact of the 1998 haze-related air pollution episode in Brunei Darussalam. <em>ASEAN Economic Bulletin</em>, 193-205.</li>
<li id="Bic99">Bickerstaff, K.J. and Walker, G.P. (1999). Clearing the smog: public responses to air quality information. <em>Local Environment, 43</em>, 279-294.</li>
<li id="Cas05">Case, D., Andrews, J.E., Johnson, J.D., & Allard, S.L. (2005). Avoiding versus seeking: the relationship of information seeking to avoidance, blunting, coping, dissonance, and related concepts. <em>Journal of Medical Library Association, 93</em>(3), 353-362.</li>
<li id="Cas12">Case, D. (2012). <em>Looking for Information: A Survey of Research on Information Seeking, Needs, and Behavior</em>. United Kingdom: Emerald Group Publishing Limited.</li>
<li id="Che13">Cheng, C. (2013, Jul 23). Haze crisis: Harmful effects of spreading rumours online. <em>The Straits Times</em>. Retrieved July 30, 2013 from http://www.straitstimes.com/premium/forum-letters/story/haze-crisis-harmful-effects-spreading-rumours-online-20130723</li> 
<li id="Cot04">Cotten, S. R., & Gupta, S. S. (2004). Characteristics of online and offline health information seekers and factors that discriminate between them. <em>Social Science & Medicine, 59</em>(9), 1795-1806.</li>
<li id="Cri13">Crighton, E.J., Brown, C., Baxter, J., Lemyre, L., Masuda, J.R., & Ursitti, F. (2013). Perceptions and experiences of environmental health risks among new mothers: A qualitative study in Ontario, Canada. <em>Health, Risk & Society, 15</em>(4), 295-312.</li>
<li id="Cut10">Cutilli, C. C. (2010). Seeking health information: What sources do your patients use? <em>Orthopaedic Nursing, 29</em>(3), 214-219.</li>
<li id="DeZ10">DeZwart, O., Veldhuijzen, I. K., Richardus, J. H., & Brug, J. (2010). Monitoring of risk perception and correlates of precautionary behavior related to human avian influenza during 2006-2007 in the Netherlands: Results of seven consecutive surveys. <em>BMC Infectious Diseases, 10</em>, 114.</li>
<li id="Dut04">Dutta-Bergman, M. J. (2004). Primary sources of health information: Comparisons in the domain of health attitudes, health cognitions, and health behaviors. <em>Health Communication, 16</em>(3), 273-288.</li>
<li id="For95">Ford, T.E. & Kruglanski, A.W. (1995). Effects of epistemic motivations and the use of accessible constructs in social judgement. <em>Personality and Social Psychology Bulletin, 21</em>, 950-962.</li>
<li id="Fry87">Fry, L.W. & Smith, D.A. (1987). Congruence, contingency and theory building. <em>Academy of Management Review, 12</em>, 117-132.</li>
<li id="Gri00">Griffin, R. J., & Dunwoody, S. (2000). The relation of communication to risk judgment and preventive behavior related to lead in tap water. <em>Health Communication, 12</em>(1), 81-107.</li>
<li id="Hea91">Heaton, A.W. & Kruglanski, A.W. (1991). Person perception by introverts and extraverts under time pressure: effects of need for closure. <em>Personality and Social Psychology Bulletin, 17</em>, 161-165.</li>
<li id="Joh95">Johnson, D. J., Donohue, W. A., Atkin, C. K., & Johnson, S. (1995). A comprehensive model of information seeking: Tests focusing on a technical organization. <em>Science Communication, 16</em>(3), 274-303.</li>
<li id="Joh93">Johnson, J.D. (1993). <em>Tests of a comprehensive model of cancer-related information seeking</em>. Presentation at the Speech Communication Association Annual Convention. Miami, Florida.</li>
<li id="Joh97">Johnson, J.D. (1997). <em>Cancer-related information seeking</em>. Creskill, NJ: Hampton Press, 70-100.</li>
<li id="Joh03">Johnson, J.D. (2003). On contexts of information seeking. <em>Information Processing and Management</em>, 39, 735-760.</li>
<li id="Kru87">Kruglanski, A.W. & Mayseless, O. (1987). Motivational effects in the social comparison of opinions. <em>Journal of Personality and Social Psychology, 53</em>, 834-842.</li>
<li id="Mar12">Marton, C., & Choo, C. W. (2012). A review of theoretical models of health information seeking on the web. <em>Journal of Documentation, 68</em>(3), 330-352.</li>
<li id="Mil80">Miller, S.M. (1980). When is a little information a dangerous thing? Coping with stressful life-events by monitoring vs blunting. In S.Levine and H.Ursin (Eds), <em>Coping and health</em>. New York: Plenum Press.</li> 
<li id="Mil87">Miller, S.M. (1987). Monitoring and blunting: Validation of a questionnaire to assess styles of information seeking under threat. <em>Journal of Personality and Social Psychology, 52</em>, 345-353.</li>
<li id="Mil83">Miller, S.M. & Mangan, C.E. (1983) Interacting effects of information and coping style in adapting to gynaecological stress: should a doctor tell all? <em>Journal of Personality and Social Psychology, 45</em>, 223-236</li>
<li id="Nat">National Environment Agency. (n.d.). <em>PSI Readings</em>. Retrieved August 24, 2013 from http://app2.nea.gov.sg/anti-pollution-radiation-protection/air-pollution/psi/historical-psi-readings</li>
<li id="Ros09">Rosen, N. O., & Kn&auml;uper, B. (2009). A little uncertainty goes along way: State and trait differences in uncertainty interact to increase information seeking but also increase worry. <em>Health Communication, 24</em>(3), 228-238.</li>
<li id="Sav93">Savolainen, R. (1993). The sense-making theory: Reviewing the interests of a user-centred approach to information seeking and use. <em>Information Processing Management, 29</em>, 13-28.</li>
<li id="Sav95">Savolainen, R. (1995). Everyday life information seeking: Approaching information seeking in the context of 'way of life'. <em>Library and Information Science Research, 17</em>, 259-294.</li>
<li id="Sav06">Savolainen, R. (2006). Time as a context of information seeking. <em>Library & Information Science Research, 28</em>(1), 110-127.</li>
<li id="See03">Seeger, M.W., Sellnow, T. & Ulmer, R.R. (2003). <em>Communication and organisational crisis</em>. Westport, CT: Praeger.</li>
<li id="Sem08">Semenza, J. C., Wilson, D. J., Parra, J., Bontempo, B. D., Hart, M., Sailor, D. J., & George, L. A. (2008). Public perception and behavior change in relationship to hot weather and air pollution. <em>Environmental Research, 107</em>(3), 401-411.</li>
<li id="Vak97">Vakkari, P. (1997). Information seeking in context: A challenging metatheory. In P.Vakkari, R.Savolainen, & B.Dervin (Eds), Information seeking in context: <em>Proceedings of a meeting in Finland 14-16 August 1996</em> (pp. 451-463). London: Taylor Graham.</li>
<li id="van11">van der Weerd, W., Timmermans, D. R. M., Beaujean, D. J. M. A.,Oudhoff, J., & van Steenbergen, J. E. (2011). Monitoring the level of government trust, risk perception and intention of the general public to adopt protective measures during the influenza A (H1N1) pandemicin the Netherlands. <em>BMC Public Health, 11</em>, 575.</li>
<li id="van91">van Zuuren , F.J. & Wolfs, H.M. (1991) Styles of information seeking under threat: personal and situational aspects of monitoring and blunting. <em>Personality and Individual Differences, 12</em>, 141-149</li>
<li id="Wal88">Waldron, V.R. & Dervin, B. (1988). Sensemaking as a framework for knowledge acquisition. In L.C.Smith (Ed), Artificial intelligence, expert systems and other applications: <em>American Society for Information Science Mid-Winter Meeting</em> (p. 9). Ann Arbor, Michigan: Greenwood Press.</li>
<li id="Wal12">Walter, D., B&ouml;hmer, M. M., Reiter, S., Krause, G., &Wichmann, O. (2012). Risk perception and information-seeking behaviour during the 2009/10 influenza A-H1N1 pandemic in Germany. <em>Euro Surveill, 17</em>(13). Retrieved from http://www.eurosurveillance.org/ViewArticle.aspx?ArticleId=20131</li>
<li id="Wat13">Watson, B. R., Riffe, D., Smithson-Stanley, L., & Ogilvie, E. (2013). Mass media and perceived and objective environmental risk: Race and place of residence. <em>Howard Journal of Communications, 24</em>(2), 134-153.</li>
<li id="Wea10">Weaver III., J. B., Mays, D., Weaver S. S., Hopkins, G.L., Eroglu, D., & Bernhardt, J.M. (2010). Health information-seeking behaviors, health indicators, and health risks. <em>American Journal of Public Health. 100</em>(8), 1520-1525.</li>
<li id="Won10">Wong, L. P., & Sam, I. C. (2010). Public sources of information and information needs for pandemic influenza(H1N1). <em>Journal of Community Health, 35</em>(6), 676-682.</li>
<li id="Yon13">Yong, C. (2013, 21 June). Haze update: PSI 401 at noon; many pharmacies still out of masks. <em>The Straits Times</em>. Retrieved from http://www.straitstimes.com/breaking-news/singapore/story/singapore-haze-update-psi-401-noon-many-pharmacies-still-out-masks-201 </li>
</ul>

</section>

</article>