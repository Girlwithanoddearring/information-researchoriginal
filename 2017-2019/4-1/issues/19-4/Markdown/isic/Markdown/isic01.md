<header>

#### vol. 21 no. 4, December, 2016

Proceedings of ISIC: the information behaviour conference, Zadar, Croatia, 20-23 September, 2016: Part 1.

* * *

</header>

<article>

# A framework for understanding information sharing: an exploration of the information sharing experiences of female academics in Saudi Arabia

#### Fatmah Almehmadi, Mark Hepworth and Sally Maynard

#### Abstract

> **Introduction.** Information sharing has recently become an area of interest within the information behaviour literature. Yet, limited published empirical research can be found. This paper reports results concerning the information sharing behaviour of a group of female academics in Saudi Arabia.  
> **Method.** The data for this paper were collected via semi-structured interviews, informed by the critical incident technique, and diaries between May-September, 2013 to gather data from 24 female academics. Eight of whom kept a diary for a two-week period.  
> **Analysis.** Thematic data analysis was undertaken to analyse different aspects of information sharing, motivation and strategies for sharing information.  
> **Results.** Information sharing was an embedded aspect of the participants' information behaviour and exhibited in both the work and everyday life context. A tendency for retaining and withholding information was also identified. The analysis allowed different types of information sharing to be distinguished and helped to explain why and how the participants shared information in relation to a range of situations that related to their daily life.  
> **Conclusions.** The study indicated the complexity of information sharing and identified why people shared or withheld information; the information sharing process; the situations where information was shared and the nature of their information sharing and adds to our understanding of information sharing. Nevertheless, further distinctions are likely to be identified in the course of further analysis and placed in the broader context of information behaviour.

<section>

## Background

The findings presented in this paper stemmed from a wider study of the information behaviour of Saudi female academics within the context of their daily life. This paper focuses on their information sharing. Previous research into people's information behaviour has largely investigated different populations and how information seeking varies in relation to individual attributes such as age, gender, and cognitive style. Such studies have been reviewed by Case (2012) and Fidel (2012) and tend to focus on the link between patterns of information seeking and disciplinary differences (e.g., Ellis, Cox and Hall, 1993; Ellis and Haugan, 1997; Gardiner, McMenemy and Chowdury, 2006; Ge, 2010).

Various authors have offered definitions of information behaviour, for example, Wilson (2000) defined it as:

> 'the totality of human behaviour in relation to sources and channels of information, including both active and passive information seeking, and use. Thus, it includes face-to-face communication with others, as well as the passive reception of information as in, for example, watching TV advertisements, without any attention to act on the information given (p.49).

In a similar vein, Pettigrew, Fidel and Bruce (2001, p. 44) described information behaviour as '_the study of how people need, seek, give and use information in different contexts, including the workplace and everyday living_'.

The majority of developed models, reviewed by Case (2012), such as Ellis (1989), Byström and Järvelin (1995), Kuhlthau (2004) and Urquhart and Rowley (2007) tend to focus on information seeking. In addition, although information sharing, defined as 'an umbrella concept that covers a wide range of collaboration behaviors from sharing accidentally encountered information to collaborative query formulation and retrieval' (Talja, 2002), has been recognised and highlighted in a number of information behaviour models (e.g., Wilson, 1981; Krikelas, 1983; Godbold, 2006), its component elements such as types, motivations, and strategies are not widely discussed. These aspects are explored, analysed, and discussed in this paper.

Due to the limitation on the length of this paper, an overview of previous research into information sharing is presented here. For an in-depth review see Pilerot (2012). As noted previously, since the 1990s, information behaviour in the workplace has constituted a large proportion of the research (Case, 2012). While many studies focused on active information seeking, information sharing, especially the preference for people as an information source (Wang et al., 2007) and collaborative information seeking (Foster, 2006) have been explored less. Talja (2002) and Ibrahim and Allen (2012) are notable exceptions because they specifically investigated aspects of information sharing such as types of information sharing associated with collaborative information seeking among academics and the relationship between information sharing and the concept of trust.

The term 'everyday life information seeking' refers to non-work situations (Savolainen, 1995). Similar to workplace studies, the focus of everyday life studies has largely been concerned with information seeking. However, information sharing has been briefly discussed in relation to people's preference for turning to others to get information (McKenzie, 2003; Savolainen, 2008) and the information behaviour of marginalized groups (Chatman, 1999; Agosto and Hughes-Hassell, 2006). Two notable examples which specifically looked at information sharing include a study exploring the motivation for sharing information based on interviews with 20 Finnish environmental activists (Savolainen, 2007) and one examining the dynamics of HIV/AIDS information networks and information sharing (Veinot, 2009). However, in each case the need for further research was recognized.

While the body of research described above offers insights into how and why people seek information, it lacks explanation for how acquired information is used (Kari, 2010). One way in which acquired information can be used is to share it, and although information sharing constitutes an important part of information behaviour, there has been limited research that specifically investigates this form of information behaviour (Wilson, 2010 and Fidel, 2012). Pilerot's (2012) comprehensive review of research into information sharing, and recent paper (Pilerot, 2013) support this conclusion. In this paper, we recognize this situation and adds to our understanding of information sharing, through an analysis of the preliminary results of research into the information behaviour of female academics in Saudi Arabia.

## Method

Qualitative, interpretive methods, were adopted for the purpose of data collection. Twenty-four female academics participated who worked in two Saudi universities, (10 from natural and applied sciences and 14 from social sciences; one professor, four readers, seven senior lecturers, and twelve lecturers). The academics were recruited via a snowball sampling method and participated in semi-structured interviews lasting 40-70 minutes. In addition, eight participants agreed to keep a diary for 14 days about their daily life activities and where they interacted with information. A total of 112 diary entries were collected. The critical incident technique (CIT) (Flanagan, 1954; Dervin, 1992; Hughes, 2012) was used to structure the interviews. Participants were asked to recall incidents where they were interested in finding information and what happened to the information once found or acquired. Examples of questions used included: Describe the situation? What were you looking for? And what did you do with information once found? The CIT was also used to inform the design of the diary. For example, participants were asked to report on incidents where they were trying to find out about something, asking for help, giving advice to others, and telling someone something. Ideally, this would have been accompanied by observation and observational data that could then have been compared to participants' self reports gathered via their diaries and interviews, however time did not permit this.

A thematic data analysis approach as explained by Braun and Clarke (2006) was used for the purpose of data analysis which consisted of a number of stages. The first stage, called data familiarisation, involved reading and re-reading the transcripts to familiarise the researcher with the data (Bryman, 2008). The second stage of analysis was coding and re-coding the data. Coding was done by "tagging and naming selection of text within each data item (Braun and Clarke, 2006, p. 89). Examples of narrower categories derived from this stage of data analysis included 'constant competition', 'giving advice or comment', 'eliciting others' interest', and 'raising awareness'. Searching for themes (i.e. broader categories) was the third stage of data analysis and included categories such as 'withholding information', 'providing information', 'retaining information', 'oral strategies' and 'motivations for sharing information'. The constant comparison method, line-by-line coding, and memo-writing were the key techniques used during data analysis.

## Results

The thematic data analysis identified three main categories:

*   types of information sharing
*   motivation and
*   strategies for information sharing

Quotations are used to illustrate categories and sub-categories and reflect patterns in the data.

### Types of information sharing

Data analysis led to the identification of four main types of information sharing, discussed below:

#### Providing information

Providing information identifies information sharing as a one-way process and was further divided into responding to a request and proactive information providing.

#### Responding to a request

This was further divided into answering a question and giving advice or comment. For example, in the following excerpt the participant was asked by her sister about a device she owned, and therefore she gave her information and answered her questions:

> one of my sisters has asked me about the Walkman I have got recently and asked me some questions about it. I gave her some information about what it could be used for. (L.NS.5).

Similarly, this participant provided information in a response to a request made by one of her colleagues who asked her about accessories she was wearing and where she could find similar ones:

> She asked me where I got them from and if they have a website. I told her that unfortunately they do not have any website but gave her the address and their phone number so that she can check with them and ask them more questions (L.NS.24).

In addition to answering questions, responding to a request also took the form of giving advice or comments. For example, a situation where the participant was asked by a group of new lecturers to give advice or comment on scholarly publishing, particularly how to better deal with editors' and reviewers' suggestions, and responded by providing the relevant information:

> I also told them that they do not necessarily have to make all the changes if they thought that these are not important but they have to give explanations (R.SS.22).

> Another participant described a situation where she provided information in response to a request made by one of her colleagues who asked her for advice on who she should contact to informally offer an admin role in her college:

> I told her it would be better if she talks to people in another college because someone might be interested in taking such a role (R.SS.20).

#### Proactive information providing, without being asked

This was further divided into one-to-one and one-to-many. One-to-one proactive providing of information represented situations where information was shared between two persons and where participants gave information to another person who had not necessarily asked for such information. For example, information was shared with a friend about a type of Estrogen treatment available in the market:

> While thinking about that I remembered a friend of mine who talked about this l don't know when but kind of felt that since I know something that is important to her and spent time looking and examining a number of articles why not send her this information (SL.SS.9).

Another example indicated that, when this participant met one of her relatives, she proactively informed her about a health-related matter and gave her information about it:

> when I met her I told her about an Ophthalmologist I've recently visited and encouraged her to do so (SL.SS.8).

In addition, proactive providing of information could take the form one-to-many. This included situations where information was proactively given to a number of people such as the participant's colleagues to whom they had sent information about conferences and research fund opportunities:

> throughout the day I was monitoring my email and forwarding some emails about upcoming conferences and another one for applying for research fund to my colleagues (L.NS.12).

Similarly, a participant talked about sharing health-related information about a good doctor which she thought useful to others. This form of information sharing took place while the participant was talking to others in what she called a 'Majlis', a synonym of 'guests' room', which means, in the Saudi culture, a special room in the house where guests sit together:

> having information I sometimes when I am sitting with people, when am in a Majlis and they are talking about a patient or someone who is suffering from something I just say well I know someone, there is a good doctor (R.SS.20).

### Information exchange

This term was used to describe information sharing that was interactive or a two-way process and was further divided into _information exchange in physical settings_ and _information exchange in online settings_.

#### Information exchange in physical settings

This represents situations where information exchange took place in physical settings which were further divided into _regular_ (i.e., common and frequent) and _incidental_ social interaction. Regular information exchange was a form of information sharing that occurred during frequent and common social interaction, for example, when family members gather during dinner. In the following example, this participant described a situation where the exchange of information took place while family members were having dinner.

> I was talking about this during dinner and was able to tell everyone which type of personality they might have according to their sleeping style (L.NS.6).

In the Saudi Arabian culture, lunch and dinner tend to be family occasions attended by the extended family that usually live near each other. Another example was where a participant described trip planning where information was exchanged between family members during dinner:

> We had a discussion about this while having dinner and as I said each one has his or her ideas so each one was telling others why he or she thinks his or her choice is the best one (L.SS.3).

In addition, information exchange in physical settings was also identified taking place during _incidental social interactions_. These included situations where the participants were gathering with people they know and during such gathering they met others and talked and exchanged information. For example, this participant was visiting a friend's house and met other academics and had the opportunity to interact and exchange information with them:

> Today, I went to visit a friend who lives in another city. During the course of that brief visit, I got an opportunity to meet and talk with a group of academic researchers that she had invited to her house. I thought I was the only one invited. We talked about our backgrounds, research experiences, and interests and so on (L.NS.6).

In another example, a participant described a situation where the exchange of information occurs during a visit where she had been introduced to her relative's friends and exchanged information about their travel experiences.

> and she introduced me to her friends and we had a nice discussion about our travel experiences (R.SS.20).

Information exchange in online settings was further divided into _online forums_ and _social media_ settings. Participants used online forums as a platform to exchange information and interact with other people. For example, a participant posted a list of references about research methods on the University online forum and the exchange of information took place between the participant and a group of postgraduate students:

> and later on in the day, when I opened the forum, I found that students were having a discussion about it and some had also added other references (SL.SS.8).

Similarly, a participant described a situation where the exchange of information took place via a women's online forum in which the discussion was about reflecting on one's experience of a diet programme:

> I found that many others raised other points and were having a debate about it (L.NS.12).

Online information exchange also took place via social media. For example, a Facebook platform was used to exchange information about learning English on the move.

> I made a Facebook post on my page because I thought others might be interested in such information since many have talked about learning English on the move and many have commented on the post made. (L.SS.7).

Another social media application called 'WhatsApp', which facilitates instant messaging, was used by this participant to exchange information with regard to her view on online assessment methods used by her University to ensure teaching quality as indicated below.

> There were comments advocating as well as challenging my suggestion (SL.SS.9).

### Withholding information

Further analysis identified withholding information as a type of information sharing i.e., deciding to keep it to oneself. This was mentioned in relation to scholarly information, particularly as a result of feeling a sense of competition between academics which led to the decision not to share information with others, described below.

#### Constant competition

In the following example, information was withheld because of a perceived level of competition between female academics. She also indicated that this explains her colleagues' decision to withhold information:

> some of my colleagues are not willing to share information with me when I asked them some questions about some papers they had published recently l don't know why maybe because there is a high level of competition since many worked on very similar areas. And I believe for the same reason I do not usually share information about my research with my colleagues (R.SS.20).

Below a situation is described where this participant was talking about constant competition in relation to publishing and on that basis she decided not to share scholarly information with others and kept it for herself:

> In our department, there is a lot of people who share very similar research interests and compete especially in relation to scholarly publishing you know or probably not if you publish alone you get more points than if you choose to publish with others so that's why I do not share information of this kind you know related to research with others but I do share other kinds of information for example information related to teaching methods or my way of evaluating students' assignments and exam papers and so on quite very often (SL.SS.16).

#### Retaining information

The last type of information sharing was retaining information, that is, not exposing others to information. This type of information sharing was noted in relation to everyday life information behaviour, in particular, health-related information and is described below.

#### Avoiding upsetting others

The decision to retain information was taken because sharing information could make others feel upset. For example, this participant talked about her decision not to share information because she thought that sharing it with her father would make him feel upset since he had already heard enough about his health status:

> I very much like to know about health-related matters but I do understand that such kind of information could made some people feel angry, such as, my father because he often says he had enough information given to him by his doctor so did not like to hear more, so I do not talk with him about information related to his health status (L.NS.24).

In another example a participant thought of sharing information but decided not to do so because she anticipated that revealing it would make her sister upset:

> Sometimes you have to think before just sending information to people because some had just heard enough, like, my sister who certainly does not welcome information about obesity risks and so I do not send her information of this kind because I know she will be upset so there is no need to tell her anything about that topic (R.NS.14).

#### Respecting others' feelings

Data analysis shows that when participants knew about and respected others' feelings they were likely to retain information. This was noted in relation to health-related information, in particular, information about cancer. For example, this participant indicated that because she respected the feeling of one of her friends who does not like to know about cancer, she does not share cancer-related information with her:

> Some people, like one of my friends, do not welcome some information and therefore I do not talk with her about or send her information about cancer, for example, especially now because it would remind her of someone she lost (R.SS.22).

Below is a situation where this participant expressed her interest in knowing about recent developments in cancer research, but indicated that she does not reveal such information or talk about it to her friend. Respecting her friend's feeling was the reason behind her decision because talking about this topic would make her friend recall a sad story and she did not want her to go through it again:

> I am interested in knowing about recent developments in cancer research but I do not talk about this with a friend of mine because this would just make her recalling a sad memory (L.NS.18).

#### Information would not be of interest to others

A perceived belief that others would not be interested in information because it was not relevant to them led to the decision to retain information. For example, this participant decided to retain information because it was not relevant to those working in different research areas:

> I do not talk with my colleagues about my research and share information of this kind with others but l think because I am working on an area that is very different from others and thought that no one would be interested in getting such information (SL.SS.9).

Another example describes a situation where the participant thought of sharing the full-text of a research paper she got from its author but decided not to:

> Well, I could send it to others but did not do so because obviously no one would be interested in such information (L.SS.17).

#### Motivations for sharing information

Case (2012, p. 387) defined motivation as: '_a mental and/or emotional state that causes a person to act_'. For the purpose of this paper, motivation relates to the reasons behind sharing information. A range of different motivations were identified. These were grouped into explicit and implicit motivations as follows.

### Explicit motivations for sharing information

#### Anticipating relevance to others

Data analysis indicated that the decision to share information was because it was thought to be very relevant to others, as shown in the example below with regard to job opportunities:

> and some of which I thought were very relevant to my sister's husband, who is currently out of job, so I called her immediately. (L.NS.6).

#### Eliciting others' interest

Another explicit motivation for sharing information was probably of interest to others. For example, the quote below shows a participant sharing information about a special type of glasses for use on a computer:

> so I can tell my friends about it. I think they are going to be very interested since you know we used computers a lot and every day (L.SS.2).

Similarly, a participant was also engaged in reading a newspaper and found a book review about tourism in Saudi that she thought would be of interest to her friend; she posted information about it on her Facebook page in the form of a picture:

> l was reading the newspaper and one of the things l found interesting was a book talking about tourism in Saudi and I thought that this might be interesting to others especially my friends on Facebook who like travelling so I took a picture of this and then posted online on my Facebook page (L.SS.4).

#### Informing others

For example, this participant shared information about research methods textbooks because she wanted to inform other researchers about books which she found useful:

> so that they could also benefit from since I found them very useful (SL.SS.8).

### Implicit motivations for sharing information

#### Saving time and effort

The following example features a participant sharing information about Estrogen treatment available in the market with a friend.

> kind of felt that since I know something that is important to her and spent time looking for information and examining a number of articles why not send her this information (SL.SS.9).

#### Raising awareness

Raising awareness about the risk of speed driving among members of this participant's Facebook page was the motivation for sharing information:

> I took a picture of that and put it on my Facebook page so others could see it and comment on as well (L.SS.4).

Another example was where the participant shared information because she wanted to raise awareness about how women could protect their children while they use the Internet:

> That was an important piece of information I think most women who have children should know about and so I posted some information about this topic via whatsApp so family members and friends can see and know about (R.SS.20).

#### Sharing common interest

A perception of sharing common interest was also a motivation for sharing information, for example, this participant shared information about learning English on the move.

> because thought others might be interested in such information since many have talked about learning English on the move (L.SS.7).

Another example, shown below, indicated that this participant shared information on the basis that she and her friends shared common interest especially in relation to travel matters:

> Usually and especially if I was online and found something that seems to me interesting I send such information to my friends' email. We all are interested in information about travel (SL.SS.16).

#### Sharing one's story or experience

For example, this participant shared information with her colleagues about her experience with using a portable projector for teaching purposes:

> I was talking about that with some of my colleagues who said that this information has just come on time because they needed to have one and were not sure which is the best in the market (L.NS.6).

### Information sharing strategies

A range of strategies for sharing information were identified. These included: oral-based strategies which included: face-to-face talking; telephoning, and Skype conversations, and written-based strategies which included forwarding strategies including forwarding emails, SMS, and social media instant messages; and writing strategies including writing emails, SMS, online forum posts, and social media instant messages. Due to the word count limitation, information sharing strategies are not discussed in this paper.

## Discussion

Data analysis led to the development of a framework that indicates the various dimensions of information sharing behaviour in both the (academic) workplace and everyday life contexts (see Fig.1).

<figure>

![A framework for understanding dimensions of the information sharing behaviour](../isic01fig1.jpg)

<figcaption>Figure 1: A framework for understanding dimensions of the information sharing behaviour</figcaption>

</figure>

Figure 1 shows the framework developed from the analysis. The framework indicates that information sharing can be analysed from four perspectives: information sharing situations; types of information sharing or non-sharing; the motivation for sharing information; and the strategies for information sharing and answer the following questions:

*   What information context led to information sharing in relation to work and everyday life situations?
*   Which type of information sharing was exhibited?
*   Why was information shared?
*   How was information shared?

Data analysis indicated that information sharing was embedded within daily social interactions taking place at home and the workplace; traditional as well as virtual environments. According to figure 1, the process of information sharing tends to begin when a person engages with a range of situations (shown in the top left), whether work or everyday life related, where information has been acquired, which then leads to either sharing or non-sharing of the acquired information. For example, a participant (R.SS.20) stated that she '_was reading an article about twin pregnancy_' and while engaging in reading, she anticipated the relevance of the information she found for someone and consequently decided to share this information via e-mail. This analysis corresponds with Rioux (2005) who made a link between acquiring and sharing information and indicated that people '_make associations between the information they have acquired and possible recipients of this information_' which then '_prompts information sharing behaviour_' (Rioux, 2005, p. 171). If information sharing occurs it can take the form of a one-way process where information is given to others either in response to a request or proactively, as well as in the form of an interactive process where information is being exchanged whether taking place in physical or virtual settings. If information sharing occurred it was shown to be driven by a range of explicit and implicit motivations shown at the bottom of the diagram. Information is then shared via different oral-based strategies which included face-to-face conversations, telephoning, and Skype conversations or via written strategies which included forwarding emails, SMS messages, and instant messages via social media applications and writing emails, SMS messages, online forum posts, and social media instant messages.

However, acquiring information did not necessarily lead to sharing it with others. For example, a participant (L.SS.17) contacted a scholar to obtain the full-text of a research paper she needed. When asked by the researcher about what she did with it once she got it, she said that apart from reading and taking notes, she "could send it to others but did not do so because obviously no one would be interested in such information". This statement indicates that information sharing was considered but did not occur. This is represented in the above diagram as 'retaining information'. Further distinctions were made between withholding information and retaining information. In both retaining and withholding information, sharing was considered and thought of, yet the ultimate decision was not to share information. Initially these two aspects of information sharing were coded as one, however, it became clear that the latter was associated with scholarly information and where participants mentioned competition with their peers as a reason for not sharing, whereas retaining information related situations where it was thought that information would affect people in a negative way, such as health-related information. The resulting framework is considered distinctive in that it provides a more detailed analysis of information sharing and helps to make finer distinctions between types of information, their motivation and how they take place in both the workplace and everyday life. This work builds on previous research, particularly in relation to types of information sharing and motivation for sharing information.

In terms of types of information sharing, Savolainen identified information sharing, which he called '_giving information_', as '_a two-way activity in which information is given and received in the same context_' (Savolainen, 2007, p. 1) which is similar to information exchange in Figure 1\. However, information sharing was also identified as a one way process, called '_providing information_', where information was given to others either in response to a request or proactively. Although a brief reference to these two distinctions: responding to a request and giving information proactively was first made by Sonnenwald (2006), here these are further elaborated and broken down. For example, responding to a request was broken down into answering a question, and giving advice, or comment, and proactive providing of information was broken down into one-to-one and one-to-many. Talja (2002) also discussed the occurrence of non-sharing in her exploration of collaborative information seeking in academic communities as a form that '_takes place in the academic research community when the community as a whole cannot provide information about relevant documents to one of its member_' (Talja, 2002, p. 153). In this paper, however, the occurrence of non-sharing was shown to be either the respondent holding back relevant information or consciously not exposing acquired information to others. These two distinctions were, respectively, called withholding information and retaining information, where in both cases the participants considered information sharing but decided not to share. Furthermore, although previously explored in the work context (Talja 2002), in this paper, it has been explored in relation to both work and everyday life context.

In terms of types of information sharing, Savolainen identified information sharing, which he called 'giving information', as '_a two-way activity in which information is given and received in the same context_' (Savolainen, 2007, p. 1) which is similar to information exchange in figure 1\. However, information sharing was also identified as a one way process, called '_providing information_', where information was given to others either in response to a request or proactively. Although a brief reference to these two distinctions: responding to a request and giving information proactively was first made by Sonnenwald (2006), here these are further elaborated and broken down. For example, responding to a request was broken down into answering a question, and giving advice, or comment, and proactive providing of information was broken down into one-to-one and one-to-many. Talja (2002) also discussed the occurrence of non-sharing in her exploration of collaborative information seeking in academic communities as a form that '_takes place in the academic research community when the community as a whole cannot provide information about relevant documents to one of its member_' (Talja, 2002, p. 153). In this paper, however, the occurrence of non-sharing was shown to be either the respondent holding back relevant information or consciously not exposing acquired information to others. These two distinctions were, respectively, called withholding information and retaining information, where in both cases the participants considered information sharing but decided not to share. Furthermore, although previously explored in the work context (Talja 2002), in this paper, it has been explored in relation to both work and everyday life context.

Few researchers have specifically explored motivation behind sharing information. Savolainen (2007) is an exception, having explored the motivation for sharing information of a group of Finnish environmental activists. He identified three main motives for what he called '_information giving in non-work contexts_'. These were '_serendipitous altruism to provide help to other people_', '_pursuit of the ends of seeking information by proxy_' which means looking for information for others and then share it with them and '_duty-driven needs characteristic of persons elected to positions of trust_' (Savolainen, 2007, p.1). The latter category, was described as being driven by one's duty. This was similar to this study where information giving and exchange was shown to take place between academics and their students. However, in addition, other motivations for sharing information were also identified including eliciting others' interest; informing others; raising awareness; and sharing one's story or experience with others.

Pilerot (2012, p. 575) highlighted that there has been limited discussion '_about the connection between social media and information sharing_' within the information behaviour literature. In this study, it was notable that social media played a significant role in both oral and written based information sharing strategies. With recent developments in information technology, there is more opportunity to share information almost instantly. Such platforms represent a space where people can engage in discussions that facilitate information sharing. At the information sources level, data analysis indicated the contribution of both social media and trusted people in facilitating information sharing. At the content level, the range of topics related to both work (computer products and software, students' assignments and projects, forthcoming scholarly conferences, research fund opportunities, research-based backgrounds, experiences and interests) and everyday life contexts (learning English, food and diet recipes, shopping items, health-related issues such as pregnancy and obesity risks, and job opportunities) were identified. Although this may be representative of types of information sharing in general it is unlikely that this list is exhaustive. Interestingly, health-related information featured in categories in which information was shared as well as in which it was retained.

It is therefore argued that the framework contributes to our understanding of information sharing behaviour, firstly by adding to our understanding of its various aspects such as types and situations of information sharing and motivation and strategies for sharing information; and secondly by extending the scope of previously developed information behaviour frameworks to consider information sharing as one of its significant forms. The model helps to integrate various elements relating to types of information sharing, motivation and strategies for sharing information and situations, where acquired information was shared. To some extent these situations could provide the basis from which information sharing could be explored further and draws attention to the embedded nature of information sharing within what was experienced by the respondents on a daily basis. On the other hand, the model draws attention to situations where information sharing was considered but did not occur. For example, when respondents described a situation where there is constant competition between academics or when anticipating that information would not be of interest to others or when it could make others feel upset, sharing information was unlikely.

This information sharing framework broadens the conception of information behaviour (Wilson, 2010). Previous researchers have tended to study information behaviour from the perspective of information retrieval and were primarily interested in exploring information seeking. As a result, information sharing behaviour tends to be missing from previous models (Ellis's, 1989; Kuhlthau's, 2004; Urquhart and Rowley's, 2007), or briefly included (Wilson's, 1981; Krikelas's, 1983; Godbold's, 2006) with little elaboration or distinctions being made.

In addition the framework provides a relatively holistic structure which can be used to focus further research and encourage thought about the appropriate methodologies and data gathering techniques that could be used, for example, to explore the motivation behind sharing information.

## Conclusion and areas for further analysis

Whilst information seeking has been the subject of much scrutiny, a recent review by Pilerot (2012) concluded that less attention has been paid to information sharing behaviour and called for further research into this area. This paper presents the preliminary results of a larger project that explores the information behaviour of female academics in Saudi Arabia. The specific focus of this paper was to explore the information sharing experiences of female academics in the context of their daily life, both work-related and beyond. Despite being at an early stage of analysis, it was possible to identify information sharing scenarios or situations in relation to both work-related and everyday life-related information. These included types of information sharing such as: providing or giving information and exchanging information as well as instances where information sharing was considered but not undertaken. Information sharing was shown to be driven by a range of explicit and implicit motivations and conducted via written as well as spoken strategies, virtual as well as physical.

The analysis presented in this paper adds to our understanding of the concept of information sharing, particularly, regarding the nature of the information that was shared; the types of information sharing that existed; and why and how acquired or found information was shared with others. However, such analysis is not claimed to be exhaustive but rather illustrative and further analysis is likely to identify other nuances. Furthermore, it should be noted that the snowball sampling strategy may have introduced some bias and, as noted earlier, data collection via observation would have helped to validate self-reporting data from the respondents. Nevertheless, it is argued that our results and analysis contribute to developing our understanding in this area and provides further granularity to our explanation of information sharing behaviour in both work-related and everyday life-related contexts.

## Acknowledgements

The results reported in this paper are part of the first author's ongoing PhD research study which investigates the information behaviour of female academics in Saudi Arabia. She would like to thank the participants who agreed to take part and shared their experiences with her, her supervisors for their comments and suggestions, and also the Ministry of Higher Education in Saudi Arabia for funding her PhD study.

</section>

<section>

## References

*   Allen, D. K. & Wilson, T. D. (2003). Information overload: context and causes. _New Review of Information Behaviour Research, 4_(1), 31-44
*   Agosto, D. & Hughes-Hassell, S. (2006). Toward a Model of the Everyday Life Information Needs of Urban Teenagers, Part 2: Empirical Model. _Journal of the American Society for Information Science, 62_(11), 1418-1426.
*   Braun, V. & Clarke, V. (2006). Using thematic analysis in psychology. _Qualitative Research in Psychology, 3_ (2), 77-101.
*   Bryman, A. (2008). _Social Research Methods._ 3rd ed., Oxford: Oxford University Press.
*   Byström, K., & Järvelin, K. (1995). Task complexity affects information seeking and use. _Information Processing and Management, 31_(2), 191-213\.
*   Case, D. (2012). _Looking for information: A Survey of Research on Information Seeking, Needs and Behavior._ 3rd ed., Amsterdam: Academic Press.
*   Chatman, E., A. (1999). A theory of life in the round. _Journal of the American Society for Information Science and Technology, 50_(3), 207-217\.
*   Dervin, B. (1992). From the mind's eye of the user: The Sense-Making qualitative-quantitative methodology. In: J. D. Glazier & R. R. Powell, eds., _Qualitative research in information management_, Englewood, CO: Libraries Unlimited, 61-84\.
*   Ellis, D. (1989). A behavioural model for information retrieval system design. _Journal of Information Science, 15_(4-5), 237-247\.
*   Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences, _Journal of Documentation, 49_ (4), 356-69.
*   Ellis, D. & Haugan, M. (1997). Modeling the information seeking patterns of engineers and research scientists in an industrial environment, _Journal of Documentation, 53_ (4), 384- 403.
*   Fidel, R. (2012). _Human information interaction: An ecological approach to information behavior_. Cambridge, MA: MIT Press.
*   Flanagan, J.C., (1954). The critical incident technique. _Physiological bulletin, 51_(4), 1-22.
*   Foster, J. (2006). Collaborative information seeking and retrieval. _Annual Review of Information Science and Technology, 40_(1), 329-56\.
*   Gardiner, D., McMenemy, D., & Chowdhury, G. (2006). A snapshot of information use patterns of academics in British universities. _Online Information Review, 30_(4), 341-359\.
*   Ge, X. (2010). Information-seeking behavior in the digital age: a multidisciplinary study of academic researches. _College and Research Libraries_, September, 435-455\.
*   Godbold, N. (2006). Beyond information seeking?: towards a general model of information behaviour. _Information Research, 11_(4) paper 269\. Retrieved from http://informationr.net/ir/11-4/paper269.html
*   Hughes, H. (2012). An expanded critical incident approach for exploring information use and learning. _Library and Information Research, 36_(112), 72-95
*   Ibrahim, N.H. & Allen, D. (2012). Information sharing and trust during major incidents: findings from the oil industry. _Journal of the American Society for Information Science and Technology, 63_(10), 1916-1928.
*   Kari, J. (2010). Diversity in the conceptions of information use. _Information Research, 15_(3). Retrieved from http://www.informationr.net/ir/15-3/colis7/colis709.html
*   Krikelas, J. (1983). Information Seeking Behavior: Patterns and Concepts. _Drexel Library Quarterly, 19_, 5-20.
*   Kuhlthau, C. (2004). _Seeking meaning: a process approach to library and information services_. 2nd ed., Westport, CT: Libraries Unlimited.
*   McKenzie, P.J. (2003). A model of information practices in accounts of everyday-life information seeking. _Journal of Documentation, 59_(1), 19-40\.
*   Pettigrew, K. E., Fidel, R., & Bruce, H. (2001). Conceptual frameworks in information behavior. _Annual Review of Information Science and Technology, 35_, 43-78.
*   Pilerot, O. (2012). LIS Research on information sharing activities - people, places, and information. _Journal of Documentation, 68_(4), 559-581.
*   Pilerot, O. (2013). A practice theoretical exploration of information sharing and trust in a dispersed community of design scholars. _Information Research, 18_(4) paper 595\. Retrieved from http://InformationR.net/ir/18-4/paper595.html
*   Rioux, K. (2005). Information acquiring-and-sharing. In: K. Fisher, S. Erdelez & L. McKechine, Eds., _Theories of information behavior_, Medford, New Jersey: Information Today, Inc.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of way of life. _Library & Information Science Research, 17_(3), 259- 294
*   Savolainen, R. (2007). Motives for giving information in non-work contexts and the expectations of reciprocity: the case of environmental activists. _Proceedings of the American Society for Information Science and Technology, 44_(1), 1-13.
*   Savolainen, R. (2008). Source preferences in the context of seeking problem-specific information. _Information Processing & Management, 44_(1), 274-293.
*   Sonnenwald, D.H. (2006). Challenges in sharing information effectively: examples from command and control. _Information Research, 11_(4) paper 270\. Retrieved from http://InformationR.net/ir/11-4/paper270.html
*   Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _New Review of Information Behaviour Research, 3_, 143-59\.
*   Urquhart, C. & Rowley, J. (2007). Understanding student information behaviour in relation to electronic information services: lessons from longitudinal monitoring and evaluation, part 2\. _Journal of the American Society for Information Science and Technology, 58_(8), 1188-1197
*   Veieinot, T. (2009). Interactive acquisition and sharing: understanding the dynamics of HIV/AIDS information networks. _Journal of the American Society for Information Science and Technology, 60_(11), 2313-32\.
*   Wang, P., Dervos, D. A., Zhang, Y. & Wu, L. (2007). Information-seeking behaviours of academic researchers in the internet age: A user study in the United States, China and Greece. _Proceeding of the American Society for Information Science and Technology, 44_(1), 1-29.
*   Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation, 37_(1), 3-15.
*   Wilson, T.D. (2000). Human information behaviour. _Informing Science, 3_(2), 49-55.
*   Wilson, T.D. (2010). Information sharing: an exploration of the literature and some propositions. _Information Research, 15_(4) paper 440\. Retrieved from http://InformationR.net/ir/15-4/paper440.html

</section>

</article>