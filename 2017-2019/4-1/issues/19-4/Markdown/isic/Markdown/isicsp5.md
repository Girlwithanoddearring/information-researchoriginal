<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# The 2014 Scottish independence referendum: a study of voters' online information behaviour

#### [Graeme Baxter](#author) and [Rita Marcella](#author)  
Department of Information Management, Aberdeen Business School, Robert Gordon University, Garthdee Rd, Aberdeen AB10 7GE, UK

#### Abstract

> **Introduction.** The paper will present the preliminary results of a study of voters' online information behaviour being conducted during the 2014 Scottish independence referendum campaign. The referendum provides a rare opportunity to explore politicians' use of the Internet, and citizens' information behaviour, in a completely different campaign setting: one where traditional political opponents have joined forces to either support or oppose the independence argument.  
> **Method.** The study uses the authors' interactive, electronically-assisted interview method, where participants are observed and questioned as they search for, browse and use information on the web sites and social media sites of political actors taking part in the pro- and anti-independence campaigns.  
> **Analysis.** All interviews are being audio-recorded digitally and transcribed. The transcripts will then be analysed to identify the important themes and issues emerging.  
> **Results.** The paper presents the researchers' initial impressions of the nature and impact of voters' online behaviour during the campaign. For example, it will report on the participants' views of the relevance and comprehensibility of the information obtained, and on the potential influence of this information on their voting decisions.  
> **Conclusions.** The paper presents some preliminary conclusions concerning the relationship between the provision and use of online political information and the democratic process.

In April 2011, at the height of the Scottish Parliament election campaign, the current authors conducted a study of voters' online information behaviour, where 64 citizens of Aberdeen, in North-east Scotland, were observed and questioned as they searched for, browsed and used information on the web sites and social media sites of political parties and individual parliamentary candidates. That study used the interactive, electronically- assisted interview method, previously developed by the authors during an investigation of the provision and use of online parliamentary information ([Marcella, Baxter and Moore, 2003](#Mar03)).

The results of the 2011 study ([Baxter _et al_., 2013](#Bax13)) revealed a dichotomy between the information needs of the general public (at least in North-east Scotland) and the nature and extent of the information actually provided online by electoral contestants. Citizens wanted to see concise policy information relating specifically to their local constituencies, up-to-date campaign news, and evidence of online engagement between politicians and voters - all of which was lacking in the sites visited by study participants. Participants were also decidedly unimpressed with the social media sites examined, with '_trivial_', '_puerile_' and '_shallow_' being amongst the terms used to describe the politicians' efforts. And although online campaign sites were generally regarded as serving a useful purpose, as being easy to use and understand, relatively interesting, and likely to be visited again, there was very little evidence to indicate that they had any significant impact on citizens' voting intentions. Rather, many participants suggested that more traditional information sources, particularly print and broadcast media, coupled with long-established campaign techniques, such as leaflet deliveries and door knocking, would continue to be more influential in determining their democratic choice. Others indicated that they had a long-established allegiance to a specific party, which was unlikely to be affected by receiving campaign information, either online or offline.

That 2011 study was a particularly timely and significant addition to an ever-expanding body of literature on online electioneering, for, as Gibson and Ward ([2009](#Gib09)) have pointed out, the literature has been dominated by '_supply-side questions_', where researchers have quantified the extent to which political actors have adopted online campaign tools, or where they have analysed the content of campaign websites. Far less attention has been paid to the '_demand side_' of e-campaigning - studies that have explored the extent to which the electorate visit campaign websites, or, more significantly, what impact exposure to these sites has on voting decisions. Gibson and Ward also emphasise that most studies of users of online campaign sites have been largely quantitative in nature, where the researchers have used multiple regression techniques to explore relationships between Internet use during elections and citizens' levels of political efficacy, knowledge, trust or engagement; or where participants have been exposed to candidates' websites and their attitudes towards the candidates and political issues haven then been measured using Likert-type scales. They bemoan the lack of qualitative user studies, and that '_we still know relatively little about the way citizens engage with technology and online information_' ([Gibson and Ward, 2009](#Gib09), p.96). Indeed, Gibson and Römmele ([2005](#Gib05), p. 283) argue that obtaining '_a better in-depth understanding of individuals' online election experiences_' would assist in better shaping the questions asked in quantitative opinion surveys, allowing more precise causal inferences to be drawn about voters' exposure to campaign sites.

During the summer of 2014, the current authors have continued their more qualitative approach with a study of voters' online information behaviour during the Scottish independence referendum campaign. On 18 September 2014, the people of Scotland will be asked the dichotomous Yes or No question, '_Should Scotland be an independent country?_', which may result in the most significant constitutional change in the United Kingdom in over 300 years. This referendum provides a rare opportunity to explore political actors' use of online technologies, and citizens' information behaviour, in a completely different campaign setting: one where traditional political opponents have joined forces to either support or oppose the independence argument; and where much of the debate will be based around unknowns and imponderables, where the true impact of an independent Scotland will only emerge following post-referendum negotiations with the UK Government, the European Union, and other international partners, such as the United Nations and the World Trade Organization.

Two formal campaign groups have been established: the pro-independence Yes Scotland group, led by the Scottish National Party, with support from the Scottish Green Party; and the pro-union Better Together group, with broad support from the Labour, Liberal Democrat and Conservative parties. Each campaign group has its own website and associated social media sites; and each of the major political parties is running its own pro- or anti-independence campaign, and making extensive use of online technologies in the process. An added dimension to the independence referendum is the extension of the voting franchise to 16- and 17-year-olds throughout Scotland. The UK's Electoral Commission ([2013](#Ele13)) has acknowledged that social media will play an important role in encouraging these young people to register to vote; and it is perhaps safe to assume that both the Yes and the No campaigns will be using social media extensively in an effort to attract these new voters.

With these points in mind, the authors have been using their interactive, electronically- assisted interview method in a series of research events held in public spaces, such as libraries and community centres, across North-east Scotland. The interview schedule used in the study comprises four distinct parts:

1.  demographic questions exploring age, occupation, education and residence;
2.  structured questions on voting patterns, political participation, computer use, and past need for election campaign information;
3.  a free-form period of undirected information seeking on the referendum campaign site(s) of the participant's choice; and
4.  post-search, structured questions on the user friendliness of the sites visited, the relevance and comprehensibility of the information found, the likelihood of such sites being revisited prior to the referendum polling day, and the extent to which the information obtained might influence voting behaviour.

The ISIC conference is to take place just two weeks before the poll on 18 September, by which time most of these research events will have been completed. The short paper will, therefore, provide a necessarily brief overview of the researchers' initial impressions of the nature and democratic impact of voters' online behaviour during what is a key episode in Scottish constitutional history.

<section>

## References

<ul>

<li id="Bax13">Baxter, G., Marcella, R., Chapman, D. & Fraser, A. (2013). Voters' information behaviour when using political actors' websites during the 2011 Scottish Parliament election campaign. Aslib Proceedings, 65(5), 515-533.</li>

<li id="Ele13">Electoral Commission (2013). Public awareness for the Scottish independence referendum. Retrieved from http://www.electoralcommission.org.uk/    data/assets/pdf_file/0018/157500/Public- awareness-briefing-June-2013.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6Pr7iLmQc)</li>

<li id="Gib09">Gibson, R. & Ward, S. (2009). Parties in the digital age?a review article. <em>Representation, 45</em>(1), 87-100.</li>

<li id="Gib05">Gibson, R.K. & R&ouml;mmele, A. (2005). Truth and consequence in Web campaigning: is there an academic digital divide? <em>European Political Science, 4</em>(3), 273-287.</li>

<li id="Mar03">Marcella, R., Baxter, G. & Moore, N. (2003). Data collection using electronically assisted interviews in a roadshow ? a methodological evaluation. <em>Journal of Documentation, 59</em>(2), 143-167.</li>
</ul>

</section>

</article>