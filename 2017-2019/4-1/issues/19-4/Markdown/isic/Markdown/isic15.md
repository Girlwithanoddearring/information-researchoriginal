<header>

#### vol. 19 no. 4, December, 2014

##### Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# The general public's access and use of health information: a case study from Brazil

#### Ariadne Chloe Furnival  
Department of Information Science, Universidade Federal de São Carlos (UFSCar), S.P., Brazil

#### Nelson Sebastian Silva-Jerez  
Universidade Estadual de São Paulo, Marília, S.P. Brazil

#### Abstract

> **Introduction.** The aim of the research was to map how members of the general public in Brazil find, access, evaluate and use health information found on the internet.  
> **Method.** A survey of structured questions was made available online using _Limesurvey_, the same instrument being used as a protocol for face-to-face structured interviews with members of the public randomly selected.  
> **Analysis.** The collected data underwent statistical analysis, performing simple correlations with variables of age, gender and occupation. Adapting the monitoring tool developed by Niemelä et al., a score for the participants' degree of _everyday health information literacy_ was generated based on the participants' own perceptions of how they carry out certain daily information tasks.  
> **Results.** Although participants obtain health information from medical and/or scientific sources, overall they lack confidence in their own skills to retrieve information and assess its reliability. Perceived credibility of health information sources on the Internet was higher for scientific articles, although only a quarter of participants in fact use these. Many participants claim to have used, or would use, health information found on the Internet in the decision-making process, including to change doctors.  
> **Conclusions.** There was an identified latent desire to use more reliable information - understood in this research as being that of scientific, peer-reviewed and open access origin - in everyday health issues. Participants' relatively low estimation of their health information literacy skills could be tackled with public programmes directly aimed at minimising this perceived lacuna.

## Introduction

The rapid growth and deepening global omniscience of digital culture and the worldwide push for more open access, access to knowledge and knowledge sharing - a push precisely aiming to confront the equally deepening economic grip of a few powerful players who seek to restrict the free flow of information - means that the general public has arguably never before experienced such potential freedom to access high quality, peer-reviewed scientific information. The benefits of open access for researchers - namely, that the elimination of access barriers to the scientific literature can help to accelerate scientific research and thus scientific and technological progress - are arguably more tangible than possible benefits to potential users of this literature spread throughout the general public, although access to the scientific literature by members of this potential user group is often cited as one of the main moral arguments in favour of open access ([Willinsky, 2006](#Wil06); [Suber, 2012](#Sub12); [Zuccala, 2009](#Zuc09)). The advocacy for possible access and use of the specialist, peer-reviewed literature by laypeople is seen by open access activists as entirely natural and unpolemical, a natural extension of the democratic _access principle_ ([Willinsky, 2006](#Wil06)), but at the same time, as something unnecessary by open access opponents (some of the big commercial publishers, for example), who argue that the lay public would not understand such literature once accessed. But Suber ([2012](#Sub12)) observes that to argue against open access for laypeople because they do not need it is akin to arguing against the development of a new drug because not everyone will _need it_, or, as Willinsky ([2006](#Wil06)) claims, it is analogous to saying that laypeople should not visit art galleries if they are not art connoisseurs. The public's understanding of science and technology in its varied manifestations today constitutes a sophisticated global research agenda, not least because it is recognised that calls for the public's participation in policymaking processes in many countries will inevitably intersect with their understanding of scientific and technological information, due to the _technoscientific_ nature of policy problems.

Harnard ([2006](#Har06)) has observed that what usually comes to mind for the lay public when talking of scientific research and breakthroughs are cures for cancer or an end to global warming. It is probably true that the area the lay public is most likely to seek scientific information is that of health and medical information. The principle of promoting free access to such information was consecrated in the 2001 Declaration of Havana for Equal Access to Health Information. Around the same time, an alliance between American Library Association and the Alliance for Taxpayer Access forged links to promote access to information, and in 2005, five professional librarianship organisations officially supported the CURES Bill, a bill that called for expanded public access to articles resulting from research financed by the National Institute of Health (NIH) in the United States ([Mullen, 2010](#Mul10)). In the UK, the Wellcome Trust pioneered such open access mandates for the researcher-beneficiaries of its research funding. In Brazil, where the open access landscape is more densely concentrated around open access journals than repositories, there are currently around 95 such journal in the health sciences accessible from the Scientific Electronic Library Online - SciELO.

Zuccala ([2009](#Zuc09)) has emphasised the relevance of laypeople having online access to publish results of medical research because the reading of it can help to reduce uncertainty, and provide a basis for meaningful discussion with health professionals, fomenting a sense of empowerment and responsibility for the choices and decisions in their own health treatment possibilities: in short, such access has the potential to promote shared decision-making. In such a context, the issue of Health Information Literacy assumes great relevance, and according to the Medical Library Association, this modality of information literacy can be defined as:

> the set of abilities needed to: recognize a health information need; identify likely information sources and use them to retrieve relevant information; assess the quality of the information and its applicability to a specific situation; and analyze, understand, and use the information to make good health decisions ([Medical Library Association, 2003](#Med03)).

In the light of the intersections of the two fronts briefly delineated above - open access and the public's access to and potential use of health information online, the research here related sought to map members of the general public's perceptions, access and potential use of scientific health information, and the identification of potential decision making processes made in the light of such access. Thus, the directing goals of the study were to ascertain: how and where users access such information; if they assess the reliability of such information; if they distinguish between peer reviewed scientific information encountered via open access and scientific journalism; their potential use of this type of information. The results collected also helped to formulate initial scores for _everyday health information literacy_, discussed further on in more detail.

## Method

The study here reported, conducted in a municipality in the interior of the state of São Paulo, Brazil, in the first half of 2013, used quantitative methods for data collection and analysis. A structured survey-type questionnaire was elaborated, loosely based on that of Palmer, Dill and Christie ([2009](#Pal09)) (used for mapping librarians' perceptions of open access). The work of Niemelä _et al_. ([2012](#Nie12)) was important for providing a tool for measuring the _everyday health information literacy_ of the research participants. Our final questionnaire was composed of 25 questions covering three thematic fronts: questions number 1 to 6 were adapted from Niemelä _et al_. ([2012](#Nie12)) and aimed to obtain a general view of the image the participants have of their own abilities and informational competencies on the subject of health as well as how they deal with and recognise their own informational needs. The second group of questions gauged how the participants access health information and what their perceptions are of the reliability of the information found in relation to the canals and means used to reach it.Within this group, a sub-group of six questions specifically aimed to assess the participants' level of awareness and use of scientific literature and also of open access. And finally, there were four questions asking about their use of information found online and if this had affected their decision making - with or without health professionals - in any way.

To expediate data collection during the structured interviews, and to facilitate quantitative analysis of the data, a Likert scale was adopted. Mattel and Jacoby ([1972](#Mat72)) discuss the issue of the ideal number of options on such scales, as well as the problem of using a 'null' or 'no opinion' category. Bertram ([2007](#Ber07)) also discusses the tendency of the interviewee to gravitate towards the centre option, to avoid extreme categories, and likewise, Mattel and Jacoby ([1972](#Mat72)) consider that the inclusion of the middle category to accommodate uncertainties or lack of opinion on the subject is '_sometimes considered inadvisable because it provides too easy and attractive an escape for respondents who are disinclined to express a definite view_'(p. 506). Regarding the ideal number of items on a scale, these authors found that '_[…] when time or time-related factors (e.g., fatigue, warm-up, boredom) are of concern, especially given lengthy tests and test batteries, fewer alternatives should not necessarily be employed_', concluding that '_[…] 6- or 7-point scales would appear to be optimal_' ([Mattel and Jacoby, 1972](#Mat72), p. 508).

Our questionnaire was pre-tested with six participants: four university students, one administrative worker and one retired seamstress, covering a 22-70 age range. The average response time for the whole questionnaire was eleven minutes. The final version of the questionnaire was also then implemented online using the open software _Limesurvey_, which also facilitates data analysis, and fifty-five participants responded the questionnaire via this online version. Eighteen face-to-face interviews with members of the general public were conducted using the same questionnaire as a structured interview protocol. For this data collection, the first author of this paper approached people in the street or in front of their homes.

## Presentation and discussion of some results

Respondents were mainly female (63%), single (78.1%) and students (49.3%), with 23.3% of the sample possessing all three of these traits. Regarding age, 32.9% fell into the 17 to 21 age group, and 39.7% in the 22 to 26 age group, the remaining 27% falling within other age groups. This evidently is related to the fact that younger respondents used the internet survey, given that 66.7% of the live interview respondents were over 27\. For reasons of lack of space, we will here only present some of the results deemed to be more relevant to the conference theme of information behaviour.

There was almost unanimous total agreement - 98.6% and 97% respectively - with two initial statements: '_It is important to be informed about health related topics_' and '_I think it is important to have access to health information from scientific and medical journals_'. Following Niemelä _et al_.'s _everyday health information literacy_ model discussed above, questions 2 to 5 asked the research participants to manifest their degree of agreement with statements related to their abilities and competencies in retrieving and evaluating health information. These statements are shown on the horizontal axis of Graph 1 below, which presents the results and which shows that once again, most of the participants agreed with all of the statements, with the exception of statement number 3, for which the sum of those who disagree reaches 49.3% contrasted to 32.9% for those agreeing. A clear division is thus perceptible between those who consider themselves capable or not to assess the trustworthiness/reliability of health information they encounter. But it is also interesting to note, as evident from Graph 1, that as much as among those disagreeing as agreeing, most of the participants adopt a posture that is less convincing, opting for the so-called _partial_ choice, and furthermore, a larger group (17.8%) than seen for the other statements opted for the neutral reply. Finally, it should be noted that in Graph 1, while statements 2 and 3 have a positive connotation in relation to participants' abilities and competencies, statements 4 and 5 have negative connotations, in that agreement with them implies the participants' acknowledging their deficiencies and insecurities concerning the abilities and competencies outlined in the statements.

<figure>

![Participants' perceptions of their Everyday Health Information Literacy](../isic15fig1.jpg)

<figcaption>Figure 1: Participants' perceptions of their Everyday Health Information Literacy</figcaption>

</figure>

Following Niemelä _et al_. (2012), we also attributed points to these answers, in order to obtain an _everyday health information literacy_ score that would be able to identify '_individuals with problems related to their interest and motivation, finding, understanding, evaluating and using of health information_' (p. 130), dividing them into groups in accordance with their scores. The Likert values used by Niemelä _et al_. were from 1 to 5 for statements of a positive nature regarding the skill in question, inverting these for more negative statements. We adapted this slightly, using values between -2 and 2, believing that in this way the neutral and central point on the scale was better represented by zero, instead of a 3, as in Niemelä _et al_.'s method. Our belief was that to attribute 3 points to a central and neutral category introduces an unnecessary bias, because it can make a neutral answer - which often points to the inexistence of opinion on a subject or even the wish not to speak out on it for whatever reason - three times more valued than a reply that reveals a strong conviction in the opposite sense. We also chose to attribute double the weight to the answers that manifest a strong conviction, in one or other sense. In this way the spectrum of possible scores in our instrument goes from -8 for participants who disagreed totally in all of the statements, to 8 for those who agreed totally with all of them. Applying this method of scoring to our sample produced results (as seen in Graph 2 below) showing that none of the participants obtained scores at the positive or negative extremes of the spectrum, but that just over half of the participants (50.7%) had negative scores, with 37% obtaining a positive score, seeming to point to only a relatively good level of health information literacy amongst the participants. Crossing these results with gender though, women have a greater preponderance to negative scores: 52.2%, (compared to 32.6% positive), while men had more well-distributed scores: 48% having positive scores, and 44.4% negative ones.

<figure>

![Graph 2: Health Information Literacy Scores](../isic15fig2.jpg)

<figcaption>Figure 2: Health Information Literacy Scores</figcaption>

</figure>

Graph 3 shows how the participants replied to the question: '_Where do you usually look for health information?_', indicating the main information sources used. As to be expected, the Internet was cited by 83.6%, followed by health professionals at 74%. In Brazil, the figure of the family doctor is a _dying breed_, which possibly explains why only 19.2% of the participants chose this option. The fact that 47.9% also chose labels and medicine instruction leaflets is indicative of the fact that in Brazil, these are extremely detailed and informative, despite this information also being in very small print (although increasingly, online forms of the same content are available). Among the 61 participants (83.6%) who cited using the Internet as an important source of health information, the type of site most used were institutional sites (47.9%), news sites (38.4%) and governmental sites (37%). Repositories were cited by 26%. And regarding perceived trustworthiness/reliability of information retrieved from the Internet, interestingly these were considered to be the most trustworthy source of health information.

<figure>

![Participants' main health information sources](../isic15fig3.jpg)

<figcaption>Figure 3: Participants' main health information sources</figcaption>

</figure>

From Graph 4 below, most of the sites were classified by the participants as being '_reasonably trustworthy_', or as in the case of wikis, obtaining this classification alongside equally the neutral term, '_neither trustworthy nor untrustworthy_'. Blogs obtained the most number of classifications as being '_untrustworthy_'. And only governmental and institutional sites, along with scientific article repositories obtained '_totally trustworthy_', but the latter was the only information source type on the Internet to obtain not even one '_untrustworthy_' classification, and for which the number of participants classifying them as 'totally trustworthy' outnumbered the 'reasonably trustworthy' classifications. This was the first time scientific article repositories had been mentioned in the research, and a probable explanation for such trust in repositories is that the sample contained a considerable number of students, being that the local where the research was performed is considered an university city, with a high number of students in the population.

<figure>

![Participants' perceptions of information source trustworthiness](../isic15fig4.jpg)

<figcaption>Figure 4: Participants' perceptions of information source trustworthiness</figcaption>

</figure>

Participants had previously answered a question to ascertain whether they believed they could clearly distinguish between a scientific article and a scientific journalism article. Despite the high percentage (84.2%) affirming that they could indeed make such a distinction, and, as just noted, repositories of scientific articles being perceived positively in terms of trustworthiness/reliability, it should be remembered that just 26% of participants use scientific articles as a health information source on an everyday basis. Likewise, although almost 57% of participants know what SciELO is, 62% claimed never to have used SciELO as a source of health information.

The last four questions in the questionnaire sought to establish how participants use the information actively found and/or encountered, and particularly how they use it in their decision making processes, which could be seen as an indicator of the level of trust they deposit in the information found, as well as of their specific abilities in finding and judging the relevance of information to the needs, which is to say, an implicit self-confidence in their own levels of health information literacy. Graph 5 shows the results to these questions.

<figure>

![Use of information found on the Internet (%)](../isic15fig5.jpg)

<figcaption>Figure 5: Use of information found on the Internet (%)</figcaption>

</figure>

It was found that around 60% use information in this critical way to inform their decision making processes, and that 75% have taken or would take an important decision to try to change their health professional in the light of information found on the Internet (remembering again that in Brazil, many people have private health plans and are therefore in the situation of being able to _change doctors_ according to their tastes). No conclusive or relevant correlations were found on crossing these results with gender and occupation, although it was observed that while a greater percentage of women have already questioned their health professionals about some treatment about which they have read on the internet (67.6% contrasted with 56.5% of the male participants), the male participants seem to be more open to the possibility of disagreeing with the professional opinion of a doctor because of information found on the internet, and also, to the possibility of seeking another professional.

## Concluding remarks

The research showed that despite the participants perceiving the importance of being well informed on health-related subjects, and the importance of being able to obtain such information from scientific-medical sources, this did not translate into similar levels of confidence in their own abilities to retrieve information and assess its reliability, as seen from the _everyday health information literacy_ scores. That is to say, most of the participants recognise the relevance of the subject to their daily lives, declare to know where to find the information, but at the same time, manifest a certain skepticism about their abilities to gauge its reliability/trustworthiness or partiality, and to understand it. Despite this, it was notable that health information found or encountered on the internet has or would, alter the participants' decision making processes regarding health professionals and choices of courses of treatment. This seems to indicate that assimilation of health information found occurs on one, not necessarily specialised level, but that its availability certainly does foment the potential for empowered patients equipped, up to a point, for dialogue and shared decision making about their own health with health professionals, which surely constitutes a strong argument in favour of promoting continued open access to this type of information. Despite the increasing dissemination of the internet as a source of everyday health information and of the general public's latent desire to be able to identify and use more reliable information, such as of the scientific peer-reviewed type, the study revealed self-perceptions of health information literacy skills that could potentially benefit from more explicit and directed programmes in everyday health information literacy, for example for elderly members of the public.

Finally, we evidently believe that a more detailed study, and most importantly, with a larger sample, would obtain more conclusive and generalisable results, and we would hope that other colleagues in other parts of Brazil might adapt, improve and share this research agenda and method.

## Acknowledgements

We are grateful to CNPq/CAPES (process: 401875/2011-3) for a research grant awarded to ACF and to FAPESP (process: 2012/02219-7) for a Scientific Initiation research grant awarded to NSSJ.

<section>

## References
 
<ul>
<li id="Ber07">Bertram, D. (2007). <em>Likert scales.are the meaning of life?</em> Retrieved from http://pages.cpsc.ucalgary.ca/~saul/wiki/uploads/CPSC681/topic-dane-likert.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6SC3EuUBM)</li>
<li id="Har06">Harnard, S. (2006). <em>Opening access by overcoming Zeno's paralysis</em>. In N. Jacobs (Ed.), Open Access: key strategic, technical and economic aspects (pp. 73-86). Oxford: Chandos.</li>
<li id="Mat72">Mattel, M.S. & Jacoby, J. (1972). Is there an optimal number of alternatives for Likert-Scale items? Effects of testing time and scale properties. <em>Journal of Applied Psychology</em>, 56, 506-509.</li>
<li id="Med03">Medical Library Association (2003). <em>Health Information Literacy Definitions</em>. Retrieved from https://www.mlanet.org/resources/healthlit/define.html (Archived by WebCite&reg; at http://www.webcitation.org/5d2E2w7PV)</li>
<li id="Mul10">Mullen, L.B. (2010). <em>Open Access and its practical impact on the work of academic librarians</em>. Chandos Information Professional Series. Oxford: Chandos.</li>
<li id="Nie12">Niemel&auml;, R., Ek, S., Eriksson-Backa, K. & Huotari, M-L. (2012). A Screening Tool for Assessing Everyday Health Information. <em>Libri, 62</em>, 125-134.</li>
<li id="Pal09">Palmer, K.L., Dill, E. & Christie, C. (2009). Where there's a will there's a way?: Survey of academic librarian attitudes about open access. <em>College & Research Libraries</em>, 70, 315-335.</li>
<li id="Sub12">Suber, P. (2012). <em>Open Access</em>. Cambridge, MA: MIT Press.</li>
<li id="Wil06">Willinsky, J. (2006). <em>The access principle: the case for open access to research and scholarship</em>. Cambridge, MA: MIT Press.</li>
<li id="Zuc09">Zuccala, A. (2009). The lay person and Open Access. <em>Annual Review of Information Science and Technology, 43</em>(1), 1-62.</li>
</ul> 

</section>

</article>