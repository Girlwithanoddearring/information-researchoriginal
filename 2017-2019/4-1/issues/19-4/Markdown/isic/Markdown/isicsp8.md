<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# A survey of information sharing on Facebook

#### Chandramohan Ramaswami, Murugiah Murugathasan, Puvandren Narayanasamy, and Christopher S.G. Khoo  
Wee Kim Wee School of Communication and Information, Nanyang Technological University, 50 Nanyang Avenue, Singapore 639798

#### Abstract

> **Introduction.** This study sought to identify the types of information shared by Facebook users with different categories of friends.  
> **Method.** A questionnaire survey was administered to a sample of students at the Nanyang Technological University, Singapore. The questionnaire listed the following types of information: school, health, entertainment, shopping, food, hobby, work, fashion and sports-related information. The categories of friends were defined as schoolmates, family and relatives, colleagues, close friends, friends with a common interest, and online friends.  
> **Analysis.** Statistical analysis was carried out using cross-tabulations and Chi-squared test of independence.  
> **Results.** Of the 314 respondents, 305 (97%) indicated they had a Facebook account. Entertainment-related information was the most common type of information shared, followed by food-related information and hobby-related information. There were significant age and gender differences in the types of information shared. Females, younger users, undergraduates, frequent Facebook users and users with more friends were more likely to share entertainment information. Women were also more likely to share shopping and fashion information, whereas men were more likely to share sports-related information and reviews of mobile devices. Facebook users in the 26-35 age group were more likely to share reviews of mobile devices than the younger or older users.  
> **Conclusions.** Information shared on Facebook is of the more _frivolous_ variety, but there is some indication that as people distinguish between types of friends and the closeness of the relationships, they will share more _serious_ types of information such as health, work- related, school-related and product review information.

## Introduction

There has been a shift in online information behaviour with the growth of social networking sites that support information sharing and exchange among people who are known to each other and have established relationships ([Lampe, Vitak, Gray and Ellison, 2012](#Lam12)). Search engines and websites are the primary sources of online information. However, social networking sites are becoming alternative or complementary online information sources, providing useful, personalized and trusted information that is difficult to obtain from websites and search engines ([Morris, Teevan & Panovich, 2010b](#Mor10b)).

This paper reports an initial study of the types of information shared by users on Facebook across different categories of friends, through a questionnaire survey of undergraduate and graduate students at the Nanyang Technological University, Singapore in October 2013.

#### Previous studies

We have found only two studies that have attempted to identify the types of information shared on social networking sites. Xu, Zhang, Wu and Yang ([2012](#Xu12)) examined the motivations for sharing three types of information on Twitter: breaking news, posts from friends and information related to the user's intrinsic interest. They found that breaking news was the most popular type of information to be shared on Twitter.

Morris, Teevan and Panovich ([2010b](#Mor10b)) surveyed Microsoft employees (including college interns) to collect example questions that the respondents had posted to their social networks (mainly Facebook and Twitter) as status messages. The respondents were asked to contribute an example of the questions they had posted. The resulting 249 contributed questions were categorized into the following types and topics: recommendation type question (29% of contributed questions), opinion or rating type question (22%), technology topic (29%), entertainment topic (including movies, TV, the arts, books, sports and music) (17%), home/family topic (including children, pets, health, cooking, gardening and real estate) (12%), professional topic (including jobs, education and conferences) (11%), places (travel and local transportation) (8%), restaurant topic (6%), current events or news (5%), and shopping (non-technology) (5%). Most of these information types were incorporated into our questionnaire.

## Research method

Survey participants were recruited by visiting classes in the School of Communication and Information and the School of Physical and Mathematical Sciences, and also by approaching students in high density areas in the university such as student lounges, study areas and canteens. In the survey questionnaire, types of information were grouped into the broad categories of school, health, entertainment, shopping, food, hobbies, and fashion- related information. These were broken down into sub-types of information.

Of the 314 respondents, 9 (3%) indicated they did not have a Facebook account and were left out of the analysis. There were almost equal numbers of graduate and undergraduate students, with 55% of the respondents being female and 20% working professionals. The majority of the respondents were in the age group of 21-25 years (58%), and 68% were Singapore citizens. The data analysis was carried mainly with cross- tabulations and using the chi-squared test of independence to test for statistical significance at the α=0.05 level.

## Results

42% of the participants were constantly logged on to Facebook throughout the day, and 78% logged on at least once a day. 35% had more than 500 friends. Most of the users (72%) had not set up friends lists in Facebook. The most common friends lists that had been set up were: close friends, family/relatives and schoolmates.

Looking at the types of information, entertainment information was shared by the most number of respondents, followed by food-related information and hobby-related information. Other types of information were associated with particular categories of friends:

*   School-related information with schoolmates
*   Health and shopping-related information with family/relatives
*   Work-related information with colleagues
*   Hobby-related information with common-interest friends
*   Fashion and sports information appear to be shared primarily with close friends.

Close friends obtained a higher percentage for all types of information: ranging from 37% of respondents who indicated they shared work-related information with close friends, to 77% of respondents who shared entertainment information with close friends.

As indicated earlier, entertainment information was shared by the most number of respondents. However, there were significant differences by age, gender, and other attributes. Respondents who were more likely to share entertainment information were: the 18-25 age group, females, undergraduates, users who logon a least weekly, and users with more than 300 friends. A higher percentage of women shared entertainment, shopping and fashion information, whereas a higher percentage of males shared sports-related information.

We found significant age and gender differences for the numbers of respondents who shared reviews/recommendations of mobile devices. A significantly higher number of the 26-35 age group indicated that they shared reviews of mobile devices, compared to the younger 18-25 group and the older 36-55 group. Males were also more likely to share such reviews than females.

Comparing users who had created friends lists versus users who had not: users who had created friends lists were significantly more likely to share health, hobby and work-related information with family and relatives.

## Conclusion

The survey has identified some broad patterns in the types of information shared on Facebook by students at a Singapore university. Information shared on Facebook is of the more _frivolous_ variety. There is some indication that as people distinguish between types of friends on Facebook and the closeness of the relationships, they will share more _serious_ types of information such as health, work-related, school-related and product review information. Even though only 26% of the Facebook users had created friends lists, a majority of the survey participants distinguished between categories of friends in terms of the types of information that they indicated they shared. We are developing a more refined survey instrument with more specific categories of information, to be used in a multi-country survey.

<section>

## References

<ul>
<li id="Lam12">Lampe, C., Vitak, J., Gray, R. & Ellison, N. (2012). Perceptions of facebook's value as an information source. In CHI'12: <em>Proceedings of the SIGCHI Conference on Human Factors in Computing Systems</em> (pp. 3195-3204). New York: ACM.</li>
<li id="Mor10a">Morris, M.R., Teevan, J. & Panovich, K. (2010a). A comparison of information seeking using search engines and social networks. In <em>Proceedings of the Fourth International AAAI Conference on Weblogs and Social Media</em> (ICWSM 2010) (pp.23-26). Palo Alto, CA: Association for the Advancement of Artificial Intelligence. Retrieved June 14, 2014, from http://www.aaai.org/ocs/index.php/ICWSM/ICWSM10/paper/download/1518/1879</li>
<li id="Mor10b">Morris, M. R., Teevan, J. & Panovich, K. (2010b). What do people ask their social networks, and why?: A survey study of status message Q&A behaviour. In <em>CHI'10: Proceedings of the SIGCHI Conference on Human Factors in Computing Systems</em> (pp. 1739-1748). New York: ACM.</li>
<li id="Xu12">Xu, Z., Zhang, Y., Wu, Y. & Yang, Q. (2012). Modeling user posting behaviour on social media. In <em>Proceedings of the 35th International ACM SIGIR Conference on Research and Development in Information Retrieval</em> (pp. 545-554). New York: ACM.</li>
</ul>

</section>

</article>