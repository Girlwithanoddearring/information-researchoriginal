<header>

#### vol. 22 no. 3, September, 2017

</header>

<article>

# From paper-based towards post-digital scholarly publishing: an analysis of an ideological dilemma and its consequences

## [Jarmo Saarti](#author) and [Kimmo Tuominen](#author).

> **Introduction**. Even though the current publishing model is based on digital dissemination, it still utilizes some of the basic principles of printed culture. Recently a policy emphasis towards _open access_ has been set for publicly funded research. This paper reports on a study of the practices, business models and values linked with scholarly publishing.  
> **Method**. Conceptual analysis was conducted, drawing on literature on scholarly publishing policies, practices, values and economies, with an emphasis on the structures and conflicts between license-based and open publishing models.  
> **Results**. Scholarly interests of sharing collide with commercial interests of generating profits. In the digital era, the scientific community might have a third economically viable alternative. This third way is based on what the authors call _post-digital scholarly publishing._  
> **Conclusion**. Science should aim at as complete openness as possible. Scholarly activities advance best when the whole scientific community has access to both publications and research data. What seems to stand in the way of scientific sharing is the global publishing industry in its present form. In the future, post-digital scholarly publishing might provide a means for finding an economically viable way between sharing economy and commercial interests.

<section>

## Introduction

The promise of free and efficient information transfer is a cornerstone of the ideology of information society. In 1994, mathematician Andrew Odlyzko predicted a brave new world of scholarly e-publishing that would be dramatically cheaper than the traditional paper journal-based model: _‘if I can get a preprint of a published paper for free, why should I (or my library) pay for the journal?’_ (p. 9) he asks, and promotes the idea of scholars being their own publishers and information disseminators. According to Odlyzko ([1994](#odl94)), the future of intermediaries like publishers and librarians looks bleak, and the advancement of technology will largely eliminate their jobs in the very near future.

Almost twenty years after the appearance of Odlyzko’s paper, the present seems to be quite different than predicted. The dramatic decrease of the purchasing costs of e-journals is yet to be seen. Actually, scientific publishing is considered as one of the most profitable and successful businesses, as Odlyzko ([2013](#odl13)) himself has recently remarked. The profit margins of scientific publishing have been estimated to be between twenty and thirty percent ([van Noorden, 2013](#van13), p. 427; [Monbiot, 2011, August 29](#mon11)). Libraries remain an integral part of the publishing model, which is based on outsourcing the dissemination of scientific results to publishing companies ([Morrison, 2013](#mor13); [Odlyzko, 2013](#odl13)). This model has proven to be extremely challenging in economically-turbulent times because it is based on the continuous growth of the subscription costs of scholarly journals.

What we have eventually witnessed in recent years is the rise of paywalls, i.e., technical mechanisms separating the digital content that one has to pay for from the rest of the content on the Internet. Most new scientific information is behind these paywalls. The traditional paper-based publishing model has transformed, but many aspects of the model are still left in a new form. There has been evolution, but not the kind of complete digital revolution many commentators on scientific publishing have prophesised. Although the digital technology enables the free dissemination of scientific and other information, economic, social and political barriers disable this development ([Ragnedda and Muschert, 2013](#rag13)). In addition there is evidence that scientific publishing has been monopolizing and centralizing during the digital era, i.e. the major publishers are, at present, publishing roughly half of the papers ([Larivière, Haustein and Mongeon 2015](#lar15)).

The predictions of information society and open access visionaries like Odlyzko and his early predecessors, Frederick W. Lancaster ([1978](#lan78)) and J.C.R. Licklider ([1965](#lic65)), have been logical and clear: digital information technology affords us the possibility of global open access to scholarly knowledge and data. However, what is technologically possible might not be socially or ideologically viable. Technology does not completely determine the outcomes of social processes ([Tuominen, Savolainen and Talja, 2005](#tuo05)). When one contrasts the actual present-day practices of scholarly publishing and e-commerce with deterministically-coloured prophesies, one might wonder what kind of social practices and values hinder transitioning to global open access.

The present paper concentrates on the _‘sociological factors’_ ([Odlyzko, 1994](#odl94), p. 35), and asks what kind of conflict of values lies behind the processes of the dissemination of scientific knowledge. To achieve better understanding about this issue we analyse the ideological factors behind scholarly publishing and ask how they affect the roles of the different actors, i.e., academics, publishers and research libraries. To analyse how conflict or dilemma manifest through history we present a sketch of the evolution of scholarly publishing, proceeding from paper to digital to post-digital phases. We conclude with a discussion of what kinds of alternative business models and practices could at least partially overcome the limitations of traditional and present-day publishing models in the post-digital future.

Of course, analysis of the sociological factors affecting scientific publishing is not a new pursuit. Scholars have analysed, among other things, the cultural and linguistic biases of scientific publishing. For example, geographers Chris Gibson and Natascha Klocker ([2004](#gib04), pp. 425-426) argue that the current way of producing scientific results resembles the working of the global entertainment industry. According to Gibson and Klocker, multi-cultural and many-faced scientific publishing is actually impossible when the major publishers are predominantly English speaking. This means that _‘for northern hemisphere English-speaking academics, the lines of access to the more powerful, influential journals and publishers are shorter and much less difficult to negotiate than for geographers elsewhere’_ ([Gibson and Klocker, 2004](#gib04), p. 426). In many ways, scientific domains can be viewed as language games and power structures that prevent new kinds of scientific texts from entering scientific canon if they, for example, too radically question the existing rules of the game ([Lyotard, 1984](#lyo84)).

Because of the conceptual nature of the present paper, the argumentation remains on a general level and does not examine the variations between the publishing cultures and practices of different research fields (cf. [Kling and McKim, 1999](#kli99); [Puuska, 2014](#puu14)). Although we see and fully understand the potential of such approaches, for the purposes of the present paper we concentrate on those sciences in which journal publishing is the mainstream model of delivering scientific results, i.e., on natural and medical sciences. However, the ideological dilemma characterizing scholarly publishing might be seen as a broader phenomenon.

## The ideological dilemma of scientific publishing

Billig _et al._, ([1988](#bil88)) reject the view of ideology as false consciousness and concentrate on lived, _common-sense_ ideologies as composed of beliefs, values and practices. Ideologies are vague and unclear cultural value sets that are not at all coherent and integrated. On the contrary, lived ideologies are characterized by inconsistency, fragmentation and contradiction: common sense often contains the seeds of its own negation. In this spirit, Billig _et al._ ([1988](#bil88)) analyse and identify common ideological ruptures running through the everyday thinking on themes like equality and authority, freedom and necessity, individualism and collectivism.

In the spirit of Billig _et al._ ([1988](#bil88)), we claim that the social world of scientific publishing is ideologically dilemmatic. _There is a tension between the view of scientific knowledge as a social good, on the one hand, and as a commodity, on the other_. As we have witnessed in recent decades, this dilemma is a nasty problem that has good arguments for both sides but does not have an easy solution. Put bluntly, the ideological dilemma of scientific publishing is that between a _Mertonian_ kind of scientific communism, which many scientific ideals reflect, and the capitalist modes of production characterizing scientific publishing industry and contemporary societies more generally. This ideological tension is inherent in the production and distribution of scientific knowledge in the present-day society.

Sociology of science was one of the subfields of sociology Robert K. Merton heavily developed. In his seminal paper, Normative structure of science, Merton ([1973](#mer73)) presents a conception of ideal types or norms not directing but associated with scientific practices (cf. [Shapin, 1988](#sha88)). According to Merton, _‘four sets of institutional imperatives - universalism, **communism**, disinterestedness, organized scepticism - are taken to comprise the ethos of modern science’_ (p. 270, emphasis added).

Universalism is concerned with the impersonality of knowledge claims, which should be independent of and disinterested in personal, political and nationalistic biases, with the moral accountability of individual researchers concerning their motives, and with the _‘institutional control of a wide range of motives which characterizes the behaviour of scientists’_. Organized scepticism is concerned with the research results being in _‘the exacting scrutiny of fellow experts... in terms of empirical and logical criteria’_, i.e., peer-review as a collective practice of scrutiny ([Merton, 1973](#mer73), pp. 270-278).

For the purposes of the present paper, the most interesting of the Mertonian values is communism, i.e., a notion of scientific knowledge as a common good. This principle of radical scientific openness can be seen to be a prerequisite for organized scepticism: one cannot properly assess knowledge claims if one does not have all the information available on which these claims are based. As an integral element of scientific ethos, scientific communism sees, for example, property rights as _‘whittled down to a bare minimum’_ and science as part of the public domain, with a moral imperative for an individual scientist to communicate his or her findings as openly as possible ([Merton 1973](#mer73), pp. 273-275).

However, Merton notes, this conception of scientific knowledge as public good is not without challenges. He stresses that _‘the communism of scientific ethos is incompatible with the definition of technology as a “private property” in a capitalist economy’_. He gives the patenting system as an example, which proclaims _‘exclusive rights of use and, often, non-use’_ for the inventor as a kind of absolute property owner ([Merton 1973](#mer73), p. 275).

As Mitroff ([1974](#mit74)) notes, Merton was fully aware of the ambivalence of sociological norms: every social norm seems to have a counter norm. The dominant norm, e.g., scientific neutrality and disinterestedness, is suitable for using in certain circumstances, but in other contexts these norms could be deemed as naive myths that can be used in philosophy of science textbooks. However, in practice the development of science needs men and women who have _pet hypotheses_, i.e., are deeply and emotionally committed to their theoretical presumptions. According to Mitroff ([1974](#mit74)), even scientific communism has counter norms and practices, like stealing and secrecy. Even if they are morally and ethically dubious practices they can sometimes drive forward the development of science.

Unlike Merton, Mitroff does not consider the dissemination system of scholarly information as an example on which the ideological dilemma of scientific knowledge production is most explicitly seen. More or less conscious stealing of ideas and being secretive one’s (preliminary) research results is the micro behaviour of individual scientists, but Merton identifies a kind of _system-level_ ideological dilemma in how the patenting system prevents the development of science.

Perhaps the most prominent case of this kind of macro-level ideological dilemma of scientific communism against capitalism is how the present-day journal publishing industry works. The principle of radical scientific sharing and capitalistic industry are opposing sides of the same coin, or perhaps the Janus faces of knowledge production and dissemination. The ideological dilemma between scientific communism and capitalism is summarized in Table 1.

<table><caption>Table 1: Value conflicts in scholarly publishing</caption>

<tbody>

<tr>

<th>Scientific knowledge as a common good</th>

<th>Scientific knowledge as a commodity</th>

</tr>

<tr>

<td>academic freedom</td>

<td>business value</td>

</tr>

<tr>

<td>open publicity</td>

<td>copyright restriction</td>

</tr>

<tr>

<td>networking</td>

<td>ownership of intellectual property</td>

</tr>

<tr>

<td>critical deliberation</td>

<td>creating sellable products</td>

</tr>

<tr>

<td>communality</td>

<td>individual and private enterprise</td>

</tr>

</tbody>

</table>

## The evolution of the scientific communication: three phases

Scholarly publishing in the present sense is possible because of writing as a social technology. As noted by Walter Ong ([1988](#ong88), p. 78), writing is not just a tool for conveying ideas, but has changed the human mind and socio-technical practices more profoundly than any other human invention. In comparison to preliterate cultures, writing enables deeper critical reflection and argumentation.

The development of the printing press provided the means for mass duplication and dissemination of written documents. The traditional scholarly publishing model rested on the paper-based model, which dominated from the Middle Ages to the 1990’s. In essence, the paper-based model defined the structures and forms of scientific texts as monographs, serials and research papers.

Scholarly publishing developed tremendously during the printed era of scientific publishing ([Eisenstein, 2005](#eis05)). As printing technologies and book trade advanced, the publishing of research results became a business. During this era, it was mainly libraries that secured the access to scientific knowledge by collecting and buying works and serials into their collections. Access was provided for the local users, and a system for inter-library loans evolved at the end of the 19th century.

Although the paper format afforded copying and relatively effective distribution, it had a serious limitation. The available _space in top journals_ became precious because of publishing competition: many wanted to get published but only a few succeeded. Because of the lack of space in paper-based journals, there were limits on the number of accepted papers and on the number of words, figures and references in papers. This meant that filtering before publishing, i.e., the referee process and pre-filtering done by the main editor became especially important. The brand image of the publisher or the journal benefitted from the competition for space in the paper: the more competition and rejections of submitted manuscripts, the better. Bibliometric methods, like journal impact factors, supported the view that if one was able to overcome the barriers and pre-filters of the top journals, one was a prominent scholar and there was not so much need for in-depth assessment of one’s research results. Naturally, this was quite a cost-effective and seemingly objective way to assess the quality of one’s research.

The implementation of digital technologies disrupted the existing business model, which was based on the paper-based distribution of scholarly works. In _the first digital phase of scientific publishing_, paper-based publishing and printing processes were transferred into the digital realm. The most radical change of the first digital phase was that the efficiency of scientific document delivery started to increase exponentially, which meant that publishing businesses experienced exceptional success. In contrast to the predictions of scholars like Odlyzko ([1994](#odl94)), _commercial_ scientific publishing started to flourish: scientific documents and especially scholarly journals became an even more reliable and stable source of economic profit. Although many visionaries of digital publishing embraced open access, in practice the publishing industry started to utilize pay walls. License-based publishing models created a digital divide between those who could afford to pay for access and those who could not. In contrast to the paper-based model, the licenses often did not give permission for inter-library lending and, thus, the digital divide grew even further. The Mertonian ideal of scientific knowledge as a common good faced the harsh reality of the costs and economic interests of the world outside the ivory tower.

Even though publishing business developed very well in the first digital phase of scholarly publishing, many aspects of the paper-based model still remained the same. The space in top journals was seen as more precious than ever before even though virtually unlimited digital storage could make space considerations almost irrelevant. The traditional model of peer review was still dominant and journal-level metrics and impact factors were widely used. The brand of top journals and top publishers was more important than ever before. The big players in the publishing field obtained a kind of practical monopoly over scholarly publishing, which meant that they could demand constantly growing license costs for their services. According to critics, big publishers with their big deals held the whole research community as a hostage and the ransom for institutional or country-based access to scholarly output was too high.

The inertia for change is high in the scientific publishing world. In many ways, we still live the first digital phase of scholarly publishing. However, something new is happening and even the top publishers are now trying to integrate concepts like open access into their business models. We could call this _new_ with many words: _open science_ or _open scholarship_, even _science 2.0_. The most utopian visionaries of this new stress, as Odlyzko ([1994](#odl94)) did in the nineties, that we are now witnessing a renaissance of scientific knowledge creation and communication that could make Mertonian scientific communism possible. What characterizes this _new_, which is also called the fourth paradigm of science, is the possibility for more coordinated collaboration in data-intensive scientific environments, with high-speed networks and massive amounts of computing power ([Hey, Tansley and Tolle, 2009](#hey09); [Lynch, 2014](#lyn14), p. 12). As pointed out in Floridi ([2015](#flo15)), we are moving from the age of scarcity to the age of abundance. This move beyond paper is going to have significant consequences for many issues, including scholarly publishing.

When considered from the point of view of the scientific publishing industry, the business model of open science is not based on freedom but on manuscript processing charges, i.e., one has to pay for publishing his or her manuscripts openly. Hybrid journals are one aspect of this change: traditional subscription journals offer a possibility for the authors or their home institutions to pay for open access. This leads to _double dipping_, where the institutions pay twice for journal publishing; i.e., they cover both the subscription costs and the manuscript processing fees. Hybrid journals might be seen as an example of the post-digital publishing phase in which combinations of old and new media and various business models live side-by-side ([Cramer, 2013](#cra13)).

This _new_ we are witnessing in the scientific practices and publishing world may be called _post-digital scholarly publishing_. Even though the concept of post-digital scholarly publishing is not yet clearly defined, we claim that the culture of scientific knowledge creation is entering a new period in which it makes greater use of the affordances of digital media and their potential to move us into the age of abundance. Thus, one does not copy the paper-based document models into the digital realm, but starts to experiment with and utilize different enhanced issues or features that the world of bytes makes possible (cf. [Gradmann, 2014](#gra14); [Priem; 2013](#pri13)). This means that the paper-based model no longer serves as the archetype of scientific publishing, but researchers and publishers start to use digital technologies more profoundly in knowledge creation processes. In essence, we are moving beyond paper to a totally digital environment of scientific document production and dissemination.

We use the term _post-digital_ and distinguish between the first digital and post-digital phases of scholarly publishing in a similar way that Aylesworth ([2015](#ayl15)) does with modernity and postmodernity. According to him, postmodernism is not _‘an attack upon modernity or a complete departure from it’_ (para. 2), but rather _‘a continuation of modern thinking in another mode’_ (para 2). Similarly, and in a larger historical picture, both digital phases could be seen as a continuum in contrast to printed science. However, at present it is useful to make a distinction between these phases because post-digital scholarly publishing goes radically beyond the possibilities of paper-based media, and the first digital phase does not.

<table><caption>Table 2: Phases of scholarly publishing</caption>

<tbody>

<tr>

<th>Printed science</th>

<th>First digital phase of scholarly publishing</th>

<th>Post-digital scholarly publishing</th>

</tr>

<tr>

<td>printed documents</td>

<td>e-journal supplier, printed book warehousing</td>

<td>born digital documents & archives</td>

</tr>

<tr>

<td>storing and warehousing documents and collections</td>

<td>digitizing the printed</td>

<td>utilizing the affordances of digital media (age of abundance)</td>

</tr>

<tr>

<td>born of traditional bibliometrics</td>

<td>developing and using bibliometric methods</td>

<td>using bibliometrics and alternative metrics</td>

</tr>

<tr>

<td>postal services & storing documents</td>

<td>retrieving digital documents (e-mail attachments, browsers)</td>

<td>creating knowledge (totally digital workplace and work processes)</td>

</tr>

<tr>

<td>buying separate documents</td>

<td>buying services</td>

<td>co-creating services</td>

</tr>

</tbody>

</table>

Table 2 captures some aspects of the change from the printed era towards the post-digital characterized by born-digital documents and more so by collective knowledge creation practices. In the age of abundance, the space in journals is not as precious as it used to be. So-called mega journals like _PLOS ONE_ have made possible that which was impossible or infeasible in the paper-based model. _PLOS ONE_ is based on light peer review: if submitted manuscripts are technically and methodologically adequate, they are not excluded on the basis of lack of perceived importance or adherence to a scientific field. The _PLOS ONE_ online platform employs a _publish first, judge later_ methodology, with post-publication user discussion and rating features (i.e., paper-based alternative metrics, or _altmetrics_). This means that _PLOS ONE_ and its adherents can publish many more papers than traditional journals that are still emulating the paper-based model. In 2013, _PLOS ONE_ published 31,500 papers, which was 8,000 more than in 2012, and in June 2014 _PLOS ONE_ published its 100,000th paper.

In addition to mega journals, we can witness other post-digital publishing trends. In essence, the recent calls for moving away from the journal-based traditional metrics (like journal impact factor) and towards paper-level metrics utilizing download numbers, commenting applications and link track backs are indicators of the move towards post-digital scholarly publishing (e.g., the _San Francisco declaration on research assessment_, [American Society for Cell Biology, 2012](#ame12)). In contrast to traditional metrics, altmetrics measure the almost immediate impact of new scholarly publications on collective knowledge dissemination and creation ([Priem, 2013](#pri13)). Thus, perhaps the most radical change of the post-digital phase is the shrinking of the publishing timeframe: previously it may have taken months or even years to write, print and post documents. In post-digital scientific environments, scholarly knowledge can be created and used much faster. Furthermore, _citizen science_ and various crowdsourcing mechanisms allow for lay people to take part in the scientific data and knowledge creation from their home computers.

In addition to the light peer review of the mega journals, other kinds of more open and sometimes also faster peer reviewing processes are used. Furthermore, the increase of social media platforms for scholars (like [CiteULike](http://www.citeulike.org/), Connotea (discontinued in 2013), [Academia.edu](https://www.academia.edu/) and [ResearchGate](https://www.researchgate.net/)) is changing the scholarly landscape. Priem ([2013](#pri13), p. 438) argues that during the next decade _‘most scholars will join such networks, driven by both the value of improved networking and the fear of being left out of important conversations’_. The process of knowledge creation can become even more social and collective as social media support the informal interaction of scholars across distances in more user-friendly and intuitive ways than, for example, e-mail. The digital realm could make Mertonian scientific sharing economy a reality, not just an ideal. But what is the standpoint of the scholarly publishing industry towards this utopian vision?

## Practices and business models for post-digital scholarly publishing

How will post-digital scientific knowledge creation and dissemination transform in the future? Will the researchers take digital knowledge creation tools into their own hands, with a strong commitment to co-operation and the total openness of the scientific process ([Assante _et al._, 2015](#ass15); [Priem, 2013](#pri13))? There are commercial interests involved in processing and mining, for example, of medical scientific data for the creation of new kinds of medicines and treatments. Because processing massive amounts of data for the purposes of text mining can be commercially viable and scientific publishing has been enormously profitable in recent years, it seems unlikely that strong multinational publishing corporations would give up their position in the scholarly knowledge creation and communication value chain. What we have seen in recent years is that, for example, commercial actors are developing collaboration environments for researchers and providing new kinds of text and data mining tools, with accompanying licenses (see, e.g., [Elsevier, 2017](#els17); [Wiley, 2017](#wil17)). It is unlikely that the dilemma between scientific sharing and the capitalist publishing industry will fade away in the future.

For analytical purposes we divide scholarly communication into two sides or properties:

*   intellectual property – the immaterial results, their intellectual ownership and dissemination
*   material property – the material works and right to make and own copies of them

As we have stressed, the basic ethos of scholarly communication is that scientific outputs are a part of the common good and should be open to all. This allows the development of science as an intellectual endeavour, which is based on systematic and critical analysis. The material part of scholarly publishing involves managing the costs of the publishing, publication and dissemination processes and the long-term preservation and curation of scientific outputs. Both the immaterial and material sides of scholarly publishing are needed for scientific progress.

The commercial side of scientific document dissemination is concerned with information predominantly as a material object, or thing to be sold in the markets. The aim of the publishing industry to maximize profits could, in the future, be balanced with non-profit publishing organizations that operate with public money or manuscript processing charges and go for _gold_ open access. For example, Morrison ([2013](#mor13)) states that _‘there is more than enough revenue if library acquisition budgets were redeployed to fund an open access scholarly publishing system’_. Libraries and other non-profit organizations could make it possible to integrate scientific publishing more centrally into the digital lifecycle of scholarly activities. This development might even lead to blurring the distinction between research lifecycle and research publishing in Science 2.0 environments, as Assante _et al._ ([2015](#ass15)) and Priem ([2013](#pri13)) envision.

There are also suggestions that the re-allocation of costs could transform the scholarly publishing process and even increase its efficiency. Leslie ([2005](#les05), pp. 412-413) proposes that the author should pay fees for the journals to fund the referees’ work: _‘economists advocate the merits of prices, rather than queues, as a generally better allocation mechanism. If the price system works elsewhere, it might do a better job than **pro bono** peer review’_ ([Leslie, 2005](#les05), p. 412, emphasis added). Leslie does not talk about open access per se, but his suggestion might better fit the kind of scholarly publishing ecosystem that is based on manuscript processing charges rather than submissions. In addition, Cotton ([2013](#cot13)) studied the effect of submission fees on submission times in selected economics journals. It seems that the submission fee has a positive effect on the response time to the authors. He bases his study on the game theory model and suggests that: _‘relying too heavily on fees results in similar inefficiencies as relying too little on fees. A combination of moderate fees and delays provides the optimal screening of submissions and maximizes journal quality’_ ([Cotton, 2013](#cot13), p. 507).

There seems to be a need for a mixed model, merging publishing costs and open access to fulfil the needs of both scholarly work and the market economy, or the immaterial and material sides of scientific communication. This might mean, for example, that the publisher should not be allowed to own the whole scholarly communication value chain, and that the cost distribution in the scholarly publishing process might change from the present-day standard, i.e., institutional licences paid by university libraries.

The open access movement has helped the scientific community in disseminating journals to a broader audience, and at the same time it has opened a discussion about the profit and cost of scholarly publishing. Van Noorden ([2013](#van13), p. 427) argues that open access publishing seems to be more cost effective than traditional commercial publishing. Based on his analysis, the average price tag of commercial publishing is $3,500 – 4,000; publishers then make twenty to thirty percent profit, so the total cost approximates $5,000\. The cost to publish one paper in _BioMed Central_ or _PLOS ONE_ are usually between $1,350 – 2,250.

There is evidence that the impact and visibility of open access papers are greater than those of papers published behind a paywall ([Antelman, 2004](#ant04), p. 379; [Gargouri _et al._, 2010](#gar10); [Swan, 2010](#swa10)). According to Laakso and Björk ([2012](#laa12), pp. 8-9) open access is becoming the mainstream of scholarly publishing: the publishing models are already feasible and the Internet has _‘thoroughly rewritten the rules of the game’_. What remains to be seen is if the new actors (like mega journals) _‘will take over the market or if the old established actors, commercial and society publishers with subscription-based revenue models, will be able to adapt their business models and regain the ground they have so far lost’_ ([Laakso and Björk , 2012](#laa12), p. 9).

Important funding policy changes provide an additional reason for open access to become mainstream. The open access policies of major research funders like the National Institutes of Health, the National Science Foundation and the European Union make open access mandatory for researchers receiving financial support through these organs. Furthermore, the open access mandates of regions, countries, universities and research institutes have a great impact on how research outputs are published and archived.

<table><caption>Table 3: Three models of scientific publishing</caption>

<tbody>

<tr>

<th>Open, non-commercial publishing</th>

<th>Mixed-model scientific publishing</th>

<th>Commercial publishing based on licenses</th>

</tr>

<tr>

<td>communitarian</td>

<td>private-public partnership</td>

<td>commercial</td>

</tr>

<tr>

<td>independence</td>

<td>co-operation</td>

<td>dependence</td>

</tr>

<tr>

<td>open to all</td>

<td>open to all</td>

<td>open to those who have paid fees</td>

</tr>

<tr>

<td>costs covered from public funds</td>

<td>costs divided by all the actors involved</td>

<td>costs covered from subscriptions</td>

</tr>

</tbody>

</table>

In Table 3 we present three possible models for scholarly publishing. The prevalent scientific publishing model, i.e., the commercial license based model, seems to be slowly losing ground. It is likely that it will prevail in fields of study that rely heavily on the selling of printed books or monographs, such as the social sciences and humanities. However, the adoption of e-books could cause these fields to favour open access models. For example, the [Knowledge Unlatched](http://www.knowledgeunlatched.org/) project converts monographs into open access e-books. In this project, libraries pay processing charges for the scholarly monographs they would have bought anyway.

We are witnessing a movement towards open- or mixed-model publishing. By mixed-model we mean open access that uses some commercial methods to cover publishing and editing costs but does not aim to use the work of the scientific community to maximize profit. As early as 1999, John W. T. Smith ([1999](#smi99), p. 82) argued for a distributed scientific publishing model. According to Smith, this could be achieved by a collection of co-operating actors or agencies. This means that the focus is turned away from the publishers to the subject focal points, i.e., gateway-like services ([Smith, 1999](#smi99), p. 83) – undoubtedly like libraries – where access is free and open and helps users to find the relevant documents. Our ideal model of post-digital scientific publishing combines both the immaterial and material sides of science for opening scientific data and documents, and aims at fair publishing and long-time preservation in which the costs are shared by the whole community. The sustainability of future business models should be the primary concern of the movement towards open science, including taking care of the long-term preservation of the scientific data and documents. Long-term preservation is especially important for post-digital scholarly publishing and science: because of non-existent policies, practices and technologies we have already lost many scholarly born-digital documents published during the last decades.

## Conclusion

_‘Oh, East is East, and West is West, and never the twain shall meet’_, states the first line of Rudyard Kipling’s poem _The Ballad of East and West_, foregrounding the contrast between Eastern and Western lifestyles. As we have shown in the previous sections, this seemingly irreconcilable opposition also exists in the field of science communication: although science aims at openness and objectivity, scientific communication channels are mainly privately owned and subject to the laws of the business world.

Ideally, science should aim at complete open access to subject all research to the systematic critique of the whole scientific community. Science can be better advanced when all researchers have access to both scientific publications and the material from which they draw, i.e., research data. What stands in the way of this Mertonian kind of scientific utopia seems to be the publishing industry as a capitalist endeavour aiming at maximizing profits. Multinational publishing corporations are not willing to give up their place in the value chain of science communication without a fight. As hybrid journals and double dipping practices have shown, international academic publishers have partly integrated open access into their own business models to secure their substantial cash flow.

In Kipling’s poem, the Western thesis and the Eastern antithesis end up forming the synthesis of blood brotherhood. Similarly, reforming the practices of scientific publishing calls for a third way between the utopia of scientific communism and the practices of the capitalist publishing industry. New post-digital publication and knowledge creation means that would not be possible in the paper-based model of publishing scientific research must be developed. Organisation-specific and discipline-specific publication archives and other online services based on the principles of non-profit publishing, which advance the distribution of scientific knowledge, could help sow seeds for a bright future in scientific knowledge creation and communication. In addition to technological and social publishing innovations, we especially need a far-sighted science policy. The research funders and other scientific political actors should not drive scholars to knowledge creation practices that would cement the paper-based models of scientific publishing and excessively bolster the already strong status of large corporations. We need to see more public institutions as publishers guaranteeing the long-term preservation of scientific documents and data if the open science movement aims to fulfil its goals.

## <a id="author"></a>About the authors

**Jarmo Saarti** (PhD, 2000, University of Oulu, Finland and 2013, University of Jyväskylä, Finland) works as a Library Director of the University of Eastern Finland (UEF), Kuopio, Finland. He can be reached at [jarmo.saarti@uef.fi](mailto:jarmo.saarti@uef.fi).  
**Kimmo Tuominen** (PhD, 2001, University of Tampere) works as a University Librarian and Professor at the Helsinki University Library, Helsinki, Finland. He can be reached at [kimmo.tuominen@helsinki.fi](mailto:kimmo.tuominen@helsinki.fi).

</section>

<section>

## References

<ul>
<li id="ant04">Antelman, K. (2004). Do open-access articles have a greater research impact? <em>College & Research Libraries, 65</em>(5), 372–382.</li>
<li id="ass15">Assante, M., Candela, L, Castelli, D., Manghi, P. &amp; Pagano, P. (2015). <a href="http://www.webcitation.org/6Z87lbTx4">Science 2.0 repositories: time for a change in scholarly communication</a>. <em>D-Lib Magazine, 21</em>(1/2). Retrieved from http://www.dlib.org/dlib/january15/assante/01assante.html (Archived by WebCite® at http://www.webcitation.org/6Z87lbTx4)</li>
<li id="ayl15">Aylesworth, G.  (2015). <a href="http://www.webcitation.org/6ZCgWD9SK/">Postmodernism</a>. In E. N. Zalta (Ed.), <em>The Stanford Encyclopedia of Philosophy.</em>Retrieved from http://plato.stanford.edu/archives/spr2015/entries/postmodernism/ (Archived by WebCite® at http://www.webcitation.org/6ZCgWD9SK) </li>
<li id="bil88">Billig, M., Condor, S., Edwards, D., Gane, M, Middleton, D. &amp; Radley, A. (1988). <em>Ideological dilemmas: a social psychology of everyday thinking</em>. London: Sage.</li>
<li id="cot13">Cotton, C. (2013). Submission fees and response times in academic publishing. <em>American Economic Review, 103</em>(1), 501–509.</li>
<li id="cra13">Cramer, F, (2013). <a href="http://www.webcitation.org/6Z87ufyGy">What is ‘post-digital’?</a> <em>Post-digital research, 3</em>(1). Retrieved from http://www.aprja.net/?p=1318 (Archived by WebCite® at http://www.webcitation.org/6Z87ufyGy) </li>
<li id="eis05">Eisenstein, E. (2005). <em>The printing revolution in early modern Europe</em> (2nd ed.). Cambridge: Cambridge University Press.</li>
<li id="els17">Elsevier (2017). <em><a href="http://www.webcitation.org/6sYryTGwT">Text and data mining.</a></em> Retrieved from https://www.elsevier.com/about/open-science/research-data/text-and-data-mining (Archived by WebCite® at http://www.webcitation.org/6sYryTGwT) </li>
<li id="flo15">Floridi, L. (Ed.). (2015). <em>The online manifesto: being human in hyperconnected era</em>. Cham: Springer.</li>
<li id="gar10">Gargouri, Y., Hajjem, C., Larivière, V., Gingras, Y., Carr, L. &amp; Brody, T. (2010). <a href="http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0013636">Self-selected or mandated, Open Access increases citation impact for higher quality research</a>. <em>PLoS ONE, 5</em>(10). Retrieved from http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0013636 (Archived by WebCite® at http://www.webcitation.org/6Z89Yuydl)</li>
<li id="gib04">Gibson, C. &amp; Klocker, N. (2004). Academic publishing as ‘creative’ industry, and recent discourses of ‘creative economies’: some critical reflections. <em>Area, 4</em>(36), 423–434.</li>
<li id="gra14">Gradmann, S. (2014). From containers to content to context, the changing role of libraries in eScience and eScholarship. <em>Journal of Documentation, 70</em>(2), 241–260.</li>
<li id="hey09">Hey, T., Tansley, S. &amp; Tolle, K. (2009). <em><a href="http://www.webcitation.org/6Z8857Rm6">The fourth paradigm, data-intensive scientific discovery</a>. </em> Redmond, WA: Microsoft Corporation. Retrieved from http://research.microsoft.com/en-us/collaboration/fourthparadigm/4th_paradigm_book_complete_lr.pdf (Archived by WebCite® at http://www.webcitation.org/6Z8857Rm6) </li>
<li id="kli99">Kling, R. &amp; McKim, G. (1999). Scholarly communication and the continuum of electronic publishing. <em>Journal of the American Society for Information Science, 50</em>(10), 890–906.</li>
<li id="laa12">Laakso, M. &amp; Björk, B. (2012). <a href="http://www.webcitation.org/6Z88Lb3FV">Anatomy of open access publishing: a study of longitudinal development and internal structure</a>. <em>BMC Medicine, 10</em>(124). Retrieved from http://www.biomedcentral.com/1741-7015/10/124 (Archived by WebCite® at http://www.webcitation.org/6Z88Lb3FV)</li>
<li id="lan78">Lancaster, F.W. (1978). <em>Toward Paperless Information Systems</em>. New York, NY: Academic Press.</li>
<li id="lar15">Larivière, V., Haustein, S. &amp; Mongeon, P. (2015). <a href="http://www.webcitation.org/6ZgrQR4aD">The oligopoly of academic publishers in the digital era.</a> <em>PLoS ONE, 10</em>6), e0127502. Retrieved from http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0127502 (Archived by WebCite® at http://www.webcitation.org/6ZgrQR4aD) </li>
<li id="les05">Leslie, D. (2005). Are delays in academic publishing necessary? <em>The American Economic Review, 95</em>(1), 407-413.</li>
<li id="lic65">Licklider, J.C.R. (1965). <em>Libraries of the Future</em>. Cambridge, MA: MIT Press.</li>
<li id="lyn14">Lynch, C.A. (2014). <a href="http://www.webcitation.org/6Z88VSogT">The ‘digital’ scholarship disconnect</a>. <em>Educause Review, 49</em>(3), 10-15.  Retrieved from http://www.educause.edu/ero/article/digital-scholarship-disconnect (Archived by WebCite® at http://www.webcitation.org/6Z88VSogT)</li>
<li id="lyo84">Lyotard, J.-F. (1984). <em>The postmodern condition: a report on knowledge</em>. Minneapolis, MN: University of Minnesota Press.</li>
<li id="mer73">Merton, R.K. (1973). The normative structure of science. In N.W. Storer (Ed.), <em>The sociology of science: theoretical and empirical investigations</em>(pp. 267–278). Chicago, IL: The University of Chicago Press.</li>
<li id="mit74">Mitroff, I.I. (1974).Norms and counter-norms in a select group of the Apollo moon scientists: a case study of the ambivalence of scientists. <em>American Sociological Review, 39</em>(4), 579–595.</li>
<li id="mon11">Monbiot, G. (2011, August 29). <a href="http://www.webcitation.org/6Z88ZoHDx">Academic publishers make Murdoch look like a socialist</a>. <em>The Guardian</em>. Retrieved from http://www.theguardian.com/commentisfree/2011/aug/29/academic-publishers-murdoch-socialist (Archived by WebCite® at http://www.webcitation.org/6Z88ZoHDx)</li>
<li id="mor13">Morrison, H. (2013). <a href="http://www.webcitation.org/6Z89PqQm1">Economics of scholarly communication in transition</a>. <em>First Monday, 18</em>(6). Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/4370/3685. (Archived by WebCite® at http://www.webcitation.org/6Z89PqQm1)</li>
<li id="odl94">Odlyzko, A. (1994). <em><a href="http://www.webcitation.org/6Z88fbLka">Tragic loss or good riddance? The impending demise of traditional scholarly journals</a></em>. Retrieved from http://www.jucs.org/jucs_0_0/tragic_loss_or_good/Odlyzko_A.pdf (Archived by WebCite® at http://www.webcitation.org/6Z88fbLka) </li>
<li id="odl13">Odlyzko, A. (2013). <em><a href="http://www.webcitation.org/6Z88jGiwJ">Open access, library and publisher competition and the evolution of general commerce</a></em>. Retrieved from http://www.dtc.umn.edu/~odlyzko/doc/libpubcomp.pdf (Archived by WebCite® at http://www.webcitation.org/6Z88jGiwJ)</li>
<li id="ong88">Ong, W.J. (1988). <em>Orality and literacy: the technologizing of the word</em>. London: Routledge.</li>
<li id="pri13">Priem, J. (2013). <a href="http://webcitation.org/6Z9mG8km7">Scholarship: beyond the paper</a>. <em>Nature, 495</em>(7442), 437–440. Retrieved from http://www.nature.com/nature/journal/v495/n7442/full/495437a.html (Archived by WebCite® at http://www.webcitation.org/6Z9mG8km7)</li>
<li id="puu14">Puuska, H.--M. (2014). <em><a href="http://www.webcitation.org/6Z88mxtQ4">Scholarly publishing patterns in Finland: a comparison of disciplinary groups</a></em>. Tampere: Tampere University Press. Retrieved from https://tampub.uta.fi/handle/10024/95381 (Archived by WebCite® at http://www.webcitation.org/6Z88mxtQ4)</li>
<li id="rag13">Ragnedda, M. &amp; Muschert, G.W. (Eds.) (2013). <em>The digital divide: the Internet and social inequality in international perspective</em>. New York, NY: Routledge.</li>
<li id="ame12">American Society for Cell Biology. (2012). <em><a href="http://www.webcitation.org/6Z892GEkU">San Francisco declaration on research assessment</a></em>. Retrieved from http://am.ascb.org/dora/ (Archived by WebCite® at http://www.webcitation.org/6Z892GEkU)</li>
<li id="sha88">Shapin, S. (1988). Understanding the Merton thesis. <em>Isis, 79</em>4), 594–605.</li>
<li id="smi99">Smith, J.W.T. (1999). The deconstructed journal: a new model for academic publishing. <em>Learned Publishing, 12</em>(2), 79–91.</li>
<li id="swa10">Swan, A. (2010). <em><a href="http://www.webcitation.org/6Z897mB2J">The open access citation advantage: studies and results to date</a></em>. Southampton: School of Electronics and Computer Science, University of Southampton. Retrieved from http://eprints.soton.ac.uk/268516/ (Archived by WebCite® at http://www.webcitation.org/6Z897mB2J)</li>
<li id="tuo05">Tuominen, K., Savolainen, R. &amp; Talja, S. (2005). Information literacy as a socio-technical practice. <em>The Library Quarterly, 75</em>(3), 329–345.</li>
<li id="van13">Van Noorden, R. (2013). <a href="http://www.webcitation.org/6Z89D2tkz">Open access: the true cost of science publishing</a>. <em>Nature, 495</em>(7442), 426–429. Retrieved from http://www.nature.com/news/open-access-the-true-cost-of-science-publishing-1.12676 (Archived by WebCite® at http://www.webcitation.org/6Z89D2tkz)</li>
<li id="wil17">Wiley (2017). <em><a href="http://www.webcitation.org/6sYrjSHbY">Text and data mining policy</a></em>. Retrieved from http://olabout.wiley.com/WileyCDA/Section/id-829771.html (Archived by WebCite® at http://www.webcitation.org/6sYrjSHbY) </li>
</ul>

</section>

</article>