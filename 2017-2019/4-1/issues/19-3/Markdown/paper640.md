<header>

#### vol. 19 no. 3, September, 2014

</header>

<article>

# Student search behaviour in an online public access catalogue: an examination of 'searching mental models' and 'searcher self-concept'

#### [Rebekah Willson](#author1) and [Lisa M. Given](#author2)  
Charles Sturt University, School of Information Studies, Wagga Wagga, Australia

#### Abstract

> **Introduction.** This paper presents a qualitative exploration of university students' experience of searching an online public access catalogue. The study investigated how students conceptualise their searching process, as well as how students understand themselves as seekers of information.  
> **Method.** Following a search task, thirty-eight university students were interviewed using a qualitative, semi-structured interview design. The interviews explored students' experience of searching, conceptualised aspects of their searches, their information seeking strategies, confidence in searching, and any difficulties encountered.  
> **Analysis.** The interviews were analysed using a grounded theory approach. The analysis involved iterative review and constant comparison of the transcripts, including line-by-line open coding followed by a second round of focused coding.  
> **Results.** The results of the project present an emergent theory that explores a set of conceptual patterns in students' searching mental model of online systems, a typology of searchers' perceptions of their information retrieval skills (i.e., their searcher self-concept), and categorisation of types of searchers.  
> **Conclusion.** With increased knowledge of how students conceptualise their search process and view themselves as seekers of information, educators and information professionals can work more effectively with students to search for the literature of their disciplines. Similarly, system designers can devise interfaces that suit students' needs.

<section>

## Introduction

University students search for information in increasingly complex environments, requiring them to navigate multiple information systems and use of a range of information sources. In exploring users' understanding of information systems, many researchers have used mental models (a limited, internal representation of an external system, object, or process that is developed through interaction and serves a functional purpose) as a way to conceptualise users' knowledge ([Holman, 2011](#hol11); [Slone, 2002](#slo02); [Westbrook, 2006](#wes06); [Zhang, 2008a](#zha08a), [2008b](#zha08b), [2013](#zha13)). However, few studies have explored how a searcher's self-concept influences his/her approach to information seeking in digital environments. This paper presents the results of qualitative interviews with undergraduate students engaged in searching tasks, where the students' mental models are explored in the context of their self-concept as searchers.

While mental models are limited to systems, self-concept (i.e., a cognitive organizational structure that categorises information about oneself) is broader in its application. As individuals act on the world, and reflect and gain feedback on that action, they develop a personal self-concept ([Huitt, 2004](#hui04)). Self-concept has a long history in personality psychology research, including global (overall self-concept) and multidimensional (self-concept related to specific areas) personality traits, yet has not been systematically explored within information science. As students engage in university study they begin to develop their understanding of their academic abilities: their academic self-concept. Through continuous self-reflection on their abilities, students also develop self-efficacy, or a belief in their capability to exert influence over their lives through their actions ([Bandura, 1994](#ban94)). Mental representations are important to consider in studies of information seeking and retrieval, as these representations affect behaviour, with implications for students' academic information needs, as well as system design and information literacy. Further, the affect of academic self-concept on students' academic outcomes, including academic achievement, has been well-documented (e.g., [Marsh, 2008](#mar08a); [Marsh and Martin, 2011](#mar11)).

The qualitative results presented in this paper emerged from a larger, mixed methods study that included a quantitative experiment to explore the effects of spelling errors on information retrieval in an online library catalogue ([Willson and Given, 2010](#wil10)). The qualitative phase of the project explored university students' concept of themselves as searchers, in order to contextualise the searching tasks conducted in the experiment. While types of academic self-concept have been studied extensively in the field of psychology, research in library and information science has typically focused on cognitive structures and confidence levels, rather than on how students understand themselves as part of the information seeking process. This study explored how students conceptualise their searching process (including the shape of their searches and their information environments), as well as how students understand themselves as seekers of information (including personal agency in relation to technology and self-efficacy as a searcher). The motivation for this study was to contribute to the growing body knowledge about of how students conceptualise their search process and view themselves as seekers of information, focusing on students' voices. With this understanding, information professionals can work more effectively with students to search for the literature of their disciplines, and system designers can devise interfaces that suit students' needs.

## Literature review

This literature review will examine the online information behaviour of students to provide a background of how students seek information online using library systems, as well as mental models that are formed through interaction with systems that impact online searching. While mental models have been researched to understand mental representations of information systems, little has been done to explore representations of the self as a searcher. Self-concept, including academic self-concept and the related topic of self-efficacy, will be explored as mental representations and beliefs about oneself that are formed through interaction with the environment. Academic self-concept, and its precursor self-efficacy, are discussed as areas that may influence students' library catalogue searching.

### Overview of student online information behaviour

Information behaviour incorporates a multitude of activities and interactions, including environments, personalities, motivations, emotions, technology and culture. People seek information to reduce uncertainty about, and make sense of, the world ([Case, 2012](#cas12)). The motivations for finding information include objective motivations (to solve specific problems or make decisions) or subjective motivations (where information is needed to address missing knowledge or reduce anxiety) ([Case, 2012](#cas12)). Emotions are also closely linked to individuals' subjective experiences ([Nahl, 2007](#nah07)). With so much complexity, information behaviour research must address multiple perspectives.

Students have specific requirements for information gathering imposed by their instructors, where the student has not selected the topic and may know little of the background needed to understand the relevant literature. Gross ([1995](#gro95)) describes this imposed query as an information need given to someone else to satisfy. Not only are students expected to interpret the query, they must gather materials, evaluate relevance and properly cite what they use. Students must also navigate myriad proprietary databases alongside the library's catalogue. So while students may have a lot of experience seeking information online using a variety of different tools, often their assignments require online information seeking using specific resources.

Online searching is difficult for many students (e.g., [Griffiths and Brophy, 2005](#gri05)) who typically use rudimentary search strategies. Malliari and Kyriaki-Manessi ([2007](#mal07)) examined library catalogue transaction logs and found users employ few advanced search features. Other library catalogue researchers have noted short queries, simple keyword searches and rare use of Boolean ([Lau and Goh, 2006](#lau06); [Novotny, 2004](#nov04)). Library catalogues were originally designed for expert searchers, but these still require advanced knowledge of the types identified by Borgman ([1996](#bor96)): conceptual knowledge of information retrieval; semantic knowledge search implementation; and, technical knowledge about skills and syntax. Unfortunately, students may not understand these key elements.

Personal characteristics also influence students' searching ([Heinström, 2005](#hei05)). Novotny ([2004](#nov04)) found that students with particular domain knowledge had more successful searches than those with no background. Tabatabai and Shore ([2005](#tab05)) found that students with more years of university completed had more successful searches. Affect also has a major influence on students' behaviour, including how and what they search, and how they judge their search results (e.g., [Given, 2007](#giv07); [Parker and Berryman, 2007](#par07)).

Students also search for information in their personal lives, often with different requirements for the quality, source and extent of the information retrieved than for academic work. Students' personal searching using systems external to the library (e.g., Google) has an impact on library-related search strategies and their expectations of library catalogues ([Novotny, 2004](#nov04)). For example, a student may presume that a keyword search in the library catalogue will function in the same way as a keyword search in Google, even though both systems have unique features that may not apply universally. While there are similarities between search engines and library catalogues, the latter are more complex systems; the advanced knowledge and skill needed to construct effective search strategies in library catalogues (as well as proprietary databases) have resulted in robust offerings of information literacy instruction sessions over the last few decades, as well as a large body of research on students' information literacy needs (e.g., [Ellis and Salisbury, 2004](#ell04); [Novotny, 2004](#nov04); [Patterson, 2009](#pat09); [Pinto, 2010](#pin10)).

### Mental models

Much of the existing research, however, does not acknowledge the link between students' own ideas about searching practices and their identities as searchers, which they have developed through the use of various search tools. These experiences create mental models of information and information systems, which are then applied when searching for library resources. Mental models have been defined in many ways, over many decades, particularly in psychology. Norman ([1983, p. 7](#nor83)) discussed mental models as internal representations resulting from interaction with the external environment, human beings, and technological artifacts. While rarely technically accurate, these models are functional, predictive, explanatory, and evolving; however, they are limited by an individual's technical background, previous experiences, and the structure of the information processing system. Zhang ([2008b, p. 2087](#zha08b)) notes that library and information science researchers typically define mental models as ‘_people's mental representation of information objects, information systems, and other information related processes_', Westbrook ([2006](#wes06)) identified Doyle and Ford's (1998) definition of mental models as being the most appropriate for information studies. Their definition states that a

> mental model of a dynamic system is a relatively enduring and accessible, but limited, internal conceptual representation of an external system whose structure maintains the perceived structure of that system (Doyle and Ford, 1998, as cited in [Westbrook, 2006, p. 565](#wes06)).

From these definitions a mental model can be viewed as a limited, internal representation of an external system, object, or process that is developed through interaction and serves a functional purpose.

In studying undergraduates' mental models of the Web, Zhang ([2008b](#zha08b)) found that only a few students had no mental model; most had a mental model that they could articulate with varying degrees of sophistication. In a further study using MedlinePlus, Zhang ([2013](#zha13)) found that students have an archetypal mental model of systems, which they then modify to make a mental model of a specific system; a number of cognitive, emotional, and behavioural developmental processes occur with mental models as students interact with a system. Wang, Berry and Yang ([2003](#wan03)) describe users' experiences with search engines as informed mostly by trial and error, which may never lead to correct mental models that can be applied to other systems.

Mental models also have an impact on searching. Zhang ([2008a](#zha08a)) found four categories of mental models emerged in students' searching, which led to different performances and feelings as they used the Web. Those with different mental models varied in the amount of time they spent on searches and how search strategies were used. Slone ([2002](#slo02)) found that goals and mental models work together to determine search strategies and information sources used, as well as how long searches last, the number of resources used and browsing behaviours. Makri and colleagues ([2007](#mak07)) found that masters students used their understanding of search engines to infer how to search digital libraries, however, a lack of understanding about access restrictions and the system's search functions led to risk-averse and trial-and-error behaviour, respectively. Holman ([2011](#hol11)), when looking at first-year undergraduate students' views of the relationship between keywords and results, found that those with more definite mental models used more complex search strategies, though few students were successful in their searching. Similarly, Brandt and Uden ([2003](#bra03)) assert that without clear mental models of search engines, novices are likely to fail at information gathering.

### Self-concept

#### Academic self-concept

Self-concept is _‘a cognitive schema that organizes abstract and concrete memories about the self and controls the processing of self-relevant information_' ([Campbell, 1990](#cam90), p. 539). Self-concept has been explored extensively in the field of personality psychology as both a global (stable) trait and as a multidimensional (multi-component) trait. As self-concept is formed through experience and interpretation of the environment, it is affected by reinforcements, evaluations of others, and attributions of one's own behaviour ([Marsh and O'Mara, 2008](#mar08b)); it is useful, as it can explain and predict actions ([Shavelson, Hubner, and Stanton, 1976](#sha76)).

A multidimensional view of self-concept is not mutually exclusive with a global trait view ([Marsh and O'Mara, 2008](#mar08b)); however, self-concept can vary according to context. Academic self-concept has been studied extensively using a multi-dimensional hierarchical model. Academic self-concept is related to academic achievement ([Marsh and Martin, 2011](#mar11)) and content-specific components of academic self-concept have correlated to academic achievement in the same content area ([Marsh, 1992](#mar92)). In addition to academic achievement, academic self-concept is also related to other educational outcomes such as course selection and educational aspirations ([Marsh, 2008](#mar08a)). As academic achievement is related to academic self-concept, self-concept becomes an important aspect to understanding individuals' academic lives.

While there is some research on how aspects of self-concept affect online searching (e.g., [Hupfer and Detlor, 2006](#hup06)), there has been no discussion of the information seeking dimension of academic self-concept. How individuals view themselves as searchers for or users of information is a gap in the current discussion of academic self-concept. Information behaviour is prompted by individuals' needs and those needs exist within the context of the person, the person's roles and the environment ([Wilson, 2000](#wil00)). Existing within the context of the person means that the whole person, including self-concept, affects one's information seeking behaviour. In fact, Franken (as cited in [Huitt, 2004, para. 3](#hui04)) states,

> [T]here is a great deal of research which shows that the self-concept is, perhaps, the basis for all motivated behavior. It is the self-concept that gives rise to possible selves, and it is possible selves that create the motivation for behavior.

Examining individuals' fundamental beliefs about themselves is an important part of the person-centered approach to information behaviour research.

#### Self-efficacy

Related to self-concept is self-efficacy. Self-efficacy is defined as ‘_people's beliefs about their capabilities to produce designated levels of performance that exercise influence over events that affect their lives_' ([Bandura, 1994, p. 71](#ban94)). Pajares ([2002](#paj02)) states that social cognitive theory (including self-efficacy), is rooted in human agency, where individuals have self-beliefs that allow them to control their lives. Though information on the self comes from many sources, self-efficacy depends on cognitive processing of that information. In using information, individuals determine the extent to which they believe they have mastery over their lives. While academic self-concept and self-efficacy share many traits, they are separate constructs and self-efficacy is a precursor to academic self-concept ([Bong and Skaalvik, 2003](#bon03); [Ferla, Valcke and Cai, 2009](#fer09)).

Self-efficacy varies along the dimensions of magnitude, strength and generality ([Bandura, 1977](#ban77)). In magnitude, individuals' efficacy expectations may be limited to particular levels of task difficulty. In strength, individuals' expectations vary between strong and weak, affecting persistence in coping efforts. In generality, individuals' experiences may contribute to an overall sense of efficacy, beyond the specific situation, while others only pertain to certain circumstances. Fletcher ([2005](#fle05)) cites studies in the domains of academic performance, computing and Internet use, which demonstrate validity of self-efficacy levels in specific task domains, indicating that self-efficacy has both global and specific aspects.

Self-efficacy is explored in many studies on information seeking and computer use. Ren ([2000](#ren00)) defined self-efficacy as students' ability to feel capable of conducting electronic searches to retrieve relevant information, and noted higher levels of self-efficacy among frequent library database users. Ford, Miller and Moss ([2001](#for01)) linked Internet-based information retrieval failure on low self-efficacy: an inability to maintain control; an inability to find one's way; and, an inability to stay on target and avoid getting lost. Fletcher ([2005](#fle05)) cites evidence that self-efficacy is related to increased probabilities of individuals' success in academic and computing tasks. Self-efficacy is included in Wilson's model and is related to the model's activating mechanism through its association with risk/reward theory ([Wilson, 1997](#wil97)). Wilson ([1997, p. 563](#wil97)) quotes Bandura's assertion that ‘_the strength of people's convictions in their own effectiveness is likely to affect whether they will even try to cope with given situations_.' Investigating self-efficacy in relation to information behaviour is important to understanding the reasons searchers behave as they do and to understand searchers as individuals, with varying personality characteristics.

This literature review examined students' online information behaviour, mental models of online information systems, academic self-concept, and self-efficacy to examine how students search and the mental representations they make of systems and themselves. The next section will describe the design of the current study that examined how students conceptualise their search process and view themselves as information seekers.

## Method

This paper presents findings from qualitative interviews designed to explore university students' conceptions of their searching and search behaviour. This study was inspired by Varnhagen et al.'s Spelling and the Web ([2009](#var09)) project, which examined children's and university students' responses to misspellings when unable to retrieve desired information. Designed as a mixed methods study, combining an experimental design with grounded theory, this research included a pre-search checklist, an online search task, and a qualitative, semi-structured interview with each student participant. Findings from the experimental search tasks are reported separately ([Willson and Given, 2010](#wil10)); this paper focuses on the results from the qualitative interviews. A grounded theory approach was used for data analysis, resulting in an emergent set of findings related to students' mental models of searching and personal self-concepts. Details on the design of the qualitative phase of the research are presented here, followed by the study results.

### Participants

A variety of methods was used to recruit students, including posters, listservs, classroom announcements and introductory psychology participant pools. The convenience sample consisted of 38 students (6 graduates and 32 undergraduates). Students came from a range of academic disciplines, including Science (45%), Arts (18%), Engineering (13%), Education (11%), Physical Education (11%), and Open Studies (3%). There were 24 females (63%) and 14 males (37%), who ranged from 18-59 years of age (with a median age of 19.5 years). English was not the native language of several students; also, one student had significant vision impairment. Ethics approval was obtained from the university's research ethics board prior to data collection. Students were assigned pseudonyms to protect their confidentiality as participants in the study.

### Interviews and search tasks

Participants participated in a semi-structured, qualitative interview (which lasted up to 25 minutes) after completing the experimental phase of the study. This approach to study design used a ‘_concurrent nested strategy_' ([Creswell, 2003, p. 218](#cre03)), which gathers quantitative and qualitative data simultaneously. The experimental phase of the study included a pre-search checklist and a series of search tasks. The checklist obtained participants' demographic information, as well as their comfort using computers, level of experience in online searching, and confidence in search abilities (using Likert scales). Participants were separated into two groups, the _easy spelling group_ and the _difficult spelling group_, and given search tasks that consisted of four separate searches for specified search terms in the university library's catalogue. The level of spelling difficulty was the independent variable. The search terms that were given to the _easy spelling group_ were at approximately a 10th-grade or 15 to 16 year-old reading level, as determined by the Flesch-Kincaid reading level, and included: lemming, civilian, Bolivia, and Sigmund Freud. The search terms given to the _difficult spelling group_ came from lists of commonly misspelled words or are orthographically impossible in English and included: ptarmigan, millennium, Qatar, and Michel Foucault. (The results of the experimental phase are reported in [Willson and Given, 2010](#wil10).)

While the search tasks were used to examine search behaviour, interviews were used to discuss with students (a) their experience with the search tasks, (b) how they conceptualise the components of their searches, and (c) how they evaluate their actions, particularly related to problem solving. The semi-structured interview questions were created using research tools from the literature that explored Web searching ([Slone, 2000](#slo00); [Varnhagen et al., 2009](#var09)) along with new questions designed for this study. The questions were used to follow up with participants about how spelling affected their searching and particular actions they took during the search tasks, and to explore previous experiences in students' searching. The interviews provided a rich dataset for all 38 participants, as these explored participants' confidence in their searching, any task-related problems, their experience in searching the library catalogue, and whether spelling affected their searches. In addition to the interview questions, aspects of the participants' searches that were noted during the search task were discussed with participants. The interviews and search tasks were recorded using Camtasia, a screen-capture program that also records audio.

### Analysis

The interviews were transcribed verbatim and then analyzed using a grounded theory approach where the researchers reserved a priori judgments about the data and looked for emergent concepts through a close reading of participants' words. The analysis involved an iterative review of the transcripts and use of the constant comparison method. In the open coding procedure that followed initial, line-by-line coding, descriptive terms were used to categorise what was happening in the data and short analytic labels were used ([Charmaz, 2001](#cha01); [Charmaz and Bryant, 2008](#cha08)). A second round of focused coding was then undertaken where the most frequently appearing initial codes were used to sort and synthesise the data ([Charmaz, 2001](#cha01)). These concepts were then amalgamated into overarching categories, from which the resulting themes emerged; these are outlined in the sections that follow. In this way participants' responses were examined in an exploratory manner. With an emergent qualitative research approach, the literature was used at the analysis stage to illuminate the findings coming from the participants. The themes that emerged were analysed through literature from psychology and library and information science, particularly self-concept, self-efficacy and mental models of searching.

## Results and discussion

The data provide emergent, exploratory findings that point to two major themes relating to students' searching behaviour and attitudes, as informed by several data categories: _searching mental model_ (including search shape and students' conception of information) and _searcher self-concept_ (including relation to technology and self-efficacy, as well as searcher type). _Searching mental model_ was the code used to describe aspects of the participants' discussion as mental conceptions of students' searching came out through the interviews. _Searcher self-concept_ was the code used to describe discussions of students' self attributes as a searcher, leading to a robust exploration of searcher types across the set of participants. The first author has a background in psychology and an understanding of the themes that emerged, which are psychological in nature, were influenced by literature in the field of psychology. The discussion that follows explores all of these elements, with figures included as visual representations of the analysis.

### Searching mental model

#### Search shape

The phrase _search shape_ is used to describe the students' conception of their search progress and was represented by broadened, narrowed and interlaced shapes. Described in very visual terms, all participants stated whether less or more information was retrieved and whether that information was closer to or further away from their topic. Their statements about search shape depend on their conception of information and how it is organized, as well as their internal motivation to find quality resources. In particular, students discussed the relatedness of one topic to another, i.e., broader, narrower and related terms, when discussing search shape and as their search conceptions moved from less to more definite.

_Broadened_. Here, the shape of a search begins with a specific idea or keyword (e.g., from an assignment) and expands to include more information. Once a wider array of information is retrieved, students evaluate that information against their information need. Broadening is undertaken by some students to ensure that they have not missed any information that might have been excluded using a narrower search. It is rarely used as a final search shape, but often used during the student's initial exploration. Some students were unsure of how to broaden, stating that they simply added more terms to their search to get more results (i.e., indicating that some students were unaware of the effects of their actions). The idea of a Broadened Search Shape is illustrated by first-year undergraduate student Han's discussion:

> If I'm looking up the name of some kind of drug …if I sort of know what it does …I can sort of go…one step above it [to] find something. Like, suppose the drug is an analgesic [I'll search and] see if that comes up.

_Narrowed_. Here, the search begins with a broad exploration of the topic; once a better understanding of the topic is gained, a more focused search is made to increase precision. Some students rely on technology to narrow the information retrieved, while others prefer to narrow the results themselves. Many participants discussed this narrowed search shape. Jason, a 19 year-old undergraduate science student, discussed his approach with search terms:

> I go to advanced search and then you usually – it narrows it down because you can combine phrases and words. So you can look for particular things you're interested in.

Arianna, a 24 year-old open studies student, discussed her use of specific library catalogue features:

> I searched for a specific topic and I narrowed it down, the language, to English and then I narrowed it down to Subject instead of Any field, so I think I pretty much narrowed down the subject and then there's quite a lot of – the search that I need is actually in there.

'

_Interlaced_. Here, the search shape is repeatedly broadened and narrowed as the topic becomes more defined. Students who preferred an interlacing approach to searching discussed their search attempts as several searches within a larger search, i.e., the exploration process as a series of broadening and narrowing (see Figure 1), rather than simply going in one direction. Here, initial searches inform later searches and work together as part of a complex process. The interlaced search shape can take place within one search system or across many systems. Eunice, a mature student, discussed her search as a process rather than a single search. Eunice's search exemplifies an interlaced search shape:

> A lot of times I've end[ed] up with zero items, but, you know, I go back and broaden the net and go at it again.

<figure>

![Figure 1:  Interlaced search shape](../p640fig1.jpg)

<figcaption>Figure 1: Interlaced search shape</figcaption>

</figure>

The interlaced search shape examines searching as a series of broadening and narrowing searches that inform subsequent searching. Rather than a repetition of modified searches, which come closer to the desired search (and which tend to be explored in many studies of search behaviour, including [Shute and Smith, 1993](#shu93); [Wildemuth, 2004](#wil04)), interlaced searching is not linear. The results from the broadened and narrowed searches give feedback to the searchers, informing them about topics, how topics are connected, offering possible wording choices, exploring information available on topics, and how they might change their search the better to access information. Interlacing is a more complex and robust approach to the searching activity than is typically explored in information science studies of searching practices. Similar to Bates's ([1989](#bat89)) evolving search (discussed with the berrypicking model), an interlaced search is continually modified, using feedback from previous searching: a series of broadened and narrowed searches used to answer a particular query. To modify a search effectively (e.g., broaden, narrow or change focus), a searcher must have some understanding of the trajectory of their search. Thatcher ([2006](#tha06)) discusses search strategies such as broad first in which general terms are first used or safe player in which specific terms are used first. Searchers first formulate a query and then examine the results to determine if the information need is met. If the need is not met the search results are used as feedback to modify the searches. Thatcher focuses on specific search strategies in an online search environment, while Bates' berrypicking focuses on the overall search process. The interlaced search strategy that emerged in this study is unique in that it specifies a particular type of searching within the overall search process.

#### Conception of information

As students progress as searchers and information users, their conceptions about information develop and change. In the students' interviews, the ways that information relates to specific academic topics, as well as the specifics of design and use of information systems, were discussed. Though students may not be cognizant of their own information conceptions, these do affect search behaviour. Conceptions of information can be at the topic or system level; they can vary in their level of complexity and abstraction and may be modified through search activities.

_Information relation_. This view is the understanding of information as it relates to a particular topic, including factual knowledge about the topic, vocabulary and inter-relatedness with other topics (see Figure 2). Factual knowledge of the topic is a starting place for many students. As students become more experienced in a knowledge space, they begin to understand that the vocabulary used can vary and that the language used to describe the topic is important to searching. Liam, a second-year undergraduate student, used his topic knowledge of Qatar to find add words to his search to find its correct spelling:

> And then for the Qatar, I thought I knew how to spell it so I didn't think I needed to go anywhere else but nothing was turning up, so I had to go to the other search engines and put in other related words like ‘Middle East' or something like that.

Students also see how topics interconnect to broader, narrower and related topics.

Taya, a fourth-year graduate student, discusses the importance of categories to her search, indicating the importance of knowing how information is related within the organizational system:

> Some of the topics [for courses] were so broad, some were so narrow [so] I had to make sure I found the right category, that the site grouped them with, so I can search properly. So it was all about, for me, learning about how they categorised it.

<figure>

![Composite of information relation from several participants](../p640fig2.jpg)

<figcaption>Figure 2: Composite of information relation from several participants - understanding how on a topic such as Qatar contains multiple aspects and it is related to other topics></figcaption>

</figure>

Figure 2 shows the movement between broader and narrower topics; having information about one topic can lead to information about the broader or narrower subject. It also demonstrates how facts can be entry points into the topic itself. Knowing something about Qatar, for example, helped students find the correct spelling of the word and further information on the topic. Some students searched for Qatar (with various spellings) and Middle East. Knowing that Qatar is located in the Middle East gave students access to the information about the topic that they were unable to find by searching with the misspelled country name. Knowing facts about the country then gave students some context and further understanding of the topic and what was related to the topic (e.g., knowing that Qatar is in the Middle East and Saudi Arabia is in the Middle East helps searchers to understand the topic and its context).

_Information systems_. Understanding the organization of information systems can come from having knowledge of a particular information system (e.g., the university library's catalogue) or understanding the overall framework of a general type of information system (e.g., online public access catalogues). Inexperienced students may not see the difference between online resources; however students do see a difference between print and online resources (see Figure 3). Over time, and with experience, an understanding develops of how different systems work and what actions can be taken to achieve desired results. Liam, a second year undergraduate science student, discussed his understanding of search engines and how he can use different resources for different purposes:

> [T]hroughout school I've learned to use different search engines and which one's better and which one's too broad and which one narrows it down more, or gives you     suggestions for other searches, like Ask Jeeves.

Students may or may not understand how different information systems are related and how they form an overall framework. Students may over-generalise knowledge of one system by trying to apply it to another. Understanding how proprietary databases, library catalogues and the World Wide Web are related and how they are different (including how those differences will determine how that information should be accessed and used) is a complicated process. While many students referred to the library catalogue, databases and the World Wide Web as ‘the Web' or ‘online', some students understood that online information comes from many different sources. Jeremy, a third-year undergraduate engineering student, discussed the library catalogue searching he did for the study in comparison to the world of information:

> Well, I don't think that I could say that I came close to all of the topics, on anything that I was searching for in the library. And I mean that's just the library resources,     too. There would be Web pages about lemmings and Freud and…

<figure>

![Figure 3: Example of information systems](../p640fig3.jpg)

<figcaption>Figure 3: Example of information systems – how many students perceive the organization of the overall framework of information.</figcaption>

</figure>

The largest box in Figure 3 represents the entire world of information including all formats. The offline information world box (small dashes) represents information found in physical formats; typically, students do not have difficulty distinguishing between these. The format and the paratext provided by that format present students with information needed to make distinctions. The series of smaller boxes within the figure represents online information. Many searchers do not make distinctions between the different aspects of the online information world (a database, a search engine or a Website). The move toward making online information seamless (e.g., discovery tools) and the increasing numbers of online resources (e.g., databases include conference proceedings, magazines, newspapers, etc.) make it difficult for students to understand what they are accessing. This can make it difficult to determine if the source relevant.

Understanding how searchers conceive of how information organization is important to understanding how people search. For example, if searchers know how major and minor subject headings are structured, they will understand the vocabulary used to describe a work and that the minor subject headings can be used in combination with other subject headings to search for specific aspects of the work. Bates ([1989](#bat89)) discusses change in search queries, snoting that learning more about the topic or how the topic is organized while seeking information changes the path of the search. An evolving search is based on the idea that the search changes as new resources are found and new understanding is made of the topic. Bates also discusses the importance of helping searchers to create a mental representation of the information system. Savolainen ([2002](#sav02)) discusses the importance of knowledge and skills in turning inherent abilities into competence once action is taken. Specifically, knowing how networked information sources are organized is a requirement for network competency. While conception of information (i.e., understanding how information is organized) has been acknowledged as important to searching, few models of how this affects searching have been proposed.

This section described the mental models students developed of their searching. The next section describes the mental representations students developed of themselves as searchers.

### Searcher self-concept

#### Relation to technology

As searching for academic and personal information is heavily dependent on technology, the lines between searching and technology use can become blurred. Many students expressed a preference for particular types of technology or sources, indicating that they are (becoming) familiar with these. As students gain experience, they change their understanding of how technology works and of their own uses of that technology. Technology is what allows the cognitive understanding of the information environment to interact with that information environment. Students position themselves in relation to technology in many ways, including technology use, technology reliance and technology expectation.

_Technology use_. Students described the technologies they used, their levels of confidence and their motivations, providing insight into whether technology was used as intended or in creative ways. While related to students' knowledge of technology, the concept of Technology Use examines the varied actions students take with technology. Richard, a fourth-year undergraduate student, describes using online searches in a very straightforward manner using keywords:

> Well, if you know about it you can put more words that you know that relate it. But, well, in a sense it also works the other way too because if you know exactly what it is you don't need too, sort of poke with a stick you just put in the right keyword and you can find it. So you can't say generally how you search depending on how well you know, but it does change the way you find it, though.

Stella, a first-year undergraduate student who is familiar with online searching uses one of the sources she frequently uses in a novel way:

> I often use Google as a dictionary. Well, at least for spelling, just because I find if you use an online dictionary you have to type it in correctly spelled, pretty much, before you get it.

_Technology reliance_. This view explores how much students see technology as the actor in the situation, ranging from relying on technology as a guide or friend that performs the search, to seeing technology as a tool to be wielded. Three participants, all discussing Google, showed a progression from technology reliance to technology use. Huang (a novice searcher, reliant on technology) had this to say:

> Yeah, Google's my best friend. Google and Wikipedia.

Rather than complete reliance, Han, who searches the Internet everyday, describes services Google provides as a convenience:

> Like sometimes with Google you just type it in wrong and then it comes up with a link that just takes you to there so you don't have to type it in again. It's pretty convenient.

June, an experienced searcher, uses Google as a tool that provides more information in her decision-making process:

> Google's good for that because you can spell it sort of correct and it'll tell you, it'll give you one or two forms and you can read the descriptions – this is completely wrong and this one's right.

_Technology expectation_. This view examines students' expectations of technological features based on their needs, past experiences and understanding of systems. Technological expectations range from specific ideas to solve specific problems (practical expectations) to hopes for the future where technology would perform more tasks (idyllic expectations). Andrea, an 18 year-old biological sciences major, wanted the library catalogue to provide a summary of the books contents:

> The catalogue, the library catalogue. They don't have just a general summary. That would help a lot if they had summarized some of the books and what they had in them.

Expectations of technology were often expressed as frustrations with the current situation and a desire for technology to behave differently. Cory, a 20 year-old drama major, commented:

> And l also I was saying about the Amazon.com, where you can see inside the books. That would be quite an interesting feature for some of them because it's so frustrating sometimes when you go all the way up to the fifth floor, wherever, there's two sentences on it…

Searchers' views of how they interact with technology are important as so much searching is mediated by technology. Savolainen ([2002](#sav02)) discusses network competence as a part of information literacy with four requirements: awareness of networked resources and how they are organized; use of information technology tools; evaluation of information; and, computer-mediated communication. Network competence is similar to _relation to technology_ in that it examines searchers' understanding, use of and beliefs about technology, particularly online. Savolainen then discusses network competence within a social cognitive model in which perceived network competence is related to self-efficacy, outcome expectations, experiences of information seeking and affect. The model suggests that all these aspects are interconnected; information seeking is affected by searchers' perceived network competence as well as their perceived self-efficacy in relation to network competence.

#### Self-efficacy as a searcher

Self-concept consists of three parts: self-efficacy, self-esteem, and stability of the self ([Luhr, 2005](#luh05)). Students' sense of agency in the information seeking process was central to their self-concept. When examined as a whole, students' comments reflected their beliefs in their ability to exert influence over their searching. Self-efficacy emerged as a theme as students positioned themselves as agents of action or objects on which action is enacted. Ainsley, a first-year undergraduate student, stated:

> I can usually figure it out. I'll work for a while and do it from a lot of different angles so I normally get it.

Noah, a fourth year undergraduate student, discussed his experience looking for information for an assignment as a somewhat passive observer:

> ‘Cause like the other day I had to go and find some books and I had to go talk to the librarian, and I had like already tried searching and I had found a couple of books already and I knew where they were but they didn't relate really to what I was looking for. And then when I got her to search it for me, she was like typing in all these different symbols and I had no idea what she was doing …

Self-efficacy also varied in magnitude and strength. For example, students discussed aspects of their searching (e.g., how many pages they would review find information or how long they would spend on searching). Wadiah, a nineteen year-old biology student, provided this example:

> Like you know how you said the first one ptarmigan? That was like, “Maybe I'm spelling it right, maybe I'm not.” I wasn't even sure – until I'm sure, I satisfy myself. I would keep searching it. I don't care how long it would take me but I will make sure I find the right one.

Both self-concept and self-efficacy are constructed through processing information based on several information sources. Self-concept and self-efficacy are constructed through self-reflection. As its inclusion in Wilson's ([2000](#wil00)) model indicates, self-efficacy is a useful concept when examining information behaviour. Han, a first-year undergraduate, knows there are library resources that are best to use but does not have a strategy to find them:

> Well usually if I get a whole bunch of results I sort of just get lazy and I just look on the first page and find 2 or 3 that seem to fit and I go from there. And if I need more information then I can search something more specifically.

### Searcher types

When examining the themes related to searcher self-concept, three types of searchers emerged: the _scattered searcher_, the _settling searcher_ and the _shrewd searcher_.

_Scattered searchers_ are disorganized, inexperienced and come to searching without a plan. Their emotions may be desperate or indifferent, unsure or uncaring. They also tend to be nervous about using technology, unsure of what it does or how to wield it. Melanie, a second-year undergraduate student, when asked how she searches for unfamiliar information said,

> Yeah, then I phone my boyfriend, ‘cause he can navigate the Internet much better than I can. Hung, a first-year undergraduate student, is one example. When asked about how he is able to find information on unfamiliar topics he responded, It's kind of a toss, it's pretty much based on luck, really.

_Settling searchers_ are repetitious searchers. They tend to have experience but that experience dictates their search strategy. Settling searchers will use the same resources and the same strategies from successful past searches, even if those resources or strategies are inappropriate for the current task. Corrina, an 18 year-old undergraduate arts student, demonstrates a reliance on top-ranked results:

> Yeah, if I see something, say, on the first page that I know will help I'll just – I'll usually if I go to it and it's helpful, I'll take it and I won't spend more time looking for other stuff that might be a little more helpful if that has all the information I need then that's good enough for me.

They may be overly reliant on technology to do the work of finding information. These searchers are comforted by familiarity and overly confident about the value of their usual resources. Carrie, a 20 year-old undergraduate science student, illustrates not only a preference for using a familiar resource but a resistance to using what is less familiar or more difficult to use:

> I'm not a big fan of going into the library catalogue. I'm not going to lie. Usually whenever we have to use if for a lab or a paper, I'm like, 'Aww, really? Do we have to? It has to be peer-reviewed and we can't just Google it?'

_Shrewd searchers_ are adaptive. They have a fair amount of experience that they adapt to different situations. They will try a mix of resources and strategies, as appropriate for the situation, and use feedback from searching to inform next steps. Sunil, a 22 year-old graduate student, discussed how he would use different sources to explore unfamiliar topics:

> So basically, I mean, I think I should think about if this [is] something not familiar to my field, I will actually first spend some time on Wikipedia or something like that and maybe read that article or find more and then they will have some references for articles on that topic, so I probably come back to the Website and find if there is anything that they have on the Website.

They are confident and see technology as a tool to be used to accomplish information goals, rather than a panacea. Shrewd searchers tend to have a better conception of information systems and can use these more adaptively. June, a 26 year-old graduate student, talked about the different types of information you can get from different sources and how she uses these effectively to meet her needs:

> I think what I do a lot when I'm journal article searching is I like to go through the references at the back, so that's another source that you don't get when you're on the Internet, in the books of course you don't get that, whereas if you can download the journal article you can find a lot of great sources just by looking at what they sourced.

Searchers have been discussed in many ways. In earlier research it was often systems, rather than searchers, that were studied (see [Case, 2012](#cas12)). This system-oriented view of information behaviour loses the complexity of what searchers actually do, focusing on what searchers do in constrained situations with specific, formal information resources rather than naturalistic search behaviour.

### Searching mental model and searcher self-concept

In discussing their experiences, students' _searching mental models_ (their conceptions of the information world) and their _searcher self-concept_ (their conceptions of themselves acting in the information world) emerged. Part of the student search behaviour discussed in the interviews could be classified under _search shape_, _conception of information_ or _relation to technology_. These were the conceptual models students used to describe the information environment. The other parts of student search behaviour discussion could be classified under _relation to technology_ and _self-efficacy_. These were how students described their personal interaction with and feeling about searching for information, and how the students conceived of themselves as searchers. Discussions of searching ranged from very certain, with clearly delineated aspects of behaviour and knowledge, to very tentative, with limited behaviours and a vague sense of understanding. Experienced searchers, with more education, discuss their searches clearly, indicating more defined mental models and conception of self as a searcher.

As the focus of the interviews was on academic information searching, _searcher self-concept_ was conceived as a content-specific dimension of academic self-concept and the discussions centered on students' academic searches. The multidimensional hierarchical model is useful in understanding this concept, as this self-perception is part of a larger dimension (academic self-concept), which in turn is part of the overarching self-concept. The proposed _searcher self-concept_ is important to individuals' searching for academic information, yet is only one small part of individuals' conception of themselves. Understanding an individual's self-concept as it relates to a specific task is important to understanding the person-in-context.

## Conclusion

Two major themes emerged from the interviews with university students about their searching. Searching Mental Model was discussed as containing both students' perceptions of the shape of their search and their conception of how information is organized and works within systems. In library catalogue-user studies search activities have typically been characterised in terms of discrete actions, with few examining the overarching mental models of those actions. This study examined those overarching mental models. The contribution that this study makes is the proposal of _searcher self-concept_ as a specific type of academic self-concept, relating to students' view of their agency in relation to technology and their self-efficacy as a searcher. In addition, this study also proposes three types of searchers: _scattered_, _settling_, and _shrewd_. As self-concept may be the basis for all motivated behaviour, this helps us to understand why searchers seek information in the ways they do. The results discussed here, while limited to this study, give empirical evidence that further exploration is warranted on searcher self-concept. Academic self-concept has been a rich area of study in which many specific areas of content have been confirmed. The idea of an academic self-concept should also be considered in relation to Wilson's revised general model of information seeking behaviour, especially in relation to Wilson's person-in-context. It should be remembered, however, that the searcher self-concept proposed here is by no means exhaustive, and is only a preliminary exploration.

As research in information seeking behaviour continues to focus on the user, rather than the system, more work should be done in the areas of how academic self-concept and self-efficacy affect information seeking and use. University students' lack of self-reflection in their searching and their lack of awareness of their own online searching patterns is a lost opportunity for their intellectual growth. Understanding how students conceptualise information systems and how they see themselves in relation to technology has curricular and pedagogical implications for information literacy instruction, as well as implications for information systems design. This study also gives evidence for using self-efficacy to examine library catalogue-related information seeking behaviour, supporting findings in existing information seeking behaviour literature.

### Implications and future directions

Research on mental models has implications for information systems design. As Westbrook ([2006](#wes06)) demonstrates, understanding users' mental models of systems provides great opportunities for Web designers to understand how users think and what suits their needs. In addition to information systems, this work has pedagogical implications for information literacy instruction. Instructors can help students understand how they view themselves as searchers, an essential step in helping students develop their skills. Within the classroom educators can help provide scaffolding to students (a technique involving a teacher giving individual support to a student, which is gradually removed as the student is able to work more independently), providing learning experiences with searching to increase their understanding of information systems and resources. Students often lack self-reflection about their searching. In bringing students' attention to aspects of their searching such as search shape and their conception of information, their mental models of the information systems they work with can be influenced, which in turn which impact how they interact with those systems. Students' understanding of systems comes from previous experience but it also comes from knowledge about the system, providing an opportunity for educators to increase the accuracy of mental models. Searcher Self-Concept, including students' interaction with technology and self-efficacy, is related to the agency and locus of control students feel about their searching. In guiding students' interactions with information systems, it is possible to show students not only how systems work but also how to control the system.

Following this work there are opportunities for future research. From the interview data searching mental model emerged and searcher self-concept was proposed. The concept of searcher self-concept should be further explored to determine what constituent components make up this proposed construct. Both mental models and self-concept are mental representations that are constructed through interaction with the external world and influence future exploits. As there are similarities between the two and they both emerged as major themes, how they are related to one another should be examined. In addition, the influence of searcher self-concept on searching should be examined. As self-concept influences outcomes (e.g., academic self-concept has been linked to academic achievement), searcher self-concept should be investigated for its impact on information-seeking behaviour.

## Acknowledgements

The authors acknowledge the help of Dr. Connie Varnhagen and her laboratory for the use of equipment and expertise, Dr. Lynn Westbrook for her useful suggestions, as well as the Social Sciences and Humanities Research Council of Canada for their support (Award No. 766-2007-0415).

## About the authors

<a id="author1">**Rebekah Willson**</a> is a PhD candidate in the School of Information Studies at Charles Sturt University. Her dissertation research is on the information behaviour of early career academics in Australia and Canada as they transition from doctoral studies to their first full-time academic position. She can be contacted at [rwillson@csu.edu.au](mailto:rwillson@csu.edu.au)  
**<a id="author2">Lisa M. Given</a>** is Professor of Information Studies (School of Information Studies), Acting Associate Dean, Research (Faculty of Education) and a member of the Research Institute for Professional Practice, Learning and Education (RIPPLE) at Charles Sturt University. Her research interests include information behaviour, Web usability, design of library space, qualitative research methods, and information use in the context of higher education. She can be contacted at [lgiven@csu.edu.au](mailto:lgiven@csu.edu.au)

</section>

<section>

## References

<ul>
<li id="ban94">Bandura, A. (1994). <a href="http://www.webcitation.org/6RjfNGVrs">Self-efficacy</a>. In V. S. Ramachaudran (Ed.), <em>Encyclopedia of human  behavior</em> (Vol. 4, pp. 71-81). New York, NY: Academic Press.
Retrieved from http://www.uky.edu/~eushe2/Bandura/BanEncy.html  (Archived by WebCite&reg; at http://www.webcitation.org/6RjfNGVrs)</li>

<li id="ban77">Bandura, A. (1977). Self-efficacy: toward a unifying theory of behavioral change. <em>Psychological  Review, 84</em>(2), 191–215. </li>

<li id="bat89">Bates, M. J. (1989). The design of browsing and berrypicking techniques for the online search interface. <em>Online  Review, 13</em>(5), 407–424.</li>

<li id="bon03">Bong, M., &amp; Skaalvik, E. M. (2003). Academic self-concept and self-efficacy: How different are they really? <em>Educational Psychology Review, 15</em>(1), 1–40.</li>

<li id="bor86">Borgman, C. L. (1986). Why are online catalogs hard to use? Lessons learned from information-retrieval studies. <em>Journal of the American Society for Information Science, 37</em>(6), 387–400. </li>

<li id="bor96">Borgman, C. L. (1996). Why are online catalogs still hard to use? <em>Journal of the American Society for Information Science, 47</em>(7), 493–503.</li>

<li id="bra03">Brandt, D., &amp; Uden, L. (2003). Insight into mental models of novice Internet searchers. <em>Communications of the ACM, 46</em>(7), 133–137.</li>

<li id="cam90">Campbell, J. D. (1990). Self-esteem and clarity of the self-concept. <em>Journal of Personality &amp; Social Psychology, 59</em>(3), 538–549.</li>

<li id="cas12">Case, D. O. (2012). <em>Looking for information: a survey of research on information seeking, needs, and behavior</em> (3rd ed.). Bingley, UK: Emerald.</li>

<li id="cha01">Charmaz, K. (2001). Qualitative interviewing and grounded theory analysis. In <em>Handbook of interview research</em> (pp. 675–695). Thousand Oaks, CA: SAGE
Publications, Inc. doi:10.4135/9781412973588</li>

<li id="cha08">Charmaz, K., &amp; Bryant, A. (2008). Grounded theory. In <em>The SAGE encyclopedia of qualitative research methods</em>. Thousand Oaks, CA: Sage Publications.</li>

<li id="cre03">Creswell, J. W. (2003). <em>Research design: qualitative, quantitative, and mixed method approaches</em> (2nd ed.). Thousand Oaks, CA: SAGE.</li>

<li id="dew96">Dewdney, P., &amp; Michell, G. (1996). Oranges and peaches: understanding communication accidents in the reference interview. <em>RQ, 35</em>(4), 520.</li>

<li id="ell04">Ellis, J., &amp; Salisbury, F. (2004). Information literacy milestone: building upon prior knowledge of first-year students. <em>Australian Library Journal, 53</em>(4),
383-396.</li>

<li id="fer09">Ferla, J., Valcke, M., &amp; Cai, Y. (2009). Academic self-efficacy and academic self-concept: reconsidering structural relationships. <em>Learning and Individual Differences, 19</em>(4), 499–505.</li>

<li id="fle05">Fletcher, K. M. (2005). Self-efficacy as an evaluation measure for programs in support of online learning literacies for undergraduates. <em>Internet and Higher
Education, 8</em>, 307–322.</li>

<li id="for01">Ford, N., Miller, D., &amp; Moss, N. (2001). The role of individual differences in Internet searching: an empirical study. <em>Journal of the American Society for
Information Science and Technology, 52</em>(12), 1049–1066.</li>

<li id="giv07">Given, L. M. (2007). Emotional entanglements on the university campus: the role of affect in undergraduates' information behaviors. In D. Nahl &amp; D. Bilal (Eds.), <em>Information and emotion: the emergent affective paradigm in information behavior research and theory</em> (pp. 161–176). Medford, NJ: Information Today.</li>

<li id="gri05">Griffiths, J. R., &amp; Brophy, P. (2005). Student searching behavior and the Web: use of academic resources and Google. <em>Library Trends, 53</em>(4), 539–554. </li>

<li id="gro95">Gross, M. (1995). The imposed query. <em>RQ, 35</em>(2), 236–243.</li>

<li id="hei05">Heinström, J. (2005). Fast surfing, broad scanning and deep diving: the influence of personality and study approach on students' information-seeking behavior. <em>Journal
of Documentation, 61</em>(2), 228–247.</li>

<li id="hol11">Holman, L. (2011). Millennial students' mental models of search: implications for academic librarians and database developers. <em>The Journal of Academic Librarianship, 37</em>(1), 19–27.</li>

<li id="hui04">Huitt, W. (2004). <a href="http://www.webcitation.org/6RjfRZdAH">Self-concept and self-esteem</a>. <em>Educational psychology interactive</em>. Valdosta, GA: Valdosta State University. Retrieved from http://www.edpsycinteractive.org/topics/self/self.html (Archived by WebCite&reg; at http://www.webcitation.org/6RjfRZdAH)</li>

<li id="hup06">Hupfer, M. E., &amp; Detlor, B. (2006). Gender and Web information seeking: a self-concept orientation model. <em>Journal of the American Society for Information Science and Technology, 57</em>(8), 1105–1115. </li>

<li>Kuhlthau, C. C., Maniotes, L. K., &amp; Caspari, A. K. (2007).  <em>Guided inquiry: learning in the 21st century</em>. Westport, CT: Libraries Unlimited.</li>

<li id="lau06">Lau, E. P., &amp; Goh, D. H.-L. (2006). In search of query patterns: a case study of a university OPAC. <em>Information Processing &amp; Management, 42</em>(5), 1316–1329.</li>

<li id="luh05">Luhr, M. E. (2005). Self-concept and efficacy. In S. W. Lee (Ed.), <em>Encyclopedia of school psychology</em> (pp. 491–492). Thousand Oaks, CA: Sage Publications. </li>

<li id="mak07">Makri, S., Blandford, A., Gow, J., Rimmer, J., Warwick, C., &amp; Buchanan, G. (2007). A library or just another information resource? A case study of users' mental models of traditional and digital libraries. <em>Journal of the American Society for Information Science &amp; Technology, 58</em>(3), 433–445.</li>

<li id="mal07">Malliari, A., &amp; Kyriaki-Manessi, D. (2007). Users' behaviour patterns in academic libraries' OPACs: a multivariate statistical analysis. <em>New Library World, 108</em>(3),
107–122.</li>

<li id="mar07">Markey, K. (2007). Twenty-five years of end-user searching. Part 1: research findings. <em>Journal of the  American Society for Information Science and Technology, 58</em>(8), 1071–1081.</li>

<li id="mar08a">Marsh, H. W. (2008). A multidimensional, hierarchical model of self-concept: an important facet of personality. In G. J. Boyle, G. Matthews, &amp; D. H. Saklofske (Eds.), <em>Sage handbook of personality theory and assessment: personality theories and models</em> (Volume 1). (pp. 447–469). Los Angeles, CA: Sage Publications.</li>

<li id="mar92">Marsh, H. W. (1992). <em>The content specificity of relations between academic self-concept and achievement: an extension of the Marsh/Shavelson model.</em> Paper presented at the Annual Meeting of the American Educational Research Association, San Francisco, CA, April 14-20, 1992. (ERIC No.: ED349315)</li>

<li id="mar11">Marsh, H. W., &amp; Martin, A. J. (2011). Academic self-concept and academic achievement: relations and causal ordering. <em>The British Journal of Educational
Psychology, 81</em>, 59–77. </li>

<li id="mar08b">Marsh, H. W., &amp; O'Mara, A. (2008). Reciprocal effects between academic self-concept, self-esteem, achievement, and attainment over seven adolescent years: unidimensional and multidimensional perspectives of self-concept. <em>Personality and Social Psychology Bulletin, 34</em>(4), 542–552.</li>

<li id="nah07">Nahl, D. (2007). The centrality of the affective in information behavior. In D. Nahl &amp; D. Bilal (Eds.), <em>Information and emotion: the emergent affective paradigm in information behavior research and theory</em> (pp. 3–38). Medford, NJ: Information Today.</li>

<li id="nor83">Norman, D. A. (1983). Some observations on mental models. In D. Genter &amp; A. L. Stevens (Eds.), <em>Mental models</em> (pp. 7–14). Hillsdale, NJ: Lawrence Erlbaum.</li>

<li id="nov04">Novotny, E. (2004). I don't think I click: a protocol analysis study of use of a library online catalog in the Internet age. <em>College &amp; Research Libraries, 65</em>(6), 525–537.</li>

<li id="paj02">Pajares, F. (2002). <a href="http://www.uky.edu/%7Eeushe2/Pajares/eff.html">Overview of social cognitive theory and of self-efficacy</a>. Lexington, KY: University of Kentucky. Retrieved from http://www.uky.edu/~eushe2/Pajares/eff.html (Archived by WebCite&reg; at http://www.webcitation.org/6RjfVFNcn) </li>

<li id="par07">Parker, N., &amp; Berryman, J. M. (2007). The role of affect in judging ‘What is enough?' In D Nahl &amp; D. Bilal (Eds.),<em> Information and emotion: the emergent
affective paradigm in information behavior research and  theory</em> (pp. 85–95). Medford, NJ: Information Today.</li>

<li id="pat09">Patterson, A. (2009). A needs analysis for information literacy provision for research: a case study in University College Dublin. <em>Journal of Information Literacy, 3</em>(1), 5-18.</li>

<li id="pin10">Pinto, M. (2010). Design of the IL-HUMASS survey on information literacy in higher education: a self-assessment approach. <em>Journal of Information Science, 36</em>(1),
86-103.</li>

<li id="ren00">Ren, W.-H. (2000). Library instruction and college student self-efficacy in electronic information searching. <em>The Journal of Academic Librarianship, 26</em>(5), 323.</li>

<li id="sav02">Savolainen, R. (2002). Network competence and information seeking on the Internet: from definitions towards a social cognitive model. <em>Journal of Documentation, 58</em>(2),
211–226.</li>

<li id="sha76">Shavelson, R. J., Hubner, J. J., &amp; Stanton, G. C. (1976). Self-concept: validation of construct interpretations. <em>Review of Educational Research, 46</em>(3), 407–441. </li>

<li id="shu93">Shute, S., &amp; Smith, P. (1993). Knowledge-based search tactics. <em>Information Processing &amp; Management, 29</em>(1), 29-45. </li>

<li id="slo02">Slone, D. J. (2002). The influence of mental models and goals on search patterns during Web interaction. <em>Journal of the American Society for Information Science and Technology, 53</em>(13), 1152–1169.</li>

<li id="slo00">Slone, D. J. (2000). Encounters with the OPAC: online searching in public libraries. <em>Journal of the American Society for Information Science, 51</em>(8), 757–773.</li>

<li id="son99">Sonnenwald, D. H., &amp; Iivonen, M. (1999). An integrated human information behavior research framework for information studies. <em>Library &amp; Information Science
Research, 21</em>(4), 429–457.</li>

<li id="tab05">Tabatabai, D., &amp; Shore, B. M. (2005). How experts and novices search the Web. <em>Library &amp; Information Science Research, 27</em>(2), 222–248.</li>

<li id="tha06">Thatcher, A. (2006). Information-seeking behaviours and cognitive search strategies in different search tasks on the WWW. <em>International Journal of Industrial
Ergonomics, 36</em>(12), 1055–1068.</li>

<li id="var09">Varnhagen, C. K., McFall, G. P., Figueredo, L., Takach, B. S., Daniels, J., &amp; Cuthbertson, H. (2009). Spelling and the Web. <em>Journal of Applied Developmental
Psychology, 30</em>(4), 454–462.</li>

<li id="wan03">Wang, P., Berry, M. W., &amp; Yang, Y. (2003). Mining longitudinal Web queries: trends and patterns. <em>Journal of the American Society for Information Science and Technology, 54</em>(8), 743–758. </li>

<li id="wes06">Westbrook, L. (2006). Mental models: a theoretical overview and preliminary study. <em>Journal of Information Science, 32</em>(6), 563–579. </li>

<li id="wil04">Wildemuth, B. (2004). The effects of domain knowledge on search tactic formulation. <em>Journal of the American Society for Information Science and Technology, 55</em>(3),
246-258.</li>

<li id="wil10">Willson, R., &amp; Given, L. M. (2010). The effect of spelling and retrieval system familiarity on search behavior in online public access catalogs: a mixed methods study. <em>Journal of the American Society for Information Science &amp; Technology, 61</em>(12), 2461–2476.</li>

<li id="wil00">Wilson, T. D. (2000). <a href="http://www.webcitation.org/6RkbKIivy">Human information behavior</a>. <em>Informing Science, 3</em>(2), 49–55. Retrieved from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf  (Archived by WebCite&reg; at http://www.webcitation.org/6RkbKIivy)</li>

<li id="wil97">Wilson, T. D. (1997). Information behaviour: an interdisciplinary perspective. <em>Information Processing &amp; Management, 33</em>(4), 551.</li>

<li id="zha13">Zhang, Y. (2013). The development of users' mental models of MedlinePlus in information searching. <em>Library &amp; Information Science Research, 35</em>(2), 159–170.</li>

<li id="zha08a">Zhang, Y. (2008a). The influence of mental models on undergraduate students' searching behavior on the Web. <em>Information Processing &amp; Management, 44</em>(3),
1330–1345. </li>

<li id="zha08b">Zhang, Y. (2008b). Undergraduate students' mental models of the Web as an information retrieval system. <em>Journal of the American Society for Information Science and Technology, 59</em>(13), 2087–2098.</li>
</ul>

</section>

</article>