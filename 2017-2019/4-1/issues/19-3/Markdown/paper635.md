<header>

#### vol. 19 no. 3, September, 2014

</header>

<article>

# An empirical analysis of mobile Internet acceptance in Chile

#### [Patricio Esteban Ramírez-Correa](#author)  
School of Business Sciences, Catholic University of the North, Coquimbo, Chile  
#### [Francisco Javier Rondán-Cataluña](#author), and [Jorge Arenas-Gaitán](#author)  
Department of Business Management and Marketing, University of Seville, Seville, Spain

#### Abstract

> **Introduction**. This paper examines the applicability of the unified theory of acceptance and use of technology, version 2, to mobile Internet acceptance in Chile. This theory is an extension of the unified theory of acceptance and use of technology model to a contrasting consumer context of technologically developing countries, such as Chile.  
> **Method**. An interview survey using a schedule based on constructs of individual acceptance was used to measure acceptance of mobile Internet by a quota sample of 501 users.  
> **Analysis**. The partial least squares technique was used to analyse the research model.  
> **Results**. The model explains the behavioural intention and the use behaviour in the adoption of mobile Internet in Chile. Specifically, 29% of the variable ‘increase in mobile Internet usage’ is explained by the variables habit, facilitating conditions, hedonic motivation, performance expectancy, price value and social influence.  
> **Conclusions**. The extended unified theory model is applicable to Chile.

## Introduction

The tempo and direction of the information systems discipline are set by the advanced economies of the world (Avgerou, 2008). Nonetheless, the need to understand whether theories developed in technologically advanced settings are applicable to technologically developing countries is an important issue for the discipline ([Nasco, Toledo and Mykytyn, 2008](#nas08)).

Although Chile is a technological leader in Latin America ([Everis..., 2012](#eve12)), it should not be considered as a technologically advanced country. It is still far from world technological leaders such as the Republic of Korea or Sweden ([International..., 2013](#int13)). Mobile phone penetration in Chile reached 95.7% of households in 2012\. This is well above the Latin American average of 79.1%, but far from markets such as Hong Kong, which has a mobile phone penetration of 229% ([Euromonitor International, 2013](#eur13)). Moreover, as a result of investments in infrastructure and the large number of smartphones operating in the country, Internet usage in Chile between 2007 and 2012 grew 63.8%, taking the population of Chileans using the Internet to 56.1%. This is fifteen percentage points above the average for Latin America, but still twenty percentage points below technologically advanced countries ([Euromonitor International, 2013](#eur13)). Forecasts indicate that Chile will continue to advance technologically. In particular, mobile Internet has had a remarkable growth in recent years and the Chilean Government estimates that the demand for mobile Internet services will grow to eighteen times its current size in the next four years ([Chile, 2012](#chi12)). Furthermore, cross-cultural studies including Chilean samples suggest that cultural distance is a moderator of information technologies’ acceptance ([Alshare, Mesak, Grandon and Badri, 2011](#als11); [Arenas-Gaitan, Ramirez-Correa and Rondan-Cataluna, 2011](#are11)).

In recent years there has been a dramatic expansion of Internet users which is expected to continue over coming years. According to the International Telecommunication Union ([2013](#int13)) estimates, there were 6.8 billion mobile-cellular subscriptions by the end of 2013\. Mobile Internet devices such as tablets and smartphones are one of the drivers of this spread of Internet access. In fact, the development of mobile technologies is changing the way users connect to the Internet and communicate interactively with others. Mobile Internet devices are used in various activities, including surfing the Internet, visiting social networking sites, using email, online gaming, reading reviews, geographic positioning and online shopping. Mobile Internet is much more common in advanced economies than in developing countries, with seventy-five per cent penetration in the former compared with twenty per cent in the latter. A major difference between developed and developing countries is that in developed countries mobile Internet is often a complement to, rather than a substitute for, fixed-broadband access. In developing countries, mobile broadband took off in 2010, and penetration rates will have increased from 4.4%to almost 20 % by the end of 2013\. Understanding how mobile Internet is adopted in emerging markets will help accelerate the reduction of the digital divide.

The individual-level adoption of mobile Internet has been a focus of research in recent years ([Cheong and Park, 2005](#che05); [Choi, Seol, Lee, Cho and Park 2008](#cho08); [de Reuver, Ongena, and Bouwman, 2013](#der13); [Gerpott, Thomas and Weichert, 2013](#ger13); [Hong, Thong and Tam, 2006](#hon06); [Hsu, Lu and Hsu, 2007](#hsu07); [Kim, Chan and Gupta, 2007](#kim07); [Koenigstorfer and Groeppel-Klein, 2012](#koe12); [Lee, 2009](#lee09); [Liu and Li, 2010](#liu10); [Pedersen, 2005](#ped05); [Thong, Hong and Tam, 2011](#tho11); [Venkatesh, Thong and Xu, 2012](#ven12); [West and Mace, 2010](#wes10)). For the most part, these studies extended or adjusted models of information technology acceptance that had been developed originally for organizational settings. However, in many cases, these models by themselves are not satisfactory to explain the phenomenon of technology acceptance from the consumers’ perspective ([Kim, _et al_., 2007](#kim07)). Recently, Venkatesh, _et al._ ([2012](#ven12)) proposed the unified theory of acceptance and use of technology, version two (hereafter, ‘the theory’ or ‘the unified theory’) to explain the acceptance of mobile Internet. The theory was built based on past extensions of the unified theory of acceptance and use of technology ([Venkatesh, Morris, Davis and Davis, 2003](#ven03)) in order to place particular emphasis on the context of consumer use. Because of the specific context of the orientation of the theory, it is sensitive to the characteristics of the consumer market. Therefore, the theory will be useful in consumer markets in technologically advanced countries, but is this approach appropriate to explain the phenomenon in less technologically advanced countries? ([Venkatesh _et al._, 2012](#ven12)). Consequently, the need to understand whether the theory applies to populations in technologically developing countries is pertinent.

Therefore, given its pioneering experience in consumer technologies and the manifest digital divide with regards to technologically advanced countries, we believe that Chile is a good place to study the applicability of the unified theory. In addition, given the proximity between the Chilean culture and the cultures of other Latin America countries, we consider that the results of this analysis can help to understand the phenomenon of acceptance of mobile Internet in this increasingly important region of the world. In this context, the main objective of this paper is to examine the applicability of the unified theory of acceptance and use of mobile Internet in Chile.

There are two main contributions of this study. First, it provides an empirical validation of a recently proposed model which explains the acceptance of technologies in the consumer context, examining a sample of users more varied than used by Venkatesh _et al._ ([2012](#ven12)), in relation to the proportionality of age and gender. Secondly, but equally important, it provides a unique analysis of the phenomenon of mobile Internet consumption in a technologically developing country and in the Latin American cultural context. The remainder of the paper is structured as follows. First, we present a literature review on the adoption of mobile Internet and of Chilean online consumer behaviour. Secondly, a research model is proposed and hypotheses are developed. Thirdly, the research method is described and study results are exhibited. Finally, we deliver a discussion, implications and conclusions.

## Literature review

### Adoption of mobile Internet

In the literature there are various approaches to research on the acceptance and use of mobile Internet. Some studies have applied the theory of diffusion of innovation ([Rogers, 2003](#rog03)), which understands innovation diffusion as the process by which an innovation is communicated through certain channels over time among members of a social system. In addition, the theory explains that in this social system there are several categories of adopters: innovators, early adopters, early majority, late majority and laggards. Uses of diffusion of innovation theory in this context are diverse: (i) as a basis for developing hypotheses or models that explain the use of mobile Internet ([de Reuver _et al._, 2013](#der13); [Hsu, Lu and Hsu, 2008](#hsu08); [Kim and Yang, 2012](#kim12); [Yamakami, 2006](#yam06)), (ii) as a theoretical element to discuss research findings ([Gerpott, 2011](#ger11); [West and Mace, 2010](#wes10)), or (iii) to classify mobile Internet users ([Okazaki, 2006](#oka06)).

Other studies have used models framed in the study of human behaviour such as the theory of reasoned action and the theory of planned behaviour ([Ajzen, 1991](#ajz91); [Ajzen and Fishbein, 1980](#ajz80)). According to these theories, an individual's behaviour is determined by the intention to perform that behaviour. This intention is a function of attitude and subjective norms, which are preceded by beliefs about the behaviour and normative beliefs, respectively. The attitude represents individual feelings, positive or negative, on the completion of a specific behaviour, while the intent describes the strength of purpose of performing a specific behaviour ([Ajzen and Fishbein, 1980](#ajz80)). Moreover, beliefs about behaviour refer to the positive or negative evaluation of realization of a particular conduct by an individual and normative beliefs represent individual perception to social pressures to perform or not perform comportments. The relative weights of these two types of beliefs can vary among individuals ([Ajzen and Fishbein, 1980](#ajz80)). In particular, in the context of mobile Internet usage, the theory of reasoned action and theory of planned behaviour have been used as a theoretical basis for research models ([Cheon, Lee, Crooks and Song, 2012](#che12); [Hsiao, 2013](#hsi13)), to integrate with others models ([Shin and Choo, 2012](#shi12b)) and to compare between countries ([Shin, 2012](#shi12a)).

A significant number of studies have used a model of information technology acceptance originally developed for organizational settings and widely validated, called the technology acceptance model ([Davis, 1989](#dav89)). The technology acceptance model is influenced by the conceptual framework established by the theory of reasoned action and the theory of planned behaviour, and its purpose is to explain the process of acceptance of information technology at an individual level. The technology acceptance model's primary focus is on two theoretical ideas, perceived usefulness and perceived ease of use, which are identified as key determinants to explain the user's behavioural intention to use a technological innovation, particularly, to accept or to reject information technology. The perceived ease of use describes the degree of ease associated with the use of technology, and the perceived usefulness indicates the degree to which a person believes that using the technology will help to achieve performance improvements. Specifically, the technology acceptance model indicates that individual intention to use information technology is a function of perceived ease of use and perceived usefulness. Furthermore, the model notes that perceived ease of use directly affects the perceived utility. Examples of the use of the technology acceptance model in the context of mobile Internet include using it directly as the research model ([Baek, Park and Lee, 2011](#bae11); [Coursaris and Sung, 2012](#cou12)), making extensions to the model ([Kim _et al._, 2007](#kim07); [Lee, 2009](#lee09); [Park and del Pobil, 2013](#par13a)) and integrating it with other models to develop a research model ([Lee and Chang, 2013](#lee13); [Zhou, 2011b](#zho11b)).

Additionally there is a group of studies that used a research approach based on versions two and three of the technology acceptance model. Version two ([Venkatesh and Davis, 2000](#ven00)) is a theoretical extension of the technology acceptance model that explains perceived usefulness and intention to use in terms of social influence (subjective norm, voluntariness and image) and cognitive instrumental processes (job relevance, output quality, result demonstrability and perceived ease of use). Version three ([Venkatesh and Bala, 2008](#ven08)) is an extension of version two and emerges from an extensive analysis of the use of the technology acceptance model. Version three describes a set of determinants of perceived usefulness. The use of these models in the context of mobile Internet adoption has usually been empirically validating some variables and relationships ([Abbasi, Chandio, Soomro and Shah, 2011](#abb11); [Vlachos, Giaglis, Lee and Vrechopoulos 2011](#vla11)) or integrating them with other models to propose a research model for the study (Leong _et al._, 2013a).

Another approach used in the study of the acceptance of mobile Internet is the unified theory of acceptance and use of technology ([Venkatesh _et al._, 2003](#ven03)). This is a model that attempts to comprehensively explain why individuals use information technology at their disposal. It was developed as a synthesis of research on the acceptance of technologies available. The unified theory of acceptance and use of technology proposes four constructs (performance expectancy, effort expectancy, social influence and facilitating conditions) that affect the intended or actual use of technology. In the context of mobile Internet, this model has been validated empirically ([Thong, Venkatesh, Xu, Hong and Tam, 2011](#tho11); [Wang and Wang, 2010](#wan10)) and integrated with other models ([Mardikyan, Besiroglu and Uzmaya, 2012](#mar12); [Park, Yang, and Lehto, 2007](#par07); [Zhou, Lu and Wang, 2010](#zho10)).

The most recent research approach used in mobile Internet is version two of the theory ([Venkatesh _et al._, 2012](#ven12)), which seeks to adapt the unified theory of use and acceptance of technology to a consumer context. Version two integrates three new variables to explain the intention to use technology: hedonic motivation, price/value and habit. The theory can be explained as follows. By increasing expectation of performance, effort expectancy, social influence, facilitating conditions, hedonic motivation, price/value or habit, intent to use will increase. Additionally, an increase in intent to use, facilitating conditions or habit will directly increase the use of technology. Because of the novelty of the theory, the current literature references are mostly associated with the variables that it shares with the unified theory of use and acceptance of technology ([Alwahaishi and Snasel, 2013](#alw13); [Chu, 2013](#chu13); [Gogus, Nistor, and Lerche, 2012](#gog12); Leong _et al._, 2013b; [Lin, Zimmer and Lee, 2013](#lin13); [Mantymaki and Salo, 2013](#man13); [Nistor, 2013](#nis13a); [Nistor, Gogus and Lerche, 2013](#nis13b); [Nistor, Schworm and Werner, 2012](#nis12)), and just a few studies use the new variables proposed ([Chong, 2013](#cho13); [Escobar-Rodríguez and Carvajal-Trujillo, 2013](#esc13); [LaRose, De Maagd, Chew, Tsai, Steinfield, Wildman and Bauer, 2012](#lar12)).

### Chilean online consumer behaviour

There are very few academic publications studying business in Latin America ([Donoso and Crittenden, 2008](#don08)). In particular, studies about consumers' Internet behaviour are very limited ([Andrews and Bianchi, 2013](#and13)). However, in the case of Chile it is possible to indicate some features. First, in 2012 the percentage of the population using the Internet reached 61.4%, mobile Internet usage is also common ([International..., 2013](#int13)). Studies suggest that while there is an inverse relationship between age and perception and use of the Internet, Chilean consumers attach value to information technology irrespective of age or sex ([Maldifassi and Canessa, 2009](#mal09)). In addition, and in relation to buying on the Internet, the attitude towards online shopping relates positively to the intention to buy online ([Andrews and Bianchi, 2013](#and13)).

Secondly, Chile is a collectivist culture ([Hofstede, 2001](#hof01)); therefore Chileans are more influenced by other consumers. A more collectivist culture could be the explanation for some research findings on Chilean online consumer behaviour: (i) result demonstrability in?uences consumer attitudes towards online purchasing ([Andrews and Bianchi, 2013](#and13)); and (ii) social identification is highly predictive of e-commerce adoption ([Arenas-Gaitán _et al._, 2013](#are13); [Grandón, Nasco, and Mykytyn, 2011](#gra11); [Nasco _et al._, 2008](#nas08)).

Thirdly, Chile is a culture with high uncertainty avoidance ([Hofstede, 2001](#hof01)); therefore Chileans request certainty in their daily life. Being a culture with a higher uncertainty avoidance could be the explanation for other research findings on Chilean online consumer behaviour: (i) perceived online risk negatively influences consumer attitudes towards online purchasing ([Bianchi and Andrews, 2012](#bia12)); (ii) compatibility with values, buying patterns and past experiences influence attitudes towards continuing online purchasing ([Andrews and Bianchi, 2013](#and13)); (iii) perceived behavioural control does not influence the intention to adopt e-commerce ([Grandón _et al._, 2011](#gra11); [Nasco _et al._, 2008](#nas08)); and (iv) Chilean users’ effort expectancy has no significant effect on the behavioural intention to use, or on the behaviour of online information technologies usage ([Al-Khateeb, 2007](#alk07); [Andrews and Bianchi, 2013](#and13); [Ramírez-Correa _et al._, 2013](#ram13)).

## Research model and hypotheses

### Research model

Figure 1 shows the research model based on the unified theory and the hypotheses of this study.

<figure>

![Figure1: Research model and hypotheses](../p635fig1.jpg)

<figcaption>Figure 1: Research model and hypotheses</figcaption>

</figure>

The research model aims to explain the acceptance of mobile Internet. In the model, use behaviour is explained, first, by the behavioural intention, as occurs in other contexts, and second, due to consumer context related to the phenomenon, by the facilitating conditions and habit. Moreover, behavioural intention is explained by two groups of antecedent variables. The first group is generally used in other contexts and it is composed of performance expectancy, effort expectancy and social influence. The second group is associated with the consumer context and is composed of hedonic motivation, price value , facilitating conditions and habit.

### Hypotheses

The unified theory has been applied to explain the acceptance of mobile Internet ([Mardikyan _et al._, 2012](#mar12); [Park and Kim, 2013](#par13b); [Shin and Choo, 2012](#shi12b); [Thong, _et al._, 2011](#tho11); [Wang and Wang, 2010](#wan10); [Zhou, 2011a](#zho11a)). Below we explain the hypotheses associated with this model.

_Behavioural intention_ is the degree to which a person has formulated conscious plans to perform or not perform some specified future behaviour ([Ajzen and Fishbein, 1980](#ajz80); [Davis, 1989](#dav89); [Venkatesh, _et al._, 2003](#ven03); [Venkatesh, _et al._, 2012](#ven12)). Several studies specify variables that explain the increase in behavioural intention to use mobile Internet.

Some studies indicate that _performance expectancy_, defined as the degree to which using a technology will provide benefits to individuals in performing certain activities ([Venkatesh, _et al._, 2003](#ven03); [Venkatesh, _et al._, 2012](#ven12)), affects behavioural intention in adopting mobile Internet ([Mardikyan _et al._, 2012](#mar12); [Park and Kim, 2013](#par13b); [Thong, _et al._, 2011](#tho11); [Wang and Wang, 2010](#wan10); [Zhou, 2011a](#zho11a)).

Also the literature suggests that _effort expectancy_, defined as the degree of ease associated with individuals’ use of technology ([Venkatesh, _et al.,_ 2003](#ven03); [Venkatesh, _et al._, 2012](#ven12)), affects behavioural intention in adopting mobile Internet ([Mardikyan _et al._, 2012](#mar12); [Thong, _et al._. 2011](#tho11); [Wang and Wang, 2010](#wan10)).

Furthermore, various studies point out that _social influence_, defined as the extent to which an individual perceives that people who are important to him believe he should use a particular technology ([Ajzen and Fishbein, 1980](#ajz80); [Venkatesh, _et al._, 2003](#ven03); [Venkatesh, _et al._, 2012](#ven12)) - affects behavioural intention in adopting mobile Internet ([Mardikyan _et al._, 2012](#mar12); [Shin and Choo, 2012](#shi12b); [Thong, _et al._, 2011](#tho11); [Wang and Wang, 2010](#wan10)).

Finally, it is indicated that facilitating conditions, defined as the individual’s perceptions of the resources and support available to behave ([Venkatesh, _et al._, 2003](#ven03); [Venkatesh, _et al._, 2012](#ven12)), affect use behaviour in adopting mobile Internet ([Zhou, _et al._, 2010](#zho10)).

Considering the importance of a replica in a technologically developing country and with different cultural context, and based on these previous studies, the following hypotheses are proposed:

> H1\. Performance expectancy is positively related to behavioural intention in the adoption of mobile Internet in Chile.  
> H2\. Effort expectancy is positively related to behavioural intention in the adoption of mobile Internet in Chile.  
> H3\. Social influence is positively related to behavioural intention in the adoption of mobile Internet in Chile.  
> H4\. Facilitating conditions are positively related to use behaviour in the adoption of mobile Internet in Chile.

The revised uniform theory has been applied to explain mobile Internet acceptance ([Venkatesh, _et al._, 2012](#ven12)). Below we explain the hypotheses associated with this model.

_Use behaviour_ is the frequency of information technology usage ([Davis, 1989](#dav89); [Venkatesh and Bala, 2008](#ven08); [Venkatesh and Davis, 2000](#ven00); [Venkatesh, _et al._, 2003](#ven03); [Venkatesh, _et al._, 2012](#ven12)), and it has been used as the final consequence in all the models of information technology acceptance. The literature indicates that behavioural intention explains use behaviour in adopting mobile Internet ([Venkatesh, _et al._, 2012](#ven12)).

Furthermore, the literature suggests that the _facilitating conditions_ explain the increase in behavioural intention to use mobile Internet ([Thong, _et al._, 2011](#tho11); [Venkatesh, _et al._, 2012](#ven12)). Several authors suggest that both behavioural intention and use behaviour in adopting mobile Internet are predicted by variables associated with the consumer context.

Some studies indicate that _hedonic motivation_, defined as the fun or pleasure derived from using a technology ([Venkatesh, _et al._, 2012](#ven12)), affects behavioural intention towards adopting mobile Internet ([Cheong and Park, 2005](#che05); [Liu and Li, 2010](#liu10); [Thong, _et al._, 2006](#tho06); [Venkatesh, _et al._, 2012](#ven12); [Yang, Lu, Gupta and Cao 2012](#yan12)).

Additionally, various studies suggest that _price value_, defined as the individuals’ cognitive trade-off between the perceived benefits of the applications and the monetary cost for using them ([Venkatesh, _et al._, 2012](#ven12)), affects behavioural intention towards adopting mobile Internet ([Cheong and Park, 2005](#che05); [Kim, _et al._, 2007](#kim07); [Mardikyan _et al._, 2012](#mar12); [Venkatesh, _et al._, 2012](#ven12); [Wang and Wang, 2010](#wan10)).

Finally, the literature indicates that habit - defined as the extent to which people tend to perform behaviours automatically because of learning ([Venkatesh, _et al._, 2012](#ven12)) - affects both behavioural intention towards adopting mobile Internet ([Venkatesh, _et al._, 2012](#ven12)) and use behaviour in the adoption of mobile Internet ([Kim, _et al._, 2010](#kim10)).

Considering the importance of a replica in a technologically developing country, and based on these previous studies, the following hypotheses are proposed:

> H5\. Behavioural intention is positively related to use behaviour in the adoption of mobile Internet in Chile.  
> H6\. Facilitating conditions are positively related to behavioural intention in the adoption of mobile Internet in Chile.  
> H7\. Hedonic motivation is positively related to behavioural intention in the adoption of mobile Internet in Chile.  
> H8\. Price value is positively related to behavioural intention in the adoption of mobile Internet in Chile.  
> H9\. Habit is positively related to behavioural intention in the adoption of mobile Internet in Chile.  
> H10\. Habit is positively related to use behaviour in the adoption of mobile Internet in Chile.

In addition, we will investigate the moderating effects proposed in the unified theory as we consider that there may be important differences with the results obtained in a developed market due to the cultural and technological gap. Specifically, we propose to explore the moderating effects of gender, age and experience in the adoption of mobile Internet in Chile.

## Research method

Empirical research was based on a non-random sampling method. Quota sampling was used to select participants based on age range and gender. Quotas were selected according to the profile of Chilean Internet users ([Ipsos, 2012](#ips12)). We chose this method because one advantage of quota sampling is that it provides sufficient statistical power to detect group differences. Data were collected in Chile in two major cities 900 kilometres apart through a face-to-face survey in November 2012\. The exclusion of invalid interview schedules provided a final sample size of 501 mobile Internet users, 266 males and 235 females. 60% of the participants had more than one year of experience using mobile Internet. Table 1 shows the survey sample.

<table><caption>Table 1: Survey sample description</caption>

<tbody>

<tr>

<th>Age range</th>

<th>% all</th>

<th>% male</th>

<th>% female</th>

</tr>

<tr>

<td>13-14</td>

<td>5.6</td>

<td>2.4</td>

<td>3.2</td>

</tr>

<tr>

<td>15-19</td>

<td>15.4</td>

<td>8.8</td>

<td>6.6</td>

</tr>

<tr>

<td>20-24</td>

<td>16.0</td>

<td>8.0</td>

<td>8.0</td>

</tr>

<tr>

<td>25-34</td>

<td>25.3</td>

<td>13.4</td>

<td>12.0</td>

</tr>

<tr>

<td>35-44</td>

<td>18.6</td>

<td>10.0</td>

<td>8.6</td>

</tr>

<tr>

<td>45-59</td>

<td>15.2</td>

<td>8.0</td>

<td>7.2</td>

</tr>

<tr>

<td>60 and older</td>

<td>4.0</td>

<td>2.6</td>

<td>1.4</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**100.0**</td>

<td>

**53.1**</td>

<td>

**46.9**</td>

</tr>

</tbody>

</table>

The measurement scales used were similar to those proposed by Venkatesh, et al., (2012). All items were measured using a seven-point Likert scale. Experience was measured in months and age was measured in years. Sex was codified to 0 (Female) and 1 (Male). The interviews were conducted in Spanish. Before the use of the schedule, it was translated from English to Spanish and then back to English to assure equivalence in translation, then a pilot test was conducted with 130 mobile Internet users. The results indicated the reliability of the scales (see appendix with measurement instrument).

The partial least squares path model approach to structural equation modelling was applied to test the research model ([Chin, 1998](#chi98); [Tenenhaus, Vinzi, Chatelin and Lauro, 2005](#ten05)). Specifically, SmartPLS 2.0 M3 software ([Ringle, Wende and Will, 2005](#rin05)) was used for model analysis.

## Results

A partial least squares path model is described by two models: (1) a measurement model relating the manifest variables to their own latent variables and (2) a structural model relating some endogenous latent variables to other latent variables.

### Measurement model analysis

Previous to analysing the structural model, the reliability and validity of the measurement model were measured. For use behaviour the measurement method used is formative, for all other variables the measurement method used is reflective. For each reflective latent variable, the individual reliability was assessed examining the loads (?) of the measures with their respective latent variable. The ? of all indicators should be 0.5 or above on their expected latent variable ([Hair, Anderson and Tatham, 1986](#hai86)) and they should be significant at least at the 0.05 level ([Fornell and Larcker, 1981](#for81)). The ? of indicators for the research model were mostly above 0.7 (all above 0.5) and they were significant at the 0.001 level. For use behaviour the variance inflation factor was calculated in order to confirm that there was no collinearity between the different indicators. In all cases the values remained below the recommended maximum value of 3.3 ([Cenfetelli and Bassellier, 2009](#cen09)). Table 2 shows the results from the cross-loading procedure.

<table><caption>Table 2: Loadings and cross-loadings for latent variables</caption>

<tbody>

<tr>

<th>Indicator</th>

<th>BI</th>

<th>PE</th>

<th>EE</th>

<th>SI</th>

<th>FC</th>

<th>HD</th>

<th>HT</th>

<th>PV</th>

<th>UB</th>

<th>p</th>

<th>VIF</th>

</tr>

<tr>

<td>Behavioural intention 1</td>

<td>

**0.89**</td>

<td>-0.02</td>

<td>0.06</td>

<td>-0.02</td>

<td>0.07</td>

<td>-0.04</td>

<td>-0.25</td>

<td>-0.01</td>

<td>0.03</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Behavioural intention 2</td>

<td>

**0.90**</td>

<td>-0.03</td>

<td>-0.06</td>

<td>0.04</td>

<td>-0.06</td>

<td>0.01</td>

<td>0.33</td>

<td>0.01</td>

<td>-0.06</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Behavioural intention 3</td>

<td>0.94</td>

<td>0.04</td>

<td>0.00</td>

<td>-0.02</td>

<td>-0.02</td>

<td>0.03</td>

<td>-0.08</td>

<td>0.00</td>

<td>0.02</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Performance expectancy 1</td>

<td>0.11</td>

<td>

**0.91**</td>

<td>0.11</td>

<td>-0.03</td>

<td>-0.01</td>

<td>0.09</td>

<td>-0.02</td>

<td>-0.05</td>

<td>0.03</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Performance expectancy 2</td>

<td>0.03</td>

<td>

**0.91**</td>

<td>0.00</td>

<td>-0.05</td>

<td>-0.02</td>

<td>0.09</td>

<td>-0.09</td>

<td>0.01</td>

<td>-0.01</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Performance expectancy 3</td>

<td>-0.16</td>

<td>

**0.82**</td>

<td>-0.12</td>

<td>0.09</td>

<td>0.04</td>

<td>-0.20</td>

<td>0.11</td>

<td>0.04</td>

<td>-0.02</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Effort expectancy 1</td>

<td>-0.09</td>

<td>0.06</td>

<td>

**0.93**</td>

<td>0.02</td>

<td>0.05</td>

<td>-0.02</td>

<td>0.01</td>

<td>-0.03</td>

<td>-0.01</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Effort expectancy 2</td>

<td>0.07</td>

<td>0.04</td>

<td>

**0.91**</td>

<td>-0.02</td>

<td>-0.06</td>

<td>0.00</td>

<td>-0.05</td>

<td>0.09</td>

<td>0.02</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Effort expectancy 3</td>

<td>-0.04</td>

<td>0.03</td>

<td>

**0.92**</td>

<td>-0.03</td>

<td>0.00</td>

<td>0.07</td>

<td>-0.04</td>

<td>-0.03</td>

<td>0.05</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Effort expectancy 4</td>

<td>0.07</td>

<td>-0.14</td>

<td>

**0.86**</td>

<td>0.03</td>

<td>0.01</td>

<td>-0.06</td>

<td>0.09</td>

<td>-0.03</td>

<td>-0.07</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Social influence 1</td>

<td>-0.02</td>

<td>0.02</td>

<td>-0.01</td>

<td>

**0.94**</td>

<td>0.04</td>

<td>-0.01</td>

<td>-0.08</td>

<td>0.03</td>

<td>0.01</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Social influence 2</td>

<td>-0.03</td>

<td>-0.01</td>

<td>0.01</td>

<td>

**0.96**</td>

<td>-0.02</td>

<td>-0.02</td>

<td>0.06</td>

<td>-0.04</td>

<td>-0.04</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Social influence 3</td>

<td>0.04</td>

<td>-0.01</td>

<td>-0.01</td>

<td>

**0.94**</td>

<td>-0.02</td>

<td>0.03</td>

<td>0.01</td>

<td>0.01</td>

<td>0.04</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Facilitating conditions 1</td>

<td>-0.11</td>

<td>0.04</td>

<td>0.36</td>

<td>-0.02</td>

<td>

**0.85**</td>

<td>-0.04</td>

<td>-0.02</td>

<td>-0.05</td>

<td>-0.01</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Facilitating conditions 2</td>

<td>-0.50</td>

<td>-0.08</td>

<td>-0.08</td>

<td>0.02</td>

<td>

**0.79**</td>

<td>-0.08</td>

<td>0.22</td>

<td>0.05</td>

<td>0.02</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Facilitating conditions 3</td>

<td>0.65</td>

<td>0.04</td>

<td>-0.32</td>

<td>0.00</td>

<td>

**0.74**</td>

<td>0.13</td>

<td>-0.22</td>

<td>0.00</td>

<td>-0.01</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Hedonic motivation 1</td>

<td>0.02</td>

<td>-0.02</td>

<td>0.03</td>

<td>0.00</td>

<td>-0.02</td>

<td>

**0.95**</td>

<td>0.03</td>

<td>-0.06</td>

<td>-0.03</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Hedonic motivation 2</td>

<td>0.02</td>

<td>0.05</td>

<td>-0.02</td>

<td>0.00</td>

<td>0.02</td>

<td>

**0.93**</td>

<td>-0.04</td>

<td>0.02</td>

<td>0.02</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Hedonic motivation 3</td>

<td>-0.04</td>

<td>-0.04</td>

<td>-0.01</td>

<td>0.00</td>

<td>0.00</td>

<td>

**0.92**</td>

<td>0.01</td>

<td>0.05</td>

<td>0.01</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Habit 1</td>

<td>0.15</td>

<td>0.07</td>

<td>0.04</td>

<td>-0.10</td>

<td>0.07</td>

<td>-0.01</td>

<td>

**0.88**</td>

<td>-0.06</td>

<td>0.00</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Habit 2</td>

<td>-0.23</td>

<td>-0.07</td>

<td>0.01</td>

<td>-0.01</td>

<td>-0.07</td>

<td>0.05</td>

<td>

**0.90**</td>

<td>-0.02</td>

<td>0.02</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Habit 3</td>

<td>0.09</td>

<td>0.01</td>

<td>-0.03</td>

<td>0.11</td>

<td>0.00</td>

<td>-0.04</td>

<td>

**0.86**</td>

<td>0.08</td>

<td>-0.03</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Price value 1</td>

<td>-0.15</td>

<td>-0.03</td>

<td>0.04</td>

<td>-0.01</td>

<td>-0.09</td>

<td>-0.06</td>

<td>0.05</td>

<td>

**0.94**</td>

<td>0.04</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Price value 2</td>

<td>-0.27</td>

<td>0.01</td>

<td>0.06</td>

<td>0.04</td>

<td>-0.10</td>

<td>-0.04</td>

<td>0.11</td>

<td>

**0.93**</td>

<td>-0.02</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Price value 3</td>

<td>0.76</td>

<td>0.07</td>

<td>-0.19</td>

<td>-0.05</td>

<td>.34</td>

<td>0.19</td>

<td>-0.28</td>

<td>

**0.51**</td>

<td>-0.03</td>

<td>&lt;0.001</td>

<td> </td>

</tr>

<tr>

<td>Use behaviour 1</td>

<td>0.07</td>

<td>-0.14</td>

<td>-0.18</td>

<td>0.05</td>

<td>0.20</td>

<td>0.12</td>

<td>-0.31</td>

<td>-0.01</td>

<td>

**0.51**</td>

<td>&lt;0.001</td>

<td>1.07</td>

</tr>

<tr>

<td>Use behaviour 2</td>

<td>-0.23</td>

<td>-0.37</td>

<td>0.01</td>

<td>0.13</td>

<td>-0.05</td>

<td>0.47</td>

<td>0.21</td>

<td>-0.11</td>

<td>

**0.54**</td>

<td>&lt;0.001</td>

<td>1.10</td>

</tr>

<tr>

<td>Use behaviour 3</td>

<td>0.09</td>

<td>0.12</td>

<td>-0.03</td>

<td>-0.07</td>

<td>0.03</td>

<td>-0.09</td>

<td>0.05</td>

<td>0.01</td>

<td>

**0.81**</td>

<td>&lt;0.001</td>

<td>1.36</td>

</tr>

<tr>

<td>Use behaviour 4</td>

<td>0.03</td>

<td>0.27</td>

<td>0.16</td>

<td>-0.06</td>

<td>-0.14</td>

<td>-0.36</td>

<td>0.01</td>

<td>0.09</td>

<td>

**0.67**</td>

<td>&lt;0.001</td>

<td>1.26</td>

</tr>

<tr>

<td colspan="12">

_Loadings (reflective items) and weights (formative items) are shown in bold; loadings and cross-loadings are oblique-rotated; p-values refer to loadings and were obtained through bootstrapping with 100 resample; VIF, variance inflation factor for indicator_</td>

</tr>

</tbody>

</table>

The reliability of the reflective latent variable indicates how rigorous observed variables are measuring the same reflective latent variable, Cronbach's a coefficient was used as the index of reflective latent variable reliability (CA =0.7 were accepted). In addition, composite reliability was calculated. The latent variable convergent validity was assessed by examining the average variance extracted (AVE > 0.5 were accepted) ([Fornell and Larcker, 1981](#for81)). To test for multi-collinearity, the variance inflation factor was calculated for each latent variable (all were less than 3.1). Table 3 shows the variance inflation factor, Cronbach's a coefficient, composite reliability and average variance extracted for each latent variable.

<table><caption>Table 3: Latent variable coefficients</caption>

<tbody>

<tr>

<th>Latent variable</th>

<th>VIF</th>

<th>CR</th>

<th>CA</th>

<th>AVE</th>

</tr>

<tr>

<td>Behavioural intention</td>

<td>3.09</td>

<td>0.93</td>

<td>0.89</td>

<td>0.82</td>

</tr>

<tr>

<td>Performance expectancy</td>

<td>2.40</td>

<td>.91</td>

<td>.86</td>

<td>.78</td>

</tr>

<tr>

<td>Effort expectancy</td>

<td>2.54</td>

<td>.95</td>

<td>.93</td>

<td>.82</td>

</tr>

<tr>

<td>Social influence</td>

<td>1.30</td>

<td>0.96</td>

<td>0.94</td>

<td>0.89</td>

</tr>

<tr>

<td>Facilitating conditions</td>

<td>3.08</td>

<td>0.84</td>

<td>0.70</td>

<td>.63</td>

</tr>

<tr>

<td>Hedonic motivation</td>

<td>2.25</td>

<td>0.95</td>

<td>0.92</td>

<td>0.87</td>

</tr>

<tr>

<td>Habit</td>

<td>2.55</td>

<td>0.91</td>

<td>0.85</td>

<td>0.77</td>

</tr>

<tr>

<td>Price value</td>

<td>1.44</td>

<td>0.85</td>

<td>0.72</td>

<td>0.67</td>

</tr>

<tr>

<td>Use behaviour</td>

<td>1.44</td>

<td colspan="3">Formative</td>

</tr>

<tr>

<td colspan="5">

_CR, composite reliability coefficient for latent variable; CA, Cronbach’s a coefficient for latent variable; VIF, variance inflation factor for latent variable; AVE, average variances extracted for latent variable_</td>

</tr>

</tbody>

</table>

The latent variable discriminant validity was tested by analysing if the square root of the average variance extracted from each latent variable is greater than the correlations with the rest of the latent variable (see Table 4).

<table><caption>Table 4: Latent variable correlations</caption>

<tbody>

<tr>

<th>Variable</th>

<th>BI</th>

<th>PE</th>

<th>EE</th>

<th>SI</th>

<th>FC</th>

<th>HM</th>

<th>HT</th>

<th>PV</th>

<th>UB</th>

</tr>

<tr>

<td>Behavioural intention</td>

<td>

**0.91**</td>

<td colspan="8"> </td>

</tr>

<tr>

<td>Performance expectancy</td>

<td>0.62</td>

<td>

**0.88**</td>

<td colspan="7"> </td>

</tr>

<tr>

<td>Effort expectancy</td>

<td>0.51</td>

<td>0.63</td>

<td>

**0.91**</td>

<td colspan="6"> </td>

</tr>

<tr>

<td>Social influence</td>

<td>0.40</td>

<td>0.36</td>

<td>0.18</td>

<td>

**0.94**</td>

<td colspan="5"> </td>

</tr>

<tr>

<td>Facilitating conditions</td>

<td>0.66</td>

<td>0.62</td>

<td>0.73</td>

<td>0.29</td>

<td>

**0.79**</td>

<td colspan="4"> </td>

</tr>

<tr>

<td>Hedonic motivation</td>

<td>0.60</td>

<td>0.59</td>

<td>0.61</td>

<td>0.29</td>

<td>0.67</td>

<td>

**0.93**</td>

<td colspan="3"> </td>

</tr>

<tr>

<td>Habit</td>

<td>0.74</td>

<td>0.61</td>

<td>0.46</td>

<td>0.41</td>

<td>0.52</td>

<td>0.50</td>

<td>

**0.88**</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>Price value</td>

<td>0.47</td>

<td>0.42</td>

<td>0.31</td>

<td>0.33</td>

<td>0.41</td>

<td>0.46</td>

<td>0.44</td>

<td>

**0.82**</td>

<td> </td>

</tr>

<tr>

<td>Use behaviour</td>

<td>0.44</td>

<td>0.47</td>

<td>0.41</td>

<td>0.14</td>

<td>0.44</td>

<td>0.46</td>

<td>0.42</td>

<td>0.27</td>

<td>

**NA**</td>

</tr>

<tr>

<td colspan="10">

_Square roots of average variances extracted shown on diagonal. All correlations are significant at p < 0.001_.</td>

</tr>

</tbody>

</table>

### Structural model analysis

Next, once validity and reliability of the measurement model were supported, relationships between constructs were tested. The hypotheses were assessed by examining path coefficients (ß) and their significance levels. Bootstrapping with 100 subsamples was performed to test the statistical significance of each path coefficient using t-tests. The variance explained (R2) in the endogenous latent variable and the regression coefficients’ significance (F-test) serve as indicators of the explanatory power of the model. As shown in Table 5, the model fit indices were calculated to globally assess the model.

<table><caption>Table 5: Model fit indices</caption>

<tbody>

<tr>

<th>Index</th>

<th>Value</th>

<th>p</th>

</tr>

<tr>

<td>Average of path coefficient (APC)</td>

<td>0.175</td>

<td>&lt;0.001</td>

</tr>

<tr>

<td>Average of square root (ARS)</td>

<td>0.463</td>

<td>&lt;0.001</td>

</tr>

<tr>

<td>Average of variance inflation factor (AVIF)</td>

<td>2.125</td>

<td> </td>

</tr>

<tr>

<td>Average of average variances extracted (AAVE)</td>

<td>0.781</td>

<td> </td>

</tr>

<tr>

<td>Goodness of fit (GoF)</td>

<td>0.601</td>

<td> </td>

</tr>

</tbody>

</table>

The main results of partial least squares analyses to test the model are shown in Figure 2\. All the results of partial least squares analyses for the model are shown in Table 6 and Table 7, including moderator effects of age, sex and experience.

<figure>

![Figure1: Research model and hypotheses](../p635fig2.jpg)

<figcaption>Figure 2: Hypotheses and related results</figcaption>

</figure>

<table><caption>Table 6: Structural model results: use behaviour</caption>

<tbody>

<tr>

<th>Dependent variable: use behaviour</th>

<th>D only</th>

<th>ES</th>

<th>D + I</th>

<th>ES</th>

</tr>

<tr>

<td>R<sup>2</sup></td>

<td colspan="2">0.25</td>

<td colspan="2">0.29</td>

</tr>

<tr>

<td>Q<sup>2</sup></td>

<td colspan="2">0.25</td>

<td colspan="2">0.28</td>

</tr>

<tr>

<td>Behavioural intention (BI)</td>

<td>0.14 *</td>

<td>0.06</td>

<td>0.19 **</td>

<td>0.08</td>

</tr>

<tr>

<td>Habit (HT)</td>

<td>0.19 ***</td>

<td>0.08</td>

<td>0.17 *</td>

<td>0.07</td>

</tr>

<tr>

<td>Facilitating conditions (FC)</td>

<td>0.25 ***</td>

<td>0.11</td>

<td>0.17 **</td>

<td>0.07</td>

</tr>

<tr>

<td>Age (AGE)</td>

<td colspan="2" rowspan="18"> </td>

<td>-0.07</td>

<td>0.01</td>

</tr>

<tr>

<td>Sex (SEX)</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>Experience (EXP)</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>BI × EXP</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>SEX x AGE</td>

<td>-0.06</td>

<td>0.01</td>

</tr>

<tr>

<td>AGE × EXP</td>

<td>0.00</td>

<td>0.00</td>

</tr>

<tr>

<td>SEX × EXP</td>

<td>0.11 +</td>

<td>0.01</td>

</tr>

<tr>

<td>SEX × AGE × EXP</td>

<td>-0.08</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × SEX</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>FC × AGE</td>

<td>0.07</td>

<td>0.02</td>

</tr>

<tr>

<td>FC × EXP</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × AGE</td>

<td>0.03</td>

<td>0.01</td>

</tr>

<tr>

<td>HT × EXP</td>

<td>0.07</td>

<td>0.01</td>

</tr>

<tr>

<td>HT × SEX × AGE</td>

<td>0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × SEX × EXP</td>

<td>-0.08</td>

<td>0.01</td>

</tr>

<tr>

<td>FC × AGE × EXP</td>

<td>0.01</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × AGE × EXP</td>

<td>-0.01</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × SEX × AGE × EXP</td>

<td>0.02</td>

<td>0.00</td>

</tr>

<tr>

<td colspan="5">

_D only: direct effects only; D+I: direct effects and interaction terms; ES: effect size; *** p &lt; 0.001; ** p &lt; 0.01; * p &lt;0.05; + p &lt;0.10_</td>

</tr>

</tbody>

</table>

<table><caption>Table 7: Structural model results: behavioural intention</caption>

<tbody>

<tr>

<th>Dependent variable: behavioural intention</th>

<th>D only</th>

<th>ES</th>

<th>D + I</th>

<th>ES</th>

</tr>

<tr>

<td>R<sup>2</sup></td>

<td colspan="2">0.68</td>

<td colspan="2">0.73</td>

</tr>

<tr>

<td>Q<sup>2</sup></td>

<td colspan="2">0.68</td>

<td colspan="2">0.73</td>

</tr>

<tr>

<td>Social influence (SI)</td>

<td>0.05 *</td>

<td>0.02</td>

<td>0.05 +</td>

<td>0.02</td>

</tr>

<tr>

<td>Habit (HT)</td>

<td>0.46 ***</td>

<td>0.34</td>

<td>0.44 ***</td>

<td>0.33</td>

</tr>

<tr>

<td>Hedonic motivation (HM)</td>

<td>0.12 **</td>

<td>0.07</td>

<td>0.16 ***</td>

<td>0.09</td>

</tr>

<tr>

<td>Effort expectency (EE)</td>

<td>-0.09 *</td>

<td>0.04</td>

<td>-0.04</td>

<td>0.02</td>

</tr>

<tr>

<td>Performance expectency (PE)</td>

<td>0.08 *</td>

<td>0.05</td>

<td>0.06</td>

<td>0.04</td>

</tr>

<tr>

<td>Facilitating conditions (FC)</td>

<td>0.31 ***</td>

<td>0.21</td>

<td>0.33 ***</td>

<td>0.22</td>

</tr>

<tr>

<td>Price value (PV)</td>

<td>0.06 *</td>

<td>0.03</td>

<td>0.02</td>

<td>0.01</td>

</tr>

<tr>

<td>Age (AGE)</td>

<td colspan="2" rowspan="47"> </td>

<td>0.10 *</td>

<td>0.00</td>

</tr>

<tr>

<td>Sex (SEX)</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>Experience (EXP)</td>

<td>0.13 *</td>

<td>0.03</td>

</tr>

<tr>

<td>SEX x AGE</td>

<td>-0.05</td>

<td>0.00</td>

</tr>

<tr>

<td>AGE × EXP</td>

<td>-0.18 *</td>

<td>0.01</td>

</tr>

<tr>

<td>SEX × EXP</td>

<td>-0.07</td>

<td>0.01</td>

</tr>

<tr>

<td>SEX × AGE × EXP</td>

<td>0.17 *</td>

<td>0.02</td>

</tr>

<tr>

<td>PE × SEX</td>

<td>0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>SI × SEX</td>

<td>0.05 *</td>

<td>0.00</td>

</tr>

<tr>

<td>FC × SEX</td>

<td>0.01</td>

<td>0.00</td>

</tr>

<tr>

<td>HM × SEX</td>

<td>0.00</td>

<td>0.00</td>

</tr>

<tr>

<td>PV × SEX</td>

<td>-0.03</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × SEX</td>

<td>0.01</td>

<td>0.00</td>

</tr>

<tr>

<td>PE × AGE</td>

<td>0.10</td>

<td>0.02</td>

</tr>

<tr>

<td>EE × AGE</td>

<td>0.08</td>

<td>0.02</td>

</tr>

<tr>

<td>SI × AGE</td>

<td>-0.03</td>

<td>0.00</td>

</tr>

<tr>

<td>FC × AGE</td>

<td>0.10</td>

<td>0.01</td>

</tr>

<tr>

<td>HM × AGE</td>

<td>-0.06</td>

<td>0.01</td>

</tr>

<tr>

<td>PV × AGE</td>

<td>0.00</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × AGE</td>

<td>0.03</td>

<td>0.00</td>

</tr>

<tr>

<td>EE × EXP</td>

<td>0.07</td>

<td>0.01</td>

</tr>

<tr>

<td>SI × EXP</td>

<td>-0.12 *</td>

<td>0.01</td>

</tr>

<tr>

<td>FC × EXP</td>

<td>0.03</td>

<td>0.00</td>

</tr>

<tr>

<td>HM × EXP</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × EXP</td>

<td>-0.08</td>

<td>0.01</td>

</tr>

<tr>

<td>PE × SEX × AGE</td>

<td>0.10</td>

<td>0.01</td>

</tr>

<tr>

<td>EE × SEX × AGE</td>

<td>0.08</td>

<td>0.01</td>

</tr>

<tr>

<td>SI × SEX × AGE</td>

<td>0.06</td>

<td>0.00</td>

</tr>

<tr>

<td>FC × SEX × AGE</td>

<td>-0.16 +</td>

<td>0.01</td>

</tr>

<tr>

<td>HM × SEX × AGE</td>

<td>0.05</td>

<td>0.00</td>

</tr>

<tr>

<td>PV × SEX × AGE</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × SEX × AGE</td>

<td>-0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>EE × SEX × EXP</td>

<td>-0.12</td>

<td>0.02</td>

</tr>

<tr>

<td>SI × SEX × EXP</td>

<td>0.05</td>

<td>0.00</td>

</tr>

<tr>

<td>FC × SEX × EXP</td>

<td>-0.05</td>

<td>0.01</td>

</tr>

<tr>

<td>HM × SEX × EXP</td>

<td>0.09</td>

<td>0.01</td>

</tr>

<tr>

<td>HT × SEX × EXP</td>

<td>0.05</td>

<td>0.00</td>

</tr>

<tr>

<td>EE × AGE × EXP</td>

<td>-0.14</td>

<td>0.03</td>

</tr>

<tr>

<td>SI × AGE × EXP</td>

<td>-0.04</td>

<td>0.00</td>

</tr>

<tr>

<td>FC × AGE × EXP</td>

<td>0.26 +</td>

<td>0.03</td>

</tr>

<tr>

<td>HM × AGE × EXP</td>

<td>-0.09</td>

<td>0.02</td>

</tr>

<tr>

<td>HT × AGE × EXP</td>

<td>0.14</td>

<td>0.01</td>

</tr>

<tr>

<td>EE × SEX × AGE × EXP</td>

<td>0.15</td>

<td>0.02</td>

</tr>

<tr>

<td>SI × SEX × AGE × EXP</td>

<td>-0.05</td>

<td>0.00</td>

</tr>

<tr>

<td>FC × SEX × AGE × EXP</td>

<td>-0.17</td>

<td>0.01</td>

</tr>

<tr>

<td>HM × SEX × AGE × EXP</td>

<td>0.02</td>

<td>0.00</td>

</tr>

<tr>

<td>HT × SEX × AGE × EXP</td>

<td>-0.03</td>

<td>0.00</td>

</tr>

<tr>

<td colspan="5">

_D only: direct effects only; D+I: direct effects and interaction terms; ES: effect size; *** p &lt; 0.001; ** p &lt; 0.01; * p &lt;0.05; + p &lt;0.10_</td>

</tr>

</tbody>

</table>

The model explains 73% of the variation in behaviour intention and 29% of the variation in behaviour of mobile Internet usage. In addition, effect sizes of all relationships are above the threshold minimum, highlighting the effect size of the variable habit.

The results support hypotheses 1, 3, 4, 5, 6, 7, 8, 9 and 10\. However, hypothesis 2 is not supported. Table 8 summarizes the findings.

<table><caption>Table 8: Summary of findings</caption>

<tbody>

<tr>

<th>Hypothesis</th>

<th>Dependent variables</th>

<th>Independent variables</th>

<th>Moderators</th>

</tr>

<tr>

<td>H1</td>

<td>Performance expectancy</td>

<td>Behavioural intention</td>

<td> </td>

</tr>

<tr>

<td>H3</td>

<td>Social influence</td>

<td>Behavioural intention</td>

<td>Sex, experience</td>

</tr>

<tr>

<td>H4</td>

<td>Facilitating conditions</td>

<td>Use behaviour</td>

<td> </td>

</tr>

<tr>

<td>H5</td>

<td>Behavioural intention</td>

<td>Use behaviour</td>

<td> </td>

</tr>

<tr>

<td>H6</td>

<td>Facilitating conditions</td>

<td>Behavioural intention</td>

<td>Sex, experience, age</td>

</tr>

<tr>

<td>H7</td>

<td>Hedonic motivation</td>

<td>Behavioural intention</td>

<td> </td>

</tr>

<tr>

<td>H8</td>

<td>Price value</td>

<td>Behavioural intention</td>

<td> </td>

</tr>

<tr>

<td>H9</td>

<td>Habit</td>

<td>Behavioural intention</td>

<td> </td>

</tr>

<tr>

<td>H10</td>

<td>Habit</td>

<td>Use behaviour</td>

<td> </td>

</tr>

</tbody>

</table>

## Discussion

Overall, the study results indicate that the unified theory model is valid in Chile. The exception to this statement is the relationship between effort expectancy and behavioural intention (H2), a slightly negative relationship, contrary to the proposal made in the research model. This indicates that when mobile Internet is perceived easier to use, intention to use mobile Internet decreases. One explanation for this result may be associated with the cultural characteristics of Chile; the literature suggests that a high index of uncertainty avoidance is correlated with a low or non-significant relationship between effort expectancy and behavioural intention ([Cardon and Marshall, 2008](#car08)). Consistent with this explanation, other empirical studies of acceptance of online technologies in samples of Chilean users have delivered results where this relationship was non-significant ([Al-Khateeb, 2007](#alk07) ; [Andrews and Bianchi, 2013](#and13); [Ramírez-Correa, _et al._, 2013](#ram13)). Moreover, a recent study that applied the theory in a context of online consumer behaviour in Spain ([Escobar-Rodríguez and Carvajal-Trujillo, 2013](#esc13)), a country with an index of uncertainty avoidance similar to Chile, indicated a negative relationship between these two variables, although this was non-significant. Considering these facts, we believe that this relationship should be closely studied in the future.

Research findings indicate that the variables that increase behavioural intention towards mobile Internet usage are, in order of importance: habit; facilitating conditions; hedonic motivation; performance expectancy; price value; and social influence. Among these variables the first two are of huge relevance. Therefore, the automatic and continuing use of mobile Internet generates greater intention towards mobile Internet usage, and the user’s perception of the resources and support available to use mobile Internet is essential to increasing behavioural intention towards mobile Internet usage. Moreover, research findings indicate that the variables which increase mobile Internet usage are, in order of importance: habit; behavioural intention; and facilitating conditions. Again, habit appears as a relevant precedent to explain the acceptance of mobile Internet in Chile. Given the recent proposal of this variable as an explanation of both the intention to use mobile Internet and mobile Internet usage, we believe that this result is an important finding that may help explain how people adopt such technologies in Latin American countries.

We would like to highlight the role of habit in the model. According to Venkatesh _et al._ ([2012](#ven12)), habit has been defined as the degree to which individuals tend to perform behaviour mechanically because of learning ([Limayem, Hirt and Cheung, 2007](#lim07)), while Kim and Mallhostra ([2005](#kim05)) associate habit with automaticity. This includes the idea that former use is an important predictor of future behaviour ([Kim and Mallhostra, 2005](#kim05)). From this point of view, these results are very consistent, since in the case of mobile Internet consumers already had the habit of using both Internet and mobile phones. The new technology, mobile Internet, only combines two habits that consumers already maintained.

Finally, we highlight the results in relation to the moderations that have been discovered in the analysis of the model. According to these results the relationship between social influence and behavioural intention is moderated, firstly by gender and secondly by experience. The effect of social influence on behavioural intention increases slightly in men and decreases with the experience of using mobile Internet. For this relationship, moderation of gender and experience together has been described in the literature ([Thong, _et al._, 2011](#tho11); [Venkatesh, _et al._, 2012](#ven12)), but not independently.

Similarly, the relationship between facilitating conditions and behavioural intention is moderated, first, by age and experience, and second, by gender and age. The effect of facilitating conditions on behavioural intention increases in older people with more experience of using mobile Internet, and also increases in the case of older women, regardless of their experience in the use of mobile Internet. This latter moderation is consistent with the original study outlining the unified theory ([Venkatesh, _et al._, 2012](#ven12)).

Overall, the result of the exploration of the potential moderations of the variables sex, age and experience in the acceptance of mobile Internet in Chile indicates a quite homogeneous consumer market.

## Implications

From a theoretical point of view, this research validates a novel model of factors influencing the adoption of technology in a consumer context in Chile. In this sense, the main theoretical contribution of this research is to dismiss the hypothesis that, in a consumer setting of a country with high uncertainty avoidance index, when effort expectation decreases, intention to use technology increases. Another major contribution is the importance of habit as a predictor of individual behaviour in consumer technology acceptance; this result strengthens research findings in the context of technologically advanced countries ([Limayem and Hirt, 2003](#lim03)).

Beyond the aforementioned implications associated to the replica, this study has other implications. Accepting the positivism paradigm which compels us to search for more thorough explanations of the phenomenon of mobile Internet acceptance, and given that the around 70% of the variance in behaviour intention is explained in the model, it is imperative to both elicit new variables and seek out new relationships between variables in order to increase this percentage. In regard to the former, study findings suggest that these new variables can be associated with cultural dimensions; one particular path to find these variables is to test the research model from this study in countries culturally similar to Chile, such as Argentina, Mexico and Venezuela ([Schwartz, 2006](#sch06)). In regards to the latter, it is important to conceptually develop and assess non-linear relationships between behaviour intention and the variables which explain it.

This research further provides practical implications for the marketing of mobile Internet in Chile and other culturally similar countries. Firstly, the market for mobile Internet in Chile is relatively homogeneous, therefore consumers should respond similarly to advertising campaigns incentivising the adoption of this technology. An important exception to this rule is the group of older consumers with experience in the use of technologies, as they will be much more responsive to improvements associated with the resources and support available for using mobile Internet.

Secondly, habit is a fundamental variable in predicting intention to use mobile Internet and mobile Internet usage. As we know that this habit is achieved through learning, developing strategies to improve early and ongoing access to this technology may be a cornerstone for increasing its use. An illustration of a good strategy to create the habit could be the development of applications that require continuous use of mobile Internet and that have a high value for customers. Thirdly, effort expectancy and performance expectation, common variables in explaining the use of technology in organizational settings are not important predictors of mobile Internet use at a consumer level. Therefore, companies should observe other variables when trying to encourage mobile Internet usage, for instance, the hedonic motivations associated with its use. Finally, both social influence and price/value, which could be of great impact on the intention towards mobile Internet usage in a collectivist culture and an emerging economic market, appear in our results in a third level of relevance. In fact, social influence has a significant impact on the start of mobile Internet usage. However, this impact disappears with increasing experience in its use. Hence, companies can attract new users through social influence, but this influence does not ensure its permanence.

## Conclusions

This research was devised to test the applicability of an information technology acceptance model in a technologically developing country. The revised unified theory was applied in a proportional sample of mobile Internet users in Chile.

In summary, we can affirm that Chilean acceptance behaviour of mobile Internet technology matches global observations according to the theory. Specifically, on one hand, the measurement model was validated successfully, and on the other hand, the structural model supported nine of the ten hypotheses proposed, achieving a 73% explanation of the intention to use mobile Internet. Therefore, the increase in mobile Internet usage is explained by the variables habit, facilitating conditions, hedonic motivation, performance expectancy, price value and social influence. The exploration of the potential moderations of the variables sex, age and experience in the acceptance of mobile Internet in Chile indicates a quite homogeneous consumer market.

The study findings highlighted habit as the major determinant in the acceptance of mobile Internet. We believe this is an interesting topic for future research, in particular the study of variables that moderate the effect of habit in the acceptance of information technologies. Additionally, another finding of this study is the unusual effect of effort expectancy in mobile Internet acceptance. This is an issue that should be analysed in future research.

Finally, we point out two limitations of this study. First, the use of a non-random sampling method limits the generalization of findings. Secondly, the study is cross sectional, a longitudinal study would be advisable to compare the different stages of the adoption of mobile Internet.

## Acknowledgements

We would like to thank anonymous reviewers and the Associate Editor Dr. Charles Cole for their insightful comments. The authors would like to extend their gratitude to Professor Dr. Felix Tan, Head of the Department of Business Information Systems, Auckland University of Technology, New Zealand, for his support on this study during post-doctoral stay of the first author. This research is partially supported by the Catholic University of the North, Chile.

## <a id="author"></a>About the authors

**Patricio Esteban Ramírez-Correa** is Associate Professor at the School of Business Sciences of Catholic University of the North in Coquimbo, Chile. He received his BS degree in Computer from Pontifical University Catholic of Valparaiso, Chile and his PhD degree in Business Administration from University of Seville, Spain. His research interests include enterprise systems and information technology acceptance. He can be reached at [patricio.ramirez@ucn.cl](mailto:patricio.ramirez@ucn.cl)  
**Francisco Javier Rondán-Cataluña** is Associate Professor at the Department of Business Management and Marketing at the University of Seville, Spain. He received his BA and PhD degrees in Business Administration from University of Seville, Spain. His research interests include franchise, price management and consumer behaviour. He can be reached at [rondan@us.es](mailto:rondan@us.es)  
**Jorge Arenas-Gaitán** is Assistant Professor at the Department of Business Management and Marketing at the University of Seville, Spain. He received his BA and PhD degrees in Business Administration from University of Seville, Spain. His research interests include behaviour of firms in global markets, cross-cultural studies and information technology acceptance. He can be reached at [jarenas@us.es](mailto:jarenas@us.es)

<section>

## References

<ul>
<li id="abb11">Abbasi, M. S., Chandio, F. H., Soomro, A. F. &amp; Shah, F. (2011). Social influence, voluntariness, experience and the internet acceptance: an extension of technology acceptance model within a south-Asian country context. <em>Journal of Enterprise Information Management, 24</em>(1), 30-52.
</li>
<li id="ajz91">Ajzen, I. (1991). The theory of planned behavior. <em>Organizational Behavior and Human Decision Processes, 50</em>(2), 179-211.
</li>
<li id="ajz80">Ajzen, I. &amp; Fishbein, M. (1980). <em>Understanding attitudes and predicting social behavior</em>. Englewood Cliffs, NJ: Prentice-Hall.
</li>
<li id="alk07">Al-Khateeb, F. B. (2007). Predicting Internet usage in two emerging economies using an extended technology acceptance model (TAM). In <em>International Symposium on Collaborative Technologies and Systems, Orlando, Florida, 25-25 May 2007.</em> , (pp. 143-149). Piscataway, NJ: IEEE.
</li>
<li id="als11">Alshare, K. A., Mesak, H. I., Grandon, E. E. &amp; Badri, M. A. (2011). Examining the moderating role of national culture on an extended technology acceptance model. <em>Journal of Global Information Technology Management, 14</em>(3), 27-53.
</li>
<li id="alw13">Alwahaishi, S. &amp; Snasel, V. (2013). Modeling the determinants influencing the diffusion of mobile Internet. <em>Journal of Physics: Conference Series, 423</em>, 1-8.
</li>
<li id="and13">Andrews, L. &amp; Bianchi, C. (2013). Consumer Internet purchasing behavior in Chile. <em>Journal of Business Research, 66</em>(10), 1791-1799.
</li>
<li id="are11">Arenas-Gaitan, J., Ramirez-Correa, P. E. &amp; Rondan-Cataluna, F. J. (2011). Cross cultural analysis of the use and perceptions of Web based learning systems. <em>Computers &amp; Education, 57</em>(2), 1762-1774.
</li>
<li id="are13">Arenas-Gaitán, J., Rondán-Cataluña, F. J. &amp; Ramírez-Correa, P. (2013). Social identity, eWOM &amp; referrals in social network services. <em>Kybernetes, 42</em>(6), 1149-1165.
</li>
<li id="avg08">Avgerou, C. (2008). Information systems in developing countries: a critical research review. <em>Journal of information Technology, 23</em>(3), 133-146.
</li>
<li id="bae11">Baek, J., Park, S. &amp; Lee, C. C. (2011). Identifying drivers for continual usage of wireless broadband. <em>International Journal of Mobile Communications, 9</em>(4), 317-340.
</li>
<li id="bia12">Bianchi, C. &amp; Andrews, L. (2012). Risk, trust, and consumer online purchasing behaviour: a Chilean perspective. <em>International Marketing Review, 29</em>(3), 253-275.
</li>
<li id="car08">Cardon, P. W. &amp; Marshall, B. (2008). National culture and technology acceptance: the impact of uncertainty avoidance. <em>Issues in Information Systems, 9</em>(2), 103-110.
</li>
<li id="cen09">Cenfetelli, R. T. &amp; Bassellier, G. (2009). Interpretation of formative measurement in information systems research. <em>MIS Quarterly, 33</em>(4), 689-707.
</li>
<li id="che12">Cheon, J., Lee, S., Crooks, S. M. &amp; Song, J. (2012). An investigation of mobile learning readiness in higher education based on the theory of planned behavior. <em>Computers &amp; Education, 59</em>(3), 1054 - 1064.
</li>
<li id="che05">Cheong, J. H. &amp; Park, M. C. (2005). Mobile Internet acceptance in Korea. <em>Internet Research: Electronic Networking Applications and Policy, 15</em>(2), 125-140.
</li>
<li id="chi12">Chile. <em>Ministerio de Transportes y Telecomunicaciones, Subsecretaría de Telecomunicaciones</em> (2012). <em>Radiografía de servicios de Internet fija y móvil.</em> [Detailed analysis of fixed and mobile Internet services.] Santiago: Ministerio de Transportes y Telecomunicaciones. Retrieved from http://www.innovacion.gob.cl/wp-content/uploads/2012/09/radiografia_internet_06sept2012.pdf (Archived by WebCite® at http://www.webcitation.org/6PNcVZNlD)
</li>
<li id="chi98">Chin, W. W. (1998). The partial least squares approach for structural equation modeling. In G. A. Marcoulides (Ed.), <em>Modern methods for business research</em> (pp. 295-336). Hillsdale, NJ: Lawrence Erlbaum Associates.
</li>
<li id="cho08">Choi, J., Seol, H., Lee, S., Cho, H. &amp; Park, Y. (2008). Customer satisfaction factors of mobile commerce in Korea. <em>Internet Research, 18</em>(3), 313-335.
</li>
<li id="cho13">Chong, A. Y.-L. (2013). A two-staged SEM-neural network approach for understanding and predicting the determinants of m-commerce adoption. <em>Expert Systems with Applications, 40</em>(4), 1240-1247.
</li>
<li id="chu13">Chu, K.-M. (2013). Motives for participation in Internet innovation intermediary platforms. <em>Information Processing &amp; Management, 49</em>(4), 945-953.
</li>
<li id="cou12">Coursaris, C. K. &amp; Sung, J. (2012). Antecedents and consequents of a mobile Website's interactivity. <em>New Media &amp; Society, 14</em>(7), 1128-1146.
</li>
<li id="dav89">Davis, F. D. (1989). Perceived usefulness, perceived ease of use, and user acceptance of information technology. <em>MIS Quarterly, 13</em>(3), 319-340.
</li>
<li id="der13">de Reuver, M., Ongena, G. &amp; Bouwman, H. (2013). Should mobile Internet be an extension to the fixed web? Fixed-mobile reinforcement as mediator between context of use and future use. <em>Telematics and Informatics, 30</em>(2), 111-120.
</li>
<li id="don08">Donoso, P. &amp; Crittenden, V. L. (2008). Strategic management in Latin America. <em>Journal of Business Research, 61</em>(6), 587-589.
</li>
<li id="esc13">Escobar-Rodríguez, T. &amp; Carvajal-Trujillo, E. (2013). Online drivers of consumer purchase of Website airline tickets. <em>Journal of Air Transport Management, 32</em>(1), 58-64.d
</li>
<li id="eur13">Euromonitor International Ltd. (2013). <em>Passport: technology, communications and media.</em> London: Euromonitor International Ltd.
</li>
<li id="eve12">Everis and IESE Business School. (2012). Indicador de la sociedad de la información (ISI). Situación de las tecnologías de la informació´n en Latinoamérica, la Unió´n Europea y EE.UU. Especial referencia a los casos de Argentina, Brasil, Chile, Colombia, México and Perú. [Information society indicator. The status of information technologies in Latin American, the European Union and the US. With special reference to the cases of Argentina, Brazil, Chile, Colombia, Mexico and Peru.] Madrid: everis. Retrieved from http://www.iese.edu/Aplicaciones/upload/ISIdiciembre2011.pdf (Archived by WebCite® at http://www.webcitation.org/6PNnd5BoB)
</li>
<li id="for81">Fornell, C. &amp; Larcker, D. F. (1981). Evaluating structural equation models with unobservable variables and measurement error. <em>Journal of Marketing Research, 18</em>(1), 39-50.
</li>
<li id="ger11">Gerpott, T. J. (2011). Determinants of self-report and system-captured measures of mobile Internet use intensity. <em>Information Systems Frontiers, 13</em>(4), 561-578.
</li>
<li id="ger13">Gerpott, T. J., Thomas, S. &amp; Weichert, M. (2013). Characteristics and mobile Internet use intensity of consumers with different types of advanced handsets: an exploratory empirical study of iPhone, Android and other Web-enabled mobile users in Germany. <em>Telecommunications Policy, 37</em>(4-5), 357-371.
</li>
<li id="gog12">Gogus, A., Nistor, N. &amp; Lerche, T. (2012). Educational technology acceptance across cultures: a validation of the unified theory of acceptance and use of technology in the context of Turkish national culture. <em>Turkish Online Journal of Educational Technology, 11</em>(4), 394-408.
</li>
<li id="gra11">Grandón, E. E., Nasco, S. A. &amp; Mykytyn, P. P. (2011). Comparing theories to explain e-commerce adoption. <em>Journal of Business Research, 64</em>(3), 292-298.
</li>
<li id="hai86">Hair, J. F., Anderson, R. E. &amp; Tatham, R. L. (1986). <em>Multivariate data analysis with readings</em>. New York, NY: Macmillan Publishing Co., Inc.
</li>
<li id="hof01">Hofstede, G. H. (2001). <em>Culture's consequences: comparing values, behaviors, institutions and organizations across nations</em>. Thousand Oaks, CA: Sage Publications
</li>
<li id="hon06">Hong, S. J., Thong, J. Y. L. &amp; Tam, K. Y. (2006). Understanding continued information technology usage behavior: a comparison of three models in the context of mobile Internet. <em>Decision Support Systems, 42</em>(3), 1819-1834.
</li>
<li id="hsi13">Hsiao, K. L. (2013). Android smartphone adoption and intention to pay for mobile Internet: perspectives from software, hardware, design, and value. <em>Library Hi Tech, 31</em>(2), 216-235.
</li>
<li id="hsu07">Hsu, C. L., Lu, H. P. &amp; Hsu, H. H. (2007). Adoption of the mobile Internet: an empirical study of multimedia message service (MMS). <em>Omega, 35</em>(6), 715-726.
</li>
<li id="hsu08">Hsu, H. H., Lu, H. P. &amp; Hsu, C. L. (2008). Multimedia messaging service acceptance of pre- and post-adopters: a sociotechnical perspective. <em>International Journal of Mobile Communications, 6</em>(5), 598-615.
</li>
<li id="int13">International Telecommunication Union. (2013). <em><a href="http://www.webcitation.org/6RLxId7MQ">Measuring the information society, 2013</a></em>. Geneva, Switzerland: International Telecommunication Union. Retrieved from http://www.itu.int/en/ITU-D/Statistics/Documents/publications/mis2013/MIS2013_without_Annex_4.pdf (Archived by WebCite® at http://www.webcitation.org/6RLxId7MQ)
</li>
<li id="ips12">Ipsos. (2012). <a href="http://www.webcitation.org/6PNdT0Pug"><em>Estudio de opinión pública. Tercera medición en "población conectada"</em></a>. [Study of public opinion. Third measurement of "connected people"] Paris: Ipsos. Retrieved from http://www.ipsos.cl/documentos/Informe_de_Actualidad_en_Poblacion_Conectada_26_04_2012_Medios_VF.pdf (Archived by WebCite® at http://www.webcitation.org/6PNdT0Pug)
</li>
<li id="kim07">Kim, H. W., Chan, H. C. &amp; Gupta, S. (2007). Value-based adoption of mobile Internet: an empirical investigation. <em>Decision Support Systems, 43</em>(1), 111-126.
</li>
<li id="kim10">Kim, H. W., Kwahk, K. Y. &amp; Lee, H. Y. (2010). An integrated model of mobile Internet services usage and continuance. <em>International Journal of Mobile Communications, 8</em>(4), 411-429.
</li>
<li id="kim12">Kim, S. &amp; Yang, T. (2012). Consumer preferences for mobile Internet: a comparative cross-national mixed methods study. <em>Social Behavior and Personality, 40</em>(10), 1695-1711.
</li>
<li id="kim05">Kim, S. S. &amp; Malhotra, N. K. (2005). A longitudinal model of continued IS use: an integrative view of four mechanisms underlying post-adoption phenomena. <em>Management Science, 51</em>(5), 741-755.
</li>
<li id="koe12">Koenigstorfer, J. &amp; Groeppel-Klein, A. (2012). Consumer acceptance of the mobile Internet. <em>Marketing Letters, 23</em>(4), 917-928.
</li>
<li id="lar12">LaRose, R., De Maagd, K., Chew, H. E., Tsai, H.-Y. S., Steinfield, C., Wildman, S. S. &amp; Bauer, J. M. (2012). Measuring sustainable broadband adoption: an innovative approach to understanding broadband adoption and use. <em>International Journal of Communication, 6</em>(1), 2576-2600.
</li>
<li id="lee09">Lee, S. (2009). Mobile Internet services from consumers' perspectives. <em>International Journal of Human-Computer Interaction, 25</em>(5), 390-413.
</li>
<li id="lee13">Lee, S. H. &amp; Chang, B. H. (2013). Factors influencing the use of portals on mobile Internet devices. <em>International Journal of Mobile Communications, 11</em>(3), 279-298.
</li>
<li id="leo13a">Leong, L.-Y., Hew, T.-S., Tan, G. W.-H. &amp; Ooi, K.-B. (2013). Predicting the determinants of the NFC-enabled mobile credit card acceptance: a neural networks approach. <em>Expert Systems with Applications, 40</em>(14), 5604-5620.
</li>
<li id="leo13b">Leong, L.-Y., Ooi, K.-B., Chong, A. Y.-L. &amp; Lin, B. (2013). Modeling the stimulators of the behavioral intention to use mobile entertainment: does gender really matter? <em>Computers in Human Behavior, 29</em>(5), 2109-2121.
</li>
<li id="lim03">Limayem, M. &amp; Hirt, S. G. (2003). Force of habit and information systems usage: theory and initial validation. <em>Journal of the Association for Information Systems, 4</em>(1), 65-97.
</li>
<li id="lim07">Limayem, M., Hirt, S. G. &amp; Cheung, C. M. K. (2007). How habit limits the predictive power of intentions: the case of IS continuance. <em>MIS Quarterly, 31</em>(4), 705-737.
</li>
<li id="lin13">Lin, S., Zimmer, J. C. &amp; Lee, V. (2013). Podcasting acceptance on campus: the differing perspectives of teachers and students. <em>Computers &amp; Education, 68</em>(1), 416-428.
</li>
<li id="liu10">Liu, Y. &amp; Li, H. X. (2010). Mobile Internet diffusion in China: an empirical study. <em>Industrial Management &amp; Data Systems, 110</em>(3-4), 309-324.
</li>
<li id="mal09">Maldifassi, J. O. &amp; Canessa, E. C. (2009). Information technology in Chile: how perceptions and use are related to age, gender, and social class. <em>Technology in Society, 31</em> (3), 273-286.
</li>
<li id="man13">Mantymaki, M. &amp; Salo, J. (2013). Purchasing behavior in social virtual worlds: an examination of Habbo Hotel. <em>International Journal of Information Management, 33</em>(2), 282-290.
</li>
<li id="mar12">Mardikyan, S., Besiroglu, B. &amp; Uzmaya, G. (2012). Behavioral intention towards the use of 3G technology. <em>Communications of the IBIMA, 2012</em>(1), 1-10.
</li>
<li id="nas08">Nasco, S. A., Toledo, E. G. &amp; Mykytyn Jr, P. P. (2008). Predicting electronic commerce adoption in Chilean SMEs. <em>Journal of Business Research, 61</em>(6), 697-705.
</li>
<li id="nis13a">Nistor, N. (2013). Stability of attitudes and participation in online university courses: gender and location effects. <em>Computers &amp; Education, 68</em>(1), 284-292.
</li>
<li id="nis13b">Nistor, N., Gogus, A. &amp; Lerche, T. (2013). Educational technology acceptance across national and professional cultures: a European study. <em>Educational Technology Research and Development, 61</em>(4), 733-749.
</li>
<li id="nis12">Nistor, N., Schworm, S. &amp; Werner, M. (2012). Online help-seeking in communities of practice: modeling the acceptance of conceptual artifacts. <em>Computers &amp; Education, 59</em>(2), 774-784.
</li>
<li id="oka06">Okazaki, S. (2006). What do we know about mobile Internet adopters? A cluster analysis. <em>Information &amp; Management, 43</em>(2), 127-141.
</li>
<li id="par13a">Park, E. &amp; del Pobil, A. P. (2013). Modeling the user acceptance of long-term evolution (LTE) services. <em>Annals of Telecommunications, 68</em>(5-6), 307-315.
</li>
<li id="par13b">Park, E. &amp; Kim, K. J. (2013). User acceptance of long-term evolution (LTE) services: an application of extended technology acceptance model. <em>Program: Electronic Library and Information Systems, 47</em>(2), 188-205.
</li>
<li id="par07">Park, J., Yang, S. &amp; Lehto, X. (2007). Adoption of mobile technologies for Chinese consumers. <em>Journal of Electronic Commerce Research, 8</em>(3), 196-206.
</li>
<li id="ped05">Pedersen, P. E. (2005). Adoption of mobile Internet services: an exploratory study of mobile commerce early adopters. <em>Journal of Organizational Computing and Electronic Commerce, 15</em>(3), 203-222.
</li>
<li id="ram13">Ramírez-Correa, P., Rondán-Cataluña, F. J. &amp; Arenas-Gaitán, J. (2013). Exploring factors that affect the adoption of social networks services by generation Y: an empirical study in Chile. <em>Interciencia, 38</em>(9), 628-633.
</li>
<li id="rin05">Ringle, C.M., Wende, S. &amp; Will, A. (2005). <em>SmartPLS 2.0 (M3)</em>. Retrieved from http://www.smartpls.de (Archived by WebCite® at http://www.webcitation.org/6PNnyCV9a)
</li>
<li id="rog03">Rogers, E. M. (2003). <em>Diffusion of innovations</em>. (5th ed.) New York, NY: Simon and Schuster.
</li>
<li id="sch06">Schwartz, S. H. (2006). A theory of cultural value orientations: explication and applications. <em>Comparative Sociology, 5</em>(2-3), 137-182.
</li>
<li id="shi12a">Shin, D. H. (2012). Cross-analysis of usability and aesthetic in smart devices: what influences users' preferences? <em>Cross Cultural Management, 19</em>(4), 563-587.
</li>
<li id="shi12b">Shin, D. H. &amp; Choo, H. (2012). Exploring cross-cultural value structures with smartphones. <em>Journal of Global Information Management, 20</em>(2), 67-93.
</li>
<li id="ten05">Tenenhaus, M., Vinzi, V. E., Chatelin, Y. M. &amp; Lauro, C. (2005). PLS path modeling. <em>Computational Statistics &amp; Data Analysis, 48</em>(1), 159-205.
</li>
<li id="tho06">Thong, J. Y. L., Hong, S. J. &amp; Tam, K. Y. (2006). The effects of post-adoption beliefs on the expectation-confirmation model for information technology continuance. <em>International Journal of Human-Computer Studies, 64</em>(9), 799-810.
</li>
<li id="tho11">Thong, J. Y. L., Venkatesh, V., Xu, X., Hong, S. J. &amp; Tam, K. Y. (2011). Consumer acceptance of personal information and communication technology services. <em>IEEE Transactions on Engineering Management, 58</em>(4), 613-625.
</li>
<li id="ven08">Venkatesh, V. &amp; Bala, H. (2008). Technology acceptance model 3 and a research agenda on interventions. <em>Decision Sciences, 39</em>(2), 273-315.
</li>
<li id="ven00">Venkatesh, V. &amp; Davis, F. D. (2000). A theoretical extension of the technology acceptance model: four longitudinal field studies. <em>Management Science, 46</em>(2), 186-204.
</li>
<li id="ven03">Venkatesh, V., Morris, M. G., Davis, G. B. &amp; Davis, F. D. (2003). User acceptance of information technology: toward a unified view. <em>MIS Quarterly, 27</em>(3), 425-478.
</li>
<li id="ven12">Venkatesh, V., Thong, J. Y. L. &amp; Xu, X. (2012). Consumer acceptance and use of information technology: extending the unified theory of acceptance and use of technology. <em>MIS Quarterly, 36</em>(1), 157-178.
</li>
<li id="vla11">Vlachos, P. A., Giaglis, G., Lee, I. &amp; Vrechopoulos, A. P. (2011). Perceived electronic service quality: some preliminary results from a cross-national study in mobile Internet services. <em>International Journal of Human-Computer Interaction, 27</em>(3), 217-244.
</li>
<li id="wan10">Wang, H.-Y. &amp; Wang, S.-H. (2010). User acceptance of mobile Internet based on the unified theory of acceptance and use of technology: investigating the determinants and gender differences. <em>Social Behavior and Personality, 38</em>(3), 415-426.
</li>
<li id="wes10">West, J. &amp; Mace, M. (2010). Browsing as the killer app: explaining the rapid success of Apple's iPhone. <em>Telecommunications Policy, 34</em>(5-6), 270-286.
</li>
<li id="yam06">Yamakami, T. (2006). A social view on unsolicited bulk email impacts on mobile mail addresses. In <em>Seventh International Conference on Parallel and Distributed Computing, Applications and Technologies, 2006</em>. (pp. 295-299). Los Alamitos, CA: IEEE Computer Society.
</li>
<li id="yan12">Yang, S. Q., Lu, Y. B., Gupta, S. &amp; Cao, Y. Z. (2012). Does context matter? The impact of use context on mobile Internet adoption. <em>International Journal of Human-Computer Interaction, 28</em>(8), 530-541.
</li>
<li id="zho11a">Zhou, T. (2011a). An empirical examination of initial trust in mobile banking. <em>Internet Research, 21</em>(5), 527-540.
</li>
<li id="zho11b">Zhou, T. (2011b). The effect of initial trust on user adoption of mobile payment. <em>Information Development, 27</em>(4), 290-300.
</li>
<li id="zho10">Zhou, T., Lu, Y. &amp; Wang, B. (2010). Integrating TTF and UTAUT to explain mobile banking user adoption. <em>Computers in Human Behavior, 26</em>(4), 760-767.
</li>
</ul>

</section>

</article>

* * *

## Appendix: Measurement instrument

<table>

<tbody>

<tr>

<td colspan="2">Items <sup>(a)</sup></td>

</tr>

<tr>

<td colspan="2">_Behavioural intention (BI)_</td>

</tr>

<tr>

<td>BI1</td>

<td>I intend to continue using mobile Internet in the future.</td>

</tr>

<tr>

<td>BI2</td>

<td>I will always try to use mobile Internet in my daily life.</td>

</tr>

<tr>

<td>BI3</td>

<td>I plan to continue to use mobile Internet frequently.</td>

</tr>

<tr>

<td colspan="2">_Performance expectancy (PE)_</td>

</tr>

<tr>

<td>PE1</td>

<td>I find mobile Internet useful in my daily life.</td>

</tr>

<tr>

<td>PE2</td>

<td>Using mobile Internet helps me accomplish things more quickly.</td>

</tr>

<tr>

<td>PE3</td>

<td>Using mobile Internet increases my productivity.</td>

</tr>

<tr>

<td colspan="2">_Effort expectancy (EE)_</td>

</tr>

<tr>

<td>EE1</td>

<td>Learning how to use mobile Internet is easy for me.</td>

</tr>

<tr>

<td>EE2</td>

<td>My interaction with mobile Internet is clear and understandable.</td>

</tr>

<tr>

<td>EE3</td>

<td>I find mobile Internet easy to use.</td>

</tr>

<tr>

<td>EE4</td>

<td>It is easy for me to become skilful at using mobile Internet.</td>

</tr>

<tr>

<td colspan="2">_Social influence (SI)_</td>

</tr>

<tr>

<td>SI1</td>

<td>People who are important to me think that I should use mobile Internet</td>

</tr>

<tr>

<td>SI2</td>

<td>People who influence my behaviour think that I should use mobile Internet</td>

</tr>

<tr>

<td>SI3</td>

<td>People whose opinions I value prefer that I use mobile Internet</td>

</tr>

<tr>

<td colspan="2">_Facilitating conditions (FC)_</td>

</tr>

<tr>

<td>FC1</td>

<td>I have the knowledge necessary to use mobile Internet.</td>

</tr>

<tr>

<td>FC2</td>

<td>Mobile Internet is compatible with other technologies I use.</td>

</tr>

<tr>

<td>FC3</td>

<td>I feel comfortable using mobile Internet</td>

</tr>

<tr>

<td colspan="2">_Hedonic motivation (HM)_</td>

</tr>

<tr>

<td>HM1</td>

<td>Using mobile Internet is fun.</td>

</tr>

<tr>

<td>HM2</td>

<td>Using mobile Internet is enjoyable.</td>

</tr>

<tr>

<td>HM3</td>

<td>Using mobile Internet is very entertaining.</td>

</tr>

<tr>

<td colspan="2">_Habit (HT)_</td>

</tr>

<tr>

<td>HT1</td>

<td>The use of mobile Internet has become a habit for me.</td>

</tr>

<tr>

<td>HT2</td>

<td>I am addicted to using mobile Internet.</td>

</tr>

<tr>

<td>HT3</td>

<td>I must use mobile Internet.</td>

</tr>

<tr>

<td colspan="2">_Price value (PV)_</td>

</tr>

<tr>

<td>PV1</td>

<td>Mobile Internet is reasonably priced.</td>

</tr>

<tr>

<td>PV2</td>

<td>Mobile Internet is good value for money.</td>

</tr>

<tr>

<td>PV3</td>

<td>Overall, the use of mobile Internet delivers good value</td>

</tr>

<tr>

<td colspan="2">_Use behaviour (UB)_</td>

</tr>

<tr>

<td>USE1</td>

<td>Please choose your usage frequency for SMS</td>

</tr>

<tr>

<td>USE2</td>

<td>Please choose your usage frequency for games</td>

</tr>

<tr>

<td>USE3</td>

<td>Please choose your usage frequency for browsing Websites</td>

</tr>

<tr>

<td>USE4</td>

<td>Please choose your usage frequency for e-mail</td>

</tr>

<tr>

<td colspan="2">(a) All items were measured on a 7-point Likert scale. The scale ranged from “1-never” to “7-many times per day” for use1, use2, use3 and use4\. The scale ranged from “1-very strongly disagree” to “7-very strongly agree” for all other items.</td>

</tr>

</tbody>

</table>