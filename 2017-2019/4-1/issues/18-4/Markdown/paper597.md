<header>

#### vol. 18 no. 4, December, 2013

</header>

<article>

# Hobby-related information-seeking behaviour of highly dedicated online museum visitors

#### [Mette Skov](#author)  
Aalborg University, Department of Communication and Psychology,  
Nyhavnsgade 14, DK-9000 Aalborg, Denmark

#### Abstract

> **Introduction.** This paper explores the characteristics of online museum visitors in an everyday life, information-seeking context.  
> **Method.** A triangulation of research methods was applied. A Web questionnaire survey gave initial, quantitative information about online museum visitors to a military museum. Follow-up interviews (n = 24) obtained rich, qualitative data to validate and elaborate the characteristics of online museum visitors' information-seeking behaviour.  
> **Analysis.** Based on the serious leisure perspective, data analysis led to the identification of two different user groups named collectors and liberal arts enthusiasts. Analysis showed that the two hobby classes have distinct profiles including differences in the nature of their knowledge acquisition.  
> **Results.** Across the two hobby classes, participants can be characterised as special interest museum visitors pursuing a long-standing interest or hobby. The identified information needs were surprisingly well-defined known item needs and only few exploratory information needs were identified. Participants stressed the importance of personal channels and the social context of the hobby.  
> **Conclusions.** The present study contributes to the research area of everyday life information seeking within serious leisure. It also contributes to the emerging field of museum informatics by adding to the characteristics of the online museum visitor.

<section>

## Introduction

Studies of information-seeking behaviour have long been concerned with work-related environments and scientific users and students in particular ([Butterworth and Perkins 2006](#but06), [Hartel 2003](#har03), [Savolainen 1995](#sav95)). Savolainen proposed that non-job information seeking deserves equal attention and introduces the concept of everyday life information seeking. Since then, a rich variety of everyday life information seeking themes has been explored from a number of theoretical perspectives. This paper contributes to a growing body of research on the role of information in serious leisure by investigating the everyday life information seeking behaviour of online museum visitors, with the focus on what Booth ([1998](#boo98)) denotes as special interest museum visitors.

A current trend is to make museum collections widely accessible by digitising cultural heritage collections for the Internet, which builds on the idea of the visitor-centred museum ([Anderson 2004](#and04)). Online visits to museum Websites have become popular and some museums report that the number of online museum visitors exceeds the number of visitors to the physical museum (e.g. [Fantoni _et al._ 2012](#fan12)). As a result, the importance of extending the notion of museum visitation to also cover visits to museum Websites has recently been stressed ([Ellenbogen _et al._ 2008](#ell08), [Fantoni _et al._ 2012](#fan12), [Marty 2008](#mar08), [Marty 2011](#mar11)) together with a call for a broader research perspective on online museum visitation ([Ellenbogen _et al._ 2008](#ell08), [Falk _et al._ 2008](#fal08), [Marty 2008](#mar08)). Accordingly, this paper studies the characteristics of the online museum visitor and three factors are explored: information needs, seeking and use. Understanding online museum visitor behaviour is critical to the development of relevant and useful museum Websites. Further, the paper explores how to categorise online museum visitors within Stebbins's ([2007](#ste07)) taxonomy of classes of leisure pursuit, and links this to patterns in the role of information seeking. The following research questions guided the study:

1.  How can online museum visitors be categorised within Stebbins's taxonomy of classes of leisure pursuit?
2.  What characterises the information-seeking behaviour of online museum visitors? Particular attention was given to:
    *   their information-related leisure tasks and derived information needs
    *   the information channels and sources used.

The remainder of the paper is structured as follows. First, a review of the previous research on everyday life information seeking from a serious leisure perspective and studies of online museum visitor behaviour are presented. Next, the methodology of the empirical study is presented and the data collections methods regarding the Web questionnaire survey and the qualitative interviews are described. The following section presents and discusses the results of the study, and the final section concludes the article and makes recommendations for future research.

## Literature review

### Everyday life information seeking from a serious leisure perspective

Motivated by a need to elaborate the role of social and cultural factors that affect preferences for and use of information sources in everyday settings, Savolainen ([1995](#sav95)) introduced the model of everyday life information seeking. The model contains the concept of way of life referring to a person's everyday activities and the value the individual assigns to each of these activities. Based on these values, people decide the order in which these activities will be addressed in terms of information gathering ([Savolainen 1995](#sav95)). A way of life reflects the individual's major interests, such as hobbies. Thus, one way to operationalise _way of life_ is to look at the nature of hobbies. An analysis of hobbies can inform us of things people find most pleasant, plus the analysis can also point to information interests during leisure time. Since Savolainen introduced the everyday life information seeking model, a rich variety of themes has been explored from a number of theoretical perspectives. In the context of the present study, the area of serious leisure information seeking is particularly relevant as the online museum visitors can be categorised as hobbyists (see section on results).

The concept of serious leisure was coined by sociologist Robert Stebbins and can be defined as the ‘_systematic pursuit of an amateur, hobbyist, or volunteer activity sufficiently substantial and interesting in nature for the participant to find a career there in the acquisition and expressing of a combination of its special skills, knowledge, and experience_’ ([Stebbins 1994: 173](#ste94)). Serious leisure is an interdisciplinary concept, which was later introduced within library and information science research ([Fulton and Vondracek 2009](#ful09), [Hartel 2003](#har03), [Hartel 2006](#har06), [Stebbins 2009](#ste09)).

Hartel ([2003](#har03)) advocates the importance of serious leisure for research in information-seeking behaviour as serious leisure activities are personally cherished and socially important parts of everyday life and highly informational because they involve knowledge acquisition. According to Stebbins ([2007](#ste07)), there are three general forms of serious leisure: amateurism, volunteering and hobbies. In this paper, the focus is on hobbies. A hobby is the systematic and enduring pursuit of a reasonably evolved and specialised free-time activity. A hobby is practised for pleasure, without the goal, necessarily, of external rewards. A hobby is the most popular of the three forms of serious leisure and there are five hobby classes: collectors, makers and tinkerers, activity participants, players of sports and games and liberal arts enthusiasts ([Stebbins 2007](#ste07)). The hobby classes are self-explanatory except for the liberal arts enthusiast, who performs ‘_the systematic and fervent pursuit during free time of knowledge for its own sake_’ ([Stebbins 1994: 175](#ste94)). This definition indicates that the process of developing expertise is important and enjoyed by the liberal arts enthusiasts; however, they do not necessarily further implement their knowledge. The study of information seeking is especially of interest in relation to the hobby class of liberal arts enthusiasts, given their emphasis on knowledge acquisition ([Hartel 2003](#har03)).

Several empirical studies have explored information behaviour within leisure contexts including studies of pleasure reading ([Ross 1999](#ros99)), genealogy ([Fulton and Vondracek 2009](#ful09), [Yakel 2004](#yak04)), gourmet cooking ([Hartel 2006](#har06), [2010](#har10)), knitting ([Prigoda and McKenzie 2007](#pri07)), food blogging ([Cox and Blake 2011](#cox11)) and collecting ([Case 2009](#cas09), [Case 2010](#cas10), [Lee and Trace 2009](#lee09)). The two studies of hobbyist collectors are of particular interest since the majority of the participants in the present study are categorised as collectors. According to Stebbins, collectors ‘_develop a technical knowledge of the commercial, social, and physical circumstances in which their fancied items are acquired_’ ([1982: 261](#ste82)).

The big question, why do people collect, is thoroughly discussed by Case ([2009](#cas09)) in his article on the motivations, practices and information behaviour of coin collectors. He describes coin collecting as a highly information-intensive leisure-time pursuit wherein a broad array of information sources are used. Further, characteristics of coin collecting are mapped to four common themes of collecting with “_extension of the self_” being the major theme ([Case 2009: 742-745](#cas09)). Likewise, Lee and Trace ([2009](#lee09)) explore the intersection between information-seeking behaviour and serious leisure pursuits of toy rubber duck collectors. Building on Stebbins's serious leisure perspective, they suggest a typology of rubber duck collectors as social, casual or serious, and show how each collector type exhibits different information behaviour. Further, Lee and Trace identify the information needs of hobbyist collectors, and find that ‘_it is almost impossible to extricate information needs from object needs. Object needs are the driving force for most collectors, but object needs spawn information needs_’ [Lee and Trace 2009: 633](#lee09)). Together, the above studies of information behaviour in leisure contexts indicate that information behaviour during leisure, at least in some respects, differs from scholarly and professional contexts.

### Online museum visitors

Although many studies examine visitor motivation and behaviour in the physical museum, studies of how online visitors search and interact with digital museum resources remain few and scattered ([Fantoni _et al._ 2012](#fan12), [Goldman and Schaller 2004](#gol04), [Kravchyna 2002](#kra02), [Marty 2007](#mar07), [Marty 2008](#mar08), [Marty 2011](#mar11)). Web questionnaire surveys have been the predominant data collection method applied in these studies. While questionnaire surveys have proven useful in describing why online museum collections are used and by whom, they have limitations in explaining low response rates ([Goldman and Schaller 2004](#gol04), [Marty 2007](#mar07), [Marty 2008](#mar08)) and how users interact with rich museum content and the context of the online museum visit. Accordingly, the present study aims to add to the previous research by taking a broader perspective on online museum visitor behaviour by studying the intersection between information seeking and the serious leisure perspective. Applying this broader perspective is inspired by calls in the museum literature. Black ([2005](#bla05)) finds that the importance of learning in some shape has received an overwhelming emphasis in the museum-led literature. Yet people have many reasons for choosing one leisure activity over another and therefore Black ([2005](#bla05)) suggests applying a broader perspective and viewing the museum visit in a social recreational context wherein visitors are motivated principally by personal interests. Falk and colleagues ([Ellenbogen _et al._ 2008](#ell08), [Falk _et al._ 2008](#fal08)) examine the complexity of what drives and motivates a museum visit in the physical context and they call for research in virtual museum contexts. Also, Marty ([Marty 2008: 184](#mar08)) argues for a broader research perspective on online museum visitation:

> In order to address difficult questions about the use of online museum resources, museum professionals need to know more about the role those resources play in our visitors' lives outside of the museum. Shifting to the ‘museum in the life of the user perspective’ is difficult, but it will likely be the best way to get the clearest picture of our users' needs as well as whether their needs are being met.

When aiming to strengthen our understanding of online museum visitors' information-seeking behaviour, research on museum visitor motivation is an important point of departure. Visitor motivation and intentions are elements of the cultural and social context influencing how visitors search and interact with online museum resources. Goldman and Schaller's ([2004](#gol04)) literature review of the most common motivations for Website visits lists the following motivational categories:

*   gathering information for planning an upcoming visit to the physical museum (opening hours, admissions, etc.);
*   performing self-motivated research for specific content information;
*   doing assigned research (school or job assignments) for specific content information;
*   and engaging in casual browsing.

Recently, a fifth motive has been added by Fantoni _et al._ ([2012](#fan12)): “_make a transaction on the Website_”. Several studies show that planning an upcoming visit to the physical museum is the most frequently mentioned motivation for visiting a museum Website ([Goldman and Schaller 2004](#gol04), [Marty 2007](#mar07)). However, in line with Goldman and Schaller ([2004](#gol04)), the present study focuses on users who look at museum Websites for a content-based reason, and add to our knowledge of the motivational category of self-motivated research for specific content information. Finally, the concept of meaning-making provides an additional approach to understanding the museum visitor experience and motivation ([Silverman 1995](#sil95), [Weil 2002](#wei02)), stressing the museum visitor's active role in creating the meaning of museum objects and exhibitions. Objects displayed in an online exhibition or collection database do not themselves represent facts nor have any fixed or inherent meaning. Therefore (online), museum visitors' interaction with and understanding of a museum object depends on the individual. Meaning-making or the process by which those objects acquire meaning for individual members of the public will in each case ‘_involve the specific memories, expertise, viewpoint, assumptions and connections that the particular brings_’ ([Weil 2002: 212](#wei02)).

## Methodology

The user study was designed using a triangulation of research methods. A Web questionnaire survey gave initial, quantitative information about online museum visitors to a military museum. A subsequent user study consisted of two parts: 1) a user study on participants' interactions with a museum collection database focusing on information-searching behaviour ([Skov and Ingwersen, forthcoming](#skofo)), and 2) interviews with twenty-four online museum visitors to obtain rich, qualitative data to validate and elaborate the characteristics of online museum visitors in an everyday life information-seeking context. The present article reports findings from the Web questionnaire survey and qualitative interviews.

The study was carried out in the context of the National Museum of Military History (in short, the Military Museum) situated in Copenhagen, Denmark. The Military Museum is a museum of cultural history, covering the history of the Danish defence and development of weapons from the introduction of firearms to the present day. Their heterogeneous collections of approximately 200,000 museum objects cover a variety of different media. As part of the Military Museum's digitisation strategy, online access is provided to a part of the collections on their Website ([www.thm-online.dk](http://www.thm-online.dk)).

### Web questionnaire survey

The Web questionnaire survey served two purposes. First it provides initial information about online museum visitors' areas of interests, purpose of visit to the museum Website and preferred data elements as well as demographic data. Second, it recruited participants to the succeeding user study. The questionnaire was published on the Military Museum's Website, and, in addition, advertised in a relevant newsgroup and a printed journal. The questionnaire consisted of closed, pre-coded questions in combination with a few open-ended questions. As the target group of the questionnaire was the users of the Military Museum's collection database, the links to the questionnaire were placed on the homepage of the collection database and on a site providing access to a variety of online content. In line with Goldman and Schaller ([2004](#gol04)) and Marty ([2011](#mar11)), the aim was to reach respondents who visited the Website for a content-based reason, excluding the large number of people who visited the museum Website to find practical information (e.g. opening hours and admissions) about the physical museum. Analysis of survey data shows that compared with other surveys of online museum visitors ([e.g. Thomas and Carey 2005](#tho05)), few respondents (21.5%) indicate plan a visit to the museum as the purpose of a visit to the Military Museum's Website. Instead, the most frequent purpose of a visit relates to the museum's collections, which shows that the Web survey succeeded in reaching mainly the intended respondents. The online questionnaire was administered over two months, from February 2008 to April 2008\. A total of 153 respondents completed the questionnaire and in the following we focus on the 132 respondents who visited the Military Museum's Website in connection with their hobby or leisure interest area. Respondents were aged between sixteen and seventy-two with an average age of forty-six. Only two respondents were women.

The response rate is difficult to calculate in this Web survey because it is tricky to determine the size of the target population. An estimation of a response rate, however, can be calculated based on visitor numbers from Google analytics software. According to Google analytics, 1,897 unique visitors visited the Military Museum's collection database during the two-month period, resulting in an estimated response rate of 8%. This is a quite low response rate, which is an often discussed disadvantage of Web surveys ([Zhang 2000](#zha00)). Further, the use of online questionnaires with a self-selected sample has limitations because of difficulties in obtaining a random sample and, accordingly, the present study does not claim representativeness. A self-selected sample method was chosen as it is seen as the only possible way to reach the group of online museum visitors. This is especially the case in the present study, as it aims at going beyond a Web survey as the data collection method.

The quantitative data from the Web questionnaire survey were exported from the questionnaire software to the statistical software, SPSS, for further analysis. First, basic descriptive statistics were used to summarise and describe the data set by calculating percentage distributions. Next, chi-squared tests were applied to explore possible associations between two variables. Associations were tested between three independent variables (age, knowledge level and type of hobby) and relevant dependent variables. The Web survey is based on a self-selected sample and therefore the probability statistics (e.g. chi-squared tests) are not appropriate for making estimates from the collected data. Having said that, chi-squared tests were applied to support qualitative analysis of the interview data.

### Interviews

The interviews aimed to provide elaborate answers and illustrative examples and allowed participants to use their own words, which is important if one wishes to collect data on cognitive perceptions. Besides covering the areas of the questionnaire, more broadly based questions were also asked in the interviews, as very little is known about why users seek digital museum resources ([Ellenbogen _et al._ 2008](#ell08), [Marty 2007](#mar07)) or how they use and integrate digital museum resources into their everyday lives. Two earlier studies ([Hartel 2003](#har03), [Yakel 2004](#yak04)) addressing the information-seeking behaviour of non-professionals and drawing on the everyday life information seeking framework by Savolainen ([1995](#sav95)) inspired the design of the interview guide in relation to including these broader questions. The interview guide was structured according to the following four parts:

1.  Characteristic of hobby and/or area of interest
2.  Description of the critical incident
3.  Description of information sources and channels
4.  Description of information sharing and use

The critical incident technique ([Flanagan 1954](#fla54)) was used to trigger the participant to recall and explain his cognitive perceptions at the time of the event. Here, the critical incident was defined as the trigger of an information need or interest resulting in a virtual visit to the Military Museum's collection database. The interview focused mainly on the specific and recent incident when participants also filled in the Web questionnaire. By focusing on a specific and recent incident, we try to minimise a retrospective bias, reflecting the participants' limited ability to recall an incident and, accordingly, recount a faulty reconstruction of the phenomena of interest ([Bolger _et al._ 2003](#bol03)). This said, attention was also given to other incidents mentioned by interviewees to cover as broad, complex and diverse incidents as possible. As stated earlier, the twenty-four interviewees were recruited through the Web questionnaire survey. They were all men and aged between thirty-two and seventy-two with an average age of forty-nine. The majority (twenty) of the interviews took place in an office at the Military Museum's administration building, two sessions took place in participants' private homes and the last two were conducted at the participant's place of work. Seventeen of the twenty-four interviewees lived in the Copenhagen area while the remaining seven interviewees lived in other parts of Denmark. The interviews were voice-recorded and lasted on average thirty minutes. The interviews were transcribed and imported to the qualitative analysis software ATLAS.ti. The primary method of analysis was inductive content analysis, and no formal testing of hypotheses was attempted. From the beginning, the qualitative analysis focused on the identification of themes, classes of leisure pursuit, categories of work tasks, information needs and preferred data elements and sources. In addition, the coding scheme was elaborated and refined through several iterations of coding and analysis.

The following section on results shows how interview data are used to answer the first research question on how online museum visitors can be categorised within Stebbins's taxonomy of classes of leisure pursuit. The second research question, however, is answered by using a triangulation of both qualitative interview data and quantitative survey data.

## Results

### Hobby classes of collectors and liberal arts enthusiasts

Data from the Web questionnaire provide a firsthand impression of the leisure context as to visitors' interest areas. Table 1 shows that the two most often indicated interest areas are _a specific object type_ (indicated by 61.4% of respondents) and _defence and military history in general_ (indicated by 57.6% of respondents). These two answer categories, although overlapping, often cover two rather different approaches to defence and military history as a leisure interest. Table 1 also shows that respondents are interested in different historic aspects, such as specific defence and military history events (31.8%), a specific defence and military history era (15.9%) or history in general within a time period (12.1%). Finally, small groups of respondents are interested in activities of historical re-enactment, genealogy or making of military miniatures.

<table><caption>Table 1: Characteristics of hobby or interest area.  
(Respondents were allowed to indicate more than one answer)</caption>

<tbody>

<tr>

<th rowspan="2">Description of hobby or interest area</th>

<th>All hobbyist respondents</th>

<th colspan="3">Respondents participating in qualitative interviews</th>

</tr>

<tr>

<th>n = 132%</th>

<th>All n = 24%</th>

<th>Collectors n = 16%</th>

<th>Liberal arts enthusiasts n = 8%</th>

</tr>

<tr>

<td>A specific object type</td>

<td>61.4</td>

<td>63.3</td>

<td>93.8</td>

<td>50.0</td>

</tr>

<tr>

<td>Defence and military history in general</td>

<td>57.6</td>

<td>53.3</td>

<td>56.3</td>

<td>87.5</td>

</tr>

<tr>

<td>Specific defence and military history event</td>

<td>31.8</td>

<td>20.0</td>

<td>25.0</td>

<td>25.0</td>

</tr>

<tr>

<td>Specific period of defence and military history</td>

<td>15.9</td>

<td>13.3</td>

<td>25.0</td>

<td>0.0</td>

</tr>

<tr>

<td>History in general within a time period</td>

<td>12.1</td>

<td>20.0</td>

<td>31.3</td>

<td>12.5</td>

</tr>

<tr>

<td>Re-enactment</td>

<td>7.6</td>

<td>6.7</td>

<td>6.3</td>

<td>12.5</td>

</tr>

<tr>

<td>Military miniaturism</td>

<td>6.8</td>

<td>3.3</td>

<td>0.0</td>

<td>12.5</td>

</tr>

<tr>

<td>Local history</td>

<td>4.5</td>

<td>0.0</td>

<td>0.0</td>

<td>0.0</td>

</tr>

<tr>

<td>Genealogy</td>

<td>3.8</td>

<td>3.3</td>

<td>6.3</td>

<td>0.0</td>

</tr>

<tr>

<td>Other</td>

<td>13.6</td>

<td>0.0</td>

<td>0.0</td>

<td>0.0</td>

</tr>

</tbody>

</table>

The majority of the questionnaire respondents have either _some experience_ or are _highly experienced_ in relation to their hobby or interest area (see Table 2). This also holds for participants in the user study, where no novices were recruited. All participants in the user study (except one, participant V) have had this hobby for more than ten years, and at least the ten highly experienced participants can be categorised as lay-experts within their sub-fields. Using Booth's ([1998](#boo98)) terminology, our user study participants are special interest museum visitors pursuing a long-standing interest or hobby. From an information-seeking perspective, this information-intensive type of hobbyist is of particular interest.

<table><caption>Table 2: Survey respondents' knowledge level.  
Web survey: n = 132, user study: n = 24</caption>

<tbody>

<tr>

<th>Respondents' knowledge level in relation to their hobby or interest area  
</th>

<th>Web survey  
%</th>

<th>User study  
%</th>

</tr>

<tr>

<td>I'm highly experienced and have extensive background knowledge</td>

<td>30.3</td>

<td>41.7</td>

</tr>

<tr>

<td>I have some experience and background knowledge</td>

<td>57.6</td>

<td>54.2</td>

</tr>

<tr>

<td>I'm a novice with little knowledge within this area</td>

<td>10.6</td>

<td>0.0</td>

</tr>

<tr>

<td>I don't know</td>

<td>1.5</td>

<td>4.2</td>

</tr>

</tbody>

</table>

As a first step to characterising the information-seeking behaviour of online museum visitors, the twenty-four user study participants are categorised within Stebbins's taxonomy of classes of leisure pursuit. Table 3 provides examples of categorisations and short descriptions of participants' hobbies. Based on Stebbins's ([1994](#ste94), [2007](#ste07)) theoretical work on the serious leisure perspective, participants are categorised according to hobby class.

<table><caption>Table 3: Examples of categorisation and a short description of participants' hobbies</caption>

<tbody>

<tr>

<th>Hobby classes</th>

<th>Short description of participants' hobbies or interests</th>

</tr>

<tr>

<td>Collectors</td>

<td>Collector of edged weapons and powder horns. Especially interested in regimental and proof markings on weapons (participant A).  
Collector of photographs of military uniforms from about 1864 to 1914 (participant P).  
Collector of Danish military hand weapons and edged weapons. Primarily interested in the period of the First and Second War of Schleswig (1848–1851 and 1864). Also collector of model trains, pocket watches, old cars etc. (participant R).  
Collector of hand weapons. Participates in historical black powder shooting (participant S).  
Collector of ammunition primarily from the period 1864–1945\. Repairs historical weapons (participant U).</td>

</tr>

<tr>

<td colspan="2">

* * *

</td>

</tr>

<tr>

<td>Liberal arts enthusiasts</td>

<td>Broad interest in nineteenth century military history, chemistry and meteorology. Also collector of a few edged weapons (participant B).  
Broad interest in the Second World War. Also a sport shooter (participant V).  
Primarily interested in Danish colonial history (participant X).  
General military history interest and specifically German military history from 1700 forward. Also a collector of Second World War artefacts (participant Y).</td>

</tr>

</tbody>

</table>

The results show a clear distinction between two primary groups of hobbyists. Collectors form the largest group and liberal arts enthusiasts of defence and military history the other main group. In general, the interviews show how collectors are focused on building, developing and maintaining their private collections. Objects are the collectors' primary interest, while the historical context is secondary. This is reflected in the following quotes of how collectors define their interest area:

> [I'm interested in] Danish military weapons. The technical line of development. Not specifically the Battle of Isted [a specific battle in 1850] and what decisions were made and why they lost. That is interesting entertainment in connection with pleasure reading. But it is the weapons that are interesting.(Participant F: line 21)

> The historical eras [in the collection database] suggest that you are interested in a historical period. That you are interested in the political situation and from there you orient yourself towards the detail level of objects. My interest goes the other way around. I become fascinated by an object and start by examining the object and then I look into how it has been used and its historical relations. (Participant Q: line 96)

Collectors pursue technical information on the commercial, social and physical circumstances in which items are acquired along with knowledge, thereby providing a broad understanding of the items' historical and contemporary production and use. Thus, the present study confirms the findings by Lee and Trace ([2009](#lee09)) that with hobbyist collectors, it is difficult to extricate information needs from object needs, and that object needs are the driving force. Liberal arts enthusiasts, on the other hand, define their hobby as a broader historical interest and seek broad and humanising knowledge, while the objects are secondary:

> The objects as such are less important to me because I'm interested in the objects in a historical context. How the objects have been used in a human context. How have they been used, where have they been used, by whom, and what have they caused. The colonial history: 'We have the Maxim gun and they have not'. (Participant X: line 91)

> I'm interested in military history from a general historic point of view. That is, how the military history development affected the general history development. Besides that, I'm interested in German military history from 1700 and forward. I would say … social and structural war history and its influence… Objects become interesting because they help explain how some weapons, that apparently were judged decisive, were used. (Participant Y: line 20)

The above two quotes illustrate that a broad historic interest, and not object needs, are the driving force for liberal art enthusiasts. These differences are reflected in the Web questionnaire data: more collectors than liberal arts enthusiasts indicate an object-related primary purpose of visit (Table 1, a specific object type), whereas more liberal arts enthusiasts indicate a broad topical purpose of visit (Table 1, Defence and military history in general). However, a chi-squared test shows only a low level of significance between the two groups in relation to this variable (significance level p = 0.15, df = 1). The boundaries between hobby classes observed in the case study are overlapping. That is, six of the eight liberal art enthusiasts are also categorised as collectors (see for example the description of participant B in Table 3). However, the interviews show how liberal arts enthusiasts emphasise the historical perspective and, accordingly, the collection of objects is secondary. This can explain why only 50% of liberal arts enthusiasts indicate that they are interested in a specific type of object, whereas 93.8% of collectors pursue this interest (see Table 1). Like liberal arts enthusiasts, collectors also pursue historical information but with a narrower and more object-centred focus. Accordingly, more liberal arts enthusiasts than collectors indicate an interest in _defence and military history in general_ (see Table 1). This characteristic applies well to Stebbins's proposal that the difference between the two classes of hobbies is partly the nature of knowledge acquisition. According to Stebbins ([1982](#ste82)), collectors pursue technical knowledge of the commercial, social and physical circumstances in which items are acquired along with knowledge providing a broad understanding of the items' historical and contemporary production and use. Liberal arts enthusiasts, on the other hand, seek broad and humanising knowledge ([Stebbins 1994](#ste94)). This difference in knowledge acquisition is further discussed in the following, as it is reflected in both military history hobbyists' leisure tasks and derived information needs.

The present study focuses on the two main hobby classes identified (collectors and liberal arts enthusiasts). However, the three remaining hobby classes ([Stebbins 2007](#ste07)) were also identified among the user study participants: _makers and tinkerers_ (one participant repairs historical weapons), _activity participants_ (four hunters) and _players of sports and games_ (six participants are sport shooters with modern and/or historical weapons). Participants are all either collectors or liberal arts enthusiasts as well, and their information-related tasks and derived information needs are mainly concerned with these hobbies. Consequently, this study views their activities as related to either collecting or the hobby of liberal arts enthusiasts.

### Leisure tasks and derived information needs

The critical incident technique helped establish a relation between hobby tasks undertaken by participants of the two hobby classes and derived information needs. Thirty-nine critical incidents and derived information needs were identified in the qualitative interviews. Information-related leisure tasks or interest of collectors and liberal arts enthusiasts are shown in Table 4 together with examples of information needs.

The main leisure task of collectors is focused on building, developing and maintaining private collections. Three information-related sub-tasks were identified. Firstly, all collectors in the user study build and maintain personal archives to document and contextualise objects in their private collections. The term archive covers a variety of repositories, either paper or computer based, from a few sheets of paper to extensive material collected through decades. Twenty-two of the thirty-nine information needs identified through the critical incident method relate to building and maintaining archives on objects in private collections. Of these, the majority (eighteen) are known-item needs, three are exploratory item needs, and a single information need is categorised as known-topic. _To build and maintain archives for a wider audience_ is the second task type of collectors. Here archive material is made available for a wider audience, for example by publishing it on the Web. Five user study participants (participants A, F, I, J and U) perform this second leisure task. The study shows that, whereas the first task type is initiated by the task performers themselves and performed in solitude, the second task type is both initiated and performed as a team effort. Finally, the interviews point to sharing and communicating information as an integrated part of being a collector.

The main leisure task of liberal arts enthusiasts is to pursue their interest within defence and military history. This is in line with Stebbins's description of the development of expertise as important and enjoyed, especially by liberal arts enthusiasts; however, they do not necessarily further implement their knowledge. Instead they pursue knowledge for its own sake ([Stebbins 1994](#ste94)). The two information-related sub-tasks of this group are: 1) to pursue this leisure interest and 2) to share information related to the interest area (see Table 4). Summing up, the majority of information needs identified through collectors' descriptions of critical incidents were known-item needs. Three critical incidents of liberal arts enthusiasts were identified and the derived information needs were all exploratory topical needs. Given that hobbyists' information-seeking behaviour is driven by interest and a clear end-goal may be lacking, the information needs identified were surprisingly well-defined. This said, the analysis also indicated results supporting exploratory seeking behaviour. Seven highly exploratory information needs were identified in the critical incidents, which cannot be confined to either hobby class or information need type.

The analysis of participants' hobby areas, their leisure tasks and derived information needs shows that a leisure task is rarely assigned. Instead, it is constructed intellectually by the actor. It is the interest, personal enrichment, addiction or compulsion etc. of the hobby that motivates collecting ([Case 2009](#cas09)) and initiates the information-seeking process.

<table><caption>Table 4: Information-related leisure tasks and derived information needs</caption>

<tbody>

<tr>

<th>Hobby class  
</th>

<th>Information-related leisure tasks or interests</th>

<th>Examples of hobbyists' information needs</th>

</tr>

<tr>

<td rowspan="3">Collectors</td>

<td>Build and maintain personal archives on private collections</td>

<td>

Find object-related information, e.g.:  
- Find information on wheel-lock gun from 1611  
- Find information on pistols related to 1750–1864</td>

</tr>

<tr>

<td>Information sharing: contribute to shared/non-personal archives</td>

<td>

- Verification of dragoon pistol model 1741 (contributing to a private virtual museum on arms and armour)</td>

</tr>

<tr>

<td>Information sharing: discuss with peers, write books and post articles on news groups</td>

<td>

- Seek information on King Christian the Tenth's hunting weapons (writing a journal article)</td>

</tr>

<tr>

<td colspan="3">

* * *

</td>

</tr>

<tr>

<td rowspan="2">Liberal arts enthusiasts</td>

<td>Pursue general interest</td>

<td>

Collect historical information on a time period or historical event, e.g.:  
- Pursue general interest in uniforms from 1700 by browsing through online exhibition  
- Pursue interest in historical role-playing by finding information on edged weapons</td>

</tr>

<tr>

<td>Information sharing: discuss with peers, write books and post articles on news groups</td>

<td>

- None identified</td>

</tr>

<tr>

<td colspan="3">

* * *

</td>

</tr>

<tr>

<td>Across hobby classes</td>

<td>General exploration of hobby or interest</td>

<td>Exploratory curiosity-driven browsing/not looking for anything specific</td>

</tr>

</tbody>

</table>

To further understand participants' motivations for collecting objects and object-related information, they were asked to explain why they collect. Very tellingly, participant X refers to the phrase “homo collecticus” (the collecting man), and the following two quotes reflect the appreciation of the objects:

> Never ask a collector, why he collects. He doesn't know. That is just the way it is. There is something fascinating about it.(Participant O: line 35)

> I have collected objects since I was a small boy. I have also collected stamps. I have collected everything… Over the years, I have changed focus and I have been interested in many things. I have collected English objects, Scottish and German objects – not 1939–45 – but the First World War and earlier... At one time, I collected objects from both the army and the marines, but that was too much. I had 350 caps, I had uniforms, 100 military decorations, badges and buttons, a collection of military photographs and other military papers and all sorts of things … (Participant M: line 23–24)

In his literature review on the complexity of what motivates collectors, Case ([2009](#cas09)) points to several motives for collecting things, e.g. addiction or compulsion, qualities intrinsic to the object (e.g. aesthetics, rarity) and effort expended to acquire or maintain the objects. Further, when asked how object-related information is used, the two following quotes show that the information is not necessarily further implemented but collected for its own sake:

> That is the awful part of it. I don't use it … The information is nice to have. I'm curious. (Participant S: line 82)

> Use? You mean, whether I write articles or so? No. It is stored on the computer. It is nice to know… It is a way to satisfy my curiosity. (Participant X: line 75–76)

The quotes illustrate how information-seeking activities are not necessarily clearly end-goal driven. Instead, information may be collected for its own sake, that is, to pursue defence and military history or build and maintain knowledge related to a private collection.

### Use of information channels and sources

Results from the Web questionnaire survey show that respondents use a variety of information sources and channels when pursuing their defence and military history hobby. On average, each respondent indicated 3.9 information sources. The most often mentioned source is _people who share the same hobby or interest area_, followed by _the Internet in general_ and _own book collection_ (see Table 5). Chi-squared tests were used to further explore differences between use of sources and the following three independent variables: 1) age, 2) knowledge level and 3) hobby class. The chi-squared tests (significance level p = 0.15, df = 1) revealed a weaker tendency that _family and friends_ and electronic sources (_discussion groups_, _blogs_ and _the Internet in general_) are used more by the group of younger respondents (age 16 to 45) than by the group of older respondents (age 46 to 84). On the other hand, the group of older respondents uses significantly more _special and/or research libraries_ and their _own book collection_, than do younger respondents (significance level p = 0.05, df = 1). Together with archives, respondents with extensive experience likewise use these last two sources significantly more than novices. Novices, in contrast, use _the Internet in general_ significantly more than respondents with extensive experience (significance level p = 0.05, df = 3). Finally, the results indicate that the sources listed in the Web questionnaire are equally relevant to and used by both hobby classes (collectors and liberal arts enthusiasts).

<table><caption>Table 5: Results from Web survey on information sources and channels</caption>

<tbody>

<tr>

<th>Where do you find information related to your hobby? (n = 132)  
</th>

<th>%</th>

</tr>

<tr>

<td>Personal channels:</td>

<td></td>

</tr>

<tr>

<td>- Other people who share the same hobby or interest area</td>

<td>75.8</td>

</tr>

<tr>

<td>- Family and frieds</td>

<td>15.2</td>

</tr>

<tr>

<td colspan="2">

* * *

</td>

</tr>

<tr>

<td>Electronic channels:</td>

<td></td>

</tr>

<tr>

<td>- The Internet in general</td>

<td>67.4</td>

</tr>

<tr>

<td>- Discussion groups and/or mailing lists on the Internet</td>

<td>40.2</td>

</tr>

<tr>

<td>- Documentary programs on TV or DVDs</td>

<td>28.0</td>

</tr>

<tr>

<td>- Blogs</td>

<td>9.8</td>

</tr>

<tr>

<td colspan="2">

* * *

</td>

</tr>

<tr>

<td>Written, non-electronic channels:</td>

<td></td>

</tr>

<tr>

<td>- Own book collection</td>

<td>53.8</td>

</tr>

<tr>

<td>- Special libraries and/or research libraries</td>

<td>35.6</td>

</tr>

<tr>

<td>- Public libraries</td>

<td>25.8</td>

</tr>

<tr>

<td>- Archives</td>

<td>24.2</td>

</tr>

<tr>

<td colspan="2">

* * *

</td>

</tr>

<tr>

<td>Other:</td>

<td></td>

</tr>

<tr>

<td>- Other</td>

<td>11.4</td>

</tr>

<tr>

<td>- I don't know</td>

<td>1.5</td>

</tr>

<tr>

<td colspan="2">

* * *

</td>

</tr>

<tr>

<td colspan="2">

Legend: Respondents were allowed to indicate more than one answer. Personal channels cover all communication forms that involve interaction between persons, such as face-to-face or telephone conversations, meetings and e-mails. Written non-electronic channels include public libraries, special and/or research libraries and archives as well as internal sources like participants' own book collection</td>

</tr>

</tbody>

</table>

In the interviews, participants were asked to describe their hobby-related use of information channels and sources, both in general and as part of their description of critical incidents. The three most often mentioned information sources are the same as in the Web questionnaire survey. Thus the interviews confirm and validate the findings from the Web questionnaire. Personal channels and the social context of a hobby are highly important to user study participants, and hobby-related discussions with peers are often mentioned as rewarding:

> We can talk for hours. For example, an acquaintance of mine bought a pistol, a flint lock pistol, in a very poor condition. We have discussed this pistol for hours because it is atypical. What if …? Could it be …? Now that I have cleaned it, I can see that … It is wonderful to have these conversations. It is the fun part of it. It is much more fun than actually having the objects. (Participant A: line 88)

In this way we confirm earlier studies of the information behaviour of collectors and in other leisure contexts by pointing to the importance of the social aspect (e.g., [Case 2009](#cas09), [Fulton and Vondracek 2009](#ful09), [Lee and Trace 2009](#lee09), [Prigoda and McKenzie 2007](#pri07), [Yakel 2004](#yak04)). Communication through personal channels takes place either face to face, by telephone or e-mail. It is mainly based on informal contacts. However, also more formalised personal contacts are mentioned like membership of different associations related to defence and military history. Participation in these formalised organisations is often restricted according to individual interest areas. Participants selectively choose to attend meetings, lectures or trips based on what specifically relates to their interest areas. A parallel can be drawn with Butterworth and Perkin's ([2006](#but06)) study of non-professional information use of personal history researchers. Their study describes personal history researchers' participation in organisations (e.g. family and local history research groups) as sporadic and boundaries between organisations as ill defined.

In relation to written, non-electronic channels, participants' private book collections are the most frequently used source supplemented with occasional use of books from special or public libraries. Four of the participants explicitly explained how they preferred books to the Internet because of the general low-quality content on Websites related to their interest areas. As an information source, the Internet is, however, the most often indicated electronic information channel and all participants use the Internet for both seeking and giving hobby-related information. The interviews reflect how participants are prepared to put a considerable effort into locating, searching and extracting information from Internet sources. For example, two participants use Websites in languages they do not know to explore their interest area. Supported by visual data elements, dictionaries and online language translation tools, they are able to extract relevant information. Likewise, four participants explain how they daily or weekly scan Websites of auction houses, discussion groups and hobby Websites to keep up to date. Finally, in relation to discussion groups and mailing lists on the Internet, many of the participants put considerable effort into reading, writing and answering others' posts. In general, the participants are critical of information located on the Internet and use more than one source to verify information.

## Discussion and conclusion

Previous studies of information seeking in relation to hobbies have mainly focused on a single hobby class like collecting ([Case 2009](#cas09), [Case 2010](#cas10), [Lee and Trace 2009](#lee09)) or a central activity like pleasure reading ([Ross 1999](#ros99)), gourmet cooking ([Hartel 2006](#har06), [2010](#har10)) or genealogy ([Fulton and Vondracek 2009](#ful09), [Yakel 2004](#yak04)). The present study differs from these studies as the starting point is a specific Web-based system (the Military Museum's collection database). This explains the more varied group of participants that was mapped to primarily two of Stebbins's hobby classes: collectors and liberal arts enthusiasts. Although overlapping, the two hobby classes have distinct profiles including the nature of knowledge acquisition. Both data from the Web questionnaire and the qualitative interviews showed that objects are collectors' primary interest, while the historical context is secondary. Liberal arts enthusiasts, on the other hand, define their hobby as a broader historical interest, while the objects are secondary. The identified information needs were surprisingly well-defined known item needs and only a few exploratory information needs were identified. Across the two hobby classes, the process of searching for hobby-related information encompasses much more than just casual searching of the Internet or browsing books on the topic. In this regard, Savolainen's ([1995](#sav95)) study concerning the role of the _way of life_ in information seeking provides a useful framework for understanding participants' information-seeking activities as an integrated component of everyday life. Participants' dedication, long-standing interests and the often considerable time spent on the hobby indicate that their hobbies are integrated components of everyday practices. In relation to information sources and channels, participants stressed the importance of personal channels and the social context of the hobby. Similar reliance on social networks has been identified in other studies of everyday life information seeking and leisure contexts ([Case 2009](#cas09), [Lee and Trace 2009](#lee09), [Yakel 2004](#yak04)). Across the two hobby classes, user study participants can be characterised as special-interest museum visitors pursuing a long-standing interest or hobby. Participants saw the online and physical museum as complementary. The interviews reflect that the original, authentic museum object exhibited in the physical museum represents something unique, which cannot be replaced by a virtual reproduction. On the other hand several interviewees point to advantages of the online collection database such as flexible and easy access from home, background information, and photos with zoom function etc. ([Skov 2009](#sko09)).

Based on a primarily qualitative research design, the present study was conducted as an exploratory case study. The methodical approach does not claim representativeness. Instead, the case study approach is an appropriate method for investigating real-life phenomena with a large variety of factors and relationships. The aim was to arrive at a comprehensive understanding of the information-seeking behaviour of the online museum visitor, and thus adding to earlier, primarily quantitative studies. The present study contributes confirmatory data to the research area of everyday life information seeking within serious leisure. The results also contribute to the emerging field of museum informatics ([Marty _et al._ 2003](#mar03)) by adding to the characteristics of the online museum visitor, e.g. visitor motivation and the leisure context of the museum visit, thereby answering a call for a broader research perspective on virtual museum contexts ([Ellenbogen _et al._ 2008](#ell08), [Falk _et al._ 2008](#fal08), [Marty 2008](#mar08)).

The present case study focuses on a small museum collection. Future research should encompass a more realistic research design on user interaction with larger cultural heritage collections, e.g. the Europeana portal ([www.europeana.eu](http://www.europeana.eu)) providing access to millions of European cultural heritage objects. Further, participants in the present study were characterised as special-interest museum visitors (with medium or extensive background knowledge) pursuing a long-standing interest or hobby. Data from the Web questionnaire show that the Military Museum's collection database is also used by novice users. It would be interesting to improve our understanding of user groups including novice users, e.g. from a combined seeking and life-long learning perspective.

## Acknowledgements

I would like to thank the staff at the National Museum of Military History who introduced me to the collections, engaged in discussions on users and use of online museum collections, and who kindly agreed to link to this survey. I would also like to thank my former PhD supervisor, Professor Emeritus Peter Ingwersen, for his support and valuable advice in regard to this research project.

## About the author

Mette Skov is an Assistant Professor in the Department of Communication and Psychology, Aalborg University, Denmark. She received her Master and PhD from the Royal School of Library and Information Science in Copenhagen, Denmark. Her research focuses on information seeking behaviour and interactive information retrieval in relation to digital cultural heritage. She can be contacted at: [skov@hum.aau.dk](mailto:skov@hum.aau.dk)

<section>

## References

*   Anderson, G. (2004). _Reinventing the museum: historical and contemporary perspectives on the paradigm shift_. Lanham, MD: Altamira Pr.
*   Bolger, N., Davis, A. & Rafaeli, E. (2003). Diary methods: capturing life as it is lived. _Annual Reviews in Psychology_, **54**, 579–616.
*   Booth, B. (1998). Understanding the information needs of visitors to museums. _Museum Management and Curatorship_, **17**(2), 139–157.
*   Black, G. (2005). _The engaging museum: developing museums for visitor involvement_. London: Routledge.
*   Butterworth, R., & Perkins, V.D. (2006). Using the information seeking and retrieval framework to analyse non-professional information use. In I. Ruthven, P. Borlund, P. Ingwersen, N. J. Belkin, A. Tombros & P. Vakkari (Eds.), _Information interaction in context: International Symposium on Information Interaction in Context (IIiX 2006)_, (pp. 275–287). New York, NY: ACM Press.
*   Case, D. (2009). Serial collecting as leisure, and coin collecting in particular. _Library Trends_, **57**(4), 729–752.
*   Case, D. (2010). [A model of the information seeking and decision making of online coin buyers](http://www.webcitation.org/6KO9fMzIu). _Information Research_, **15**(4). Retrieved 10 October, 2013 from http://informationr.net/ir/15-4/paper448.html (Archived by WebCite® at http://www.webcitation.org/6KO9fMzIu)
*   Cox, A.M. & Blake, M.K. (2011). Information and food blogging as serious leisure. _Aslib Proceedings_, **63**(2/3), 204–220.
*   Ellenbogen, K., Falk, J. & Goldman, K.H. (2008). Understanding the motivations of museum audiences. In P.F. Marty & K.B. Jones (Eds.) _Museum informatics: people, information, and technology in museums._ (pp. 187–194). New York, NY: Routledge.
*   Falk, J.H., Heimlich, J. & Bronnenkant, K. (2008). Using identity-related visit motivations as a tool for understanding adult zoo and aquarium visitor's meaning making. _Curator: The Museum Journal_, **51**(1), 55–80.
*   Fantoni, S.F., Stein, R. & Bowman, G. (2012). [Exploring the relationship between visitor motivation and engagement in online museum audiences.](http://www.webcitation.org/6KXiiKZWP) In J. Trant and D. Bearman (Eds.). _Museums and the Web 2012: proceedings_. Toronto: Archives & Museum Informatics. Retrieved 18 March, 2013 from http://www.museumsandtheweb.com/mw2012/papers/ exploring_the_relationship_between_visitor_mot (Archived by WebCite® at http://www.webcitation.org/6KXiiKZWP)
*   Flanagan, J. C. (1954). The critical incident technique. _Psychological Bulletin_, **51**(4), 327-358.
*   Fulton, C. & Vondracek, R. (2009). Introduction: pleasurable pursuits: leisure and LIS research. _Library Trends_, **57**(4), 611–617.
*   Goldman, K. H. & Schaller, D. (2004). [Exploring motivational factors and visitor satisfaction in on-line museum visits.](http://www.webcitation.org/6KXj9ha70) In D. Bearman & J. Trant (Eds.). _Museums and the web 2004: Proceedings_. Toronto: Archives & Museum Informatics. Retrieved 10 October, 2013 from http://www.archimuse.com/mw2004/papers/haleyGoldman/haleyGoldman.html (Archived by WebCite® at http://www.webcitation.org/6KXj9ha70)
*   Hartel, J. (2003). The serious leisure frontier in library and information science: hobby domains. _Knowledge Organization_, **30**(3-4), 228-238.
*   Hartel, J. (2006). [Information activities and resources in an episode of gourmet cooking](http://www.webcitation.org/6KXhcUvHb) . Information Research, **12**(1), paper 282 Retrieved 10 October, 2013 from http://informationr.net/ir/12-1/paper282.html (Archived by WebCite® at http://www.webcitation.org/6KXhcUvHb)
*   Hartel, J. (2010). Managing documents at home for serious leisure: a case study of the hobby of gourmet cooking. _Journal of Documentation_, **66**(6), 847–874.
*   Kravchyna, V. & Hastings, S. (2002). [Informational value of museum web sites.](http://www.webcitation.org/6KXjqDda1) _First Monday_, **7**(2). Retrieved 10 October, 2013 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/rt/printerFriendly/929/851 (Archived by WebCite® at http://www.webcitation.org/6KXjqDda1)
*   Lee, C.P. & Trace, C. (2009). The role of information in a community of hobbyist collectors. _Journal of the American Society for Information Science and Technology_, **60**(3), 621–637.
*   Marty, P.F. (2007). Museum websites and museum visitors: before and after the museum visit. _Museum Management and Curatorship_, **22**(4), 337–360.
*   Marty, P.F. (2008). Museum websites and museum visitors: digital museum resources and their use. _Museum Management and Curatorship_, **23**(1), 81–99.
*   Marty, P.F. (2011). My lost museum: user expectations and motivations for creating personal digital collections on museum websites. _Library & Information Science Research_, **33**(3), 211–219.
*   Marty, P.F., Rayward, W.B. & Twidale, M.B. (2003). Museum informatics. _Annual Review of Information Science and Technology_, **37**, 259–294.
*   Prigoda, E. & McKenzie, P.J. (2007). Purls of wisdom: a collectivist study of human information behaviour in a public library knitting group. _Journal of Documentation_, **63**(1), 90–114.
*   Ross, C.S. (1999). Finding without seeking: the information encounter in the context of reading for pleasure. _Information Processing and Management_, **35**(6), 783–799.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of way of life. _Library & Information Science Research_, **17**(3), 259–294.
*   Silverman, L.H. (1995). Visitor meaning-making in museums for a new age. _Curator_, **38**(3), 161-170.
*   Skov, M. (2009). [The reinvented museum: exploring information seeking behaviour in a digital museum context.](http://www.webcitation.org/6KXjwGOHe) Unpublished doctoral thesis, Royal School of Library and Information Science, Copenhagen, Denmark. Retrieved 10 October, 2013 from http://pure.iva.dk/ws/files/30768221/MetteSkovThesis.pdf (Archived by WebCite® at http://www.webcitation.org/6KXjwGOHe)
*   Skov, M. & Ingwersen, P. (forthcoming). Museum web search behavior of special interest visitors. _Library and Information Science Research_
*   Stebbins, R.A. (1982). Serious leisure: a conceptual statement. _Pacific Sociological Review_, **25**(2), 251–272.
*   Stebbins, R.A. (1994). The liberal arts hobbies: a neglected subtype of serious leisure. _Society and Leisure_, **17**(1), 173–186.
*   Stebbins, R.A. (2007). _Serious leisure: a perspective for our time_. New Brunswick, NJ: Transaction Publishers.
*   Stebbins, R. (2009). Leisure and its relationship to library and information science: bridging the gap. _Library Trends_, **57**(4), 618–631.
*   Thomas, W.A. & Carey, S. (2005). [Actual/virtual visits: what are the links?](http://www.webcitation.org/6KXjbUYG2) In J. Trant & D. Bearman (Eds.), _Museums and the web 2005: Proceedings._ Toronto: Archives and Museum Informatics. Retrieved 10 October, 2013 from http://www.museumsandtheweb.com/mw2005/papers/thomas/thomas.html (Archived by WebCite® at http://www.webcitation.org/6KXjbUYG2)
*   Weil, S. E. (2002). _Making museums matter_. Washington, DC: Smithsonian Institution Press.
*   Yakel, E. (2004). [Seeking information, seeking connections, seeking meaning: genealogists and family historians.](http://www.webcitation.org/6KXi3zemD) _Information Research_, **10**(1), paper 448\. Retrieved 10 October, 2013 from http://informationr.net/ir/15-4/paper448.html (Archived by WebCite® at http://www.webcitation.org/6KXi3zemD)
*   Zhang, Y. (2000). Using the internet for survey research: a case study. _Journal of the American Society for Information Science_, **51**(1), 57–68.

</section>

</article>