#### vol. 14 no. 3, September, 2009

* * *

# Working paper

# The information professional's profile: an analysis of Brazilian job vacancies on the Internet

#### [Miriam Vieira da Cunha](#author)  
Universidade Federal de Santa Catarina.  
Department of information science,  
Campus Universitário,  
Trindade 88049-910, Florianópolis,  
Brazil  

#### Abstract

> **Introduction.** Report of a study to discover and describe job vacancies for information professionals available online at specific sites and discussion lists between January 2005 and February 2008\.  
> **Method.** The study uses Bardin's content analysis technique and the following analysis criteria: information source, institutional type, professional type, location, qualifications, job description, experience and foreign language requirements.  
> **Analysis.** The results are compared with national and international studies on the profiles and skills of information professionals.  
> **Results.** The results show that expansion in Brazil in the field of information and its professionals is still slow and this can be seen from the results presented.  
> **Conclusions.** The profile for a typical information professional from the job advertisements analysed is for a librarian, with a degree in librarian studies, to perform technical and management functions in a private institution in the city of São Paulo.

## Introduction

A confluence of interests led to my decision to investigate the differing profiles of information professionals. One of these was my previous experience as a librarian in Brazilian, French, Mozambiquean and Cape Verdean institutions. Another was my own teaching and research experience. This means that my reflections are based on my professional experience, and that I therefore am proffering my own views but from a practical base. This “double sided” approach has both its advantages and disadvantages.The greatest advantage is perhaps being able to study something based on experience, but this method also runs the risk of being overly close to the object of study.

Studying information professionals has been a much discussed subject over the past ten years, the motivation being to understand their environment and the changes of context in our lives: a context full of uncertainties, mutations and complex international relations.

The considerable advances in the processes of information communication in all its forms have been the impetus for these changes over the past thirty years. Historic and technological transformations have strong repercussions in the field of information activities. They translate into uncertainties, threats and doubts, and also into the need to redefine the characteristics of the fields of human knowledge and actions. Various issues arise around the future of information professions in this new reconfiguration.

According to Morigi & Silva ([2005:126](#mori05)), the “advent of the information society is establishing new forms of viewing work, jobs and professions”. For these authors “the information age advocates an about-turn in pre-established values, a radical change from the old paradigms of certain professions, principally those that deal with information and knowledge.”

These transformations create new demands and are changing our paradigms. We are entering what Browning ([1993](#bro93)) called the “age of wall-less libraries for page-less books.” Technology is creating new paths for information and communication, as are the resultant culture and behaviour in this scenario.

In the age of economic globalisation, information is omnipresent in every sector of human life. It is essential to the life of organizations, and performs a fundamental economic and strategic role. ([ADBS, 2005](#adb05a)).

The emergence of the Internet as a means to access information, the development of electronic networks and the general ease of access to information are leading to an unprecedented increase in users’ research independence, and consequently to the disappearance of various forms of mediation.

With the ever-greater diffusion of informational technology and of the skills needed to use and develop them, the lines between technology and content have blurred. A result of this phenomenon is that the delineation between professions with links to information is increasingly tenuous. However, it is not only between professions connected with information that this _dilution_ of frontiers is found. In general, the delimitations between professional groups are ever more fluid and indistinct, making it more difficult for any particular group to justify its _area of jurisdiction_ (1) (to use Abbott’s interpretation, [1988](#abb88)) over any other. This phenomenon of _dis-intermediation_ and the idea that an increasing amount of information is available through the Internet causes distrust among information professionals.

Because of this, transformations in our knowledge society are leading these professionals to redefine their roles in the more traditional professional spaces. While the workload for information professionals is now more complex, this redefinition has not resulted in strengthening the traditional information professions. Rather, it has opened up the field to professionals who work in alternative spaces in other areas, frequently in interdisciplinary teams. These new work areas create new sets of relations between the professions and in some cases can break down barriers.

The communication explosion brought about by the Internet together with the perception of information as a strategic resource in itself, is leading growing numbers of people to work with information sources. This movement, although it inevitably creates tensions, also creates new opportunities for alliances in this inter-professional space. The union of these movements, tensions and alliances forms a new field of competence or a new “professional jurisdiction” ([ABBOTT, 1988](#abb88)).

## Information professionals in Brazil

Studies on information professionals in Brazil have become increasingly common since the 1990s, especially in information science postgraduate programmes. They follow an international tradition of studies of this type in the United States, France, England and Spain. The North-American study of Wasserman & Bundy ([1969](#was69)) is cited as the first research paper in this area. ([Cunha, 1998](#cun98)). Another reference in Brazilian studies is the [FID](#fid92) – International Federation of Documentation study on the profile of new information professionals published in 1992.

In Brazil these studies have been the subject of discussion by the Informação, Educação e Trabalho (Research Group on Work, Information and Education), which presents its results each year at the annual meeting of the Associação Brasileira de Pesquisa em Ciência da Informação (Brazilian Association for Research in information science). Important studies from this group include those by Souza (education and work, 2006 and 2008), Crivellari (information professionals seen from the perspectives of labour sociology and the sociology of the professions, [2004](#cri04) and [2008](#cri08)) and Jannuzzi (information professionals seen from the perspective of the Labour Sociology, [2005](#jan05)). The group discusses how changes in the content of information science and its practices have arisen at its margins and in the overlapping areas between it and other sciences. ([Abbott 1988](#abb88)).

Studies on information, education and work also follow traditions set by studies on the relationship between technology and work, a trans-disciplinary field. As well as the studies in the field of information science, research is based on academic production in complementary fields, principally in sociology, economics, education, political science, human resources administration and social psychology.

The social contribution of studies with roots in different areas of the human and social sciences is vital for the field of information, education and work, which needs to focus on the social relations built, modified and in many cases fragmented because of the adoption of information technologies. These studies are theoretically grounded in the following approaches: the sociology of professions ([Larson 1977](#lar77); [Abbott 1988](#abb88); [Rodrigues 2002](#rod02); [Friedson 2003](#fri03) and [Dubar 2005](#dub05), among others); labour sociology (Boyer, [1990](#boy90), [1993](#boy93)); urban sociology (Castells, [1996](#cas96); Santos, [2000](#san00)); the sociology of communication ([Lojkine 1995](#lok95)); the theory of social representations ([Moscovici 2003](#mosc03)); the sociology of knowledge (Bourdieu, [2006](#bour06); [Berger and Luckmann 1985](#berg85)); and the sociology of science ([Latour 2000](#lat00)).

Methodological approaches worthy of mention are: content analysis ([Bardin 2004](#bar04); [Minayo 2002](#min02)) and discourse analysis ([Lefevre, 2000](#lef00)).

The present study is aligned with others on simliar work areas (mainly Crivellari and Jannuzzi) and it continues on from earlier studies on the employment market and professional training based on the Sociology of Professions. It has been fuelled by the diversification of the market and by new demands, which require continual study and research to set up and identify job profiles due to the need to understand the size of this market in constant evolution and in order to keep up with society’s demands. This type of research is intended to support training for information professionals who are now required to meet these new demands.

## Methods

In context of complex and diversified requirements, the Internet is like a richly patterned, but fragmented information panel that is growing at an exponential rate and numerous professionals concerned with “giving a meaning” to web information and with evaluating and recovering web content have appeared over the past few years.

In Brazil, the dissemination of offers for information professionals is relatively sparse and limited. An early study ([Cunha 1998](#cun98)) showed that at that time in Brazil the major national newspapers were the most commonly used vehicles for these offers. However, the low incidence of vacancies advertised using these means now leads one to believe that they are disseminated using alternative means such as the Internet.

The Net has been a valuable tool for job vacancy postings over the last five years ([Arévalo 2000](#are00)). This diffusion of employment positions is spread though specific sites and discussion lists in the field.

In the field of information professions, employment market studies are essential in order to understand its evolution. There are several ways of approaching the subject. The level of analytical complexity varies depending on the methods of data collection: those most commonly used by information science specialists are through the analysis of job offers, and questionnaires and/or interviews with employees and employers.

In England, Moore ([1987](#moo87)) used job offers and questionnaires in his analysis. In Brazil, Tarapanoff ([1997](#tar97)) and Beraquet & Ciol ([2003](#ber03)) based their analyses on interviews while Rosemberg _et al._ ([2003](#ros03)) based their analysis on a questionnaire.

Job vacancy postings seem an ideal tool for this study, as this easily accessible data represents the _visible_ criteria for employers’ choices. It also allows one to verify the diversity and the specificities of the information professions, showing the evolution of the job market and the fluctuations of the offers. They serve as a _barometer_ of the professions’ visibility and reality at a determined moment in time.

One should remember that the blanket term _information professional_ covers a wide range of activities that are more or less specific depending on those taking part. It was first used in the 1980s to designate professionals who, according to the Classificação Brasileira de Ocupações [Brazilian job classification]:

> ...supply information for any support system; manage units such as libraries, documentation centres, information centres and similar, as well as information networks and systems. They treat technically, and develop informational resources; disseminate information with the aim of facilitating access to and the creation of knowledge; create studies and research projects; undertake cultural diffusion; develop educational activities. They may provide help and consultation services. ([Brasil 2002](#bra02))

The choice of job offers in Brazil posted on the Internet as a research source is based on the growing spread of these offers in professional and academic discussion lists in recent years, the increase of specific sites for job vacancies and their availability on the Internet.

### Definition of analysis criteria

According to Bardin ([2004:37](#bar04)), content analysis is:

> a group of techniques for communication analysis that, through systematic and objective procedures of message content description, aim to obtain indicators (quantitative or not) that allow one to infer knowledge relating to the production/reception conditions of these messages.

The principle of content analysis is the inference of knowledge relating to the object of study. According to Bardin ([2004:89](#bar04)) its stages are the description of the characteristics of the text, the treatment of data through inference and the interpretation and analysis of the characteristic of the data through the inference.

To undertake this analysis, _meaning nuclei_ should be identified that compose the communication, whose presence, absence or frequency mean something in relation to the analytical object under study. Semantically analysing a text makes it necessary to build a table for analysis. The variables used were the following:

*   information source,
*   institution type,
*   profession type,
*   qualifications,
*   position,
*   required foreign languages,
*   professional experience, and
*   city

The sites analysed using these criteria were:

*   [CATHO online](http://www.catho.com.br/) – site of a human relations consulting company.
*   [INFOHOME](http://www.ofaj.com.br/) – Site of Professor Oswaldo Francisco de Almeida Jr. www.ofaj.com.br
*   [Bibliotecarias](http://www.bibliotecarias.com.br/) (Librarians)

Other sites were also included such as that of the journalist Gilberto Dimenstein, [Aprendiz](http://aprendiz.uol.com.br/homepage.mmp) (Apprentice), the [Globo](http://www.globo.com/) network job vacancy site, and that of the [Pontifícia Universidade Católica do Rio de Janeiro](http://www.puc-rio.br/) The _Aprendiz_ site did not offer any jobs for information professionals over the period studied. The _Globo_ and Pontifícia Universidade sites stopped posting job vacancies from 2005.

The discussion lists analysed were:

*   [Instituto Brasileiro de Informação em Ciência e Tecnologia](http://www.bibvirtual.com.br/) (IBICT) (Brazilian Institute for Scientific and Technological Information).
*   [Instituto para Inclusão na Sociedade da Informação](http://www.iasi.org.br/) (IASI) (Institute for Inclusion in the Information Society); an information discussion list.
*   [ABECIN](http://www.abecin.org.br/) – Discussion list for the Associação de Educação em Ciência da Informação (Association of information science Education).
*   Those of the Associações de Bibliotecários (Library Assocations);
*   The Curso de Biblioteconomia da UFSC – Universidade Federal de Santa Catarina (Library Studies course at UFSC- Federal University of Santa Catarina)

The ABECIN and IBICT discussion groups were chosen for being the most influential in the field of information science in Brazil and because they regularly post job vacancies. The IASI list, moderated by Aldo Barreto, professor of IBICT has from the start included discussions of fundamental importance for the area as well as information about events, book and journal launches, job offers and placements. The sites were chosen for having connections with accredited institutions, for being free, and for posting offers for all kinds of jobs, in all knowledge areas, the exceptions being the INFOHOME and Bibliotecárias sites which post only offers for information professionals.

## Analysis of results

In Brazil, the evolution, expansion and diversification of the field of information activities and its professionals is still sluggish, and this can be demonstrated using the following data.

Between January 2005 and February 2008 there were 2,283 job postings for information professionals on the sites and discussion lists under study. The greatest number of offers was on the INFOHOME site, with 48.89% of the total, showing this site is gaining credibility. This was followed by the _CATHO online_ site, which offered jobs in all the knowledge areas.

Regarding institution type, 37.01% of the institutions that advertised for information professionals are private; 33% were public, and the rest were not specified. These results confirm other studies that profiled information professionals in the states of Santa Catarina ([Cunha 2007](#cun07a)) and Rio Grande do Sul ([Cunha _et al._, 2007](#cun07b)), where the majority work in private institutions.

There has been a proliferation of private higher education institutions in Brazil over the last twenty years. Data from 2005 indicates that these institutions represent around 83% of the total ([Brasil 2005](#bra05)). The fact that these organizations make up one of the largest employers of information professionals in the country probably explains the predominance of private institutions among the job vacancies that were analysed.

In comparison, in the French study by ADBS ([2005](#adb05b)), the vast majority of information professionals work in public institutions. The reason for this is the State’s level of influence in the French economy.

The great majority (81.17%) advertise for librarians. This is then followed by vacancies for archivists and assistant librarians.

One should stress that in the description of information professionals’ responsibilities, the _Classificação Brasileira de Ocupações_ shows an expansion and diversification in their work (cf.p4). In this Classification, information professionals form a family constituted by the following professions:

2612-05 – **Librarian -** Bibliographer, Technical librarian, Information scientist, Information consultant, Information specialist, Information manager.

2612-10 – **Documentalist -** Information analyst, Documentation specialist, Documentation manager, Documentary process control supervisor, Documentary Control supervisor, Documentation technician, Documentation support technician.

2612-15 – **Information analyst -** Network information researcher.

According to the CBO the qualification needed for these positions is a Bachelor’s in Library Studies.

It is clear that, for the _Classificação Brasileira de Ocupações,_ information professionals are librarians – and that this post between the three components of the occupational family should be able to perform most of the typical activities of that family. ([Cunha and Crivellari 2004:50](#cun04)).

The _Classificação_ also excludes archivists and museologists from the group of information professionals. As Robredo asserts (2003:162-163), “it is impossible to think of any type of information professional who has universal knowledge”, though the CBO seems to advocate this when it compresses the tasks of distinct professions into one degree – that of Library Studies. This confusing state caused by the CBO emphasizes the difficulty, highlighted by Abbott ([1988](#abb88)) among other authors, of establishing boundaries for information professionals.

Despite all the modifications that have been made in the system configuration of these professions and in spite of the diversity of jobs and work spaces for information professionals, the _Conselhos de Biblioteconomia_ (institutions aimed at “guiding, disciplining and supervising the regulation of Librarianship and contributing to the professional responsibilities.”} in Brasil continues, at least for the moment, to control and organize the work in its jurisdiction, in this way keeping the market relatively closed and protected. To our minds, this seems to be the most likely explanation for the predominance of librarians over other information professional groups in the context of this study.

However, it is worth mentioning that there has been growth, albeit timid, in the field of information activities over the past three years, with offers (though still the exception rather than the rule) for information analysts, documentation and information technicians, knowledge managers, terminology analysts and information architects.

All this would lead one to believe that this diversification is the result of a diversification of courses in the field of information studies at Brazilian universities, thus revealing an opening-up of the jurisdiction towards other specializations like iinformation management, information systems, information science and information administration, among others. In spite of this, library studies continues to be the most visible course, with thirty-five courses offered in the country. There are only seven archive management and administration courses, three information management courses, two information science courses, one information administration course and two museology courses. ([ABECIN 2008](#abe08)). This expansion, however, appears to point towards some maturing of the information professions. According to Abbott ([1988](#abb88)), based on market demands, a mature profession is constantly dividing itself into areas of specialization and inter-professional competition.

The popularity of postings for library assistants has two main reasons: the first is the “confusion about the boundaries between the job of librarians and library assistants” ([Danner 1998:6](#dan98)) and the second is economic—if an assistant is able to perform the same job as a librarian, it is cheaper to employ an assistant.

In his study on the system of professions, Abbott ([1988:65-66](#abb88)) states that lower ranking professionals “(…) learn a version of the profession’s knowledge system 'on-the-job'”. For him, within the “work jurisdiction system, it is the actual performance of the individual and not the status of their qualifications that is important.”

The results show that the information professionals most in demand continue to be two of the “traditional three Marys”, to use an expression coined by Johanna Smit ([1994](#smi94)); those being librarians and archivists. Museologist vacancies were only posted in six of the offers studied.

3.24% of the offers were non-specific in their advertisements about whether they wanted librarians or archivists. In comparison, 10.7% of the Brazilian postings analysed by Cunha in 1988 were unspecific about their preferences. This kind of offer can be interpreted in two ways: either employers are still unfamiliar with the area and, consequently, are deliberately vague, or employers are unsure which professional is required and, therefore, advertise for all.

A study in France in 2005 by the _Association des Profesionnels de l’Information et de la Documentation_ – ADBS shows that documentalists make up 34% of French professionals, while Librarians constitute 13% (ADBS, 2005). One should clarify that the distinction between the careers and the qualifications of librarians and documentalists is a particularly French one. In this country librarians and documentalists develops their own qualification system and each qualification corresponds to a specific position. ([Cunha 1998](#cun98))

With regard to qualifications, 57.03% of the postings asked for a degree in library studies; 10.82% were looking for a specialized professional with an unspecified degree; 3.94% requested a degree in Archival Studies and 1.41 asked for a degree in archival studies or library studies. It should be noted that twenty-two of the offers asked for a professional with a degree in library studies or information sciences and 6 for a graduate in library studies or lnformation management.

This leads one to believe that, even though most postings advertise for a librarian, not all employers are able to relate this professional to their specific qualifications, thus implying a possible lack of understanding of the subject.

In relation to job descriptions, most asked for technical classification skills, e.g., cataloguing and indexing (38.22%); this was followed by information management and user service skills, thus corroborating the results of other studies. For example, Moreiro Gonzalez’s study ([2001](#mor01)) on Spanish information professionals shows that the positions most in demand are those involving technical areas and information searches.

With regard to technical responsibilities, Moreiro Gonzalez confirms that “the results of this study show that they continue to be important and represent the _core_ competencies required to adequately develop the responsibilities of information professionals.” ([2001:31](#mor01)) For Danner ([1998:20](#dan98)) this premise is also true; he states “in this new reality the expertise content of the information professional will continue to be important.”

One should also note that the survey by the _Association des Professionnels de l’Information et de la Documentation_ – ADBS in 2005 indicates that technical skills constitute 68% of the work of information professionals in France. ([ADBS 2005](#adb05b))

Only 12.61% of postings asked for a foreign language and of this percentage the vast majority (79%) was for knowledge of the English language. All this indicates that most employers assume that the professionals already are familiar with a foreign language, and thus do not consider it necessary to specify this in the advertisements. In our age of globalisation and network domination, knowledge of foreign languages is essential, as highlighted by Tomael & Alvarenga ([2000](#tom00)), Marchiori ([1996](#marc96)) and Montalli ([1997](#mon97)) among others.

In a study on the employability of information professionals in Spain ([Moreiro 2001](#mor01)), the language most in demand was English, as it was cited by 46% of employers. This was followed by French and then German.

37.23% of the postings analysed requested professional experience. Most asked for information technology experience, fundamental for information professionals in the world today.

São Paulo comes top of the list of cities that most employ information professionals, with 25% of the total job offers posted. This comes as no surprise as the city, as well as being the most populated, also has the largest GDP in Brazil, and it is thus logical that it has the greatest number of job vacancies. São Paulo is followed by Rio de Janeiro, Brasília and Belo Horizonte. Rio de Janeiro is the most important city in terms of economic development after São Paulo. In addition to its demand for government positions, in the last few years Brasília has also attracted service companies who also require professionals with information skills.

## Conclusion

This study is a snapshot at a precise moment in time of an environment that is in constant change. With regard to the professional plurality of 3.24% of the job offers, studies by Baptista ([2004](#bap04)), Cronin _et al._ ([1993](#cro93)) and Moore ([1987](#moo87)), mention the lack of employer consensus in relation to the profile of the professionals they are seeking. The results also show the lack of distinction that exists between different information professionals, with a particular emphasis on librarians and archivists. Correia and Vasconcelos ([1990:216](#cor90)) in their study on the education of information professionals in 1990 pointed in a similar way to a certain interpenetration of the inherent responsibilities of information professionals.

Mueller ([1989:69](#mue89)) suggests a “possible solution” to this relative lack of definition of the professional profile; an education based on associations between like professions.

Such an association could be created in the professional education system, with the implementation of a structure that would allow movement not only vertically, as we have currently have from a bachelor’s degree to a master’s and doctorate in a single career, but between careers. People with diverse basic qualifications would be able to follow on to other course levels and professional qualifications in any related areas and be legally recognised. Therefore, there would be not only a librarian class, but a class of information professionals, of which librarians would form a part.

This study confirms the results of earlier research, such as that by Tarapanoff ([1997](#tar97)), which shows the significant demand for librarians. However, as stressed above, in many cases employers seem to be uncertain of the profile of the professionals they are looking for, and also of their qualifications, in the same offer for example advertising for librarians and/or archivists, with degrees in library studies or archive studies and library studies or information management.

In comparison, Moreiro’s study ([2001:34](#mor01)) on Spanish professionals suggests “the profiles of professionals wanted are imprecise, this is perhaps due to the continual posting of new offers and the recent creation of new degree courses in the information field.”

Yet, in spite of the changes and of the reorganization of the workspace, and of new partnerships, the informational field in Brazil still is resistant to expansion, and shows little sign of change. We must conclude that job opportunities in the information field in our country continue to be dominated by the _traditional_ professionals. The data shows that the _typical_ professional advertised using the specific sites and discussion lists on the Internet between January 2005 and February 2008 is a librarian who is a graduate in Library Studies, with information technology experience, required to perform technical and management functions in a private institution in São Paulo.

One should remember that according to Abbott ([1988](#abb88)), each profession has an individual place in the system of the professions and develops its activities within various jurisdictions. This place is determined through a central nucleus, denominated by Cronin _et al._ ([1993](#cro93)) as a “heartland”, a clearly delineated field of activities, over which the group has complete and legal control. Thus, it is possible to state that the space occupied by Brazilian information professional, the heartland, or central nucleus of jurisdiction, continues to be biased for librarians.

However, the growing diversity of courses in the field of information seems to suggest a trend for growth in competitiveness between different professionals and emerging professional groups ([Friedson 2003](#fri03)), and this is demonstrated by the demand for information architects and information managers. This reality, albeit in its early days, is pressuring an opening up in the field and in the battle for professional jurisdiction.

As the professional environment is continually changing due to the emergence of new professions, new technology and political changes in the workplace, competition is an inherent fact of professional life. ([Danner 1998](#dan98)).

In the face of the growing importance of information technology in every aspect of professional life, it is interesting to point out the lack of emphasis on IT skills in general in the job offers analysed. This discovery corroborates a study by Dufour and Bergeron ([2001:38](#duf01)) on job offers for information professionals in Web information services in the USA. The authors conclude from their study that the “technological dimension is less present in the job offers than in the literature.”

What can be seen from the results of this study is that, apart from a bias for a traditional professional, employers in many cases cannot distinguish between the type of professional that they want. This lack of distinction is, to our understanding, the result of mutations in work areas. In spite of the transformations taking place in the labour market in Brazil, the space for information professionals is still seen as one for traditional professions.

## References

*   <a id="abb88"></a>Abbott, A. (1988). _The system of professions: an essay on the division of expert labour_. Chicago, IL: The University of Chicago Press.
*   <a id="are00"></a>Arévallo, J.A. (2000). [Características del comportamiento del mercado de trabajo en Biblioteconomía, Archivística y Documentación, bienio 98/99.](http://www.webcitation.org/5ivLjDWNh) [Characteristics of the behaviour of the labour market in librarianship, documentation and archives, 98/99 biennium.] _Anales de Documentación_ No. 3, 9-24\. Retrieved 10 August, 2009 from http://revistas.um.es/analesdoc/article/viewFile/2531/2521 . (Archived by WebCite® at http://www.webcitation.org/5ivLjDWNh)
*   <a id="abe08"></a>Associação Brasileira de Educação em Ciência da Informação. (2008). _Educação_. Retrieved 10 February, 2008 from http://www.abecin.org.br [Note that this part of the site no longer exists, and there appears to be no replacement.]
*   <a id="adb05a"></a>Association des Professionnels de L’information et de la Documentation. (2005). _Documentaliste d’aujourd'hui et de demain._Retrieved 20 February, 2008 from http://www.adbs.fr [Note that this part of the site no longer exists, and there appears to be no replacement.]
*   <a id="adb05b"></a>Association des Professionnels de L’information et de la Documentation (2005). _[Professionnels de l'Information-Documentation, qui êtes-vous?](http://www.webcitation.org/5jAdWdo9c)_ Résultats de l’enquête 2005\. Retrieved 1 March, 2008 from http://www.adbs.fr/professionnels-de-l-information-documentation-qui-etes-vous-resultats-de-l-enquete-adbs-2005-15393.htm?RH=MET_REFMETIER (Archived by WebCite® at http://www.webcitation.org/5jAdWdo9c)
*   <a id="bap04"></a>Baptista, S.G. (2004). As oportunidades de trabalho existentes na Internet na área de construção de páginas de unidades de informação. [The job opportunities on the Internet in the area of constructing Web pages of information organizations.] In S.G. Baptista & S.P.M. Mueller, (Eds.). _Profissional da Informação: o espaço de trabalho_. (pp.224-241). Brasília: Tesaurus.
*   <a id="bar04"></a>Bardin, L. (2004). _Análise de conteúdo._ [Content analysis]. Lisboa: Edições 70.
*   <a id="ber03"></a>Beraquet, V. & Ciol, R. (2003). [O profissional da informação no paradigma virtual: atuação em saúde pública.](http://www.webcitation.org/5jAeVnzMY) [The information professional in the virtual paradigm: the situation in public health.] _Biblios: Revista electrónica de bibliotecología, archivología y museología_, No. 16\. Retrieved 19 August, 2009 from http://dialnet.unirioja.es/servlet/fichero_articulo?codigo=759411&orden=0 (Archived by WebCite® at http://www.webcitation.org/5jAeVnzMY)
*   <a id="berg85"></a>Berger, P.I. & Luckmann, T. (1985). _A construção social da realidade: tratado de sociologia do conhecimento._ [The social construction of reality: a treatise in the sociology of knowledge.] 6th ed. Petrópolis, Brazil: Vozes.
*   <a id="bour06"></a>Bourdieu, P. (2006). _O poder simbólico_. [Symbolic power.] São Paulo, Brazil: Bertrand-Brasil.
*   <a id="boy90"></a>Boyer, R. (1990). _Teoria da regulação: um balanço crítico_. [Regulation theory: a critical balance.] São Paulo, Brazil: Nobel.
*   <a id="boy93"></a>Boyer, R. & Caroli, E. (1993). _Changement de paradigme productif et rapport  éducatif: performance de croissence comparé France-Allemagne._ [The changing productive paradigm and educative affinity: results of a France-Germany cross comparison.] Paris: INRA, CEPREMAP.
*   <a id="bra02"></a>Brasil. _Ministério do Trabalho e Emprego_. (2002). _[Classificação Brasileira de ocupações](http://www.webcitation.org/5jAtVhEKU) _. [Brazilian classification of occupations.] CBO2002\. Retrieved 30 January, 2008 from http://www.mtecbo.gov.br/cbosite/pages/pesquisas/BuscaPorTituloResultado.jsf (Archived by WebCite® at http://www.webcitation.org/5jAtVhEKU)
*   <a id="bra05"></a>Brasil. _Ministério da Educação. Instituto Nacional de Estudos Pedagógicos._ (2005). _[Manual: Dicas de consulta do EDUDATA BRASIL](http://www.webcitation.org/5jAuC5OEP)_ [Manual: consulting tips for EDUDATA BRASIL]. Retrieved 15 December, 2008 from http://www.edudatabrasil.inep.gov.br/manual/edudatabrasil_como_consultar.pdf (Archived by WebCite® at http://www.webcitation.org/5jAuC5OEP)
*   <a id="bro93"></a>Browning, L. (1993). [Libraries without walls for books without pages.](http://www.webcitation.org/5iwjacmyy) _Wired_, **1**(1), 1-5\. Retrieved 10 August, 200 from http://www.wired.com/wired/archive/1.01/libraries.html . (Archived by WebCite® at http://www.webcitation.org/5iwjacmyy)
*   <a id="buf04"></a>Bufrem, L.S.; Pereira, E.C. (2004). Os profissionais da informação e a gestão de competências. [Information professionals and the management of competencies.] _Perspectivas em Ciência da Informação_, 9( 2),170-181\. Retrieved 15 December, 2008 from [http://www.eci.ufmg.br/pcionline/index.php/pci/issue/view/70](http://www.eci.ufmg.br/pcionline/index.php/pci/issue/view/70)
*   <a id="cas96"></a>Castells, M.  (1996). _A sociedade em rede._ [The network society.] São Paulo, Brazil: Editora Paz e Terra.
*   <a id="cor90"></a>Correia, Z. & Vasconcelos, A.C. (1990). A formação de profissionais de informação face à evolução do mercado de informação. [The training of information professionals in the face of the evolution of the information market.] _Anais do Congresso Nacional de Bibliotecários, Arquivistas e Documentalistas_, (pp. 207-229). Lisbon: BAD,
*   <a id="cri08"></a>Crivellari, H.M.T. & Pena, A.S. (2008). Transformações políticas e mudanças no trabalho do profissional da informação: 1985 a 2005\. [Political transformations and changes in the work of the information professional: 1985 to 2005.] _Revista Educação & Tecnologia_, **12**(3), 1-4.
*   <a id="cri04"></a>Crivellari, H.M.T & Cunha, M.V. (2004). Os bibliotecários como profissionais da informação: estratégias e paradoxos de um grupo profissional. [Librarians as information professionals: strategies and paradoxes of a professional group.] In _XXVIII Encontro Anual da ANPOCS, 2004, Caxambu. Anais_. Caxambu: ANPOCS. (On CD).
*   <a id="cro93"></a>Cronin, B., Stiffler, M. & Day, D. (1993). The emergent market for information professionals: educational opportunities and implications. _Library Trends_, **42**(2), 257-276.
*   <a id="cun98"></a>Cunha, M.V. (1998). _L’émergence des nouveaux professionnels de l’information: fonctions, compétences, marché. Etude comparée des situations brésilienne et française._. [The emergence of new information professionals: functions, skills, market. Comparative study of the Brazilian and French situations] Unpublished doctoral thesis, Conservatoire National des Arts et Métiers, Paris, France.
*   <a id="cun07a"></a>Cunha, M.F.V. (2007). Les professionnels de l'information au Brésil: profil des professionnels diplômés par l Université fédérale de Santa Catarina. [Information professionals in Brazil: profile of professionals graduated by the Federal University of Santa Catarina.] _Documentaliste-Sciences de l'Information_, **44**(3), 259-263.
*   <a id="cun04"></a>Cunha, M.V. & Crivellari, H.M.T. (2004). O mundo do trabalho na sociedade do conhecimento e os paradoxos das profissões de informação. [The world of work in the knowledge society and the paradoxes of the information professional] In M.L.P. Valentim, M.L.P. (Ed,). _Atuação profissional na área de informação_. (pp.41-54). São Paulo, Brazil: Polis.
*   <a id="cun07b"></a>Cunha, M.V., Silva, C.C.M. & Kill, C.F. (2007). [Perfil do bibliotecário formado pela Universidade Federal do Rio Grande do Sul.](http://www.webcitation.org/5jAhOEP0r) [Profile of the librarian trained at the Federal University of Rio Grande do Sul.] _Informação & Sociedade: Estudos_, 17(1), 109-115\. Retrieved 18 August, 2009 from http://www.ies.ufpb.br/ojs2/index.php/ies/article/view/504/1470 (Archived by WebCite® at http://www.webcitation.org/5jAhOEP0r)
*   <a id="dan98"></a>Danner, R.A. (1998). Redefining a profession. _Law Library Journal_, **90**(3), 315-356\. Retrieved in March 2008 from http://eprints.law.duke.edu/599/1/callweb.htm (Archived by WebCite® at http://www.webcitation.org/5jAt9Trdo)
*   <a id="dub05"></a>Dubar, C. (2005). _A Socialização: construção das identidades sociais e profissionais_. [Socialization: the construction of social and professional identities.] São Paulo, Brazil: Martins Fontes.
*   <a id="duf01"></a>Dufour, C. & Bergeron, P. (2001). Lorsque professionnels de l’information et systems d’information Web se rencontrent: analyse d’offres d’emploi liées au Web. [When information professionals and Web information systems meet: analysis of job-offers placed on the Web] _Canadian Journal of Information and Library Science_**,** 26(1), 19-28.
*   <a id="fid92"></a>Féderation International de Documentation. _Special Interest Group on Roles, Careers and Development of the Modern Information Professional_. (1992). State of the modern information professional: 1992-1993: an international view of the state of the information professional and the information profession in 1992-1993\. The Hague: FID.
*   <a id="fri03"></a>Friedson, E. (2003). _Renascimento do profissionalismo_. [The renaissance of professionalism.] São Paulo, Brazil: EDUSP.
*   <a id="jan05"></a>Jannuzzi, P.M.(2005). [Biblioteconomistas e outros profissionais da informação no mercado de trabalho brasileiro: 1980-1996](http://www.webcitation.org/5jAxweZPK). [Librarians and other information professionals in the Brazilian job market: 1980-1996.] In _V ENANCIB - Encontro Nacional de Pesquisa em Ciência da Informação, 2005, Belo Horizonte. Anais_. Belo Horizonte: UFMG. (On CD). http://www.portalppgci.marilia.unesp.br/enancib/viewpaper.php?id=296 (Archived by WebCite® at http://www.webcitation.org/5jAxweZPK)
*   <a id="lar77"></a>Larson, M.S. (1977). _The rise of professionalism. A sociological analysis._ Berkeley, CA: University of California Press.
*   <a id="lat00"></a>Latour, B. (2000). _Ciência em ação_. [Science in action.] São Paulo, Brazil: UNESP.
*   <a id="lef00"></a>Lefevre, F., Lefevre, A.M.C. & Teixeira, J.J.V. (Editors). (2000). _O discurso do sujeito coletivo: uma nova abordagem metodológica em pesquisa qualitativa_. [The discourse of the collective subject: a new methodological approach in qualitative research.] Caxias do Sul, Brazil: EDUCS.
*   <a id="lok95"></a>Lokjine, J. (1995). _A revolução informacional_. [An informational revolution.] São Paulo, Brazil: Cortez.
*   <a id="marc96"></a>Marchiori, P.  (1996). Que profissional queremos formar para o século XXI? [What professional do we want to be trained for the 21st century?] _Informação & Informação_, **1**(1), 27-34.
*   <a id="min02"></a>Minayo, M.C.S. (2002). _Pesquisa Social_. [Social research.] Rio de Janeiro, Brazil: Vozes.
*   <a id="mon97"></a>Montalli, K. (1997). Perfil do profissional da informação tecnológico e empresarial. [Profile of the technological and business information professional.] _Ciência da Informação,_26(3), 290-295\. Retrieved 18 August, 2008 from http://revista.ibict.br/ciinf/index.php/ciinf/article/view/385/345 (Archived by WebCite® at http://www.webcitation.org/5jAtpY6L6)
*   <a id="moo87"></a>Moore, N. (1987). _The emerging markets for librarians and information workers._ Boston Spa, U.K.: The British Library. (Library and Information Research Report, 56)
*   <a id="mor01"></a>Moreiro, J.A. (2001). [Figures on employability of Spanish library and information graduates.](http://www.webcitation.org/5ivag1Ngp) _Libri_, **51**(1), 27-37\. Retrieved 10 August, 2009 from http://www.librijournal.org/pdf/2001-1pp27-37.pdf (Archived by WebCite® at http://www.webcitation.org/5ivag1Ngp)
*   <a id="mori05"></a>Morigi, V.J. & Silva, L.S. (2005). Paradigma tecnológico e representações sociais dos bibliotecários sobre seu perfil e suas práticas no contexto da sociedade da informação. _Informação & Sociedade_**:** _Estudos_, 15(1), 123-145\. Retrieved in 15 December, 2008 from http://www.ies.ufpb.br/
*   <a id="mosc03"></a>Moscovici, S. (2003). _Representações sociais: investigações em psicologia social._ [Social representations: investigations in social psychology.] Petrópolis, Brazil: Vozes.
*   <a id="mue89"></a>Mueller, S.P.M. (1989). Reflexões sobre a formação profissional para a biblioteconomia e sua relação com as demais profissões da informação. [Reflections on professional training for librarianship and its relationship with other information professions] _Transinformação_, **1**(2), 175-186.
*   <a id="rob03"></a>Robredo, J. (2003). _Da ciência da informação revisitada aos sistemas humanos de informação._ [From information science revisited to human information systems.] Brasília, Brazil: Thesaurus.
*   <a id="rod02"></a>Rodrigues, M.L. (2002). _Sociologia das profissões._ [Sociology of the professions.] Lisbon: Celta.
*   <a id="ros03"></a>Rosemberg, D.S., Biancardi, A.M.R., Ricardo, S.P. & Facini, B. (2003). O Cenário do mercado de trabalho na percepção dos empresários Capixabas. [Scenario of the labour market in the perception of 'Capixaba' entrepreneurs [Editor: entrepreneurs from the state of Espirito Santo in Brazil] ] _Arquivos & Bibliotecas_, **11**(11), 61-77.
*   <a id="san00"></a>Santos, M. (2000). _Por uma outra globalização - do pensamento único à consciência universal._ [For another globalization - the only thought the universal consciousness.] São Paulo, Brazil: Record.
*   <a id="smi94"></a>Smit, J.W. (1994). [Eu, bibliotecário, rg xxx e cpf yyy, trabalho em arquivo ou museu... algum problema?](http://www.webcitation.org/5ivgtllm6) [I, a librarian, RG xxx and CPF yyy, work in an archives or a museum... is there a problem?] _Revista Palavra-Chave_, No. 8, 12-13\. Retrieved 10 August, 2009 from http://www.scribd.com/doc/7177771/JohanaSmith (Archived by WebCite® at http://www.webcitation.org/5ivgtllm6)
*   <a id="sou08"></a>Souza, F. C. (2008). Tendências em informação, educação e trabalho: as dimensões currículo e mercado profissional na pesquisa em Ciência da Informação no Brasil. [Trends in information, education and work: curricula dimensions and the professional market in research in information science in Brazil] _Tendências da Pesquisa Brasileira em Ciência da Informação,_ **1**(1), 1-38.
*   <a id="sou06"></a>Souza, F. C. (2006). [A formação acadêmica de bibliotecários e cientistas da informação e sua visibilidade, identidade e reconhecimento social no Brasil.](http://www.webcitation.org/5ivkMbY66) [The academic training of librarians and information scientists and their visibility, identity and social recognition in Brazil.] _Informação & Sociedade_. **16**(1)  32-46\. Retrieved in 15 December, 2008 from http://inseer.ibict.br/ancib/index.php/tpbci/article/view/1/15 (Archived by WebCite® at http://www.webcitation.org/5ivkMbY66)
*   <a id="tar97"></a>Tarapanoff, K. (1997). _Perfil do profissional da informação no Brasil_. [Profile of the information professional in Brazil] Brasília, Brazil: IEL.
*   <a id="tom00"></a>Tomael, M.I. & Alvarenga, G.M. (2000). [Profissional da informação: seu espaço e atuação em empresas industriais.](http://www.webcitation.org/archive.php) [The information professional: their space and action in industrial companies.] _Perspectivas em Ciência da Informação_, 5(1), 81-90\. Retrieved 15 December, 2008 from http://www.eci.ufmg.br/pcionline/index.php/pci/article/viewFile/549/312 (Archived by WebCite® at http://www.webcitation.org/5ivkqZU2h)
*   <a id="was69"></a>Wasserman,P. & Bundy, M.L. (1969). _A program of research into the identification of manpower requirements, the educational preparation and the utilization of manpower in the library and information professions: final report_. Washington, DC: U.S. Department of Health, Education and Welfare.