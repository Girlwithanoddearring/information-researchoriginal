#### vol. 14 no. 3, September, 2009

* * *

# Categorical and specificity differences between user-supplied tags and search query terms for images. An analysis of _Flickr_ tags and Web image search queries

#### [EunKyung Chung](mailto:echung@ewha.ac.kr)  
Library and Information Science,  
Ewha Womans University,  
Seoul,  
Korea

#### [JungWon Yoon](mailto:jyoon@cas.usf.edu)  
School of Library and Information Science,  
University of South Florida,  
Tampa, FL,  
United States

#### Abstract

> **Introduction**. The purpose of this study is to compare characteristics and features of user-supplied tags and search query terms for images on the _Flickr_ Website in terms of categories of pictorial meanings and level of term specificity.  
> **Method**. This study focuses on comparisons between tags and search queries using Shatford's categorization schemes and the level of specificity based on the basic level theory.  
> **Analysis**. Frequency distributions and chi-squared analyses were performed on the data. The results of statistical analyses demonstrated that there were significant differences in categories among tags and different stages of search query terms. The overall distributions of the levels of term specificity also had similar pattern in both tags and search query terms, but statistically significant differences were found between tags and search query terms.  
> **Results**. The results of this study demonstrated that _Flickr_ tags have their own unique features compared to users' queries for image searching. The findings suggest that image tags should not be generally applied to other image collections although they have been considered as useful data in developing a user-centered indexing system.  
> **Conclusions**. When utilizing user-supplied tags for user-centered indexing systems, it is desirable to consider the functions and users' tasks applied in tags rather than depending on statistical features merely obtained from the tag analysis.

## Introduction

Tags, users' own descriptions of images, are becoming widely used as participation on the Web increases. The potential of user-generated descriptors was offered by O'Connor ([1996](#oco96)) more than a decade ago. He asserted that 'by changing our model of where the act of representation takes place', (p. 150) images can be represented with user-generated tags as well as adjective and functional tags which are difficult for a single indexer to represent. The advent of Web 2.0 technology made this approach possible in the real world, and _Flickr_ became a popular image tagging system in America. Accordingly, a great deal of practical approaches and research endeavors have focused on tag utilization in user-centered indexing mechanisms (see the Related studies section). Compared to document-centred indexing, user-centred indexing is more interested in users' needs and focuses on incorporating possible user queries into the indexing terms ([Fidel 1994](#fid94); [Soergel 1985](#soe85)). Therefore, in order to evaluate the actual effectiveness of tag utilization as a user-centred indexing mechanism, user-generated tags need to be reflected in search query terms, i.e., representations of user needs.

However, comparative analyses between image tags and search queries have not been adequately investigated. Studies on image search queries have mainly compared them to the general search queries on the Web. These comparisons between image search queries and general search queries reveal quantitative differences in the amount of image search queries and the distribution of query terms ([Jansen _et al._ 2000](#jan00); [Goodrum and Spink 2001](#goo01)). Jansen _et al._ ([2000](#jan00)) reported that image search queries formed a small portion (less than 3%) of overall users' queries on the Web. Goodrum and Spink ([2001](#goo01)) found that image search queries contained a larger number of terms and terms that were more diverse compared to general search queries. On the other hand, research on image tags primarily focuses on the potential of tags for developing user-centred image indexing systems (i.e., folksonomies).

As a way of employing tags as a user-centred organization tool, researchers have attempted to identify patterns and features of social tags ([Golder and Huberman 2006](#gol06); [Morrison 2008](#mor08); [Stvilia and Jogensen 2007](#stv07)) or to develop controlled vocabulary systems using computational algorithms ([Schmitz 2006](#sch06); [Aurnhammer et al. 2006](#aur06)). These research efforts are based on the underlying assumption that utilizing user-supplied tags for images has benefits when building user-centred indexing systems. Since end-users directly supply the tags when describing images, it seems reasonable to assume that tags employed in user-centred indexing systems represent users' needs and perceptions of images. However, to determine this representation, it is necessary to investigate to what extent user-supplied tags are similar or different from search queries. This study is designed to fill this gap, because there is little empirical research evaluating user-supplied tags in terms of search queries for images.

This study aims to explore the characteristics and features of user-supplied tags for non-domain-specific images compared to search queries on the Web. More specifically, this study focuses on comparisons between tags and search queries using Shatford's ([1986](#sha86)) categorization schemes and the level of specificity based on the basic level theory. These two methods have been adopted as tools in related studies ([Armitage and Enser 1997](#arm97); [Choi and Rasmussen 2003](#cho03); [Jogensen 2003](#jor03); [Rorrisa 2008](#ror08)) because they provide a clear understanding of an image's semantic content. Using a categorization scheme makes it possible to examine dominant categories used in image representation (describing or searching) processes. An image contains multilayered meanings, so it is important to elucidate dominant categories (or attributes) in an image ([Choi and Rasmussen 2003](#cho03)). In addition, an object can be articulated differently depending on term specificity (e.g., the same object can be described as 'animal', 'dog', or 'Chihuahua'). Similar to dominant meanings in images, if it is possible to find any specific level of terms, those specific levels of terms should be the main focus of the indexing process ([Bates 1998](#bat98)). Following two methods, Shatford's categorization and specificity level, the characteristics and features of image tags are elucidated through comparison to search queries. In this context, the goals of this study are as follows:

*   To determine to what extent user-supplied tags and search query terms for images are different in categorization.
*   To determine to what extent user-supplied tags and search query terms for images are different in the level of specificity.

## Related studies

This section introduces lines of studies on examining image search queries and user-supplied tags on images. Their fundamental purposes were to understand users' needs, perceptions of image searching behaviour, so that the findings could provide evidences on implementing more effective image retrieval systems.

### Query analysis for image retrieval

Studies of image query analysis mainly consist of two areas: studies on image search queries which have been submitted to traditional visual/image collections in libraries and museums, and studies on image search queries in Web search engines.

Studies on image search queries generally attempted to identify the users' needs by analysing the users' queries submitted to visual information archives in libraries and museums. Armitage and Enser ([1997](#arm97)) analysed 1,749 queries submitted to visual information archives of seven libraries for image retrieval by categorizing users' requests based on Shatford's categorical analysis. The results of this study identified observable similarities in image query categorization across multiple libraries. The majority of users' queries of visual information archives from seven libraries were categorized as Specific, and the remaining queries were categorized as Generic or Abstract (the latter containing the fewest queries). More specifically, Hastings ([1995](#has95)) analysed queries of art historians for digitized Caribbean paintings. The analysis identified four levels of complexity from least complex to most complex. The least complex type of queries included questions such as _Who?_, _Where?_, and _When?_; while the most complex type of queries included _Meaning_, _Subject_, and _Why?_. The intermediate level of queries included _How?_, _Identity of Object_, and _Activities_ as well as _What are?_ questions. The results of this categorical analysis were applied to retrieval parameters for image and image characteristics.

Another query analysis on a specific image collection was conducted by Choi and Rasmussen ([2003](#cho03)). Based on Batley's ([1988](#batl98)) four categories, they identified image search needs by analysing thirty-eight search requests from the Library of Congress [American Memory](http://memory.loc.gov/ammem/index.html) photo archive. The results demonstrated that more than half of search queries were categorized general/namable needs (60.5%), then specific needs (26.3%), general/abstract needs (7.9%), and general or subjective needs (5.3%). They also analysed 185 search terms using Shatford's category, and demonstrated that 64.87% of search terms were included in the generic category and 26.49% and 8.64% were in the specific and abstract categories, respectively.

Given the characteristics of general searching behaviour in the context of the Web, there are several studies focusing on image search queries conducted on the Web. Jansen, Goodrum and Spink ([2000](#jan00)) identified image, audio, and video search queries from 1,025,908 search queries and 211,058 sessions on a major Web search engine. They identified 27,144 image queries representing 2.65% of all search queries. In terms of search query characteristics, they demonstrated that users applied more search terms (3.27 terms for images) when searching multimedia compared to general Web searches (2 terms for general searches). In addition, Goodrum and Spink ([2001](#goo01)) examined users' Web image queries to understand visual information needs in terms of the number of image queries, terms, and sessions. The average number of image queries per user was 3.36 while the average number of general queries was 2\. The categories were identified as diverse including image terms, modifiers, sexual terms, cost, sex, other, people, and art and leisure. From another perspective, Goodrum _et al._ ([2003](#goo03)) identified search query reformulation patterns by using Markov analysis of state transitions with seventy-one image search queries on the Web. Eighteen state categories were identified as search tool, collection selection, queries, context moves, or relevance judgments.

### Social tagging as an image representation mechanism

Recently, social tagging has received attention in the library and information science field as a promising information organization mechanism. Based on the idea that users not only organize information for their own use but also share their organized collections with others, researchers in the field expect that user-supplied tags can serve as a user-oriented indexing approach. A social tagging system has promising advantages: for example, information loss, which inevitably occurs during the information representation process, can be overcome, or at least decreased, through social tags. This is because for most of the social tagging systems, the information loss is from people having different viewpoints and is not from a single indexer's perspective ([Shirky, n.d.](#shind)). In addition, since users engaged with social tagging systems describe content with their own vocabulary, tagging systems can reveal the conceptual structure and current terminologies of the user community ([Furnas _et al._ 2006](#fur06)).

The potential of social tagging seems more beneficial for image indexing and retrieval. First, information loss has been identified as one of main obstacles in representing image documents. In other words, as an image document conveys multiple levels of meaning, including subjective impression, it has been argued that a single indexer cannot provide all possible index terms for an image document. However, user-supplied tags, even infrequently used tags, can be utilized in expanding indexing terms by reflecting a diversity of users' viewpoints ([Jogensen 2007](#jor07)). Secondly, it has been recognized that there are discrepancies between professional indexers' and naive users' perspectives in interpreting and representing image documents. As Bates ([1998](#bat98)) noted, although professional indexers assign index terms to assist users, their professional knowledge often leads to mismatches between index terms and search terms. By using user-supplied tags, it will be possible to reflect index terms that are familiar to end-users. Thirdly, browsing has been addressed as a significant activity during the image retrieval process, because verbal queries have limitations in expressing visual needs. Therefore, social tagging systems, which can assist users' browsing activities, will be a beneficial feature for an image retrieval system. Finally, there is another unique feature of image documents that can take advantage of a social tagging system. An image includes multi-layered messages that belong to different attributes (or categories). Therefore, as noted above, a prominent research area has been the discovery of which attributes of pictorial messages are significant in retrieving image documents. By analysing user-supplied tags, it is possible to discover which attributes are frequently adopted by users for organizing images for their own and others' use.

Since a social tagging system demonstrates its potential for providing access to image documents, several researchers, mostly in information science, have examined and utilized user-supplied tags. A series of studies has investigated how users use [Flickr](http://www.flickr.com/), a photo management and sharing site, which employs social tagging. On the _Flickr_ site, which was launched in 2004, users may upload their photographs with tags. Susequently, photographs may be viewed and searched by the public. Compared to other social tagging Websites, where users assign tags for digital resources created by others, _Flickr_ users assign tags for their own photographs. Guy and Tonkin's ([2006](#guy06)) study attempted to investigate how to make tags more effective as access points, based on the finding that there is a convergence of tags as time goes on. They focused on 'sloppy tags' from _delicious_ (the social bookmarking site) and _Flickr_ and proposed methods for improving tags by handling these sloppy tags. However, they also pointed out that these tidying up processes may discourage users' participation.

Marlow et al. ([2006](#mar06)) analysed _Flickr_ tag usage patterns to propose a tagging system based on their findings. According to their results, most users have only a few unique tags, and the growth of unique tags adopted by an individual user declines over time. They also found correlations between contact networks among _Flickr_ users and the formation of tag vocabulary (i.e., degree of common tag usage). Stvilia and Jogensen ([2007](#stv07)) investigated the collection building behaviour of _Flickr_ users by comparing descriptions given to two different types of photo sets (i.e., user-selected thematic collections), individual users' photo sets and groups' photo sets. They found that, whereas descriptions of individual users' photosets were focused on the users' contexts and events, descriptions of group photosets include more general concepts and the scope of the group.

A few studies have compared tags and the traditional indexing approach. Matusiak ([2006](#mat06)) compared tags and professionally-created metadata using two sets of images, one from the _Flickr_ site and the other from a digital image collection. She concluded that tags cannot be used as an alternative to professional indexing because of their inconsistency and inaccuracy. Instead, they may be used as an enhancement or a supplement to indexing. Winget ([2006](#win06)) focused on authority and control issues, and asserted a positive potential for tags with digital resources. According to Winget, users choose appropriate, thorough and authoritative terms, and there are informal policies which enforce appropriate tagging behaviour among users.

Researchers in computer science have developed algorithmic models connecting tags with existing indexing mechanisms. Schmitz ([2006](#sch06)) proposed a model which induced ontology from the _Flickr_ tag vocabulary, and discussed how the model can improve retrieval effectiveness by integrating it into a tagging community. Aurnhammer _et al._ ([2006](#aur06)) proposed combining tagging and visual features, and demonstrated that their model can overcome problems that may occur by only using one of two approaches.

The potential of social tagging has been explored in the museum community as a mechanism to bridge the gap between professional cataloguers and naive viewers. Although subject indexing is a significant access point for viewers, most cataloguing standards for museum collections do not require subject descriptions as a core element. Even if professionals assign subject index terms, findings reveal they cannot easily represent naive users' viewpoints ([Bearman and Trant 2005](#bea05)). Considering that tags can represent museum objects with the users' language as well as provide diverse views from many individual contributions, several museums, such as the [Metropolitan Museum of Art](http://www.metmuseum.org/), the [Guggenheim Museum](http://www.guggenheim.org/), and the [Cleveland Museum of Art](http://www.clevelandart.org/Explore/), have implemented projects integrating tags in museum collections. According to a study comparing terms assigned by professional cataloguers and by volunteer taggers at the Metropolitan Museum of Art, 88% of tags were not included in existing cataloguing records. Of these, 75% were evaluated as appropriate terms by the museum's Subject Cataloguing Committee. This study showed that tags can increase the number of user-friendly access points ([Trant 2006](#tra06)).

## Research design

### Data set

This study used two data sets for comparison: a set of search terms and a set of user-supplied tags. For search terms, the Web search log of _Excite_ 2001 was used. The Web search log of Excite 2001, which has been used frequently in several Web query studies ([Spink _et al._ 2002](#spi02); [Eastman and Jansen 2003](#eas03); [Jansen and Spink 2005](#jan05)), contains 262,025 sessions and 1,025,910 queries ([Spink _et al._ 2002](#spi02)). Since the search engine _Excite_ did not provide an explicit means to specify users' queries as image search queries, users had to supply specific terms to denote image search queries (e.g. _apple image_ rather than simply _apple_). Accordingly, for the first phase of query processing, the image queries needed to be selected using the specific terms which were identified in Jansen _et al._ ([2000](#jan00)) (Table 1). Out of the total of 1,025,910 queries, 32,664 image queries remained.

<table><caption>

**Table 1: Image terms in the queries ([Jansen _et al._ 2000](#jan00))**</caption>

<tbody>

<tr>

<td>

art, bitmap,bmp, .bitmap, .bmp, camera, cartoon, gallery, gif, .gif, image, images, jpeg, jpg, pcx, .jpeg, .jpg, .pcx, photo, photographs, photograph, photos, pic, pics, .pic, pics, picture, pictures, png, .png, tif, tiff, .tif, .tiff</td>

</tr>

</tbody>

</table>

For the second phase, each of 32,664 queries was reviewed to eliminate the following queries from the data set:

1.  repeated queries which were re-sent by users without change,
2.  pornographic terms in queries (according to Goodrum and Spink's ([2001](#goo01)) study, which used the same data set, twenty-five terms among the 100 most frequent search terms dealt with sexual content),
3.  queries containing simply 'image, picture, photo, etc.', and
4.  others (such as non-English queries).

After the de-selection process, a total of 8,444 queries and 5,688 sessions remained. For the third phase, three subsets of queries, initial query, second revised query and third revised query, were extracted for two reasons. First, some sessions include a large number of queries; for example one session has thirty queries. If the queries are analysed as a whole, highly repeated query terms in one session may cause biased results (e.g., a query term occurring thirty times in one session should be distinguished from a query term occurring thirty times in thirty different sessions). To eliminate high-frequency queries generated by a single searcher, queries were analysed by search stages. Secondly, by analysing queries based on the search stages, it is possible to determine whether there are any differences in features of search queries during the progress of the search. Finally, since the tagging system used in this study allows only individual words as tags, search queries were also parsed into one-word search terms, except for people's names. Then, to exclude highly subjective search terms, only terms that appear more than three times were used for comparing user-supplied tags (see Table 2).

<table><caption>

**Table 2: Features of query in terms of query revision process**</caption>

<tbody>

<tr>

<th>Query</th>

<th>Number of queries</th>

<th>Number of unique terms occurring more than three times</th>

</tr>

<tr>

<td>Initial query</td>

<td>5,688</td>

<td>629</td>

</tr>

<tr>

<td>2nd revised query</td>

<td>1,478</td>

<td>135</td>

</tr>

<tr>

<td>3rd revised query</td>

<td>598</td>

<td>60</td>

</tr>

</tbody>

</table>

A data set, consisting of user-supplied tags, was collected from _Flickr_. Using the API provided by the _Flickr_ Website, 33,742 tags assigned to 8,998 photographs were collected - half of the photographs were uploaded in September and October of 2004 and the other half were uploaded in May 2007\. A possible limitation of this study is the time difference between the _Flickr_ data set and the Excite search query. However, an analysis demonstrated no differences between tags generated in 2004 and in 2007 in terms of categorization distribution and specificity levels. Therefore, based on this result, it is assumed that the time difference between the two data sets does not significantly influence the current study results. Since tags provided by a single user can be too subjective, 535 unique tags provided by more than two users were identified as the data set.

### Comparison tools

We adopted and revised a classification scheme developed by Shatford ([1986](#sha86)) (see Table 3) to compare category distributions of terms used in tags and queries. Shatford proposed categorizing image subjects as _Generic of_, _Specific of_ and _About_, based on Panofsky's theory which describes three levels of pictorial meanings. She then developed a faceted classification scheme by applying _Who_, _What_, _When_ and _Where_ facets to those three categories. Shatford's faceted classification scheme has been used in examining the categories of meanings included in an image and which categories are dominant during a search for images ([Choi and Rasmussen 2003](#cho03); [Armitage and Enser 1997](#arm97)). This study investigates whether there are differences in category distributions between user-supplied tags and search terms.

<table><caption>

**Table 3: Category of pictorial meaning**</caption>

<tbody>

<tr>

<th colspan="2">Shatford's faceted classification</th>

<th>Revised category</th>

<th>Example</th>

</tr>

<tr>

<td rowspan="6">Abstract (A)</td>

<td>Abstract object (A1)</td>

<td>Mythical or fictitious being (A1)</td>

<td>Dragon</td>

</tr>

<tr>

<td rowspan="3">Emotion/Abstraction (A2)</td>

<td>Symbolic value (A2-1)</td>

<td>Classic</td>

</tr>

<tr>

<td>General feeling, atmosphere (A2-2)</td>

<td>Cold</td>

</tr>

<tr>

<td>Individual affection, emotional cue (A2-3)</td>

<td>Happy</td>

</tr>

<tr>

<td>Abstract location (A3)</td>

<td>Place symbolized (A3)</td>

<td>Urban</td>

</tr>

<tr>

<td>Abstract time (A4)</td>

<td>Emotion, abstraction symbolized by time (A4)</td>

<td>-</td>

</tr>

<tr>

<td rowspan="7">Generic (G)</td>

<td rowspan="3">Generic object (G1)</td>

<td>Kind of person, people, parts of a person (G1-1)</td>

<td>Baby</td>

</tr>

<tr>

<td>Kind of animal, parts of an animal (G1-2)</td>

<td>Bear</td>

</tr>

<tr>

<td>Kind of thing (G1-3)</td>

<td>Airplane</td>

</tr>

<tr>

<td rowspan="2">Generic event/activity (G2)</td>

<td>Kind of event (G2-1)</td>

<td>Birthday</td>

</tr>

<tr>

<td>Kind of action (G2-2)</td>

<td>Bowling</td>

</tr>

<tr>

<td>Generic location (G3)</td>

<td>Kind of place (G3)</td>

<td>Beach</td>

</tr>

<tr>

<td>Generic time (G4)</td>

<td>Cyclical time, time of day (G4)</td>

<td>Morning</td>

</tr>

<tr>

<td rowspan="7">Specific (S)</td>

<td rowspan="3">Specific object (S1)</td>

<td>Individually named person (S1-1)</td>

<td>Chris</td>

</tr>

<tr>

<td>Individually named animal (S1-2)</td>

<td>Heron</td>

</tr>

<tr>

<td>Individually named thing (S1-3)</td>

<td>Sega</td>

</tr>

<tr>

<td rowspan="2">Specific event/activity (S2)</td>

<td>Individually named event (S2-1)</td>

<td>Olympic</td>

</tr>

<tr>

<td>Individually named action (S2-2)</td>

<td>-</td>

</tr>

<tr>

<td>Specific location (S3)</td>

<td>Individually named geographic location (S3)</td>

<td>Florida</td>

</tr>

<tr>

<td>Specific time (S4)</td>

<td>Linear time (date or period) (S4)</td>

<td>2007</td>

</tr>

<tr>

<td rowspan="6">[Others]</td>

<td> </td>

<td>Colour (C)</td>

<td>Black</td>

</tr>

<tr>

<td> </td>

<td>Boolean + search command (B)</td>

<td>AND, Find</td>

</tr>

<tr>

<td> </td>

<td>Image related (I)</td>

<td>Photo etc.</td>

</tr>

<tr>

<td> </td>

<td>Flickr related (F)</td>

<td>Geotag</td>

</tr>

<tr>

<td> </td>

<td>Number (N)</td>

<td>1</td>

</tr>

<tr>

<td> </td>

<td>Part of speech (P)</td>

<td>And</td>

</tr>

</tbody>

</table>

For comparing the level of term specificity, the basic level theory was adopted. The basic theory explains that concepts can be categorized into one of three levels, the superordinate level, the basic level or the subordinate level. Experimental studies have demonstrated most people tend to use the basic level concept rather than the superordinate or subordinate concept ([Rosch _et al._ 1976](#ros76)). Since it has been found that a set of basic level terms are dominantly used and commonly shared by general users, researchers in library and information science assumed that basic level terms should be the level of specificity for concepts, and should receive focus during the indexing process ([Bates 1998](#bat98); [Green 2006](#gre06)). By following this assumption, this study examined the level of specificity of user-supplied tags and search terms by applying the basic level theory. Since most research on basic level theory has explored concrete objects and colours, this study also analysed tags and search terms in the _Generic_ and _Colour_ categories.

Rosch and her colleagues demonstrated features of superordinate, basic and subordinate categories through their empirical studies ([Rosch _et al._ 1976](#ros76)), but they did not provide established criteria which can clearly distinguish those three categories; whereas, in information scince, some recent studies developed their coding schemes for applying the basic level theory ([Green 2006](#gre06); [Rorissa 2008](#ror08); [Rorissa and Iyer 2008](#ror082)). This study attempted to establish a coding scheme which reflects an existing hierarchical structure among concepts in addition to considering features of three categories illustrated by previous studies.

This study made use of the hierarchies appearing in the Library of Congress Thesaurus for Graphic Materials (hereafter, 'the Thesaurus') by following three steps. First, it examined how nine taxonomies used in the empirical study of Rosch _et al._ ([1976](#ros76)) are designated in the the Thesaurus hierarchy (Figure 1). We found that the absolute level of depth appearing in the Thesaurus hierarchy cannot be directly used in deciding three categories. For example, in the case of an `Object → Food → Fruit → Apple` hierarchy, the lowest-level word, `Apple`, obviously satisfies features of the basic level, the upper three concepts belong to the superordinate level and this hierarchy does not include a subordinate level. The examples of `Hammer, Saws` and `Crosscut saws` shows that two basic-level terms, `Hammer` and `Saws`, belong to two different levels in the Thesaurus and a subordinate term `Crosscut saws` is placed at the same level as `Hammer`. Although the absolute depth of the Thesaurus's hierarchy cannot be a criterion for deciding basic-level categories, it was obvious that considering its hierarchical relations among concepts can help make decisions on basic levels. Therefore, secondly, other tags and terms not included in nine taxonomies but found in the Thesaurus were categorized into one of three levels. This was done by considering features of the three categories as well as Thesaurus hierarchies. Finally, tags and terms not included in the Thesaurus were also categorized in a consistent way (refer to [Yoon(2009)](#yoon09) for a more detailed explanation). With regards to basic level colours, the analysis process was more straightforward because eleven basic colours were identified in a previous study ([Berlin and Kay 1969](#ber69)): black, white, grey, red, yellow, green, blue, pink, orange, brown, and purple.

<figure>

![figure 1\. tgm hierarchy and rosch's nine taxonomies. bolded concepts exist in nine taxonomies used by rosch, et al. (1976).](../p408fig1.jpg)

<figcaption>

**Figure 1\. TGM hierarchy and Rosch's nine taxonomies. Bolded concepts exist in nine taxonomies used by Rosch _et al._ ([1976](#ros76))**</figcaption>

</figure>

Categories and term specificity were coded by a trained masters' level student in the School of Library and Information Science at the University of South Florida. For checking the reliability of the coding for categorical analysis, two methods were used. First, tags and query terms were sorted by attribute and then alphabetical order, and then one of the researchers reviewed the coding, discussed with the student the anomalous codes and corrected anomalous codes (error rate < .01%). Secondly, another trained masters' level student in the same school performed coding checks on 10% of the records. The percentages of inter-coder agreement were 92% for user-supplied tags and 96.4% for search terms. The reliability of the coding for term specificity analysis was checked by examining inter-coder agreement. Again a trained masters' level student in the same school performed coding checks on 10% of the records. The percentages of inter-coder agreement were 89% for both user-supplied tags and search terms.

## Findings

### General description by categories

As a way of identifying characteristics of user-supplied tags and search query terms, a general observation of categorical distributions was described, respectively. First, as shown in Table 4, categorical distributions of user-supplied tags were indicated with respect to the number of unique tags and tag occurrence. The Generic category is the highest number of unique tags (338 unique tags, 63.18%). The Specific category is the next holding 105 unique tags at approximately 20%. The Abstract category and _Flickr_ category show similar percentages of 8.05% and 6.17%, respectively. The _Part of speech_ category takes only three unique tags, less than 1%. It can be noted that there is uniformity across the number of unique tags and tag occurrence. The tags in the _Generic_ category appeared most frequently, 4,905 times or 52.10%, and those in the _Specific_ category appeared 2,740 times (29.97%). The _Flickr_, _Abstract_ and _Colour_ category appeared 697 times (7.45%), 594 times (6.35%) and 389 times (7.45%), respectively. In general, an overall observation between the number of unique tags and their occurrence confirms that categories with more unique terms have more occurrences of those unique tags.

<table><caption>

**Table 4: Frequencies of unique tag and tag occurrence**</caption>

<tbody>

<tr>

<th rowspan="2">Category</th>

<th colspan="2">Unique tag</th>

<th colspan="2">Tag occurrence</th>

</tr>

<tr>

<th>Number</th>

<th>%</th>

<th>Number</th>

<th>%</th>

</tr>

<tr>

<td>Abstract</td>

<td>43</td>

<td>8.04</td>

<td>594</td>

<td>6.35</td>

</tr>

<tr>

<td>Colour</td>

<td>13</td>

<td>2.43</td>

<td>389</td>

<td>4.16</td>

</tr>

<tr>

<td>Generic</td>

<td>338</td>

<td>63.18</td>

<td>4,905</td>

<td>52.40</td>

</tr>

<tr>

<td>Specific</td>

<td>105</td>

<td>19.63</td>

<td>2,740</td>

<td>29.27</td>

</tr>

<tr>

<td>Part of speech</td>

<td>3</td>

<td>0.56</td>

<td>35</td>

<td>0.37</td>

</tr>

<tr>

<td>Flickr</td>

<td>33</td>

<td>6.17</td>

<td>697</td>

<td>7.45</td>

</tr>

<tr>

<td>Total</td>

<td>535</td>

<td>100</td>

<td>9,360</td>

<td>100</td>

</tr>

</tbody>

</table>

On the other hand, Table 5 shows the uniqueness and occurrence of search terms in three stages of the search process: initial, second, and third stages. Search term distributions in categories are opposite to tag distributions, with more unique tags appearing more frequently in tag distributions. It can be noted that there is little uniformity across the number of unique terms and term occurrence. In the initial stage, unique terms in the _Generic_, _Specific_, and _Abstract_ categories account for more than 80%, but _Image related_, _Part of speech_, and _Boolean categories_ comprise more than 75% of term occurrences. The tendency for several non-semantic terms to occur very frequently in image search queries similarly appeared in the second and third stages with slight variations. The search query terms in the _Generic_ category show the highest percentage of unique terms in all three stages. In the case of term occurrence distributions, the _Image related_ (I) category accounts for approximately 50% of total term occurrences. As mentioned above, users are supposed to include image related terms in order to articulate their visual information needs when using the Excite search engine. Also, this study finds that users frequently used Boolean (B) terms and Part of Speech (P) when articulating their search needs into queries.

In summary, as shown in Table 4 and Table 5, there are clear differences between tags and search query terms. More specifically, it is recognized that there is considerable discrepancy due to characteristics unique to either search query terms or tags. For instance, search query terms are likely to contain substantial numbers of _Image related_ terms and _Boolean operators_ in order to express users' visual information needs in a query form; whereas tags include _Flickr_ related tags, which are only meaningful in _Flickr_ communities. Since this study compares tags and search queries in order to see the potential of tags as a user-centred subject indexing mechanism, it is reasonable to select semantically meaningful categories such as _Abstract_, _Colour_, _Generic_, and _Specific_ as a way of analysing the differences between tags and search query terms.

<table><caption>

**Table 5: Frequencies of unique search term and term occurrence**</caption>

<tbody>

<tr>

<th rowspan="3">Category</th>

<th colspan="4">Initial search term</th>

<th colspan="4">2nd search term</th>

<th colspan="4">3rd search term</th>

</tr>

<tr>

<th colspan="2">Unique term</th>

<th colspan="2">Term occurrence</th>

<th colspan="2">Unique term</th>

<th colspan="2">Term occurrence</th>

<th colspan="2">Unique term</th>

<th colspan="2">Term occurrence</th>

</tr>

<tr>

<th>No.</th>

<th>%</th>

<th>No.</th>

<th>%</th>

<th>No.</th>

<th>%</th>

<th>No.</th>

<th>%</th>

<th>No.</th>

<th>%</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>A</td>

<td>81</td>

<td>13.41</td>

<td>596</td>

<td>4.17</td>

<td>18</td>

<td>13.95</td>

<td>96</td>

<td>3.47</td>

<td>6</td>

<td>11.54</td>

<td>25</td>

<td>2.27</td>

</tr>

<tr>

<td>C</td>

<td>8</td>

<td>1.32</td>

<td>88</td>

<td>0.62</td>

<td>3</td>

<td>2.33</td>

<td>20</td>

<td>0.72</td>

<td>1</td>

<td>1.92</td>

<td>6</td>

<td>0.55</td>

</tr>

<tr>

<td>G</td>

<td>276</td>

<td>45.70</td>

<td>1904</td>

<td>13.32</td>

<td>65</td>

<td>50.39</td>

<td>326</td>

<td>11.78</td>

<td>20</td>

<td>38.46</td>

<td>93</td>

<td>8.45</td>

</tr>

<tr>

<td>S</td>

<td>138</td>

<td>22.85</td>

<td>734</td>

<td>5.13</td>

<td>12</td>

<td>9.30</td>

<td>47</td>

<td>1.70</td>

<td>4</td>

<td>7.69</td>

<td>12</td>

<td>1.09</td>

</tr>

<tr>

<td>P</td>

<td>37</td>

<td>6.13</td>

<td>1581</td>

<td>10.62</td>

<td>12</td>

<td>9.30</td>

<td>422</td>

<td>15.25</td>

<td>8</td>

<td>15.38</td>

<td>180</td>

<td>16.36</td>

</tr>

<tr>

<td>N</td>

<td>10</td>

<td>1.66</td>

<td>49</td>

<td>0.34</td>

<td>2</td>

<td>1.55</td>

<td>7</td>

<td>0.25</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>I</td>

<td>22</td>

<td>3.64</td>

<td>7424</td>

<td>51.92</td>

<td>14</td>

<td>10.85</td>

<td>1294</td>

<td>46.77</td>

<td>11</td>

<td>21.15</td>

<td>547</td>

<td>49.73</td>

</tr>

<tr>

<td>B</td>

<td>3</td>

<td>0.50</td>

<td>1848</td>

<td>12.92</td>

<td>2</td>

<td>1.55</td>

<td>549</td>

<td>19.84</td>

<td>2</td>

<td>3.85</td>

<td>237</td>

<td>21.55</td>

</tr>

<tr>

<td>O</td>

<td>29</td>

<td>4.80</td>

<td>138</td>

<td>0.97</td>

<td>1</td>

<td>0.78</td>

<td>6</td>

<td>0.22</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>Total</td>

<td>604</td>

<td>100.00</td>

<td>14299</td>

<td>100.00</td>

<td>129</td>

<td>100.00</td>

<td>2767</td>

<td>100.00</td>

<td>52</td>

<td>100.00</td>

<td>1100</td>

<td>100.00</td>

</tr>

<tr>

<td colspan="13">

A: Abstract, C: Colour, G: Generic, S: Specific, P: Part of speech, N: Number, I: Image related, B: Boolean, O: Others</td>

</tr>

</tbody>

</table>

### Categorical comparisons

Figure 2 demonstrates categorical distributions of unique terms used in tags, the initial search stage, the second search stage and the third search stage. As shown in Figure 2, the overall pattern among tags, initial search terms, second search terms, and third search terms are found to be similar. The _Generic_ category accounts for the majority of tags and search terms in three different stages, and the _Colour_ category comprises only a minor portion. The _Abstract_ and _Specific_ categories are second to the _Generic_ categories, but the order between the two categories is dependent on whether they are in tags, initial search terms, second search terms, or third search terms.

<figure>

![figure 2\. categorical distribution of tags, initial, 2nd, & 3rd search terms](../p408fig2.jpg)

<figcaption>

**Figure 2\. Categorical distribution of tags, initial, second, & third search terms**</figcaption>

</figure>

Based on overall categorical distributions of tags and terms in three stages, a chi-squared test was used to examine whether differences among them are statistically significant. As shown in Table 6, there are significant differences in category distributions among the tag and the search terms in three stages. In addition, there are significant differences between the categories of tags and initial search terms. Even among the search terms in different stages, there are significant differences in categorical distributions.

<table><caption>

**Table 6: Chi-squared results for category distribution**</caption>

<tbody>

<tr>

<th>Row variable</th>

<th>Column variable</th>

<th rowspan="2">Chi squared</th>

<th rowspan="2">df</th>

<th rowspan="2">p</th>

</tr>

<tr>

<th>Category</th>

<th>Source term</th>

</tr>

<tr>

<td rowspan="3">Abstract; Colour;  
Generic; Specific</td>

<td>Tag, Initial, 2nd, & 3rd search terms</td>

<td>34.422</td>

<td>(4-1)*(4-1)=9</td>

<td>0.000</td>

</tr>

<tr>

<td>Tag & Initial search term</td>

<td>23.562</td>

<td>(4-1)*(2-1)=3</td>

<td>0.000</td>

</tr>

<tr>

<td>Initial, 2nd, & 3rd search terms</td>

<td>13.359</td>

<td>(4-1)*(3-1)=6</td>

<td>0.038</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 7: Frequencies of tag and search terms in four categories**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="2">Tag</th>

<th colspan="2">Initial search term</th>

<th colspan="2">2nd search term</th>

<th colspan="2">3rd search term</th>

</tr>

<tr>

<th> </th>

<th>Number</th>

<th>%</th>

<th>Number</th>

<th>%</th>

<th>Number</th>

<th>%</th>

<th>Number</th>

<th>%</th>

</tr>

<tr>

<td>A1</td>

<td>1</td>

<td>0.20</td>

<td>8</td>

<td>1.59</td>

<td>2</td>

<td>2.04</td>

<td>2</td>

<td>6.45</td>

</tr>

<tr>

<td> A2-1</td>

<td>20</td>

<td>4.01</td>

<td>37</td>

<td>7.36</td>

<td>9</td>

<td>9.18</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td> A2-2</td>

<td>11</td>

<td>2.20</td>

<td>27</td>

<td>5.37</td>

<td>5</td>

<td>5.10</td>

<td>2</td>

<td>6.45</td>

</tr>

<tr>

<td> A2-3</td>

<td>7</td>

<td>1.40</td>

<td>5</td>

<td>0.99</td>

<td>1</td>

<td>1.02</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td>A2</td>

<td>38</td>

<td>7.62</td>

<td>69</td>

<td>13.72</td>

<td>15</td>

<td>15.31</td>

<td>4</td>

<td>12.90</td>

</tr>

<tr>

<td>A3</td>

<td>4</td>

<td>0.80</td>

<td>4</td>

<td>0.80</td>

<td>1</td>

<td>1.02</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>A4</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>A</td>

<td>43</td>

<td>8.62</td>

<td>81</td>

<td>16.10</td>

<td>18</td>

<td>18.37</td>

<td>6</td>

<td>19.35</td>

</tr>

<tr>

<td>C</td>

<td>13</td>

<td>2.61</td>

<td>8</td>

<td>1.59</td>

<td>3</td>

<td>3.06</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td> G1-1</td>

<td>28</td>

<td>5.61</td>

<td>48</td>

<td>9.54</td>

<td>17</td>

<td>17.35</td>

<td>6</td>

<td>19.35</td>

</tr>

<tr>

<td> G1-2</td>

<td>31</td>

<td>6.21</td>

<td>32</td>

<td>6.36</td>

<td>6</td>

<td>6.12</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td> G1-3</td>

<td>189</td>

<td>37.88</td>

<td>109</td>

<td>21.67</td>

<td>23</td>

<td>23.47</td>

<td>8</td>

<td>25.81</td>

</tr>

<tr>

<td>G1</td>

<td>248</td>

<td>49.70</td>

<td>189</td>

<td>37.57</td>

<td>46</td>

<td>46.94</td>

<td>15</td>

<td>48.39</td>

</tr>

<tr>

<td> G2-1</td>

<td>11</td>

<td>2.20</td>

<td>8</td>

<td>1.59</td>

<td>4</td>

<td>4.08</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td> G2-2</td>

<td>19</td>

<td>3.81</td>

<td>39</td>

<td>7.75</td>

<td>7</td>

<td>7.14</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>G2</td>

<td>30</td>

<td>6.01</td>

<td>47</td>

<td>9.34</td>

<td>11</td>

<td>11.22</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td>G3</td>

<td>52</td>

<td>10.42</td>

<td>34</td>

<td>6.76</td>

<td>7</td>

<td>7.14</td>

<td>3</td>

<td>9.68</td>

</tr>

<tr>

<td>G4</td>

<td>8</td>

<td>1.60</td>

<td>6</td>

<td>1.19</td>

<td>1</td>

<td>1.02</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td>G</td>

<td>338</td>

<td>67.74</td>

<td>276</td>

<td>54.87</td>

<td>65</td>

<td>66.33</td>

<td>20</td>

<td>64.52</td>

</tr>

<tr>

<td> S1-1</td>

<td>13</td>

<td>2.61</td>

<td>49</td>

<td>9.74</td>

<td>6</td>

<td>6.12</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td> S1-2</td>

<td>2</td>

<td>0.40</td>

<td>1</td>

<td>0.20</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td> S1-3</td>

<td>2</td>

<td>0.40</td>

<td>32</td>

<td>6.36</td>

<td>3</td>

<td>3.06</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>S1</td>

<td>17</td>

<td>3.41</td>

<td>82</td>

<td>16.30</td>

<td>9</td>

<td>9.18</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td> S2-1</td>

<td>3</td>

<td>0.60</td>

<td>6</td>

<td>1.19</td>

<td>1</td>

<td>1.02</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td> S2-2</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>S2</td>

<td>3</td>

<td>0.60</td>

<td>6</td>

<td>1.19</td>

<td>1</td>

<td>1.02</td>

<td>1</td>

<td>3.23</td>

</tr>

<tr>

<td>S3</td>

<td>76</td>

<td>15.23</td>

<td>45</td>

<td>8.95</td>

<td>2</td>

<td>2.04</td>

<td>3</td>

<td>9.68</td>

</tr>

<tr>

<td>S4</td>

<td>9</td>

<td>1.80</td>

<td>5</td>

<td>0.99</td>

<td>0</td>

<td>0.00</td>

<td>0</td>

<td>0.00</td>

</tr>

<tr>

<td>S</td>

<td>105</td>

<td>21.04</td>

<td>138</td>

<td>27.44</td>

<td>12</td>

<td>12.24</td>

<td>4</td>

<td>12.90</td>

</tr>

<tr>

<td>Total</td>

<td>499</td>

<td>100.00</td>

<td>503</td>

<td>100.00</td>

<td>98</td>

<td>100.00</td>

<td>31</td>

<td>100.00</td>

</tr>

<tr>

<td colspan="9">

(A: Abstract, C: Color, G: Generic, S: Specific)</td>

</tr>

</tbody>

</table>

As shown in Table 7, categorical distributions were analysed in detail using the sub-categories for _Abstract_, _Colour, Generic_, and _Specific_. From the _Abstract_ category, image searchers on the Web used abstract terms more frequently in all three search stages compared to _Flickr_ users (8.62% in tags vs. 16.10% in initial search, 18.37% in second search, and 19.35% in third search). More specifically, Figure 3 presents various search terms that appear in the _Mythical and fictitious beings_ (A1) and the _Emotion/Abstraction_ (A2) categories. In the A1 category, terms such as angel, devil, gods, ghost and so on, were only found as search terms. In the A2 category, search terms in _Symbolic value_ (A2-1) and _Atmosphere_ (A2-2) were more diverse than tags. The _Abstract location_ category (A3), however, was identified nearly in similar proportions. The _Abstract time_ category (A4) was neither used in tags or search query terms.

<figure>

![figure 3\. detailed comparison in abstract category](../p408fig3.jpg)

<figcaption>

**Figure 3\. Detailed comparison in the _Abstract_ category**</figcaption>

</figure>

For the _Colour_ category, it was found that basic colour terms identified in a previous study ([Berlin and Kay 1969](#ber69)) were used in both tags and search query terms.

By examining the subcategories of the _Generic_ category in Figure 4, it can be noted that the subcategories of G1-3 (_Kind of thing_), G1-2 (_Kind of animal_), G2-1 (_Kind of event_), and G3 (_Generic location_) present more diversity in tags than in search query terms. One plausible explanation for this can be deduced by considering photo-storing behaviors in _Flickr_. Just as with analogue photo albums, users might use _Flickr_ to store travel photos, pet photos, etc. In this sense, image taggers are more likely to use various generic terms compared to image searchers on the Web. On the other hand, search query terms in G1-1 (_Kind of person_) and G2-2 (_Kind of action_) are more various compared to tags. Whereas it was difficult to identify what type of G2-2 terms were more frequently used as search queries, in the G1-1 category, terms representing people's occupations (fighter, knights, president, queens, sailor, slave, wife and so on) were prominent in search terms.

<figure>

![figure 4\. detailed comparison in generic category](../p408fig4.jpg)

<figcaption>

**Figure 4\. Detailed comparison in the _Generic_ category**</figcaption>

</figure>

The _Specific_ category is shown in Figure 5\. By examining subcategories, it was found that tags in S1-2 (_Individually named animal_), S3 (_Individually named geographic location_), and S4 (_linear time: date and period_) were more diversely used than search query terms in overall, mainly because of S3 (15.23% out of 21.04%). This result can be comprehended in terms of _Flickr_'s photo album features, because _Flickr_ users often apply _Specific location_ names when they upload their pictures for their travel photos and there are only a limited number of popular places where people travel. On the other hand, S1-1 (_individually named person_), S1-3(_individually named thing_), and S2-1(_individually named event_) were in a variety of search terms compared to tags. This trend can be understood due to search engines' general usages, since users often want to find photos of celebrities, cartoon or movie characters, and specific brand names. For S2-2 (_individually named action_), there was no incidence in tags or search query terms.

<figure>

![figure 5\. detailed comparison in specific category](../p408fig5.jpg)

<figcaption>

**Figure 5\. Detailed comparison in the _Specific_ category**</figcaption>

</figure>

### Term specificity comparison

With respect to term specificity in tags and search query terms, the level of term specificity was examined in the _Colour_ and _Generic_ categories based on the basic level theory. In the case of the _Colour_ category, eleven basic colour names were used in tags and all stages of search terms. In the _Generic_ category, as noted in Table 8 and Figure 6, the overall distribution of term specificity among tags and search terms was similar; the distribution pattern, with basic level concepts most frequent, was also consistent with the related studies' results. However, this study result also demonstrated that compared to tags, search engine users tend to use subordinate level terms less frequently (13.61% from tag vs. 6.88%, 6.15% & 5.00% from search terms). In addition, contrasting basic and subordinate levels, it seems that the superordinate level in search terms on the Web relatively increases as users revise search query terms toward second and third search phases. In other words, while _Flickr_ users tag images by using more specific terms, Web searchers tend to use superordinate terms more frequently and attempt broader terms when revising initial search queries.

<table><caption>

**Table 8: Basic level distribution in the _Generic_ category**</caption>

<tbody>

<tr>

<th>Level</th>

<th colspan="2">Tag</th>

<th colspan="2">Initial search term</th>

<th colspan="2">2nd search term</th>

<th colspan="2">3rd search term</th>

</tr>

<tr>

<th> </th>

<th>Number</th>

<th>%</th>

<th>Number</th>

<th>%</th>

<th>Number</th>

<th>%</th>

<th>Number</th>

<th>%</th>

</tr>

<tr>

<td>Superordinate</td>

<td>20</td>

<td>5.92</td>

<td>18</td>

<td>6.52</td>

<td>8</td>

<td>12.31</td>

<td>4</td>

<td>20.00</td>

</tr>

<tr>

<td>Basic</td>

<td>272</td>

<td>80.47</td>

<td>239</td>

<td>86.59</td>

<td>53</td>

<td>81.54</td>

<td>15</td>

<td>75.00</td>

</tr>

<tr>

<td>Subordinate</td>

<td>46</td>

<td>13.61</td>

<td>19</td>

<td>6.88</td>

<td>4</td>

<td>6.15</td>

<td>1</td>

<td>5.00</td>

</tr>

<tr>

<td>Total</td>

<td>338</td>

<td>100.00</td>

<td>276</td>

<td>100.00</td>

<td>65</td>

<td>100.00</td>

<td>20</td>

<td>100.00</td>

</tr>

</tbody>

</table>

<figure>

![figure 6\. graphical representation of basic level distribution](../p408fig6.jpg)

<figcaption>

**Figure 6\. Graphical representation of basic level distribution**</figcaption>

</figure>

A chi-squared analysis was performed at the level of term specificity of the _Generic_ category to determine whether there were any statistically significant differences among tags, initial search terms, second search terms, and third search terms. As shown in Table 9, it was found that there are significant differences in basic level distribution among the tags and three stages of search terms. Further analysis shows that whereas there is a significant difference between tags and initial search terms, there is no significant difference among search terms in the three stages.

<table><caption>

**Table 9: Chi-squared results for basic level distribution**</caption>

<tbody>

<tr>

<th>Row variable</th>

<th>Column variable</th>

<th rowspan="2">chi squared</th>

<th rowspan="2">df</th>

<th rowspan="2">p</th>

</tr>

<tr>

<th>Term specificity level</th>

<th>Source term</th>

</tr>

<tr>

<td rowspan="3">Superordinate;  
Basic;  
Subordinate</td>

<td>Tag, Initial, 2nd, & 3rd search terms</td>

<td>17.297</td>

<td>(3-1)*(4-1)=6</td>

<td>0.008</td>

</tr>

<tr>

<td>Tag & Initial search term</td>

<td>7.265</td>

<td>(3-1)*(2-1)=2</td>

<td>0.026</td>

</tr>

<tr>

<td>Initial, 2nd, & 3rd search terms</td>

<td>6.139</td>

<td>(3-1)*(3-1)=4</td>

<td>0.189</td>

</tr>

</tbody>

</table>

## Discussion

Recent studies have utilized user-supplied tags, especially _Flickr_ tags, as a way of representing images from users' perspectives and for indexing schemes and thesaurus constructions. Although these endeavours have been conducted on the assumption that user-supplied tags have considerable potential as a user-centred organization mechanism, there has been little research to understand how these tags compare to search terms. This study investigated the features of tags and search query terms by categorical comparisons and the level of specificity comparisons. In addition to examining overall patterns, statistical analyses were conducted to examine whether there were any significant differences in categories and specificity levels between tags and search query terms in three different stages.

In general, tags, initial, second, and third search terms appeared to have similar categorical and term specificity distribution; however, the results of chi-squared analyses demonstrated that there are significant differences both in categories and term specificity between tags and search query terms. Since this research is one of the first studies to compare tags and search queries, these findings can be explained in many ways. Explanations from a more fundamental perspective would be desirable in this sense. First, although both _Flickr_ and Web search engines contain general or non-domain-specific image collections which are open to public users, there exist to some extent unique characteristics that inherently distinguish the two collections. _Flickr_ users tag their own images not only for sharing with others (i.e., indexing), but also for storing and organizing their photos (i.e., describing), whereas search engine users search images (i.e., retrieving), which have been created by others without any concrete ideas of which images are searchable and how. For example, _Flickr_ users often upload pictures from their travels, producing many general and specific location tags, whereas search engine users are more likely to search pictures on the basis of specific information such as celebrities' names, cartoon characters, and products with specific brand names.

Secondly, a task-oriented perspective can explain differences between describing tasks in tags and retrieving tasks in queries. In the case of the _Abstract_ category, this result is consistent with Jogensen's ([1995](#jor95); [1998](#jor98)) and Fidel's ([1997](#fid97)) results which showed that users have a tendency to use abstract terms more frequently in retrieving tasks than in describing tasks. Again, this result supports the idea which emphasizes the importance of providing an access mechanism for abstract categories in image retrieval systems, in spite of the difficulties in representing abstract messages ([Greisdorf and O'Connor 2002](#gre02); [Black _et al._ 2004](#bla04); [Enser _et al._ 2007](#ens07)). The results of the analysis of term specificity level also can be understood on the same basis. As found in previous basic level studies on images ([Jogensen 2003](#jor03); [Rorissa 2008](#ror08)), overall basic level terms were dominant in tags as well as all stages of search terms. However, when statistically comparing tags and search queries, it was found that there are differences in the level of term specificities between describing and searching tasks; image searchers who do not have a clear idea of what they want to find are more likely to use superordinate level terms, whereas _Flickr_ users who describe their own photographs tend to use subordinate terms more frequently than searchers.

In this sense, the findings of this study might present a challenge to current research efforts on utilizing user-supplied tags as a promising access point for images. As introduced in the Related studies section, lines of research have attempted to utilize _Flickr_ tags in order to understand users' image describing patterns as well as to develop a user-centred controlled vocabulary. These recent studies have been based on the assumption that frequently used terms in _Flickr_ can be an access point in the image search process. Overall, patterns of categorical and specificity distribution results support this approach. However, statistical results demonstrated significant differences in categorical distribution and term specificity levels. The findings of this study suggest that although _Flickr_ tags, which are currently the most popular image tagging system, can be a valuable source for understanding user-centred image representation patterns, it is important to consider its image collection features and its user groups - i.e., Flickr users describe their own pictures. In other words, _Flickr_ tags can provide some basics for public users' image describing behaviours in general, but they need to be customized depending on the collection. In addition, the findings of this study suggest collecting tags should be collected for each collection, if possible, and then utilized for that particular collection, such as in the [Steve Museum](http://www.steve.museum/) project.

In addition to the comparisons between tags and search terms, this study compared search terms in different stages. In general, tags and initial search query terms are similar in terms of overall categorical distributions. The _Generic_ category is the most popular followed by the _Specific_ and the _Abstract_ categories; the _Colour_ category is the least popular. On the other hand, compared to initial search query terms, second search query terms and third search query terms present slightly different categorical distributions, as the _Abstract_ and _Generic_ categories are more frequently used, and the _Specific_ category is less prominently used. Although a significant difference was not identified, the term specificity level analysis showed that searchers tend to adopt more superordinate terms instead of subordinate terms as they revise search terms. This implies that an image retrieval system should facilitate users revising their searches by providing semantically related concepts including related abstract terms and superordinate concepts. Also, if users tend to avoid terms in the _Specific_ category due to the difficulties in finding alternative specific terms, the image retrieval system should provide useful guidelines for alternative terms - either other terms in the _Specific_ category or related terms in the _Generic_ category - for terms in _Specific_ category.

## Conclusion

Image descriptions supplied by users are clearly good resources to adopt when constructing user-centred indexing systems. Many efforts have attempted to understand the characteristics and features of tags, while little empirical research exists to explore user-supplied tags compared to search queries. In this sense, this study explored the differences between tags and queries submitted for searching images in order to investigate the features and characteristics of user-supplied tags in terms of user-centred indexing system construction.

This study identified differences between user-supplied tags and search queries for images in terms of categories and levels of specificity. Overall distribution of categories and levels of specificity were found to be similar between user-supplied tags and search query terms. The _Generic_ category is the most frequently used for both tags and search query terms. Following the _Generic_ category, the _Specific_ and _Abstract_ categories were next in frequency. The _Colour_ category was identified as the least used category.

The findings of this study are in line with previous research ([Chen 2001](#che01); [Collins 1998](#col98); [Choi and Rasmussen 2003](#cho03); [Jogensen 1998](#jor98)). Regarding levels of specificity, distribution in three levels (superordinate, basic, and subordinate) demonstrated that the basic level was most frequently used. Superordinate and subordinate levels followed. Moreover, statistical analyses on distributions were performed to examine whether the differences in categories and levels were statistically significant. In regards to categories, significant differences were found among tags, initial search terms, second search terms, and third search terms. Statistical analyses on the level of specificity demonstrated significant differences between tags and the three stages of search query terms, but no significant differences among the three different stages of a single search. While many possible explanations can be applied to the results of this study, one fundamental reason for these differences in categories and levels of specificity can be induced from the inherent functionality of each collection, _Flickr_ and image search engines on the Web. For instance, tags in _Flickr_ are mainly created for storing and sharing, not considering retrieval uses, while search queries for images on the Web are primarily for searching images. Another fundamental perspective is to understand the inherent differences because of dissimilar tasks such as searching and describing an image.

These findings have fundamental and practical implications. Basically, the findings of this study imply that directly utilizing _Flickr_ tags on user-centred indexing systems needs to be reconsidered. It is desirable to take into account collections, users' features, and differences in tasks when designing user-oriented index systems. More practically, involvement would at least address interface design issues of image searching and tagging. In terms of image searching and tagging interface design, the results in this work provide categorical and specificity guidelines for designing image retrieval and tagging system interfaces. For instance, image tagging and searching interfaces could employ appropriate categories and levels of specificity as users progress their searches.

Evidently, future studies and analyses are necessary to further comprehend the relationships between user-supplied tags and search queries for images. This study compared two different data sets, _Flickr_ and Web search queries. It is a meaningful approach to have used the most popular image tagging system in analysing user-tagging behaviors, because it demonstrated that _Flickr_ tags have their own unique features which cannot simply be generalized for other image collections. However, in order to investigate differences between two tasks, tagging and searching, future studies should compare two data sets which are extracted from a single collection. Also, tags might exist which do not match search queries but would assist users to navigate or browse image collections. Therefore, in addition to comparing tags and queries in quantitative ways, it should be investigated how users use tags during the actual image search process and how tags improve search effectiveness. Another future research area proposed by the current study is image search query reformulation. As this study demonstrated changes from initial search queries to transformed queries, once reformulation patterns can be specified, they will be useful in designing an interactive image retrieval system which effectively support the query revising process of users.

## Acknowledgements

The authors gratefully acknowledge Dr. Jim Jansen's generous sharing of Excite data and insightful reviews from two anonymous reviewers. We also thank Stacy Davis for her assistance in data analysis and Ga Young Lee for her various assistance.

## References

*   <a id="arm97"></a>Armitage, L.H. & Enser, P.G.B. (1997). Analysis of user need in image archives. _Journal of Information Science_, **23**(4), 287-299.
*   <a id="aur06"></a>Aurnhammer, M., Hanappe, P. & Steels, L. (2006). [Integrating collaborative tagging and emergent semantics for image retrieval.](http://www.webcitation.org/5j0KFvhoy) _In Proc. Of the Collaborative Web Tagging Workshop_ Retrieved November 10 2008 from http://www3.isrl.illinois.edu/~junwang4/langev/localcopy/pdf/aurnhammer06semanticsWWW.pdf Archived by WebCite® at http://www.webcitation.org/5j0KFvhoy)
*   <a id="bat98"></a>Bates, M. (1998). Indexing and access for digital libraries and the Internet: human, database, and domain factors. _Journal of the American Society for Information Science_, **49**(13), 1185-1205.
*   <a id="batl98"></a>Batley, S. (1988). Visual information retrieval: browsing strategies in pictorial databases. In: _Online information/'88_, Vol. 1, (Proceedings of the 12th International Online Information Meeting). (pp. 373-381). Oxford: Learned Information Ltd.
*   <a id="bea05"></a>Bearman, C. & Trant, J. (2005). [Social terminology enhancement through vernacular engagement: exploring collaborative annotation to encourage interaction with museum collections](http://www.webcitation.org/5j0KZ04nl). _D-Lib Magazine_, **11**(9). Retrieved June 2 2008 from http://www.dlib.org/dlib/september05/bearman/09bearman.html (Archived by WebCite® at http://www.webcitation.org/5j0KZ04nl)
*   <a id="ber69"></a>Berlin, B. & Kay, P. (1969). _Basic color terms: their universality and evolution_. Berkeley, CA: University of California.
*   <a id="bla04"></a>Black, J.A., Kahol, K., Tripathi, P., Kuchi, P. & Panchanathan, S. (2004). [Indexing natural images for retrieval based on kansei factors.](http://www.webcitation.org/5j0LJK9Q6) In _Human Vision and Electronic Imaging IX: Proceedings of the SPIE_, **5292**, 353-375\. Retrieved 13 August, 2009 from http://www.public.asu.edu/~kkahol/publications/kansei.pdf (Archived by WebCite® at http://www.webcitation.org/5j0LJK9Q6)
*   <a id="che01"></a>Chen, H. (2001). An analysis of image retrieval tasks in the field of art history. _Information Processing & Management_, **37**(5), 701-720.
*   <a id="cho03"></a>Choi, Y. & Rasmussen, E. (2003). Searching for images: the analysis of users' queries for image retrieval in American history. _Journal of the American Society for Information Science and Technology_, **54**(6), 498-511.
*   <a id="col98"></a>Collins, K. (1998). Providing subject access to images: a study of user queries. _The American Archivist_, **61**(1), 36-55.
*   <a id="eas03"></a>Eastman, C. & Jansen, B. J. (2003). Coverage, relevance, and ranking: the impact of query operators on Web search engine results. _ACM Transactions on Information Systems_, **21**(4), 383-411.
*   <a id="ens07"></a>Enser, P.G.B., Sandom, C.J., Hare, J.S. & Lewis, P.H. (2007). Facing the reality of semantic image retrieval. _Journal of Documentation_, **63**(4), 465-481.
*   <a id="fid94"></a>Fidel, R. (1994). User-centered indexing. _Journal of the American Society for Information Science_, **45**(8), 572-576.
*   <a id="fid97"></a>Fidel, R. (1997). The image retrieval task: implications for the design and evaluation of image databases. _The New Review of Hypermedia and Multimedia_, **3**, 181-199.
*   <a id="fur06"></a>Furnas, G., Fake, C., Ahn, L., Schachter, J., Golder, S., Fox, K., Davis, M., Marlow, C. & Naaman, M. (2006). Why do tagging systems work?. In _CHI '6 extended abstracts on human factors in computing systems_, (pp. 36-39). New York, NY: ACM Press.
*   <a id="gol06"></a>Golder, S.A. & Huberman, B.A. (2006). Usage patterns of collaborative tagging systems. _Journal of Information_, **32**(2), 198-208.
*   <a id=" goo03"></a>Goodrum, A., Bejune, M.M. & Siochi, A.C. (2003). A state transition analysis of image search patterns on the Web. In G. Goos, J. Hartmanis and J. van Leeuwen. (Eds.) _Image and Video Retrieval. Second International Conference, CIVR 2003 Urbana-Champaign, IL, USA, July 24-25, 2003 Proceedings_ (pp. 281-290). Berlin: Springer. (Lecture Notes in Computer Science, 2728)
*   <a id="goo01"></a>Goodrum, A. & Spink, A. (2001). Image searching on the Excite Web search engine. _Information Processing & Management_, **37**(2), 295-311.
*   <a id="gre06"></a>Green, R. (2006). [_Vocabulary alignment via basic level concepts. Final Report 2003 OCLC/ALISE Library and Information Science Research Grant Project_](http://www.webcitation.org/5j0RF151A). Dublin, OH: OCLC Online Computer Library, Inc. Retrieved May 14 2008 from http://www.oclc.org/programsandresearch/grants/reports/green/rg2005.pdf (Archived by WebCite® at http://www.webcitation.org/5j0RF151A)
*   <a id="gre02"></a>Greisdorf. H. & O'Connor, B. (2002). Modeling what users see when they look at images: a cognitive view point. _The Journal of Documentation_, **58**(1), 6-29.
*   <a id="guy06"></a>Guy, M. & Tonkin, E. (2006). [Folksonomies: tidying up tags?](http://www.webcitation.org/5j0RSPqV7). _D-Lib Magazine_, **12**(1). Retrieved May 21 2008 from http://www.dlib.org/dlib/january06/guy/01guy.html (Archived by WebCite® at http://www.webcitation.org/5j0RSPqV7)
*   <a id="has95"></a>Hastings, S.K. (1995). Query categories in a study of intellectual access to digitized art images. _Proceedings of the American Society for Information Science_, **32,** 3-8.
*   <a id="jan00"></a>Jansen, B.J., Goodrum, A. & Spink, A. (2000). [Searching for multimedia: analysis of audio, video, and image Web queries.](http://www.webcitation.org/5j0RgU7bT) _World Wide Web_, **3**, 249-254\. Retrieved 13 August, 2009 from http://ist.psu.edu/faculty_pages/jjansen/academic/pubs/jansen_www.pdf (Archived by WebCite® at http://www.webcitation.org/5j0RgU7bT)
*   <a id="jan05"></a>Jansen, B.J. & Spink, A. (2005). How are we searching the World Wide Web? An analysis of nine search engine transaction logs. _Information Processing & Management_, **42**(1), 248-263.
*   <a id=" jor95"></a>Jörgensen, C. (1995). Classifying images: criteria for grouping as revealed in a sorting task. In R. Schwartz (Ed.), _Advances in classification research 6 (proceedings of the 6th ASIS SIG/CR Classification Research Workshop)_ (pp. 45-64). Medford, NJ: Information Today.
*   <a id=" jor98"></a>Jörgensen, C. (1998). Attributes of images in describing tasks. _Information Processing & Management_, **34**(2/3), 161-174.
*   <a id=" jor03"></a>Jörgensen, C. (2003). _Image retrieval: theory and research_. Lanham, MD: Scarecrow Press.
*   <a id="jor07"></a>Jörgensen, C. (2007). [Image access, the semantic gap, and social tagging as a paradigm shift](http://www.webcitation.org/5j17lGhUa). In Joan Lussky, (Ed.) _Proceedings 18th Workshop of the American Society for Information Science and Technology Special Interest Group in Classification Research, Milwaukee, Wisconsin_. Retrieved June 20 2008 from http://dlist.sir.arizona.edu/2064/ (Archived by WebCite® at http://www.webcitation.org/5j17lGhUa)
*   <a id="mar06"></a>Marlow, C., Naaman, M., Boyd, D. & Davis, M. (2006). [_Position paper, tagging, taxonomy, Flickr, article, ToRead._](http://www.webcitation.org/5j1IwV2j5) Paper presented at the Collaborative Web Tagging Workshop, 15th International World Wide Web Conference, Edinburgh, Scotland, May 2006\. Retrieved June 13 2008 from: http://www.danah.org/papers/WWW2006.pdf (Archived by WebCite® at http://www.webcitation.org/5j1IwV2j5)
*   <a id="mat06"></a>Matusiak, K. (2006). Towards user-centered indexing in digital image collections. _OCLC Systems & Services: International Digital Library Perspectives_, **22**(4), 283-298.
*   <a id="mor08"></a>Morrison, P. J. (2008). Tagging and searching: Search retrieval effectiveness of folksonomies on the World Wide Web. _Information Processing & Management_, **44**, 1562-1579.
*   <a id="oco96"></a>O'Connor, B. C. (1996). _Explorations in indexing and abstracting: pointing, virtue, and power_. Englewood, CO: Libraries Unlimited.
*   <a id="ror08"></a>Rorissa, A. (2008). User-generated descriptions of individual images versus labels of groups of images: a comparison using basic level theory. _Information Processing & Management_, **44**(5), 1741-1753.
*   <a id="ror082"></a>Rorissa, A. & Iyer, H. (2008). Theories of cognition and image categorization: What category labels reveal about basic level theory. _Journal of the American Society for Information Science and Technology_, **59**(9), 1383-1392.
*   <a id="ros76"></a>Rosch, E., Mervis, C.B., Gray, W.D., Johnson, D.M. & Boyes-Braem, P. (1976). Basic objects in natural categories. _Cognitive Psychology_, **8**(3), 382-439.
*   <a id="sch06"></a>Schmitz, P. (2006). [_Inducing ontology from Flickr tags._](http://www.webcitation.org/5j1JkOAlA) Paper presented at the Collaborative Web Tagging Workshop, 15th International World Wide Web Conference, Edinburgh, Scotland, May 2006\. Retrieved November 10 2008 from http://www.semanticmetadata.net/hosted/taggingws-www2006-files/22.pdf (Archived by WebCite® at http://www.webcitation.org/5j1JkOAlA)
*   <a id="sha86"></a>Shatford, S. (1986). Analysing the subject of a picture: a theoretical approach. _Cataloging & Classification Quarterly_, **6**(3), 39-62.
*   <a id="shind"></a>Shirky, C. (n.d.). [Ontology is overrated: categories, links, and tags](http://www.webcitation.org/5j1PMWpJ7). Retrieved June 19 2008 from http://www.shirky.com/writings/ontology_overrated.html (Archived by WebCite® at http://www.webcitation.org/5j1PMWpJ7)
*   <a id="soe85"></a>Soergel, D. (1985). _Organizing information: principles of database and retrieval systems_. Orlando, FL: Academic Press.
*   <a id="spi02"></a>Spink, A., Jansen, B. J., Wolfram, D. & Saracevic, T. (2002). From E-sex to E-commerce: Web search changes. _IEEE Computer_, **35**(3), 107-111.
*   <a id="stv07"></a>Stvilia, B. & Jörgensen, C. (2007). End-user collection building behavior in Flickr. _Proceedings of the Annual Meeting of the American Society for Information Science and Technology_, **44**(1), 1-20
*   <a id="tra06"></a>Trant, J. (2006). Exploring the potential for social tagging and folksonomy in art museums: Proof of concept. _New Review of Hypermedia and Multimedia_, **12**(1), 83-105.
*   <a id="win06"></a>Winget, M. (2006). [_User-defined classification on the online photo sharing site Flickr...or, how I learned to stop worrying and love the million typing monkeys_](http://www.webcitation.org/5j1QG8p4W). Paper presented at the 17th Workshop of the American Society for Information Science and Technology Special Interest Group in Classification Research, Austin, Texas. Retrieved May 17 2008 from http://dlist.sir.arizona.edu/1854/ (Archived by WebCite® at http://www.webcitation.org/5j1QG8p4W)
*   <a id="yoon09"></a>Yoon, J. (2009). Towards a user-oriented thesaurus for non-domain-specific image collections. _Information Processing & Management_, **45**(4), 452-468.