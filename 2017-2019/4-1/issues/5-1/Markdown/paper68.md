#### Information Research, Vol. 5 No. 1, October 1999

# Experiencing information seeking and learning: a study of the interaction between two phenomena

#### [Louise Limberg](mailto:louise.limberg@ctv.gu.se)  
Högskolan i Borås  
Borås, Sweden

#### Abstract

> Reports the design and findings of a research project for a doctoral thesis on information seeking and use in a learning context. Theoretical frames were found in LIS use and user studies and in phenomenographic learning theory. The empirical study was conducted in a naturalistic setting with a group of 25 high school seniors. Phenomenographic method, designed to investigate variation in people's ways of experiencing phenomena in the world, was used. Three major ways of experiencing information seeking and use were identified; (a) fact-finding, (b) balancing information in order to make correct choices, (c) scrutinizing and analysing. The variation in information seeking and use interacted closely with variation in learning outcomes. These findings form a basis for further research on variation in users' ways of experiencing the content of information. They also provide a basis for didactic development of information literacy instruction.

## Research problem

This is the summary of a dissertation which grew out of a need for better research based understanding of information seeking and use in a specific context. Several researchers in library and information studies (LIS) pointed out the predominance of research on information needs and information seeking in the area of LIS user studies and expressed a need for the study of information _use_ (e.g., [Kuhlthau](#ref1) 1993; [Vakkari](#ref2) 1997; [Wilson](#ref2) 1981). The role of _context_ for information seeking behaviour has been stressed (e.g., [Dervin](#ref1) 1997; [Wilson](#ref2) 1981). The complex process of information seeking and use in learning contexts needs further exploration, according to, among others, [Kuhlthau](#ref1) (1993).

From practical experience in education on all levels including undergraduate university programmes, there is evidence that teaching methods are shifting from a transmission view of learning to problem oriented or problem based learning. This entails an increased use of libraries and a wide variety of information sources, which in turn calls for close cooperation between librarians and teachers. One problem in such cooperation seems to be that teachers tend to stress the subject matter of a learning assignment and underestimate the complexities of information seeking for the students. Librarians, on the other hand, will often prefer to focus on the information search process and disregard subject content or learning outcome.

There is a lack of research based knowledge about the interaction between how students seek and use information for learning assignments and what they actually learn about subject matter.

The aim of this disseration is to study information seeking through an explorative investigation of the interaction between information seeking and use and learning outcome. The work has been conducted along two interacting strands:

1.  an analysis of research and theory that I found relevant to the problem
2.  an empirical study of how high school seniors seek and use information to learn about the subject content of an assignment.

## Theoretical frame. Research overview

### Use and user studies

An overview of the field of user studies based on e.g., [Wilson](#ref2), T (1981, 1994) gives examples of theories and models of the information search process (e.g., [Dervin](#ref1) 1992; [Ellis _et al_.](#ref1) 1993; Höglund & [Persson](#ref2) 1980a and b; [Ingwersen](#ref2) 1996a; [Kuhlthau](#ref2) 1989). A common trait of the models is the effort to describe information seeking processes and behaviour on a general level and to disregard variation in information seeking.

Research on information seeking and use has applied theories from various disciplines e.g. communication (e.g., Dervin; Savolainen), sociology (e.g., Ellis, Höglund, Wilson), cognitive sciences (e.g., Allen, Belkin, Ingwersen). Since the mid-80's ARIST overviews of use and user studies have paid considerable attention to the cognitive approach ([Dervin & Nilan](#ref1) 1986; [Hewins](#ref1) 1990; [Allen](#ref1) 1991).

Kuhlthau's model of the information search process is of particular relevance to my study, since it was developed in a learning context. She emphasizes that her model describes the information search process as understood by users ([Kuhlthau](#ref2) 1989, 1991, 1993). Kuhlthau views information seeking as a process of seeking meaning.

Pitts studied students' (16-17 years old) use of information while they were engaged in a science assignment ([Pitts](#ref2) 1994). During data collection Pitts discovered that it was impossible to examine the students' use of information without paying attention to other aspects of their learning assignment. She found that students' information seeking and use was one of four identified aspects, or "strands", of learning. Subject matter was another such strand. These strands were constantly intertwined during the students' learning process.

Pitts' findings contradict the usual conception of information seeking as a general process, regardless of content (cf. e.g., [Ingwersen](#ref2) 1996a and b). On the contrary, she underlines that students never used their prior knowledge of information seeking in isolation, but always intertwined with prior knowledge from the other three learning strands, e.g. subject matter. These results raise new questions for research on information seeking behaviour.

It is worth observing that the overview of user studies showed a certain discontent among researchers over the large quantity of such studies and the lack of results that might explain or illuminate the problems or be of practical use for the profession (e.g., [Ellis](#ref1) 1992; [Enmark](#ref1) 1997; [Ford](#ref1) 1987; [Hewins](#ref1) 1990; [Hjørland](#ref1) 1993, 1995 p 41-42; [Malmsjö](#ref1) 1997; [Wilson](#ref2) 1994, p 42).

### Phenomenography

One aspect of my research problem is concerned with students' learning from the information they use. This leads to a want for knowledge from relevant learning theory, and I decided to use phenomenography, which may be described as both a set of theoretical assumptions and a methodology. The main reason for choosing phenomenography was the need to study _variation_ in students' learning outcomes and to compare this with _variation_ in students' information seeking and use. A general model of the information search process would not allow this kind of comparison between information seeking and learning outcomes.

The object of phenomenography is to explore people's different ways of experiencing or understanding or thinking about phenomena in the world. This means that it is not phenomena in the world as such that are the objects of interest but instead people's conceptions about phenomena.

The core concept of "conception" or "way of experiencing" is not a mental representation or a cognitive structure. It is a way of being aware of something ([Marton](#ref2) 1994, p I). A basic assumption in phenomenography is that it is possible to identify various ways of how people experience or conceptualize or understand phenomena and to describe these in a limited number of categories of description. A conception always has two dimensions, one focusing on _what_ people experience, i.e. the _content_ or the referential aspect, and the other focusing on _how_ someone thinks about the specific phenomenon, i.e. the _structural_ aspect (e.g., [Marton](#ref2) 1996, p 180).

The distinction between two types of questions like "What are the reasons for starvation in the world?" and "What do people think about the reasons for starvation in the world?" is fundamental to phenomenography. Phenomenographers ask the second type of question and call this a _second-order perspective_ ([Marton](#ref2) 1981, passim).

Marton insists on the non-psychological character of phenomenography. A conception is not a mental structure, it is a way of experiencing a specific phenomenon. An experience cannot be placed inside a person, it is a relation between a person and a specific phenomenon, and the object of research focuses on one as much as the other. "... phenomenography is about constitution. People's experiences of the world are relations between people and the world, reflecting one as much as the other" ([Marton](#ref2) 1996, p 168).

## Knowledge and learning

According to phenomenography knowledge is always linked to a specific content and can only be described as knowledge about something. A person's conception of something is this person's knowledge about this phenomenon. "Objective knowledge" about a phenomenon includes the various ways in which people think about this phenomenon. This means that knowledge is always provisional. It also means that knowledge is qualitative. Differences between people's knowledge is not a matter of more or less. To become more knowledgeable in a subject implies a qualitative change to a deeper and more complex understanding of a phenomenon.

This view of knowledge is closely related to the phenomenographic conception of learning as a change of ways of understanding a phenomenon. This implies criticism of conceptualizing or describing learning as a general process. Learning cannot be seen as separated from a specific learning content (cf. [Entwistle](#ref1) 1976).

## Comparison between theoretical approaches

The review of theory above shows differences between user studies and phenomenography concerning four aspects that I judge important for the design and understanding of my dissertation.

#### Content

It is characteristic for information science not to focus on how users experience or understand the content of information. User studies that adopt a cognitive approach often focus on cognitive structures or mental models. Other approaches have been to study information seeking behaviour among various groups, e.g. researchers in science or the humanities. These types of studies take a common interest in _structures_, cognitive structures in individuals or the structures of various disciplines.

Phenomenography always considers both content and structure and thus differs from LIS, that does not take content into account (other than for subject representation in information retrieval), in stead focusing on structures or processes.

Learning is a central concept of phenomenography but is seldom considered in user studies (unless when it concerns what users learn about information skills). A "change in cognitive structures" may be a corresponding term in information science, but how such changes happpen is not a primary focus of interest for research.

Kuhlthau underlines that "focusing the topic" or "formulation of a focus" is the pivotal point in her model of the search process. This implies that a topic content does interact with information seeking, but she has not investigated the character of this interaction. Pitts' findings also signify that how students think about and conduct their information seeking is not independent of subject matter, i.e. the content of information. This contradicts the established view of information science, and may give impulses for further research in this direction.

#### Ontology

The view of the relationship between information or learning content (the object) and the user or the learner (the subject) differs radically between user studies and phenomenography. This influences how a researcher views information seeking as the object of research. The cognitive viewpoint in LIS is explicitely dualistic in placing information (the object) outside the user (the subject). Information is said to be "transmitted via a channel" (e.g., [Ingwersen](#ref2) 1996b, p 101).

This dualistic view is in obvious contrast to the non-dualistic stance of phenomenography, "considering person and world to be internally related" ([Marton](#ref2) 1996, p 175). As mentioned earlier, conceptions or ways of experiencing, are viewed as relations between the experiencer and the specific phenomenon that is experienced. The phenomenon is part of both.

One problem connected to a non-dualistic stance is that it is impossible for human beings to discern alternative ways of experiencing something at the same time. Therefore we change perspectives, and this shifting between different perspectives shapes our thinking and understanding of the world. With this view, to learn something does not mean to receive knowledge or information, but instead it means that the relationship between person and world changes. The use of the terms "receiver" or "recipient" is well established in LIS. This terminology indicates ontological differences between the two research approaches.

#### Research perspective

As mentioned earlier, user studies often focus on mental processes, while phenomenographers focus on how people experience or conceptualize phenomena in the world. Phenomenographers use a second-order perspective. Research on cognitive structures or mental models use a first-order perspective, focusing on those structures or models or processes as such. There are examples of LIS user studies that chose a perspective similar to that of phenomenography (e.g., [Ellis](#ref1) 1989; [Ellis _et al_.](#ref1) 1993; [Kuhlthau](#ref2) 1991; [Schamber _et al_](#ref2). 1990). The models of Ellis and Kuhlthau build on users' perspectives of information seeking. Schamber _et al_. studied users' criteria of relevance judgements and their research focus was on relevance, not on specific differences between the persons interviewed.

#### Individual and group

Person-centered user studies as well as phenomenographic studies build on individual people's behaviour or understanding. While the aim of these studies in LIS is to try and identify different individual characteristics, e.g. cognitive styles or differences between various groups, phenomenographers focus on differences between people's ways of experiencing a specific phenomenon or situation. The most widely used data collection method in phenomenography is interviewing. As the same individual may express more than one way of understanding the phenomenon which is the object of research, the individual is not the basic unit of analysis. During analysis of data borders between the individuals are temporarily abandoned and attention is focused on differences between conceptions of phenomena, not on differences between individuals. This has consequences for the design of my empirical study and for the interpretation of results.

### Framework questions

In accordance with Kuhlthau's and Pitts' view of the information search process as distinguishable but integrated in the larger learning process, my research object is to study how these two processes interact with one another, viewed from the perspective of the learner. Consistent with my choice of phenomenographic theory and method the following four questions were formulated for the empirical study:

*   How do the students experience the information seeking process?
*   How do the students think about various information sources and their use of them?
*   How do the students understand or think about subject matter during and after the learning process?
*   How do students' conceptions of information seeking and use interact with their conceptions of subject matter?

## The Empirical Study

The empirically grounded assumption in phenomenography is that it is possible to describe variation in conceptions of phenomena through a limited number of categories of description. The goal of the research is often to provide as rich, comprehensive and substantial descriptions as possible of the ways in which people experience a phenomenon.

### Selection of case

Some prerequisites directed my selection of case. I had to find a group of students engaged in a learning assignment implying the independent seeking and use of information. The teacher and the students would have to tolerate my presence in the class room many times during the process. They must grant me time for interviews in their leisure time. There should be a reasonably well equipped library in the school. The group selected included 25 students (18-19 years old). They were in their last year of senior high school in the science stream.

### Description of Empirical Case

The topic of the students' assignment was "What will be the positive or negative consequences of a possible Swedish EU membership?" The study took place in the academic year of 1993/94, i.e. it was concluded six months before the Swedish referendum on EU membership. The students worked cooperatively in five groups, each group choosing a subtopic. Examination of the assignment was both oral and written. Each group had to submit a paper of about 20 pages. The students worked at the assignment for four months, starting in early December and concluding in mid-April.

The task was introduced by the teacher during a couple of lessons devoted to a short presentation of the EU, historical background and organisation. The introductory lessons also gave the framework including goals, time plan, requirements as to content, assessment and organisation of work. Subtopics were formulated through brain-storming and the class divided into five groups, each working in-depth at one subtopic under the following headings: 1) defense and security, 2) education and research, 3) environment, 4) industry and competition and, 5) labour-market. Each group was required to formulate four research questions. Once these research questions were approved by the teacher, the students launched into comprehensive information seeking.

An integral and important part of information gathering was carried out during a visit to Stockholm, where the students met with and interviewed experts or influential people representing government, industry, science, authorities, etc. Students' papers were due three weeks after the return from Stockholm.

### Data collection

My main data collection method was interviews. Besides interviews, observational sessions in the classroom were used at the early and concluding stages of the work process. Observation took place in the school library during the phase of intense information seeking. Moreover, the empirical material included students' written reports, and the teacher's detailed written assessment to every final paper.

#### Interviews

In order to follow the whole work process the students were interviewed three times: 1) at the beginning of their work, 2) in a phase of intense information seeking, and 3) after presentation and conclusion of the assignment. In order to hit these three phases of the work process it was necessary to interview all 25 students as simultaneously as possible. This led to my finding three assistant interviewers, who were trained to arrive at a common understanding of the object of and procedure for questioning.

Interviews were rather structured and contained questions on the goal of the assignment, subject content, information seeking tools, information sources, use of different materials, interventions by teacher and librarian. Questions were asked both about how students thought, felt and acted in relation to their experiences. The interviews lasted 15 to 30 minutes in the first round, and 30 to 45 minutes on the second and third occasions.

The teacher was interviewed twice, before and after the assignment, and questions to him concerned assignment goals, planning, his view on information seeking and use in this assignment, the division of responsibility between him, the students and the librarian. The librarian was interviewed three times, before and after the assignment and also during the phase of intense information seeking among the students. The questions to her concerned questions similar to those addressed to the teacher.

The collected empirical material comprised 80 interviews from 27 persons. The total interview time was about 42 hours. All interviews were tape-recorded and transcribed by me. The interviews constitute the essential empirical material and was complemented by observation notes and the above mentioned documents.

My role as a researcher in this high school class was one of interviewer and observer. Of course, I may have influenced the students in their ways of thinking about information seeking and use during their work process. There is some evidence of that in interview protocols. However, I never actively intervened in their work and my agreement with the teacher was not to interfere in the process. I did influence the teacher so that he was less inclined than usual to serve information sources to the students, because he knew about the topic of my study. In other respects, there are no indications that my presence influenced the teacher, who was apparently very familiar with having visitors in the class room.

### Analysis of empirical material

Phenomenographic analysis is undertaken without any particular frame of interpretation as for instance in psychodynamic or feminist or marxist analysis. Efforts are made to find patterns in the variations of people's conceptions of a phenomenon in order to develop a set of categories of description.

It is important to remember that in phenomenography conceptions have their origins in individual interviews, but once identified and described these categories of conceptions are linked to the phenomenon investigated, not primarily to the various individuals. "The description is a description of variation, a description on the collective level. In this sense individual voices are not heard." ([Marton](#ref2) 1996, p 187)

#### Foci of analysis

Phenomenographic analysis is a hermeneutical process. Throughout this process two main foci were maintained, one directed toward analysing the students' information seeking and use, the other one toward their understanding of subject matter. Each of these analyses implied several rounds of reading, rereading and reflexion to discern possible patterns and develop categories of conceptions of these phenomena.

The final step in the analysis of the empirical material was to compare the two sets of categories of description, i.e. students' conceptions of information seeking and use and conceptions of subject matter grounded in variation in learning outcomes. This concluding step of analysis focused on the interaction between information seeking and use and learning.

The interviews with the teacher and librarian were used to add a teaching perspective to students' conceptions of the assignment, thus it gave contextual information.

### Group patterns

Early during the empirical study I discovered that the fact that the students worked in groups strongly influenced their ways of thinking and acting. I assumed that it would also affect the learning outcomes. That is why questions about group work and cooperative learning were included in the interviews with the students. So a separate analysis of cooperative learning was undertaken. This analysis focused on four aspects: 1) How was the group set up? subject interest or group members? 2) How was group work organised? division of resonsibility? 3) Intensity and progression in the work? 4) Conceptions of cooperative learning? Previous and present experiences, apprehensions, etc?

The analysis indicated two main differences between the five groups related to

1.  approach to assignment and topic, and
2.  approach to cooperative learning and group work.

A combination of ways of experiencing group work and topic interest gave three categories:

> X - Weak topic interest and an atomistic approach to group work ("labour market")
> Y - Medium topic interest and a holistic approach to group work ("industry and competition")
> Z - Strong topic interest and a holistic approach to group work ("defense", "education", "environment")

The interaction between these categories of topic interest and group work and students' information seeking and learning is discussed in chapter 8.

## Information seeking and use

The choice of aspects for analysis and the gradual development of categories are grounded in the content of interviews and the similarities and differences between the ways in which the students talked about these various contents. The aspects chosen for categorisation are: students' conceptions of _relevance criteria_, ways of experiencing _information overload_, criteria for judging when they had _enough information_, their ways of experiencing the _cognitive authority_ of information sources as well as _bias_ in information. The next step in analysis was to aggregate the categories built on these aspects and to develop three categories of students' ways of experiencing information seeking and use.

For the distribution of students' conceptions in the various categorie see Table 6.1 (Enc.). The number of students who held conceptions according to the various categories is given below with each category of description. When the total number for each category of conception does not equal 25, the reason is that in some cases interviews did not contain enough substance related to that particular aspect.

### Categories of description

#### Relevance criteria

Three categories were found and described:

_A. Material that refers to each research question_

According to this conception relevant information was information that answered each discrete research question, and information that was physically and intellectually easily accessible. The wish for current information led the students to judge periodical articles more relevant than books. Five students held A-conceptions of relevance criteria.

_B. Answers to the research questions - directly or indirectly_

The wording "indirectly" indicates the main difference between this category and category A. Relevant information might not give a manifest answer to a specific question but might help the students answer the four research questions that the group had formulated. The aim was to cover the subtopic for the group assignment. Relevance criteria changed during the work process from a wish for overview and orientation in the early stages to more in-depth information later on. Information that would help students carry out analysis while writing their paper was much appreciated, hence information gathered from experts was considered valuable. Eleven students held B-conceptions of relevance criteria.

_C. Different perspectives on the topic_

This conception of relevance criteria emphasized the importance of finding information that would provide different perspectives on the topic. Biased information as well as more neutral information was considered useful. This conception of relevance exceeded the group's subtopic and opened up toward a wider context. Currency in information was a relevance criterion, as well as information that helped students structure their topic. Nine students held C-conceptions of relevance criteria.

#### Information overload

Two categories of conceptions were identified.

_A. Mechanical reduction of the number of information sources_

A-conceptions imply that problems caused by information overload should be solved by avoiding large quantities of information. Students thought it advisable to limit the number of libraries, information searches, and information sources used. This conception implies a technical or mechanical reduction of the quantity of information sources. Five students experienced information overload according to category A.

_B. Selection through structuring and analysing_

Students considered that the problem of information overload might be solved through careful scrutiny of information sources in order to structure the topic. Some students mentioned that redundancy in information helped them select material. Sixteen students experienced information overload according to category B.

#### Enough information

Three categories of conceptions were developed.

_A. Enough to cope with_

Students meant that they had enough material when "it looked enough". They judged that they would not have time or energy to go through more. This conception would often be expressed by the same students who experienced information overload according to category A. Four students held A-conceptions of enough.

_B. Material to cover the topic_

The students would think it appropriate to conclude their information seeking, when they had enough material to answer their research questions and thus cover their topic. They wished to find material from organisations representing both Yes and No in the referendum campaign, and they wanted neutral material as well. Ten students experienced enough according to category B.

_C. Enough to analyse and discuss the topic_

Students considered that they had enough information, when it would enable them to analyse and discuss their topic in a comprehensive, in-depth manner. Nine students held C-conceptions of criteria of enough information.

#### Cognitive authority

Two categories were identified related to how students valued or assessed the cognitive authority of information sources.

_A. Information sources assessed from surface signs_

Surface signs of sources were status, position, capacity, and use of expert terminology in texts. The students thought about the experts interviewed as more knowledgeable than others in their capacity of politicians or officials in high positions. Therefore information from them was the most reliable and important. Seventeen students held A-conceptions of cognitive authority.

_B. Information sources assessed from surface and content_

The B-conception focussed on both content and status of information sources. If the position of an expert did not agree with the content of information as anticipated by the students, this was considered particularly interesting. B-conceptions implied that information from expert sources ought to be compared with the content of other information sources. Seven students held B-conceptions of cognitive authority.

#### Bias

Among a variety of sources all the students used biased material. Some of this material was gathered particularly because it was biased. Other sources were revealed by the students as being biased. Three categories of ways of experiencing bias in information sources were developed.

_A. Bias provides faulty information_

Openly biased information was considered difficult to use or foggy because of lack of facts or clear evidence. Contradictory information was talked about as confusing. Students experienced biased information as lacking coherence. Four students held A-conceptions of bias.

_B. Bias should be balanced_

In the early phases of the process B-conceptions implied a wish to strike a balance between Yes and No campaign material. Later on, when this was found impossible B-conceptions meant accepting the answer from one side as the right answer, i.e. they accepted bias. Biased material was considered difficult to use. Thirteen students experienced bias according to category B.

_C. Bias may be used to understand opposite standpoints_

Openly biased campaign material was considered useful for understanding the arguments of either side. Students tried to scrutinize ways of argumentation, underlying values and possible inconsistencies. They made efforts to compare various sources and contradictory arguments. A lack of either EU positive or negative information urged the students to make extra efforts to seek the wanted information. The clearest difference between categories B and C is that B-conceptions focussed on choosing the right side, while C-conceptions implied efforts to understand motives and values behind different standpoints. Eight students held C-conceptions of bias.

### Aggregated categories

A comparison between the categories referring to the various aspects made it possible to discern a pattern on an overall level. Often the same students held A-conceptions of relevance criteria as well as information overload, enough information and bias. There were similar patterns of overlap between B-categories and C-categories. Three categories of conceptions of information seeking and use were developed on an aggregated level. The following table presents an overview of the students' conceptions of the five aspects of information seeking and the distribution between categories on an aggregated level.

<table><caption>Overview of distribution of students' conceptions of five aspects of information seeking and on an aggregated level</caption>

<tbody>

<tr>

<td>

_Student_

</td>

<td>

_Relevance criteria_

</td>

<td>

_Overload_

</td>

<td>

_Enough_</td>

<td>

_Cogn.authority_

</td>

<td>

_Bias_

</td>

<td>

_Aggreg.level_

</td>

</tr>

<tr>

<td>Agneta</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A</td>

</tr>

<tr>

<td>Alex</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A</td>

</tr>

<tr>

<td>Anders</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>A?</td>

<td>A</td>

</tr>

<tr>

<td>Annika</td>

<td>A</td>

<td>-</td>

<td>-</td>

<td>A</td>

<td>A</td>

<td>A</td>

</tr>

<tr>

<td>Anton</td>

<td>-</td>

<td>A</td>

<td>A</td>

<td>A</td>

<td>B?</td>

<td>A</td>

</tr>

<tr>

<td>Fanny</td>

<td>C</td>

<td>B</td>

<td>C?</td>

<td>A</td>

<td>C</td>

<td>C</td>

</tr>

<tr>

<td>Felix</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>A</td>

<td>B/C?</td>

<td>C</td>

</tr>

<tr>

<td>Filip</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>C</td>

</tr>

<tr>

<td>Fredrik</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>C</td>

</tr>

<tr>

<td>Frida</td>

<td>B/C</td>

<td>B</td>

<td>B</td>

<td>-</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Madeleine</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>C</td>

</tr>

<tr>

<td>Maja</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>C</td>

</tr>

<tr>

<td>Marianne</td>

<td>C</td>

<td>-</td>

<td>C</td>

<td>A</td>

<td>C?</td>

<td>C</td>

</tr>

<tr>

<td>Matilda</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>B</td>

<td>C</td>

<td>C</td>

</tr>

<tr>

<td>Mona</td>

<td>C</td>

<td>B</td>

<td>C?</td>

<td>B</td>

<td>C</td>

<td>C</td>

</tr>

<tr>

<td>Nadja</td>

<td>B</td>

<td>-</td>

<td>B</td>

<td>A?</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Niklas</td>

<td>B</td>

<td>A</td>

<td>-</td>

<td>A</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Nils</td>

<td>B</td>

<td>B</td>

<td>B</td>

<td>B?</td>

<td>B?</td>

<td>B</td>

</tr>

<tr>

<td>Nina</td>

<td>B</td>

<td>B</td>

<td>B</td>

<td>A</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Nora</td>

<td>B?</td>

<td>A</td>

<td>B</td>

<td>A?</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Ulla</td>

<td>B</td>

<td>?</td>

<td>B</td>

<td>A</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Ulla-Karin</td>

<td>B</td>

<td>B</td>

<td>B</td>

<td>A</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Ulrika</td>

<td>B</td>

<td>-</td>

<td>B</td>

<td>A</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Urban</td>

<td>B</td>

<td>B</td>

<td>B</td>

<td>A</td>

<td>B</td>

<td>B</td>

</tr>

<tr>

<td>Ursula</td>

<td>B</td>

<td>B</td>

<td>B</td>

<td>A</td>

<td>B</td>

<td>B</td>

</tr>

</tbody>

</table>

On the aggregated level students experienced information seeking and use as;

#####A. Fact-finding

Information seeking was experienced as fact-finding or finding the right answer to discrete questions. Easy access was an important criterion of relevance. Information was judged as enough when students did not have time or energy to use any more. The cognitive authority of sources was assessed from surface signs such as status and expertise. Biased information was not considered useful due to the lack of facts. Information seeking was considered as finding the "right" facts to answer the research questions one by one. Students with A-conceptions used a smaller number of information sources than other students.

#####B. Balancing information in order to choose right

Information seeking and use was considered as finding enough information for forming a personal standpoint on the controversial issue of Swedish EU membership. The most important relevance criterion was that information would allow students to answer their research questions and cover their subtopic. The cognitive authority of information sources was assessed by surface signs rather than content. The students thought it difficult to handle bias in information, and when they met a lack of balance between Yes and No, they chose one side. Students with B-conceptions used a larger variety of sources and libraries than those with A-conceptions.

#####C. Scrutinizing and analysing

Information seeking was experienced as seeking and using information for understanding a topic. When the topic happened to be a controversial issue, the students thought of information seeking as critically evaluating and analysing information sources. This view meant placing the topic in a wider context, thus not restricting relevance judgements to the subtopic. Scrutinizing information would imply trying to reveal and structure underlying values and motives in information sources. C-conceptions concurred with the use of the widest variety of sources among the whole group of students.

As I see it, the clearest and most important differences between the categories on an aggregated level are the differences related to relevance criteria and bias. When, as in this case, the topic was for the students to understand and critically analyse a controversial issue, differences of conceptions of relevance criteria and bias became obvious. We may infer that the character of the assignment influenced variation in students' ways of experiencing information seeking and use.

## Learning process and outcome

The analysis of students' learning focused on students' various ways of understanding, or talking about, the subject matter of the assignment during the work process. Three categories were developed based on variation in learning outcomes, as students expressed their understanding of the subject matter in the third interview, i.e. after conclusion of the assignment. Changes in students' ways of understanding the subject during the process were looked for in the analysis of their learning process.

Three categories of conceptions of subject matter were identified, based on differences as expressed in the last interview.

#####A. Consequences of EU membership cannot be assessed due to lack of facts. Fragmentary knowledge about the EU

After conclusion of the assignment the students knew discrete bits about the EU, related to the research question that they had concentrated on. These students had divided the questions between them. They said that it was impossible to know much about the pros and cons of membership, since they had found no answers. Small changes ocurred during the learning process in these students' ways of reasoning about the subject matter. Four students held A-conceptions of the subject content.

#####B. Possible consequences of membership were related to the subtopic. The EU is understood as mainly economic cooperation

Students conceptions of subject matter changed from a vague to a clear idea about the EU and from uncertainty to a personal standpoint about possible advantages or drawbacks connected to membership and related to the subtopic. Students' prior knowledge of the subject matter of their subtopic was related to the learning outcome. Some of the students with conceptions in this category had a very weak prior knowledge about their subtopic ("industry and competition"). Other students holding B-conceptions of the subject content worked with the topic of education, which caused certain difficulties, because it was less controversial than other subtopics. Twelve students held B-conceptions of the subject content after conclusion of the assignment.

#####C. EU membership is considered as a matter of ethical or political decision or commitment. The EU is seen as a power block

Understanding of subject matter changed from discrete pieces of knowledge about the EU and possible consequences of membership to critical assessment of the issue grounded in a deep understanding and evaluation of the matter. Students' analyses were grounded in but not restricted to the subtopic. These students talked about various aspects of EU membership related to a large context and they considered that political and moral values were at stake. Nine students held C-conceptions of the subject matter after conclusion of the assignment.

### Comparison with teacher's assessment of learning outcomes

The teacher gave a qualitative assessment of students' papers that was comprehensive and detailed. Strictly summarized, the teacher's assessment may be described as a ranking list:

1\. Fragmentary and insufficient knowledge, inadequate analysis.

2\. Partly sufficient knowledge, lack of critical approach and analysis.

3\. Sufficient knowledge and satisfactory efforts to analyse a difficult topic.

4\. Deep knowledge and admirable critical analysis.

There was great overlap between the placement of students according to the teacher's assessment and to the categories of students' conceptions of subject matter as expressed in the interviews.

## Interaction between information seeking and learning outcomes

A comparison between the three categories of conception of information seeking and the three categories of conception of subject content showed great overlap. Three students were placed in different categories relating to information seeking and subject content. For the rest of the students, there was overlap between conception of information seeking and of subject content.

Comparisons between various aspects of the two sets of categories showed that students' weak prior knowledge of subtopics rendered information seeking and use more difficult, something that ocurred for the students working with the topics of "labour market" and "industry and competition". The character of the subtopic of "education" caused particular difficulties for the students, because it was less controversial than the other subtopics. Overweight of information from one side, Yes or No, caused problems for the students working at the subtopics of "environment" and "industry", but they handled this problem differently. The students who studied "industry" and experienced information seeking as balancing between two opposing sides, decided to accept the opinion of one side. The students studying "environment" made extra efforts both to find more information and to analyse ways of argumentation and underlying values and motives in information sources. The interaction between information seeking and subject content clearly concern _what_ the students focused on in information sources.

Students' research questions appear as pivotal points of interaction between information seeking and use and subject content. Students searched and used information for formulating research questions. The information gathered influenced how they formulated their questions, which in turn influenced how they went on seeking and using information.

The result of the comparisons between the two sets of categories indicates that differences between students' conceptions of subject content influenced how they searched for and used information. Differences as regards students' conceptions of information seeking and use influenced both how they searched and used information and what they learnt about content.

There is great overlap between the two sets of categories of conceptions and group patterns. Few students differed from their group as to conceptions of either information seeking and use or learning outcome.

The conclusions drawn from the comparisons between the different sets of categories are that:

*   Students' conceptions of information seeking and use are not independent of the content of the information used.
*   Interaction between information seeking and use and learning primarily concern the _use_ of information. The most important aspects seem to be conceptions of relevance criteria, enough information and bias.
*   Group patterns have strongly influenced both the information seeking and use and learning processes.

## Discussion of results

The discussion is focused on questions regarding the implications of the described variations in students' ways of experiencing information seeking and use, about the discovery that students' conceptions of information seeking and use interact closely with their conceptions of the content of information, and about the methodological interest for further research in LIS use and user studies.

The most important conclusions are:

*   The results provide a deeper understanding of information seeking and use through the description of variation in three empirically grounded categories of conceptions of the phenomenon, as compared to earlier published models on a general level.

In LIS literature A-conceptions of information seeking have been described as "misconceptions". C-conceptions of information seeking seem to best resemble the models of information seeking in the literature on user education. Authors of such models often stress the important components of critical thinking and evaluation of information. These models seem to be ideals of information seeking, i.e. prescriptive. Researchers have shown less interest in describing how users, who do not follow the models, actually think or act. Many previous user studies have emphasized that information seeking is a complex process, but my results contribute to a better understanding of _what_ is complicated and _why_ this is so, as from the users' perspective. There are similarities between the three conceptions of information seeking and use and Perry's scheme of the intellectual and ethical development of college students ([Perry](#ref2) 1970).

Further use studies should investigate whether it is possible to find the same and other conceptions of information seeking and use that might be categorised and described. I believe that a research based repertoire of various conceptions of information seeking and use would contribute valuable knowledge both to LIS research and practice.

There are didactic implications of the variation of information seeking that should be explored in further research and development, e.g. regarding user education.

*   The variation in information seeking and use as presented in the three categories of description interact closely with variation in ways of experiencing or understanding the content of information, as presented in the three categories of description of learning outcomes.

The conclusion that information seeking is not independent of the content of information, as experienced by these users, contradicts the established view of information seeking as a general process regardless of content. My results support those of Pitts (1994) and may be considered as an elaboration of the role of topic in Kuhlthau's model with its emphasis on focus formulation. This opens up an important area for further investigations.

*   Phenomenographic method was useful in studying variation among twenty-five students as to how they experienced information seeking and use. The phenomenographic view that it is possible to describe variation in peoples' ways of experiencing a specific phenomenon in a limited number of categories of description proved fruitful for investigation and analysis.

The phenomenographic approach to analyse the manifest content of interviews, trying to find internal logic and consistencies in people's ways of reasoning, is different from and more straight-forward than investigating cognitive structures or mental models. Nonetheless, it provided rich and complex answers to the research problem. The effort to describe variation in stead of one general model or process of information seeking has not been widely used in LIS research, but these results indicate that this type of study may solve some acknowledged methodological problems and may enrich our understandings of the phenomenon of information seeking and use.

## References

<a id="ref1"></a>
*   Allen, B (1991): Cognitive Research in Information Science: Implications for Design. _Annual Review of Information Science and Technology (ARIST)_ 26, pp 3-37.
*   Dervin, B (1992): From the Mind's Eye of the User: The Sense-Making Qualitative-Quantitative Methodology. _Qualitative Research in Information Management,_ ed. J D Glazier and R R Powell. Englewood, Colo.: Libraries Unlimited. pp 61-84.
*   Dervin, B (1997): Given a Context by any Other Name: Methodological Tools for Taming the Unruly Beast. _Information Seeking in Context._ Edited by P Vakkari, R Savolainen & B Dervin. London: Taylor Graham. pp 13-38.
*   Dervin, B & Nilan, , M (1986): Information Needs and Uses. _Annual Review of Information Science and Technology (ARIST)_ 21, pp 3-33.
*   Ellis, D (1989): A Behavioural Approach to Information Retrieval System Design. _Journal of Documentation_ 45:3, pp 171-212.
*   Ellis, D (1992): The Physical and Cognitive Paradigms in Information Retrieval Research. _Journal of Documentation_ 48:1, March, pp 45-64.
*   Ellis, D et al. (1993): A Comparison of the Information Seeking Patterns of Researchers in the Physical and Social Sciences. _Journal of Documentation_ 49:4, pp 356-369.
*   Enmark, R (1997): Punkten som inte finns - om ett ämnesdefinierande informationsbegrepp. _Human IT_ 2, pp 6-30.
*   Entwistle, N (1976): The verb "to learn" takes the accusative. _British Journal of Educational Psychology_ 46, pp 1-3.
*   Ford, N (1987): Research and Practice in Librarianship : A Cognitive View. _The Reference Librarian_ 18, summer, pp 21-47.
*   Hewins, E T (1990): Information Need and Use Studies. _Annual Review of Information Science and Technology (ARIST)_ 25, pp 145-172.
*   Hjørland, B (1993): _Emnesrepræsentation og informationssøgning. Bidrag til en teori på kundskapbsteoretisk grundlag._ Borås och Göteborg: VALFRID.
*   Hjørland, B (1995): _Informationsvidenskabelige grundbegreber._ 2\. rev. udg. København: Danmarks Biblioteksskole. Bind I.
*   Höglund, L & Persson, O (1980a): _Datorbaserad litteratursökning. Två fallstudier._ Umeå. (Research Reports from the Department of Sociology, University of Umeå, 57)
*   Höglund, L & Persson, O (1980b): _Kommunikation inom vetenskap och teknik._ Umeå. (Research Reports from the Department of Sociology, University of Umeå, 58)
<a id="ref2"></a>
*   Ingwersen, P (1996a) Cognitive Perspectives of Information Retrieval Interaction: Elements of a Cognitive IR Theory. _Journal of Documentation_ 52:1 (March), pp 3-50.
*   Ingwersen, P (1996b): Information and Information Science in Context. _Information Science. From the Development of the Discipline to Social Interaction._ J Olaisen, E Munch-Petersen & P Wilson (eds.) Oslo: Scandinavian University Press. pp 69-111
*   Kuhlthau, C C (1989): Information Search Process: A Summary of Research and Implications for School Library Media Programs. _School Library Media Quarterly_ Fall, pp 19-25.
*   Kuhlthau, C C (1991): Inside the Search Process: Information Seeking from the User's Perspective. _Journal of the American Society for Information Science_ 42:5, pp 361-371.
*   Kuhlthau, C C (1993): _Seeking Meaning: A Process Approach to Library and Information Services_. Norwood, NJ: Ablex.
*   Malmsjö, A (1997): Information Seeking Behaviour and Development of Information Systems. A Contextual View. _Information Seeking in Context._ Edited by P Vakkari, R Savolainen & B dervin. London: Taylor Graham. pp 222-235.
*   Marton, Ference (1981): Phenomenography - Describing Conceptions of the World Around Us. _Instructional Science_ 10, pp 177-200.
*   Marton, F (1994): Phenomenography. _The International Encyclopedia of Education._ Second edition. Vol. 8. Eds. Torsten Husén & T. Neville Postlethwaite. Pergamon. pp 4424-4429. www.ped.gu.se/phgraph/civil/main/1res.appr.html (Reference established 1998-01-26)
*   Marton, F (1996): Cognosco ergo sum - Reflections on reflections. _Reflections on Phenomenography. Toward a Methodology?_ Gloria Dall'Alba & Biörn Hasselgren (Eds.) Göteborg: Acta Universitatis Gothoburgensis. (Göteborg Studies in Educational Science ; 109) pp 163-187.
*   Perry, W G (1970): _Forms of Intellectual and Ethical Development in the College Years. A Scheme._ New York: Holt, Rinehart and Winston, Inc.
*   Pitts, J M (1994): _Personal Understandings and Mental Models of Information: a Qualitative Study of Factors Associated with the Information Seeking and Use of Adolescents._ Ph.D. thesis. The Florida State University: School of Library and Information Studies.
*   Schamber, Linda et al. (1990): A Re-examination of Relevance: Toward a Dynamic, Situational Definition. _Information Processing & Mangement_ Vol. 26:6, pp 755-776.
*   Vakkari, P (1997): Information Seeking in Context. A challenging metatheory. _Information Seeking in Context._ Edited by P Vakkari, R Savolainen & B Dervin. London: Taylor Graham. pp 451-464.
*   Wilson, T D (1981): On User Studies and Information Needs. _Journal of Documentation_ 37:1, pp 3-15.
*   Wilson, T D (1994): Information Needs and Uses: Fifty Years of Progress? _Fifty Years of Information Progress: A Journal of Documentation Review,_ ed. B C Vickery. London: Aslib. pp 15-51.