# ![](../infres.gif)

## <font color="maroon">Doctoral Workshop Papers from "Information Seeking in Context" - an International Conference on Information Needs, Seeking and Use in Different Contexts, Sheffield, August 1998</font>

#### [Mapping the development of user constructs of relevance assessment as informed by topicality](http://InformationR.net/ir/4-2/isic/anderson.html), by Theresa Anderson

#### [Modelling the information seeking and use process in the workplace: employing sense-making approach](http://InformationR.net/ir/4-2/isic/cheuk.html), by Bonnie Wai-yi Cheuk

#### [The role of telecentres in the provision of community access to electronic information](http://InformationR.net/ir/4-2/isic/ellen.html), by Debbie Ellen

#### [University students' information seeking behaviour in a changing learning environment - How are students' information needs, seeking and use affected by new teaching methods?](http://InformationR.net/ir/4-2/isic/eeskola.html), by Eeva-Liisa Eskola

#### [Information seeking in the newsroom: application of the cognitive framework for analysis of the work context](http://InformationR.net/ir/4-2/isic/fabritiu.html), by Hannele Fabritius

#### [Investigating methods for understanding user requirements for information products](http://InformationR.net/ir/4-2/isic/hepworth.html), by Mark Hepworth

#### [Paranormal information seeking in everyday life -- part II: the paranormal in information action](http://InformationR.net/ir/4-2/isic/kari.html), by Jarkko Kari

#### [Work-related use of an electronic network](http://InformationR.net/ir/4-2/isic/lintila.html), by Leena Lintil�

#### [The discourses of contemporary information science research: an alternative approach](http://InformationR.net/ir/4-2/isic/olsson.html), by Michael Olsson

#### [Solving problems on the World Wide Web](http://InformationR.net/ir/4-2/isic/pharo.html), by Nils Pharo

#### [The impact of access to electronic and digital information resources on learning opportunities for young people: a grounded theory approach](http://InformationR.net/ir/4-2/isic/pickard.html), by Alison Pickard

#### [The use of the Internet by English academics](http://InformationR.net/ir/4-2/isic/shaw.html), by Wendy Shaw

#### [Information seeking in electronic environment: a comparative investigation among computer scientists in British and Greek Universities](http://InformationR.net/ir/4-2/isic/siatri.html), by Rania Siatri

#### [Information as social and intellectual capital in the research career: a gender perspective](http://InformationR.net/ir/4-2/isic/wiklund.html), by Gunilla Wiklund

#### [The use of certainty and the role of topic and comment in interpersonal information seeking interaction](http://InformationR.net/ir/4-2/isic/yoon.html), by Kyunghye Yoon

* * *

_Information Research_ is designed and maintained by Professor Tom Wilson. © Department of Information Studies, University of Sheffield, 1996, 1997, 1998