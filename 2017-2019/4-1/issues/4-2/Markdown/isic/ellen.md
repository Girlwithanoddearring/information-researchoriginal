# The role of telecentres in the provision of community access to electronic information

### Debbie Ellen  
Department of Information and Communications  
Manchester Metropolitan University  
_D.Ellen@mmu.ac.uk_

## Introduction

Telecentres are a means for providing community access to electronic information by utilising information and communication technologies (ICT). The research will evaluate the design, development and implementation of such centres from an end user perspective. The originality and significant contribution to knowledge of this research will be to propose a bridge between two disciplines; information systems design and community development. This research is designed to provide a framework specifically for community based telematics projects which will draw upon participatory techniques developed based on research and practice in these two disciplines. The framework will be informed by the results of field research of three telecentres. The field research will not only contribute to an understanding of users of telecentres but also non users in terms of the aims outlined below. The study will not aim to generalise its findings across all UK telecentres, but rather to provide a basis for future investigations.

## 2\. Aims

*   to identify and analyse the use of telecentres by local communities;
*   to identify and analyse patterns of information awareness, information capability and information handling skills amongst people who utilise telecentres and non -users of telecentres;
*   to identify and assess the impact of the strategies that have been adopted to involve end users and key actors in the design, development and implementation of telecentres;
*   to propose a framework for combining community development theory with participatory information system design theory as a possible guide to future community telematics projects.

## 3\. Context and Rationale

Access to electronic information is becoming an increasingly important issue as more and more information is provided in electronic format. The recent Green Paper ([CITU,1996](#ref)) outlines plans to increase the use of this communication medium. Within the UK telecentres are being established as well as other initiatives such as Community Networks which provide electronic community information. Telecentres provide local centres where individuals can utilise ICT for personal or business use in order to gain access to electronic information. These developments are aimed at providing access to ICT to those who do not have such facilities in their own home or workplace; to ensure that they are not excluded from accessing increasing amounts of electronic information. Teleservice centres typically have trained staff dedicated to support and training for users.

Research in Scandinavia has shown that there are important factors to be considered when setting up telecentres ([Qvortrup, 1995](#ref)). In order to address the issues surrounding access to electronic information, it is important that strategies are adopted which enable people to overcome barriers which affect use of ICT. Qvortrup proposes four barriers to access ; network (physical), service (needs), cost and qualification. The proposed research aims to identify other factors which affect access to teleservices by examining the social context of their design, implementation and development. Telecentres are seen as one method of providing community access to ICT in Europe. However, research in this area has not analysed what local communities use teleservice centres for. [Graham](#ref) (1992) put forward proposals for best practice which looked at organisational requirements for implementation of the teleservice centre model. However, little research has been identified which examines the impact of involving (or not involving) end users in the design, development and implementation of telecentres.

By bringing together the aims of identifying what local people use teleservice centres for, patterns of information skills and the impact of community involvement in telecentres it is intended that this study will result in strategies which could be used to guide future development of community telematics projects. Work has been done which looks at information systems as social systems within an organisations ([Walsham _et al._,1988](#ref)), the proposed study seeks to extend this research into a community setting.

As this study will focus on the end user perspective, the theoretical framework for the research methods being adopted here follows that suggested by [Dervin and Nilan](#ref) (1986). Their study of information needs and uses called for a paradigm shift from a system-based focus toward one which views users as active players in the design and implementation of systems geared towards meeting their information requirements. The proposed research will adopt Dervin's Sense-Making theory to study users' experiences of telecentres (i.e. information and communication system design, implementation and practice). The standard Sense-Making triangle of situation-gap-help/use will be utilised in this study to examine the aims set out above.

## 4\. Research methods

The research will be an exploratory study which will use the case study research strategy. Sense-Making as the key data collection technique is an intensive and potentially time-consuming process. Taking this factor into account, as well as the time frame of the research and available resources, it is proposed that three telecentres will be investigated. The telecentres will be selected from a target population as listed by the Telecottage Association (139 in the October 1996 ([Anon](#ref))) using ten criteria. All data collection and analysis will be piloted at a separate telecentres prior to carrying out the research on the three telecentres selected.

A literature review will include work to identify significant research in the fields of community development and user led information systems design in order to inform the resulting framework for community telematics projects.

Data collection to achieve aims will rely on multiple sources of evidence, including:

*   **Documentation:** key documents such as project proposals, progress reports and evaluations that have been undertaken will be used to provide background information. Other sources will include minutes of meetings, reports of events, articles appearing in the mass media.
*   **Archival records:** service records, organisational records.
*   **Interviews:** open ended interviews (using Sense-Making theory) will be carried out with a sample of users, key actors and people living in the catchment area of the centres identified to be investigated (up to 30 people in total at each telecentre).
*   **Data analysis** Atlas/ti software will be used to process and analyse data gathered from the open ended interviews. Content analysis as used in numerous Sense-Making studies will be used (Dervin, 1983 and Shields et al, 1993).; Content analysis will also be used for analysis of documentary and archival records. A case study database will be established in order to allow for detailed examination of all interviews and documentation.

## 5\. Time scale and key research activities

[figures in brackets indicate months, bold text indicates work completed]

<table>

<tbody>

<tr>

<td>Month</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

<td>6</td>

<td>7</td>

<td>8</td>

<td>9</td>

<td>10</td>

<td>11</td>

<td>12</td>

<td>13</td>

<td>14</td>

<td>15</td>

<td>16</td>

<td>17</td>

<td>18</td>

<td>19</td>

<td>20</td>

<td>21</td>

<td>22</td>

<td>23</td>

<td>24</td>

<td>25</td>

<td>26</td>

<td>27</td>

<td>28</td>

<td>29</td>

<td>30-36</td>

</tr>

<tr>

<td>Literature review [ongoing]</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Conduct preliminary interviews [2]</td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Identify case study sites [2]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Design open ended interview questions [2]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Pilot study and data analysis [3]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Conduct field research [6]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Undertake data analysis [7]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td> </td>

</tr>

<tr>

<td>Develop framework [3]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td> </td>

</tr>

<tr>

<td>Write up thesis [7]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

<td>■</td>

</tr>

</tbody>

</table>

## 6\. Progress to date

The start date for this PhD was March 1997 and the expected completion date is December 1999.  A pilot study has been completed, data has been processed, analysed and a draft case study report has been written. The research design utilised parts of the research instrument used for a study of everyday information needs undertaken in 1976 ([Dervin _et al._](#ref)), which used Sense-Making. The decision to use a tried and tested research instrument was taken because of the potential for comparative work spanning two decades during which there have been enormous changes in information seeking tools. Early reactions to using Sense-Making as a mechanism for data collection are positive in terms of the richness of the data produced.

Data processing is being carried out using a Computer Assisted Qualitative Data Analysis Software (CAQDAS) package called Atlas/ti. This product was chosen in preference to NUD*IST because it was felt to be a more flexible and user friendly tool. Data from the pilot study was processed using a mixture of coding techniques. For some aspects of the study content analysis schemes developed by [Dervin _et al._](#ref) (1976) were used. For example, Dervin et al. developed a scheme for topic focus for everyday information needs and situations. This was used to enable comparisons to be made between the two studies. Other data was coded as themes and patterns emerged. This process has laid the foundation for data to be collected for the main study, which is now underway.

Data collection for the first case study of an urban telecentre was carried out during July and analysis of this data has just begun (consequently few results are available as this thesis summary is being written). Data for the other two case studies is scheduled to be collected in September and November. Plans for the main study were revised, allowing more time for data processing and analysis as a result of experience gained from the pilot study. The plan is that each case will be completed, data processed and analysed before the next begins, in order to learn from each research cycle.

### Changes to research design following the pilot

The study was designed so that use (or non use) of electronic information could be seen in the context of people's everyday lives. However, it was important that people were not led down a path focusing on situations where they had used electronic information. Therefore respondents were asked to talk about everyday problems worries and concerns that had affected them in the past month or so, following the approach taken in the Seattle study ([Dervin et al. 1976](#ref)). There was some initial concern that this strategy could lead to a lack of data about electronic information. Consequently, the research instrument was modified by adding a series of questions at the end of the interview to explore people's perceptions of electronic information available via the Internet. Hypothetical situations are discussed and then respondents are asked if they would consider using any aspect of the Internet to deal with these situations, and what would get in the way of them using the Internet. Putting these questions at the end of the interview avoided the possibility of leading respondents into talking about use of the Internet.

#### Emerging themes

*   The importance of informal information and communication in dealing with everyday situations;

*   Access - the most important factor is cost, not providing the hardware. People do not feel they have access when they have to pay £3.00 per hour to use a service;

*   Patterns of usage - people either use alternative information sources or seek out access that is free (friends, college, businesses out of office hours);

*   Need for support in using the Internet - this is crucial in order to encourage novice users not providing support will effectively exclude a large number of people;

*   The Internet is becoming too slow for efficient use;

*   Online shopping - hypothetical situations show a lack of enthusiasm for this activity;

*   Older people - those interviewed have been enthusiastic to learn about the Internet.

*   Community involvement strategies - few identified from the pilot and first case study; development is driven by category of funding sources available.

#### Use of enabling technologies

At the outset of the study there was a strong commitment to utilise enabling technologies such as voice recognition software as a means of speeding up the transcription process; the use of a scanner, optical character recognition and indexing software (Zyimage) to facilitate the preparation of documents for analysis and the use of software to process data for analysis.

**Voice recognition software** Initial experience of using voice recognition software (IBM Via Voice) has not been very favourable. The plan was to listen to recorded interviews and simultaneously repeat the speech running it through the voice recognition software. This has been a qualified success. Great patience is required to "train" the software to recognise your individual speech pattern, and this process is an extremely slow one. Once the software has interpreted what has been said, the user must correct each mis-understood word in order to up date the personal speech file. Failure to do this creates a situation where errors made by the software are repeated again and again. For example if you say "I wear a red jacket" and the software interprets this as "I tear a red jacket" just over typing "tear" with "wear" will mean that every subsequent use of the word "wear" will be interpreted as "tear". To correct mis-understood words is a time-consuming process when the number of errors can be 30% of the text. Consequently, it is faster to simply type the transcript and not use the voice recognition software. Undoubtedly voice recognition software has the potential to ease the burden of transcribing interviews, but in this case it has had to be abandoned.

**Zyimage** Use of a scanner, optical character recognition and indexing software (Zyimage) has not yet been attempted, but once the technology is up and running this will be tried. What is interesting about this tool is that documents can first be scanned and then indexed by Zyimage, subsequently retrieved in raw text form, then imported into Atlas/ti for processing. That is the theory anyway. In this way key documents can be coded and analysed within the same unit as the transcribed interviews, allowing for links to be made across the two types of data using the CAQDAS software.

**Atlas/ti** The CAQDAS package has been extremely useful. It is an easy and powerful package to use. A wide range of reports can be provided with little difficulty, visual representations of data can be created and everything can be converted to HTML for ease of web publishing. Documents are introduced into Atlas/ti as simple text files and then the user begins the process of coding by selecting text, called `quotations' which are assigned codes. In addition memos can be created and quotations, codes and memos can be linked together. All three can have comments assigned to them to facilitate consistency during the coding process. Memos can be used to begin the process of theory building as you process the data. Documents, codes and memos can be organised into 'families' which is extremely useful for filtering data during coding and analysis. Users see the codes alongside the data (although one drawback is that it is not yet possible to have a report which replicates this screen view). Unlike NUD*IST, its more well known competitor, Atlas/ti is flexible in its treatment of text. Coding within a document can be done at a word, phrase, sentence or paragraph level. An active discussion list provides useful support to people getting to grips with using the software for the first time. There is no doubt that time spent learning how to use this software would be well spent.

#### Further improvements

Overall, the study is meeting its research aims and objectives. However, one area of concern relates to the issue of interviewing non users. The focus for approaching non users was the local library (because it is another place people go to seek information). Posters asking for volunteers were put up in the library a week before data collection began. No-one came forward having seen the posters. It was difficult for the researcher to approach non users in the library, and communicate the reasons for wanting to interview people NOT using the telecentre without providing too much leading information about the study. Consequently, the data collection took longer than planned. This problem will be addressed by asking telecentre staff to approach users before the beginning of the data collection period (and if possible organise appointments for interviews) leaving more time to approach non users.

Another area causing concern is how to assess peoples information awareness, information capability and information handling skills based on the data collected (research aim 2.2). Investigation of literature has not revealed any work in this area. Research into how teachers assess pupils information handling skills under the national curriculum did not provide a framework, as assessment is based around subject areas, rather than overall skills in this area. Any ideas from workshop participants on this issue would be very helpful, as it would be good not to have to reinvent the wheel if something already exists.

## <a id="ref"></a>7\. References

1.  Anon (1996) Telecottages Guide. _Teleworker_, October-November, p.20-21
2.  Central Information Technology Unit, (1996) _Government Direct : A Prospectus for the Electronic Delivery of Government Services_. (Green Paper) HMSO: London.
3.  Dervin, B et al. (1976) The development of strategies for dealing with the information needs of urban residents: Phase I - Citizen Study, Appendix D of the _Final report, Project L0035JA_. Washington, DC: Office of Education, Office of Libraries and Learning resources, US Department of Health, Education and Welfare.
4.  Dervin, B., (1983) An overview of Sense-Making research: Concepts, methods and results to date. Paper presented at the _International Communication Association Annual Meeting, Dallas Texas May 26-30th_.
5.  Dervin, B. and Nilan, M. (1986) Information Needs and Uses, <u>In</u>Williams, M.A. (Ed) _Annual Review of Information Science and Technology_ Knowledge Industry publications. pp. 3-33
6.  Graham, S., (1992) _Best Practice in developing Community Teleservice Centres,_ Centre for Applied Social Research, University of Manchester.
7.  Qvortup, L., (1995, 27 February 1997) Community Teleservice Centres. Paper presented at the _World Telecommunication Development Conference (WTDC) 1995, on the impact of Community Teleservice Centres on rural development._ [online] URL: http://www.icbl.hw.ac.uk/telep/telework/ttpfolder/tcfolder/ctc.html
8.  Shields, P., Dervin, B., Richter, C., and Soller, R. (1993) Who needs POTS-plus services? A comparison of residential user needs along the rural-urban continuum. _Telecommunications Policy_, November, pp. 563 - 587.
9.  Taylor, M., (1995) _Unleashing the Potential: Bringing residents to the centre of regeneration._York: Joseph Rowntree Foundation.
10.  Walsham,G., Symons,V. and Waema,T (1988) Information Systems as Social Systems: Implications for Developing Countries, _Information Technology for Development_, 3, 189-204.

**[Information Research](http://InformationR.net/ir/), Volume 4 No. 2 October 1998**  
_The role of telecentres in the provision of community access to electronic information_, by [Debbie Ellen](MAILTO: D.Ellen@mmu.ac.uk)  
Location: http://InformationR.net/ir/4-2/isic/ellen.html    © the author, 1998\.  
Last updated: 14th September 1998