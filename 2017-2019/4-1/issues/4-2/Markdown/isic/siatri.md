# Information seeking in electronic environment: a comparative investigation among computer scientists in British and Greek universities.

### Rania Siatri  
Department of Information & Communications  
Manchester Metropolitan University  
_o.siatri@mmu.ac.uk_

## 1\. Aim and objectives

The aim of the study is to examine the information seeking behaviour of academic computer scientists in an electronic environment

The objectives are:

1.  To investigate the different practices and methods used by academic computer scientists in retrieving information from electronic information sources.
2.  To identify the types and range of electronic information resources used currently by academics and determine the level and spread of their use.
3.  To examine the impact of these systems on communication and exchange of professional knowledge among computer scientists.
4.  To examine if it is possible to identify the factors which trigger the information need and the association of ideas, or logical consequence which leads to a particular information seeking behaviour.
5.  To investigate if the availability of information resources affects the information seeking patterns and communication of academics and to what extent.
6.  To determine whether or not different kinds of information needs ( e.g. professional, personal) lead to a different information seeking behaviour and communication channels.

It was decided to concentrate the study on computer scientists, since they are leaders in information technology, and thus can be expected to be at the forefront of exploiting IT in their communication behaviour. Although there have been many studies examining the behaviour of various academic and non-academic groups, none of them has focused on computer scientists. Narrowing the focus of the study should provide a better understanding of the information tools used by the particular group of scientists, a more detailed and accurate profile of the users leading to an in-depth understanding of the information seeking process. This study seeks to investigate how electronic information resources and information communication facilities especially those located on the Internet, have affected the computer scientists in terms of communication, exchange of knowledge and information seeking behaviour.

Another feature of the study is that, part of it will take place in Greece. The different level of information and library provision in Britain and Greece makes it appropriate to compare and contrast groups from these countries; this should enable us to draw a picture of the current situation in information use, given the different development of information and library provision in the two countries.

## 2\. Background Information

User studies are one of the areas most researched in library and information science and form a large body of literature in the discipline. However it is an area which combines some of the most used and the least precisely defined concepts of library and information science. These concepts, such as information use or need, information seeking behaviour channel of communication, exist in a system of complicated and interdependent relations. Wilson provides a framework for defining the field and "suggests that the information seeking behaviour results from recognition of some need perceived by the user" [[Wilson, 1981](#wil), p4.] A user may follow formal and/or informal channels of communication in order to acquire the desired information. The course of action that a user will engage determines and the information seeking behaviour.

The origin and conception of user studies were based upon the notion "that if one could somehow identify the information needs and uses of a population subset, one could design effective information systems"[[Crawford, 1978](#cra)]. This phrase summarises exactly the essence of user studies; studying a community and its needs helps in order to provide the information services demanded.

Throughout the historical development of user studies, methodological issues always seemed to be the centre of attention and debate among information scientists. However the main issue is not the use of defective methodology but rather the absence of a theoretical framework surrounding user studies. Methodology, in order to be effective, should be embedded within in a coherent conceptual framework which somehow will provide the boundaries of the subject area, by describing and defining the concepts within the field and by creating a systems of dependencies and interrelation among these concepts.

There are two dominant kinds of user studies, the system-oriented studies, and the user-oriented studies. In the first case users are viewed as passive recipients of information and the study investigates their external behaviour, generally by means of qualitative methods. Although these surveys have yield much quantitative data, which gives an overall picture of information needs and seeking behaviour, they fail to convey a real picture regarding the factors which trigger the information search and a more in-depth insight into the individual's conceptions and thoughts. On the other hand in user-oriented studies, users are viewed as active and self-controlling recipients of information and they are concerned with the internal cognitions of users, which are investigated by qualitative methods. [[Dervin & Nilan,1986](#der)] The researcher espouses the conceptual framework upon which user-oriented studies are based, as she acknowledges the dynamic and responsive to changes nature of human behaviour and thus of information seeking.

Some conceptual frameworks have been developed, which respond to the user-oriented model of user study. According to Sugar there are four wide categories of such frameworks. The cognitive approach, where the researchers focus their efforts to understand "how individuals process information and then illustrate this process through models" [[Sugar, 1995](#sug), p.80] In the holistic approach the researcher is not only interested in the cognitive needs of the users but also takes into account other factors which influence the users such as physiological and affective needs. Another category is the action research: this approach views users as active participants during their research and tries to understand -- the language, the activities and the social interrelationships of users. A more recent one is the theory of usability techniques, which was developed in order to respond to the increased number of studies, which deal with the use of computers and new technology in conjunction with information seeking behaviour. They focus on the use of a system but from the user's point of view. They take as a base user's needs in order to create an environment which will be friendly, effective and easy for a user to handle.

Despite the large number of user studies, information scientists are still left with an unsettling feeling of an elementary level of knowledge as far as it concerns user needs and information seeking behaviour. Taking into account the constant development in the provision of recent electronic systems, the lack of understanding the information seeking behaviour poses an obstacle in the process interpreting the way in which the electronic information services are being delivered. There is a need for alternative research methods and conceptual frameworks which will provide to the information science community, the evidence to acquire a more in-depth understanding of the users of information services.

## 3\. Methodology

To meet the main aim and objectives of the study, a combination of quantitative (questionnaires) and qualitative (interviews and logbooks) research methods along with a comprehensive literature review will be used. The first part of the review will identify previous and present research projects on the subject and will assist the researcher to gain a better understanding of the complexity and diversity of the subject. The second part of the review will deal with the possible impact of the electronic environment on academic information use. It will examine whether or not the development of new technology has affected the theoretical frameworks developed, as well as the research methods which are employed in user studies.

A questionnaire was designed and it was distributed to a number of academic computer scientists in all universities in each country. Its purpose was to collect evidence concerning the use of electronic information resource and the patterns of different practices used by academics to retrieve the required information. The analysis (SPSS is used for it) and critical evaluation of the data should draw an overall picture of the use of electronic information resources and patterns of information seeking by academic computer scientists in both countries (aims 1, 2, 3).

The means which will produce the qualitative information required will take the form of semi-structured and in-depth interviews along with some personal logbooks which will be kept for a period of time, by a small number of academics in both countries. Dervin's sense-making theory will be applied to both interviews and logbooks. The interviews will attempt to identify the factors which are liable to affect the information seeking behaviour of a researcher such as nature of the information need, the purpose of it will serve or the existence of external barriers posed by the environment such as availability of resources (aims 5, 6). The logbooks will serve as a readily available tool of documenting any thoughts, questions and information needs that an academic researcher may have during the pre-arranged period of time (aim 4). The information documented in the logbooks will hopefully provide an insight into whether or not is possible to identify the factors which trigger the information needs. The researcher is aware of the difficulties involved with research which is heavily depended on keeping journals but it is considered that is worth while. The use of questionnaires, interviews and logbooks will provide the grounds for rigorous and constant comparisons of the data. Additionally the engagement of different research methods seeks to assist towards the validity of the findings.

## 4\. Progress to date

A literature review of information seeking behaviour and user studies has been undertaken. The review assisted the researcher to identify previous and present research projects and has provided valuable knowledge for the understanding of the theoretical and methodological issues surrounding user studies.

A questionnaire has been developed tested and administered in both Greece and the UK. I have obtained preliminary results from the UK and currently I am working on the analysis of the results, from the questionnaires administered in Greece. For the analysis of the data it is used the SPSS. The interviews are almost prepared and they are scheduled to be piloted in September.

## 5\. Timetable

<table>

<tbody>

<tr>

<td> </td>

<td>

_**Activity**_

</td>

<td>

_**Target completion**_

</td>

<td>

_**Status**_

</td>

</tr>

<tr>

<td>

**1**

</td>

<td>

Literature review

</td>

<td>

----------

</td>

<td>

1st version completed but ongoing

</td>

</tr>

<tr>

<td>

**2**

</td>

<td>

Determine research methodology

</td>

<td>

----------

</td>

<td>

Completed

</td>

</tr>

<tr>

<td>

**3**

</td>

<td>

Design of Questionnaire

</td>

<td>

----------

</td>

<td>

Completed

</td>

</tr>

<tr>

<td>

**4**

</td>

<td>

Pilot questionnaire (UK & Greece)

</td>

<td>

----------

</td>

<td>

Completed

</td>

</tr>

<tr>

<td>

**5**

</td>

<td>

Data collection UK & Greece

</td>

<td>

----------

</td>

<td>

Completed

</td>

</tr>

<tr>

<td>

**6**

</td>

<td>

Data analysis & comparison of findings

</td>

<td>

Aug. 98

</td>

<td>

Currently undertaken

</td>

</tr>

<tr>

<td>

**7**

</td>

<td>

Design & pilot interview (UK & Greece)

</td>

<td>

Nov. 98

</td>

<td>

Under consideration

</td>

</tr>

<tr>

<td>

**8**

</td>

<td>

Conduct interviews and analysis

</td>

<td>

Mar. 99

</td>

<td>

----------

</td>

</tr>

<tr>

<td>

**9**

</td>

<td>

Design & pilot logbooks ( UK & Greece)

</td>

<td>

April 99

</td>

<td>

----------

</td>

</tr>

<tr>

<td>

**10**

</td>

<td>

Data collection & analysis of logbooks

</td>

<td>

July 99

</td>

<td>

----------

</td>

</tr>

<tr>

<td>

**11**

</td>

<td>

Compare findings form interviews & logbooks

</td>

<td>

Oct. 99

</td>

<td>

----------

</td>

</tr>

<tr>

<td>

**12**

</td>

<td>

Writing up of thesis

</td>

<td>

June 00

</td>

<td>

----------

</td>

</tr>

<tr>

<td>

**13**

</td>

<td>

Slippage

</td>

<td>

Sept. 00

</td>

<td>

----------

</td>

</tr>

</tbody>

</table>

## 6\. References

1.  <a id="cra"></a>Crawford, Susan. [1978] Information needs and uses. In: Williams, Martha, ed. <u>Annual Review of Information Science and Technology: volume 13</u>. Knowledge Industry Publications, p.61-81.
2.  <a id="der"></a>Dervin, Brenda and Nilan, Michael.[1986] Information needs and uses. In: Williams, Martha, ed<u>. Annual Review of Information Science and Technology: volume 21</u>. Knowledge Industry Publications, p.3-33.
3.  <a id="sug"></a>Sugar, Williams. [1995] User-centered perspectives of information retrieval research and analysis methods. In: Williams, Martha, ed. <u>Annual Review of Information Science and Technology: volume 30</u>. American Society of Information Science, p.77-109
4.  <a id="wil"></a>Wilson,T.D. [1981] On user studies and information needs. <u>Journal of Documentation</u>, 37(1), p.3-15.

**[Information Research](http://InformationR.net/ir/), Volume 4 No. 2 October 1998**  
_Information seeking in electronic environment: a comparative investigation among computer scientists in British and Greek Universities._, by [Rania Siatri](MAILTO: o.siatri@mmu.ac.uk)  
Location: http://InformationR.net/ir/4-2/isic/siatri.html    © the author, 1998\.  
Last updated: 9th September 1998