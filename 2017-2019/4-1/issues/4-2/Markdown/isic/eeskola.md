# University students' information seeking behaviour in a changing learning environment - How are students' information needs, seeking and use affected by new teaching methods?

### Eeva-Liisa Eskola  
Department of Information Studies  
Abo Akademi University  
_eeskola@abo.fi_

* * *

## Introduction and aim of the study

### Introduction

The theoretical basis of this study is partly in theories and conceptions of learning and partly in that research in information seeking, needs and uses which emphazies the user and takes the social context into consideration.

The need for changes and improvement in the quality of education has become apparent also in Finland in the higher education during this decade. The reasons have been the growth of information, a growing number of students in higher education, demands on output and productivity from the society, and the criticism against existing university policy. University policy has been critizised both in national evaluation reports of Finnish universities and in the OECD-report on evaluation of Finnish universities from 1994\. Generally the development in scientific research in learning and knowledge has influenced the need for changes also in the university pedagogy. ([Sallinen 1995, 10-12](#ref3))

The old educational system has been critizised for its conception of knowledge. In the education learners have received pieces of information which have no connection with the real life. For that kind of information and knowledge is little use in the modern society. Instead there is a need for knowledge and skills for problem-solving and critical and creative thinking ([Voutilainen et al. 1991, 19-21](#ref3)). The old static conception of knowledge has to be replaced with a dynamic conception of knowledge. According to the dynamic view knowledge is something continuously changing and growing when individuals are actively using and producing it ([Voutilainen et al. 1991, 9-10](#ref3)).

Instruction and study are always based on some conception of learning. Two main traditions in the conceptions of learning are the empirical/behavioural and the constructivist conceptions. The constructivist conception of learning has during the last decades challenged the behavioural one. The constructivist conception has its roots in the rationalistic epistemology and in "image of man" of evolutionary theory. It has been influenced by pragmatics (Peirce), functionalistic psychology (James), progressiv pedagocics (Dewey) and symbolic interactionism (Mead). Representatives of these approaches emphasized the critical role of action in the existence of human beings generally and also in learning. The constructivist conception has also been influenced by the ideas of Gestalt psychology, Bartless' theory of schemes, Piaget, Vygotsky and the cognitive approach ([von Wright 1994](#ref3)).

According to cognitive psychology human beings are basically active and goal-oriented and willing to get information about themselves and the world. Their actions are directed by intentions, expectations and response. They maintain knowledge in memory in hierarcially organized structures, schemes, and new knowledge is constructed on the basis of previously learned knowledge. This process of construction has features in common for every human being but the contents are individual. Learning occurs in connection with action and is part of the cognitive process. During the last decades the cognitive approach has emphazised aspects in the contents and the context of learning ([von Wright 1994, 16-18](#ref3)).

The constructivist conception of learning leads inevitably to emphazising flexible methods of instruction which take account of learners' characteristics. ([von Wright 1994, 19](#ref3)).

When the focus has been changed from the results of teaching to the learning process itself the goals of education will be to maximize learners own efforts in the learning process. Teachers' task in the learning process is to support learners in activating their prior knowledge and skills, and to give response. This type of teaching is called activating teaching. Essential is that learners not are passive recipients of knowledge but actively processing and producing it ([Lonka 1991, 10](#ref2)).

Activating methods of teaching which support the learning process are for example activating writing assignments, projects, tutorials, journals and cooperative learning ([Lonka and Lonka 1991, 28-45](#ref2)). Problem-based learning (PBL) which has been applied specially in medical education is a pedagogical method which is based on the modern conception of learning. In PBL the real-life situations form a starting point for problem-solving and are the basis for learning. Self-directed learning and learning in group are characteristic of PBL ([Problembaserad 1993, 11](#ref3)).

PBL as an activating pedagogical approach to learning emphasizes the students' independent information seeking. It's assumed that the students are gathering information from different sources, because predetermined lists of literature, set by teachers, are usually not used. Information sources like libraries, databases, experts, different textbooks and journal articels are mentioned as possible sources. Although lectures are not regarded as a primary mode of instruction, they are used also in PBL, and can form a source of information for students ([Nikkarinen and Hoppu 1994](#ref2); [Andrup et al. 1995](#ref); [Poikela and Öystilä 1996](#ref3)). Also members of the tutorial group function as information sources to each others (e.g. [Nikkarinen and Hoppu 1994](#ref2)). Egidus has pointed out that because of the lack of reference materials in libraries, PBL students often have to buy relevant books in the beginning of education ([Egidus 1991](#ref)).

The problem-solving process, learning in groups, integration of basic sciences into patient cases, and self-directed learning are all that kind of factors which may have an impact on information behaviour of students. In the process of problem solving, during the different steps different kinds of information from a variety of sources may be needed. When PBL students are seeking information they should be aware of which type of information they need in the problem solving and learning process, for example overview of the subject area, information about the new developments or relations to other subjects ([Andrup et al 1995](#ref)).

Because teachers' task isn't any more to deliver pieces of information but to support the learning process independent information seeking and gathering plays an important role in all activating teaching and learning methods.

**Studies in information needs and seeking** is a central part of research in information studies ([Järvelin 1987, 18](#ref2)). Studies have been conducted from the 1940s and the perspective was first the view of information systems. This research tradition has been called the system oriented paradigm. In those studies the aim has been to obtain knowledge to support organizational development and administrative decision-making. The approach has been critizised because of unsufficient theories, concepts and research methods and because it hasn't taken into consideration the needs of the information-seeking persons. In analysis of data quantitative methods usually has been used (e.g [Dervin & Nilan 1986](#ref); [Ginman 1995](#ref); [Järvelin & Vakkari 1981](#ref2); [Wilson 1994](#ref4)).

At the end of 1970's and in the beginning of 1980's researchers began to realize that questions in information needs, seeking and use couldn't been seen only from the systems point of view. The user of the information and his/her needs came into focus and research in cognitive science was applied in the studies. The new view was called the new paradigm or the cognitive view ([Dervin & Nilan 1986](#ref); [Ginman 1995, 14](#ref)).

Today the cognitive view has been critizised for not taking into consideration peoples social and cultural contexts in studies of information needs and seeking. (e.g. [Capurro 1992, 82-96](#ref); [Miksa 1992, 242-243](#ref2); [Vakkari 1994, 51](#ref3)). To obtain better knowledge about human information behaviour there is a need for a more holistic view which takes contextual aspects in to account in the research in information behaviour.

It has been pointed out that information seeking and use should be studied as a whole (e.g., [Savolainen 1994,115](#ref3); [Wilson 1981](#ref4)). Use of information has been studied in information as studies in scientific communication, for example citation analysis. There has been efforts to explore the question of use by studies in use and relevance of different information channels and sources. Savolainen mentiones Brenda Dervin (Sense-making theory), and Robert S. Taylor (Information use environments) as examples of the few researchers who have tried to conceptualize questions concerning use of information more deeply. Existing theoretical and methodological problems have influenced that use of information hasn't been successfully studied and it has been suggested that also results of research in other disciplines (e.g. artificial intelligence, cognitive research in learning, memory and thinking) should be taken into consideration when information scientist try to conceptualize processes in information use. ([Savolainen 1994, 111-117](#ref3))

### Aim of the study

The aim of this two-phased study is to investigate what kind of activating teaching methods are in use in the Finnish universities, and how students' information behaviour is affected by these methods. A model for information behaviour of university students will be the result of the study. The model will be developed on the basis of existing theories of information behaviour, learning, and empirical studies.

The two phases are:

> I. How are students information needs, seeking and use affected by the problem-based learning (PBL) approach? - A comparative study of information behaviour between medical students studying according the problem-based curriculum and the traditional curriculum.
> 
> II. University students' information behaviour in a changing learning environment - The effect of new teaching methods on information behaviour.

In the first phase the main problem is to study how problem-based learning affects information needs, seeking and use, i.e. information behaviour, of university students.

Questions of concern are:

1.  What kind of information is needed for learning purposes ?
2.  What kind of sources and channels are used, how and why are they used?
3.  Which factors, connected to the teaching methods (PBL vs. traditional) and learning environment, affect information seeking and use and in what way?
4.  How is information used in the process of learning?

In the second phase the questions of concern are:

1.  Which other kind of student-centred activating teaching methods than PBL are in use in the Finnish universities?
2.  How much do these methods imply students independent information seeking ?
3.  Do students get any instruction in information skills?
4.  What kind of information is needed for studies?
5.  How is this information gathered? What kind of sources and channels are used, how and why are they used?
6.  Which factors, connected to the teaching methods and learning environment affect information seeking and use and in what way?
7.  How is information used for the purpose of learning?

All these questions together will give an answer to the main question of the second phase: How do activating, self-directed methods of teaching and learning influence university students' information behaviour?

## Basic concepts

### A. Information

In information science the concept of information is defined in many different ways. In the following are definitions which have through the cognitive approach relations with the modern conception of learning. In the cognitive viewpoint of information science [Belkin](#ref) (1977 in 1978, 81) defines: "the information associated with a text is the generator's modified (by purpose, intent, knowledge of recipient's state of knowledge) conceptual structure which underlines the surface structure (e.g. language) of that text". Definition is subsequantely elaborated by [Ingwersen](#ref2) (1995, 165; 1996, 98) as information being "the result of a transformation of the generator's cognitive structures (by intentionality, model of the recipients' state of knowledge, and in the form of signs), and "on the other hand information is something-a structure- which, when perceived, may affect and transform the recipient's state of knowledge". Information is seen as "something constructed by human beings" ([Dervin & Nilan 1986, 16](#ref)).

In this study information is conceptualized generally as something which students need during their studies when they construct meaning about the subjects in the process of learning. After the empirical studies further specification can be made of the concept.

### B. Information behaviour

Information behaviour can be defined by the general model of information behaviour developed by Wilson. ([Wilson 1997a, 39](#ref4)).

According to Wilson a general model of information behaviour needs to include at least the following three elements:

*   "an information need and its drivers, i.e., the factors that give rice to an individual's perception of need;
*   the factors that affect the individual's response to the perception of need; and
*   the processes or actions involved in that response."

Taylor defines information behaviour as the product of certain elements of the information use environment. The elements are:

*   "The assumptions, formally learned or not, made by a defined set of people concerning the nature of their work.
*   The kinds and structure of the problems deemed important and typical by this set of people.
*   The constraints and oppoturnities of typical environments within which any group or subgroup of this set of people operates and works.
*   The conscious, and perhaps unconscious, assumptions made as to what constitutes a solution, or, better said, a resolution of problems, and what makes information useful and valuable in their contexts."

Based on the definition he believes that the information behaviour of different groups of people, also is different. ([Taylor 1991, 221-22](#ref3))

### C. Information need

Information need is often understood in information science as evolving from a vague awareness of something missing and as culminating in locating information that contributes to understanding and meaning ([Kuhlthau 1993](#ref2)). Information need is described as an anomalous state of knowledge (ASK) ([Belkin et al. 1982](#ref)) or a gap in individual's knowledge in sense-making situations ([Dervin & Nilan 1986](#ref)). Wilson points out that there must be an attendant motive when a person experiences an information need ([Wilson 1997b](#ref4)).

### D. Learning

[Lehtinen](#ref2)(1997) summarizes the latest results of learning research and defines learning as constructivist, cumulative, structural, self-directed, strategic, goal-oriented, situated, abstract, co-operative, and individually different processing of knowledge. ([Lehtinen 1997, 14-20](#ref2))

### E. New teaching methods

By new teaching methods are referred to student-centred methods which emphasize learners' own activity in learning process. Essential in these methods are the active role of learners as processors and producers of information and knowledge, self-directed learning and independent information seeking (e.g. [Lonka 1991](#ref2)).

### F. Learning environment

Learning occurs everywhere and on all levels of education the role of information technology and information retrieval systems has been intensifyed. The conception of learning environment has expanded from the place of the study, for instance, the school to include also those sources for information which can be used and followed by different media, and those events which students can take part in outside the school virtually or directly ([Lehtinen 1997](#ref2)).

### Relations between the concepts

The basic assumption of this study is that activating, self-directed methods of learning, for example PBL, lead students to a diversified and active use of various sources and channels of information. This assumption is based on the modern conception of learning according to which learners are activly constructing knowledge and skills, the cognitive psychology's view of human beings as active and goal-directed, who seek for feedback and information about themselves and the world. The assumption is also based on the experiences reported by studies concerning student-centered teaching methods and library use as part of information behaviour.

## Previous research on students' information behaviour

Many of the studies in information science concerning aspects in university students' information behaviour has described students' use of library services and problems in using those services. Wilson, dividing the research field of information needs and uses in system studies and user studies, subsumes the studies conducted until the beginning of the seventies under the general heading of library surveys in his review of research in information needs and uses. Also attitudes of students towards libraries and their staff and the extent to which students buy books has been explored in the surveys. These studies report that students seldom buy books, they have difficulties in using libraries and they often use the neighbouring public library and that seminars on library use have little effect. ([Wilson 1994](#ref4))

During the last decades the interest in students' library use and information behaviour has increased mainly because of the increase in student numbers and libraries needs to meet their clients demands in the best possible way. Studies of students' attitudes to and use of university libraries has been conducted in several countries. Findings from earlier studies concerning students problems in trying to use the library are validated in the studies performed in 80's and 90's. ([Höglund & Thorstéinsdóttir 1996](#ref))

Studies of undergraduate students relations to libraries has been conducted also in the Nordic countries in Danmark 1995, Sweden 1995 and Finland 1996\. The results show that students are rather frequent library users ([Höglund et al. 1995](#ref); [Pors 1995](#ref3); [von Ungern-Sternberg 1996](#ref3)) although there are large differencies in how students of different disciplines utilize library services ([Höglund et al. 1995](#ref)). The students want more study carrels, more course litterature and more generous opening hours ([Pors 1995](#ref3)). The user education programs of libraries are not frequently used by the students ([Pors 1995](#ref3)). According to the Finnish and Swedish studies students themselves ask for more education in information seeking and library use ([Höglund et al. 1995](#ref); [von Ungern-Sternberg 1996](#ref3)). The students also expect a traditional service from the library and look at the library as a depot for literature and do not identify the library as an information center or as a place for studies ([von Ungern-Sternberg 1996](#ref3)). The results indicate also that systematic cooperation is needed between teachers and librarians, both regarding education in information seeking and in support of information service to students ([von Ungern-Sternberg 1996](#ref3)).

In these studies the perspective is the users and knowledge has been obtained about the role of libraries in the students information needs and seeking.

More generally students' information behaviour has been studied by Carol C. Kuhlthau. She has made empirical research about students' information seeking behaviour in libraries and developed a general model of the information seeeking process (ISP). ISP consists of 6 stages: initiation, selection, explication, formulation, collection, and prestation. Students experience the ISP holistically with an interplay of taughts, feelings, and actions ([Kuhlthau 1993](#ref2)). Limbergs study concerns the interaction between information use and learning outcome, when high school students work at an assignment implying independent use of information. According to the study, which has been done by using the phenomenographic method, variation in information seeking and use interact closely with variation in ways of experiencing and understanding the content of information ([Limberg 1998](#ref2)).

Questions concerning the relation between teaching methods and students' information behaviour has not been investigated thorougly - although PBL- students' information seeking behaviour and libray use have been compared in a few studies, which are described later in connection with medical students' information behaviour.

In Finland, in addition to the study of students' library use mentioned above ([von Ungern-Sternberg 1996](#ref3)), a few master and licentiate theses have been done in the area of students information needs and uses at the departments of information studies in Tampere and Oulu. For example Rissanen has studied health care students information needs, uses and information literacy in a case study. Teachers, staff, and the collections on the practice department, the libray of the college and the public library, textbooks and journals were the most important information channels for the studied students. Their informations gathering was restricted by lack of time and money, language problems and poor skills in information seeking. ([Rissanen 1994](#ref3)). Kautto has explored instruction given to students in seeking and using literature as a part of university education. The findings showed that teachers do not usually teach the central literature in a domain and information skills of students are regarded as satisfactory by teachers. The subject field strongly influences on teachers' thinking and actions connected to instruction given in seeking and using literature. ([Kautto 1997](#ref2)).

### Medical students' information behaviour

Medicine is among many other sciences an area in which the expansion of information is enormous and which is critically dependent on up-to-date information. These factors have influenced the implementation of problem-based learning (PBL) approach in the medical education. Students can't learn during their formal education all what is needed and therefore skills in problem-solving and independent information seeking are emphasized in the PBL-curriculum.

Although interest in information behaviour in the context of health care has increased, the main part of it concerns physicians ([Hewins 1990](#ref)). Medical students' information behaviour has been explored in connection of library user or use studies. In the beginning of 1990's Rankin wrote that few libray user or use studies have centered on medical students. According to a study conducted in the beginning of 1980's medical students use most often textbooks and general or overview material to meet their information needs ([Rankin 1992](#ref3)). Results of Mick's study of information behaviour of medical students indicate that the students were dependent on their personal notes and colleagues (Mick 1972 in [Taylor 1991](#ref3)).

Due to changes in the medical education interest in medical students' information behavior has grown during the last decade.

PBL students' use of libraries and different information sources has been explored in a few studies. Results of these studies indicate that the way how the library and its services are used changes, and the use of the library, and library services increases when the PBL -method is applied in the curriculum. PBL-students use a greater variety of sources more frequently than students taught with traditional methods. PBL- students choose sources which support learning process and they learn how to seek information on an early stage of education ([Saunders et al. 1985](#ref3) in Andrup 1995; [Rankin 1992](#ref3); [Fridén & Oker-Blom 1995](#ref)). According to Marshall et al. students in the problem-based curriculum use Medline searching more often than the students in conventional curriculum ([Marshall et al. 1993.](#ref2))

The implementation of student-centred learning and teaching methods in the medical curriculum has also resulted in a growing body of literature which explores different aspects of information literacy and user education of libraries. One of the main concerns has been questions of the integration of skills in information literacy or information management instruction in the curriculum (e.g. [Minchow 1996](#ref2); [Schilling 1995](#ref3)).

Wildemuth et al. has conducted several studies on medical students' information and search behaviours (e.g. [Wildemuth et al., 1991, 1992](#ref4)). They have for example explored medical students' information seeking behaviour by studying the questions the students have asked of librarians and physicians. When first-year medical students sought information about clinical scenarios in toxicology, they asked questions of a physician concerning the explanation of symptoms of a case, what tests or methods could be used to distinguish between toxins, and how to treat the patient. Librarians were asked how to find the bibliographic references relevant to clinical cases ([Wildemuth et al. 1994](#ref4)). In another study medical students' personal knowledge, searching proficiency, and database use in problem solving has been investigated. Medical students solved biomedical problems on four occasions over a two year period. The results of the study indicated that there was little evidence of any relationship between personal domain knowledge and searching proficiency (i.e. search results, selection of search terms, improvement in selection of search terms over the course of the search, and efficiency). Search results, selection of search terms, and efficiency were found to be related to database assisted problem solving performance ([Wildemuth et.al. 1995](#ref4)).

Different subjects and settings produce different definitions of information needs, and information systems designed for one setting can be unuseful in an other context. Questions and information needs present in educational settings, for example in medical education, differ traditionally from those of acting physicians ([Gorman 1995](#ref)). On the other hand changes are made in medical education in order to enhance problem-solving skills of students and to help them learn to think like experts when solving clinical problems. It may be possible that these changes also influence students' information behaviour and it may become more similiar to the behaviour of practicing physicians.

Questions concerning the relations between teaching methods and students' information behaviour has not been investigated thorougly - although PBL-students' information seeking behaviour and library use have been compared in a few studies. Results of the international studies can't directly be adopted to the Finnish society because the educational systems and practices alter by countries. However, when an essential factor in the learning environment, the teaching methods, is changing it can be assumed that new, important knowledge can be obtained from this area.

## Data material and method

The study consists of a theoretical and an empirical part. The empirical part is divided into two phases. The research methods in the first phase are qualitative. In the second phase both qualitative and quantitative methods are applied.

### Qualitative methods

In the social sciences it is common to distinquish between two different methodological approaches; quantitative and qualitative methods. Within the quantitative approach statistical methods for gathering and analysing data are generally used and the qualitative approach uses methods which are not based on quantitative measuring. Depending on theories, hypothesis and research questions methods from both approaches can be used in the same research project ([Silverman 1995, 2](#ref3)). Qualitative methods are applied to research on complex fenomena of the society for example social processes, structures, and relations when the purpose is to gather information which can't be expressed in numbers and quantities ([Holme & Sovang 1991, 85-7](#ref)). By using qualitative methods a researcher is not trying to generalize results but to obtain more understanding about the problems studied ([Holme & Sovang 1991, 95](#ref)).

There are four main data collecting methods which are used by qualitative researchers: observation, analysis of texts and documents, interviews, records and transcripts. These methods are often combined with each other ([Silverman 1995, 8-9](#ref3)). Use of several methods (triangulation) is one way to ensure validity in qualitative research ([Fidel 1993, 232](#ref)).

Researchers in information science have paid attention to qualitative analysis methods because of three different factors: unsuccess with quantitative methods in research; change to a user-oriented research tradition in information science; the growing interest in qualitative methods in other areas of the social sciences. Information scientists have pointed out that individual, situational, historical, and contextual factors which are essential in information behaviour have not been taken into account in research which has applied only quantitative methods ([Fidel 1993, 233](#ref)).

### Research material and data collecting methods

The material of the study consists of data which shall be collected and analysed by qualitative and quantitative methods.

In the first phase the information behaviour of students in medical education will be studied. The study objects are students in the university of Tampere medical faculty in the problem-based learning education and students in the medical faculty of Turku university studying according to the traditional curriculum. Because of the qualitative methods for collecting and analysing data the number of subjects in both groups will be 15 - 20.

The methods for collecting data are interviews (open-ended questions), students' diaries and observation .

The second phase of the study will be based on the findings of the first phase. The developed model for information behaviour will be tested empirically in the context of other activating teaching methods in addition to PBL. First, information about those methods and the use of them in the Finnish universities is to be collected by questionnaries to university teachers. The study objects will be randomized among the teachers. Because of the large sample data will be analysed by quantitative methods. Secondly, those students which have been taught by activating teaching methods will be interviewed about their information behaviour. The interviews will be more structured than in the first phase because of the larger number of informants and in the analysis both quantitative and qualitative methods will be utilized.

## Proposed work plan and the progress to date

Work plan for the study is:

Collection of data for the first phase 1997-1998, analysis and conclusions of the first phase 1998 - spring 1999, collection of data for the second phase autumn 1999-2000, analysis and conclusions of the second phase 2001.

The work was started by getting acquainted with the theoretical backgrounds and preparations for the empirical research in 1997\. During the spring 1998 data for the first phase was gathered by open-ended thematic interviews and students' diaries about their learning process. The students have been interviewed about the following themes: studies, learning process, information needs, information seeking and use. In connection to the different themes various factors influencing the learning process and information behaviour are explored. In the diaries students have described their learning situations and thaughts, possible difficulties and feelings concerning studying and learning. Observation of different learning situations has been used in order to obtain more knowledge about those situations and their role as information source in the students' learning process. Transcription of the recorded interviews has begun during the summer 1998.

## <a id="ref"></a>References

1.  Andrup, S., Fagerjord, K. & Magnussen, M.T. (1995). Fagbibliotekets rolle i problembasert læring (PBL). Oslo: RBT/Riksbibliotektjenesten. (Skrifter fra RBT/Riksbibliotektjenesten, Nr. 69).
2.  Belkin, N. (1978). Information concept for information science. Journal of documentation 34(1): 55-58.
3.  Belkin, N.J., Brooks, H.M., Oddy, R.N. (1982) ASK for information retrieval. Journal of Documentation 38:61-71.
4.  Capurro, R. (1992). What is information science for? A philosophical reflection. In: Conseptions of library and information science. Historical, empirical and theoretical perspectives. Ed. P. Vakkari & B. Cronin. London: Taylor Graham, 82-96.
5.  Dervin , B. & Nilan, M. (1986). Information needs and uses. In: Annual Review of Information Science and Technology, vol. 21\. Ed. M.E.Williams. New York: Knowledge Industry Publications, 3-33.
6.  Egidus, H. (1991). Problembaserad inlärning - en introduktion. Lund: Studenlitteratur.
7.  Fidel, R. (1993). Qualitative methods in information retrieval research. Library & Information Science Research 15:219-247.
8.  Fridén, K. & Oker-Blom, T. (1995) Påverkar problembaserad inlärning studenternas informationsvanor och biblioteksanvändning?. Stockholm: Karolinska institutes bibliotek och informationscentral. (Linköpings universitetsbibliotek, Publikation nr 52).
9.  Ginman, Mariam (1995). Paradigm och trender inom biblioteks- och informationsvetenskap. In: Biblioteken, kulturen och den sociala intelligensen. Aktuell forskning inom biblioteks- och informationsvetenskap. Ed. Lars Höglund. Stockholm : Forskningsrådsnämnden, FNM, 9-18.
10.  Gorman, P. (1995). Information needs of physicians. Journal of the American society for information science 46 (10): 729-736.
11.  Hewins, E.T. (1990). Information needs and uses. In: M. E. Wilson (ed.) Annual Review of Information Science and Technology. American Society of Information Science. Vol. 25: 145-168.
12.  Holme, I.M. & Sovang, B.K. (1991). Forskningsmetodik. Om kvalitativa och kvantitativa metoder. Lund: Studentlitteratur.
13.  Höglund, L. et al. (1995). Tusen studenter om biblioteket - vanor, attityder och krav. Stockholm: BIBSAM. (Delstudie 2)
14.  Höglund, L., Thorstéinsdóttir, G. (1996). Students and the university library. Attitudes to and use of university libraries among students and faculty. Svensk biblioteksforskning 4: 29-42.
15.  <a id="ref2"></a>Ingwersen, P. (1995). Information and Information Science. In: Allen Kent (ed.). Encyclopedia of Library and Information Science. 56\. Suppl. 19\. New York: Marcel Dekker. 137-174.
16.  Ingwersen, P. (1996). Information and information science in context. In: J. Olaisen, E. Munch-Petersen, P. Wilson (eds.) Information Science. From the development of the discipline to social interaction. Oslo: Scandinavian University Press. 69-111.
17.  Järvelin, K. (1987). Kaksi yksinkertaista jäsennystä tiedon hankinnan tutkimista varten. Kirjastotiede ja informatiikka 6(1): 18-24.
18.  Järvelin, K. & Vakkari, P. (1981). Tiedontarpeiden ja kirjaston käytön tutkimisesta. Kaksi tutkielmaa. Tamperen yliopisto. Kirjastotieteen ja informatiikan laitos. Julkaisuja A Tutkimusraportit nro 15.
19.  Kautto, V. (1997). Kirjallisuuden haun ja kirjaston käytön opetus korkeakouluopetuksen osana. Informaatiotutkimuksen lisensiaatintutkimus. Oulu: Oulun yliopisto. Unpublished.
20.  Kuhlthau, C. (1993). Seeking meaning. A process approach to library and information services. Norwood, N.J. : Ablex.
21.  Limberg, L. (1998). Att söka information för att lära. En studie av samspel mellan informationssökning och lärande. Doctoral Thesis. Borås: Valfrid. Skrifter från Valfrid, nr 16.
22.  Lehtinen, E. (1997). Tietoyhteiskunnan haasteet ja mahdollisuudet oppimiselle. In E. Lehtinen (ed.) Verkkopedagogiikka. Helsinki: Edita, 12-40.
23.  Lonka, Kirsti (1991). Oppimiskäsitys muutuu – entä koulutus? In: Aktivoiva opetus. Käsikirja aikuisten ja nuorten opettajille. Toim. Kirsti Lonka ja Irma Lonka . s. 7-11\. Helsinki: Kirjayhtymä.
24.  Lonka, Kirsti ja Lonka Irma (1991). Aktivoivia ja prosessipainoitteisia työtapoja. Aakkosellinen hakemisto. In: Aktivoiva opetus. Käsikirja aikuisten ja nuorten opettajille. Toim. Kirsti Lonka ja Irma Lonka . s. 28-45\. Helsinki: Kirjayhtymä.
25.  Marshall, J. & Fitzgerald, D. & Busy, L. & Heaton, G. (1993). A study of library use in problem-based and traditional medical curricula. Bulletin of the Medical Library Association. 81 (july 1993): 299-305.
26.  Miksa, F.L. (1992). Library and information science. Two paradigms. In: Conseptions of library and information science. Historical, empirical and theoretical perspectives. Ed. P. Vakkari & B. Cronin. London: Taylor Graham, 229-252
27.  Minchow, R.L. (1996). Changes in information-seeking patterns of medical students: second-year students' perceptions of information management instruction as a component of a problem-based learning curriculum. Medical reference services quarterly, vol. 15 (1): 16-31.
28.  Nikkarinen, T. & Hoppu, K. (1994). Ongelmakeskeinen opetus, ongelmalähtöinen oppiminen ja aktivoivat opetusmenetelmät. Duodecim 110:1548-1555.
29.  <a id="ref3"></a>Poikela, S. & Öystilä, S. (1996). Ongelmaperustainen oppiminen korkeakoulutuksessa. Esimerkkinä Tampereeen yliopiston lääkärikoulutus. Korkeakoulutieto 2:81-86.
30.  Pors, N.O. (1995). Studerende og biblioteker. En undersøgelse af studerendes bibliotesbenyttelse. Statens Bibliotekstjeneste: København. (Skrifter fra Statens Bibliotekstjeneste, 9).
31.  Problembaserad inlärning. Erfarenheter från Hälsouniversitet (1993). Red. Karin Kjellgren, Johan Ahlner, Lars Owe Dahlgren, Lena Haglund. Lund: Studentlitteratur.
32.  Rankin, I. A. (1992). Problem-based medical education - effect on library use. Bulletin of the Medical Library Association 80(1):36-43.
33.  Rissanen, K. (1994). Opiskelijat terveydenhuoltoalan tietoa etsimässä. Case-tutkimus Tamperen terveydenhuolto-oppilaitoksessa. Tampere. Ammattikasvatushallinon koulutuskeskus. (Ammatti-kasvatushallinnon koulutuskeskuksen julkaisuja 1/1994).
34.  Sallinen, Aino (1995). Opetus ja yliopiston tuloksellisuus. In: Yliopisto-opetus. Korkeakoulupedagogiikan haasteita. Toim: Juhani Aaltola, Markku Suortamo, s. 10-24\. Porvoo: WSOY.
35.  Schilling, K., Ginn, D.S., Mickelson, P., Roth, L.H. (1995). Integration of information seeking skills and activities into a problem-based curriculum. Bulletin of the Medical Library Association 83(2):176-183.
36.  Savolainen, R. (1994). Tiedon käytön tutkimus informaatiotutkimuksessa. Kirjastotiede ja informatiikka 13(4): 101-119.
37.  Silverman, D. (1995). Interpreting qualitative data. Methods for analysing talk, text and interaction. Reprint. London: Sage.
38.  Taylor, R. S. (1991). Information Use Environments. In: Brenda Derwin & Melvin J. Voigt (eds). Progress in Communication Sciences. Norwood, NJ: Ablex. Vol. 10: 217-255.
39.  Vakkari, P. (1994). Library and information science. Its content and scope. In: Advances in Librarianship. Vol. 18\. New York: Academic press, 1-55.
40.  von Ungern-Sternberg, S. (1996). Studenterna och biblioteket. En fokusgrupp-undersökning av Åbostudenternas informationsanskaffning. Åbo: Åbo Akademi, Instituitonen för biblioteks- vetenskap och informatik. (Finnish Information Studies, 2).
41.  von Wright, J. (1994). Oppimiskäsitysten historiaa ja pedagogisia seurauksia. 2\. muuttamaton p. Helsinki: Opetushallitus
42.  Voutilainen T., Mehtäläinen J., Niiniluoto, I. (1991). Tiedonkäsitys. 5p. Helsinki: Kouluhallitus.
43.  <a id="ref4"></a>Wildemuth, B. & Jacob, E. & Fullington, A. & de Bliek, R. & Friedman, C. (1991). A detailed analysis of end-user search behaviours. In: Jose-Maria Griffiths (ed.) ASIS '91\. Systems understanding people. Proceedings of the 54th annual meeting of the American society for information science. Vol. 28: 302-312.
44.  Wildemuth, B. & Friedman, C. & He, S. & de Bliek, R. (1992). Search moves made by novice end users. In: Debra Shaw (ed.) Proceedings of the 55th annual meeting of the American society for information science, Pittsburgh, 26-29 Oct 1992\. pp. 154-161.
45.  Wildemuth, B. M & de Bliek, R. & Friedman, C.P. & Miya, T (1994). Information-seeking behaviors of medical students: a classification of questions of librarians and physicians. Bulletin of the Medical Library Association 82(3): 295-304.
46.  Wildemuth, B. & File, D. & Friedman, C. & de Bliek, R. (1995). Medical students' personal knowledge, searching proficiency, and database use in problem solving. Journal of the American society for Information Science 46 (8): 590-607.
47.  Wilson, T.D. (1981). On user studies and information needs. Journal of Documentation 37(1): 3-15.
48.  Wilson, T.D. (1994). Information needs and uses: Fifty years of process? In: B. Vickery (ed.) Fifty years of information progress. A Journal of Documentation review. London: Aslib, 15-51.
49.  Wilson, T.D. (1997a). Information behaviour: An inter-diciplinary persepective. In: P. Vakkari, R. Savolainen & B. Dervin (eds.) Information seeking in context. Proceedings of an international conference on research in information needs, seeking and use in different contexts 14-16 August, 1996, Tampere, Finland. London: Taylor Graham, 39-50.
50.  Wilson, T.D. (1997b). Information behaviour: An inter-diciplinary persepective. Information Processing & Management 33(4):551-572.

    **Information Research, Volume 4 No. 2 October 1998**  
    _University students' information seeking behaviour in a changing learning environment_, by [](MAILTO: eeskola@abo.fi)Eeva-Liisa Eskola  
    Location: http://www.shef.ac.uk/~is/publications/infres/isic/eeskola.html    © the author, 1998\.  
    Last updated: 9th September 1998