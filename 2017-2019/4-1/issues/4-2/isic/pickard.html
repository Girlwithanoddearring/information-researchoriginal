<!DOCTYPE html>
<html lang="en">

<head>
	<title>The impact of access to electronic and digital information resources on learning opportunities for young
		people: A grounded theory approach</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords"
		content="electronic information resources, learning and teaching, young people, grounded theory">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>The impact of access to electronic and digital information resources on learning opportunities for young people:
		a grounded theory approach</h1>
	<h3>Alison Jane Pickard</h3>
	<p>Department of Information and Library Management<br>
		University of Northumbria at Newcastle, UK<br>
		<em>alison.pickard@unn.ac.uk</em></p>
	<h2>Introduction.</h2>
	<p>The primary concern of this study is the impact of access to electronic information resources on learning
		opportunities for young people. &quot;Learning with hypermedia is essentially an information usage
		activity,&quot; (<a href="#ref4">Yang, 1997</a>: 71) which places this research firmly within the field of LIS
		information use studies. The focus of this study will be the individual youth and his/her entire electronic
		information environment, both school-related and personal. (<a href="#ref2">Latrobe &amp; Havener, 1997</a>).
		Initial inquiries carried out in the early stages of the research confirmed that, not only is access to
		electronic and digital information sources fragmented throughout separate schools, public libraries, and homes,
		there is also a lack of consistency of access within individual institutions (<a href="#ref2">Great Britain...,
			1998</a>; <a href="#ref1">Cole, 1996</a>; DfEE, 1997; <a href="#ref2">Library Association, 1998</a>; <a
			href="#ref3">NiAA, 1997</a>). These reports concentrate on statistical averages relating to physical
		provision, they do not concentrate on the level of individual access, both physical and intellectual.</p>
	<h2>Aims and objectives.</h2>
	<h3>Aims</h3>
	<ul>
		<li>To answer the question; does access to electronic and digital information resources have a role in breaking
			down barriers to learning encountered by young people?</li>
		<li>If so, how, why and under what circumstances, to provide a clear understanding of the use of these
			resources.</li>
		<li>To suggest a guide to good practice in managing learning resources.</li>
	</ul>
	<p>A set of 8 specific objectives has been established arising from these aims:</p>
	<ol>
		<li>Identify and evaluate contemporary use of electronic and digital information by young people.</li>
		<li>Examine Central Government, LEA, and individual school policy documents to establish the current context.
		</li>
		<li>Observe and examine the level of peer and tutor interaction, and/or the level of solitary competitiveness
			during electronic and digital information use.</li>
		<li>Establish what participants assume they are doing in relation to the process witnessed by the researcher.
		</li>
		<li>Consider motivation to use electronic and digital information and the effect this has on the participants
			learning environment.</li>
		<li>Establish the level and quality of information skills training available to young people, and examine its
			application during the information seeking process.</li>
		<li>Examine the conditions which influence use.</li>
		<li>Establish the conditions under which access to electronic and digital information does impact positively on
			learning.</li>
	</ol>
	<h2>Background.</h2>
	<p>The purpose of this research is to generate a grounded theory, not to test a theory that has been determined
		<em>a priori</em>. However, there is a need to underpin the research with a sound framework; &quot;Theory
		grounded in an earlier investigation may be available&quot; (<a href="#ref2">Lincoln &amp; Guba, 1985</a>:.209)
		and can be used to develop a strong theoretical base; a collection of signposts which will alert the researcher
		to established concepts, without excluding the emergence and development of unforeseen issues.</p>
	<p>Following the advice of Wilson (<a href="#ref4">1994</a>) and Westbrook (<a href="#ref4">1993</a>), the
		theoretical base for this research was drawn from multi-disciplinary exploration. The theoretical framework was
		developed by integrating research from educational psychology, learning theory, cognitive science and
		information science.</p>
	<p>Work by Kuhlthau (<a href="#ref2">1989 &amp; 1991</a>), and Pitts (<a href="#ref3">1993</a>), has provided
		important insight into the research process of young people. Other researchers have studied the
		information-seeking behaviour of students within specific electronic environments, excluding, by design, all
		other information resources. (<a href="#ref1">Borgman, 1995</a>: <a href="#ref2">Large, 1995</a>: <a
			href="#ref2">Marchionini, 1995</a>: <a href="#ref3">Neuman, 1995</a>; &amp; <a href="#ref4">Yuan, 1997</a>).
		Other studies have concentrated on various aspects of the individual students social and personal environments;
		Burdick (<a href="#ref1">1995</a>), Ford &amp; Miller (<a href="#ref1">1994</a>), Jacobson (<a
			href="#ref2">1994</a>), and Poston-Anderson &amp; Edwards (<a href="#ref3">1993</a>), all considered the
		impact of gender issues on information use. Influences of social class and race on information use have been
		examined by Martinez (<a href="#ref3">1994</a>), and Sutton (<a href="#ref3">1991</a>).</p>
	<p>As Yuan points out; &quot;most end-user studies aimed at searching behaviour have focused on descriptions of
		behaviour or performance on a particular set of tasks&quot; (p. 219).</p>
	<p>Thirty four models of information seeking behaviour were identified, eleven of which were found to be the most
		relevant to this research. For the purposes of this research the Kuhlthau model (<a href="#ref2">1989 &amp;
			1991</a>) was selected based on the user group concerned and the theoretical framework behind the model.</p>
	<p>Learning is a combination of several, inseparable aspects: the outcome (what is learned), the process (how it is
		learned), the situation (where it is learned), and the internal characteristics of the learner (genetic and
		historical influences) (<a href="#ref3">Schmeck, 1988</a>).Learning is &quot;an active, constructive,
		cumulative, and goal-oriented process&quot;(<a href="#ref3">Shuell, 1990</a>: 532), which involves both the
		innate, domain specific knowledge of the individual, and the constructive process which builds on that innate
		skeletal framework (<a href="#ref2">Karmiloff-Smith, 1995</a>). Socio-cultural theorists, such as Vygotsky (<a
			href="#ref4">1978</a>), viewed development as not merely being influenced by social settings, but being
		grounded in social-settings. The social dimension of mental development was fundamental to Vygotsky's theory,
		individual consciousness was secondary (<a href="#ref3">Wertsch &amp; Stone. 1988</a>).</p>
	<p>The model of information integration developed by Das (<a href="#ref1">1984</a>), identifies four basic
		components of the integration process; sensory input, a sensory register, a central processing unit, and
		behavioural outputs, all of which are dependent on the individual.</p>
	<p>Practitioners and researchers differ greatly in their opinion of the effect on learning being influenced by the
		presentation of information; Clarke (<a href="#ref1">1983</a>) stated that, &quot;media are mere vehicles that
		deliver instruction but do not influence student achievement any more than a truck that delivers our groceries
		causes changes in our nutrition.&quot; (p. 459). Other researchers in education are of the opinion that the
		nature of information and the medium used to present that information have considerable impact on learning (<a
			href="#ref3">Underwood &amp; Underwood, 1990</a>; &amp; <a href="#ref1">Crook, 1994</a>). However they are
		aware that &quot;the cognitive effects of the more recently developed environments are speculative. Research is
		needed to extend this understanding.&quot; (<a href="#ref2">Kozma, 1991</a>: 210).</p>
	<h2>Methodology</h2>
	<p>The form of this research question is exploratory and descriptive, the use of electronic and digital information
		by the young people involved takes place in their own, multi-layered environment. Their activities are
		interwoven into this framework and therefore their behaviour significantly influenced by it. The main aim of
		this research is to produce in-depth, holistic case studies (<a href="#ref4">Yin, 1984</a>), giving the reader
		sufficient contextual and environmental descriptions to allow them to transfer the case based on conceptual
		applicability. These case studies should be reported &quot;with sufficient detail and precision to allow
		judgements about transferability.&quot; (<a href="#ref1">Erlson, <em>et al.</em>, 1993</a>: 33). The second aim
		of this research is &quot;to generate theory which is fully grounded in the data.&quot; (<a href="#ref1">Dey,
			1993</a>: 103.) Glaser and Strauss (<a href="#ref1">1967</a>) define a grounded theory as being one which
		will be &quot;readily applicable to and indicated by the data&quot; and &quot;be meaningfully relevant to and be
		able to explain the behavior under study.&quot;(p.3)</p>
	<p>Strauss and Corbin (<a href="#ref3">1990</a>), have identified three levels of analysis; a) to present the data
		without interpretation and abstraction, the participants tell their own story, b) to create a &quot;rich and
		believable descriptive narrative&quot; (p.36) using field notes, interview transcripts and researcher
		interpretations, c) building a theory using high levels of interpretation and abstraction. This research aims to
		combine the second and third approaches; to present rich, descriptive narratives at a micro level, to provide
		detailed descriptions, which will allow the reader to make sufficient contextual judgements to transfer the
		holistic case studies to alternative settings. It will also draw out the concepts and tenets of a theory using
		cross-case themes as they emerge from the analysis.</p>
	<p>The concern here is with the multiple constructions of reality as experienced by the individual. This
		constructivist methodology has two aspects; hermeneutics and dialectics (<a href="#ref2">Guba, 1996</a>). Here
		the hermeneutic aspect will be dealt with by the single, sealed unit, the holistic case. The dialectic aspect
		will be dealt with by workshops where participants are encouraged to compare and contrast the individual
		constructions of reality by debate. Thus giving them the opportunity to confirm the credibility of their own
		stories and examine the cross-case themes as interpreted by the researcher (<a href="#ref1">Brown &amp;
			Gilligan, 1992</a>).</p>
	<p>Each &quot;case is instrumental to learning about&quot; the impact of access to electronic information resources
		on learning &quot;but there will be important co-ordination between the individual studies.&quot; (<a
			href="#ref3">Stake, 1995</a>: 3).</p>
	<p>The emergent design of a naturalistic inquiry does not allow for a detailed plan before the research begins
		&quot;the research design must therefore be &quot;played by ear&quot;; it must unfold, cascade, roll,
		emerge.&quot; (<a href="#ref2">Lincoln &amp; Guba, 1985</a>: 203). However, it is possible to develop a model
		which allows for the iterative nature of this study. A model has been developed based on past research in the
		field and adapted from Lincoln and Guba's generic research model (<a href="#ref2">1985</a>: 188), this model can
		be found in Figure 1.</p>
	<h3>Field work.</h3>
	<p>A provisional plan for each case study has been etched out, bearing in mind that at any one time during the
		research the researcher will be involved in any combination of different phases as new samples are identified.
		Triangulation of multiple data sources and data collection techniques will strengthen the transferability of the
		findings (<a href="#ref2">Marshall &amp; Rossman, 1989</a>).</p>
	<p>Phase 1: Maintain researchers' diary throughout all Phases.</p>
	<ul>
		<li>Initial contact.</li>
		<li>Consent via sign-off forms.</li>
		<li>Interview to identify salient issues.</li>
		<li>Observation of the participant using electronic information sources.</li>
		<li>Identify subsequent participants using snowball sampling.</li>
		<li><strong>Time: 3 months.</strong></li>
	</ul>
	<p>Phase 2: <em>Focused exploration,</em> using;</p>
	<ol>
		<li>Interviews.</li>
		<li>Observation.</li>
		<li>Non human sources: <em>Content analysis of documentation</em>.</li>
		<li>Interviews with experts, these are likely to be learning resource managers, librarians and/or
			teacher/librarians.</li>
	</ol>
	<p>Time;- 8 months.</p>
	<p>Phase 3: Compile preliminary case studies.</p>
	<ul>
		<li>Member check: Case study subject to scrutiny by the participant individually and during workshops.</li>
		<li><strong>Time;-2-3 months.</strong></li>
		<li>Write up final case studies.</li>
	</ul>
	<p>Analysis will be ongoing throughout all phases using the constant comparative method originally developed by
		Glaser and Strauss (<a href="#ref1">1967</a>).</p>
	<p>In an attempt to minimise any distortion of the raw data by the presence of the researcher, and to build trust
		between participant and researcher, prolonged engagement with each case is necessary (<a href="#ref2">Lightfoot,
			1983</a>).</p>
	<h2>Phase One of field work</h2>
	<h3>Gaining entry</h3>
	<p>Although contact had been made with four schools a year before the field work was scheduled to begin, the
		iterative nature of the research meant further planning and establishing new informants. Gaining the trust of
		gatekeepers, at many hierarchical levels within the schools, was a time consuming and sometimes frustrating
		process. Although now established in each of the settings, the researcher is constantly involved in maintaining
		the relationships which have been established with gatekeepers. Reciprocity in the form of reports and
		presentations to various school committees has been agreed upon. This has not only increase the level of
		cooperation within the school, it has also provided a means of dissemination of the study findings to policy
		makers (<a href="#ref4">Yeager &amp; Kram, 1990</a>). It must also be noted that gaining entry does not only
		include the formal aspects of signing off and gaining permission, it also includes establishing trust and
		building up a rapport with all of the stakeholders; participants, informants and gatekeepers.(<a
			href="#ref3">Patton, 1990</a>).</p>
	<h3>Sample.</h3>
	<p>The objective was to sample for maximum variation (<a href="#ref3">Patton, 1987</a>), selecting a relatively
		small sample of great diversity to produce detailed, information-rich descriptions of each case. Any shared
		themes which emerge being all the more significant for having come from a small, heterogeneous sample. This
		sample &quot;was not chosen on the basis of some ‘a priori’ criteria but inductively in line with the developing
		conceptual requirements of the studies.&quot; (<a href="#ref1">Ellis, 1993</a>: 473). In order to build this
		sample Maykut &amp; Morehouse recommend the technique of snowball sampling 'to locate subsequent participants or
		settings very different from the first.&quot; (<a href="#ref3">1994</a>: 57). Snowball sampling can be
		accomplished in two ways, the first is to make initial contact with key informants who, in turn, point to
		information-rich cases. The second, and the technique used in this research, is to begin with an initial
		respondent who, through the process of interview and observation, will point out characteristics and issues
		which need further inquiry.</p>
	<p>The sample itself was likely to converge as the number of differing characteristics fell. Ford (<a
			href="#ref1">1975</a>), recommends termination of snowballing once the existing sample becomes saturated
		with first-order meaning. The size of the sample could not be predetermined, &quot;the criterion invoked to
		determine when to stop sampling is informational redundancy, not a statistical confidence level.&quot; (<a
			href="#ref2">Lincoln &amp; Guba, 1985</a>: 203). However, they state that &quot;a dozen or so interviews, if
		properly selected, will exhaust most available information; to include as many as twenty will surely reach well
		beyond the point of redundancy.&quot; (p.235). 16 cases were identified using the ‘dry-run’ as the starting
		point, to reduce the bias of this initial participant she will not be included as a case study in the final
		analysis. All participants are aged 13 to 14 years, this allows the researcher to conduct the field work both
		before and after they have embarked upon their specialist GCSE subjects.</p>
	<h3>Initial contact.</h3>
	<p>The preparation of the necessary consent forms was thought to be a vital part of this research, Erlson <em>et
			al.</em> (<a href="#ref1">1993</a>), suggest that they should be as open-ended as possible in order to avoid
		locking the researcher into a path that may run contrary to the research.</p>
	<p>In all but one of the 16 cases initial contact was made through school, this being seen as the least contentious
		method of locating and recruiting cases. Consent forms were prepared by the individual schools involved and
		given to parents and participants. Each participant was given an outline of the research and time scales for
		each of the activities involved at a preliminary meeting before agreeing to participate.</p>
	<h3>Interviews.</h3>
	<p>Open ended, exploratory interviews were carried out with all 16 participants. Because of the iterative nature of
		this research it was necessary to examine each interview and carry out initial analysis of both content and
		method, this was constant throughout Phase One. An interview guide was used which began with questions of the
		‘grand tour type’ (<a href="#ref3">Spradley, 1979</a>); to discover background detail on the individual, to
		relax the participant by easing them into the interview situation, and to establish the need for personal
		responses. The interview guide is shown below..</p>
	<h3>Observations.</h3>
	<p>Two types of observations were used during Phase One; unobtrusive observations with the researcher being as
		interesting as wallpaper (<a href="#ref1">Glesne &amp; Peshkin, 1992</a>), and semi-participant observations
		with the researcher interacting to a certain degree with the participant in order to begin to experience the
		reality of the participant (<a href="#ref2">Marshall &amp; Rossman, 1989</a>). The unobtrusive observations took
		place in each of the schools during lunch breaks and after school, the participants were observed in areas which
		provided open access to electronic information resources. These observations allowed the researcher to witness
		contextual settings and social interactions from a distance, as well as the behaviour of the participant.</p>
	<h3>Search logs.</h3>
	<p>Each participant was given a personal Search Log and asked to record every search for information using
		electronic resources. At the end of Phase 1, 9 of the 16 cases had begun to use their logs to keep a record of
		their searches, those 9 were happy with the organisation and format of the log and thought that the information
		provided in the log was clear. The 7 cases who had not made any entries gave a variety of reasons; &quot;I
		haven't done any research projects.&quot; &quot;I forgot what to do&quot;, &quot;it interferes with my
		work&quot;, &quot;It takes up too much time.&quot;, &quot;I can't be bothered.&quot; and &quot;I don't want to
		walk around with a great big yellow book&quot;. One of the participants' designed a database using Microsoft
		Access, using the column headings in the log as the fields definitions for the database.</p>
	<h3>Researchers log.</h3>
	<p>&quot;For the purposes of reporting, then, if an episode or remark is not in the log, it didn't happen.&quot; (<a
			href="#ref3">Strauss, 1987</a>: 79).</p>
	<p>A Researchers Log has been maintained throughout this study although entries have only become daily and detailed
		since the actual field work began, prior to that entries were based largely on theoretical concerns, both
		methodological and phenomenological. Many researchers advise the use of a researcher's log or reflexive journal,
		Ely (<a href="#ref1">1994</a>) goes further than this and gives the log a central role in both data collection
		and analysis as it &quot;contains the data upon which the analysis is begun and carried forward. It is the home
		for the substance that we use to tease out meaning and reflect upon them as they evolve.&quot; (p69). During
		Phase One of the research, the log has proved to be an invaluable tool, it has provided the arena for ideas to
		be tested, it has acted as a place of retreat to reformulate plans and it has provided a blow by blow account of
		the field work that could not have been achieved through any other means.</p>
	<h2>Conclusion.</h2>
	<p>Phase 1 of the research has now been completed in all case studies, the inductive analysis techniques used in
		this research mean that tentative categories have already begun to emerge. However, due to the nature of the
		research it is impossible to express interpretations at this stage before member checking has taken place. An
		evaluation of methods used has established that interviews and observations have been successful, the
		Researchers' Log has, and continues to be, an indispensable tool. The same cannot be said for Participant Logs,
		the researcher is currently experiencing some doubts about the use and usefulness of these logs. Phase 2 is
		currently underway and the research is on schedule, field work will end in March 1999 and the final thesis will
		be submitted in September 1999.</p>
	<h2><a id="ref1"></a>References</h2>
	<ul>
		<li>Borgman, C. L. et. al. (1995) Children's searching behaviour on browsing and keyword searching online
			catalogs: the science library catalog project. <em>Journal of the American Society for Information
				Science,</em> <strong>46,</strong> 663-684.</li>
		<li>Brown, L. M. &amp; Gilligan, C. (1992) <em>Meeting at the crossroads: women's psychology and girls'
				development.</em> Cambridge, MA.: Harvard University Press.</li>
		<li>Burdick, T. A. (1995) Success and diversity in information seeking: Gender and the information search styles
			model. <em>School library media quarterly.</em> <strong>Fall,</strong> 19-26.</li>
		<li>Clarke, R. (1983) Reconsidering research on learning from media. <em>Review of educational research.</em>
			<strong>53</strong>, 445-459</li>
		<li>Cole, C. (1996) A modest proposal. <em>Times educational supplement.</em> <strong>October,</strong> 21.</li>
		<li>Crook, C. (1994). _Computers and the collaborative experience of learning._London: Routledge.</li>
		<li>Das, J. (1984) Intelligence and information integration. In: J. Kirby, ed. <em>Cognitive strategies and
				educational performance.</em> New York: Academic Press.</li>
		<li>Dey, I. (1993) <em>Qualitative data analysis: A user friendly guide for social scientists.</em> London:
			Routledge.</li>
		<li>Ellis, David (1993) Modeling the information-seeking patterns of academic researchers: a grounded theory
			approach. <em>Library Quarterly</em>. <strong>63,</strong> 469-486.</li>
		<li>Ely, M. <em>et al.,</em> (1994) <em>Doing qualitative research: circles within circles.</em> London: Falmer
			Press.</li>
		<li>Erlandson, D. A., Harris, E. L., Skipper, B. L., &amp; Allen, S. D. (1993) <em>Doing naturalistic inquiry. A
				guide to methods.</em> London: Sage.</li>
		<li>Farrell, E., Peguero, G., Lindsay, R. &amp; White, R. (1988) Giving voice to high school students: Pressure
			and boredom, ya know what I'm sayin? <em>American Educational Research Journal,</em> <strong>25,</strong>
			489-502.</li>
		<li>Ford, J. (1975) <em>Paradigms and fairytales: an introduction to the science of meanings.</em> London:
			Routledge &amp; Kegan Paul.</li>
		<li>Ford, N. &amp;Miller, D. (1994) Gender differences in Internet perceptions and use. <em>ASLIB
				Proceedings</em> . <strong>48,</strong> 183-192.</li>
		<li>Glaser, B.G. &amp; Strauss, A.L. (1967) <em>The discovery of grounded theory.</em> Aldine.</li>
		<li>Glesne, C., &amp; Peshkin, A. (1992) <em>Becoming qualitative researchers.</em> London: Longman.</li>
		<li><a id="ref2"></a>GREAT BRITAIN. British Educational Communications and Technology Agency. (1998)
			<em>Connecting schools, networking people.</em> BECTa</li>
		<li>GREAT BRITAIN. Department for Education and Employment. (1997a) <em>Connecting the learning society:
				National grid for learning.</em> London: DfEE/</li>
		<li>GREAT BRITAIN. Department for Education and Employment. (1997b) _Preparing for the information age: Synoptic
			report of the education Departments' Superhighways Initiative._London: DfEE.</li>
		<li>GREAT BRITAIN. Department for Education and Employment. (1997c) _Survey of information technology in schools
			in England._London: HMSO.</li>
		<li>Guba, E. (ed.) (1992) <em>The Alternative Paradigm.</em> London: Sage.</li>
		<li>Jacobson, F. F. (1994) Finding help in all the right places: Working towards gender equity. <em>Youth
				services in libraries.</em> <strong>Spring,</strong> 289-293.</li>
		<li>Karmiloff-Smith, A. (1995) <em>Beyond modularity: A developmental perspective on cognitive science.</em>
			Cambridge, MA: MIT Press</li>
		<li>Kozma, R. B. (1991) Learning with media. <em>Review of educational research.</em> <strong>61,</strong>
			179-211.</li>
		<li>Kuhlthau, C.C. (1989) Information search process: a summary of research and implications for school library
			media programs. <em>School Library Media Quarterly.</em> <strong>18,</strong> 19-25.</li>
		<li>Kuhlthau, C.C. (1991) Inside the search process: Information seeking from the user's perspective.
			<em>Journal of the American Society for Information Science</em> <strong>42,</strong> 361-371.</li>
		<li>Latrobe, K. &amp; Havener, W. M. (1997) The information-seeking behaviour of high school honors students: an
			exploratory study. <em>Youth Services in Libraries.</em> <strong>Winter,</strong> 189</li>
		<li>Large, A. <em>et al.</em> (1995) Multi-media and comprehension: the relationship among text, animation, and
			captions. <em>Journal of the American Society for Information Science</em> <strong>46,</strong> 340-347.
		</li>
		<li>Library Association. (1998) <em>Survey of UK secondary school libraries.</em> London: Library Association
			Publishing.</li>
		<li>Lightfoot, S. L. <em>The good high school.</em> New York: Basic Books.</li>
		<li>Lincoln, Y. S., &amp; Guba, E. G. (1985) <em>Naturalistic inquiry.</em> London: Sage.</li>
		<li>Marchionini, G. (1995) <em>Information seeking in electronic environments.</em> Cambridge: Cambridge
			University Press.</li>
		<li><a id="ref3"></a>Martinez, M. E. (1994) Access to information technologies among school-age children:
			Implications for a democratic society. <em>Journal of the American Society for Information Science</em>
			<strong>45,</strong> 395-400.</li>
		<li>Marshall, C. &amp; Rossman, G.B. (1989) <em>Designing qualitative research.</em> London, Sage.</li>
		<li>Maykut, P., &amp; Morehouse, R. (1994) <em>Beginning qualitative research, a philosophical and practical
				guide.</em> London: Falmer Press.</li>
		<li>Mellon, C. (1990) <em>Naturalistic inquiry for library science: Methods and applications for research,
				evaluation and teaching.</em> Cambridge: Cambridge University Press.</li>
		<li>Neuman, D. (1995) High school students' use of database: Results of the National Delphi Study. <em>Journal
				of the American Society for Information Science</em> <strong>46,</strong> 284-298.</li>
		<li>NiAA. (1997) <em>IT use in education: Survey.</em> Gateshead: NiAA Education Sector Group.</li>
		<li>Patton, M. Q. (1990) <em>Qualitative Evaluation and Research Methods.</em> 2nd ed. London: Sage.</li>
		<li>Pitts, J. M. (1993) Mental models of information: The 1993-94 AASL / Highsmith Research Award Study.
			<em>School library media quarterly.</em> <strong>23</strong></li>
		<li>Poston-Anderson, B. &amp; Edwards, S. (1993) The role of information in helping adolescent girls with their
			life concerns. <em>School library media quarterly.</em> <strong>Fall,</strong> 25-30.</li>
		<li>Schmeck, R. R. (ed.) (1988) <em>Learning strategies and learning styles.</em> Plenum Press.</li>
		<li>Shuell, T. J. (1990) Phases of learning. <em>Review of educational research.</em> <strong>60,</strong>
			531-547.</li>
		<li>Spradley, J. P. (1979) <em>The enthographic interview.</em> New York: Holt, Rienhart, &amp; Winston.</li>
		<li>Stake, R.E. (1995) <em>The art of case study research.</em> London: Sage.</li>
		<li>Strauss, A. L. (1987) <em>Qualitative data analysis for social scientists.</em> Cambridge: Cambridge
			University Press.</li>
		<li>Strauss, A. &amp; Corbin, J. (1990) <em>Basics of qualitative research: grounded theory procedures and
				techniques.</em> London: Sage.</li>
		<li>Sutton, R. E. (1991) Equity and computers in the school: a decade of research_. Review of educational
			research._ <strong>61,</strong> 475-503.</li>
		<li>Underwood, J.D.M. &amp; Underwood, G. (1990) <em>Computers in learning: helping children aquire thinking
				skills.</em> Oxford: Basil Blackwell.</li>
		<li><a id="ref4"></a>Vygotsky, L. S. (1978) <em>Mind in society.</em> Cambridge, MA.: Harvard University Press.
		</li>
		<li>Wertsch, J. V. &amp; Stone, C. A. (1988) The concept of internalization in Vygotsky's account of the genesis
			of higher mental functions. In: Wertsch, J. V. (Ed_) Culture communication and cognition: Vygotskian
			perspectives._ Cambridge: Cambridge University Press.</li>
		<li>Westbrook, L. (1993) User needs: A synthesis and analysis of current theories for practitioners. <em>RQ</em>
			<strong>Summer 4,</strong> 541-549.</li>
		<li>Wilson, T.D. (1994) Information needs and uses: fifty years of progress? In: B.C. Vickery, ed. <em>Fifty
				years of information progress: a Journal of Documentation review.</em> London: ASLIB, 15-51</li>
		<li>Yang, Shu Ching, (1997) Information seeking as problem-solving using a qualitative approach to uncover the
			novice learners' information-seeking processes in a Perseus hypertext system. <em>Library and information
				science research.</em> <strong>19,</strong> 71-92.</li>
		<li>Yeager, P. C. &amp; Kram, K. E. (1990) Fielding hot topics in cool settings: the study of corporate ethics.
			<em>Qualitative Sociology.</em> <strong>13,</strong> 127-148.</li>
		<li>Yuan, W. (1997) End-user searching behaviour in information retrieval: A longtidudinal study. <em>Journal of
				the American Society for Information Science</em> <strong>48,</strong> 218-234.</li>
		<li>Yin, R. K. (1984) <em>Case study research: Design and methods.</em> London: Sage.</li>
	</ul>
	<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
		<em>The impact of access to electronic and digital information resources on learning opportunities for young
			people: A grounded theory approach</em>, by [Alison Pickard](MAILTO: alison.pickard@unn.ac.uk)<br>
		Location: http://InformationR.net/ir/4-2/isic/pickard.html    © the author, 1998.<br>
		Last updated: 9th September 1998</p>

</body>

</html>