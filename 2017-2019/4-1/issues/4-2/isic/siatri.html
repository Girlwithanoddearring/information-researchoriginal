<!DOCTYPE html>
<html lang="en">

<head>
	<title>Information seeking in electronic environment: a comparative investigation among computer scientists in
		British and Greek Universities.</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords"
		content="information seeking, electronic environments, computer scientists, Greek universities, British universities">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>
		Information seeking in electronic environment: a comparative investigation among computer scientists in British
		and Greek universities.</h1>
	<h3>Rania Siatri</h3>
	<p>Department of Information &amp; Communications<br>
		Manchester Metropolitan University<br>
		<em>o.siatri@mmu.ac.uk</em></p>
	<h2>1. Aim and objectives</h2>
	<p>The aim of the study is to examine the information seeking behaviour of academic computer scientists in an
		electronic environment</p>
	<p>The objectives are:</p>
	<ol>
		<li>To investigate the different practices and methods used by academic computer scientists in retrieving
			information from electronic information sources.</li>
		<li>To identify the types and range of electronic information resources used currently by academics and
			determine the level and spread of their use.</li>
		<li>To examine the impact of these systems on communication and exchange of professional knowledge among
			computer scientists.</li>
		<li>To examine if it is possible to identify the factors which trigger the information need and the association
			of ideas, or logical consequence which leads to a particular information seeking behaviour.</li>
		<li>To investigate if the availability of information resources affects the information seeking patterns and
			communication of academics and to what extent.</li>
		<li>To determine whether or not different kinds of information needs ( e.g. professional, personal) lead to a
			different information seeking behaviour and communication channels.</li>
	</ol>
	<p>It was decided to concentrate the study on computer scientists, since they are leaders in information technology,
		and thus can be expected to be at the forefront of exploiting IT in their communication behaviour. Although
		there have been many studies examining the behaviour of various academic and non-academic groups, none of them
		has focused on computer scientists. Narrowing the focus of the study should provide a better understanding of
		the information tools used by the particular group of scientists, a more detailed and accurate profile of the
		users leading to an in-depth understanding of the information seeking process. This study seeks to investigate
		how electronic information resources and information communication facilities especially those located on the
		Internet, have affected the computer scientists in terms of communication, exchange of knowledge and information
		seeking behaviour.</p>
	<p>Another feature of the study is that, part of it will take place in Greece. The different level of information
		and library provision in Britain and Greece makes it appropriate to compare and contrast groups from these
		countries; this should enable us to draw a picture of the current situation in information use, given the
		different development of information and library provision in the two countries.</p>
	<h2>2. Background Information</h2>
	<p>User studies are one of the areas most researched in library and information science and form a large body of
		literature in the discipline. However it is an area which combines some of the most used and the least precisely
		defined concepts of library and information science. These concepts, such as information use or need,
		information seeking behaviour channel of communication, exist in a system of complicated and interdependent
		relations. Wilson provides a framework for defining the field and &quot;suggests that the information seeking
		behaviour results from recognition of some need perceived by the user&quot; [<a href="#wil">Wilson, 1981</a>,
		p4.] A user may follow formal and/or informal channels of communication in order to acquire the desired
		information. The course of action that a user will engage determines and the information seeking behaviour.</p>
	<p>The origin and conception of user studies were based upon the notion &quot;that if one could somehow identify the
		information needs and uses of a population subset, one could design effective information systems&quot;[<a
			href="#cra">Crawford, 1978</a>]. This phrase summarises exactly the essence of user studies; studying a
		community and its needs helps in order to provide the information services demanded.</p>
	<p>Throughout the historical development of user studies, methodological issues always seemed to be the centre of
		attention and debate among information scientists. However the main issue is not the use of defective
		methodology but rather the absence of a theoretical framework surrounding user studies. Methodology, in order to
		be effective, should be embedded within in a coherent conceptual framework which somehow will provide the
		boundaries of the subject area, by describing and defining the concepts within the field and by creating a
		systems of dependencies and interrelation among these concepts.</p>
	<p>There are two dominant kinds of user studies, the system-oriented studies, and the user-oriented studies. In the
		first case users are viewed as passive recipients of information and the study investigates their external
		behaviour, generally by means of qualitative methods. Although these surveys have yield much quantitative data,
		which gives an overall picture of information needs and seeking behaviour, they fail to convey a real picture
		regarding the factors which trigger the information search and a more in-depth insight into the individual's
		conceptions and thoughts. On the other hand in user-oriented studies, users are viewed as active and
		self-controlling recipients of information and they are concerned with the internal cognitions of users, which
		are investigated by qualitative methods. [<a href="#der">Dervin &amp; Nilan,1986</a>] The researcher espouses
		the conceptual framework upon which user-oriented studies are based, as she acknowledges the dynamic and
		responsive to changes nature of human behaviour and thus of information seeking.</p>
	<p>Some conceptual frameworks have been developed, which respond to the user-oriented model of user study. According
		to Sugar there are four wide categories of such frameworks. The cognitive approach, where the researchers focus
		their efforts to understand &quot;how individuals process information and then illustrate this process through
		models&quot; [<a href="#sug">Sugar, 1995</a>, p.80] In the holistic approach the researcher is not only
		interested in the cognitive needs of the users but also takes into account other factors which influence the
		users such as physiological and affective needs. Another category is the action research: this approach views
		users as active participants during their research and tries to understand -- the language, the activities and
		the social interrelationships of users. A more recent one is the theory of usability techniques, which was
		developed in order to respond to the increased number of studies, which deal with the use of computers and new
		technology in conjunction with information seeking behaviour. They focus on the use of a system but from the
		user's point of view. They take as a base user's needs in order to create an environment which will be friendly,
		effective and easy for a user to handle.</p>
	<p>Despite the large number of user studies, information scientists are still left with an unsettling feeling of an
		elementary level of knowledge as far as it concerns user needs and information seeking behaviour. Taking into
		account the constant development in the provision of recent electronic systems, the lack of understanding the
		information seeking behaviour poses an obstacle in the process interpreting the way in which the electronic
		information services are being delivered. There is a need for alternative research methods and conceptual
		frameworks which will provide to the information science community, the evidence to acquire a more in-depth
		understanding of the users of information services.</p>
	<h2>3. Methodology</h2>
	<p>To meet the main aim and objectives of the study, a combination of quantitative (questionnaires) and qualitative
		(interviews and logbooks) research methods along with a comprehensive literature review will be used. The first
		part of the review will identify previous and present research projects on the subject and will assist the
		researcher to gain a better understanding of the complexity and diversity of the subject. The second part of the
		review will deal with the possible impact of the electronic environment on academic information use. It will
		examine whether or not the development of new technology has affected the theoretical frameworks developed, as
		well as the research methods which are employed in user studies.</p>
	<p>A questionnaire was designed and it was distributed to a number of academic computer scientists in all
		universities in each country. Its purpose was to collect evidence concerning the use of electronic information
		resource and the patterns of different practices used by academics to retrieve the required information. The
		analysis (SPSS is used for it) and critical evaluation of the data should draw an overall picture of the use of
		electronic information resources and patterns of information seeking by academic computer scientists in both
		countries (aims 1, 2, 3).</p>
	<p>The means which will produce the qualitative information required will take the form of semi-structured and
		in-depth interviews along with some personal logbooks which will be kept for a period of time, by a small number
		of academics in both countries. Dervin's sense-making theory will be applied to both interviews and logbooks.
		The interviews will attempt to identify the factors which are liable to affect the information seeking behaviour
		of a researcher such as nature of the information need, the purpose of it will serve or the existence of
		external barriers posed by the environment such as availability of resources (aims 5, 6). The logbooks will
		serve as a readily available tool of documenting any thoughts, questions and information needs that an academic
		researcher may have during the pre-arranged period of time (aim 4). The information documented in the logbooks
		will hopefully provide an insight into whether or not is possible to identify the factors which trigger the
		information needs. The researcher is aware of the difficulties involved with research which is heavily depended
		on keeping journals but it is considered that is worth while. The use of questionnaires, interviews and logbooks
		will provide the grounds for rigorous and constant comparisons of the data. Additionally the engagement of
		different research methods seeks to assist towards the validity of the findings.</p>
	<h2>4. Progress to date</h2>
	<p>A literature review of information seeking behaviour and user studies has been undertaken. The review assisted
		the researcher to identify previous and present research projects and has provided valuable knowledge for the
		understanding of the theoretical and methodological issues surrounding user studies.</p>
	<p>A questionnaire has been developed tested and administered in both Greece and the UK. I have obtained preliminary
		results from the UK and currently I am working on the analysis of the results, from the questionnaires
		administered in Greece. For the analysis of the data it is used the SPSS. The interviews are almost prepared and
		they are scheduled to be piloted in September.</p>
	<h2>5. Timetable</h2>
	<table>
		<tbody>
			<tr>
				<td> </td>
				<td>
					<p><em><strong>Activity</strong></em></p>
				</td>
				<td>
					<p><em><strong>Target completion</strong></em></p>
				</td>
				<td>
					<p><em><strong>Status</strong></em></p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>1</strong></p>
				</td>
				<td>
					<p>Literature review</p>
				</td>
				<td>
					<hr>
				</td>
				<td>
					<p>1st version completed but ongoing</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>2</strong></p>
				</td>
				<td>
					<p>Determine research methodology</p>
				</td>
				<td>
					<hr>
				</td>
				<td>
					<p>Completed</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>3</strong></p>
				</td>
				<td>
					<p>Design of Questionnaire</p>
				</td>
				<td>
					<hr>
				</td>
				<td>
					<p>Completed</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>4</strong></p>
				</td>
				<td>
					<p>Pilot questionnaire (UK &amp; Greece)</p>
				</td>
				<td>
					<hr>
				</td>
				<td>
					<p>Completed</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>5</strong></p>
				</td>
				<td>
					<p>Data collection UK &amp; Greece</p>
				</td>
				<td>
					<hr>
				</td>
				<td>
					<p>Completed</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>6</strong></p>
				</td>
				<td>
					<p>Data analysis &amp; comparison of findings</p>
				</td>
				<td>
					<p>Aug. 98</p>
				</td>
				<td>
					<p>Currently undertaken</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>7</strong></p>
				</td>
				<td>
					<p>Design &amp; pilot interview (UK &amp; Greece)</p>
				</td>
				<td>
					<p>Nov. 98</p>
				</td>
				<td>
					<p>Under consideration</p>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>8</strong></p>
				</td>
				<td>
					<p>Conduct interviews and analysis</p>
				</td>
				<td>
					<p>Mar. 99</p>
				</td>
				<td>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>9</strong></p>
				</td>
				<td>
					<p>Design &amp; pilot logbooks ( UK &amp; Greece)</p>
				</td>
				<td>
					<p>April 99</p>
				</td>
				<td>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>10</strong></p>
				</td>
				<td>
					<p>Data collection &amp; analysis of logbooks</p>
				</td>
				<td>
					<p>July 99</p>
				</td>
				<td>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>11</strong></p>
				</td>
				<td>
					<p>Compare findings form interviews &amp; logbooks</p>
				</td>
				<td>
					<p>Oct. 99</p>
				</td>
				<td>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>12</strong></p>
				</td>
				<td>
					<p>Writing up of thesis</p>
				</td>
				<td>
					<p>June 00</p>
				</td>
				<td>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<p><strong>13</strong></p>
				</td>
				<td>
					<p>Slippage</p>
				</td>
				<td>
					<p>Sept. 00</p>
				</td>
				<td>
					<hr>
				</td>
			</tr>
		</tbody>
	</table>
	<h2>6. References</h2>
	<ul>
		<li><a id="cra"></a>Crawford, Susan. [1978] Information needs and uses. In: Williams, Martha, ed. <u>Annual
				Review of Information Science and Technology: volume 13</u>. Knowledge Industry Publications, p.61-81.
		</li>
		<li><a id="der"></a>Dervin, Brenda and Nilan, Michael.[1986] Information needs and uses. In: Williams, Martha,
			ed<u>. Annual Review of Information Science and Technology: volume 21</u>. Knowledge Industry Publications,
			p.3-33.</li>
		<li><a id="sug"></a>Sugar, Williams. [1995] User-centered perspectives of information retrieval research and
			analysis methods. In: Williams, Martha, ed. <u>Annual Review of Information Science and Technology: volume
				30</u>. American Society of Information Science, p.77-109</li>
		<li><a id="wil"></a>Wilson,T.D. [1981] On user studies and information needs. <u>Journal of Documentation</u>,
			37(1), p.3-15.</li>
	</ul>
	<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
		<em>Information seeking in electronic environment: a comparative investigation among computer scientists in
			British and Greek Universities.</em>, by <a href="mailto:o.siatri@mmu.ac.uk">Rania Siatri</a><br>
		Location: http://InformationR.net/ir/4-2/isic/siatri.html    © the author, 1998.<br>
		Last updated: 9th September 1998</p>

</body>

</html>