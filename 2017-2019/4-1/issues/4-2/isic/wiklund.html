<!DOCTYPE html>
<html lang="en">

<head>
	<title>Information as social and intellectual capital in the research career: a gender perspective</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords" content="information, social capital, intellectual capital, research careers, gender">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>Information as social and intellectual capital in the research career: a gender perspective</h1>
	<h3>Gunilla Wiklund</h3>
	<p>Department of Information and Culture<br>
		Umeå University<br>
		<em>gunilla.wiklund@infcult.umu.se</em></p>
	<h2>Introduction</h2>
	<p>The academic community is a stratified social structure built on competition and a need to be acknowledged. In
		this environment men and women do not have the same opportunities and one manifestation is that women have
		difficulties in gaining access to social networks. This is likely to affect women's access to information,
		particularly information otherwise difficult to get, since that is usually available through informal personal
		contacts. Information exchange is fundamental for researchers and the foundation for their competence and
		authorisation. By being acknowledged by others the researcher and his/her research are given a value. The
		researcher can thus be seen as part of a social system. In this system information is needed for problem solving
		as well as advancement and both the amount and type of information available affect the researchers'
		participation and position in the system. Therefore, when studying how men and women find for them relevant
		information, it is interesting to take into consideration differences in access to information. If women have
		less or different opportunities to gather needed information, how will their information behaviour be affected?
		In addition, attention may be given to new information technology with new ways of getting into contact with
		people. There could be a potential for improved means of communication, beyond established structures.</p>
	<p>The purpose of the thesis is to study the information behaviour of male and female post-graduate students from a
		gender perspective, with special focus on informal contacts and the importance of new information technology.
		This is done by a study of post-doctoral students in two academic settings. The goal is to gain a better
		understanding of the importance of information from different kinds of personal contacts for the research carrer
		and the role of information technology in the creation of such contacts. Information behaviour is regarded as
		the interplay between information needs, access to and use of information and the effects of information use (<a
			href="#ref">Höglund &amp; Persson, 1985</a>). These four aspects are treated as problem areas to which
		research questions are related. The behaviour is to a large extent affected and formed by a social context in
		which various demands, expectations and ideas affect people's ways and possibilities to act. Doctoral students
		are by means of their education, socialised into a social context in which information is necessary both for
		problem-solving and advancement and can thus be seen as an intellectual and social capital. The processes in
		which these capitals are acquired and their influence on the students' research careers are important to
		understand. Research on women and the academic community with descriptions of structural conditions will be used
		for an understanding of the students' milieu.</p>
	<h2>Problem areas and hypotheses</h2>
	<p>The need for information is governed not only by cognitive reasons; equally important are affective and
		physiological ones (<a href="#ref">Wilson, 1981</a>). The doctoral students' needs are responsive to personal
		needs and shaped in a social process where competition is central and in which information is needed in order to
		understand prevailing norms within the subject field. For example what subjects are appropriate, which scholars
		that are central to the field etc. Depending on what a student does and wish to accomplish, different kinds of
		information are sought from various sources. At the beginning of their post-graduate studies students may need
		information to understand the structure of the subject and the local environment, while later they may need to
		be familiar with international structures and actors related to their own research. This includes information
		about ways to financing, contacts and distribution of research material. For an understanding of the reasons for
		wanting information one must therefore look to the situations of the students; what they are doing, their
		expectations, future plans, financing etc. and what they need in order to handle their situations as
		post-graduate students.</p>
	<p>A person's access to information affects how various needs can be satisfied. The sources from where information
		is available are often divided into formal and informal, giving access to different kinds of information.
		Through formal sources, e.g. journals, public information is found which is central to the intellectual capital.
		This information is not always contemporary, as the process leading to publication can be protracted. Informal
		sources give access to information otherwise difficult to find about research in progress but also information
		of actors, values and other important aspects of research, i.e. the social capital needed for positioning in the
		field. The direct interaction between people is central and informal exchange of information is often considered
		one of the most important sources for information. The mapping of informal contacts are therefore of great
		importance for an understanding of the information behaviour, in particular since different kinds of personal
		contacts are likely to give access to different kinds of information and the access to informal sources may vary
		more than access to formal contacts. Concerns will therefore be given to whom the doctoral students have contact
		with, what information is gathered from whom and why.</p>
	<p>Access to information varies over time. At the beginning of the research studies the supervisor is likely to play
		a large role, not only as a mediator of information but also to introduce the students to interesting networks.
		As the students become socialised into the subject fields they will probably develop personal contacts of their
		own and habits for gathering of information. Influential factors are knowledge of sources, personal preferences
		etc. Changes over time will therefore be investigated.</p>
	<p>With an in interest in information from personal contacts it is relevant to study the use of new information
		technology since there are opportunities for interaction between people with similar interest on equal
		conditions. The study will look at how male and female students use different functions, particularly functions
		involving human interaction, such as mailing lists. There are findings indicating that women value these kinds
		of contacts more than men do (<a href="#ref">Lincoln, 1992</a>).</p>
	<p>The interest in usage of information and the effects of usage help to see information as part of daily
		activities. How different kinds of information are used and how useful different kinds of sources are. The
		results of the use of information are mirrored in how the students' information behaviour is affected, for
		example what new needs that arise due to the usage and if access, and thus the possibilities to satisfy the
		needs, are changed. It is also possible to look to changes in the students' total situations by observing
		production and advancement in the field as well as seeing to their own satisfaction. How are their theses
		affected? What are their positions in the field? Are they content with their situations and how are they
		evolving?</p>
	<p>These problem areas interact in a complex pattern, which makes it important to study all of them although the
		focus of the study is on access to information through various sources. Access to information is an important
		aspect, where inequality may exist between men and women, but availability and different kinds of needs for
		information affect the final use of sources and consequently the information the students gather. Various needs
		may change their access to information as new contacts are developed with people who are perceived to have
		important information.</p>
	<p>The preliminary hypotheses that the study will work with are:</p>
	<ul>
		<li>There are differences in access to and participation in social networks between male and female doctoral
			students, for example types of personal contacts and the proximity of these contacts to the departments in
			which the students are active. These differences affect their information behaviour and in the long run
			their research careers.</li>
		<li>These differences are likely to decrease as their research studies proceed, resulting in more equal patterns
			of progress of their careers.</li>
		<li>New information technology is important for the establishment of informal contacts for the doctoral
			students, for example to build social networks of their own, a function which may be valued higher by female
			students than male.</li>
	</ul>
	<h2>Theory</h2>
	<p>The theoretical base for the study is research on information needs and seeking in library and information
		science. Information gathering is seen as being a part of a social system in which information seeking is an
		activity performed in order to satisfy personal needs (<a href="#ref">Wilson, 1997</a>). By stressing social
		dimensions a shift occurs from studying information-seeking behaviour to the importance of information in
		different social settings. The word gather is used in order to emphasize the fact that a person may not only
		actively seek for some specific piece of information but may also browse, i.e. randomly find out if something is
		of interest, or the person may receive unasked-for information from someone, e.g. a supervisor. The person may
		or may not find this information useful.</p>
	<p>The personal sources that are of special interest in the study can be studied on an individual level but they can
		also be regarded as parts of various social networks. Social networks are here understood as contacts between
		people that are more or less regulated and continuous (<a href="#ref">Höglund &amp; Persson, 1985</a>).
		According to Höglund and Persson, information in these may be shared between well-informed people rather than
		with those less well-informed. Newcomers in a subject field may have difficulties gaining access to relevant
		networks (<a href="#ref">Cronin, 1982</a>). In the study the networks of individual doctoral students will be
		studied with respect to kinds of contacts and what information is gathered from whom (<a
			href="#ref">Haythornthwaite, 1996</a>). These networks will then be compared with one another.</p>
	<p>Another point of departure for the study is descriptions of the academic community as a hierarchic research
		culture built on competition for financing as well as visibility as a researcher, sometimes called male. This
		leads to a need to be acknowledged by people that are higher up in the hierarchy (<a href="#ref">Fox, 1991</a>).
		In this environment informal, social networks are formed, normally maintained by men, and there are reasons to
		believe that women are more exposed when trying to gain access to these.</p>
	<p>As a framework for interpretation of information behaviour and the formulation of research questions the
		so-called gender system is used (<a href="#ref">Hirdman, 1987</a>). Gender is here understood as a social
		phenomenon with a fundamental social and structural ordering of men and women in society. In this relation men
		are generally given the preferential right of interpretation, leading to an uneven distribution of resources.
		These relationships are seen as circumstances in which people have opportunities to act. Descriptions of the
		academic community as a male research culture belong here. By such a perspective, men and women do not have to
		act differently, but women have to adjust to structural and cultural conditions where they do not have the same
		status as men.</p>
	<p>Different directions of what is called The Social Shaping of Technology, for example the SCOT (Social
		Construction of Technology) school offer a way to understand information technology and its use (<a
			href="#ref">Bijker, <em>et al.</em>, 1987</a>). The meaning and function of technology are considered to be
		created in different contexts, e.g. production or usage, which changes focus from technology as a fixed artefact
		towards a conception of flexibility and ambiguity (<a href="#ref">Lie, 1995</a>). In the study this leads to an
		awareness that information technology may have different values depending on who uses it and for what reasons.
		Feminist research on e.g. women's participation and experiences of computer-mediated communication are also of
		interest here (<a href="#ref">Kramarae &amp; Taylor, 1993</a>).</p>
	<h2>Methods, workplan and progression</h2>
	<p>The methodological point of departure for the thesis is pluralistic, i.e. various methods are used to gather and
		analyse empirical data. Both quantitative and qualitative methods elements are present, although the latter
		predominate.</p>
	<p>As a preparation of interview questions the study begins with pilot interviews with three senior researchers
		about their information habits and experiences from their research careers. Overall the thesis is based on
		case-studies of two research departments, one where research groups are common, for example in the natural
		sciences, and one where individual work dominate such as in the humanities. Six doctoral students from each
		department, three male and three female, in different stages of their education, will be interviewed, a total of
		twelve students. By dividing the post-graduate studies in three phases, initial, middle and final, and choosing
		two students from each phase, it is possible to relate different phases of their careers to their information
		behaviour.</p>
	<p>Since knowledge of the social settings in which the doctoral students are active in is important, participant
		observation will be used to get an insight. Focus will be on the history of the departments, their work, culture
		of seminars etc. Complementary methods are reading of material and interviews with people with a good knowledge
		of the departments. A bibliometric mapping will also be done for a notion of connections between researchers and
		who are working with similar questions. This will later be compared with the students' personal networks for an
		understanding of their integration and positions in the field.</p>
	<p>Further, to investigate the situations of the students and their information behaviour, deep interviews and
		diaries that the students will be asked to keep, will be used. The keeping of the diaries will take place during
		three separate weeks in two months. The students will account for their daily activities; when information is
		gathered and used, what type of information, from where etc. To reduce their workload some parts of the diaries
		will consist of ready-made alternatives.</p>
	<p>There will be two interviews per student, separated by the diaries. The initial interviews will focus on a
		reconstruction of the students' way through their research studies to their present situation. In the following
		interviews the first interviews and the diaries will be followed-up with a deepening of their needs of
		information, ways of gathering information etc. Throughout the study, a mapping of personal networks as well as
		the use of information technology is important and will be given special attention.</p>
	<p>In the analysis the empirical material will be used to give a picture of the departments and the doctoral
		students' careers. These will be compared with one another for examples of the interplay between context and
		information behaviour. As the study has a gender perspective special attention will be paid to structural and
		cultural circumstances which may affect the development of information behaviour and the research careers.</p>
	<p>The gathering of data and writing of the thesis is planned to take four years, with data collection the first two
		years and analysis and writing the following two.</p>
	<h3>Preliminary work plan:</h3>
	<p><strong>Year 1</strong> Pilot study; Description of departments; Bibliometric mapping; 1<sup>st</sup> round of
		interviews<br>
		<strong>Year 2</strong> Diaries; 2<sup>nd</sup> round of interviews; Parallel working at the material<br>
		<strong>Year 3</strong> Working at the material and analysis<br>
		<strong>Year 4</strong> Continuos analysis and completion of the thesis</p>
	<p>Progress: The work is still at its beginning. Last autumn I presented a plan for my dissertation and now I am
		seeking financing. The next step is to work on a literature review and discuss information behaviour related to
		a gender perspective.</p>
	<h2><a id="ref"></a>References</h2>
	<ul>
		<li>Bijker, W., Hughes, T., &amp; Pinch, T. (Eds.). (1987). <em>The social construction of technological
				systems</em>. <em>New directions in the sociology and history of technology</em>. Cambridge, MA: MIT
			Press.</li>
		<li>Cronin, B. (1982). Progress in documentation. Invisible colleges and information transfer. A review and
			commentary with particular reference to the social sciences. <em>Journal of Documenation</em>
			<strong>38</strong>, 212-236.</li>
		<li>Fox, M. (1991). Gender, environmental milieu, and productivity in science. In H. Zuckerman, J. Cole &amp; J.
			Bruer (Eds.), <em>The Outer Circle. Women in the Scientific Community</em>. New York: Norton.</li>
		<li>Haythorntwaite, C. (1996). Social network analysis. An approach and technique for the study of information
			exchange. <em>Library &amp; Information Science Research</em> <strong>18</strong>, 323-342.</li>
		<li>Hirdman, Y. (1987). Makt och kön. I O. Petersson (Red.) <em>Maktbegreppet</em>. Stockholm: Carlsson.</li>
		<li>Höglund, L. &amp; Persson, O. (1985). <em>Information och kunskap</em>. <em>Informationsförsörjning -
				forskning och policyfrågor</em>. Umeå: INUM.</li>
		<li>Kramarae, C. &amp; Taylor H. (1993). Women and men on electronic networks. A conversation or a monologue?.
			In J. Taylor, C. Kramarae &amp; M. Ebben (Eds.), <em>Women, Information Technology, and Scholarship</em>.
			Urbana, Illinois: Center for Advanced Study.</li>
		<li>Lie, M. (1995). Technology and masculinity. The case of the computer. <em>The European Journal of Women's
				Studies</em> <strong>2</strong>, 379-394.</li>
		<li>Lincoln, Y.S. (1992). <em>Virtual community and invisible colleges. Alterations in faculty scholarly
				networks and professional self-image</em>. Paper presented at the Annual Meeting of the Association for
			the Study of Higher Education, Minneapolis, October 28-November 1. (ERIC Document Reproduction Service No ED
			352 903).</li>
		<li>Wilson, T.D. (1981). On user studies and information needs. <em>J. of Documentation</em>,
			<strong>37</strong>, 3-15.</li>
		<li>Wilson, T.D. (1997). Information behaviour. An interdisciplinary perspective. In P. Vakkari, R. Savolainen
			&amp; B. Dervin (Eds.) <em>Information seeking in context</em>. <em>Proceedings of an international
				conference on research in information needs, seeking and use in differerent contexs 14-16</em>,
			<em>August</em>, <em>1996</em>, <em>Tampere</em>, <em>Finland</em>. London: Taylor Graham.</li>
	</ul>
	<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
		<em>Information as social and intellectual capital in the research career: a gender perspective</em>, by <a
			href="MAILTO:gunilla.wiklund@infcult.umu.se">Gunilla Wiklund</a><br>
		Location: http://InformationR.net/ir/4-2/isic/wiklund.html    © the author, 1998.<br>
		Last updated: 9th September 1998</p>

</body>

</html>