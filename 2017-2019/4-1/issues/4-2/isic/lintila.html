<!DOCTYPE html>
<html lang="en">

<head>
    <title>Work-Related Use of an Electronic Network</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="description" content="doctoral workshop paper, Information Seeking in Context">
    <meta name="keywords" content="electronic network, information use, intellectual work, ">
    <meta name="resource-type" content="document">
    <meta name="distribution" content="global">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <h1>Work-Related Use of an Electronic Network</h1>
    <h3>Leena Lintilä</h3>
    <p>Department of Information Studies<br>
        University of Tampere<br>
        <em>lileli@uta.fi</em></p>

    <h2>1. Theoretical Issues</h2>
    <p>The aim of my thesis is to explore the use of an electronic network for information seeking and communication in
        intellectual work. The study approaches this question from two theoretical point of views. The first and
        foremost point of view focuses on the users of an electronic network and the ways in which this use supports
        their work. In addition, this study attempts to map the most important contextual factors which are assumed to
        influence the utilization of the network. These contextual factors are categorized as system factors,
        professional and personal factors and institutional factors. System factors stand for the characteristics of the
        information channel. Professional and personal factors include the basic nature and tasks of a profession and
        other individual traits, such as seemingly irrational preferences. Institutional factors are in this case
        limited to the information culture of an organization. The information culture is further divided into practical
        (technology, equipment), strategical (rules, information policies) and social (different conventions and
        traditions, personal relationships, status thinking, etc.) parts. (<a href="#abe">Abels, <em>et al.,</em>
            1996</a>)</p>
    <p>The background philosophy of this research is mainly teleological. Most information seeking is assumed to take
        place because the user wants to accomplish something. He or she is assumed to act rather rationally in an
        information environment composed of several possibilities between which to choose. However, the reasons from
        which choices stem are not always strictly rational or even clear. Many choices involve more than one contextual
        factor. This is why one of the objectives of this study is to weigh the impact of different factors and their
        relative importance.</p>
    <p>The presuppositions of this research assume a delicate two-way relationship between the context and the use. The
        study attempts to find out whether the use of an electronic network is different in different contexts, as well
        as how this use changes the context itself. The former is a typical approach when researching the use of various
        information sources. For example, it is popular to explore what kind of information seeking is typical of
        certain professions. (<a href="#mck">McKinnon &amp; Bruns, 1992</a>) Meanwhile, the latter point is included in
        the research because of the special nature and relative novelty of network communication. The role which
        electronic networks play in organizations is still in transition and thus the impact of a network is not
        necessarily as obvious or as predictable as the effect of older sources and channels. Plausible changes that
        such a network might cause include a different pacing of work, a new attitude towards organizational
        communication, the lessened importance of other information channels, and so on.</p>
    <p>The second theoretical point of view of this thesis deals with the role of an electronic network in the
        information culture of an organization. This frame of reference is mainly derived from information management
        research: an electronic network is seen as an information resource. (<a href="#mcg">McGee &amp; Prusak,
            1993</a>.) This can also be considered as a more extensive exploration of institutional factors. However,
        the importance of an individual is never forgotten. One of the objectives of this study is to uncover the
        relationship between individual information seeking and corporate information resources. The theoretical
        challenge of this approach is to look at the strategical aspects of information culture and their real
        consequences both through the eyes of those who are responsible for creating and maintaining those strategies
        and those who mainly experience their impact in their everyday work.</p>
    <p>The concept &quot;information culture&quot; has been influenced by the research of organizational culture. In
        this study, information culture stands for systematic and commonly accepted ways to act regards to information
        in an organization. It is a group of agreements about information and communication, whether these are written
        or unwritten, conscious or unconscious. Its function is two-fold: first, in the short run, it enables the daily
        continuity of organizational activies. In the long run, it keeps the organization together by carrying the
        values of the whole organizational culture, constantly reproducing and upholding it. A common information
        culture makes regular exchanges between the members of an organization easier and faster. New members are taught
        to follow the ways of the prevailing culture. The impact of information culture can be seen in elements which
        are typical of all culture, such as metaphors, humour and play, values, beliefs, rituals, heroes, informal
        communication networks, myths, stories and authority. While information culture directly influences the actions
        of individuals, it also forms the frame of reference inside which they interpret their actions regarding
        information. Thus information culture encompasses both ways to act and think. That is why individuals within one
        information culture are unlikely to notice its whole impact on their behavior. For example, they might notice
        outer pressures only when they have to change their information seeking strategy due to them. (<a
            href="#kre">Kreps, 1986</a>, 133- 157; <a href="#bol">Bolman &amp; Deal, 1987</a>, 148-166)</p>
    <h2>2. Proposed Work Plan</h2>
    <p>The empirical part of the study was launched in 1998. It is a case study inside one organization. The
        organization is a Finnish metallurgical combination, Outokumpu, and its data network, Outonet. The Outokumpu
        Group is rather large (it had over 13 000 employees in 1997) and it is organized into four sub-groups called
        business areas. The business areas consist of several semi-independent units which are located both in Finland
        and abroad.</p>
    <p>The four business areas are:</p>
    <ul>
        <li><strong>Base Metals.</strong> Outokumpu's base metals production involves the mining, smelting and reifining
            of copper, nickel and zinc. Mining operations are carried out in several foreign countries, while
            metallurgical plants are located in Finland.</li>
        <li><strong>Stainless Steel.</strong> The stainless steel chain encompasses the production of ferrochrome,
            rolled stainless steel and stainless steel tubes. The most important units of this chain are located in the
            Kemi-Tornio area in northern Finland.</li>
        <li><strong>Copper Products.</strong> This business area produces wrought copper and copper alloy products. The
            production plants are located in several European countries, in the USA and in the future in Asia too.</li>
        <li><strong>Technology.</strong> The technology products marketed by Outokumpu are plants and processes for the
            mining and metallurgical industry, machines, equipment and engineering services and project management.<br>
            (Outokumpu Annual Report, 1997, 2)</li>
    </ul>
    <p>The technical infrastructure of the network used in the Outokumpu Group is called Outonet. The following
        applications are the most important ones:</p>
    <ol>
        <li>
            <p>E-mail. A generic means of communication, it is used both inside and outside the organization.</p>
        </li>
        <li>
            <p>Operative, unit-specific raporting and monitoring systems. These systems deal with raw data in the form
                of constantly changing statistics, such as production or sales figures.</p>
        </li>
        <li>
            <p>Databases and bulletin boards (unit-specific databases, phone books, news and other current information,
                such as open jobs).</p>
        </li>
        <li>
            <p>Connection to the Internet (WWW).</p>
        </li>
    </ol>
    <p>The use of applications varies both according to units and jobs. For example, the law department of the Group has
        its own database which the employees of other units cannot access. On the other hand, the access to the Internet
        is decided individually for each employee. (<a href="#cor">Corporate information management strategy, 1997</a>)
    </p>
    <p>It is presumed that Outonet thus offers a means for standardized communication and information- sharing
        throughout all the units in the Group while it also serves the varying needs of different units. This is why the
        theoretical challenges described in the first chapter appear very real in the Group, surfacing as questions
        like, Does the network make a difference in employees' daily work? How is the net used and how is its utility
        perceived in different parts of the Group? How well does information managers' vision understand users' reality?
    </p>
    <p>Answering these questions naturally requires scrutinizing the nature of the unit in question, the information
        culture prevalent in the unit, the characteristics of an individual's job, information seeking and communication
        (<a href="#min">Mintzberg, 1980</a>; <a href="#sti">Stibic 1982</a>) and the special features of the network.
        For example, in order to avoid an overtly simplistic view of the network, its properties are reviewed by users
        from their own point of view, as well as the place of the network among other information channels. (<a
            href="#nas">Nass &amp; Mason, 1990</a>)</p>
    <p>The empirical data for this research has been gathered by conducting more than thirty interviews, most of which
        have been completed during the spring. It is possible that this data gathering will be complemented by e-mail
        surveys in the autumn. At this point these surveys seem necessary. Many interviewees have spontanely pointed out
        facts which other interviewees have not brought up although they might have commented on them if directly asked.
        Thus surveys would fill these gaps in the data, for example, by bringing more background information on the use
        of other information channels such as libraries and journals. This would naturally lessen the probability of
        making false assumptions in the analysis.</p>
    <p>Some problems have also been caused by the differences between interviewees, especially by the different
        characteristics of their jobs. Although this was expected before interviews and belongs to the problems being
        addressed, it has caused some difficulties in data gathering. Since all interviewees do not need or use all the
        services of Outonet, many have talked about the network from their own point of view only. This has made the
        structures and contents of individual interviews rather heterogenous.</p>
    <p>Another interesting data source consists of written documents. For example, these include the corporate
        information management strategy and the telecommunications strategy (whose use is somewhat restricted because of
        confidentiality), the annual report and various newsletters and magazines published in the Group. The magazines
        I have obtained contain articles both about Outonet and more general issues related to communication and
        information. The analysis of these articles should show how communication and information management are seen
        within different business areas and how their importance is being conveyed to employees.</p>
    <p>It is expected that the final analysis will answer the following questions:</p>
    <ol>
        <li>How is this particular network utilized in knowledge work; how can it and its services support such work and
            especially information seeking and communication? How does the network relate to other information seeking
            and communication methods?</li>
        <li>What is the exact nature of the two-way relationship between work and the network: how do they change each
            other?</li>
        <li>What part does the intranet play in the information culture of the Outokumpu Group and its many sub-groups ?
            How does its role vary in different units?</li>
        <li>How does the role of electronic networks in the information culture affect the users? How aware is an
            individual of the information policies of the organization?</li>
    </ol>
    <h2>3. Progress To Date</h2>
    <p>The study began with the exploration of relevant theories, earlier literature and a minor preliminary study on a
        related issue, the use of laptop computers by managers (<a href="#lin">Lintilä, 1997</a>). The interviews of the
        actual case study have been conducted during the spring in Finnish units of Outokumpu located in Espoo, Tornio,
        Kokkola and Pori.</p>
    <p>Seven interviews has been made in Pori which belongs to Copper Products. This is the most international business
        area in the Group. That is why this branch has been the first and most aggressive one to use advanced technology
        for communication and information-seeking. Tornio, where six people were interviewed, represents Stainless
        Steel. The employees interviewed held very different jobs (e. g. management, information service, sales,
        computer department). Kokkola belongs to Base Metals. Only two persons were interviewed there, so it is probable
        that in the final analysis Kokkola is either dropped or more people will be interviewed there.</p>
    <p>The 15 interviewees in Espoo can be divided into two main categories: 1.) the corporate management and other
        similar specialist jobs, and 2.) the employees of the Technology business area. The former group includes the
        information management which has had the main responsibility for developing Outonet. Other special departments
        include law, environmental issues and education.</p>
    <p>At the moment the transcription and analysis of interviews has just begun. The following thoughts are preliminary
        observations gathered during the interviews.</p>
    <p>The employees of Outokumpu recognize the influence of information culture to some extent. This is especially true
        of those employees who deal with more than one business area. The remarkable differences between business areas
        are due to the disintegration which was encouraged in Outokumpu in the early 90's. This is reflected in general
        activities but also in information management. Each business area and even separate units used to have their own
        information systems and networks. Outonet was intented to remove this incompability barrier. In addition, the
        structure of the Group influences information culture otherwise, too. Interviewees frequently mentioned that the
        large size of the organization makes information gathering difficult, since it is often hard to know where the
        needed information or person is located.</p>
    <p>Other characteristics typical of the Group are conservativism and strong loyality between employees and the
        Group, which causes the low turnover of employees. This is viewed both positively and negatively. For example,
        many people in Tornio praised their community spirit, &quot;Outokumpu values&quot; and open communication. On
        the other hand, a director of finance in Espoo mentioned that this characteristic hinders the flow of fresh
        ideas. Aged personnel can also have more difficulties in adapting to new technology.</p>
    <p>The analysis of information cultures in the scrutinized units is still rather sketchy. At this point there exists
        a few scattered observations mainly on how the utility and purpose of Outonet is understood in different units.
        In Tornio (Stainless Steel), Outonet is strongly understood as an operative system which supports the practical
        activities of the unit by several raporting systems (sales, production, etc). An illustrative example is the
        selection of interviewees. When I arrived in Tornio, my prearranged meetings with managers were cancelled and I
        was asked to interview employees with more practical jobs. Managers themselves thought they were not the real
        users of Outonet. They also showed some kind of prejudice and skepticism against the corporate information
        management located in Espoo; one manager even regarded it as a waste of resources.</p>
    <p>Meanwhile, Outonet was regarded more as an information-seeking and communication tool in the corporate management
        in Espoo as well as in Pori (Copper Products). Access to different databases was considered more important than
        managing raw data. As expected, the least prejudiced attitudes were found in Pori, assumedly because of their
        wide international connections. This unit was very active and determined to develop its communication methods.
    </p>
    <p>A surprising fact was how negatively Outonet was viewed within the Technology business area in Espoo. For
        example, the only persons who complained about the cost of Outonet were managers in this area. They saw Outonet
        as the decision of the highest management: they could not have turned it down although they had not found it as
        necessary as other business areas had. This is partly understandable since the activities and products of
        Technology are quite different from the other areas. On the other hand, attitudes towards the Internet seem
        unreasonably harsh: some units demand that the employees who apply for an account are evaluated by the executive
        director himself. The director in question saw no problems with this, while employees who were lower in the
        hierarchy wished for more relaxed policies.</p>
    <p>Which characteristics of an individual information seeking are affected by information culture lacks a more
        definite analysis. A preliminary notion of mine is that the most concrete influences can be seen when some
        opportunities exist or lack (for example, the lack of the Internet connections), Observing more delicate
        influences will be started when all the interviews have been transcribed.</p>
    <p>At this point, this study will be continued with the evaluation of relevant literature and a more definite
        analysis of interviews. When analysing the interviews in detail, I will concentrate on the following issues:</p>
    <ol>
        <li>What symbolism and metaphors do different units associate with information seeking and Outonet? How do they
            vary in jobs?</li>
        <li>What values can be seen in the Group and in different units?</li>
        <li>What kind of problems appear when the network is used for information seeking and communication? How are
            they handled? For example, technical problems are usually fixed fast, while strategical issues can be around
            for years if their use is even moderately succesful.</li>
        <li>What kind of problems do different employees and units notice first?</li>
        <li>How have different requirements in units been taken into account when Outonet has been planned?</li>
        <li>What rituals of the organization culture have to do with Outonet?</li>
        <li>Whose opinions weigh a lot? How big a difference do the managers' attitudes make?</li>
        <li>What kind of stories or anecdotes deal with the network?</li>
        <li>What points do different units emphasize when they inform employees about Outonet?</li>
        <li>What kind of new ideas and wishes do different units have concerning the network?</li>
        <li>How does the hierarchy influence communication? How is Outonet used to circumvent hierarchical structures?
        </li>
    </ol>
    <h2 id="references">References</h2>
    <ul>
        <li><a id="abe"></a>Abels, Eileen G., Liebscher Peter &amp; Denman, Daniel W. (1996). Factors that influence the
            use of electronic networks by science and engineering faculty at small institutions. Part I. Queries.
            <em>Journal of the American Society for Information Science</em>, <strong>2</strong>, 146-158.</li>
        <li><a id="bol"></a>Bolman, Lee G. &amp; Deal, Terrence E. (1987). <em>Modern approaches to understanding and
                managing organizations.</em> San Francisco: Jossey-Bass Publishers.</li>
        <li><a id="cor"></a><em>Corporate information management strategy (1997)</em>. An unpublished hand-out, written
            by the Information Management department of the Outokumpu Group.</li>
        <li><a id="kre"></a>Kreps, Gary L.(1986). <em>Organizational communication: theory and practice.</em> New York:
            Longman.</li>
        <li><a id="lin"></a>Lintilä, Leena (1997).<em>Kannettava tietokone johtamisen apuvälineenä: Loppuraportti
                LifeBook-käyttötutkimuksesta. [How Managers Use Laptop Computers: The Conclusive Report of the LifeBook
                Usage Study.]</em> Working Papers 3/1997. Tampere: University of Tampere, Information Society Research
            Centre.</li>
        <li><a id="mcg"></a>McGee, James V. &amp; Prusak, Laurence (1993). <em>Managing information strategically. New
                York: John Wiley &amp; Sons.</em></li>
        <li><a id="mck"></a>McKinnon, Sharon M. &amp; Bruns, William J., Jr. (1992). <em>The information mosaic.</em>
            Boston: Harvard Business School Press.</li>
        <li><a id="min"></a>Mintzberg, Henry (1980). <em>The nature of managerial work.</em> Englewood Cliffs:
            Prentice-Hall.</li>
        <li><a id="nas"></a>Nass, Clifford &amp; Mason, Laurie (1996). On the study of technology and task: a
            variable-based approach. In <em>Organizations and communication technology.</em> Edited by Janet Fulk&amp;
            Charles Steinfield. Newbury Park: Sage Publications, 46-67.</li>
        <li><a id="out"></a><em>Outokumpu Annual Report</em> (1997). Espoo: Outokumpu.</li>
        <li><a id="sti"></a>Stibic, V. (1982). <em>Tools of the mind: techniques and methods for intellectual work.</em>
            Amsterdam: North-Holland</li>
    </ul>
    <p>.</p>
    <p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
        <em>Work-Related Use of an Electronic Network</em> , by <a href="MAILTO:lileli@uta.fi">Leena Lintilä.</a><br>
        Location: http://InformationR.net/ir/4-2/isic/lintila.html   © the author, 1998.<br>
        Last updated: 9th September 1998</p>

</body>

</html>