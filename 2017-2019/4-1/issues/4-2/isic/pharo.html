<!DOCTYPE html>
<html lang="en">

<head>
	<title>Solving problems on the World Wide Web</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords" content="problem solving, World Wide Web, information seeking">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>Solving problems on the World Wide Web</h1>
	<h3>Nils Pharo</h3>
	<p>Faculty of Journalism, Library and Information Science<br>
		Oslo College<br>
		Norway<br>
		<em>nils.pharo@jbi.hioslo.no</em></p>
	<h2>Introduction</h2>
	<p>The Web is a digital environment that exists as a parallel universe to the physical. To exploit the Web users may
		be required to choose between a myriad of different paths. It could thus be argued that the time people spend on
		the Web is more active and challenging than the time spent engaged in media like television or books. The Web
		has only existed for 7 years and it is constantly expanding, for this reason both its authors and readers
		struggle to create fitting mental models of it. Several factors influence this model building, these include the
		complexity of the problem; at what stage of the process he is in; his educational background and previous
		experience; the psychological and emotional constraints put upon him by himself and his surroundings; and the
		information sources and channels available. These are factors that are often addressed in the information
		seeking literature. Additionally there are constraints set during actual Web interaction. A fully computerised
		medium strongly depends on the possibilities to smoothly present its preconceived properties. It is a great
		diversity in the computer equipment available to the users as well as the capacity of their Internet connection.
	</p>
	<h2>The research problems</h2>
	<p>Users' problem solving with the Web are dependent on some information seeking factors and some information
		searching factors where the latter is a subset of the former. &quot;Web interaction&quot; is here preferred used
		to Web &quot;information searching&quot;. Originally it was thought that the focus should be on the
		interrelationships between high-level information seeking factors and the low-level Web interaction. A review of
		the literature reveals that very few studies have been performed that focus on actual use of the Web. It was
		therefore decided that the project was to be concentrated on this.</p>
	<p>Web interaction should be viewed in the light of the problem at hand. Problems are of different complexity and
		they are solvable with different means. Since individuals' knowledge structures are dynamic (see <a
			href="#ref">Ingwersen 1992</a>) the problem may change constantly, not only as a results of information
		retrieval. A problem may in fact &quot;disappear&quot;, not necessarily because it is solved, but because during
		the problem solving process the individual discovers, as the result of information retrieval or other actions –
		be they internal or external – that the problem needs to be replaced by a new and more fitting one.
		Metaphorically one may say that a change somewhere in the user's knowledge structure indirectly, or by a
		&quot;chain reaction&quot;, <em>restructures</em> the knowledge structure and thus causes him to see things from
		a new point of view. In other instances the individual may discover that the problem is unsolvable and choose to
		spend his time in a more fertile way.</p>
	<p>It is further thought that a problem can be divided into several tasks, which may be of different complexity.
		Task complexity depends on several properties, including difficulty; domain; duration; importance; and
		repetitiveness. Routine tasks are often performed in no time without the need of any information seeking
		behaviour. The more complex task may on the other hand be divided into several subtasks. The results from
		Byström &amp; Järvelin's study (<a href="#ref">1995</a>) indicates that the more complex a task gets the more
		necessary it is to enact in information seeking, they say &quot;as the tasks grow more complicated, the
		information needs also become more complicated.&quot; (p. 208) From this one should think that individuals only
		engage in Web interaction to perform tasks above a certain level of complexity.</p>
	<p>The subject domain to which a problem belongs is interesting because the extent – both quantitatively and
		qualitatively – to which different domains are presented on the Web reflects the development of the Web itself.
		The harder sciences (like computer science, mathematics) started to publish on the Web early and it is still a
		bias in Academia's Web publishing.</p>
	<p>When individuals decide to engage in Web interaction I believe that it is possible to identify distinct tasks
		with identifiable beginnings and ends. A Web session may consist of one or several task performances, and these
		performances may in turn belong to different problem solving processes or they may all be performed to solve a
		single problem. Examples of task may rank from the simple &quot;finding out what is the capital of Ireland&quot;
		to the complex, e.g. &quot;purchase the newest album by Madonna, but make sure to get it at a good price and
		have it sent here by the end of the next week&quot;. Some tasks may originate during interaction. Meta-problems
		exemplify this, e.g. a user wishes to use an IR tool, but has problems understanding how it works. The first
		task to be accomplished is &quot;find out how to use IR tool&quot;.</p>
	<p>By Web interaction I mean activities involved in accessing and reading pages on the Web. The factors mentioned
		above obviously play a role also during Web interaction, but here factors directly influenced by the interaction
		also come into play. These are partly system and partly user dependent. System dependent interaction factors
		include the effect of functionalities embedded in the client program (the &quot;browser&quot;) as well as
		usability and interface issues. The performance of IR tools is another important factor. Additionally it is
		clear that the individual authors' ability to use the hypertext format in text structuring is important. In her
		master thesis Elisabeth Grylling (<a href="#ref">1997</a>) proposes the following criteria to evaluate the
		quality of Web resources: authority; accuracy; objectivity; actuality; topicality; coverage; linking; stability;
		context dependency; and accessibility. Although the criteria originally was developed to make rules for
		including Web resources in a database I believe they are usable as descriptors of how individuals verify the
		content of Web resources. The degree to which users are critical in evaluating Web resources are of course also
		dependent on their previous Web experience.</p>
	<p>An important factor that influences interaction is the ability of the active Web page to change or restructure
		the user's knowledge structures in such a way that it helps him getting closer to solving his problem. In other
		words, if the page contains the needed information the user may choose to end the interaction or he may start
		working with another problem or task. It is hypothesised that such changes in interaction are observable.</p>
	<p>Different Web resources may also influence how interaction takes place. Some resources have a content and form
		that is familiar to the user from the physical world, e.g. monographs and journals. Users with long experience
		probably have used a larger variety of resources and feel familiar with them, but it may also be that the
		specific groups of users publish and reads specific kinds of Web resources.</p>
	<p>I believe it is fruitful to look at both the &quot;hows&quot; and the &quot;whys&quot; of Web interaction. How a
		person physically act when interacting depends on the system's ability to fit his cognitive actions, i.e. his
		thoughts. Since the hypertext format is based on a notion of associative access it is therefore very interesting
		to try to classify navigation by both functional and cognitive facets. In the section on methods a possible way
		of doing this is examined.</p>
	<p>The Web is one out of many possible information sources available to problem solvers. In certain circumstances
		problem solvers may choose to use the Web as their information source, this, among other things, depends on the
		kind of problem they have and the tasks that need to be accomplished to solve the problem. The research problems
		I seek to answer are the following:</p>
	<ol>
		<li>Is it possible to identify patterns of cognitive and physical nature in Web interaction?</li>
		<li>If it is possible to identify such Web interaction patterns, is there any correlation between the nature of
			these interaction patterns and particular kinds of Web resources?</li>
	</ol>
	<p>In this section I present the collection methods hitherto used, and the methods I plan to use in the data
		analysis.</p>
	<h2>Data collection and analysis</h2>
	<p>LIS students on the brink of ending their education have been chosen as objects of study. They constitute an
		actual group of problem solvers; they need to write a final thesis to have their diploma. Although the students
		do not have a long experience in use of IR tools, their training in this area is up-to-date. Additionally the
		students have quite a long experience in Web use; the Web is used actively in several courses followed by the
		students and they have had Web access for three years. For a second study I have planned to use graduate
		students in LIS who are working on their master thesis.</p>
	<p>I believe that using experts (<a href="#ref">Ingwersen, 1992</a> p. 141) as my objects of study may help to
		reveal whether there are specific properties embedded in their IR knowledge that are of particular help during
		Web interaction. On the other side it could be the exact opposite, perhaps their IR knowledge in some ways
		actually hinder them from efficiently exploiting the Web. This is not a main research question in my project,
		but it may be a potential interesting spin off effect to follow up.</p>
	<h3>Data collection</h3>
	<p>To collect my data I made agreements with the students to contact me when they want to use the Web as part of
		their thesis work. I put no constraints on the number of students working together, i.e. if two students wanted
		to conduct a Web session together they were allowed to do so.</p>
	<p>I have equipped one computer with a GrandArt video converter, which is a little black box that converts computer
		screen signals to video format. The converter also has a microphone connected to it, which makes it possible to
		record sound data simultaneously. The only external equipment needed is a VCR to record the data.</p>
	<p>The computer is connected to the faculty's local area network and the students may log in to their own accounts
		and thus use a browser, which to a certain degree may be individually configured. The students' default browser
		is set to be Netscape, version 3.03.</p>
	<p>In order to learn more about the problem at hand and the users' intention with the forthcoming session an
		unstructured open-ended interview is conducted in front of each Web session. Here the students are asked to tell
		about what they have done since the last session; how they feel about the progress of their work; why they want
		to use the Web today and similar questions.</p>
	<p>A second short and unstructured open-ended interview is performed after the session. In this interview the
		students are asked how they felt about the session and whether they could have used other sources to answer
		their questions/accomplish their task(s). The follow-up interview is also used to clarify whether the user was
		able to do what he intended. It is also possible for the observer to ask questions that may help him in
		clarifying the intentions behind particular interaction activities.</p>
	<p>The students are asked to <em>talk-aloud</em> while interacting with the Web. It is an important distinction
		between talk-aloud and <em>think</em>-aloud, since the latter method demands that the user formulate thoughts
		that are not verbal, but e.g. visual. When a user is asked to talk aloud he is only to voice those thoughts that
		are already encoded in verbal form (<a href="#ref">Ericsson &amp; Simon 1996</a>). The problem with the
		think-aloud technique is that it prolongs the sessions since the user is required to encode and utter every
		thought verbally. Talk-aloud protocols on the other hand do not intercept mental processes that are not encoded
		verbally.</p>
	<p>During the session an observer watches the sessions and makes sure the computer and recording equipment work
		satisfactory. The observer is not to engage in any conversation with the user, but he may ask the users to
		&quot;keep talking&quot; to keep the loud-talking process going.</p>
	<h3>Data analysis</h3>
	<p>The model developed by Belkin, Marchetti &amp; Cool (<a href="#ref">1993</a>) has been used to analyse a few
		tasks. This is a four-dimensional model focusing on both functional and cognitive aspects of information
		strategies. The dimensions were <em>method</em> (scan|search), <em>goal</em> (learn|select), <em>mode</em>
		(recognise|specify) and <em>resource</em> (information|meta-information). The analyses performed revealed
		problems related to the different dimensions. Some of these problems are directly related to the hypertext
		format and some could be of a more general nature. Below one task is analysed.</p>
	<p>The example shows a task performed by users who are apparently quite uncertain about the topic they try to find
		more information about. They look for information on &quot;Index translatonium&quot;, which appears to be a
		database on translation. The task starts with an intermezzo where the users try to find an entry for Alta Vista
		in the bookmark file (ISS 8), without success. They then inspect the page currently in the browser and finds and
		select the link directly to Alta Vista, this is also classified as ISS 8 since they look for the link in a list
		of links to various IR tools.</p>
	<p>In Alta Vista the users enter the query &quot;Index translatonium&quot; (ISS 16), thereafter they examine and
		follows the first entry in the results list (ISS 8). Thereafter they read the page to learn more about what
		order to learn more about what &quot;index translatonium&quot; is (ISS 3). The users look through the page, they
		are obviously uncertain about how to proceed (&quot;What's this? [pause] only information about?&quot; [a CD-ROM
		database]), they end up choosing a random link (&quot;but can't you just push on of those [buttons] and see what
		happens?&quot;), this is an example of ISS 1. The next two pages are accessed similarly (ISS 1), then they find
		and follow an explicit link to &quot;Index translatonium&quot; (7), but this turns up to be a link to the page
		they originally found. They have now obviously learnt that it is a database, and they do a half-hearted attempt
		at finding a way to search it by following a link saying &quot;New search&quot; (ISS 7). This leads them to an
		irrelevant page and they continue trying to solve another task.</p>
	<p><img src="Image11.gif" alt="Figure 1"><br>
		<strong>Figure 1 Graphical display example</strong></p>
	<p>For the purpose of analysing Web session it is felt that the mode dimension is the most difficult to value. It
		does characterise an important aspect of IR interaction – <em>uncertainty</em>, if the user knows what he wants
		to find, i.e. he can <em>specify</em> it, his uncertainty is low. Similarly he or she is probably more uncertain
		when he/she does not exactly know what to look for. It is perhaps easier to identify users' level of uncertainty
		than the recognise/specify mode they are in during searching.</p>
	<p>The limitation of method to &quot;scanning&quot; and &quot;searching&quot; is problematic when it comes to
		dealing with following links. Following links can be seen as a parallel to picking a book from a shelf, thus it
		has been characterised as scanning. This, however, makes it impossible to distinguish the reading or overlooking
		of a page from the selection of a link to access a new page. These are clearly different kinds of interaction
		methods and it is probably wise to treat them differently.</p>
	<p>This raises two further questions: Should we distinguish between the following of different kinds of links? And
		how should we treat predefined searches implemented as links? These questions illuminate some features that
		characterise Web authoring and thus affect the ways people are able to interact with the Web.</p>
	<p>When analysing the sessions it became apparent that the distinction between the two resource-types wasn't as
		clear as it was thought to be. In a fully digitised environment like the Web every page or resource is in
		principal only a mouse-click away, while in the &quot;real&quot; world IR systems and documents are physically
		separated. Belkin et al. (<a href="#ref">1995</a> p. 381) exemplifies the use of ISS 5 by a user scanning the
		shelves in a library, i.e. the resource is classified as information. Web IR tools include direct links to the
		resources, the results lists could thus be compared to a shelf of books where each document can be picked out,
		looked at and returned. It should not be controversial to claim that when a user inputs a query in a search
		engine the resource he uses is to be classified as meta-information. How to treat the output of a search is,
		however, not unproblematic. Should it be focused on the entries' role as resource surrogates, or should the
		entries be treated like anchors to the &quot;real thing&quot;, i.e. &quot;similar&quot; to the back of books on
		a shelf? Since the surrogates that are presented in the results lists are generated from a database and because
		this database will never be fully updated and thus in theory do not represent the current content of the page it
		seems natural to treat them as meta-information. Manually generated subject indices like, for instance, Yahoo!
		do not cause the same problem. As long as the subject indices contain intellectually created descriptions of the
		entries they should be treated like meta-information. The same is usually true for those privately generated
		&quot;favourite&quot; pages, which thus are defined as meta-information.</p>
	<p>Possible interdependencies between dimensions are not treated here.</p>
	<h2>Work plan and current status</h2>
	<p>The project is scheduled to run over a period of four years, 75 % of the time is to be devoted to the project,
		i.e. 36 months. The following time schedule is approximate and some parts of the work will undoubtedly be
		overlapped:</p>
	<table>
		<tbody>
			<tr>
				<td>Literature and methods studies</td>
				<td>6 months</td>
			</tr>
			<tr>
				<td>Development of methods, pre-surveys, method evaluation</td>
				<td>4 months</td>
			</tr>
			<tr>
				<td>Survey and interviews</td>
				<td>8 months</td>
			</tr>
			<tr>
				<td>Handling of results, writing down results</td>
				<td>4 months</td>
			</tr>
			<tr>
				<td>Analysis of results</td>
				<td>3 months</td>
			</tr>
			<tr>
				<td>Conclusion</td>
				<td>3 months</td>
			</tr>
			<tr>
				<td>Thesis production and other forms of documentation</td>
				<td>8 months</td>
			</tr>
			<tr>
				<th>Sum</th>
				<th>36 moths</th>
			</tr>
		</tbody>
	</table>
	<p>Throughout the period I will also need to follow courses and seminar to claim the necessary credit points from
		Tampere University.</p>
	<p>At this point I have been able to claim 31 out of the 40 credit units necessary. To no surprise the different
		points on the work plan to a certain degree merge. I have performed literature studies in the fields of IR,
		information seeking, hypertext, and related areas. The collection methods are developed and I have collected
		approximately 14 hours of data. I also plan to collect some more data. I have started to transcribe the data and
		done a pre-analysis of a small amount of data to test a method of analysis.</p>
	<h2><a id="ref"></a>References</h2>
	<ul>
		<li>Belkin, N. J, Marchetti, P. G. &amp; Cool, C. (1993). &quot;BRAQUE: design for an interface to support user
			interaction in information retrieval&quot;. <em>Information processing &amp; management</em>,
			<strong>29</strong>, 325-344</li>
		<li>Belkin, Nicholas J. et al. (1995). &quot;Cases, scripts, and information-seeking strategies – on the design
			of interactive information-retrieval systems&quot;. <em>Expert Systems with Applications</em>,
			<strong>9</strong>, 379-395.</li>
		<li>Byström, Katriina &amp; Järvelin, Kalervo (1995). &quot;Task complexity affects information seeking and
			use&quot;. <em>Information Processing and Management</em>, <strong>31</strong>, 191-213.</li>
		<li>Ericsson, K. Anders &amp; Simon, Herbert A. (1996). <em>Protocol analysis: verbal reports as data</em>. –
			Cambridge, Mass.: MIT Press.</li>
		<li>Ingwersen, Peter (1992). <em>Information retrieval interaction</em>. London: Taylor Graham.</li>
		<li>Grylling, Elisabeth (1997). <em>Organisering av World Wide Web-referanser til undervisningsformål ved
				Høgskolen i Oslo</em>. Oslo: Høgskolen i Oslo.</li>
	</ul>
	<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
		<em>Solving problems on the World Wide Web</em>, by [Nils Pharo](MAILTO: Nils.Pharo@jbi.hioslo.no)<br>
		Location: http://InformationR.net/ir/4-2/isic/.html   © the author, 1998.<br>
		Last updated: 15th September 1998</p>

</body>

</html>