<!DOCTYPE html>
<html lang="en">

<head>
	<title>Paranormal information seeking in everyday life -- part II: the paranormal in information action</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords" content="paranormal phenomena, information seeking">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>Paranormal information
		seeking in everyday life: the paranormal in information action</h1>
	<h3>Jarkko Kari</h3>
	<p>Department of Information Studies<br>
		University of Tampere<br>
		<em>csjakar@uta.fi</em></p>
	<h2>Introduction</h2>
	<p>The purpose of this licentiate's study is to examine information action (needs, seeking and use) in the context
		of paranormal information (information about the paranormal or information supposedly acquired by paranormal
		means) in a particular situation, as experienced by people who are interested in the supernatural. The central
		aspects of the research are its focus on the individual, situation, meanings and process, which should amount to
		a holistic picture of information action. The study is primarily theory-driven, and its conceptual framework is
		grounded upon Dervin's sense-making theory. The basic elements, which are situation, gap (need) and use, are
		enhanced by two new elements, construction (seeking) and barrier. This is done in an effort to develop a theory
		of information action that is more concrete and unambiguous than the original sense-making theory. The main
		objective of the study at hand, however, is to analyze the content of information action, rather than its
		structure. The research is qualitative and somewhat exploratory by its nature. The data is constituted by 20
		micro-moment time-line interviews which were conducted in Finland in 1995. Right now, the material is being
		coded for analysis.</p>
	<h2>Points of departure</h2>
	<p>During the last couple of decades, it has been noted that interest in paranormal or supernatural phenomena has
		grown substantially in America and Europe (<a href="#ref">Heino, 1994</a>). Today interest and belief in
		paranormal phenomena is remarkably common (<a href="#ref2">Parsons, 1992</a>; <a href="#ref2">Sparks <cite>et
				al.</cite> , 1995</a>) -- perhaps even more common than ever before (see <a href="#ref">Griffon,
			1991</a>). The figures on the share of believers in the paranormal vary, but on the basis of several
		surveys, it can be said that well over half of population believes in the existence of paranormal phenomena (<a
			href="#ref">Blackmore, 1990</a>). Paranormal beliefs are universal, which suggests that they are very
		important to the human being (<a href="#ref2">Schumaker, 1987)</a>. It would strike one as natural then that
		supernatural experiences and interest in these would tend to arouse in people a need to know more about these
		phenomena.</p>
	<p>I call information relating to paranormal phenomena <cite>paranormal information</cite> . Little is known about
		how this information is manifested in information action, that is, in information needs, seeking and use.
		Nevertheless the title of a leader by Tapani Kuningas2 -- &quot;Need for information is growing&quot; (<a
			href="#ref2">Kuningas, 1995</a>) -- tells us something. From the viewpoint of scientific research,
		paranormal information is like a black box: the scientific community is aware of it, but scholars either
		belittle it or they lack the courage to take a look what is inside. This is why hardly anything at all is known
		about the relationship between paranormal phenomena and information action, or, to put it differently, between
		paranormal information and everyday life. As far as I know, my master's thesis, <cite>Paranormal information
			seeking in everyday life -- part I</cite> (<a href="#ref">Kari, 1996</a>), was the first and apparently
		still the only scientific study in the world that examines paranormal information in information action. That
		study merely succeeded in scratching the surface of the phenomena in question, so that there is an obvious need
		for further research.</p>
	<p>My licentiate thesis in preparation is a continuation of my master's thesis. These two studies are meant to
		complement each other. Being a survey, the purpose of my master's thesis was to map the general features of
		needs and seeking of paranormal information. On the other hand, with the help of my licentiate thesis, which is
		based on interview material, I hope to &quot;penetrate the surface&quot; and to discover ways to interpret the
		results of my earlier study.</p>
	<p>The current study looks at information action in the context of paranormal phenomena. In practice, this means
		examining needs, seeking and use of paranormal information in a particular situation, as experienced by people
		who are interested in the supernatural. The features that centrally characterize my study are its focus on the
		individual, situation, and process, which, taken together, should amount to a holistic picture of information
		action. Special attention is paid to the meanings that are given to paranormal information in everyday action.
		This is facilitated by taking Brenda Dervin's <cite>sense-making</cite> <cite>theory</cite> (see e.g. <a
			href="#ref">Dervin, 1992</a>) as a metatheory or a background theory for my study.</p>
	<h2>Outline of a theory</h2>
	<p>Because the study at hand is primarily theory-driven, the conceptual framework that I am using is to be
		introduced properly. The theoretical basis has three levels: metatheory, formal theory and unit theory. Due to
		space restrictions, I will only present the first two levels briefly.</p>
	<h3>Metatheory</h3>
	<p><strong>Sense-making theory.</strong> The main part of the metatheory used in the study is constituted by the
		sense-making theory. In essence, this theory generally deals with how an individual makes sense of his
		environment in a given situation, and specifically how seeking information serves bridging &quot;gaps&quot; or
		&quot;discontinuities&quot; that the individual has perceived in reality. In the case of my own study, this is a
		question of how people make sense of their situation, their need for paranormal information, their seeking
		and/or finding this information, as well as their using of the information.</p>
	<p>I chose the sense-making theory for three reasons. Firstly, the theory enables me to scrutinize the meanings that
		are given to the paranormal, on the one hand, and to information action, on the other, which occupies a
		prominent position in my study. Secondly, the sense-making theory presupposes that information action is not a
		static state, but a dynamic process, which is another important aspect of the current study. Thirdly, the
		approach makes it possible for me to get at a particular, actual situation that a person has gone through.</p>
	<p><strong>Basic concepts.</strong> General background concepts also belong to metatheory. They have little
		analytical value, but knowing them is vital for comprehending the whole study. Three basic concepts worth
		defining here are &quot;paranormal phenomenon&quot;, &quot;paranormal information&quot; and &quot;information
		action&quot;.</p>
	<p>Probably the most typical way to define <cite>paranormal phenomenon</cite> is to regard it as a hypothetical
		phenomenon which contradicts the scientific laws that are taken for laws of nature -- or, more generally -- the
		most fundamental suppositions and principles of science -- or, most generally -- today's scientific conception
		of the world (see <a href="#ref">Alcock, 1981</a>; <a href="#ref">Alcock, 1991</a>; <a href="#ref">Collins &amp;
			Pinch, 1979</a>; <a href="#ref2">Kurtz, 1985</a>), on the one hand, and the expectations of common sense and
		our everyday experiences (<a href="#ref2">Kurtz, 1985</a>; <a href="#ref2">Schumaker, 1987</a>; <a
			href="#ref2">Tobacyk &amp; Milford, 1983</a>), on the other hand. Although the role of the supernatural in
		information action is explored from the angle of those who believe in the paranormal, <cite>my dissertation will
			not take any stand whatsoever on the existence of these phenomena</cite>.</p>
	<p>The concept of <cite>paranormal information</cite> is easy to define once you know what &quot;paranormal
		phenomenon&quot; means: paranormal information is information about paranormal phenomena. However, the term can
		also be used to refer to information that has supposedly been acquired by supernatural means.</p>
	<p><cite>Information action</cite> is a term that comes from Wersig and Windel (1985). It can be defined as action
		that &quot;involves various forms of users' conceptual and physical contacts with information&quot; (<a
			href="#ref">Erdelez, 1997</a>). &quot;Information action&quot; is a broad concept which contains the three
		major stages of an information process: information need, seeking and use (see <a href="#ref2">Vakkari,
			1997</a>).</p>
	<h3>Formal theory</h3>
	<p><strong>Information process.</strong> The analytical core of the sense-making theory is formed by the famous
		triangle of situation-gap-use (Fig. 1) which is an attempt at depicting the information process. Before
		explaining what these concepts or stages mean, I would like to express my dissatisfaction with this triangle.
		The problem with the triangle is that it is too abstract and metaphorical. This in turn leads to the model being
		overly simple and ambiguous. For example, such a central concept as &quot;information seeking&quot; is missing
		altogether.</p>
	<p><img src="figure1.gif" alt="Figure 1"></p>
	<p>Figure 1. The sense-making triangle: situation-gap-use (source: <a href="#ref">Dervin, 1992</a>)</p>
	<p>In the spirit of the sense-making theory, the stage of information seeking could be called
		&quot;construction&quot;. So instead of the <cite>triangle</cite> of situation-gap-use, we now have the
		<cite>square</cite> of situation-gap-<cite>construction</cite> -use or, in more analytical terms, the square of
		situation-need-seeking-use. After closer consideration, however, it seems that &quot;situation&quot; is
		different in quality from the other concepts. &quot;Need&quot;, &quot;seeking&quot; and &quot;use&quot; are
		stages of information action, whereas &quot;situation&quot; is rather the context of this process than a part of
		it. Because I am also interested in problems in information action faced by the individual, I will add
		<cite>barrier</cite> in the middle of the model (see Fig. 2).</p>
	<p><img src="figure2.gif" alt="Figure 2"></p>
	<p>Figure 2. The process of information action: need, seeking and use (interfered with by barrier) in situation.</p>
	<p>Therefore, the main analytical concepts of my study are situation, gap/information need, construction/information
		seeking, information use and barrier. Except for the first and last one, these also stand for the various stages
		of the information process.</p>
	<p><strong>Major stages of the process.</strong> At the general level, Dervin defines <cite>situation</cite> as
		&quot;an epistemological time-space context that an individual would recognize as being meaningfully separate
		from other epistemological contexts&quot; (<a href="#ref">Halpern &amp; Nilan, 1988</a>). In other words,
		situation is a point in space and time (<a href="#ref2">Perttula, 1994</a>) in which the individual constructs
		meanings (<a href="#ref">Dervin, 1983</a>).</p>
	<p>The concept of <cite>gap</cite> means &quot;an unclear aspect of a situation that a person feels the need to
		clarify in order to continue movement in a direction that the individual considers to be constructive or
		desirable&quot; (<a href="#ref">Halpern &amp; Nilan, 1988</a>). In the current study, I consider &quot;gap&quot;
		to be analytically synonymous with &quot;information need&quot;. <cite>Information need</cite> is the
		individual's conception of what information he needs to satisfy a more basic need of his (<a
			href="#ref2">Wilson, 1977</a>; see also <a href="#ref2">Wilson, 1981</a>), that is, to achieve his goal.</p>
	<p>According to an English language dictionary, <cite>construction</cite> is &quot;the creating of something such as
		an idea&quot; or &quot;the way in which people can interpret something that is written, said, or done&quot; (see
		<a href="#ref">Collins, 1987</a>). In sense-making terms, construction signifies the individual's making of
		sense of whatever is puzzling about the situation. The more technical term for &quot;construction&quot; is
		&quot;information seeking&quot;. I define <cite>information seeking</cite> as a purposeful process in which the
		individual attempts to find information through information sources in order to satisfy his information need
		(see <a href="#ref2">Krikelas, 1983</a>; <a href="#ref2">Wilson, 1977</a>). Information seeking includes the
		accidental discovery of information as well.</p>
	<p>If construction stands for building a bridge across the gap, then <cite>use</cite> means &quot;the outcome or
		outcomes of Sense-Making aimed at addressing gaps&quot; (<a href="#ref">Halpern &amp; Nilan, 1988</a>). Thus
		<cite>information use</cite> refers to how the individual applies the acquired information in his (inner or
		outer) action (cf. <a href="#ref2">Tuominen &amp; Savolainen, 1997</a>; <a href="#ref2">Ward, 1983</a>).</p>
	<p><cite>Barrier</cite> in information action can be broadly defined as a factor which the individual perceives as
		hindering his information-related activities. All difficulties that the individual encounters at any stage of
		the process are such barriers.</p>
	<h2 id="research-problem">Research problem</h2>
	<p>My licentiate study has two principal problems or objectives. In their order of importance, these are: 1) to get
		an understanding of information action in the context of paranormal phenomena and paranormal information, and 2)
		to develop a theory of information action as a holistic process. In other words, my current work concentrates on
		the <cite>content</cite> of information action. Examining its <cite>structure</cite> will have to wait until the
		doctoral thesis to come.</p>
	<p>On the ground of the theory and objectives of my study arise the six main research questions along with their
		subquestions:</p>
	<ol>
		<li>
			<p>How and why do people get to know about the paranormal in the first place?</p>
		</li>
		<li>
			<p>In what kind of situations do people need paranormal information?</p>
			<ul>
				<li>what is their Situation Movement State3 in these situations?</li>
				<li>do they consider their situations as perfectly natural or do they perceive some supernatural
					elements in these situations?</li>
				<li>what goals do they have in these situations?</li>
			</ul>
		</li>
		<li>
			<p>What are people's needs for paranormal information like?</p>
			<ul>
				<li>why do they need this information?</li>
				<li>about what topics do they need this information?</li>
				<li>what questions do they have in their mind?</li>
				<li>on what entities do these needs focus?</li>
				<li>on what time do these needs focus?</li>
				<li>what is the primary criterion for seeking this information?</li>
			</ul>
		</li>
		<li>
			<p>How do people seek and/or find paranormal information?</p>
			<ul>
				<li>a) which strategies of information seeking do they use?</li>
				<li>b) what sources of information do they use?
					<ul>
						<li>what sort of sources do they use?</li>
						<li>how do they find these sources?</li>
						<li>why do they use these sources?</li>
					</ul>
				</li>
			</ul>
		</li>
		<li>
			<p>c) what sense do they make of the received information?</p>
			<ul>
				<li>about what topics is the information?</li>
				<li>on what entities does the information focus?</li>
				<li>on what time does the information focus?</li>
				<li>do they get the information by normal or paranormal means?</li>
				<li>to what extent does the information satisfy their information needs?</li>
			</ul>
		</li>
		<li>
			<p>How do people use paranormal information?</p>
			<ul>
				<li>to what purposes do they apply this information?</li>
				<li>what negative effects does this information have?</li>
			</ul>
		</li>
		<li>
			<p>How do people experience barriers to information action?</p>
			<ul>
				<li>what sort of barriers are perceived?</li>
				<li>at which stages of the process do these barriers emerge?</li>
			</ul>
			<h2>Work plan and its execution</h2>
			<p>The study is being conducted at the University of Tampere, Department of Information Studies, Finland. I
				began working on the research project in October 1996, and I expect to bring the study to its conclusion
				by March 1999. Below I will introduce to you the various stages of the research process. These stages
				are not in a strictly linear (chronological) order, but in the order in which they should progress in
				the &quot;ideal&quot; case. In addition, the stages are not distinct, for they do overlap each other.
				Because of the lack of a scholarship, my work advanced quite slowly from October 1996 until July 1997.
			</p>
			<h3>Literature review</h3>
			<p>After reading hundreds of articles and books on supernatural phenomena and paranormal information as well
				as on information needs, seeking and use, I finally finished the literature review in February 1998. The
				review concentrated on the theory basis of my thesis, leaving the presentation of other empirical
				studies until the final report.</p>
			<h3>Work plan</h3>
			<p>At this very moment, you are reading a summary of the original 40-page research proposal that I composed
				in March 1998. That paper described the intended operationalization of the unit theory and the planned
				analysis of the interview data. The work plan is still in a continuous flux, for it must be revised time
				after time as the work proceeds.</p>
			<h3>Data collection</h3>
			<p>The method of data collection was an application of the so-called &quot;micro-moment time-line
				interview&quot; (see Dervin, 1983; Dervin, 1992) which is the main method associated with the
				sense-making theory. This interview technique facilitates the accurate but understanding investigation
				of information action as a process.</p>
			<p>In actual fact, the interviews were conducted as early as in the summer of 1995, at the same time as the
				survey data were collected for my master's thesis. This was done because I had the unique opportunity to
				interview some of those people who had also filled in the questionnaire for my other study. In this way,
				the data from the two corpuses could be examined together, thus combining qualitative and quantitative
				approaches. There were altogether 20 adult interviewees, and the interviews were conducted personally by
				me during some 40 days.</p>
			<h3>Preparing the data for analysis</h3>
			<p>This means transforming the interviews from speech on audio tapes into text on computer. The recorded
				interviews were not transcribed until last autumn (1997), owing to the lack of time. The data were
				initially fed to a word processing program. After this, the interview texts were transformed into a
				format that is acceptable to <cite>NUD*IST</cite> , the analysis program that I am using as a central
				tool in my study.</p>
			<h3>Data analysis</h3>
			<p>This stage involves three steps. First, a code list which reflects the research questions is developed.
				The codes represent concepts and their categories, and they will make it possible for me to do complex
				analyses on the data, as well as to construct and revise theory. Second, the interview data is coded
				with the codes from the code list. The code list must be flexible enough to allow for changes in it.
				Third, answers to the research questions are sought by examining the database code by code, across
				cases, what the content of the pieces of text that are coded with each code is. In this case, the
				&quot;case&quot; or unit of analysis is a process of information action, not an individual.</p>
			<p>In taking the above-mentioned steps of analysis, I will be greatly aided by <cite>NUD*IST</cite> , a
				versatile qualitative analysis software package. It enables me to do quite intricate and powerful
				analyses on the interview data. Right now, I am taking my second step, that is, coding the interviews.
			</p>
			<h3>Reporting on the results</h3>
			<p>Principally, this means doing the final version of my licentiate's thesis. I have not really planned how
				to accomplish this, because it is not timely yet. However, an article on the results of my current study
				will probably be published in <cite>Informaatiotutkimus [Information Studies]</cite> which is the one
				and only refereed journal on information studies in Finland. It is at least remotely possible that I
				will also give a speech on the results in an international conference some time and place next year, or
				that an article will be published in an international journal in the future.</p>
			<h2>Notes</h2>
			<p>1. The paper was presented at ISIC'98 Doctoral Workshop, 12 August 1998, Sheffield, UK.</p>
			<p>2. Tapani Kuningas has been the chief editor of <cite>Ultra</cite> since its beginning, that is, 1975.
				<cite>Ultra</cite> is the most widely-read magazine in Finland dealing with the paranormal.</p>
			<p>3. This means the way in which the individual feels that his movement through time-space is hindered (see
				Dervin, 1983), or, more generally, the nature of the individual's movement through time-space.</p>
			<h2><a id="ref"></a>References</h2>
			<ul>
				<li>Alcock, James E. (1981). <cite>Parapsychology: science or magic? A psychological perspective</cite>
					. (Foundations and Philosophy of Science and Technology Series). Oxford: Pergamon.</li>
				<li>Alcock, James E. (1991). On theimportance of methodological skepticism. <cite>New Ideas in
						Psychology</cite> , 9 (2), 151 155.</li>
				<li>Blackmore, Susan (1990). The lure of the paranormal. <cite>New Scientist</cite> , 127 (1735), 62 65.
				</li>
				<li><cite>Collins Cobuild English Language Dictionary.</cite> (1987). London: Collins.</li>
				<li>Collins, H.M. &amp; Pinch, T.J. (1979). The construction of the paranormal: nothing unscientific is
					happening. In Roy Wallis (Ed.) <cite>On the margins of science: the social construction of rejected
						knowledge</cite>. (Sociological Review Monograph No. 27). (pp. 237 269). Keele: University of
					Keele.</li>
				<li>Dervin, Brenda (1983). <cite>An overview of sense-making research: concepts, methods, and results to
						date</cite>. Paper presented at International Communication Association Annual Meeting. Dallas,
					May 1983.</li>
				<li>Dervin, Brenda (1992). From the mind's eye of the user: the sense-making qualitative-quantitative
					methodology. In Jack D. Glazier &amp; Ronald R. Powell (Eds.) <cite>Qualitative research in
						information management</cite>. Englewood: Libraries Unlimited. (pp. 61 84)</li>
				<li>Erdelez, Sanda (1997). Information encountering: a conceptual framework for accidental information
					discovery. In Pertti Vakkari, Reijo Savolainen &amp; Brenda Dervin (Eds.) <cite>Information seeking
						in context</cite>. <cite>Proceedings of an international conference on research in information
						needs, seeking and use in different contexts</cite> . (14 16 August, 1996, Tampere, Finland).
					London: Taylor Graham. (pp. 412 421)</li>
				<li>Griffon, T. Wynne (1991). <cite>History of the occult</cite> . London: Grange Books.</li>
				<li>Halpern, David &amp; Nilan, Michael S. (1988). A step toward shifting the research emphasis in
					information science from the system to the user: an empirical investigation of source-evaluation
					behavior information seeking and use. In Christine L. Borgman &amp; Edward Y.H. Pai (Eds.)
					<cite>ASIS '88. Proceedings of the 51st annual meeting of ASIS</cite> . (pp. 169 176). Medford:
					ASIS.</li>
				<li>Heino, Harri (1994). Lukijalle (in Finnish). [To the reader]. In Håkan Arlebrand <cite>Aura, karma,
						kabbala : uuden aikakauden hengellisyys ja salatieteen nousu</cite> (in Finnish). <cite>[Aura,
						karma, cabbala : spirituality and the rise of occultism in the new era]</cite>. (Oili Aho
					Trans.). (p. 5). Helsinki: Kirjaneliö.</li>
				<li>Kari, Jarkko (1996). <cite>Rajatiedon hankinnan arkipäivää -- osa I: kyselytutkimus rajatiedon
						harrastajien paranormaaleihin ilmiöihin liittyvistä tiedontarpeista ja tiedonhankinnasta
						arkielämän tiedonhankinnan viitekehyksessä</cite> (in Finnish). <cite>[Paranormal information
						seeking in everyday life -- part I: a survey on paranormal information needs and seeking in the
						framework of everyday life information seeking].</cite> Unpublished master's thesis, University
					of Tampere.</li>
				<li><a id="ref2"></a>Krikelas, James (1983). Information-seeking behavior: patterns and concepts.
					<cite>Drexel Library Quarterly</cite>, 19 (2), 5 20.</li>
				<li>Kuningas, Tapani (1995). Tiedon tarve kasvaa (in Finnish). [Need for information is growing].
					<cite>Ultra</cite> , 24 (11), 3.</li>
				<li>Kurtz, Paul (1985). Is parapsychology a science? In Paul Kurtz (Ed.) <cite>A skeptic's handbook of
						parapsychology</cite>. Buffalo: Prometheus. (pp. 503 518).</li>
				<li>Parsons, Keith M. (1992). The study of pseudoscience and the paranormal in the university
					curriculum. <cite>Electronic Newsletter of the Georgia Skeptics</cite> ,5(5).
					ftp://ftp.netcom.com/pub/an/anson/Skeptic_Newsletters/Georgia_Skeptic/GS05-05.TXT</li>
				<li>Perttula, Suvi (1994). Tiedollisen toiminnan prosessilähtöisestä tutkimisesta (in Finnish). [On
					studying information action as a process]. <cite>Kirjastotiede ja informatiikka [Library and
						Information Science]</cite> , 13 (2), 38 47.</li>
				<li>Schumaker, Jack F. (1987). Mental health, belief deficit compensation, and paranormal beliefs.
					<cite>Journal of Psychology</cite> , 121 (5), 451 457.</li>
				<li>Sparks, Glenn G., Sparks, Cheri W. &amp; Gray, Kirsten (1995). Media impact on fright reactions and
					belief in UFOs: the potential role of mental imagery. <cite>Communication Research</cite> , 22 (1),
					3 23.</li>
				<li>Tobacyk, Jerome &amp; Milford, Gary (1983). Belief in paranormal phenomena: assessment instrument
					development and implications for personality functioning. <cite>Journal of Personality and Social
						Psychology</cite> , 44 (5), 1029 1037.</li>
				<li>Tuominen, Kimmo &amp; Savolainen, Reijo (1997). A social constructionist approach to the study of
					information use as discursive action. In Pertti Vakkari, Reijo Savolainen &amp; Brenda Dervin (Eds.)
					<cite>Information seeking in context. Proceedings of an international conference on research in
						information needs, seeking and use in different contexts</cite> . (14 16 August, 1996, Tampere,
					Finland). (pp. 81 96). London: Taylor Graham.</li>
				<li>Vakkari, Pertti (1997). Information seeking in context: a challenging metatheory. In Pertti Vakkari,
					Reijo Savolainen &amp; Brenda Dervin (Eds.) <cite>Information seeking in context. Proceedings of an
						international conference on research in information needs, seeking and use in different
						contexts</cite>. (14 16 August, 1996, Tampere, Finland). London: Taylor Graham. (pp. 451 464).
				</li>
				<li>Ward, Spencer A. (1983). Epilogue: outline of a research agenda. In Spencer A. Ward &amp; Linda J.
					Reed (Eds.) <cite>Knowledge structure and use: implications for synthesis and interpretation</cite>.
					Philadelphia: Temple University. (pp. 671 681)</li>
				<li>Wersig, G. &amp; Windel, G. (1985). Information science needs a theory of &quot;information
					actions&quot;. <cite>Social Science Information Studies</cite>, (5), 11 23.</li>
				<li>Wilson, Patrick (1977). <cite>Public knowledge, private ignorance: toward a library and information
						policy</cite> . (Contributions in Librarianship and Information Science No. 10). Westport:
					Greenwood.</li>
				<li>Wilson, T.D. (1981). On user studies and information needs. <cite>Journal of Documentation,</cite>
					37(1), 3 15.</li>
			</ul>
			<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October
					1998</strong><br>
				<em>Paranormal information seeking in everyday life -- part II: the paranormal in information
					action</em>, by [Jarkko Kari](MAILTO: csjakar@uta.fi)<br>
				Location: http://InformationR.net/ir/4-2/isic/.html   © the author, 1998.<br>
				Last updated: 9th September 1998</p>
		</li>
	</ol>

</body>

</html>