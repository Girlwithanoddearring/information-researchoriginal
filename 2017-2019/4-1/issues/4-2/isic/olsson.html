<!DOCTYPE html>
<html lang="en">

<head>
	<title>The Discourses of Contemporary Information Science Research: An Alternative Approach.</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords" content="information science, discourse analysis, co-citation analysis, Foucault">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>The Discourses of Contemporary Information Science Research: An Alternative Approach.</h1>
	<h3>Michael Olsson</h3>
	<p>Department of Information Studies<br>
		University of Technology, Sydney<br>
		<em>molsson@hum.uts.edu.au</em></p>
	<h2>Introduction</h2>
	<p>My doctoral research seeks to explore an alternative theoretical framework for examining information behaviour in
		its social context, based on the concept of &quot;discourse&quot; first put forward by the French philosopher
		and historian Michel Foucault. In doing so, it will also endevour to develop an appropriate methodology for
		examining discourse in the context of information behaviour research. This methodology, outlined below, uses the
		results of author co-citation analysis, as pioneered by <a href="#whiG">White &amp; Griffith</a> (1981), in
		order to identify discourses within the chosen field (in this case information scientists). These results are
		then used as the basis for further study using social network analysis, as outlined by <a
			href="#hay">Haythornthwaite</a> (1996). The aim of this second stage in the research process is to examine
		and understand the nature of the relationships within the discourse and to develop an understanding of the role
		of discourse in shaping information behaviour.</p>
	<p>My research will use the outlined methodology to undertake an examination of the discourses of contemporary
		information science research. Among the reasons for applying the approach to our own field was the same as that
		given by White &amp; McCain in their study: that IS researchers &quot;will best be able to judge its validity
		when it is applied to their own field&quot; (<a href="#whiM">White &amp; McCain, 1998</a>, 327). Other reasons
		for studying information science research include the desirability of the researcher being familiar with the
		&quot;rules&quot; and conventions of the discourse/s being studied and the practical consideration that
		information science researchers are (hopefully) more likely to be willing to participate in an IS research
		project than many other groups.</p>
	<p>The research puts forward &quot;discourse&quot; as an alternative to current approaches to group-based
		information behaviour research based on &quot;user&quot; or &quot;target&quot; groups. Many prevailing
		approaches to information behaviour research can be broadly divided into those that describe rather than
		theorise about information behaviour and those that seek to explain information behaviour by focusing their
		theoretical attention on the individual information user. My research adopts a different approach in which the
		group (or discourse) itself is of central theoretical importance to the understanding of information behaviour.
		In addition, where as prevailing approaches tend to conceive of information users, information systems, and
		their social context as discrete entities, albeit entities that interact, discourse is a more holistic approach
		that sees all these elements as nodes in a network of power relationships.</p>
	<h2>Foucauldian Theory and Foucault's Methods- Discourse &amp; Information Science</h2>
	<p>A distinction worth making is that while my research adopts theories and concepts from Foucault's work in IS
		research, it is not advocating the wholesale adoption of Foucault's methods of discourse analysis by IS
		researchers. While other writers in the IS sphere, such as <a href="#fro">Frohmann</a> (1992,1994), have used
		not only Foucault's theories but have also adopted the literature-based approach to examining discourse
		pioneered by Foucault himself. Rather, the paper's approach follows <a href="#tal">Talja</a> (1996), in using
		discourse as a metatheory to underpin information behaviour research but holds that information behaviour
		researchers need to find the own methods for exploring the issues raised by discursive theory.</p>
	<p>Such an assertion is, after all simply an application of the most fundamental principles of the discourse
		analytic approach. The context of Foucault's own writings was very different from that of contemporary
		information behaviour research. This very different context means that, even with a body of theory in common,
		different questions will arise based on those theories and, in consequence, different methodological approaches
		will be needed to be found in order to address these questions. For example, a discourse analytic approach to
		the study of information behaviour, will find it desirable to explore the role of discourse in shaping informal
		information behaviours and methodological frameworks for addressing this issue will need to be found. My
		research seeks to develop and explore one potential framework for exploring the concept of discourse in the
		context of information behaviour research.</p>
	<h2>Author Co-citation Analysis</h2>
	<p>The first aim of my research, given it seeks to employ a discourse analytic approach to studying information
		behaviour must be &quot;to identify the different knowledge formations, or discourses inside... (the chosen)...
		field.&quot; (<a href="#tal">Talja, 1996</a>, 10). It will therefore need, as a first step, to employ a
		methodology which can identify the major discourses in the field of contemporary IS research and , ideally, also
		tell us something about their archives and the make up of their discourse communities.</p>
	<p>It is my contention that a framework for the preliminary identification of discourse communities within
		disciplines and the discursive affiliation of individual members already exists within information science
		research, at least for academic groups such as information science researchers: Author co-citation analysis, as
		pioneered by <a href="#whiG">White &amp; Griffith</a> in their 1981 study of the literature of information
		science.</p>
	<p>Author co-citation analysis, &quot;is a set of data gathering, analytical and graphic display techniques that can
		be used to produce empirical maps of prominent authors in various areas of scholarship.&quot; (<a
			href="#mcc">McCain, 1990</a>, 433).</p>
	<p>Co-citation analysis is based on the fundamental premise that &quot;the greater the number of times a pair of
		documents are cited together the more likely it is that they are related in content&quot; (Bellardo in <a
			href="#bay">Bayer et. al., 1990</a>, 444).Whereas earlier work, such as Small's, used individual scientific
		papers as the unit of analysis, author co-citation analysis as pioneered by White &amp; Griffith is &quot;based
		on the frequency by which any work by an author is linked to any work by another author in a third and later
		work&quot; (<a href="#bay">Bayer et. al., 1990</a>, 444).</p>
	<p>“‘Author’ in this context means something like what the French call an oeuvre - a body of writings by a person -
		and not the person himself” (<a href="#whiG">White &amp; Griffith, 1981</a>, 163). This important distinction,
		not always made by bibliometric researchers, in many ways parallels the distinction drawn in discourse theory
		between the author as a discursive construct and the author as a physical being. This parallel already suggests
		the possibility of a discourse analytic interpretation of co-citation analysis data. In dealing with the whole
		body of an author's writings in the chosen field, a discourse analytic interpretation would argue that what is
		being mapped is a profile of the author's &quot;discursive identity&quot;, at least as a producer of published
		&quot;texts&quot; (additional methodological techniques will need to be employed in order to investigate how
		this may differ from the author's &quot;informal&quot; discursive identity).</p>
	<p>While existing co-citation analysis studies do not explicitly link their work to a discourse analytic theoretical
		perspective, the discursive nature of what these studies measure may be judged by statements such as:</p>
	<p>&quot;The positioning, it should be emphasized, is based on the composite judgment of hundreds of citers, rather,
		than on any one person's judgment. In effect it is the field's view...&quot; (<a href="#whiG">White &amp;
			Griffith, 1981</a>, 163).</p>
	<p>This can readily be given a discourse analytic interpretation: that what is being measured by co-citation
		analysis is the composition of the archives (or rather their formal, published component) of the discourses in
		the field, as determined by the discourse community members through the application of their discursive rules,
		as seen through the other formal, published &quot;texts&quot; in the discourse.</p>
	<p>Furthermore, quantitative analysis of co-citation analysis data using Johnson clustering, factor analysis etc.
		has shown that these &quot;author profiles&quot; form coherent groups &quot;akin to schools&quot; (<a
			href="#whiG">White &amp; Griffith, 1981</a>, 165).,Author co-citation analysis has been used successfully to
		&quot;map&quot; not only information science (<a href="#whiG">White &amp; Griffith, 1981</a>; <a
			href="#whiM">White &amp; McCain, 1998</a>), but also a number of other disciplines including communications
		(<a href="#pai">Paisley, 1990</a>), research in organizational behaviour (<a href="#cul">Culnan et. al.,
			1990</a>), and fiction studies (<a href="#beg">Beghtol, 1995</a>).</p>
	<p>My research therefore puts forward the hypothesis that the groupings created by author co-citation analysis can
		be used as a quantitative representation of the discourses of a given field. Author co-citation analysis
		therefore offers the potential to provide us with at least a preliminary profile of the discourses operating in
		the field being studied. It can therefore be an important tool in identifying for examination the discourses of
		many academic and professional groups where the researcher has ready access to a large body of published work in
		the field. My research will therefore use the results of <a href="#whiM">White &amp; McCain's</a> study (1998)
		as the basis for a preliminary identification of the major discourses of contemporary information science
		research.</p>
	<p>I would, however, concur with Lievrouw in arguing that the narrow focus of bibliometric approaches such as
		co-citation analysis, which examine only the formal outputs of information behaviour (the bibliographies of
		published journal articles), mean that such studies can not in themselves provide us with sufficient information
		to understand the nature of the groups they represent. While its proponents are entirely correct in suggesting
		that author co-citation analysis is an ideal tool for providing an independent, quantitative map of the
		specialties and sub-fields of a discipline and the authors prominently associated with them, it can not, of
		itself, tell us enough about the nature of the social dynamics and relationships between the authors. While we
		can reasonably hypothesise, that the groupings produced by co-citation analysis represent &quot;a simple
		indicator of more complex underlying behaviors or social relationships&quot; (<a href="#lie">Lievrouw, 1988</a>,
		55) , co-citation analysis alone can not provide enough evidence to support this. The inability to provide such
		sufficient evidence underpins much of the criticism of bibliometric approaches as being &quot;sterile
		mathematics&quot;.</p>
	<h2>Social Network Analysis - Investigating Discourse &amp; Information Behaviour</h2>
	<p>The next aim of my research is to investigate the hypothesis that the groupings produced by co-citation analysis
		represent a quantitative indicator of discursive affiliation, in order to explore the relationship between such
		affiliation and information behaviour, both formal and informal. A more holistic methodological approach will be
		necessary, using the results of co-citation analysis as its basis. Since rich, contextualised material will be
		needed to explicate these questions, a qualitative approach would seem to be appropriate (<a href="#mel">Mellon,
			1990</a>; <a href="#pat">Patton, 1990</a>). Interestingly, Lievrouw, in critiquing the weaknesses of purely
		bibliometric approaches, suggested that &quot;techniques typical of user studies in the information science
		tradition ... might be helpful&quot; (<a href="#lie">Lievrouw, 1988</a>, 55). One technique already used by IS
		researchers, that may be particularly useful in this context is the social network analysis approach described
		by <a href="#hay">Haythornthwaite</a> (1996). Although often used to quantitatively, social network analysis is
		increasingly being used in conjunction with qualitative research methods.</p>
	<p>Social network analysis &quot;focuses on patterns of relationships between actors&quot; (<a
			href="#hay">Haythornthwaite, 1996</a>, 323). Actors &quot;may be individuals, but they may also be
		organizations or institutions...&quot; (<a href="#hay">Haythornthwaite, 1996</a>, 324). Originally developed to
		examine tangible, economic relationships (<a href="#wel">Wellman, 1988</a>), it has been successfully adapted to
		examine those based on &quot;intangibles such as information, social support, or influence&quot; (<a
			href="#hay">Haythornthwaite, 1996</a>, 323-324).</p>
	<p>As a non IS-specific methodology, social network analysis is not intrinsically &quot;wedded&quot; to a discourse
		analytic theoretical perspective, or any other theory of information behaviour - Haythornthwaite's emphasis on
		&quot;information exchange&quot; suggests an implicitly objectivist epistemological basis for her own research.
		Nonetheless, it seems to the author that its &quot;focus on patterns of relationships&quot; (<a
			href="#hay">Haythornthwaite, 1996</a>, 324) and its contention that &quot;the world is made up of networks
		not groups&quot; (<a href="#wel">Wellman, 1988</a>, 37) makes it a methodology that is highly compatible with
		discourse theory. This paper therefore contends that social network analysis is an appropriate methodology for
		examining the question of whether co-citation analysis is a functional indicator of discourse/s within a field
		or merely produces bibliometric chimaeras.</p>
	<p>Social network analysis is a qualitative approach, using questionnaires or in-depth interviews to in order to
		collect richly contextualised research material about the nature of the participants' information relationships.
		As with co-citation analysis, social network analysis can be used generate graphical representations of the
		field being investigated: &quot;Regular patterns of relationships reveal themselves as social networks, with
		actors as nodes in the network, and relationships between actors as connectors between nodes.&quot; (<a
			href="#hay">Haythornthwaite, 1996</a>, 324).</p>
	<p>Again as with co-citation analysis, the amenability of such an approach to a discourse analytic interpretation,
		seems to the author to be particularly striking. It is therefore this paper's contention that a useful method of
		learning more about discourse in the context of information behaviour research would be to use the results of
		co-citation analysis, as the basis for a more holistic study of the same field using social network analysis</p>
	<h2>Progress - the Research Plan</h2>
	<p>The last six weeks have seen my research undergo some quite far-reaching changes, as the process of writing my
		ISIC paper has caused me to re-evaluate and re-conceptualise the theoretical underpinnings of my research.
		Whereas, I had previously tended to view discourse theory as a new mechanism for grouping information users, I
		have recently come to the realisation that exploring the role of discourse in shaping information behaviour
		called for a more radical, more holistic approach: one where information users, texts and institutions are seen
		as inter-related parts of a single theoretical &quot;entity&quot; - the discourse.</p>
	<p>This change, although a major step towards a much stronger final thesis, will require a significant degree of
		re-working of both the research plan (previous versions of which have been rendered effectively obsolete) and of
		the structure of the finished thesis. While I would say that at present, work on my thesis is progressing more
		rapidly, and with a greater level of understanding and sense of purpose than at any previous stage in my
		candidature, this re-planning is still in its early stages. While the major theoretical and methodological
		components of my thesis have now (finally) fallen into place, the detailed planning necessary to carry my
		research forward to a successful conclusion still largely lies ahead.</p>
	<h2>References</h2>
	<ul>
		<li><a id="bay"></a>Bayer, Alan E. et. al. (1990) Mapping Intellectual Structure of a Scientific Subfield
			through Author Cocitations. Journal of the American Society for Information Science 41(6), 44-452.</li>
		<li><a id="#beg"></a>Beghtol, Clare (1995) Domain Analysis, Literary Warrant and Consensus: The Case of Fiction
			Studies. Journal of the American Society for Information Science 46(1), 30-44.</li>
		<li><a id="cul"></a>Culnan Mary J. et. al. (1990) Intellectual Structure of Research in Organizational Behavior,
			1972-1984: A Cocitation Analysis. Journal of the American Society for Information Science 41(6), 453-458.
		</li>
		<li><a id="#fro"></a>Frohmann, Bernd (1992) The Power of Images: A Discourse Analysis of the Cognitive
			Viewpoint. Journal of Documentation V48, 365-386.</li>
		<li><a id="hay"></a>Haythornthwaite, Caroline (1996) Social Network Analysis: An Approach and Technique for the
			Study of Information Exchange. Library and Information Science Research V18 , 323-342.</li>
		<li><a id="lie"></a>Lievrouw, Leah A. (1988) Bibliometrics and Invisible Colleges: At the Intersection of
			Communication Research and Information Science. Proceedings of the American Society for Information Science
			Annual Meeting 88, 54-58.</li>
		<li><a id="mcc"></a>McCain, Katherine W. (1990) Mapping Authors in Intellectual Space: A Technical Overview.
			Journal of the American Society for Information Science 41(6), 433-443.</li>
		<li><a id="pai"></a>Paisley, William (1990) An Oasis Where Many Trails Cross: The Improbable Cocitation Networks
			of a Multidiscipline. Journal of the American Society for Information Science 41(6), 459-468.</li>
		<li><a id="#tal"></a>Talja, Sanna (1996) Constituting &quot;Information&quot; and &quot;User&quot; as Research
			Objects: A Theory of Knowledge Formations as an Alternative to the Information Man Theory. Information
			Seeking in Context Conference Tampere, Finland.</li>
		<li><a id="#wel"></a>Wellman, Barry (1988) Structural Analysis: From Method and Metaphor to Theory and
			Substance. In Wellman, B. &amp; Berkowitz S.D. (Eds.) Social Struture: A Network Approach Cambridge U.K.:
			Cambridge University Press.</li>
		<li><a id="whi"></a>White, Howard D. (1990) Introduction. Journal of the American Society for Information
			Science 41(6), 430-432.</li>
		<li><a id="whiG"></a>White, Howard D. &amp; Griffith, Belver C. (1981) Author Cocitation: A Literature Measure
			of Intellectual Structure. Journal of the American Society for Information Science 32, 163-171.</li>
		<li><a id="whiM"></a>White, Howard D. &amp; McCain, Katherine W. (1998) Visualizing a Discipline: An Author
			Co-Citation Analysis of Information Science, 1972-1995. Journal of the American Society for Information
			Science 49, 327-355.</li>
	</ul>
	<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
		<em>The Discourses of Contemporary Information Science Research: An Alternative Approach</em>, by <a
			href="MAILTO:molsson@hum.uts.edu.au">Michael Olsson</a><br>
		Location: http://InformationR.net/ir/4-2/isic/olsson.html    © the author, 1998.<br>
		Last updated: 9th September 1998</p>

</body>

</html>