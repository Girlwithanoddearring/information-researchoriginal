#### Information Research, Vol. 4 No. 3, February 1999

# Information in organisations: directions for information management

#### [Joyce Kirk](mailto:joyce.kirk@uts.edu.au)  
Associate Professor and Associate Dean (Coursework Programs)  
Faculty of Humanities and Social Sciences  
University of Technology, Sydney  
Australia

#### Abstract

> The work of managers in small and medium-sized enterprises is very information-intensive and the environment in which it is done is very information rich. But are managers able to exploit the wealth of information which surrounds them? And how can information be managed in organisations so that its potential for improving business performance and enhancing the competitiveness of these enterprises can be realised? Answers to these questions lie in clarifying the context of the practice of information management by exploring aspects of organisations and managerial work and in exploring the nature of information at the level of the organisation and the individual manager. From these answers it is possible to suggest some guidelines for managing the integration of business strategy and information, the adoption of a broadly-based definition of information and the development of information capabilities.

## Introduction

Information management is practised in organisations. Yet information is used by individuals in those organisations. The counterpoint between the organisation and its individual members has particular relevance to information management because of its responsibilities to both the organisation at one level and to individuals at another level. This counterpoint means that we need to consider both the organisation and its members in information terms as a starting point for developing strategies for effective information management in small and medium sized enterprises (SMEs). The purpose of the paper is to develop some general guidelines for effective information management. There are three major topics:

*   information and organisations
*   information and managers
*   information management in SMEs

SMEs, according to the standard EU definition, employ no more than 249 people, have less than 40 million ECU turnover and no more than 25 percent ownership or control by one or more enterprises which are not SMEs.

## Information and organisations

This discussion of organisations and information has two parts to it. The first provides an overview of organisations by examining a number of images used to describe them and then drawing some implications for information management. The second outlines a hierarchy of definitions of information which are appropriate for organisations and draws further implications for information management. The discussion concludes with a set of propositions about information management.

### Images of organisations

Machines, organisms, political systems and cultures ([Morgan, 1986](#morg)): these are familiar and conventional images of organisations. To these can be added a fifth image which has emerged in the past decade or so: learner ([Senge, 1990](#seng)). Each image represents a perspective on the nature of organisations. For example the machine image suggests that information is one of the resources that keeps the wheels ticking over and the task of information management is to ensure that information is delivered where and when it is needed through clearly defined and understood communication channels. The organism image implies that information from internal and external sources is required to keep the organisation in a state of equilibrium. Information management has a critical role in drawing in information about trends and developments in the external environment so that the organisation can respond to changes triggered by social, economic, technological and legislative forces.

The image of the political system recognises quite properly that because different groups in organisations have different interests they will need and use information differently in the exercise of power and influence, in the seeking of support and negotiating conflict. The image is a reminder of the political and social context of information management and draws attention to the ethics of information management practice. The image of the organisation as culture is particularly powerful with its suggestions of shared beliefs, values, norms and meaning and its emphasis on ritual, myth, language and symbol. It suggests that the use of information in an organisation will have cultural aspects to it, in contrast to the assumption that the use of information is essentially a rational human activity. Information management has a clear role in making meaning and will embody through its practice, the beliefs and values of the organisations.

The image of the organisation as learner suggests a community which regenerates itself through the creation of knowledge, the outcome of learning. Information management needs to ensure that the organisation has the information and information capabilities necessary to continuously adapt to its changing internal and external environments. It does this by adopting a forward-looking approach and by adapting itself to the ambiguity and uncertainties found in these environments. We need to remind ourselves that the image of the organisation as a learner is an emerging one which is still posing questions to some organisation theorists ([Spender, 1996](#spen); [Grant, 1996](#gran)).

Although none of these five images is by itself an adequate representation, together they highlight the complexity of organisations and the processes which sustain them. It is this complexity which is part of the context of information management in organisations and informs information management practice.

What are some implications for information management that emerge from these images of organisations?

1.  Information management has the potential to contribute to the achievements of organisations
2.  Information management has different purposes in different organisations. These purposes will be influenced by the organisation's goals as well as by its culture and its stance on information.
3.  Information management is practised in a political, social and cultural context which shapes both what information management does and how it does it.
4.  Information management practice is value laden and so it has an ethical dimension. The ethics of information management practice are most often implicit.
5.  Organisational learning concepts and theory are applicable to information management in some organisations. Not all organisations are ready for this development, nor is it an appropriate direction for all organisations.

### Some definitions of information

Organisations are increasingly aware of the potential of information in providing competitive advantage and sustaining their success ([Porter, 1985](#port)) as evidenced in a number of published case studies ([Owens, _et al._, 1996](#owen); [Grimshaw, 1995](#grim)) and commentaries ([Broadbent, 1977](#broa)). The descriptions of information as an asset and a resource ([Burk & Horton, 1988](#burk); [Best, 1996](#best)) are no longer unusual. However, the origin of these descriptions in classical economics ignores the place of information in the fabric of a political system or culture of an organisation. If information is to provide competitive advantage then its full potential needs to be considered.

A very useful hierarchy of definitions of information ([Braman, 1989](#bram)) has been developed in the area of information policy studies. The hierarchy is applicable to organisations for a number of reasons; firstly, it recognises the qualitative differences among definitions of information; secondly, its macro view is more appropriate to organisations than definitions based only on the individual as an information user; thirdly, it provides a range of definitions which are useful in different situations; and fourthly, it foreshadows the need for information policy in organisations.

The hierarchy consists of four levels, each based on a category of definitions drawn from many different fields.

1.  Information as a resource. "Information, its creators, processors and users are viewed as discrete and isolated entities. Information comes in pieces unrelated to bodies of knowledge or information flows into which it might be organised" ([Braman, 1989](#bram): 236).
2.  Information as a commodity. Complementary to definitions of information as a commodity is the concept of an information production chain through which information gains in economic value. The notion of information as a commodity incorporates "the exchange of information among people and related activities as well as its use" ([Braman, 1989](#bram): 238) and implies buyers, sellers and a market. In contrast to the absence of power of information as a resource, information as a commodity has economic power.
3.  Information as perception of pattern. Here the concept of information is broadened by the addition of context. Information "has a past and a future, is affected by motive and other environmental and casual factors, and itself has effects" ([Braman, 1989](#bram): 238). The concept of information and its processes is broadened so much so that information in this sense can be applied to a highly articulated social structure. Information has a power of its own although its effects are isolated. The example given is of information reducing uncertainty but only in regard to a single specific question.
4.  Information as a constitutive force in society. Information has a role in shaping context. "Information is not just affected by its environment, but is itself an actor affecting other elements in the environment" ([Braman, 1989](#bram):239). The definitions in this category "apply to the entire range of phenomena and processes in which information is involved, can be applied to a social structure of any degree of articulation and complexity, and grant information, its flow and use an enormous power in constructing our social (and ultimately physical) reality" ([Braman, 1989](#bram): 214].

The hierarchy of definitions of information presents a broadly based view of information and one which reflects the images of organisations which have been discussed. The traditional view of information management has focussed very much on information as a resource and as a commodity and on information management as providing a service to the organisation. That service has taken the form of providing access to information in a range of sources including on-line commercial databases, archival collections, websites and in-house databases. The definition of information as perception of pattern extends information management into a place in achieving the goals of an organisation. It is as a constitutive force though that information is most potent as a basis for future action and innovation. Information management shifts from service provision to strategy formation.

What implications can be drawn for information management in organisations from this hierarchy of definitions of information?

1.  Information management needs to encompass the full range of information from a resource to a force for change and development.
2.  Information can be integrated into organisational processes and so it can influence organisational culture, structure and work patterns.
3.  Information management can properly address information products, information services, information flow and information use in an organisation.
4.  Useful measures of the effectiveness of information management can be based on the impact of information on the organisation.

Our consideration of organisations and information has provided some implications for information management. They elaborate the scope of information management in organisations and provide a context for information management. The implications have been drawn together in a number of propositions presented in Table 1 below.

<table>

<tbody>

<tr>

<td align="justify">**Proposition 1:** Information and its management contributes to the achievement of organisational goals</td>

</tr>

<tr>

<td align="justify">**Proposition 2:** information management is contextualised by the organisation/td></td>

</tr>

<tr>

<td align="justify">**Proposition 3:** To be as effective as possible information management assumes a broad view of information</td>

</tr>

<tr>

<td align="justify">**Proposition 4:** Information can be value laden so too can be information management practice</td>

</tr>

</tbody>

<caption align="bottom">  

<div>Table 1\. Propositions about information management from organisations.</div>

</caption></table>

## Information and Managers

Having looked at the broad organisational context of information management, we need now to narrow our focus to individuals in organisations. Because of the responsibilities of managers in strategy development and implementation and in enabling organisations to meet their goals it is appropriate to restrict ourselves to considering managers rather than all individuals in organisations. This discussion of managers and information is in two parts. The first part deals with the nature of managerial work as described in a number of research studies and draws a number of implications for information management. The second part considers the nature of information from the point of view of individual users of information and is based on work in information science and in management. Again, implications for information management are drawn. The discussion concludes with some propositions about information management.

### The work of managers

The classic view of managerial functions as planning, organising, communicating, coordinating and controlling ([Fayol, 1949](#fayo)) suggest a rational and ordered approach to management activities. Yet studies of managers in their workplaces present a picture of an approach to managerial activities that is quite different. A brief overview of a number of studies based on questionnaires and observation of managers in their workplaces highlight the seemingly active, informal, fragmented and chaotic nature of managerial work.

One study ([Mintzberg, 1975](#mint)) identified ten different roles for managers. The roles were categorised into three groups to form an integrated view of what senior managers do. The interpersonal roles of figurehead, leader and liaison stem from the manager's formal authority. The informational roles of monitor, disseminator, and spokesman derive from the manager's interpersonal contacts. In this role the manager emerges as the "nerve centre" of the organisational unit ([Mintzberg, 1975](#mint): 55). The decisional roles of entrepreneur, disturbance handler, resource handler and negotiator arise from the manager as the formal authority of the organisational unit who can commit the unit to action. This approach to managing acknowledges the action-oriented, outward-looking and ritualistic aspects of managerial work as well as managers' strong preferences for verbal media in finding information.

Another study of managers at work identified three major processes in which they are engaged ([Kotter, 1982](#kott)): agenda setting, network building and implementing their agendas through networks. The key challenges for general managers reflect the information and people oriented demands of these three processes: "figuring out what to do (making decisions) in an environment characterised by uncertainty, great diversity, and an enormous quantity of potentially relevant information" and "getting things done (implementation) through a large and diverse group of people despite having relatively little control over them" ([Kotter, 1982](#kott): 20).

Networking is a feature of another view of managerial work ([Luthaus, _et al._, 1988](#luth)). Three other categories of activity developed from the research of managers and their subordinates were routine communication, traditional management and human resources management. Networking includes interaction with outsiders and socialising/politicking inside and outside the organisation; routine communication activities include exchanging information and handling paperwork; traditional management activities consist of planning, decision making and controlling; and human resource management includes motivating/reinforcing, disciplining/ punishing, managing conflict, staffing and training/developing. This study distinguished between successful and effective managers, between those who are promoted and those who have "satisfied, committed subordinates and produce organisational results" ([Luthaus, _et al_., 1988](#luth): 62). We are reminded that managers have their own career interests as well as interests in the organisations in which they work. The categories of activity contribute differently to success and effectiveness. Networking had the strongest relationship with success, whereas routine communication had the strongest with effectiveness. The type of activity with the weakest relationship with success is human resource management and with effectiveness the weakest is networking.

The final study considered here explores the work of middle managers. It suggests that managing relationships, finding innovation, creating a mindset and facilitating learning ([Floyd & Wooldridge, 1996](#floy)) are integral to creating competitive advantage. The study of middle managers focussed on strategy formation, a process that moves away from the overly rational, command and control model of strategy as a two stage process of formulation and implementation and has "more to do with learning than planning" ([Luthaus, _et al_., 1988](#luth): 39). Four distinct roles for middle managers in strategy were identified: championing strategic alternatives; synthesising information; facilitating adaptability; and implementing deliberate strategy. By engaging in these roles, middle managers link strategic purpose and organisational action.

These studies of managerial work range across managers at different levels in organisations from senior management to middle management. They also span different kinds of organisational structures from the more traditional hierarchies to post-entrepreneurial organisations ([Kanter, 1989](#kant)) with leaner, flatter structures and participative, team-based work units.

What are some implications for information management that emerge from this overview of managerial work?

1.  Information management has the potential to contribute to the effectiveness of managers in their diverse organisational roles.
2.  Information management should seek to meet the information needs of managers and enhance their information capabilities.
3.  Information management must recognise informal and formal information sources and flows both internal and external to the organisation.
4.  Information management needs to enable managers to integrate business strategy and information.

### Some definitions of information

Because managers in organisations use information and it is integral to their work we need to be clear about what information is at the level of individual users of information. There are two perspectives which are useful to a consideration of information management in organisations. One comes from the field of information and communication, the other from management.

The first is central to the user-oriented paradigm ([Dervin & Nilan, 1986](#derv1)) in information studies and it developed as an alternative to the traditional supplier-oriented paradigm which dominated earlier developments in information systems and the applications of information technology to information services. This perspective posits that information is a process that applies to adaptive and creative behaviour. It relies on _Information1_, or "information which describes reality, the innate structure or pattern of reality ...." ([Dervin, 1977](#derv2): 22) and _Information2_ or "ideas, the structures or pictures imputed to reality by people" ([Dervin, 1977](#derv2): 22). Individuals move between _Information2_, subjective, internal information and _Information1_ objective, external information by _Information3_, a set of behaviours, or the "how" of the information process.

Similar categories of information have been derived from an analysis of dictionary definitions ([Buckland, 1991](#buck)): information-as-thing, information-as-knowledge and information-as-process. Information-as-knowledge is intangible and to be communicated needs to be expressed or represented in some physical way so becoming tangible information-as-thing. The point is made that information-as-thing has a "fundamental role" in information systems ([Buckland, 1991](#buck): 352), not only computer-based systems but also libraries, museums and collections of potentially-informative objects which might include people, products and events.

The second perspective, based on an analysis of successful companies in Japan, is centred on the individual as a creator of new knowledge ([Nonaka, 1991](#nona)). It draws a distinction between explicit and tacit knowledge. Explicit knowledge is formal, systematic and codified rather like _Information1_ or information-as-thing. Tacit knowledge is highly personal, consisting of technical skills and "know-how" as well as having cognitive dimensions such as implicit mental models and beliefs which shape our perceptions of the world. There are similarities among _Information2_, information-as-knowledge and tacit knowledge.

This perspective links individual and organisational knowledge. New knowledge always begins with an individual and it is this individual's personal knowledge which is transformed into organisational knowledge which is of value to the organisation as a whole. There are four patterns suggested for creating knowledge in an organisation and some indications of the creation process through the spiral of knowledge ([Nonoka, 1991](#nona): 97-99) are given. These are similar to _Information3_ and information-as-process.

1.  From tacit to tacit which occurs when one individual shares tacit knowledge directly with another for example through observation, imitation, conversation so that it becomes part of another's tacit knowledge base. This knowledge rarely becomes explicit.
2.  From explicit to explicit which occurs when an individual combines discrete pieces of explicit knowledge into a new whole, for example when a finance director puts together information collected from different sources in the organisation in a financial report.
3.  From tacit to explicit which occurs when tacit knowledge is articulated, for example when the finance director develops a new approach to budgetary control based on tacit knowledge. The explicit knowledge created draws on the director's experience, judgement, insight and know-how and it is then available for sharing with others in the organisation. This process extends the organisation's knowledge base.
4.  From explicit to tacit which occurs when explicit knowledge is shared throughout the organisation and others internalise it to broaden, reframe and extend their own tacit knowledge. For example, the finance director's approach to budgetary control is used by others in the organisation and it is eventually taken for granted as one of the organisation's tools or resources.

These two perspectives on information present a contrast between information as an object and information as a construct. Both kinds of information are necessary for managers in organisations. Not only do managers use information as one of the resources available to them but they also have a role in knowledge creation for themselves, their co-workers and their organisations. Because information as a tangible object is usually easier to capture, store, retrieve and disseminate, particularly with the ever-expanding range of communication and information technologies available to organisations, there is a risk that the value of intangible information as a user construct will be overlooked. Yet, it is the "informal, anomalous, multifaceted, interdisciplinary, idiosyncratic individualistic aspects of information transfer" ([Bawden, 1986](#bawd): 210) which is central to creativity and innovation.

What are some implications about information management that can be drawn from a discussion of information as used by individuals?

1.  If information in organisations is conceptualised as a process it can then be integrated into strategy formation.
2.  Information management needs to encompass information as an object and information as a construct as well as interactions between them.
3.  Information management has a role in enhancing the information capabilities of individuals in organisations.
4.  Almost all managerial activities have an information component and so information management practice should be responsive to the needs of managers as information users and as information producers or knowledge creators.

Implications for information management have emerged from this overview of managers in their work roles and as users of information. They serve to elaborate and clarify the scope of information management in organisations. Some propositions about information management are presented in Table 2 below.

<table>

<tbody>

<tr>

<td>**Proposition 1:** Managers are in a unique position to integrate information and organisational strategy.</td>

</tr>

<tr>

<td>**Proposition 2:** A process approach to information supports the integration of information and business strategy and is a foundation for information management.</td>

</tr>

<tr>

<td>**Proposition 3:** Information management must adopt a broadly-based approach to information so that it is concerns itself with information as an object and as a user construct formal and informal flows of information inside and outside organisations sources of information internal and external to organisations enhancing the information capabilities of individuals in organisations.</td>

</tr>

<tr>

<td>**Proposition 4:** The effectiveness of information management can be measured by the impact of information on the organisation.</td>

</tr>

</tbody>

<caption>  

<div>Table 2\. Propositions about information management from managers</div>

</caption></table>

The propositions based on the nature of the information in organisations and the nature of information in managerial work challenge information management thinking and practice. They underpin a shift in information management away from the provision of information with an emphasis on technology and access towards strategy with focus on information use and its integration into business strategy

## Information management in SMEs

The propositions about information management shown in Tables 1 and 2 are based on an overview of organisations, managers' work and information. The propositions are tentative, some more so than others. They are however based on a range of perspectives, the central one being information which has the potential to sustain an organisation's competitive position if it is used effectively by the members of that organisation. The links among the perspectives on information are shown in Figure 1 below.

![Figure 1](../p57fig1.gif)

<div>Figure 1: The information environment of information in organisations</div>

Broad guidelines for information management have been developed from the propositions and each one will now be explored. The guidelines fall into the categories related to the purpose, scope and implementation of information management. The guidelines are shown in Table 3 below.

<table><caption>  

<div>Table 3\. Guidelines for information management based on propositions about organisations, managers and managers and information</div>

</caption>

<tbody>

<tr>

<td>**1\. Purpose of information management**</td>

</tr>

<tr>

<td>Information and its management contributes to the achievement of organisational goals</td>

</tr>

<tr>

<td>A process approach to information management supports the integration of information and strategy</td>

</tr>

<tr>

<td>**2\. Scope of information management**</td>

</tr>

<tr>

<td>Information management is contextualised by the organisation and is value-laden</td>

</tr>

<tr>

<td>Information management must adopt a broadly-based approach to information and encompasses:</td>

</tr>

<tr>

<td>information as an object and as a user construct  
formal and informal flows of information inside and outside organisations  
sources of information internal and external to organisations  
enhancing the information capabilities of individuals in organisations.</td>

</tr>

<tr>

<td>**3\. Implementing information management**</td>

</tr>

<tr>

<td>Managers are in a unique position to integrate information and business strategy</td>

</tr>

<tr>

<td>The effectiveness of information management can be measured by the extent of knowledge creation or innovation in organisations.</td>

</tr>

</tbody>

</table>

### Purpose of information management

**Information management contributes to the achievement of organisational goals**. There is little doubt that information does contribute to the success of organisations. Whether information is regarded as a resource or a force for change and development it is clear that information can contribute to the achievement of organisational goals. The assumptions are made that the goals of an organisation are explicit, thus are known throughout the organisation and there is some way determining the extent to which the goals have been met.

A survey of high performing companies in the UK ([Owens, _et al._, 1997](#owen2)) found almost universal acceptance of the view that information is a valuable asset. The more successful companies were concerned with information management issues and some were working towards the creation of an information culture as a way of ensuring continued success. In the words of one manager interviewed in the study:

> We do recognise that information is an asset and, thereby, we should maximise the value from that asset, and make it available to all those who can obtain value from it. We are working on the devolution of decision making responsibility. We talk a lot about flattening the organisation structure, and we have achieved a certain amount of progress. I think we've got the right attitude, but we're still going through a learning process. I believe the more enlightened managers understand that a facultative management style is the right style for modern business practice. That staff working in the business processes are best placed to make these processes effective, and these are the ones who really need the information. ([Owens, _et al._, 1997](#owen2): 25)

This manager's concern with the need to link business strategy and information has been shared by managers in other countries in the western world ([Broadbent, 1992](#broa2)). A key factor in linking or aligning business strategy and information has been information technology, a tool essential for business success is a global economy. The potential for information technology and information to transform organisations is evident in those companies which have redesigned their business processes. Information and information technology have acted as enabling and integrating tools for survival and growth in rapidly changing environments ([Johannessen & Olaisen, 1993](#joha); [Moreton, 1995](#more)).

**A process approach to information management supports the integration of business strategy and information.** The calls for integration come from at least three different communities: information management, information systems and management. Successful companies are those that adapt to and shape their environments and do so by using and creating information in a process of continuous improvement and innovation.

Information management has been described in one particular model as a continuous cycle of related activities which encompass the information value chain. The activities in the cycle ([Choo, 1995](#choo): 24-26) are:

1.  identification of information needs defined by subject matter requirements and situation
2.  information acquisition involving evaluation of current sources, assessment of new sources and matching of sources to needs
3.  information organisation and storage of the organisation's memory or its knowledge and expertise
4.  information products and services aimed at enhancing the quality of information
5.  information distribution through sharing information informally or formally
6.  information use in the creation and application of information in interpretation and decision making.

Implicit in the model is organisational learning and the development of individuals. For example, information organisation and storage activities will ensure that the organisation's knowledge is made available to individuals or groups in an organisation and is built on. Information distribution activities can develop fresh insights and novel solutions which can then be captured through organisation and storage activities and made available for later use. the spiral of knowledge can be seen through this process approach to information.

The activities of identification of information needs and information use are not always regarded as components of information management. Yet it is these two activities which require the integration of business strategy and information. Information needs will arise in the context of the organisation's goals and the objectives of work teams and it is this context which surrounds the use of information. Certainly these two activities are beyond the scope of information resource management and information systems management as they have been practised.

The integration of business strategy and information also suggests different ways of organising work and designing the organisation. Teams are very appropriate to strategy formation and achieving competitive advantage. In the learning organisation there are three groups of experts who need to work together as knowledge partners ([Choo, 1995](#choo)). The groups are domain experts in the organisation who create and use knowledge, for example operators, professional managers; information experts with "skills, training and know-how for organising knowledge information systems and structures that facilitate the productive use of information and knowledge resources" ([Choo, 1995](#choo): 198); and information technology experts who have specialised expertise in building and developing the organisation's information infrastructure.

The focus of each group of experts is different. Organisation effectiveness is the focus of the domain expert, enlightenment is the focus of the information expert and process efficiency is the focus of the information technology experts. As the partners in the teams work together they will create, organise and use knowledge. The partners will engage in learning and in furthering the organisation's objectives. They will assume information responsibility within the team and for themselves ([Drucker, 1988](#druc1)).

Key factors for success in SMEs reinforce the need for integration of business strategy and information. Managers in firms see their success as dependent on their organisation's ability to accommodate and manage change and to respond to changes in their environments. The key factors are:

*   relationships with customers and suppliers
*   flatter management structures and better use of resources
*   training and quality
*   environmental issues ([Abell, 1994](#abel): 236)

Each of these factors rests on information, its use, creation, storage and distribution. Issues of the quality of information (in terms of its accuracy, validity, timeliness), its accessibility, availability, presentation, ease of use, and its organisation and storage are the concern of teams or task forces developing strategy or engaging in projects in relation to any one of these factors.

### Scope of information management

**Information management is contextualised by the organisation and is value-laden**. The earlier discussion of images of organisations suggests different approaches to information management in organisations. While the objectives of information management will be linked to the effectiveness of organisations, information management practice will vary across organisations. For example, in the organisation which is like a machine, the information management function might be centrally located in a unit established to control internally generated information. This unit would have links to an IT unit. There might also be a library in the organisation which provides an information service based on externally generated information. Depending on the industry sector, the market place, the culture and the nature of work in the organisation, such a structural arrangement for information management might be appropriate. The objectives and priorities for information management will be framed within this context.

By contrast, in the organisation which is like a learner, the information management might be decentralised in a federal structure which supports teams but locates some information management functions centrally. Each team will be responsible for providing information to know how databases accessible through the organisation's intranet. Compared to the previous example, the objectives for information management and priorities for services will be quite different. Some differences will be seen in:

*   distinctions made between internally and externally generated information
*   the reuse and sharing of information arising from activities in each organisation
*   the relationship among library, information management and IT staff
*   the applications of IT
*   the value attributed to information
*   the information ethos
*   measures of success used by the organisation and by information management.

It needs to be recognised that the differences in these two examples are valid. There is no one approach to information management which is inherently more effective than any other.

An important element in information management is information politics which indicate the assumptions made about how people generate and use information in organisations. Some models of information politics ([Davenport, _et al._, 1996](#dave)) that have been developed include:

*   technocratic utopianism: a heavily technical approach to information management stressing categorisation and modelling of an organisation's full information assets.
*   anarchy: the absence of any overall information management policy leaving individuals to obtain and manage their own information.
*   feudalism: the management of information by individual business units or functions, which define their own information needs and report only limited information to the overall corporation. The definition of information categories and reporting structure by the firm's leaders, who may or may not share the information willingly after collecting it.
*   federalism: an approach to information management based on consensus and negotiation on the organisation's key information elements and reporting structures.

The federalism model is probably most appropriate to the organisation as learner, although it is possible that a number of models could co-exist in an organisation. The models reinforce the point that the context of the organisation is an important influence on information management and a reminder that it may in fact act as a barrier to the development of sound information management practice.

The issue of context is applicable to SMEs as well as large organisations. It may be particularly relevant to SMEs which are interested in establishing strategic partnerships with external information services. SMEs will need to clearly state their expectations of any service providers and to evaluate the organisational context of any partners so that the risks associated with potential sources of difference can be assessed.

**Information management must adopt a broadly-based approach to information**. Just as there is a hierarchy of definitions of information there is possibly a hierarchy of definitions of information management. A possible hierarchy of definitions is suggested in Table 4 below.

<table><caption>  

<div>Table 4: Possible hierarchy of definitions of information management</div>

</caption>

<tbody>

<tr>

<th width="50%">Definitions of information*</th>

<th width="50%">Definitions of information management</th>

</tr>

<tr>

<td>Information as resource</td>

<td>Information management as IT systems</td>

</tr>

<tr>

<td>Information as commodity</td>

<td>Information resource management</td>

</tr>

<tr>

<td>Information as perception of pattern</td>

<td>Information management as aligning information strategy and business strategy</td>

</tr>

<tr>

<td>Information as constitutive force</td>

<td>Information management/knowledge management integrating strategy formation and information</td>

</tr>

<tr>

<td colspan="2">* Definitions of information based on Braman (1989).</td>

</tr>

</tbody>

</table>

If information management is to influence the development of the organisation then it should recognise as many categories of information as possible, as broad a range of sources and media as possible, and as broad a range of uses of information as possible. The information provided by the information expert in work teams should be seamless and all information created by the team which is likely to be of value should be organised and stored by the information expert. The information will no doubt be stored on many different media including discs, maps, photographs, brochures, samples, diagrams, videos, reports, printouts, all of which are part of the organisation's memory and history and have the potential to be reused in later projects. The tacit knowledge of the team becomes explicit knowledge for the next team or task force working on a similar project.

Information experts have a role in enhancing the information capabilities of individuals in organisations. Efforts to adapt human behaviour to the information systems have not been successful. IT offers some potential for developing interactive systems which should be capable of adapting to human behaviour. Systems are needed for identifying information need by supporting IT experts and managers in articulating their information needs, capturing information or tacit knowledge for storage in databases and adding value to information.

There are a number of approaches to adding value to information already in use but there is room for further development. In organisations information experts might discuss with managers their media preferences, information use strategies and barriers they have encountered in using and applying knowledge. Information experts can then begin tailoring information products and services to enable managers to make decisions, solve problems, think strategically, scan the environment and carry out other aspects of their work roles. One approach adds value to information to help information users match the information provided by a system with their needs ([Taylor, 1985](#tayl)). The added values include ease of use, noise reduction, quality, adaptability, time-saving and cost saving. Another approach is directed toward reducing information overload for managers (34) by increasing the quality of information. Some of the values are related to the scarcity of information and the degree of confidence a manager places in information.

It is possible for SMEs to adopt a broadly-based approach to information and ensure that the information they need in order to maintain their competitive advantage is accessible and usable. There are a number of published studies and commentaries ([Choo, 1995](#choo); [Taylor, 1985](#tayl); [Browne, 1993](#brow); [Diamantopoulos & Souchon, 1996](#diam)) outlining managers' information behaviour and from them further approaches for adding value to information can be developed. The studies challenge some common assumptions held by information professionals about managers' attitudes to information, their use of it in decision making and the strategies they develop to cope with vast quantities of information.

### Implementing information management

**Managers are in a unique position to integrate information and business strategy**. Successful implementation of information management presupposes senior management support, expressed most visibly in funding priorities and through information-related activities and projects from training programs to information system enhancements. The integration of information and business strategy presupposes a learning organisation which is team based. Managers as domain experts are able to use and create information and knowledge so that both information and business strategy are embedded in the innovations, products, processes or services developed by the team.

It is in their daily work that managers have opportunities to integrate business strategy and information. In the knowledge creating company ([Nonaka, 1991](#nona)) there is a continual shift in meaning as new knowledge is diffused through the company . At times the confusion created by this shift escalates to ambiguity and even chaos which can lead to fresh insights and a new sense of direction. The job of managers is then to "orient this chaos towards purposeful knowledge creation" ([Nonaka, 1991](#nona): 103). Senior managers articulate metaphors, symbols and concepts about the company's future, middle managers orient the chaos with their co-workers on company teams. In the learning organisation, middle managers make explicit the tacit knowledge of senior managers and their co-workers and incorporate it into new technologies and products. In this sense, managers are knowledge engineers. In other words, middle managers and their teams create information as an object on the basis of the information constructs of others.

This process is similar to some definitions of knowledge work. One refers to "activities using individual and external knowledge to produce outputs characterised by information content" ([Davenport _et al._, 1996](#dave2): 54). Another suggests that knowledge work characterised by variety and exception and is performed with a high level of skill and expertise. Included in knowledge work processes are activities such as education, accounting, research and development, law and management processes of strategy and planning ([Davenport _et al._, 1996](#dave2)). This view suggests that there are five different processes in knowledge work:

1.  finding existing knowledge, understanding knowledge requirements, searching for it among multiple sources and passing it on to a user. An example is a competitive intelligence process in an insurance company.
2.  creating new knowledge in say creative processes in advertising or research activities in a pharmaceutical firm.
3.  packaging knowledge created externally to the processes. An example is the editing, design and proofing processes in publishing.
4.  applying existing knowledge, for example is a surgeon applying existing medical knowledge.
5.  revising knowledge. Examples are the reuse of existing components in an engineering project or the residential development of former warehouses.

These five processes are used by managers and all are dependent on information. By engaging in these processes in their role in strategy formation managers are able to integrate information and business strategy. These knowledge work processes are possible in SMEs. Smaller organisations might in fact have more flexibility in moving to project teams. They may also have advantages in being able to apply existing knowledge or create new knowledge through innovation. The task of information management is to enable managers to engage effectively in knowledge work. Key contributions by information management include:

*   developing information filters so that that volume of information is contained
*   enhancing the quality of information
*   building know-how databases
*   facilitating information sharing across teams

Managers in SMEs supported by effective information management and information experts, like their colleagues in large organisations, are well placed to integrate information and business strategy.

**The effectiveness of information management can be measured by the extent of knowledge creation or innovation in organisations.** The evaluation of information management needs to take into account different views on information. Evaluation based solely on information as object or tangible information will be misleading. The focus of evaluation needs to include also information as a construct and information processes. One approach to the evaluation of information management could be based on the processes of information management discussed earlier. Another approach might be based on innovations in the organisation and might include consideration of the information capabilities of managers and their co-workers.

The measures of the effectiveness of information management will be very similar in SMEs and large organisations. They will be directed towards teams and the organisation itself. Some measures will require evaluation over a period of time, for example when the information ethos of an organisation is used as an indicator of effective information management. Other measures might be applied over shorter time periods for example in the evaluation of training programs. The indicators will require qualitative and quantitative measures. The challenge is to identify what it is that information and learning have made possible in an organisation.

Information management has multiple meanings. Its meanings are shaped by different perspectives on information, on organisations and on the work of managers. Information management has the potential to transform organisations but only when information and business strategy are integrated. Managers in SMEs and large organisations have a key role in putting information to productive use and ensuring that their organisations are successful. In the words of Drucker:

> " ... knowledge is the only meaningful resource today. The traditional "factors of production" - land (ie natural resources), labour and capital - have not disappeared, but they have become secondary. They can be obtained, and obtained easily, provided there is knowledge. And knowledge in this new sense means knowledge as a utility, knowledge as the means to obtain social and economic results. These developments, whether desirable or not, are responses to an irreversible change: _knowledge is now being applied to knowledge._ Supplying knowledge to find out how existing knowledge can best be applied to produce results is, in effect, what we mean by management. But knowledge is now also being applied systematically and purposefully to define what new knowledge is needed, whether it is feasible, and what has to be done to make knowledge effective. It is being applied, in other words, to systematic innovation" ([Drucker, 1993](#druc2): 42).

In organisations, information management is the key to systematic innovation and to the benefits that innovation brings.

## References

*   <a id="abel"></a>Abell, A. (1994) Information use and business success: a review of recent research on effective information delivery. In: M. Feeney and M. Grieves (Editors), _The Value and Impact of Information_, London: Bowker Saur, 229-253
*   <a id="bawd"></a>Bawden, D. Information systems and the stimulation of creativity, _Journal of Information Science_, 12, 203-216
*   <a id="best"></a>Best, D.P. ed. (1996) _The Fourth Resource: Information and Its Management_. Aldershot, Hants: Aslib/Gower.
*   <a id="bram"></a>Braman, S. (1989) Defining information: an approach for policymakers, _Telecommunications Policy_ 13, 233-242
*   <a id="broa"></a>Broadbent, M. (1977) The emerging phenomenon of knowledge management, _Australian Library Journal_ 46, 6-23
*   <a id="broa2"></a>Broadbent, M., Lloyd, P., Hansell, A. & Dampney,C.N.G. (1992) Roles, responsibilities and requirements for managing information systems in the 1990's, _International Journal of Information Management_ 12, 21-38
*   <a id="brow"></a>Browne, M. (1993) _Organisational Decision Making and Information_. Norwood, NJ: Ablex.
*   <a id="buck"></a>Buckland, M. (1991) Information as thing, _Journal of the American Society for Information Science_ 42, 351-360
*   <a id="burk"></a>Burk, C.F. & Horton, F.W. (1988) _Infomap: A Complete Guide to Discovering Corporate Information Resources_. Englewood Cliffs, NJ: Prentice Hall.
*   <a id="choo"></a>Choo, C.W. (1995) _Information Management for the Intelligent Organisation: the Art of Scanning the Environment_. Medford, NJ: Information Today.
*   <a id="dave"></a>Davenport, R.H., Eccles, R.G. & Prusak, L. (1996)Information politics, _Sloan Management Review_ (Summer), 53-65
*   <a id="dave2"></a>Davenport, T.H., Jarvenpaa, S.L. & Beers, M.C. (1996) Improving knowledge work processes, _Sloan Management Review_ (Summer 1996) 53-65
*   <a id="derv2"></a>Dervin, B. (1977) Useful theory for librarianship: communication, not information, _Drexel Library Quarterly_ 13, 16-32
*   <a id="derv1"></a>Dervin, B. & Nilan, M.(1986) Information needs and uses, _Annual Review of Science and Technology_ 21, 3-32
*   <a id="diam"></a>Diamantopoulos, A. & Souchon, A.L. (1996) Instrumental, conceptual and symbolic use of export information: an exploratory study or UK firms, _Advances in International Marketing_ 8, 117-144
*   <a id="druc1"></a>Drucker, P. (1988) The coming of the new organisation, _Harvard Business Review_ (Jan-Feb), pp.
*   <a id="druc2"></a>Drucker, P. (1993) _Post-Capitalist Society_. New York: HarperCollins.
*   <a id="fayo"></a>Fayol, J. (1949) _General Industrial Management_. London: Pitman.
*   <a id="floy"></a>Floyd, S.W. & Wooldridge, B. (1996) _The Strategic Middle Manager: How to Create and Sustain Competitive Advantage_. San Francisco: Josey Bass.
*   <a id="gran"></a>Grant, R.M. (1996) Toward a knowledge-based theory of the firm, _Strategic Management Journal_ 17, 109-122.
*   <a id="grim"></a>Grimshaw, A. ed. (1995) _Information Culture and Business Performance_.Hatfield, Herts: University of Hertfordshire Press.
*   <a id="joha"></a>Johannessen, J.A. & Olaisen, J. (1993) The information intensive organisation: a study of governance, control and communication in a Norwegian shipyard, _International Journal of Information Management_ 13, 341-354
*   <a id="kals"></a>Kalseth,K. (1991) Business information strategy, _Information Services and Use_ 11, 155-164.
*   <a id="kant"></a>Kanter, R.M. (1989) The new managerial work, _Harvard Business Review_ 67, 85-92
*   <a id="kott"></a>Kotter, J.P. (1982) _The General Managers_. New York: Free Press.
*   <a id="luth"></a>Luthaus, F., Hodgetts, R.M. & Rosenkrantz, S.A. (1988)_Real Managers_. Cambridge, MA: Ballinger.
*   <a id="mint"></a>Mintzberg, H. (1975) The manager's job: folklore and fact, _Harvard Business Review_ 53, 49-61
*   <a id="more"></a>Moreton, R. (1995) Transforming the organisation: the contribution of the information systems function, _Journal of Strategic Information Management_ 4, 149-163
*   <a id="morg"></a>Morgan, G. (1986) _Images of Organisation_. Beverly Hills, CA: Sage.
*   <a id="nona"></a>Nonaka, I. (1991) The knowledge-creating company, _Harvard Business Review_ (November-December), 96-104
*   <a id="oppe"></a>Oppenheim, C. (1997) Managers' use and handling of information, _International Journal of Information Management_ 17, 239-248.
*   <a id="owen"></a>Owens, I., Wilson, T.D. & Abell, A. (1996) _Information and Business Performance: A Study of Information Systems and Services in High Performing Companies_. London: Bowker Saur.
*   <a id="owen2"></a>Owens, I. & Wilson, T.D. with Abell, A. (1997) Information and business performance: a study of information systems and services in high performing companies. _Journal of Librarianship and Information Science_ 29, 19-28
*   <a id="port"></a>Porter, M.E. (1985) _Competitive Advantage: Creating and Sustaining Superior Performance_. New York: Free Press.
*   <a id="seng"></a>Senge, P.M. (1990) _The Fifth Discipline: the Art and Practice of the Learning Organisation_. New York: Doubleday Currency
*   <a id="simp"></a>Simpson, C.W. & Prusak, L (1995) Troubles with information overload moving from quantity to quality in information provision _International Journal of Information Management_ 15, 413-425.
*   <a id="spen"></a>Spender, J.C. (1996) Making knowledge the basis of a dynamic theory of the firm, _Strategic Management Journal_ 17, 45-62
*   <a id="tayl"></a>Taylor, R.S. (1985) _Value-Added Processes in Information Systems_. Norwood NJ: Ablex.
*   <a id="vict"></a>Victor, B., Pine, B.J. & Boynton, A.C. (1996) Aligning IT with new competitive strategies. In J. N. Luftman (Editor), _Competing in the Information Age: Strategic Alignment in Practice_,Oxford: Oxford University Press, 75-94.