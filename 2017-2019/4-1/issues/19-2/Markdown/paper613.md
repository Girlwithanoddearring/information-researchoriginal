<header>

#### vol. 19 no. 2, June, 2014

</header>

<article>

# Impact of assessment criteria on publication behaviour: the case of communication research in Spain

#### [Pere Masip](#author)  
School of Communication Blanquerna, Ramon Llull University, Barcelona, Catalonia, Spain

#### Abstract

> **Introduction.** This paper outlines the evolution of Spanish production in the area of communication research over the last seventeen years. It analyses whether the consolidation of the existing systems of assessment of scientific activity have been mirrored by an increase in the output of Spanish authors in journals indexed by the Social Sciences Citation Index  
> **Method.** A bibliometric approach to the subject matter has been selected.  We have analysed indicators such as institutional and individual productivity, models of publishing and dynamics of co-operation (intra-and inter-institutional, national and international).  
> **Analysis.** This method has been applied to thirty-four journals included in the communication category of the Social Sciences Citation Index. To ensure consistency in the data collected, only journals that have remained in this database over the seventeen years covered by the research, from 1994 to 2010, have been selected.  
> **Results.** Results reveal that the output of Spanish researchers in communication has increased significantly over five years, from forty-eight papers in 1994-2005, to eighty-two for the period 2006-2010.  
> **Conclusions.** The increase coincides with the creation in 2002 of National Quality and Accreditation Evaluation Agency (ANECA) whose assessment criteria give priority to publication in journals indexed by the Social Sciences Citation Index

<section>

## Communication as a scholarly discipline in Spain

Communication as an academic discipline is continually growing in Spain. The first universities began to teach the subject just over forty years ago and since then the number of new faculties and schools has steadily increased. In 1971 the first three communication faculties in Spain were created, Complutense University of Madrid, Autonomous University of Barcelona and University of Navarra. These universities took up the baton from the former Escuelas Oficiales which had previously undertaken the training of journalists, advertising and audiovisual (film and television) specialists. Since then, new schools have gradually appeared. In 1995 there were already thirteen, employing 912 teachers ([Spanish Directory, 1995](#spa05)); and in 1998 there were twenty-two, with 2,000 teachers and 20,000 students ([Jones, 1998](#jon98)).

However, the great boom in Communication studies in Spain took place in the first years of the new millennium. In 2005 there were already forty-four schools ([Moragas, 2005](#mor05)), which admitted 6,000 new students each year. There are currently fifty-six schools offering a degree in any of the three specialities which have traditionally been linked to communication in Spain: journalism, audiovisual communication and advertising and public relations. In the academic year 2008/2009 Spain had 17,583 students enrolled in journalism, 16,566 in advertising and public relations and 12,486 in audiovisual communication ([Abuín, 2010](#abu10)).

To meet the growing demand, a substantial community of teachers has emerged, who carry out important teaching and research activity. Traditionally, the communication faculties have drawn on teaching staff from two different backgrounds. Firstly, teachers with a strong academic background, whose achievements have been closely linked to the university, in which they have developed their teaching and research career. Joining them are working practitioners who combine professional practice with teaching. They do not tend to give priority to obtaining a doctorate or to research, although there are cases of working practitioners who collaborate in research and eventually become fully integrated into the university. This dual sensitivity, which some authors have dubbed as "communicologists" and "professionalists" ([Borrat, 1990](#bor90); [Martínez Nicolás, 2006](#mar06)), also defines the character that some people believe communication faculties should have. While the former place the emphasis on providing concepts and tools for the understanding and analysis of communication processes, the latter identify the need to focus on the technical training of students for the production, management and dissemination of content, whether journalistic, advertising or audiovisual, the three specialities that are usually taught in Spanish communication schools ([Martínez Nicolás 2006](#mar06), [2009](#mar09)).

While Spanish universities have featured this twofold teaching profile, the recent legal reforms, which require an ever-increasing number of teaching staff with doctorates in universities, will make it difficult to continue with the system of retaining working practitioners in the classroom. In particular, the Organic Law 4/2007 states that, '_at least 50% of the faculty must possess a PhD. and at least 60% of the faculty with PhDs must have obtained a positive assessment from the National Quality and Accreditation Evaluation Agency (ANECA)_'.

The strengthening of communication faculties led to academic professionalization and the establishment of teaching staff who were committed not only to teaching but also to research. The logical consequence was a steady increase in scientific output, which was increasingly complex and diverse. A good indicator of academic activity is the number of doctoral dissertations on communication defended in Spanish universities. Between 1926 and 1998, 1,550 doctoral dissertations were defended, 64% of which were during the nineties, coinciding with the creation of new faculties and a consequent increase in demand for teachers ([Jones and Baró, 2000](#jon00)). Along the same line, recent studies of Spanish production in communication confirm a growing number of scientific papers published in Spanish journals ([Martínez Nicolás and Saperas, 2011](#mar11); [Fernández Quijada, 2010](#fer10)).

Martínez Nicolás ([2006](#mar06)) criticised the imbalance between the fervour that surrounds communication research, as measured by the number of scientific publications, conferences and meetings, and the little interest in research on communication research. Indeed, the imbalance described by Martínez Nicolás has existed. During the 1980s and 90s, albeit with some important exceptions, little attention was paid to meta-research, although in recent years there has been a growing interest in research on communication research, which should be interpreted as a strengthening of the speciality. Good examples of this are the contributions of Berrio ([1997](#ber97)), Jones ([1998](#jon98), [2000](#jon00)), Moragas ([2005](#mor05)), Martínez Nicolás ([2006](#mar06), [2009](#mar09)), García Jiménez ([2007](#gar07)) among others.

Within this theme, recent bibliometric approaches to research in communication should also be included. Some of this work has focused on characterising the research in communication in a specific period ([Masip and Fernández Quijada, 2011](#mas11); [Castillo and Carretón, 2010](#cas10); [Fernández Quijada, 2010](#fer10)), or in a particular speciality ([Xifra and Castillo, 2006](#xif06)). Others have widened their interest and have also analysed the types of research carried out by Spanish academics and the techniques employed ([Martínez Nicolás and Saperas 2011](#mar11); [López and Vicente 2011](#lop11)).

While output in Spanish journals has increased considerably, Daniel Jones ([2007](#jon07)) claimed that the Spanish output was excessive, repetitive and with little international exposure. This final point has been borne out by authors such as Lauf ([2005](#lau05)) and Masip ([2005](#mas05)). Between 1994 and 2004, there were few Spanish authors in journals in the Social Sciences Citation Index, Spanish contribution was limited to forty-three papers, which put them in 16th place worldwide and 8th in Europe. The major European producers were the United Kingdom, the Netherlands and Germany, followed by the Scandinavian countries. Spain, along with other countries with similar characteristics such as France or Italy, played a secondary role, with a very low level of institutional productivity ([Masip 2005](#mas05)).

The reasons for the low international profile of Spanish research in communication, and generally in the Social Sciences, are diverse. The local nature of some research means that national journals are the most natural channel for its dissemination ([Archambault et al., 2006](#arc06); [Nederhof, 2006](#ned06); [Hicks, 2004](#hic04)). Duality between national journals and “international” journals, usually defined by the language used - the native language used for national journals and English for the international ones - does not exist for Anglo-Saxon academics who rarely publish in journals in languages other than English ([van Leeuwen et al., 2001](#van01)). It is also worth mentioning that monographs have a significant role in scientific exchange in the Social Sciences and Humanities ([Larivière et al., 2006](#lar06); [Nederhof, 2006](#ned06); [Hicks, 2004](#hic04)).

Alongside these markedly academic factors, others of a very different nature must be added. One of the principal ones is language. English has become the lingua franca of research. Journals in languages other than English have little presence in major bibliographic databases. 85% of journals indexed by Scopus are written primarily in English ([Moya-Anegón et al., 2007](#moy07)) and 60% of journals in the Social Sciences Citation Index are North American ([Archambault et al., 2006](#arc06)). Only 10% of Spanish journals in Social Sciences and Humanities are to be found in the large international databases ([Osca-Lluch and Haba, 2005](#osc05)). However, regardless of the number of Spanish journals included in international indices, the fact is that researchers who are looking for greater visibility and wider dissemination of their work are forced to publish in English ([Rey-Rocha and Martin-Sempere, 2004](#rey04)). This situation implies an extra effort for those authors whose native language is not English ([Lauf, 2005](#lau05)).

## Assessment of scientific activity in Spain

In the establishment and gradual consolidation of the culture of assessment of teaching and research in Spain the National Quality and Accreditation Evaluation Agency has had a critical role. Created in 2002 as a result of the development of the organic law of universities , the National Quality and Accreditation Evaluation Agency is responsible for assessing and accrediting the teaching, research and management of university teachers with the overall aim of improving the quality of the university system.

The National Quality and Accreditation Evaluation Agency had an immediate antecedent, which was the National Commission for the Assessment of Research Activity (CNEAI), created in 1989, whose function was to assess the research work of the university teachers and researchers of the Higher Scientific Research Council (CSIC) with the main aim of awarding them with a financial incentive every six years in recognition of their research activity.

The panorama of Spanish agencies for research assessment is completed by the National Agency for Assessment and Forecasting (ANEP), which assesses the research groups and research projects which apply for public funding under the national Research, Development and Innovation plan promoted by the ministry of science and innovation; and a myriad of regional assessment agencies (e.g. Agencia de Evaluación de la Calidad y Acreditación del Sistema Universitario Vasco (UNIBARQ), Agència per a la Qualitat del Sistema Universitari de Catalunya (AQU), Agencia de Calidad y Acreditación Prospectiva de las Universidades de la Comunidad de Madrid (ACAP).

Despite the multiplicity of agencies, the assessment criteria they use are similar. The agencies assign a predominant weighting to university teachers’ research activities, with a percentage that varies by agency. Therefore, for the National Quality and Accreditation Evaluation Agency, research activity represents 60% of the final assessment, while the remaining 40% assesses excellence in teaching and management (ANECA, 2006). In the case of the Andalusian assessment agency (AGAE) research has a 95% weighting and in the Catalan assessment agency (AQU), only research excellence is assessed.

With such emphasis placed on research, the achievements that are taken into account during teacher evaluation are: publication in journals, books and book chapters, participation in research projects, technology transfer, supervision of doctoral theses and participation in conferences. The percentages attributed to each indicator vary by agency, but, broadly speaking, publication in scientific journals is given priority. For the National Quality and Accreditation Evaluation Agency, journal papers account for 50% of the assessment of research experience (thirty out of sixty points).

These indicators show that university teachers must prove their quality mainly through research and, more specifically, through the dissemination of research results. Scientific journals have become the focus of the system for the assessment of research in Spain. In the field of communication, the National Quality and Accreditation Evaluation Agency and National Commission for the Assessment of Research Activity as well as various regional agencies prioritise and place greater importance in the assessment process on papers published in journals indexed in the Social Sciences Citation Index. Most agencies also assess the papers published in journals listed in other databases (Econlit) and national or international classifications (such as In-recs, Latindex, Dice, etc.), although the weight assigned to the Social Sciences Citation Index is greater.

The criteria used by assessment agencies have implications for everyone involved in the assessment process, but especially for researchers and journals. Researchers are obliged to tailor the research they carry out. They also admit that agencies' requirements are their main concern and put pressure on them to publish too much, too soon, and in inappropriate formats ([RIN 2009](#rin09)). Giménez and Alcaín leave no room for doubt that '_what previously was a decision shaped by the circumstances, by the research topic or by preferences for certain journals, now becomes something more premeditated, the result of a plan aimed at a better evaluation of an individual’s CV_' ([2006](#gim06), p. 107). Jaume Soriano calls the influence that the official scientific accreditation and assessment procedures have on research in Spain “the ANECA effect". According to Soriano, the academic community have changed the way they disseminate their research results and young researchers plan their careers on the subjects of study which best meet the criteria defined by the agencies ([Soriano, 2008](#sor08), p. 12).

Journals have been the main beneficiaries of the assessment criteria set by the agencies ([Giménez, 2011](#gim11)). The number of journals has multiplied. In the last update of Impact Index of Spanish Social Science Journals (IN-RECS) , twenty-three journals are included from the field of communication, two more than in the 2008 edition and six more than in 2000\. In addition, most have made significant efforts to improve their quality indicators, at least the formal aspect. Publishers are increasingly concerned about meeting the quality criteria of catalogues such as Latindex or Difusión y Calidad Editorial de las Revistas Españolas de Humanidades y Ciencias Sociales y Jurídicas (DICE) and improving their impact factor in Impact Index of Spanish Social Science Journals, adopting strategies to aid them in obtaining a higher index.

In this long-distance race for quality, some journals focused their attention on the Social Sciences Citation Index and are now beginning to reap the first benefits. In two years we have gone from not having any Spanish journals in the communication field to having as many as three (_Comunicar_, _Comunicación y Sociedad_ and _Estudios sobre el Mensaje Periodístico_).

As we have seen, the consolidation of the existing accreditation systems mainly based on the calculation of research output and especially papers in indexed journals, has had the effect of improving the quality of the Spanish communication journals. Similarly, a growing interest has developed in the study of the journals, their assessment and the development of rankings which support assessors and researchers in their work ([Giménez, Román and Alcain, 2007](#gim07)). Furthermore, given the value assigned to publication in the Social Sciences Citation Index, we should assume that there has been at least one other important consequence, international recognition of research carried out in Spain.

## Objectives and methodology

Taking this hypothesis as a starting point, the main objective of this paper is to analyse whether the consolidation of the existing systems of assessment of scientific activity has been mirrored by an increase in the output of Spanish authors in journals with international coverage, in particular from the Social Sciences Citation Index, in the field of communication. In addition it seeks to describe the main characteristics of Spanish production in the Social Sciences Citation Index.

To carry out this research, a bibliometric approach to the subject matter has been selected, i.e., the application of quantitative methods to scientific literature and the authors who produce it, to study various facets of scientific communication and activity. Specifically, we have analysed indicators such as institutional and individual productivity, models of publishing and dynamics of co-operation (intra-and inter-institutional, national and international).

This method has been applied to thirty-four journals included in the communication category of the Social Sciences Citation Index. To ensure consistency in the data collected, we have selecdted those journals that have remained in this database over the seventeen years covered by the research, from 1994 to 2010.

The longitudinal nature of the study allows the evolution of Spanish output to be described over time, and the observation of whether any kind of relationship can be inferred with the consolidation of a culture of assessment of research in Spanish universities.

In analysing the output of joint authors, the whole work has been attributed to each author, as if they had written it individually. This criterion assumes that the author is essential and prevents co-authors who are not named first, from being penalised. This explains the existence of redundant results and the difference between the number of papers and authorship. The same principle has been used for the analysis of the institutions and countries.

## Results

The Spanish contribution to the Social Sciences Citation Index over the seventeen years covered by this research amounts to 130 papers. This contribution puts Spain in the tenth position in the world and the fourth in Europe -after Great Britain (1093), the Netherlands (407) and Germany (323).

As shown in Figure 1, Spanish production in the Social Sciences Citation Index offers very modest numbers until 2006, when a change in trend can be perceived. Until then, only 1997 with eight items, and to a lesser extent 1998 and 2000, stand out slightly from a bleak picture of scarcely any Spanish contributions.

Since 2006 there has been an undeniable improvement in the relative position of Spain in the European research scene. Between 1994 and 2005 Spanish scholars were authors of forty eight papers, standing in a modest eighth place, behind countries such as Belgium (63 papers), Finland (69) Sweden (75), and France (50) (which, like Spain, has a very powerful native language). Since 2006, the situation has radically changed. In five years the country has overtaken the countries listed and, more importantly, reduced its gap with the three countries leading European output in communication.

In the five years, from 2006-2010, the Spanish contribution to the Social Sciences Citation Index communication journals increased to eighty-two papers, fifty-two of which were published in 2009 and 2010\. This is an increase of 356% from the period 2001-2005 and of 242% from 1994-2000\. This increase also results in a significant leap in the ranking of more productive European nations. Spain climbs to fourth place, surpassed only by United Kingdom, the Netherlands and Germany. In relative terms, the Spanish contribution now accounts for 7.2% of the European contribution to the Social Sciences Citation Index, well above the 2.7% of the previous period.

<figure>

![Spanish production in the Social Sciences Citation Index-communication](../p613fig1.jpg)

<figcaption>Figure 1: Spanish production in communication listed in the Social Sciences Citation Index</figcaption>

</figure>

## Affiliation of the authors

The 130 papers are signed by 220 authors, which relates to 163 different researchers. The authors are affiliated with fifty institutions, representing 163 institutional signatures. As expected, most institutions are universities (76%), although there are also companies and foundations linked to companies (16%), public research institutions (4%) and other public bodies (4%).

The analysis of the institutions to which the authors are affiliated reveals that on average, 3.3 papers have been published per institution over the seventeen years of the analysis. Most, however, only contribute a single piece of work; while one university, the University Pompeu Fabra, is responsible for thirteen papers. Next are the University of Navarra and the Madrid Autonomous University responsible for twelve and eleven papers respectively, and the Rovira i Virgili University with eight; there are seven papers by authors from the University of Girona and the University Rey Juan Carlos. These six institutions together with IE University (6) represent over 40% of Spanish output in the Social Sciences Citation Index (Table 1).

<table><caption>

Table 1\. Output by affiliated institution</caption>

<tbody>

<tr>

<th>Institution</th>

<th>Papers</th>

<th>%</th>

<th>Institution</th>

<th>Papers</th>

<th>%</th>

</tr>

<tr>

<td>Universitat Pompeu Fabra</td>

<td>13</td>

<td>8.0</td>

<td>Universidad de Santiago de Compostela</td>

<td>4</td>

<td>2.5</td>

</tr>

<tr>

<td>Universidad de Navarra</td>

<td>12</td>

<td>7.4</td>

<td>Universitat d'Alacant</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>Universidad Autónoma de Madrid</td>

<td>11</td>

<td>6.7</td>

<td>Universidad Carlos III</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>Universitat Rovira i Virgili</td>

<td>8</td>

<td>4.9</td>

<td>Universidad de Granada</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>Universitat de Girona</td>

<td>7</td>

<td>4.3</td>

<td>Universidad Pública de Navarra</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>Universidad Rey Juan Carlos</td>

<td>7</td>

<td>4.3</td>

<td>Universidad del País Vasco</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>University</td>

<td>6</td>

<td>3.7</td>

<td>Universidad de Sevilla</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>Universitat Autònoma de Barcelona</td>

<td>5</td>

<td>3.1</td>

<td>Universidad de Salamanca</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>Universidad Complutense de Madrid</td>

<td>5</td>

<td>3.1</td>

<td>Universidad de Zaragoza</td>

<td>3</td>

<td>1.8</td>

</tr>

<tr>

<td>Universidad de Murcia</td>

<td>5</td>

<td>3.1</td>

<td>& Gest Redes</td>

<td>2</td>

<td>1.2</td>

</tr>

<tr>

<td>Universitat Politècnica de València</td>

<td>5</td>

<td>3.1</td>

<td>Universidad de A Coruña</td>

<td>2</td>

<td>1.2</td>

</tr>

<tr>

<td>Consejo Superior de Investigaciones Científicas</td>

<td>4</td>

<td>2.5</td>

<td>Universidad de Almería</td>

<td>2</td>

<td>1.2</td>

</tr>

<tr>

<td>Fundación BBVA</td>

<td>4</td>

<td>2.5</td>

<td>Universidad Politénica de Madrid</td>

<td>2</td>

<td>1.2</td>

</tr>

<tr>

<td>Universitat de Barcelona</td>

<td>4</td>

<td>2.5</td>

<td>Universitat Ramon Llull</td>

<td>2</td>

<td>1.2</td>

</tr>

<tr>

<td>Universidad de Deusto</td>

<td>4</td>

<td>2.5</td>

<td>Universitat de València</td>

<td>2</td>

<td>1.2</td>

</tr>

<tr>

<td>Universitat de les Illers Balears</td>

<td>4</td>

<td>2.5</td>

<td>Institutions with one contribution</td>

<td>19</td>

<td>11.7</td>

</tr>

<tr>

<th> </th>

<th> </th>

<th> </th>

<th>Total</th>

<th>163</th>

<th>100</th>

</tr>

</tbody>

</table>

The detailed study of affiliations identifies some particularly relevant points. First, although our field of interest is communication, it is revealing to see how the contributions from faculties and departments of communication represent less than 50% of total production (Table 2). The rest is mainly distributed between faculties and departments of economics and business (19%) and humanities and social sciences (14.7%), although faculties of sociology, psychology and medicine are also represented, as well as companies. This distribution highlights the multidisciplinary nature of the field.

The contribution of the faculties of communication has remained constant over the years studied, at around 40%. In contrast, over the last five years a significant increase has been seen in papers by researchers affiliated with departments of economics and  business. Between 2006 and 2010, papers from these departments rose to 30% of the total.

<table><caption>

Table 2\. Affiliation of the authors by discipline</caption>

<tbody>

<tr>

<th>School or Department</th>

<th>N</th>

<th>%</th>

</tr>

<tr>

<td>Communication</td>

<td>67</td>

<td>41.10</td>

</tr>

<tr>

<td>Economics or Business</td>

<td>31</td>

<td>19.02</td>

</tr>

<tr>

<td>Social Sciences and Humanities.</td>

<td>24</td>

<td>14.72</td>

</tr>

<tr>

<td>Companies</td>

<td>14</td>

<td>8.59</td>

</tr>

<tr>

<td>University</td>

<td>13</td>

<td>7.98</td>

</tr>

<tr>

<td>Psychology</td>

<td>4</td>

<td>2.45</td>

</tr>

<tr>

<td>Medicine</td>

<td>3</td>

<td>1.84</td>

</tr>

<tr>

<td>Sociology</td>

<td>2</td>

<td>1.23</td>

</tr>

<tr>

<td>Other</td>

<td>5</td>

<td>3.07</td>

</tr>

<tr>

<th>Total</th>

<th>163</th>

<th>100</th>

</tr>

</tbody>

</table>

Few institutions have published continuously over the years. Those which have include the University of Navarra, Pompeu Fabra University, Madrid Autonomous University and Complutense University of Madrid. Since 2000, newer, smaller universities have increased their presence in the Social Sciences Citation Index significantly, becoming some of the most productive. These include the Rovira i Virgili University, Girona University and University Rey Juan Carlos, to which we must add the Pompeu Fabra University, where, despite having already published in the mid 1990s, most of its output has taken place well into the new millennium. By contrast, universities which had monopolised the low Spanish production during the 1990s and early 2000s, such as the University of Navarra, began to play a secondary role.

## Authorship

Most of the 163 different authors who published in journals in the Social Sciences Citation Index did so with a single paper (82.8%) and 9.8% with two papers. At the other extreme are twelve researchers who, despite representing only 7.4% of the authors, contributed 24.1% of papers (53). One of these authors contributed nine papers.

On observing the productivity of authors a set of very characteristic elements is shown. From the figures cited one can infer that the Spanish contribution to the social science citation index is highly fragmented, with a large number of transient authors, i.e., those with a single contribution. At the same time the absence of so-called authors of high productivity (logn10) is apparent, i.e. those with a productivity index (PI)>= 1\.

The number of authors who make a single contribution to the Social Sciences Citation Index is high, but can be considered as normal in the social sciences. This particular distribution and especially the absence of large producers can be explained by the short publishing history of Spanish researchers in Social Sciences Citation Index journals, which they appear to have rejected until mid-way through the first decade of the new millennium. If the publication rate of recent years is maintained, this anomaly will be corrected and a core group of authors with a high productivity rate will emerge, while the number of transitional authors should decrease.

At this point, we must recall that the productivity index does not enable us to identify the quality and importance of an author but only their output. The most productive authors tend to be individuals who are highly specialised in their area of study; and usually highly-regarded by their colleagues, but this does not mean that their work is necessarily of the highest quality

The specialism of the authors who make a greater number of contributions can be noted in the Spanish case. The most obvious example is given by the most productive author, whose nine papers have been accepted by the same publication.

<table><caption>

Table 3\. Authors with n papers  
</caption>

<tbody>

<tr>

<th>Institution</th>

<th>1 paper</th>

<th>2 papers</th>

<th>3 papers</th>

<th>4 papers</th>

<th>5 papers</th>

<th>6 papers</th>

</tr>

<tr>

<td>Universitat Pompeu Fabra</td>

<td>12</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Universidad de Navarra</td>

<td>6</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Universidad Autónoma de Madrid</td>

<td>9</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>Universitat Rovira i Virgili</td>

<td>2</td>

<td>2</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Universitat de Girona</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Universidad Rey Juan Carlos</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>IE University</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>Universitat Autònoma de Barcelona</td>

<td>8</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Universidad Complutense de Madrid</td>

<td>8</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Universidad de Murcia</td>

<td>8</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>Universitat Politècnica de València</td>

<td>4</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

</tbody>

</table>

The dispersion of Spanish production among a large number of authors with a single contribution can also be seen in Table 3\. We have taken the most productive institutions – those with at least five papers published - and have grouped the authors by number of papers. It is clear that most of the contributions are by authors with a single paper. The main exception is the University of Navarra, where a few authors are responsible for the majority of papers; one is responsible for five and another for four. As noted above, most of these papers were published in the last few years of the last century, making the university the leading Spanish contributor in the Social Sciences Citation Index. As from the middle of the first decade of the new millennium, newer institutions assumed this role, when a myriad of authors began to publish abroad.

## Collaboration

The sociology of science believes that modern science promotes co-operative work, while it is a sign of the professionalization of research activity. Some studies have also shown that studies undertaken by more than one author tend to have greater impact than that of a single author, with the exception of the humanities, and are more frequently accepted for publication than those with a sole author ([Villagrá, 1992](#vil92)).

Unlike other disciplines in the social sciences and humanities, communication research, or at least that reported in the Social Sciences Citation Index, is not solitary research. Only 29.2% of papers have one author (Table 4). 36.2% of the papers have two authors and 21.5% have three (Table 5). The co-authorship index is 2.5 authors per paper.

<table><caption>

Table 4\. Type of authorship</caption>

<tbody>

<tr>

<th>Year</th>

<th>Single  
authorship %</th>

<th>Multiple  
authorship %</th>

</tr>

<tr>

<td>1994</td>

<td>100.0</td>

<td>0.0</td>

</tr>

<tr>

<td>1995</td>

<td>100.0</td>

<td>0.0</td>

</tr>

<tr>

<td>1996</td>

<td>100.0</td>

<td>0.0</td>

</tr>

<tr>

<td>1997</td>

<td>50.0</td>

<td>50.0</td>

</tr>

<tr>

<td>1998</td>

<td>16.9</td>

<td>83.3</td>

</tr>

<tr>

<td>1999</td>

<td>0.0</td>

<td>100.0</td>

</tr>

<tr>

<td>2000</td>

<td>33.3</td>

<td>66.7</td>

</tr>

<tr>

<td>2001</td>

<td>50.0</td>

<td>50.0</td>

</tr>

<tr>

<td>2002</td>

<td>33.3</td>

<td>66.7</td>

</tr>

<tr>

<td>2003</td>

<td>33.3</td>

<td>66.7</td>

</tr>

<tr>

<td>2004</td>

<td>25.0</td>

<td>75.0</td>

</tr>

<tr>

<td>2005</td>

<td>0.0</td>

<td>100.0</td>

</tr>

<tr>

<td>2006</td>

<td>10.0</td>

<td>90.0</td>

</tr>

<tr>

<td>2007</td>

<td>44.4</td>

<td>55.6</td>

</tr>

<tr>

<td>2008</td>

<td>36.7</td>

<td>63.6</td>

</tr>

<tr>

<td>2009</td>

<td>21.4</td>

<td>78.6</td>

</tr>

<tr>

<td>2010</td>

<td>16.7</td>

<td>83.3</td>

</tr>

<tr>

<th>1999/2010</th>

<th>29.2</th>

<th>70.8</th>

</tr>

</tbody>

</table>

<table><caption>

Table 5\. Forms of collaboration</caption>

<tbody>

<tr>

<th>Authors  
per paper</th>

<th>Papers</th>

<th>%</th>

<th>Apparent  
authors</th>

</tr>

<tr>

<td>1</td>

<td>38</td>

<td>22.2</td>

<td>38</td>

</tr>

<tr>

<td>2</td>

<td>47</td>

<td>36.2</td>

<td>94</td>

</tr>

<tr>

<td>3</td>

<td>28</td>

<td>21.5</td>

<td>84</td>

</tr>

<tr>

<td>4</td>

<td>11</td>

<td>8.5</td>

<td>44</td>

</tr>

<tr>

<td>5</td>

<td>2</td>

<td>1.5</td>

<td>10</td>

</tr>

<tr>

<td>6</td>

<td>1</td>

<td>0.8</td>

<td>6</td>

</tr>

<tr>

<td>10</td>

<td>1</td>

<td>0.8</td>

<td>10</td>

</tr>

<tr>

<td>17</td>

<td>1</td>

<td>0.8</td>

<td>17</td>

</tr>

<tr>

<td>21</td>

<td>1</td>

<td>0.8</td>

<td>21</td>

</tr>

<tr>

<th>Total</th>

<th>106</th>

<th>100</th>

<th>324</th>

</tr>

</tbody>

</table>

Except in the first three years of analysis, in which all the papers had only one author, from 1997 onwards, papers by multiple authors represent the majority of work or are at least as numerous as those by sole authors. The results from 2006 onwards are particularly significant. From this point a sharp increase in Spanish production in the Social Sciences Citation Index can be observed, and also multiple authorship remains predominant.

A more detailed analysis of the forms of collaboration provides important information about the patterns of co-operation between institutions. The most common form is international co-operation, which represents 41.3% of cases with multiple authors. This is closely followed by intra-institutional co-operation between members of the same organisation (34.8%), and lastly, inter-institutional co-operation, which is 23.9% of cases (Table 6).

International co-operation has remained the most constant over time. Intra-and inter-institutional co-operation, however, were rare until around 2005\. These new forms of co-operation could be linked to the impetus of research conducted within groups and the demands of collaboration between universities that are driven by competitive public tenders for research projects. Participation in this kind of project is one of the criteria included by assessment agencies.

Despite the growth of collaborative publishing, it is interesting to note that  in recent years the number of studies by sole authors has also risen significantly. Although it is difficult to prove, this increase should not be disassociated from the need for younger teachers to build a CV which allows them to confidently apply for the positions offered by universities, and who see publication in international journals as the fastest and most effective route.

<table><caption>

Table 6\. Number of papers by type of collaboration</caption>

<tbody>

<tr>

<th>Year</th>

<th>Intra-institutional</th>

<th>Inter-institutional</th>

<th>International</th>

<th>Sole authorship</th>

<th>Total</th>

</tr>

<tr>

<td>1994</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>1995</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>4</td>

<td>4</td>

</tr>

<tr>

<td>1996</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>1997</td>

<td>1</td>

<td>0</td>

<td>3</td>

<td>4</td>

<td>8</td>

</tr>

<tr>

<td>1998</td>

<td>1</td>

<td>0</td>

<td>4</td>

<td>1</td>

<td>6</td>

</tr>

<tr>

<td>1999</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>2</td>

</tr>

<tr>

<td>200</td>

<td>1</td>

<td>0</td>

<td>3</td>

<td>2</td>

<td>6</td>

</tr>

<tr>

<td>2001</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>2</td>

</tr>

<tr>

<td>2002</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>3</td>

</tr>

<tr>

<td>2003</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>3</td>

</tr>

<tr>

<td>2004</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>4</td>

</tr>

<tr>

<td>2005</td>

<td>2</td>

<td>1</td>

<td>3</td>

<td>0</td>

<td>6</td>

</tr>

<tr>

<td>2006</td>

<td>3</td>

<td>4</td>

<td>2</td>

<td>1</td>

<td>10</td>

</tr>

<tr>

<td>2007</td>

<td>3</td>

<td>1</td>

<td>1</td>

<td>4</td>

<td>9</td>

</tr>

<tr>

<td>2008</td>

<td>4</td>

<td>2</td>

<td>1</td>

<td>4</td>

<td>11</td>

</tr>

<tr>

<td>2009</td>

<td>9</td>

<td>6</td>

<td>7</td>

<td>6</td>

<td>28</td>

</tr>

<tr>

<td>2010</td>

<td>5</td>

<td>5</td>

<td>10</td>

<td>4</td>

<td>24</td>

</tr>

<tr>

<th>Total</th>

<th>32</th>

<th>22</th>

<th>38</th>

<th>38</th>

<th>130</th>

</tr>

<tr>

<th>Total (%)</th>

<th>24.6</th>

<th>16.9</th>

<th>29.2</th>

<th>29.2</th>

<th>100</th>

</tr>

<tr>

<th>Multiple authorship (%)</th>

<th>34.8</th>

<th>23.9</th>

<th>41.3</th>

<th>-</th>

<th>100</th>

</tr>

</tbody>

</table>

As the table above shows, 29.2% of the papers published by Spanish authors were co-authored with authors from non Spanish universities, representing a total of seventy-five different instances of international co-operation. The US is the preferred country, participating in twenty-three of the seventy-five partnerships (30.7%). Next, and some way behind, are the UK (ten contributions) and Argentina and Germany (four). With three jointly-authored papers are the Netherlands and Ireland, and with two, Japan, Brazil and France. These are followed by twenty two more countries, each with a single case of collaboration.

The dominance of the US and Britain may be considered normal, as they are the leading countries worldwide in communication research ([Masip, 2010](#mas10)). The third place of Argentina could be explained by language and cultural factors, although co-operation with Latin American countries is limited. Apart from the four pieces of work developed with Argentine teachers, only four papers have been found with researchers from Mexico, Peru, Chile and Bolivia.

## Publications

The Spanish output in the Social Sciences Citation Index is spread over twenty-five of the thirty-four publications analysed, highlighting an apparent dispersion. The reality, however, is different, since just five journals account for nearly 50% of the total. These are _Public Relations Review, Telecommunications Policy, Public Understanding of Science, Discourse & Society_ and _European Journal Communication_ which publish 52.3% of Spanish papers (Table 7).

<table><caption>

Table 7\. Spanish output in the Social Sciences Citation Index. Publicactions</caption>

<tbody>

<tr>

<th>Journals</th>

<th>Papers</th>

<th>%</th>

<th>Cumulative %</th>

</tr>

<tr>

<td>Public Relations Review</td>

<td>21</td>

<td>16.2</td>

<td>16.2</td>

</tr>

<tr>

<td>Telecommunications Policy</td>

<td>19</td>

<td>14.6</td>

<td>30.8</td>

</tr>

<tr>

<td>Public Understanding of Science</td>

<td>11</td>

<td>8.5</td>

<td>39.2</td>

</tr>

<tr>

<td>Discourse & Society</td>

<td>9</td>

<td>6.9</td>

<td>46.2</td>

</tr>

<tr>

<td>European Journal of Communication</td>

<td>8</td>

<td>6.2</td>

<td>52.3</td>

</tr>

<tr>

<td>International Journal of Public Opinions Research</td>

<td>6</td>

<td>4.6</td>

<td>56.9</td>

</tr>

<tr>

<td>Journal of Advertising</td>

<td>6</td>

<td>4.6</td>

<td>61.5</td>

</tr>

<tr>

<td>Media, Culture & Society</td>

<td>6</td>

<td>4.6</td>

<td>66.2</td>

</tr>

<tr>

<td>Javnost-The Public</td>

<td>5</td>

<td>3.6</td>

<td>70.0</td>

</tr>

<tr>

<td>Journal of Communication</td>

<td>5</td>

<td>3.8</td>

<td>73.8</td>

</tr>

<tr>

<td>New Media & Society</td>

<td>5</td>

<td>3.8</td>

<td>77.7</td>

</tr>

<tr>

<td>Journal of Advertising Research</td>

<td>4</td>

<td>3.1</td>

<td>80.8</td>

</tr>

<tr>

<td>Science Communication</td>

<td>4</td>

<td>3.1</td>

<td>83.8</td>

</tr>

<tr>

<td>Journal of Media Economics</td>

<td>3</td>

<td>2.3</td>

<td>86.2</td>

</tr>

<tr>

<td>Language & Communication</td>

<td>3</td>

<td>2.3</td>

<td>88.5</td>

</tr>

<tr>

<td>Political Communication</td>

<td>3</td>

<td>2.3</td>

<td>90.8</td>

</tr>

<tr>

<td>Journal of Health Communication</td>

<td>2</td>

<td>1.5</td>

<td>92.3</td>

</tr>

<tr>

<td>Journal of Social and Personal Relationships</td>

<td>2</td>

<td>1.5</td>

<td>93.8</td>

</tr>

<tr>

<td>Journalism & Mass Communication Quartelry</td>

<td>2</td>

<td>1.5</td>

<td>95.4</td>

</tr>

<tr>

<td>Communication Research</td>

<td>1</td>

<td>0.8</td>

<td>96.2</td>

</tr>

<tr>

<td>Communication Theory</td>

<td>1</td>

<td>0.8</td>

<td>96.9</td>

</tr>

<tr>

<td>Health Communication</td>

<td>1</td>

<td>0.8</td>

<td>97.7</td>

</tr>

<tr>

<td>Human Communication Research</td>

<td>1</td>

<td>0.8</td>

<td>98.5</td>

</tr>

<tr>

<td>Media Psychology</td>

<td>1</td>

<td>0.8</td>

<td>98.2</td>

</tr>

<tr>

<td>Written Communication</td>

<td>1</td>

<td>0.8</td>

<td>100</td>

</tr>

<tr>

<th>Total</th>

<th>130</th>

<th>100</th>

<th> </th>

</tr>

</tbody>

</table>

_Public Relations Review_ contains most papers, twenty-one. It is worth emphasising, however, that eighteen of the twenty-one papers were published in recent years, between 2006 and 2010\. It is also significant that a single author has nine papers in this journal and three other authors have a further three papers.

The publishing pattern in _Telecommunications Policy, Public Understanding Science_ and _Discourse & Society_ differs from _Public Relations Review_. Far from concentrating on a few years and authors, the papers in these journals appear steadily over the years and are written by a wide range of authors and institutions.

## Discussion

The results obtained show an increase in Spanish output in Social Sciences Citation Index communication journals, particularly over the last five years. Between 2006 and 2010, the Spanish contribution to the publications in the communication section of the Social Sciences Citation Index amounted to eighty-eight papers, fifty-two of them in 2009 and 2010, which represents an increase of 356% from the period 2001-2005 and 242% from 1994-2000\. This increase also implies a significant jump up the ranking of the most productive European nations.

Although it is too soon to draw conclusions, the longitudinal nature of this research enables us to suggest a link between the results obtained and the growth of the culture of research assessment in Spanish universities and, especially, the criteria that the agencies have defined for this assessment. The increase in Spanish publications in the Social Sciences Citation Indexs follows the creation in 2002 of National Quality and Accreditation Evaluation Agency, the establishment of accreditation systems and the definition of assessment criteria. These criteria, as we have seen, give priority to the publication of research results in journals, and especially in journals included in the Social Sciences Citation Index.

The analysis of indicators of activity has also identified a dual reality according to whether the authors publish in journals in the Social Sciences Citation Index or in Spanish journals. The institutions which contribute most to domestic journals do not do so to the Social Sciences Citation Index. The larger universities and those with a longer track record such as Universitat Autònoma de Barcelona, Universidad Complutense de Madrid or Universidad del País Vasco, are the most productive in Spanish journals ([Castillo and Carretón, 2010](#cas10); [Fernández Quijada, 2010](#fer10)), but their presence in international journals is modest,. In contrast, the institutions with the biggest presence in the Social Sciences Citation Index, especially between 2006 and 2010, are small and medium-size universities which have been established more recently: Universitat Rovira i Virgili, Universidad Autónoma de Madrid, Universitat de Girona, Universitat Pompeu Fabra and Universidad Rey Juan Carlos.

Differences in researchers’ behaviour are also reflected in the forms of authorship and the patterns of co-operation. Regarding forms of authorship, sole authorship predominates in Spanish journals (76.75%) and there is a co-authorship index of 1.36 ([Fernández Quijada, 2010](#fer10)) In international journals multiple authorship is in a clear majority with a co-authorship index of 2.5\.

In most cases of multiple authorships the authors are from the same institution, and collaboration between institutions is rare. When international collaboration takes place, it is almost exclusively with Latin American institutions ([Fernández Quijada, 2010](#fer10); [Masip and Fernández Quijada, 2011](#mas11)). This is in stark contrast to the findings of the present study, in which international collaboration predominates.

The preferred journals for Spanish researchers to disseminate their research are _Public Relations Review_, _Telecommunications Policy_, _Public Understanding of Science_ and _Discourse & Society_, i.e., hyper-specialised publications. None of the four are among the most cited by Spanish authors when they publish in national journals ([Fernández Quijada, 2010](#fer10)), or the most relevant among academics in the field of journalism ([Giménez and Alcaín, 2006)](#gim06).

Bibliometric studies enable us to obtain an accurate picture of the research in a particular discipline, in this case communication. Their results cannot/should not generate strictly qualitative appraisals but only quantitative ones. From this perspective, a significant increase in the presence of Spanish authors in the Social Sciences Citation Index has been established, which, we believe, should be linked to the growth of the culture of assessment and the criteria used by agencies to assess the CVs of researchers and research groups. The increase in output does not necessarily imply a better quality of published papers. In this regard, this analysis would need to be supplemented by other measures such as citation analysis, to see if the increase in productivity is accompanied by an increase in the number of citations received by papers by Spanish authors. Although, as we have said, further research is needed, in a very preliminary approach, we have observed a relationship between the number of papers and their quality. Before 2006, figures of papers published in journals included in the first two quartiles of the Journal Citation Reports represented 50% of the Spanish production, from 2006 the percentage increased to 58.33%. Similarly, from 2006 Spanish authors also published in journals with a higher impact factor. Before 2006 the average impact factor was 0.474\. After 2006 it reached 0.844, showing a direct correlation (r2= 0.7545) between the increase of production and the quality of papers.

Similarly, it is important to emphasise the divergence in patterns of institutional affiliation and collaboration, according to whether the paper is published in Spanish or international journals. These differences may reflect the types of research carried out, the subject-matter which is tackled, geographical scope of the issues or even the methodologies. The study of this should undoubtedly be a research theme for future projects.

Jiménez, de Moya and Delgado ([2003](#jim03)) already stressed the impact of assessment agencies on Spanish research. According to the authors, Spanish research underwent a remarkable increase in productivity during the period from 1974 to 1997 as a result of the scientific policy promoted by the National Commission for the Assessment of Research Activity, which was particularly successful in stimulating production and international diffusion. Despite the results obtained, Jimenez and his colleagues confine their application to exact and experimental sciences and highlight the limited results obtained in the fields of social sciences and humanities.

The scientific policies previously developed by the National Commission for the Assessment of Research Activity are clearly having an effect on the Social Sciences and the field of communication. This may be attributed to the creation of the National Quality and Accreditation Evaluation Agency, its strict assessment criteria and the priority given to publications included in the Social Sciences Citation Index.

Beyond the discussion about the merits or demerits of the assessment system of teaching and research in Spain, we can state that this model has had an impact on research and above all their own researchers, whose work is conditioned by the assessment criteria. For example, we know that some journals prefer certain methodological approaches and some subject-matter is more difficult to place than others in journals indexed in the Social Sciences Citation Index. If researchers wishes to publish in these journals, they will have to adapt their work to these requirements, most often unwritten but known to all those working in the field.

Authors, aware of the value attributed by agencies to international papers, particularly in the Social Sciences Citation Index, seem to have concentrated their efforts towards getting published in these journals. This has its advantages, including positive assessments and greater potential visibility of the research and its Spanish authors. There are, however, some less favourable consequences. Giménez and Alcain ([2006](#gim06)) point out, for example, that because of the importance attached to the journals the authors tend to publish work in these journals even when they are not the most suitable ones. This would be to the detriment of Spanish journals, which are more appropriate for accepting research which deals with issues of a local nature.

The geographic and linguistic biases of the Social Sciences Citation Index are well-known ([Archambault _et al._2006](#arc06)), to which other constraints must be added, such as the limited coverage of the different disciplines. In the field of communication, for example, the 2009 edition included three specific Advertising journals (_Journal of Advertising_, _International Journal of Advertising_ and _Journal of Advertising Research_), two Health communication journals (_Health Communication_ and _Journal of Health Communication_), or scientific communication journals (_Public Understanding of Science_ and _Science Communication_), but just one unequivocally devoted to Journalism: _Journalism & Mass Communication Quarterly_, with a strong North-American emphasis . It did not include, for example, three journals which can certainly be considered as points of reference in journalism: _Journalism Studies_ (Routledge), _Journalism Practice_ (Routledge) and _Journalism: Theory, Practice & Criticism_ (Sage).

This shortage of publications on j ournalism may influence the publishing strategy of authors in the discipline. Authors are firstly penalised for not finding journals on their subject in the Social Sciences Citation Index. They are then pressurised by the agencies’ assessment criteria into sending originals to journals included in Social Sciences Citation Index but which do not form the core of the speciality, thus impeding the visibility of these papers among researchers in the field.

## <a id="author"></a>About the author

**Pere Masip** is Professor at the Blanquerna School of Communication of Ramon Llull University in Barcelona (Catalonia). He received his Bachelor's degree in Library and Information from University of Barcelona, and his PhD in Communication from Ramon Llull University. His main research subjects are online journalism, the impact of digital technologies on journalistic practice, and bibliometrics. He can be contacted at: peremm@blanquerna.url.edu

</section>

<section>

## References

<ul> 

<li id="abu10">Abu&iacute;n Vences, N. (2010). Los estudios de comunicaci&oacute;n en  Espa&ntilde;a. [Communication studies in Spain.] In H. Vivar Zurita (Ed.), <em>Los  estudios de comunicaci&oacute;n en Espa&ntilde;a: reflexiones en torno al Libro Blanco</em> (pp. 31-54). Madrid: Icono 14. </li>

<li id="ane05">Agencia Nacional de Evaluaci&oacute;n de la Calidad y Acreditaci&oacute;n. (2006). <em><a href="http://www.webcitation.org/6OfJbMPNL">Principios  y orientaciones para la aplicaci&oacute;n de los criterios de evaluaci&oacute;n </a></em>. Madrid: ANECA. Retrieved from http://www.aneca.es/var/media/551398/pep_2010_07_ppios_070515.pdf. (Archived by WebCite® at http://www.webcitation.org//6OfJbMPNL)</li>

<li id="arc06">Archambault, &Eacute;., Vignola Gagn&eacute;, &Eacute;., Côt&eacute;, G., Larivi&egrave;re, V. &amp; Gingras, Y. (2006). Benchmarking scientific output in the social sciences and humanities: the limits of existing databases. <em>Scientometrics, 68</em>(3),  329-342.</li>

<li id="ber97">Berrio, J.  (Ed.). (1997). <em>Un segle de recerca sobre comunicaci&oacute; a Catalunya. Estudi  cr&iacute;tic dels principals &agrave;mbits d’investigaci&oacute; de la comunicaci&oacute; de massa.</em> [A century of communication research in Catalonia. Critical study of the main research areas of mass communication] Bellaterra, Spain: Universitat Aut&ograve;noma de Barcelona.</li>

<li id="bor90">Borrat, H.  (1990).  El debat entre professionalistes  i comunic&ograve;legs. <em>Annals del Periodisme Catal&agrave;</em>, (16), 54-63.</li>

<li id="cas10">Castillo, A. &amp; Carret&oacute;n, M.C. (2010). Investigaci&oacute;n en comunicaci&oacute;n. Estudio bibliom&eacute;trico de las revistas de comunicaci&oacute;n en Espa&ntilde;a. [Research in communication. Bibliometric study of communication journals in Spain.] <em>Comunicaci&oacute;n y sociedad, 23</em>(2), 289-327.

</li><li id="fer10">Fern&aacute;ndez Quijada, D. (2010). El perfil de las revistas espa&ntilde;olas de comunicaci&oacute;n:  (2007-2008). [The profile of Spanish journals in communication studies.] <em>Revista Espa&ntilde;ola de Documentaci&oacute;n Cient&iacute;fica, 33</em>(4), 551-579.</li>

<li id="gar07">Garc&iacute;a  Jim&eacute;nez, L. (2007). <em>Las teor&iacute;as de la comunicaci&oacute;n en Espa&ntilde;a: un mapa sobre  el territorio de nuestra investigaci&oacute;n (1980-2006)</em>.  [Theories of communication in Spain: a map of the territory of our investigations (1980-2006)]. Madrid: Tecnos</li>

<li id="gim11">Gim&eacute;nez, E. (2011). <em>La opini&oacute;n de los expertos sobre las revistas  espa&ntilde;olas de comunicaci&oacute;n y otros indicadores de calidad.</em> [Expert opinion on Spanish journals of communication studies and other indicators of quality.] Paper presented at the  Simposio Investigar la Comunicaci&oacute;n en Espa&ntilde;a. Madrid.</li>

<li id="gim06">Gim&eacute;nez, E. &amp; Alca&iacute;n, M.D. (2006). Estudio de las revistas espa&ntilde;olas de  periodismo. [Study of Spanish journalism periodicals]. <em>Comunicaci&oacute;n y sociedad, 19</em>(2), 107-131.</li>

<li id="gim07">Gim&eacute;nez, E, Rom&aacute;n, A. &amp; Alca&iacute;n, M.D. (2007). From experimentation to coordination in the  evaluation of Spanish scientific journals in the humanities and social  sciences. <em>Research Evaluation, 16</em>(2),  137-148.</li>

<li id="hic04">Hicks, D. (2004). The four literatures of social  sciences. In, H. Moed, W. Gl&auml;nzel &amp; U. Schmoch, (Eds.). <em>The handbook of quantitative science and  technology research</em>. (pp. 473–496). Dordrecht, The Netherlands: Kluwer.</li>

<li id="jim03">Jim&eacute;nez  Contreras, E, de Moya Aneg&oacute;n, F. &amp; Delgado L&oacute;pez-C&oacute;zar E. (2003). The evolution of research activity in Spain. The impact of National  Commisssion for the Evaluation of Research Activity (CNEAI). <em>Research Policy, 32</em>(1), 123-142. Retrieved from http://eprints.rclis.org/12867/1/Research_policy.pdf  (Archived by WebCite&reg; at http://www.webcitation.org/6PPmZX5UY)</li>

<li id="jon07">Jones, D. (2007).  La comunicaci&oacute;n en el escaparate. [A window on communication.] In B. D&iacute;az Nosty, (Ed.) <em>Tendencias’07.  Medios de comunicaci&oacute;n: el escenario iberoamericano, </em>(pp. 395-408).  Barcelona, Spain: Ariel and  Madrid: Fundaci&oacute;n Telef&oacute;nica.</li>

<li id="jon98">Jones, D. (1998). Investigaci&oacute;n sobre comunicaci&oacute;n en  Espa&ntilde;a: evoluci&oacute;n y perspectivas. [Research on communication in Spain: evolution and perspectives.] <em>Zer,</em>, (5). 13-52.</li>

<li id="jon00">Jones, D. &amp; Bar&oacute;, J. (2000). <em>Investigaci&oacute;n sobre comunicaci&oacute;n en Espa&ntilde;a. Aproximaci&oacute;n bibliom&eacute;trica  a las tesis doctorales (1929-1998).</em> [Research on communication in Spain. Bibliometric approach to doctoral theses (1929-1998)]. Barcelona, Spain: ComCat</li>

<li id="lar06">Larivi&egrave;re,  V., Archambault, &Eacute;., Gingras Y. &amp; Vignola Gagn&eacute; &Eacute;. (2006). The place of serials in referencing practices: Comparing natural  sciences and engineering with social sciences and humanities. <em>Journal of  the  American Society for Information  Science and Technology. 57</em>(8),  997-1004</li>

<li id="lau05">Lauf, E. 2005. National diversity of major international journals in the  field of communication. <em>Journal of Communication</em>. <strong>55</strong>(1), 139-151.</li>

<li id="lop11">L&oacute;pez, P. &amp; Vicente, M. (2011). <em>M&eacute;todos y t&eacute;cnicas de investigaci&oacute;n  dominantes en las revistas cient&iacute;ficas espa&ntilde;olas sobre comunicaci&oacute;n  (2000-2009).</em> [Dominant research methods and techniques in Spanish communication journals (2000-2009)]. Paper presented at the Simposio Investigar la Comunicaci&oacute;n en  Espa&ntilde;a. Madrid. Retrieved from http://www.revistacomunicar.com/pdf/2011-04-Lopez-Vicente.pdf  (Archived by WebCite&reg; at http://www.webcitation.org/6PPni7niT)</li>

<li id="mar09">Mart&iacute;nez  Nicol&aacute;s, M. (2009). <a href="http://www.webcitation.org/6OfJQ5Lyd">La investigaci&oacute;n sobre comunicaci&oacute;n en Espa&ntilde;a. Evoluci&oacute;n  hist&oacute;rica y retos actuales</a>, <em>Revista Latina de Comunicaci&oacute;n Social</em>, 64, 1-14. Retreived from http://www.ull.es/publicaciones/latina/09/art/01_800_01_investigacion/Manuel_Martinez_Nicolas.html. (Archived by WebCite&reg; at http://www.webcitation.org/6OfJQ5Lyd) </li>

<li id="mar06">Mart&iacute;nez Nicol&aacute;s, M. (2006). Masa en (situaci&oacute;n) cr&iacute;tica. La investigaci&oacute;n sobre el periodismo en Espa&ntilde;a. [Critical mass (situation). Research about journalism in Spain.] <em>An&agrave;lisi</em>, (33), 137-170. </li>

<li id="mar11">Mart&iacute;nez  Nicol&aacute;s, M. &amp; Saperas, E. (2011). <a href="http://www.webcitation.org/6OfJBOs09">La investigaci&oacute;n sobre comunicaci&oacute;n en  Espa&ntilde;a (1998-2007). An&aacute;lisis de los art&iacute;culos publicados en revistas cient&iacute;ficas</a>. [Communication research in Spain, 1998-2007. An analysis of articles published in Spanish communication journals.] <em>Revista Latina de Comunicaci&oacute;n Social</em>, 66. Retrieved from http://www.revistalatinacs.org/11/art/926_Vicalvaro/05_Nicolas.html. (Archived by WebCite&reg; at http://www.webcitation.org/6OfJBOs09)</li>

<li id="mas10">Masip, P. (2010). <em>Mapping communication research in Europe (1994-2009): a bibliometric approach.</em>  Paper presented at the <em>3rd European Communication Conference</em>. ECREA,  Hamburg.</li>

<li id="mas05">Masip, P. (2005). European research in communication during the years  1994-2004: a bibliometric approach. Paper presented at the <em>1st European  Communication Conference</em>. ECCR, Amsterdam.</li>

<li id="mas11">Masip, P. &amp;  Fern&aacute;ndez-Quijada, D. (2011). Mapping  communication research in Catalonia: a comparative analysis of publication  patterns in scholarly journals (2007–2009). <em>Catalan Journal of Communication &amp; Cultural  Studies</em> <strong>3</strong>(1),  95-108</li>

<li id="mor05">Moragas, M.  de (2005). Investigaci&oacute;n de la comunicaci&oacute;n y pol&iacute;tica cient&iacute;fica en Espa&ntilde;a.  Paper presented at the Reuni&oacute;n Cient&iacute;fica de la Sociedad Espa&ntilde;ola de  Period&iacute;stica (SEP). Santiago de Compostela, Spain.</li>

<li id="moy07">Moya-Aneg&oacute;n, F., Chinchilla, Z., Vargas, B., Correa, E.,  Mu&ntilde;oz, F.J.. Gonz&aacute;lez, A. &amp; Herrero, V. (2007). Converage  analysis of Scopus: A journal metric approach. <em>Scientometrics, 73</em>(1),  53-78.</li>

<li id="ned06">Nederhof, A. J. (2006). Bibliometric monitoring of  research performance in the social sciences and the humanities: a review. <em>Scientometrics, 66</em>(1), 81-100.</li>

<li id="osc05">Osca-Lluch, J. &amp; Haba, J. (2005). Dissemination of  Spanish social sciences and umanities. <em>Journal  of Information Science, 31</em>(3),  230-237.</li>

<li id="rey04">Rey-Rocha, J. &amp; Martin-Sempere, M.J. (2004).  Patterns of the foreign contributions in some domestic vs. international  journals on earth ciences. <em>Scientometrics, 59</em>(1), 95-115.</li>

<li id="rin09">Research Information Network. (2009). <a href="http://www.webcitation.org/6OfIvvCZA"><em>Communicating knowledge: how and why researchers publish and disseminate their findings</em></a>. London: Research Information  Network. Retrieved from http://www.rin.ac.uk/system/files/attachments/Communicating-knowledge-report.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6OfIvvCZA)</li>

<li id="sor08">Soriano, J. (2008). <a href="http://www.webcitation.org/6OfIa3dRg">El efecto ANECA</a> Paper presented at the <em>Congreso internacional fundacional de la AE-IC</em>.  Santiago de Compostela. Spain. Retrieved from http://www.ae-ic.org/santiago2008/contents/pdf/comunicaciones/286.pdf. Archived by WebCite&reg; at http://www.webcitation.org/6OfIa3dRg)</li>

<li id="spa05"><em>Spanish  directory on mass communication research</em> (2005). Barcelona: Generalitat de Catalunya. Centre d’Investigaci&oacute; de la Comunicaci&oacute;. </li>

<li id="van01">Van Leeuwen, T. N., Moed, H.K.; Tijssen, R. Visser,  M.S. &amp;  Van Raan, A. (2001) Language  biases in the coverage of the Science Citation Index and its consequences for international comparasions of national research performance. <em>Scientometrics, 51</em>(1), 335-346.</li>

<li id="vil92">Villagr&aacute;, A. (1992). Scientific production of Spanish universities in  the field of social sciences and languages. <em>Scientometrics, 21</em>(1), 3-19.</li>

<li id="xif06">Xifra, J. &amp; Castillo, A. (2006). Forty years of doctoral public  relations research in Spain: a quantitative study of dissertation contribution to theory development. <em>Public Relations Review, 32</em>(3), 302-308.</li>
</ul>

</section>

</article>