<header>

#### vol. 19 no. 2, June, 2014

</header>

<article>

# Digital textbooks: school librarians' stages of concerns in initial implementation

#### [Ji Hei Kang](#authors) and [Nancy Everhart](#authors)  
School of Information, The Florida State University, 142 Collegiate Loop, Tallahassee, Florida 32306, USA

#### Abstract

> **Introduction.** A legislative mandate to adopt digital textbooks in Florida provides an opportunity to identify concerns school librarians have regarding digital textbook implementation.  
> **Method.** The stages of concern questionnaire was used to identify stages of concern among Florida school librarians about their potential role in this implementation.  
> **Analysis.** Descriptive statistics were used to describe the sample and the distribution of responses to each question. Questions were separated into seven separate stages, with key differences analysed using the stages of concern profiles and the analysis of variance test.  
> **Results.** Results indicate that participants conform to the non-user profile, which has higher unconcerned (Stage 0), informational concerns (Stage 1) and personal concerns (Stage 2). It is also found that school librarians' restrictive schedules and heavy workloads prevent them from studying new innovations - in particular digital textbooks. Intensified personal concerns (Stage 2) suggest potential resistance to digital textbooks.  
> **Conclusions.** These findings could provide librarians as educators an opportunity to establish more interest in digital textbooks and to discuss those textbooks' impact on teaching and learning. More broadly, the findings can provide some implications and recommendations to help educational policy makers and school administrators identify issues related to expanding digital textbooks.

<section>

## Introduction

With much information and rapidly developing technologies continually being made available, the educational paradigm has shifted from the traditional educational goal of transmitting and acquiring accumulated knowledge, to the 21st century goal of problem-solving and integrating and synthesizing knowledge. As a part of the new tools for flexible, interactive and creative education, digital textbooks are expected to improve the public education system by enabling a formative assessment and self-regulated learning mechanism ([Kim and Jung, 2010](#kim10)).

Beyond these pedagogical reasons, digital textbooks contain other merits. Cost-savings, increased student engagement, technology integration, health and safety, and even ecological reasons have all been cited as advantages of digital textbooks over their print counterparts and are accelerating their diffusion in schools ([Mardis, Everhart, Smith, Newsum and Baker, 2010](#mar10)). In fact, the promise of digital learning has prompted calls to action from major education organisations and policy makers starting with a mission set forth by U.S. Education Secretary Arne Duncan ([Lederman, 2012](#led12)) for all schools to transition as quickly as possible to digital textbooks. According to a European Commission report, European, Denmark, Hong Kong, Japan, Singapore and South Korea have all embarked on digital textbook projects ([Kampylis _et al._, 2013](#kam13)).

These initiatives indicate the growing importance of digital textbooks in education and have implications for school libraries and school librarians. Although formal roles regarding digital textbook implementation have yet to be identified for school librarians, they are expected to play crucial roles in integrating digital textbooks into schools ([Dotson and Jones, 2011](#dot11); [Johnston, 2012](#joh12); [Mardis and Everhart, 2011](#mar11); [Mardis _et al._, 2010](#mar10); [Martin 2011](#mart11)). Responsibilities for school librarians might logically evolve from current expertise in identifying educational goals, collecting relevant resources, organising content, collaborating with teachers and serving as professional developers ([Mardis _et al._, 2010](#mar10)). Promoting reading and its importance may be expanded into the digital textbook realm.

> In an age when many teacher-librarians are not sure about the continued relevance of their promotion of reading and love of books, e-books and digital textbooks may represent a fresh way to continue advocacy for the importance of reading... as well as for the teacher-librarian's crucial leadership role in technology integration ([Mardis and Everhart, 2011, p. 8](#mar11)).

However, while comprehensive digital textbook adoption in Florida is imminent ([Mardis and Everhart, 2011](#mar11)), there are few research studies that provide insight into the reaction a wide-sweeping mandate might evoke in the educators who will be impacted. Existing research has dealt with technological aspects of digital textbooks rather than human and social aspects, such as perception of, motivation to use, or satisfaction with digital textbooks ([Hill, 2010](#hil10); [Kim and Jung, 2010;](#kim10) [Kim, Park, Seo and Lee, 2011](#kim11)). More specifically, there are no empirical inquiries into the relationship between digital textbooks and school librarians, a group that is frequently confronted with the challenge of keeping up with technological changes.

This research focuses on determining the levels of concern that librarians from kindergarten through grade 12 (K-12) have regarding digital textbooks, and does so by applying the concerns based adoption model. In the first part of this paper, the researchers discuss previous studies focusing on educators' concerns about innovation and their perceptions of digital textbooks. The second part explains both Concerns Based Adoption Model and stages of concern as theoretical frameworks. Thirdly, after examining the methodology for conducting a survey, the researchers present the findings from their investigation. Finally this paper concludes by suggesting implications for both school librarians and school administrators.

## Literature review

### Educators' concerns about innovation

It is natural to resist the uncertainty that comes with innovation and institutional change ([Rogers, 1983](#rog83)). In particular, education is the area of greatest resistance to innovation ([Germann and Sasse, 1997](#ger97)). Consequently, educational-change theorists assume that change is a developmental process in which continuous training and leadership are required ([Hall and Hord, 1987](#hal87)).

The concerns of classroom teachers have been widely studied, and the studies have covered a diverse spectrum of educational innovations. These innovations include not only general technology integration, but also specific technological innovations such as microcomputers and Web 2.0\. Most studies have utilised the concern based adoption model to measure educators' concerns and levels of use when implementing innovations ([Stroh, 1999](#str99)). The concern based adoption model was developed to measure three areas of perceptions among educators: stages of concern, levels of use, and innovation configurations.

Regardless of the kind of innovation and the level of awareness among educators, most study findings are that educators' concerns lie in the _self_ domain at the unconcerned, informational and personal stages, which are lower stages that address technology ([Stroh, 1999](#str99); [Yang and Huang, 2008](#yan08)). This phenomenon reveals that experienced educators who infuse technology into their pedagogy may need as many as five years to fully master that new technology ([Yang and Huang, 2008](#yan08)). In addition, researchers have found that educators' concerns about technology itself, its implementation, and external factors such as classroom readiness fall into various patterns and levels. Therefore, in order to understand educators' multifarious concerns, and to respond to practical as well as academic needs, variations in educators' levels of concern should be researched over time.

### Educators' perceptions of digital textbooks

As the implementation of digital textbooks has increased, several recent studies have begun to investigate educators' perception of digital textbooks. Kim, Choi, and Kim ([2012](#kim12)) found that teachers have positive perceptions of digital textbooks. The authors surveyed 157 pilot school teachers in South Korea to identify the factors influencing the use and acceptance of multimedia-based digital textbooks. Their findings were that teachers are slightly inclined toward optimism about digital textbooks, and that teachers rank positively toward all seven constructs: enjoyment (mean=3.82), educational impact (mean=3.77), perceived benefit (mean=3.74), intention to use (mean=3.49), ease of use (mean=3.37), interaction (mean=3.15) and content quality (mean=3.06). In another study, Chaohua, Guang, Wei, Xiaoxuan and Ronghuai ([2013](#cha13)) further illustrate the complexity of teachers' perceptions toward digital textbooks by confirming that most teachers interviewed held positive attitudes toward digital textbooks. The authors found that most teachers were motivated by functions such as an attractive interface, instant feedback, note taking and the ability to play interactive content. Conversely, teachers said they had a hard time collecting multimedia materials and were desperate for a technician to assist them in handling hardware and software.

However, there is also a negative side to these positive perspectives. Lam and Tong ([2012](#lam12)) observed that pre-service teachers were much more conservative with using e-textbooks. Only one third of participants reported that they would support the use of e-textbooks in local secondary schools. The researchers discerned that the participants were concerned about equity, maintenance, administrative and practical issues, student safety and student distraction.

To the best of our knowledge, there is no empirical study identifying school librarians' perceptions toward or concerns about digital textbooks. Such studies would provide various implications for both school libraries and the many schools where digital textbooks are forthcoming.

## Theoretical framework

In order to answer the research questions, this study integrates the concern based adoption model as a model, and the stages of concern questionnaire in concern based adoption model as an instrument. The concern based adoption model is widely used as both theory and methodology to identify an individual's concern and level of engagement when implementing innovations and new technologies ([Anderson, 1997](#and97); [Hall and Hord, 1987](#hal87); [Hollingshead, 2009](#hol09)). In the early 1970s, staff members at the Research and Development Center for Teacher Education at the University of Texas developed the concern-based adoption model. Until the centre closed in the mid-1980s, its personnel continued to develop the model. Throughout this period, as various educational changes and innovations have been attempted, the concern based adoption model has been actively used in research, coining the term _concern based adoption model practitioners_ ([Anderson, 1997](#and97)). Since the late 1980s, the concern based adoption model has been applied to evaluate various educational innovations.

In the concern based adoption model, concern is defined as '_the composite representation of the feeling, preoccupation, thought, and consideration given to a particular issue or task_' ([Hall and Hord, 1987, p. 58](#hal87)). A concern is a psychological action based on personal make-up, knowledge, and experience when a person faces new experiences or environments and perceives the need for improvement or change ([Hall and Hord, 1987](#hal87)); thus each person has different kinds and levels of concerns. Indeed students, teachers, librarians, principals, administrators, and all people, not just those involved in education, have concerns.

The concern based adoption model includes the concept of _stages of concern_, which is the main concept employed by this study. It also incorporates concepts of _levels of use_, _innovation configurations_, _change facilitator styles_, and _interventions_ ([Anderson, 1997](#and97); [Dirksen, 1997](#dir97)). This theory rests on several assumptions proposed by Hall and Hord ([1987](#hal87)): (a) change is a process, not an event; (b) an individual accomplishes change; (c) the change process is an extremely personal experience; (d) individuals go through various stages of change; (e) the availability of a client-centered prescriptive model can develop the individual's capability with staff development; and (f) people need to be changed in an adaptive and systematic way in which the process of change is monitored.

The seven stages of concern are listed in Table 1\. Table 1 indicates dimensions and seven stages of concern and concerns in each stage. The researchers developed the statements provided in the table from typical participant responses. Although these seven stages have distinct characteristics, they are not exclusive of one another. The model assumes that when individuals confront an innovation, they become interested in all stages, yet they are most interested in a specific stage.

<table><caption>Table 1: Stages of concern and typical statements. (Source: George, Hall and Stiegelbauer, 2013, p. 8)</caption>

<tbody>

<tr>

<th>Dimension</th>

<th>Stage</th>

<th colspan="2">Concern & typical statements</th>

</tr>

<tr>

<td>Unrelated</td>

<td>0</td>

<td>Unconcerned</td>

<td>Little concern or involvement with the initiation  
?I?m not worried about digital textbooks coming to our school."</td>

</tr>

<tr>

<td rowspan="2">Self</td>

<td>1</td>

<td>Informational</td>

<td>Gains more information about the innovation  
?I would like to know about digital textbooks."/ ?I need to join the digital textbook task force team to get more information on it."</td>

</tr>

<tr>

<td>2</td>

<td>Personal</td>

<td>Uncertainty about the personal requirement toward the new program  
"How would the use of digital textbooks impact me?"/ ?I?m concerned about the changes that I may need to make in my routines."</td>

</tr>

<tr>

<td>Task</td>

<td>3</td>

<td>Management</td>

<td>Managing skills such as scheduling and integrating  
"How would I ensure that all of our students had the proper hardware?" "Would we have enough broadband capacity?"</td>

</tr>

<tr>

<td rowspan="3">Impact</td>

<td>4</td>

<td>Consequence</td>

<td>Tests innovation on students  
"How are digital textbooks supposed to improve student learning?"/ "I would like to reflect on students' feedback on their digital textbook use."</td>

</tr>

<tr>

<td>5</td>

<td>Collaboration</td>

<td>Shares interests with others in the new program  
"I am concerned about relating the use of digital textbooks to teachers."/ "Will digital textbooks enhance or limit collaboration with teachers?"</td>

</tr>

<tr>

<td>6</td>

<td>Refocusing</td>

<td>Focuses on pursuing more benefits of the innovation or exploring an alternative program  
"I have some ideas to improve digital textbook use." "Some digital textbooks are better than others."</td>

</tr>

</tbody>

</table>

The seven stages of concern consist of three different types of concerns: self, task, and impact. _Self_ concerns are low level concerns which '_evolve around general characteristics, effects, requirement of use and financial or status implications of the innovation_' ([Samiei, 2008, p. 26](#sam08)). _Task_ concerns are associated with managing the process and specific duties that may result. _Impact_ concerns reflect a more advanced level of involvement than the other type of concern. These dimensions are also provided later in the findings in Table 2\. At the initial phase of an innovation's use, educators typically have intensive self concerns (Stage 1, informational; Stage 2, personal). They would like to know more about the innovation and discover any changes that the innovation might bring. Educators will also be curious about when the innovation will be implemented, who will be charge of the change, and how it is expected to work. Even though educators may not state it openly, they may still have intense personal concerns in the pioneering phases. Teachers may also worry about their ability, responsibility, and potential mistakes or changes in their work routine.

_Task concerns_ (Stage 3, management) are likely to be most intense in the latter part of innovation preparation ([Hord, Rutherford, Huling-Austin and Hall, 2006](#hor06)). In the early stages of use, educators will consider management concerns such as meeting various student needs and learning styles, maximising the effectiveness of learning materials, accounting for prep-time prior to instruction and arranging classroom procedures and materials.

When the innovation catches on across schools and its influence permeates the educational culture, all educators may develop intense _impact concerns_ (Stage 4, consequence; Stage 5, collaboration; Stage 6, refocusing). Only a few educators will reach impact level, and their concerns may include the outcome of activities related to the innovation, how they will work together with others, and how they may find better ways to apply the innovation.

According to Anderson ([1997](#and97)), the concern based adoption model is a powerful tool for evaluating top-down and bottom-up innovations. It proves an innovation's real worth when educators introduce it into instructional practices or curriculum. Particularly, this method is applied during the time in which these innovations in curriculum and instruction are put into practice?innovations include cooperative learning, language learning, constructivist instruction for science and math education, and new educational technologies. This study uses the concern based adoption model to discover school librarians' stages of concern in the earliest phases of digital textbook implementation, and to determine the implications for teachers as well as school stakeholders.

## Method

The purpose of this study is to identify the level of concerns that school librarians have with respect to the adoption of digital textbooks. This study identifies and describes concerns of school librarians in Florida during the initial phases of digital textbook implementation. It is framed by the following research questions:

> RQ1: What are the initial stages of concern that Florida school librarians have expressed regarding digital textbooks, as measured by the concerns-based adoption model?  
> RQ2: What are the highest and second highest stages of concern for school librarians, and what main factors influence these two marginal stages?  
> RQ3: What correlations among stages can be discerned among initial stages of concerns?

A survey using the _stages of concern_ questionnaire was applied as a means to identify the answers to the research questions. This questionnaire has been widely used because of its usefulness in helping to explain developing patterns for perceived concerns and attitudes that individuals experience. It was created by Hall, George and Rutherford in 1979 and revised by George, Hall and Stiegelbauer in 2006\. The questionnaire has several advantages: it is convenient and simple for a researcher to manipulate as well as for participants to answer because it consists of thirty-five items and uses a seven-point Likert scale; it is designed to be completed in ten to fifteen minutes, and its strong reliability and validity have been confirmed. The questionnaire's test-retest correlation results range from 0.65 to 0.86 of Cronbach alpha. Moreover, its internal consistency is ranged from 0.64 to 0.83 of Cronbach alpha ([Samiei, 2008](#sam08)).

The population of this study is school librarians who work in elementary and secondary school libraries in the State of Florida, which became the first state to mandate digital textbooks by enacting regulations in 2011 that required digitized instructional materials, including digital textbooks ([Florida SB 2120, 2011](#flo11)). Florida plans to transition to an all-digital environment, including the exclusive use of digital textbooks, by the 2015-16 school year ([Mardis and Everhart, 2011](#mar11)). Given this approaching deadline, it is vital to identify the concerns of school librarians in Florida not only to assist them in becoming successful leaders, but also to help school administrators identify issues regarding the implementation. The study employs a purposive sample method, requiring that the researchers select subjects based on some characteristic ([Schutt, 2008](#sch08)). Researchers recruited participants through a promotional email targeted to Florida Association for Media in Education members which yielded a response rate of 17.5%. Because of the relatively modest number of participants (n=170), the results of this study are not generalisable, but rather provide discovery data in this initial phase of digital textbook implementation.

The survey was conducted for one month in fall 2012 using the online encrypted version of the stages of concern questionnaire. The Southwest Educational Development Laboratory in Austin, Texas holds the copyright to the questionnaire and charges $.50 per completed questionnaire. Throughout the stages of concern questionnaire the standard term _innovation_ was replaced with _digital textbooks_ to customise the instrument.

The stages of concern questionnaire can be interpreted using peak stage score interpretation, first and second high stage score interpretation, and profile interpretation ([Hall and Hord, 1987](#hal87); [Samiei, 2008](#sam08)). To analyse the data, all responses were calculated in the two ways recommended by Hord, Rutherford, Huling-Austin, and Hall ([2006](#hor06)), which are raw scores and percentile scores. Raw scores of each stage of concern were averages of all five survey items that belong to each stage. The resulting percentiles were determined by the raw score to percentile conversion table providing the prior percentiles for each stage by ([Hall, George and Rutherford, 1977](#hal77)). The percentiles of the different levels of concern were calculated by matching the average score of each stage to the established percentiles. In this study researchers averaged raw scores for statistical analysis because the percentile scores do not have equal intervals ([Shoulders and Myers, 2011](#sho11)).

## Findings

_Analysis of research question one: What are the initial stages of concern that Florida school librarians have expressed regarding digital textbooks, as measured by the Concerns-Based Adoption Model?_

Overall, this sample's profile for concerns about digital textbooks reflects patterns of the typical non-user in the stages of concern questionnaire profile ([Hall _et al._, 1977](#hal77); [Hord _et al._, 2006](#hor06)). The non-user concern profile is the most common, having the highest value on self concerns (Stages 0, 1 and 2) and relatively lower value on impact concerns (Stages 4, 5 and 6). Moreover, this non-user profile demonstrates that school librarians? concerns regarding digital textbooks are very much in the initial phase. If the innovation is propitious and proper support for implementation is guaranteed, the plotted concern profile will trend toward the higher stages because the stages of concern questionnaire considers that individual educators advance their concerns over the course of stages ([Hord _et al._, 2006](#hor06)).

<figure>

![Figure 1: School librarians' stages of concern profile regarding digital textbooks](../p625fig1.jpg)

<figcaption>Figure 1: School librarians' stages of concern profile regarding digital textbooks</figcaption>

</figure>

Another noteworthy finding, in conformity with Hall, George, and Rutherford ([1977](#hal77)) is it can be reasonably assumed that there may be potential resistance to the implementation of digital textbooks. If Stage 2 is more intense than Stage 1, and Stage 6 is tailing up, Hall, George, and Rutherford ([1977](#hal77)) consider that this profile presents potential resistance. This digital textbook profile (Figure 1) sheds light on the first reason for that negative disposition. As Figure 1 shows, the percentile of Stage 2 (78%) is slightly higher than that of Stage 1 (75%). This intensive Stage 2 demonstrates Florida school librarians have more personal concerns about digital textbooks. Florida school librarians also exhibited that they are more concerned about digital textbooks over impact and responsibility that accompany digital textbooks than over obtaining indispensable information about digital textbooks. The concern profile having a prominent Stage 2 describes respondents as having a level of disagreement or doubt regarding digital textbooks. In order to proceed forward, concerns in Stage 2 should be reduced before school librarians appreciate the innovation ([Hall _et al._, 1977](#hal77)). In other words, ideally school librarians' personal concerns would be resolved before they fully adopt digital textbooks. In terms of digital textbooks, school librarians' roles, reward systems in their schools, decision-making processes, any additional conflicts and financial issues should be settled by the principal agent?in this case the State of Florida. Fortunately, the Stage 6 tail goes down at the end of the curve, suggesting that participants do not have any other alternatives competing with digital textbooks.

_Analysis of research question two: What are the highest and second highest stages of concern for school librarians, and what main factors influence these two marginal stages?_

The peak stage bespeaks the greatest intensity for the corresponding stage of concern. In terms of the peak stage score that Figure 1 reveals, it is apparent that concerns were predominantly high in Stage 0 among school librarians. As mentioned above, this phenomenon is normal for non-users. The 91% intensity for Stage 0 concerns clearly indicates that school librarians are intensely concerned with digital textbooks at Stage 0 (i.e., with their lack of exposure to digital textbooks) ([Hall _et al._, 1977](#hal77)). Hall et al. ([1977](#hal77)) conclude that a high percentile of Stage 0 intensity indicates either that experienced users, in this case, school librarians, have other concerns not related to the innovation, or that the responders are non-users just becoming aware of the innovation.

In this study, it would be more likely that the high scores for Stage 0 indicate that the non-user group is uncertain about other issues concerning digital textbooks and not the textbooks themselves, partially because the survey was taken before the implementation of those textbooks and also because school librarians have some experience with e-books.

In the stages of concern questionnaire, each item's average has a range from 0 to 7, and each stage consists of five items. Stage 0 is composed of questions 3, 12, 21, 23 and 30, as shown in Table 2\. An analysis of variance was conducted to determine differences of concerns among these five questions. As a whole, the analysis reveals significant differences (_p_ = 0.000) among the items. Item 3 is significantly different from item 21, 23 and 30 (_p_ = 0.000) and item 12 is significantly different from item 21, 23 and 30 (_p_ = 0.000). As _post-hoc_ comparisons for one-way analysis of variance, Duncan's multiple range test was applied, and it categorized into three groups: item 3 and 12; item 23; and item 21 and 30.

<table><caption>Table 2: The five questionnaire items for Stage 0</caption>

<tbody>

<tr>

<th>Question no.</th>

<th>Mean</th>

<th>Std. Dev.</th>

<th>Questions</th>

</tr>

<tr>

<td>3</td>

<td>1.92</td>

<td>1.625</td>

<td>I am more concerned about another innovation.</td>

</tr>

<tr>

<td>12</td>

<td>2.27</td>

<td>1.954</td>

<td>I am not concerned about digital textbooks at this time.</td>

</tr>

<tr>

<td>21</td>

<td>4.16</td>

<td>2.271</td>

<td>I am completely occupied with things other than digital textbooks.</td>

</tr>

<tr>

<td>23</td>

<td>3.56</td>

<td>2.232</td>

<td>I spend little time thinking about digital textbooks.</td>

</tr>

<tr>

<td>30</td>

<td>4.33</td>

<td>2.353</td>

<td>Currently, other priorities prevent me from focusing my time on digital textbooks.</td>

</tr>

</tbody>

</table>

A group of items 30 and 21 has higher mean scores, and thus has a profound effect on the higher raw scores for Stage 0\. These two items confirm that the reason school librarians report high concerns for Stage 0 is that their sizeable workloads prevent them from studying the digital textbook innovation; rather, they are indifferent to digital textbooks.

The second-highest stage is Stage 2, and all five questionnaire items were answered evenly. All items acquired more than 4 points, meaning that, on average, participants agree that these items are more than "somewhat true of me now" (2~4), trending toward "very true of me now" (5~7).

<table><caption>Table 3: The five questionnaire items for Stage 2</caption>

<tbody>

<tr>

<th>Question no.</th>

<th>Mean</th>

<th>Std. Dev.</th>

<th>Questions</th>

</tr>

<tr>

<td>7</td>

<td>4.09</td>

<td>2.224</td>

<td>I would like to know the effect of digital textbooks on my professional status.</td>

</tr>

<tr>

<td>13</td>

<td>4.77</td>

<td>2.123</td>

<td>I would like to know who will make the decisions in the new system.</td>

</tr>

<tr>

<td>17</td>

<td>4.24</td>

<td>2.027</td>

<td>I would like to know how my teaching or administration is supposed to change.</td>

</tr>

<tr>

<td>28</td>

<td>4.51</td>

<td>2.024</td>

<td>I would like to have more information on time and energy commitments required by digital textbooks.</td>

</tr>

<tr>

<td>33</td>

<td>4.42</td>

<td>1.975</td>

<td>I would like to know how my role will change when I am using digital textbooks.</td>

</tr>

</tbody>

</table>

Table 3 indicates that school librarians want to know about the person in charge of implementing digital textbooks (item 13) and the additional amount of time and effort required to integrate digital textbooks (item 28).

_Analysis of research question three: What correlations among initial stages of concerns can be discerned?_

To determine the correlation among Stage 0 through Stage 6, researchers created correlation matrixes, with each stage's raw score, applying SPSS statistical software. The Pearson product-moment correlation coefficient measures how strong a linear association is between two variables on a matrix and confirms the high correlation among the stages. Table 4 displays the Pearson product-moment correlation coefficient matrix among the seven stages.

<table><caption>Table 4: Correlation coefficients for relations between stages of concern</caption>

<tbody>

<tr>

<th> </th>

<th>Stage 0</th>

<th>Stage 1</th>

<th>Stage 2</th>

<th>Stage 3</th>

<th>Stage 4</th>

<th>Stage 5</th>

<th>Stage 6</th>

</tr>

<tr>

<td>Stage 0</td>

<td>-</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Stage 1</td>

<td>0.023</td>

<td>-</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Stage 2</td>

<td>-0.006</td>

<td>0.751**</td>

<td>-</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Stage 3</td>

<td>0.223**</td>

<td>0.420**</td>

<td>0.592**</td>

<td>-</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Stage 4</td>

<td>-0.076</td>

<td>0.474**</td>

<td>0.537**</td>

<td>0.509**</td>

<td>-</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Stage 5</td>

<td>-0.180*</td>

<td>0.505**</td>

<td>0.565**</td>

<td>0.302**</td>

<td>0.691**</td>

<td>-</td>

<td> </td>

</tr>

<tr>

<td>Stage 6</td>

<td>-0.014</td>

<td>0.317**</td>

<td>0.413**</td>

<td>0.539**</td>

<td>0.663**</td>

<td>0.530**</td>

<td>-</td>

</tr>

</tbody>

</table>

The strongest correlation that was uncovered is a correlation between Stage 1 and Stage 2, _r_(168) = 0.75, _p_ &lt; 0.01\. This indicates that a very strong positive linear relationship exists between Stages 1 and 2\. In other words, if school librarians have high concerns in Stage 1, they are more likely to have high concerns in Stage 2\. Inversely, this finding can be analysed in the opposite way. For example, if school librarians have fewer concerns in Stage 1, they are more likely to have fewer concerns in Stage 2.

## Implications and conclusion

As leaders in digital technology adoption, school librarians will have to address digital textbooks as another important issue for their schools. As a first step toward researching the relationship between school librarians and digital textbooks, the present study identifies the current stages of concern that school librarians have associated with digital textbooks. Early results indicate that the stages of concern are still in the initial phase, as survey participants responded in line with the typical non-user profile, their highest values falling in Stages 0, 1 and 2 and their lower values in Stages 4, 5 and 6\. The overall pattern of stages possibly foreshadows some negative reactions in the early stage of digital textbook implementation. To avoid this opposition, issues concerning school librarians' roles in the implementation process should be resolved. This reflects uncertainty in Florida for an impending mandate where no funding or professional development has been provided to date.

Analysing the individual items of the stages of concern questionnaire enables researchers to determine the reasons why specific stages have more intensity. Stage 0 (unconcerned) reveals that a large number of school librarians are paying no regard to digital textbooks because of their other duties. Also, it was found that there are widespread concerns about school librarians' personal tasks (Stage 2) such as making decisions about implementation, investing additional time and effort, adjusting to a changing role in the school, adjusting to the changing roles for teachers and administrators and accounting for digital textbooks in their professional roles. This result aligns with Johnston's ([2011](#joh11)) research which found that time constraints are the most frequently reported barrier to integration, and that "exclusion from leadership opportunities" is the second. For the successful introduction of digital textbooks, it is necessary for school librarians to secure more time, as well as to be enabled to take a leadership role.

Hord _et al._ ([2006](#hor06)) suggest a stages of concern intervention that could serve as an enabling tool. Applying their model, in Stage 0 more school librarians would need to involve themselves in consultations and discussions about digital textbook implementation. By doing so, this information and acknowledgement would instill the importance of digital textbooks while exerting every effort to minimise incorrect and misguided information.

When it comes to the intervention, the study demonstrates a very strong correlation between Stages 1 and 2, Stages 4 and 5, and Stages 4 and 6\. This finding is critical in many practical ways. As a solution to lessen concerns in Stages 1 and 2, school administrators may intervene in either of these stages, and the level of concern for the other stage is likely to decrease. For instance, a webinar providing accurate information about digital textbooks, which is an intervention for Stage 1, is especially likely to reduce personal concerns, which falls under Stage 2\. Moreover, Table 4 describes even stronger correlations. The data for correlation between Stage 4 and Stage 5 (_r_ = 0.69), and correlation between Stage 4 and Stage 6 (_r_ = 0.663) are shown to be highly correlated.

Thus it is reasonable to propose that the authorities offer integrated interventions related to these stages rather than focusing on only one stage. For example, Stage 2 is the second-highest stage; intervention in Stage 2 would lessen the personal concerns of school librarians. This study suggests that when the efforts to lessen the personal concerns (Stage 2) are applied, school librarians' information concerns (Stage 1) can be reduced. Therefore, if school districts provide encouragement and address personal concerns about digital textbook use (Stage 2) while providing accurate information regarding digital textbooks at the same time (Stage 1), that effort would prove doubly effective. The same rules can be applied to other relationships. If the authority brings in effective facilitators to resolve school librarians' consequence concerns (Stage 4), it may offer facilitations for Stages 5 and 6\. For instance, school principals could provide educators with opportunities to visit other schools where digital textbooks are successfully used to solve consequence concerns (Stage 4), it may be appropriate if school districts match educators who are interested in collaboration (Stage 5) and help teachers access the resources to allow them to modify digital content (Stage 6).

As an initial inquiry which examines school librarians' concerns about innovative educative technology, this research can offer a starting point for further studies. Since the present research addresses human-resource issues pertaining to the diffusion of digital textbooks, this research can impact the key areas of cognitive research on people influenced by innovation and human factors related to digital textbooks such as leadership, collaboration, and social norms. In addition, the findings could provide librarians as educators an opportunity to establish more interest in digital textbooks and to discuss those textbooks' impact on teaching and learning. Identifying and understanding the issues from the point of view of school librarians can help inform human resource departments. More broadly, the findings can provide some implications and recommendations to help educational policy makers and school administrators identify issues within school libraries that require further attention and resources.

## <a id="authors"></a>About the authors

**Ji Hei Kang** is a doctoral candidate at the School of Information within the College of Communication and Information, at Florida State University, USA. She is conducting research on how school librarians have reacted to the adoption of technological innovations. She plans to continue researching digital textbooks by analysing school librarians? stages of concern and level of use. She is also studying mobile augmented reality in order for information specialists to promote this new technology for children and library users with easier and more useful mobile augmented reality services. She can be contacted at: [jk11e@my.fsu.edu](mailto:jk11e@my.fsu.edu)  
**Nancy Everhart** is a Professor at the School of Information within the College of Communication and Information at Florida State University, USA where she also directs the school library media program and the PALM Center, an interdisciplinary research centre focused on school librarian leadership in technology integration. She has authored over 100 publications and is the winner of numerous research awards. Dr. Everhart served as President of the American Association of School Librarians in 2010-2011 and conducted a Vision Tour visiting outstanding school libraries in thirty-eight states. She can be contacted at: [everhart@fsu.edu](mailto:everhart@fsu.edu)

</section>

<section>

## References

<ul>
<li id="and97">Anderson, S. E. (1997). Understanding teacher change: revisiting the concerns-based adoption model. <em>Curriculum Inquiry, 27</em>(3), 331-367.
</li>
<li id="cha13">Chaohua, G., Guang, C., Wei, C., Xiaoxuan, Y. &amp; Ronghuai, H. (2013). Potential issues on initiatively utilizing e-textbooks in K-12 classrooms. In Proceedings of the 2013 IEEE 13th International Conference on Advanced Learning Technologies, (pp. 314-318). Washington, DC: IEEE Computer Society
</li>
<li id="dir97">Dirksen, D. J. (1997). <a href="http://www.webcitation.org/6PcWmaRPK">Utilizing the concerns-based adoption model to facilitate systemic change</a>. In Jerry Willis, Jerry D. Price, Sara McNeil, Bernard Robin and Dee Anna Willis, (Eds.), <em>Proceedings of Society for Information Technology &amp; Teacher Education International Conference 1997</em> (pp. 1064-1067). Chesapeake, VA: AACE. Retrieved from http://www.editlib.org/p/47256 (Archived by WebCite® at http://www.webcitation.org/6PcWmaRPK)
</li>
<li id="dot11">Dotson, K. B. &amp; Jones, J. L. (2011). Librarians and leadership: the change we seek. <em>Journal of the American Society for Information Science,</em> <strong>17</strong>(2), 78-85.
</li>
<li id="flo11">
<a href="http://www.webcitation.org/6PcXGJLRN">SB 2120 K-12 education funding, ? 1006.29-30.</a> Florida Senate, Session 2011. (Amendment by Senator Simmons). (2011).Retrieved from http://www.flsenate.gov/Session/Bill/2011/2120/Amendment/625648/PDF (Archived by WebCite® at http://www.webcitation.org/6PcXGJLRN)
</li>
<li id="geo13">George, A. A., Hall, G. E. &amp; Stiegelbauer, S. M. (2013). <em>Measuring implementation in schools: the stages of concern questionnaire</em> (3rd ed.). Austin, TX: SEDL.
</li>
<li id="ger97">Germann, P. J. &amp; Sasse, C. M. (1997). Variations in concerns and attitudes of science teachers in an educational technology development program. <em>Journal of Computers in Mathematics &amp; Science Teaching, 16</em>(2-3), 405-423.
</li>
<li id="hal77">Hall, G. E., George, A.A. &amp; Rutherford, W. L. (1977). <em>Measuring stages of concern about the innovation: a manual for use of the SoC Questionnaire.</em> Austin, TX: Texas University at Austin Research and Development Center for Teacher Education.
</li>
<li id="hal87">Hall, G. E. &amp; Hord, S. M. (1987). <em>Change in schools: facilitating the process.</em> Albany, N.Y: State University of New York Press.
</li>
<li id="hil10">Hill, R. (2010). Turning the page: forget about those bulky backbreakers, digital textbooks are the future. <em>School Library Journal, 56</em>(10), 24-27.
</li>
<li id="hol09">Hollingshead, B. (2009). The concerns-based adoption model: a framework for examining implementation of a character education program. <em>NASSP Bulletin, 93</em>(3), 166-183.
</li>
<li id="hor06">Hord, S. M., Rutherford, W. L., Huling-Austin, L. &amp; Hall, G. E. (2006). <em>Taking charge of change.</em> Austin, TX: SEDL.
</li>
<li id="joh11">Johnston, M. P. (2011). <em>School librarians as technology integration leaders: enablers and barriers to leadership enactment.</em> Unpublished doctoral dissertation, Florida State University, Tallahassee, Florida, USA
</li>
<li id="joh12">Johnston, M. P. (2012). Connecting teacher librarians for technology integration leadership. <em>School Libraries Worldwide, 18</em>(1), 18-33.
</li>
<li id="kam13">Kampylis, P., Law, N., Punie, Y., Bocconi, S., Brecko, B., Han, S., . . . Miyake, N. (2013). <a href="http://www.webcitation.org/6pcxuhuvs"><em>ICT-enabled innovation for learning in Europe and Asia.</em></a> Seville, Spain: Joint Research Centre of the European Commission. Retrieved from http://ftp.jrc.es/EURdoc/JRC83503.pdf. (Archived by WebCite® at http://www.webcitation.org/6PcXUhuVS)
</li>
<li id="kim10">Kim, J. H.-Y. &amp; Jung, H.-Y. (2010). South Korean digital textbook project. <em>Computers in the Schools, 27</em>(3-4), 247-265.
</li>
<li id="kim12">Kim, M.-R., Choi, M.-A. &amp; Kim, J. (2012). Factors influencing the usage and acceptance of multimedia-based digital textbooks in pilot school. <em>KSII Transactions on Internet and Information Systems, 6</em>(6), 1707-1717.
</li>
<li id="kim11">Kim, S. J., Park, K. C., Seo, H. S. &amp; Lee, B. G. (2011). Measuring the service quality of digital textbook for u-learning service. <em>International Journal of Technology Enhanced Learning, 3</em>(4), 430-440.
</li>
<li id="lam12">Lam, P. &amp; Tong, A. (2012). Digital devices in classroom - hesitations of teachers-to-be. <em>The Electronic Journal of e-Learning, 10</em>(4), 387-395.
</li>
<li id="led12">Lederman, J. (2012, February 10). Arne Duncan calls for textbooks to become obsolete in favor of digital. <em>Huffington Post Education.</em> Retrieved from http://www.huffingtonpost.com/2012/10/02/education-chief-wants-tex_0_n_1933469.html (Archived by WebCite® at http://www.webcitation.org/6PcXbJpG3)
</li>
<li id="mar11">Mardis, M. &amp; Everhart, N. (2011). Digital textbooks in Florida: extending the teacher-librarians' reach. <em>Teacher Librarian, 38</em>(3). 8-11.
</li>
<li id="mar10">Mardis, M., Everhart, N., Smith, D., Newsum, J. &amp; Baker, S. (2010). <a href="http://www.webcitation.org/6PcXmqHPr">From paper to pixel: digital textbooks and Florida's schools.</a> Tallahassee, FL: PALM (Partnerships Advancing Library Media) Center. Retrieved from http://files.eric.ed.gov/fulltext/ED522907.pdf (Archived by WebCite® at http://www.webcitation.org/6PcXmqHPr)
</li>
<li id="mart11">Martin, V. D. (2011). <em>Perceptions of school library media specialists regarding their practice of instructional leadership.</em> Unpublished doctoral dissertation, Walden University, Minneapolis, Minnesota, USA.
</li>
<li id="rog83">Rogers, E. M. (1983). <em>Diffusion of innovations</em> (3rd ed.). New York, NY: Free Press.
</li>
<li id="sam08">Samiei, A. (2008). <em>The concerns of elementary educators with the diffusion of information and communication technology in schools.</em> Unpublished doctoral dissertation, Simon Fraser University, Vancouver, British Columbia, Canada.
</li>
<li id="sch08">Schutt, R. K. (2008). <em>Investigating the social world: the process and practice of research</em>. (6th ed.). Thousand Oaks, CA: SagePublications.
</li>
<li id="sho11">Shoulders, C. W. &amp; Myers, B. E. (2011). <a href="http://www.webcitation.org/6PdDn70vY">An analysis of national agriscience teacher ambassadors' stages of concern regarding inquiry-based instruction.</a> <em>Journal of Agricultural Education, 52</em>(2), 58-70. (Archived by WebCite® at http://www.webcitation.org/6PdDn70vY)
</li>
<li id="str99">Stroh, E. A. W. (1999). <em>Teachers' concerns about technology as a classroom innovation.</em> Unpublished doctoral dissertation, Indiana State University, Bloomington, Indiana, USA
</li>
<li id="yan08">Yang, S. C. &amp; Huang, Y.-F. (2008). A study of high school English teachers? behavior, concerns and beliefs in integrating information technology into English instruction. <em>Computers in Human Behavior, 24</em>(3), 1085-1103.
</li>
</ul>

</section>

</article>