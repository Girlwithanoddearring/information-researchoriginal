<header>

#### vol. 18 no. 1, March, 2013

</header>

<article>

# The nature and constitution of informal carers' information needs: what you don't know you need is as important as what you want to know

#### [Basil Alzougool](#author) and [Shanton Chang](#author)  
_Department of Computing and Information Systems, The University of Melbourne, Australia_  
#### [Kathleen Gray](#author)  
_Health and Biomedical Informatics Research Unit,_ _Faculty of Medicine, Dentistry and Health Sciences_ & _Department of Computing and_ _Information Systems, The University of Melbourne, Australia_

#### Abstract

> **Background.** We explore the nature and aspects of the information needs of informal carers. A comprehensive understanding of these needs is important because it is the first step to meeting these needs effectively.  
> **Aim.** We report on an empirical validation of a framework for understanding the information needs of carers, composed of four need states: recognised-demanded, recognised-undemanded, unrecognised-demanded, and unrecognised-undemanded.  
> **Methods.** A qualitative study employed semi-structured interviews with nine informal carers of diabetic children. The study also helped to develop items for the subsequent questionnaire that was used to measure the information needs state with 198 informal carers (over 18 years old).  
> **Results.** The existence of four states of information needs was confirmed empirically. The occurrence and frequency of the four states may vary.  
> **Conclusions.** The framework provides a basis for investigating, understanding and classifying the information needs of informal carers and information needs in general. It also assists in designing and providing information that may meet the needs of informal carers more effectively.

<section>

## Introduction

Informal carers provide unpaid healthcare services for people who are unable to care for themselves; carers are usually parents, children, or relatives. They have played, and continue to play, a major role in assisting and supporting formal healthcare services, and carry out many primary care duties ([Odhiambo _et al._ 2003](#odh03); [Heaton 1999](#hea99)). Over the last few decades there has been increasing evidence and acknowledgment of the importance of informal carers to persons needing care, to the healthcare system and to the community, particularly in the developed world ([Jorgensen _et al._ 2009](#jor09); [Keefe _et al._ 200](#kee08); [Buckner and Yeandle 2007](#buc07); [McKune _et al._ 2006](#mck06); [Australian Institute of Health and Welfare (AIHW) 2004](#aih04)). In addition, the experience of becoming an informal carer is very challenging and requires vast amount of information to manage the caring role effectively. Research has confirmed the importance of information for informal carers; it is one of the many things that informal carers need along with such things as practical and emotional support, and finance (e.g., [Yedidia and Tiedemann 2008](#yed08); [Zapart _et al._ 2007](#zap07)). However, while research on the information needs of informal carers is increasing, this paper argues that much of the research uses terms such as _information needs_, _demand for information_ and _information seeking behaviour_ interchangeably, which can lead to fractured understandings of the needs of carers.

Adams _et al._ ([2009](#ada09)) indicate that few researchers have provided a definition of information needs in their studies. Those who have, define information needs as a recognition and expression of a lack of information (e.g., [Beaver and Witham 2007](#bea07)). Adams _et al._'s ([2009](#ada09)) review of papers published between 1998 and 2008 on theinformation needs of partners and family members of cancer patients found that_information need_was poorly conceptualised in the literature**.** Moreover, they argued that very few studies used conceptual theories, models or frameworks to assist in exploring and explaining the comprehensiveness of information needs and to guide the empirical work. Therefore, the question we address here is:

_How can we understand the information needs of carers comprehensively?_

Alzougool _et al._ ([2008](#alz08)) previously drew on existing and related research on the information needs of informal carers to develop a framework of information needs state of carers. The framework encompassed four states of information needs that could be used for investigating, understanding and classifying these needs comprehensively. These states are: recognised-demanded, recognised-undemanded, unrecognised-demanded and unrecognised-undemanded information needs. This paper presents the validation of the framework using sequential empirical data (qualitative followed by quantitative data) collected from informal carers. Finally, the findings are discussed and some conclusions drawn about areas for future research.

## Background

While many researchers have investigated information needs, they have all use the term _information needs_ as a generic concept that can interchangeably refer to, and encompass, various ways of describing information. This paper attempts to differentiate the various ways that the current literature presents information needs and provides a summary of some of these terms in Table 1.

<table><caption>Table 1: A summary of some of information needs terms in informal carers literature</caption>

<tbody>

<tr>

<th>Terms</th>

<th>Examples</th>

<th>Example references</th>

</tr>

<tr>

<td>

**Information topics**</td>

<td>Examples of these topics in the literature are information about disease and treatment, information about available services, and information about coping strategies.</td>

<td>

[Andreassen _et al._ 2005](#and05), [Collier _et al._ 2001](#col01), [De Rouck and Leys 2009](#der09), [Docherty _et al._ 2008](#doc08), [Hummelinck and Pollock 2006](#hum06), [Ikemba _et al._ 2002](#ike02), [Kendall _et al._ 2004](#ken04), [Pain 1999](#pai99), [Rees 2000](#ree00), [Rees _et al._ 2003](#ree03), [Richardson _et al_. 2007](#ric07), [Thon and Ullrich 2008](#tho08), [Wong _et al._ 2002](#won02).</td>

</tr>

<tr>

<td>

**Information types**</td>

<td>Examples of these types in the literature are facts, advice, opinions, and explanations.</td>

<td>

[Cleary _et al._ 2005](#cle05), [Richardson _et al._ 2007](#ric07).</td>

</tr>

<tr>

<td>

**Information formats**</td>

<td>Examples of these formats in the literature like oral (spoken word), written (textual), lists, tables, figures, and images.</td>

<td>

[Beaver and Witham 2007](#bea07), [Cleary _et al._ 2005](#cle05), [Gustafsson _et al._ 2010](#gus10), [Hoffmann and McKenna 2006](#hof06), [Rahi _et al._ 2003](#rah03), [Rees 2000](#ree00), [Smith _et al._ 2004](#smi04), [Wong _et al._ 2002](#won02) . </td>

</tr>

</tbody>

</table>

While all the research listed in Table 1 used the term _information needs_ to describe the focus of the studies, it became clear that each researcher or group of researchers was looking at quite different aspects of information needs:

*   _Information topics_ refers to the categories and sub-categories that are given to the needed information by informal carers. Prior research concentrated mainly on exploring the information topics (e.g., [Docherty _et al._ 2008](#doc08)). For example, Wong _et al._ ([2002](#won02)) found that informal carers needed information about the management of pain, weakness and fatigue, and causes of cancer.
*   _Information types_ refers to the kinds of needed information by informal carers. These types maybe describe either the needed information in general, such as factual information, or the topics of needed information, such as factual information about the condition or advice on different treatment options. Prior research has rarely concentrated on exploring the information types (e.g., [Cleary _et al._ 2005](#cle05)). However, Richardson _et al._ ([2007](#ric07)) found that informal carers perceived receiving practical help and advice as one of the benefits of interventions that targeted them. 
*   _Information formats_ refers to the presentation and methods of information provision of needed information for informal carers. Prior research has explored information formats to a small degree (e.g., [Hoffmann and McKenna 2006](#hof06)) in conjunction with a study of information sources (e.g., [Cleary _et al._ 2005](#cle05); [Rees 2000](#ree00)). Other studies have explored the formats of needed information by informal carers from the viewpoint of occupational therapists (e.g., [Gustafsson _et al._ 2010](#gus10)). Beaver and Witham ([2007](#bea07)) found that written information was highly valued by informal carers.

Researchers rarely distinguished between information needs on the one hand and these information topics, types and formats on the other. While most researchers are interested in identifying the information topics, types and formats (e.g., [Krishnasamy _et al._ 2007](#kri07); [Cooper and Urquhart 2005](#coo05)), they rarely clarify the nature of information needs or what fundamentally constitutes an actual need.  

Due to the lack of differentiation between these different aspects of information needs, few researchers have tried to conceptualise the information needs of informal carers. In one example, Hepworth ([2004](#hep04)) developed a framework based on the findings of previous research in information behaviour in general, and analysed the factors affecting the information seeking behaviour of informal carers. This framework did not analyse the information needs of informal carers in great depth; rather it studied the information demanded by informal carers.  

In general, the aspects used by most studies to capture the information needs of informal carers concentrate on their active and passive information seeking behaviour. Prior research (e.g., [Janda _et al._ 2006](#jan06); [Salminen _et al._ 2004](#sal04)) has identified some aspects of information needs of informal carers, including the information received by carers, information that carers actively seek, are lacking, or recognise as a need, and information that they demand from an information source or express to a researcher. While some aspects were used extensively (for example, information that informal carers lack (e.g., [Brotherton and Abbott 2009](#bro09); [Hummelinck and Pollock 2006](#hum06))), others have received less attention from researchers (for example, information that informal carers expect (e.g., [Yedidia and Tiedemann 2008](#yed08))). These aspects may indeed help us to understand part of the nature of information needs. However, the lack of differentiation between the different aspects of information needs makes it hard for all aspects to be addressed comprehensively, leading to the dominant practice of providing only information that is sought by the informal carers (e.g., [Porter and Edirippulige 2007](#por07)) or of providing as much information as possible leading to information overload ([Lambert and Loiselle 2007](#lam07)).  

Moreover, although researchers used the above aspects to study the information needs of informal carers, they found that:

1.  informal carers were not always aware of what information they might need (e.g., [Kendall _et al._ 2004](#ken04));
2.  they avoided relevant information that satisfies some topics of required information (e.g., [Hummelinck and Pollock 2006](#hum06));
3.  they used their personal experience and knowledge (e.g., [Buri and Dawson 2000](#bur00)); and
4.  many informal carers scarcely express their information needs and some may not even know how to articulate those needs (e.g., [Hummelinck and Pollock 2006](#hum06)).

These unexpected findings may also shed light on the nature of information needs. However, very little research has defined and explored these unexpected findings empirically and/or used these findings to elicit the required information. Furthermore, little research investigated the information needs of informal carers in terms of the varying importance of these needs over time and during different situations that constituted the experience of being an informal carer.  

In sum, it is important to clarify the nature of information needs and the possible aspects that constitute the information needs ([Shenton and Dixon 2004](#she04)) before identifying the information topics, types and formats. As suggested by the above discussion, a comprehensive understanding of the nature and potential aspects of information needs does not seem to exist as yet. Any such comprehensive understanding should include all potential aspects of these information needs (e.g., recognised, expressed, lacking, sought, unsought, ignored, received, currently held, and not yet recognised needs). Therefore, we propose a framework for aspects of the information needs of informal carers that can capture and assist in understanding their needs comprehensively.

## A framework of the information needs state of informal carers

In this section, we present the framework of the information needs state of informal carers adopted from our previous study ([Alzougool _et al._ 2008](#alz08)). The primary purpose of the framework is to provide a way that assists in understanding, including and classifying the diversity of potential aspects of information needs of informal carers. The framework is empirically testable, and also provides a way to investigate the information needs more fully.  

To develop the framework, Alzougool _et al._ ([2008](#alz08)) introduce a new definition of information needs. This defines information needs as _information that is essential to individuals as a result of the context in which they act_ (here, the caring context). This essential information may be recognised or not recognised; it may be demanded or not demanded; individuals may have it or lack it. This definition takes account of various states of information needs. For example, parents may or may not recognise that they need information about their child's illness, but this does not eliminate the fact that they need information about this important matter in order to understand and manage that illness responsibly. We can also apply this example to the demand for needed information:  parents may recognise this information need but at the same time they may or may not want (demand) that information; they may demand it because it helps them understand the illness of their child or they may not demand it because it may increase their worry. By using these states of information need, a researcher can capture and identify a more complete picture of information needs, including a lack of information, for any group of individuals. Based on this definition, the framework (Figure 1) divides the information needs of informal carers into four states:

<figure>

![A framework of the information needs state  of informal carers](../p563fig1.png)

<figcaption>Figure 1: A framework of the information needs state of informal carers</figcaption>

</figure>

### The recognised-demanded state

In this state, informal carers are aware of their information needs and are ready to take actions to obtain the information that meets these needs. These actions may include several forms of behaviour, such as seeking the information, accepting the offered information, or finding it by chance. Informal carers take these actions because either they lack this information or they want more information, and/or to confirm or disconfirm information that they already have. Most information behaviour research has investigated information needs that fall into this state by studying information seeking behaviour (e.g., [Attfield _et al._ 2006](#att06); [Andreassen _et al._ 2005](#and05); [Wilson 1999](#wil99)); however, this exploration is limited to sought and received information.

### The recognised-undemanded state

In this state, informal carers are aware of their information needs but do not desire to have information to fulfil these needs. They take actions to ignore this essential but undesired information, by not seeking it, ignoring received information or that found by chance, recreating it themselves, or using their own knowledge. Informal carers take these actions because either they lack the information and they do not want to have it for whatever reason (e.g., it will increase their worries), or they own this information and they do not want to confirm or disconfirm it for whatever reason (e.g., they satisfy and trust the information they already have). Researchers have rarely explored the information needs that fall into this state.

### The unrecognised-demanded state

In this state, informal carers are not aware of their specific information needs, but they have the desire to have information. By taking actions, informal carers hope to realise what these needs are. Informal carers may intentionally become aware of these needs by themselves during the process of seeking the available information; subsequently they will demand information that fulfils these needs. This state requires informal carers to explore the information system in order to recognise what information they need, and once these needs are recognised then the informal carers will choose to fulfil them. Information providers can promote and suggest such essential information to individuals in order to facilitate them in recognising these needs ([Shenton and Dixon 2004](#she04)). Most information behaviour research has investigated information needs that fall into this state in the same way as those in the recognised-demanded state, by studying information seeking behaviour. However, these explorations have been limited to investigating the sought information.

### The unrecognised-undemanded state

In this state, informal carers are not aware of their information needs, nor do they desire information. Even though informal carers may be in a context where it would be possible for them to become aware of these information needs accidentally (e.g., while looking for other information, by listening to other people, or by receiving information from information providers), they do not recognise or demand that these needs be met. The information needs that fall into this state are yet to be explored systematically.

## Method

Two empirical studies were employed to test and validate the usefulness, soundness and appropriateness of the framework of the information needs state of carers. The qualitative study (study 1) was used to gain extensive in-depth and real data on the information needs state, and to test and refine these states as well as develop questions that were used in the questionnaire to measure the information needs states in the quantitative study. These states were then revalidated using the quantitative research approach (study 2) with a large sample of informal carers. The following section presents the data collection method, analysis and results of the qualitative study.

## Study 1: The qualitative study

The qualitative study employed a semi-structured interview with nine informal carers of diabetic children as a sample group of informal carers. This study used a convenience and purposive sample, identifying experienced informal carers who were willing and able to give to a researcher a full description of experiences that could be analysed to build a comprehensive picture of their information needs. Potential participants were recruited through not-for-profit, non-clinical organizations that interact with informal carers or children with diabetes, and through personal contact. The participants were asked to answer questions regarding their everyday experiences and tasks as carers to derive implications of their information needs. For each task or experience, the participants were asked to answer further sub-questions that covered the information they used and recognised, their information seeking behaviour, their prioritisation of information and the sources of their information, and why they make certain choices around their identified information needs. Such questions about information requirements were used to elicit descriptions of their information needs, and how they thought about and behaved with regard to these needs.  

The interview transcripts were analysed by a researcher using two strategies: inductive and deductive content analysis ([Berg 2004](#ber04)) and pattern-matching logic ([Yin 2002](#yin02)). The inductive content analysis was firstly used to discover patterns and categories in the collected data. It involved reading and rereading the selected text, coding, preliminary categorisation, and further classified data into categories (experiences and scenarios) related to the information needs state. The accuracy of these categories was verified by working backwards and forwards between the data and the coding scheme. Data were also analysed using the pattern-matching logic described by Yin ([2002](#yin02)). This technique was used to compare some patterns across the four states of information needs (as predicted from the information needs state in the conceptual framework and the related literature) with the pattern of states of information needs induced from data collected through the semi-structured interviews (experiences and scenarios). The four states of information needs were used as a template and compared with which the empirical results that emerged from the nine informal carers' interviews. In order to facilitate this process, a number of scenarios that depicted each state were identified, and used to label sentences or paragraphs in the data. Following this process, deductive content analysis was used to define the categories (experiences and scenarios) that were derived from inductive content analysis and pattern-matching logic into the four states of information needs in the conceptual framework: recognised-demanded, recognised-undemanded, unrecognised-demanded and unrecognised-undemanded. Two additional researchers, working separately, evaluated this analysis for accuracy and completeness.  

The nine carers reflect a range of caring experiences. Six of these informal carers were mothers and three were fathers of a diabetic child. Seven of the informal carers were aged 30-45 years and two of them were aged above 45 years. Seven of these carers were also living in urban areas and two in rural areas. Of the nine informal carers, five identified their ethnicity as African, three as White Anglo-Saxon, and one as Arabian. The average caring experiences of these carers were approximately 6 years. Six of the informal carers were caring for their sons, two were caring for their daughters, and one was caring for both a son and daughter.

### Findings of the qualitative study

Analysis of the collected data supported the existence of the four states of information needs and these four states are empirically initially identified based on the qualitative data. The following sub-sections present a description of each state of information needs of informal carers as found in the qualitative study. Each state of information needs also includes a number of scenarios under which this state occurs. Therefore, one or more representative excerpts from the raw data were used to show that each state of information needs and its related scenarios were supported by data from the nine informal carers. This in turn shows that these four states of information needs are important in achieving a more complete understanding of information needs of informal carers.

#### The recognised-demanded state

In this state, informal carers recognise their information needs and want to have information that satisfies their needs. The recognised-demanded state may occur in either of two scenarios.  

The first scenario is when informal carers face a new situation during their caring journey. They may be able to articulate their information needs as well as want to have information that satisfies these needs at that point of time. This new situation may raise their awareness of some of the needed information. In turn, they can precisely articulate the needed information depending on their personal characteristics, such as self-efficacy, confidence and previous experience. In addition, depending on the context of the needed information (e.g., urgency), they also want to have relevant information that satisfies this need at that point of time.  

> _He_ [her son] _constantly wants to have them_ [injections] _in his thighs, so I probably sought information from doctors about that… I didn't start to think about what will happen in school until the months prior to him starting kinder_[garten]) _really._ **(Carer 4)**

The second scenario is when informal carers already have information that satisfies their needs and they want to confirm or disconfirm the information they have at the time and/or they want to have additional information about it. Therefore, they already know exactly what information they need and they want to obtain more information or they want to confirm or disconfirm the information they have.  

> _We are still not totally satisfied with what he_ [his son] _used_ [insulin] _and we would love to find more… they_ [hospital staff] _did explain to you but it is not in detail…you need more guidelines…_ [So I] _search the Internet._ **(Carer 7)**

Apart from the above point, this scenario also occurs when informal carers already have information that satisfies their information needs but they want to have the most up-to-date information to add to what they already have. They are able to articulate their information needs.  

> _We've satisfied with the information … from the hospital in the beginning of diagnosis but … day after day … you need to update about the cure how is developing._ **(Carer 6)**

#### The recognised-undemanded state

In this state, informal carers recognise their information needs but they do not want to have information that might satisfy these needs. The recognised-undemanded state may occur in either of two scenarios.  

The first scenario is when informal carers face situations during their caring journey and they do not currently want to have information that might satisfy these needs. This situation may raise their awareness of some of the needed information. They in turn precisely articulate this needed information depending on their personal characteristics, such as self-efficacy, confidence and previous experience. However, depending on the context of this needed information (e.g., cost), they do not want to have relevant information that satisfies this need at the point of time.  

> _I could go and make an appointment to go and see my diabetes educator, but it all comes down to … getting in the car, driving an hour, making an appointment … and you think about it and you think ‘it's not worth it … you tend to talk yourself into just … manage it on your own until something happens ... from my own experience._ **(Carer 2)**

The second scenario is when informal carers realise that they need to know information to deal with and manage tasks, activities and situations but they do not want to have information that satisfies these needs at the current time, and/or they do not want to confirm or disconfirm the information they already have, and/or they do not want to obtain additional information around information they already have for various reasons (e.g., they think the information they have is adequate, they do the same routine tasks and activities). They also ignore the information they were offered or came across for various reasons (e.g., they cannot face up to it).  

> _Do I need information now? … not so much on the daily_ [activities]_… it's more of an ongoing thing … but not so much on the daily_ [activities]. **(Carer 5)**

> _When my daughter has been diagnosed, they taught me how to test it but for my son_ [when diagnosed] … _I knew how to do that…  they did not teach me_. **(Carer 3)**

#### The unrecognised-demanded state

In this state, informal carers do not recognise their information needs but they feel that they want to have information that satisfies these needs. The unrecognised-demanded state may occur in either of two scenarios.  

The first scenario is when informal carers do not recognise the information need but they are offered information that satisfies these needs or they come across information that satisfies these needs by chance while they are reading a book, watching TV, or interacting with another person.  

> _At the hospital ... when they were discharging us, they were telling us_ [about the pump] _…_ [I did not ask them] _because we are not medical professionals… the input information that require for him as a child was something very important._ **(Carer 9)**

The second scenario is when informal carers face new situations during their caring journey. They may not be able to articulate what they need to know but they feel that they want to have information that satisfies their information needs at that point of time. Because the shortage of this information is considered risky to informal carers, they in turn explore what information is available in order to identify their needs, and find and have information that satisfies those needs. This new situation may raise their awareness that they need some information to deal with and manage it but they cannot precisely articulate this needed information; this is dependent on their personal characteristics (e.g., self-efficacy). However, depending on the context of this needed information (e.g., urgency), they want to have relevant information that satisfies this unrecognised need at the point of time. Therefore, they start seeking information in order to identify, find and have any relevant information that satisfies this need.  

> [Upon diagnosis] _we almost lost we did not know what to do… yes, I was going to know every thing about it_ [diabetes]_… I did not know anything about it before …_ [I] _knew it after reading_ [about it after diagnosis]_… sometimes I go to the library and get books about diabetes but not for certain information but just general information just read it about the whole sickness_ [diabetes]_… Of course yes_ [I needed this information] _because we have never known any thing about it before._ **(Carer 8)**

#### The unrecognised-undemanded state

In this state, informal carers neither recognise their information needs nor want to have information that satisfies these needs. The unrecognised-undemanded state may occur in any of three scenarios.  

The first scenario is when informal carers face new situations and they do not know that they need to know information to deal with these situations in the first place. Subsequently they cannot decide if they want to have information that satisfies the need or not. This scenario may also occur if they are focused on the work of caring rather than wondering about what information is needed, as they do not have the time to do so.  

> _I didn't_ [realise she needs this information] _I probably never thought really that I need  information … because you don't think, it's not about me, it's about him … I've never really thought I need information to help me cope with this … I've just thought that this is life._ **(Carer 4)**

The second scenario is when informal carers neither recognise their information needs nor are offered information that satisfies these needs or they come across information that satisfies these needs by chance. This may happen at any point of time from the beginning of the diagnosis up to facing new situations during the caring journey. These new situations enable them to have an idea of what they might need to know. However, as time goes on and new situations are faced, informal carers may start to realise that they need to know some information that they do not have yet, and then they make the decision to have information that satisfies these needs or not, based on the situations and the informal carers themselves.  

> _He_ [her son] _has been suffering urticarias for a couple of weeks. This rash … occurs when he exercises or becomes hot … information that would be valuable would be regarding side effects of insulin_ … [I] _spoke with Pharmacist, searched the net … endocrinologist … GP … this information would be required when insulin is first administered so as to enable the carer to be alert in case of allergic/adverse reactions to insulin …_ [the time I realised this information is needed] _probably a little bit further on … after couple of months ... I was probably too much shocked to realise_ [it before that]_._ **(Carer 1)**

### Validity and reliability of the findings of the qualitative study

As stated in previous sections, there are four states of information needs of informal carers: recognised-demanded, recognised-undemanded, unrecognised-demanded and unrecognised-undemanded. Instances of all four states of information needs were identified and observed (as indicated by the scenarios) in the lived experiences of the nine informal carers. None of the nine carers was found to face only one, two or three of the states. Additionally, no scenarios were identified that reflected any new states of information needs of informal carers. Therefore, this suggests that these four states of information needs are necessary, sufficient (and mutually exclusive) to understanding the information needs state. It also means that the four states are independent, and no more than these four states of information needs are necessary to understanding the information needs state. With regard to the generalised inferences of these findings, the qualitative study was extended by a quantitative study of a larger sample of informal carers regardless of the condition of the persons needing care (see next section).

## Study 2: The quantitative study

The information needs states that were initially identified in nine in-depth qualitative interviews were further tested, with a questionnaire completed by 198 informal carers (over 18 years old) of all kinds who provide unpaid healthcare services for other people (e.g., parents, children, relatives, and friends). These people are unable to care for themselves for different reasons, such as being frail elderly, having significant disability, or acute or chronic diseases, or physical or mental illnesses. Since there was no pre-existing specific measures scale in the literature, measures were developed based on the findings from the scenarios identified in the qualitative study in order to further validate the information needs state.  

The questionnaire included items that measured six variables:

1.  recognition of information needs: operationalised into five items that encompass recognising the information needs without any help, based on carers' knowledge, experiences and skills, and recognising the information needs after they are offered or come across information that may satisfy these needs;
2.  non-recognition of information needs: operationalised into six items that encompass not recognising the information needs and carers realise this themselves, they realise this with the assistance of others and/or accidentally, and they neither realise this themselves nor with the assistance of others;
3.  recognised-demanded state: operationalised into six items that encompass the carers seeking out relevant information purposely and actively, having relevant information that they are provided with, having more information about and/or confirming or disconfirming (double-checking) information they already have, and generally wanting to have any relevant information;
4.  recognised-undemanded: operationalised into five items that encompass holding the information need itself at a point of time, ignoring relevant information after they are offered it by others or come across it by accident, and creating the needed information based on their ideas and experience without consulting any other source of information;
5.  unrecognised-demanded: operationalised into six items that encompass exploring what information is available for any topic that might be relevant to carers and having any relevant information they are offered by others and that they come across by chance; and
6.  unrecognised-undemanded: operationalised into six items that encompass avoiding (not thinking of) the information need itself, giving little attention to any kind of available information that might be relevant to them, neglecting information that they are offered by others or that they come across by accident, and depending on their own ideas to deal with and manage a situation without thinking about any kind of information.

It is worth noting that the two variables of recognition and non-recognition of information needs were added to the questionnaire because the demand and non-demand of information needs are a consequence of recognition and non-recognition. Therefore, it was important to establish the existence of the recognition and non-recognition of information needs first.  

Participants responded to the statements in the questionnaire on a five-point Likert-type scale, ranging from _never_ (1) to _always_ (5). There were two additional points on the scale: _Yes but I cannot say how often_ and _Maybe but I cannot say for sure_ in order to identify participants who were not sure about the frequency of the occurrence of these scenarios. These were placed as the last two choices of the rating responses. Participants therefore responded by selecting one of the seven response choices. However, the results from the use of two additional points scale were included in the descriptive statistics only and were excluded from further analysis. Two formats of the anonymous questionnaire were designed and used to collect the required data (online and paper-based). The questionnaire was in English, and was divided into different sections for easy reading and completion.

### Pilot test of the questionnaire

A panel of seven postgraduate research students in the discipline of Information Systems was asked to review a draft of the questionnaire by applying it to their own experience of needing information for their research. As a result of their feedback, some modifications were made. The revised questionnaire was then reviewed by six informal carers. The three who responded commented that overall the questions were fairly self-explanatory, readable, understandable and sensible to them as carers. Moreover, two senior researchers refined a number of drafts of the questionnaire during the process of questionnaire development and pilot testing.

### Administration of the questionnaire

The online questionnaire was hosted and stored on the Survey Monkey website. The questionnaire was anonymous and the IP addresses of respondents were not stored. The survey was online for approximately a three month period, from 4 May to 31 July 2009\. The paper-based questionnaire, including an invitation to participate and copies of the questionnaire itself, was mailed with a stamped, self-addressed return envelope to the members of some carers' organisations with the assistance of the organisations themselves.

### Sample

For the online questionnaire, the potential participants were recruited through an email announcement that provided them with a link to the online questionnaire. Many not-for-profit, non-clinical organisations that interact with informal carers worldwide (e.g., Australia, Canada, UK, USA), and online forums and discussion groups on the Internet were contacted to promote and pass the link of the questionnaire to the carers they interact with. The paper-based questionnaire was administered only in Australia, and not-for-profit, non-clinical carer organisations were contacted to assist in distributing the paper-based questionnaire. The target population consisted of all informal carers, regardless of the condition of the persons needing care, anywhere in the world. This was done in order to ensure maximum variations of experiences in different situations and cultures as recommended by Lincoln and Guba ([1985](#lin85)). The online respondents were self-selected volunteers, and the anonymous nature of online questionnaires makes it impossible to verify whether the respondents meet the inclusion criteria. Therefore, it is not known whether these responses are representative of informal carers as a whole because there is no information on the total number of informal carers in the world. However, online questionnaires are comparable to traditional questionnaires in terms of validity and reliability of the data, as found by Eysenbach and Wyatt ([2002](#eys02)).  

In total, 249 informal carers participated in the study, of whom, 220 and 29 participated in the online and paper-based formats of the questionnaire respectively. After discarding 51 questionnaires for various reasons (e.g., too much missing data, respondents were not carers), there were 198 questionnaires suitable for analysis. The descriptive statistics for the respondents are displayed in Table 2\.

<table><caption>Table 2: Description of questionnaire respondents</caption>

<tbody>

<tr>

<th rowspan="2">Variables</th>

<th rowspan="2">Codes</th>

<th colspan="2">Total sample (n=198)</th>

<th rowspan="2">Missing values</th>

</tr>

<tr>

<th>Freq</th>

<th>%</th>

</tr>

<tr>

<td rowspan="2">Sex of the carers</td>

<td>Female</td>

<td>173</td>

<td>88.3</td>

<td rowspan="2">2</td>

</tr>

<tr>

<td>Male</td>

<td>23</td>

<td>11.7</td>

</tr>

<tr>

<td rowspan="5">

**Age group of the carers**</td>

<td>18-25 yrs</td>

<td>4</td>

<td>2.0</td>

<td rowspan="5">2</td>

</tr>

<tr>

<td>26-35 yrs</td>

<td>19</td>

<td>9.7</td>

</tr>

<tr>

<td>36-45 yrs</td>

<td>60</td>

<td>30.6</td>

</tr>

<tr>

<td>46-55 yrs</td>

<td>47</td>

<td>24.0</td>

</tr>

<tr>

<td>Above 55 yrs</td>

<td>66</td>

<td>33.7</td>

</tr>

<tr>

<td rowspan="5">

**Highest level of education carers have completed**</td>

<td>Primary school</td>

<td>4</td>

<td>2.0</td>

<td rowspan="5">1</td>

</tr>

<tr>

<td>Secondary school</td>

<td>51</td>

<td>25.9</td>

</tr>

<tr>

<td>Post-secondary technical or vocational training</td>

<td>55</td>

<td>27.9</td>

</tr>

<tr>

<td>University undergraduate degree</td>

<td>50</td>

<td>25.4</td>

</tr>

<tr>

<td>University postgraduate degree</td>

<td>37</td>

<td>18.8</td>

</tr>

<tr>

<td rowspan="6">

**Population of the area carers live in**</td>

<td>Settlement up to 5,000 people</td>

<td>26</td>

<td>13.3</td>

<td rowspan="6">2</td>

</tr>

<tr>

<td>Settlement up to 10,000 people</td>

<td>18</td>

<td>9.2</td>

</tr>

<tr>

<td>Small town up to 25,000 people</td>

<td>24</td>

<td>12.2</td>

</tr>

<tr>

<td>Large town up to 100,000 people</td>

<td>35</td>

<td>17.9</td>

</tr>

<tr>

<td>Major city up to 1,000,000 people</td>

<td>39</td>

<td>19.9</td>

</tr>

<tr>

<td>Major city greater than 1,000,000 people</td>

<td>54</td>

<td>27.6</td>

</tr>

<tr>

<td rowspan="5">

**Country carers live in**</td>

<td>Australia</td>

<td>84</td>

<td>43.1</td>

<td rowspan="5">3</td>

</tr>

<tr>

<td>United Kingdom</td>

<td>48</td>

<td>24.6</td>

</tr>

<tr>

<td>Canada</td>

<td>25</td>

<td>12.8</td>

</tr>

<tr>

<td>United States of America</td>

<td>34</td>

<td>17.4</td>

</tr>

<tr>

<td>Others</td>

<td>4</td>

<td>2.1</td>

</tr>

<tr>

<td rowspan="2">

**Language carers mainly speak at home**</td>

<td>English</td>

<td>187</td>

<td>95.9</td>

<td rowspan="2">3</td>

</tr>

<tr>

<td>Others</td>

<td>8</td>

<td>4.1</td>

</tr>

<tr>

<td rowspan="5">

**Length of time as a carer**</td>

<td>Less than 1 year</td>

<td>6</td>

<td>3.1</td>

<td rowspan="5">3</td>

</tr>

<tr>

<td>1-5 yrs</td>

<td>72</td>

<td>36.9</td>

</tr>

<tr>

<td>6-10 yrs</td>

<td>41</td>

<td>21.0</td>

</tr>

<tr>

<td>11-15 yrs</td>

<td>28</td>

<td>14.4</td>

</tr>

<tr>

<td>More than 15 yrs</td>

<td>48</td>

<td>24.6</td>

</tr>

<tr>

<td rowspan="2">

**Type of caring role**</td>

<td>Sole carer of the person(s) carers are caring for</td>

<td>128</td>

<td>67.0</td>

<td rowspan="2">7</td>

</tr>

<tr>

<td>Member of a team who cares for the person(s) carers are caring for</td>

<td>63</td>

<td>33.0</td>

</tr>

<tr>

<td rowspan="3">

**Number of person(s) carers is caring for**</td>

<td>One person</td>

<td>150</td>

<td>76.5</td>

<td rowspan="3">2</td>

</tr>

<tr>

<td>Two persons</td>

<td>37</td>

<td>18.9</td>

</tr>

<tr>

<td>Three persons and more</td>

<td>9</td>

<td>4.6</td>

</tr>

<tr>

<td rowspan="3">

**Number of conditions carers is caring for regardless of the number of persons needing care**</td>

<td>One Condition</td>

<td>128</td>

<td>65.3</td>

<td rowspan="3">2</td>

</tr>

<tr>

<td>Two Conditions</td>

<td>48</td>

<td>24.5</td>

</tr>

<tr>

<td>Three Conditions and more</td>

<td>20</td>

<td>10.2</td>

</tr>

<tr>

<td rowspan="6">

**Carers relation to the person(s) they are caring for**</td>

<td>Immediate family member</td>

<td>181</td>

<td>92.3</td>

<td rowspan="6">2</td>

</tr>

<tr>

<td>Other relative</td>

<td>5</td>

<td>2.6</td>

</tr>

<tr>

<td>Neighbour</td>

<td>1</td>

<td>.5</td>

</tr>

<tr>

<td>Friend</td>

<td>6</td>

<td>3.1</td>

</tr>

<tr>

<td>Immediate family member and other relative</td>

<td>2</td>

<td>1.0</td>

</tr>

<tr>

<td>Immediate family member and friend</td>

<td>1</td>

<td>.5</td>

</tr>

<tr>

<td rowspan="6">

**Amount of time carers spend caring per month on average**</td>

<td>10 hours or less</td>

<td>7</td>

<td>3.6</td>

<td rowspan="6">3</td>

</tr>

<tr>

<td>11-20 hours</td>

<td>13</td>

<td>6.7</td>

</tr>

<tr>

<td>21-50 hours</td>

<td>24</td>

<td>12.3</td>

</tr>

<tr>

<td>51-100 hours</td>

<td>21</td>

<td>10.8</td>

</tr>

<tr>

<td>101- 250 hours</td>

<td>24</td>

<td>12.3</td>

</tr>

<tr>

<td>251 hours or more</td>

<td>106</td>

<td>54.4</td>

</tr>

</tbody>

</table>

### Data analysis

Data analysis involved two distinct stages using SPSS 15.0 for Windows. The first stage checked the issues related to the validity and reliability of the developed questionnaire by performing factor analysis and Cronbach's alpha tests as specified in Tabachnick and Fidell ([2007](#tab07)) and Pallant ([2007](#pal07)). This stage also aimed to find out whether the information needs states exist and are distinct from each other. The second stage analysed the existence of the information needs states by performing descriptive analysis as described in Pallant ([2007](#pal07)) to find the occurrence of the four states of information needs.

### Findings of factor analysis, validity and reliability of the questionnaire

Exploratory factor analysis was carried out by undertaking three main steps ([Pallant 2007](#pal07); [Costello and Osborne 2005](#cos05)): factorability (checking the suitability of the data for factor analysis), extracting the factors, and rotating and interpreting the extracted factors. Checking the suitability of the data for factor analysis was done by testing the correlation between the items (the recommended value of 0.3 ([Coakes and Steed 2001](#coa01))), Bartlett's test of sphericity (at p smaller than 0.05), Kaiser-Meyer-Olkin (KMO) measure of sampling adequacy (the recommended value of 0.6 ([Garson 2011](#gar11); [Coakes and Steed 2001](#coa01); [Norusis 1994a](#nor94a))), and the adequacy of sample size (the recommended value is 10 respondents to 1 item ([Coakes and Steed 2001](#coa01); [Fabrigar _et al._ 1999](#fab99))). The eigenvalues (greater than 1), scree plot (looking at the plot and locating the distinct break, at which the curve becomes nearly parallel to the horizontal axis), and parallel analysis were investigated to determine the number of extracted factors.

Extraction of factors was done by determining both the method for extracting the factors and the number of underlying factors necessary to represent the data ([Garson 2011](#gar11); [Pallant 2007](#pal07); [Costello and Osborne 2005](#cos05); [Norusis 1994b](#nor94b)). Oblique rotation (Direct Oblimin with Kaiser Normalization) was used because theoretically the variables in the study were likely to be related. Generally, factor loadings below 0.4 are considered low in defining a factor, and low-loading items should be suppressed (e.g., [Garson 2011](#gar11); [DeVaus 2002](#dev02)). After extracting the factors and obtaining a relatively clear factor solution, factors are labelled based on the theoretical framework to make them interpretable. If factors are interpretable in regard to the theory, it guarantees the validity of the constructs ([Hair _et al._ 2006](#hai06)).

The above-mentioned guidelines were followed exactly in this study, and a number of different representations were explored before deciding on the particular solutions. The decision process described below was guided by the principle that the most simple structure will generally be the most easily interpretable, meaningful and replicable ([Fabrigar _et al._ 1999](#fab99)).

#### Recognised and unrecognised information needs

Since the demanded and undemanded information needs are a consequence of recognised and unrecognised of these needs, it is important to establish the existence of the recognised and unrecognised information needs first. By analysing the 11-item scale that measures the recognised and unrecognised information needs, it can be determined whether the recognised information needs are distinct from the unrecognised information needs. In summary, seven out of the original eleven items formed two conceptually sound factors. The first factor has five items and it was interpreted as the unrecognised information needs, and the second factor has two items and it was interpreted as the recognised information needs. All the factor loadings were greater than 0.548 and the two factors that emerged from the analysis explained 54% of the total variance, with the eigenvalues of the extracted factors greater than one, and supporting construct and discriminant validity as shown in Table 3\. The Cronbach's alpha for the first and second factors was 0.667 and 0.651 respectively. The reliability of the two scales (unrecognised and recognised information needs) was not improved by deleting any items.

<table><caption>Table 3: Final principal component analysis of the recognised and unrecognised information needs with oblique rotation (7 items)</caption>

<tbody>

<tr>

<th>Code</th>

<th>Items</th>

<th>Unrecognized needs</th>

<th>Recognized needs</th>

</tr>

<tr>

<td>UR-2</td>

<td>I was left to my own devices to determine whether there was information that I might need.</td>

<td>

**0.820**</td>

<td>-0.119</td>

</tr>

<tr>

<td>UR-6</td>

<td>I knew what information I needed but too late.</td>

<td>

**0.727**</td>

<td>-0.024</td>

</tr>

<tr>

<td>UR-3</td>

<td>I proceeded without anything occurring to throw light on what information I might need.</td>

<td>

**0.623**</td>

<td>0.175</td>

</tr>

<tr>

<td>R-4</td>

<td>I stumbled across something that assisted me to get a clear idea of what information I needed.</td>

<td>

**0.618**</td>

<td>-0.029</td>

</tr>

<tr>

<td>UR-1</td>

<td>I vaguely felt I was missing some information</td>

<td>

**0.548**</td>

<td>0.016</td>

</tr>

<tr>

<td>R-3</td>

<td>I knew exactly what information I was missing.</td>

<td>-0.079</td>

<td>

**0.859**</td>

</tr>

<tr>

<td>R-2</td>

<td>I knew in my own mind what information I needed.</td>

<td>0.085</td>

<td>

**0.853**</td>

</tr>

<tr>

<td colspan="2">Eigenvalue</td>

<td>2.337</td>

<td>1.456</td>

</tr>

<tr>

<td colspan="2">Percentage of variance</td>

<td>33.390</td>

<td>20.801</td>

</tr>

<tr>

<td colspan="2">Cumulative percentage of variance</td>

<td>33.390</td>

<td>54.191</td>

</tr>

<tr>

<td colspan="2">Intercorrelations</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="2">Factor 1 (UR)</td>

<td>1.000</td>

<td> </td>

</tr>

<tr>

<td colspan="2">Factor 2 (R)</td>

<td>0.120</td>

<td>1.000</td>

</tr>

<tr>

<td colspan="4">

**Note: Bold indicates factor loadings greater than 0.50\.**</td>

</tr>

</tbody>

</table>

#### Recognised-demanded and recognised-undemanded states

Since the results of factor analysis of the scale of recognised and unrecognised information needs established the existence of recognised information needs as a separate scale, this section discusses whether the recognised information needs has demanded and undemanded aspects. By analysing the eleven-item scale of recognised-demanded and recognised-undemanded information needs, it can be determined whether the recognised-demanded is distinct from the recognised-undemanded information needs. In summary, nine out of the original eleven items formed two conceptually sound factors. The first factor has five items and it was interpreted as the recognised-demanded state, and the second factor has four items and it was interpreted as the recognised-undemanded state. All the factor loadings were greater than 0.498 and the two factors that emerged from the analysis explained 53.6% of the total variance, with the eigenvalues of the extracted factors greater than one and supporting construct and discriminant validity as shown in Table 4\. The Cronbach's alpha for the first and second factors was 0.775 and 0.681 respectively. The reliability of the two scales (recognised-demanded and recognised-undemanded ) will improve to 0.784 and 0.722 respectively by deleting item (RD-6) from the recognised-demanded scale and item (RUD-1) from the recognised-undemanded scale. Since the Cronbach's alpha of the two scale were higher than the accepted value of 0.60 for exploratory research and the deleting of the two items (RD-6 and RUD-1) will discard other important aspects from each scale (i.e. double-checking of already possessed information and using own mind), the researchers decided to retain these two items for further analysis.

<table><caption>Table 4: Final principal component analysis of the recognised-demanded and recognised-undemanded states with oblique rotation (7 items)</caption>

<tbody>

<tr>

<th>Code</th>

<th>Items</th>

<th>Recognised-demanded</th>

<th>Recognised-undemanded</th>

</tr>

<tr>

<td>RD-4</td>

<td>I strongly wanted to have the information that I sought out.</td>

<td>

**0.819**</td>

<td>-0.098</td>

</tr>

<tr>

<td>RD-1</td>

<td>I strongly wanted to have information to satisfy my needs.</td>

<td>

**0.776**</td>

<td>0.025</td>

</tr>

<tr>

<td>RD-3</td>

<td>I really wanted to have the information that I was provided with</td>

<td>

**0.769**</td>

<td>-0.227</td>

</tr>

<tr>

<td>RD-2</td>

<td>I actively sought out information when it didn't seem to be readily available or provided.</td>

<td>

**0.714**</td>

<td>0.134</td>

</tr>

<tr>

<td>RD-6</td>

<td>I double-checked the information that I already had with other sources.</td>

<td>

**0.498**</td>

<td>0.231</td>

</tr>

<tr>

<td>RUD-4</td>

<td>I ignored some relevant information that I did not trust.</td>

<td>0.067</td>

<td>

**0.837**</td>

</tr>

<tr>

<td>RUD-2</td>

<td>I ignored some relevant information that I could not face up to.</td>

<td>-0.043</td>

<td>

**0.810**</td>

</tr>

<tr>

<td>RUD-5</td>

<td>I put off a need for information that I felt was not urgent at the time.</td>

<td>0.225</td>

<td>

**0.575**</td>

</tr>

<tr>

<td>RUD-1</td>

<td>I made up my own mind rather than consulting other sources of information.</td>

<td>-0.131</td>

<td>

**0.545**</td>

</tr>

<tr>

<td colspan="2">Eigenvalue</td>

<td>2.893</td>

<td>1.935</td>

</tr>

<tr>

<td colspan="2">Percentage of variance</td>

<td>32.146</td>

<td>21.496</td>

</tr>

<tr>

<td colspan="2">Cumulative percentage of variance</td>

<td>32.146</td>

<td>53.642</td>

</tr>

<tr>

<td colspan="2">Intercorrelations</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="2">Factor 1 (RD)</td>

<td>1.000</td>

<td> </td>

</tr>

<tr>

<td colspan="2">Factor 2 (RUD)</td>

<td>.137</td>

<td>1.000</td>

</tr>

<tr>

<td colspan="4">Note: Bold indicates factor loadings greater than 0.40.</td>

</tr>

</tbody>

</table>

#### Unrecognised-demanded and unrecognised-undemanded states

Since the results of factor analysis of the scale of recognised and unrecognised information needs established the existence of unrecognised information needs as a separate scale, this section discusses whether the unrecognised information needs has demanded and undemanded aspects. By analysing the 11-item scale of unrecognised-demanded and unrecognised-undemanded information needs, it can be determined whether the unrecognised-demanded is distinct from the unrecognised-undemanded information needs. In summary, seven out of the original eleven items formed two conceptually sound factors. The first factor has four items and it was interpreted as the unrecognised-demanded state, and the second factor has three items and it was interpreted as the unrecognised-undemanded state. All the factor loadings were greater than 0.60 and the two factors that emerged from the analysis explained approximately 52% of the total variance, with the eigenvalues of the extracted factors greater than one and supporting construct and discriminant validity as shown in Table 5\. The Cronbach's alpha for the first and second factors was 0.613 and 0.600 respectively. The reliability of the unrecognised-demanded scale will not improve by deleting any more items. In contrast, the reliability of the unrecognised-undemanded scale will improve to 0.621 if item URUD-2 is deleted. However, as this deletion will discard an important aspect of the scale, which is, dealing with the situation based on own experience, the researchers decided to retain this item in the scale for further analysis. Although these values of Cronbach's alpha are weak, they are considered satisfactory in this particular context.

<table><caption>Table 5: Final principal component analysis of the unrecognised-demanded and unrecognised-undemanded states with oblique rotation (7 items)</caption>

<tbody>

<tr>

<th>Code</th>

<th>Items</th>

<th>Unrecognised-demanded</th>

<th>Unrecognised-undemanded</th>

</tr>

<tr>

<td>URD-6</td>

<td>I explored what information might be available from various sources.</td>

<td>

**0.796**</td>

<td>-0.140</td>

</tr>

<tr>

<td>URD-3</td>

<td>I surfed the Internet for any information that might be relevant to my situation.</td>

<td>

**0.682**</td>

<td>-0.027</td>

</tr>

<tr>

<td>URD-4</td>

<td>I came across the information that I really wanted by chance.</td>

<td>

**0.653**</td>

<td>0.188</td>

</tr>

<tr>

<td>URD-5</td>

<td>I told my story to other carers, hoping they might provide insights into my situation.</td>

<td>

**0.639**</td>

<td>-0.035</td>

</tr>

<tr>

<td>URUD-1</td>

<td>I was too focused on doing the work of caring to bother with having information.</td>

<td>-0.103</td>

<td>

**0.827**</td>

</tr>

<tr>

<td>URUD-3</td>

<td>I paid little attention to what information might be available that I might need.</td>

<td>-0.063</td>

<td>

**0.747**</td>

</tr>

<tr>

<td>URUD-2</td>

<td>I made up my own mind about how to deal with a situation.</td>

<td>0.120</td>

<td>

**0.609**</td>

</tr>

<tr>

<td colspan="2">Eigenvalue</td>

<td>2.002</td>

<td>1.633</td>

</tr>

<tr>

<td colspan="2">Percentage of variance</td>

<td>28.603</td>

<td>23.331</td>

</tr>

<tr>

<td colspan="2">Cumulative percentage of variance</td>

<td>28.603</td>

<td>51.934</td>

</tr>

<tr>

<td colspan="2">Intercorrelations</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="2">Factor 1 (URD)</td>

<td>1.000</td>

<td> </td>

</tr>

<tr>

<td colspan="2">Factor 2 (URUD)</td>

<td>-0.014</td>

<td>1.000</td>

</tr>

<tr>

<td colspan="2">Note: Bold indicates factor loadings greater than 0.60</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

#### Summary of the findings of factor analysis, validity and reliability

In the light of the above results of factor analysis, it can be concluded that the items related to the information needs state (22 items) formed four conceptually sound distinct factors (states) as predicted by the conceptual framework and the qualitative study. These four factors (states) are recognised-demanded, recognised-undemanded, unrecognised-demanded, and unrecognised-undemanded. Table 6 shows a summary of the four distinct states of information needs of informal carers along with their indicators as identified in the data analysis of the questionnaires

<table><caption>Table 6: A summary of the four distinct states of information needs with their indicators as identified in the data analysis</caption>

<tbody>

<tr>

<td> </td>

<th>Demanded</th>

<th>Undemanded</th>

</tr>

<tr>

<th>Recognised</th>

<td>Informal carers recognise their information needs and they want to have information that satisfies these needs. This can be known from four indicators:  

1\. Having relevant information by seeking it out purposely and actively.  
2\. Having more information about and/or confirming double-checking) or disconfirming information they already have.  
3\. Having any relevant information that they are offered.  
4\. Generally, having any relevant information strongly</td>

<td>Informal carers recognise their information needs but they do not want to have information that satisfies these needs. This can be known from three indicators:  

1\. Holding off their information needs at that point of time.  
2\. Creating the needed information based on their mind and experience without consulting any source of information.  
3\. Ignoring relevant information that they are offered by others or that they come across by accident that satisfies their information needs.</td>

</tr>

<tr>

<th>Unrecognised</th>

<td>Informal carers do not recognise their information needs but they want to have information that satisfies these needs. This can be known from two indicators:  

1\. Exploring any information that might be relevant to them.  
2\. Having any relevant information that they come across by chance.  
</td>

<td>Informal carers neither recognise their information needs nor want to have information that satisfies these needs. This can be known from three indicators:  

1\. Avoiding (not thinking of) the information need itself for different reasons such as focusing on one job at a time (e.g., caring) and in turn they do not know that they need to know anything.  
2\. Giving little attention to any kind of available information that might be relevant to them.  
3\. Depending on own mind to deal with and manage a situation without thinking about any kind of information.</td>

</tr>

</tbody>

</table>

### The occurrence of the four distinct states of information needs of informal carers

As shown in Table 7, informal carers face and experience the four states of information needs in various ways. While the state of information needs that is faced most (_frequently_ to _always_) by informal carers is the recognised-demanded, the state they face least often is the recognised-undemanded (_infrequently_ to _occasionally_), followed by the unrecognised-undemanded. Informal carers also _occasionally_ to _frequently_ face the unrecognised-demanded state of information needs.

<table><caption>Table 7: Mean, standard deviation, and number of items of the four states of information needs of informal carers</caption>

<tbody>

<tr>

<th>Factors (information needs states)</th>

<th>Number of items</th>

<th>Mean</th>

<th>Standard deviation</th>

</tr>

<tr>

<td>Recognised-demanded</td>

<td>5</td>

<td>4.11</td>

<td>0.72</td>

</tr>

<tr>

<td>Recognised-undemanded</td>

<td>4</td>

<td>2.43</td>

<td>0.77</td>

</tr>

<tr>

<td>Unrecognised-demanded</td>

<td>4</td>

<td>3.49</td>

<td>0.71</td>

</tr>

<tr>

<td>Unrecognised-undemanded</td>

<td>3</td>

<td>2.69</td>

<td>0.69</td>

</tr>

</tbody>

</table>

## Discussion and implications of the findings

Analysis of the collected data supported the existence of the four states of information needs and these four states were distinct from each other and refined based on the data. Therefore, it is reasonable to suggest that the four states reveal a more holistic view of the information needs of informal carers than have previous studies. The findings of this study suggest that both the undemanded states of information needs are just as important as the demanded states of these needs. The findings also suggest that the lack of information is just as important as the information informal carers might already have. This may in turn have an impact on both the way we investigate the information needs of informal carers and the way we provide information to informal carers.

More specifically, the results demonstrate that recognition of information needs is distinct from non-recognition of information needs as predicted by the conceptual framework and the qualitative study. The non-recognition of information needs accounts for 33% of the variance and implies that informal carers are not able to articulate their information needs and they realise this is the case (i) by themselves, and/or (ii) with the assistance of others and/or (iii) accidentally after they obtain any relevant information; and informal carers may not know what information they need to carry out their tasks. This is consistent with previous research that has found that informal carers were not always aware of what information they might need (e.g., [Andreassen _et al._ 2005](#and05); [Hepworth 2004](#hep04)). The recognition of information needs accounts for 21% of the variance and implies that informal carers may have limited ability to articulate their information needs. This is consistent with previous research that has found that informal carers were unable to identify and predict some topics of needed information before they found themselves in an unfamiliar or critical situation that forced them to know about these topics (e.g., [Hummelinck and Pollock 2006](#hum06)).

The results also indicate that the four states of information needs are distinct from each other as predicted by the conceptual framework ([Alzougool _et al._ 2008](#alz08)) and the qualitative study.

### Recognised-demanded state

In the recognised-demanded state, the informal carers both recognise that they have an information need and want to have information to meet this need. Informal carers may either lack or have the needed information. Therefore, they want to address this lack of information or to have more information and/or to confirm or disconfirm the information they already have. However, in all these cases they can precisely articulate their information needs. This is consistent with previous research that found that informal carers:

1.  engage in information-seeking behaviour to satisfy their information needs (e.g., [James _et al._ 2007](#jam07); [Hummelinck and Pollock 2006](#hum06));
2.  selectively accepted advice from healthcare professionals (e.g., [Buri and Dawson 2000](#bur00)); and
3.  expected that information to be given more spontaneously (e.g., [Smith _et al._ 2004](#smi04); [De Rouck and Leys 2009](#der09)); and additionally
4.  information that individuals already have might be recognised and expressed to an information system in the case of individuals wanting to confirm this information by requiring further information ([Shenton and Dixon 2004](#she04))

An information system could address such needs by including two main design features:

1.  the ability to identify this group of informal carers and provide them with this relevant information, and
2.  the ability to enable informal carers to search for this relevant information and find it in formats that are most relevant to them. In other words, this means including design features that facilitate the finding of the needed information (no more and no less) rapidly. The system could include a smart search engine that allows informal carers to enter their recognised-demanded information need and get relevant information that satisfies this need accurately and rapidly, saving carers' time and energy.

### Recognised-undemanded state

In the recognised-undemanded state, the informal carers recognise that they have an information need but do not want to have information for any number of reasons ranging from already having the information to being satisfied with not knowing. Nevertheless, in all these cases they can precisely articulate their information needs. This is consistent with previous research that found that

1.  informal carers avoided relevant information (e.g., [Hummelinck and Pollock 2006](#hum06); [Feltwell and Rees 2004](#fel04)), which is particularly relevant in relation to health, where information needs and behaviour can change during the patient pathway ([Bath 2008](#bat08)) or the caring journey of informal carers;
2.  informal carers find themselves in a position where they face conflict between seeking and avoiding information (e.g., [Andreassen _et al._ 2005](#and05); [Feltwell and Rees 2004](#fel04)); and
3.  informal carers used their own lived experience and personal knowledge (e.g., [Kuuppelomaki _et al._ 2004](#kuu04); [Buri and Dawson 2000](#bur00)). This particular state has implications for just-in-time provision of information services which are able to provide information, when it is eventually demanded.

To address this need, information systems could include various design features such as:

1.  the ability to identify the reasons why those informal carers do not want to have this information and in turn provide them with suggested solutions to these reasons and encourage them to have this information, as well as to highlight the consequences of not having this information; and
2.  the ability to identify what will encourage or discourage informal carers' demands for information, and when informal carers are likely to demand this information, and to offer this information at the appropriate time.

### Unrecognised-demanded state

In the unrecognised demanded state, the informal carers do not recognise that they have an information need but want to have information unknown to them that would meet this need. In this state, informal carers only lack the needed information. Therefore, they want to satisfy this lack of information because the shortage of what is needed is considered risky to them. This is consistent with previous research, which has stated that the absence of the unrecognised needed information may potentially lead to some detriment to individuals ([Faibisoff and Ely 1976](#fai76)). However, informal carers cannot precisely articulate what information they lack. Therefore, they use two strategies that help them to articulate their information needs and find information that satisfies these needs (they explore what information is available for any information that might be relevant to them and/or accept any relevant information that they come across by chance). This is consistent with previous research that found that informal carers:

1.  explore any available information to find out relevant information (e.g., [Robinson _et al._ 2009](#rob09); [Porter and Edirippulige 2007](#por07); [Wainstein _et al._ 2006](#wai06)); and
2.  often found needed information by chance (e.g., [Smith _et al._ 2004](#smi04)).

An information system could address such needs by including design features such as:

1.  the ability to enable informal carers to explore a range of information via stories and case studies that might be relevant to them, in order to recognise and find the needed information and satisfy the need. In this feature, it is important to provide as many exploration techniques as possible because exploration is critical in recognising the needed information and subsequently satisfying this need. It is also important to maintain the engagement of informal carers with the system as long as possible to encourage this active exploration in order to have a good idea of the available information, and in turn recognise this need and satisfy it;
2.  the ability to identify this group of informal carers and provide them with this relevant information (i.e., promoting this relevant information to them) in order to facilitate their recognition of this need, and allowing informal carers to feel that they found this relevant information accidentally by suggesting (e.g., in the form of reminders) the relevant information during the exploration process; and
3.  the ability to identify the intervening factors that may motivate or inhibit informal carers from recognising this need.

### Unrecognised-undemanded state

In the unrecognised-undemanded state, the informal carers neither recognise that they have an information need nor want to have information that would meet this need. In this state informal carers cannot evaluate whether they lack or have the needed information because they do not feel they need to know anything in the first place, nobody assists them directly and/or they do not directly come across anything by chance that helps them. This is consistent with previous research that found that

1.  informal carers neglected their own needs, including information, because of the time constraints (e.g., [Zapart _et al._ 2007](#zap07)); and
2.  generally, people are likely to avoid and ignore relevant information if this information requires them to alter their beliefs or causes uncertainty ([Case _et al._ 2005](#cas05)). This is an area which needs more research and places carers and patients most at risk.

An information system could include a number of design features to address this need:

1.  the ability to identify this group of informal carers and not provide them with relevant information until the reasons behind non-recognition of this need are identified and resolved, because recognition of this need is the critical issue in identifying whether they want to have relevant information or not;
2.  the ability to identify what will engage or disengage informal carers in order to negotiate and interact with them about this need, because engaging in negotiations and conversations with information providers and possibly with other similar informal carers or other people is possibly a prerequisite to recognising this information and then deciding if they want to satisfy their need for it;
3.  the ability to identify the reasons why those informal carers do not recognise this information need, which may in turn empower carers and raise their awareness of the existence of this need, as well as encourage informal carers to have information that satisfies this need; and
4.  the ability to identify when informal carers are likely to recognise and demand information that fulfils this need.

## Limitations and further research

This study has certain limitations. First, it concentrated on the information needs of informal carers as experienced by them. The perspectives of other parties (e.g., clinicians, paid or formal carers, the patients receiving care) who interact with informal carers and may have the ability to identify the information needs of informal carers, were excluded in this study for two reasons: this view is consistent with an information systems perspective which concentrates on the point of view of end-users of health information systems (i.e. informal carers), and this view is also consistent with a phenomenological approach ([Budd 2005](#bud05)) which concentrates on the study of conscious experiences (perception, imagination, thought, emotion, desire, volition, and action) as experienced, lived or performed from the first-person point of view (in this case the informal carers). Therefore, further research could investigate these needs from the perspectives of these other parties, and compare and integrate their views with the informal carers' perspective presented in this study.

Second, this study explored the information needs of informal carers who are older than 18 years, because it was easier to recruit these carers with human research ethics approval. Therefore, the applicability of these findings to younger informal carers (less than 18 years old) needs further investigation.

Third, this study explored the experiences of informal carers who care for people with long-term conditions such as chronic disease, people who have a permanent disability, and frail elderly people. Therefore, the applicability of these findings to other informal carers who care for people with temporary conditions such as acute illness needs further investigation.

Fourth, since this study was cross-sectional, a better understanding of the dynamic nature of states of information needs of informal carers requires more longitudinal studies.

Fifth, the generalizability of the results of the quantitative study is limited by the lack of information regarding nonparticipants. Only a small number of informal carers (26 informal carers) who do not use the Internet had the opportunity to complete the questionnaire, and for this reason, the researchers could not analyse if there were significant differences between informal carers who completed the online questionnaire and those who completed the paper-based questionnaire. Therefore, the study may be biased for this reason. Nevertheless, it could be argued that the validity of the two states related to undemanded information (i.e., recognised-undemanded and unrecognised-undemanded) was strongly supported because they also exist in the lived experiences of informal carers who use the Internet.

Apart from informal carers, other groups of healthcare consumers have information needs and are equally important end users of health information systems. Investigating consumers' information needs is resource-intensive, thus, this research was limited to one group of consumers, namely informal carers. Nevertheless the methodology and approach that were taken in this research may be applicable to other consumers.

In addition to the above, this study opens a number of further research opportunities. Further research is needed to study the relationships and fit between each state and other concepts such as information design, information behaviour, and information source and channel. For example, it can be proposed that every state of information needs corresponds in a different way to features in information design. Also, as this study was mainly concerned with identifying and proving the existence of the four states of information needs of informal carers, further research is also required to thoroughly investigate each state of information needs and to extensively explain the variations in the four states among informal carers. Moreover, further research is also required to examine the relationship between distinct temporal stages in the work of caring and the four states of information needs.

## Conclusions

This paper has reported findings from sequential empirical testing of a framework of the information needs state of informal carers. The findings of the qualitative study identified the indicators of the existence of each state (on the forms of scenarios and experiences) and these indicators were in turn worded as items that were used to measure each state. Then, these indicators were validated with a large sample of informal carers by conducting the quantitative study. Therefore, the framework has implications relating to the design and provision of information to informal carers, taking into consideration the state of their information needs. In other words, the information needs state may assist in developing and providing relevant information that satisfies information needs of informal carers. Information providers may present the information relating to each state. The findings of this study are also fundamental to exploring other concepts such as information behaviour and use, because understanding information needs is the basis for understanding and applying these other information concepts.

## Acknowledgements

We thank the carers who participated in this research for their time and insights.

## About the authors

**Basil Alzougool** is a Research Fellow in the Department of Computing and Information Systems, The University of Melboune, Australia. He received his Bachelor's degree in Public Administration and Master of Public Adminstration from University of Jordan, Jordan and his PhD from the Department of Information Systems at the University of Melbourne, Australia. He can be contacted at: [b.alzougool@gmail.com](mailto:shanton.chang@unimelb.edu.au)  
**Shanton Chang** is a Senior Lecturer in the Department of Computing and Information Systems, The University of Melbourne, Australia. He completed his Bachelor's degree in Commerce from the University of Western Austrlia and his PhD from Monash University. He can be contacted at: [Shanton.Chang@unimelb.edu.au](mailto:shanton.chang@unimelb.edu.au)  
**Kathleen Gray** is a Senior Research Fellow in the Health and Biomedical Informatics Research Unit, The University of Melbourne, Australia. She completed her PhD from The University of Melbourne. She can be contacted at: [kgray@unimelb.edu.au](mailto:kgray@unimelb.edu.au)

</section>

<section>

## References

<ul>
<li id="ada09">Adams, E., Boulton, M. &amp; Watson, E. (2009). The information needs of partners and
    family members of cancer patients: a systematic literature review. <em>Patient Education and
        Counseling,</em> <strong>77</strong>(2), 179-86</li>

<li id="alz08">Alzougool, B., Chang, S. &amp; Gray, K. (2008). Towards a comprehensive understanding of
    health information needs. <em>Electronic Journal of Health Informatics</em>, <strong>3</strong>(2),
    e15</li>

<li id="and05">Andreassen, S., Randers, I., Naslund, E., Stockeld, D. &amp; Mattiasson, A.C. (2005).
    Family members' experiences, information needs and information seeking in relation to living with a
    patient with oesophageal cancer. <em>European Journal of Cancer Care,</em> <strong>14</strong>(5),
    426-434</li>

<li id="att06">Attfield, S.J., Adams, A. &amp; Blandford, A. (2006). Patient information needs: pre- and
    post-consultation. <em>Health Informatics Journal,</em> <strong>12</strong>(2), 165-177</li>

<li id="aih04">Australian Institute of Health and Welfare (AIHW). (2004). <em>Carers in Australia:
        assisting frail older people and people with a disability</em>. Canberra, Australia: AIHW (Aged
    Care Series)</li>

<li id="bat08">Bath, P.A. (2008). Health informatics: current issues and challenges. <em>Journal of
        Information Science</em>, <strong>34</strong>(4), 501–518</li>

<li id="bea07">Beaver, K. &amp; Witham, G. (2007). Information needs of the informal carers of women
    treated for breast cancer. <em>European Journal of Oncology Nursing,</em> <strong>11</strong>(1),
    16-25</li>

<li id="ber04">Berg, B.L. (2004). <em>Qualitative research methods for social sciences</em> (5th ed.).
    Boston, MA: Pearson; Allyn and Bacon</li>

<li id="bro09">Brotherton, A. &amp; Abbott, J. (2009). Clinical decision making and the provision of
    information in PEG feeding: an exploration of patients and their carers' perceptions. <em>Journal of
        Human Nutrition and Dietetics,</em> <strong>22</strong>, 302–309</li>

<li id="buc07">Buckner, L. &amp; Yeandle, S. (2007, September). <em><a
            href="http://www.webcitation.org/6AvjwizJf">Valuing carers: calculating the value of unpaid
            care</a></em>. Carers (UK). Retrieved 24 September, 2012 from
    http://www.carersuk.org/media/k2/attachments/Valuing_Carers___Calculating_the_value_of_unpaid_care.pdf.
    (Archived by WebCite&reg; at http://www.webcitation.org/6AvjwizJf)</li>

<li id="bud05">Budd, M.J. (2005). Phenomenology and information studies. <em>Journal of
        Documentation</em>, <strong>61</strong>(1), 44–59</li>

<li id="bur00">Buri, H. &amp; Dawson, P. (2000). Caring for a relative with dementia: a theoretical
    model of coping with fall risk. <em>Health, Risk &amp; Society,</em> <strong>2</strong>(3), 283-293
</li>

<li id="cas05">Case, D.O., Andrews, J.E., Johnson, J.D. &amp; Allard, S.L. (2005). Avoiding versus
    seeking: the relationship of information seeking to avoidance, blunting, coping dissonance and
    related concepts. <em>Journal of the Medical Library Association,</em> <strong>93</strong>(3),
    353-362</li>

<li id="cle05">Cleary, M., Freeman, A., Hunt, G.E. &amp; Walter, G. (2005). What patients and carers
    want to know: an exploration of information and resource needs in adult mental health services?
    <em>Australian and New Zealand Journal of Psychiatry</em>, <strong>39</strong>(6), 507–513</li>

<li id="coa01">Coakes, S.J. &amp; Steed, L.G. (2001). <em>SPSS: analysis without anguish: version 10.0
        for Windows.</em> Brisbane, Australia: Wiley</li>

<li id="col01">Collier, J., Pattison, H., Watson, A. &amp; Sheard, C. (2001). Parental information needs
    in chronic renal failure and diabetes mellitus. <em>European Journal of Pediatrics</em>,
    <strong>160</strong>(1), 31-36</li>

<li id="coo05">Cooper, J. &amp; Urquhart, C. (2005). The information needs and information-seeking
    behaviours of home-care workers and clients receiving home care. <em>Health Information &amp;
        Libraries Journal,</em> <strong>22</strong>(2), 107-116</li>

<li id="cos05">Costello, A.B. &amp; Osborne, J.W. (2005). <a
        href="http://www.webcitation.org/6Avh1w09I">Best practices in exploratory factor analysis: Four
        recommendations for getting the most from your analysis</a>. <em>Practical Assessment Research
        &amp; Evaluation,</em> <em><strong>10</strong></em>(7). Retrieved 2 March, 2013 from
    http://pareonline.net/getvn.asp?v=10&amp;n=7. (Archived by WebCite&reg; at
    http://www.webcitation.org/6Avh1w09I)</li>

<li id="der09">De Rouck, S. &amp; Leys, M. (2009). Information needs of parents of children admitted to
    a neonatal intensive care unit: a review of the literature (1990–2008). <em>Patient Education and
        Counselling, </em><strong>76</strong>, 159–173</li>

<li id="dev02">DeVaus, D.A. (2002). <em>Surveys in social research</em> (5th ed.). Crows Nest,
    Australia: Allen &amp; Unwin</li>

<li id="doc08">Docherty, A., Owens, A., Asadi-Lari, M., Petchey, R., Williams, J. &amp; Carter, Y.H.
    (2008). Knowledge and information needs of informal caregivers in palliative care: a qualitative
    systematic review. <em>Palliative Medicine,</em> <strong>22</strong>(2), 153-171</li>

<li id="eys02">Eysenbach, G. &amp; Wyatt, J.C. (2002). <a
        href="http://www.webcitation.org/6AvhEnbu7">Using the Internet for surveys and health
        research.</a> <em>Journal of Medical Internet Research,</em> <strong>4</strong>(2). Retrieved 2
    March, 2013 from http://www.jmir.org/2002/2/e13/ (Archived by WebCite&reg; at
    http://www.webcitation.org/6AvhEnbu7)</li>

<li id="fab99">Fabrigar, L.R., Wegener, D.T., MacCallum, R.C. &amp; Strahan, E.J. (1999). Evaluating the
    use of exploratory factor analysis in psychological research. <em>Psychological Methods</em>,
    <strong>4</strong>(3), 272-299</li>

<li id="fai76">Faibisoff, S. G. &amp; Ely, D. P. (1976). Information and information needs.
    <em>Information Reports and Bibliographies</em>, <strong>5</strong>(5), 2–16</li>

<li id="fel04">Feltwell, A.K. &amp; Rees, C.E. (2004). The information-seeking behaviours of partners of
    men with prostate cancer: a qualitative pilot study. <em>Patient Education and Counselling,</em>
    <strong>54</strong>(2), 179-185</li>

<li id="gar11">Garson, G.D. (2011, March 3). <a href="http://www.webcitation.org/5xvl6DQ10">Factor
        analysis</a>. Retrieved 14 April, 2011 from
    http://faculty.chass.ncsu.edu/garson/PA765/factor.htm. (Archived by WebCite&reg; at
    http://www.webcitation.org/5xvl6DQ10)</li>

<li id="gus10">Gustafsson, L., Hodge, A., Robinson, M., McKenna, K. &amp; Bower, K. (2010). Information
    provision to clients with stroke and their carers: self-reported practices of occupational
    therapists. <em>Australian Occupational Therapy Journal</em>, <strong>57</strong>(3), 190-196</li>

<li id="hai06">Hair, J.F., Black, B., Babin, B., Anderson, R.E. &amp; Tatham, R.L. (2006).
    <em>Multivariate data analysis</em> (6th ed.).  Upper Saddle River, NJ: Pearson Prentice Hall</li>

<li id="hea99">Heaton, J. (1999). The gaze and visibility of the carer: a Foucauldian analysis of the
    discourse of informal care. <em>Sociology of Health &amp; Illness,</em> <strong>21</strong>(6),
    759-777</li>

<li id="hep04">Hepworth, M. (2004). A framework for understanding user requirements for an information
    service: defining the needs of informal carers. <em>Journal of the American Society for Information
        Science and Technology,</em> <strong>55</strong>(8), 695-708</li>

<li id="hof06">Hoffmann, T. &amp; McKenna, K. (2006). Analysis of stroke patients' and carers' reading
    ability and the content and design of written materials: recommendations for improving written
    stroke information. <em>Patient Education and Counselling</em>, <strong>60</strong>(3), 286-293</li>

<li id="hum06">Hummelinck, A. &amp; Pollock, K. (2006). Parents' information needs about the treatment
    of their chronically ill child: a qualitative study. <em>Patient Education and Counselling,</em>
    <strong>62</strong>(2), 228-234</li>

<li id="ike02">Ikemba, C.M., Kozinetz, C.A., Feltes, T.F., Fraser, Ch.D., McKenzie, E.D., Shah, N. &amp;
    Mott, A.R. (2002). Internet use in families with children requiring cardiac surgery for congenital
    heart disease. <em>Pediatrics,</em> <strong>109</strong>(3), 419-422</li>

<li id="jam07">James, N., Daniels, H., Rahman, R., McConkey, C., Derry, J. &amp; Young, A. (2007). A
    study of information seeking by cancer patients and their carers. <em>Clinical Oncology,</em>
    <strong>19</strong>(5), 356-362</li>

<li id="jan06">Janda, M., Eakin, E.G., Bailey, L., Walker, D. &amp; Troy, K. (2006). Supportive care
    needs of people with brain tumours and their carers. <em>Supportive Care in Cancer,</em>
    <strong>14</strong>(11), 1094-1103</li>

<li id="jor09">Jorgensen, D., Parsons, M. &amp; Jacobs, S. (2009, January 20). <em><a
            href="http://www.webcitation.org/6AvgpqDYb">Carers assessment of needs: the experiences of
            informal caregivers in New Zealand: &ldquo;Caring is like a jigsaw puzzle with no picture
            and pieces missing&rdquo;</a></em>. Carers New Zealand, University of Auckland.  Retrieved
    24 Septebmer, 2012 from http://www.supportingfamiliesnz.org.nz/CANFinalReport.doc. (Archived by
    WebCite® at http://www.webcitation.org/6AvgpqDYb)</li>

<li id="kee08">Keefe, J., Guberman, N., Fancey, P., Barylak, L. &amp; Nahmiash, D. (2008). Caregivers'
    aspirations, realities, and expectations: the CARE tool. <em>Journal of Applied Gerontology,</em>
    <strong>27</strong>(3), 286-308</li>

<li id="ken04">Kendall, S., Thompson, D. &amp; Couldridge, L. (2004). The information needs of carers of
    adults diagnosed with epilepsy. <em>Seizure,</em> <strong>13</strong>(7), 499-508</li>

<li id="kri07">Krishnasamy, M., Wells, M. &amp; Wilkie, E. (2007). Patients and carer experiences of
    care provision after a diagnosis of lung cancer in Scotland. <em>Supportive Care in Cancer,
    </em><strong>15</strong>(3), 327-332</li>

<li id="kuu04">Kuuppelomaki, M., Sasaki, A., Yamada, K., Asakawa, N. &amp; Shimanouchi, S. (2004).
    Coping strategies of family carers for older relatives in Finland. <em>Journal of Clinical
        Nursing,</em> <strong>13</strong>(6), 697-706</li>

<li id="lam07">Lambert, S.D. &amp; Loiselle, C.G. (2007). Health information seeking behaviour.
    <em>Qualitative Health Research,</em> <strong>17</strong>(8), 1006-1019</li>

<li id="lin85">Lincoln, Y.S. &amp; Guba, e.g., (1985). <em>Naturalistic inquiry.</em> Newbury Park, CA:
    Sage Publications</li>

<li id="mck06">McKune, S.L., Andresen, E.M., Zhang, J. &amp; Neugaard, B. (2006). <em>Caregiving: a
        national profile and assessment of caregiver services and needs.</em> Americus, Georgia:
    Rosalynn Carter Institute for Caregiving</li>

<li id="nor94a">Norusis, M.J. (1994a). <em>SPSS 11.0 guide to data analysis.</em> Sydney, Australia:
    Pearson Education</li>

<li id="nor94b">Norusis, M.J. (1994b). <em>SPSS professional statistics 6.1.</em> Chicago, IL: SPPS Inc
</li>

<li id="odh03">Odhiambo, F., Harrison, J. &amp; Hepworth, M. (2003). The information needs of informal
    carers: an analysis of the use of the micro-moment time line interview. <em>Library and Information
        Research,</em> <strong>27</strong>(86), 19-29</li>

<li id="pai99">Pain, H. (1999). Coping with a child with disabilities from the parents' perspective: the
    function of information. <em>Child: Care, Health and Development,</em> <strong>25</strong>(4),
    299-313</li>

<li id="pal07">Pallant, J. (2007). <em>SPSS survival manual: a step-by-step guide to data analysis using
        SPSS for Windows (Version 15)</em> (3rd ed.). Crows Nest, Australia: Allen &amp; Unwin</li>

<li id="por07">Porter, A. &amp; Edirippulige, S. (2007). Parents of deaf children seeking hearing
    loss-related information on the Internet: the Australian experience. <em>Journal of Deaf Studies and
        Deaf Education, </em><strong>12</strong>(4), 518-529</li>

<li id="rah03">Rahi, J. S., Manaras, I. &amp; Barr, K. (2003). Information sources and their use by
    parents of children with ophthalmic disorders. <em>Investigative Ophthalmology and Visual
        Science</em>, <strong>44</strong>(6), 2457-2460</li>

<li id="ree00">Rees, C.E. (2000). The information needs and source preferences of women with breast
    cancer and their family members: a review of the literature published between 1988 and 1998.
    <em>Journal of Advanced Nursing</em>, <strong>31</strong>(4), 833-841</li>

<li id="ree03">Rees, C. E., Sheard, C. E. &amp; Echlin, K. (2003). The relationship between the
    information-seeking behaviours and information needs of partners of men with prostate cancer: a
    pilot study. <em>Patient Education and Counselling</em>, <strong>49</strong>(3), 257-261</li>

<li id="ric07">Richardson, A., Plant, H., Moore, S., Medina, J., Cornwall, A. &amp; Ream, E. (2007).
    Developing supportive care for family members of people with lung cancer: a feasibility study.
    <em>Supportive Care in Cancer</em>, <strong>15</strong>(11), 1259-1269</li>

<li id="rob09">Robinson, A., Elder, J., Emden, C., Lea, E., Turner, P. &amp; Vickers, J. (2009).
    Information pathways into dementia care services: family carers have their say. <em>Dementia,</em>
    <strong>8</strong>(1), 17–37</li>

<li id="sal04">Salminen, E., Vire, J., Poussa, T. &amp; Knifsund, S. (2004). Unmet needs in information
    flow between breast cancer patients, their spouses, and physicians. <em>Supportive Care in
        Cancer,</em> <strong>12</strong>(9), 663-668</li>

<li id="she04">Shenton, A.K. &amp; Dixon, P. (2004). The nature of information needs and strategies for
    their investigation in youngsters.&nbsp;<em>Library and Information Science Research,</em>
    <strong>26</strong>(3), 296-310</li>

<li id="smi04">Smith, L.N., Lawrence, M., Kerr, S.M., Langhorne, P. &amp; Lees, K.R. (2004). Informal
    carers' experience of caring for stroke survivors. <em>Journal of Advanced Nursing,
    </em><strong>46</strong>(3), 235-244</li>

<li id="tab07">Tabachnick, B.G. &amp; Fidell, L.S. (2007). <em>Using multivariate statistics</em> (5th
    ed.). Boston, MA: Pearson; Allyn &amp; Bacon</li>

<li id="tho08">Thon, A. &amp; Ullrich, G. (2008). Information needs in parents of children with a
    rheumatic disease. <em>Child: Care, Health and Development,</em> <strong>35</strong>(1), 41–47</li>

<li id="wai06">Wainstein, B.K., Sterling-Levis, K., Baker, S.A., Taitz, J. &amp; Brydon, M. (2006). Use
    of the Internet by parents of paediatric patients. <em>Journal of Paediatrics and Child Health,</em>
    <strong>42</strong>(9), 528-532</li>

<li id="wil99">Wilson, T.D. (1999). Models in information behaviour research. <em>Journal of
        Documentation,</em> <strong>55</strong>(3), 249–270</li>

<li id="won02">Wong, R.K., Franssen, E., Szumacher, E., Connolly, R., Evans, M., Page, B., <em>et
        al.</em> (2002). What do patients living with advanced cancer and their carers want to know? A
    needs assessment. <em>Supportive Care in Cancer, </em><strong>10</strong>(5), 408-415</li>

<li id="yed08">Yedidia, M. &amp; Tiedemann, A. (2008). How do family caregivers describe their needs for
    professional help? <em>American Journal of Nursing,</em> <strong>108,</strong> 35-37</li>

<li id="yin02">Yin, R.K. (2002). <em>Case study research: design and methods</em> (3rd ed.). Newbury
    Park, CA: Sage Publicaitions.</li>

<li id="zap07">Zapart, S., Kenny, P., Hall, J., Servis, B. &amp; Wiley, S. (2007). Home-based palliative
    care in Sydney, Australia: the carer's perspective on the provision of informal care. <em>Health and
        Social Care in the Community,</em> <strong>15</strong>(2), 97–107</li>
</ul>

</section>

</article>