#### Information Research, Vol. 5 No. 3, April 2000

# Recent trends in user studies: action research and qualitative methods

#### [T.D. Wilson](mailto:t.d.wilson@shef.ac.uk), Ph.D.  
Department of Information Studies  
University of Sheffield  
Sheffield, UK

#### Abstract

> This paper was commissioned by Professor Gernot Wersig of the Freie Universität, Berlin in 1980, as part of his Project, _Methodeninstrumentarium zur Benutzforschung in Information und Dokumentation._ It attempted to set out what was, for the time, a novel perspective on appropriate methodologies for the study of human information seeking behaviour, focusing on qualitative methods and action research, arguing that the application of information research depended upon its adoption into the managerial processes of organizations, rather than its self-evident relationship to any body of theory. It is presented here as it was originally written, with the figures re-drawn.

#### Contents

<table>

<tbody>

<tr>

<td>

[1\. ‘Information science.’](#one)</td>

</tr>

<tr>

<td>

[2\. Information science and the information user.](#two)</td>

</tr>

<tr>

<td>

[3\. The information user in his contexts.](#three)</td>

</tr>

<tr>

<td>

[4\. Information need.](#four)</td>

</tr>

<tr>

<td>

[5. User studies and research methods.](#five)</td>

</tr>

<tr>

<td>

[6. Action research.](#six)</td>

</tr>

<tr>

<td>

[7\. Qualitative research.](#seven)</td>

</tr>

<tr>

<td>

[8. Conclusion](#eight)</td>

</tr>

<tr>

<td>

[Acknowledgements](#ackn)</td>

</tr>

<tr>

<td>

[Notes](#notes)</td>

</tr>

<tr>

<td>

[References](#refs)</td>

</tr>

</tbody>

</table>

## <a id="one"></a>1\. ‘Information science’

In the past twenty years we have witnessed a continuing debate on the nature of 'information science', in events such as the Aberystwyth conference of 1975 ([Debons & Cameron, 1975](#ref1)) and in papers such as those by [Belkin](#ref1) (1978), [Farradane](#ref2) (1976) and [Debons](#ref1) (1980). This debate has been characterized as much as anything by confusion: confusion over the nature of 'science' as an enterprise, confusion over the possibilities of a 'unifying theory' of information science, and confusion over the distinction between the _practice_ of information _work_ and the pursuit of information science.

This being the case, it is necessary for the author of this paper to state his position, which can be summed up as follows:

*   'information science' is a multi-disciplinary field, the research 'objects' of which range from chemical formulae for which efficient storage is being sought in computer memories to people behaving in particular ways to obtain needed information; therefore,
*   a 'unifying theory' is unattainable: the research objects are so diverse, representing so many 'integrative levels' that theories drawn from many disciplines are needed to guide effective research on each distinct sub-branch of 'information science';
*   furthermore, because those who practise information _work_ are in a position to determine research problems, to provide the research environments, and to accept or ignore research findings, there is a strong push towards making information science an _applied_ science rather than a basic science.

The danger here of course is that problem-solving on a pragmatic basis may take the place of scientific research, allowing even less theoretical work capable of being applied.

This paper, therefore, addresses some of the issues which the author sees as a consequence of the 'confusion' mentioned earlier.

## <a id="two"></a>2\. Information science and the information user

However we define it, and from whatever year we choose to date it, information science has been concerned with the information user. For example, the [Royal Society Scientific Information Conference](#ref3) (1948) contained several seminal studies of information users (notably by Bernal and Urquhart) and by the time its successor was held, ten years later, ([International conference on scientific information, 1958](#ref2)) the subject had become a veritable industry with the 'Literature and reference needs of scientists,' attracting thirteen papers from the USA, the UK, Czechoslovakia, and Scandinavia. By 1977 [Ford](#ref2) was able to identify more than 200 investigations (mostly published since 1970) worth listing in a literature review. By general consensus over the years all of this attention has led to no very serious conclusion. In 1960, for example, Menzel noted:

> ‘To date there has been little awareness of the wide range of significantly different topics and relationships that have been and can be explored in studies of scientists' information-gathering behavior and experiences.’ ([Menzel, 1960:2](#ref2))

Five years later Paisley found no great improvement:

> ‘Since no noticeable effort has been made to replicate methods we now have a collection of case studies whose findings can be compared only if a **ceteris paribus** assumption is stretched over gross differences in procedure.’ ([Paisley, 1965](#ref3):11 - 1)

In his review of the 1967 literature Paisley also noted that, as a consequence of the 'shallow conceptualization' in information user studies,

> ‘... it is hard to glimpse a real scientist or technologist at work, under constraints and pressures, creating products, drawing upon the elaborate communication network that connects him with sources of necessary knowledge.’ ([Paisley, 1968:2](#ref3))

In some degree or other the three elements these writers drew attention to in the 1960s have been reiterated by other researchers and writers ever since:

*   inadequate exploration of significant relationships;
*   non-comparability of results; and
*   shallow conceptualization.

Whether this situation can ever be resolved to everyone's satisfaction is debatable, but the remainder of this paper can be viewed as a personal response to the issues raised.

## <a id="three"></a>3\. The information user in his contexts

The 'user' may be found in many communication/information contexts and 'user studies' need to distinguish among these contexts before research is planned. This process may also help the researcher to draw upon related concepts and theories from these alternative contexts.

The contexts that appear to be appropriate for 'user studies' and within which investigations have been carried out include:

*   the user as **_communicator_**, drawing upon personal or organizational information resources in communicating with organizational colleagues or fellows in society; the research literature in these areas is to be found in social psychology (e.g., [McQuail, 1975](#ref2)) and in communication studies generally (see, for example, a recent collection edited by [Weinshall](#ref3), 1979);
*   the user as **_information-seeker_**; 'communicator' covers virtually all other contexts, but within communication information-seeking may be identified as a separate task and one which involves not only inter-personal communication, but also:
*   the use of **_formal information systems_**, defined widely, as any device, product or system intended for information representation, storage, conservation, retrieval, or re-packaging. That is, for example, any library, information service, abstracting journal, primary journal, on-line bibliographic data base, organizational record file, etc., etc. User studies in the past have often been studies of the interaction of people with systems (e.g., [Burns & Hasty, 1975](#ref1)) or enquiries into the use of systems in general (e.g., [Bernal, 1948](#ref1)) rather than the study of underlying needs;
*   the user as a **_recipient_** of information services: not all information systems/services are passive, some take their products to the user: SDI and current awareness bulletins are the most common examples in information science but radio or TV news bulletins can be looked at in the same way. The typical mode of research in this area is evaluation, principally of automated SDI services, which seeks to establish some kind of index of 'success' for the service (e.g., [Dugger & Klinger, 1967](#ref1); [Dammers, 1974](#ref1));
*   the user as a **_user_** of information: paradoxically, user studies has been concerned with almost everything apart from the use to which information is put by the recipient or information seeker. The reason for this seems to be a desire to draw policy conclusions (either for a single information agency or more generally) from data on aggregated behaviour rather than a desire to _understand_ the user.

The implications of this brief (and, possibly, incomplete) analysis are rather straightforward: a _total_ view of the information user will demand attention to all of these aspects, and any partial view demands rigorous definition of _which_ context applies.

## <a id="four"></a>4\. Information need<sup>[(1)](#note1)</sup>

The concept of 'information need' is central to the study of the user in any of the above contexts and, considering this centrality, it is rather surprising that confusion exists over the definition of the term.

Part of the difficulty lies with the troublesome concept 'information'. Numerous definitions have been evolved and recently there have been attempts at a single concept of information for information science (e.g., [Belkin, 1978](#ref1)). However, the problem seems to lie not so much with the lack of a _single_ concept but with a failure to distinguish among alternative, common-sense meanings of the word 'information'. It can be variously understood, in the context of user-studies research, as a _physical entity_ or phenomenon (as in the case of questions relating to the number of books read in a year, the number of journals subscribed to, and so on), as the _channel of communication_ through which the data are transferred (as when we speak of the incidence of oral versus written communication), or as the _subject data_ contained in a document or transmitted orally. Information may also be understood as _factual data_ objectively transferred, or as _advice_ or _opinions_ into which value judgements enter.

These multiple uses of the term cause confusion because researchers sometimes fail to distinguish between one sense and another or simply leave the reader to discover which sense is meant by reading the paper or report. Even then, it is sometimes unclear which of the senses the researcher had in mind when setting the research objectives.

The confusion has a more fundamental basis, however: it lies in the mere association of the two words 'information' and 'need'. This association imbues the combined concept with connotations of a basic 'need' qualitatively similar to other basic 'human needs'. However, if we examine the literature on human needs we-find that it is divided by psychologists into three categories:

*   physiological needs, such as need for food, water, shelter etc,
*   emotional or 'affective' needs, such as the need for attainment, for domination, etc,
*   cognitive needs, such as the need to plan, to learn a skill, etc.

(See, for example, the entry for the word NEED in [Eysenck, _et al.,_ 1972](#ref2))

Taking the three categories of personal needs, it will be quickly recognized that they are inter-related: physiological needs may trigger affective and/or cognitive needs, affective needs may give rise to cognitive needs, and problems relating to the satisfaction of cognitive needs (such as a failure to satisfy needs, or fear of disclosing needs) may result in affective needs (eg for reassurance). These inter-relationships are expressed in Figure I, which shows that, as <u>part</u> of the search for the satisfaction of these needs, an individual may engage in information-seeking behaviour: indeed, it may be advisable to remove the term 'information needs' entirely from our professional vocabulary and to think instead of 'information-seeking towards the satisfaction of needs.'

![Figure 1](../p76fig1.gif)

This is not to suggest that some affective or cognitive need will _immediately_ 'trigger' the response of information-seeking. Many factors other than the existence of a need will play a part: the importance of satisfying the need, the penalty incurred by acting in the absence of full information, the availability of information sources and the costs of using them, and so forth. Many decisions are taken with incomplete information or on the basis of beliefs, whether we call these prejudice, faith or ideology. So, information- seeking may not occur at all, or there may be a time delay between the recognition of the need and the information-seeking acts, or, in the case of affective needs, neither the need nor its satisfaction may be consciously recognized by the actor, or a cognitive need of a fairly low level of salience may be satisfied by chance days, months or even years after it has been recognized, or the availability of the information may bring about the recognition of a previously unrecognized cognitive need. These factors are represented in figure I as personal, interpersonal, and environmental barriers to information-seeking.

If we take the earlier analysis of the definitions of information we can see that the different senses are more or less related to the above needs. Thus:

*   the **_factual content,_** or subject data, or a document may satisfy cognitive needs, in fact this is the usual sense in which we think about the use to which information is put. However, it may also satisfy affective needs such as the need for security, for achievement, or for dominance. Some documents are designed exactly with affective needs in mind from pornography to the Tibetan 'Book of the dead';
*   the **_channel of communication_**, particularly the choice of oral channels may well be guided by affective needs as much, if not more than, by cognitive needs. For example in seeking information from a superior someone more interested in being recognized and accepted as a particular kind of person than in the actual subject content of the message: in other words he may be seeking approval or recognition. The transfer of information to others may also be done for affective reasons: for example, to establish dominance over others by reminding them that you are better informed and, therefore, in some sense superior;
*   the **_physical document_** may satisfy an affective need as when someone collects rare bindings because of their beauty, or illustrated books for the same reason. Under extreme circumstances documents may serve physiological needs as when the tramp on the park-bench covers himself with newspapers to avoid freezing to death.

The fact that information may be represented in modes other than the physical document should not be ignored, of course, particularly when those modes have either a strong influence in a culture (e.g., television) or when the mode offers the possibility of interaction (e.g., computer conferencing). Choice of modes to use in searching will be guided not only by cognitive needs (e.g., when it is known that an appropriate data-base is on-line) but also by affective needs (e.g., the greater visual variety offered by TV) and by other environmental or economic factors.

If, as suggested here, personal needs are at the root of motivation towards information-seeking behaviour, it must also be recognized that these needs arise out of the roles an individual fills in social life. So far as specialized information systems are concerned the most relevant of these roles is 'work-role', that is, the set of activities, responsibilities, etc of an individual, usually in some organizational setting, in pursuit of earnings and other satisfactions.

At the work-role level it will be clear that the performance of particular tasks, . and the processes of planning and decision-making will be the principal generators of cognitive needs; while the nature of the organization coupled with the individual's personality structure will create affective needs such as the need for achievement, for self-expression, and self-actualization. The particular pattern of needs and the resulting form of information-seeking behaviour will be a function of all of these factors, plus factors such as the organizational level at which a role is performed and the 'climate' of the organization.

Again, the search for determining factors related to needs and information-seeking behaviour must be broadened to include aspects of the environment within which the work-role is performed. The immediate work-environment and its 'climate' has been mentioned above, but the socio-cultural environment, and the physical environment will all have an impact in particular ways. The relationships will be too numerous to detail here but examples can be instanced:

*   the economic climate and the differential stratification of resources will define some work environments as 'information-poor' and others as 'information-rich' with consequent effects upon the probability of information-seeking behaviour and the choice of channel of communication;
*   the political system may define certain types of information as forbidden to particular groups (including the general public) and, consequently, the non-availability of this material may affect performance in specific work roles;
*   the physical environment will have clear effect upon the performance of work-roles and upon cognitive, affective and physiological needs, determining, for example, the physical conditions within which work is performed and the type of cognitive need that may result (questions emerging out of drilling for oil in the North Sea are likely to differ in many cases from those that emerge out of drilling in Saudi Arabia).

Figure 1 presents a view of probable inter-relations among personal needs and these other factors. When we talk of "users' information needs", therefore, we should not have in mind some conception of a fundamental, innate, cognitive or emotional 'need' for information, but a conception of information (facts, data, opinion, advice) as one means towards the end of satisfying such fundamental needs. Most user-studies research has suffered from its concentration on the means rather than on the ends and it is this that has led to dissatisfaction with the results of such research since the service implications of the results have been far from obvious. There are exceptions: the Baltimore study- ([Warner, _et al._, 1973](#ref3)) comes immediately to mind but even here, I think, the analysis has not been fully followed through to its logical conclusion.

## <a id="five"></a>5\. User studies and research methods

From the analysis in Sections 3 and 4 it will be clear that 'user studies' is a term which covers a very wide range of potential research, from the study of users' choices of books from a university library, through reactions to on-line search outputs, to the in-depth analysis of the underlying needs that result in information-seeking.

It should be equally clear that this implies the use of not one model of the research process or one method of research but multiple models and methods. However, in the past the dominant model has been that of 'scientific method' (often mis-interpreted) with the large-scale social survey as the predominant method of data collection. It is not suggested here that the 'scientific method' is necessarily inappropriate in the study of information users. The process of problem definition, classification of phenomena, hypothesis formulation, data collection, hypothesis testing and so forth, and the interaction of theory and method, is probably relevant to any kind of study. The point at issue, however, is whether quantitative methods have been employed in user studies inappropriately.

Quantitative social survey methods are powerful data collection devices, so much so that they are often misused to collect a great deal of data without the researcher having a theoretical (or even descriptive) framework into which to fit the data. In effect, collecting data becomes a substitute for thinking about the problem. This is to criticise the unthinking application of methods, rather than the methods themselves.

As a result, it might fairly be said that the practitioners of information work have been disappointed by user-studies research, largely because they fail to find within it recommendations for service provision. Equally, information researchers have generally failed to make an impact within any social scientific discipline because their work lacks integration with theories within those disciplines. It is suggested below that the answer to the first of these problems may lie in turning to a different model of research, where its application or utilization is considered to be part of the process. An answer to the second problem is more difficult to propose because it is bound up with the problems of socialisation into research within a field of practice without a research tradition: 'qualitative research' is proposed as a way of confronting directly the issue of the lack of theory in user studies.

## <a id="six"></a>6\. Action research

Information work is carried out in the context of organizations. Either:

*   the work is done for people who work within organizations, or
*   the work is done by an organization for the benefit of the community at large.

Much information research, therefore, must be related to the organizations or organizational sub-units in which information work is practised, and information workers will generally be more hospitable towards applied research,

Even applied research findings, however, may not find immediate utilization, no matter how well or how widely they are disseminated. The reasons for this become apparent when we compare 'pure scientific research' and its utilization with 'applied organizational research' and _its_ utilization, as in Figure 2.

![Figure 2](../p76fig2.gif)

In science research findings are 'used' insofar as they add to, change or refute elements of the total body of scientific knowledge. The diffusion of research findings, therefore, can lead directly to their utilization since the _practice_ of science is scientific research. The process of research utilization in organizations is not quite as straightforward. To begin with, the findings must be perceived by managers as relevant to the organization; they must then have sufficient weight or persuasive power to cause the manager to contemplate changing some aspect of organizational structure, technology, tasks or staff. Clearly, the changes must also carry with them the promise of reduced costs or improved performance or both. Organizations, however, are generally resistant to change and there is a definite gap between intention to change as a result of research findings and the actual accomplishment. Resistance to change is, in fact, one of the principal causes of organizational failure.<sup>[(2)](#note2)</sup>

The issues of organizational change and research utilization are tackled directly by 'action research', which, as [Foster](#ref2) (1972) notes, owes its origins to Lewin and the Research Centre for Group Dynamics in the USA. Definitions of action research vary, depending, to some extent, upon the sphere of action of the writer. Cherns offers a useful definition which emphasises the _research_ elements:

> ‘We have come to regard action research not only as a technique for engaging with organizations but also as a methodology for obtaining otherwise unavailable information and, above all, as a strategy for the diffusion of knowledge.’ ([Cherns, in Clark, 1972](#ref1) :xi)

Others emphasize the _action_ elements:

> ‘Action research aims to contribute both to the practical concerns of people in an immediate problematic situation and to the goals of social science by joint collaboration within a mutually acceptable ethical framework.’ ([Rapoport, 1970:499](#ref3))

If we think of action research as being concerned with organizational change, then an appropriate question is: 'With what aspects of the organization may action research be concerned?' Figure 3 suggests an answer:

![Figure 3](../p76fig3.gif)

The crucial organizational variables are the _structure_ of the organization the _technology_<sup>[(3)](#note3)</sup> used for the accomplishment of _tasks_, and the _people_ who use that technology within the organizational structure. Each of these variables may be subject to change and it is quite likely that change in any one may result in a need to change others. We are probably most familiar today with the idea of 'technological change' and it is not unknown, even in conservative organizational sub-units like information services, for such change to bring about a need for new organizational structures or a need for different kinds of people to use the new technology.

A need for change maybe the result not of changes in technology, but of less dramatic phenomena. Organizational growth, for example, may overwhelm existing structures, leading to inefficient task performance. In other words, the 'problematic situation' mentioned by Rapoport as a precursor of research may arise in any of these organizational elements and for a variety of reasons. When such situations do occur they offer an opportunity for action research.

Although action research has been pursued chiefly in organizations there is no need to restrict its application in this way. Services to neighbourhoods or entire communities may equally be the focus of the method. The situation is more complex, of course, when it comes to gaining 'entry' to the community at large for the purpose of research and when gaining local support for community innovation but, in principle, community processes and institutions are as available to action research as the processes and structures of organizations.

That opportunities are readily taken by action researchers can be indicated by drawing attention to the wide range of issues and contexts that action research has been concerned with, from leadership development in Japanese shipyards ([Misumi, 1975](#ref2)), through social work ([Lees & Lees, 1975](#ref2)) and the probation service ([Fineman & Eden, 1979](#ref2)), to various aspects of education: adult education for rural people ([Bowers, 1977](#ref1)), experiential education ([Margeris, 1973](#ref2)) and 'merging male and female athletic or physical-education departments' ([Parkhouse & Holmen, 1978](#ref3)).

We can now return to the user of information and ask, 'How is action research applicable to him?' Again, the answer can be given diagrammatically:

![Figure 4](../p76fig4.gif)

If we consider any information agency in terms of the four variables identified by Clark it is clear that the user of such an agency may be affected by any one of the four. Change in the needs of users over time may require changes in the way service tasks are performed; or changes in technology may enable an information service to perform tasks in service for the user not possible previously; or the needs of users may require the appointment of subject-specialized personnel to deal with them; or the growth of the number of users to be served (e.g., as a result of urban expansion) may lead to new organizational structures (e.g., branch library services). The examples could be multiplied many times over but the essentials

are there in the diagram: action research may be used to create a match between:

*   users and the organizational structure;
*   users and the tasks performed in serving them;
*   users and those who serve them; and
*   users and the technology used to deliver service.

Because action research requires an organizational or community setting the key element in the total process is _negotiation_: that is, coming to agreement with the chief executive and staff of the organization on the conditions under which the research will be permitted, on the rights of the organizational members, and on the rights of the researchers.

The process of negotiation whereby agreement on these issues is reached will be affected initially by whether the approach is from the organization to the researcher, or from the researcher to the organization. In the first case the organizational problem has been recognized by someone in the organization who seeks research help; in the second case the possibility of a problem or set of problems is postulated by the researcher who then seeks an organizational setting for the work.

When the researcher makes the approach, certain issues are likely to arise:

*   the problem postulated by the researcher may not exist, or it may be the surface effect of more' deeply-rooted problems. As a result, if this is not detected sufficiently early, the researcher may be faced with issues and difficulties for which he is totally unprepared;
*   a variation of this is that the problem presented by the researcher may not be recognized by organizational management as a problem;
*   the source of the problem is believed by the researcher to be in the organization but believed by management to be in the environment or in some controlling body;
*   the concern of the researcher, in presenting his perception of organizational problems, may be in research as an academic activity: it may be difficult to negotiate, therefore, if he fails to realize that the manager may be more concerned with survival or growth;
*   is acceptance of the researcher's ideas a ploy in a personal power-game, etc., rather than a genuine recognition that the problems outlined are real?

will key individuals seek to direct the research towards justification of decisions already taken?

In brief, if the researcher is to be successful in negotiation he must be sensitive to these issues.

Although problems are likely to arise at almost any stage of an action research programme (as in any other kind of research) the process itself is relatively easy to describe, at least in 'ideal type' terms. Figure 5 is an attempt to illustrate the essentially cyclical nature of the process. In theory the process could continue indefinitely but, clearly, at some point an optimum position may be reached with regard to whatever aspect of organizational functioning is under investigation to allow the outside researcher to withdraw. Alternatively, the research element in the process may be taken over by organizational members themselves and continued until a satisfactory position is reached.

![Figure 5](../p76fig5.gif)

Something has been said already about the negotiation phase of action research, but the remaining elements in Figure 5 can be expanded as follows:

**_Data collection_**: all standard methods of data collection may be employed in an action research programme. In the work described in the following chapters it will be seen that observation and interviewing were chiefly employed but self-observation through diary-keeping<sup>[(4)](#note4)</sup>, self-completed questionnaires, activity records, and the analysis of documents may all be appropriate. The nature of the organization, the degree of co-operation that can be achieved, and the research aims will influence the final choice.

**_Data feedback_**: is an essential element in action research, which distinguishes it from much other research. Much research is never reported directly to those from whom the data were collected but, in action research, feedback is essential if management and other organizational members are to be persuaded of the need for action. The feedback may be done by written report, by oral presentation at meetings, or by both of these means. The use of the data in in-house training courses may well be the most effective way to give feedback.

**_Discussion_**: follows naturally upon the feedback phase, particularly in oral presentations and training programmes. Ideas for action may emerge early in an observation phase but the researchers' ideas will be modified in the course of translating them into action.

**_Action plans_**: are simply the conversion of the previous discussions into statements of intent and detailed descriptions of changes to be made or innovations to be introduced into a system. A detailed time-table will be needed and a clear understanding must be established of the different needs of the researcher and the practitioner. This may require further negotiation and the appointment of a steering group for the duration of the research so that researchers and members of the client organization have a forum for the discussion of problems as they arise.

**_Implementation and monitoring_**: the idea of 'implementation' is self-explanatory; the action plan is put into effect, the intended time-table is adhered to (as far as possible), and the separate activities that constitute the change or innovation are embarked upon. What puts the research element into this is, of course, the monitoring process. Monitoring is simply another phase of data collection and the remarks made above under that heading apply here. Monitoring may involve the collection of records of various activities. For example, in assessing the use of a current awareness bulletin one may conduct interviews with users on the uses to which the documents they received were put, as well as collecting aggregate data on the circulation of the bulletin, demands arising, the sources from which demands were satisfied, and costs.

**_Evaluation_**: involves the assessment of all data collected during the monitoring process (both quantitative and qualitative) with a view to determining the 'success', 'usefulness', or 'value' of the innovation. In many cases value judgements are bound to enter into the treatment of-data, particularly in the weight given to different kinds and sources of data. Such value judgements must be clearly exposed to the organization in the next phase.

**_Feedback_**: the monitoring process is necessary to enable the client organization to decide whether to continue an innovation, extend its scope, change it in some way indicated by the data, or end it. So that the appropriate decision can be reached it is necessary to engage in another feedback round: again this may be a written or oral presentation or (preferably) both. Discussion of the data, and the researcher's evaluation, may reveal alternative lines of action not perceived by the researcher, leading naturally to another round of investigation.

**_Problem re-definition, etc._**: if any action other than ending the innovation results from the evaluation it will be necessary to redefine the problem and engage in another round of data collection, action planning, etc. At this stage the researcher may withdraw, leaving the organization to pursue the research process alone, may remain as an advisor giving technical assistance on method etc., or may remain fully involved. Clearly, this will depend upon the needs and wishes of the organization and upon the availability of research funds.

As noted earlier, the above description defines an 'ideal type' of action research and projects may vary in the emphasis given to one phase or another, in the extent to which the actions are viewed as experimental or permanent changes, and in the degree of involvement of the client organization's managerial or other staff. At this point, therefore, it is necessary to show how a specific project matched the 'action research' model.

Project INISS<sup>[(5)](#note5)</sup> was described from the planning stage as an action research project in which there would be three stages:

*   a period of data-collection through structured observation in departments which would thereby give the research staff insights into the day-to-day functioning of departments as well as allowing the development of research hypotheses;
*   a stage of structured interviews to allow for the collection of a greater amount of data to test research hypotheses developed following the third stage; and
*   a period of experimental implementation of innovations in information services following upon the feedback of data from stages one and two and the choice of innovations in consultation with departmental management and field staff.

The project, therefore, might be described as being 'research-led' rather than arising spontaneously out of problems perceived by the participating organizations. This, however, is too simplistic a view: the project had its origins in the research forum mentioned earlier. Top of the participants' list of priorities was a set of studies on

> ‘...the present state of information exchange ... including case-studies of social services departments, their channels of communication, the information-seeking behaviour or practitioners and the dissemination of research findings to them.’ (Mann and Wilson, 1974:25)

Furthermore, the process of identifying interested departments and then negotiating entry ensured that the research would be carried out in departments that saw communication and information transfer as problematical.

In general, the project followed the course described above as an ideal type: after the first period of data-collection by observation the results were fed back to the subjects in the form of 'narrative accounts', and, following the interview stage, reports were submitted to each participating organization. Further feedback took place during the negotiation stages which prefaced the experimental innovations.

The 'action plans' for the innovations were developed through discussions with management and with a 'steering group' for each innovation. Following implementation, monitoring and evaluation reports were made to the participating departments and further action ensued.

Another project in the UK which was going on at the same time as Project INISS was conducted in Wiltshire Social Services Department ([Blake _et al._, 1979](#ref1)). Very little methodological information is available for this project but it is clear that it too, was research-led although apparently less dependent upon data-collection and more concerned with the qualitative analysis of information needs. The final report on the Project is not yet available.

In the USA [Olson](#ref3) (1979) has employed an action-research approach in the analysis of the impact of information innovations on R & D projects. One of Olson's conclusions is extremely interesting as it draws attention to a benefit which is not usually sought from a 'normal' research project but which is typical of an action-research project:

> ‘It is clear that a major benefit of doing experimental research in a company is the surfacing of the entire range of factors which affect the project's success and greater awareness of the importance of the problem.’ ([Olson, 1979:IV-5](#ref3))

The question may be asked: 'Is it possible to move from action research to the more generally understood model of scientific, empirical research?' To a certain extent this reverses what is generally seen as the direction of influence between the two modes. For example, [Clark](#ref1) (1972) notes that action research is 'a strategy for distributing knowledge' and that the 'rules of the game' 'differ sharply from those adopted in presenting papers to colleagues in the scientific community. ' However, Clark also notes that action research _is_ concerned with adding to the stock of social science knowledge and it appears that this may be achieved at least two ways:

a) a practical situation of problem solving offers the possibility of testing social/behavioural science models and theories in the 'real world.' Given sufficient case studies involving similar situations, an appreciation of the congruence between theory and reality can be achieved; and

b) the process of testing theory in the real world will lead to modifications in those theories, which will result in the formulation of hypotheses which require empirical, quantitative testing.

There should be no doubt, however, that action research is an _applied_ research strategy which deals with practical problems. It is not 'pure' research and never can be; the establishment of links between 'action' and 'science' is a matter for the action researcher to wrestle with in the course of his investigations.

## <a id="seven"></a>7\. Qualitative research

Action research had its origins in the behavioural science approaches to organizational research and in training and organizational development (OD). Action research is defined by [French and Bell](#ref2) (1973) as an integral part of the OD consulting process. 'Qualitative research', another emerging trend in information science had _its_ origins in sociology - or, rather, in social anthropology via sociology.

Up to the 1960s it is fair to say that the dominant 'school' in sociology, particularly in the USA, was functionalism, with Parsons (1951) as the 'headmaster'. The emphasis of functionalism was upon causal relationships with a strong quantitative, statistical component. As a recent text on social research method puts it:

> ‘In general, the structural functionalist tends towards macrotheoretical considerations. He ... tends to analyze social behavior by identifying structural antecedents to behavior and assessing these behaviors in :terms of their functional or disfunctional aspects ... Behavior is characteristically defined as the result of factors acting upon individuals "causing" them to behave in a certain way.’ ([Hartman & Hedblom, 1979:53](#ref2))

This quantitative, causal analysis approach is also associated with the term 'positivism': a term of abuse in the writings of some authors. Essentially, positivism identifies the rational approach of the physical sciences (although, with the development of quantum mechanics the notion of rationality itself is somewhat at risk even within physics, see, for example, [Zukav](#ref3) (1979)).

Brown notes:

> ‘With respect to the human studies, these philosophical canons of natural science require that persons be redefined as objects. People become bodies, actions become motions, situations become events. Cognition is separated into objective and subjective dimensions — corresponding to the primary and secondary qualities of things in the external world.’ ([Brown, 1977: 80](#ref1))

Brown comments further, in discussing the various schools that have arisen in response to positivism:

> ‘The naturalistic fact-world is not in itself a finding of science, it is a presupposition or assertion about the nature of Being in general ... positive science gives procedures for identifying shared attributes and temporal correlations of entities ... Yet, ... these procedures offer no help in knowing either the nature of the entities themselves or the meaning or relevance of the attributed causes, probabilities or laws.’ ([Brown, 1977:94](#ref1))

This philosophical shift from social facts as _objective_ facts to the idea of facts as subjectively and socially _constructed_ has come to be associated with the concept of _qualitative_ research - indeed, it can be said that the ideas are necessarily associated since a change in the metaphysical basis of investigation demands a change in methods of research.

In the field of economics Piore came to a similar conclusion noting that 'information' in this field is synonymous with the values of parameters in an economic model:

> ‘...limited information ... in economics generally means lack of information about the precise value of a given set of variables.’ ([Piore, 1979:565-566](#ref3))

He comments further:

> ‘This conception of the problem, however, obscures a priori step in the epistemological process, identifying the variables that are worth estimating in the first place. Econometrics avoids this step by drawing upon economic theory. But economic theory also skips this step! It attributes to the economic actors a priori knowledge about how the world is structured and what the values in the relevant variables actually are ... [but] it is not at all clear how the actors acquire the knowledge that economists attribute to them ...’ ([Piore, 1979:: 566](#ref3))

The methodological consequence for Piore was to use open-ended, unstructured interviewing rather than the structured schedule he had employed at the beginning of his investigation. The result, for Piore, was striking:

> ‘The stories [free-flowing comment in interviews] revealed that the processes of technological change and labor allocation, indeed the basic process of business management, were totally different from the ways in which the original project had been conceived.’ ([Piore, 1979: 561](#ref3))

The methods employed in qualitative research overlap to some extent those of traditional 'quantitative' social research in that they include interviewing but the qualitative researcher is likely to use less formally structured interviewing procedures and may, in addition, use methods such as observation, free-flowing discussion, and the analysis of documents, whether personal or organizational, produced by the subjects. Under one school of thought, 'Qualitative research ... is concerned with developing concepts rather than applying pre-existing concepts,' ([Halfpenny, 1979](#ref2)) and, given the state of theory in information science (that is, its undeveloped state) it can well be argued that 'developing concepts' is what is needed. At a recent seminar at the Open University some of the main points of difference between quantitative and qualitative research were identified by speakers:

*   the epistemological issue is fundamental: social phenomena _are_ different in kind from physical phenomena and the fact that different ways of knowing are appropriate should direct one's entire research strategy;
*   associated with this is the notion of differing perceptions of the same phenomena or situations by the different actors, and the need to uncover these perceptions through the choice of appropriate method;
*   the in-depth analysis of situations and their perception is more appropriately undertaken through the study of cases than through the study of samples;
*   the analysis of case data proceeds by interpretation rather than by causal analysis. It is here that problems arise in demonstrating the _rigour_ of qualitative research and of revealing _how_ the interpretation has been performed;
*   to speak of 'data' in qualitative research may be misleading: the word 'evidence' may be more appropriate since the relevance of the evidence to the research objectives involves a greater exercise of judgement than in quantitative research;
*   in qualitative research the emphasis is on interpretative analysis and understanding, while in quantitative research the emphasis is on causal analysis and predictive understanding;

finally, the application of the results of qualitative research must take place through a comparison of a given situation with that reported: the researcher says, "This is what I believe to be the case in the situations I have studied. Examine my evidence and my interpretation and consider for yourself whether the conclusions I reach are applicable to your situation. ' In quantitative research the results and conclusions are presumed to be of general applicability because of the representative nature of the sample members.

Qualitative research seems particularly appropriate to the study of the needs underlying information-seeking behaviour because:

*   our concern is with uncovering the facts of the everyday life of the people we are studying;
*   by uncovering those facts we aim to understand the needs that exist which press the individual towards information-seeking behaviour;
*   by better understanding of those needs we are able to understand what meaning information has in the everyday life of the people; and
*   by all of the foregoing we should have a better understanding of the user, be better able to design more effective information services, and be better able to create useful theory of information-seeking behaviour and information use.

## <a id="eight"></a>8\. Conclusion

This paper suggests a number of related propositions:

*   that 'information science' is a multi-disciplinary field rather than a single, united discipline;
*   that information science studies of information users have often been unsatisfactory because of, (a) a lack of theory to guide research; and (b) an overly crude use of social research methods;
*   that the development of a theory of information-seeking behaviour and information use must be related to factors that generate cognitive and affective needs in individuals and their resulting goal-seeking behaviour;
*   that, methodologically, these relationships are more likely to be found through the use of qualitative research methods, certainly as a preliminary to quantitative research and, possibly, as a complete alternative; and
*   that the insights gained through basic research are most likely to find practical application through action research in organizations.

These propositions are closely inter-related and, clearly, may be seen as the basis of a plan to improve the quality of user studies research, its application to systems and more effective systems design.

A shift of focus in information science research towards qualitative methods and action research would support the proposition that information science is a social science by insisting upon the more intelligent use of social research methods for the development of models from the point-of-view of the philosophy of social (rather than physical) science. It would require that such models pay more attention to the behavioural and organizational contexts of information-seeking than hitherto, and to the totality of information resources and transfer mechanisms. Information science is likely to make more progress in this way than by seeking to evolve on the basis of socially sterile models from cybernetic theory.

An information science firmly founded upon an understanding of information users in the context of their work is also likely to be of more use to the information practitioner by pointing the way to practical innovations in information services and to potentially beneficial associations with other communication/information-related systems. It does little injustice to the historic record to suggest that information sercice has developed more by copying previously-existing examples than by genuine analysis of the needs of potential users. An orientation towards the user in the true sense, that is, avoiding preconceptions about what constitutes 'information' while concentrating upon the problems that create cognitive and/or affective needs in the information user, must result in a greater humility about the potential value of traditional information practices and a greater willingness to innovate and experiment.

## <a id="ackn"></a>Acknowledgements

My thanks first, to Professor Gernot Wersig for taking the trouble to discover a copy of this report in his archives, which are obviously much better organized than my own! At the time the report was written, it owed much to the work I had carried out on the INISS project with my colleagues, David Streatfield, Christine Mullings, Barbara Pendleton and Vivien Lowndes-Smith, and I would like to acknowledge my debt in this re-publication.

<a id="notes"></a>**_Notes:_**

<a id="note1"></a><sup>1</sup> The ideas presented here also appear in a paper submitted for publication (Wilson, 1980)

<a id="note2"></a><sup>2</sup> The additional, methodological point made in the diagram is that pure scientific research is incremental because of continued attention to the same problems usually by more than one researcher or research team. Applied organizational research has no such guarantee of continuity or replication.

<a id="note3"></a><sup>3</sup> 'Technology' as used here simply means 'the ways in which things are done.'

<a id="note4"></a><sup>4</sup> See Stewart (1979) for an excellent survey of the advantages and disadvantages of diaries for data-collection in organizational research.

<a id="note5"></a><sup>5</sup> The acronym stood for two titles over the life of the Project: 'Information needs and information services in local government social services departments', and 'Information innovations in social services'. (Wilson and Streatfield, 1977; Wilson, Streatfield and Mullings, 1979, Streatfield and Wilson, 1980)

**_Bibliographical note:_ **I suggest that the appropriate bibliographic reference for this paper should be:

> Wilson, T.D. _Recent trends in user studies: action research and qualitative methods_. Berlin: Freie Universität, Institut fűr Publizistik und Dokumentationswissenschaft, 1980\. (Projekt Methodeninstrumentarium zur Benutzerforschung in Information und Dokumentation, MIB P1 11/80) Available at: http://InformationR.net/ir/paper76.html

<a id="refs"></a>

## References

<a id="ref1"></a>
*   Belkin, N. (1978) 'Information concepts for information science.' _Journal of Documentation_ **34**, 55-85.
*   Bernal, J. D. (1948) ‘Preliminary analysis of pilot questionnaire on the use of scientific literature.’ <u>in</u> _Proceedings of the Royal Society scientific information conference._ London: Royal Society.
*   Blake, B, Markham, T and Skinner, A (1979) 'Inside information: social welfare practitioners and their information needs.' _Aslib Proceedings,_ **31**, 275-283.
*   Bowers, J. (1977) 'Functional adult-education for rural people - communication, action research and feedback.' _Convergence_<u>,</u> **10**, (3), 34-43.
*   Brown, R. H. (1977) "The emergence of existential thought: philosophical perspectives on positivist and humanist forms of social theory. ' <u>in</u> _Existential sociology,_ edited by J. D. Douglas and J. M. Johnson. Cambridge: CUP. 77-100.
*   Burns, R. W. and Hasty, R. W. (1973) _A survey of user attitudes toward selected services offered by the Colorado State University Libraries._ Fort Collins, CO.: Colorado State University Libraries.
*   Clark, P. A. (1972) _Action research and organizational change._ London: Harper & Row.
*   Dammers, H. F. (1974) 'Economic evaluation of current awareness systems' <u>in</u> _Eurim: a European conference on research into the management of information services and libraries. 20-22 November 1973 ..._ Paris. London: Aslib.
*   Debons, A. (1980) Foundations of information science, <u>in</u> _Theory and application of information research: proceedings of the Second international research forum on information science ... 1977_ **...** Edited by Ole Harbo and Leif Kajberg. London: Mansell.
*   Debons, A. _and_ Cameron, W. J. _eds_. (1975) _Perspectives in information science: proceedings of the NATO Institute ... Aberystwyth, 1975_<u>.</u> Leyden: Noordhoff.
*   Dugger, E. <u>and</u> Klinger, R. F. (1967) ‘User evaluation of information-services’, <u>in</u> _Information retrieval: the user's viewpoint, an aid to design. Fourth annual national colloquium on information retrieval_, edited by A. B. Tonik. Philadelphia, PA: International information inc.
*   Eysenck, H. J., Arnold, W. and Meili, R. eds. (1972) _Encyclopedia of psychology_. London: Search Press.
<a id="ref2"></a>
*   Farradane, J. (1976) 'Towards a true information scientist.' _Information Scientist,_ **10**, 91-101.
*   Fineman, S. <u>and</u> Eden, C. (1979) 'Change within the probation service - evaluation from an action research perspective.' _Personnel Review_, **8**, 30-35
*   Ford, G. ed. (1977) _User studies: an introductory guide and bibliography_. Sheffield: Centre for Research on User Studies. (Occasional paper no. 1)
*   Foster, M. (1972) ‘An introduction to the theory and practice of action research in work organizations’. _Human Relations,_ **25**, 529-556.
*   French, W. L. <u>and</u> Bell, C. H. (1973) <u>Organization</u> <u>development:</u> <u>behavioral science</u> <u>interventions</u> <u>for</u> <u>organization</u> <u>improvement.</u> Englewood Cliffs., N.J Prentice Hall.
*   Halfpenny, P. (1979) "The analysis of qualitative data', <u>Sociological</u> <u>Review (New</u> <u>series),</u> _27_,_ 799-825.
*   Hartman, J. J. <u>and</u> Hedblom, J. H. (1979) <u>Methods</u> <u>for</u> <u>the</u> <u>social</u> <u>sciences.</u> Westport, Conn.: Greenwood Press.
*   International conference on scientific information (1959) <u>Proceedings</u> <u>... Washington,</u> November, 1958\. Washington, D.C.: National Academy of Sciences - National Research Council.
*   Lees, R. I. _and_ Lees, B. (1975) 'Social issues in social work practice: case for an action research approach.' _British Journal of Social Work_<u>,</u> **5**, 161-174.
*   McQuail, D. (1975) _Communication_<u>.</u> London, New York: Longman.
*   Mann, M. G. and Wilson, T. D. _eds_<u>.</u> (1974) _Forum on social welfare library/information research **...** Report of proceedings, together with background papers prepared for the Forum_. Sheffield: University of Sheffield, Postgraduate School of Librarianship and Information Science.
*   Margeris, C. J. (1973) 'Experiential education and action research - some organizational dilemmas of action research in education.' _Instructional Science_<u>,</u> **2**, 53-62.
*   Menzel, H. (1960) _Review of studies in the flow of information among scientists_. New York: Columbia University, Bureau of Applied Social Research.
*   Misumi, J. (1975) 'Action research on development of leadership, decision-making processes and organizational performances in a Japanese shipyard.' _Psychologia,_ **18,** 187-193.
<a id="ref3"></a>
*   Olson, E. E. (1979) _The impact of behavioral and technical information interventions on industrial R. 5 D. projects._ _Final report._ Rockville, MD: Capital Systems Group, Inc.
*   Paisley, W. J. (1965) _The flow of (behavioral) science information; a review of the research literature._ Stanford: Institute for Communication Research.
*   Parkhouse, B. L. 1\. and Holmen, M. G. (1978) 'Action research paradigm to facilitate merging male and female athletic or physical-education departments.' _Research Quarterly_, **49**, 228-236.
*   Parsons, T. (1951) _The social system._ Glencoe, IL.: Free press.

*   Piore, M. J. (1979) 'Qualitative research techniques in economics.' _Administrative Science Quarterly,_ **24**, 560-569.
*   Rapoport, R. N. (1970) 'Three dilemmas in action research. _Human Relations_, **23**, 499-514\.
*   Royal Society Scientific Information Conference. (1948) _Proceedings .._. London: Royal Society.
*   Stewart, R. (1979) 'Diary keeping: a review of its use and utility as a research tool.' in _Managerial communication; concepts, approaches and techniques_, edited by T. D. Weinshall. London: Academic Press, 53-54.
*   Streatfield, D. R. <u>and</u> Wilson, T. D. (1980) _The vital link: some current research on communication in social services departments._ London: _Community Care_and the Joint Unit for Research in Social Services.
*   Warner, E. S., Murray, A. D. and Palmour, V. E. (1975) _Information needs of urban residents._ Washington, D. C.: U. S. Dept. of Health, Education, and Welfare, Bureau of Libraries and Learning Resources.
*   Weinshall, T. D. ed. (1979) _Managerial communication: concepts, approaches and techniques._ London: Academic Press.
*   Wilson, T. D. (1980) On user studies and information needs. (Unpublished, draft paper). [Subsequently published in _Journal of Documentation_, **37**, 3-15.]
*   Wilson, T. D. <u>and</u> Streatfield, D. R. (1977) 'Information needs in local authority social services departments: an interim report on Project INISS.' _Journal of Documentation_<u>,</u> **33**, 277-293.
*   Wilson, T. D., Streatfield, D. R. and Mullings, C. (1979) 'Information needs in local authority social services departments: a second report on Project INISS' _Journal of Documentation_<u>,</u> **35**, 120-136.
*   Zukav, G. (1979) _The dancing Wu Li masters: an overview of the new physics._ London: Rider/Hutchinson.