#### Vol. 12 No. 3, April 2007

* * *

# Users' information behaviour - a gender perspective

#### [Jela Steinerová](mailto:jela.steinerova@fphil.uniba.sk) and [Jaroslav Šušol](mailto:jaroslav.susol@fphil.uniba.sk)  
Comenius University Bratislava,  
Faculty of Philosophy,  
Gondova 2,  
818 01 Bratislava,  
Slovakia

#### Abstract

> **Introduction**. The paper is based on the study of library users in Slovakia as part of a larger research project on the use of information.  
> **Method.** A large-scale questionnaire survey was conducted in 2002 in sixteen academic and research libraries with 793 subjects, especially students and educators.  
> **Analysis.** The data were analysed with the use of statistical package SPSS. Gender differences are analysed with regard to ways of information seeking, use of electronic resources and publishing.  
> **Results.** Results indicate that men prefer individual information seeking and women apply collaborative information use. By sorting user types it was found out that women tended to manifest a pragmatic way of information use (the S type). Men confirmed analytic information processing (the A type). Women declared less experience in the use of electronic resources and publishing. Differences in orientation, collaboration and feelings have been noted.  
> **Conclusion.** Gender as a variable can be productive for better understanding of cognitive and social background of human information processing. Findings can inform design of services and systems and information literacy policies.

## Introduction

Human information behaviour is an integrated activity at various levels of information processing and use. Many theories and models have already emerged from user studies in librarianship and information science since 1960s; others have integrated influences from social sciences including cognitive and behavioural studies. Since 1990s we have noted new integrated models and methodologies (e.g., [Wilson 1999](#wi99), [Dervin 2003](#de03), [Ellis 1990](#el90), [Järvelin and Ingwersen 2004](#ja04), [Kuhltau 1993](#ku93), [Bates 1989](#ba89), [Nahl 2001](#nah01)). The studies can be applied to practice of organizations, to information management and to education and information literacy. The results can also inform practice of new media and the everyday life information use (e.g., [Chatman 2000](#cha00)). Most significant contributions to theories of information behaviour have been summarized in _Theories of Information Behavior_ ([Fisher _et al._ 2005](#th05)). New developments can be found in _New Directions in Human Information Behavior_ ([Spink and Cole 2005](#ne05)). In spite of the fact that one can find plethora of theories, the subject can hardly be unified into a common framework. A possible view is the focus on gender differences in users' information seeking behaviour.

In this paper we present a gender analysis of data that have been acquired as part of our research project on the Interaction of man and the information environment. The first sections determine the concepts of gender, personality and related research. In the second part we report on our own survey including questions, methodology and results. We concentrate on manifestations of male and female information seeking behaviour, gender differences in the use of electronic information resources and comparison of user types and gender.

## Concepts of gender and personality in human information behaviour

We regard gender as a cultural and social construction of a personality which is manifested in qualities and behaviour of men and women. Sex is innate and usually bears a sign of personality development, identity and social roles. Social psychology defines physical, mental and social differences of men and women. The former includes original roles of hunters, fighters and protectors and the latter includes qualities of care and sensitivity to others, emotional expressivity, adaptability (e.g., [Kalnická and Geller 2001](#ka01), [Ruisel 2004](#ru04), [Renzetti and Curran, 2003](#re03)).

Following a general social psychological approach, the personality is viewed as a complex of biological, mental and social factors ([Nakonecny 1998](#nak98)), which can influence human information behaviour. Five personality factors used in information behaviour studies ([Heinström 2000](#he00), [2003](#he03)) and cognitive styles ([Ford _et al._ 2000](#fo00), [Ruisel 2004](#ru04)) have been the main sources for data analyses at the level of personality types. Based on this we have derived user types and compared them with gender differences in information seeking behaviour.

We suppose that different development of mental representations and different emphasis on social and cultural contexts have an impact on information behaviour. While women stress the need for relations between people, men are concentrated on individual performance. This has been proved by several studies of differences between men and women in the use of Internet (e.g., [Fallows 2005](#fa05), [Losh 2003](#lo03), [Kennedy _et al._ 2003](#ke03)). The _gender gap_ can be seen in different communication, information and recreation patterns. The longitudinal study of Pew Internet and American Life Project ([Fallows 2005](#fa05)) shows that women appreciate especially the communicative features of the Internet, while men are more likely to use online transactions, get information, play games and use entertainment. Women have admitted more concerns with regard to privacy and misuse of Internet and larger information overload. Some authors explain the gender digital divide by different educational and occupational experiences ([Losh 2003](#lo03)).

Research studies of human information behaviour address questions of different cultural and social contexts, work tasks, domains, or information grounds ([Fisher _et al._ 2005](#th05)). These contexts cannot reduce information behaviour to mechanical information processing. Therefore, we take into account integrative levels of information processing (neurophysiological, cognitive, social) and try to find evidence of gender-specific information behaviour manifestations. Of course, current psychological literature provides more complex and controversial debate on biologically or socially constructed gender, which is out of the scope of this paper ( see, e.g., [Renzetti and Curran 2003](#re03), [Julien 2005](#ju05), [Nakonecny 1998](#nak98).

### Gender differences in related human information behaviour studies

Previous research shows that men and women value technologies differently ([Fallows 2005](#fa05), [Agosto 2001](#ag01)). In the use of technologies women prefer social collaboration, contextual information and personal identification. Men's use of information and technologies are determined by preferences of individual work and competition. For men new technologies represent an intellectual challenge and play. With women usually more emotional perception occurs. According to Agosto ([2001](#ag01)), for women it is important to include the information into broader context or story. In learning, women make use of personal identification and imagination. Studies also prove that women suffer from lower self-confidence in managing technologies ([Kennedy _et al._ 2003](#ke03)).

Differences of online communication have an impact on questions and queries in information seeking and retrieval. Because of men's preferences for logical and analytical thinking, it is easier for them to use Boolean logic for query formulation ([Agosto 2001](#ag01)). The rich verbal abilities of women support the use of a wider vocabulary and multiple syntactic relationships.

In practice of information seeking systems women's ways of knowing have been devalued, e.g., contextual, connected, intuitive knowing. Deeper understanding of women's way of knowing has great potential for information behaviour research and '_highlights the value of gender as potentially significant variable_' ([Julien 2005](#ju05): 390).

Higgins and Hawamdeh ([2001](#hi01)) point to the under-representation of non-linear, non-hierarchical _female_ procedures of information processing in information systems. Interfaces and information representations of information systems are usually driven by "male" principles of intervention and control. Studies show that different information seeking behaviour of schoolboys and schoolgirls emerged. Girls used judgements based on intuition and insight; they were more patient when reading information from screen and preferred work in groups. The performance of boys was better in browsing and use of keywords. They preferred individual work and graphical (visual) representations as opposed to text. With girls the information need was conceptualized as a social event, as opposed to individualistic approach of boys ([Higgins and Hawamdeh 2001](#hi01)). Non-linear principles of information behaviour (e.g., [Foster 2005](#fo05)) and women's use of information systems are interesting for researchers. Enochsson ([2005](#en05)) points to different voices used by boys and girls when using the Internet, based on technological language and mental models. As girls lack the technological language, they show computer competences and interest differently.

Agosto ([2001](#ag01)) presented a model of gender-specific information behaviour. The use of information from Web resources by young women has been marked by the following principles: collaboration, social networking, flexibility and movement, inclusion into community, contextuality and personal engagement. Other experiments have focused on information seeking in Dialog databases ([Sullivan, M. _et al._ 1990](#su90)). It has been proved that women spent less time on preparations of information seeking and made more mistakes in database languages. Different attitudes have also been noted with regard to relevance judgements. Women judged a larger amount of information as relevant than men did. When communicating with systems women's behaviour was more interactive. They were more willing to pay for information seeking.

On the other hand, Enochsson ([2005](#en05)) shows that with the new _net_ generation differences between men and women in the use of Internet diminish. However, the socio-cultural background of gender still leaves women with more computer anxiety and feelings of lower self-efficacy. Social positioning theory offers a framework for examination of gender differences in information behaviour in terms of qualitative research methods. ([Given 2005](#gi05)).

## Research on library users in Slovakia: aspects and research questions

In Slovakia, gender studies have emerged especially since 1990, both at theoretical (philosophy, ethnology and social psychology) and practical levels. However, information behaviour has not been examined from gender perspective so far, which is why we have analysed gender differences based on surveys of library users.

We were interested in four aspects of information behaviour of library users.

1.  the usual ways of information seeking as perceived by users;
2.  current behaviour at the moment of information seeking in the library;
3.  evaluation upon completing the information seeking process;
4.  the use of electronic resources, namely seeking in Internet, network resources and perceptions of electronic publishing.

For this paper we have analysed the data on differences between the answers of men and women. We have also used detailed sorting of data based on personality types and compared the answers of men and women with those of user types. Findings do not represent the whole population of Slovakia, but predominantly student and researcher population.

We will interpret the data in terms of the following questions. What are the differences between the answers of men and women with respect to perceived ways of information seeking? What are the gender differences in the answers to current information seeking behaviour? What are the gender differences in the evaluation of information seeking process? What are the gender differences with respect to use of electronic resources and electronic publishing? What is the relationship between the answers of men and women and answers of user types based on personality types?

### Methodology

We have identified cognitive components (e.g., orientation, motivation and relevance judgements), affective components (emotions) and social components (collaboration) of information behaviour. In the survey we followed such variables as the first orientation in information resources (Internet, library catalogues), cooperation (with librarians or colleagues), perception of quality of information (professional periodicals), depth of information processing (problem understanding, increase of interest). The other variables were time, organization of work, relevance judgements and emotions.

The data were gathered by a series of questionnaire surveys in sixteen Slovak academic libraries. The main survey took place in July-September 2002\. The questionnaire was composed of statements aimed at users' perceptions of information seeking. Some questions applied a five-point Likert scale (1 - almost always, 5 - almost never) ([Kuhltau 1993](#ku93)), others offered multiple and open responses. The first three parts (A, B, C) included statements about stable preferences, information seeking behaviour and perceptions of success upon the completion of information seeking. The two other parts (D, E) were composed of questions about the use of electronic resources and electronic publishing. 793 users were included in further analyses. Details about questionnaire design, methodology and results can be found in the final report ([Steinerová _et al_. 2004](#st04)). The English version of the questionnaire is provided in the [Appendix.](paper320app.html)

The libraries were selected to represent three separate regions of western, central and eastern Slovakia and the distribution of universities in larger cities. The response rate was 79.3%, from 1000 questionnaires distributed. The users were mainly university students, academic staff and researchers. Gender division included 57.3% men and 40.7% women.

For data analyses we have used the statistical package, SPSS, version 8.0, for frequency distributions, scaling and cross-tabulations. Statistical importance of differences in data was tested (Fisher exact test, Student t-test) in GraphPadInStat software.

## Findings

### Gender differences in information seeking

In this section we report on differences synthesised from a number of previous analyses. The interpretation is based on different types of questions, that is why some statements are supported by percentage, others are derived from mean values and ordering of answers. We derived differences not only from statistical analyses, but also from open questions and qualitative analyses.

Men confirmed preferences of individual work and independent information seeking more frequently than women. Men apply straightforward access to information resources. Women use librarians' help more frequently. No significant differences have been found as for the increase of problem understanding or interest in the process of seeking.

The use of library catalogues and reference works has been confirmed more strongly by women. No differences have been noted in the use of professional journals. However, women indicated wider use of bibliographies and indexes. Women are more patient in information seeking, while with men the increasing use of fast retrieval tools has been noted. An interesting finding concerns the evaluation upon the completion of information seeking task. For example higher percentage of men (58.3%) agrees that the information confirmed their prior knowledge (compared to 41.7% of women). This could point to a pattern of a more assertive attitude of men to their knowledge states. A higher percentage of men has also confirmed that information seeking took them more time than they expected. However, women perceived lack of time more intensely (e.g., 30% of women, 20% of men).

As for one's own organization of work, 55.1% of men and 42.4% of women assess themselves as _excellent_; while the _weak_ organization of work was indicated by 63% of men and 33% of women [the proportions reflect the fact that respondents could make multiple responses to the question.] Higher self-appraisal and higher self-criticism can be identified as typical for male behaviour. The most relevant types of documents have been appreciated evenly by both groups (professional articles, encyclopaedias, monographs). There are slight differences in higher preferences of professional papers with men and higher preferences of monographs and encyclopaedias with women.

The expectations of men tend to be optimistic. They are more satisfied with results. Women approach the information seeking process more carefully. They have confirmed stronger uneasiness and anxiety at the beginning of information seeking process. Upon the completion of information seeking they indicated deeper relief. Women confirmed more frequent cooperation with colleagues. The use of Internet as the first resource is slightly more dominant with men.

With regard to feelings men indicated especially trust, disappointment and relaxation. Women mentioned trust and disappointment, but the third feeling is confusion. Another difference noted was that women expressed more doubt. At the initial stage of problem formulation men's feelings point to frustration and relaxation, while women confirmed more worries, confusion and disappointment.

Upon the completion of information seeking both groups expressed the feeling of satisfaction. However, men tended to be more satisfied with results of information seeking. With both groups the second feeling was relief, but with women it was confirmed more strongly. The careful approach of women to information use has also been confirmed by greater fear of assessment. Another interesting difference is more curiosity with women. Both men and women have an optimistic approach to information seeking. It is manifested by feelings of trust in the course of seeking and satisfaction upon the completion of seeking.

Our findings can be illustrated by mean values of answers to questions about the course of information seeking in Table 1\. None of the differences between the means for any of measures are statistically significant. There is a marginally significant difference in favour of men for choosing the Internet as the first source. Other differences can be considered in further research, for example, the tendency of women to ask for a librarian's help more frequently. Women also tend to use catalogues as the first information resource in a larger extent. Other tendencies can be noted, e.g., men confirmed preference for individual work, women the tendency to collaborate.

<table><caption>

**Table 1: Comparison of mean values of answers (overall, women, men)**  
Answers: 1 - almost always, 2 - often, 3 - sometimes, 4 - seldom, 5 - almost never  
\* marginal significance, &nbsp; p = 0.0939,  &nbsp; statistically significant difference (tested by Student t-test); &nbsp; SD - Standard Deviation</caption>

<tbody>

<tr>

<th rowspan="2">Concept</th>

<th colspan="3">Overall</th>

<th colspan="3">Women</th>

<th colspan="3">Men</th>

</tr>

<tr>

<th>Mean</th>

<th>Rank</th>

<th>SD</th>

<th>Mean</th>

<th>Rank</th>

<th>SD</th>

<th>Mean</th>

<th>Rank</th>

<th>SD</th>

</tr>

<tr>

<td>

1\. use of more libraries</td>

<td>2.35</td>

<td>5</td>

<td>1.20</td>

<td>2.34</td>

<td>5</td>

<td>1.17</td>

<td>2.36</td>

<td>6</td>

<td>1.23</td>

</tr>

<tr>

<td>

2\. librarian's help</td>

<td>3.16</td>

<td>10</td>

<td>1.17</td>

<td>2.91</td>

<td>11</td>

<td>1.14</td>

<td>3.34</td>

<td>10</td>

<td>1.15</td>

</tr>

<tr>

<td>

3\. 1st source - Internet</td>

<td>2.35</td>

<td>5</td>

<td>1.18</td>

<td>2.45</td>

<td>7</td>

<td>1.22</td>

<td>2.27</td>

<td>5</td>

<td>1.14*</td>

</tr>

<tr>

<td>

4\. 1st source - catalogue</td>

<td>2.65</td>

<td>6</td>

<td>1.13</td>

<td>2.44</td>

<td>6</td>

<td>1.13</td>

<td>2.80</td>

<td>7</td>

<td>1.13</td>

</tr>

<tr>

<td>

5\. 1st source - reference works</td>

<td>2.91</td>

<td>9</td>

<td>1.13</td>

<td>2.76</td>

<td>10</td>

<td>1.13</td>

<td>3.02</td>

<td>9</td>

<td>1.12</td>

</tr>

<tr>

<td>

6\. individual work</td>

<td>2.13</td>

<td>3</td>

<td>0.98</td>

<td>2.25</td>

<td>4</td>

<td>0.96</td>

<td>2.06</td>

<td>2</td>

<td>0.98</td>

</tr>

<tr>

<td>

7\. increase of interest</td>

<td>2.11</td>

<td>2</td>

<td>0.96</td>

<td>2.10</td>

<td>2</td>

<td>0.93</td>

<td>2.12</td>

<td>3</td>

<td>0.97</td>

</tr>

<tr>

<td>

8\. collaboration with colleagues</td>

<td>2.69</td>

<td>7</td>

<td>1.03</td>

<td>2.67</td>

<td>8</td>

<td>0.97</td>

<td>2.77</td>

<td>8</td>

<td>1.07</td>

</tr>

<tr>

<td>

9\. professional periodicals</td>

<td>2.16</td>

<td>4</td>

<td>1.00</td>

<td>2.16</td>

<td>3</td>

<td>1.03</td>

<td>2.17</td>

<td>4</td>

<td>0.99</td>

</tr>

<tr>

<td>

10\. problem understanding</td>

<td>1.96</td>

<td>1</td>

<td>0.87</td>

<td>1.95</td>

<td>1</td>

<td>0.90</td>

<td>1.96</td>

<td>1</td>

<td>0.85</td>

</tr>

<tr>

<td>

11\. indexes, bibliographies</td>

<td>2.76</td>

<td>8</td>

<td>1.16</td>

<td>2.70</td>

<td>9</td>

<td>1.14</td>

<td>2.80</td>

<td>7</td>

<td>1.18</td>

</tr>

</tbody>

</table>

Differences between men and women in information seeking result from many additional contexts. Social, organizational and cultural contexts are integrated in new models of information retrieval and information seeking (e.g., [Järvelin and Ingwersen 2004](#ja04)). Other data analyses of our study point also to different preferences of information resources within certain topics. For example, if humanities and social sciences tend to use monographs and encyclopaedia to a larger extent; sciences and engineering make more use of journals and electronic resources. Our data confirmed traditional gender stereotypes. Women tend to be involved more in the humanities and social sciences (39,3% of women, 21,14% of men); men in engineering (38,1% of men, 19,19% of women). These factors have an impact on the articulation of information needs, choice of seeking strategies and relevance judgements. Social conditions are manifested especially by tasks and everyday life situations and emerge from educational and occupational components.

### Gender differences in the use of electronic information resources

Our research has concentrated especially on networked information resources, i.e., the resources that are made available through network technologies and break the traditional image of stable and _reliable_ documents. At the level of electronic communication we have identified two possible ways in which information resources are used: for reading and for publishing. A general model derived from analyses indicates that the users' readiness to study electronic information resources is somewhat higher than their willingness to publish through electronic communication channels. Comparisons among three groups of users of electronic resources (frequent users, rare users and non-users) show that in each of these groups the proportion of the subjects who are not willing to publish electronically is higher than 30% (31.23%, 45.96% and 50.00% for each of the above mentioned groups respectively) ([Šušol 2005](#su05)).

A general model derived from analyses indicates that the users' readiness to study electronic information resources is somewhat higher than their willingness to publish through electronic communication channels ([Šušol 2005](#su05)). We have also noticed differences between real use and preferences of access to electronic resources. While 92% of subjects claim to use electronic resources during their work (of those, 67% frequently and 25% rarely), only 42% of them declare their preference for the use of electronic resources (reported in [](#st05)Steinerová and Šušol 2005 We assume that the level of real use of electronic communication channels is to a high degree conditioned by the accessibility of appropriate infrastructure.

#### Gender differences in assessment of electronic resources

As for the access to the Internet, as many as 40% of the subjects use the Internet access in the library. Only approximately 16% of users declare that they also have access to Internet at home, while 3% of them have no access. Women show a slightly lower percentage of Internet access use at home and in the library and, on the contrary, higher use in the workplace, but these differences are only marginal. The differences in the frequency of use of electronic resources cannot be considered significant. On average, almost 68% of subjects stated they use electronic resources often, while only approximately 6% of them do not use these resources at all. Women use the Internet slightly less than men, they show higher proportion of rare use and non-use of electronic resources and a lower proportion of frequent use.

More important gender differences can be found in answers to the question on using paid electronic resources (Table 2). The results indicate that men put much more stress on free, non-paid access to electronic resources, while women use more frequently resources on the basis of various licensing agreements between publishers and users' own institution. With regard to different features of electronic and traditional information resources, the results seem to confirm gender differences regarding the use of paid electronic resources (Table 3). Free access to electronic resources is less important for women than it is for men (11.4% - 16%). Differences in other variables of these tables were not statistically significant. When it comes to traditional resources, no significant gender differences were found, although women reflect slightly more on the role of a publisher, its seriousness and prestige and, surprisingly, the factor of simple use seems to be a bit less important for women.

<table><caption>

**Table 2: Using paid electronic resources**  
\*p < 0.05 - statistically significant difference between men and women (Fisher's exact test)</caption>

<tbody>

<tr>

<th rowspan="2">Using resources</th>

<th colspan="2">Women</th>

<th colspan="2">Men</th>

<th colspan="2">Overall</th>

</tr>

<tr>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

</tr>

<tr>

<td>Available free of charge</td>

<td>135</td>

<td>46.6</td>

<td>234</td>

<td>55.2</td>

<td>376</td>

<td>51.6</td>

</tr>

<tr>

<td>Paid by the institution</td>

<td>122</td>

<td>42.1</td>

<td>130*</td>

<td>30.7</td>

<td>259</td>

<td>35.5</td>

</tr>

<tr>

<td>Paid individually</td>

<td>33</td>

<td>11.4</td>

<td>60</td>

<td>14.2</td>

<td>94</td>

<td>12.9</td>

</tr>

<tr>

<td>Total</td>

<td>290</td>

<td>100.0</td>

<td>424</td>

<td>100.0</td>

<td>729</td>

<td>100.0</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 3: Features of electronic resources important for the efficiency of work**  
\*p < 0.05 - statistically significant difference between men and women (Fisher's exact test)</caption>

<tbody>

<tr>

<th rowspan="2">Quality</th>

<th colspan="2">Women</th>

<th colspan="2">Men</th>

<th colspan="2">Overall</th>

</tr>

<tr>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

</tr>

<tr>

<td>Quick retrievability</td>

<td>208</td>

<td>30.5</td>

<td>289</td>

<td>30.6</td>

<td>510</td>

<td>30.8</td>

</tr>

<tr>

<td>Up-to-datedness</td>

<td>183</td>

<td>26.8</td>

<td>230</td>

<td>24.4</td>

<td>419</td>

<td>25.3</td>

</tr>

<tr>

<td>Free access</td>

<td>78</td>

<td>11.4</td>

<td>151*</td>

<td>16.0</td>

<td>232</td>

<td>14.0</td>

</tr>

<tr>

<td>Full-text searching</td>

<td>56</td>

<td>8.2</td>

<td>91</td>

<td>9.7</td>

<td>149</td>

<td>9.0</td>

</tr>

<tr>

<td>Linking to other resources</td>

<td>101</td>

<td>14.8</td>

<td>108</td>

<td>11.5</td>

<td>213</td>

<td>12.9</td>

</tr>

<tr>

<td>Environmental friendliness</td>

<td>16</td>

<td>2.3</td>

<td>25</td>

<td>2.7</td>

<td>41</td>

<td>2.5</td>

</tr>

<tr>

<td>Multimediality</td>

<td>32</td>

<td>4.7</td>

<td>42</td>

<td>4.5</td>

<td>77</td>

<td>4.6</td>

</tr>

<tr>

<td>Other</td>

<td>9</td>

<td>1.3</td>

<td>7</td>

<td>0.7</td>

<td>16</td>

<td>1.0</td>

</tr>

<tr>

<td>Total</td>

<td>683</td>

<td>100.0</td>

<td>943</td>

<td>100.0</td>

<td>1657</td>

<td>100.0</td>

</tr>

</tbody>

</table>

Taking into account various new technological features of electronic information resources, further questions of the survey were directed at finding out if the users prefer working with information in electronic form. The results indicate that the users in general do not prefer electronic resources over printed ones. Although the percentage of those who do prefer electronic resources is slightly higher among men, and a larger proportion of women seems to be _neutral_ (no answer), the gender differences cannot be considered significant (Table 4).

<table><caption>

**Table 4: Preferences of electronic resources over printed ones**</caption>

<tbody>

<tr>

<th rowspan="2">Preferring electronic resources</th>

<th colspan="2">Women</th>

<th colspan="2">Men</th>

<th colspan="2">Overall</th>

</tr>

<tr>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

</tr>

<tr>

<td>Yes</td>

<td>123</td>

<td>38.1</td>

<td>206</td>

<td>45.4</td>

<td>333</td>

<td>42.0</td>

</tr>

<tr>

<td>No</td>

<td>157</td>

<td>48.6</td>

<td>204</td>

<td>44.9</td>

<td>372</td>

<td>46.9</td>

</tr>

<tr>

<td>No answer</td>

<td>43</td>

<td>13.3</td>

<td>44</td>

<td>9.7</td>

<td>88</td>

<td>11.1</td>

</tr>

<tr>

<td>Total</td>

<td>323</td>

<td>100.0</td>

<td>454</td>

<td>100.0</td>

<td>793</td>

<td>100.0</td>

</tr>

</tbody>

</table>

#### Gender differences in perceptions of electronic publishing

This part of the survey concentrated on the use of three types of communication technologies for publishing: electronic conferences, Web pages and electronic journals. The data confirm that women still have less experience with electronic publishing. There is a higher percentage of those female subjects who claim not to have a personal Web page (17% men, 9% women).

Users have the least practical experience with publishing in an electronic journal (6.1%). But at the same time it is judged as potentially the most acceptable among the three types (38.8% of subjects).

The proportion of those who are unlikely to publish electronically is balanced, with all the three types of publishing channels reaching approximately the same percentage of 35.3%-36.3%. Women show less practical experience and, at the same time, a somewhat higher level of doubts as far as their willingness to use technologies for publishing is concerned.

Statistically important differences can be found in practical experience with publishing in an electronic conference (women 5.9%, men 13.7%) and on a Web page (women 6.8%, men 12.6%). It is interesting to note that the difference in case of an electronic journal is not so dramatic (women 5.3%, men 6.4%). It is probably due to the fact that this type of publishing is less used by men as well. Women's refusal is strongest for publishing in electronic conference (40.9%) and weakest for electronic journal (38.7%). With men it is vice versa - weakest refusal for electronic conference (32.2%), strongest for electronic journal (34.6%). However, the overall level of rejection of publishing in electronic journal with men is still lower than with women ([Steinerová and Šušol 2005](#st05)).

Another set of data seems to confirm our findings about a reserved attitude of women towards the use of electronic communication channels for publishing (Table 5). When explicitly asked about the equality of electronic and traditional publishing, the support of men is much more straightforward.

<table><caption>

**Table 5: Equality of electronic and traditional publishing**  
**p < 0.01 - statistically significant difference between men and women (Fisher's exact test)</caption>

<tbody>

<tr>

<th rowspan="2">Equality of electronic and traditional publishing</th>

<th colspan="2">Women</th>

<th colspan="2">Men</th>

<th colspan="2">Overall</th>

</tr>

<tr>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

</tr>

<tr>

<td>Yes</td>

<td>64</td>

<td>19.8</td>

<td>149**</td>

<td>32.8</td>

<td>218</td>

<td>27.5</td>

</tr>

<tr>

<td>No</td>

<td>103</td>

<td>31.9</td>

<td>136</td>

<td>30.0</td>

<td>244</td>

<td>30.8</td>

</tr>

<tr>

<td>Do not know</td>

<td>85</td>

<td>26.3</td>

<td>88</td>

<td>19.4</td>

<td>176</td>

<td>22.2</td>

</tr>

<tr>

<td>No answer</td>

<td>71</td>

<td>22.0</td>

<td>81</td>

<td>17.8</td>

<td>155</td>

<td>19.5</td>

</tr>

<tr>

<td>Total</td>

<td>323</td>

<td>100.0</td>

<td>454</td>

<td>100.0</td>

<td>793</td>

<td>100.0</td>

</tr>

</tbody>

</table>

The analysis of results regarding qualities of electronic and traditional publishing (Table 6) shows that the attitudes of men and women are significantly different in two aspects. Men are more comfortable with the statement that principles of editorial and peer-reviewing in electronic publishing will secure the quality. Women, on the other hand, are slightly more concerned with long-term access to documents in electronic publishing (archiving). Overall, the results show that both men and women see little difference in terms of quality between print and electronic publishing.

<table><caption>

**Table 6: Qualities of electronic and traditional publishing**  
*p < 0.05 - statistically important difference between men and women (Non-pair t-test)</caption>

<tbody>

<tr>

<th rowspan="2">Qualities of electronic and traditional publishing</th>

<th colspan="2">Women</th>

<th colspan="2">Men</th>

<th colspan="2">Overall</th>

</tr>

<tr>

<th>Mean</th>

<th>SD</th>

<th>Mean</th>

<th>SD</th>

<th>Mean</th>

<th>SD</th>

</tr>

<tr>

<td>No difference in quality</td>

<td>2.91</td>

<td>1.09</td>

<td>2.77</td>

<td>1.15</td>

<td>2.82</td>

<td>1.13</td>

</tr>

<tr>

<td>Principles of editorial reviewing</td>

<td>2.77</td>

<td>0.90</td>

<td>2.58*</td>

<td>0.98</td>

<td>2.66</td>

<td>0.96</td>

</tr>

<tr>

<td>Problem of archiving in e-publishing</td>

<td>2.69</td>

<td>0.96</td>

<td>2.89*</td>

<td>1.17</td>

<td>2.81</td>

<td>1.09</td>

</tr>

<tr>

<td>Print publishing more trustworthy</td>

<td>2.70</td>

<td>1.02</td>

<td>2.70</td>

<td>1.13</td>

<td>2.70</td>

<td>1.09</td>

</tr>

<tr>

<td>Threat of changes in e-document</td>

<td>2.51</td>

<td>0.98</td>

<td>2.59</td>

<td>1.06</td>

<td>2.55</td>

<td>1.02</td>

</tr>

<tr>

<td>The importance of publishing prestige</td>

<td>2.06</td>

<td>0.91</td>

<td>2.06</td>

<td>0.89</td>

<td>2.06</td>

<td>0.89</td>

</tr>

</tbody>

</table>

It can be stated that women are less confident about the capacities of modern communication technologies for traditional publishing. This is quite clearly readable in the results summarising the importance of individual factors of choice of publishing medium (Table 7). Women appreciate more the "traditional" factors such as the suitability of the medium for scholarly communication, the range of readers' community, primacy and copyright or adherence to the criteria of scientific assessment. On the other hand it is important to point out that women also appreciate some factors which do not necessarily indicate any kind of technological scepticism - e.g., the speed of publishing. Men, on the contrary, are more interested in economic aspects of publishing, either from a viewpoint of authoring royalties or that of lower production costs. In general, both men and women saw little difference between print and electronic publication. However, men valued the economic aspects more highly and had higher regard for the review process and lower production costs.

<table><caption>

**Table 7: Factors of choice of publishing medium**  
*p < 0.05, **p < 0.01 - statistically important difference between men and women (Fisher's exact test)</caption>

<tbody>

<tr>

<th rowspan="2">Factor</th>

<th colspan="2">Women</th>

<th colspan="2">Men</th>

<th colspan="2">Overall</th>

</tr>

<tr>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

<th>Freq.</th>

<th>% resp.</th>

</tr>

<tr>

<td>Suitability of medium for scholarly communication</td>

<td>118</td>

<td>20.3</td>

<td>141</td>

<td>16.3</td>

<td>264</td>

<td>17.9</td>

</tr>

<tr>

<td>Economic evaluation</td>

<td>15</td>

<td>2.6</td>

<td>55**</td>

<td>6.4</td>

<td>74</td>

<td>5.0</td>

</tr>

<tr>

<td>More publishing possibilities</td>

<td>64</td>

<td>11.0</td>

<td>88</td>

<td>10.2</td>

<td>156</td>

<td>10.6</td>

</tr>

<tr>

<td>Primacy and copyright</td>

<td>44</td>

<td>7.6</td>

<td>59</td>

<td>6.8</td>

<td>106</td>

<td>7.2</td>

</tr>

<tr>

<td>Criteria of scholarly evaluation</td>

<td>36</td>

<td>6.2</td>

<td>52</td>

<td>6.0</td>

<td>90</td>

<td>6.1</td>

</tr>

<tr>

<td>Prestige - editorial reviewing</td>

<td>18</td>

<td>3.1</td>

<td>51**</td>

<td>5.9</td>

<td>72</td>

<td>4.9</td>

</tr>

<tr>

<td>General perception of the medium</td>

<td>27</td>

<td>4.6</td>

<td>43</td>

<td>5.0</td>

<td>71</td>

<td>4.8</td>

</tr>

<tr>

<td>Low production costs</td>

<td>42</td>

<td>7.2</td>

<td>75*</td>

<td>8.7</td>

<td>120</td>

<td>8.1</td>

</tr>

<tr>

<td>Range of readers community</td>

<td>83</td>

<td>14.3</td>

<td>110</td>

<td>12.7</td>

<td>195</td>

<td>13.2</td>

</tr>

<tr>

<td>Relevance of readers community</td>

<td>48</td>

<td>8.3</td>

<td>72</td>

<td>8.3</td>

<td>122</td>

<td>8.3</td>

</tr>

<tr>

<td>Speed of publishing</td>

<td>82</td>

<td>14.1</td>

<td>109</td>

<td>12.6</td>

<td>195</td>

<td>13.2</td>

</tr>

<tr>

<td>Other factors</td>

<td>4</td>

<td>0.7</td>

<td>8</td>

<td>0.9</td>

<td>12</td>

<td>0.8</td>

</tr>

<tr>

<td>Total</td>

<td>581</td>

<td>100.0</td>

<td>863</td>

<td>100.0</td>

<td>1477</td>

<td>100.0</td>

</tr>

</tbody>

</table>

Results of the use of electronic communication channels analysis did not reveal significant gender differences. However, data indicate traditional gender division. Women showed a slightly lower level of practical use of electronic information resources. Their attitude can be defined as more responsible and informed. Women more frequently use information resources offered to them by their institution and they select these resources rather with their quality or reliability in mind. Women also declare the lesser practical experience in using information technologies for publishing.

### Comparison of gender differences and users' personality types

Based on the data analyses and algorithmic sorting we have identified two main user personality types, the type S and the type A. Most of users have been categorized into the mixed, average type (type M). The type S (strategic type) users are marked by horizontal way of information processing and use. They use a broad range of information resources, prefer fast solutions supported by technologies. Their information behaviour draws on clear organization of information, instant access, authorities (authors) and verified quality of information. In relevance judgements they use especially formal criteria. They appreciate fast links between pieces of information in electronic resources. They usually make their decisions based on the first acquired information. In feelings the anxiety from ambiguity of concepts and information overload can be identified.

The type A (analytical) users are characterized by deep information processing and usually open personality. They prefer new information; in information seeking they combine more ways of access to information and different types of resources. These users are often willing to invest time and money to information seeking. They critically evaluate information, look for contexts and links to prior knowledge. They create their own viewpoint in understanding information. The information use is controlled by internal motivation and interest. Type A can cope better with anxiety and uncertainty in information use. It is manifested by good organization of work and planning of a conceptual model.

Within the type A, we have identified 60% of men and 36.5% of women. Within the type S, the proportion was 56.6% of men and 41.6% of women. The data analyses (Table 8) point to the type A as more similar to male behaviour and to the type S as represented by a larger portion of women. For further interpretations we have used the overall proportion of the three user types (A, S, M - 10.7/57/32.3). A tendency towards a deeper way of information processing and use with male users has been disclosed (Table 9). With women the proportion is 9.6/58.2/32.2, with men it is 11.2/56.4/32.4\. Deviations from the overall proportion in the whole data set show a slight prevalence of the type A in men, but the differences are not statistically significant (chi-squared = 0.589).

<table><caption>

**Table 8: Proportion of personality types (A/S/M) within gender groups**</caption>

<tbody>

<tr>

<th rowspan="2">Personality types</th>

<th colspan="2">Men</th>

<th colspan="2">Women</th>

<th colspan="2">No answer</th>

<th colspan="2">Total</th>

</tr>

<tr>

<th>Freq.</th>

<th>%</th>

<th>Freq.</th>

<th>%</th>

<th>Freq.</th>

<th>%</th>

<th>Freq.</th>

<th>%</th>

</tr>

<tr>

<td>A</td>

<td>51</td>

<td>60</td>

<td>31</td>

<td>36.5</td>

<td>3</td>

<td>3.5</td>

<td>85</td>

<td>100</td>

</tr>

<tr>

<td>S</td>

<td>256</td>

<td>56.6</td>

<td>188</td>

<td>41.6</td>

<td>8</td>

<td>1.8</td>

<td>452</td>

<td>100</td>

</tr>

<tr>

<td>M</td>

<td>147</td>

<td>57.4</td>

<td>104</td>

<td>40.6</td>

<td>5</td>

<td>2.0</td>

<td>256</td>

<td>100</td>

</tr>

<tr>

<td>Total proportion</td>

<td>454</td>

<td>57.3</td>

<td>323</td>

<td>40.7</td>

<td>16</td>

<td>2.0</td>

<td>793</td>

<td>100</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 9: Analysis of data by gender within personality types**</caption>

<tbody>

<tr>

<th rowspan="2">Sex</th>

<th colspan="2">Type A</th>

<th colspan="2">Type S</th>

<th colspan="2">Type M</th>

<th colspan="2">Total</th>

</tr>

<tr>

<th>Freq.</th>

<th>%</th>

<th>Freq.</th>

<th>%</th>

<th>Freq.</th>

<th>%</th>

<th>Freq.</th>

<th>%</th>

</tr>

<tr>

<td>Men</td>

<td>51</td>

<td>11.2</td>

<td>256</td>

<td>56.4</td>

<td>147</td>

<td>32.4</td>

<td>454</td>

<td>100</td>

</tr>

<tr>

<td>Women</td>

<td>31</td>

<td>9.6</td>

<td>188</td>

<td>58.2</td>

<td>104</td>

<td>32.2</td>

<td>323</td>

<td>100</td>

</tr>

<tr>

<td>Total proportion</td>

<td>85</td>

<td>10.7</td>

<td>452</td>

<td>57.0</td>

<td>256</td>

<td>32.3</td>

<td>793</td>

<td>100</td>

</tr>

</tbody>

</table>

## Conclusions

Gender differences which emerged from our analyses were interpreted in broader analytical framework. Gender as a variable can help us better understand cognitive and social frameworks of human information processing. Findings can inform design of services and systems and information literacy policies. The limitations of statistical analyses are represented by the discovery of tendencies. Statistical methods have to be complemented by qualitative methods and other analyses. We are aware that gender analysis represents just one part of the complex picture of human information behaviour. Additional personal and social factors, e.g., age, education, job, technological skills, foreign languages, situations, tasks, etc. are part of complex modelling of information behaviour. We have noted slight differences in gender manifestations and found it interesting to share them in the light of similar studies. Research on gender differences still remains open. We are aware that sufficient empirical evidence is required. Our paper presented a potential framework for further research. For example, methods of observations, interviews and experiments could be beneficial.

The results of our study form a part of a larger picture, because we have concentrated mainly on library users in Slovakia, especially students and university teachers. The statistical differences are not high and not often supported by statistically significant measures. However, what has been confirmed statistically, is higher preference of the Internet as the first source with men. Men also put more stress on non-paid electronic resources as opposed to women's more frequent use of licensed resources (paid by the institution). With men, especially free and fast access to electronic resources was appreciated. Men tended to regard electronic and traditional publishing as equal and were more concerned with editorial reviewing and archiving of electronic resources. It has also been statistically proved that men valued economic aspects of electronic publishing more highly, as well as lower production costs.

Our analyses, however, cannot be limited to statistical testing. This is due to the complex nature of the questionnaire we used and further analyses and modelling. Biological, mental and social differences between men and women can be manifested in different ways of information seeking and use. It may be caused by different information needs and strategies of access to information resources. Gender differences can be traced in different orientation in resources and in concepts. In our study women preferred more cooperation, men preferred Internet and individual work. Different voices in communication and gender-sensitive feelings may control the process of information seeking.

For further studies it would be interesting to find out if gender differences of information behaviour influence ways of organization of knowledge. We can suppose that coming from male behaviour the preferred structure of information would impose hierarchy, logic and linearity. Female information behaviour puts stress on communication and values of contexts. Situational and non-linear information structures could be closer to women's information seeking. Personal identification, metaphoric and emotional contexts could be more female-specific features of information seeking and structuring.

Our paper analysed prior research and linked it with empirical study of library users. The idea behind this work was driven by our curiosity to find out not only what the statistics can reveal, but also if we can develop a framework for further modelling gender manifestations of information behaviour. We found that gender differences represent a complex issue which is difficult to encompass in one paper. Interplay of gender, personality, information seeking and electronic environment may be one of frameworks for understanding human information behaviour in action.

## Acknowledgements

The research has been supported by the project VEGA 1/2481/05.

We thank the Editor and anonymous referees for their comments and advice in revising the manuscript.

## Appendix

An English version of the questionnaire used [can be found here](paper320app.html) .

## References

*   <a id="ag01"></a>Agosto, D.E. (2001). [Propelling young women into Cyber Age: gender considerations in the evaluation of Web-based information.](http://www.webcitation.org/5NnmSZmff) _School Library Media Research_ **4**. Retrieved 10 April, 2007 from http://www.ala.org/ala/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume42001/agosto.htm.
*   <a id="an01"></a>Anderson, K. et al. (2001). [Publishing online-only peer-reviewed biomedical literature: three years of citation, author perception, and usage experience.](http://www.webcitation.org/5NnmX8roo) _Journal of Electronic Publishing_, **6**(3). Retrieved 10 April, 2007 from http://www.press.umich.edu/jep/06-03/anderson.html
*   <a id="ba89"></a>Bates, M.J. (1989). [The design of browsing and berrypicking techniques for the online search interface.](http://www.gseis.ucla.edu/faculty/bates/berrypicking.html) _Online Review_, **13**(5), 407-424\. Retrieved 10 April, 2007 from http://www.gseis.ucla.edu/faculty/bates/berrypicking.html.
*   <a id="bj00"></a>Bjork, B. & Turk, Z. (2000). _[A survey of the impact of the Internet on scientific publishing in construction IT and construction management.](http://www.itcon.org/2000/5/paper.htm)_ Stockholm: Royal Institute of Technology, 2000\. Retrieved 10 April, 2007 from http://www.itcon.org/2000/5/paper.htm.
*   <a id="bu95"></a>Butler, H.J. (1995). Where does scholarly electronic publishing get you? _Journal of Scholarly Publishing_, **26**(4), 234-246.
*   <a id="cha00"></a>Chatman, E.A. (2000). Framing social life in theory and research. _The New Review of Information Behaviour Research._ Vol.1, 2000, 3-17.
*   <a id="de03"></a>Dervin, B. (2003). [Human studies and user studies: a call for methodological inter-disciplinarity.](http://www.webcitation.org/5JVzfVksB) _Information Research_, **9**(1), paper 166\. Retrieved 10 April, 2007 from http://InformationR.net/ir/9-1/paper166.html.
*   <a id="el90"></a>Ellis, D. (1990). _New horizons in information retrieval._ London: Library Association Publishing..
*   <a id="en05"></a>Enochsson, A. (2005). [A gender perspective on Internet use: consequences for information seeking.](http://informationr.net/ir/10-4/paper237.html) _Information Research,_ **10**(4), paper 237\. Retrieved 10 April, 2007 from http://informationr.net/ir/10-4/paper237.html.
*   <a id="fa05"></a>Fallows, D. (2005). [_How women and men use the Internet._](http://www.webcitation.org/5KoU9pik4) Washington, DC: Pew Internet & American Life Project, December 28, 2005\. Retrieved 10 April, 2007 from http://www.pewInternet.org/pdfs/PIP_Women_and_Men_online.pdf.
*   <a id="fi04"></a>Fisher, K.E., Durrance, J.C. & Hinton, M.B. (2004). Information grounds and the use of need-based services by immigrants in Queens, New York: a context-based, outcome evaluation approach. _Journal of the American Society for Information Science and Technology,_ **55**(8), 754-766.
*   <a id="th05"></a>Fisher, K.E., Erdelez, S. & McKechnie, L. (Eds.) (2005). _Theories of information behavior._ Medford, NJ: Information Today. 2005.
*   <a id="fo00"></a>Ford, N., Wilson, T., Foster, A., Ellis, D., Spink, A. (2000). Individual differences in information seeking: an empirical study. In _ASIS 2000._ Proceedings of the 63rd ASIS Annual Meeting. 2000, vol. 37\. Medford, NJ: Information Today, 2000, 14-24.
*   <a id="fo05"></a>Foster, A. (2005). [A non-linear model of information seeking behaviour.](http://informationr.net/ir/10-2/paper222.html) _Information Research,_ **10**(2), paper 222\. Retrieved 10 April, 2007 from http://informationr.net/ir/10-2/paper222.html.
*   <a id="gi05"></a>Given, L. (2005). Social positioning. In Fisher, K.E., Erdelez, S. & McKechnie, L. (Eds.), _Theories of information behavior_ (pp. 334-338). Medford, NJ: Information Today.
*   <a id="he00"></a>Heinström, J. (2000). [The impact of personality and approaches to learning on information behaviour.](http://InformationR.net/ir/5-3/paper78.html) _Information Research_, **5**(4), paper 78\. Retrieved 10 April, 2007 from http://InformationR.net/ir/5-3/paper78.html.
*   <a id="he03"></a>Heinström, J. (2003). [Five personality dimensions and their influence on information behaviour.](http://www.webcitation.org/5KLUg1rqR) _Information Research_, **9**(1), paper 165\. Retrieved 10 April, 2007 from http://InformationR.net/ir/9-1/paper165.html.
*   <a id="hi01"></a>Higgins, S.E. & Hawamdeh, S. (2001). Gender and cultural aspects of information seeking and use. _New Review of Information Behaviour Research,_ **2**, 17-28.
*   <a id="ja04"></a>Järvelin, K. & Ingwersen, P. (2004) [Information seeking research needs extension towards tasks and technology.](http://InformationR.net/ir/10-1/paper212.html) _Information Research_, **10**(1), paper 212\. Retrieved 10 April, 2007 from http://InformationR.net/ir/10-1/paper212.html.
*   <a id="ju05"></a>Julien, H. (2005). Women's ways of knowing. In Fisher, K.E., Erdelez, S. & McKechnie, L. (Eds.), _Theories of information behavior_ (pp. 387-391). Medford, NJ: Information Today.
*   <a id="ka01"></a>Kalnická, Z. & Geller, J.L. (2001). Mysel a rod. In: E. Visnovský, M. Popper & J. Plichtová, (Eds.), _Príbehy o hladaní mysle_ (pp. 184-200). Bratislava, Slovakia: Veda SAV.
*   <a id="ke03"></a>Kennedy, T., Wellman, B. & Klement, K. (2003). [Gendering the digital divide.](http://www.webcitation.org/5O1V8NzWV) _IT and Society_, **1**(5), 72-96\. Retrieved 10 April, 2007 from http://www.stanford.edu/group/siqss/itandsociety/v01i05/v01i05a05.pdf.
*   <a id="ku93"></a>Kuhltau, C.C. (1993). _Seeking meaning: a process approach to library and information services._ Norwood, NJ: Ablex.
*   <a id="lo03"></a>Losh, S.C. (2003). [Gender and educational digital gaps: 1983 - 2000.](http://www.webcitation.org/5O1VAHbyL) _IT and Society_, **1**(5), 56-71\. Retrieved 10 April, 2007 from http://www.stanford.edu/group/siqss/itandsociety/v01i05/v01i05a04.pdf.
*   <a id="mc97"></a>McCann, L. (1997). [_Academic use of electronic publications in social sciences and humanities and changing roles for libraries._](http://www.webcitation.org/5O1VCMKgQ) Nashville, TN: Association of College and Research Libraries. . Retrieved 10 April, 2007 from http://www.ala.org/ala/acrlbucket/nashville1997pap/mccann.htm.
*   <a id="nah01"></a>Nahl, D. (2001). [A conceptual framework for explaining information behavior.](http://www.webcitation.org/5O1VKnPsv) _SIMILE: Studies in Media & Information Literacy Education_, **1**(2). Retrieved 10 April, 2007 from http://www.utpjournals.com/simile/issue2/nahl1.html.
*   <a id="nak98"></a>Nakonecny, M. (1998). _Psychologie osobnosti._ 2\. vyd. Prague, Czech Republic: Academia.
*   <a id="re03"></a>Renzetti, C.R. & Curran, D.J. (2003). _Zeny, muzi a spolecnost._ Prague, Czech Republic: Karolinum.
*   <a id="ro00"></a>Rockwell, G. & Siemens, L. (2000). [_The credibility of electronic publishing: report on responses to the questionnaire_.](http://www.webcitation.org/5O1VTf55s) Nanaimo, BC, Canada: Malaspina University. Retrieved 10 April, 2007 from http://web.mala.bc.ca/hssfc/Final/QuestionnaireR.htm.
*   <a id="ru04"></a>Ruisel, I. (2004). _Inteligencia a myslenie._ Bratislava, Slovakia: Ikar.
*   <a id="ne05"></a>Spink, A. & Cole, Ch. (Eds.). (2005). _New directions in human information behavior._ Dordrecht, the Netherlands: Springer.
*   <a id="st03"></a>Steinerová, J. (2003). In search for patterns of user interaction for digital libraries. In T. Koch & I.T. Solvberg (Eds.), _Research and Advanced Technology for Digital Libraries. 7th European Conference, ECDL 2003\. Proceedings_ (pp. 13-23). Berlin: Springer Verlag.
*   <a id="st04"></a>Steinerová, J., Šušol, J., Makulová, S., Matthaeidesová, M., Verceková, J., Rankov, P. (2004). [_Správa o empirickom prieskume pouzívatelov knizníc ako súcast grantovej úlohy VEGA 1/9236/02 Interakcia cloveka s informacným prostredím v informacnej spolocnosti._](http://www.webcitation.org/5O1VYHLsG) [Report on empirical survey of library users as part of grant project VEGA 1/9236/02 Interaction of man and information environment in information society]. Bratislava, Slovakia: Filozofická fakulta UK, KKIV. Retrieved 10 April, 2007 from http://www.old-kkiv.fphil.uniba.sk/vyskum2004/obsah.htm.
*   <a id="st05"></a>Steinerová, J. & Šušol, J. (2005). Library users in human information behaviour. _Online Information Review,_ **29**(2), 139-156.
*   <a id="su90"></a>Sullivan, M. V., Borgman, C. L. & Wippern, D. (1990). End-users, mediated searches, and front-end assistance programs on Dialog: A comparison of learning, performance, and satisfaction. _Journal of the American Society for Information Science,_ **41**(1), 27-42.
*   <a id="su05"></a>Šušol, J. (2005). [Elektronické informacné zdroje - vztah pouzívatelských a autorských preferencií.](http://www.webcitation.org/5O1VdmSzx) [Electronic information resources - correlation between user and author preferences]. _Ikaros,_ **9**(9). Retrieved 10 April, 2007 from http://www.ikaros.cz/node/2004.
*   <a id="wi99"></a>Wilson, T.D. (1999). [Models in information behaviour research.](http://www.webcitation.org/5IYMXQGwQ) _Journal of Documentation,_ **55**(3). Retrieved 10 April, 2007 from http://informationr.net/tdw/publ/papers/1999JDoc.html.