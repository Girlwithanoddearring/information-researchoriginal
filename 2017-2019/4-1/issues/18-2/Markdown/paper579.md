<header>

#### vol. 18 no. 2, June, 2013

</header>

<article>

# Improving users' credibility perception of travel expert blogs: useful lessons from television travel shows

#### [Wee-Kheng Tan and Yu-Chung Chang](#author)  
Department of Information & Electronic Commerce, Kainan University, Taoyuan County, Taiwan

<section>

#### Abstract

> **Introduction**. This paper used the Decision Making Trial and Evaluation Laboratory method to display how consumers assess the credibility of such travel expert blogs and television travel shows.  
> **Method**. Data were collected through consumer surveys of readers of travel expert blogs, viewers of television travel shows or both. Respondents could choose either of the information sources or both information sources to complete the questionnaire.  
> **Analysis**. Non-recursive models were constructed to show how information consumers assess the credibility of information sources by source, message and media credibility dimensions. The outcomes were analysed and compared to suggest how the credibility of travel expert blogs could be improved.  
> **Results**. Credibility dimensions and their underlying measurements may exhibit complex and inter-related relationships. The relationship differs for different types of tourism information sources. The analysis method showed that a credibility dimension can boost other dimension(s) or underlying measurements. Credibility is affected by the celebrity factor and the degree of establishment of the medium.  
> **Conclusions**. This paper confirmed the relationship between credibility dimensions and measurements. By comparing travel expert blogs with television travel shows, suggestions were made on how travel experts can improve the credibility of their travel blogs.

## Introduction

Information is the lifeblood of the tourism industry ([Sigala 2011](#s05)). Credible tourism information bolsters the economic success of tourism and hospitality providers. It also satisfies the consumers' need to reduce information asymmetry ([Zehrer _et al._ 2011](#z01)), lower uncertainty ([McIntosh and Goeldner 1990](#m05)) and improve the quality of their tourism and hospitality purchases.

Tourism-related information is already a very popular content area on the Internet ([Carson 2008](#c01)). Online participatory culture has led to the democratisation of publications ([Dondio _et al._ 2006](#d01)) and the rapid growth of both user-generated content and online tourism information sources ([Sigala 2009](#s04)). Blogs are a product of this trend. Blogs are frequently edited Webpages which record individual articles arranged in chronological order ([Herring _et al._ 2005](#h01)). Travel blogs contain tourists' descriptions of their travel experiences and assessments of their destinations ([Pan _et al._ 2007](#p01)). They are useful in both the anticipatory and reflective phases of the tourist experience ([Tussyadiah and Fesenmaier 2009](#t02)). Travel expert blogs are a subset of travel blogs. Travel expert blogs are set up and written by experienced individuals who travel frequently and have the interest and flair for writing: these are known as travel experts. Some examples of Chinese travel expert blogs are [Travel and Living](http://www.wretch.cc/blog/amarylliss), [Trip Writer](http://trip.writers.idv.tw/) and [Jerrynote](http://www.wretch.cc/blog/jerrynote).

Tourism information consumers are concerned about the credibility of Internet content ([Kiousis 2001](#k04)) as they get more dependent on online information sources. Online content can be easily created or altered by anonymous authors and it is not protected by the traditional forms of _gatekeepers_ ([Johnson and Kaye 2000](#j03); [Kaye and Johnson 2011](#k01); [Metzger _et al._ 2003](#m06); [Rains and Karmikel 2009](#r02)). Markers, which are available in traditional media and can serve as cues to the credibility of a Website, are not readily available ([Rains and Karmikel 2009](#r02)). Some researchers have argued that blogs are less credible because any individual can post content which is not subjected to editorial scrutiny ([Mack _et al._ 2008](#m01)). While these observations could be generally applied to travel blogs, the less credibility element is mitigated by travel blogs being viewed as more credible than traditional marketing communications ([Akehurst 2009](#a01)), and fake comments and reviews in travel blogs are easily identified ([Schmollgruber 2007](#s02)). The travel experts are not so anonymous and they are more likely to be subjected to certain ethical codes of conduct and wider public scrutiny than general blog writers.

Travel expert blogs are a relatively new type of tourism information source. More traditional sources can serve as useful guides to help writers improve the credibility of their blogs. Given that credibility is a perceptual variable ([Flanagin and Metzger 2007](#f02)) and a receiver-based construct ([Gunther 1992](#g04)), this paper seeks to understand how readers or viewers of tourism information sources assess credibility. This paper compares travel expert blogs with television travel shows, a traditional tourism information source. As evidenced by Chinese travel planning ([Sparks and Pan 2009](#s07)), television programmes are a primary source of travel information in both Eastern and Western cultures. Serving as a covert, induced and autonomous agent ([Gartner 1993](#g01)), television is an important source of destination image formation ([Govers _et al._ 2007](#g03); [Sparks and Pan 2009](#s07)) while providing the benefits of entertainment and relaxation ([Anderson and Hanson 2010](#a02)) . It is also an important contributor of film-induced tourism ([Connell 2005](#c09)). This paper aspires to use the television travel show as a useful reference to provide suggestions for writers of travel expert blogs to improve the credibility of their blogs.

This study is also in line with recent research on how perceptions of credibility are formed ([Kaye and Johnson 2011](#k01)). It is also in tune with the recent trend where "scholars have recently begun exploring genre credibility, the process of assessing credibility of different Internet components" ([Kaye and Johnson 2011: 237](#k01)). There is still plenty of room to understand travel blogs as '_with only a few studies to date, research on travel blogs has still not reached the stage of maturity in that researchers are still trying to make out the "what" and "how" of analysing travel blogs_' ([Banyai and Glover 2012](#b01): 275). This study adds to the literature on travel blogs, though restricted to the area of credibility assessment of travel expert blogs.

## Literature review

Tourism information satisfies the needs of tourism information consumers in several ways. Information meant for functional purposes ([Vogt and Fesenmaier 1998](#v01)) can lead to easier, faster and more reliable purchase decisions ([Sigala 2009](#s04)). Tourism information users narrow down choices among competing destinations through information searches ([Sirakaya and Woodside 2005](#s06)). Information consumers use the information to build an image of a destination which contains the sum of knowledge, beliefs, ideas, prejudice and impressions a person has of a destination ([Crompton 1979](#c11); [Tasci and Gartner 2007](#t01)). Tourists are likely to choose a destination with a positive destination image ([Ritchie and Crouch 2003](#r04)). The degree of familiarity with a destination also affects the destination image and leads to higher visiting intention ([Chen and Lin 2012](#c05)). Tourism information users may also obtain information to create feelings, experiences and emotions ([Hyde 2009](#h06)).

Tourism information users often use more than one source of tourism information. Personal experience, traditional word-of-mouth and brochures are highly rated as information sources ([Fodness and Murray 1999](#f04)). Traditional word-of-mouth is influential in destination image formation and decision-making ([Beerli and Martin 2004](#b02)). Television is also a dominant information source ([Sparks and Pan 2009](#s07)). Travel agents are a major information source for complex itineraries ([Castillo-Manzano and Lopez-Valpuesta 2010](#c02)). The Internet offers online sources for tourists to seek out tourist destinations, research options, conduct trip planning and find the best price or value ([Pan, MaClaurin and Crotts 2007](#p01)). Travel blogs can affect consumer decisions and the way destinations are promoted ([Banyai and Glover 2012](#b01)). Travel blogs influence readers' perceptions of the destination. Wang ([2012](#w01)) found that generating empathy and experiencing appeal (as building affective images variables), providing guides (as a building cognitive images variable), and social influence and cybercommunity influence (as facilitating interpersonal interactions variables) influence travel blog readers' perceptions of travel destinations.

Information users are concerned with the credibility of tourism information sources and their content. Credibility is an important aspect of information quality ([Hilligoss and Rieh 2008](#h02)). According to the information adoption model, information quality and source credibility are essential determinants of information usefulness which in turn is related to information adoption ([Sussman and Siegal 2003](#s10)). Credibility is useful in evaluating experience attributes ([Jain and Posavac 2001](#j01)) and reduces performance uncertainty ([Weathers _et al._ 2007](#w02)). It is also a determinant of strong persuasion and generates attitude change ([Park and Lee 2009](#p02); [Xie _et al._ 2011](#x01)). A travel blog can influence the destination image of those readers who trust its content ([Yen _et al._ 2011](#y01)). Whether or not an information source is considered to be credible can determine its long-term success ([Kaye and Johnson 2011](#k01)) since there are many types of tourism information sources vying for the attention of users.

Despite being widely discussed, there is no single and agreed-upon definition of credibility ([Flanagin and Metzger 2008](#f03)). Being a receiver-based judgement ([Kaye and Johnson 2011](#k01)), credibility is a complex concept with '_dozens of other concepts and combinations_' ([Self 1996: 421](#s03)). Credibility is evaluated in many different ways ([Fogg _et al._ 2001](#f06)). The focus of credibility study is somewhat field-specific. Information science tends to focus on message credibility while social psychology and communication focus on source credibility ([Flanagin and Metzger 2008](#f03)). Conceptually, credibility is traditionally classified as source, message and media credibility ([Eysenbach 2008](#e02)). Many studies were also researched along these three major dimensions ([Chung _et al._ 2010](#c06)). These dimensions are also used in studies on new media to measure online information credibility ([Chung _et al._ 2010](#c06)).

Source credibility seems to be the most widely studied dimension. It refers to the characteristics of persuasive sources ([Metzger _et al._ 2003](#m06)). Source credibility is a major contributor to the effectiveness of advertisements ([Clow _et al._ 2006](#c08)). As mentioned by Metzger _et al._ ([2003](#m06): 299), different researchers found different dimensions of source credibility and '_credibility dimensions may differ depending upon the type of source being evaluated and the context in which the evaluation occurs_'. Hovland and colleagues ([Hovland and Weiss 1951](#h04); [Hovland _et al._ 1953](#h03)) provided a source credibility model containing competence (expertness) and trustworthiness dimensions. McCroskey ([1966](#m03)) used character, competence, sociability, extroversion and composure to measure source credibility. Leathers ([1992](#l01)) used competence, trustworthiness and dynamism. Celebrity endorsement is often a topic in source credibility studies. Schlecht ([2003](#s01): 3) defined celebrities as those '_people who enjoy public recognition by a large share of a certain group of people_'. As celebrity endorsers, the celebrities use their public recognition to endorse and transfer their values to the products ([Byrne _et al._ 2003](#b05); [McCracken 1989](#m02)). Ohanian ([1990](#o01)) used expertise, trustworthiness and attractiveness to measure source credibility that is related to celebrity endorsers.

Message credibility refers to the issue of message or information characteristics. Its two major underlying measurements are message structure, language and delivery, and message content ([Flanagin and Metzger 2008](#f03)). Information digitisation has further increased message complexity which is now '_a multimedia combination of texts, photos, sounds, movies and even animated infographics_' ([Opgenhaffen 2008: 495](#o02)). Hyperlinked structures are also common on the Internet. Cues on credibility of Websites now lie mainly within the interface ([Clewley _et al._ 2009](#c07)). Structural characteristics such as carefully edited text ([Opgenhaffen 2008](#o02)) and typographical errors ([Fogg _et al._ 2003](#f07)) can influence perception of credibility. Other structural characteristics include degree of aesthetic treatment of websites ([Robins and Holmes 2008](#r05)). Issues related to content may include use of evidence and citations, accuracy, comprehensiveness and currency ([Flanagin and Metzger 2008](#f03)).

Media credibility is the perception of believability of the media channels through which a message is sent. It deals with whether a medium is known to be consistently trustworthy, accurate, complete and unbiased. The field of journalism has found that the more people rely on a medium for news, the more credible they believe that medium to be ([Flanagin and Metzger 2008](#f03)). Gaziano and McGrath ([1986](#g02)) used accuracy, believability, depth (completeness) and lack of bias as measurements for evaluating media credibility. Metzger, Flanagin and Zwarun ([2003](#m07)) mentioned believability, accuracy, trustworthiness, bias, and completeness of information as measurements of media credibility. Johnson and Kaye([1998](#j02)) pointed to believability, accuracy, bias, and depth or completeness as a robust measurement structure of media credibility.

However, "while conceptually tidy, Chaffee ([1982](#c03)) argued that various dimensions of credibility overlap, and that many information consumers do not distinguish, for example, between the source of a message and the channel through which they receive the message" ([Flanagin and Metzger 2008: 9](#f03)). Convergence of media (especially online media) makes credibility assessment more complex. A study by Kim ([2010](#k03)) on Yahoo! Answers in the U.S. has suggested that "the credibility of a social questions and answers site as a medium is transferred to individual answers in that site". Past studies often do not make clear distinctions between the three credibility dimensions (source, message and media credibility) or focus on one or two of them selectively. Hence, Flanagin and Metzger ([2008](#f03): 9) remarked that '_source, message, and media credibility are overlapping concepts in many instances, and research designs that do not always enable clear distinctions among these components complicate our current understanding of online credibility_'.

Thus, credibility assessment may be more complicated than envisaged. Intertwining of credibility dimensions and measurements is possible. Though the source, message and media credibility classification is widely used, it is not the only framework to understand credibility assessment. Flanagin and Metzger ([2007](#f02)) used message (which dealt with information quality and language intensity), site (which dealt with site features and degree of interactivity), and sponsor credibility (which dealt with the reputation of, or personal experience with, the website's sponsor) to measure credibility in the Web environment. Considering the element of group and social engagement of information sources, Flanagin and Metzger ([2008](#f03)) introduced the idea of conferred, tabulated, reputed and emergent credibility. Conferred credibility is a result of entities leveraging their expertise to approve a resource, such as '_libraries and teachers confer credibility on the information databases to students_' (p. 10). Tabulated credibility relies on the mechanism of peer rating, such as the rating system on eBay. Reputed credibility is one where information consumers depend on reputational cues obtained from their social networks. Emergent credibility is evident in information repositories which are the collective effort of self-coordinating individuals (such as Wikipedia).

Credibility assessment may be processed in terms of cognitive units '_rather than on administrative categories imposed upon messages by the media industry_' ([Sundar 1999: 373](#s08)). Some information users, such as students, may not have the required skills to critically evaluate a potential source ([Currie _et al._ 2010](#c12)). Hilligoss and Rieh ([2008](#h02)) suggested three interlinked levels of credibility judgements: construct (pertaining to how a person constructs, conceptualises or defines credibility), heuristics (general rules of thumb that make judgements of credibility applicable to different situations), and interaction (credibility judgements based on content and cues). Credibility can also be assessed in the form of a multi-item checklist, such as the Stanford Guidelines for Web Credibility ([Fogg 2002](#f05)).

Using cognitive heuristics as mental shortcuts to evaluate credibility is common. Cognitive heuristics are '_information-processing strategies consisting of useful mental short cuts, rules-of thumb, or guidelines that reduce cognitive load during information processing and decision making_' ([Metzger _et al._ 2010: 417](#m08)). Metzger _et al._ ([2010](#m08)) mentioned five cognitive heuristics that are often used to simplify the process of Website credibility assessment. Reputation heuristic relies on the reputation of Websites and may be a subset of the _authority_ heuristic in credibility assessment. Endorsement heuristic (conferred credibility) refers to a belief in Website credibility simply because others feel that the Website is credible. This heuristic is likely to be based in part on _consensus heuristic_ ([Chaiken 1987](#c04)) and _bandwagon heuristic_ ([Sundar 2008](#s09)). Consistency heuristic is the confirmation of consistency of information by checking other relevant websites. Expectancy violation heuristic is invoked if the website does not meet the expectation of users in terms of appearance, layout or content. Lastly, persuasive intent heuristic refers to the negative feeling about the credibility of a Website arising from commercial content. Other heuristics include the sex of the blog author (male authors are considered to be more credible than female authors ([Armstrong and McAdams 2009](#a03)).

The demographic characteristics of blog users also affect their perception of the credibility of different types of blogs ([Johnson and Kaye 2010](#j05); [Johnson and Kaye 2011](#j06)). Information that is provided by a news organization Website is likely to be viewed as more credible than a Website of a special interest group ([Flanagin and Metzger 2007](#f02)). Blog reliance is a strong predictor of credibility ([Johnson and Kaye 2004](#j04); [Johnson and Kaye 2011](#j06)). Based on Petty and Cacioppo's elaboration likelihood model and Chaiken and Eagly's heuristic-systematic model of information processing, Metzger _et al._ ([2010](#m08)) suggested that highly motivated online information consumers would more likely perform rigorous information evaluation and be more sensitive to information quality cues. However, less motivated information seekers would instead depend on _peripheral_ or _heuristic_ credibility cues. Emphasis on convenience may reduce critical evaluation of the credibility of information sources ([Biddix _et al._ 2011](#b03)).

Credibility is allied closely with the concept of trust ([Flanagin and Metzger 2008](#f03)). Being a multi-faceted concept ([Corritore _et al._ 2003](#c10); [Rempel _et al._ 1985](#r03)), trust as a concept is complex and confusing for researchers ([McKnight _et al._ 2002](#m04)). There are many definitions of trust ([Dondio _et al._ 2006](#d01)). Trust is a psychological state and integrates '_microlevel psychological processes and group dynamics with macrolevel institutional arrangements_' ([Rousseau _et al._ 1998](#r06): 393). It '_can be imagined as the mental process of leaping - enabled by suspension (the mechanism that brackets out uncertainty and ignorance) - across the gorge of the unknowable from the land of interpretation into the land of expectation_' ([Mollering 2001](#m09): 412). Trust is related to an individual's confidence that something will or will not occur in a predictable or promised manner. Trust and risk are inseparable concepts in decision-making ([Morrison and Firmstone 2000](#m10)). Trust involves the element of risk and serves to reduce risk ([Kelton _et al._ 2008](#k02); [Lewis and Weigert 1985](#l02); [Luhmann 1988](#l03)). Trust seems to resemble the trustworthiness dimension of credibility ([Flanagin and Metzger 2008](#f03)) and acts as a mediator between information quality and information usage ([Kelton _et al._ 2008](#k02)). Trust derived from knowing that others are recommending a website can serve as an endorsement heuristic for credibility evaluation ([Metzger _et al._ 2010](#m08)).

## Research issue and method

Television travel shows are traditional, highly popular and well established tourism information sources. The objective of this paper is to use television travel shows as a reference to provide useful suggestions to writers of travel expert blogs to improve the credibility of their blogs, a relatively new though increasingly popular tourism information source. While tourism information consumers generally appreciate the importance of credibility, it is less obvious as to how they evaluate the credibility of this type information sources. To achieve the objective, this study has to first find out how information consumers assess the credibility of these two tourism information sources (television travel shows and travel expert blogs) and compare them. Once the dynamics of credibility assessment have been understood, this study can then proceed to provide suggestions on how travel experts can improve the credibility of their blogs.

This paper adopts the traditional classification of credibility (source, message and media credibility dimensions) as it is widely accepted. Since the presenters of television travel shows often enjoy and court public recognition, this paper uses Ohanian's ([1990](#o01)) expertise, trustworthiness and attractiveness as the underlying measurements of source credibility. Message structure and message content ([Flanagin and Metzger 2008](#f03)) are used as the underlying measurements of message credibility. Accuracy, non-bias and fairness, completeness and trust are used as the underlying measurement of media credibility ([Gaziano and McGrath 1986](#g02)). Unlike many studies which focus on one or two credibility dimensions, this study examines them simultaneously. This study further adopts the position of Chaffee ([1982](#c03)) and recognises that the three credibility dimensions may be overlapping concepts in many instances. Thus, this study abandons the recursive model which is adopted in many credibility-related studies. The recursive model may be restrictive and cannot provide for the potential existence of feedback loops.

This paper employs the Decision Making Trial and Evaluation Laboratory (DEMATEL) method ([Fontela and Gabus 1976](#f08)) to accommodate for the reciprocal causal relationships between credibility dimensions and measurements. Belonging to the family of multiple criteria decision making research methods, the method is often used to research and analyse complex and intertwined problems. The method categorises the factors as (1) the cause factors, those that have more effect on other factors than being affected them; and (2) the effect factors, those that receive more influence from other factors than affecting other factors. Based on graph theory, the method uses digraphs (rather than directionless graphs) and converts relationships between the causes and effects of these factors into an intelligible cause-effect structural model ([Hung _et al._ 2006](#h05); [Tzeng _et al._ 2007](#t03)). The end product of the analysis is a visual presentation in the form of influence maps. The _modus operandi_ of the method can be summarised in the following steps:

Step 1: Suppose there are _n_ factors under consideration and there are _H_ respondents. Each respondent is asked to state the degree a factor _i_ affects factor _j_ through a score ranging from 0 to 4, with 0 indicating _no influence_ and 4 indicating _very high influence_. _H_ answer matrices are obtained and each answer matrix _X_ is a matrix with element _k_ where 1 = _k_ = H. The initial direct relation matrix _A_ is obtained as below:

<figure>

![Equation 1](../p579Equation1.jpg)</figure>

Step 2: The normalised initial direct-relation matrix _D_ is obtained by normalising matrix _A_: _D_ = _A_/_s_ where:

<figure>

![Equation 2](../p579Equation2.jpg)</figure>

Step 3: The next step is to obtain the total relation matrix _T_. Total relation matrix _T_ is an _n_ x _n_ matrix and is defined as in equation (3) as _m_ approaches infinity and matrix _I_ is an _n_ x _n_ identity matrix.

<figure>

![Equation 3](../p579Equation3.jpg)</figure>

Step 4: The sum of rows _r_ and the sum of columns _c_ of the total relation matrix _T_ is obtained as in equation (4). The sum _r_ + _c_ of factor _i_ gives an index that represents the total effects both given and received by the factor _i_. The difference _r_ - _c_ of factor _i_ shows the net effect factor _i_ contributes to the problem. If the difference _r_ - _c_ of factor _i_ is positive, factor _i_ is a net causer (cause factor). When _r_ - _c_ of factor _i_ is negative, factor _i_ is a net receiver (effect factor). Finally, a threshold value is set to draw the influence map.

<figure>

![Equation 4](../p579Equation4.jpg)</figure>

## Analysis and discussions

### Methods

A literature review was conducted to obtain a concise description of the credibility dimensions and measurements. The wordings were then modified to suit the nature of the two tourism information sources and translated to Chinese since the survey participants were Taiwanese.

The Decision Making Trial and Evaluation Laboratory method was used to find out the dynamic of credibility assessment; confirm or disconfirm the interdependence among credibility dimensions and measurements; and if the interrelationship exists, to construct visual maps of the directed inter-relationship. This component of the survey provides the four entry matrices for each tourism information source. The first 4 x 4 matrix with twelve entries (excluding the diagonal elements) is for the overall, source, message and media credibility. Another 4 x 4 matrix with twelve entries (excluding the diagonal elements) is for the source credibility and its three underlying measurements (trustworthiness, expertise and attractiveness). The third matrix is a 3 x 3 matrix with six entries (excluding the diagonal elements) and is for the message credibility and its two underlying measurements (message structure and message content). The last matrix is a 5 x 5 matrix with twenty entries and is for the media credibility and its four underlying measurements (trust, accuracy, non-bias and fairness, and completeness). Hence, there are fifty entries per tourism information source. In accordance with the _modus operandi_ of the method, respondents were asked to state the degree a factor (credibility dimension or measurement) _i_ affects factor _j_ through a score ranging from 0 - no influence to 4 - very high influence for every entry. Demographic data was also obtained.

The sample was recruited through the convenience sampling method, subjected to the conditions that the respondents must be twenty years old or above and be general tourism information consumers (viewers of television travel shows, readers of travel expert blogs or both). Examples of travel expert blogs and television travel shows were provided so that all shared the same understanding of the two information sources. Respondents were then asked to recall and answer the questionnaire based on the travel expert blogs and/or television travel shows which they have recently accessed or viewed. Respondents could choose either of the information sources or both information sources to complete the entry matrices.

This paper differs from many studies using this method, where survey respondents are often experts in their fields. This paper is concerned with how _non-expert_ tourism information consumers assess the credibility of tourism information sources. This study therefore required a much bigger sample size than the handful of experts needed in many studies using this method, which differs from more familiar and conventional statistical five-point or seven-point Likert-type scale survey questions. Initial briefing on the matrix was conducted to introduce respondents to the different style of questioning so as to improve the accuracy of the survey. An online survey was thus not suitable for this study. A face-to face paper-based survey was adopted.

One hundred and forty-eight valid returns were received for television travel shows. About 59% of the respondents were male, 67% had received university education, and close to 82% were in the age group of 25 year old and above with the biggest sub-group (50%) being in the age group of 25-30\. One hundred and nineteen valid returns were received for travel expert blogs. About 53% of the respondents were male, 65% had received university education, and close to 71% were in the age group of 25 year old and above with the biggest sub-group (38%) in the age group of 25-30.

### Source, message, media and overall credibility

The influence maps of television travel shows (Figure 1a) and travel expert blogs (Figure 1b) show that source, message and media credibility of both types of information sources contribute to the perception of overall credibility. The overall credibility of the two sources are net receiver factor (with _R_ - _C_ <0) and it has the most negative _R_ - _C_ value. This is consistent with many recursive credibility models where source, message and media credibility contribute to the information consumers' perception of overall credibility (i.e., overall credibility is at the receiving end of all other contributing credibility dimensions).

The source credibility of the two information sources contributes to their respective overall credibility. The result is also in line with many studies where the credibility of presenters of television travel shows (with television corporations acting as the gatekeepers and providing the credibility backing) and travel experts boost the overall credibility of their respective sources.

This study has further revealed that the overall credibility of travel expert blogs can in turn contribute to source credibility (hence the bi-directional line between source and overall credibility in Figure 1b). However, the bi-directional relationship between source and overall credibility is absent in television travel shows. The reason is information consumers are often familiar with television corporations and travel show presenters. Television corporations and travel show presenters enjoy public recognition, are often well-established and have also gained credibility not only by producing or hosting travel shows but also through producing or engaging in other activities. Hence, tourism information consumers consider their credibility, to certain extent, as given. It is not necessarily the case for travel experts. Since travel experts are individuals whose activities often revolve primarily around their blogs, reputed credibility from other activities is not present or strong. In addition, information consumers may be in the process of assessing and making judgement on these travel experts. Having good overall travel blog credibility (which is a much more multi-faceted component than source credibility) can thus add to the credibility of travel experts (source credibility). In other words, credibility flow (credibility transfer) exists.

<figure>

![Figure 1: Influence maps showing overall credibility and its dimensions](../p579fig1.jpg)

<figcaption>Figure 1: Influence maps showing overall credibility and its dimensions</figcaption>

</figure>

Message credibility deals with the issue of credibility at the level of blog article. It is concerned with message structure and judgement of content. For television travel shows, message structure refers to how the programme is presented, such as whether it is narrative, fun or serious. For expert travel blogs, message structure refers to how the text and pictures are presented, whether they are well organized, easy to search and understand. Judgement of content refers to whether the content is correct, complete and up-to-date. As has been shown in Figure 1, and similar to many previous credibility studies, message credibility contributes to the overall credibility of television travel shows and travel expert blogs. Interestingly, the non-recursive models show that overall credibility of these two tourism information sources also influence message credibility. If the viewers of television travel shows or readers of travel expert blogs are pleased with the overall credibility of information sources, such positive feeling can cause a flow of credibility to the message credibility (credibility transfer). A positive view among information consumers is generated on the message structure and content of the television show episode and blog article.

Media credibility deals with whether the medium is known to be consistently trustworthy, accurate, complete and unbiased. Building media credibility involves the collective effort of many parties and is painstakingly accumulated. Hence, it is not the property of a single television corporation, television show presenter or travel expert. The influence maps show that, regardless of the type of information source, media credibility contributes to overall credibility. However, overall credibility influences the media credibility of television travel shows but not travel expert blogs. Overall credibility of television travel shows, which is accumulated over time, adds to the accumulated credibility of the television medium. Like the television medium, it is also reasonable to expect that the overall credibility of travel blogs also adds to the credibility of the travel expert blog medium. However, it is not the case. Information consumers view the travel blog medium as a new medium and they are uncertain about its credibility. The credibility of such a medium still needs to be further ascertained.

The non-recursive model has shown that source and message credibility influence one another in television travel shows and travel expert blogs. Furthermore, source credibility influences the media credibility of television travel shows but is absent in travel expert blogs. It is known that engaging the help of celebrity endorsers is effective for companies ([Elberse and Verleun 2012](#e01)). Celebrities who are well-known or established in other domains can also be a plus point for the television programmes which they hosted or the blogs which they authored. Known as the celebrity factor, it can be used to explain the above observations. Television travel show presenters and travel experts (to a lesser extent) are often celebrities. Hence, information consumers who believe in the credibility of the presenters and travel experts (source credibility) will also view content spoken, written and presented by them (message credibility) more favourably. Similarly, a good episode of a travel show or written blog article (message credibility) can enhance the credibility of presenters and travel experts. However, the celebrity factor of television travel show presenters is more prevalent and stronger than the travel experts since the former often engages in other activities that can further boost their celebrity status. When it comes to the issue of presenters and travel experts influencing media credibility, only the celebrity factor of television show presenters is strong enough to influence the television medium.

### Source credibility and its dimensions

The influence maps reveal that while the source credibility of television travel shows (Figure 2a) contributes to expertise and attractiveness, such a dynamic is absent in travel expert blogs (Figure 2b). Since television corporations and presenters have many activities that enhance their credibility, credibility transfer can take place and boost the expertise of television travel shows. However, travel experts do not have this advantage as their activities are much more limited.

The expertise of television travel shows influence attractiveness but not trustworthiness. On the other hand, the expertise of travel expert blogs influences trustworthiness instead of attractiveness. This is an indication of the higher entertainment nature of television travel shows and the word-of-mouth element of travel expert blogs where trustworthiness is an important feature. Another explanation is, unlike television travel show presenters who enjoy a higher profile, travel experts are often less well-known.

<figure>

![Figure 2: Influence maps showing source credibility and its dimensions](../p579fig2.jpg)

<figcaption>Figure 2: Influence maps showing source credibility and its dimensions</figcaption>

</figure>

### Message credibility and its dimensions

Message structure and content contribute to the message credibility of television travel shows (Figure 3a). However, only message content adds to the message credibility of travel expert blogs (Figure 3b).

Recognising that travel experts are individuals, information consumers are more prepared to accept the more free-and-easy style of writing where articles are structured as personal diaries. On the other hand, information consumers view television corporations as gatekeepers and expect them to set the rules on content presentation. However, it is too extreme to say that message structure is not important for travel expert blogs since it does have an impact on message content and vice versa.

<figure>

![Figure 3: Influence maps showing message credibility and its dimensions](../p579fig3.jpg)

<figcaption>Figure 3: Influence maps showing message credibility and its dimensions</figcaption>

</figure>

### Media credibility and its dimensions

Trust and accuracy measurements (unlike completeness and non-bias and fairness) contribute to the media credibility of television travel shows (Figure 4a). The non-recursive models further reveal that these types of relationships are mutually reinforcing. As mentioned above, media credibility is not the sole property of a television corporation or television presenter. It is a collective perception which is accumulated through the action of many players at the medium level. Hence, media credibility can possibly cascade down to the individual measurements. Non-bias and fairness is not relevant here because information consumers already have some pre-conceived idea that television travel shows serve certain commercial purposes.

The media credibility of travel expert blogs offers a more complex picture (Figure 4b). Only trust and accuracy measurements contribute to media credibility. Similar to television travel shows, they are mutually reinforcing. An accurate medium also contributes to a trusted medium. Non-bias and fairness play a role in travel expert blogs because the opinion of travel experts is often viewed as an electronic form of word-of-mouth. Surprisingly, media credibility contributes to non-bias and fairness and completeness of travel expert blogs. The reason is the medium associated with such travel blogs is still new and needs the collective effort of many travel expert blogs to establish the reputation of completeness, non-bias and fairness.

<figure>

![Figure 4: Influence maps showing media credibility and its dimensions](../p579fig4.jpg)

<figcaption>Figure 4: Influence maps showing media credibility and its dimensions</figcaption>

</figure>

### Discussion

This study has provided several interesting theoretical implications for the study of credibility assessment. It is interesting to note that credibility dimensions and their underlying measures may exhibit complex and inter-related relationships under certain circumstances. This result thus confirms the remark by Chaffee ([1982](#c03)) that various dimensions of credibility overlap. Furthermore, the measures of the credibility dimension can also interact with one another. Hence, the non-recursive model adopted in this study is suitable for providing new insights on how information consumers evaluate the credibility of tourism information sources. The Decision Making Trial and Evaluation Laboratory method is also a suitable tool to obtain a pictorial non-recursive model that displays the interactions. The analysis, using this tool, has shown that tourism information users tend to relate credibility dimensions and measures together during their credibility assessment. Such a process can possibly simplify the mental process of credibility assessment and could be used to explain the dynamics of heuristics ([Hilligoss and Rieh 2008](#h02);[Metzger _et al._ 2010](#m08)) in credibility assessment. This paper proposes more research should be made in this direction

There is no single answer to how the credibility of tourism information sources is assessed. There is also no single answer to how the various credibility dimensions and measures are inter-related. The answer depends on the types of information source being considered. It thus confirms the comment by Flanagin and Metzger ([2000](#f01)) that credibility of online sources may be linked to the type of sources visited and the relations between credibility dimensions are dependent on the types of the information sources. This argument can also be extended to non-online sources. This result also supports the need to explore genre credibility ([Kaye and Johnson 2011](#k01)).

Credibility transfer exists and some of the findings can be explained through this mechanism. As an example, credibility transfer from overall credibility to message credibility occurs in both types of tourism information sources. Information consumers may extend the general perception of a television travel show or related travel blog as credible (overall credibility) to a specific episode of the travel show or an article in the related travel expert blog. This study has thus provided the evidence for the presence of some of the labels used to describe credibility, such as conferred credibility, reputed credibility ([Flanagin and Metzger 2008](#f03)) and reputation heuristic ([Metzger _et al._ 2010](#m08)) that rely on the transfer of credibility.

The degree of establishment of source and medium can affect the interaction between credibility dimensions and also the interaction between their related underlying measurements. In this paper, television travel shows are more established than travel expert blogs. There is the presence of _reputed credibility_ where credibility is transferred from television corporations and presenters. Travel blogs set up by travel experts are less established and the different credibility dimensions may still be in the state of a work-in-progress. If the source credibility originates from a less established source with its credibility still in the process of evaluation (such as the travel experts), overall credibility may flow and contribute to the source credibility.

The celebrity factor exists: a well known source (celebrity), i.e., one with high source credibility, can affect message credibility. It also impacts on media credibility but the celebrity element has to be much stronger in this case to be successful. A less established medium (such as the medium of travel expert blogs) can help to build the perception of medium completeness and non-bias gradually.

This study provides several suggestions on how travel experts can improve the credibility of their own travel blogs. Unlike television travel shows, travel expert blogs have less reputed credibility, possess weaker celebrity factor, have relatively few gatekeepers, and is a less established medium. All these factors affect their credibility and must be addressed.

Travel experts could rely on the credibility transfer mechanism to add credit to their travel blogs. A carefully cultivated and enhanced overall credibility of travel expert blogs can boost the source and message credibility of the blogs.

Travel experts should also build and make use of celebrity factor so that their travel blogs appear to be more credible. Similar to television travel show presenters who participate in many other activities, travel experts should appear and participate in more non-travel blog public events. They should court attention and gain familiarity among users and potential users so as to generate the required reputed credibility. Being more exposed to the public can also speed up the process of information users assessing and making judgements on travel experts. As with television travel shows, higher source credibility can add to the perceived expertise of the travel blogs. In turn, the expertise of travel experts can enhance the attractiveness of these experts. By boosting their level of celebrity, travel experts can hopefully be strong enough to influence the travel blog medium.

Media credibility depends on the collective effort of many parties. While people view television as being established and traditional, they also view the travel expert blog as being less established and new. Travel experts, while competing with one another, should also act collectively to promote the blog medium so that more users or potential users are aware and willing to accept the medium as being established. In the process, the credibility of such a medium can be enhanced.

While information consumers are more willing to accept the relatively personal style of writing with articles being structured as personal diary, travel experts should consider putting some obvious elements of gatekeeper mechanisms in place to contribute to higher message credibility.

Users often have the pre-conceived idea that television travel shows are set up to serve certain commercial purposes. Hence, completeness as well as non-bias and fairness are not relevant. However, users are looking for completeness as well as non-bias and fairness in travel expert blogs. This is a confirmation that many people view travel expert blogs as a form of word-of-mouth. However, the influence map has also shown that the credibility of the travel expert blog medium contributes to non-bias and fairness and completeness of travel expert blogs. The possible reason is the medium associated with travel expert blogs is still relatively new and needs the collective effort of many travel experts and blogs to establish the reputation of completeness, non-bias and fairness. Hence, to distinguish themselves and to perform better than television travel shows in these two aspects, travel experts as a whole should practice self-discipline to project such an image instead of looking for short-term financial benefits.

### Limitations and future research directions

A few caveats are necessary: since this study used convenience sampling, generalisation of results should be made with caution. The respondents of this study were of a higher age group who might be less familiar with blogs. Hence, a comparative analysis with younger respondents could be conducted. Travel expert blogs are still viewed as "young" by the respondents of this study. Analysis has shown that celebrity factor and degree of establishment of the medium influence credibility assessment. As perceived credibility may change with experience and as respondents get more familiar with travel expert blogs, a longitudinal study can be conducted to confirm this point. Another limitation of the study is the sample size. The questionnaire used differs from many conventional statistical surveys. Thus this study had to put in a lot of effort to encourage participation and conduct briefings to introduce survey respondents to the different style of questioning. Despite the sample limitation, the findings still provide useful results and ideas on how information consumers assess credibility and the complexity involved. Future research should use larger samples to improve upon this study.

## Acknowledgements

The authors wish to thank Prof. T. Wilson, Prof. N. Pharo and all the anonymous reviewers for their kind suggestions and comments for improving this particular research. The authors will also like to thank Ms J. Elliot, the Editorial Associate, for editing the paper.

## About the authors

**Wee-Kheng Tan** is an assistant professor of the Department of Information & Electronic Commerce at Kainan University, Taiwan. He received his Ph.D. from National University of Singapore and his research interests include e-tourism and information search behavior. He can be contacted at: [tanwk@mail.knu.edu.tw](mailto:tanwk@mail.knu.edu.tw)  
**Yu-Chung Chang** is a Master's student of Department of Information & Electronic Commerce at Kainan University, Taiwan

</section>

<section>

## References

<ul>
<li id="a01">Akehurst, G. (2009). User generated content: the use of blogs for tourism organizations and tourism consumers. <em>Service Business</em>, <strong>3</strong>(1), 51-61
</li>
<li id="a02">Anderson, D.R. &amp; Hanson, K.G. (2010). From blooming, buzzing confusion to media literacy: the early development of television viewing. <em>Developmental Review</em>, <strong>30</strong>(1), 239-255
</li>
<li id="a03">Armstrong, C.L. &amp; McAdams, M.J. (2009). Blogs of information: how gender cues and individual motivations influence perceptions of credibility. <em>Journal of Computer-Mediated Communication</em>, <strong>14</strong>(3), 435-456
</li>
<li id="a04">Azariah, D.R. (2012). When travel meets tourism: tracing discourse in Tony Wheeler's blog. <em>Critical Studies in Media Communication</em>, <strong>29</strong>(4), 275-291
</li>
<li id="b01">Banyai, M. &amp; Glover, T.D. (2012). Evaluating research methods on travel blogs. <em>Journal of Travel Research</em>, <strong>51</strong>(3), 267-277
</li>
<li id="b02">Beerli, A. &amp; Martin, J.D. (2004). Factors influencing destination image. <em>Annals of Tourism Research</em>, <strong>31</strong>(3), 657-681
</li>
<li id="b03">Biddix, J.P., Chung, C.J. &amp; Park, H.W. (2011). Convenience or credibility? A study of college student online research behaviors. <em>Internet and Higher Education</em>, <strong>14</strong>(3), 175-182
</li>
<li id="b04">Bosangit, C., McCabe, S. &amp; Hibbert, S. (2009). What is told in travel blogs? Exploring travel blogs for consumer narrative analysis. In W. Hopken, U. Gretzel &amp; R. Law (Eds.), <em>Information and communication technologies in tourism 2009</em> (pp. 61-72). Vienna: Springer-Verlag
</li>
<li id="b05">Byrne, A., Whitehead, M. &amp; Breen, S. (2003). The naked truth of celebrity endorsement. <em>British Food Journal</em>, <strong>105</strong>(4/5), 288-296
</li>
<li id="c01">Carson, D. (2008). The 'blogosphere' as a market research tool for tourism destinations: a case study of Australia's Northern Territory. <em>Journal of Vacation Marketing</em>, <strong>14</strong>(2), 111-119
</li>
<li id="c02">Castillo-Manzano, J.I. &amp; Lopez-Valpuesta, L. (2010). The decline of the traditional travel agent model. <em>Transportation Research Part E</em>, <strong>46</strong>(5), 639-649
</li>
<li id="c03">Chaffee, S.H. (1982). Mass media and interpersonal channels: competitive, convergent, or complementary? In G. Gumpert &amp; R. Cathcart (Eds.), <em>Inter/media: interpersonal communication in a media world</em> (pp. 55-77). New York, NY: Oxford University Press
</li>
<li id="c04">Chaiken, S. (1987). The heuristic model of persuasion. In M.P. Zanna, J.M. Olsen &amp; C.P. Herman (Eds.), <em>Social influence: the Ontario symposium Vol. 5</em> (pp. 3-39). Hillsdale, NJ: Lawrence Erlbaum Associates
</li>
<li id="c05">Chen, C.C. &amp; Lin, Y.H. (2012). Segmenting mainland Chinese tourists to Taiwan by destination familiarity: a factor-cluster approach. <em>International Journal of Tourism Research</em>, <strong>14</strong>(4), 339-352
</li>
<li id="c06">Chung, C.J., Kim, H. &amp; Kim, J.H. (2010). An anatomy of the credibility of online newspapers. <em>Online Information Review</em>, <strong>34</strong>(5), 669-685
</li>
<li id="c07">Clewley, N., Chen, S.Y. &amp; Liu, X. (2009). Evaluation of the credibility of Internet shopping in the UK. <em>Online Information Review</em>, <strong>33</strong>(4), 805-826
</li>
<li id="c08">Clow, K.E., James, K.E., Kranenburg, K.E. &amp; Berry, C.T. (2006). The relationship of the visual element of an advertisement to service quality expectations and source credibility. <em>Journal of Services Marketing</em>, <strong>20</strong>(6), 404-411
</li>
<li id="c09">Connell, J. (2005). Toddlers, tourism and Tobermory: destination marketing issues and television-induced tourism. <em>Tourism Management</em>, <strong>26</strong>(5), 763-776
</li>
<li id="c10">Corritore, C.L., Kracher, B. &amp; Wiedenbeck, S. (2003). On-line trust: concepts, evolving themes, a model. <em>International Journal of Human-Computer Studies</em>, <strong>58</strong>(6), 737-758
</li>
<li id="c11">Crompton, J.L. (1979). An Assessment of the image of Mexico as a vacation destination and the influence of geographical location upon the image. <em>Journal of Travel Research</em>, <strong>18</strong>(4), 18-23
</li>
<li id="c12">Currie, L., Devlin, F., Emde, J. &amp; Graves, K.(2010). Undergraduate search strategies and evaluation criteria: searching for credible sources. <em>New Library World</em>, <strong>111</strong>(3/4), 113-124
</li>
<li id="d01">Dondio, P., Barrett, S., Weber, S. &amp; Seigneur, J.M. (2006). Extracting trust from domain analysis: a case study on The Wikipedia Project. In L.T. Yang (Ed.), <em>Autonomic and trusted computing</em> (pp. 362-373). Berlin &amp; Heidelberg: Springer Verlag.
</li>
<li id="e01">Elberse, A. &amp; Verleun, J. (2012). The economic value of celebrity endorsements. <em>Journal of Advertising Research</em>, <strong>52</strong>(2), 149-165
</li>
<li id="e02">Eysenbach, G. (2008). Credibility of health information and digital media: new perspectives and implications for youth. In M. Metzger &amp; A. Flanagin (Eds.), <em>Digital media, youth, and credibility</em> (pp. 123-154). Cambridge, MA: The MIT Press
</li>
<li id="f01">Flanagin, A.J. &amp; Metzger, M.J. (2000). Perceptions of Internet information credibility. <em>Journalism &amp; Mass Communication Quarterly</em>, <strong>77</strong>(3), 515-540
</li>
<li id="f02">Flanagin, A.J. &amp; Metzger, M.J. (2007). The role of site features, user attributes, and information verification behaviors on the perceived credibility of web-based information. <em>New Media &amp; Society</em>, <strong>9</strong>(2), 319-342
</li>
<li id="f03">Flanagin, A.J. &amp; Metzger, M.J. (2008). Digital media and youth: unparalleled opportunity and unprecedented responsibility. In M. Metzger &amp; A. Flanigan (Eds.), <em>Digital media, youth, and credibility</em> (pp. 5-28). Cambridge, MA: The MIT Press
</li>
<li id="f04">Fodness, D. &amp; Murray, B. (1999). A model of tourist information search behaviour. <em>Journal of Travel Research</em>, <strong>37</strong>(3), 220-230
</li>
<li id="f05">Fogg, B.J. (2002). <em>Stanford guidelines for web credibility, a research summary.</em> Stanford, CA: Stanford Persuasive Technology Lab, Stanford University
</li>
<li id="f06">Fogg, B.J., Marshall, J., Laraki, O., Osipovich, A., Varma, C., Fang, N., Paul, J., Rangnekar, A., Shon, J., Swani, P. &amp; Treinen, M. (2001). What makes web sites credible? A report on a large quantitative study. In <em>Proceedings of ACM CHI 2001 Conference on Human Factors in Computing Systems</em> (pp. 61-68). New York, NY: ACM Press.
</li>
<li id="f07">Fogg, B.J., Soohoo, C., Danielson, D.R., Marable, L., Stanford, J. &amp; Trauber, E.R. (2003). How do users evaluate the credibility of web sites? A study with over 2,500 participants. In <em>Proceedings of the 2003 conference on Designing for User Experiences</em>, (pp. 1-15). New York, NY: ACM Press.
</li>
<li id="f08">Fontela, E. &amp; Gabus, A. (1976). <em>The DEMATEL observer, DEMATEL 1976 report.</em> Geneva: Battelle Geneva Research Center
</li>
<li id="g01">Gartner, W.C. (1993). Image formation process. <em>Journal of Travel and Tourism Marketing</em>, <strong>2</strong>(2-3), 191-216
</li>
<li id="g02">Gaziano, C. &amp; McGrath, K. (1986). Measuring the concept of credibility. <em>Journalism Quarterly</em>, <strong>63</strong>(3), 451-462
</li>
<li id="g03">Govers, R., Go, F.M. &amp; Kumar, K. (2007). Promoting tourism destination image. <em>Journal of Travel Research</em>, <strong>46</strong>(1), 15-23
</li>
<li id="g04">Gunther, A.C. (1992). Biased press or biased public? Attitudes toward media coverage of social groups. <em>Public Opinion Quarterly</em>, <strong>56</strong>(2), 147-167
</li>
<li id="h01">Herring, S.C., Scheidt, L.A., Wright, E. &amp; Bonus, S. (2005). Weblogs as a bridging genre. <em>Information Technology &amp; People</em>, <strong>18</strong>(2), 124-171
</li>
<li id="h02">Hilligoss, B. &amp; Rieh, S. (2008). Developing a unifying framework of credibility assessment: construct, heuristics, and interaction in context. <em>Information Processing &amp; Management</em>, <strong>44</strong>(4), 1467-1484
</li>
<li id="h03">Hovland, C.I., Irving, L.J. &amp; Harold, H.K. (1953). <em>Communication and persuasion: psychological studies of opinion change</em>. New Haven, CT: Yale University Press
</li>
<li id="h04">Hovland, C.I. &amp; Weiss, W. (1951). The influence of source credibility on communication effectiveness. <em>Public Opinion Quarterly</em>, <strong>15</strong>(1), 635-650
</li>
<li id="h05">Hung, Y.H., Chou, S.C.T. &amp; Tzeng, G.H. (2006). Using a fuzzy group decision approach-knowledge management adoption. In <em>Proceedings of the Association of Pacific Rim Universities, Distance Learning and Internet Conference, University of Tokyo, Japan, 8�10 November, 2006</em>, (pp. 225-228). Tokyo: University of Tokyo.
</li>
<li id="h06">Hyde, K.F. (2009). Tourist information search. In M. Kozak &amp; A. Decrop (Eds.), <em>Handbook of tourist behavior - theory and practice</em> (pp. 50-66). New York, NY: Routledge
</li>
<li id="j01">Jain, S. &amp; Posavac, S. (2001). Prepurchase attribute verifiability, source credibility and persuasion. <em>Journal of Consumer Psychology</em>, <strong>11</strong>(3), 169-180
</li>
<li id="j02">Johnson, T.J. &amp; Kaye, B.K. (1998). Cruising is believing? Comparing Internet and traditional sources on media credibility measures. <em>Journalism and Mass Communication Quarterly</em>, <strong>75</strong>(2), 325-340
</li>
<li id="j03">Johnson, T.J. &amp; Kaye, B.K. (2000). Using is believing: the influence of reliance on the credibility of online political information among politically interested Internet users. <em>Journalism and Mass Communication Quarterly</em>, <strong>77</strong>(4), 865-879
</li>
<li id="j04">Johnson, T.J. &amp; Kaye, B.K. (2004). Wag the blog: how reliance on traditional media and the internet influence credibility perceptions of weblogs among blog users. <em>Journalism &amp; Mass Communication Quarterly</em>, <strong>81</strong>(3), 622-642
</li>
<li id="j05">Johnson, T.J. &amp; Kaye, B.K. (2010). Believing the blog of war: How blog users compare on credibility and characteristics in 2003 and 2007. <em>Media, War and Conflict</em>, <strong>3</strong>(3), 315-333
</li>
<li id="j06">Johnson, T.J. &amp; Kaye, B.K. (2011). Can you teach a new blog old tricks? How blog users judge credibility of different types of blogs for information about the Iraq War. In B.K. Curtis (Ed.), <em>Psychology of trust</em>, (pp. 1-25). New York, NY: NOVA Publishers
</li>
<li id="k01">Kaye, B.K. &amp; Johnson, T.J. (2011). Hot diggity blog: a cluster analysis examining motivations and other actors for why people judge different types of blogs as credible. <em>Mass Communication and Society</em>, <strong>14</strong>(2), 236-263
</li>
<li id="k02">Kelton, K., Fleischmann, K.R. &amp; Wallace, W.A. (2008). Trust in digital information. <em>Journal of the American Society for Information Science and Technology</em>, <strong>59</strong>(3), 363-374
</li>
<li id="k03">Kim, S. (2010). <a href="http://www.webcitation.org/6H26Ceixg">Questioners' credibility judgments of answers in a social question and answer site.</a> <em>Information Research</em>, <strong>15</strong>(2) paper 432. Retrieved 10 January 2013 from http://InformationR.net/ir/15-2/paper432.html (Archived by WebCite� at http://www.webcitation.org/6H26Ceixg)
</li>
<li id="k04">Kiousis, S. (2001). Public trust or mistrust? Perceptions of media credibility in the information age. <em>Mass Communication and Society</em>, <strong>4</strong>(4), 381-403
</li>
<li id="l01">Leathers, D.G. (1992). <em>Successful nonverbal communication: principles and applications</em>. New York, NY: Macmillian
</li>
<li id="l02">Lewis, J.D. &amp; Weigert, A. (1985). Trust as a social reality. <em>Social Forces</em>, <strong>63</strong>(4), 967-985
</li>
<li id="l03">Luhmann, N. (1988). Familiarity, confidence, trust: Problems and alternatives. In D. Gambetta (Ed.), <em>Trust: Making and breaking cooperative relations</em> (pp. 94-108). New York, NY: Basil Blackwell
</li>
<li id="m01">Mack, R.W., Blose, J. &amp; Pan, B. (2008). Believe it or not: credibility of blogs in tourism. <em>Journal of Vacation Marketing</em>, <strong>14</strong>(2), 133-144
</li>
<li id="m02">McCracken, G (1989). Who is the celebrity endorser? Cultural foundations of the endorsement process. <em>Journal of Consumer Research</em>, <strong>16</strong>(3), 310-322
</li>
<li id="m03">McCroskey, J.C. (1966). Scales for the measurement of ethos. <em>Speech Monographs</em>, <strong>33</strong>(1), 65-72
</li>
<li id="m04">McKnight, D.H., Choudhury, V. &amp; Kacmar, C. (2002). Developing and validating trust measures for e-commerce: an integrative typology. <em>Information Systems Research</em>, <strong>13</strong>(3), 334-359
</li>
<li id="m05">McIntosh, R.W. &amp; Goeldner, C.R. (1990). <em>Tourism: principles, practices, philosophies</em>. New York, NY: Wiley
</li>
<li id="m06">Metzger, M.J., Flanagin, A.J., Eyal, K., Lemus, D. &amp; McCann, R. (2003). Can you teach a new blog old tricks? How blog users judge credibility of different types of blogs for information about the Iraq War. In B.K. Curtis (Ed.), <em>Communication Yearbook Vol. 27</em> (pp. 293-335). Mahwah, NJ: Lawrence Erlbaum
</li>
<li id="m08">Metzger, M.J., Flanagin, A.J. &amp; Medders, R.B. (2010). Social and heuristic approaches to credibility evaluation online. <em>Journal of Communication</em>, <strong>60</strong>(3), 413-439
</li>
<li id="m07">Metzger, M.J., Flanagin, A.J. &amp; Zwarun, L. (2003). College student Web use, perceptions of information credibility, and verification behavior. <em>Computers &amp; Education</em>, <strong>41</strong>(4), 271-290
</li>
<li id="m09">Mollering, G. (2001). The nature of trust: From Georg Simmel to a theory of expectation, interpretation and suspension. <em>Sociology</em>, <strong>35</strong>(2), 403-420
</li>
<li id="m10">Morrison D.E. &amp; Firmstone, J. (2000). The social functional of trust and implications for e-commerce. <em>International Journal of Advertising</em>, <strong>19</strong>(5), 599-623
</li>
<li id="o01">Ohanian, R. (1990). Construction and validation of a scale to measure celebrity endorsers' perceived expertise, trustworthiness, and attractiveness. <em>Journal of Advertising</em>, <strong>19</strong>(3), 39-52
</li>
<li id="o02">Opgenhaffen, M. (2008). Divergent news media in computer mediated news communication. In S. Kelsey &amp; K.S. Amant (Eds.), <em>Handbook of research on computer mediated communication</em> (pp. 492-507). Hershey, PA: Information Science Reference
</li>
<li id="p01">Pan, B., MaClaurin, T. &amp; Crotts, J.C. (2007). Travel blogs and the implications for destination marketing. <em>Journal of Travel Research</em>, <strong>46</strong>(1), 35-45
</li>
<li id="p02">Park, C. &amp; Lee, T.M. (2009). Information direction, website reputation and eWOM effect: a moderating role of product type. <em>Journal of Business Research</em>, <strong>62</strong>(2), 61-67
</li>
<li id="r01">Rak, J. (2005). The digital queer: weblogs and Internet identity. <em>Biography</em>, <strong>28</strong>(1), 166-182
</li>
<li id="r02">Rains, S.A. &amp; Karmikel, C.D. (2009). Health information-seeking and perceptions of website credibility: examining web-use orientation, message characteristics, and structural features of websites. <em>Computers in Human Behavior</em>, <strong>25</strong>(2), 544-553
</li>
<li id="r03">Rempel, J.K., Holmes, J.G. &amp; Zanna, M.P. (1985). Trust in close relationships. <em>Journal of Personality and Social Psychology</em>, <strong>49</strong>(1), 95-112
</li>
<li id="r04">Ritchie, J.R. &amp; Crouch, G.I. (2003). <em>The competitive destination: a sustainable tourism perspective</em>. Wallingford, UK: CABI Publishing
</li>
<li id="r05">Robins, D. &amp; Holmes, J. (2008). Aesthetics and credibility in web site design. <em>Information Processing and Management</em>, <strong>44</strong>(1), 386-399
</li>
<li id="r06">Rousseau, D.M., Sitkin, S.B., Burt, R.S. &amp; Camerer, C.F. (1998). Not so different after all: a cross-discipline view of trust. <em>Academy of Management Review</em>, <strong>23</strong>(3), 393-404
</li>
<li id="s01">Schlecht, C (2003). <em><a href="http://www.webcitation.org/6H3Cj7ipk">Celebrities' impact on branding</a></em>. New York, NY: Columbia Business School. Retrieved 10 January 2013 from http://worldlywriter.com/images/portfolio/Proposals/Celebrity_Branding.pdf (Archived by WebCite� at http://www.webcitation.org/6H3Cj7ipk)
</li>
<li id="s02">Schmollgruber, K. (2007, April 23). <em>Fake travel reviews are no issue - interview with Ian Rumgay from Tripadvisor.</em> [Web log post] Retrieved 10 January 2013 from http://passionpr.typepad.com/tourism/2007/04/fake_travel_rev.html
</li>
<li id="s03">Self, C.C. (1996). Credibility. In M.B. Salwen &amp; D.W. Stacks (Eds.), <em>An integrated approach to communication theory and research</em> (pp. 435-456). Mahwah, NJ: Lawrence Erlbaum
</li>
<li id="s04">Sigala, M. (2009). E-service quality and Web 2.0: expanding quality models to include customer participation and intercustomer support. <em>The Service Industries Journal</em>, <strong>29</strong>(10), 1341-1358
</li>
<li id="s05">Sigala, M. (2011). Preface - special issue on Web 2.0 in travel and tourism: empowering and changing the role of travelers. <em>Computers in Human Behavior</em>, <strong>27</strong>(2), 607-608
</li>
<li id="s06">Sirakaya, E. &amp; Woodside, A.G. (2005). Building and testing theories of decision making by travelers. <em>Tourism Management</em>, <strong>26</strong>(6), 815-832
</li>
<li id="s07">Sparks, B. &amp; Pan, G.W. (2009). Chinese outbound tourists: understanding their attitudes, constraints and use of information sources. <em>Tourism Management</em>, <strong>30</strong>(4), 483-494
</li>
<li id="s08">Sundar, S.S. (1999). Exploring receivers' criteria for perception of print and online news. <em>Journalism &amp; Mass Communication Quarterly</em>, <strong>76</strong>(2), 373-386
</li>
<li id="s09">Sundar, S.S. (2008). The MAIN model: a heuristic approach to understanding technology effects on credibility. In M. Metzger &amp; A. Flanagin (Eds.), <em>Digital media, youth, and credibility</em> (pp. 73-100). Cambridge, MA: MIT Press
</li>
<li id="s10">Sussman, S. &amp; Siegal, W. (2003). Informational influence in organizations: an integrated approach to knowledge adoption. <em>Information Systems Research</em>, <strong>14</strong>(1), 47-65
</li>
<li id="t01">Tasci, A.D.A &amp; Gartner, W.C. (2007). Destination image and its functional relationships. <em>Journal of Travel Research</em>, <strong>45</strong>(4), 413-425
</li>
<li id="t02">Tussyadiah, I.P. &amp; Fesenmaier, D.R. (2009). Mediating tourist experiences: access to places via shared videos. <em>Annals of Tourism Research</em>, <strong>36</strong>(1), 24-40
</li>
<li id="t03">Tzeng, G.H., Chiang, C.H. &amp; Li, C.W. (2007). Evaluating intertwined effects in e-learning programs: a novel hybrid MCDM model based on factor analysis and DEMATEL. <em>Expert Systems with Applications</em>, <strong>32</strong>(4), 1028-1044
</li>
<li id="v01">Vogt, C.A. &amp; Fesenmaier, D.R. (1998). Expanding the functional tourism information search model: incorporating aesthetic, hedonic, innovation, and sign dimensions. <em>Annals of Tourism Research</em>, <strong>25</strong>(3), 551-578
</li>
<li id="w01">Wang, H.Y. (2012). Investigating the determinants of travel blogs influencing readers' intention to travel. <em>The Service Industries Journal</em>, <strong>32</strong>(2), 231-255
</li>
<li id="w02">Weathers, D, Sharma, S. &amp; Wood, S.L. (2007). Effects of online communication practices on consumer perceptions of performance uncertainty for search and experience goods. <em>Journal of Retailing</em>, <strong>83</strong>(4), 393-401
</li>
<li id="w03">Wenger, A. (2008). Analysis of travel bloggers' characteristics and their communication about Austria as a tourism destination. <em>Journal of Vacation Marketing</em>, <strong>14</strong>(2), 169-176
</li>
<li id="x01">Xie, H., Miao, L., Kuo, P. &amp; Lee, B. (2011). Consumers' responses to ambivalent online hotel reviews: the role of perceived source credibility and pre-decisional disposition. <em>International Journal of Hospitality Management</em>, <strong>30</strong>(1), 178-183
</li>
<li id="y01">Yen, C.C., Liu, C.Y., Su, W.S., Liang, C.H. &amp; Chang, C.M. (2011). Verification of destination image influence model for Taiwan bicycle tourism blogs for Chinese tourists. <em>African Journal of Business Management</em>, <strong>5</strong>(19), 7931-7938
</li>
<li id="z01">Zehrer, A., Crotts, J.C. &amp; Magnini, V.P. (2011). The perceived usefulness of blog postings: an extension of the expectancy disconfirmation paradigm. <em>Tourism Management</em>, <strong>32</strong>(1), 106-113
</li>
</ul>

</section>

</article>