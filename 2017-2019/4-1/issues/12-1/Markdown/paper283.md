#### Vol. 12 No. 1, October 2006

* * *

# Uncertainty in action: observing information seeking within the creative processes of scholarly research

#### [Theresa Dirndorfer Anderson](mailto:anderdorfer@gmail.com)  
Information & Knowledge Management Program, Faculty of Humanities and Social Sciences, University of Technology, Sydney (UTS)

#### Abstract

> **Introduction**. This paper discusses the role uncertainty plays in judgments of the meaning and significance of ideas and texts encountered by scholars in the context of their ongoing research activities.  
> **Method.** Two experienced scholars were observed as part of a two-year ethnographic study of their ongoing research practices. Layered transcriptions of document-by-document discussions, conversations and interviews with informants were analysed for evidence of uncertainty in informants' processes of discovery, evaluation, use and generation of information.  
> **Analysis.** Three themes are discussed illustrating the dynamic interplay between positive and negative forms of uncertainty: partial relevance, boundaries of understanding and uncertainty tolerance.  
> **Results.** The uncertainty experienced by informants takes many forms deeply embedded in the interwoven layers of information seeking and use. Positive forms of uncertainty were often, but not exclusively, associated with the informants' explorations within the wider research tasks in which they were engaged.  
> **Conclusion.** The intricacy of the relationship between what we might consider desirable as opposed to undesirable uncertainty is not easily unravelled. From the searcher's perspective, the interplay between positive and negative forms may be one way of explaining their ability to tolerate challenging encounters within their information and research processes.

## Introduction

Uncertainty is not only portrayed as a necessary feature of the human condition within information behaviour research but within the wider realm of human behaviour research. As Boholm expresses it:

> In real life situations, the boundary between certitude and uncertainty is of course seldom razor-sharp, and vagueness and ambiguity tend to be the rule rather than the exception ([Boholm 2003](#boh03): 168).

Uncertainty is often associated with risk, fear and danger. However, the perception and strategic use of uncertainty can be both a positive and negative influence on behaviour. We live uncertain lives. The ambiguities, doubts and non-determinability of this uncertain existence allow new ideas to arise. Researchers are becoming increasingly interested in examining the role these positive forms of uncertainty may play for creativity and innovation.

In information seeking research, uncertainty is associated with the dynamism of this constructive process:

> Interpreting, choosing, and creating the inconsistent, often incompatible information encountered is likely to cause profound feelings of uncertainty, confusions, anxiety, and even threat ([Kuhlthau 2004](#kuh04): 23).

As reflected in the statement above, many discussions of uncertainty in information seeking relate to the negative feelings associated with the experience of uncertainty. It is described as part of a struggle and a situation in which a person experiences anxiety and a lack of confidence. While researchers in the social sciences have also acknowledged that uncertainty can have positive effects, there is very little research detailing these positive impacts and analyzing the potential interplay between positive and negative experiences of uncertainty.

This paper discusses the role uncertainty plays in judgments of the meaning and significance of ideas and texts encountered by scholars in the context of their ongoing research activities. It explores the nature of uncertainty in scholarly research: what does uncertainty look like in this context? What forms do positive and negative perceptions of uncertainty take? How much uncertainty are people in this context prepared to tolerate? As the title suggestions, it examines uncertainty **in action** because the themes presented here are not based on interviews about uncertainty, but on a process-oriented, longitudinal study of these judgments. This paper seeks to contribute to a fuller understanding of the complexity surrounding searcher experiences of uncertainty by demonstrating how, within the creative processes of scholarly research activities, working through uncertainty becomes a mediating strategy for knowledge generation.

## Conceptual background: framing uncertainty in research

Uncertainty and variability characterize real world existence. In the field of social anthropology we can find discussions about the possibility of an _anthropology of uncertainty_ (e.g., [Boholm 2003](#boh03); [Hadolt and Stöckl](#had04) 2004). Uncertainty is presented as a fundamental experiential realm of human existence associated with tolerance and risk-taking (e.g., [Boholm 2003](#boh03); [Kuhlthau 2004](#kuh04)). Risk entails a state of uncertainty that Boholm contends can relate both to the possibility of a negative outcome as well as to the nature of the outcome itself:

> ...one can ask how people identify, understand and manage uncertainty in terms of knowledge of consequences and probabilities of events ([Boholm 2003](#boh03): 166).

We can also ask how much uncertainty an individual is prepared to tolerate in any given situation. Later sections of this paper will return to this question in the context of scholarly research activities.

### The uncertainty concept in information science

Uncertainty takes on many different meanings in information science research. In information retrieval, for example, it is often associated with probability theory, fuzzy logic and the reduction of _noise_ in the transmission of information. Information seeking research draws attention to the uncertainty in the experiences of the searcher, with discussion of the anxiety that can be associated with it and strategies for assisting people in this situation (see further examples in [Wilson, _et al._ 2002](#wil02)).

#### _Desirable_ and _undesirable_ uncertainty

Given that the focus here is on perceptions of uncertainty in information seeking, it is beyond the scope of this paper to detail discussions of uncertainty in information retrieval. However, Cole's ([1993](#col93)) analysis of uncertainty as portrayed in Shannon's seminal research is worth discussing briefly.

In examining Shannon's theory of communication, Cole ([1993](#col93)) demonstrates how Shannon's _uncertainty_ takes on a 'divided or double-edged meaning' in his theory (p209). The most contradictory relationship is one which sees information both producing and reducing uncertainty. In his discussion, Cole draws on Weaver's ([1949](#wea49)) particular interpretation linking information to the uncertainty associated with the degree of freedom of choice in selecting a message. Cole also points to the two different kinds of uncertainty Weaver named _desirable uncertainty_ (associated with information) and _undesirable uncertainty_ (associated with noise and errors in a message) ([Weaver 1949](#wea49): 109 as cited by [Cole 1993](#col93): 209). He also surmises that the second (undesirable) form of uncertainty is often emphasized because Shannon's most likely use of the term uncertainty appeared to favour the reduction of uncertainty, _...because it accords with our instinctive understanding of what information does (i.e., relieve anxiety)_ ([Cole 1993](#col93): 209).

He suggests that this everyday meaning leads to a focus on the anxiety-laden undesirable form. The possibility of thinking of _desirable_ and _undesirable_ is worth further exploration and will be revisited in the context of the study described in this paper.

#### Information seeking and role played by uncertainty

Uncertainty is acknowledged as a persistent characteristic in information seeking. Wilson ([1999](#wil99): 265) calls it _'the ghost at the feast'_ that is always there. From the user's perspective, therefore, uncertainty is a natural experience within the process of information seeking and making meaning. However, it can give rise to feelings of doubt, confusion, frustration and anxiety ([Kuhlthau](#kuh04) 2004). Portraying uncertainty as a natural part of a searcher's experience represents a significant shift from the bibliographic paradigm and puts the focus on accommodating uncertainty rather than reducing it. The focus turns to the **perception** of uncertainty in any given situation, rather than any inherent certainty or uncertainty.

Kuhlthau's model of the _information search process_ portrays information seeking as a process of construction, with uncertainty decreasing as understanding increases ([1991](#kuh91); [2004](#kuh04)). This depiction incorporates the thoughts, feelings and actions common in the process of information seeking. Uncertainty is described as _...a cognitive state that commonly causes affective symptoms of anxiety and lack of confidence_ ([Kuhlthau 2004: 103](#kuh04)).

Kuhlthau presents six stages as a movement from uncertainty through to understanding.

Uncertainty features in some form in each stage of the information search process before completion (for instance, [Kuhlthau 1999a](#kuh99a): 400). Kuhlthau acknowledges that the linearity of this model is metaphoric since, from a searcher's perspective, information seeking is not experienced as a linear sequence of stages. The model does however help illustrate that uncertainty (due to a lack of understanding, a gap in meaning, or a limited construct) initiates the process of information seeking. Early stages are marked with confusion, doubt, and anxiety, but these negative feelings diminish as uncertainty decreases and confidence increases (accompanied by feelings of relief and satisfaction). Subsequent discussions depict these stages as _focus formulation_ ([Vakkari 1999](#vak99)). If a person experiences success in the process of information seeking, the uncertainty diminishes and confidence increases. However, uncertainty is also associated with a _dip_ (the sharp increase in uncertainty and decrease in confidence) during the exploration stage.

Yoon and Nilan suggest that their concept of certainty encompasses not only the user's knowledge and experience represented in this corollary, but also that user's perception of the need situation and perspectives to view the world. They offer an alternative focus on certainty as a concept for information seeking, because: _The concept of uncertainty, however, can not stand alone without the concept of certainty. Human beings can only define what they do not know in contrast to what they do know_ ([Yoon and Nilan 1999](#yoo99): 873).

They relate their definition of certainty to Kuhlthau's _redundancy_ corollary (which states that the discovery of what we already know decreases as uncertainty increases). The relationship between certainty and uncertainty will be revisited in view of findings reported later in this paper.

#### Relationship between uncertainty and relevance

The connection between the concepts of uncertainty and relevance exists in both information retrieval (e.g., [van Rijsbergen 1986](#van86)) and information seeking research (e.g., [Kuhlthau 2004: 205](#kuh04)). The evaluation of relevance is thereby linked to the measure of uncertainty. This relationship is also evident from a searcher's perspective during information seeking. Kuhlthau's redundancy corollary expands this link between uncertainty and a person's confidence in making relevance judgments.

Previous knowledge (or lack of knowledge) is indispensable in assessments of relevance. Also, judgments of partial relevance may be explained by a person's uncertainty in evaluating the relevance of an item ([Hjørland and Christensen 2002](#hjo02); [Maglaughlin and Sonnenwald 2002](#mag02)). Whether the focus is on certainty or uncertainty, what is known as well as what is not known about an idea, citation or text influences a decision to select or reject it.

### Uncertainty in the context of work

Expertise and the perceived complexity of the information seeking task have been found to be factors influencing uncertainty. Research exploring the relationship between uncertainty in information seeking and wider information tasks often frames these wider tasks as complex tasks involving problem-solving ([Attfield _et al._ 2003](#att03); [Byström and Järvelin 1995](#bys95); [Kuhlthau 1993](#kuh93); [Vakkari 1999](#vak99); [Wilson 1999](#wil99)). Without entering into a discussion about the different frameworks one can use to identify the _task_ or _problem_ (see for example, [Byström and Hansen 2005](#bys05)), it is reasonable to bound the behaviour under investigation in this paper as goal-directed and the task (problem, issue) as complex.

#### Complex tasks

In a complex task, the information required to accomplish that task cannot be determined in advance. ([Byström and Järvelin 1995](#bys95); [Vakkari 1999](#vak99)). Vakkari ([1999](#vak99)) points out that increasing knowledge about a task's information requirements, process and outcomes implies less uncertainty about the elements of the task at hand that give it structure. Hs observation is similar to Kuhlthau's _uncertainty principle for information seeking_ and the movement from uncertainty to understanding.

In relation to Kuhlthau's model, Byström and Järvelin ([1995](#bys95)) distinguish between _problem formulation_ and _problem solving_, suggesting each has its own set of information needs. Vakkari ([1999](#vak99)) calls these two stages of task performance _prefocus_ and _postfocus_,

We can thus conclude that a person involved in a complex task at work may perceive uncertainty at varying levels:

*   engaging with the information requirements associated with the task (with further distinctions possible in pre- and post-focus formulation stages);
*   during the process of information seeking (immediate search);
*   during information retrieval activities associated with a search; and
*   in terms of the task's overall outcomes (wider task goal).

However, a person engaged in a complex task is unlikely to make such fine distinctions since, from a searcher's perspective, neither information seeking nor the wider task are experienced as linear, clear-cut processes. Furthermore, while human information behaviour may involve a series of nested relationships ([Wilson 1999](#wil99)), information seeking is difficult to distinguish from the wider work task ([Byström and Hansen 2005](#bys05)). Conceptually at least, we can examine uncertainty in relation to various levels of information activity.

#### Experts in the workplace

The informants participating in this study are experienced professional academics who can be considered to be performing information intensive work tasks involving: _...inherently and/or explicitly information-related activities to a considerable degree_ ([Byström and Hansen 2005](#bys05): 1050).

In such a workplace, with layers of information-related tasks ([Byström and Hansen 2005](#bys05)), uncertainty is associated not only with information requirements and information seeking activities, but the wider task as well (e.g., [Kuhlthau 1999b](#kuh99b); [Kuhlthau and Tama 2001](#kuh01)).

Expertise can influence the perception of uncertainty ([Kuhlthau 1999b](#kuh99b); [Wilson](#wil02) _et al_. 2002). However, while the 'expert' is rarely at the true beginning of information seeking ([Kuhlthau 2004](#kuh04)), it is not uncommon for a person to be an expert in one area, but a novice in another. This observation is particularly appropriate for the study presented in this paper. While the informants can be considered experienced academic researchers, as will be shown, their research projects move them into areas extending beyond their areas of expertise.

Kuhlthau's series of studies exploring experts in the workplace demonstrated that uncertainty is not always perceived as agonizing and frustrating. The anxiety Kuhlthau reports as being present in the experience of the securities analyst ([1999b](#kuh99b)) is a contrast to the experience of the lawyers who seemed to experience less frustration and anxiety. In fact _Many of the lawyers actually related the sense of fun to more complex tasks that led to innovation and construction_ ([Kuhlthau and Tama 2001: 31](#kuh01))

While recognizing that uncertainty appears to have a positive role to play, there is little detail about the form uncertainty takes in such situations or how such tolerances evolve in the course of a work task.

#### Research as creative, innovative work

The work performed by the experts in this study is research. Exploring uncertainty within the context of the information seeking of scholarly researchers is particularly intriguing. Successful research involves moving beyond established understandings about themes under investigation. The creativity and innovation required means travelling beyond what is known - a situation bringing with it inherent uncertainty. Research involves original thought, lateral thinking, and a certain amount of serendipity ([Bawden 1986](#baw86); [Foster and Ford 2003](#fos03)). The complexity of research and information practices leads to a non-linear, dynamic process involving a tacking back and forth between deduction and induction ([Budd 2004](#bud04)). It involves balancing divergent thinking with the convergence of ideas (see [Ford 1999](#for99): 528). The processes of exploring and working with information are critical for building connections, discovery and creativity. The creativity associated with research relies on the effective provision, processing and manipulation of information at all stages of a project.

## Describing the study framework

This paper draws on two years of ethnographic field work involving two scholars engaged in the discovery, evaluation, use and generation of information and knowledge as part of their own ongoing research practices. Based upon the theoretical claims presented earlier in this paper about the relationship between judgments of relevance and uncertainty, the extensive material collected during two years of fieldwork affords a unique opportunity for examining dimensions of uncertainty in relation to judgments made over time and in context.

### Information seeking in the context of scholarly research

Kuhthau ([1999b](#kuh99b)) showed the value of exploring one individual's perceptions of uncertainty over time in the context of their work. However, as she pointed out, there is still much to learn about how workers use information to achieve their goals ([1999b: 411](#kuh99b)). This paper follows that example to present findings about the nature of uncertainty observed in a longitudinal study of two experienced academic researchers.

Examining what might be considered information-seeking activities in the context of academic work presents particular challenges because information seeking is deeply embedded in the wider task (scholarly research). Previous research has found that information seeking process is considered by many information workers as a necessary, but preliminary activity to what they perceived as more important work tasks ([Byström and Hansen 2005](#bys05); [Kuhlthau and Tama 2001](#kuh01)). While the lines between information seeking and use are never clearly drawn, in scholarly research, these distinctions appear particularly blurry. For the scholar, information seeking is deeply integrated in the wider, goal-driven task of research production and scholarly communication. Even in the supposed closing stages of a research project, it is not uncommon for new questions to be generated or new information seeking tasks to arise.

### Study design

The analysis of uncertainty is based on the unfolding of observations of and discussions with two academic researchers. Both informants (_John_ and _Catherine_) were experienced academics at, or near, the beginning of research projects involving the use of networked information systems. They were also experienced users of these systems. John is a senior lecturer involved in a research project that emerged from a conference paper and a workshop conducted a few months before the observation period began. He is observed collecting and evaluating information for his project. Catherine is a senior lecturer who, at the start of the field work, had spent three years working part-time on a doctoral thesis in her field. During the period of observation, Catherine collects book and serial literature for various chapters of her thesis. Both informants describe themselves as curious, avid explorers of ideas. Encounters with them during the course of this study confirm their self-assessments. They display enthusiasm for their topics and a willingness to remain open to insights. At the same time, however, they display a degree of pragmatism that allows them to work successfully as academics in their respective departments.

A naturalistic framework was applied, drawing on the work of Lincoln and Guba ([1985](#lin85)), Erlandson _et al._ ([1993](#erl03)) and Barry ([1994](#bar94)). When engaging with the informants in the field, two broad questions initiated discussions: What are you looking for? and, How do you know when you have found it?

Discussions with the informants and observation of their practices moved on from these questions in directions determined by their responses and the research activities they were undertaking at the time. Engaging with these interpretive processes from within the informants' worlds meant allowing them to drive the circumstances and the manner in which their practices were examined, observing what they did, and listening to their explanations of their actions.

Over a two year period, informants' information seeking and judgments of relevance were observed as part of their own ongoing research projects. Audio and video tools recorded informants' 'talk-aloud' sessions as they searched and evaluated both networked and print information resources (e.g., citations, abstracts and texts). Semi-structured interviews were conducted throughout this period to discuss their research projects and gain a better understanding of their research activities. Informants were also observed preparing documents as part of their research work, discussing their project with colleagues and delivering presentations about their work.

This study did not involve interviews about uncertainty, but rather discussions and observations with two informants in the course of their search and research activities. Layered transcriptions of document-by-document discussions, conversations and interviews with informants were reviewed for evidence of uncertainty in relation to:

*   document and citation evaluations in individual search sessions;
*   broader search and information seeking processes; and
*   the overall research project.

Previous research has demonstrated that uncertainty impacts judgments at all of these levels. However, as noted previously, the study takes a holistic stance, recognizing that informants would be experiencing these nested relationships in a systematically connected manner. Therefore, this study had no intention of making qualitative distinctions at these levels, but felt that these conceptual levels served as helpful operational markers when examining field notes and transcripts for evidence of uncertainty.

Drawing on the conceptual framework introduced earlier, analysis was based on an assumption that uncertainty is associated with an informant's inadequate knowledge about the contents and significance of documents as well as the appropriateness of ideas encountered. Statements about the partial relevance of information (ideas as well as particular texts) were also examined. As will be illustrated in samples from informants' comments, outward signs of uncertainty include expressions of doubt, ambiguity, indecision or a lack of confidence in making a judgment.

## Features of uncertainty in scholarly research

Uncertainty was detectable as part of the search processes and the wider research processes of each informant in statements like:

> 'I don't know'  
> 'I am not really confident'  
> 'I'm not sure'  
> 'It's not clear to me'

Negative forms of uncertainty corresponded with earlier descriptions associating it with frustration, confusion and anxiety. Frustration would sometimes cause informants difficulty in moving through the task at hand. Positive forms were also evident in situations where uncertainty was linked to fascination, motivation and advancement in a search or research incident. In such circumstances, uncertainty is linked more to interest and motivation than anxiety. Three themes are discussed in this section to offer a richer picture of the role played by uncertainty and to illustrate the dynamic interplay between these positive and negative forms.

### Uncertainty and partial relevance

Uncertainty permeated judgments of partial relevance, corresponding with claims made by Hjørland and Christensen ([2002](#hjo02)) that a person's uncertainty in evaluating a document's relevance is a dimension of partial relevance.

Uncertain about the extent to which an item may be appropriate for her work, Catherine explains that the ideas represented in a particular work are still worth knowing. Her initial reaction to the citation when she first encountered it a few weeks earlier suggested this might be a promising item for her research. Now, after looking at the full article, however, she describes it as not being very useful. Catherine clarifies:

> It's not exactly on that topic, but it does help me in that kind of building a picture of - you know, what's the field like. So, you know it's a good thing to have in that sense that it actually feeds into the context that we're entering. [in terms of the topic she is investigating]

The vast amount of material that she was uncovering at this time was generally described by her as being only partially related to her areas of interest, but she remained reluctant to ignore what she was finding. Regardless of how uncertain she is of the significance of the content of this item, she still finds it useful for developing a framework for her research.

In this way, uncertainty about the potential significance of the ideas potentially present in such works was shown to be the trigger for selection or attention; making informants aware of issues they needed to start picking up on in their research.

### Uncertainty observed at the margins of understanding

Analysis of informants' document selections and decisions associated with their research topics illustrates how statements of uncertainty mark the boundary of understandings about a topic. Examining this movement toward understanding and focus formulation raises two different sets of questions: What is the margin or boundary? What does uncertainty look like at such a point?

The first question involves discussion of a rich area of inquiry beyond the scope of this paper. The boundaries are both socially and individually defined, although the individual's engagement with the social plays out in human experience ([Schutz 1970](#sch70); [Zerubavel 1993](#zer93)). Under discussion here are informants' perceptions of margins or boundaries in relation to their personal understanding, a topic or field of interest or a discipline.

Observations of uncertainty at the margins of understanding can be related to the ambiguity Catherine is experiencing in a series of search sessions observed over a three week period. Documents she was very excited about locating only a week earlier no longer seemed interesting. Catherine takes me through the annotations she had made about the potential value of these items a few days earlier and explains that she is no longer certain why she chose them in the first place:

> These are a mystery to me. I cannot understand what I was looking for. I can see it vaguely but I think to myself — I'm not going to read those. [She laughs.] I don't know why I selected them!

She expresses uncertainty about the texts she is evaluating and her research framework. This discussion also shows how situated in time and space her judgments about these items are.

Significantly, despite Catherine's frustration in relation to her exploration of the literature at this point in her research, the experience proved to be useful for refining her topic. Even though she no longer wanted to pursue the articles themselves, she explains that the evaluation process contributed to her understanding of her topic. These citations, with or without abstracts or full text, helped her get a sense of the scope of material available, or unavailable, and proved important for formulating of the framework within which she would address the topic. The experience proved to be a very helpful bounding exercise for the topic she was working on at that time.

John exhibits an ability to create a boundary to a topic with statements like _clearly relevant_ or _clearly not relevant_ when discussing documents or ideas. Uncertainty related to the boundaries of areas of expertise is present in these evaluations. He also expresses uncertainty in relation to the evaluation of specific texts and to judgments about the scope of the wider search. In one incident, John is hesitant in his evaluation, and goes on to explain that it is a difficult decision:

> Again, where do you draw the line? There is a literature on memes and memetics and I'd be loathe to go beyond that unless I could see a particular relevance in an article to do that. So, I'm not going to do that with that one.

John has a sense of the margins, or boundaries, of the topic he is investigating, even if he cannot always articulate how he knows he has reached it. But, as this extract shows, he is in fact drawing a line, however imaginary. This particular item is beyond that boundary so it is no longer useful to him.

Statements by informants explaining they feel _uncertain_ or _unclear_ about the appropriateness of topics they are encountering may in fact be the outward signs of having reached the margins of the topic they are pursuing at that time. Equally, expressions like _where do you draw the line_ or _don't know if I want to go that far_ are part of this bounding. The uncertainty witnessed at these boundaries was not only associated with a lack of confidence or frustration, but with fascination as well. Experiencing uncertainty played a vital role in the engagement with information and ideas at these boundaries. Thus, it occurs at both the level of searching and engaging in the research process. It is an indication of the embedded character of these boundaries that discussions with both informants about setting boundaries in terms of document selections often became discussions about the boundaries of the topics being explored in their projects.

### Tolerating uncertainty

A tolerance of uncertainty can manifest itself in a person's willingness to stick with a situation in which they are experiencing uncertainty. In terms of information seeking, we can associate it with Kuhlthau's _mood corollary_, in that the invitational mood that is so beneficial in the exploration and formulation stages of information seeking hinge on a person's capacity for tolerating uncertainty. It can also be related to the motivation and intellectual engagement discussed in the _interest corollary_ ([Kuhlthau 1993](#kuh93)). Informants demonstrated tolerance when evaluating information through statements

*   about a document (e.g., _I'm not really sure, but am inclined to keep it_); or
*   about a research topic (e.g., _I'm not sure, it could be interesting... I'm squirming a bit at this one because I don't really want to go that far [in my research]_.

Analysis of informants' experiences raises interesting questions about how much uncertainty scholars may be prepared to tolerate in their searching and wider research activities.

Kuhlthau found that, as an expert, the securities analyst expected uncertainty when working on some of his tasks, but seemed considerably less tolerant of that uncertainty. She suggests that such intolerance could be attributable to the time pressure associated with that informant's work commitments _...that left little time for the sustained work required in major projects_ ([1999b](#kuh99b): 409). Catherine's and John's experiences appear similar to that finding in that their teaching loads and other work commitments impacted their willingness to work through some of the challenges they encountered in such situations.

Like most academics, Catherine and John found it a challenge to pursue their research in the midst of teaching loads, administrative duties and resource limitations. This everyday context had a clear impact upon the research goals set by both informants and their tolerance of uncertainty encountered at a particular moment in time. The reality of busy timetables, for example, changed earlier decisions and influenced immediate decisions to pursue or not pursue resources or lines of inquiry about which they felt some ambivalence or doubt.

However, in terms of the research topic each was investigating, both informants displayed a level of tolerance more akin to that attributed to Kuhthau's securities analyst-as-novice and the lawyers ([Kuhlthau and Tama 2001](#kuh01)). In the midst of their challenging workloads, discussions about the nature of their projects showed Catherine and John remain motivated and committed despite great uncertainty about the outcomes of their projects. The excitement of exploring new territory was sometimes enough to overcome the confusion of a particular moment. Not knowing about something motivated informants to pursue the idea even further, suggesting that a certain amount of uncertainty and interest together drive innovation and creativity. Perhaps the curiosity that motivates researchers to examine uncharted waters in the first place brings with it a degree of uncertainty tolerance.

The way Catherine and John deal with uncertainty in the course of their research work is related to the confidence they have in their own abilities as searchers as well as in their confidence about themselves as researchers. It can also be related to their perceptions of their field, their specific research topics and their position within their discipline.

## Embedded quality of positive and negative forms of uncertainty

Uncertainty serves a critical function in the exploration and generation of knowledge in the ever-evolving circumstances of informants' research projects. The uncertainty experienced by them takes many forms deeply embedded in the interwoven layers of information seeking and use. Uncertainty and understanding are intermingling: while clarity about one aspect emerges, a new aspect of the inquiry surfaces to challenge established understandings. There is, thus, a continuous interplay between exploration and formulation, inquiry and inspiration. However, it must also be said that even these seasoned researchers were sometimes observed experiencing the frustration and doubt that has been described as accompanying uncertainty in previous research. While it is a positive force for exploring the world of ideas, uncertainty can become overwhelming at times, particularly at the immediate level of search engagements.

The research process of both informants was largely non-sequential. It was not uncommon for them to be in the focus formulation phase of one aspect of their project whilst completing the write-up of another. Complexity is clearly at play in the experiences of these informants. Neither informant's research is either straightforward or finite in any pre-determined way. Deadlines shift and research goals and priorities change for a number of reasons that may or may not have something to do with intentions Catherine and John had for their own work. In both cases, their projects could in fact be seen as a number of separate but inter-related ones. Equally, the topics they were exploring could be divided into themes which would in themselves **still** be complex undertakings. Furthermore, the informants' research work cannot be completely isolated from the other work performed by them. This situation is not unusual; it is the nature of research practice in this academic context. It does, however, present challenges when we try to understand how we might best support the search and research activities in these conditions.

#### Interplay between certainty and uncertainty

Both certainty and uncertainty were detected in relation to the informants' research and search activities. There were situations when both informants stated that they were _very certain_ about a decision they were making, as evidenced by statements such as:

> I know I need to go down that track [discussing the direction research was taking] or  
> That is clearly appropriate [explaining why a citation was relevant].

In this way the study supported Yoon and Nilan's ([1999](#yoo99)) argument about the interplay between knowing and not knowing in an information-seeking situation. Perceptions are also dynamic: an informative artefact that is _clearly relevant_ in one instance can be at the centre of indecision and uncertainty at some other time.

Knowledge about the field John is exploring, for instance, helps make him aware of issues he needs _to start picking up on_. While explaining that he is uncertain about the meaning of one such article, John proceeds to explain what he thinks it is about:

> ...there is a risk that a lot of the existing literature on social evolution is actually coming out of a historical framework - analytical framework - and maybe articles like this are beginning to challenge that framework and suggest an alternative framework. So, clearly it is something that I need to be familiar with. So, I'll include it.

There appears to be a level of certainty, or confidence, in this statement. Based on what he **does** know about the topic he is investigating, he senses he needs to move into this area so he adds this abstract to his growing list of relevant items.

Sometimes Catherine and John appear very certain about a selection they have made, or the significance of an idea or topic for their project. On the other hand, there is evidence of situations of greater uncertainty than certainty about a decision. This finding suggests that, like the relationship between relevance-irrelevance, a spectrum of certainty-uncertainty is present in these judgments.

#### Sharing uncertainty

Working through uncertainty is a mediating strategy used for knowledge generation, information discovery and focus formulation. It was found that observable uncertainty can be indicative of a person having difficulty communicating. In such situations, judgments cannot be readily articulated by an individual. Informants' experiences resemble Yoon and Nilan's ([1999](#yoo99)) depiction of communicative behaviour as an attempt to share the meaning they are trying to create in any given moment.

The relation between uncertainty and the social sharing of ideas with colleagues warrants further exploration. Both informants remarked that verbalising their doubts and uncertainties regarding documents and their topic often helped them to clarify their research interests overall. Catherine, for instance, uses her copies of conversations recorded in the field work as a way of working through her ideas. She explains:

> I think the difference between just thinking about things in your head and actually just having a conversation about them have been quite useful to me.

Conference attendances at timely points in a project or conversations with colleagues were also shown to have an impact in both informants' cases.

#### Identifying and supporting desirable uncertainty

Uncertainty provided an opportunity for growth, marking the evolving horizon of informants' understanding. An awareness of this uncertainty drove them forward in their research pursuits. Such positive forms of uncertainty were often - but not exclusively -- associated with the informants' explorations within the wider research tasks in which they were engaged. What allows some challenges associated with uncertainty to be welcomed as signs informants were getting somewhere in their work rather than as anxiety-laden obstacles? Initially, it appears that these positive forms can be related conceptually to the notion of _desirable uncertainty_ Cole ([1993](#col93)) describes, where uncertainty is _...associated with the degree of freedom of the act or process whereby one message is chosen or selected from a set of possible messages..._ ([Cole 1993](#col93): 209).

Focusing on the informing potential of the _messages_ a person might encounter, Cole observes: _The more improbable the selection the more information associated with the particular informing process. An improbable selection might lead to things like insight or a turning point in the person's research._ ([Cole 1993](#col93): 210)

The positive forms of uncertainty experienced by Catherine and John appear to relate to the aspirational qualities inherent in Cole's description and the recognition of the exciting moments of discovery that motivate researchers to carry on with their work.

Theoretically at least _desirable uncertainty_ is something that should be supported by system design and intermediary intervention. However, as is seen in the descriptions of informants' experiences, sometimes it is sometimes difficult to distinguish desirable from undesirable uncertainty. The negative and positive are closely bound to one another in a situation. The uncertainty that contributes to anxiety in the search process may in fact be a constitutive part of the research process in a positive rather than negative sense.

The bounding exercise recounted earlier in relation to Catherine's experience shows this mix of positive and negative forms. Reflecting on her own experience, she talks about how valuable she found her struggle through the confusion surrounding the framework she was trying to formulate for a particularly troublesome thesis chapter:

> I'm happy to have found it... The thing that's been so useful is to really think through the categories [in relation to search terms] that I was looking for - how they combine, what they can produce. And if I'm not finding things through those categories, how, what other ways - what other resources am I going to be able to draw on in developing the chapter.

The searches she refers to here were full of frustration for her, shaking her confidence in her approach to that chapter. Catherine's experience challenges assumptions that developing clearer task goals (a reduction in task uncertainty) can reduce information seeking uncertainty and contribute to the ability to make more confident and discriminating relevance judgements (see [Attfield _et al_. 2003](#att03): 435). The uncertainty associated with Catherine's problem structure appeared to contribute to a great deal of the frustration she experienced in her search for information for the chapter's topic. And yet, longitudinal examination of Catherine's experiences suggests that, in the midst of this experience, she frames her frustration in terms of the wider goal (her chapter and her thesis) in such a way that suggests she finds it a productive, useful activity.

Herein lies our challenge. The intricacy of the relationship between what we might (on the surface at least) consider desirable as opposed to undesirable uncertainty is not easily unravelled. While not all negative manifestations of uncertainty can be considered helpful, the positive and negative are not easily uncoupled from one another. From the searcher's perspective, desirable uncertainty appears to emerge through interplay between these positive and negative forms. We cannot pull them apart without risking destroying the very features we would like to support.

## Conclusions

By describing the forms of uncertainty observed within the search and research activities of informants, this study contributes to earlier discussions about the way experts deal with uncertainty when completing complex tasks in a professional setting. The richness of the fieldtexts used for the analysis of uncertainty in action presented here is difficult to encapsulate in one paper. The results presented here, however, demonstrate that this ethnographic approach provides valuable insight into the complexity surrounding the uncertainty within these informants' experiences.

Uncertainty was found to be a necessary and unavoidable condition for each informant's research progress. Uncertainty was embedded in the interpretive processes by which informants made connections between new ideas or information encountered and their pre-existing knowledge. These judgments were integral to the shaping, expanding, refining and reforming of boundaries for their research activities. Uncertainty appeared particularly evident near the boundaries of the topics each informant was exploring and in relation to judgments of information considered partially relevant. At such boundaries of understanding, uncertainty is associated with both frustrating and motivating qualities. However, it is the holistic experience of both the positive and negative forms of uncertainty that often contributes to what we might consider _desirable uncertainty_.

The uncertainty witnessed in the activities of these two informants motivates, makes them aware of potentially significant lines of inquiry and contributes to the innovative and creative processes associated with their research work. While this explosion of ideas is immensely valuable for their research, it is sometimes accompanied by a lack of confidence, doubt or frustration. Significantly, neither informant appeared deterred from their research at any point in the study; their passion and curiosity is evident in conversations with them throughout the two years of engagement with them. The interplay between positive and negative forms of uncertainty may be one way of explaining their ability to tolerate challenging encounters within their information and research processes.

## References

*   <a id="att03"></a>Attfield, S., Blandford, A. & Dowell, J. (2003). Information seeking in the context of writing: a design psychology interpretation of the 'problematic situation'. _Journal of Documentation_, **59**(4), 430-453\.
*   <a id="bar94"></a>Barry, C.L. (1994). User-defined relevance criteria: an exploratory study. _Journal of the American Society for Information Science_, **45**(3), 149-159\.
*   <a id="baw86"></a>Bawden, D. (1986). Information systems and the stimulation of creativity. _Journal of Information Science_, **12**(), 203-216\.
*   <a id="boh03"></a>Boholm, Å. (2003). The cultural nature of risk: can there be an anthropology of uncertainty? _Ethnos_, **68**(2), 159-178.
*   <a id="bud04"></a>Budd, J.M. (2004). Relevance: language, semantics, philosophy. _Library Trends_, **52**(3), 447-462.
*   <a id="bys05"></a>Byström, K. & Hansen, P. (2005). Conceptual framework for tasks in information studies. _Journal of the American Society for Information Science and Technology_ , **56**(10), 1050-1061\.
*   <a id="bys95"></a>Byström, K. & Järvelin, K. (1995). Task complexity affects information seeking and use. _Information Processing & Management_, **31**(2) 191-213\.
*   <a id="col93"></a>Cole, C. (1993). Shannon revisited: Information in terms of uncertainty. _Journal of the American Society for Information Science_, **44**(), 204-211\.
*   <a id="erl03"></a>Erlandson, D.A., Harris, E.L., Skipper, B.L. & Allen, S.D. (1993). _Doing naturalistic inquiry. a guide to methods_. Newbury Park, CA: Sage Publications.
*   <a id="for99"></a>Ford, N. (1999). Information retrieval and creativity: towards support for the original thinker. _Journal of Documentation_, **55**(5), 528-542\.
*   <a id="fos03"></a>Foster, A. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation_, **59**(3), 321-340\.
*   <a id="had04"></a>Hadolt, B. & Stöckl, A. (2004, September). _Reconfiguring 'uncertainty': ontological insecurity, partial knowledge and reasoning in a changing world._ Paper presented at the EASA2004 - Face to Face: Connecting Distance and Proximity, European Association of Social Anthropologists 8th Biennial Conference, Vienna, Austria. (Workshop 66).
*   <a id="hjo02"></a>Hjørland, B. & Christensen, F.S. (2002). Work tasks and socio-cognitive relevance: a specific example. Brief Communication. _Journal of the American Society for Information Science_, **53**(11), 960-965\.
*   <a id="kuh91"></a>Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371\.
*   <a id="kuh93"></a>Kuhlthau, C. C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, **49**(4), 339-355\.
*   <a id="kuh99a"></a>Kuhlthau, C. C. (1999a). Investigating patterns in information seeking: concepts in contexts. In T. D. Wilson & D. K. Allen (Eds.), _Exploring the contexts of information behaviour: proceedings of the second international conference on research in information needs, seeking and use in different contexts 13/15 August 1998, Sheffield, UK_ (pp. 10-20). London: Taylor Graham.
*   <a id="kuh99b"></a>Kuhlthau, C. C. (1999b). The role of experience in the information search process of an early career information worker: perception of uncertainty, complexity, construction, and sources. _Journal of the American Society for Information Science_, **50**(5), 399-412\.
*   <a id="kuh04"></a>Kuhlthau, C. C. (2004). _Seeking meaning: a process approach to library and information services_ (2nd ed.). Westport, CN: Libraries Unlimited.
*   <a id="kuh01"></a>Kuhlthau, C. C. & Tama, S. L. (2001). Information search process of lawyers, a call for 'just for me' information services. _Journal of Documentation_, **57**(1), 25-43.
*   <a id="lin85"></a>Lincoln, Y. S. & Guba, E. G. (1985). _Naturalistic inquiry_. Newbury Park, CA: Sage Publications.
*   <a id="mag02"></a>Maglaughlin, K. L. & Sonnenwald, D.H. (2002). User perspectives on relevance criteria: a comparison among relevant, partially relevant, and not-relevant judgments. _Journal of the American Society for Information Science and Technology_, **53**(5), 327-342\.
*   <a id="sch70"></a>Schutz, A. (1970). _Reflections on the problem of relevance._ Edited, annotated, and with an introd. by Richard M. Zaner. New Haven, CT & London: Yale University Press.
*   <a id="vak99"></a>Vakkari, P. (1999). Task complexity, problem structure and information actions: Integrating studies on information seeking and retrieval. _Information Processing & Management_, **35**(6), 819-837\.
*   <a id="wea49"></a>Weaver, W. (1949). Recent contributions to the mathematical theory of communication. In C.E. Shannon & W. Weaver, _The mathematical theory of communication_ (pp. 94-117). Urbana, IL: The University of Illinois Press.
*   <a id="wil99"></a>Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270\.
*   <a id="wil02"></a>Wilson, T.D., Ford, N., Ellis, D., Foster, A. & Spink, A. (2002). Information seeking and mediated searching. Part 2\. Uncertainty and its correlates. _Journal of the American Society for Information Science and Technology_, **53**(9), 704-715.
*   <a id="yoo99"></a>Yoon, K. & Nilan, M. S. (1999). Toward a reconceptualization of information seeking research: focus on the exchange of meaning. _Information Processing & Management_, **35**(6), 871-890\.
*   <a id="zer93"></a>Zerubavel, E. (1993). Horizons: on the sociomental foundations of relevance. _Social Research_, **60**(2), 397-413\.