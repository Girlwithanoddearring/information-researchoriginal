#### Vol. 12 No. 1, October 2006

* * *

# A naïve ontology for concepts of time and space for searching and learning

#### [Makiko Miwa](mailto:durrance@umich.edu)  
National Institute of Multimedia Education, Chiba, Japan

#### [Noriko Kando](mailto:fisher@u.washington.edu)  
National Institute of Informatics, Tokyo, Japan

#### Abstract

> **Introduction.** In this paper, we propose a new approach for developing naïve ontology as the basis for optimal information access interfaces for multimedia digital documents intended for novice users.  
> **Method.** We try to elicit the knowledge structure of domain novices and patterns of its modification in their searching and learning processes by eye-tracker and showing eye-movements in the post-search interviews.  
> **Analysis.** Recorded interview data were fully transcribed and coded using Atlas.ti and analysed following a bottom-up strategy of the constant-comparative technique.  
> **Results.** We developed a taxonomy of knowledge modification which includes (1) adding, (2) correcting, (3) limiting, (4) relating, (5) specifying and (6) transforming.  
> **Conclusion.** The taxonomy may be expanded and elaborated as the project progress and findings are expected to be incorporated into the design of the naïve ontology. The study results provided theoretical implications on knowledge building, methodological implications on data collection using eye-tracker and showing eye-movements in the post-search interviews and useful information on the design of information access interface for novices users.

## Introduction

User-oriented digital information search environments call for flexible information access interfaces that may interact with a dynamically changing searcher _view_ in capturing a variety of media. Optimal use of conventional libraries and bibliographic databases requires a general understanding of the knowledge structure of the collection domain ([Hsieh-Yee 1993](#hsi93); [Pennanen & Vakkari 2003)](#pen03). Novice searchers without such understanding, however, can seek the help of librarians and intermediaries when they get lost in search processes.

Increasing numbers of digital libraries and online resources on the Internet provide potential users with opportunities to access and interact with these resources directly from offices and homes. Such trends seem to offer searchers useful information access environments for a variety of information resources. However, in such environments, novice searchers are forced to seek the information they need without the help of librarians or other intermediaries. In reality, many novice users of digital libraries do not have a general understanding of the knowledge structure of the digital collections held by these libraries. Eventually they may give up pursuing their information needs when they get lost during search processes or obtain unsatisfactory search results.

This research project seeks to find a way to overcome such limitations of existing information access interfaces developed for traditional libraries and bibliographic information services. Specifically, we explore a qualitative research method for eliciting the knowledge structure of novice searchers and patterns of its modification in their search and learn processes, and build on it a naïve ontology for time and space.

In this paper, we report findings from a series of Web searching experiments where novice searchers' search and learn processes were conducted as the initial stage of the exploration. Through these experiments, we identified a reliable method of approaching the phenomenon. We also identified several characteristics of patterns of knowledge modification as well as other search and learn behaviour of novice searchers.

## Related works

This research attempts to overcome the limitation of conventional subject access systems developed for traditional libraries and information retrieval systems, from the standpoint of managing a large collection of multimedia digital documents. The focus of the study is on the elicitation of searchers' knowledge structure and tracking of its modification during their search and learns processes.

A conventional role of ontology in information science is to provide labels or keywords for indexing documents so that both indexers and searchers can use the same label to represent a concept. This convention allows a searcher to identify the documents relevant to their information needs as long as the searcher's knowledge is well structured to reflect that of domain experts. Novice searchers without such a well-structured domain knowledge may suffer from search failure, particularly when they do not have much experience in searching for information.

Belkin _et al._ ([1982)](#bel82) suggested Anomalous State of Knowledge as the basis for information needs and identified five types of anomalous states by representing and comparing a series of search requisites. By comparing tactics used by novice searchers and information professionals, Hsieh-Yee ([1993](#hsi93)) found that domain knowledge affects only experienced searchers. By comparing thesaurus terms used by novices and experts in a domain, Vakkari and his colleagues concluded that only those with sufficient domain knowledge may improve search results by using thesaurus terms in query expansion ([Vakkari _et al._ 2001](#vak01); [Sihvonen & Vakkari 2004](#sih04); [Pennanen & Vakkari 2003](#pen03)).

The increasing availability and accessibility of Internet resources to the general public allow the novice searcher to interact with document collections. In response to such trends, the main role of ontology seems to be shifting from a search aid to a navigation tool. This transition is reflected in recently proposed navigation tools that combine browsing and searching functions in a seamless manner. The Flamenco system allows searchers to browse a large collection of architectural images using hierarchically faceted metadata. Searchers of the system can navigate without disturbance of their thought processes ([Hearst _et al._ 2002](#hea02)). [JuNii Plus](http://ju.nii.ac.jp/), an information access interface for the shared portal of the Japanese academic community, incorporates ontology-based and content-based retrieval for ranking Web documents. The system intends to provide a seamless switching between searching and browsing ([Kando _et al._ 2006](#kan06)). As demonstrated in the examples above, ontology for navigation tools is expected to be naturalistic for searchers so that they can follow their own _view_ of the world during search processes without being disturbed by the mandatory use of unfamiliar ontology intended for domain experts.

## Conceptual framework

There are several different approaches to information retrieval within the field of information science. In addition, human information seeking behaviour, the domain of this study, is currently the focus of research in various fields external to information science. Such diversity inevitably introduces serious terminological confusion. To avoid such confusion, we will define key terms below as we introduce a conceptual framework.

The term _information access interface_ describes the interface that mediates between information resources and users, and provides aids for users in accessing or using information resources stored on electronic media. The information access interface may include metadata, question answering systems, navigation systems, and virtual agents that intend to support optimal interaction between searchers and information objects.

The term _ontology_ is used in this research to mean an explicit structure that describes patterns of associations among concepts, as typified by _classification schema_. _Naïve ontology_ is a type of information access interface that allows novice searchers to refine their knowledge interactively by acquiring information chunk-by-chunk as they encounter it in digital environments. Specifically this is a kind of navigation tool that supports browsing and searching by novice searchers throughout their search and learn processes.

### Approaches in developing ontology

This study follows the principle of user warrant in developing naïve ontology in order to overcome limitations in the principle of literacy warrant.

There are two basic approaches, automatic and intellectual, in developing ontological tools for conventional text-based information retrieval systems. The automatic approach is based on statistical manipulation of text messages revealing associations between terms or clusters of terms. For the intellectual approach, ontology is created and designed by humans based on various sources including knowledge structures of taxonomies and experts. Here, the bottom-up approach has been recommended as more reliable than the top-down approach. There are two principles, literary warrant and user warrant, within the bottom-up procedure of the intellectual approach. These two principles may be combined to increase the reliability of ontological tools ([Lancaster 1986](#lan86)).

This study uses the principle of user warrant. This is because the principle of literary warrant is too limiting for the development of naïve ontology intended for novice searchers who are not experts in the domain. The principle of literary warrant requires terms and concepts to be used for the basis of the ontology to be collected from literature written by authors who are experts in the domain. Such a procedure may develop ontological tools useful for domain experts but not for novice searchers who are unfamiliar with the domain. In addition, it is suggested that the principle of literary warrant relies heavily on terminology and concepts in the text portion of documents. Thus, the principle is limiting for developing ontological tools for the ever-increasing number of multimedia documents that may not include much text data.

In order to overcome these limitations, this research expands the principle of user warrant and tries to elicit knowledge structure as well as its modification by novice searchers during their search and learn processes.

### Unit of analysis in browsing

Human beings cannot function at all without the ability to categorize either in the physical world or in our social and intellectual world. Thus, understanding of how we categorize things and concepts is central to understanding how we think, learn, and behave ([Lakoff 1987](#lak87): 1). When we see things using visual perception, our eyes isolate the input data from all other data and see only such correspondence as is determined by the input identification ([Bertin 1967](#ber67): 11). Kwasnik ([1992](#kwa92): 194) defined the notion of _a view_ as _what a person articulate as seeing at one time, that is, a span of attention_ and proposed to use it as a unit of analysis in browsing.

By extending the notion of _view_ into the cognitive world and using it as the unit of analysis, we might be able to elicit novice searchers' knowledge structure and patterns of its modification during their search and learn processes. They may be useful as a basis for developing naï ontology to be incorporated in the design of information access interfaces intended for novice searchers.

### Needs for naïve ontology for concepts of time and space

Concepts of time and space may have more important roles in browsing and searching multimedia documents than texts. The international standard metadata profiles for multimedia documents such as Dublin Core and IEEE/Learning Object Metadata (LOM) include an element of _coverage_ that describes the special or temporal characteristics of the intellectual content of the resource (see the [Taskforce Website](http://dublincore.org/educationwiki/DCMIIEEELTSCTaskforce)). Spatial coverage refers to a physical region using place names or coordinates. Temporal coverage refers to what the resource is about rather than when it was created or made available, and is typically specified using a named time period or date and time format. However, limited sets of controlled vocabulary are recommended: the Library of Congress Subject Headings (LCSH) and the Getty Thesaurus of Geographic Names in addition to numeric schemes for spatial qualifiers; LCSH, the Art and Architecture Thesaurus and the Lexicon of stratigraphic nomenclature for temporal qualifiers. Such controlled vocabularies may be useful for domain experts but are not readily comprehensible to novice searchers. This is because existing controlled vocabularies seem to be too complex for novice searchers who have not yet developed a comprehensive knowledge of domain structure. Thus, this research focuses on the knowledge structure of novice searchers or non-domain experts for concepts of time (temporal) and space (geographic) aspects. We chose history and geography as instances of domains in eliciting knowledge structure to develop naïve ontology.

Specifically, we ask the following two research questions to guide our exploration:

*   Q1: How are the concepts of time and space held by novice searchers structured?
*   Q2: How are novice searchers' concepts of time and space modified during search processes?

## Method

We conducted a series of experiments involving college students as participants to seek for an appropriate methodology to approach the phenomena. We invited college students because we considered them to represent novice searchers in the domains of history and geography.

So far, we have conducted two experiments: one in September 2005 with three participants and the other in December 2005 with four participants. Participants were selected from third-grade female college students. The experiments consisted of the following processes:

**_Questionnaire_**: Participants were requested to complete a short questionnaire that asked them the frequency of their Internet usage, favourite browsers and search engines, topics chosen at the college entrance examination, whether they were planning to get a teacher's certificate, and topics of interest in history and geography.

**_Pre-search_ ****_Activities_**: For the first experiment, each participant was given two vignette scenarios prepared by the researcher. The first scenario asked the participant to conduct a Web search to prepare for teaching a middle school history class on a particular topic. The second scenario asked the participant to conduct a Web search to prepare for visiting a world heritage site. Names of people and places for the visit used in each vignette scenario were selected based on a short questionnaire survey conducted in a college library and information science class from which participants for the first experiment were recruited.

For the second experiment, we changed the procedure and invited a pair of participants to discuss their topics of interest in the domains of history and geography for about ten minutes to let them choose two topics for Web search, one for each participant, a history and a geography topic. The modification in the procedure was introduced because each participant in the first experiment was tense and nervous because she had to wear unfamiliar devices and sit alone in a lab environment. This was also because one participant was unable to make progress in her search and learn because of the confusion caused by encountering incomprehensible information. In both experiments, participants were asked what and how much they already knew about the topic of the search.

**_Recording of Search and Learn Processes_**: During the search and learn processes, screen shifts, mouse movements and participants' eye movements were captured using an eye tracker (EMR-NL8B). Each session took about fifteen to twenty minutes.

**_Post-search Interview_**: After each Web search, an interview was conducted where the recorded search process was displayed, and the participant reported why she made a particular move (e.g. typing, mouse click, browsing) and what she thought, felt and expected at each moment throughout the search and learn process. We applied the notion of _a view_ in collecting data on participants' cognitive movements during the search process by showing their eye-movement on the screen at post-search interview to help them recall what they did, thought and felt at the time when they were browsing a particular part of the screen. The interviews were recorded and transcribed in detail, then analysed using ATLAS.ti following a bottom-up strategy with the constant comparative technique.

## Results

In the post-search interview, participants articulated what they did, thought, felt, and expected at each point of movement, either voluntarily or in response to the interviewer's inquiries. From their communication, we identified several characteristics of knowledge acquisition and search and learn as follows.

### Expressions of time and space related concepts

Participants' expressions of the concepts for time and space, as embedded in their communication, were categorized into three types.

_Solely time-related concepts_ were expressed as dates in the Christian era, temporal relationships with some well-known incidents, and dates in relation to ages of a famous figure. Table 1 presents examples of these three types.

<table><caption>

**Table 1: Three types of temporal expressions**</caption>

<tbody>

<tr>

<th>Type</th>

<th>Expressions*</th>

</tr>

<tr>

<td>Solely time-related concepts</td>

<td>13th Century; 1600s</td>

</tr>

<tr>

<td>Temporal relationships with some well-khown incidents</td>

<td>just after Japan opened trade  
He came to Japan during the seclusion era</td>

</tr>

<tr>

<td>Dates in relation to ages of a famous figure</td>

<td>the anniversary of Beethoven's death</td>

</tr>

<tr>

<td colspan="2">

_* In this and other tables the expressions were translated from Japanese by the author_</td>

</tr>

</tbody>

</table>

_Solely space-related concepts_ were expressed either as place names, country names, areas of a country, or distances between places. Examples are shown in Table 2.

<table><caption>

**Table 2: Expressions of space**</caption>

<tbody>

<tr>

<th>Type</th>

<th>Expressions</th>

</tr>

<tr>

<td>Place/country names</td>

<td>the Alhambra is located in Granada, Spain</td>

</tr>

<tr>

<td>Areas of a place or country</td>

<td>Basho travelled around the north-eastern region in Japan</td>

</tr>

<tr>

<td>Distance between places</td>

<td>The distance between the north and south ends of Israel is 500 km.</td>

</tr>

</tbody>

</table>

_Concepts embracing time and space_ were articulated as periods or eras, as exemplified in Table 3.

<table><caption>

**Table 3: Expressions of concepts embracing time and space**</caption>

<tbody>

<tr>

<th>Expressions</th>

</tr>

<tr>

<td>

*   The Taj Mahal was built during the Mughal Empire.
*   The Alhambra was first built in the post-Umayyad era and expanded in the Nasrid dynasty.
*   in the early Edo period.
*   Japanese era: Genroku or Kasei.

</td>

</tr>

</tbody>

</table>

These expressions imply that concepts of time and space are deeply interrelated and that historical incidents and figures are kept as contemporaneous entities in participants' knowledge. This finding suggests the provision of functions that allow the searcher to browse time and space facets simultaneously or interchangeably.

### Modification of knowledge

Participants' knowledge has been modified during their search processes. Modification of knowledge was categorized into six types as described in Table 4 with some examples.

<table><caption>

**Table 4: Six types of knowledge modification**</caption>

<tbody>

<tr>

<th>Type</th>

<th>Definition</th>

<th>Example of expressions</th>

</tr>

<tr>

<td>Adding</td>

<td>Acquire novel information to increase knowledge.</td>

<td>

*   I found a picture of Qumran in which the Dead Sea Scrolls were hidden. I was surprized how they could hide it in a place like this.

</td>

</tr>

<tr>

<td>Correcting</td>

<td>Clear up a misunderstanding.</td>

<td>

*   The area he travelled was not all over Japan, but in the north-eastern region.

</td>

</tr>

<tr>

<td>Limiting</td>

<td>Narrow down the scope of the concept</td>

<td>

*   The site deals with present leisure in Israel, which is not what I'm looking for.

</td>

</tr>

<tr>

<td>Relating</td>

<td>A concept is related to another concept.</td>

<td>

*   The Taj Mahal was influenced by Hymayun's tomb, which is some distance from Taj Majal in terms of time and space.

</td>

</tr>

<tr>

<td>Specifying</td>

<td>A concept is narrowed by increasing specificity.</td>

<td>

*   The Alhambra is located in Granada, Spain.

</td>

</tr>

<tr>

<td>Transforming</td>

<td>A concept is expressed in a different framework.</td>

<td>

*   He lived in Genroku or Kasei (Japanese era) => He lived in the 1600s.

</td>

</tr>

</tbody>

</table>

Identified patterns of modification in knowledge structure provide useful suggestions on the functions to be embedded in the information access interface intended for novice searchers.

Adding: when a participant found pictures of a known place, she increased intimacy and placed her knowledge in context. This finding indicates that the availability of pictures and plans of places and monuments helps novice searchers to place their knowledge in context.

Correcting: sometimes, a searcher found information that contradicted her existing knowledge and corrected it by acquiring the new information. On the other hand, a searcher who found information that contradicted her existing knowledge and tried to resolve the contradiction without success gave up the search. These findings imply that a navigation tool for browsing historical transitions among cultures, people, and religions may be helpful for novice searchers to structure and correct their existing knowledge.

Limiting: identification of irrelevant information or sites increases the focus of the search. This finding implies that a function to help specify time and place of figures and incidents may help novice searchers to put their search within a historical or geographic framework.

Relating: when new concepts were identified, they were related to known concepts. Relating multiple concepts added contexts in knowledge structure. These findings imply the need for a navigation tool that makes it easier for novice searchers to locate contemporaneous figures or incidents in order for them to develop a historical or geographic context to increase familiarity with the topic of the search.

Specifying: specificity of the time and space concepts evolved with the progress of the search. This finding suggests the provision of functions to allow novice searchers to start with a broad concept and to increase specification in both time and space.

Transforming: expressions of time-related concepts were transformed between different calendar systems, such as the Christian era and Japanese eras. This finding indicates that the provision of a navigation tool that allows switching between different calendar systems may be helpful for novice searchers.

Encountering: In addition to the six types of knowledge modification described above, encountering unexpected but somewhat familiar information during the search process would increase participants' affinity with the topic of the search. This finding implies that each participant's life experiences may influence her perceived familiarity with the topic of the search, and its relevance to her.

### Characteristics of searching and learning processes

Participants' general searching behaviour tended to begin with typing one or two known keywords representing relatively broad concepts. They then browsed the search results by clicking links to the sites from the top of the list. If the contents of the first few sites were relevant to the topic of the search, they scanned the contents of each site one by one, quickly looking at basic information such as description, chronology, and glossary to acquire basic information. When they encountered seemingly useful sites, they tended to bookmark them. When they started finding similar information, they moved to the next step by typing more specific keywords obtained from the browsing. Several participants copied keywords found on the relevant site and used them in the next step. This behaviour is consistent with that described by Pennanen & Vakkari ([2003](#pen03)). One of the participants articulated this progressive behaviour as _loose at the beginning, gradually tightening, but changing keywords if too restrictive_.

When participants encountered sites highly relevant to their topic of the search, they read texts in detail and looked at visual information such as pictures of interest and graphics representing correlation among relevant people and incidents. When they encountered sites of interest, such as free wallpapers, historical quizzes, and advertisements for TV programs with popular actors, they interacted with these sites intensively. When they encountered unexpected information, they articulated their surprise and tried to obtain further information for comprehension. These findings imply that duration of browsing in a particular site may indicate the site's level of perceived relevance to the searcher, as suggested by Kelly & Teevan ([2003](#kel03)) and Joachims ([2005](#joa05)). Here the perceived relevance is not limited to the topic but may also include situational or contextual relevance.

Participants acquired or used the following knowledge on the functions of the information access interface during the search processes.

*   One participant indicated that the first three items in Yahoo search results are advertisements for businesses. She sometimes avoided clicking on these items because of that knowledge.
*   One participant changed the colour of texts when she had difficulty reading because of a high density of text.
*   One participant found a button to enlarge the size of pictures displayed on the screen when she was browsing a site with high-quality pictures.
*   One participant learned of the availability of a free online translation service from her partner when she encountered a site written in German. She used that service to obtain information she was looking for.

These findings indicate that searchers modify not only domain knowledge but also knowledge on a variety of functions embedded in the information access interface during their search and learn processes.

### Impressions of sites

While participants were browsing the sites found during their search, they sometimes articulated their impressions.

*   One participant said that a site seemed to be easy to interact with because it was simple and texts were displayed using several different colours, each representing a different category.
*   One participant complained that the overseas security information site offered by the Japanese Ministry of Foreign Affairs had many missing links.
*   One participant asked for a way to limit search results to primary documents rather than secondary documents such as bibliographic information on books.
*   One participant articulated her appreciation of images on a particular site with many high-density pictures.
*   One participant appreciated the format of _Wikipedia_ and said that important parts were written in blue text, making it easy to read.
*   One participant appreciated a graphical presentation of the correlation between people and incidents and said that it was easy to grasp the general chronology without scrolling and reading a long textual document.

The above comments may be useful for designing Web pages or information access interfaces for novice searchers.

### Comments and afterthoughts on the individual search processes

At the end of the post-search interview, the interviewer asked each participant to comment on her own search processes. Participants responded as follows.

*   Even though I copied good candidates for keywords for the next step of the search, I forgot about that and took a different move.
*   A participant knew that Internet search results are noisy and noticed that her search process was characterized by quick scanning when she observed her eye movements on the recorded search processes.
*   A participant found herself looking at pictures for a long time when she encountered a relevant site with many high-quality pictures.
*   A participant said that she perceived the time she spent searching as much shorter than the recorded search time when she saw it afterwards.
*   A participant indicated that she did not notice an extremely relevant piece of information shown on the screen because she was watching a different part of the screen.
*   All of the participants said that the eye movements shown on the screen pointed to the actual place of the screen they were looking at, and some of them said that watching their own eye movements helped them recall their search and learn processes.

These comments imply that search processes on the Internet are opportunistic. Novice searchers sometimes plan their next step but do not always follow their plan. Their perception of time during the search may be lost because of the high level of concentration. In addition, the notion of _view_ as expanded to the cognitive world and chosen as the unit of analysis for this study was found to be adequate for analysing search processes on the Internet; searchers' visual perception tended be focused on a part of the screen or a _view_.

It should be noted that watching their own eye movements helped participants recall their search and learn processes. If so, the reliability of data elicited, and eventually the reliability of the study, should be increased. Thus, the method of using the eye tracker in capturing search and learn processes and showing the eye movement during the post-search interview may be recommended in collecting data on search and learn processes.

## Implications

This exploration provided us with useful information for designing naïve ontology, while offering theoretical and methodological implications for studying human information behaviour.

### Practical implications

Results of elicitation and analysis of novice searchers' search and learn processes gave us information for the design of naïve ontology as reported in the previous chapter and synthesized in Table 6.

<table><caption>Table 6: Implications for the design of a naïve ontology</caption>

<tbody>

<tr>

<th>Aspect</th>

<th>Implications</th>

</tr>

<tr>

<td>Browsing</td>

<td>

*   browse time and space simultaneously or interchangeably
*   browse time and space from broad to specific
*   trace and overview historical transitions to see high-quality pictures
*   choose pictures from among many thumbnails to see high-quaity pictures

</td>

</tr>

<tr>

<td>Contents</td>

<td>

*   provide high-quaity pictures and visual images of places and monuments
*   provide information on the genre of contents
*   represent correlations between people and incidents using graphics
*   provide a glossary of technical terms
*   provide a dictionary of historical figures and events
*   use colour to categorize texts
*   present the content in a simple manner

</td>

</tr>

<tr>

<td>Navigation</td>

<td>

*   allow switching between different calender systems
*   provide links to contemporaneous figures and incidents
*   provide historical and geographic frameworks for features and incidents
*   provide links to free online translation services

</td>

</tr>

</tbody>

</table>

The above implications are expected to be expanded and elaborated through the further progress of the research project.

### Theoretical implications

Theoretical aspects of information seeking behaviour were identified from the study as follows.

Perceived relevance is associated with an individual searcher's life experience. Participants brought their past experience and social contexts into search and learn processes. A participant was fascinated by information encountered on interests of a friend and free wallpapers that could be downloaded for her own use. This may lead to the enhancement of the concept of situational or contextual relevance ([Schamber _et al._ 1990](#sch90)), which may be measured by the time spent on a site as well as patterns of searchers' eye movements ([Joachims 2005](#joa05)).

Searchers learn the functions of the information access interface through searching and learning. Participants' articulations indicated that they learn not only domain knowledge but also functions embedded in the information access interface. Thus, search and learn processes of novice searchers modify not only their domain knowledge but also knowledge and skills for using various functions available on the Web.

The search and learn process on the Web is opportunistic. Searchers make plans during the progress of their search and learn process, but they may forget them when they encounter interesting information. They may stray from the initial topic of the search, particularly when the goal of the search is self-generated. Many researchers also suggest viewing searchers' information retrieval and information behaviour as situated and _ad hoc_ ([Hert 1997](#her97); [Xie 1997](#xie97)).

### Methodological implications

One of the objectives of the study, conducted as the initial stage of the research project and reported in this paper, was to find an appropriate methodology to elicit knowledge structure and patterns of its modification by novice searchers during their search and learn processes. Through the series of experiments reported above, we obtained several implications concerning methodological aspects.

First, we were convinced that the unit of analysis for browsing, suggested by Kwasnik ([1992](#kwa92)), is adequate in capturing the dynamically changing knowledge structure of searchers during their searching and learning processes. During the post-search interviews, participants reported voluntarily or in response to the researchers' inquiry what they thought, felt and had done in relation to what they saw on the screen with their eye movements. They said the eye movements shown on the screen pointed to the exact places that they were looking at and that watching their own eye movements helped them recall their search and learn processes. Thus, the research method of using eye trackers in data collection of Web search processes and showing eye movements during the post-search interviews is recommended for future studies, not only to identify what searchers are looking at rather than what data are shown on the screen but also to increase the reliability of data elicited from searchers, which eventually increases the reliability of research findings.

The use of vignette scenarios of imposed goals may lead to incomplete processes. The use of vignette scenarios introduced searching behaviour peculiar for imposed goals, while allowing participants to choose topics produces naturalistic searching behaviour. Participants of former study designs may suffer from incomprehensible information resulting from a completely unknown search topic. Participants of the latter design may stray from the initial search and learn goals set by themselves because of the much greater freedom that they have ([Miwa 2000](#miw00)).

## Conclusion and future research

In an effort to find an optimal approach for developing naïve ontology for information access interfaces to better navigate novice searchers of multimedia digital documents, two Web searching experiments were conducted by volunteer college students. The concept of _view_ was used as the unit of analysis in eliciting novice searchers' knowledge structure and patterns of its modification while they undergo search and learn processes.

We obtained useful information for the design of a naïve ontology as presented in Table 6\. These implications may be expanded and elaborated as the project progresses and are expected to be incorporated into the design of the naïve ontology to be developed as the output of the project.

The study results provided theoretical implications on (1) perceived relevance to be generated from life experiences of searchers, (2) modification of search skills by learning functions of the information access interface in search and learn processes, and (3) the situations of search and learn processes.

We also obtained methodological information through this study. The results demonstrated that the use of an eye tracker in capturing search and learn processes and showing eye movements to the participants during the post-search interview helped improve reliability.

What we have described above are results of two small case studies conducted at the initial stage of the research project. We will continue this research project by recruiting more participants. Through such study, we will expand and elaborate these functions. The design of an information access interface, the goal of this research project, will be attained through continuous interaction with novice searchers as the research project progresses.

## Acknowledgement

This research was funded by the National Institute of Informatics joint research grant, and we wish to thank Dr Barbara Kwasnik at Syracuse University for her helpful suggestions and the notion of _view_ she proposed as the unit of analysis for browsing, which helped us to develop the conceptual framework of this study.

## References

*   <a id="bel82"></a>Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982). ASK for information retrieval I & II. _Journal of Documentation_, **38**(2), 61-71 & (3), 145-164.
*   <a id="ber67"></a>Bertin, J. (1967). _Semiologie graphique_. Paris and Den Haag: Mouton. [English translation: _Semiology of graphics_ Madison, WI: University of Wisconsin Press]
*   <a id="dcm02"></a>Dublin Core Metadata Initiative and Institute of Electrical and Electronic Engineers. _Learning Technology Standards Committee_ Taskforce. (2002). _IEEE Learning Object Metadata RDF binding_. Retrieved 16 December, 2006 from http://dublincore.org/educationwiki/DCMIIEEELTSCTaskforce
*   <a id="hea02"></a>Hearst, M., English, J., Shinha, R., Swearingen, K. & Yee, P. (2002). Finding the flow in Web site search. _Communications of the ACM_, **45**(9), 42-49.
*   <a id="her97"></a>Hert, C.A. (1997). _Understanding information retrieval interactions: theoretical and practical implications_. Greenwich, CT: Ablex Publishing.
*   <a id="hsi93"></a>Hsieh-Yee, I. (1993). Effects of search experience and subject knowledge on the search tactics of novice and experienced searchers. _Journal of the American Society for Information Science_, **27**(3), 117-120.
*   <a id="joa05"></a>Joachims, T., Granka, L., Pan, B., Hembrooke, H. & Gay, G. (2005). Accurately interpreting clickthrough data as implicit feedback. In _Proceedings of the 28th annual international ACM SIGIR conference on Research and development in information retrieval, Salvador, Brazil, 2005._ (pp. 154-161). New York, NY: ACM Press.
*   <a id="kan06"></a>Kando, N., Kanazawa, T. & Miyazawa, A. (2006). [Retrieval of Web resources using a fusion of ontology-based and content-based retrieval with the RS vector space model on a portal for Japanese universities and academic institutes.](http://csdl2.computer.org/comp/proceedings/hicss/2006/2507/03/250730054a.pdf) In, _Proceedings of the 39<sup>th</sup> Hawaii International Conference on System Science, Kauai, HI, 2006\. Track 3_. (pp. 54.1-54.10). Washington, DC: IEEE.
*   <a id="kel03"></a>Kelly, D. & Teevan, J (2003). [Implicit feedback for inferring user preference: a bibliography](http://www.acm.org/sigs/sigir/forum/2003F/teevan.pdf). _SIGIR Forum_, **37**(2), 18-28\. Retrieved 19 December, 2006 from http://www.acm.org/sigs/sigir/forum/2003F/teevan.pdf
*   <a id="kwa92"></a>Kwasnik, B. (1992). A descriptive study of the functional components of browsing. In J. Learson & C. Unger (Eds.), _Engineering for Human-Computer Interaction_, _Proceedings of 5<sup>th</sup> IFIP Working Conference on User Interfaces. Ellivouri, Finland, Aug. 10-14._ (pp. 191-203). Amersterdam: North-Holland Publishing
*   <a id="lak87"></a>Lakoff, G. (1987). _Women, fire and dangerous things: what categories reveal about the mind_. Chicago. IL: University of Chicago Press.
*   <a id="lan86"></a>Lancaster, F.W. (1986). _Vocabulary control for information retrieval_. Arlington, VA: Information Resource Press.
*   <a id="miw00"></a>Miwa, M. (2000). _[Use of human intermediation in information problem solving: a users' perspective](http://www.nime.ac.jp/~miwamaki/dissertation.htm)_. Unpublished doctoral dissertation, Syracuse University, Syracuse, NY. Retrieved 30 January, 2006 from http://www.nime.ac.jp/~miwamaki/dissertation.htm
*   <a id="pen03"></a>Pennanen, M. & Vakkari, P. (2003). Students' conceptual structure, search process and outcome while preparing a research proposal. _Journal of the American Society for Information Science_, **54**(8), 759-770.
*   <a id="sch90"></a>Schamber, L., Eisenberg, M. & Nilan, M. (1990). A re-examination of relevance: toward a dynamic, situational definition. _Information Processing & Management_, **26**(6), 755-776.
*   <a id="sih04"></a>Sihvonen, A. & Vakkari, P. (2004). Subject knowledge improves interactive query expansion assisted by a thesaurus. _Journal of Documentation_, **60**(6), 673-690.
*   <a id="vak03"></a>Vakkari, P., Pennanen, M. & Serola, S. (2003). Changes of search terms and tactics while writing a research proposal. _Information Processing & Management_, **39**(3), 445-463.
*   <a id="xie97"></a>Xie, H. (1997). Planning and situated aspects in interactive IR: patterns of user interactive intentions and information seeking strategies. _Proceedings of the 60<sup>th</sup> ASIS Annual Meeting_, **34**, 101-110.