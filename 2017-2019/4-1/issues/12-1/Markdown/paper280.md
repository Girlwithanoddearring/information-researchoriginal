#### Vol. 12 No. 1, October 2006

* * *

# Teaching information seeking: relating information literacy education to theories of information behaviour

#### [Louise Limberg](mailto:louise.limberg@hb.se) and [Olof Sundin](mailto:olof.sundin@kult.lu.se)  
The Swedish School of Library and Information Science  
Göteborg University and University College of Borås  
SE-501 90 Borås, Sweden

#### Abstract

> **Introduction**. The article argues for a closer association between information seeking research and the practices of teaching information seeking. Findings are presented from a research project on information seeking, didactics and learning (IDOL) investigating librarians' and teachers' experiences of teaching information seeking.  
> **Method.** Thirteen teachers and five librarians, teaching 12-19 year-old students in three schools, participated. Forty-five interviews were conducted over a period of three years.  
> **Analysis.** The IDOL project adopted a phenomenographic approach with the purpose of describing patterns of variation in experiences. The findings were also analysed by way of relating them to four competing approaches to the mediation of information literacy.  
> **Results.** A gap was identified between experiences of teaching content that focused on sources and order, and experiences of assessment criteria applied to students' work that focused on the importance of correct facts and the analysis of information. These findings indicate a highly restricted range of teaching contents when compared with the four theoretical approaches to the mediation of information literacy.  
> **Conclusion.** Teaching information seeking might be enhanced by a wider repertoire of contents reflecting more varied theoretical understandings developed in information seeking research, particularly as regards the importance of content and context related to user perspectives.

## Introduction

As a practice, information seeking is invariably performed in relation to certain, institutionally-shaped objectives. One important setting for information seeking is that of education. In schools at different levels, from preschools to universities, the various practices of information seeking play a central role. This role has increased dramatically over the last decades, a development that can be related to two strong trends: the powerful development of new information and communication technologies and an increasingly student-centred and problem-based pedagogical orientation. The ability to critically seek, evaluate and use information and tools for information seeking within different communities is a competence that is given increasing importance in contemporary western society. To investigate the practice of teaching information seeking is therefore a crucial task for library and information studies.

Information Seeking in Context (ISIC) is a conference that has explored how information seeking practices are embedded and given meaning in various kinds of contexts. With some notable exceptions (e.g., [Kuhlthau 1991](#kuhlth1991); [Limberg 1999](#limb1999)), information seeking and learning in educational settings has not been explored to a great extent, despite the size of the educational sector and the development of student centred problem-based teaching methods. Instead, these questions have been discussed in the field of information literacy, a field that has been developed primarily within academic librarianship, outside the information seeking research community (e.g., [Bruce 2000](#bruce2000)). However, at the ISIC 2006 conference a considerable number of papers on information seeking in educational contexts were presented, an observation that may indicate a growing research interest in this field. While studies on information seeking and learning (cf. Kuhlthau and Limberg) have adopted a learners' or users' perspective, information literacy literature often uses a teaching perspective. As we see it, an understanding of the practices of teaching information seeking requires a view of the interdependence of the two perspectives of teaching and learning.

The aim of this article is to present findings from a study on the practice of teaching information seeking and to discuss the implications of these findings for understanding the relationship between information seeking research and information literacy. The article reports a three year study on librarians' and teachers' experiences of teaching information seeking, _Information Seeking, Didactics and Learning (IDOL)._<sup>[[1](#note1)]</sup> The research interest in exploring a teaching perspective on information seeking emerged out of earlier research findings on students' information seeking and learning, resulting in conclusions that there is a close interaction between the quality of students' information seeking and the quality of their learning outcomes ([Alexandersson & Limberg 2003](#alex2003); [2005](#alex2005); [Limberg 1999](#limb1999)). Another point of departure for the IDOL study was previous research findings that teachers have vague notions of how to teach information seeking related to students' research-based learning (cf. [Moore 2000](#moore)) and that librarians involved in a project of curricular development changed their views of student learning in the library ([Kuhlthau & McNally 2001](#kuhlth2001)).

In this article, we claim that the research fields of information seeking and information literacy have not influenced each other in the way that they have potential so to do. An understanding of information literacy would benefit from being based on an understanding of information seeking: information seeking and information literacy are two sides of the same coin. Information seeking research has the potential to provide information literacy with a theoretical grounding and the empirical field of information literacy gives information seeking research an institutional context where the objectives for information seeking practices are formed. In the article, we use the term information seeking according to Wilson's ([1999](#wils1999)) nested model of information behaviour research, where information seeking is seen as a theoretical concept which includes an understanding of series of searches related to the same task. We would like to emphasize that 'task' is not here understood in a narrow sense, but as tightly interwoven with the tools for information seeking along with aspects of information use, such as evaluating and utilizing sources that are given meaning in different communicative contexts.

In the IDOL study, information seeking as an object of learning is closely related to the concept of information literacy. This implies that the object of learning is constituted by the goals (the intended learning outcomes) and contents of teaching. These goals may be expressed as information literacy; that is, a set of abilities to seek and use information in purposeful ways related to the task, situation and context in which information seeking practices are embedded.

The article starts with a short discussion of information literacy research and how this research can be related to information seeking research. In the next section of the article, the phenomenographic research approach adopted in the IDOL study is introduced, followed by a description of the design of the study. Thereafter, the empirical findings are presented. The article is concluded with a theoretical discussion advocating a closer relationship between information seeking research and the practices of teaching information literacy.

## Alternative views on information literacy

The practice of teaching information seeking is related to earlier and more narrowly oriented library concepts, such as library education or bibliographic instruction. We regard teaching information seeking, in a broad sense, as a way of creating space within which learners are able to develop information literacy. The term information literacy indicates a broadening of focus from practices and tools within, or closely related to, the physical library towards the ability to critically seek, evaluate and use information in various contexts, using a range of search tools and information formats. In this respect, the renewed interest in the practice of teaching information seeking for information literacy is simultaneously related to a long term interest in pedagogical issues within librarianship and to the specific conditions that contemporary information and communication technologies provides.

As has been repeatedly criticized, there is a strong tendency to portray information literacy as sets of isolated skills that have to be mastered by a decontextualized and individual user (e.g., [Simmons 2005](#simm); [Talja 2005](#talja); [Tuominen _et al._ 2005](#tuom); [Webber & Johnston 2000](#webb)). Instead, alternative views on information literacy have been developed. For example, Tuominen _et al._ stress the importance of understanding information practices as embedded in 'the social, ideological, and physical contexts and environments in which information and technical artefacts are used' ([Tuominen _et al._ 2005](#tuom): 340). Such a perspective promotes a view of information literacy education as grounded in an understanding of the communities and practices of which the users are a part. It underlines the importance of considering the social, institutional, and cultural contexts of information seeking practices. Another example of a critical stance in relation to the information literacy literature is Simmons who, from a genre theory standpoint, gives examples of important questions to put to, in her case, undergraduate students:

> 'Who benefits from having this information published and disseminated?' 'Whose voices are not represented in this research?' and 'What 'counts' as knowledge in this discipline?' ([Simmons 2005](#simm): 308)

Information and information seeking practices are not neutral and any teaching of information seeking, regardless of level, has, for example, to make visible how information relevance within different disciplines, organizations or communities is socially constructed.

The phenomenographic tradition, as applied in library and information science (e.g., [Bruce 1997](#bruce1997); [Limberg 1999](#limb1999)), provides another line of critique against information literacy literature oriented towards the individual and skills. In her relational model, Bruce illuminates seven conceptions of information literacy: she shows how information literacy education focuses primarily on only two of these conceptions: information sources and information technology ([Bruce 1997](#bruce1997): 172f). In a study of senior high school students' information seeking related to the same learning task, Limberg ([1999](#limb1999)) identifies three major ways of experiencing information seeking, as; fact-finding, choosing the right side, or analyzing and scrutinizing information. These three major categories are based on the patterns of variation of five aspects of information seeking, such as students' relevance criteria, their assessment of _enough_ and of the cognitive authority of sources, and their approaches to information overload as well as to bias in information. Limberg's findings underline the close relationship between information seeking and the content of the task thus emphasizing a need for taking content and context into account in the teaching of information seeking.

Tools for the mediation of information literacy, including, for instance, textbooks, Web-based tutorials, or classroom teaching, are not simply neutral instruments. Rather, they need to be seen as contingent, in the sense that they imply different and occasionally conflicting world-views ([Sundin & Johannisson 2005](#sund2005)). Drawing on an empirical study of academic libraries' Web-based tutorials for information literacy, Sundin ([in press](#sund)) identifies four main approaches to information literacy education and thus to information literacy. These are a source approach, a behavioural approach, a process approach and a communication approach.

A _source_ approach to information literacy mediation focuses on information sources and bibliographical tools. This approach corresponds with what has often been described as a system orientation to information seeking, one which takes its points of departure in the information system and not in the user. The system oriented approach has been the object of much criticism within information seeking research from the early 1980s until today (e.g., [Wilson 1981](#wils1981); [Kuhlthau 1991](#kuhlth1991)).

While the main teaching interests in the _behavioural_ approach are still bibliographical tools and information sources, the focus shifts towards the manner and the order in which to use these. This approach caters for a generalised structure for information seeking, which users can apply in varying situations, practices and contexts. The approach corresponds to the behavioural perspective in both library and information science and educational science, where the primary interest is in how to study people's actual, often measurable, behaviour. Both the source approach and the behavioural approach are, according to Kuhlthau ([2004](#kuhlth2004)), based on a 'bibliographic paradigm' characterized by structure and order.

In the _process_ approach the different aspects of information seeking are introduced from the perspective of the user. Hence, it is not actual behaviour that is treated but rather how users experience information seeking and create meaning. This approach corresponds to the user-oriented research direction that has been prevalent in information seeking research since the 1980s and to the many models of the information seeking process that have been presented since then (e.g., [Wilson 1981](#wils1981); [Kuhlthau 1991](#kuhlth1991)). The process approach focuses on the individual user and is based on a constructivist view of information seeking. Kuhlthau ([2004](#kuhlth2004): 89-105) proposes the 'uncertainty principle' as the theoretical basis for this constructivist view.

Finally, the _communication_ approach emphasizes the social and communicative aspects of information seeking practices. It challenges concepts of information seeking as a predominantly individual and generalisable process, which does not have to be adapted to and which is unaffected by its context. Hence, information seeking practices of various kinds are understood within the context in which they are carried out. The approach corresponds to an increasing awareness of the sociocultural aspects of information seeking, a perspective that has gradually become more apparent, not least at the ISIC conferences (e.g., [Alexandersson & Limberg 2003](#alex2003); [Sundin 2002](#sund2002)). The communication approach should also be related to the alternative viewpoint to information literacy as presented above.

The first three approaches in Sundin's ([in press](#sund)) study have similarities with the three approaches (a source approach, a pathfinder approach and a process approach) to user education presented by Carol Kuhlthau in an article from [1987](#kuhlth1987) (comp. [Tuckett & Stoffle 1984](#tuck)). However, while Kuhltau positions the pathfinder approach as domain dependent, Sundin's understanding of the behavioural approach views it as domain independent. Furthermore, the communication approach is not found in Kuhlthau's categorisation. We see Sundin's categorisation of four approaches to information literacy mediation as a further development of Kuhlthau's three approaches to user education.

## Phenomenography and variation

The IDOL Project is based on a phenomenographic research approach, where the research focuses on people's various ways of experiencing a certain phenomenon, in our case, teaching information seeking. The study was based on the assumption that the connection between teaching and learning lies in the object of learning. The view of information literacy adopted in the study is well aligned with the communication approach described above, in underlining that it varies with the context and situation in which information seeking is conducted. In the IDOL study we treated information seeking as the object of learning and information literacy as the goal of teaching and learning.

According to Pong and Morris ([2002](#pong): 16-17) the enacted object of learning, that is, the ways in which the intended object of learning is made available for students through the acts of teaching, will create the space within which learners are able to develop certain new understandings or capabilities expected of them. Pong and Morris further underline, '...that the 'real' object of learning must be jointly constituted in the interactions between teachers and students" in particular learning situations ([Pong and Morris 2002](#pong): 16). In the IDOL Project we adopted this view of the relation between the objects of teaching and learning. This means that teachers offer students possibilities (space) to understand the object of learning, information seeking, through treating this object in such ways that various dimensions become discernable at a certain point in time. This further means that for offering students space to understand various dimensions of information seeking, pedagogues (librarians and teachers) need to be aware of what it means to be information literate, or expressed differently, what it means to be able to apply information seeking practices in purposeful ways related to various tasks, situations and contexts.

The concept of experience (or conception) is central to phenomenography. An experience is a particular way of being aware of a specific phenomenon, for instance teaching. An experience is seen as a relation between the person and the specific phenomenon. In the study, we use the verbs _conceptualise_, _understand_, _experience_, _see_, _view_ (and corresponding nouns, e.g., conceptions) to denote our object of research, which is teachers' and librarians' experiences of teaching information seeking.

Phenomenography has previously been used in library and information science by researchers mainly from Sweden and Australia ([Limberg 2005](#limb2005)). The majority of these studies were conducted in contexts of formal education. Limberg ([1999](#limb1999)) and Lupton ([2004](#lupt)) studied students' ways of experiencing information seeking related to formal learning tasks in senior high school and undergraduate education respectively. Bruce ([1997](#bruce1997)) adopted a teaching perspective, investigating higher educators' conceptions of information literacy. Edwards ([2005](#edw)) explored undergraduate students' experiences of information literacy and developed software for information literacy education. Kirk ([2002](#kirk)) identified and described five different ways in which managers experience information use related to their work environment and occupational tasks. In all these studies the phenomenographic focus on variation instead of on the general or average allowed new dimensions of phenomena to appear.

## Research design

Similar to most phenomenographic studies the IDOL Project was designed as an interview study, with a focus on the teaching practices of the project participants. The view of teaching adopted in the project included the various components of goals, contents, methods, assessment and evaluation. Individual interviews were conducted with librarians and teachers from three schools on recurrent occasions over three years. Interviews took their points of departure in concrete assignments carried out with students during the period of the interviews. Interview questions focused on the contents and methods of teaching information seeking. Questions were also asked about the difficulties related to students' information seeking that the project participants identified as well as what criteria they used for assessing the quality of students' information seeking.

The selection of schools and participants was guided by a research interest in deriving rich variation in experiences. An explicit condition for participation in the study was an interest in the problems of teaching information seeking and that participants were prepared to reflect on and reconsider their teaching during the project period. Altogether thirteen teachers and five librarians in three schools participated. The teachers mainly taught social studies in grades 6 to 12 (12-19 year old students); one was a science teacher. In total, forty-five interviews were conducted. Each interview lasted between 45 and 60 minutes. The empirical material also encompassed examples of students' reports as well as the pedagogues' written instructions for student assignments including advice on information seeking.

Analyses of the interviews were directed at identifying and describing variation in experiences of teaching information seeking. Phenomenographic research findings are presented as categories of experiences of the phenomenon that is the object of research. It is important to observe that categories of conceptions are not tied to the different individual interviewees but are composed from the total interview material. This means that various conceptions can be expressed by the same individual.

## Synthesis of findings

In this paper, the presentation of findings will be concentrated to a comparison between experiences of teaching contents on the one hand, and experiences of criteria for the assessment of the quality of students' information seeking, on the other hand. The scope of this article does not allow a rich description of categories but a few examples will be given with excerpts from interviews. Sources of quotations are coded as T 1-13 (for teachers) and L 1-5 (for librarians).

### Experiences of teaching content

Experiences of teaching content focused on; specific sources, demonstrating tools, a recommended order between types of sources and the user's experience of the information seeking process. Advice about specific sources is a common feature when librarians and teachers describe what they teach about, as illustrated in the quotation below:

> Yes, I advise them about various databases and search engines. I was prepared for this and I knew where to find suitable information on the Vietnam War or whatever they asked for. When they ask me, I tell them where to look and how to search. (T1)

The idea of teaching students about a specific order to follow between types of sources during their information seeking is dominant in the material. A typical quotation runs as follows;

> I try to encourage them to start with an encyclopaedia for general orientation, so they understand what to do, to become familiarized with the topic, etc... then they must understand how to continue, to consult handbooks and to find books to check out... we look at books, CD-ROMs, maybe compare different versions. Next journals, help them log on to the Institute of Foreign Studies, for instance, to go to Mediearkivet. [A Swedish database of articles from regional and national newspapers] (L1)

The evaluation of sources was mainly experienced as something that students were unable to master; it was rarely mentioned as an object of teaching. The overall impression of the experienced teaching contents is that the pedagogues tended to focus on procedural matters, i.e. choosing the 'right' source, finding the 'right' answer or using a tool in the 'right way'. More abstract concepts linked to conscious relevance judgements, the critical evaluation of sources or the relationship between information seeking and various contexts tended not to emerge in our material.

### The quality of students' information seeking

The quality of students' information seeking was experienced as the ability to:

1.  read and understand the content of a text,
2.  write a correct bibliography,
3.  critically evaluate information,
4.  work independently, including producing a text of one's own, and
5.  synthesize information from various sources.

It is worth observing that all of these categories of experiences focus on _using_ information rather than seeking and finding.

The emphasis on reading is often talked about as a problem for students to understand various sources.

> Generally, students have difficulties in understanding and utilising the material, in really appropriating it, making it their own, creating real insight. It often happens that they copy, which is an indication that they don't understand. They use words that aren't their own. (L5)

It is considered a quality that students work independently, that is 'without help'.

> I know that he's much better at searching on his own in the library. But she needed help. I gave him some advice, but he never asked. He says 'yes, I have, I can cope'. (T8)

The matter of the critical evaluation of information is talked about as an important assessment criterion in the interviews, however, in practice, it is referred to in terms of deficiencies in students' reports.

Findings indicate an overall strong focus on a predefined content of subject matter, and on the analysis and synthesis of information, as illustrated in the quotation below;

> **Interviewer**: How do you assess the reports?  
> **Respondent**: Mainly on the basis of subject content, what is there, and then, to what extent they've managed to analyze the material, and their own comments and viewpoints, what they found out. They should draw conclusions, and... if they managed to use their textbook as a source. Those steps should be there in some way... and a list of references. (T9)

It is worth observing that neither information seeking nor the choice of sources are hardly mentioned in the assessment criteria.

Summarizing and comparing the findings we observe some interesting contradictions between experiences of teaching contents on the one hand and assessment criteria on the other hand. While project participants emphasized the importance for students to learn to critically evaluate their sources and to analyze and synthesize information from a variety of sources, these issues rarely appeared in participants' descriptions of teaching content.

## Discussion of IDOL findings

A comparison between the categories of experiences of teaching contents and the categories of experiences of assessment criteria indicates a striking lack of consistency (see Table 1). There is hardly any correlation between experiences of teaching content and conceptions of assessment criteria apart from the relationship between citing sources as teaching content and correct bibliography as an assessment criterion.

<table><caption>

**Table 1: Comparison between teaching contents and assessment criteria**</caption>

<tbody>

<tr>

<th>Teaching content</th>

<th>Assessment criteria</th>

</tr>

<tr>

<td>finding sources</td>

<td>reading, understanding and correctly presenting factual information</td>

</tr>

<tr>

<td>citing sources</td>

<td>correct bibliography or reference list</td>

</tr>

<tr>

<td>applying recommended order among types of sources</td>

<td>critical evaluation of sources</td>

</tr>

<tr>

<td>experience of search process</td>

<td>independent learning</td>

</tr>

<tr>

<td> </td>

<td>synthesizing information</td>

</tr>

</tbody>

</table>

The conditions for students' learning are shaped by a combination of the teaching content and teachers' assessment criteria ([Ramsden 1992](#ramsden); [Pong & Morris 2002](#pong)). We find it remarkable that the predominant teaching content does not appear to be included in the assessment of the quality of students' information seeking. Likewise, it is surprising that the critical features of assessment criteria do not appear as expressed objects of teaching. The consequence may be that students are actually not offered the appropriate opportunities for learning information seeking through education in school.

Experiences of teaching content according to these findings indicate teaching practices that are highly restricted when compared with models and theories of information seeking. This underpins our claim that information seeking research and information literacy education do not influence each other in the way that they have potential to do. We argue that information literacy education would benefit from using concepts and models from information seeking research in order to offer students the space to discern various dimensions of information seeking, which, in the IDOL findings, tended to be invisible. Examples of such dimensions are the awareness of how relevance criteria are negotiated in different contexts, the critical evaluation of sources, coping with information overload, the awareness that the relevance of information is not permanent but dependent on the context, situation, and task at hand. The experiences of assessment criteria in Table 1 also underline the need to teach how to use information sources through analysis and synthesis.

The issue of consistency between teaching contents and assessment criteria concerns the didactic intention of the pedagogues, i.e. the relationship between the object of learning, the object of teaching and what constitutes and characterises this object ([Limberg & Folkesson 2006](#limb2006)). The lack of consistency found in our study calls for further analysis of appropriate contents and ways of mediating information literacy. We suggest that for such considerations a theoretical understanding of different approaches to the mediation of information literacy will be beneficial.

## Theoretical approaches and teaching experiences

An analysis of the findings of the IDOL study through application of the four approaches to the mediation of information literacy introduced above sheds light on the relationship between information seeking and information literacy. The overview illuminates various conceptual and theoretical implications of the experiences of teaching.

<table><caption>

**Table 2: Overview of conceptual and theoretical implications**</caption>

<tbody>

<tr>

<th>Experience of teaching content (IDOL findings)</th>

<th>Approach to mediation</th>

<th>Theoretical perspective</th>

<th>Relation to context</th>

</tr>

<tr>

<td>Focus on specific sources and citing sources, tools for information seeking</td>

<td>Source approach</td>

<td>System oriented, bibliographic paradigm</td>

<td>Context dependent</td>

</tr>

<tr>

<td>Applying recommended order between types of sources, combining search terms</td>

<td>Behavioural approach</td>

<td>System oriented, bibliographic paradigm, behaviouristic</td>

<td>Context independent</td>

</tr>

<tr>

<td>User's experience of the information seeking process</td>

<td>Process approach</td>

<td>User oriented, uncertainty paradigm, individual constructivist</td>

<td>Context independent</td>

</tr>

<tr>

<td>  -  
(not present)</td>

<td>Communication approach</td>

<td>Sociocultural perspective, constructionism</td>

<td>Context dependent</td>

</tr>

</tbody>

</table>

The overview presented in Table 2 indicates a number of ways in which theories from information seeking research may be relevant for the mediation of information literacy. We want to particularly stress three areas: the need for a wider repertoire of various dimensions of information seeking in education; the relation between information seeking, content and context; and the particular conditions of the context of education for information seeking.

### A wider repertoire of understanding information seeking

As pointed out above, the IDOL findings indicate that the contents of teaching information seeking are highly restricted when compared to models and theories of information seeking. It seems that the strong focus on a source approach would benefit from being inspired by the shift in research from a system oriented to a user oriented approach, which occurred more than two decades ago. Abundant findings and numerous models from information seeking research might serve as a knowledge base to inform the practices of teaching information seeking (see e.g., [Wilson 1999](#wils1999)). The basic idea would be to utilise the research findings evidencing the importance of understanding user needs, experiences, situations and contexts and to supplement the prevalent source approach and system perspective with alternate understandings. Such a shift of perspectives would also be in alignment with the shift in education from teacher centred to student centred learning.

Moreover, a number of aspects of information seeking practices which have drawn a lot of attention in seeking research, such as the evaluation of information sources through negotiating relevance in different contexts, assessing the credibility and authority of sources and coping with information overload might be observed as aspects of the object of learning in the teaching of information seeking. Our findings indicate that such aspects rarely appear as teaching content. The gap between teaching content and assessment criteria identified in the IDOL findings calls for a reconsideration of what it means to be information literate, i.e. what it means to master information seeking practices in purposeful ways for various tasks, situations and contexts. This underlines our wish to see closer interaction between the fields of information seeking research and information literacy.

### Information seeking, content and context

For information seeking in contexts of education the content of information is of vital importance. The objectives of information seeking in contexts of education are to learn something. The object of learning may concern the topic of an assignment or, as in the research discussed in this article, the practice of information seeking. Limberg ([1999](#limb1999)) found a close interaction between students' understanding of the topic of their task and their approaches to information seeking. Based on our previous research we suggest that the role of the content of information, as understood by users, might be observed in teaching information seeking. This means that a user-oriented and context-dependent approach would be increasingly adopted in teaching. What is at stake for the practices of teaching information seeking is the idea of more elaborated views of information seeking as the object of learning, as regards both the essence and the variation of information seeking; that is, both what is specific in a particular situation and what is generic for information literacy across contexts and situations. The communication approach presented above, might help to clarify such critical features of variation and essence in information seeking.

### Conditions of the context of education for information seeking

The problems inherent in de-contextualising the object of learning are common to all educational settings. A pedagogue's ability to facilitate students' learning greatly influences the enacted object of learning. Thus, the space created by pedagogues would benefit from being shaped in a way that enables a re-contextualisation of the object of learning, i.e., information seeking, in varying contexts and situations, as well as helping students to discern the critical features of information seeking practices related to varying situations. The phenomenographic focus on capturing and describing variation combined with a communication approach based on a sociocultural perspective of learning offer particular possibilities for learning through collaborative interaction in class, among students and between students and pedagogues. Together they may create favourable conditions for learning, if utilized for enacting the object of learning. Developing an awareness of various understandings of information seeking practices and its interwoven artefacts related to different communities of learning might be one focus in such mediation.

## Concluding remark

With our claim for closer interaction between information seeking research and the mediation of information literacy, we identify a parallel to previous claims from the information seeking research community that findings from its research are not being observed or applied for developing more user-friendly interfaces or search tools. In similar ways, our findings indicate that the potential of theories and models from information seeking research are not observed or utilized for the development of typical user-oriented services such as user education. Nevertheless, we hope that with this paper, we have drawn attention to the potential embedded in information seeking research for the mediation of information literacy as well as the idea that information literacy practices provide important contexts for information seeking research.

## Acknowledgements

The research reported in this paper was conducted within the framework of the Swedish national research programme LearnIT, funded by the Swedish Knowledge Foundation. The authors wish to thank the anonymous referees for valuable comments.

## Note

<a id="note1"></a>1\. The IDOL project was conducted by Louise Limberg and Lena Folkesson ([Limberg & Folkesson 2006](#limb2006)) and was funded by the Swedish National Agency for School Improvement 2001-2004.

## References

*   <a id="alex2003"></a>Alexandersson, M. & Limberg, L. (2003). Constructing meaning through information artefacts. _The New Review of Information Behaviour Research_, **4**, 17-30,
*   <a id="alex2005"></a>Alexandersson, M. & Limberg, L. (2005). _[In the shade of the knowledge society and the importance of information literacy.](in_the_shade.html)_ Paper presented at the 11th Earli (European Association of Research on Learning and Instruction) conference, 23-27 August, Nicosia, Cyprus.
*   <a id="bruce1997"></a>Bruce, C. (1997). _The seven faces of information literacy_.Adelaide: Auslib Press.
*   <a id="bruce2000"></a>Bruce, C. (2000). Information literacy research: dimensions of the emerging collective consciousness. _Australian Academic and Research Libraries_, **31**(2), 91-109.
*   <a id="edw"></a>Edwards, S.L. (2005). _Panning for gold: influencing the experience of Web-based information searching_. Unpublished doctoral dissertation, Queensland University of Technology. Brisbane, Queensland, Australia
*   <a id="kirk"></a>Kirk, J. (2002). _Theorising information use: managers and their work_. Unpublished doctoral dissertation, University of Technology Sydney, Sydney, New South Wales, Australia
*   <a id="kuhlth1987"></a>Kuhlthau, C.C. (1987). An emerging theory of library instruction. _School Library Media Quarterly_, **16**(1), 23-28.
*   <a id="kuhlth1991"></a>Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science (JASIS)_, **42**(5), 361-371.
*   <a id="kuhlth2004"></a>Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services_. (2nd. ed.). Westport, CT: Libraries Unlimited.
*   <a id="kuhlth2001"></a>Kuhlthau, C.C. & McNally, M.J. (2001). Information seeking for learning: a study of librarians' perceptions of learning in school libraries. _The New Review of Information Behaviour Research_, **2**, 167-177.
*   <a id="limb1999"></a>Limberg, L. (1999). Three conceptions of information seeking and use. In T. D. Wilson & D. K. Allen (Eds.), _Exploring the contexts of information behaviour_ (pp. 116-135). London: Taylor Graham.
*   <a id="limb2005"></a>Limberg, L. (2005). Phenomenography. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of Information Behavior_ (pp. 280-283). Medford, NJ: Information Today, Inc.
*   <a id="limb2006"></a>Limberg, L. & Folkesson, L. (2006). _Undervisning i informationssökning: slutrapport från projektet Informationssökning, didaktik och lärande (IDOL)_ . [Teaching information seeking. Final report from the project Information seeking, didactics and learning.] Borås, Sweden: Valfrid.
*   <a id="lupt"></a>Lupton, M. (2004). _The learning connection: information literacy and the student experience_. Adelaide: Auslib Press.
*   <a id="mart"></a>Marton, F. & Booth, S. (1997). _Learning and awareness_. Mahwah, NJ: Lawrence Erlbaum.
*   <a id="moore"></a>Moore, P. (2000). Learning together: staff development for information literacy education. In C. Bruce & P. Candy (Eds.), _Information literacy around the world: advances in programs and research_ (pp. 258-270). Wagga Wagga, Australia: Charles Sturt University, Centre for Information Studies
*   <a id="pong"></a>Pong, W.Y. & Morris, P. (2002). Accounting for differences in achievement. In F. Marton & P. Morris (Eds.), _What matters? Discovering critical conditions of classroom learning_ (pp. 9-18). Gothenburg, Sweden: Acta Universitatis Gothoburgensis.
*   <a id="ramsden"></a>Ramsden, P. (1992). _Learning to teach in higher education_. London: Routledge.
*   <a id="simm"></a>Simmons, M.H. (2005). Librarians as disciplinary discourse mediators: using genre theory to move toward critical information literacy. _Portal: Libraries and the Academy_, **5**(3), 297-311.
*   <a id="sund"></a>Sundin, O. (In press). Negotiations on information seeking expertise: a study of Web-based tutorials for information literacy. _Journal of Documentation_.
*   <a id="sund2002"></a>Sundin, O. (2002). Nurses' information seeking and use as participation in occupational communities. _The New Review of Information Behaviour Research_, **3**, 187-202\.
*   <a id="sund2005"></a>Sundin, O. & Johannisson, J. (2005). Pragmatism, neo-pragmatism and sociocultural theory: communicative participation as a perspective in LIS. _Journal of Documentation_, **61**(1), 23-43.
*   <a id="talja"></a>Talja, S. (2005). The social and discursive construction of computing skills. _Journal of the American Society for Information Science and Technology_, **56**(1), 13-22.
*   <a id="tuck"></a>Tucket, H.W. & Stoffle, C.J (1984). Learning theory and the self-reliant library user. _Reference Quartely_, **24**(1), 58-66.
*   <a id="tuom"></a>Tuominen, K., Savolainen, R. & Talja, S. (2005). Information literacy as a socio-technical practice. _Library Quarterly_, **75**(3), 329-345.
*   <a id="Webb"></a>Webber, S. & Johnston, B. (2000). Conceptions of information literacy: new perspectives and implications. _Journal of Information Science_, **26**(6), 381-397.
*   <a id="wils1981"></a>Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation_, **37**(1), 3-15.
*   <a id="wils1999"></a>Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270.