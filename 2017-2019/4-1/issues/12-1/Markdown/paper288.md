#### Vol. 12 No. 1, October 2006

* * *

# Information use and secondary school students: a model for understanding plagiarism

#### [Kirsty Williamson](mailto:kirsty.williamson@infotech.monash.edu.au)  
Director, Information and Telecommunications Needs Research, School of Information Studies, Charles Sturt University, Wagga Wagga, Australia and Monash University, Victoria, Australia  

[Joy McGregor](mailto:JoyH.McGregor)  
Director, Centre for Studies in Teacher Librarianship, School of Information Studies, Charles Sturt University, Wagga Wagga, Australia

#### Abstract

> **Introduction**: The paper describes an interim model for understanding the influences on information use in relation to plagiarism, with a focus on secondary school students. Available literature mostly focuses on the tertiary level and on quantifying the extent of plagiarism, with limited availability of theory or empirical research focussing on information use, learning and plagiarism.  
> **Theoretical context:** Possible theoretical bases for the model are considered, and the reasons for choosing Williamson's ([2005](#william05)) modified ecological model, as the basis, are outlined.  
> **Empirical Research:** The data from a pilot study, using ethnographic techniques in a constructivist framework, contributed to the development of the interim model. The study was undertaken with Year 11 students in an Australian country high school. The data analysis from this study was influenced by constructivist grounded theory. Themes and categories were developed from this analysis.  
> **Model Development:** The themes and categories, together with the gaps in understanding as revealed by the pilot research experience, were used to modify Williamson's ecological model to provide a diagrammatic representation suited to the topic of information use and plagiarism. The themes, encompassing a number of categories which might provide understanding about influences on information use in relation to plagiarism, are people, practices, attitudes and technology.  
> **Conclusion:** Developing the model gave the researchers new insights at a crucial stage when they were about to embark on a major study, building on their pilot project. Although the target group for the research is secondary school students, the model is applicable to any group of information users.

## Introduction

Plagiarism has become a major issue in education worldwide with literature on the issue, particularly at the tertiary education level, now being prolific (e.g., [Marsden _et al._ 2004](#marsden)). While the problem is not confined to copying from the Internet, its significant contribution to the problem is particularly emphasised (e.g., [McCabe 2004](#mccabe)). As far back as 1998, Todd ([1998](#todd): 28) showed the extent to which the electronic environment played a part in the problem of plagiarism at secondary school level.

As Carroll ([2002](#carroll)) has shown, in the past many studies of plagiarism have tended to focus on quantifying the extent of plagiarism, primarily at the tertiary level. For example, in a survey of cheating in the UK by Newstead, _et al._ ([1996](#newstead)), the 947 student responses showed that more students admitted to paraphrasing from a source without acknowledging than to direct copying (54% compared with 42%). Other surveys (e.g. [Bull and Collins 2001](#bull); [Culwin _et al._ 2001](#culwin)) have substantiated the claim that plagiarism is a significant problem at tertiary levels. Perceptions of plagiarism were investigated by Ashworth and Bannister ([1997](#ashworth)), finding through nineteen tertiary student interviews that students were very unclear as to exactly what constituted plagiarism and were concerned about accidental plagiarism. Stark _et al._ ([2005](#stark)) explored unintentional plagiarism through two experimental studies of undergraduate students, in which they found that substantial amounts of unconscious plagiarism existed, even when participants were offered a financial incentive to not plagiarise.

There is little research at any educational level that has explored what influences students to plagiarise or has examined plagiarism in the broader context of information use. With regard to the former, Cox _et al._ ([2001](#cox)) found that students and staff in their survey, identified _time management_ and _needing help_ as two of the main reasons for copying or paraphrasing without citing, both of which were seen as serious forms of cheating. Carroll ([2002](#carroll): 19) includes a table of reasons tertiary students have given for plagiarising based the findings of an electronic plagiarism detection project ([Chester 2001](#chester)). They include lack of time, the need to succeed, the difficulty of the task, and lack of intrinsic interest in the topic. Since there appears to be no research at the secondary level, understanding what influences students, before they reach tertiary education, deserves attention.

Although a number of researchers have contributed research about information use by students (e.g., [Kuhlthau 1993](#kuhlthau); [Limberg 1999](#limberg)), they have not directly considered the relationship to plagiarism. One exception to this is McGregor ([1993](#mcg93)) and McGregor and Streitenberger ([1998](#mcgandSt98)) who examined information use and cognitive processes of high school students in two qualitative studies. They noted an apparent relationship between students' thinking and the degree of process and product orientation of that thinking. Plagiarism was a major strategy for using information by students who exhibited mainly a product orientation, ie, were principally concerned with the results of their work rather than the processes of making sense, seeking meaning, learning, thinking, or doing research. Students who were aware of the processes, on the other hand, tended to manipulate and interpret their information and not to use plagiarism as a strategy. Students who were primarily oriented towards product exhibited less complex thinking behaviours than did those who also focused on processes.

The earlier McGregor research thus identified a possible major influence on information use in relation to plagiarism. Considering that clarification was required, the present researchers set out to explore further this relationship between the orientation of secondary school students' thinking and the way in which they used the information they located, including the degree of plagiarism. They also wanted to investigate the other influences that might be significant.For example, what might be the effect of student attitudes, such as their level of engagement and interest in their topic?

Using a sample of secondary school students from an Australian country high school, the researchers began their exploration of influences on information use and plagiarism in a pilot project ([McGregor and Williamson 2005](#mcgandWill05)) designed to underpin an application for Australian Research Council funding (The Australian Research Council is the major funding body for Australian universities). This study explored the level of engagement and the degree of learning by the students as well as the idea that a relationship exists between level of engagement and/or the degree of learning and the level of plagiarism exhibited by students. Interviews with students provided rich data for considering the range of influences on strategies for using information, including plagiarism.

When the researchers found that their ARC application had been successful, they thought it timely to develop an interim model of the influences on information use in relation to plagiarism before embarking on the major research. They decided to base this on the findings of the pilot study but also to attempt to point to the gaps in understanding as revealed by the pilot research experience.

From this point the paper describes the genesis of the model, followed by a description of the philosophical framework and method used for the pilot study, and then the findings that have contributed to the interim model. The final sections provide a discussion of the model and a conclusion.

## Genesis of the Model

Recently there has been considerable discussion of the role of theory in research on human information behaviour. _Theories of Information Behavior: A Researcher's Guide_ ([Fisher, Erdelez & McKechnie 2005](#fisher)), a book designed to document metatheories, theories, and models of information behaviour, indicates the commitment to conceptual research in the field. As the introduction to the book states: "Conceptual work is the greatest and most constant challenge for many researchers" (xix).

With regard to information use, related to plagiarism, some theory has already been developed. As a result of her research findings, McGregor ([1993](#mcg93), [1995](#mcg95)) developed theory about thinking during information use and incorporated this into a model. One of the elements in this model is the influence of the product/process orientation on the complexity of skill usage in solving an information problem. The relationship to plagiarism, further explored in the pilot study ([McGregor and Williamson 2005](#mcgandWill05)), has been described above.

Given that the present researchers wanted to extend their thinking to explore a broad range of influences on information use in relation to plagiarism, they looked for other models that might assist. For her doctoral study, which examined the information-seeking behaviour of older people in relation to everyday life information, Williamson ([1995](#william95)) used and adapted ecological theory discovered in the human development field ([Bronfenbrenner 1977](#bronfen)). Later ([1998](#william98)) she developed a model to present that theory graphically. A key influence for her ecological framework was Hummert, Nussbaum and Wiemann ([1992](#hummert)), who argued that research about people must be grounded in a view of nature as _personal existence_, meaning that human beings should not be conceptualised exclusively as either individual entities or socially constructed entities. Rather, they should be seen as self-creating, but within contexts that involve various kinds of biological and social circumstances and constraints (418-419).

In developing her ecological theory, Williamson ([1995](#william95),[1998](#william98)) considered other potentially useful theories of information-seeking behaviour, drawing relevance from them as applicable. From Dervin came the emphasis on the need to understand information needs and uses in particular situations ([Dervin _et al._ 1976](#dervinZ)) and to bring the user into key focus ([Dervin and Nilan 1986](#dervinn)). Wilson ([1981](#wilson81)) also developed useful theory about the effect of context on the major human needs (physiological, cognitive and affective) which he saw as affecting information-seeking behaviour (later updated and extended in Wilson ([2000](#wilson00)).

Based on an extensive body of her research from 1998, Williamson ([2005](#william05)) broadened and modified her ecological model to study information-related behaviour beyond the everyday life area. Her view is similar to that of Bates ([2002](#bates): 13) who postulated that the scientific, the cognitive, and the socially constructed metatheories all have value and a possible continuing role. She has particularly used both social constructionist theory ([Berger and Luckman 1967](#berger)) and personal constructivist theory ([Kelly 1963](#kelly)) to capture both shared and individual meanings - the consensus and the dissonance - about information seeking and use. In her contribution to the 'theories' book ([Fisher, Erdelez and McKechnie 2005](#fisher)) the major example was based on a project focussing on information for people with breast cancer, first reported at an earlier ISIC Conference ([Williamson and Manaszewicz 2002](#williamandm02)).

The researchers decided that Williamson's ([2005](#william05)) modified model provides a useful basis for the development of their interim model of influences on information use of secondary students in relation to plagiarism. The influences in question are contextual and the philosophical underpinnings of the pilot study research are constructivist. The research philosophy and method of the pilot study - from which the data for the interim model emerged - are described in the next section.

## Research philosophy and method

As will be the case with the major research beginning in 2006, constructivist philosophy underpinned the pilot study, both in terms of approaches to learning and to research about learning through information use. As described above, the constructivist approach is particularly appropriate for research where ecological theory, focusing on individual and social contexts, will also be applied. The constructivist approach has also been supported by leading educators who posit that constructivist principles (i.e., that learners are active constructors of knowledge) should underpin teaching and learning (see, e.g., [Bruner 1973](#bruner); [Jonassen 1999](#jona).) Because each student builds on different previous learning and experience, each student's learning is unique. In the research arena, the constructivist paradigm assumes a relativist ontology (multiple realities), a subjectivist epistemology (investigator and respondent co-create understanding), and a naturalistic (in the natural world) set of methodological procedures ([Denzin and Lincoln 2005](#denzin): 24). Constructivists believe that there is no single objective reality 'out there'; instead there are multiple constructed realities about the phenomenon under study.

The pilot study was guided by constructivist paradigms, both personal constructivist and social constructionist. The former is explained by a range of theorists who have postulated that individual reality is determined by each person's perceptions of what is real, and the notion that the 'meanings' that each person makes may differ from those of others ([Kelly 1963](#kelly); [Lincoln and Guba 1985](#lincoln); [Hammersley 1995](#hammersley)). Social construct theory, which places emphasis on the ways people develop meanings together, emerges from philosophical roots similar to those of personal construct theory. Well-known proponents of social construct theory are Berger and Luckmann ([1967](#berger)) who argue that meaning is developed through the interactions and social processes involving people, language and religion. As Schwandt states: 'We do not construct our interpretations in isolation but against a backdrop of shared understandings, practices, language, and so forth'. ([Schwandt 2000](#schwandt): 197)

The above philosophical framework articulated into ethnographic method, often used by constructivists, enabling the researchers to understand students' constructions regarding information use and plagiarism, as well as their skills in writing to avoid it. Ethnography is most closely linked with participant observation. For example, Saule describes ethnographers as 'studying people in their everyday contexts' ([Saule 2002](#Saule): 179) and Minichiello _et al._ describe participant observation as 'studying people by participating in social interactions with them in order to observe and understand them' ([Minichiello _et al._ 1990](#Minich):18). Modern ethnography or participant observation uses a range of techniques such as interviewing, focus groups, observation and questionnaires and 'also has the flexibility to emphasize some techniques over others, and to leave some techniques out altogether' ([Bow 2002](#bow): 267).

### Research aims and questions

The aims of the pilot research were:

*   To investigate students' understanding of plagiarism and their ability to recognise it when it occurs in the work of others.
*   To examine the influences on their use of information, including the extent to which plagiarism occurs in their own work.
*   To continue to explore whether students' level of engagement with their work and interest in their topic are significant factors in whether or not students plagiarise, as indicated in earlier studies.
*   To continue to explore whether students who plagiarise, compared with those who do not, focus more on the product, e.g., their written assignment, rather than processes such as learning and making sense as also indicated in earlier studies).

The research questions which operationalised these aims were:

*   What do secondary students know about appropriate information use?
*   Do they understand plagiarism and are they able to recognise it when it occurs?
*   Does their understanding match their information use behavior?
*   Do students who do not plagiarise have a high level of engagement with, and interest in, their topic?
*   Do students who plagiarise, compared with those who do not, focus more on the product, e.g., their written assignment, rather than processes such as learning and making sense?
*   What other influences on information use, including the extent of plagiarism, can be discerned?

### Sample and data collection

The sample consisted of seventeen students, from a NSW country high school, where the key university involved in the project is situated. The students needed to come from one class as the researchers wanted to follow the complete process of their information use for one assignment, starting with the introduction of the assignment by the teacher. The class chosen was studying Year 11 Ancient History and undertaking an assignment on archaeology. The researchers considered any secondary year level to be appropriate and the assignment, involving extensive use of information sources, was considered suitable. All students in the class who returned their student and parent 'informed consent' forms were included in the project.

Data collection took place from February to May, 2004, using the techniques of observation, research logs, one written question, interviews and document examination. The observation took place during each of the periods when the students were working on the project. Researchers took the opportunity to observe the sources being used and ask students questions about what they were doing. Students recorded their progress in research logs throughout the project. At the end of the data collection period when it was too late in the school term to schedule further interviews, it was found that responses to a single question were needed to confirm an emerging assertion that students were not sure what it means to 'acknowledge sources'. Students were therefore asked to respond to a written question about their understanding of that element.

In terms of the development of the model, the key technique is the _interviews_, although document examination (analysis of the _assignments_) is important in that it enabled the students' ideas about information use plagiarism, as they emerged in the interviews, to be compared with their actual practice (including the extent to which they plagiarised) as revealed in their final assignments. Fifteen of the students were interviewed four times; the other two only three times because they left school before the field work was completed. Interviews also took place with the teacher and the teacher librarian. All seventeen assignments were available for analysis.

_Interviews_ were guided by schedules of questions, with follow up prompts, for each of four interviews with students, as well as for the teacher and the teacher librarian. The student questions were based on the phases of Kuhlthau's Information Search Process (ISP) ([Kuhlthau 1993](#kuhlthau)). The first interview was introductory, principally to get to know the students, to talk about the task (the 'definition' stage of Kuhlthau's ISP), the process of choosing their topics (the 'selection' stage), and their attitudes to the assignment and the process they envisaged in undertaking it. The second discussed their progress and again discussed the process as they saw it. Most students were in the 'collection' phase of the ISP at this point. The third interview occurred when students had completed their assignments, focusing on what they believed they had learned in terms of skills and in finding and using information. Here we probed for the relative importance to them of their mark (which they knew at this stage), the product and the extent of their learning. The fourth and final interview took place a month later, following analysis of student papers, and again focused on what had been learned, especially in relation to finding and learning new information. This interview sought to determine students' understanding of plagiarism, bibliographies, and citing. Their ability to recognize plagiarism in the work of others was included at this stage.

### Data analysis

Although the data analysis does not constitute a 'grounded theory', it was influenced by the 'constructivist grounded theory' approach of Charmaz ([2003](#charmaz)). Charmaz says that, unlike the original grounded theory ([Glaser and Strauss 1967](#glaser)), constructivist grounded theory is not 'objectivist'. It 'recognises that the viewer creates the data and ensuing analysis through interaction with the viewed' and therefore the data do not provide a window on an objective reality ([Charmaz 2003](#charmaz): 273). Charmaz therefore recognises that researchers' backgrounds will influence their interpretations of the data. They cannot avoid being influenced by 'disciplinary emphases' and 'perceptual proclivities' ([Charmaz 2003](#charmaz): 259). This means that, although there is every effort made to be true to the views expressed by participants, there is acceptance that 'we shape the data collection and redirect our analysis as new issues emerge' ([Charmaz 2003](#charmaz): 271).

There were five steps to the initial data analysis of the _interviews_: (1) Data were transcribed. (2) The two researchers read through the data, making notes about the tentative themes (with definitions) which appeared to be emerging. (3) Passages of data were labelled with categories (or codes) and linked to one of the themes so that identically labelled or categorised data could be retrieved as needed. Further themes (broader than categories) were identified and defined as necessary. As with themes, categories were given a short title and a definition if needed. Initially categories were sometimes broad and sub-divided to be more precise as the analysis progressed. (4) Categories were conceptually organised, meaning that thought was given to the similarities, differences and relationships among the categories.(5) Final themes were developed in preparation for the writing up of the research findings. The themes and categories considered relevant to this paper are listed in [Table 1](#table1).

For analysis of _assignments_, each assignment was converted to electronic format and submitted to a plagiarism detection site. Results from this analysis were extended through use of the Google search engine to search for key phrases, resulting in locating Websites that the plagiarism detection site had not found, often because of student spelling errors or minor changes to wording. All books cited in the bibliographies were searched except for two that could not be located. The student papers were colour coded to show which words and phrases had been taken from which books and Websites. The amount of direct copying, whether plagiarized or appropriately cited, was quantified.

### Choosing the categories and themes relevant to the model

When the researchers decided to develop an interim model from the pilot study data, with the idea of exploring the concepts of the model further in their major study, they discussed the themes and categories that might be relevant, based on their familiarity with the data from the earlier analyses. Having made an initial list they found that, co-incidentally, several of their categories fitted into three of the themes that had been identified as part of a definition of an _information ecology_ by Nardi and O'Day ([1999](#nardi)). Although these authors were talking about an information ecology as a setting such as a library, the elements of their ecological system—people, practices, values and technologies—encompassed many of the categories already identified for the interim model, therefore providing confirmation for the findings. In the proposed model, the 'values' theme has been replaced with 'attitudes' and the 'technologies' theme has been changed to 'technology', given that the only technology relevant to our research is the Internet. The theme 'prior learning' has been added. Data were again searched to confirm the suggested categories and to identify other relevant ones within the nominated themes. The outcome of this search appears in the next section where it will be discussed in relation to the model.

## A model for understanding influences on information use and plagiarism

Having identified the themes and categories, a key step was to modify Williamson's ([1998](#william98)) ecological model to provide a diagrammatic representation suited to the topic of information use and plagiarism. Figure 1 presents this diagrammatic model. Since there are several categories that fit within each of the themes (surrounding the outer circle), it is possible only to include the themes in the diagram

<figure>

![Figure 1](../p288fig1.jpg)

<figcaption>

**Figure 1: A model for understanding influences on information use and plagiarism**</figcaption>

</figure>

The user (in this case a secondary-school student) is the centre of the model. The influences that bear upon student information use, epecially the level of plagiarism, come into the five themes shown beyond the outer circle: people, practices, attitudes, technology and prior learning. The broad groups of information sources through which these influences are filtered, are indicated in the other concentric circles.

Each of the themes, and the categories that fall within them, is discussed below. Table 1 provides examples of student quotations gleaned during the interviews, demonstrating information use within the categories identified. The way in which some categories might be related to levels of plagiarism may not yet be obvious, but the model aims to be broad and inclusive rather than parsimonious. Previous analysis shows relationships between some of the categories, such as engagement with their topics and attitudes toward processes, within the theme of "attitudes" ([McGregor and Williamson 2005](#mcgandWill05)), but much is yet to be learned.

<a id="table1"></a>

<table><caption>

**Table 1: Examples of themes, categories, and quotations**</caption>

<tbody>

<tr>

<th>Themes</th>

<th>Categories</th>

<th>Quotations (in italics) and examples</th>

</tr>

<tr>

<td rowspan="5">People</td>

<td>Teachers</td>

<td>

_He gave me a book and I got a lot of information from it, and he actually found the topic that I was doing cause I just didn't have a clue._</td>

</tr>

<tr>

<td>Teacher librarian</td>

<td>

_I asked her for some other key words about [topic]._</td>

</tr>

<tr>

<td>Family members</td>

<td>

_When I needed her_ [her mother], _she just read out bits that she thought were relevant and I just typed it down._</td>

</tr>

<tr>

<td>Peers</td>

<td>

_Yeah I think I talked about it with_ [another student] _and other people who were sitting with us and just said, "Does this sound right?" or something like that._</td>

</tr>

<tr>

<td>Others</td>

<td>

_I found it on the Website, and I e-mailed him_ [an archaeologist] _for some further information . . . he just sort of showed me some sites, that I could find information from, like resources and stuff._</td>

</tr>

<tr>

<td rowspan="3">Practices</td>

<td>Notetaking</td>

<td>

_Um, I'll probably get heaps of information and like just from books and photocopy and make notes and from the internet and get it all together and then I will go through it and highlight what I think I need to know and then I'll put it all together._</td>

</tr>

<tr>

<td>Use of time</td>

<td>

_I just took a long time to sort of decide on a topic then rushed it all in at the end._</td>

</tr>

<tr>

<td>Location</td>

<td>

_It was just a waste of time for me, and I can't work like in libraries and stuff with everyone there._</td>

</tr>

<tr>

<td rowspan="2">Technology</td>

<td>Copy and paste</td>

<td>

_Like before you could sort of get away with like copy and paste and that now you can't just do that; you've got to change it around into your own writing and that._</td>

</tr>

<tr>

<td>Download</td>

<td>

_think it_ [plagiarism] _means not acknowledging where you got the information from, especially if you copied it word for word, even though that's not really appropriate, or just completely downloading information from the Internet._</td>

</tr>

<tr>

<td rowspan="4">Attitudes</td>

<td>Toward processes</td>

<td>

_Making sense of the process of excavation, talking about how these things we've been talking about are actually used in practice rather than just in theory. Helps to understand them._</td>

</tr>

<tr>

<td>Engagement</td>

<td>

_Yeah it was interesting. It was good to learn about it._</td>

</tr>

<tr>

<td>Motivation</td>

<td>

_I just don't have enough motivation to get my assignments done quickly._</td>

</tr>

<tr>

<td>Affect</td>

<td>

_I am sort of getting used to it, but then I am sort of getting annoyed with it because whenever I want to find some information whatever I can't find, it just really ticks me off, cause it's just so hard._</td>

</tr>

<tr>

<td rowspan="7">Prior Learning</td>

<td>Information skills</td>

<td>

_When you got to find information you should try to use a lot of different sources, like the Internet, books and encyclopaedias and stuff._</td>

</tr>

<tr>

<td>Quoting</td>

<td>

_Dad explained to me like if you actually copy an idea, you are supposed to actually quote through the actual document itself._</td>

</tr>

<tr>

<td>Citing</td>

<td>[No example found in interviews. Lack of prior learning of appropriate methods of citing exhibited in student papers.]</td>

</tr>

<tr>

<td>Plagiarism</td>

<td>

_But this is the first assignment that I have been told, you can get caught for it_ [plagiarism].</td>

</tr>

<tr>

<td>Alternatives (e.g. synthesis)</td>

<td>

_wish I was a little bit better at interpreting like rewriting. I wish I was a bit better at it.rewording big words._</td>

</tr>

<tr>

<td>Bibliography writing</td>

<td>

_Just have never had to do a bibliography._</td>

</tr>

<tr>

<td>Topic</td>

<td>

_I'd heard of it once. Um, some documentary thing I think._</td>

</tr>

</tbody>

</table>

It was clear that _people_ had an effect on the way in which students used information. People that were mentioned by students were teachers, teacher librarians, family members and peers. One student mentioned contacting an expert in the field, which led us to considering a category of 'others' to encompass possibilities that did not emerge from this group of participants. The quotations above show some ways in which students received help from these people

Students revealed a number of _practices_ related to information use. The way they take (or do not take) notes may be related to their tendency to plagiarise, with some students highlighting on photocopied pages and others writing in bullet-point form. A number of students pointed out how their use of time affected their final product, with many students procrastinating and then rushing to meet a deadline. The effect of the place in which they worked (location), such as the library or their home, may have some bearing on how easy it is to plagiarise or the level of engagement.

In terms of use of _technology_, students suggested that copying and pasting was an easy thing to do. Some admitted to having done it themselves in the past, but claimed to be aware now that it was inappropriate. One student also suggested that wholesale downloading of information might be a strategy used by some people.

Student _attitudes_ were revealed in many of their comments. Attitude towards processes such as making sense of information, towards learning about the topics themselves (engagement), towards the assignment itself (their motivation or lack of same) were apparent in many cases. The place of their feelings and emotions (affect) as they worked were also demonstrated. These areas probably contain considerable overlap, as it may be hard to distinguish motivation from engagement, for example. The area of attitudes is the one in which some analysis has already been published ([McGregor and Williamson 2005](#mcgandwill05)).

The _prior learning,_ or lack of it, experienced by students may influence their tendency to plagiarise. What they already knew and did not know about finding and using information (information skills) was often revealed. Some students were aware of how to use quotations but others were not. All students seemed to have some understanding of what plagiarism is, but most were unable to recognise it. Some believed they did not know enough about how they could fulfil the requirement to use their own words, while others clearly grasped the concept of synthesis, evidenced in the analysis of their papers. Great variation in prior knowledge of the purpose of a bibliography emerged as did knowledge of their topics. Some students chose their topic because they had never heard of it while others selected based on already knowing something about it.

## Discussion

The interim model already provokes further questions and reveals some gaps that may be filled by a deeper understanding that will come with the next stage of research. The terms selected for the five themes are not carved in stone and could change. For example, it could be argued that the categories included within the "attitudes" theme are not necessarily attitudes. Is engagement an attitude or a state of being? While motivation can certainly be related to attitude, is it appropriate to call it an attitude? Is there a more inclusive term that could replace attitude? Are there other categories that are related to the 'attitudes' theme? Three examples of themes/categories from the literature that did not emerge in the interviews but might benefit from further examination are 'beliefs', 'values', and 'philosophical perspectives'. What do students believe about using other people's words? Do they value ownership of ideas? Where and how do ethics enter the picture?

When considered historically, ownership of ideas is a relatively new concept ([Lackie and D'Angelo-Long 2004](#lackie)) and one that is not necessarily subscribed to by the current generation of students. Popular culture may have an influence on their attitudes towards giving credit for use of the work of others. For example, the concept of 'sampling' used by disc jockeys, in which bits of music are selected and reprocessed to create a new whole without credit to the original work, is one that invites application to written work. Some musicians who insert recognisable segments into their music have been required to credit the original source and, on occasion, pay royalties, but the trend is one that creates new mind-sets regarding ownership of ideas in general ([Bowman 2004](#bowman): 9-10). 'Popular culture' could be considered as another theme in the model, although it did not emerge from the pilot study data.

The theme of 'prior learning' contains a category 'download'. The idea of downloading information from the Internet was mentioned by one student, but no one alluded to the myriad of Websites that give away or sell pre-written papers, perhaps because at this stage these students were unaware of them. The potential of prior knowledge of these sites is one that the researchers will need to be conscious of in the upcoming research.

The "prior learning" theme contains a category "citing" for which there were no examples found in the interview transcripts. The category is included because student papers clearly exhibited a lack of prior learning regarding how to acknowledge and cite other people's ideas appropriately, especially when not quoting. This concept will be explored further in the upcoming study.

## Conclusion

The benefits of simplicity in model building have already been demonstrated by Bates' ([2002](#bates): 4) 'Modes of Information Seeking' model. Our interim model is simple and also flexible. The themes and categories can be expanded as our thinking and empirical data suggest. Developing the model has already given the researchers new insights at a crucial stage when they are about to embark on a major study, building on their pilot project. They believe that the ARC-funded project will benefit greatly as a result. Although the target group for the research is secondary school students, the model is applicable to any group of information users.

As Nardi and O'Day ([1999](#nardi)) said, 'an ecology is complex'. However, at this stage it does not seem that the information and plagiarism research can determine the relationship between tools and people and their practices as Nardi and O'Day discussed. The first step is to try to understand as many as possible of the various influences on information use in relation to plagiarism at various levels. It is likely that still other categories and themes will emerge in the new project and through ongoing literature exploration. The researchers will be open to this possibility and expect to revise the interim model accordingly.

## References

*   <a id="ashworth"></a>Ashworth, P., Bannister, P. & Thorne, P. (1997). Guilty in whose eyes? University students' perceptions of cheating and plagiarism in academic. _Studies in Higher Education_, **22**(2), 187-204\.
*   <a id="bates"></a>Bates, M. (2002). Toward an integrated model of information seeking and searching. _The New Review of Information Behaviour Research_, **3**, 1-15.
*   <a id="berger"></a>Berger, P. L. & Luckmann, T. (1967). _The social construction of reality: a treatise in the sociology of knowledge_. New York, NY: Anchor Press.
*   <a id="bow"></a>Bow, A. (2002). Ethnographic techniques. In K. Williamson, _Research methods for students and professionals: information management and systems_, (2nd ed.) (pp. 265-280). Wagga Wagga, NSW: Centre for Information Studies, Charles Sturt University.
*   <a id="bowman"></a>Bowman, V. (2004). Teaching intellectual honesty in a tragically hip world: a pop-culture perspective. In V. Bowman (Ed.), _The plagiarism plague: a resource guide and CD-Rom tutorial for educators and librarians_ (pp. 3-10). New York, NY: Neal-Schuman Publishers.
*   <a id="bronfen"></a>Bronfenbrenner, U. (1977). _Towards an experimental ecology of human development_. London: Cambridge University Press.
*   <a id="bruner"></a>Bruner, J. (1973). _Beyond the information given: studies in the psychology of knowing_. New York, NY: W.W. Norton & Co.
*   <a id="bull"></a>Bull, J., Collins, C., Coughlin, E. & Sharpe D. (2001) [Technical review of plagiarism detection software report](http://www.Webcitation.org/5J98e3u27). Luton, UK: University of Luton. Retrieved 30 August 2006 from http://www.jisc.ac.uk/uploaded_documents/luton.pdf
*   <a id="carroll"></a>Carroll, J. (2002). _Handbook for deterring plagiarism in higher education_. Oxford: Oxford Brookes University.
*   <a id="charmaz"></a>Charmaz, K. (2003). Grounded theory: objectivist and constructivist methods. In N. K. Denzin & Y. S. Lincoln (Eds.), _Strategies of qualitative inquiry_ (2nd ed.) (pp. 249-291). Thousand Oaks, CA: Sage.
*   <a id="chester"></a>Chester, G. (2001). Plagiarism detection and prevention: final report of the JISC electronic plagiarism detection project. Cited in Carroll, J. (2002). _Handbook for deterring plagiarism in higher education_. Oxford: Oxford Brookes University, 19.
*   <a id="cox"></a>Cox, A., Currall, J & Connolly, S (2001). [_The human and organisational issues associated with network security_](http://www.Webcitation.org/5JANbotne). Bristol, UK: JISC Committee for Awareness, Liaison and Training. Retrieved 30 August, 2006 from www.jisc.ac.uk/uploaded_documents/ACF28B.rtf
*   <a id="culwin"></a>Culwin, F., MacLeod, A, & Lancaster, T. (2001). [_Source code plagiarism in UK HE Computing Schools_](http://www.Webcitation.org/5JANlwHty). Bristol, UK: Joint Information Systems Committee. (Technical Report SBU-CISM-01-01). Retrieved 30 August, 2006 from http://www.jisc.ac.uk/uploaded_documents/southbank.pdf
*   <a id="denzin"></a>Denzin, N.K. & Lincoln, Y.S. (2005). Introduction: the discipline and practice of qualitative research. In N.K. Denzin, & Y.S. Lincoln (Eds.), _Handbook of qualitative research_. (3rd ed.) (pp. 1-32). Thousand Oaks, CA: Sage.
*   <a id="dervinn"></a>Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33.
*   <a id="dervinz"></a>Dervin, B., Zweizig, D., Banister, M., Gabriel, M., Hall, E. P., Kwan, C., et al. (1976). _The development of strategies for dealing with the information needs of residents, Phase 1 Citizen study, Final report on project L0035JA, Grant No. OEG 0 74 7308 to the Office of Education._ Seattle, WA: School of Communication, University of Washington. (ERIC Document ED125640).
*   <a id="fisher"></a>Fisher, K.E., Erdelez, S. & McKechnie, E.F. (Eds.) (2005). _Theories of information behavior: a researcher's guide._ Medford, NJ: Information Today.
*   <a id="glaser"></a>Glaser, B. G. & Strauss, A. L. (1967). _The discovery of grounded theory_. Chicago, IL: Aldine.
*   <a id="hammersley"></a>Hammersley, M. (1995). _The politics of social research_. London: Sage.
*   <a id="hummert"></a>Hummert, M. L., Nussbaum, J. F. & Wiemann, J. M. (1992). Communication and the elderly: cognition, language and relationships. _Communication Research_, **19**(4), 413 422.
*   <a id="jona"></a>Jonassen, D.H. (1999) _Learning with technology: a constructivist perspective_. Upper Saddle River, NJ: Merrill.
*   <a id="kelly"></a>Kelly, G. (1963). _The psychology of personal constructs_. Vols. 1 & 2\. New York, NY: Norton.
*   <a id="kuhlthau"></a>Kuhlthau, C.C. (1993). _Seeking meaning: a process approach to library and information services_. Norwood, N.J.: Ablex Publishing Corporation.
*   <a id="lackie"></a>Lackie, R.J. & D'Angelo-Long, M. (2005). It's a small world?: cross-cultural perspectives and ESL considerations. In V. Bowman (Ed.), _The plagiarism plague: a resource guide and CD-Rom tutorial for educators and librarians_ (pp. 35-48). New York, NY: Neal-Schuman Publishers.
*   <a id="limberg"></a>Limberg, L. (1999). [Experiencing information seeking and learning: a study of the interactions between two phenomena]( http://informationr.net/ir/5-1/paper68.html). _Information Research_, **5**(1), paper 68\. Retrieved 3 January, 2006 from http://informationr.net/ir/5-1/paper68.html
*   <a id="lincoln"></a>Lincoln, Y. S. & Guba, E. G. (1985). _Naturalistic inquiry_. Newbury Park, CA: Sage.
*   <a id="marsden"></a>Marsden, H., Hicks, M. & Bundy, A. (2004). _Educational integrity: plagiarism and other perplexities. Proceedings of the first Australasian Educational Integrity Conference_. Adelaide, S.A.: University of Adelaide.
*   <a id="mccabe"></a>McCabe, D. (2004). Promoting academic integrity: a US/Canadian perspective. In Marsden, H., Hicks, M. & Bundy, A. (Eds.), _Educational integrity: plagiarism and other perplexities. Proceedings of the first Australasian Educational Integrity Conference_ (pp. 3-11). Adelaide, S.A.: University of Adelaide.
*   <a id="mcg93"></a>McGregor, J. H. (1993). _Cognitive processes and the use of information: a qualitative study of higher order thinking skills used in the research process by students in a gifted program._ (Doctoral dissertation, Florida State University, 1993). _Dissertation Abstracts International_, **54**, 2367\.
*   <a id="mcg95"></a>McGregor, J. H. (1995). Process or product: constructing or reproducing knowledge. _School Libraries Worldwide_, **1**(1), 28-40.
*   <a id="mcgandst98"></a>McGregor, J. H. & Streitenberger, D. (1998). [Do scribes learn? Copying and information use](http://www.Webcitation.org/5JAOMNC4w ). _School Library Media Quarterly Online_ **1**(1). Retrieved 3 January, 2006 from http://www.ala.org/ala/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume11998slmqo/mcgregor.htm
*   <a id="mcgandwill05"></a>McGregor, J. & Williamson, K. (2005). Appropriate use of information at the secondary school level: understanding and avoiding plagiarism. _Library and Information Science Research_, **27**(4), 496-512\.
*   <a id="minich"></a>Minichiello, V., Aroni, R., Timwell, E. & Alexander, L. (1990). _In-depth interviewing: researching people_. Melbourne, VIC: Longman Cheshire.
*   <a id="nardi"></a>Nardi, B.A. & O’Day, V.L. (1999). [Information ecologies: using technology with heart. Chapter four: information ecologies](http://www.firstmonday.org/issues/issue4_5/nardi_chapter4.html). _First Monday_, **4**(5). Retrieved 22 January, 2006 from http://www.firstmonday.org/issues/issue4_5/nardi_chapter4.html
*   <a id="newstead"></a>Newstead, S.E., Franklyn-Stokes, A. & Armstead, P. (1996). Individual differences in student cheating, _Journal of Educational Psychology_ **88**(2), 229-241.
*   <a id="saule"></a>Saule, S. (2002). Ethnography. In K. Williamson, _Research methods for students, academics and professionals: information management and systems_ (2nd ed.) (pp. 177-193). Wagga Wagga, NSW: Charles Sturt University, Centre for Information Studies.
*   <a id="schwandt"></a>Schwandt, T.A. (2000). Three epistemological stances for qualitative inquiry: interpretivism, hermaneutics, and the social constructionism. In N.K. Denzin, & Y.S. Lincoln (Eds.), _Handbook of qualitative research_. (2nd ed.) (pp. 189-213). Thousand Oaks, CA: Sage.
*   <a id="stark"></a>Stark, L-J, Perfect, T.J. & Newstead, S.E. (2005). When elaboration leads to appropriation: unconscious plagiarism in a creative task. _Memory_, **13**(6), 561-573.
*   <a id="todd"></a>Todd, R. (1998). WWW, critical literacies and learning outcomes. _Teacher Librarian_, **26**(2), 16-21.
*   <a id="william95"></a>Williamson, K. (1995). _Older adults: information, communication and telecommunications._ Unpublished doctoral dissertation. Royal Melbourne Institute of Technology, Monash, Australia.
*   <a id="william98"></a>Williamson, K. (1998). Discovered by chance: the role of incidental learning acquisition in an ecological model of information use. _Library & Information Science Research_, **20**(1), 23-40\.
*   <a id="william05"></a>Williamson, K. (2005). Ecological theory for the study of human information behaviour. In Fisher, K E., Erdelez, S. & McKechnie, E. F. (Eds.) _Theories of information behavior: a researcher's guide_. Medford, NJ: Information Today.
*   <a id="williamsandm02"></a>Williamson, K. & Manaszewicz, R. (2002). Breast cancer information needs and seeking: Towards an intelligent, user sensitive portal to breast cancer knowledge online. _New Review of Information Behaviour Research_, **3**, 203-219\.
*   <a id="wilson81"></a>Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation_, **37**(1), 3-15.
*   <a id="wilson00"></a>Wilson, T.D. (2000). [Recent trends in user studies: research and qualitative methods](http://informationr.net/ir/5-3/paper76.html). _Information Research_, **5**(3), paper 164\. Retrieved 24 January, 2006 from http://informationr.net/ir/5-3/paper76.html