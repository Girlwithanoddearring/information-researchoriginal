#### Vol. 12 No. 1, October 2006

* * *

# Iranian engineers' information needs and seeking habits: an agro-industry company experience

#### [Zahed Bigdeli](mailto:bigdeli_zahed@yahoo.co.in)  
Department of Library and Information Science,
School of Education and Psychology,
Shahid Chamran University, Ahvaz, Iran

#### Abstract

> **Introduction.** This descriptive research attempted to investigate the information seeking behaviour of engineers at Khuzestan Sugar-Cane and By-Product Company in Iran.  
> **Method.** To collect the data, a questionnaire was distributed using Likert-type questions with six points ranging from 'never use' to 'very useful'. Of the 250 questionnaires distributed, 158 (63.2%) were used in the data analyses. The reliability coefficient was measured by the Cronbach Alpha which was 0.81  
> **Analysis.** Analysis of variance and the Tukey test were used to test the sole hypothesis of the research and to see if engineers who worked in various sites were different in their information-seeking behaviour. To answer the research questions, descriptive statistics were employed.  
> **Results.** The results showed that the engineers in different work areas were significantly different in terms of information-seeking behaviour. The most important motivations for seeking information were: to develop their knowledge and expertise; to be able to use new job-related technologies and to be up-to-date in their specialty.  
> **Conclusions.** Distance appears to have impact on information use and information-seeking behaviour of engineers. Thus, librarians must consider the Zipf's 'principle of least effort' in serving their clients.

## Introduction

Information has become the tool for authority and an important factor for development. People depend on information in social, cultural, political and scientific and technological fields. Because of the rapid development in agricultural knowledge during the last century and its vital role in the survival of nations, it is essential for agricultural specialists to keep pace with the developments achieved elsewhere. Thus, it is essential to know the information needs and information-seeking behaviour of agricultural specialists who work for industries related to strategic crops and industries such as sugar-cane.

The plantation of sugar-cane in Iran dates back to antiquity. However, during the last five decades, great efforts have been made to develop its plantations and to employ the latest technologies. Nevertheless, sugar-cane is planted only in Khuzestan, a province located in southwest of the country, because of the rich natural resources (i.e., land, water and suitable weather conditions) found there.

Fifteen years ago, a new project started to develop the planting of sugar-cane in Khuzestan to achieve self-sufficiency for the country in terms of sugar. This huge project covers more than 84,000 hectares of land (about 325 square miles) and is a complex of seven factories.

The Company's library, The Centre for Sugar-Cane Scientific and Technical Information, hereafter the Information Centre, serves as the body responsible for meeting the information needs of employees.

This study attempted to identify the information needs and the information sources used by the engineers, the motivations and the information channels used for searching and obtaining information as well as the barriers they may encounter.

## Review of the literature

Part one of the literature review deals with the research conducted in countries other than Iran and the second part focuses on the studies carried out in Iran.

Adedigba ([1985](#ade85)) found that most Nigerian agricultural specialists and researchers used their institutions' special libraries only and that they mainly used special textbooks to meet their information needs. Bettiol ([1990](#bet90)) also found that the Brazilian agricultural biotechnologists often used special libraries of their institutions. Kaniki ([1991](#kan91)) reported the same results for agricultural engineers in Zambia. The research conducted by Izah ([1996](#iza96)) in Nigeria revealed that variation in searching for information, interaction with colleagues and search patterns affect information-seeking behaviour of agricultural engineers and other specialists.

Majid _et al._ ([1999](#maj99)) found that most Malaysian agricultural specialists referred to the library to use OPACs, scan journal articles and use information sources. In another study ([Majid _et al._ 2000](#maj00)), they claimed that Malaysian researchers spent sixteen percent of their time for reading relevant materials. Dulle ([2000](#dul00)) believes that Tanzanian agricultural specialists use personal library, professional meetings and periodicals as their major information sources.

In Iran, Dizaji's survey ([1995](#diz95)) showed that half of the experts and researchers of the Ministry of Agriculture either did not use the library, or spent one to two hours a week using library books. Whereas Hakimi ([1997](#hak97)) claimed that agricultural researchers frequently used consultation with experts; they also used books, research results, periodicals, databases and patents, respectively. However, their membership in professional associations was minimal. Noormohamadi ([1997](#noo97)) found a significant relationship between the academic degree and years of service with information-seeking behaviour of agricultural specialists and that specialists mostly used research results, books and periodicals.

Rasouli Azad ([2001](#aza01)) reported that the Iranian agricultural specialists' aim in searching information sources was to conduct research and to be up-to-date. More than fifty percent of respondents did not have access to computers, special databases and computer networks in the libraries they referred to. Journal articles and books, respectively, ranked as the first and second most used sources. Akrami ([2001](#akr01)) indicated that books, standards and technical catalogues were used more than other materials by the engineers at food industry factories in Mash'had metropolitan area. However, they were deprived of an active information center.

In sum, the literature showed that agricultural engineers and specialists mostly refer to their institutions' special libraries, use textbooks, journals, personal libraries, interact with colleagues and other experts. They also use standards, patents and technical reports, catalogues and plans to meet their information needs. They mostly use information sources to keep pace with developments in their fields and conduct research and generally encounter problems in searching for information.

## Methodology

A twenty-eight item questionnaire was used to collect the data. The questions asked about the respondents' age, sex, degree and the time they spend daily in reading scientific and technical materials. It also asked them about the information sources and channels, the motivations to use information and the problems they encounter in using information sources.

The questions were of a Likert-type with 6 points ranging from 'never use' to 'very highly use'. The values zero to five were assigned to the options, respectively. Of the 250 questionnaires distributed, 158 (63.2%) were used in data analysis. Reliability coefficient was measured by the Cronbach Alpha which was 0.81.

### Sampling and data analysis

A systematic random sampling method was employed to select 250 people among the Company's 750 engineers at the time of the study. Descriptive statistics, ANOVA and Tukey tests were used to introduce the sample, answer research questions and test the sole hypothesis:

> The information-seeking behaviour of the engineers in different sites is significantly different.

### Research questions

1.  What subjects do engineers mainly study?
2.  What types of information sources do engineers usually use?
3.  What channels do engineers usually use?
4.  How much access do engineers have to information sources?
5.  What are the aims and motivations of engineers for seeking information?
6.  What problems/barriers do engineers encounter in seeking information?

## Findings and results

ANOVA was used to see if engineers who worked in various sites differed in their information-seeking behaviour. The results showed that engineers _did_ significantly differ (F=3.456, df=7, P<0.01) where alpha equals 0.01\. The Tukey test revealed that engineers in group one (who worked in the main office and close to the Information Center) were significantly different from those in groups three, five and six who worked, respectively, in sites 75, 65, an 45 kilometers away from the Information Center. Thus, we may conclude that the engineers who worked close to the Information Center had enough time to use the library and had access to the information sources, while those far from the library did not have enough time to do so. The results also showed that 84.2% of engineers studied in their subject fields and 10.1% of them studied _little_ or _very little_, 41.1% studied _moderately_ and 32.9% studied _much_ or _very much_.

Engineers also studied a wide range of subjects; for example, agriculture (29.7%0), accounting (4.4%), civil engineering and electrical engineering (each 3.8%), chemistry and management (each 2.5%), etc.

<table><caption>

**Table 1-Frequency and relative frequency of different types of information sources used (n=158).**</caption>

<tbody>

<tr>

<th>Types of info. Sources</th>

<th>No. of users</th>

<th>Percent.</th>

</tr>

<tr>

<td>Persian books</td>

<td>94</td>

<td>59.5</td>

</tr>

<tr>

<td>Persian periodicals</td>

<td>82</td>

<td>51.9</td>

</tr>

<tr>

<td>English books</td>

<td>65</td>

<td>41.1</td>

</tr>

<tr>

<td>Technical Reports</td>

<td>65</td>

<td>41.1</td>

</tr>

<tr>

<td>Technical plans</td>

<td>59</td>

<td>37.3</td>

</tr>

<tr>

<td>English periodicals</td>

<td>58</td>

<td>36.7</td>

</tr>

<tr>

<td>Theses</td>

<td>54</td>

<td>34.2</td>

</tr>

<tr>

<td>CDs</td>

<td>44</td>

<td>27.8</td>

</tr>

<tr>

<td>Others</td>

<td>38</td>

<td>24.1</td>

</tr>

</tbody>

</table>

Table 1 shows that Persian books, Persian periodicals and English books respectively ranked first, second and third among the information sources used, followed by technical reports, technical plans, English periodicals, etc. Table 2 shows the highest percentage of the amount of access by engineers to different types of information sources.

<table><caption>

**Table 2\. The highest percentages of the amount of access to information sources**</caption>

<tbody>

<tr>

<th>Information source</th>

<th>Percentage of engineers who accessed</th>

<th>Level of access</th>

</tr>

<tr>

<td>Persian books</td>

<td>34.2</td>

<td>Moderate</td>

</tr>

<tr>

<td>Persian periodicals</td>

<td>20.9</td>

<td>Moderate</td>

</tr>

<tr>

<td>English periodicals</td>

<td>15.8</td>

<td>Very high</td>

</tr>

<tr>

<td>CDs</td>

<td>15.2</td>

<td>Very high</td>

</tr>

<tr>

<td>Technical reports</td>

<td>15.2</td>

<td>Moderate</td>

</tr>

<tr>

<td>Theses</td>

<td>13.9</td>

<td>Very high</td>

</tr>

<tr>

<td>English books</td>

<td>13.2</td>

<td>High</td>

</tr>

<tr>

<td>Technical plans</td>

<td>12</td>

<td>Moderate</td>

</tr>

</tbody>

</table>

Engineers primarily used _informal channels_ and then _formal channels_. Other libraries and information centres such as the Centre for Agricultural Information and Documents in Tehran followed the formal channels.

The engineers' most important motivations to search for information were to:

*   develop knowledge and expertise and use new technologies, each 41.8%
*   obtain up-to-date information, 40.5%
*   do research, 10.1%
*   solve problems in the work environment, 3.8%
*   write papers, 3.2%
*   teach, 0.6%

The problems and barriers encountered by the engineers are shown in Table 3.

<table><caption>

**Table 3-Frequency and percentage of problems and barriers encountered (n=158).**</caption>

<tbody>

<tr>

<th>Type of problems/barriers</th>

<th>No.</th>

<th>Percent.</th>

</tr>

<tr>

<td>Length of daily working hours</td>

<td>104</td>

<td>65.8</td>

</tr>

<tr>

<td>Distance between work place and the Information Center</td>

<td>98</td>

<td>62</td>

</tr>

<tr>

<td>Lack of time</td>

<td>92</td>

<td>58.2</td>

</tr>

<tr>

<td>Inadequate library facilities and equipment</td>

<td>31</td>

<td>19.6</td>

</tr>

<tr>

<td>Lack of skills in using new information technologies</td>

<td>16</td>

<td>10.1</td>

</tr>

<tr>

<td>Lack of proficiency in English</td>

<td>13</td>

<td>8.2</td>

</tr>

</tbody>

</table>

Employees are required to work overtime constantly. Thus, the length of daily working hours has been reported as the first and most important barrier against using the Information Centre.

## Discussion and conclusion

Reading special materials ranged from agriculture (29.7%) to mechanics (0.6%). It seems that the relatively high percentage of agricultural materials used is due to the priority given to agricultural activities involved in this project. However, 41.1% of respondents studied moderately. Among the information sources used, Persian books (59.5%) ranked the highest, while English periodicals (which presumably contain the most recent information) with (36.7%) stands somewhere near the bottom of the ladder of information sources used.

The inadequate use of English periodicals may be explained partly due to few numbers of English periodicals subscribed to by the Information Centre, partly because of the out-of-datedness of the periodicals available in the Information Centre and partly to difficulty in understanding English.

The importance of English periodicals in the advancement of research in this company has been neglected. Enough budgets are not allocated to subscribe of scientific foreign journals. While this part of the findings of the present study does not agree with Adedigba ([1985](#ade85)), it is in line with other findings in Iran which focused on books. Adedigba found that agricultural researchers in Nigeria mostly referred to special materials including journals, but Mokhtari Dizaji ([1995](#diz95)) reported that Iranian engineers gave priority to books, followed by periodicals, reports and documents and theses, respectively. Hakimi ([1997](#hak97)) also claimed that books were the first source of information for the Iranian agricultural specialists, while reports, projects, theses, as well as periodicals followed books.

However, Azad ([2001](#aza01)) reported periodical articles and books as the first source of information for Iranian specialists. He also reported that more than fifty percent of them did not have access to computers and could not use specialized databases or library networks.

In terms of information channels, engineers in the present study, first used _informal channels_ and then _formal ones_. Among the information channels used by the engineers under the study, consultation with friends and colleagues (72.8%) and participation in seminars and conferences (44.3%) were on top. This finding is in line with the findings of Noormohamadi ([1997](#noo97)) and Hakimi ([1997](#hak97)). Noormohamadi found that the most important channel for agricultural specialists in Iran was personal experience gained through participating in local and international meetings and Hakimi claimed that consultation with experts was the first channel used by the researchers at the Research Centre for Natural Resources (Iran).

Among _formal channels_, use of personal library (65.2%) and the Information Centre (55.1%), as well as other libraries (44.9%) were the most preferred information channels. However, Adedigba ([1985](#ade85)), Bettiol ([1990](#bet90)) and Kaniki ([1991](#kan91)), respectively, reported that the Nigerian agricultural specialists, the Brazilian agricultural biotechnologists and the Zambian agricultural specialists first referred to the special library of their institutions.

Nevertheless, Dulle ([2000](#dul00)), found that use of personal libraries, attendance at professional seminars and conferences, as well as periodicals were the three principal information sources used by the Tanzanian agricultural specialists. Thus, this part of the findings of the present study does not conform to the findings of Adedigba ([1985](#ade85)), Bettiol ([1990](#bet90)) and Kaniki ([1991](#kan91)), but it is in agreement with Dulle ([2000](#dul00)). The reason for this may be the working conditions of the Company and the location of the Information Centre. Long daily working hours, necessity to work overtime and the distance between the work sites and the Information Centre leave no time and opportunity for the engineers to use the Company's Information Centre. Hence, 82.3% of the respondents reported their access to information as _moderate_.

The most important motivation of the engineers for obtaining information was respectively: promoting their knowledge, up-to-dating special information and having access to the most recent research findings.

Respondents reported their most important barriers as follows: length of work hours (usually ten to twelve hours a day, six days a week), far distance between the work areas and the Information Centre and lack of time.

Majid _et al._ ([2000](#maj00)) reported poor collections and poor library services, Noormohamadi ([1997](#noo97)) reported lack of needed sources in the library and delayed information services, Hakimi ([1997](#hak97)) found lack of personal skills and lack of information about new information sources, inadequate library services and lack of information about the existence and place of information as the most important barriers. Moreover, Akrami ([2001](#akr01)) reported that lack of skills in information retrieval, lack of skills in using information technologies, lack of time and lack of access to library and information sources as main barriers for the Iranian managers and specialists.

This part of the findings of the present study partly confirms those of Akrami, but not others. The reason for this is that the libraries referred to in the research of others are in the workplace of the specialists, while the Company under the study has seven work sites located in remotely from each other and from the Information Centre with the nearest work site being twenty-five kilometers from the Information Centre. At the time of this study, three professional librarians with Master's degrees in library and information science worked in the Centre; perhaps for this reason, unlike in other studies, the respondents did not mention factors such as _inadequate library service_ as a barrier to using the Information Centre.

In sum, the present situation of the Company makes access to library materials and sources a laborious task. This situation may strongly affect use of the library. Librarians must consider the employees as distance users and must come up with new and innovative ways to meet their needs. Librarians must also attempt to define and identify their users and their need and in turn, develop and implement a set of library resources and services in support of these needs. Promotion of the resources and services available is necessary in order to successfully provide library support. Engineers and other specialists must know what services are available and how to access them.

## Suggestions

The following suggestions to improve service to the Company's engineers are made:

*   establish a mobile book delivery system to circulate books and other materials among the Company's seven plants;
*   subscribe to new English journals; and
*   establish an intranet to enable the engineers to connect to the Centre from their work sites

## References

*   <a id="ade85"></a>Adedigba, Y.A. (1985). Budgeting in the agricultural libraries and documentation Centres in Nigeria. _International Library Review_, **20**(2), 215-226.
*   <a id="akr01"></a>Akrami, M. (2001). A survey of Mash'had food industry factories' information needs and facilities. _Journal of Library and Information Science_, _4_(3), 13-33.
*   <a id="aza01"></a>Azad, M.R.R. (2001). _A survey of information-seeking behaviour of experts and researchers at the Agricultural Research, Education and Promotion Organization in sing agricultural special libraries._ Unpublished Master's thesis, Islamic Azad University, Northern Tehran Campus, Iran
*   <a id="bet90"></a>Bettiol, E.M. (1990).Information needs in the area of agricultural biotechnology in Brazil. [nece:Information na Area de Biotechnologia Agropecuaaria no Brasil]. _Ciencia da Informacao_, **19**(1), 3-11.
*   <a id="diz95"></a>Dizaji, B.M. (1995). _A survey of the attitudes of experts and researchers at the organizations under the supervision of the Ministry of Agriculture towards agricultural information services among these organizations._ Unpublished Master's thesis, Islamic Azad University, Northern Tehran Campus, Iran
*   <a id="dul00"></a>Dulle, F.W. (2000). The extension triad approach in disseminating agricultural information to extension workers: some experiences from the Southern Highlands Dairy Development Project, Tanzania. _Journal of Information Science_, **26**(2), 121-128
*   <a id="hak97"></a>Hakimi, H. (1997). _A study of information-seeking behaviour of researchers at the Iranian Natural Resources Research Centres_. Unpublished master thesis, University of Tehran, Tehran, Iran.
*   <a id="iza96"></a>Izah, M. (1996). Information needs and seeking-behaviour of extension specialists and technical officers of National Agricultural Extension and Research Liaison Services. Ahmadu Bello University, Zaria. _Library Focus_, (13/14), 50-66.
*   <a id="kan91"></a>Kaniki, A. (1991). Information-seeking and information providers among Zambian farmers. _Libri_, **41**(3), 147-164\.
*   <a id="maj00"></a>Majid, S., Anwar, M.A. & Eisenschits,T.S. (2000). Information needs and information seeking-behavior of agricultural scientists in Malaysia. _Library and Information Science Research_, **22**(2), 145-163
*   <a id="maj99"></a>Majid, S., Eisenschits, T.S., & Anwar, M.A. (1999). Library use pattern of Malaysian agricultural scientists. _Libri_, **49**(4), 225-235
*   <a id="noo97"></a>Noormohammadi, H. (1997). _A study of information-seeking behaviour of specialists in obtaining scientific and technical information at Engineering Research Centres of Ministry of Agriculture_. Unpublished Master's thesis, Tarbiat Moddarres University, Tehran, Iran.