#### Information Research, Vol. 8 No. 1, October 2002

# I = 0  
(Information has no intrinsic meaning)

#### F.J. Miller  
Principal Consultant, Fernstar  
Brisbane, Queensland, Australia

#### **Abstract**

> This paper was written mainly to help identify some contradictions that can be found in the notion of knowledge management though its application is wider-ranging. The author suggests that knowledge - that is to say 'what we know' - can scarcely be understood and managed even by ourselves, much less by means of sophisticated information and communications (i.e., groupware and shareware) technologies. We have progressed from the industrial age through the information age into what is being promoted as the 'golden age' of knowledge and, in the process, we've been led to believe that information contains meaning - rather than just standing for, provoking or evoking meaning in others. The paper argues that unless we take the trouble to face and understand the significance and implications of I = 0 (ie that information has no intrinsic meaning) and that knowledge is the uniquely human capability of making meaning from information - ideally in face-to-face relationships with other human beings - we may never emerge into any 'golden' age at all! The consequences of I = 0 for communications, learning, safety, quality, management (itself), and winning work are also discussed.

## Introduction - What's the problem?

We are increasingly being bombarded by information about something called 'knowledge management', the topic of possibly greatest management fascination in the late 1990's. Most articles available on this subject (though, thankfully, not all) tacitly promote the notion that knowledge and information are effectively interchangeable. We are being told that we can 'database' and 'capture' knowledge, that information, in effect, contains meaning. But does it? And is this an issue worth worrying about? Let's assume for the moment that it is, and explore why.

In a recent research study into attributes of "knowledge management" by [Davies and McDermott](#dav97) (1997), we are told that a number of "lessons were learnt". Here are some of them:

*   "All companies studied have common categories, terms and document attributes so they can easily find any knowledge element"
*   "Several companies have integrated knowledge capture and sharing into the natural flow of the user's work"
*   "Most companies have a system for organizing and disseminating explicit knowledge".

Extending this theme (i.e., that knowledge can be captured, made explicit, documented organized, disseminated etc., in effect, managed), recently, Hansen _et al._ (1999), writing in the _Harvard Business Review_ proclaim:

*   "Invest heavily in IT; the goal is to connect people with reusable codified knowledge"
*   "The reuse of knowledge saves work, reduces communications costs, and allows a company to take in more projects".
*   "Because knowledge is a core asset of consultancies, they were among the first businesses to pay attention to - and make heavy investments in - the management of knowledge. They were also among the first to aggressively explore the use of information technology to capture and disseminate knowledge".

On the surface such observations sound quite reasonable. But are they?

## Messages and meaning - what's the key distinction?

Most of us naturally assume that everything we say and do, every piece of information - in effect every message we send and receive, everything we read and write - has meaning. We've all said at some time: "If I've told you once, I've told you a thousand times", believing that what we "told them" contained meaning. Why else would we have bothered? Why would we make the effort to exchange information with each other - socially, or in business - if we thought that the effort might not constitute exchange of meaning? We generally assume that, when we communicate, others can understand our meaning so long as we make the effort to craft our messages carefully. We conscientiously attend meetings and are party to an endless stream of emails, faxes, memos and so on, both to - and from - just about everywhere on the planet, yet we are frustrated by the many unexpected and often unsatisfactory outcomes. People, somehow, always manage to misconstrue our meanings. We sometimes think: "Wasn't what we communicated clear enough?" "Can't people understand plain English"?

<figure>

![Figure 1](../p140fig1.gif)</figure>

We spend untold amounts of money on management education and 'training' yet we are sometimes dispirited by the apparently contrary behaviour of people and the difficulty organizations so often experience in their efforts to achieve alignment to business purpose, industrial harmony and sustainable business performance. Projects often over-run time and budget and we frequently attribute these outcomes to problems with the quantity and quality of information. We rarely stop to consider whether our view of information itself is at the root of the problem.

In the new millennium, we must wonder whether there is some basic underpinning principle, some seriously fundamental concept, which we may not yet have fully grasped despite the advances in the sciences and humanities which have provided such great improvements in quality of life for so many people on earth. Is it possible that we are on the verge of another breakthrough in our understanding of universal laws as the twenty-first century emerges?

## Is it possible that the "Information Age" has actually undermined human progress?

It is generally acknowledged that we have been living in the 'information age' since the middle of the twentieth century. Before this, the world lived through the "industrial age". Production, for the most part, was the consequence of manual labour - augmented to an increasing extent by evolving technology - directed and controlled by people responsible for the productivity of machines.

During the "industrial age", it was our personal relationships with others that gave our lives meaning. Most of the experience we gained in those days was tangible and immediate. Communication was largely face-to-face or paper-based. Our circle of family, friends and acquaintances was generally small, constrained by the primitive state of communications technology and the tyranny of distance. Interpretation of events could be tested by direct perception and immediate awareness by our senses of what was going on around us. Our _knowledge_ - that is to say what we knew from our direct experiences - was closely akin to the _knowledge_ of others with whom we necessarily lived our lives in close proximity.

The "information age" changed all that. Through technological innovation and breakthroughs in science, it became possible to deliver information (i.e., messages) accurately - and in an instant - to others, wherever they live on the face of the globe, whether we have any life experience in common with each another or not. _**And therein lies the essence of our problem and the cause of so many of our quite tragic human and organizational dilemmas.**_

We can send information and provoke a response in almost anyone we wish anywhere on the planet, but we can never be sure - unless we know these people personally - how they are likely to interpret (ie what meaning they are likely to make of) the information they receive from us. And even if we did know others very well indeed, we still could never be certain how their mood at the time might influence the interpretation they make of the information they receive. But there's an even deeper issue lurking here...

[Karl-Erik Sveiby](#sve97) (1997) has this to say about information:

> ...we should turn our concept of information on its head and acknowledge the following radical notion: information is meaningless and of low value. Currently, however, governments and many businesses alike act as if information is meaningful and has a high value.... Yet the value does not lie in the information stored but in the knowledge created [from it].

_Information_, it turns out, is simply the vehicle by which we attempt to provoke - or evoke - a human response. Information on its own is quite static and lifeless. It simply exists - on multimedia computer screens, in text books, magazines, movies, TV, CDs, reports, letters, emails, faxes, memos and so on - all waiting to be interpreted, all waiting to have meaning attached - _by people_. As Hugh Mackay explains in his book _The Good Listener_, although information certainly _stands for meaning_, it is never _meaning_ itself. Meaning is a mental thing and is only ever tacit, that is to say, 'in us'. Identical information almost invariably provokes (or evokes) different meanings in each of us. This should not surprise us. Rarely do two people (even identical twins) attach the same meaning to experiences - even when the experiences appear on the surface to be identical - like reading the same newspaper article, watching the same movie, attending the same political rally or participating in the same meeting. Identical _information_ always provokes different meanings in us because our interests, motivation, beliefs, attitudes, feelings, sense of relevance etc are always personal and changing - almost minute by minute.

## What then is the basic difference between information and knowledge?

The following chart ([Sveiby, 1997](#sve97)) suggests how information and knowledge are distinguished:

<table><caption>

**Figure 1: Information and Knowledge**</caption>

<tbody>

<tr>

<th>Information</th>

<th>Knowledge</th>

</tr>

<tr>

<td>Static</td>

<td>Dynamic</td>

</tr>

<tr>

<td>Independent of the individual</td>

<td>Dependent on individuals</td>

</tr>

<tr>

<td>Explicit</td>

<td>Tacit</td>

</tr>

<tr>

<td>Digital</td>

<td>Analogue</td>

</tr>

<tr>

<td>Easy to Duplicate</td>

<td>Must be re-created</td>

</tr>

<tr>

<td>Easy to broadcast</td>

<td>Face-to-face mainly</td>

</tr>

<tr>

<td>No intrinsic meaning</td>

<td>Meaning has to be personally assigned</td>

</tr>

</tbody>

</table>

Knowledge is, after all, _what we know_. And _what we know_ cannot be commodified. Perhaps if we did not have the word "knowledge" and were constrained to say "what I know", the notion of "knowledge capture" would be seen for what it is - nonsense!

The more we consider these issues, the more we are driven to the unpalatable and counter-intuitive conclusion that information is intrinsically meaningless on its own and remains so unless - and until - it is interpreted by human beings, within some context. "_I = 0_" is perhaps an elegant way of expressing this essential truth.

## If _I = 0_, then what role does information play?

Why do we attach so much importance to information? Because without information we would not have 'triggers' to alert us to the need to interpret events (assuming we think the events are relevant enough to bother interpreting). Information provides us with an opportunity to make meaning of sensory input. Without information, there is nothing to provoke us to 'sit up and take notice'. This is its primary value. Of course, information provokes different people in different ways, and this is also a critical issue. The simple fact is that without information, our senses are devoid of stimulation. And without information, we can have no way of being certain we even exist. (Experiments with total sensory deprivation produce symptoms of madness.) The life-long learning process itself could be said to be the on-going re-construction of meaning stimulated by a perpetual series of _I = 0_ events.

For this reason, information is critically important in the lives of people. _But if we then take the step of ascribing intrinsic meaning to the information itself, we cross the boundary of rationality and enter a bizarre world where we assume that impersonal stimuli have minds of their own and can have their own meaning!_

(The only time information could be said to 'imply meaning' is when the meaning provoked - or evoked - is predictable, as might be the case in the transmission of scientific information. But, as we know, even mathematical proofs are subject to interpretation.)

When then does _information_ become _knowledge_? The answer: **at the moment of its human interpretation** (and not an instant before!)

Writing from the perspective of Ericksonian psychotherapy, [Brown](#bro91) (1991) notes that:

> "Unless abstract symbols are mediated through personal experience they have no meaning".

[Myers and Myers](#mye98) (1998) state:

> "... words don't have meanings. There is no direct relationship between the thing you are talking about and the words you use. Only as these words are related through the thoughts of a person do they have meaning. Meaning is not in the object or in the symbol but in the interaction of these through the human [communication process]".

[Hugh Mackay](#mac98) (1998) suggests that:

> "It's not what our message does to the listener - but what the listener does with our message [that determines our success in communication]".  
>   
> "It's not the meaning we put into the message but the meaning the audience puts into the message that matters".

[Keith Devlin](#dev97) (1997), senior researcher at Stanford University's Centre for the Study of Language and Communication refers to

> ". the work of Alfred Tarski, a Polish-American logician who, in the early 1920s, in attempting to develop a mathematical theory of meaning made a crucial observation about any language, formal or natural, namely that 'Language does not, in of itself, have meaning. Meaning is not intrinsic to language. The meaning of a word or a sentence arises from whatever it is that the word or sentence refers to.'"

Why then do we persist in the assumption that messages contain intrinsic meaning when the literature abounds with evidence to the effect that information possesses no intrinsic meaning, and that in fact _I_ always = _0_?

## Can we ever manage knowledge?

To answer this question, we have to ask ourselves whether we can ever manage 'what people know'.

[Verna Allee](#all97) (1997) puts it this way:

> "There is no way I could possibly catalog even my own personal knowledge. What makes us think we can somehow catalog or map all the knowledge that resides in a complex enterprise of hundreds or thousands of people? What on earth do we think we can accomplish by 'managing' knowledge?"

[Nonaka and Takeuchi](#non95) (1995) also make the point:

> " ... information is a flow of messages, while knowledge is created by that very flow of information anchored in the beliefs and commitment of its holder. This ... emphasises that knowledge is essentially related to human action"

Thus the notion that we can 'capture' knowledge becomes ludicrous, just as ludicrous as the notion that we can 'capture people's thoughts'. In fact, it is a useful test when attempting to understand the notion of 'knowledge' to exchange it for a moment with the word 'people', and consider whether the sentence still makes sense. If it doesn't, a more appropriate word would probably have been 'information'!

Michael Polanyi, a pre-eminent thinker and author in this field (see, for example, [The Tacit Dimension](#pol66), 1966), has been widely misinterpreted (in this author's opinion) by more recent writers who have suggested that Polanyi viewed knowledge as - in one sense tacit - and in another sense explicit. This was not his view. For Polanyi, knowledge was only ever tacit. Once we attempt to make knowledge (i.e., what we 'know') explicit, it reverts immediately to an 'information' state again and requires human intervention anew for sense to be made of it. This is slippery business. Is it conceivable that the tacit/explicit confusion can be traced to the very existence of the term 'knowledge'?

## What's wrong with the term "knowledge"?

Language (unfortunately) makes it possible to treat as 'objective' (i.e., explicit) what is only ever 'subjective' (i.e., tacit). For example

*   "how I feel" (tacit) can become "feeling" (explicit)
*   "being safe" (tacit) can become "safety" (explicit)
*   "what I learn" (tacit) can become "education" or "training" (explicit)
*   "how I relate to you" (tacit) can become a "relationship" (explicit)
*   "what I know" (tacit) can become "knowledge" (explicit)

and so on.

This point is supported by the recent work of [Pfeffer and Sutton](#pfe00) (2000) who point out that

> "The . problem with much of the existing literature and practice of knowledge management is that it conceptualises knowledge as something tangible and explicit that is quite distinct from philosophy or values.. The noun 'knowledge' implies that knowledge is a 'thing'. Knowledge is embedded in. shared spaces where it is then acquired through one's own experience or reflections on the experiences of others. Knowledge is intangible."

Language - and in particular its 'noun' form - enables us to distance ourselves from our thoughts, feelings and experience and to 'objectify' them. This is a serious issue because, by ostensibly transforming the personal into the impersonal, we tacitly accept the legitimacy of capture, packaging and storage, implying that our thoughts, feelings and experience can be re-established - just as they once were - at another time. The act of 'objectifying' stops time dead in its tracks and transforms live experience into patterns of data and symbols - i.e., into 'information'. And while 'information' can, of course, be captured, stored, accessed and processed, _the human experience underpinning it - and making sense of it - cannot._

### Some implications of _I = 0_ for dictionaries

We assume that words have intrinsic meaning and that dictionaries are the repositories of those meanings. "If you want to know the meaning of a word, look it up in the dictionary" we are told. Yet, dictionaries are not repositories of meaning at all, but simply history books of word usage (Hayakawa). Language is simply the explicit vehicle by which we transform meanings (in us) into symbols (eg words) which then provoke (or evoke) meanings in others. Although the words we use _stand for meaning_, we should not assume that those words necessarily provoke the same meaning in others. In fact, more often than not, they provoke quite different meanings. The rule is: words don't possess intrinsic meaning, they simply serve to initiate the making of meaning. Stated another way, _I = 0_. For this reason, we must be wary of dictionaries. They can never - and were never designed to - resolve an argument about meanings.

### Some implications of _I = 0_ for communications

Most would agree that almost nothing is more important than effective and efficient communications. Yet how many of us think that we communicate well? Is it possible that something very basic is lacking in our understanding of communication despite the books we read, the courses we attend, or the consultants we've engaged? Maybe we simply don't communicate enough? Or is it that we communicate too much. Or is it something entirely different..?

We need to remind ourselves that the word 'communicate' is a _verb_, and as with all verbs, can be performed unilaterally, as in: 'I communicate', 'you communicate', 'he/she communicates' etc. But, we must ask ourselves, is 'communicating' something that can be done by individuals in isolation? Interestingly, the verb 'communicate' provokes/evokes different meanings in people. Some examples of 'communications' language are:

> transmit, pass on, report, get through, tell, convey, deliver, contact, inform, broadcast, speak, announce, relate, send, share, commune, exchange, swap, connect, understand, dialogue, listen, converse, explore etc.

Where is the meaning here? Certainly not in the words themselves! The tragedy of the entire notion of communications is that so many of us believe that by delivering messages (ie information) we are thereby delivering meaning. The reality is, however, that information contains no intrinsic meaning i.e., _I = 0_. Successful communications depends on knowing others well enough or caring about others deeply enough (the tacit dimension) to imagine how they are likely to interpret the (explicit) messages we exchange with them. Small wonder _e-mails_ - and the many other forms of explicit information - have the potential to create such havoc in human relationships.

### Some implications of _I = 0_ for training and learning

How many people stop to consider what the difference is between _training_ and _learning_? Most of us probably take for granted that they are two sides of the same coin, though the one (training) is explicit (i.e., information delivery) and the other (learning) is tacit (i.e., the making of personal meaning).

Experiments recently conducted (by Hatch Associates) attempted to understand the meanings people attach to certain key words in the workplace. When the word _training_ was thrown into the ring, surprisingly, it typically evoked negative reactions. Words like _teaching, classrooms, schedules, assessment, authority, competency measurement, control, accreditation, dependency, tests, discipline, boredom, manipulation_ etc., covered the white board.

_Learning_, on the other hand, generated a quite different and more positive list, evoking such responses as: _self-direction, understanding, enthusiasm, self-pacing, independence, open discussion, success, commitment, freedom, ease of access, excitement, maturity, honesty,_ etc. Despite these very different perceptions and responses, why do we still continue to use the language of _training_ and _learning_ virtually synonymously? Is it because we do not appreciate that _I = 0_ and that people will invariably interpret language in their idiosyncratically personal contexts?

### Some implications of _I = 0_ for safety

Safety is clearly the concern of everyone. Yet how many of us interpret the notion of safety in the same way? When a few people were asked what it evoked for them, there were many responses:

> LTI's, policy, procedures, reports, threat, blame, protection, vigilance, precaution, shelter, training, passports, statistics, policing, covering up, hazards, life, caring, sharing, shielding, uncertainty, consequences, threat . and so on.

This provides clear evidence, once again, that it is not what the message does to the audience but what the audience does with the message that really matters. From the list above, it would be difficult to find a single thought about safety that everyone shares. Is it conceivable that the noun 'safety' (the commodity) might not, in fact, be the best language to inspire people to behave safely? In light of _I = 0_ might not the language of 'safely' or 'be safe' evoke more powerful human (i.e., tacit) responses and hence serve to encourage more responsible and safer workplace behaviour?

### Some implications of _I = 0_ for _quality_ initiatives

For years now, many organizations have taken 'quality' to mean compliance with documented processes intended to achieve 'fitness for purpose'. (One might well ask: who decides what's fit or whether a purpose is appropriate or not; indeed even whether the person deciding is 'fit for purpose'!) The very act of documentation transforms ideas - which can originate only in the minds of people (tacitly) - into databases of information, which if inappropriately interpreted, can result in unwanted outcomes indeed. Sadly, we seem not to have appreciated that attempts to 'capture' (i.e., make explicit) human intentions serves only to transform them into intrinsically meaningless symbols even if made efficiently accessible from procedure manuals, computer databases, intranets and other sophisticated information sources. Captured information always relies on responsible people (i.e., of quality) interpreting it within a context - and sharing and comparing interpretations where alignment to business purpose is a desired outcome. Information cannot interpret itself! _I = 0_

### Some implications of _I = 0_ for bidding on jobs

We traditionally assume - when we receive requests to bid on jobs - that bid documentation provided has intrinsic meaning and reflects accurately the intentions of the customer. In effect, we assume that the 'message contains the meaning' and that we can decode intentions accurately, if we work hard enough at it! But _I = 0_, and as a result, we can but make our own meaning of information received. We then attempt to construct a reply which - while _standing_ for our meaning - cannot itself 'contain' our meaning. The bid response must somehow then invoke an appropriate meaning in the mind of the customer, which invariably occurs in yet another context. Thus the 'meaning' chain linking the thoughts of documentation authors, the interpretations of bidders and the final recipients of bids is broken many times in the process.

Can anything be done to retrieve the situation? Yes, but only if a relationship is established between the parties involved _which is sufficiently trusting_ to enable them to share and compare the various interpretations of their messages when sending and receiving information. If the bidding process does not allow such sharing and comparison to take place, the prospect for desired outcomes is severely hampered. The reason should be clear: _I = 0_. (See Figure 2)

<figure>

![Figure 2](../p140fig2.gif)</figure>

## Where does this leave the notion of Knowledge Management (KM)?

This is a vexed issue. KM is, sadly, deeply embedded in most modern literature connected with the productivity of intangible assets. Yet this paper tries to make clear that when subjected to critical analysis, KM is an untenable notion. Knowledge (i.e., what people know) simply cannot be captured or managed, and hence the term Knowledge Management is inappropriate. Worse still, the language of KM suggests that knowledge is a commodity capable also of being processed, delivered, transmitted etc when it is not. Whilst knowledge sharing is an acceptable concept, the notion of knowledge management is, at best, dubious!

## Conclusion

As explained in this paper, information - though it serves a fundamental and vital role as a provoker and evoker of meaning in people - possesses no intrinsic meaning of its own. This has enormous practical implications for people, safety, communications, knowledge sharing, relationships, training and learning, quality, business processes, customer service and so on. The statement "I = 0" is designed to remind us of an essential truth ... and to help us resist the pressure of information technology (and of its well-meaning protagonists) to undermine the value and significance of 'knowledge' as a human concept. Knowledge (ie 'what we know') is only ever 'tacit' and can never be 'explicit'. It must never be thought of as a commodity to be captured, processed, stored, transmitted, managed etc. Only human beings can intelligently make sense of - and provide an appropriate context for - information. Only human beings have the capacity to construct meaning from information and to sense 'meaning' evolving in themselves and in others. Only human beings can compare interpretations with a view to achieving a shared purpose. Information, no matter how elegantly processed and presented, is incapable - on its own - of achieving anything! _The irony of all this, of course, is that this paper likewise contains no intrinsic meaning. Whatever meanings are ascribed to it are simply the interpretations and judgements made of it by its readers._

The author would, nevertheless, wish readers to infer from this paper that the importance increasingly being placed on _accessibility to_ information is seriously out of balance with the importance that needs to be placed on _interpretation and sharing of_ information, and that this imbalance needs urgent action to redress.

Lastly, the language of 'knowledge management' serves only to confuse. It seriously diminishes the prospect that intangible assets (people) will be recognised within business and industry as being the _sine qua non_ of all value, growth, innovation, development and prosperity. KM serves people up on the altar of technology. If we lose sight - even for a moment - of the message that _I = 0_, humanity could well be subsumed entirely within technology in the years ahead to its unfathomable cost.

## Acknowledgments

The author acknowledges the great debt owed to Hugh Mackay and to Dr Karl-Erik Sveiby for so many of the ideas contained in this paper and for inspiration to call the paper I = 0\. Special thanks go to the many colleagues at work and at conferences who were prepared to indulge the author's passion for exploration of ideas until, together, we have all come to a deeper understanding of the issues under consideration.

# Afterword

This paper has created somewhat of a stir among 'Knowledge Management' aficionados since it was originally presented to the KNOW'99 Knowledge Conference at the University of Technology, Sydney Australia in Nov 1999\. Some salient reactions were:

*   If information has no meaning, how can we ever get our message across?
*   Of course knowledge can be stored, managed and accessed. Libraries have been doing it for centuries.
*   If we can't capture peoples' knowledge, how can we hope to retain corporate memory in these downsizing times?
*   KM is a time-honoured initiative in business. Many large organizations swear by it and are spending fortunes on it. Are you suggesting that KM is not an intellectually robust activity despite the vast resources being applied to it?
*   Information may possess very little meaning but certainly not zero meaning! .. and so forth

Of the many comments received, only a very small percentage went to the nub of the argument being presented, i.e., that information has no intrinsic meaning _(I = 0)_ and that shared human interpretation is always necessary. Assumptions were constantly being made about language and its supposed disembodied meaning without an attempt to explore implied or inferred meanings i.e., the human response. In effect, the critical question "What do you mean by...?" was rarely being asked, or answers explored. Most observers were comfortable assuming that 'what was said was meant, and, in effect, that 'knowledge management' was exactly what it implied, i.e., 'knowledge management'!

### What might we do to fix the problem?

It should be becoming obvious that language, and the meanings we attach to it, lie at the core of many human dilemmas and certainly is at the root of the KM predicament. KM simply has no intrinsic meaning (I = 0). Experience has demonstrated that because we have sanctioned use of the single word 'knowledge' to evoke such radically different interpretations, sensible and balanced discussion has become virtually impossible. What is needed now is a pragmatic approach to eliminating semantic confusion in a simple straightforward manner.

The answer, I would suggest, lies in differentiating between 'messages' and 'meaning'. The term 'message' tends to evoke images of things inanimate, explicit and capturable etc whereas 'meaning' tends to evoke notions of things human, personal, tacit, uncapturable etc. By linking (and restricting) the term 'knowledge' to 'meaning'-related language and 'information' to 'message'-related language, the KM predicament is capable of being resolved.

As far as the term 'management' is concerned, the 'message/meaning' dichotomy assists us here as well. Whilst we can easily manage 'messages', we cannot manage 'meaning'. We can however 'share meaning', though the process is still poorly understood. (There's a world of difference between communicating to and communicating with!)

For the above reasons, it is suggested that the KM predicament is best resolved by jettisoning the term KM once and for all in favour of unambiguous differentiated language viz Information Management (IM) and Knowledge Sharing (KS) (for example). The term 'knowledge' must never again be permitted to serve two masters!

It's time to stop genuflecting to the KM 'Emperor' once and for all. As Edmund Burke (1729-1797) suggested long ago: 'there's a point past which forbearance ceases to be a virtue' (Burke, 1762). In relation to KM, we are now well past that point.

> With regret, we note the death of the author on 23rd February, 2005 at his home in Brisbane, Australia, following a long battle against cancer. _The Editor._

## References

*   <a id="all97"></a>Allee, Verna (1997) _The knowledge evolution, building organizational intelligence_ Boston, MA: Butterworth-Heinemann.
*   <a id="bro91"></a>Brown, Peter (1991) "Oral poetry: toward an integrative framework for Erickson's clinical approaches", in: _Views on Ericksonian brief therapy, process and action_, edited by Stephen R. Langton, Stephen Gilligan, and Jeffrey K. Zeig. pp.66-92 New York, NY: Brunner/Mazel Inc. (Ericksonian Monographs, No 8)
*   <a id="bur1762">Burke, Edmund (1762)  _Thoughts on the cause of the present discontents. Together with observations on a late publication intituled 'The Present State of the Nation.'_</a>

*   <a id="dav97"></a>Davies, H. & McDermott, R. (1997) "Knowledge management: summary of lessons learned from other organizations", in, _Using IT to support knowledge management, final report_. Houston, TX: American Productivity and Quality Centre.
*   <a id="dev97"></a>Devlin, K. (1997) _Goodbye Descartes: the end of logic and a search for a new cosmology of the mind._ New York, NY: John Wiley & Sons.
*   <a id="han99"></a>Hansen, M.T., Nohria, N. & Tierney, T. (1999) "What's your strategy for managing knowledge?" _Harvard Business Review_, **77**(2)106-116.
*   <a id="mac98"></a>Mackay, Hugh (1998) _The good listener_. Sydney: Pan Macmillan. (First published as _Why don't people listen?_, 1994)
*   <a id="mye98"></a>Myers, G.E. & Myers, M.T. (1998) _The dynamics of human communications_. New York, NY: McGraw Hill
*   <a id="non95"></a>Nonaka, I. & Takeuchi, H. (1995) _The knowledge-creating company_. New York, NY: Oxford University Press
*   <a id="pfe00"></a>Pfeffer, J. & Sutton, R.I. (2000) _The knowing-doing gap - how smart companies turn knowledge into action_. Cambridge, MA: Harvard Business School Press.
*   <a id="pol66"></a>Polanyi, Michael (1966) _The tacit dimension_. New York, NY: Doubleday.
*   <a id="sve97"></a>Sveiby, Karl-Erik _The new organizational wealth: managing and monitoring knowledge-based assets_. San Francisco, CA: Barrett-Kohler

## Related reading

*   Brand, A. (1998) "Knowledge management and innovation at 3M". _Journal of Knowledge Management_ **2**(1), 17-22
*   Eisenberg, H. (1998) "Reengineering and dumbsizing: mismanagement of the knowledge resource". _IEEE Engineering Management Review_, **26**(3), 78-86
*   Fahey, L. & Prusak, L. (1998) "The eleven deadliest sins of knowledge management." _California Management Review_, **40**(3), 265+
*   Goss, T. , Pascale, R. & Athos, A. (1993) "The reinvention roller-coaster: risking the present for a powerful future" _Harvard Business Review_, **71**(6), 97-108
*   Kirby, J. (1999) "Downsizing gets the push". _Business Review Weekly_, **21**(10), 50-52
*   Miller, F.J. (1999) "[BNA* - getting back to business basics](http://www.fernstar.com.au/publications/papers/bna.htm)." Paper for BHP Engineering, Brisbane. Available at http://www.fernstar.com.au/publications/papers/bna.htm [Site visited 18th March 2002]
*   Miller, F.J. (1998) "[Why don't people learn? The message and the meaning for learning organizations](http://www.fernstar.com.au/publications/papers/message_meaning.htm)" Paper for BHP Engineering, Brisbane. Available at http://www.fernstar.com.au/publications/papers/message_meaning.htm [Site visited 18th March 2002]
*   Senge, P.M., Kleiner, A., Roberts, C.B., Roth, G. and Ross, R.B. (1999) _The dance of change_. New York, NY: Doubleday