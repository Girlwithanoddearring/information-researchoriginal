<header>

#### vol. 22 no. 4, December, 2017

</header>

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016.

<article>

# Charrette: case study of participatory library space designing in a postgraduate course

## [Hilary Hughes](#author)

> **Introduction.** This qualitative case study explores the potential of the charrette as a participatory designing process and learning approach for libraries and other educational contexts. It focuses on postgraduate students’ experience of learning space design through charrettes in a library-related unit of study.  
> **Method.** Qualitative case study methodology was used to examine the charrette process from the perspective of twenty-five postgraduate student participants at an Australian university. All participants’ reflections about their charrette experience were accessed online; follow-up semi-structured interviews were conducted with eight participants.  
> **Analysis.** Reflection and interview data were combined and analysed thematically through qualitative coding and categorisation.  
> **Results.** First-hand insights of postgraduate students’ experience of the charrette process demonstrate how a charrette is beneficial in enabling multiple stakeholders to participate in transformative learning and designing.  
> **Conclusion.** A charrette offers a participatory process and context for the design of fit-for-contemporary-purpose learning spaces in library and other education and community sites. It also supports informed learning, critical thinking and creativity for real world outcomes. The findings are of potential interest to library mangers, practitioners, educators and researchers concerned with innovative learning and/or participatory designing approaches.

<section>

## Introduction

A charrette presents both a process and context for participatory designing across community and educational contexts, including libraries. A key benefit of a charrette is that it involves a wide range of stakeholders, for example architects, library staff and diverse patron groups. On a practical level, a charrette can enable the creation of fit-for-contemporary-purpose learning spaces that respond to the needs of the intended users. From a conceptual perspective, a charrette recognises relationships between spaces and learning ([Blackmore, Bateman, Loughlin, O’Mara, and Aranda, 2011](#bla11); [Ellis and Goodyear, 2016](#ell16b)) and library user experience ([McArthur and Graham, 2015](#mca15)). A charrette can also be a powerful educational tool for developing a range of professional capabilities such as creativity, critical thinking and teamwork.

As this qualitative case study shows, innovative learning approaches and learning space design share similar aims with regard to accommodating a range of individual user preferences and needs. This paper first introduces key concepts and the charrette process in the Background and literature section and then outlines qualitative case study methodology. It next outlines the case study context and participants before presenting findings about postgraduate students’ experience of charrettes in a library-related masters unit. The final section discusses the potential of charrettes to support the practice, learning and research of participatory designing in library and other community and education contexts.

## Background and literature review

This case study draws upon several inter-connected concepts from the design, education librarianship and fields, as outlined in this section.

### Learning environment and learning spaces

The terms _learning environment_ and learning spaces are often used interchangeably. In this paper, learning environment is understood as a _‘holistic eco-system’_ that integrates educators, learning processes, content and resources’ ([OECD, 2013, p. 23](#oec13)). The latter include physical resources such as buildings, and technical infrastructure such as lighting and climate control. Learning spaces are areas within the wider environment and may be indoors, outdoors, digital, built and natural ([Oblinger, 2006](#obl06)). In addition, a learning space has a social dimension sustained through relationships between the learners and teachers who inhabit a particular space and the ways they use it ([Blackmore _et al._, 2011](#bla11); [Lefebvre, 1991](#lef91)).

Learning environment is sometimes considered to be a _third teacher_ that adds aesthetic and sensory stimuli to the learning process ([Strong-Wilson and Ellis, 2007](#str07)). Learning spaces are often claimed to influence learning outcomes, although there is no evidence of a direct link between them (<a hef="#bla11">Blackmore _et al._, 2011</a>). However as Blackmore et al. indicate, spaces can shape relationships and create the conditions for physical and mental wellbeing that are conducive to learning.

### Library learning spaces

Libraries offer a variety of learning spaces across the school ([Elliott Burns, 2016](#ell11); [Whisken, 2012](#whi12)), public ([Cohen, 2009](#coh09)) and academic ([Beard and Dale, 2010](#bea10); [Bryant, Matthews and Walton, 2009](#bry09); [Head, 2016](#hea16)) library sectors. Academic libraries in particular are increasingly compelled to transform their identity and service models in line with rapidly evolving pedagogies and technologies [Head, 2016](#hea16); [Weed, Willman, Whitmire, Prentice and Minars, 2015](#wee15)). Simultaneously, they are responding to users’ expectations for renovation of the library’s physical spaces:

> institutional leaders are starting to reflect on how the design of library spaces can better facilitate the face-to-face interactions that most commonly take place there. …Many libraries are making room for active learning classrooms, media production studios, makerspaces, and other areas conducive to collaborative and hands-on work. These changes reflect a deeper pedagogical shift in higher education to foster learning experiences that lead to the development of real-world skills and concrete applications for students. ([Adams Becker et al., 2017, p. 9](#ada17))

Across all sectors, versatility in the design of library spaces is necessary to ensure adaptability for a range of learning purposes that include informal discussion, group work and individual study. However, in the (re)designing process libraries face significant challenges related to the social, cultural and educational diversity of learners and library users; and the need to ensure that new or refurbished spaces are not only cost-effective but also fit for multiple purposes. Therefore, designing library spaces calls for shared local knowledge and understanding among all stakeholders, in particular between librarians and architects, about particular learning, teaching, information and research strategies ([Head, 2016](#hea16)).

### Designing spaces for learning

Designing spaces for learning is a complex process due to the many potential stakeholders involved. These include the people who commission, fund, plan, build and use the resultant spaces. Thus for example, stakeholders in library learning spaces include professionals such as architects and project managers, as well as end-users such as library leaders, staff and clients.

The contemporary emphasis on learner-centred social learning, would suggest a key role for learners in learning space design. However, it is often those with a transient connection (architects and builders) who have most influence on the design of learning spaces, whilst those for whom it is intended (learners, educators and librarians) have little or no say in the outcome ([Elliott Burns, 2016](#ell16a)). While architects and builders are vital to construction of a learning space their professional purview seldom encompasses the processes of learning. For example, children’s imaginings about the school library they would like are often different to what adults, including architects, anticipate ([Bland, Hughes and Willis, 2013](#bla13); [Elliott Burns, 2011](#ell11)). Moreover, there is no one-fits-all learning space design solution. Sometimes designs that are functional in one place are unsuitable when _‘parachuted in’_ to other locations, due to differing site conditions or community demographics ([Elliott Burns, 2011](#ell11)).

The complexity of addressing multiple stakeholder needs calls for participatory designing which is:

> an approach to building spaces, services, and tools where the people who will use those things participate centrally in coming up with concepts and then designing the actual products. ([Foster, 2012, pp. 2-3](#fos12))

A participatory designing approach enables many individuals to share their expectations and ideas and thus contribute to the creation of spaces that are conducive to their diverse learning needs. However, engaging specialists and lay people in a collaborative project can be problematic, due to their differing knowledge and experience of designing ([Foster, 2012](#fos12)). Therefore, this paper proposes the charrette as an inclusive designing process that enables the participation of multiple stakeholders with differing backgrounds.

### Charrette for participatory designing

A charrette (French for _little cart_) supports the participatory consensus-based design of spaces that respond to the expectations and needs of a particular user community ([Pernice, 2013](#per13); [Roggema, 2014](#rog14)). It involves

> A process of collaboration, intense dialogue and deliberation between participants to promote understanding and facilitate planning activity ([Kotval-K & Mullin 2015, p. 494](#kot15))

A charrette generally takes the form of a workshop led by an experienced facilitator that lasts for one or several days ([Howard and Somerville, 2014](#how14)). It involves individuals with varied backgrounds but a common interest in a design project, such as new community centre or library refurbishment. The aim is to generate a collective design concept that reflects stakeholder consensus. Acting as vernacular designers, the participants work collaboratively in one or several groups. They undertake a series of tasks which might include evaluation of current spaces, brainstorming design, discussing alternative design options and representing them visually. To ensure that everyone can participate, the tools and materials used are non-specialist and low tech. The intended outcome is concept drawing(s) or model(s) which may inform the work of architects, builders and interior designers ([Sutton and Kemp, 2002](#sut02), [2006](#sut06)).

The charrette enables a participatory approach that not only ensures inclusive decision-making about design outcomes, but also supports learning about design, and uses the design process and associated learning to facilitate change ([Singer and Woolner, 2015](#sin15)). It embodies the notion of participation as an ongoing co-creative process that brings people together in dialog, where participation is seen as ‘a value, it is learning and it is action shaped by interdependent views’ ([Parnell, 2015, p. 126](#par15)).

### Charrette in a library context

Charrettes are occasionally used in library design projects (for example, Imholz, [2008](#imh08); Marshfield Library & Senior Community Center, [2010](#mar10)). The most extensive published example is from University of Colorado Denver where two charrettes contributed to the major Auraria Library renovation project. Using participatory action research, this project aimed to redesign library services, systems and spaces in addressing rapidly changing learning and teaching approaches ([Howard and Somerville, 2014](#how14)). The charrettes brought a particular focus to re-imagining the library spaces and achieved implementable outcomes through shared ideas and participatory prototyping. Each charrette comprised three stages through which participants considered the existing space, developed and prototyped design ideas, and determined priority outcomes.

The Auraria charrettes exemplify how _‘all participants and stakeholders could gain and contribute to a rapid understanding of the design issues, tradeoffs, and decisions involved’_ ([Humphries Poli Architects and Holzman Moss Bottino Architecture, 2011, p. 16](#hum11)). The whole experience demonstrates the benefit of developing a charrette structure in partnership with consultant architects and library stakeholders.

### Charrette in higher education

In higher education, a charrette is often used in architecture, design and urban planning programmes ([Sutton and Kemp, 2002](#sut02), [2006](#sut06)). For other disciplines, a charrette offers a novel approach that supports active learning, critical thinking and creative problem-solving for real world contexts. It can also enable students to become fluent in a range of academic discourse (<a href-="#hug17">Hughes, 2017</a>). In librarianship education, it is beneficial in preparing students for changing roles and capabilities that emphasise collaboration and innovation ([Weed _et al._, 2015](#wee15)) and the creation of library spaces that enhance user experience ([McArthur and Graham, 2015](#mca15)). The following case study illustrates this learning potential.

## Introducing the case study: methodology

This case study explores the potential of a charrette to support learning and learning space designing. It features two groups (or cases) of students who participated in a charrette during a masters unit of study entitled Designing Spaces for Learning in 2015 and 2016 at Queensland University of Technology (QUT) in Australia. As educator-researcher I adopted qualitative case study methodology ([Simons, 2009](#sim09); [Stake, 1995](#sta95)) to gain insights about the charrette process from the participant-learners’ perspective. Thus, their experience of a charrette represents a contemporary phenomenon within a real life context.

Recruitment and data collection took place on conclusion of the unit in July 2016, once all assessment had been finalised. For recruitment, I sent an email via the 2015 and 2016 class lists with an information sheet detailing the research purpose and process. This invited the students to participate in two ways: by permitting me to analyse their charrette-related reflections; and by taking part in in a follow-up interview with me. The information sheet also advised prospective participants that: their participation in this project was entirely voluntary; they had the right to withdraw from the project at any time, including during the interview, without comment or penalty; all data gathered would be treated confidentially; and pseudonyms would be used to preserve their anonymity. I placed no limit on the number of reflections that I would accept and indicated that in addition I would interview the first five from each year who expressed interest. Mindful of my previous relationship with the students as educator, I stressed that that in the context of this study, I would communicate with them only as researcher and that their participation or non-participation in the study would not affect their future relationship with the university or me. They could volunteer via a SurveyMonkey poll which included a statement that submitting the poll indicated their consent to participate in the study.

A total of twenty-five students volunteered for the study. For the first round of data collection, I downloaded all twenty-five participants’ reflections from their online design portfolio. These were generally 200-400 words long and included photos. Then, I followed up with all eight of the original participants who had self-nominated for an interview. The interviews were about 30 minutes long, held via Skype and audio-recorded. They were semi-structured and expanded upon the written reflections and included questions about what the students liked or disliked about the charrette, what they learned through participating in it, and how they might employ a charrette in their own professional context.

For data analysis, I combined the reflection and interview data and followed a qualitative coding and thematic categorisation process (Miles, Huberman and Saldaña, 2014). Thus, I identified: different ways that students experienced or conducted charrettes; their thoughts and feelings as participants in a charrette; and their critical response to the charrette process and its potential to support learning and library space designing. The findings are outlined below, after the introduction to the case study context and participants.

## Case study context: the Designing Spaces for Learning unit

The Designing Spaces for Learning unit is part of a Master of Education (MEd) programme. While the majority of the unit’s students are teacher-librarianship majors, students from a broader librarianship practice programme also take this unit as an elective, along with students from other disciplines. Most students are enrolled externally (and study online) but a small group (ten to fifteen students) attend classes internally (on-campus).

The unit is intended for students with an interest in but limited prior knowledge of the theory and practice of learning space designing. It was developed originally by Raylee Elliott Burns ([2011](#ell11)), informed by her doctoral study. Conceptually, _learning space_ embraces formal and informal sites of learning including libraries, workplaces and educational institutions ([Elliott Burns 2016](#ell16a); [Head, 2016](#hea16)). In the unit, students engage with concepts and practices of participatory values-based designing which they apply to a familiar learning space. They undertake a semester-long project which involves evaluation of a self-selected space, identification of design problem(s) inherent in this space, and a proposed design solution supported by scholarly argument and literature. They present their proposal in an online design portfolio which they create using social software such as Weebly or Wordpress. The whole portfolio is assessed.

As current coordinator, I integrated the charrette into the unit in 2014\. The charrette is embedded in the unit to give students an authentic experience of a participatory designing approach. It not only provides a learning focus within the unit but also offers a strategy for students to apply in future designing projects in libraries and elsewhere.

## Case study focus: the charrette

The charrette component of the unit is designed so that all internal and external students can equitably engage with the process. Thus internal students participate in an on-campus charrette at the University Library. Meanwhile, external students either conduct their own mini charrette with colleagues, library patrons or their own students at another convenient educational site, or they hold a virtual charrette to discuss their learning space evaluation and re-design ideas with student peers via Skype or Google Hangouts. All students have access to the same charrette guidelines and supporting resources via the unit Website. I also upload videos and photos of the on-campus charrette to provide visual examples for the external students. Assessment for this part of the unit is a written reflection about the charrette experience and outcomes.

### On-campus charrettes at QUT Library

The on-campus charrettes comprise two workshops of three hours each, which take place in two consecutive weeks at Queensland University of Technology Library. Similar to the Auraria Library charrettes (Howard and Somerville, 2014), they follow a three stage collaborative process that involves: understanding and evaluating the learning space to identify design problems; developing a consensus-based conceptual design solution; and presenting and critiquing design solutions.

#### Workshop 1: evaluating the Curriculum Collection Area

Workshop 1 involves the first charrette stage of evaluating a particular learning space, the Library’s Curriculum Collection Area. Adopting the role of facilitator, I explain that the aim of the whole charrette is to develop a conceptual design solution to revitalise this out-dated space and transform it into a children and youth learning space for the University and the community. Then, each student takes on a different stakeholder identity, for example as: student, lecturer, librarian or children’s literature researcher. Individually, the students evaluate the Curriculum Collection Area, using a self-reflective checklist ([Elliott Burns, 2016](#ell16a); [Hughes, 2017](#hug17)). They then share their evaluation findings during a brainstorming session that I facilitate. For example, a summary of their collective responses from 2016 is shown in Table 1 below.

<table><caption>Table 1: Students’ collective responses to the Curriculum Collection Area in 2016</caption>

<tbody>

<tr>

<th>Positive aspects</th>

<th>Negative aspects</th>

</tr>

<tr>

<td>

Open, spacious.  
High ceilings.  
Natural light.  
Air conditioned.</td>

<td>

The Curriculum Collection Area is hidden way, hard to find.  
Its purpose is poorly defined.  
It is unattractive, appears dated, tired, dull colour scheme.  
Collection is hard to navigate - unclear layout, inconsistent arrangement of resources, poor signage, some books locked in cabinets.  
No private study areas.  
No Help Desk or Library staff available to assist in the area.  
Technology seriously inadequate – few computers or power points.  
Uncomfortable, nowhere to relax.</td>

</tr>

</tbody>

</table>

During subsequent group discussion, the students reduce their collective responses into a succinct design problem statement. In 2016 this was:

> The Curriculum Collection Area is a hidden away, tired space that lacks identity, a sense of purpose and vitality - but with overlooked potential to engage the university community in innovative learning experiences.

#### Workshop 2: redesigning the Curriculum Collection Area

Workshop 2 integrates the second and third charrette stages of developing, and then presenting and critiquing conceptual design solutions for the Curriculum Collection Area. At the start of Workshop 2 the students share ideas for transforming the out-dated Curriculum Collection Area. They work in small groups to devise and then visually represent innovative designs for the proposed children and youth learning space. As the purpose is to produce imaginative rather than technically perfect representations, I encourage the students to draw free-hand and use scrap materials, such as coloured paper, string and cardboard boxes. Each group presents their design to the whole class, indicating salient features. They also suggest titles for the revitalised space, such as _‘Rivers of Learning’_ and _‘Living Learning Lab’_. This is followed by critical discussion of the designs and ideas for further development.

### Off-campus charrettes

External students’ charrette locations include school and public libraries, as well school classrooms and a university lecture theatre. Depending on opportunity, they engage colleagues, library patrons, or even their own children, in a series of evaluation and redesigning activities similar to those experienced by the on-campus students. Those who are unable to arrange a physical charrette still evaluate a familiar learning space and then discuss their findings and design ideas and solution with other students online, often sharing photos to illustrate key points.

## Case study participants

The twenty-five participants in this case study were students or graduates who had successfully completed the unit of study Designing Spaces for Learning in 2015 or 2016\. They represented 15% of the unit’s combined enrolment for these two years. Table 2 below shows the total enrolled students, number of study participants and percentage of participants to total students. All except one of the participants were female, which somewhat reflects the gender balance in this unit.

<table><caption>Table 2: Overview of students enrolled in the unit in 2015 and 2016</caption>

<tbody>

<tr>

<th>Year</th>

<th>Enrolled students</th>

<th>Internal students</th>

<th>External students</th>

<th>Study participants</th>

</tr>

<tr>

<td>2015</td>

<td>70</td>

<td>14</td>

<td>56</td>

<td>9  
13% (of total enrolled)</td>

</tr>

<tr>

<td>2016</td>

<td>102</td>

<td>9</td>

<td>93</td>

<td>16  
16% (of total enrolled)</td>

</tr>

<tr>

<td>Total</td>

<td>172</td>

<td>23</td>

<td>149</td>

<td>25  
15% (of total enrolled)</td>

</tr>

</tbody>

</table>

All participants’ reflections were analysed. In addition, four students from each year participated in an interview with me. As shown in Table 3 below, most of the participants were enrolled in teacher-librarianship or library and information practices postgraduate programmes. Other participants undertaking the unit as an elective were enrolled in an MEd study area such as leadership and management. Three from each year had participated in an on-campus charrette at the University’s Library and the rest had organised their own mini charrette at another learning site such as a school or public library.

<table><caption>Table 3: Participant reflections and interviews by charrette location and study area</caption>

<tbody>

<tr>

<th>Charrette location</th>

<th>Study Area/Major</th>

<th>Reflections</th>

<th>Interviews</th>

</tr>

<tr>

<td>QUT University Library</td>

<td>

Teacher-librarianship (MEd)  
Library and information practices (MIS)  
Early years (MEd)  
General studies (MEd)  
Leadership and management (MEd)  
TESOL (MEd)</td>

<td>

4  
1  
1  
0  
1  
2</td>

<td>

2  
0  
1  
0  
0  
1</td>

</tr>

<tr>

<td>Another learning space</td>

<td>

Teacher-librarianship (MEd)  
Early years (MEd)  
General dtudies (MEd)  
Leadership and management (MEd)  
TESOL (MEd)</td>

<td>

12  
1  
2  
3  
1  
</td>

<td>

2  
1  
1  
0  
0</td>

</tr>

</tbody>

</table>

## Case study findings

This section presents case study findings arising from thematic analysis of the participants’ reflection and interview data. They reveal the _nature_ of the charrette as _learning experience_; the _potential_ of the charrette _process_ to support learning space designing; and some evidence of _real world outcomes_.

### Charrette as learning experience

The students’ responses indicate that they generally find the charrette as a learning experience to be interesting and worthwhile. For some it is empowering, even fun. They also identify some adverse aspects, although positive aspects tend to predominate.

As shown in Table 4 below, five themes describe the nature of the students’ charrette learning experience, namely: _learning_, _understanding_, _connectivity_, _community_, and _engagement_. Each theme has opposing more positive and less positive aspects. For example, positive aspects of _connectivity_ are associated with the charrette’s ability to encourage active participation and collaboration and allow real world connections between lay community members and professionals (such as architects and designers). On the less positive side, some students recognised a possible imbalance between community members and professionals in instances where differing levels of design-related knowledge might affect individuals’ ability to participate or have their views valued by others.

The themes are inter-connected and indicate students’ recognition of the learning opportunity and inter-personal relationships that a charrette can support. Thus, _understanding_ and _learning_ both highlight differing aspects of developing knowledge, including new awareness of other people’s values and needs, and co-creation. Similarly, _connectivity_, _community_ and _engagement_ are associated in differing ways with participants’ interactions in a charrette. For example, _connectivity_ highlights the way a charrette enables collaborative learning activities while community indicates the relationships that develop among learners through this interaction, while engagement describes outcomes of the interaction for participants (such as sense of belonging).

<table><caption>Table 4: Student responses: more and less positive aspects of the charrette experience</caption>

<tbody>

<tr>

<th>Charrette experience</th>

<th>More positive</th>

<th>Less positive</th>

</tr>

<tr>

<td>

**Learning**</td>

<td>

Knowledge co-creation  
Participants can be co-researchers & co-designers  
Confidence</td>

<td>Participants give what they think are expected answers</td>

</tr>

<tr>

<td>

**Understanding**</td>

<td>

Understanding different perspectives  
Awareness stakeholder values & needs  
Designers draw on local knowledge</td>

<td>Difficulty understanding budgetary constraints</td>

</tr>

<tr>

<td>

**Connectivity**</td>

<td>

Encourages active participation  
Collaboration  
Connects community members with professionals</td>

<td>Possible imbalance between whose opinions are valued</td>

</tr>

<tr>

<td>

**Community**</td>

<td>

Building relationships  
Community of trust  
Safe space  
Freedom to speak out  
Strengthening community values</td>

<td>Risk of hurting or alienating people with different views</td>

</tr>

<tr>

<td>

**Engagement**</td>

<td>

Fun  
Emotional buy-in  
Belonging  
Ownership  
Empowerment</td>

<td>Quieter participants lack confidence to speak</td>

</tr>

</tbody>

</table>

### Charrette as designing process

The students’ reflections also provide critical insights about the potential of a charrette as process to support learning space designing. They identify a range of benefits as well as some challenges. As shown in Table 5 below, these relate to six themes: _creativity_, _practicality_, _inclusivity_, _commonality_, <em?flexibility< em="">and _clarit_. The first two themes indicate the potential products of the charrette, in terms of fresh design concepts (_creativity_) and implementable solutions (_practicality_). The second two describe the charrette as means or framework to bring together people with diverse backgrounds (_inclusivity_) in order to work towards the achievement of common goals (_commonality_). The last two themes highlight characteristic qualities of the charrette (_flexibility_ and _clarity_) that enable the creative, practical, inclusive outcomes to occur through the charrette process.</em?flexibility<>

Such a dynamic participatory process is also likely to involve some challenges. For example, students mentioned that sharing design concepts brings the risk of generating an over-abundance of disconnected or conflicting ideas and unrealistic expectations. These could be hard to manage to ensure equitable, generally acceptable outcomes. As a result the process could become overly expensive or time-consuming.

<table><caption>Table 5: Student responses: benefits and challenges of the charrette process</caption>

<tbody>

<tr>

<th>Charrette process</th>

<th>Benefits</th>

<th>Challenges</th>

</tr>

<tr>

<td>

**Creativity**</td>

<td>

Generate unexpected ideas  
Outside the box solutions  
Wide range of solutions</td>

<td>

Overwhelming volume of ideas  
Risk of side-tracking  
Narrow-mindedness</td>

</tr>

<tr>

<td>

**Practicality**</td>

<td>

Achieve real world outcomes  
Framework for creating shared vision  
Inform cost effective solutions</td>

<td>

Unrealistic solutions  
Managing participants’ expectations  
Expensive – venue hire, facilitator fees</td>

</tr>

<tr>

<td>

**Inclusivity**</td>

<td>

Enable sharing diverse views  
Open forum for all stakeholders  
Negotiation</td>

<td>

Conflicting ideas  
Difficulty in reaching consensus  
Privileging particular interests and needs</td>

</tr>

<tr>

<td>

**Commonality**</td>

<td>

Identify common ideas and problems  
Work to achieve common goals</td>

<td>

Entrenched ideas  
Differences of opinion</td>

</tr>

<tr>

<td>

**Flexibility**</td>

<td>

Can be formal or informal  
Conducted with different group sizes</td>

<td>

Time consuming  
Difficult to find mutually convenient time and place</td>

</tr>

<tr>

<td>

**Clarity**</td>

<td>

Fresh eyes enable fresh vision  
Visual representation overcomes need for complex explanation</td>

<td>Mixed messages</td>

</tr>

</tbody>

</table>

Despite the challenges identified above, the students more often emphasise the benefits of the charrette process, as exemplified by the two following quotations:

> The collaborative charrette tantalises the imagination and gives permission to explore the ‘big picture’ and specific thumbnails in an environment where all voices are of equal value. The environment in which this occurs provides enough flexibility and safety to generate authentic responses and conversations about a space where everyone has a vested interest. Through valuing the needs and desires of all stakeholders, a strong sense of ownership and pride is created in the design and final build. (Student A)

> When stakeholders are involved in the design process, it may be more likely they feel a connection with the space. They may be more likely to care for it and use it respectfully. The more input from stakeholders there is, the more insights there are into problems and solutions associated with the space. (Student B)

### Charrette: real world outcomes

Although the charrette was an academic activity for most participants, some reported that they implemented the design solution they had envisaged through the charrette. For example, one student (a teacher currently working in a school library) reported that she had used her charrette findings to support a successful funding application for new flexible shelving and seating, and signage. These allowed her to create the small group spaces, relaxed ambiance and more user-friendly navigation that her student participants had called for during their charrette.

## Discussion: charrette potential

As this case study and the literature reveal, the charrette contributes significant potential for transformative designing and learning. In addition to supporting the participatory design process, the charrette offers an innovative pedagogical framework that aligns with emergent informed learning theory and practice. It also has a productive synergy with participatory action research.

### Benefits to library designers and users

The charrette offers benefits to libraries and their users. In library designing, a charrette can ease relationships and develop a shared language between key stakeholders including: architects who contribute the essential technical expertise; library leaders and staff who provide input about institutional policy and practice; and library users who comment about their spatial requirements. By actively encouraging participation, a charrette develops a rich evidence base ([Parnell, 2015](#par15)) of first-hand data to inform library (re)design initiatives. This in turn enables design decisions to be informed by understanding of real needs, mitigating costly design errors that arise from taken-for-granted assumptions about people and spaces ([Elliott Burns, 2011](#ell11)). The open communication fostered by the charrette can then support the translation of multiple needs and aspirations into a workable design outcome.

With its participatory nature, the charrette offers libraries a powerful approach for addressing two inter-connected challenges: equitably serving diverse user populations; and providing spaces that support multiple learning needs. The charrette’s low-tech, hands-on approach is essentially inclusive in welcoming all comers irrespective of knowledge or skills. In addition to being productive, a charrette can be fun, as attested by many of the study’s participants. It also presents an opportunity to engage children as library users and capture their ideas about what makes library spaces appealing to young people. This is beneficial as children’s cultures and ways of being are conducive to participatory designing (Parnell, 2015). A charrette can help represent young people’s views, which are often overlooked by professionals in the designing process ([Elliott Burns, 2011](#ell11); [Ghaziani, 2008](#gha08)).

Beyond creating spaces that meet patrons’ practical needs, a charrette can enhance relationships between the library and the community and generate a sense of ownership through shared initiatives that might encourage their ongoing use. In higher education, a charrette could lead to the student-attracting and fit-for-purpose library facilities envisaged by the Horizon report ([Adams Becker et al., 2017](#ada17)) and many universities’ strategic goals.

### Charrette as pedagogy

Designing a learning space offers various learning opportunities ([Singer and Woolner, 2015](#sin15)). This case study illustrates the potential of a charrette to support active experiential learning in the professional education of library and information professionals, teacher-librarians and other educators. The charrette model outlined above could be transferable to other university, school or library settings as it offers a dynamic framework for inquiry and problem based learning across curriculum and disciplinary areas.

For higher education, a charrette can provide a context and process for developing academic discourse ([Hughes, 2017](#hug17)) and professional capabilities such as critical thinking, creative problem solving and collaboration. In librarianship, a charrette could enhance students’ understanding about spatial impacts on library user and learner experience and add library designing expertise to their professional toolkit. It could also build capacity for working in inter-disciplinary teams with a range of library stakeholders ([Weed et al., 2015](#wee15)), including facilities managers and external experts such as architects and designers. A charrette could also contribute to information literacy, especially when aligned with informed learning pedagogy.

Informed learning ([Bruce, 2008](#bru08); [Bruce and Hughes 2010](#bru10)) promotes using information critically and creatively to learn. It addresses the needs of contemporary learners immersed in information-rich environments, where information is understood to be anything that informs in a particular context. For example, information includes facts, research findings, images, gestures and natural elements. Through reflecting on the different ways they use information when learning, individuals can enhance their current and future information use in academic, personal, and workplace learning situations. Thus in a charrette, students use design-related information to learn actively about the process of learning space design. The information they use includes design texts, technical drawings, written and drawn ideas and sensory stimuli (sounds, colours and textures). In the course of using this information, they learn how to collaboratively evaluate, design and represent innovative learning spaces. They also learn about people’s differing spatial needs and preferences and how to respond to these needs in appropriate, inclusive ways. When reflecting on the charrette experience and outcomes they are also considering the different ways they have used information to learn.

In combination, charrette and informed learning have the potential to influence learning space design in higher education. In light of growing awareness of environmental impacts on student learning, they could underpin innovative approaches to designing inclusive informed learning spaces that respond to learner diversity in higher education. Across the campus, these spaces can be physical and virtual, including those of the library and its digital presence.

### Charrette as participatory research approach

The charrette has already been identified as a useful tool for applied research ([Kotval-K and Mullin, 2015](#kot15)). It could also support the learning of librarian-researchers about inquiry and research processes. For example, through a charrette, participants experience various fundamental aspects of research such as planning, evaluation, gathering and making sense of data. In particular, the charrette process shares common characteristics with participatory action research. Like the charrette, participatory action research ([Reason and Bradbury, 2008](#rea08); [Zuber-Skerritt, 1996](#zub96)) supports active participation, social inclusion and transformative outcomes in academic and community contexts, including libraries.

In combination, the charrette and participatory action research form an innovative research approach that supports designing with, rather than for, stakeholders who become co-researchers and co-designers ([Howard and Somerville, 2014](#how14)). However, when a charrette is involved, the _plan-act-observe-reflect_ phases of the traditional participatory action research might be re-ordered. Thus, the charrette cycle would generally commence with evaluation (_observe_) then progresses through _plan_, _act_ and _reflect_ as follows:

*   _Evaluate_ the existing space (_observe_)
*   _Brainstorm_ solutions (_plan_)
*   _Develop_ a design solution: drawing, model (_act_)
*   _Critique-select_ design solution(s) (_reflect_)

As with participatory action research, the charrette cycle would accommodate iterations backwards, forwards and between the four phases.

## Limitations and further research

As this is an exploratory study, the participant group is relatively small and confined to the context of one unit of study. The research approach is consistent with qualitative case study methodology ([Simons, 2009](#sim09); [Stake, 1995](#sta95)) and accords with my intention to gain detailed first hand-insights about the potential of the charrette as learning experience and design process. While the findings lack generalisability, the case study is useful in providing an evaluative overview of the charrette for learning and designing in the library and education fields. The case study also contributes a charrette pedagogical model for other educators and practitioners.

There is extensive scope to extend this research in several directions. First, the overarching need is for greater understanding about the relationships between learning and learning spaces, including in libraries and higher education. In a recent review, Ellis and Goodyear ([2016](#ell16b)) call for empirical and theoretical investigation in this field, stating that it is currently fragmented and needs conceptual models to draw together disparate theoretical and disciplinary threads. In particular, they contend, there is a need for empirically based understanding of students’ use and meaning of space. Blackmore _et al._ ([2011](#bla11)) make a similar argument about the school sector. Second, although the charrette is well established in design practice, it requires further empirical and conceptual exploration and consideration of its applicability in a wider range of library and educational settings. It would also seem opportune to further explore the apparent synergy between the charrette and participatory action research ([Howard and Somerville, 2014](#how14)). Third, from the education perspective, productive connections between the charrette and informed learning ([Bruce, 2008](#bru08); [Bruce & Hughes, 2010](#bru10)) deserve further attention.

## Conclusion

Learning environment (physical, virtual and sensory) can influence the ways people interact, use information and learn. It is therefore important to ensure that all stakeholders’ perspectives are represented in the design of new and refurbished learning spaces, including in libraries. As illustrated by this case study, the charrette participatory designing process is a powerful means to achieving active stakeholder involvement. By providing an open forum for professional and lay stakeholders, a charrette can be instrumental in ensuring that imagined spaces are physically realised in user-focused cost effective ways. The charrette discussed here also provides an innovative pedagogical model for higher education library, especially librarianship.

## Acknowledgements

Sincere thanks to the twenty-five students who so generously and thoughtfully participated in this research.  
The research was unfunded. Ethics clearance was provided by Queensland University of Technology (Ref 1600000472).  
This article is an expanded version of a paper presented at the RAILS conference, Wellington, New Zealand, December 2016.

## <a id="author"></a>About the author

Dr Hilary Hughes is Associate Professor at the Faculty of Education Faculty, Queensland University of Technology, Australia. Her research interests include information literacy, teacher-librarianship and school libraries, connected and informed learning, international student experience and learning environment design. She is chief investigator for two Australian Research Council Linkage grants and has completed several other funded projects. In her research, Hilary draws on extensive previous experience as reference librarian and information literacy educator. In 2010 Hilary was Fulbright Scholar-in-Residence at University of Colorado. She has also received several learning and teaching awards. She can be contacted at [h.hughes@qut.edu.au](mailto:h.hughes@qut.edu.au).

</section>

<section>

## References

<ul>
<li id="ada17">Adams Becker, S., Cummins, M., Davis, A., Freeman, A., Giesinger Hall, C., Ananthanarayanan, V., Langley, K., &amp; Wolfson, N. (2017). <em>NMC horizon report: 2017 library edition.</em> Austin, TX: The New Media Consortium.</li> 
<li id="bea10">Beard, J. &amp; Dale, P. (2010). Library design, learning spaces and academic literacy. <em>New Library World, 111</em>(11/12), 480–492.</li>
<li id="bla11">Blackmore, J., Bateman, D., Loughlin, J., O’Mara, J. &amp; Aranda, G. (2011). <em>Research into the connection between built learning spaces and student outcomes: literature review paper no. 22.</em> Melbourne, Australia: Department of Education and Early Childhood Development.</li>
<li id="bla13">Bland, D., Hughes, H., &amp; Willis, J. (2013). <em>Reimagining learning spaces: a research report for the Queensland Council for Social Science Innovation. Queensland University of Technology, Brisbane.</em> Retrieved from https://eprints.qut.edu.au/63000/</li> 
<li id="bru08">Bruce, C.S. (2008). <em>Informed learning.</em> Chicago, IL: ACRL.</li>
<li id="bru10">Bruce, C. S. &amp; Hughes, H. (2010). Informed learning: a pedagogical construct attending simultaneously to information use and learning. <em>Library and Information Science Research, 32</em>(4), A2-A8.</li> 
<li id="bry09">Bryant, J., Matthews, G., &amp; Walton. (2009). Academic libraries and social and learning space: a case study of Loughborough university library, UK. <em>Journal of Librarianship and Information Science, 41</em>(1), 7-18.</li>
<li id="coh09">Cohen, A. (2009). Learning spaces in public libraries. <em>Public Library Quarterly, 28</em>(3), 227-233.</li>
<li id="ell16a">Elliott Burns, R. A. (2016). Voices of experience: opportunities to influence creatively the designing of school libraries. In K. Fisher (Ed.), <em>The translational design of schools: an evidence-based approach to aligning pedagogy and learning environments</em> (pp. 179-198). Rotterdam, The Netherlands: Sense.</li> 
<li id="ell11">Elliott Burns, R.A. (2011). <em>Voices of experience : opportunities to influence creatively the designing of school libraries.</em> (Queensland University of Technology PhD dissertation). Retrieved from https://eprints.qut.edu.au/48974/</li>
<li id="ell16b">Ellis, R. A., &amp; Goodyear, P. (2016). Models of learning space: integrating research on space, place and learning in higher education. <em>Review of Education, 4</em>(2),149–191.</li> 
<li id="fos12">Foster, N. F. (2012). <a href="http://www.webcitation.org/6ufGPnAIr">Introduction.</a> In Council on Library and Information Resources, <em>Participatory design in academic libraries: methods, findings, and implementations</em> (pp. 1-3). Retrieved from https://www.clir.org/pubs/reports/pub155/pub155.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6ufGPnAIr)</li>
<li id="gha08">Ghaziani, R. (2008). Children’s voices: raised issues for school design. <em>CoDesign, 4</em>(4), 225–236.</li> 
<li id="hea16">Head, A. J. (2016). <em><a href="http://www.webcitation.org/6ufGrnTqW">Planning and designing academic library learning spaces: expert perspectives of architects, librarians, and library consultants.</a></em> Project Information Literacy. Retrieved from http://www.projectinfolit.org/publications.html (Archived by WebCite&reg; at http://www.webcitation.org/6ufGrnTqW)</li>
<li id="how14">Howard, Z. &amp; Somerville, M. M. (2014). A comparative study of two design charrettes: implications for codesign and participatory action research. <em>CoDesign, 10</em>(1) , 46-62.</li>
<li id="hug17">Hughes, H. (2017). Charrette as context and process for academic discourse in contemporary higher education: a case study. In T. Miranda &amp; J.Herr (Eds.), <em>The value of academic discourse: conversations that matter.</em> Lanham, MD.: Rowman and Littlefield.</li> 
<li id="hum11">Humphries Polis Architects, &amp; Holzman Moss Bottino Architecture. (2011). <em><a href="http://www.webcitation.org/6ufGa7aLO">Design charette for Auraria Library: final report.</a></em> Retrieved from https://library.auraria.edu/sites/default/files/documents/AurariaFinal01132011.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6ufGa7aLO)</li> 
<li id="imh08">Imholz, S. (2008). Public libraries by design: embracing change at low cost. <em>Public Library Quarterly, 27</em>(4), 335-350.</li>
<li id="kot15">Kotval-K, Z. &amp; Mullin, J.R. (2015). The strategic use of the charrette process for applied research. In E.A. Silva, P. Healey, N. Harris &amp; P. Van den Broeck (Eds.), <em>The Routledge handbook of planning research methods</em> (pp. 492-501). New York: Routledge.</li>
<li id="lef91">Lefebvre, H. (1991). <em>The production of space.</em> Oxford: Blackwell.</li>
<li id="mar10">Marshfield Library and Senior Community Center. (2010). <em><a href="http://www.webcitation.org/6ufFquyg2">Design charrette summary.</a></em> Retrieved from http://ci.marshfield.wi.us/departments/planning_and_economic_development/library_and_community_center_project.php (Archived by WebCite&reg; at http://www.webcitation.org/6ufFquyg2)</li> 
<li id="mca15">McArthur, J. A. &amp; Graham, V. J. (2015). User-experience design and library spaces: a pathway to innovation? <em>Journal of Library Innovation, 6</em>(2), n.p.</li>
<li id="mil14">Miles, M. B., Huberman, A.M. &amp; Saldaña, J. (2014). <em>Qualitative data analysis: a methods sourcebook.</em> Thousand Oaks, CA: Sage.</li>
<li id="obl06">Oblinger, D. (2006). <em><a href="http://www.webcitation.org/6uZ55ZFAz">Learning spaces.</a></em> Boulder, CO: Educause. Retrieved from https://www.educause.edu/research-and-publications/books/learning-spaces (Archived by WebCite&reg; at http://www.webcitation.org/6uZ55ZFAz)</li>
<li id="oec13">OECD. (2013). <em>Innovative learning environments.</em> Paris, France: OECD.</li> 
<li id="par15">Parnell, R. (2015). Co-creative adventures in school design. In P. Woolner (Ed.), <em>School design together</em> (pp. 123-137). Abingdon, Oxon.: Routledge.</li>
<li id="per13">Pernice, K. (2013). <em><a href="http://www.webcitation.org/6ufFzjKaK">Charrettes (design sketching): ½ inspiration, ½ buy-in.</a></em> Retrieved from https://www.nngroup.com/articles/design-charrettes (Archived by WebCite&reg; at http://www.webcitation.org/6ufFzjKaK)
</li><li id="rea08">Reason, P. &amp; Bradbury, H. (2008). <em>The Sage handbook of action research: participative inquiry and practice</em> (2nd. ed.). Los Angeles, CA: Sage.</li>
<li id="rog14">Roggema, R. (Ed). (2014). <em>The design charrette: ways to envision sustainable futures.</em> Dordrecht, The Netherlands: Springer.</li><li
></li><li id="sim09">Simons, H. (2009). <em>Case study research in practice.</em> Los Angeles, CA: Sage.</li>
<li id="sin15">Singer, J. &amp; Woolner, P. (2015). Exchanging ideas for the ever-changing school. In P. Woolner (Ed.), <em>School design together</em> (pp. 184-208). Abingdon, Oxon.: Routledge.</li>
<li id="sta95">Stake, R.E. (1995). <em>The art of case study research.</em> Thousand Oaks, CA: Sage.</li>
<li id="str07">Strong-Wilson, T., &amp; Ellis, J. (2007). Children and place: Reggio Emilia's environment as third teacher. <em>Theory Into Practice, 46</em>(1), 40-47.</li> 
<li id="sut06">Sutton, S. E. &amp; Kemp, S. (2006). Integrating social science and design inquiry through interdisciplinary design charrettes: an approach to participatory community problem solving. <em>American Journal of Community Psychology, 38</em>(1-2), 51-62.</li>
<li id="sut02">Sutton, S. E. &amp; Kemp, S. (2002). Children as partners in neighborhood placemaking: lessons from intergenerational design charrettes. <em>Journal of Environmental Psychology, 22</em>(1-2), 171-189.</li>
<li id="wee15">Weed, J., Willman, E., Whitmire, D., Prentice, A. &amp; Minars, K. (2015). Looking both ways: seeing the changing roles of librarians. In B.L. Eden (Ed.) <em>Partnerships and new roles in the 21st-century academic library: collaborating, embedding, and cross-training for the future.</em> Lanham, MD: Rowman and Littlefield.</li>
<li id="whi12">Whisken, A. (2012). Library learning spaces: one school library’s initial design brief. <em>Synergy, 10</em>(2), n.p. Retrieved from http://www.slav.vic.edu.au/synergy/volume-10-number-2-2012/learning-landscapes/258-library-learning-spaces-one-school-librarys-initial-design-brief.html </li> 
<li id="zub96">Zuber-Skerritt, O. (Ed.). (1996). <em>New directions in action research.</em> London: The Falmer Press.</li> 
</ul>

</section>

</article>