<header>

#### vol. 22 no. 4, December, 2017

</header>

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016.

<article>

# After Beall’s ‘_List of predatory publishers_’: problems with the list and paths forward

## [Stuart Yeates](#author)

> **Introduction.** Jeffrey Beall’s now-defunct List of Predatory Publishers and the American Library Association’s Freedom to Read Statement are introduced. Uses of Beall’s List are examined in the context of the ALA Statement and academic libraries. Other issues with the List are examined and alternatives are reviewed.  
> **Method.** Tensions between the List and the Statement were identified primarily by close reading and through relating these to the role of academics, as readers and writers of academic journals and as faculty decision makers.  
> **Analysis.** Beall’s descriptions of journals are analysed in the light of his claimed goals and criteria.  
> **Results.** ‘Predatory’ is found to be a ‘subversive or dangerous’ label under Proposition 5 of the ALA Statement. Tension is found to exist between Beall’s List and the statement when used anywhere in faculty-run academic institutions. A number of approaches to avoid these tensions are discussed, including other approaches to determine the quality of an open access journal.  
> **Conclusion.** Malicious academic publishers undoubtedly exist, but Beall’s List is not an ethical solution to the problem of their existence. A replacement should be built from the ground up, rather than starting from the List. A possible checklist and associated metric are provided.

<section>

## Introduction

The transition in scholarly dissemination from a traditional print-based peer-review context to an all-digital open access peer-review context has been disruptive on a number of levels. New publishers have emerged with stables of journals of largely-unknown quality, some of whom appear to have dubious business practices. Existing publishers have hugely expanded their offerings and output or completely changed business models. This has led to increasing interest in measures for the quality of peer review, such as citation counts, journal lists and so forth. Using the lens of the ALA’s Freedom to Read Statement, I examine the ethics of a particular measure of the quality of peer review, known as Beall’s List. (Note: This is a revised version of a paper presented RAILS 2016 (6-8 December 2016), which has been updated to reflect recent events).

## Publishers with questionable business practices

One result of the pressure to publish in higher education, and the large number of institutions attempting to raise their research profile is a large pool of academics (or would-be academics) searching for journals to publish in. A wide variety of new or altered journals service this demand, ranging from well-established academic journals splitting into multiple series or expanding their throughput of articles, to entirely new journals and journal publishers. A common trend in new journals is pure-digital journals using low-cost cloud-based Internet hosting and running free-to-use software such as [Open Journal Systems](https://pkp.sfu.ca/ojs/) or [Wordpress](https://wordpress.com/).

Many of these new journals undoubtedly do (or make best-effort attempts to do) all the things that librarians and academics expect academic journals to do, including connecting with a community of peers, conducting peer review within that community, copy editing, publishing of accepted papers, promoting papers to indexing and search services, and dealing in a transparent manner with criticism (of articles or the journal as a whole) as it arises.

However, not all journals are what they appear to be, raising concerns about the quality of some journals, including whether they are scholarly and ethical, or merely commercial. At the extreme end of potentially unethical behaviour are journals which are little more than a new breed of vanity press, which take the author’s article processing charges (APCs) and publish the paper without any pretence of reading it or evaluating its quality through peer review and with no meaningful response to criticism ([Berger and Cirasella, 2015](#ber15)). In between lie journals with a range of issues including plagiarism, technical competence with publishing, no apparent peer group from which to draw reviewers, publishing in English as a second language, competence with peer reviewing, and financial issues.

## Beall’s list

Jeffrey Beall, a Denver-based librarian, has extensively studied open access journals that claim to do peer review but on closer examination do not. The term _‘predatory publishers’_ was coined by Beall ([Bloudoff-Indelicato, 2015](#blo15)), and it and related terms have been widely used in the academic world ([Cartlidge, 2017](#car17); [University of Tennessee Office of Research & Engagement, 2017](#utore17)), the popular press ([Carey, 2016](#car16); [Mascarenhas, 2017](#mas17)) and by government sources ([Federal Trade Commission, 2016](#ftc16)). Until early 2017, Beall published regular reviews of journals on his blog and compiled a comprehensive list, known widely as Beall’s ([2016h](#bea16h)) _List of Predatory Publishers_ and in full as _Potential, Possible, or Probable Predatory Scholarly Open-Access Publishers_. He also listed journals: _Potential, Possible, or Probable Predatory Scholarly Open-Access Journals_, _Hijacked Journals_, and _Misleading Metrics_. Through the comments section of his blog Beall accepted feedback and nominations of further journals to review. Beall has also published extensively in traditional journals on the topic, and in so doing almost single-handedly brought this issue to the forefront of thinking in academic publishing and academic librarianship. The List and his reviews are very widely referred to, with Google giving many thousands of incoming links.

Beall ([2015a](#bea15a)) published a list of criteria for assessing open access journals, based on two documents by the Committee on Publication Ethics (COPE; [n.d.a](#cpenda), [n.d.b](#cpendb)): the _Code of Conduct for Journal Publishers_ and the _Principles of Transparency and Best Practice in Scholarly Publishing_. No direct mapping of individual criteria to specific parts of the code or principles was given by Beall.

Beall’s List and his work in general have generated significant negative feedback from the publishers of some of the journals he has reviewed; there is at least one Website whose only purpose appears to be to attack him ([Anonymous, 2017](#ano17)) and vitriol has been posted to the comments section of his Website. A number of people have criticised Beall for racism (e.g., [Houghton, 2017](#hou17); [Segev, 2016](#seg16); [Velterop, 2015](#vel15)), an issue that has been ([Peterson, 1996](#pet96)) and continues to be a problem in librarianship more generally ([Hall, 2012](#hal12)). First-hand reports suggest that Beall was open to arguments for removing journals from the list (several academics have mentioned their experiences of this to me since I presented this paper in December 2016) as well as adding new titles.

Beall’s List has been used to quality-check other lists of journals, for example, the ABDC Journal Quality List, used for national research quality evaluations in Australia, removed a number of journals in 2016 based solely on their presence on Beall’s List ([Australian Business Deans Council, 2016](#abdc16)).

## Is there a tension?

Proposition 5 of the ALA statement is _‘It is not in the public interest to force a **reader** to accept the prejudgment of a label characterizing **any expression or its author as subversive or dangerous**’_ [**emphasis added**]. This proposition lies at the heart of the current analysis of Beall’s List.

Beall’s List is only in tension with the _Statement_ if three things hold true: (a) some of those who use, or are exposed to, the List are **readers**; (b) the List labels **expressions or authors**; and (c) ‘predatory’ is a label that characterises something as **subversive or dangerous**. I examine each of these separately.

1.  Straightforward uses for the List include collection development purposes ([Grabowshy, 2015](#gra15)) or informing academics’ choice of publication venue, both common activities in academic librarianship in which there is no obvious _reader_. But these activities are commonly performed by or under the scrutiny of academics, who are by definition both readers and writers of the academic output. There are indeed no functions of an academic library which are likely to be safe from the scrutiny of academics and thus no space where scrutinising academics (i.e., readers) might not see the label applied.
2.  The primary list is a list of publishers and reviews of publishers, however, closer examination reveals that publishers are listed as a convenient shorthand for listing all of the journals published by the publisher. For example:

    > Its 23 journals all boast fake impact factors, a tactic designed to attract article submissions from researchers needing to publish in impact factor journals. … Some of the journals appear to also be published by the predatory publisher Associated Asia Research Foundation, also on my list. So the owners are recycling some journals from another predatory publisher they own. ([Beall, 2016d](#bea16d))

    Further, sometimes the actions of individual authors are examined when deciding whether to list publishers or journals:

    > The scandal-plagued, Switzerland-based publisher Frontiers has just published a chemtrails conspiracy theory paper by the same author whose earlier article was published and then retracted in an MDPI journal. ... In August, 2015, I reported that J. Marvin Herndon had published a conspiracy theory paper in the MDPI journal International Journal of Environmental Research and Public Health. After my blog post was published, MDPI quickly retracted the article. ([Beall, 2016c](#bea16c))

    Thus, it seems clear that the List de facto lists both expressions and authors, directly or indirectly.

3.  Predatory does not necessarily connote something _‘subversive or dangerous’_. The Oxford English Dictionary gives four meanings: _‘involving plunder, pillage, or ruthless exploitation’_; _‘harmful to health’_, _‘relating to predatory animals’_, and _‘of business or financial practices: unfairly competitive or exploitative’_. The Merriam-Webster Dictionary covers very similar ground, giving _‘The inclined or intended to injure or exploit others for personal gain or profit’_. Since there is very little on the Beall’s Website about predatory animals and the other definitions are all either subversive or dangerous, Beall’s use of the _predatory_ label does characterise something as subversive or dangerous.

Combined, these three things mean there is indeed likely to be tension between the List and the ALA statement when used in institutions where actively-publishing academic authors wield significant managerial influence.

## Other issues with the List

There are a number of other issues with the List, some of which have been touched on elsewhere, but are summarised here for the sake of completeness:

### (1) False positives

Of the criteria Beall uses, one eclipses all others, _‘Evidence exists showing that the publisher does not really conduct a bona fide peer review’_ ([Beall, 2015a](#bea15a)). The other criteria seem largely irrelevant for potential users of the List, except as indirect indicators of a lack of peer review. The other criteria do, however, have a very large number of false positives, incorrectly suggesting, for example, that all manner of legitimate journals are questionable. These include all student-run journals (e.g., Harvard Educational Review, Cornell International Affairs Review), all in-house journals (e.g., IBM Journal of Research and Development), all journals whose editors whose use gmail.com addresses, and so forth. Despite not meeting some ideal of what an academic journal might be, these classes of journals play significant and valuable roles in education, in research and in the dissemination of research. While none of these example journals are on the List, they certainly meet the criteria. About the only possible virtue of discounting such journals is to eliminate discipline-specific publishing forms in pursuit of academic uniformity and excellence, which has been shown elsewhere to ultimately a fruitless exercise ([Moore, Cameron, Eve, O’Donnell and Pattinson, 2017](#moo17)).

### (2) Presumption of innocence

The List condemns all journals that have flaws, and makes no attempt to separate problems resulting from deliberate deception from those resulting from incompetence (in the language of communication; in peer review; in the business aspects of publishing; in the technical aspects of online publishing; some combination of these). While in some cases the evidence of deception is pretty strong, the presumption of innocence is a very important legal and ethical principle, too important to be removed in such a high-handed fashion.

### (3) Expedited review

Beall uses _expedited review_ (where journals offer fast peer review turnaround) as an indicator that a journal doesn’t actually conduct peer review as it should ([Beall, 2013a](#bea13a), [2013b](#bea13b)). The Committee on Publication Ethics documents don’t mention this, but encourage publishers to _‘Publish content on a timely basis’_ which would seem to encourage fast turnaround rather than the reverse. On this point Beall appears to be in direct contradiction with the documents, which are his stated criteria.

### (4) International dialects of English

The Websites of a few of the journals on the List, and many of their papers, appear to be written in international dialects of English. This, presumably reflects a population of new academics in (or from) non-Western countries. Beall has used poor or broken English as an indicator of quality several times in his reviews ([Beall, 2012a](#bea12a), [2012b](#bea12b)) with no apparent consideration as to whether the usage is acceptable in the national dialect in use. The only relevant passage in the two Committee on Publication Ethics documents that Beall uses as authority refers to all text on a journal's Website being written with due care to meet professional standards; there is no requirement explicit or implicit that Websites need to be in any particular international dialect of English.

### (5) Languages other than English

Items primarily in languages other than English such as [Arab Impact Factor](http://arabimpactfactor.com/) (wholly in Arabic) and [Index Copernicus](http://indexcopernicus.com/) (primarily in Polish, with some English and Russian) feature on the Lists. Beall shows no indication of speaking the primary languages of these items and no sign of consulting a suitably-qualified speaker of the relevant language, raising serious questions about his ability to evaluate them.

### (6) Locality reputation

The reputation of the countries and cities in which journals are based is not among the criteria, but Beall not infrequently makes observations such as:

> It’s Thanksgiving Day in the United States, a fitting time to examine a selection of predatory publishers from Turkey. Higher education in Turkey is currently in crisis, with numerous academics fired from their posts and institutions ordered closed. Academic integrity has been declining in Turkey for years, with plagiarism (including many plagiarized dissertations) and the widespread use of predatory journals for academic advancement and promotions common. ([Beall, 2016g](#bea16g))

> Hyderabad, India is one of the most corrupt cities on earth, I think. It is home to countless predatory open-access publishers and conference organizers, and new, open-access publishing companies and brands are being created there every day. All institutions of higher education, all funders, governments, and researchers should be especially wary of any business based in Hyderabad. The tacit rule of thumb of Hyderabad-based businesses is: Use the Internet to generate revenue any way you can. ([Beall, 2016f](#bea16f))

> When it launched, MedCrave reported its headquarters location as Bartlesville, Oklahoma. Now it claims Edmond, Oklahoma is its home. This is a lie, as the publisher is really run out of Hyderabad, India, the home of many corrupt online businesses, including predatory publishers. ([Beall, 2016e](#bea16e))

This is not the language of an impartial evaluation and completely unrelated to any of the given criteria. Predominantly white (or Caucasian) regions with well-established academic publishing issues appear to be spared this broad-brush characterisation when evaluating journals. For example, there are no such observations in any of the coverage of journals from the Russia or the former Soviet Union ([Beall, 2015b](#bea15b), [2016a](#bea16a), [2016b](#bea16b)) despite serious issues in higher education publishing with plagiarism, as documented in some depth in the Russian-language [Dissernet](http://www.dissernet.org/)), and copyright infringement, being home to the flag-ship copyright infringer Sci-Hub ([Kemsley, 2017](#kem17); [Scheman, 2017](#sch17)). (As of November 2017, Sci-Hub was not active following an American Chemical Society lawsuit ([Chawla, 2017](#cha17)).)

Taken together the last three issues would be seem to be easy to characterise as racism, xenophobia or colonialism, depending upon the lens one uses.

### Beall’s response

Beall ([2017](#bea17)) has acknowledged criticism of his work and summarises it like this:

> Over the five years I tracked and listed predatory publishers and journals, those who attacked me the most were other academic librarians. The attacks were often personal and unrelated to the ideas I was sharing or to the discoveries I was making about predatory publishers.

I have attempted to avoid personal attacks, I have acknowledged Beall’s very large role in bringing the unethical behaviour of some publishers to a wide audience, and I have tried to keep my analysis on the text, if not the ideas, that Beall has published.

### The way forward

In order to measure something reliably, we need to know what it is, exactly, that we’re trying to measure. Now that Beall has done an excellent job of bringing questionable academic publishing practises to the fore it makes sense to attempt define how we might measure questionable academic publishing practices.

The Committee on Publication Ethics documents that Beall relied on for his criteria come from a publishing background and are effectively a checklist of thing policies and statements that a publisher can have on its Website; for example:

> 15\. Archiving: A journal’s plan for electronic backup and preservation of access to the journal content (for example, access to main articles via CLOCKSS or PubMedCentral) in the event a journal is no longer published shall be clearly indicated. ([Committee on Publication Ethics, n.d.b](#cpendb))

A better approach would use a checklist that required verification, maybe in the fashion of the [Think Check Submit](http://thinkchecksubmit.org/). For example, a better wording might read:

> Archiving: A journal’s plan for electronic backup and preservation of access to the journal content in the event a journal is no longer published shall be indicated, and easily verifiable by third parties (for example the publisher or journal’s presence on the [CLOCKSS participating publishers list](https://www.clockss.org/clockss/Participating_Publishers), [PubMedCentral’s journal list](https://www.ncbi.nlm.nih.gov/pmc/journals/) or the [Internet Archive’s live repository](https://archive.org/web/))

While it would require no more work for publishers, this _trust, but verify_ approach would allow a third party metric to be built with confidence on the checklist and independently validated. The Committee on Publication Ethics ([n.d.a](#cpenda), [n.d.b](#cpendb))documents also contain an unfortunate number of technology-specific references which are likely to cause problems with technically innovative journals (_‘...both HTML and PDFs’_; implicit assumption that journals have a singular Website, etc.), but these are relatively straightforward to fix.

### Considering other interests

A more serious issue with the Committee on Publication Ethics approach is that it is grounded in the publisher’s point of view and ignores other groups with interests (or potential interests) in peer review and publication quality. These include authors, reviewers, funding agencies, university chancellors, librarians, and readers, who do not appear to have any input into the current documents. Issues these groups might raise include: inclusion of [COUNTER](https://www.projectcounter.org/) or [Standardized Usage Statistics Harvesting Initiative (SUSHI)](http://www.niso.org/workrooms/sushi/) statistics reporting; standardised reporting of a range of journal metrics; open access mandates; ethics approval statements; a stronger archiving position; standardised article withdraw notification; improved metadata (at the journal or article level); authority control (maybe [Open Researcher and Contributor ID (ORCID)](https://orcid.org) or [Virtual International Authority File (VIAF)](http://viaf.org/)); improved transparency; author, reviewer, editor and reader demographic reporting; and affirmative action reporting.

### Building a metric

Once a suitable checklist has been compiled, the next step is building a metric on the results. There are many ways to build a metric, depending on the checklist, but a list of priorities might look like this:

1.  Avoiding pejorative naming.
2.  Explicit lists of things to be encouraged or discouraged and clear mapping from these to the metric.
3.  A comprehensive authority scheme to identify parties (publishers, authors, reviewers, editors, etc.) such as The Virtual International Authority File (VIAF) or Open Researcher and Contributor ID (ORCID).
4.  A scalable, transparent, mechanistic process rather than human-based opaque interpretive process.
5.  An open world assumption rather than a closed world assumption to better handle missing and ambiguous data.
6.  A ranking or scale (maybe deciles or ventiles) to avoid bright-line good/bad distinctions, both because they encourage arguments of where the line should be and because they discourage further improvement once crossed.
7.  Design input from game-theory experts to reduce the chances of parties exploiting the metric for unforeseen commercial or personal gain.
8.  Design input from a broad range of stakeholders in the academic publishing industry.
9.  Versioning to enable updates should flaws be found and rectified.

An interesting possibility is raised by [Publons](http://home.publons.com/), which is a partially-de-anonymised database of peer reviewers built on the ORCID authority control scheme. Marketed as a technique for reviewers to get credit, such a system could also be used to verify claims made by journals that peer review is actually happening.

A second potential information source is natural language processing. Automatic detection of national dialects of English (or for that matter other languages) is relatively straightforward ([Teahan and Harper, 2003](#tea03)), meaning that journals which claim to be publishing using (for example) the Publication Manual of the American Psychological Association (APA), and thus American English, but publishing mainly in some other English dialect, could be detected. Automatically checking the veracity of as many claims from as many parties as possible would seem to reduce the ability to mislead.

## Conclusion

While there are undoubtedly publishers (and journals and conferences) that have dubious business and peer review practices, there is clearly a tension the ALA’s Freedom to Read Statement and Beall’s List of Predatory Publishers. This is particularly obvious in academic contexts with users who are simultaneously readers and policy-setters or influencers, as is the case in most academic institutions. There are additional problems with the List which probably mean that any design for a new metric for evaluating the transparency and peer-review should be built from the ground up, rather than being a new version of Beall’s List.

### Note

This paper is a version of a presentation given at RAILS: Research Approaches, Information and Library Studies, in December 2016\. In January 2017, Beall’s List and associated pages were removed from the Internet ([Chawla, 2017](#chaw17); [Straumsheim, 2017](#str17)), with Beall ([2017](#bea17)) saying _‘facing intense pressure from my employer, the University of Colorado Denver, and fearing for my job, I shut down the blog and removed all its content from the blog platform.’_ This paper therefore blends the version presented last December with subsequent changes. References are to the Internet Archive versions of the List’s pages.

## <a id="author"></a>About the author

Stuart Yeates is a Library Technology Specialist at Victoria University of Wellington. He maintains the library's institutional repository and peer reviewed journal platforms. He has a PhD in computer science from the digital library research group at University of Waikato and more than fifty thousand edits on wikipedia. He can be contacted at [syeates@gmail.com](mailto:syeates@gmail.com) [ORCID No. orcid.org/0000-0003-1809-1062](http://orcid.org/0000-0003-1809-1062)

### Disclaimer

As part of his job, the author does technical maintenance for several journals running on the OJS open access journal hosting platform. To the best of his knowledge these have never come to the attention of Beall or featured on his List.

</section>

<section>

## References

<ul>

<li id="ano17">Anonymous (2017). Jeffrey Beall: a crook is among us. Jeffrey Beall: a tacky felon is among us. [Blog comments].  Retrieved August 18, 2017, from http://jeffreybeall.blogspot.com/  (Archived by WebCite&reg; at http://www.webcitation.org/6t7FCAVWf)</li>

<li id="abdc16">Australian Business Deans Council. (2016). <a href="http://www.webcitation.org/6t7Dge7YM"><em>ABDC journal quality list initial interim review outcomes</em></a>. Retrieved from http://www.abdc.edu.au/data/Journal_Interim_Review_2016_/ABDC_Journal_Quality_List_Final_Interim_Review_Outcomes_as_of_2016.09.29.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6t7Dge7YM)</li>

<li id="bea12a">Beall, J. (2012a, November 15). <a href="http://web.archive.org/web/20151228235912/http://scholarlyoa.com/2012/11/15/open-access-alcohol/">Open access alcohol</a> [Blog post]. Retrieved from http://web.archive.org/web/20151228235912/http://scholarlyoa.com/2012/11/15/open-access-alcohol/ (Original removed by Beall, January 2017)</li>

<li id="bea12b">Beall, J. (2012b, December 12). <a href="http://web.archive.org/web/20160915051618/https://scholarlyoa.com/2012/12/05/three-new-questionable-open-access-publishers/">Three new questionable open-access publishers</a> [Blog post]. Retrieved from http://web.archive.org/web/20160915051618/https://scholarlyoa.com/2012/12/05/three-new-questionable-open-access-publishers/ (Original removed by Beall, January 2017)</li>

<li id="bea13a">Beall, J. (2013a, May 23). <a href="http://web.archive.org/web/20160421041422/https://scholarlyoa.com/2013/05/23/hawaiian-publisher-caters-to-latin-american-scholars/">Hawaiian publisher caters to Latin American scholars</a> [Blog post]. Retrieved from http://web.archive.org/web/20160421041422/https://scholarlyoa.com/2013/05/23/hawaiian-publisher-caters-to-latin-american-scholars/  (Original removed by Beall, January 2017)</li>

<li id="bea13b">Beall, J. (2013b, November 7). <a href="http://web.archive.org/web/20160702144049/https:/scholarlyoa.com/2013/11/07/want-a-faster-review-pay-for-it/"> Want a faster review? Pay for it</a> [Blog post]. Retrieved from http://web.archive.org/web/20160702144049/https:/scholarlyoa.com/2013/11/07/want-a-faster-review-pay-for-it/ (Original removed by Beall, January 2017)</li>

<li id="bea15a">Beall, J. (2015a, January 1). <a href="http://web.archive.org/web/20161130184313/https://scholarlyoa.files.wordpress.com/2015/01/criteria-2015.pdf">Criteria for determining predatory open-access publishers (3rd edition)</a> [Blog post]. Retrieved from http://web.archive.org/web/20161130184313/https://scholarlyoa.files.wordpress.com/2015/01/criteria-2015.pdf (Original removed by Beall, January 2017)</li>

<li id="bea15b">Beall, J. (2015b, October 1). <a href="http://web.archive.org/web/20161019200558/https://scholarlyoa.com/2015/10/01/article-broker-offers-to-write-articles-for-researchers-arrange-publication/">Article broker offers to write articles for researchers, arrange publication</a> [Blog post]. Retrieved from http://web.archive.org/web/20161019200558/https://scholarlyoa.com/2015/10/01/article-broker-offers-to-write-articles-for-researchers-arrange-publication/ (Original removed by Beall, January 2017)</li>

<li id="bea16a">Beall, J. (2016a, February 2). <a href="http://web.archive.org/web/20160503043814/https://scholarlyoa.com/2016/02/02/bogus-polish-journal-has-completely-fake-editorial-board/">Bogus Polish journal has completely fake editorial board</a> [Blog post]. Retrieved from http://web.archive.org/web/20160503043814/https://scholarlyoa.com/2016/02/02/bogus-polish-journal-has-completely-fake-editorial-board/ (Original removed by Beall, January 2017)</li>

<li id="bea16b">Beall, J. (2016b, March 8). <a href="http://web.archive.org/web/20170110033531/https://scholarlyoa.com/2016/03/08/the-increasing-use-of-predatory-journals-for-advocacy-research/">The increasing use of predatory journals for “advocacy research”</a> [Blog post]. Retrieved from http://web.archive.org/web/20170110033531/https://scholarlyoa.com/2016/03/08/the-increasing-use-of-predatory-journals-for-advocacy-research/ (Original removed by Beall, January 2017)</li>

<li id="bea16c">Beall, J. (2016c, July 14). <a href="http://web.archive.org/web/20160812230621/https:/scholarlyoa.com/2016/07/14/more-fringe-science-from-borderline-publisher-frontiers/">More fringe science from borderline publisher frontiers</a> [Blog post]. Retrieved from http://web.archive.org/web/20160812230621/https:/scholarlyoa.com/2016/07/14/more-fringe-science-from-borderline-publisher-frontiers/ (Original removed by Beall, January 2017)</li>

<li id="bea16d">Beall, J. (2016d, August 11). <a href="http://web.archive.org/web/20160812002559/https:/scholarlyoa.com/2016/08/11/new-swedish-oa-publisher-launches-with-23-journals/">New “Swedish” OA publisher launches with 23 journals</a> [Blog post]. Retrieved from http://web.archive.org/web/20160812002559/https:/scholarlyoa.com/2016/08/11/new-swedish-oa-publisher-launches-with-23-journals/ (Original removed by Beall, January 2017)</li>

<li id="bea16e">Beall, J. (2016e, November 3). <a href="http://web.archive.org/web/20161222210911/https://scholarlyoa.com/2016/11/03/medcrave-update-its-still-a-dangerous-predatory-publisher/">MedCrave update: it’s still a dangerous, predatory publisher</a> [Blog post]. Retrieved from http://web.archive.org/web/20161222210911/https://scholarlyoa.com/2016/11/03/medcrave-update-its-still-a-dangerous-predatory-publisher/ (Original removed by Beall, January 2017)</li>

<li id="bea16f">Beall, J. (2016f, November 22). <a href="http://web.archive.org/web/20161222222006/https://scholarlyoa.com/2016/11/22/hyderabad-india-city-of-corruption/">Hyderabad, India: city of corruption</a> [Blog post]. Retrieved from http://web.archive.org/web/20161222222006/https://scholarlyoa.com/2016/11/22/hyderabad-india-city-of-corruption/ (Original removed by Beall, January 2017)</li>

<li id="bea16g">Beall, J. (2016g, November 24). <a href="http://web.archive.org/web/20161222210916/https://scholarlyoa.com/2016/11/24/three-open-access-publishers-from-turkey/">Three open-access publishers from Turkey</a> [Blog post]. Retrieved from http://web.archive.org/web/20161222210916/https://scholarlyoa.com/2016/11/24/three-open-access-publishers-from-turkey/  (Original removed by Beall, January 2017)</li>

<li id="bea16h">Beall, J. (2016h, December 31). <a href="http://web.archive.org/web/20170111172306/https:/scholarlyoa.com/publishers/"><em>Beall’s List: potential, possible, or probable predatory scholarly open-access publishers.</em></a> Retrieved from http://web.archive.org/web/20170111172306/https:/scholarlyoa.com/publishers/ (Original removed by Beall, January 2017)</li>

<li id="bea17">Beall, J. (2017). <a href="https://web.archive.org/web/20171115212305/https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5493177/">What I learned from predatory publishers.</a> <em>Biochemia Medica, 27</em>(2). Retrieved from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5493177/ (Archived at https://web.archive.org/web/20171115212305/https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5493177/)</li> 

<li id="ber15">Berger, M. &amp; Cirasella, J. (2015). <a href="http://www.webcitation.org/6t7FLrond">Beyond Beall’s List: better understanding predatory publishers.</a> <em>College &amp; Research Libraries News, 76</em>(3), 132-135. Retrieved from  http://crln.acrl.org/index.php/crlnews/article/view/9277 (Archived by WebCite&reg; at http://www.webcitation.org/6t7FLrond)</li>

<li id="blo15">Bloudoff-Indelicato, M. (2015, October). <a href="http://www.webcitation.org/6t7IpZvcf">Backlash after Frontiers journals added to list of questionable publishers.</a> <em>Nature News, 526</em>, 613. Retrieved from http://www.nature.com/nature/journal/v526/n7575/index.html (Archived by WebCite&reg; at http://www.webcitation.org/6t7IpZvcf)</li>

<li id="car16">Carey, K. (2016, December 29). <a href="http://www.webcitation.org/6t7KItAsl">A peek inside the strange world of fake academia.</a> <em>New York Times.</em> Retrieved from https://www.nytimes.com/2016/12/29/upshot/fake-academe-looking-much-like-the-real-thing.html  (Archived by WebCite&reg; at http://www.webcitation.org/6t7KItAsl)</li>

<li id="car17">Cartlidge, E. (2017). <a href="http://www.webcitation.org/6t7KNT5Iu">Physorg, science's spam epidemic.</a> <em>Phys.org</em>. Retrieved from https://phys.org/news/2016-12-science-spam-epidemic.html (Archived by WebCite&reg; at http://www.webcitation.org/6t7KNT5Iu)</li>

<li id="cha17">Chawla, D. S. (2017, January 17). <a href="http://www.webcitation.org/6t7Kxr3t0">Mystery as controversial list of predatory publishers disappears.</a> <em>Science.</em> Retrieved from http://www.sciencemag.org/news/2017/01/mystery-controversial-list-predatory-publishers-disappears (Archived by WebCite&reg; at http://www.webcitation.org/6t7Kxr3t0)</li>

<li id="chaw17">Chawla, D. S. (2017, November 6). <a href="http://www.webcitation.org/6vBWTCZZR">Court demands that search engines and internet service providers block Sci-Hub.</a> <em>Science.</em> Retrieved from http://www.sciencemag.org/news/2017/11/court-demands-search-engines-and-internet-service-providers-block-sci-hub (Archived by WebCite&reg; at http://www.webcitation.org/6vBWTCZZR)

</li><li id="cpenda">Committee on Publication Ethics. (n.d.a). <em>Code of conduct for publishers.</em> Retrieved from http://publicationethics.org/files/Code%20of%20conduct%20for%20publishers%20FINAL_1_0.pdf</li> 

<li id="cpendb">Committee on Publication Ethics. (n.d.b). <em>Principles of transparency and best practice in scholarly publishing.</em> Retrieved from http://publicationethics.org/files/Principles%20of%20Transparency%20and%20Best%20Practice%20in%20Scholarly%20Publishing.pdf</li>

<li id="ftc16">Federal Trade Commission. (2016, August 25). <a href="http://www.webcitation.org/6t7LIaAOi">FTC charges academic journal publisher OMICS Group deceived researchers</a> [Press release]. Retrieved August 18, 2017, from https://www.ftc.gov/news-events/press-releases/2016/08/ftc-charges-academic-journal-publisher-omics-group-deceived  (Archived by WebCite&reg; at http://www.webcitation.org/6t7LIaAOi)</li>

<li id="ftr53">The Freedom to Read: A Statement by the American Library Association and the American Book Publishers Council. (1953). <em>Bulletin of the American Association of University Professors (1915-1955), 39</em>(2), 209-214.</li> 

<li id="gra15">Grabowshy A. (2015). <a href="http://www.webcitation.org/6t7LTuHeF">The impact of open access on collection management.</a> <em>Virginia Libraries, 62</em>(1). Retrieved from https://ejournals.lib.vt.edu/valib/article/view/1325/1794.  (Archived by WebCite&reg; at http://www.webcitation.org/6t7LTuHeF)</li>

<li id="hal12">Hall, T. D. (2012). The black body at the reference desk: critical race theory and black librarianship. In A. P. Jackson, J. C. Jefferson, and A.S. Nosakhere (Eds.), <em>The 21st-century black librarian in America: issues and challenges</em> (pp. 197-202). Lanham, MD: Rowman &amp; Littlefield.</li>

<li id="hou17">Houghton, F. (2017). <a href="http://www.webcitation.org/6t7LdwUwy">Ethics in academic publishing: a timely reminder.</a> <em>Journal of the Medical Library Association: JMLA, 105</em>(3), 282. Retrieved from http://jmla.mlanet.org/ojs/jmla/article/view/122/415 (Archived by WebCite&reg; at  http://www.webcitation.org/6t7LdwUwy)</li>

<li id="kem17">Kemsley, J. (2017). <a href="http://www.webcitation.org/6t7MLD0wr">Lawsuits progress against Sci-Hub.</a> <em>Chemical and Engineering News, 95</em>(27), 5.  Retrieved from http://cen.gext.acs.org/articles/95/i27/Lawsuits-progress-against-Sci-Hub.html  (Archived by WebCite&reg; at http://www.webcitation.org/6t7MLD0wr)</li>

<li id="mas17">Mascarenhas, A. (2017, January 9).  <a href="http://www.webcitation.org/6t7Mzytod">Study rings alarm over Indian work in ‘predatory’ journals.</a> <em>The Indian Express.</em> Retrieved from http://indianexpress.com/article/india/study-rings-alarm-over-indian-work-in-predatory-journals-4465429/  (Archived by WebCite&reg; at http://www.webcitation.org/6t7Mzytod)</li>

<li id="moo17">Moore, S., Cameron N., Eve M. P., O’Donnell D. P. &amp; Pattinson, D. (2017). <a href="http://www.webcitation.org/6t7NKmfbX">‘Excellence R Us’: university research and the fetishisation of excellence.</a> <em>Palgrave Communications, 3</em>, article 16105. Retrieved from https://www.nature.com/articles/palcomms2016105  (Archived by WebCite&reg; at  http://www.webcitation.org/6t7NKmfbX)</li>

<li id="pet96">Peterson, L. (1996). Alternative perspectives in library and information science: issues of race. <em>Journal of Education for Library and Information Science, 37</em>(2), 163-174.</li> 

<li id="sch17">Scheman, R. (2017). <a href="http://www.webcitation.org/6t7NlF2nU">Sci-Hub: piracy across the open (access) seas.</a> <em>The Physiologist, 60</em>(1). Retrieved from http://www.the-aps.org/mm/Publications/Journals/Physiologist/Archive/2017-Issues/January-2017-Vol-60No-1/Sci-Hub-Piracy-Across-the-Open-Access-Seas (Archived by WebCite&reg; at http://www.webcitation.org/6t7NlF2nU)</li>

<li id="seg16">Segev, I. (2016, September 14). <a href="http://www.webcitation.org/6t7OCCUtH">Beall-listed Frontiers empire strikes back</a> [Blog post]. Retrieved August 18, 2017, from https://forbetterscience.com/2016/09/14/beall-listed-frontiers-empire-strikes-back/  (Archived by WebCite&reg; at http://www.webcitation.org/6t7OCCUtH)</li>

<li id="str17">Straumsheim, C. (2017, January 18). <a href="http://www.webcitation.org/6t7P6z1qL">No more 'Beall's List'.</a> <em>Inside Higher Ed.</em> Retrieved August 18, 2017, from https://www.insidehighered.com/news/2017/01/18/librarians-list-predatory-journals-reportedly-removed-due-threats-and-politics  (Archived by WebCite&reg; at  http://www.webcitation.org/6t7P6z1qL)</li>

<li id="tea03">Teahan, W.J. and Harper, D.J., 2003. <a href="http://www.webcitation.org/6v09clvpb">Using compression-based language models for text categorization.</a> In <em>Language modeling for information retrieval</em> (pp. 141-165). [No place], Netherlands: Springer. Retrieved from http://boston.lti.cs.cmu.edu/callan/Workshops/lmir01/WorkshopProcs/Papers/teahan.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6v09clvpb)</li>

<li id="utore17">University of Tennessee Office of Research &amp; Engagement. (2017). <em><a href="http://www.webcitation.org/6t7PFMxFF">Predatory publishing.</a></em> Retrieved from http://research.utk.edu/predatory-publishing/ (Archived by WebCite&reg; at http://www.webcitation.org/6t7PFMxFF)</li>

<li id="vel15">Velterop, J. (2015, May 1). <a href="http://www.webcitation.org/6t7PQB50n">The fenced-off ‘nice’ publication neighbourhoods of Jeffrey Beall [Blog post].</a> Retrieved August 18, 2017, from http://blog.scielo.org/en/2015/08/01/the-fenced-off-nice-publication-neighbourhoods-of-jeffrey-beall/  (Archived by WebCite&reg; at http://www.webcitation.org/6t7PQB50n)</a>

</li><li id="wie16">Wiegand, W. A. (2016, March 15). <a href="http://www.webcitation.org/6t7Pu5Qiu">The Freedom to Read: the history of ALA's vital statement on intellectual freedom.</a> <em>American Libraries.</em> Retrieved from https://americanlibrariesmagazine.org/2016/03/15/freedom-to-read/  (Archived by WebCite&reg; at http://www.webcitation.org/6t7Pu5Qiu)</li>
</ul>

</section>

</article>