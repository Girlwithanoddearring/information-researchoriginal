<header>

#### vol.22 no. 4, December, 2017

</header>

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016\.

<article>

# Losing the art and craft of know-how: capturing vanishing embodied knowledge in the 21st century

## [Michael Olsson](#author) and [Annemaree Lloyd](#author)

> **Introduction.** The study examines the information practices of enthusiast car restorers in order to gain a greater understanding of embodied information practices.  
> **Conceptual framework.** The study is informed by a range of different theoretical approaches including practice theory, sense-making and Foucauldian, multimodal and critical discourse analysis.  
> **Methodology.** The study uses an ethnographic approach, using semi-structured interviews and in the garage ethnographic observation. Analysis was undertaken using an inductive, thematic approach.  
> **Findings.** Enthusiast car restorers see the lack of information resources relating to their hobby as a challenge as much as a barrier. Car clubs and informal social networks of fellow enthusiasts provide both mentoring and moral support. Learning by doing is central to developing embodied knowledge. Participants describe working on their cars as providing them with a sense of achievement that was otherwise missing in their lives.  
> **Conclusion.** The study’s findings show that enthusiast car restorers live in a complex in formation world, where social networks and learning by doing are central. The study’s findings in relation to alienation, achievement and identity suggest that research in to embodied practices may have a broader significance than has been hitherto recognised.

> Auto restoration can unite communities, it can bring together families, and it can help bridge the gap that sometimes exists between the past and the present. ([Davis, 2012](#dav12))

<section>

## Introduction

Classic a car ownership is a large growing leisure activity, with numbers in the UK alone rising from 1.1\. million to 4.5 million people in the five years between 2005 and 2010 ([Historic Vehicle Research Institute, 2011](#his11)).The hobby has now become a multi-billion-dollar global industry, with the most desirable classic cars now worth tens of millions. Despite this growth, which might signal success, it faces a significant challenge: the disappearance of embodied knowledge and skills essential for the maintenance of these cars. Changes in automotive design and manufacturing in recent decades mean that many skills, from hand-form in bodywork to rebuilding carburetors and making wooden body frames, have become obsolete in contemporary industrialised car manufacturing. Even the most prestigious manufacturers, such as Ferrari, Aston Martin and Jaguar, have had to call former employees out of retirement to assist with the operations of their own restoration services.

The loss of knowledge, not just of what to do but of how to do it, is not only a concern for hobbyists and historians. In the midst of our _information society_, the global shift from a production to a post-production economy has led a rapidly shrinking pool of individuals who possess needed embodied craft knowledge, the _how to do something_. While information researchers and the information professions should be at the forefront of addressing the issue, they are handicapped in doing so by their own long-standing privileging of encoded information: what Buckland ([1991](#buc91)) defines as _Information as Thing_. Despite the massive changes in information technology since the foundation of the Great Library of Alexandria circa 3rd century BCE, from papyrus, to print and digital media, the information professionals and institutions continue to primarily focus their practices around the preservation and dissemination of documents in their many and various forms.

Analysis of recent literature ([Julien, Pecoskie and Reed, 2011](#jul11); [Wilson, 2000](#wil00)) demonstrates that the overwhelming majority of extant information behaviour research also focuses almost exclusively on studies of purposive information seeking and searching within the encoded knowledge medium. Consequently, a contemporary research into information practices has shown, a range of other forms of information or ways of knowing are significantly underrepresented in information research as well as being largely missing from institutional collections. These include information relating to embodied and cultural practices and skills of craft and trades people, which historically have been passed down via master-apprentice relationships ([Lave and Wenger, 1991](#lav91)).

## Conceptual framework

The study is informed by a range of different theoretical approaches drawn both from within information studies ([Lloyd, 2014](#llo14); [Savolainen, 2008](#sav08)) and also the broader social sciences, including practice theory ([Gherardi, 2008](#ghe08), [2009](#ghe09); [Schatzki, 1996](#sch96)), sense-making ([Dervin, Foreman-Wernet and Lauterbach, 2003](#der03)) and Foucauldian, multimodal ([Kress and van Leeuwen, 2001](#kre01), and critical discourse analysis ([Fairclough, 2003](#fai03); [Foucault, 1978](#fou78)).

In this study we draw from a more _'sociologically and contextually oriented line of research'_ ([Talja, 2005](#tal05)) to focus on the concept of information practices. As an alternative perspective, the information practices literature acknowledges how people engage with and are shaped by existing and ever evolving discourses through social practices. Lloyd provides a holistic view of information practice and of practicing as something that references the 'social' and emerges corporeally as ways of knowing in situ: as situated action. She defines information practice as

> An array of information-related activities and skills, constituted, justified and organized through the arrangements of a social site, and mediated socially and materially with the aim of producing shared understanding and mutual agreement about ways of knowing and recognizing how performance is enacted, enabled and constrained in collective situated action. ([Lloyd, 2011, p. 285](#llo11))

This definition treats information practice sociologically and dialogically as emerging from the _saying, doing, and relating_ of a context. Practices are prefigured over time (e.g., they are formed and reformed in relation to embodied knowledge) and therefore reflect the various knowledge and ways of knowing in a social setting.

Our view is that knowing and ways of knowing occur through the enactments of practice ([Lloyd, 2010a](#llo10a)) and this requires an active relationship with the socio-material and symbolic elements that are inherent in practices, and the information landscape created through practicing ([Lloyd, 2010a](#llo10a)). This approach requires us to understand how shared, practical understanding is derived from becoming or being embodied in context (in situ). Consequently, to know is to be capable of participating with the requisite competence in the complex web of relationships among people, material artifacts and activities ([Gherardi, 2009, p. 118](#ghe09)).

Embodied information practices therefore act as rich sources of information about the social conditions that enable and/or constrain them, allowing us to view embodiment and embodied knowing as central to practical accomplishment. As Gherardi suggests,

> Knowledge is not what resides in a person's head or in books or in data banks. To know is to be capable of participating with the requisite knowledge competence in the complex web of relationship among people material artifacts and activities … . On this definition, it follows that knowing in practice is always a practical accomplishment. ([Gherardi, 2008, p. 517](#ghe08))

Information practices acknowledges that people are entwined in discourses and constantly encountering pre-existing discourses that enable and constrain social practices. While studies drawing on this have played a valuable role in highlighting the importance of language for information practices, studies considering non-linguistic and embodied practices remain, at this point in time, relatively rare in our field ([Lloyd, 2010b](#llo10b)). The study therefore joins a small but growing body of information practices research (e.g., [Godbold, 2013](#god13); [Lloyd, 2007](#llo07), [2010](#llo10b); [Olsson, 2010](#ols10), [2016](#ols16); [Prigoda and McKenzie, 2007](#pri07); [Veinot, 2007](#vei07)) which shed light on the relationship between embodiment and information.

In seeking to develop a more holistic approach, one which incorporates and appreciation of affective as well as embodied practices, the study has also been informed by aspects of Dervin's sense-making which explicitly rejects a narrow focus on individual cognition, arguing that sense-making must be seen as

> embodied in materiality and soaring across time-space … a body-mind-heart-spirit living in time-space, moving from a past, in a present, to a future, anchored in material conditions … . ([Dervin, 1999, p. 730](#der99))

The present study aims to adopt just such a holistic approach, embodied, affective, temporally- and socially-located, as advocated by Dervin.

## Research questions

The central focus of this ethnographic study is to investigate and describe how car enthusiasts acquire, preserve and pass on embodied information practices ([Lloyd, 2010b](#llo10b) ). It follows Lloyd ([2014](#llo14)) in arguing that information researchers must overcome their Cartesian privileging of the cognitive and linguistic over the embodied if they are to achieve a truly holistic approach to understanding the richness and complexity of people's relationships with information. Three broad questions drive this study:

1.  How are embodied practices accessed and preserved?
2.  How do embodied practice travel?
3.  What is the role of the information professions, institutions and research in preserving vanishing embodied information practices?

In addressing these, the study also focuses on the challenges faced by researchers in capturing embodied information practices and knowledges, such as craft knowledges that are in danger of being lost as material practices change.

## Methodology

The study adopted an ethnographic approach ([Bryman, 2008](#bry08)). The initial phase of fieldwork reported here included semi-structured interviews and _in the garage_ ethnographic observation of six Australasian car enthusiasts currently involved in the restoration of one or more classic cars. All participants were male, over fifty years of age and four of the six were retired from full-time work. Participants chose a pseudonym from a list of names of famous former racing car drivers provided by the researcher as an ice-breaking exercise.

In concluding the interviews, the principal researcher used an interview guide incorporating elements of Dervin's sense-making methodology ([Dervin _et al.,_ 2003](#der03)), although the overall approach was also heavily influenced by Seidman's ([1991](#sei91)) less structured, more conversational approach to research interviewing. Interviews lasted from forty-five to ninety minutes and were digitally recorded. The researcher also observed four of the participants during several hours working on their cars. In practice, the division between interviewing and observation was not clear cut. Since four interviews took place in the participants' garages, it was quite natural for them to stop in the midst of the interview and show the researcher the relevant part of the car or demonstrate the technique they were describing. Similarly, there were occasions during the observation when participants would spontaneously break off from a task in order to either provide the researcher with a longer explanation of the task they were undertaking or to share a thought which had occurred to them.

Analysis was undertaken using an inductive, thematic approach. Although the analysis was consciously informed by the range of theoretical perspectives described above, the study's aim was not to test a pre-defined theory or hypothesis, but to develop a contextual, situated understanding of the relationship between participants' context, their role/s as members of the classic car community, the discourses they engaged with and their information practices. Each researcher undertook their own initial thematic analysis before coming together to develop findings drawing on both researchers' insights and expertise.

## Findings

In exploring the information practices of enthusiast car restorers, the study's findings discovered a complex information world. Whilst some of the practices participants described will be familiar from previous information seeking and information behaviour research others, particularly those related to embodied practices and identity construction, have thus far been under-researched in the field.

### Working it out

Of the six participants, only one, a retired technical and further education (TAFE) teacher, had a professional background in the automotive industry. Of the others, one had undertaken a three-month mechanic's course in the 1960s but had never worked in the industry, one was an engineer in an unrelated field and the son of a panel-beater (bodywork repairer) and the others had unrelated educational and professional experience. A common theme, however, in all participants' interviews was a life-long interest in understanding how things work:

> I was always a hands-on kind of bloke, even as a kid I'd be building things or fixing things (Moss)

> I like the challenge of working out how to do something not just as cars but clocks, lawnmowers, anything really. (Surtees).

This theme of independence, an ability to solve complex practical problems and learning by doing was a powerful discourse throughout the research:

> I just picked it up over the years, engines gearboxes. ... a fair bit of trial and error. I now feel that if I don't know how to do it, I've got enough experience I can probably work it out. (Moss)

The prevalence of this _working it out_ disclosure is one example of the complex and nuanced relationship with the information challenges of car restoration participants demonstrated: one which transcends prevailing notions of information need in the field.

Participants did, of course, talk about their projects in terms of a knowledge gap, of the lack of availability of relevant documentation and of people with the necessary knowledge and skills:

> For a lot of these cars, even if there was any build sheets or design drawings, they disappeared 50 years ago or more! (Hulme)

> Guys like me who were trained to do things like fabrication just aren't around anymore. We're all getting old and dying off! (Surtees)

However, at the same time, all participants suggested that this absence of information, challenge of working out solutions and rediscovering vanishing knowledge was one of the major appeals of the hobby for them:

> I just really like the challenge of working out how things are made and how they work. (Webber)

> It can be really frustrating but it's really great when you work it out! I might sit in the garage with a cup of tea just looking at a part of the car, working out how it was done originally. (Hulme)

### Manuals and self-documentation

This is not to suggest that participants did not value documentation. Three participants, for example, talked about original factory workshop manuals as among their most prized possessions and most valuable information resources:

> With Bristols [a brand of car], there's a really comprehensive workshop manual and if you follow that, it's really just step-by-step, exactly what to do. (Moss)

So prized are hard-to-obtain factory workshop manuals that one participant described the lengths he had gone to in order to make it available to other club members:

> Manuals were hard to get, so I actually bought a photocopier, photocopied about twenty and sold them to people ... about six or seven years ago I did the same but scanning, so you can now have the manual as 20 pdfs. (Webber)

Participants had developed a range of different strategies to overcome the lack of original documentation:

> I collect old racing magazines, even old newspaper reports, from when the car was racing, especially if they have photos. That can be the only way to find out what the car originally looked like, what wheels it was running, lots of details that nobody bothered writing down at the time. (Hulme)

All participants kept extensive files of documentation of their restoration projects as they went along. Photographs in particular were seen as an essential information resource:

> I take lots of photos as I take the car apart to keep track of things - that way hopefully I can put it back together again! (Clark)

> I keep a file with photos and paperwork for every stage as I go along. (Webber)

Most, however, saw these as an essentially personal resource, with only one participant describing sharing their project files with other enthusiasts. This participant did so in the context of a presentation he gave to his car club.

### Car clubs, mentoring and informal social networks

All participants were active members of more than one car club and regarded them as an invaluable site for gathering information to assist in their restoration projects. Two participants who were members of large marque-based clubs talked about the resources these clubs make available to members:

> The Alfa [Romeo] club has a quite big library, with workshop manuals and other useful stuff you can borrow. Other big clubs, like MG or Jaguar do the same. (Hulme)

More commonly, however, participants talked about clubs as important as a place to meet and learn from other car enthusiasts with more expertise:

> What I really like about the club is that there are people there, like Moss, who've been restoring these cars for years. Between them, they know everything there is to know about Bristols. (Webber)

One consequence, according to three participants, of the rapid growth in the hobby in recent years is that although the overall number of club members has increased, the number with expertise in restoration has stayed the same or even declined:

> The club really started as a group of people getting together trying to keep these cars on the road. Nowadays, a lot of club members have joined more for the social side... they have to rely on other people to do the work for them. (Moss)

> The MG club now is mostly about the social side, which is fine. Some of us who are more interested in restoration started the MG Restorers Club. (Surtees)

In talking about the ways in which car clubs helped them through their restoration projects participants did not talk about them solely in terms of being a source of information and expertise. Just as important to participants was the ways in which car clubs acted as multi-layered support network providing moral as well as technical support:

> The club was like a touch-base sanity for me ... you can't do it on your own. (Webber)

> Talking to other guys in club is really good. It keeps your enthusiasm up … It helps to know there are others in the same boat! (Surtees)

Car clubs were not participants' only source of expertise and support, with participants all describing the informal network of "car friends" who provided them with information and helped them developed embodied skills.

> I had this car out on the driveway and this guy was walking past and stopped to chat. He was interested and it turned out he was a retired Qantas engineer. After that, he'd stop by and lend a hand. … I'd never have been able to sort out the wiring harness without him. (Webber)

In developing complex embodied skills, participants all talked about how invaluable it was to be able to work with and learn from other, more experienced enthusiasts in this way:

> There was an older guy, an aircraft engineer, I hung out and got some advice from him. (Moss)

This form of mentoring has much in common with the master-apprentice relationships described by Lave and Wenger ([1991](#lav91)). Two participants described this mentoring in the context of their own family:

> Dad was a panel-beater and he taught me a lot from a young age. (Clark)

> It's become a family thing. My sons grew up working on the cars with me. The other day some of my grandkids were over and I was showing them how to do a few things - standing on a chair to reach the bench! (Surtees)

The researcher was able to observe one of these father-son pairings during the course of an afternoon working on their current restoration project. Although the father had been at pains to point out that his son's expertise in many areas now far suppressed his, it was clear to the observer how their deep understanding of each other's skills and work practices allowed them to work together on complex tasks with little or no explanation required. This did not prevent them commenting on and critiquing one another's work, usually in a humorous way. The researcher's observations confirmed participants' accounts of the strong friendships they developed with those who they worked on cars with.

### Multi-sensory embodied practices

All participants emphasised the centrality of _learning by doing_ when it comes to developing embodied knowledge:

> You've really got to develop a feel for the metal as you're shaping it ... Aluminium reacts very different to steel. (Clark)

> Even if you've got a manual, it can only take you so far. When you start doing it, there's lots of things a book can't tell you. (Hulme)

During the researcher's observation of participants working on their cars, they frequently used tactile language to describe what they were doing:

> You have to develop a feel for it - too hard and it breaks! (Clark)

> I'm feeling along here as I check for cracks. (Hulme)

The participants' practices and their explanations of them strongly brought to mind Olsson's earlier study of archaeologists ([2016](#ols16)) where participants also relied heavily on touch when working with archaeological artefacts.

### Virtual media

Even in the context of pre-digital cars, all participants described the internet as being an important resource in their restoration projects:

> It's all internet: queries here, queries there, queries somewhere else. (Webber)

Overwhelmingly, however, participants' discussion of virtual resources focused on EBay and other sales sites:

> I'm an EBay addict ... I've got a couple of triggers set on UK EBay: Bristol MINUS football MINUS bus! (Webber)

> The great thing is that you can jump online and find what you're looking for. You can get parts in from the US or England. (Hulme)

Perhaps surprisingly, none of the participants described being active in any restoration or marque-specific online discussion forums. Whether this related to the age of the participants, the types of cars they were restoring or other factors are questions that the next phase of the research will hope to address.

One participant, who happened to be the youngest, albeit in his mid-fifties, did describe a very different type of online practice:

> For the English wheel, there's a lot of really good stuff on YouTube … You can watch some of the top guys in the UK or wherever … . (Clark)

This participant used YouTube very purposefully as a learning tool to improve his practice using the English wheel, a metalworking tool that enables a craftsperson to form compound (double curvature) curves from flat sheets of metal such as aluminium or steel ([Smith, 2015](#smi15)). The participant was very purposeful in his use of YouTube, undertaking detailed searches to the specific technique or task he was undertaking at the time and did not subscribe to any particular channels. Whether YouTube and similar sites as multi-modal resources ([Kress and van Leeuwen, 2001](#kre01)) offer benefits in learning embodied skills is another question that the next phase of the research will hope to address.

### Embodiment, achievement and identity

The study findings also suggest that in considering the practice of embodied knowledge that we should not confine our focus simply on their usefulness in completing a particular task. A strong feature of every participants' interview was unsolicited discussion of how working on their cars provided them with a sense of achievement that was otherwise missing in their lives:

> These days you don't feel like you're achieving anything. Now I'm in charge, I do admin, not engineering. When I'm working on the car, I can see that I'm doing something! (Clark)

Participants' heart-felt discussion of the satisfaction of making things with their own hands versus the estrangement they often feel towards aspects of contemporary life and work seem to provide strong evidence that Marx's ([1964](#mar64)) concept of _alienation_ may have a newfound relevance in the context of the post-modern, post-industrial world.

Participants also described the important role that working on their cars played in helping them deal with stressful events in their lives:

> [my wife] has been battling cancer for the past few years and I've been looking after her … mucking about with these old cars has kept me sane! (Webber)

> Whenever I've had stresses or problems in my business or family, I've come out here [to the garage] and worked on the cars. (Moss)

These findings suggest that the significance of embodied practices may have more to do with identity and constructions of the self than simply the completion of tasks. This relationship is one that warrants greater attention from information researchers.

## Conclusion

The study's findings show that enthusiast car restorers live in a complex information world, one in which the participants are _'knowing subjects … cultural experts'_ ([Talja, 1997, p.77](#tal97)) who have learned to effectively take on the challenge of a scarcity of information resources, at least as they are traditionally defined by information researchers and institutions. The authors hope that the study demonstrates the ways in which the emergent information practices research tradition, informed by an inter-disciplinary, theoretical and methodological approach, can provide information researchers with useful tools to understand the information practices of communities and contexts which have hitherto received relatively little attention.

The study's findings in relation to alienation, achievement and identity suggest that research into embodied practices may have a broader significance than has been hitherto recognised. In a world where questions around a perceived crisis in masculine identity and disaffection and depression amongst the ageing population are an increasingly common feature of our daily news, it may well be that leisure activities, especially those involving the development of embodied skills, will be recognised as having a useful social, even therapeutic function. If this is the case, then it will be even more important for information researchers and institutions to understand them more fully and to develop resources to help support their development.

</section>

<section>

## <a id="author"></a>About the authors

**Michael Olsson** is a Senior Lecturer in the School of Communication at the University of Technology Sydney. He has an MA (Information) and a PhD from the University Technology Sydney. He can be contacted at [Michael.Olsson@uts.edu.au](mailto:michael.olsson@uts.edu.au)

**Annemaree Lloyd** is a Professor in Swedish School of Library and Information Science, University of Boras, Sweden. She has a Master degree in Library and Information Science from Charles Sturt University, Australia and a PhD from the University of New England, Australia. Professor Lloyd can be contacted at [annemaree.lloyd@hb.se](mailto:annemaree.lloyd@hb.se)

</section>

<section>

## References

<ul>
<li id="bry08">Bryman, A. (2008). <em>Social research methods</em> (4th ed.). Oxford: Oxford University Press. </li>
<li id="buc91">Buckland, M. (1991). Information as thing. <em>Journal of the American Society of Information Science, 42</em>(5), 351-360. </li>
<li id="dav12">Davis, D. (2012). <em><a href="http://www.webcitation.org/6nulkPbzR)"> Automobile restoration</a>.</em> Retrieved from http://discoverahobby.com/learn/Automobile_Restoration
(Archived by WebCite&amp;reg;  at http://www.webcitation.org/6nulkPbzR) </li>
<li id="der99">Dervin, B. (1999). On studying information seeking and use methodologically: the implication of connecting metatheory to method.
<em>Information Processing &amp; Management, 35,</em> 727-750. </li>
<li id="der03">Dervin, B., Foreman-Wernet, L. &amp; Lauterbach, E. (2003). <em>Sense-making methodology reader: selected writings of Brenda Derwin.</em>
Cresskill, NJ: Hampton Press. </li>
<li id="fai03">Fairclough, N. (2003). <em>Analysing disclosure: textual analysis for social research.</em> London: Routledge. </li>
<li id="fou78">Foucault, N. (1978). Politics and the study of discourse. <em>Ideology and Consciousness, 3</em>(1), 7-26. </li>
<li id="ghe08">Gherardi, S. (2008). Situated knowledge and situated action: what do practice-based studies promise? In D. Barry and H. Hansen (Eds.), 
<em>The Sage handbook of new approaches in management and organization</em>(pp. 516-527). Thousand Oaks, CA: Sage Publications Ltd. </li>
<li id="ghe09">Gherardi, S. (2009). Introduction: the critical power of the 'practice lens'. <em>Management Learning, 40</em>(2), 115-128. </li>
<li id="god13">Godbold, N. (2013). Listening to bodies and watching machines: developing health information skills, tools and services for people living with chronic kidney disease. <em>Australian Academic Research Libraries, 44</em>(1), 14-28.</li>
<li id="his11">Historic Vehicle Research Institute (2011). <em><a href="http://www.webcitation.org/6t4WvzZ0f">The British historic vehicle movement: a £4 billion hobby</a>.</em> Retrieved from  http://www.fbhvc.co.uk/research/ (Archived by WebCite&amp;reg; at http://www.webcitation.org/6t4WvzZ0f)
</li><li  id="jul11">Julien, H., Pecoske, J. &amp; Reed, K. (2011). Trends in information behaviour research, 1999-2008: a content analysis. 
<em>Library &amp; Information Science Research, 33</em>(1), 19-24. </li>
<li id="kre01">Kress, G. &amp; van Leeuwen, T. (2001). <em>Multimodal disclosure: the modes and media of contemporary communication.</em> Oxford: Oxford University Press.</li>
<li id="lav91">Lave, J. &amp; Wenger, E. (1991) <em>Situated learning: legitimate peripheral participation.</em> Cambridge: Cambridge 
University Press. </li> 
<li id="llo07">Lloyd, A. (2007). <a href="http://www.webcitation.org/6t4XCC7lM"> Recasting information literacy as sociocultural practice: implications for library and information 
science researchers</a>. <em>Information Research, 12</em>(4), colis.34.  Retrieved from http://www.informationr.net/ir/12-4/colis/colis34.html 
(Archived by WebCite&amp;reg; at http://www.webcitation.org/6t4XCC7lM)						
</li><li id="llo10a">Lloyd, A. (2010a). <em>Information literacy landscapes: information literacy in education, workplace and everyday contexts.</em> Oxford: Chandos. </li>
<li id="llo10b">Lloyd, A. (2010b). <a href="http://www.webcitation.org/6lTJfDXxc">Corporeality and practice theory: exploring emerging research agendas for information literacy.</a> <em>Information Research, 15</em>(3), colis704. Retrieved from http://InformationR.net/ir/15-3/colis7/colis704.html (Archived by WebCite&amp;reg; at http://www.webcitation.org/6lTJfDXxc)  </li>
<li id="llo11">Lloyd, A. (2011). Trapped between a rock and a hard place: what counts as information literacy in the workplace and how is it conceptualized? 
<em>Library Trends, 60</em>(2), 277-296. </li>
<li id="llo14">Lloyd, A. (2014). Informed bodies: does the corporeal experience matter to information literacy practice? In C. Bruce, K. Davis,
H. Hughes, H Partridge and I. Stoodley (Eds.), <em>Information experience: approaches to theory and practice</em>, pp. 85-99). Bingley: Emerald Group Publishing  Limited. </li>
<li id="mar64">Marx, K. (1964). <em>Economic and philosophic manuscripts of 1844.</em> New York: International Publishers. </li> 
<li id="ols10">Olsson, M. (2010). The play's the thing: theatre professionals make a sense of Shakespeare. <em>Library and Information Science Research, 32</em>(4), 272-280. </li>
<li id="ols16">Olsson, M. (2016). Making sense of the past: the embodied information practices of field archaeologists. <em>Journal of Information Science, 42</em>(3), 410-419. </li>
<li id="pri07"> Prigoda, E. &amp; McKenzie, P. (2007). Purls of wisdom: a collectivist study of human information behaviour in a public library knitting group. <em>Journal of Documentation, 63</em>(1), 90-114. </li>
<li id="sav08">Savolainen, R. (2008). <em>Everyday information practices: a social phenomenological perspective.</em> Lanham, MD.: Scarecrow Press. </li> 
<li id="sch96"> Schatzki, T. (1996). <em>Social practices: a Wittgensteinian approach to human activity and the social.</em> Cambridge: Cambridge 
University Press. </li>
<li id="sei91">Seidman, I. (1991). <em>Interviewing as qualitative research: a guide for researchers in education and the social sciences.</em> New York: Teachers College Press. </li>
<li id="smi15">Smith, S. (2015, July 15). <a href="http://www.webcitation.org/6nulvXPe7">How this medieval machine turns flat metal into beautiful car bodies.</a> <em>Road &amp; Track</em>. Retrieved from http://www.roadandtrack.com/car-culture/classic-cars/a26081/lost-art-the-english-wheel/ (Archived by WebCite&amp;reg; at http://www.webcitation.org/6nulvXPe7) </li> 
<li id="tal97">Talja, S. (1997). Consulting 'information' and 'user' as research objects: a theory of knowledge formations as an alternative
to the information-man theory. In P. Vakkari, R. Savolainen, and B. Dervin (Eds.), <em>ISIC '96 Proceedings of an international conference on
information seeking in context</em> (pp. 67-80). London: Taylor Graham Publishing. </li>
<li id="tal05">Talja, S. (2005). The domain analytic approach to scholars' information practices. In K. E. Fisher, S. Erdelez &amp; L. McKechnie (Eds.), <em>Theories 
of information behavior</em> (pp. 123-27). Medford: NJ: Information Today, Inc. </li>
<li id="vei07">Veinot, T. C. (2007). 'The eyes of the power company': workplace information practices of a vault inspector. <em>The Library
Quarterly, 77</em>(2), 157-179. </li>
<li id="wil00">Wilson, T.D. (2000). Human information behavior. <em>Informing Science, 3</em>(1), 49-55. </li>
</ul>

</section>

</article>