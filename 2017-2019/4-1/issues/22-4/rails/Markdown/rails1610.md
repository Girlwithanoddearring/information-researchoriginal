<header>

#### vol. 22 no. 4, December, 2017

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016.

</header>

<article>

# Freedom of information and the right to know: tensions between openness and secrecy

## [Maureen Henninger](#author)

> **Introduction.** This study explores the notions of government openness and secrecy in public access to government documents under Australia’s Freedom of Information Act, specifically policy-related documents, to provide an overview of relevant trends and the tensions of conditional exemptions as they relate to government decision-making processes and the public interest.  
> **Method.** Based on two case studies there are several data sources: open government datasets, government annual reports, government agencies’ information disclosure logs, and two requests for information and associated correspondence.  
> **Analysis.** Frequency distributions were produced from the datasets to establish trends in information requests and to consider the impact of government policies on the outcomes. A qualitative analysis was done on the documents of the two case studies to explore the decision-making processes.  
> **Results.** Government policies impact the effectiveness of the Freedom of Information Act. Conditional exemptions are used to deny requests, rather than to facilitate disclosure. Conflict may arise between privacy issues and disclosure of policy matters.  
> **Conclusion.** The arbitrariness of information practices in the implementation of the Freedom of Information regime in Australia means that a balance has not been achieved between openness of government and secrecy.

<section>

## Introduction

Government accountability and the enabling of participative democracy through openness and transparency are claimed as the orthodoxy of the twenty-first century, built upon the twentieth century’s right to know and open government movements. They facilitate an open society in which there is a free flow of information enabling citizens to _‘evaluate critically the consequences of the implementation of government policies, which can then be abandoned or modified in the light of such critical scrutiny’_ ([Thornton, 2016, section 6](#tho16)). Few would disagree with the principle of openness of government; the ruled and the rulers recognise that the public record, government information, is the evidence of governance and democratic processes. But the public record is not automatically made public; citizens and governments throughout the world have assumed that some aspects of the decision-making of governments should remain closed to public scrutiny. This assumption establishes a continuous conflict between citizens and the state and within the state itself (represented by the elected officials and the bureaucracy) about what to disclose and what not to, or in other words, between openness and secrecy. It becomes, therefore, a struggle for government and the bureaucracy to find the middle ground between _‘open access to information [as] a default position unless there are compelling reasons to the contrary’_ ([Office of the Australian Information Commissioner, 2011](#oaic11)), and Weber’s pronouncement that _‘every bureaucracy seeks to increase the superiority of the professionally informed by keeping their knowledge and intentions secret’_ ([2009/1946, p. 233](#web09)).

Citizens have also become increasingly aware of the implications of governments collecting data about them as individuals. They have sought the right to access this information and to ensure its accuracy, through Freedom of Information and other channels. They have also sought to ensure that information about them is not disseminated to agencies and individuals whom have no right to it. In other words, a concern for the privacy of personal details has arisen and is now the topic of reform to the Privacy Act 1988 ([Australian Attorney-General’s Department, 2016](#aag16)).

Freedom of Information legislation is one mechanism by which governments acknowledge that in principle, access to the public record is not only reasonable, but is conducive to accountability, good policy decisions and economic development. As Patricia Wald, US judge and founding chair of the Open Society Justice Initiative, commented, Freedom of Information had to be invented: _‘because the match between the interests of those who exercise power and the interests of the citizens at large is far from perfect, politics cannot be left solely to the politicians’_ ([1984, p. 654](#wal84)). She might have added that providing access to the decision-making of governments should not be left to bureaucrats either.

While this study acknowledges that an important aspect of Freedom of Information legislation is to enable individual citizens to access and amend much of their personal information, its primary focus is government decision-making processes. It is concerned with the waxing and waning of government attitudes to openness and transparency by the politicians, who in a democracy, are the legitimate authority for setting the rules in the game of secrecy vs. openness in Australia and with the ways in which the bureaucrats, on behalf of the state, interpret and implement these rules. This study provides an overview of the outcome of Freedom of Information requests over the previous thirty-five years, and reports on two case studies comprised of instances of requests for information under the Freedom of Information regime in Australia and the outcomes of those requests. The premise of the requests is that the information concerned is part of the Australian Commonwealth Government’s public record, which is not classified and is not currently available, but access to which may be granted through Freedom of Information legislation. This study used datasets made available under the 2010 Freedom of Information reforms and the Freedom of Information annual reports. The data include the total number of requests since 1982, and since 2011, individual agency data on numbers of requests and their outcomes: granted, partially granted, refused, reasons for refusal (exemptions), and appeal requests.

</section>

<section>

## Freedom of information in Australia

While freedom of information had its early roots in the right to information movements harking back to the Enlightenment, it is generally acknowledged that the Swedish Freedom of the Press Act of 1766, offering _‘improved knowledge and appreciation of a wisely ordered system of government’_ ([Mustonen, 2006, p. 8](#mus06)), is the world’s first Freedom of Information Act. In contemporary times, the first Freedom of Information legislation was introduced by the United States in 1966\. In Australia throughout the 1960s, right to know campaigns and discussions about government secrecy had proliferated ([Australian Law Reform Commission, 1995](#alrc95)). It was, in part, the secrecy of the decision-making processes and prosecution of the tragedy of the war in Vietnam that led Gough Whitlam, a former Australian prime minister, to declare _‘the Australian Labor Party will build into the administration of the affairs of this nation machinery that will prevent any government, Labor or Liberal, from ever again cloaking your affairs under <u>excessive and needless secrecy</u>’_ ([Whitlam, 1972](#whi72); emphasis added). In 1978 a Bill was introduced and finally passed as an Act in April 1982, making Australia the second country in the world to enact Freedom of Information legislation.

Hazell and Worthy ([2010](#haz10)), comparing the first three years of Freedom of Information in several countries, noted that Australia’s performance, _‘despite high levels of use and disclosure, suffer[s] from a high level of appeals, a lack of political support and consequent restrictive reform’_ (p. 358). By 1995 it became apparent that the initial optimism and strong support for Freedom of Information had faded to neglect because of factors such as substantial amendments, increase of application fees, and politically damaging requests ([Terrill, 1998](#ter98)). In subsequent years, different governments initiated inquiries by the Australian Law Reform Commission to examine the 1982 Act. The purpose and titles of these inquiries are indicative of the conflict between openness and secrecy. The stated purpose of the 1995 Australian Law Reform Commission inquiry was to _‘improve the quality of decision making by government agencies in both policy and administrative matters by removing unnecessary secrecy surrounding the decision-making process’_ ([1995, p. v](#alrc95)), while subsequent inquiries were titled _Keeping Secrets: the Protection of Classified and Sensitive Information_ (2004); and _Secrecy Laws and Open Government_ (2009). Each bears evidence that every change has an impact, since all considerations surrounding the 1978 Bill, _‘fail[ed] to engage with the extent passage of the [Freedom of Information] Act might itself encourage further change and the implications that may have’_ ([Stewart, 2015, p. 104](#ste15)).

The most far-reaching changes, recommended by the 2009 inquiry, were incorporated into the Freedom of Information Amendment (Reform) Act 2010, and included:

*   removal of Freedom of Information application fees and internal review fees;
*   removal of ministerial conclusive certificates (vetos or blanket exemptions), which were replaced by two categories of exemptions to disclosure: 1) unconditional exemptions for sensitive material and 2) conditional exemptions, based on a single public interest test; and
*   the concomitant Australian Information Commissioner Act 2010, creating the Office of the Australian Information Commissioner (OAIC) with responsibility for the implementation of the new Freedom of Information framework.

These reforms, particularly concerning conditional exemptions and the establishment of the Office of the Australian Information Commissioner, are the basis of the individual case studies testing the reality of the government’s commitment to openness. But first this paper examines the general pattern of trends that clearly shows the impact and implications of government policy changes.

</section>

<section>

## General trends

An analysis of the trends in number of requests, and the approvals in full or in part, and refusals gives an impression of the policies and concerns that have influenced the Freedom of Information regime. In the early years of Freedom of Information legislation, after a slow start in November 1982, there was a rapid rise in the number of requests until 1985-1986 (a total of 94,271 for the four years), at which point there was a steep decline to one of the lowest numbers overall: 23,543 in 1989-1990, second only to the historical low of 21,587 in 2009-2010 (Figure 1). In part, this decline can be attributed directly to the introduction of fees for applications and processing charges _‘on the basis that users of the legislation should be required, where appropriate, to contribute towards meeting its cost’_ ([Australian Attorney-General's Department, 1987, p. 72](#aag87)). The decision to introduce Freedom of Information fees appears to have had a similar effect in other countries, for example, in Ireland after their introduction in 2003, the numbers of requests fell overall by 50% and by 75% for non-personal information ([Information Commissioner of Ireland, 2004, p. 1](#ici04)). In 1991 a cap was put on the fee for personal information requests, possibly accounting for the steady rise of requests until 1996\. Finally, a steady rise is seen again from 2010 when all fees were removed for application requests, for internal reviews, and requests to amend or annotate personal records; processing charges for personal information were removed and for all other requests, and the first five hours incurred no charge ([Office of the Australian Information Commissioner, 2012](#oaic12)).

<figure>

![Factors affecting general patterns of all Freedom of Information requests 1982-2015
Data source: Freedom of Information requests, costs and charges, 1982-2015](../rails1610fig1.jpg)

<figcaption>Figure 1: Factors affecting general patterns of all Freedom of Information requests 1982-2015 Data source: Freedom of Information requests, costs and charges, 1982-2015</figcaption>

</figure>

However, the fees and charges argument cannot explain the dramatic decrease in the use of Freedom of Information from 2005-2006 until the 2010 reforms. A probable reason is that greater access to information, particularly personal information, was becoming available through other means as a result of eGovernment policies. For example, in 2007-2008 the Department of Immigration and Citizenship registered a 47% drop in requests, Centrelink, 18%, and Department of Veterans Affairs, 9% (a combined total decrease of 9,889 requests) as information could be requested and received through online and other informal channels ([Australian Attorney-General's Department, 2008](#aag08)). Thus, it can be seen that interpretation of the data needs a cautious approach. The number of requests appears to rise when fees are lowered and to fall when other avenues of access to information, especially personal information, are available.

</section>

<section>

### Openness and secrecy

When early advocates for freedom of information legislation spoke of the removal of excessive secrecy, it is assumed that, in general, they were referring to access to policy-related documents and decisions. However, Freedom of Information requests also included requests for access to personal information, and data collection did not separate policy-related information requests from requests for personal information until 2000\. Figure 2 compares the number of requests for policy-related information with requests for personal information. Although the number of requests for personal information is far greater than those for policy-related information, this section focuses on policy-related requests made between 2000 and 2015.

<figure>

![Comparison of numbers of personal and policy-related Freedom of Information requests 2000-2015
Data source: Freedom of Information requests, costs and charges, 1982-2015](../rails1610fig2.jpg)

<figcaption>Figure 2: Comparison of numbers of personal and policy-related Freedom of Information requests 2000-2015 Data source: Freedom of Information requests, costs and charges, 1982-2015</figcaption>

</figure>

Trends of policy-related requests, while similar in the period after 2005 (with the exception of 2008-2009 already noted) are quite different. The period from 2000 to 2005 shows the number of requests to be relatively stable, in the range of 3361-3766; the more obvious dissimilarity is between the personal and policy-related requests (Figure 3). While both show steady increases in the period 2009-2014, and taking into consideration, as noted above, the availability of access through other means, the increase in policy-related requests (109%) far outstripped the increase for personal information (21%). Finally, in the last year policy requests decrease while personal requests continue to rise.

<table><caption>Table 1: Comparative increases in personal and policy-related requests 2009-2014 Data source: Freedom of Information requests, costs and charges, 1982-2015</caption>

<tbody>

<tr>

<th>Year</th>

<th>Personal</th>

<th>% Increase</th>

<th>Policy-related</th>

<th>% Increase</th>

</tr>

<tr>

<td>2009-2010</td>

<td>18,823</td>

<td>–</td>

<td>2,764</td>

<td>–</td>

</tr>

<tr>

<td>2013-2014</td>

<td>22,690</td>

<td>21%</td>

<td>5,773</td>

<td>109%</td>

</tr>

<tr>

<td>2014-2015</td>

<td>30,297</td>

<td>34%</td>

<td>5,253</td>

<td>-9%</td>

</tr>

</tbody>

</table>

This dramatic increase in requests for policy-related information, coinciding as it does with Freedom of Information reforms that ended ministerial conclusive certificates (by which a minister was empowered to establish a document exempt from disclosure), possibly reflects a public perception that the reforms would result in greater government openness. But the increase may also have been event-driven since this was the time when the number asylum seekers arriving by boat began to rise. An examination of requests made to individual agencies during this period (2011-2015) shows that the Immigration portfolio received more requests than any other department (Table 2).

<table><caption>Table 2: Top ten agencies by number of policy-related requests 2011-2015 Data sources: Freedom of Information Annual Returns 2000-2015</caption>

<tbody>

<tr>

<th>Agency</th>

<th>No. of requests</th>

</tr>

<tr>

<td>Immigration portfolio</td>

<td>2,435</td>

</tr>

<tr>

<td>Australian Taxation Office</td>

<td>2,019</td>

</tr>

<tr>

<td>Health portfolio</td>

<td>1,217</td>

</tr>

<tr>

<td>Trade Marks Office</td>

<td>1,137</td>

</tr>

<tr>

<td>Australian Securities and Investments Commission</td>

<td>823</td>

</tr>

<tr>

<td>Department of Defence</td>

<td>813</td>

</tr>

<tr>

<td>Attorney-General's Department</td>

<td>708</td>

</tr>

<tr>

<td>Department of the Prime Minister and Cabinet</td>

<td>682</td>

</tr>

<tr>

<td>Department of the Treasury</td>

<td>657</td>

</tr>

<tr>

<td>Department of Foreign Affairs and Trade</td>

<td>555</td>

</tr>

</tbody>

</table>

The question needs to be asked: are some requests treated more openly than others? Or to put it another way, what are the reasons that some information is more likely to be provided than not? And are there power factors entrenched in these judgements and outcomes? If the intent of reforms of 2019-2010 was to emphasise _‘proactive publication of government information and openness as the default’_ ([McMillan and Popple, 2012, p. 1](#mcm12)), then the emphasis is on access to policy-related information. But over the entire period that statistics have differentiated personal from policy-related requests, an average of 72% of personal requests have been granted in full, in comparison to 41% of policy-related requests (Table 3), making it difficult to dispute the argument that _‘most governments like to be judged on how much personal information they released under their [Freedom of Information] regimes’_ (Rick Snell, interviewed by [Merritt, 2007](#mer07)).

<table><caption>Table 3: Average outcomes for personal and policy-related requests 2000-2015 Data sources: Freedom of Information Annual Returns 2000-2015</caption>

<tbody>

<tr>

<th>Decision</th>

<th>Personal</th>

<th>Policy-related</th>

</tr>

<tr>

<td>Granted in full</td>

<td>72%</td>

<td>41%</td>

</tr>

<tr>

<td>Partially granted</td>

<td>22%</td>

<td>40%</td>

</tr>

<tr>

<td>Refused</td>

<td>6%</td>

<td>19%</td>

</tr>

</tbody>

</table>

Since the 2010 reforms, it is apparent the situation has not changed. While there is an overall decline in requests that are fully granted, personal information is released on average more than twice as often as policy information (63% to 28%), and the steep rise in refusals of policy requests is three times the rate of personal ones, 29% to 7% (Figure 3).

<figure>

![Comparative outcome for personal and policy-related requests 2000-2015
Data sources: Freedom of Information Annual Returns 2000-2015](../rails1610fig3.jpg)

<figcaption>Figure 3: Comparative outcome for personal and policy-related requests 2000-2015 Data sources: Freedom of Information Annual Returns 2000-2015</figcaption>

</figure>

On the other hand, since the reforms, data for partially-granted requests appear to contradict the evidence of secrecy, particularly for policy documents, which on average is significantly higher (43%) than for personal documents (29%); see Figure 4.

<figure>

![Percentage of total number of requests that have been partially granted 2000-2015
Data sources: Freedom of Information Annual Returns 2000-2015](../rails1610fig4.jpg)

<figcaption>Figure 4: Percentage of total number of requests that have been partially granted 2000-2015\. Data sources: Freedom of Information Annual Returns 2000-2015</figcaption>

</figure>

However, what is not obvious from these data is the percentage of _content_ released, since disclosure is subject to conditional exemptions and determined by the public interest test.

</section>

<section>

## Exploring exemptions and the public interest

Freedom of Information legislation has always set out types of information that are exempt from disclosure. From 1982 to 2010, government ministers could also exercise a veto (conclusive certificates) over the disclosure of information. It has been speculated that the high number of appeals was a result of a heavy use of the veto that could be exercised by a single minister ([Hazell and Worthy, 2010](#haz10)). While successive reforms attempted to dilute ministerial and bureaucratic power to enforce secrecy conclusive certificates and exemptions, it was the 2010 reforms that introduced an additional category of conditional exemptions: conditional on a public interest test.

<table><caption>Table 4: Types of documents that constitute exemptions under the Freedom of Information (Reform) Act 2010</caption>

<tbody>

<tr>

<th>Unconditionally exempt</th>

<th>Conditionally exempt</th>

</tr>

<tr>

<td>s 33 affecting national security, defence or international relations</td>

<td>s 47B Commonwealth-State relations</td>

</tr>

<tr>

<td>s 34 cabinet documents</td>

<td>s 47C deliberative processes</td>

</tr>

<tr>

<td>s 37 affecting enforcement of law and protection of public safety</td>

<td>s 47D financial or property interests of the Commonwealth</td>

</tr>

<tr>

<td>s 38 secrecy provisions</td>

<td>s 47E certain operations of agencies</td>

</tr>

<tr>

<td>s 42 subject to legal professional privilege</td>

<td>s 47F personal privacy</td>

</tr>

<tr>

<td>s 45 containing material obtained in confidence</td>

<td>s 47G business (to which s 47 applies)</td>

</tr>

<tr>

<td>s 45A Parliamentary Budgetary Office documents</td>

<td>s 47H research</td>

</tr>

<tr>

<td>s 46 which would be contempt of Parliament or court</td>

<td>s 47J the economy</td>

</tr>

<tr>

<td>s 47 disclosing trade secrets or commercially valuable information</td>

<td>s 47A electoral rolls and related documents</td>

</tr>

</tbody>

</table>

The concept of the public interest is fuzzy, and its meaning has changed across time, political regimes, and democratic contexts (Box, 2007). The term is not defined in either the original Act or in the current one, but is left _‘necessarily broad and nonspecific because what constitutes the public interest depends on the particular facts of the matter and the context in which it is being considered’_ ([Office of the Australian Information Commissioner, 2014, p. 3](#oaic14)). Thus the lack of transparency about how this test remains and there is still the possibility of a bias toward secrecy. Of the ten highest categories of all exemptions in the past five years, the top four (personal privacy, certain operations of agencies, business, and deliberative processes) were conditionally exempt and therefore required to be subjected to a single public interest test (Figure 5).

<figure>

![The top ten categories of exemptions for policy-related documents 2011-2015
Data sources: Freedom of Information Annual Returns 2000-2015](../rails1610fig5.jpg)

<figcaption>Figure 5: The top ten categories of exemptions for policy-related documents 2011-2015 Data sources: Freedom of Information Annual Returns 2000-2015</figcaption>

</figure>

With regard to the public interest test, the Australian Law Reform Commission concluded that it is _‘an amorphous concept [and its determination] is essentially non-justiciable and depends on the application of a subjective rather than an ascertainable criterion’_ ([1995, para 8.13](#alrc95)). The Office of the Australian Information Commissioner ([2014](#oaic14)) issued guidelines to help bureaucrats in the assessment of non-disclosure in the public interest,

> to conclude that, on balance, disclosure of a document would be contrary to the public interest is to conclude that the benefit to the public resulting from disclosure is outweighed by the benefit to the public of withholding the information. The decision maker must analyse, in each case, whereon balance the public interest lies, based on the particular facts of the matter at the time the decision is made. ([para 6.9](#oaic14))

Additionally, the Office noted that

> the inclusion of the exemptions and conditional exemptions in the [Freedom of Information] Act recognises that harm may result from the disclosure of some types of documents in certain circumstances; for example, where disclosure could prejudice an investigation, unreasonably affect a person’s privacy or reveal commercially sensitive information. ([para 6.26](#oaic14))

</section>

<section>

## The case studies

The following case studies have been chosen to focus on some ways in which the public interest test has been used to refuse or only partially grant a request. They show the conflicts among different groups: the lawmakers and their intent, the bureaucrats who handle the requests, and those making the requests.

Each case is a request for documents that are conditionally exempt and is therefore couched in the framework that disclosure is in the public interest. The first examines a request for documents showing the processes of deliberation concerning a new policy decision (the conditional exemption of deliberative processes was used on an average of 267 times a year since 2010). The second examines the conflict between a government agency and the media and the most heavily used conditional exemption, personal privacy.

</section>

<section>

### A failure of the public interest test

In the 2014-2015 budget, the Commonwealth Government, citing the benefit of saving $10.2 million over four years, announced that from January 1st, 2015, the Office of the Australian Information Commissioner’s status as a Financial Management and Accountability Act 1997 agency would cease and funding for ongoing functions would be transferred to other agencies, and that new arrangements for privacy and regulation of Freedom of Information would commence from that date ([Australian Government, 2014](#ag14)). In October 2014, it introduced a bill to repeal the Australian Information Commissioner Act 2010, including the abolition of Office of the Australian Information Commissioner, moving all responsibilities for Freedom of Information to the Administrative Appeals Tribunal and to the Attorney-General. When the Bill had not passed the Senate by the end of 2014, in January 2015 a Freedom of Information request was made to Office of the Australian Information Commissioner for

> documents concerning discussions with the Attorney General's Department about the conduct of OAIC functions from 1 January 2015 including proposals put to or received from the Department concerning funding and staffing, and any agreement or understanding reached on these and related matter. . . The office is a key element in the Freedom of Information framework and in the exercise of citizen rights conferred by the act. The OAIC describes the information commissioner functions as “designed to ensure maximum coordination, efficiency and transparency in government information policy and practice”. Disclosure of documents concerning discussion about the conduct of these functions would advance public debate on this topic of current importance. ([Timmins, 2015](#tim15))

In March 2015, the Freedom of Information officer determined that sixty-four documents were relevant; these were emails between the Office of the Australian Information Commissioner and the departments of the Attorney-General, the Prime Minister and Cabinet and the Australian Human Rights Commission. None were granted in full. Twenty were redacted as irrelevant material, under s 22(1). The other forty-four emails were considered subject to the deliberative processes exemption s 47C; of these, thirteen were refused outright and thirty-one were heavily redacted. Under the public interest test, the factors cited were:

> the Bill has not passed through parliament [and] disclosure of the documents could reasonably be expected to impact on the ability of the OAIC to obtain full opinions and recommendations from relevant agencies [which] factors against disclosure are significant and at this point in time and outweighs [sic] the factors in favour of disclosure. ([Timmins, 2015)](#tim15)

This decision apparently equated the fact that the Bill had not yet passed to the guideline _‘where disclosure could prejudice an investigation’_. This decision on non-disclosure contradicts the government’s default position of openness, and as argued by Paterson negates the public’s need for access _‘to be able to participate meaningfully in, or to be able to understand and evaluate the decision-making of government agencies’_ ([2009, p. 3](#pat09)). The deliberations concerning the abolition of the government agency that by legislation is responsible for Freedom of Information would seem to be very much in the public interest. But an examination of the documents redacted under the conditional exemption of deliberative processes shows that this was not the judgement of the Freedom of Information officer. In fact, the information provided is unlikely to advance transparency and openness in this matter since, with the exceptions of communication pleasantries and the email trails, all relevant content is drowned in black (Figure 6).

<figure>

![Examples of Freedom of Information requests redacted under conditional exemption 47C
Source: Right to Know](../rails1610fig6.jpg)

<figcaption>Figure 6: Examples of Freedom of Information requests redacted under conditional exemption 47C Source: Right to Know</figcaption>

</figure>

At the time, given the concerns of the government, one might have speculated that the emails would disclose an overriding preference for saving money by rescinding legislation and transferring ultimate responsibility to the Attorney-General for the administration of the Freedom of Information, instead of encouraging openness of government.

There is a post script note to this case study: when the Senate adjourned the second reading in October 2014, it referred the matter to the Senate Legal and Constitutional Affairs Legislation Committee. This bill was one of those that lapsed because of the prorogation of the first session of 44th Parliament on April 17, 2016\. One might speculate that the Senate was of the opinion that any attempt to abolish the Office of the Australian Information Commissioner failed the public interest test. Under the new Prime Minister, the Office was re-funded.

</section>

<section>

### Journalism and the perception of secrecy

In democratic societies there is little doubt of the importance of journalism in holding governments accountable. Various journalistic endeavours culminated in Harold Cross’s 1953 book _The People’s Right to Know_, which is considered to be the language of the first modern Freedom of Information legislation, the US Freedom of Information Act 1966\. However, in the early Freedom of Information regimes of Australia and New Zealand there appeared to be little interest expressed by journalists in using the Act to obtain documents concerning government actions ([Australian Attorney-General's Department, 1988](#aag88); [Morrison, 1997](#mor97)). Twenty years later, Stephen Lamble commented that the _‘Australian media generally seems to have given up the fight in relation to [Freedom of Information]’_ ([2004, p. 8](#lam04)) and studies of journalists’ use of Freedom of Information conclude that there is heavy redaction to the point of uselessness ([Bluemink and Brush, 2005](#blu05); [Cuillier, 2011](#cui)), a state of affairs that re-enforces a perception of secrecy by the government and the worthlessness of the developed mechanism in supporting transparency.

This case is concerned with information related to the management of the asylum-seeking process. Over the past fifteen years the Department of Immigration has been criticised for its secrecy over asylum seekers who have arrived by boat, and their detention and treatment in offshore facilities on Nauru and Manus Island; a simple search on Factiva for Australian newspaper coverage since 2011 yielded over 325 newspaper articles that used phrases such as _‘the secrecy surrounding its maritime operations’_, _‘inordinate and unacceptable secrecy’_, and _‘shrouded by a veil of secrecy’_. Even the Select Committee on the Recent Allegations Relating to Conditions and Circumstances at the Regional Processing Centre in Nauru, commenting on the department’s contractual arrangements, concluded that given the _‘pervasive culture of secrecy which cloaks most of the department's activities in relation to the Nauru RPC, it believes that a far greater level of scrutiny, transparency and accountability is required’_ ([2015, p. 124](#assc15)). However the department’s Freedom of Information annual returns present a different picture, giving a very positive view of openness. In the years 2011-2015 only 395 of all policy-related requests (1,995) were refused, an average of 20%. Again, an analysis of requests granted either in full or partially is a better measure for openness than the number refused.

There are two constraints affecting this case. The data available for this case are not consistent during the period 2011-2015 and the disclosure logs do not identify requests made by the media. The logs for 2011-2013 give only the disclosure date and a description of the documents requested with no indication of the outcome. Nevertheless, the 2014 and 2015 show a total of 186 items, of which fifty-eight were granted in full and 128 were partially granted. Of these, sixteen requests contained the words Nauru and/or Manus in the description. Six requests were granted in full, none concerning the treatment of asylum seekers, and the other ten were mostly redacted under the personal privacy conditional exemption s 47(F), confirming, as would be expected, the dominant use of this category within the department (Figure 7). Not coincidently the steep increase in the number of refusals for access after 2011-2012 corresponds with the post-2010 election campaigns when the issue of asylum seekers and the management of the process had been key campaigning matters.

<figure>

![Top 4 exemption categories used across Immigration portfolio 2011-2015
Data sources: Freedom of Information Annual Returns 2000-2015](../rails1610fig7.jpg)

<figcaption>Figure 7: Top 4 exemption categories used across Immigration portfolio 2011-2015 Data sources: Freedom of Information Annual Returns 2000-2015</figcaption>

</figure>

The second part of this case provides an illustration of what Morrison ([1997](#mor97)) calls bureaucratic game-playing strategies to circumvent disclosure to journalists; this not only confirms public perception of secrecy but demonstrates journalists’ retaliatory strategies.

In the mid-2013 a group of independent journalists used _Right to Know_, a Freedom of Information project of Open Australia Foundation, to lodge 121 separate requests for offshore detention incident reports. All were refused. The grounds for refusal were a _‘practical refusal’_ under s 24(2). Under this mechanism, all requests could be treated as one since they _‘relate to documents, the subject matter of which is substantially the same’_. Two applicants, Kate (last name unknown) and Paul Farrell, were also advised that the requests met a second practical refusal reason under s 24AA: since all requests were to be treated as one, it was estimated they would take 255 hours ([Kate, 2013](#kat13)) and fifty-seven hours ([Farrell, 2013](#far13)) to process, respectively, both figures far above the regulatory cap of forty hours. Then in February 2016 another request was lodged for documents concerning _‘specific medical services’_ incidents, and was partially granted ([Cooney, 2016](#coo16)). The Freedom of Information officer’s decision, communicated as _‘I have interpreted the scope of your request to exclude personal identifiers (such as names), and as such, IHMS [company responsible for medical services] did not provide this information’_, was to release four documents (126 pages) of the IHMS complaints register. Indeed the register has no personal identifiers, but nevertheless every page has the same two columns redacted: the description of the complaint, and the subsequent comments, rendering the documents useless to the requestor.

By contrast, in mid-2016 Paul Farrell, now with the Guardian newspaper, received 2,000 un-redacted leaked incident reports of over 8,000 pages (known as the Nauru Papers) which set out _‘the assaults, sexual abuse, self-harm attempts, child abuse, and living conditions endured by asylum seekers held by the Australian government, painting a picture of routine dysfunction and cruelty’_ ([Farrell, Evershed and Davidson, 2016](#far16b)). To responsibly protect the personal privacy of the detainees (adopting the Freedom of Information conditional exemption s 47F(1)) The Guardian’s journalists removed the names of all asylum seekers and staff; personal identification numbers of asylum seekers (their six-digit _‘boat arrival numbers’_); ages of the asylum seekers named in reports; signatures of detention staff; nationalities with small population groups; residential tent numbers; and in some cases further identifying information ([Farrell and Evershed, 2016](#far16a)). Figure 8 contrasts one of these leaked documents, redacted equivalently to comply with s 47F(1), with its probable listing in the complaints register of IHMS.

<figure>

![Comparison of uncensored (left) and leaked, redacted (right) Freedom of Information documents 
Sources: Right to Know; The Guardian](../rails1610fig8.jpg)

<figcaption>Figure 8: Comparison of uncensored (left) and leaked, redacted (right) Freedom of Information documents Sources: Right to Know; The Guardian</figcaption>

</figure>

While the publication of the heavily redacted documents might raise further questions concerning privacy and the public interest, it is hard to argue with Ester’s ([2006](#est06)) comment that _‘one consequence of ineffective or non-existent Freedom of Information... laws is to force political journalists to depend on oral and/or “brown envelope” leaks’_ (p. 162).

</section>

<section>

## Discussion

These two case studies demonstrate the inherent tensions and inevitability of conflicts and power struggles in the concept, perhaps ideal, of open government, which favours transparency and a well-informed citizenry. On the continuum from secrecy to openness there are many intersecting points: good governance, accountability, transparency, privacy, confidentiality, national interest, public interest, and self-interest, all of which collide in the mechanism of exemptions to Freedom of Information. The number of reviews undertaken since the legislation was introduced is evidence of the tensions inherent in the Freedom of Information mechanisms. The major recommendation of the recent Hawke review was _‘that a comprehensive review of the [Freedom of Information] Act be undertaken’_ ([Hawke, 2013, p. 4](#haw13)), particularly singling out the question of exemptions that are

> a feature of all [Freedom of Information] legislation, recognising that the right of access provided by the legislation is not absolute. The purpose of exemptions is to balance the objective of providing access to government information against legitimate claims for the protection of sensitive material. The exemptions provide the confidentiality necessary for the proper workings of government and protection of the confidences and privacy of those who have dealings with government or about whom information is collected by government. ([Hawke, 2013, p. 39](#haw13))

This study has shown that the application of the conditional exemptions is subject to arbitrary decision-making or different interpretations of the rules, notwithstanding the mechanism of internal review, introduced by the 2010 reforms, designed to lessen this; in four recent years, 2011-2015, an average of 27% of the original decisions were changed after application for an internal review (Table 5).

<table><caption>Table 5: Applications and outcomes for internal reviews of policy-related requests Data sources: Freedom of Information Annual Returns 2011-2015</caption>

<tbody>

<tr>

<th>Year</th>

<th>Applications</th>

<th>Granted in full</th>

<th>Granted in part</th>

<th>Granted after deferment or in different form</th>

<th>% decision changed</th>

</tr>

<tr>

<td>2011-2012</td>

<td>273</td>

<td>18</td>

<td>53</td>

<td>11</td>

<td>30%</td>

</tr>

<tr>

<td>2012-2013</td>

<td>262</td>

<td>16</td>

<td>65</td>

<td>4</td>

<td>33%</td>

</tr>

<tr>

<td>2013-2014</td>

<td>284</td>

<td>10</td>

<td>47</td>

<td>2</td>

<td>21%</td>

</tr>

<tr>

<td>2014-2015</td>

<td>221</td>

<td>12</td>

<td>35</td>

<td>4</td>

<td>23%</td>

</tr>

</tbody>

</table>

This proportion shows that the concept of the public interest is open to interpretation and there is always room for a cynical assessment that a particular interpretation is intended to over-ride any possibilities of transparency and to prevent the flow of information as intended in the idealist concept of Freedom of Information, as comments on two exemptions will demonstrate.

The use of s 24, the _‘practical refusal’_ mechanism, is one of the conditional exemptions that appear to be used to place the interests of bureaucrats over those of the informed citizenry. That the nature of fees for Freedom of Information is contentious has already been mentioned. The Office of the Australian Information Commissioner ([2012](#oaic12)) concedes that agencies and ministers have the option to impose _‘access charges [as] a way of controlling and managing demand for documents … [and for] defraying some of the cost of [Freedom of Information] to government’_ (p. 1). Indeed the Office review into fees highlighted the _‘difficulties agencies face in using s 24AB of the Freedom of Information Act (the “practical refusal” mechanism)’_ to achieve a balance of these competing interests (p. 4). Between 2011 and 2015 the number of practical refusals by the Immigration Department increased by over 560% and of those, in 2014-2015 only 8% were subsequently processed, in contrast to 26% across all agencies.

<table><caption>Table 6: Practical refusals for policy-related requests (*presumably, nine requests had been held over from the previous year) Data sources: Freedom of Information Annual Returns 2011-2015</caption>

<tbody>

<tr>

<th>Year</th>

<th colspan="2">Notification of refusal</th>

<th colspan="2">Subsequently processed</th>

</tr>

<tr>

<th></th>

<th>All agencies</th>

<th>Immigration portfolio</th>

<th>All agencies</th>

<th>Immigration portfolio</th>

</tr>

<tr>

<td>2011-2012</td>

<td>193</td>

<td>35</td>

<td>54 (28%)</td>

<td>0 (0%) *</td>

</tr>

<tr>

<td>2012-2013</td>

<td>466</td>

<td>40</td>

<td>206 (44%)</td>

<td>49 (100%)</td>

</tr>

<tr>

<td>2013-2014</td>

<td>714</td>

<td>150</td>

<td>182 (25%)</td>

<td>15 (10%)</td>

</tr>

<tr>

<td>2014-2015</td>

<td>896</td>

<td>233</td>

<td>230 (26%)</td>

<td>19 (8%)</td>

</tr>

</tbody>

</table>

Based on these data, the practical refusal mechanism appears to be a very successful strategy for preventing the flow of information to requestors, and thus preserving notions of secrecy rather than supporting openness.

The protection of personal data is fundamental in liberal western democracies to prevent abuse of individual privacy by state powers, organisations and individuals. If one of the aims of freedom of information is to enable transparency, then it must be balanced against the right to privacy; transparency, it is suggested by Richard Oliver is _‘flash point at the intersection of the public's right to know and the individual's or organisation's right to privacy’_ ([2004, p. x](#oli04)). While the 2010 replacement of conclusive certificates with conditional exemptions dependent on a single public interest test advances the balance between secrecy and openness, this is an imperfect solution to power conflicts about the release of policy-related information.

The cynical view that governments actively work to prevent the more liberal aspects of Freedom of Information is understandable in light of the examples of the ways in which exemptions and other mechanisms prevent the disclosure of information. The following statement by an Attorney-General, when in opposition, is revealing:

> the true measure of the openness and transparency of a government is found in its attitudes and actions when it comes to freedom of information. Legislative amendments, when there is need for them, are fine, but governments with their control over the information in their possession can always find ways to work the legislation to slow or control disclosure. (Brandis, in [Australian Senate, 2009, p. 4849](#as09))

</section>

<section>

## Conclusion

This paper concludes that in Australia the Freedom of Information regime has not achieved a balance between openness and secrecy, a balance that is fundamental to a healthy democracy. Patterns of Freedom of Information requests show that successive governments’ enthusiasm for releasing and amending personal information, while an important part of the legislation, does little to advance the flow of information about government decision-making processes and the associated accountability and transparency in government. In contrast, disclosure of policy-related materials, arguably the central purpose of Freedom of Information, appears less zealous, suggesting a _‘distaste for openness is part of a larger concern about the proliferation of constraints on executive authority’_ ([Roberts, 2006, p. 19](#rob06)). There have been several attempts to resolve the tensions through reforms to the Act by counterbalancing constraints with exemptions, without which Falconer, the advocate for the UK Freedom of Information legislation, suggests _‘good government would be impossible’_ ([2004, para. 20](#fal04)). However there is strong evidence of the power of politicians and bureaucrats in the workings of the Freedom of Information regime. The situation in Australia could be summarised as follows: _‘it is easier for governments to demonstrate adherence to openness principles by maintaining [Freedom of Information] laws and then to loosen the constraints imposed by those laws through less visible administrative actions’_ ([Roberts, 2006, p. 317](#rob06)).

</section>

<section>

## <a id="author"></a>About the author

Maureen Henninger is a senior lecturer in Information and Knowledge Management at the University of Technology Sydney. Her teaching and research interests are in information retrieval, information management and data journalism, with particular reference to search in the digital environment and has written books on Internet searching and the hidden Web. More recently, she has been working in the areas of accessibility and preservation of public sector information and open government data and its visualisation for analytical purposes.​ She can be contacted at [Maureen.Henninger@uts.edu.au](mailto:maureen.henninger@uts.edu.au)

</section>

<section>

## References

<ul>
<li id="aag87">Australian Attorney-General's Department. (1987). <em>Freedom of Information Act 1982: 1986-1987 annual report by the Attorney-General on the operation of the Act.</em> Canberra, ACT: Commonwealth of Australia.</li>
<li id="aag88">Australian Attorney-General's Department. (1988). <em>Freedom of Information Act 1982: 1987-1988 Annual report by the Attorney-General on the operation of the Act.</em> Canberra, ACT: Commonwealth of Australia.</li>
<li id="aag08">Australian Attorney-General's Department. (2008). <em>Freedom of Information Act 1982: annual report 2007-2008.</em> Canberra, ACT: Commonwealth of Australia.</li>
<li id="aag16">Australian Attorney-General's Department. (2016). <em><a href="http://www.webcitation.org/6tOYIjfdd">Amendment to the Privacy Act to further protect de-identified data</a></em> [Press release]. Retrieved from https://www.attorneygeneral.gov.au/Mediareleases/Pages/2016/ThirdQuarter/Amendment-to-the-Privacy-Act-to-further-protect-de-identified-data.aspx (Archived by WebCite&reg; at http://www.webcitation.org/6tOYIjfdd)</li>
<li  id="ag14">Australian Government. (2014). <em><a href="http://www.webcitation.org/6tOZMeaBQ">Budget measures: budget paper no. 2 2014-15.</a></em> Retrieved from http://www.budget.gov.au/2014-15/content/bp2/download/BP2_consolidated.pdf. (Archived by WebCite&reg; at http://www.webcitation.org/6tOZMeaBQ).</li>
<li id="alrc95">Australian Law Reform Commission. (1995). <em><a href="http://www.webcitation.org/6tPhJhXGb">Open government: a review of the federal Freedom of Information Act 1982</a></em> (ALRC Report 77). Canberra, ACT: Australian Government Publishing Services. Retrieved from https://www.alrc.gov.au/sites/default/files/pdfs/publications/ALRC77.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tPhJhXGb).</li>
<li id="as09">Australian Senate. (2009, August 13). <em>Debates.</em>
</li><li id="assc15">Australian Senate Select Committee on the Recent Allegations Relating to Conditions and Circumstances at the Regional Processing Centre in Nauru, (2015). <em><a href="http://www.webcitation.org/6tOZAHfkD">Taking responsibility: conditions and circumstances at Australia's regional processing centre in Nauru. (Final report).</a></em> Canberra, ACT: Commonwealth of Australia. Retrieved from http://www.aph.gov.au/Parliamentary_Business/Committees/Senate/Regional_processing_Nauru/Regional_processing_Nauru/Final_Report (Archived by WebCite&reg; at http://www.webcitation.org/6tOZAHfkD).</li>
<li id="blu05">Bluemink, E. &amp; Brush, M. (2005). <em>A flawed tool: environmental reporters’ experiences with the Freedom of Information Act.</em> Jenkintown, PA: Society of Environmental Journalists.</li>
<li  id="box07">Box, R. C. (2007). Redescribing the public interest. <em>Social Science Journal, 44</em>(4), 585-598. </li>
<li id="coo16">Cooney, B. (2016). <em><a href="http://www.webcitation.org/6tOZeRP23">Claims, requests and reviews made by detainees of offshore detention centres</a></em>. Retrieved from https://www.righttoknow.org.au/request/claims_requests_and_reviews_made#incoming-4900 (Archived by WebCite&reg; at http://www.webcitation.org/6tOZeRP23).</li>
<li id="cui11">Cuillier, D. (2011). <em><a href="http://www.webcitation.org/6tPgIofJJ">Pressed for time: US journalists’ use of public records during economic crisis.</a></em> Paper presented at the Global Conference on Transparency Research, May 18-20, Newark, NJ. Retrieved from https://spaa.newark.rutgers.edu/sites/default/files/files/Transparency_Research_Conference/Papers/Cuillier_david.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tPgIofJJ).</li>
<li id="est06">Ester, H. (2006). Political journalists, 'leaks' and freedom of information. <em>Australian Journalism Review, 28</em>(1), 157-166.</li>
<li id="fal04">Falconer, C. (2004, November 26). <em><a href="http://www.webcitation.org/6tOa1K7TI">Speech to the Law for Journalists Conference, London.</a></em> Retrieved from http://www.ukpol.co.uk/uncategorized/lord-falconer-2004-speech-to-the-law-for-journalists-conference/ (Archived by WebCite&reg; at http://www.webcitation.org/6tOa1K7TI).</li>
<li id="far13">Farrell, P. (2013). <em><a href="http://www.webcitation.org/6tOa3sNzo">FOI request for detail incident report 1-78LBQ9.</a></em> Retrieved from https://www.righttoknow.org.au/request/foi_request_for_detail_incident_119 (Archived by WebCite&reg; at http://www.webcitation.org/6tOa3sNzo).</li>
<li id="far16a">Farrell, P. &amp; Evershed, N. (2016, August 10). <a href="http://www.webcitation.org/6tPgSyqPn">What are the Nauru files?</a> <em>The Guardian.</em> Retrieved from https://www.theguardian.com/news/2016/aug/10/explainer-how-to-read-and-interpret-the-nauru-files (Archived by WebCite&reg; at http://www.webcitation.org/6tPgSyqPn</li>
<li id="far16b">Farrell, P., Evershed, N., &amp; Davidson, H. (2016, August 10). <a href="http://www.webcitation.org/6tPggXCTP">The Nauru files: cache of 2,000 leaked reports reveal scale of abuse of children in Australian offshore detention</a>, <em>The Guardian.</em> Retrieved from https://www.theguardian.com/australia-news/2016/aug/10/the-nauru-files-2000-leaked-reports-reveal-scale-of-abuse-of-children-in-australian-offshore-detention (Archived by WebCite&reg; at http://www.webcitation.org/6tPggXCTP).</li>
<li id="haw13">Hawke, A. (2013). <em><a href="http://www.webcitation.org/6tOaKtxS3">Review of the Freedom of Information Act 1982 and Australian Information Commissioner Act 2010.</a></em> Canberra, ACT: Attorney-General's Department. Retrieved from https://www.ag.gov.au/Consultations/Documents/FOI%20report.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tOaKtxS3).</li>
<li id="haz10">Hazell, R. &amp; Worthy, B. (2010). Assessing the performance of freedom of information. <em>Government Information Quarterly, 27</em>(4), 352-359.</li>
<li id="ici04">Information Commissioner of Ireland. (2004). <em><a href="http://www.webcitation.org/6tOaNHE6y">Review of the Operation of the Freedom of Information (Amendment) Act 2003.</a></em> Dublin: Office of the Information Commissioner. Retrieved from https://www.oic.gov.ie/en/Publications/Special-Reports/Investigations-Compliance/Review-of-the-Operation-of-FOI2003/-Review-of-the-Operation-of-the-Freedom-of-Information-Amendment-Act-2003.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tOaNHE6y).</li>
<li id="kat13">Kate. (2013). <em><a href="http://www.webcitation.org/6tOampEOz">FOI request for detail incident report 1-2PQQHC.</a></em> Retrieved from https://www.righttoknow.org.au/request/foi_request_for_detail_incident_8 (Archived by WebCite&reg; at http://www.webcitation.org/6tOampEOz).</li>
<li id="lam04">Lamble, S. (2004). Media use of FoI surveyed: New Zealand puts Australia and Canada to shame. <em>Freedom of Information Review, 109</em>, 5-9.</li>
<li>McMillan, J. &amp; Popple, J. (2012). <em>Review of freedom of information legislation: submission to the Hawke Review.</em> Canberra, ACT: Office of the Australian Information Commissioner (OAIC).</li>
<li id="mer07">Merritt, C. (2007, March 22). Pattern of FOI secrecy emerges, <em>The Australian</em>, p. 14.</li>
<li id="mor97">Morrison, A. (1997). The games people play: journalism and the Official Information Act. In Legal Research Foundation (Ed.), <em>The Official Information Act: seminar papers general overview of official information and the Official Information Act</em>, February 1997, Wellington, New Zealand.</li>
<li id="mus06">Mustonen, J. (Ed.). (2006). <em>The world's first Freedom of Information Act: Anders Chydenius' legacy today.</em> Kokkola, Sweden: Anders Chydenius Foundation.</li>
<li id="oaic11">Office of the Australian Information Commissioner. (2011). <em><a href="http://www.webcitation.org/6tOazx90Z">Principles on open public sector information.</a></em> Canberra, ACT: Office of the Australian Information Commissioner (OAIC). Retrieved from http://www.oaic.gov.au/images/documents/information-policy/information-policy-reports/Principles_open_public_sector_info_report_may2011.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tOazx90Z).</li>
<li id="oaic12">Office of the Australian Information Commissioner. (2012). <em><a href="http://www.webcitation.org/6tOaoyUIM">Review of charges under the Freedom of Information Act 1982.</a> Report to the Attorney-General.</em> Canberra, ACT: Office of the Australian Information Commissioner (OAIC). Retrieved from https://www.oaic.gov.au/freedom-of-information/foi-resources/foi-reports/review-of-charges-under-the-freedom-of-information-act-1982 (Archived by WebCite&reg; at http://www.webcitation.org/6tOaoyUIM).</li>
<li id="oaic14">Office of the Australian Information Commissioner. (2014). <em><a href="http://www.webcitation.org/6tOazx90Z">Guidelines issued by the Australian Information Commissioner under s93A of the Freedom of Information Act 1982 [revised Oct 2014].</a></em> Canberra, ACT: Office of the Australian Information Commissioner (OAIC). Retrieved from https://www.oaic.gov.au/resources/freedom-of-information/foi-guidelines/FOI_Guidelines_-_new_compilation_-_Oct_2014.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tOazx90Z).</li>
<li id="oli04">Oliver, R. (2004). <em>What is transparency?</em> New York, NY: McGraw Hill.</li>
<li id="pat09">Paterson, M. (2009). <em><a href="http://www.webcitation.org/6tObRiVDs">Response to the review of FOI laws: submission No 20 to the Department of Prime Minister and Cabinet, inquiry on proposed freedom of information reforms (2009).</a></em> Retrieved from https://www.ag.gov.au/Consultations/Documents/ReviewofFOIlaws/Moira%20Paterson.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tObRiVDs).</li>
<li id="rob06">Roberts, A. S. (2006). <em>Blacked out: government secrecy in the information age.</em> New York, NY: Cambridge University Press.</li>
<li id="ste15">Stewart, D. (2015). Assessing access to information in Australia: the impact of freedom of information laws on the scrutiny and operation of the Commonwealth government. In J. Wanna, E. A. Lindquist and P. Marshall (Eds.), <em>New accountabilities, new challenges</em> (pp. 79-158). Canberra, ACT: Australian National University Press.</li>
<li id="ter98">Terrill, G. (1998). The rise and decline of freedom of information in Australia. In A. McDonald &amp; G. Terrill (Eds.), <em>Open government: freedom of information and privacy</em> (pp. 89-115). Basingstoke: Macmillan.</li>
<li id="tho16">Thornton, S. (2016). Karl Popper. In E.N. Zalta (Ed.), <em>The Stanford encyclopedia of philosophy</em> (Winter 2016 ed.).</li>
<li id="tim15">Timmins, P. (2015). <em><a href="http://www.webcitation.org/6tObcHOXg">Conduct of functions of OAIC from 1 January 2015.</a></em> Right to Know.org. Retrieved from https://www.righttoknow.org.au/request/conduct_of_functions_of_oaic_fro_2#incoming-3379 (Archived by WebCite&reg; at http://www.webcitation.org/6tObcHOXg).</li>
<li id="wal84">Wald, P. M. (1984). The Freedom of Information Act: a short case study in the perils and paybacks of legislating democratic values. <em>Emory Law Journal, 33</em>, 649-683.</li>
<li id="web09">Weber, M. (2009). <em>Essays in Sociology</em> (translated and edited by H.H. Gerth &amp; C.W. Mills). New York, NY: Routledge. (Original work published 1946).
</li><li id="whi72">Whitlam, G. (1972, November 13). <em><a href="http://www.webcitation.org/6tObek516">Men and women of Australia</a></em> [speech]. Retrieved from http://whitlamdismissal.com/downloads/72-11-13_whitlam-policy-speech_podium-version.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6tObek516).</li>
</ul>

</section>

</article>