<header>

#### vol. 22 no. 4, December, 2017

</header>

<article>

# Online communities: satisfaction and continued use intention

## [Barbara Apostolou, France Bélanger](#author), and [Ludwig Christian Schaupp](#author).

> **Introduction**. We report on the development and testing of a model of online community continued use grounded in the information systems success model and the theory of reasoned action. We contribute to the online community literature by studying user intentions to continue use of an established professional online community.  
> **Method**. The model is tested with a survey questionnaire completed by members of an established online community of academic professionals, with 1,675 responding.  
> **Analysis**. Responses were analysed using partial least squares.  
> **Results**. We offer empirical evidence of a theoretical integration of two established frameworks that provide a highly predictive model of both continued use and satisfaction. The results indicate that online community satisfaction is a strong predictor of continued use intention, together with perceived usefulness and post-adoptive subjective norm. Determinants of online community satisfaction include information quality and trust of the online community platform.  
> **Conclusion**. In contrast to previous research, our finding that subjective norm is negatively related to continued use intention suggests that members of a dues-paying professional community may not be susceptible to opinions of others when deciding to continue using the online platform. The concept of post-adoptive subjective norm needs exploration in future research.

<section>

## Introduction

We develop a theoretical model of satisfaction and continued use intention in online communities. Most studies investigating online communities focus on user intention to adopt or their willingness to share information in such communities (e.g., [Wang and Clay, 2012](#wan12)). However, there have been calls for information systems researchers to study post-adoption of information systems ([Furneaux and Wade, 2011](#fur11)) and online communities in particular ([Kim and Han, 2009](#kim09)). This study focuses on the post-adoptive behavioural intention to continue usage in the context of an established online community. Specifically, we examine and empirically validate the factors that lead to online satisfaction with, and intention to continue using, an online community platform. We investigate post-adoptive norms in the context of a professional online community with a contributing membership of individual users. This successful, established online community has high user participation, has been sustained over time and has users who return to the community on a regular basis.

Online communities are comprised of individuals with common interests who access the community for knowledge acquisition, exchange and collaborative decision making. In addition, communities often rally people to act for a cause or support fellow members ([Preece, 2001](#pre01)). Online communities can be classified in a variety of ways, including purpose, user type, social structure and technology base ([Jones, Wilikens, Morris and Masera, 2000](#jon00)). They are found in a wide variety of domains, including social, cultural, organizational and economic activities. For example, e-commerce vendors often create online communities where customers can converse electronically with others and share applications, problems and solutions ([Lu, Zhao and Wang, 2010](#lu10)). Other groups provide support for one another, with support ranging from health information (e.g., cancer survival) to work-related information (e.g., software user groups) ([Lasker, Sogolow and Sharim, 2005](#las05)). We view an online community as individuals with common interests who use a technical platform to interact with information and support, while subscribing to both implicit and explicit codes of conduct appropriate to the community ([Leimeister, Sidiras and Krcmar, 2006](#lei06)).

Online communities can be beneficial to organizations. For example, they can save substantial costs of servicing users who perform self-service online ([Roque, 2013](#roq13)). Organizations also can increase market share through increased satisfaction of community users who receive faster problem resolution via help by other members. Information systems researchers have studied how communities of practice better facilitate the creation and dissemination of knowledge within organizations ([Hara, 2009](#har09); [Wasko and Faraj, 2005](#was05)). Online communities have similar characteristics as communities of practice; they are collaborative, allowing users to develop shared understandings, and facilitate knowledge building ([Hara, 2009](#har09)). However, unlike communities of practice, online communities are not necessarily affiliated with an organization and can be accessible to a larger user base ([Dahlander and Magnusson, 2005](#dah05)). Users may benefit with better services, easy access to relevant information and development of relationships with like-minded people, which has resulted in the increasing popularity of online communities. Surveys indicate 74% of U.S. adults used social networking sites as of January 2014 (Pew Research Center, 2014). In 2013, 42% of online adults used multiple social networking platforms ([Duggan, and Smith, 2013](#dug13)). A longitudinal study also found that online communities have profound impact on individual behaviour, with close to 75% participating in social causes and 87% participating in new social causes since joining their online community. Further, close to 50% feel equally strongly about their online communities as they do their real-world ones ([Center for the Digital Future, 2008](#cen08)).

This study provides several important contributions to research and practice. For researchers, this study proposes and empirically validates a model of continued use by online community members. The research integrates and adapts two theoretical frameworks to provide a model that explains a large portion of the variance in both continued use and satisfaction with the online community platform, while remaining relatively parsimonious. An additional important contribution is that the community studied is a for-pay professional environment that is established and successful. This study answers the call for more theoretical research on the drivers of successful online communities ([Ren _et al_., 2012](#ren12)), as well as recent calls for more research on post- adoption use of information systems ([Bhattacherjee and Premkumar, 2004](#bha04); [Furneaux and Wade, 2011](#fur11); [Kim and Malhotra, 2005](#kim05)). The results may inform practitioners about salient factors that lead online community members to return to the community, what has been termed resilience of the community in ealier research ([Butler, Bateman, Gray and Diamant, 2014](#but14)).

The remainder of the paper is organized as follows. The first section provides the research model and hypotheses as derived from extant theory and literature. The second section presents the research design. The final section provides discussion of the findings, contribution, limitations and suggestions for future research.

</section>

<section>

## Research model and hypotheses

Online communities have two key functions: (1) creation and dissemination of knowledge and (2) socialization and development of social identity of members ([Brown and Duguid, 1991](#bro91); [Hara, 2009](#har09); [Ryu, Kim, Chaudhury and Rao, 2005](#ryu05)). While there is limited research on what makes some online communities more successful than others ([Ren _et al._, 2012](#ren12), p. 841), it can be argued that active individual members collectively define the success of an online community. Therefore, such communities thrive when a large number of users return to the community and contribute on a regular basis. In fact, resilience, defined as the willingness of members to stay involved, is a key factor in online community success ([Butler _et al._, 2014](#but14)).

With some exceptions ([Bhattacherjee and Premkumar, 2004](#bha04); [Furneaux and Wade, 2011](#fur11); [Kim and Malhotra, 2005](#kim05)), there have been few studies of what occurs after users adopt a technological innovation ([Kang, Lee and Lee, 2012](#kan12)). Success should not be defined solely by willingness to adopt, but also by subsequent use of the system. Consistent with other information systems domains, prior research in electronic environments (e.g., e-commerce, e-government and online communities) has tended to focus on one of three success variables: (1) behavioural intention to use the Website, (2) actual use of the Website, and (3) satisfaction with the platform or community. Intention to use is likely the most explored variable. However, while it has been the focus of numerous information technology adoption studies, it is not an appropriate gauge of success in the context of online communities that enjoy an established membership base. Instead, an online community must be considered based upon users who return to the community, attraction of new members and member contributions to the community.

Some recent information systems studies have examined user intentions to contribute to the online community via self-disclosure ([Posey, Lowry, Roberts and Ellis, 2010](#pos10)) or participate in a social networking site ([Sánchez-Franco Buitrago-Esquinas and Yñiguez, 2012](#san12)). Others have investigated continued use of microblogging sites ([Agrifoglio, Black, Metallo and Ferrara, 2012](#agr12); [Zhao and Cao, 2012](#zhao12)), virtual worlds ([Nevo, Nevo and Kim, 2012](#nev12)), mobile services ([Kang _et al._, 2012](#kan12); [Park, Snell, Ha and Chung, 2011](#par11)) or business intelligence ([Deng and Chi, 2012](#den12)). However, few studies have focused on continued use of the existing online community user base. We subscribe to the view that continued use is an extension of adoption, with revision of perceptions as the system is used or as the result of an evaluation of discrepancies between expectations and what the system delivers on future usage ([Furneaux and Wade, 2011](#fur11)). With limited research in this context, there is a need to better understand why members continue to return to the online community platform.

</section>

<section>

### Model of online community satisfaction and continued use intention

Our model of online community satisfaction and continued use intention integrates constructs derived from extant research and theory: (1) the information systems success model ([DeLone and McLean, 1992](#del92)), (2) the updated information systems success model ([DeLone and McLean, 2003](#del03)), (3) Lin and Lee’s ([2006](#lin06)) model of determinants of member loyalty that is based on DeLone and McLean ([2003](#del03)), and (4) the theory of reasoned action ([Ajzen and Fishbein, 1972](#ajz72)). The theory of reasoned action is widely used in information systems research, and in this research it complements the information systems success modelby adding relevant constructs for the context of online community continued use by individuals. The DeLone and McLean ([1992](#del92), [2003](#del03)) models have been used as a foundation for numerous studies (e.g., [Petter, DeLone and McLean, 2013](#pet13)), including online communities, with either Website satisfaction or member loyalty as dependent variables ([Lin and Lee, 2006](#lin06); [Schaupp, Bélanger and Weiguo, 2009](#sch09)). Lin and Lee’s ([2006](#lin06)) model of determinants of member loyalty suggests and validates a unidirectional link between use and satisfaction and suggest that user satisfaction and behavioural intentions lead to member loyalty. Studies in the context of e-government ([Teo, Srivastava and Jiang, 2008](#teo08)) and e-commerce ([Lai, 2014](#lai14); [Wang, 2008](#wan08)) also propose and validate a unidirectional link between user satisfaction and intention to continue using e-government and e-commerce.

In the theory of reasoned action, attitude toward a particular behaviour refers to the degree to which a person has a favourable or unfavourable evaluation or appraisal of the behaviour in question ([Guo, Yuan, Archer and Connelly, 2011](#guo11), p. 211). In our study, satisfaction with the online community platform reflects a user's attitude, which is similar to research on Website user satisfaction where satisfaction was defined as end-users’ attitude toward an organization’s Website ([Muylle, Moenaert and Despontin, 2004](#muy04)). The theory of reasoned action models beliefs and evaluations as antecedents to attitude. Most technology adoption theories highlight the role of perceived usefulness and perceived ease of use as highly significant predictors of attitudes and behaviour ([Davis, 1989](#dav89); [Liang and Hue, 2010](#lia10): [Venkatesh, Morris, Davis and Davis, 2003](#ven03)).

Our model is presented in Figure 1 and consists of constructs adapted from these four sources to accommodate features of online communities (references to the primary literature for each construct are noted in Figure 1), and which incorporates two proposed dependent variables (online community satisfaction and continued use intention) derived from DeLone and McLean ([2003](#del03)) and Lin and Lee ([2006](#lin06)). We briefly define each construct here as adapted to the context of the study, and further describe them in detail in the remainder of this section.

*   System quality is the degree to which users perceive the online community platform as easy to use and interact with.
*   Information quality is the degree to which users perceive that the information provided within the online community is accurate, relevant, complete and in the format they require.
*   Subjective norm is the degree to which users perceive that referent others believe he or she should use the online community platform.
*   Perceived usefulness is the degree to which users believe that using the online community will help them accomplish their goals.
*   Trust of online community platform represents the beliefs of users about benevolence, competence and integrity of the online community platform.
*   Online community satisfaction is the user's affective response regarding the output of using the online community platform.
*   Online community continued use intention is the user's behavioural intention to remain in the community.

<figure>

![Online community satisfaction and continued use intention model](../p774fig1.png)

<figcaption>Figure 1: Online community satisfaction and continued use intention model</figcaption>

</figure>

</section>

<section>

### Hypotheses development

Referring to Figure 1, we have identified eleven paths that form hypotheses about the modelled relationships leading to online community satisfaction and continued use intention. We begin our discussion with the relationship between the dependent variables of online community satisfaction and online community continued use intention, which leads to our first hypothesis. For organizations, it is imperative to identify factors that make active members return to the online community. Previous consumer behaviour research and information systems research suggests that user satisfaction is one of the most salient predictors for consumer loyalty, i.e., repeated purchase or consumption of a service ([Gelderman, 1998](#gel98); [Nelson, Todd and Wixom, 2005](#nel05); [Qi, Zhou, Chen and Qu, 2012](#qi12)). Satisfaction can be defined as '_the sum of one’s feelings or attitudes [in a given situation] towards a variety of factors affecting that situation_' ([Bailey and Pearson, 1983](#bai83), p. 531). Satisfaction has been a pervasive measure of success or effectiveness of information systems (e.g., [Conrath and Mignen, 1990](#con90); [DeLone and McLean, 1992](#del92); [Gelderman, 1998](#gel98); [Ives and Olson, 1984](#ive84); [Melone, 1990](#mel90)) and e-commerce systems (e.g., [McKinney, Yoon and Zahedi 2002](#mckin02); [Schaupp and Bélanger, 2005](#sch05); [Szymanzki and Hise, 2000](#szy00)).

For online communities, member satisfaction can occur in two ways: (1) satisfaction with using the online community platform, and (2) satisfaction with the organization and its services as a whole. We consider satisfaction as a combination of member reactions to the platform and the organization because we are studying active members of an established professional online community. Thus, we posit a unidirectional link between online community satisfaction and online community continued use intention (e.g., [Lai, 2014](#lai14); [Petter _et al._, 2013](#pet13); [Ray, Kim and Morris, 2012](#ray12); [Teo, Srivastava and Jiang 2008](#teo08); [Wang, 2008](#wan08); [Zhao and Cao, 2012](#zhao12); [Zhang, Cheung and Lee 2012](#zhang12)). Therefore, consistent with Lin and Lee ([2006](#lin06)), we predict that online community satisfaction will lead to online community continued use intention:

> H1: User satisfaction with the online community is positively related to intention to continue to use the online community.

</section>

<section>

#### System quality

Previous research has shown that technology is a crucial element of successful online communities to enable and support community user interactions and aid in building trust and feelings of belonging ([Garrety, Robertson and Badham, 2004](#gar04); [Leimeister _et al._, 2006](#lei06); [Preece, 2000](#pre00)). The continuous development process of the online community platform is essential ([Arnold, Leimeister and Krcmar 2003](#arn03)), as is a critical mass of functionality ([Williams and Cothrel, 2000](#wil00)). Research has shown that an online community platform’s attributes (e.g., usefulness and ease of use) influence member participation ([Kuo, 2003](#kuo03); [Preece, 2001](#pre01)). Therefore, online communities should be designed with emphasis on how technology can support interaction amongst users and be designed for sociability ([Preece, 2001](#pre01)). These concepts are reflected in system quality, a core construct in our model based on DeLone and McLean ([1992](#del92)). In this research, system quality is defined as the degree to which users perceive the online community platform as easy to use and interact with. Prior research in the context of e-government has found a significant relationship between system quality and use of the e-government service ([Khayun, Ractham and Firpo, 2012](#kha12)).

System quality has been linked to use and satisfaction in previous research in volitional use contexts such as e-commerce ([DeLone and McLean, 2004](#del04); [McKinney _et al.,_ 2002](#mckin02); [Szymanzki and Hise, 2000](#szy00)) and e-government ([Khayun _et al.,_ 2012](#kha12)). Perceived ease of use, ‘_the degree to which a person believes that using a particular system would be free of effort_’ ([Davis, 1989](#dav89), p. 320), is an important element of system quality. In the context of online communities, having a platform that is easy to use is crucial since users are likely to have varying levels of computer competence and knowledge of the online community platform. Recent studies have shown that ease of use (system quality) directly impacts continued use of microblogging sites ([Agrifoglio _et al._, 2012](#agr12); [Zhang _et al._, 2012](#zhang12)). System quality has also been shown to affect satisfaction with e-government ([Khayun _et al_., 2012](#kha12); [Teo _et al._, 2008](#teo08)) and e-commerce ([Wang, 2008](#wan08)). Consistent with our theoretical model and extant literature in a variety of e-contexts, system quality is expected to influence both satisfaction and continued use intention:

> H2: The user’s perception of the quality of the system related to the online community platform is positively related to the user’s satisfaction with the online community platform.
> 
> H3: The user’s perception of the quality of the system related to the online community platform is positively related to the user’s intention to continue to use the online community platform.

</section>

<section>

#### Information quality

Information quality is the degree to which information provided within the online community is accurate, relevant, complete and in the format required by the user. The three attributes of the information quality construct among the most widely studied in the information systems literature are (1) content, (2) accuracy and (3) format (e.g., [Bailey and Pearson, 1983](#bai83); [Rainer and Watson, 1995](#rai95)). Information quality has been shown to impact use and satisfaction ([DeLone and McLean, 2003](#del03); [Seddon, 1997](#sed97)), including in online environments ([McKinney _et al.,_ 2002](#mckin02)). Information quality has also been shown to influence continued use intention in the context of e-government ([Teo _et al_., 2008](#teo08)) and e-commerce ([Wang, 2008](#wan08)). As an online community’s purpose is to serve as a medium for the exchange of information, information quality is a key concern ([Chen and Whinston, 2011](#che11)). We thus expect information quality to significantly influence satisfaction with the online community and the user’s continued use intention, as suggested by our model:

> H4: The user’s perception of the quality of the information on the online community platform is positively related to the user’s satisfaction with the online community platform.
> 
> H5: The user’s perception of the quality of the information on the online community platform is positively related to the user’s intention to continue using the online community platform.

We also expect that system quality influences user perceptions of information quality. An easy-to-use online community platform that produces desired information quickly will positively influence user perceptions of the quality of the information. If the online community platform is perceived as difficult to use, the desired information may never be obtained, thus negatively influencing user perceptions of the online community’s information quality. Investigating such a link is not unprecedented. In a study of the determinants of system quality and information quality in the context of data warehouses, Nelson _et al._ ([2005](#nel05)) propose and empirically validate a link from system quality to information satisfaction, which is affected by information quality as well. Similarly, system satisfaction was found to positively affect information satisfaction ([Wixom and Todd, 2005](#wix05)). This relationship is also consistent with technology adoption research, where perceived ease of use (conceptualized here as system quality) is predicted to influence perceived usefulness ([Davis, 1989](#dav89)). Thus, system quality is expected to influence the perceptions of quality of information produced in the online community context:

> H6: The user’s perception of the quality of the system related to the online community platform is positively related to the user’s perceptions of the quality of the information on the online community platform.

</section>

<section>

#### Subjective norm

An online community user base includes individuals who interact with one another in the community and who have individual, social and organizational needs. The policies of the online community include language and protocols that guide user interactions and contribute to the development of the accepted social norms ([Preece, 2001](#pre01)). Earlier research shows that involved members and a sense of online community are important characteristics of successful online communities ([Blanchard and Markus, 2002](#bla02), [2004](#bla04)). The sense of community also suggests that individuals may be affected by others in their decisions to continue use of the online community platform. Xu, Xu and Li ([2015](#xu15)) studied aggression in online communities because of its potential for reducing interest in the community; they found that moral code of the participant was more important than formal deterrents.

Subjective norm, as adapted to our model, is the degree to which an individual perceives that referent others believe he or she should use the online community platform ([Ajzen, 1991](#ajz91)). Subjective norm is perhaps the greatest tool to building an online community’s user base and achieving a critical mass of members, which is especially true when coming from an influential referent. To date, subjective norm has primarily been found to influence use in pre-adoption mandatory organizational contexts (e.g., [Hsu and Chiu, 2004](#hsu04); [Karahanna, Straub and Chervany, 1999](#kar99); [Venkatesh and Davis, 2000](#ven00); [Venkatesh _et al._, 2003](#ven03)). However, we are interested in the impact of subjective norm on the intention of returning users who have already adopted the online community. We label this construct as subjective norm.

Alternative terms are used in the literature to refer to the social influences of others. Thompson, Higgins and Howell ([1991](#tho91)), for example, use the term social norms to define their construct, but acknowledge its similarity to subjective norm within the theory of reasoned action. Venkatesh _et al._ ([2003](#ven03)) include social norm within the social influence constructs, but found that none of the social influence constructs were significant in voluntary contexts (however, all of them were found to be significant when use was mandatory). Venkatesh and Davis ([2000](#ven00)) suggest that effects in a mandatory context could be attributed to compliance that causes social influence to have a direct effect on intention. In contrast, social influence in voluntary contexts, as in this study, functions by influencing perceptions about the online community platform. Prior studies of microblogging found that community or social norms directly affect continued use intention ([Zhao and Cao, 2012](#zhao12)). Similarly, Garg, Smith and Telang ([2011](#gar11)) found that peers in a social community have a strong influence in the diffusion of information. In one of the few studies in the context of online communities and post-adoption usage, Posey _et al._ ([2010](#pos10)) found a strong positive relationship between social influence and user intention to disclose information on the online community. We therefore expect a similar positive relationship with a direct influence of subjective norm on continued use intention:

> H7: Subjective norm is positively related to the user’s intention to continue to use the online community.

</section>

<section>

#### Perceived usefulness

Most technology adoption theories highlight the role of perceived usefulness as a highly significant predictor of both attitudes and behavioural intention ([Davis, 1989](#dav89); [Liang and Xue, 2010](#lia10); [Venkatesh _et al._, 2003](#ven03)). Perceived usefulness is ‘the degree to which a person believes that using a particular system would enhance his or her job performance’ ([Davis, 1989](#dav89), p. 320). However, in the context of this study, perceived usefulness represents the degree to which individuals believe that using the online community will help them accomplish their goals. Consistent with the theory of reasoned action and research in other contexts (e.g., [Rai, Lang and Welker, 2002](#rai02); [Seddon, 1997](#sed97)), positive perceptions of perceived usefulness improve user satisfaction with the online community platform. In addition, recent research in the context of microblogs has linked perceived usefulness to continued use ([Zhao and Cao, 2012](#zhao12)). Similarly, higher perceived value of an online community leads to increased usage and commitment ([Liu, Wagner and Chen, 2014](#liu14)). Prior research on e-commerce shows that perceived usefulness is a determinant of continuance intention ([Lai, 2014](#lai14)). Therefore, perceived usefulness is expected to have a direct effect on intention to continue to use the online community platform:

> H8: Perceived usefulness is positively related to user satisfaction with the online community.
> 
> H9: Perceived usefulness is positively related to user intention to continue to use the online community.

</section>

<section>

#### Trust

Another important belief that can impact satisfaction and continued use intention in the context of e-environments is trust in the online community platform. When individuals interact electronically, they are cooperating in an uncertain situation with inherent risks ([Crosby, Evans and Cowles, 1990](#cro90); [Grazioli and Jarvenpaa, 2000](#gra00)). For example, in e-commerce, users are concerned about the intention of Website owners ([Gefen, 2000](#gef00)). With online communities, members depend on each other and the online community platform; however, they cannot rely on trust-building behaviour such as handshakes and body language because their physical separation prevents them from direct observation ([Clarke, 1997](#cla97)).

There are several definitions of trust in the literature. In situations of uncertainty, trust refers to an individual's ability to rely on the integrity and predictability of other individuals whom they cannot control ([Mayer, Davis and Schoorman, 1995](#may95); [McKnight and Chervany, 2002](#mckche02); [Pavlou, 2003](#pav03); [Siau and Shen, 2003](#sia03); [Warkentin, Gefen, Pavlou and Rose, 2002](#war02)). Rotter ([1971](#rot71), p. 444) defines trust as ‘_an expectancy held by an individual or a group that the word, promise, verbal, or written statement of another individual or group can be relied on_’. Several studies have used the theory of reasoned action as a foundation to study the relationship between trust and user behaviour ([Gefen, Karahanna and Straub, 2003](#gef03); [Jarvenpaa, Tractinsky and Vitale, 2000](#jar00); [McKnight, Choudhury and Kacmar, 2002](#mckcho02); [Pavlou, 2003](#pav03)). In information systems, substantial research exists on the impact of trust in e-commerce contexts (e.g., [Bélanger, Hiller and Smith, 2002](#bel02); [Gefen, 2002](#gef02); [Gefen _et al._, 2003](#gef03); [Hoffman, Novak and Peralta, 1999](#hof99); [Jarvenpaa _et al.,_ 2000](#jar00); [McKnight and Chervany, 2002](#mckche02); [Van Slyke, Bélanger and Comunale, 2004](#van04)) and e-government contexts ([Bélanger and Carter, 2008](#bel08); [Bélanger and Hiller, 2006](#bel06); [Carter and Bélanger, 2005](#car05); [Welch, 2004](#wel04)).

Trust is typically measured based on three characteristics: benevolence, competence and integrity ([Mayer _et al._, 1995](#may95); [McKnight and Chervany, 2002](#mckche02)). Benevolence is defined as the extent to which the trusting party believes that the trusted party wants to do things the right way (do good) rather than just maximize their profit ([Bélanger _et al._, 2002](#bel02)). Competence is defined as the ability of the party to deliver what is promised to the user, ensuring the privacy of their personal information and security of the platform (in the case of online communities). Perceived integrity is evidence of the other party’s honesty and sincerity ([Bélanger _et al._, 2002](#bel02)), meaning the trustee makes good faith agreements, tells the truth, acts ethically and fulfills promises ([McKnight and Chervany, 2002](#mckche02)). Trust is context specific. Some suggest it may be in an individual ([Larzelere and Huston, 1980](#lar80)) or an institution ([Shapiro, 1987](#sha87)). Others suggest that the targets of trust in online environments can be the entity providing the service (party trust) and the mechanism through which it is provided (control trust) ([Tan and Theon, 2001](#tan01)). Bélanger _et al_. ([2002](#bel02)) note that the majority of the operationalizations of trust in information systems do not specify the online trust referents (i.e., the merchant or the computer system). In a study of trust in e-government, Bélanger and Carter ([2008](#bel08)) suggest that user trust combines (1) the trust in a specific entity (in their case the government agency) and (2) trust in the enabling technology (the Internet).

In their review of 140 studies based on the information systems success model, Petter _et al_. ([2013](#pet13)) found that trust has often been linked to behavioural intention, but also (in at least one study) to user satisfaction. Previous research in e-commerce supports the idea that trust in the platform is a key determinant of user satisfaction with the platform, more so than the functionality of the platform itself ([Lu, Wang and Hayes, 2012](#lu12)). In the context of online communities, trust has been found to impact loyalty or intention to return to the community ([Ray _et al._, 2012](#ray12)). Consistent with extant research findings, we conceptualize trust of online community platform as a combination of trusting beliefs about the online community platform ([McKnight, Cummings and Chervany, 1998](#mckcum98)). We predict a positive relationship between trust and satisfaction and continued use intention:

> H10: Trust is positively related to user satisfaction with the online community.
> 
> H11: Trust is positively related to user intention to continue using the online community.

</section>

<section>

## Research design

A survey of members of a successful online community was conducted. The research model was tested using partial least squares as implemented in [SmartPLS.](https://www.smartpls.com) Partial least squares has minimal demands on measurement scales, sample size and residual distributions ([Chin and Todd, 1995](#chi95); [Wold, 1985](#wol85)). In this study, the minimum requirements are easily met with n=1,675.

</section>

<section>

### Survey instrument development

The survey instrument questions were compiled from validated instruments and wording was modified to fit the online community context ([Moore and Benbasat, 1991](#moo91)). Each question was measured on a seven-point Likert-type scale, ranging from 1 (strongly disagree) to 7 (strongly agree). A group of fifteen academics and practitioners pre-tested the instrument, identifying ambiguous or poorly worded items. The instrument was pilot tested with over 200 undergraduate students. Constructs demonstrated acceptable validity and reliability measurements. The final survey instrument consists of thirty items representing seven constructs, in addition to demographic and self-reported usage items. Constructs and their sources are summarized in Table 1\.

<figure>

![Table 1](../p774tab1.png)

<figcaption>

Table 1: Summary analysis of measurement model (Figure 1) [[Click for large version](../p774tab1.png)]</figcaption>

</figure>

</section>

<section>

### Participants

For an online community to become viable in the long-term, it must have evolved through five development stages: (1) accumulating a critical mass of members, (2) aggregating a critical mass of usage profiles, (3) aggregating a critical mass of advertisers/vendors, (4) generating a critical mass of interactions, and (5) increasing that critical mass ([Hagel and Armstrong, 1997](#hag97)). In addition, the model in our study required an established online community where member participation is high. After contacting several communities, OnComm (a fictional name to maintain anonymity of the respondents) was selected because of its high participation and member commitment (members pay for continued participation in the community). OnComm has a professional focus and is owned by a parent organization. It has over 60,000 members from several domains, including educators, administrators, researchers and business representatives. At the time of data collection, the annual membership fees for OnComm ranged from \$34 for special situations to \$182 for regular members. Services on OnComm’s Website include resource sharing, discussion forums, a professional journal, technical reports and other publications. The site also offers professional development, access to conference information and a learning centre that offers interactive Web seminars. Finally, the OnComm community disseminates results from nationwide surveys and reports on their subject of interest and offers testimony to the U.S. Congress on its subject matter. The focus of our study is on perceptions of active members of the OnComm online community, not from members of its parent organization.

To obtain study participants, an e-mail was sent to approximately 3,500 members of OnComm by its Webmaster with a link to our Web-based survey. All of those members had accessed the OnComm community within the month prior to the survey. The study participants consisted of 1,675 active members, with minor adjustments for incomplete or invalid responses. The first part of the survey requested information targeting familiarity with and usage frequency of the OnComm platform. Participants were then provided a link to OnComm and were asked to accomplish a task that was designed in collaboration with the online community Webmaster to ensure it was relevant to users. Specific tasks were given to avoid recall biases that could occur and to ensure that all perceptions of the constructs of interest were based on the most recent community platform. The task itself involved gathering relevant information from OnComm to accomplish a specific goal. Participants were then directed to respond to questions aimed at assessing perceptions of our dependent and independent variables. The final portion of the survey included demographic questions and open-ended comments. Upon completion of the survey, participants were given the opportunity to enter a draw for one MP3 player. Approximately 70% of the participants were female, and the ages ranged between twenty and sixty, with the vast majority above the age of forty.

</section>

<section>

### Measurement model testing

Convergent validity was assessed with three _ad hoc_ tests ([Anderson and Gerbing, 1988](#and88) [Netemeyer, Johnston and Burton, 1990](#net90)), which are presented in Table 1: (1) the standardized factor loading (indicative of the degree of association between a scale item and a latent variable); (2) composite reliability (similar to Cronbach's alpha); and (3) average variance extracted (a measure of the variation explained by the latent variable to random measurement error). As noted in Table 1, the factor loadings are highly significant. Composite reliability (CR), which is similar to Cronbach’s alpha, ranges from 0.86 to 0.94\. Average variance extracted (AVE) ranges from 0.693 to 0.880, all exceeding the recommended lower limit of 0.5 ([Fornell and Bookstein, 1982](#for82)). Internal consistency of sub-constructs was assessed using Cronbach’s alpha and Fornell and Larcker’s ([1981](#for81)) measure of composite reliability, both of which should be at or above 0.70 ([Nunnally, 1978](#nun78)). All constructs in the study met these criteria. Therefore, the tests show generally strong evidence that the constructs have good measurement properties. All tests support convergent validity of the scales.

Discriminant validity was assessed using the test recommended by Anderson and Gerbing ([1988](#and88)). Item-construct correlations for the constructs were examined and are summarized in Table 2\. The correlation pattern in the shaded cells shows that an item posited to form a given sub-construct has a stronger correlation with it than any other construct, providing further evidence of discriminant and convergent validity.

<table><caption>Table 2: Item-construct correlations</caption>

<tbody>

<tr>

<th>Item</th>

<th>BI</th>

<th>IQ</th>

<th>PU</th>

<th>SN</th>

<th>SQ</th>

<th>TRUST</th>

<th>SAT</th>

</tr>

<tr>

<td>Continued use intention (BI1)</td>

<td>0.940</td>

<td>0.676</td>

<td>0.802</td>

<td>0.348</td>

<td>0.639</td>

<td>0.640</td>

<td>0.879</td>

</tr>

<tr>

<td>Continued use intention (BI2)</td>

<td>0.907</td>

<td>0.763</td>

<td>0.860</td>

<td>0.384</td>

<td>0.700</td>

<td>0.651</td>

<td>0.881</td>

</tr>

<tr>

<td>Continued use intention (BI3)</td>

<td>0.901</td>

<td>0.595</td>

<td>0.713</td>

<td>0.278</td>

<td>0.563</td>

<td>0.617</td>

<td>0.834</td>

</tr>

<tr>

<td>Information quality (IQ1)</td>

<td>0.626</td>

<td>0.897</td>

<td>0.711</td>

<td>0.330</td>

<td>0.722</td>

<td>0.513</td>

<td>0.676</td>

</tr>

<tr>

<td>Information quality (IQ2)</td>

<td>0.646</td>

<td>0.915</td>

<td>0.725</td>

<td>0.330</td>

<td>0.737</td>

<td>0.534</td>

<td>0.696</td>

</tr>

<tr>

<td>Information quality (IQ3)</td>

<td>0.674</td>

<td>0.921</td>

<td>0.758</td>

<td>0.327</td>

<td>0.789</td>

<td>0.526</td>

<td>0.724</td>

</tr>

<tr>

<td>Information quality (IQ4)</td>

<td>0.617</td>

<td>0.773</td>

<td>0.635</td>

<td>0.216</td>

<td>0.645</td>

<td>0.604</td>

<td>0.640</td>

</tr>

<tr>

<td>Information quality (IQ5)</td>

<td>0.635</td>

<td>0.842</td>

<td>0.698</td>

<td>0.325</td>

<td>0.769</td>

<td>0.566</td>

<td>0.696</td>

</tr>

<tr>

<td>Information quality (IQ6)</td>

<td>0.704</td>

<td>0.914</td>

<td>0.792</td>

<td>0.363</td>

<td>0.805</td>

<td>0.596</td>

<td>0.763</td>

</tr>

<tr>

<td>Perceived usefulness (PU1)</td>

<td>0.827</td>

<td>0.760</td>

<td>0.903</td>

<td>0.374</td>

<td>0.731</td>

<td>0.657</td>

<td>0.848</td>

</tr>

<tr>

<td>Perceived usefulness(PU2)</td>

<td>0.764</td>

<td>0.803</td>

<td>0.911</td>

<td>0.429</td>

<td>0.732</td>

<td>0.630</td>

<td>0.807</td>

</tr>

<tr>

<td>Perceived usefulness (PU3)</td>

<td>0.782</td>

<td>0.724</td>

<td>0.929</td>

<td>0.458</td>

<td>0.686</td>

<td>0.645</td>

<td>0.816</td>

</tr>

<tr>

<td>Perceived usefulness (PU4)</td>

<td>0.796</td>

<td>0.723</td>

<td>0.920</td>

<td>0.455</td>

<td>0.671</td>

<td>0.693</td>

<td>0.827</td>

</tr>

<tr>

<td>Subjective norm (SN1)</td>

<td>0.335</td>

<td>0.346</td>

<td>0.426</td>

<td>0.901</td>

<td>0.316</td>

<td>0.395</td>

<td>0.381</td>

</tr>

<tr>

<td>Subjective norm (SN2)</td>

<td>0.365</td>

<td>0.349</td>

<td>0.457</td>

<td>0.907</td>

<td>0.322</td>

<td>0.434</td>

<td>0.413</td>

</tr>

<tr>

<td>Subjective norm (SN3)</td>

<td>0.230</td>

<td>0.255</td>

<td>0.314</td>

<td>0.764</td>

<td>0.250</td>

<td>0.252</td>

<td>0.274</td>

</tr>

<tr>

<td>Subjective norm (SN4)</td>

<td>0.279</td>

<td>0.239</td>

<td>0.345</td>

<td>0.763</td>

<td>0.210</td>

<td>0.327</td>

<td>0.313</td>

</tr>

<tr>

<td>System quality (SQ1)</td>

<td>0.643</td>

<td>0.812</td>

<td>0.720</td>

<td>0.306</td>

<td>0.940</td>

<td>0.526</td>

<td>0.711</td>

</tr>

<tr>

<td>System quality (SQ2)</td>

<td>0.643</td>

<td>0.783</td>

<td>0.715</td>

<td>0.299</td>

<td>0.943</td>

<td>0.528</td>

<td>0.711</td>

</tr>

<tr>

<td>System quality (SQ3)</td>

<td>0.665</td>

<td>0.797</td>

<td>0.733</td>

<td>0.331</td>

<td>0.932</td>

<td>0.565</td>

<td>0.727</td>

</tr>

<tr>

<td>Trust (TRUST1)</td>

<td>0.585</td>

<td>0.519</td>

<td>0.599</td>

<td>0.379</td>

<td>0.478</td>

<td>0.873</td>

<td>0.613</td>

</tr>

<tr>

<td>Trust (TRUST2)</td>

<td>0.655</td>

<td>0.656</td>

<td>0.721</td>

<td>0.410</td>

<td>0.621</td>

<td>0.807</td>

<td>0.708</td>

</tr>

<tr>

<td>Trust (TRUST3)</td>

<td>0.459</td>

<td>0.399</td>

<td>0.485</td>

<td>0.443</td>

<td>0.374</td>

<td>0.741</td>

<td>0.499</td>

</tr>

<tr>

<td>Trust (TRUST4)</td>

<td>0.605</td>

<td>0.552</td>

<td>0.611</td>

<td>0.277</td>

<td>0.486</td>

<td>0.833</td>

<td>0.625</td>

</tr>

<tr>

<td>Trust (TRUST5)</td>

<td>0.596</td>

<td>0.513</td>

<td>0.581</td>

<td>0.282</td>

<td>0.448</td>

<td>0.845</td>

<td>0.614</td>

</tr>

<tr>

<td>Trust (TRUST6)</td>

<td>0.553</td>

<td>0.511</td>

<td>0.586</td>

<td>0.391</td>

<td>0.456</td>

<td>0.856</td>

<td>0.597</td>

</tr>

<tr>

<td>Trust (TRUST7)</td>

<td>0.562</td>

<td>0.495</td>

<td>0.559</td>

<td>0.345</td>

<td>0.451</td>

<td>0.867</td>

<td>0.593</td>

</tr>

<tr>

<td>Satisfaction (SAT1)</td>

<td>0.835</td>

<td>0.813</td>

<td>0.877</td>

<td>0.414</td>

<td>0.785</td>

<td>0.697</td>

<td>0.922</td>

</tr>

<tr>

<td>Satisfaction (SAT2)</td>

<td>0.879</td>

<td>0.734</td>

<td>0.849</td>

<td>0.407</td>

<td>0.701</td>

<td>0.694</td>

<td>0.943</td>

</tr>

<tr>

<td>Satisfaction (SAT3)</td>

<td>0.909</td>

<td>0.668</td>

<td>0.775</td>

<td>0.343</td>

<td>0.633</td>

<td>0.649</td>

<td>0.912</td>

</tr>

</tbody>

</table>

An additional discriminant validity criterion is that the variance shared by a construct with its indicators should be greater than the variance shared with other constructs in the model. Average variance extracted is used to assess the variance shared between the construct and its measurement items ([Fornell and Bookstein, 1982](#for82)). A construct is considered to be distinct from other constructs if the square root of its average variance extracted is greater than its correlations with other latent constructs ([Barclay _et al._, 1995](#bar95)). As shown in Table 3, every combination of latent variables was tested and each pairing passed, providing evidence of the discriminant validity of the scales, with one exception. There is a risk that the constructs are confused by respondents as meaning the same thing because the variance extracted estimate for satisfaction (SAT) is not greater than the squared pairwise correlation between satisfaction (SAT) and continued use intention (BI). Moore and Benbasat ([1991](#moo91)) faced the same issue when their relative advantage (RA) and compatibility (CT) items loaded together, which was found in subsequent research ([Carter and Bélanger, 2005](#car05)). Moore and Benbasat ([1991](#moo91), p. 208) conducted additional analyses and concluded the following:

> [the constructs] did not emerge as separate factors, even though they had been consistently separated throughout the sorting procedures. This may mean that, while conceptually different, they are being viewed identically by respondents, or that there is a causal relationship between the two.

While the concepts of satisfaction (SAT) and continued use intention (BI) are closely linked, the literature suggests they are conceptually different constructs. As such, we retained all of the items from each scale in the final analyses.

<table><caption>Table 3: Discriminant validity tests</caption>

<tbody>

<tr>

<th> </th>

<th>BI</th>

<th>IQ</th>

<th>PU</th>

<th>SN</th>

<th>SQ</th>

<th>TRUST</th>

<th>SAT</th>

</tr>

<tr>

<td>Continued use intention (BI)</td>

<td>

**0.916**</td>

<td colspan="6"> </td>

</tr>

<tr>

<td>Information quality (IQ)</td>

<td>0.741</td>

<td>

**0.879**</td>

<td colspan="5"> </td>

</tr>

<tr>

<td>Perceived usefulness (PU)</td>

<td>0.866</td>

<td>0.822</td>

<td>

**0.916**</td>

<td colspan="4"> </td>

</tr>

<tr>

<td>Subjective norm (SN)</td>

<td>0.368</td>

<td>0.361</td>

<td>0.468</td>

<td>

**0.837**</td>

<td colspan="3"> </td>

</tr>

<tr>

<td>System quality (SQ)</td>

<td>0.693</td>

<td>0.850</td>

<td>0.770</td>

<td>0.333</td>

<td>

**0.938**</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>Trust (TRUST)</td>

<td>0.695</td>

<td>0.633</td>

<td>0.717</td>

<td>0.431</td>

<td>0.575</td>

<td>

**0.833**</td>

<td> </td>

</tr>

<tr>

<td>Satisfaction (SAT)</td>

<td>0.944</td>

<td>0.798</td>

<td>0.901</td>

<td>0.420</td>

<td>0.763</td>

<td>0.735</td>

<td>

**0.926**</td>

</tr>

<tr>

<td colspan="8">Note: diagonals are square roots of average variance extracted (see Table 1)</td>

</tr>

</tbody>

</table>

</section>

<section>

### Structural model testing

Partial least squares analysis was accomplished with 1,000 bootstrapping subsamples to estimate path coefficients. Results of the analysis for the structural model are presented in Figure 2, which shows nine of the eleven specified paths in the research model are significant. System quality, information quality, perceived usefulness and trust of online community platform significantly influence satisfaction with the online community. Satisfaction significantly affects intention to continue to use the online community, together with system quality, subjective norm and perceived usefulness. Finally, system quality significantly influences information quality. Overall, this analysis provides a strong predictive yet parsimonious model for online community satisfaction and continued use intention ([Barclay _et al._, 1995](#bar95); [Chin and Todd, 1995](#chi95)).

<figure>

![Results of hypotheses tests](../p774fig2.png)

<figcaption>Figure 2: Results of hypotheses tests</figcaption>

</figure>

</section>

<section>

## Discussion

We investigated the determinants of satisfaction and continued use intention in an established, paid membership, professional online community. An important and unique aspect of our study is the focus on the collection of perceptions from a large number of regularly active members. Clearly, in a high-growth market, retention of members is of paramount importance to online communities. Understanding factors that influence active members’ continued use of the online community is important to all involved stakeholders. The results from the survey of 1,675 OnComm members show that nine of eleven hypotheses are supported and large amounts of variances (R2 values of 83.9% and 89.7%) are explained in key constructs in the model. Continued online community use intention

Continued member satisfaction and use are major goals for online communities because there is no greater asset than a large, loyal user base. While limited research exists on continued use intention of existing loyal online community members, there have been many calls for research that explores continued use of information systems ([Furneaux and Wade, 2011](#fur11); [Kang, Lee and Lee, 2012](#kan12); [Ren _et al._, 2012](#ren12)). We find that satisfaction with the online community (H1), perceptions of system quality (H3), subjective norm (H7) and perceptions of perceived usefulness (H9) all influence continued use intention. Together, these four determinants, derived from two main theories, help explain 89.7% of the variance in online community continued use intention. Satisfaction is by far the strongest determinant of continued use intention, which is consistent with the information systems success model and previous research on online communities ([Lin and Lee, 2006](#lin06)), as well as on e-commerce ([DeLone and McLean, 2004](#del04)). Users dissatisfied with the online community Website will move to an alternative that satisfies their information needs (switching costs typically are very low). As predicted in the success model, system quality directly impacts continued use intention. This finding indicates that the ease of use of the platform underlying the online community Website is important to users, even if they have been regular users of the online community platform in the past.

Subjective norm is a significant predictor of continued use intention, but with two important caveats: (1) it is a very small impact compared to the other three determinants of continued use intention; (2) it is negatively related to continued use intention. While this finding might appear inconsistent with both prior research and the theoretical bases, it can be explained partially by the nature of the present study. Given that members are currently already loyal to the community and pay to participate, it is less likely that the opinions of others matter to continuing to use the online community platform. Word of mouth influences initial use of the site, but not continued use over time. Long-term success of the online community is dependent upon the continued use of its members. In other words, after the initial stages of adoption, members put more value on the usability of the online community and the overall usefulness of the community as opposed to the opinions of others. This concept of post-adoptive subjective norm needs further exploration in future research.

Another possible explanation for the subjective norm finding is that the very small effect in this model is an artifact of the large number of participants. In fact, Petter _et al._ ([2013](#pet13)) reviewed 140 articles based on the success model and found that subjective norm is the second most studied predictor on intention, but that it shows extremely mixed results (i.e., numerous studies have significant impact and many others have not). Future research should isolate and carefully examine the role of subjective norm in established and successful online communities.

Consistent with previous research and the theoretical foundations, perceived usefulness is a significant determinant of both satisfaction with the online community and continued use intention ([DeLone and McLean, 2004](#del04); [Lin and Lee, 2006](#lin06); [Rai _et al._, 2002](#rai02)). Members participate in the community because there is an intrinsic value in doing so, visiting the community in an attempt to access information that is not well known to them to build their knowledge base from a trusted source that includes colleagues and peers.

Many online community users are experts on a particular topic within the community. They contribute their knowledge to others on their pet topics of interest, with the reward being the gratification of contributing to the community and profession. Online communities serve as a platform to connect and communicate with other members of the community. Membership involvement is the gateway to creating loyal advocates of the community. As a result, the perceived value of the information available on the community platform leads to increased desire to continue to participate and use the online community. Online communities should leverage this fact by regularly publicizing and emphasizing the value of the information and services provided to and by members.

Two of our hypothesized links were not supported by the research: (1) perceived information quality (H5), and (2) trust of the online community platform (H11). Neither had an impact on continued use intention. We note that the other four significant factors captured a very large portion of the variance, leaving limited explanatory power potential for these nonsignificant constructs. Both of these constructs indirectly impact continued use intention via satisfaction with the online community platform; thus satisfaction mediates these relationships. There are inconsistent findings in prior research on the link between information quality and the dependent variables in prior research predicated on the success model. For example, Teo _et al._ ([2008](#teo08)) found that information quality impacts continued use intention but not satisfaction in an e-government context. At the same time, they found that while system quality impacts satisfaction, it does not impact continued use intention. Our results show that trust impacts satisfaction; however, it does not impact continued use intention. It is possible this occurred because the present research was conducted in the context of regular, loyal and paying members as opposed to contexts of _ad hoc_ repeated use, such as might be observed with e-commerce or e-government. Clearly, future research needs to explore this particular finding and see if information quality and trust lead to continued use intention in other contexts.

</section>

<section>

### Online community satisfaction

Given that satisfaction is a strong determinant of continued use intention, it is important to explore the determinants of satisfaction. Satisfied members influence other users through word of mouth and, importantly, they support the online community, which provides not only growth for the membership base, but also for the knowledge base of the community. Our findings suggest that perceived system quality (H2), perceived information quality (H4), perceived usefulness (H8) and trust of the online community (H11) are important in determining online community satisfaction (from the perspective of active members). The effects of both system quality and information quality on satisfaction with the online community platform are consistent with the success model, and have been documented in several other contexts, as discussed previously. One implication of this finding is that online communities must be designed in a fashion that optimizes usability while providing valuable content. A practical implication would be to provide shortcuts that are more navigational or increased search capabilities for committed long-term users that would enable them to maximize the efficiency with which their desired information would be made available. Our findings further underscore the point that goal accomplishment is imperative to a user's overall satisfaction with an online community.

Furthermore, in addition to being easy to locate, the information must be valuable in itself. Online communities have created new and unprecedented opportunities for members to expand their network of professional contacts. In this digital age, where information is produced and disseminated freely, it is of the utmost importance for professionals to have access to quality information to stay ahead of the curve. Online communities are increasing in popularity for this reason, but only to the extent that the information provided is of high quality. Clearly, online communities must provide their members with desired information, in an accurate, relevant and understandable format. The quality of information is important to the online community member, but it is through satisfaction that information quality impacts continued use intention. As previously stated, satisfaction is the strongest determinant of continued use intention. For practitioners, it is imperative to understand the goals of those who use the online community most frequently. Our findings show that goal accomplishment, easily accessible information and overall ease of use of the Website are essential determinants of satisfaction. These findings can assist online community designers to tailor their Websites accordingly.

We also found significance for the predicted link between system quality and information quality (H6). This finding suggests that the ease of finding information on the online community contributes to a user's perception of the quality of information that is produced from the online community’s information system. This finding is intuitively understandable. As an example, if we consider commercial search engines, the most utilized are those that are easily understood and accessible to all users. Ease of access and competent searching capabilities for desired information will dictate a user’s perception of the information presented. In other words, inaccessible information is unusable information, regardless of its source. An interesting practical implication is that for long-term users, accessibility and quality are positively related. For researchers, these results indicate the need for further investigation of the role of system quality on user perception of the quality of the information presented by systems in different contexts.

Perceived usefulness was the strongest path in predicting satisfaction with the online community platform. Contributing members build excitement and interest for other members by sharing their own personal knowledge and experiences of the technical topics. The strong impact of perceived usefulness on satisfaction highlights the importance of integrating the behavioural theory of reasoned action with the information systems success model for online communities. Without this link between perceived usefulness and satisfaction, the online community may never build a committed, active membership base where continued long-term use and community success is realized. It is imperative for practitioners to understand that there is an intrinsic value to members in visiting the online community Website. Finally, trust of the online community platform is a significant determinant of satisfaction of online community members. Online communities provide a digital platform for members to collectively gather in order to search for and share information and advice, and provide support for one another. These activities are instrumental in the development of a foundation of trust not only amongst members, but the community as a whole.

</section>

<section>

### Limitations

This research has some limitations, which construct avenues for future research. First, we studied one specific established and successful community. As such, the findings are not generalizable to all online communities and need to be validated in other contexts with future research. However, the fact that OnComm offered both support mechanisms and online resources for members, and that it has navigated through the five stages of online community evolution, qualifies OnComm as a proper proxy for successful professional online communities. While focusing on a successful community is important to obtain perceptions of regular members, much could also be gained from exploring discontinuance or ‘_the cessation of the use of an organizational information system_’ ([Furneaux and Wade, 2011](#fur11), p. 574). It is possible that other factors, which are not captured by focusing on returning members, impact discontinuance. Second, the data were collected via Web-based surveys, leading to potential self-report bias. Another limitation was the lack of direct control over the participants while they were taking the survey. After participants accessed the survey, it was up to them to complete it at their convenience. While the time respondents spent completing the survey was monitored, respondents were not directly supervised. Another possible limitation is that requesting volunteer participation would create a self-selection bias in that only satisfied members would participate. However, by offering an interesting prize to win (a leading edge MP3 player) we attempted to get all members to participate, not just those who were most satisfied.

</section>

<section>

## Conclusion

The objective of the study was to investigate satisfaction and continued use intention by paid members of an established online community. We used a quasi-experimental approach and analysed the data using partial least squares analysis. A summary of the key empirical findings follows:

*   Satisfaction with the online community influences continued-use intention.
*   Perceptions of system quality influence continued-use intention.
*   Perceptions of subjective norm influence continued-use intention.
*   Perceptions of perceived usefulness influence continued-use intention.
*   Perceived system quality influences online community satisfaction.
*   Perceived usefulness influences online community satisfaction.
*   Trust of the online community influences satisfaction.

Our study provides several important contributions to information systems research and practice. First, we propose and empirically validate a model of continued use by online community members. There have been many calls to move information systems research beyond the adoption level to continued usage ([Bhattacherjee and Premkumar, 2004](#bha04); [Furneaux and Wade, 2011](#fur11); [Kim and Malhotra, 2005](#kim05)). We answer these calls along with those for more theoretically based research on ‘what makes some online communities more successful’ ([Ren _et al._, 2012](#ren12), p. 841).

An additional important contribution of this research is the integration of two established theoretical frameworks that provide a highly predictive model of both continued use intention and satisfaction with the online community platform, which allows us to state that both the perceived quality of the platform (e.g., system, information) and personal beliefs are all important to continued use. Future studies should continue to explore how our integrated model can explain alternative continued use contexts. With respect to the information systems success model, the research also investigated and validated the conceptual link between system quality and information quality. While this link is not always present in research, its strong effect in our study suggests that it should be incorporated in future similar research.

Finally, this research is the first to study post-adoptive subjective norms in a voluntary online community context. An unexpected finding is that subjective norm is negatively associated with continued use intention, which suggests that members of a dues-paying professional community may not be susceptible to opinions of others when making the decision to continue using the online platform. The concept of post-adoptive subjective norm needs exploration in future research.

The results of the study also provide several contributions to practitioners. The findings suggest that online communities need to address the design and content of the community platform. However, it is perhaps just as important to publicize the value of the online community itself while fostering trust among its members. Subjective norm is a significant predictor of continued use intention, which can be partially explained by our study. Peer recommendations influence initial use of the site, but not continued, paid use over time. As practitioners begin to understand factors that lead online community members to return to the community, what has been termed resilience of the community in prior research ([Butler _et al._, 2014](#but14)), they can be better positioned to facilitate growth and sustainability of the community.

We suggest that further investigation of post-adoptive subjective norms with members in the early stages of adoption versus late stages of adoption be conducted. It is possible that the mixed results of prior research and the inconsistent results in our study reflect the fact that individuals are at different stages of continued usage. Another area for possible future research is mobile platforms. With the proliferation of smart phones and tablets, an increasing number of online community members are undoubtedly shifting their access point to a mobile devices. This shift in technology provides an opportunity for the online community to elicit even more from their user base due to the fact that they now have continuous access. However, it also presents challenges to the existing online platform, which was originally designed for non-mobile use. Future research should address these issues and investigate the implications regarding satisfaction with the community platform and continued use intention of its user base.

</section>

<section>

## Acknowledgements

We are grateful to the anonymous reviewers and Editor Charles Cole for excellent comments that improved the presentation of our research study.

</section>

<section>

## <a id="author"></a>About the author

**Dr. Barbara Apostolou** is Professor of Accounting in the College of Business and Economics at West Virginia University. She can be contacted at [baapostolou@wvu.edu](mailto:baapostolou@wvu.edu).  
**Dr. France Bélanger** is the R. B. Pamplin Professor and Tom and Daisy Byrd Senior Faculty Fellow in the Department of Accounting and Information Systems, Pamplin College of Business, Virginia Tech. She can be contacted at [belanger@vt.edu](mailto:belanger@vt.edu).  
**Dr. Ludwig Christian Schaupp** is the David and Nancy Hamstead Professor of Accounting in the College of Business and Economics at West Virginia University. He can be contacted at [Christian.Schaupp@mail.wvu.edu](mailto:christian.schaupp@mail.wvu.edu).

</section>

<section>

## References

<ul> 
<li id="agr12">Agrifoglio, R., Black, S., Metallo, C. &amp; Ferrara, M. (2012). Extrinsic versus intrinsic motivation in continued Twitter usage. <em>Journal of Computer Information Systems, 53</em>(1), 33-41.</li>
<li id="ajz89">Ajzen, I. (1989). Attitude structure and behavior. In A.R. Pratkanis, S.J. Breckler &amp; A.G. Greenwald (Eds.), <em>Attitude structure and function </em>(pp. 241-267). Hillsdale, NJ: Lawrence Erlbaum Associates.</li>
<li id="ajz91">Ajzen, I. (1991). The theory of planned behavior. <em>Organizational Behavior and Human Decision Processes, 50</em>(2), 179-211.</li>
<li id="ajz72">Ajzen, I. &amp; Fishbein, M. (1972). Attitudes and normative beliefs as factors influencing behavioral intentions. <em>Journal of Personality and Social Psychology, 21</em>(1), 1-9.</li>
<li id="and88">Anderson, J.C. &amp; Gerbing, D.W. (1988). Structural equation modeling in practice:  a review and recommended two-step approach. <em>Psychological Bulletin, 103</em>(3), 411-423.</li>
<li id="arn03">Arnold, Y., Leimeister, J.M. &amp; Krcmar, H. (2003). <em>COPEP: a development process model for a community platform for cancer patients.</em> Paper presented at the 11th European Conference on Information Systems (ECIS), Naples, Italy.</li>
<li id="bai83">Bailey, J.E. &amp; Pearson, S.W. (1983). Development of a tool for measuring and analysing computer user satisfaction. <em>Management Science, 29</em>(5), 530-545.</li>
<li id="bar95">Barclay, D., Higgins, C. &amp; Thompson, R. (1995). The partial least squares (PLS) approach to causal modeling: personal computer adoption and use as an illustration. <em>Technology Studies, 2</em>(2), 285-309.</li>
<li id="bel08">Bélanger, F. &amp; Carter, L. (2008). Trust and risk in e-government adoption. <em>Journal of Strategic Information Systems, 17</em>(2), 165-176.</li>
<li id="bel06">Bélanger, F. &amp; Hiller, J.S. (2006). A framework for e-government: privacy implications. <em>Business Process Management Journal, 12</em>(1), 48-60.</li>
<li id="bel02">Bélanger, F., Hiller, J.S. &amp; Smith, W.J. (2002). Trustworthiness in electronic commerce: the role of privacy, security, and site attributes. <em>Journal of Strategic Information Systems, 11</em>(3-4), 245-270.</li>
<li id="bha04">Bhattacherjee, A. &amp; Premkumar, G. (2004). Understanding changes in belief and attitude toward information technology usage: a theoretical model and longitudinal test. <em>MIS Quarterly, 28(</em>2), 229-254.</li>
<li id="bla02">Blanchard, A.L. &amp; Markus, M.L. (2002). <a href="http://www.webcitation.org/6usMNIE88)">Sense of virtual community: maintaining the experience of belonging.</a> In <em>Proceedings of the 35th Hawaii International Conference on System Sciences. HICSS35.</em> Washington, DC: IEEE Computer Society. Retrieved from https://pdfs.semanticscholar.org/ea03/80b7ba322657cb5e6e479c78213260767b0c.pdf   (Archived by WebCite&reg; at http://www.webcitation.org/6usMNIE88) </li>
<li id="bla04">Blanchard, A.L. &amp; Markus, M.L. (2004). The experienced 'sense' of a virtual community: characteristics and processes. <em>ACM SIGMIS Database, 35(</em>1), 65-79.</li>
<li id="bro91">Brown, J.S. &amp; Duguid, P. (1991). Organizational learning and communities-of-practice: toward a unified view of working, learning, and innovation. <em>Organization Science, 2(</em>1), 40-57.</li>
<li id="but14">Butler, B.S., Bateman, P.J., Gray, P.H. &amp; Diamant, E.I. (2014). An attraction–selection–attrition theory of online community size and resilience. <em>MIS Quarterly, 38</em>(3), 699-728.</li>
<li id="car05">Carter, L. &amp; Bélanger, F. (2005). The utilization of e-government services: citizen trust, innovation and acceptance factors. <em>Information Systems Journal, 15</em>(1), 5-25.</li>
<li id="cen08">Center for the Digital Future. (2008). <em>The 2008 digital future report.</em> Los Angeles, CA: USC Annenberg School for Communication.</li>
<li id="che11">Chen, J., Xu, H. &amp; Whinston, A.B. (2011). Moderated online communities and quality of user- generated content. <em>Journal of Management Information Systems, 28</em>(2), 237-268.</li>
<li id="chi95">Chin, W.W. &amp; Todd, P.A. (1995). On the use, usefulness, and ease of use of structural equation modeling in MIS research: a note of caution. <em>MIS Quarterly, 19</em>(2), 237-246.</li>
<li id="cla97">Clarke, R. (1997). <a href="http://www.webcitation.org/6usMePGYT">Promises and threats in electronic commerce</a>. Chapman, ACT, Australia: Xamax Consultancy Pty Ltd. Retrieved from http://www.rogerclarke.com/EC/Quantum.html.  (Archived by WebCite&reg; at http://www.webcitation.org/6usMePGYT)</li>
<li id="con90">Conrath, D.W. &amp; Mignen, O.P. (1990). What is being done to measure user satisfaction with EDP/MIS. <em>Information &amp; Management, 19(</em>1), 7-19.</li>
<li id="cro90">Crosby, L.A., Evans, K.R. &amp; Cowles, D. (1990) Relationship quality in services selling: an interpersonal influence perspective. <em>Journal of Marketing, 54</em>(3), 68-81.</li>
<li id="dah05">Dahlander, L. &amp; Magnusson, M.G. (2005). Relationships between open source software companies and communities: observations from Nordic firms. <em>Research Policy, 34(</em>4), 481-493.</li>
<li id="dav89">Davis, F.D. (1989). Perceived usefulness, perceived ease of use, and user acceptance of information technology. <em>MIS Quarterly, 13</em>(3), 319-340.</li>
<li id="del92">DeLone, W.H. &amp; McLean, E.R. (1992). Information systems success: the quest for the dependent variable. <em>Information Systems Research, 3</em>(1), 60-95.</li>
<li id="del03">DeLone, W.H. &amp; McLean, E.R. (2003). The DeLone and McLean model of information success: a ten-year update. <em>Journal of Management Information Systems, 19</em>(4), 9-30.</li>
<li id="del04">DeLone, W.H. &amp; McLean, E.R. (2004). Measuring e-commerce success: applying the DeLone &amp; McLean information systems success model. <em>International Journal of Electronic Commerce, 9</em>(1), 31-47.</li>
<li id="den12">Deng, X. &amp; Chi, L. (2012). Understanding postadoptive behaviors in information systems use: a longitudinal analysis of system use problems in the business intelligence context. <em>Journal of Management Information Systems, 29</em>(3), 291-325.</li>
<li id="dol04">Doll, W.J., Deng, X., Raghunathan, T.S., Torkzadeh, G. &amp; Xia, W. (2004). The meaning and measurement of user satisfaction: a multigroup invariance analysis of the end-user computing satisfaction instrument. <em>Journal of Management Information Systems, 21</em>(1), 227-262.</li>
<li id="dug13">Duggan, M. &amp; Smith, A. (2013). <em><a href="http://www.webcitation.org/6usN5Rzm8">Social media update 2013</a>.</em> Washington, DC: Pew Research Center. Retrieved from http://www.pewinternet.org/2013/12/30/social-media-update-2013.   (Archived by WebCite&reg; at http://www.webcitation.org/6usN5Rzm8)</li>
<li id="fis75">Fishbein, M. &amp; Ajzen, I. (1975). Belief, attitude, intention and behavior: an introduction to theory and research<em></em>. Reading, MA: Addison-Wesley Publishing Company.</li>
<li id="for82">Fornell, C. &amp; Bookstein, F.L. (1982) Two structural equation models: LISREL and PLS applied to consumer exit-voice theory. <em>Journal of Marketing Research, 19</em>(4), 440-452.</li>
<li id="for81">Fornell, C. &amp; Larcker, D.F. (1981). Evaluating structural equation models with unobservable variables and measurement error. <em>Journal of Marketing Research, 18</em>(1), 39-50.</li>
<li id="fur11">Furneaux, B. &amp; Wade, M. (2011). An exploration of organizational level information systems discontinuance intentions. <em>MIS Quarterly, 35(</em>3), 573-598.</li>
<li id="gar11">Garg, R., Smith, M.D. &amp; Telang, R. (2011). Measuring information diffusion in an online community. <em>Journal of Management Information Systems, 28</em>(2), 11-37.</li>
<li id="gar04">Garrety, K., Robertson, P.L. &amp; Badham, R. (2004). Integrating communities of practice in technology development projects. <em>International Journal of Project Management, 22</em>(5), 351-358.</li>
<li id="gef00">Gefen, D. (2000). E-commerce: the role of familiarity and trust. <em>Omega, 28</em>(6), 725-737.</li>
<li id="gef02">Gefen, D. (2002). Nurturing clients' trust to encourage engagement success during the customization of ERP systems. <em>Omega, 30</em>(4), 287-299.</li>
<li id="gef03">Gefen, D., Karahanna, E. &amp; Straub, D.W. (2003). Trust and TAM in online shopping: an integrated model. <em>MIS Quarterly, 27</em>(1), 51-90.</li>
<li id="gel98">Gelderman, M. (1998). The relation between user satisfaction, usage of information systems and performance. <em>Information &amp; Management, 34</em>(1), 11-18.</li>
<li id="gra00">Grazioli, S. &amp; Jarvenpaa, S.L. (2000). Perils of internet fraud: an empirical investigation of deception and trust with experienced Internet consumers. <em>IEEE Transactions on Systems, Man and Cybernetics, Part A: Systems and Humans, 30</em>(4), 395-410.</li>
<li id="guo11">Guo, K.H., Yuan, Y., Archer, N.P. &amp; Connelly, C.E. (2011) Understanding nonmalicious security violations in the workplace: a composite behavior model. <em>Journal of Management Information Systems, 28</em>(2), 203-236.</li>
<li id="hag97">Hagel, J. &amp; Armstrong, A.G. (1997). <em>Net gain: expanding markets through virtual communities</em>. Boston, MA: Harvard Business School Press.</li>
<li id="har09">Hara, N. (2009). <em>Communities of practice: fostering peer-to-peer learning and informal knowledge sharing in the workplace.</em> Berlin: Springer.</li>
<li id="hof99">Hoffman, D.L., Novak, T.P. &amp; Peralta, M. (1999). Building consumer trust online. <em>Communications of the ACM, 42</em>(4), 80-85.</li>
<li id="hsu04">Hsu, M.H. &amp; Chiu, C.M. (2004). Predicting electronic service continuance with a decomposed theory of planned behaviour. <em>Behavior &amp; Information Technology, 23</em>(5), 359-373.</li>
<li id="ive84">Ives, B. &amp; Olson, M.H. (1984). User involvement and MIS success: a review of research. <em>Management Science, 30</em>(5), 586-603.</li>
<li id="jar00">Jarvenpaa, S.L., Tractinsky, N. &amp; Vitale, M. (2000). Consumer trust in an Internet store. <em>Information Technology and Management, 1</em>(1-2), 45-71.</li>
<li id="jon00">Jones, S., Wilikens, M., Morris, P. &amp; Masera, M. (2000). Trust requirements in e-business. <em>Communications of the ACM, 43</em>(12), 80-87.</li>
<li id="kan12">Kang, H., Lee, M.J. &amp; Lee, J.K. (2012). Are you still with us? A study of the post-adoption determinants of sustained use of mobile-banking services. <em>Journal of Organizational Computing and Electronic Commerce, 22</em>(2), 132-159.</li>
<li id="kar99">Karahanna, E., Straub, D.W. &amp; Chervany, N.L. (1999). Information technology adoption across time: a cross-sectional comparison of pre-adoption and post-adoption beliefs. <em>MIS Quarterly, 23</em>(2), 183-213.</li>
<li id="kha12">Khayun, V., Ractham, P. &amp; Firpo, D. (2012). Assessing e-excise success with DeLone and McLean's model. <em>Journal of Computer Information Systems, 52</em>(3), 31-40.</li>
<li id="kim09">Kim, B. &amp; Han, I. (2009). The role of trust belief and its antecedents in a community-driven knowledge environment. <em>Journal of the American Society for Information Science and Technology, 60</em>(5), 1012-1026.</li>
<li id="kim05">Kim, S.S. &amp; Malhotra, N.K. (2005). A longitudinal model of continued IS use: an integrative view of four mechanisms underlying postadoption phenomena. <em>Management Science, 51</em>(5), 741-755.</li>
<li id="kuo03">Kuo, Y. (2003) A study on service quality of virtual community websites. <em>Total Quality Management &amp; Business Excellence, 14</em>(4), 461-473.</li>
<li id="lai14">Lai, J.-Y. (2014). E-SERVCON and e-commerce success: applying the DeLone &amp; McLean model. <em>Journal of Organizational and End User Computing, 26</em>(3), 1-22.</li>
<li id="lar80">Larzelere, R.E. &amp; Huston, T.L. (1980). The dyadic trust scale: toward understanding interpersonal trust in close relationships. <em>Journal of Marriage and the Family, 42</em>(3), 595-604.</li>
<li id="las05">Lasker, J.N., Sogolow, E.D. &amp; Sharim, R.R. (2005). <a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1550634/">The role of an online community for people with a rare disease: content analysis of messages posted on a primary biliary cirrhosis mailing list</a>. <em>Journal of Medical Internet Research, 7</em>(1), e10. Retrieved from http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1550634/ [Archiving not possible].  </li>
<li id="lei06">Leimeister, J.M., Sidiras, P. &amp; Krcmar, H. (2006). Exploring success factors of virtual communities: the perspectives of members and operators. <em>Journal of Organizational Computing and Electronic Commerce, 16</em>(3-4), 279-300.</li>
<li id="lia10">Liang, H. &amp; Xue, Y. (2010). Understanding security behaviors in personal computer usage: a threat avoidance perspective. <em>Journal of the Association for Information Systems, 11</em>(7), 394-413.</li>
<li id="lin06">Lin, H.-F. &amp; Lee, G.-G. (2006). Determinants of success for online communities: an empirical study. <em>Behavior &amp; Information Technology, 25</em>(6), 479-488.</li>
<li id="liu14">Liu, L., Wagner, C. &amp; Chen, H. (2014). Determinants of commitment in an online community: assessing the antecedents of weak ties and their impact. <em>Journal of Organizational Computing and Electronic Commerce, 24</em>(4), 271-296.</li>
<li id="lu12">Lu, J., Wang, L. &amp; Hayes, L.A. (2012). How do technology readiness, platform functionality and trust influence C2C user satisfaction? <em>Journal of Electronic Commerce Research, 13(</em>1), 50-69.</li>
<li id="lu10">Lu, Y., Zhao, L. &amp; Wang, B. (2010). From virtual community members to C2C e-commerce buyers: trust in virtual communities and its effect on consumers’ purchase intention. <em>Electronic Commerce Research and Applications, 9</em>(4), 346-360.</li>
<li id="mckin02">McKinney, V., Yoon, K. &amp; Zahedi, F.M. (2002). The measurement of web-customer satisfaction: an expectation and disconfirmation approach. <em>Information Systems Research, 13</em>(3), 296-315.</li>
<li id="mckche02">McKnight, D.H. &amp; Chervany, N.L. (2002). What trust means in e-commerce customer relationships: an interdisciplinary conceptual typology. <em>International Journal of Electronic Commerce, 6</em>(2), 35-59.</li>
<li id="mckcho02">McKnight, D.H., Choudhury, V. &amp; Kacmar, C. (2002). Developing and validating trust measures for e-commerce: an integrative typology. <em>Information Systems Research, 13</em>(3), 334-359.</li>
<li id="mckcum98">McKnight, D.H., Cummings, L.L. &amp; Chervany, N.L. (1998). Initial trust formation in new organizational relationships. <em>Academy of Management Review, 23</em>(3), 473-490.</li>
<li id="may95">Mayer, R.C., Davis, J.H. &amp; Schoorman, F.D. (1995). An integrative model of organizational trust. <em>Academy of Management Review, 20</em>(3), 709-734.</li>
<li id="mel90">Melone, N.P. (1990). A theoretical assessment of the user-satisfaction construct in information systems research. <em>Management Science, 36</em>(1), 76-91.</li>
<li id="moo91">Moore, G.C. &amp; Benbasat, I. (1991). Development of an instrument to measure the perceptions of adopting an information technology innovation. <em>Information Systems Research, 2(</em>3), 192-222.</li>
<li id="muy04">Muylle, S., Moenaert, R. &amp; Despontin, M. (2004). The conceptualization and empirical validation of Web site user satisfaction. <em>Information &amp; Management, 41</em>(5), 543-560.</li>
<li id="nel05">Nelson, R.R., Todd, P.A. &amp; Wixom, B.H. (2005). Antecedents of information and system quality: an empirical examination within the context of data warehousing. <em>Journal of Management Information Systems, 21</em>(4), 199-235.</li>
<li id="net90">Netemeyer, R.G., Johnston, M.W. &amp; Burton, S. (1990). Analysis of role conflict and role ambiguity in a structural equations framework. <em>Journal of Applied Psychology, 75(</em>2), 148-157.</li>
<li id="nev12">Nevo, S., Nevo, D. &amp; Kim, H. (2012). From recreational applications to workplace technologies: an empirical study of cross-context IS continuance in the case of virtual worlds. <em>Journal of Information Technology, 27</em>(1), 74-86.</li>
<li id="nun78">Nunnally, J.C. (1978). <em>Psychometric theory</em>. New York, NY: McGraw-Hill.</li>
<li id="par11">Park, J., Snell, W., Ha, S. &amp; Chung, T.-L. (2011). Consumers' post-adoption of m-services: interest in future m-services based on consumer evaluations of current m-services. <em>Journal of Electronic Commerce Research, 12</em>(3), 165-175.</li>
<li id="pav03">Pavlou, P.A. (2003). Consumer acceptance of electronic commerce: integrating trust and risk with the technology acceptance model. <em>International Journal of Electronic Commerce, 7</em>(3), 101-134.</li>
<li id="pet13">Petter, S., DeLone, W. &amp; McLean, E.R. (2013). Information systems success: the quest for the independent variables. <em>Journal of Management Information Systems, 29</em>(4), 7-62.</li>
<li id="pew14">Pew Research Center. (2014). <a href="http://www.webcitation.org/6usNOasOG"><em>Social networking fact sheet</em></a>. Washington, DC: Pew Research Center. Retrieved from http://www.pewinternet.org/fact-sheets/social-networking-fact-sheet. (Archived by WebCite&reg; at http://www.webcitation.org/6usNOasOG)</li>
<li id="pos10">Posey, C., Lowry, P.B., Roberts, T.L. &amp; Ellis, T.S. (2010). Proposing the online community self-disclosure model: the case of working professionals in France and the U.K. who use online communities. <em>European Journal of Information Systems, 19</em>(2), 181-195.</li>
<li id="pre00">Preece, J. (2000). <em>Online communities: designing usability, supporting sociability.</em> Chichester, UK: John Wiley &amp; Sons.</li>
<li id="pre01">Preece, J. (2001). Sociability and usability in online communities: determining and measuring success. <em>Behavior and Information Technology Journal, 20</em>(5), 347-356.</li>
<li id="qi12">Qi, J., Zhou, Y., Chen, W. &amp; Qu, Q. (2012). Are customer satisfaction and customer loyalty drivers of customer lifetime value in mobile data services: a comparative cross-country study. <em>Information Technology and Management, 13</em>(4), 281-296.</li>
<li id="rai02">Rai, A., Lang, S.S. &amp; Welker, R.B. (2002). Assessing the validity of IS success models: an empirical test and theoretical analysis. <em>Information Systems Research, 13</em>(1), 50-69.</li>
<li id="rai95">Rainer Jr., R.K. &amp; Watson, H.J. (1995). The keys to executive information system success. <em>Journal of Management Information Systems, 12</em>(2), 83-98.</li>
<li id="ray12">Ray, S., Kim, S.S. &amp; Morris, J.G. (2012). Research note: online users' switching costs: their nature and formation. <em>Information Systems Research, 23</em>(1), 197-213.</li>
<li id="ren12">Ren, Y., Harper, F.M., Drenner, S., Terveen, L., Kiesler, S., Riedl, J. &amp; Kraut, R.E. (2012). Building member attachment in online communities: applying theories of group identity and interpersonal bonds. <em>MIS Quarterly, 36</em>(3), 841-864.</li>
<li id="roq13">Roque, R. (2013, December). <a href="http://www.webcitation.org/6usQ954NO">Grabbing the silver lining: purchasing cloud-based solutions in the public sector</a>. Government Finance Review, 10-16. Retrieved from http://www.gfoa.org/sites/default/files/GFR_DEC_13_12.pdf  (Archived by WebCite&reg; at http://www.webcitation.org/6usQ954NO)</li>
<li id="rot71">Rotter, L.B. (1971). Generalized expectancies for interpersonal trust. <em>American Psychologist, 26</em>(5), 443-452.</li>
<li id="ryu05">Ryu, C., Kim, Y.J., Chaudhury, A. &amp; Rao, H.R. (2005). Knowledge acquisition via three learning processes in enterprise information portals: learning-by-investment, learning-by-doing, and learning-from-others. <em>MIS Quarterly, 29</em>(2), 245-278.</li>
<li id="san12">Sánchez-Franco, M.J., Buitrago-Esquinas, E.M. &amp; Yñiguez, R. (2012). How to intensify the individual's feelings of belonging to a social networking site? <em>Management Decision, 50</em>(6), 1137-1154.</li>
<li id="sch05">Schaupp, L.C. &amp; Bélanger, F. (2005). A conjoint analysis of online consumer satisfaction. <em>Journal of Electronic Commerce Research, 6</em>(2), 95-111.</li>
<li id="sch09">Schaupp, L.C., Bélanger, F. &amp; Weiguo, F. (2009). Examining the success of Websites beyond e- commerce: an extension of the IS success model. <em>Journal of Computer Information Systems, 49</em>(4), 42-52.</li>
<li id="sed97">Seddon, P.B. (1997). A respecification and extension of the DeLone and McLean model of IS success. <em>Information Systems Research, 8</em>(3), 240-253.</li>
<li id="sha87">Shapiro, S.P. (1987). The social control of impersonal trust. <em>American Journal of Sociology, 93</em>(3), 623-658.</li>
<li id="sia03">Siau, K. &amp; Shen, Z. (2003) Building customer trust in mobile commerce. <em>Communications of the ACM, 46</em>(4), 91-94.</li>
<li id="szy00">Szymanzki, D.M. &amp; Hise, R.T. (2000). E-satisfaction: An initial examination. <em>Journal of Retailing, 76</em>(3), 309-322.</li>
<li id="tan01">Tan, Y.H., &amp; Theon, W. (2001). Toward a generic model of trust for electronic commerce. <em>International Journal of Electronic Commerce, 5</em>(2), 61-74.</li>
<li id="teo08">Teo, T.S.H., Srivastava, S.C. &amp; Jiang, L. (2008). Trust and electronic government success: an empirical study. <em>Journal of Management Information Systems, 25</em>(3), 99-131.</li>
<li id="tho91">Thompson, R.L., Higgins, C.A. &amp; Howell, J.M. (1991). Personal computing: toward a conceptual model of utilization. <em>MIS Quarterly, 15</em>(1), 125-143.</li>
<li id="van04">Van Slyke, C., Bélanger, F. &amp; Comunale, C. (2004). Adopting business-to-consumer electronic commerce: the effects of trust and perceived innovation characteristics. <em>ACM SIGMIS Database, 35(</em>2), 32-49.</li>
<li id="ven00">Venkatesh, V. &amp; Davis, F.D. (2000). A theoretical extension of the technology acceptance model: four longitudinal field studies. <em>Management Science, 46</em>(2), 186-204.</li>
<li id="ven03">Venkatesh, V., Morris, M.G., Davis, G.B. &amp; Davis, F.D. (2003). User acceptance of information technology: toward a unified view. <em>MIS Quarterly, 27</em>(3), 425-478.</li>
<li id="wan12">Wang, X. &amp; Clay, P.F. (2012). Beyond adoption intention: online communities and member motivation to contribute longitudinally. <em>Journal of Organizational Computing and Electronic Commerce, 22</em>(3), 215-236.</li>
<li id="wan08">Wang, Y.-S. (2008). Assessing e-commerce systems success: a respecification and validation of the Delone and McLean model of IS success. <em>Information Systems Journal, 18</em>(5), 529-557.</li>
<li id="war02">Warkentin, M., Gefen, D., Pavlou, P.A. &amp; Rose, G.M. (2002). Encouraging citizen adoption of e- government by building trust. <em>Electronic Markets, 12</em>(3), 157-162.</li>
<li id="was05">Wasko, M.M. &amp; Faraj, S. (2005). Why should I share? Examining social capital and knowledge contribution in electronic networks of practice. <em>MIS Quarterly, 29</em>(1), 35-57.</li>
<li id="wel04">Welch, E.W. (2004) Linking citizen satisfaction with e-government and trust in government. <em>Journal of Public Administration Research and Theory, 15</em>(3), 371.</li>
<li id="wil00">Williams, R. &amp; Cothrel, J. (2000). Four smart ways to run online communities. <em>Sloan Management Review, 41</em>(4), 81-91.</li>
<li id="wix05">Wixom, B.H. &amp; Todd, P.A. (2005). A theoretical integration of user satisfaction and technology acceptance. <em>Information Systems Research, 16</em>(1), 85-102.</li>
<li id="wol85">Wold, H. (1985). Partial least squares. In S. Kotz &amp; N.L. Johnson (Eds.), <em>Encyclopedia of statistical sciences</em> (Vol. 6, pp. 581-591). New York, NY: Wiley. </li>
<li id="xu15">Xu, B., Xu, Z. &amp; Li, D. (2015). Internet aggression in online communities: a contemporary deterrence perspective. <em>Information Systems Journal, 26</em>(6), 641-667.</li>
<li id="zhang12">Zhang, K.Z.K., Cheung, C.M.K. &amp; Lee, M.K.O. (2012). Online service switching behavior: the case of blog service providers. <em>Journal of Electronic Commerce Research, 13</em>(3), 184-197.</li>
<li id="zhao12">Zhao, Z. &amp; Cao, Q. (2012). An empirical study on continual usage intention of microblogging: the case of Sina. <em>Nankai Business Review International, 3</em>(4), 413-429.</li>
</ul>

</section>

</article>