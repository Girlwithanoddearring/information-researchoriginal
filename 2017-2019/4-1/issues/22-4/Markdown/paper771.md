<header>

#### vol. 22 no. 4, December, 2017

</header>

<article>

# Looking at the Khmer Rouge archives through the lens of the records continuum model: towards an appropriated archive continuum model

## [Viviane Frings-Hessami](#author)

> **Introduction**. The records continuum model was developed in Australia in the 1990s to make sense of the multiple contexts in which records are created and managed. It has been applied to a variety of cases and contexts. The present article discusses its adequacy to represent the archival processes that take place in archives which are forcibly acquired following a violent regime change.  
> **Method**. This article applies continuum modelling to the case of the Khmer Rouge archive of the Tuol Sleng interrogation and incarceration centre. This approach is supplemented by historical and historiographical research.  
> **Analysis**. The analysis reveals some of the limitations of the records continuum model to explain the archival processes that took place at the time when the Tuol Sleng Archive was appropriated by the Cambodian government that succeeded the Khmer Rouge.  
> **Results**. The author proposes an appropriated archive continuum model which better represents the processes involved in constructing a new political archive with records from an original archive.  
> **Conclusions**. The appropriated archive continuum model presented in this article helps to explain the processes that occurred in the case of the Tuol Sleng Archive. More research is needed to ascertain whether it applies to other cases of politically appropriated archives.

<section>

## Introduction

The records continuum model was developed in Australia in the 1990s as an alternative to the records lifecycle model to make sense of the complex contexts in which records are created and managed and to represent the different perspectives from which records can be seen ([McKemmish, 2017](#mck17); [Upward, 1996](#upw96), [1997](#upw97)). The present article discusses its suitability to explain the processes that occur in archives that are politically appropriated by a successor government following a change of regime. The example of the Khmer Rouge Tuol Sleng Archive was chosen by the author for this purpose because of her background knowledge of Cambodian history and because of its emotive impact.

This article starts with a background introduction on the archive discovered at Tuol Sleng in January 1979 after the Khmer Rouge regime was toppled by a Vietnamese invasion. It then gives a brief explanation of the records continuum model, before applying it to the analysis of the records that constitute the Tuol Sleng Archive. It shows the limitations of the records continuum model to explain the processes that affected the archive when it was appropriated by the new government that was set up after the collapse of the Khmer Rouge regime and proposes a new model that better represents the processes that have affected the archive from the time it was created to the time it was made available for multiple reuses by multiple actors.

</section>

<section>

## Methodology

The present article uses the records continuum as a tool to analyse the recordkeeping and archival processes that were applied to the records from the Tuol Sleng Archive. It uses continuum modelling in an iterative way, testing first the records continuum model, then an alternative appropriated archive continuum model, then further refining this model. This approach is supplemented by historical and historiographical research into the history of the Tuol Sleng Archive and into the political context at the time the archive was appropriated by the successor government following the fall of the Khmer Rouge regime. By proposing an alternative continuum model, this article aims to contribute to the development of continuum theory. In the same way that in continuum thinking, records are '_always in a process of becoming_' ([McKemmish, 1994](#mck94)), continuum theory itself is constantly evolving, expanding and '_always becoming_' ([Evans, McKemmish and Rolan, 2017](#eva17)). Continuum theory, therefore, can enable different perspectives of records to be explored and result in different models.

</section>

<section>

## The Tuol Sleng Archive

The Khmer Rouge came to power in Cambodia on 17 April 1975 following the fall of the United States-backed government of Lon Nol, and immediately decreed the evacuation of Phnom Penh, heralding a reign of terror that engulfed Cambodia for the next three and a half years. In the late 1960s, the United States had extended the Vietnam War to Cambodia, launching air attacks on the Vietnamese communist fighters who were seeking refuge in Cambodia, thereby violating Cambodia's neutrality. In 1970, they supported a military coup by General Lon Nol who overthrew Norodom Sihanouk and escalated Cambodia's involvement in the war on the side of the United States ([Chandler, 1991](#cha91)). The American bombardments, and in particular the carpet bombing of the Cambodian countryside in 1973, which saw more bombs dropped on Cambodia in the space of six months than had been dropped during all of World War II, killed an estimated 50,000-150,000 people ([Kiernan, 1989](#kie89)), led to hundreds of thousands of refugees fleeing to Phnom Penh and radicalised the Cambodian peasantry, increasing support for the Khmer Rouge ([Chandler, 1991](#cha91); [Kiernan, 1985](#kie85); [Taylor, 2006](#tay06)). After they seized power, the Khmer Rouge renamed the country Democratic Kampuchea and set out to rebuild a classless agrarian society. All the population was sent to the countryside to work as slave labourers in the rice fields. Schools, hospitals and factories were closed, money was abolished, private property was confiscated, religion was banned, and all types of dissent were harshly repressed. An estimated 2 million people, or one quarter of the population died of starvation or disease, or were executed as enemies of the regime ([Chandler, 2008a](#cha08a)).

On 7 January 1979, the Khmer Rouge regime was toppled by a Vietnamese invasion and replaced by the pro-Vietnamese regime of the People's Republic of Kampuchea headed by Heng Samrin ([Gottesman, 2003](#got03); [Vickery, 1986](#vic86)). The day after the fall of Phnom Penh, the Tuol Sleng interrogation and incarceration centre, known during Democratic Kampuchea as S-21, was discovered by two Vietnamese photojournalists ([Chandler, 1999](#cha99)). The Vietnamese army and their Cambodian allies in Phnom Penh immediately understood the important role that the records they found there could play in the propaganda war to win the hearts and mind of the Cambodian people ([Ledgerwood, 1997](#led97)) and the support of the international community for their government. Vietnamese and Cambodian staff right away closed the site, cleaned it up and started looking through the records for evidence against the Khmer Rouge leadership ([Chandler, 1999](#cha99)). In February or March 1979, Mai Lam, a Vietnamese colonel, who had developed the Museum of American War Crimes in Saigon, was given the task of (re)organising the documents into an archive and transforming the place into a Museum of Genocidal Crimes ([Chandler, 1999](#cha99)). A Cambodian survivor of S-21, Ung Pech, became the director of the museum when it opened in 1980\. Ung Pech and Mai Lam travelled to Eastern Europe to visit Holocaust museums and exhibits and built displays that emphasised similarities between the Khmer Rouge so-called '_genocidal_' regime and Nazi Germany ([Chandler, 1999](#cha99); [Ledgerwood, 1997](#led97)).

For the government of the People's Republic of Kampuchea and the ruling Kampuchean People's Revolutionary Party to present the Khmer Rouge as a '_genocidal_' regime and to distance themselves from the Khmer Rouge movement, the history of the Cambodian communist movement had to be rewritten to suit the new political situation ([Chandler, 2008b](#cha08b); [Frings, 1997](#fri97)). The government of the People's Republic of Kampuchea, like all previous Cambodian governments, linked history writing to its own priorities ([Chandler, 2008b](#cha08b)). After the relations between the Khmer Rouge and the Vietnamese communists had turned sour in the mid-1970s, the Khmer Rouge had deleted all mentions of Vietnamese assistance from their official party history ([Chandler, 1983](#cha83); [Kiernan, 1985](#kie85)). Following the establishment of the People's Republic of Kampuchea with Vietnamese assistance in 1979, the new Cambodian communist leaders, most of whom were Khmer Rouge defectors, started revising the way their history had been written, emphasising collaboration with Vietnam, presenting the leaders of the People's Republic of Kampuchea as the authentic communists who had kept the communist fight, and demonising the Khmer Rouge who had deviated from the communist ideals and committed atrocities ( [Chandler, 2008b](#cha08b); [Frings, 1997](#fri97)). Official political writings blamed the '_Pol Pot-Ieng Sary clique_' for following the line of the Chinese Cultural Revolution, '_exciting and inflaming a doctrine of reactionary chauvinism_', implementing '_extremely barbarous measures_', transforming Cambodia into a '_hell on earth_', separating Cambodia '_from the alliance of the 3 countries of Indochina, and provoking a war of aggression against Vietnam to serve the expansionist stratagems of the reactionary Beijing Chinese who were plotting with American imperialism_' (1982 History of the Kampuchean People's Revolutionary Party, quoted in [Frings, 1997](#fri97), p. 835, author's translation). In the 1980s, the government of the People's Republic of Kampuchea worked hard at focusing people's anger on the '_Pol Pot-Ieng Sary clique_', basically on two people, and at diverting the blame away from their rank and file. They welcomed Khmer Rouge defectors who were willing to collaborate with the new regime ([Chandler, 1999](#cha99)).

The development of the Tuol Sleng Museum of Genocidal Crimes as evidence of the Khmer Rouge _genocide_ by the government of the People's Republic of Kampuchea and Vietnamese advisors can be placed in this context of rewriting the history of Cambodian communism and distancing themselves from the Khmer Rouge leadership, as part of the propaganda war to win the support of the Cambodian people and international recognition for their government. The museum was designed primarily for foreign consumption ([Ledgerwood, 1997](#led97), p. 89). Its exhibits were showed to international delegations from March 1979 ([Chandler, 1999](#cha99)), but it was not open to Cambodian citizens until July 1980 ([Ledgerwood, 1997](#led97)). The Museum of Genocidal Crimes was a communist state construction, which was developed by a Vietnamese, Mai Lam, and inspired by Holocaust memorials. It incorporated exhibits that were confronting and insensitive to Khmer Buddhist traditions, particularly a map of Cambodia made of skulls ([Chandler, 2008b](#cha08b); [Ledgerwood, 1997](#led97)). During the 1980s, Tuol Sleng played a central role in the propaganda to win international recognition for the Vietnamese-backed government and to attract the economic assistance that they desperately needed to rebuild the country after a decade of war and devastation ([Chanda, 1986](#cha86); [Ledgerwood, 1997](#led97)).

The archival collection that was found at Tuol Sleng in January 1979 is reported to have included more than 100,000 documents ([Caswell, 2014](#cas14), citing Kiernan who was one of the first Westerners to see it in the early 1980s). It included mugshot photographs, typed and handwritten confessions, interrogators' notes, lists of executed prisoners, study notebooks, staff records, Khmer Rouge periodicals and copies of speeches by Khmer Rouge officials ([Boua, Kiernan and Barnett, 1980](#bou80); [Chandler, 1999](#cha99); [Hawk, 1986](#haw86); [Ledgerwood, 1997](#led97)). In the chaos that accompanied the fall of the Khmer Rouge regime and the transition to the People's Republic of Kampuchea, many documents were lost, stolen, dispersed, or used as scrap paper ([Barnett, 1980](#bar80)). The earliest accounts tell us of records spread across the floor and records found in several nearby buildings ([Chandler, 1999](#cha99)) and even outside the buildings (Barnett, 1980). In the 1980s, those records were kept in poor conditions, unclassified ([Caswell, 2010](#cas10), [2014](#cas14)), except for the mugshots that were enlarged and put on display in the Museum of Genocidal Crimes.

From the records that have survived, we know that at least 14,000 prisoners were tortured at Tuol Sleng, then executed at Choeung Ek between May 1976, when the interrogation centre became operational, and January 1979 ([Chandler, 1999](#cha99)). For 5,190 of them, the mugshots that were taken when they were brought to Tuol Sleng have survived and are now available online through the Documentation Centre of Cambodia photographic database ([2017c](#doc17c)). Most of these prisoners were Khmer Rouge cadres who had fallen out of favour with the regime ([Chandler, 1999](#cha99)). Some of them would have been associated with the Khmer Rouge's atrocities and, therefore, they cannot all be seen as absolute victims of the regime. Only twenty-three prisoners are known to have survived until January 1979 ([Documentation Centre of Cambodia, 2011](#doc11)).

As part of international efforts to ensure the preservation of the archive and its ability to be used to document the Khmer Rouge atrocities, the microfilming of the Tuol Sleng Archive was organised in 1989-93 by Cornell University and produced two sets of microfilms, one of which was deposited with Cornell University Library, the other kept at Tuol Sleng ([Caswell, 2014](#cas14); [Chandler, 1999](#cha99)). In the 1990s, the Cambodian Genocide Program was set up at Yale University, with United States government funding, to preserve, microfilm, then digitise the records with a view to gather materials to be used as evidence in an international court of law. The Documentation Centre of Cambodia was set up in 1994 as the Cambodian Genocide Program field office in Phnom Penh with Youk Chhang, a Cambodian American refugee and survivor of torture under the Khmer Rouge as its director. In 1997, it became an independently-run Cambodian non-governmental organisation ([Caswell, 2014](#cas14)). Over the years, it acquired from various sources around the country and outside Cambodia additional materials dating from the Democratic Kampuchea era not previously known to have survived, and added them to its collections ([Caswell, 2014](#cas14)). It now houses around one million documents about the Khmer Rouge, including parts of the Tuol Sleng Archive ([Documentation Centre of Cambodia, 2017b](#doc17b); [Chandler, 2008b](#cha08b)). It also organised for all the Tuol Sleng records to be microfilmed and for copies to be deposited in repositories in the United States and in Australia so that copies would survive if the originals were destroyed by political opponents in Cambodia ([Caswell, 2014](#cas14)). In addition, it created four databases and made them accessible online: a biographic database that includes biographic data on over 30,000 victims of the Khmer Rouge, a bibliographic database that records information about nearly 3,000 documents relating to the Khmer Rouge atrocities, a geographic database that maps the sites of Khmer Rouge prisons, execution centres and mass graves, and a photographic database that consists of the 5,190 surviving mugshots found at Tuol Sleng ([Cambodian Genocide Program, 2017](#cam17); [Documentation Centre of Cambodia, 2017a](#doc17a)).

</section>

<section>

## Records continuum model

The records continuum model was developed at Monash University in the 1990s by Frank Upward and his colleagues Sue McKemmish and Livia Iacovino in an effort to make sense of the complex contexts in which records are created and managed ([McKemmish, 2017](#mck17); [Upward, 1996](#upw96), [1997](#upw97)). The model challenges the definition of archives as records that have been selected for permanent preservation, and emphasises instead the continuity between records and archives. In continuum thinking, archives are thought as archives from the moment of their creation ([McKemmish, Upward and Reed, 2010](#mck10)), and recordkeeping (in one word) encompasses both the making and the management of records and archives ([McKemmish, 2017](#mck17)). The records continuum model highlights the evidential, transactional and contextual nature of records ([McKemmish, 2017](#mck17)). It sees records as logical objects that fix the documentary traces of social and organisational activities when they are captured in recordkeeping systems that have been designed to capture their evidential qualities and preserve them through time and space ([McKemmish _et al._, 2010](#mck10)). Yet, records are '_always in a process of becoming_' (McKemmish, 1994) as their contexts and use are always susceptible to change.

The four dimensions of the records continuum model are generally represented as four concentric circles (see Figure 1), but these dimensions coexist in and through time, they are not stages, and records do not always follow a linear process. The records continuum model makes it possible to look at records from different perspectives and to represent the different perspectives from which a record can be seen and the different recordkeeping trails that it may follow. In the first dimension, Create, actions take place, leave a trace that something happened, and are recorded in documents. To be able to function as evidence of the acts that they represent, they need to be captured in a recordkeeping system, which places them in a context and adds the necessary metadata for them to act as evidence. These processes take place in the second dimension, Capture. In the third dimension, Organise, the records are placed in a recordkeeping system at the organisation level so that they can act as evidence of the organisation's activities. In the fourth dimension, Pluralise, the records, are released outside the boundaries of the organisation that produced them so that they can contribute to the social memory of the broader community and can be reused in multiple ways and forms ([McKemmish _et al._, 2010](#mck10)). The four axes of the records continuum model map the documents created on the recordkeeping containers axis, the actors who are involved in the recordkeeping processes on the identity axis, the actions that take place on the transactional axis and their evidential qualities on the evidential axis.

The records continuum model is the best known, but, over the years, other continuum models have been developed. Frank Upward proposed several other models including an information continuum model, an information system continuum model, a publishing continuum model ([Upward 2000](#upw00), [2005a](#upw05a)) and a cultural heritage continuum model ([Upward, 2005b](#upw05b)). Current continuum research is leading to the development of participatory models that rename the first dimension co-create to reflect the multiple actors with rights in the records that are created ([Evans, _et al._, 2017](#eva17)). At Monash University, Rolan ([2017](#rol17)) has developed a participatory recordkeeping model and Gibbons ([2015](#gib15)) a mediated recordkeeping model.

<figure>

![Figure 1](../p771fig1.jpg)

<figcaption>

Figure 1: The records continuum model (based on [Upward, 1996](#upw96)). Reproduced with the permission of Frank Upward.</figcaption>

</figure>

</section>

<section>

## Application of the records continuum model to the Tuol Sleng Archive

### Create dimension

In the _Create_ dimension, the mugshots and confessions of the prisoners who were incarcerated at Tuol Sleng and all the S-21 administrative records were created in a systematic way following some standardised documentary forms. When they were brought to Tuol Sleng, prisoners were photographed by a Khmer Rouge cadre from the photography sub-unit to create a record of their arrival and incarceration ([Caswell, 2014](#cas14)). When they were interrogated and forced under torture to write confession after confession until they _confessed_ what their interrogators wanted to hear and implicated a sufficient number of accomplices, the documentation unit recorded, then transcribed or typed their confessions in a standardised form. The photography sub-unit and the documentation unit were part of an elaborate Khmer Rouge apparatus used to report arrests up the chain of command. The prisoners' mugshots and later their confessions both recorded and produced their criminality. They were considered guilty from the moment they arrived at Tuol Sleng ([Chandler, 1999](#cha99)) and their confessions became evidence of it ([Caswell, 2014](#cas14)).

The acts that took place in the create dimension consisted of taking the photographs, writing lists and reports, and writing, transcribing or typing the prisoners' confessions. The actors were the prisoners, the photographer, the administrative staff and the guards. The documents created were the mugshots, the confessions and various reports, lists and administrative documents. These documents are a trace that the prisoners were arrested and, by implication, had committed a treasonous activity.

However, not every act results in the creation of a record. To become a record, the photographs and the confessions had to be captured in a recordkeeping system. The process of creating criminals was part of the Khmer Rouge system of classification that divided society against class lines and ethnic lines ([Caswell, 2014](#cas14); [Chandler, 1999](#cha99)). To justify its economic failures and its repressive policies, the regime needed enemies and evidence against them. The Khmer Rouge became obsessive recordkeepers, meticulously documenting orders, keeping detailed logbooks of torture sessions, and compiling draft after draft of forced confession statements ([Caswell, 2014](#cas14); [Chandler, 1999](#cha99)).

</section>

<section>

### Capture dimension

In the _Capture_ dimension, the mugshots and confessions were captured in each prisoner's file. They were placed in a context by being inserted in a file with all the other records relating to each prisoner. Together these records provided evidence of the prisoner's alleged treason against the Khmer Rouge regime.

The work unit involved in the capture dimension was S-21's documentation unit. Their activities consisted of compiling the files to gather evidence of the prisoners' criminal activities and treason against the regime. The records captured were the prisoners' files and the various summaries and reports that they produced to send to their superiors. The documentation unit, which may have included more than fifty staff ([Chandler, 1999](#cha99), p. 27) was responsible for transcribing the confessions that were tape-recorded, typing the handwritten ones, preparing summaries and maintaining the prison's files ([Chandler, 1999](#cha99)). Over time, their activities became more routinised and their functions more defined. The forms became more formal in appearance, and retrieval and use became more standardised. A card index was developed ([Adam, 1998](#ada98); [Barnett, 1980](#bar80)). A large group of clerks were kept busy reading and rereading the confessions, stamping, annotating, and filing them ([Adam, 1998](#ada98)).

</section>

<section>

### Organise dimension

In the _Organise_ dimension, the prisoners' files were part of S-21's recordkeeping system. Together with the other records relating to the functioning of the prison, they were classified and organised to constitute the archive of the security police, the _Santebal_, and provide evidence that it was fulfilling its task of prosecuting the _enemies_ of the regime.

The archive that was constituted in the organise dimension was the S-21 Archive. The organisation involved was the security police. The function of the archive was to provide evidence against the enemies of the regime, and the organisational memory that it contributed to was the 'crimes' that the prisoners had committed against the regime and the way they had been dealt with. The records were proof of the 'hard work' that the Santebal and the S-21 staff were doing ([Chandler, 1999](#cha99), p. 50).

</section>

<section>

### Pluralise dimension

In the _pluralise_ dimension of the records continuum model, a group or organisational archive is made accessible to the broader community as collective memory. The S-21 Archive constitutes an essential part of the archives of the Khmer Rouge regime. When it entered the pluralise dimension, its records became the objects of multiple reuses in multiple contexts that could not have been imagined when the Khmer Rouge secret police compiled them. Various people and institutions became involved in pluralising them and in using them as evidence _against_ the Khmer Rouge regime. Pluralising the archive in such a way was obviously never the intention of the Khmer Rouge. What happened to the archive went _against_ what they had intended it to be and only occurred after their regime collapsed and the archive was taken over by the government that replaced them. This political appropriation of the archive and its reinterpretation by those who appropriated it make it necessary for us to look at it in a different way than through the lens of the records continuum model.

</section>

<section>

## Towards an appropriated archive continuum model

Up to January 1979, the Tuol Sleng Archive was built up as part of the repressive apparatus of the Khmer Rouge regime. Records were created, captured and organised at Tuol Sleng both as evidence against the crimes committed by the enemies of the regime and as evidence that the S-21 unit was working hard at '_smashing_' them ([Chandler, 1999](#cha99)). In the create dimension, mugshots were taken when the prisoners arrived at S-21, they were made to write confessions, and reports were written about them. These records attest that the prisoners were at Tuol Sleng during that time and constitute a trace that they were suspected of having committed a criminal activity. After S-21's documentation unit captured all the records into prisoners' files, they could be used as evidence of their criminal activities. Together all the prisoners' files constituted the S-21 Archive in the organise dimension and accomplished the function of providing evidence that the security police were working at unmasking the enemies of the regime and at eliminating them (see Figure 2).

<figure>

![Figure 2](../p771fig2.jpg)

<figcaption>Figure 2: Building the Tuol Sleng Archive up to 1979</figcaption>

</figure>

However, the archive was not released in the pluralise dimension by the people who supervised its creation. The Khmer Rouge were a very secretive regime and it is unlikely that they would have ever intended to release any of their archives into the public domain. Scholars of Cambodia believe that their intention in creating and preserving the Tuol Sleng records was that one day they could be used to write a comprehensive history of the Cambodian communist party ([Chandler, 1999](#cha99)). Based on this hypothesis, the potential pluralise dimension that they had in mind would look like what is represented in Figure 3\. The Tuol Sleng Archive would have become part of the Archives of Democratic Kampuchea and could have been used by party historians and sympathisers to write about the history of the communist party in Cambodia or, more generally, about Cambodian history.

<figure>

![Figure 3](../p771fig3.jpg)

<figcaption>Figure 3: Potential pluralise dimension in a Khmer Rouge regime.</figcaption>

</figure>

That transition to a pluralise dimension never happened. Following the Vietnamese invasion of Cambodia and the installation of a pro-Vietnamese government in Phnom Penh in January 1979, the Tuol Sleng Archive was discovered and appropriated by the new regime, which set out to use it for its own political aims. The archive was reinterpreted and repurposed before being released into the pluralise dimension as part of the propaganda of the government of the People's Republic of Kampuchea. This political appropriation and reinterpretation of the archive are fundamentally different from normal curatorial processes. The archive was taken out of its context, its structure was changed. The organisation that had compiled it and had maintained it no longer existed. The people in charge of it had fled. In the upheavals that accompanied the regime change, parts of the archive had been lost or destroyed. Records found in different places were put together. The archive was seized and managed for political purposes. Because of all these processes, the original archive no longer exists and cannot be exactly reconstructed in its original form.

Although the records continuum model was designed to accommodate the multiple uses and reuses that records can be subjected to, it cannot adequately represent a situation like this. The author postulates that an additional dimension is required between organise and pluralise to account for these processes, a dimension where the appropriation and reinterpretation processes occurred and radically transformed the archive before it was propelled into the pluralise dimension. These processes were driven by an outside political intervention. This was the key factor that led to the reinterpretation of the archive and the curation of some of the records into a Museum of Genocidal Crimes, which was set up to publicise the crimes committed by the Khmer Rouge and to build a case for international recognition for the People's Republic of Kampuchea. In the pluralise dimension, the archive of the Khmer Rouge secret police became the archive of the Museum of Genocidal Crimes controlled by the government of the People's Republic of Kampuchea. It was curated and reinterpreted to become evidence of the crimes of the Khmer Rouge, which could be used against them in a court of justice, as well as for educating the public and remembering the victims of the Khmer Rouge in many ways, in stories, books, exhibitions, documentaries, and various acts of witnessing ([Caswell, 2014](#cas14)). The processes that took place between organise and pluralise are fundamentally different from ordinary curatorial processes and from the technology-driven processes in the curate dimension of Gibbons' mediated recordkeeping model (2015). To make sense of them, the author proposes to add an additional dimension between _Organise_ and _Pluralise_, _Appropriate_ and represent it as an additional circle as illustrated in Figure 4.

<figure>

![Figure 4](../p771fig4.jpg)

<figcaption>Figure 4: Appropriated archive continuum model v.1</figcaption>

</figure>

However, this model only partly accounts for the processes that occurred in the space-time dimension between the construction of the archive and its pluralisation. The processes did not just consist of curation and reinterpretation activities. From a continuum point of view, they involved the creation, capture, organisation and pluralisation of _new_ records. Therefore, it would be more suitable to represent the processes that took place in the appropriate dimension as an arrow between one records continuum model representation that stops at the organise dimension and another one that brings the records into the pluralise dimension as shown in Figure 5\. By being reused and reinterpreted, the records were captured in a different context as evidence of something else and therefore became new records. They were re-created, re-captured and re-organised in a new context. Following their appropriation and reinterpretation, the Tuol Sleng records became the records of the government-controlled Tuol Sleng Museum of Genocidal Crimes where they were captured and organised as part of an archive that documented the crimes of the Khmer Rouge, then released into the pluralise dimension to be used in many ways by many different actors, but with their new organisational context influencing their potential to act as evidence. What was made available publically from then on was influenced by the way the archive had been repurposed and reinterpreted. The repurpose of the archive came as an outside intervention, but those who appropriated the archive from then on started controlling what they did with the archive and what they released from it, when and to whom. Once the archive had been appropriated and reinterpreted, the information flows that took place were from the inside out and under the control of those in charge of the archive.

In the case of the Tuol Sleng Archive, the appropriation of the archive occurred after the records had been organised as part of an elaborate bureaucratic system. In another context, appropriation could take place before the records are systematically captured and organised. The representation of the appropriate dimension as an arrow between two records continuum representations rather than as an additional circle between the organise and the pluralise dimensions makes it possible for the model to accommodate an appropriation of records that would take place after the records had been created but without them having been captured and organised by the organisation that created them, skipping the second and third dimensions.

<figure>

![Figure 5](../p771fig5.jpg)

<figcaption>Figure 5: Appropriated archive continuum model v.2</figcaption>

</figure>

</section>

<section>

## Discussion

The appropriated archive continuum model presented in the previous section helps us to understand that the Tuol Sleng Archive is a different archive from the one built up during the Khmer Rouge regime. The original archive can no longer be reconstructed. The records that are now in the archive have been found in several places and brought together in one archive ([Chandler, 1999](#cha99)), breaking the archival principle of _respect des fonds_, respect for the original order in which the records had been placed by the organisation that created them. In the upheaval of the first months following the breakdown of the Khmer Rouge regime, many records were lost, stolen, misappropriated or destroyed ([Barnett, 1980](#bar80); [Caswell, 2014](#cas14)). Thousands of records were '_dispersed from their original locations, leaving their chain of custody untraceable_' ([Caswell, 2014](#cas14), p. 67). During the 1980s, the remaining records were kept in poor conditions and remained uncatalogued ([Caswell, 2010](#cas10); [Caswell, 2014](#cas14)), stacked unsystematically on open desks and in old cupboards ([Barnett, 1980](#bar80)). The Cambodian staff involved in looking after them were unskilled in archival principles and in the preservation of records ([Barnett, 1980](#bar80); [Caswell, 2014](#cas14)). Some records known to have been in the archive in 1979 later disappeared ([Barnett, 1980](#bar80); [Chandler, 1999](#cha99), p. 170, n. 42). Conversely, some records were later added to the archive. In 1995, additional S-21 records, which had been held by the Ministry of Interior and are believed to have come from the archive of the Democratic Kampuchea Minister of Defence and National Security, Son Sen, were added to the Documentation Centre of Cambodia's collections ([Chandler, 1999](#cha99)). More records were found in the following years ([Chandler, 1999](#cha99)).

Right from the start, the restructuration of the archive was done for political reasons. In the early 1980s, many Cambodian people and some sections of the international community were concerned by the involvement of Vietnamese advisors. They questioned the ulterior motives of the Vietnamese ([Ledgerwood, 1997](#led97)) and the authenticity of a museum established by foreigners to press the message of parallels with the Holocaust ([Chandler, 1999](#cha99)). Ten Vietnamese advisors were reported to have been working in the archive in 1979 ([Chandler, 1999](#cha99), p. 163, n. 20). Mai Lam stayed on until 1988 as an expert-consultant ([Chandler, 1999](#cha99); [Ledgerwood, 1997](#led97)). In turning Tuol Sleng into a Museum of Genocidal Crimes and reorganising the records into a new archive, his aim was to '_arrange Cambodia's recent past to fit the requirements of the PRK [People's Republic of Kampuchea] and its Vietnamese mentors_' ([Chandler, 1999](#cha99), p. 5; [Chandler, 2008b](#cha08b)). Moreover, some of the records that were added to the archive in 1995, contained annotations in Vietnamese, which suggests that they had been reviewed by Vietnamese advisors in the 1980s ([Chandler, 1999](#cha99), p. 12).

Preservation efforts through microfilming and digitising the archive have focused on ensuring that the records would be preserved rather than on rebuilding the original structure of the archive. Cornell University first organised the microfilming of the archive in 1989-1993 at a time when the archive's continued existence was in doubt following the Vietnamese troops' withdrawal from Cambodia in 1989\. Later, Yale University's Cambodian Genocide Program and the Documentation Centre of Cambodia worked at building thematic databases that placed the information extracted from the records in new contexts with new metadata rather than on reconstructing the original structure of the archive with its original metadata. The involvement of the Cambodian Genocide Program was politically motivated and the Documentation Centre of Cambodia was actively engaged in advocating, then providing support for the mixed International-Cambodian tribunal, the Extraordinary Chambers in the Courts of Cambodia, which was established to bring to justice a handful of the most senior Khmer Rouge leaders ([Caswell, 2014](#cas14)). The Documentation Centre of Cambodia also played a role in (re)constructing the memory of the Khmer Rouge era through their publications and outreach campaigns ([Caswell, 2010](#cas10)).

The many reuses of records from the Tuol Sleng Archive, particularly the mugshots in the popular literature, in films and documentaries and in personal records, but also the translation and use of some of the confessions by scholars of Cambodia ([Chandler 1992](#cha92), [1999](#cha99); [Kiernan, 1985](#kie85), [1996](#kie96)), have led to the creation of a new archive of responses to the Khmer Rouge. The new records that have been created support human rights by '_memorializing the dead_' and '_holding those responsible legally accountable_' ([Caswell, 2014](#cas14), p. 7).

However, the repurpose of the Tuol Sleng Archive raises questions about the authenticity and the reliability of the archive and about its use as an instrument of accountability. According to records continuum thinking, the usefulness of records is '_bound up with how they have been created, how they have been captured in recorded form, how they have been organized for distribution and recall, and how accessible they are on a continuing basis_' ([McKemmish _et al._, 2010](#mck10), p. 4447). We can question to what extent the '_crafting of Tuol Sleng as a "genocide museum"_' in the early 1980s superseded and distorted the effort to preserve S-21 as it existed during the Khmer Rouge regime ([Ledgerwood, 1997](#led97), p. 89). The files were reorganised, labelled and numbered to make them more accessible ([Adam, 1998](#ada98)). Links are likely to have been lost in the process. Records that were incriminating to the new regime may have been removed or tampered with in the early days of the People's Republic of Kampuchea. Key confessions are reported to have disappeared ([Chandler 1999](#cha99), p. 164, n. 22), while others are said to have been stolen by defectors ([Barnett, 1980](#bar80)). Can the remaining records be trusted as accurate, complete, reliable and authentic sources of information? To what extent did the politically-motivated reorganisation of the archive impact on later reuses of the archive as an instrument of accountability in the Khmer Rouge trials? Did the frameworks that have been built ensure that the records retain their authenticity through time and space so that they could continue to be used as evidence? What is the Tuol Sleng Archive evidence of? Does it constitute evidence of the crimes of the Khmer Rouge or of the politically motivated repurpose of an archive to enable it to be used against the Khmer Rouge leadership?

While no expert doubts the authenticity of the content of the records in the Tuol Sleng Archive and the truth of the horrors that they document ([Chandler, 1999](#cha99); [Hawk, 1986](#haw86); Heder, as cited in [Ledgerwood, 1997](#led97); [Kiernan, 2008](#kie08); [Vickery, 1999](#vic99)), the politically-motivated repurposing and restructuring of the archive raise questions about the possibility of important metadata having been lost in the process and consequently about the ability for the archive to provide irrefutable evidence of the crimes against humanity committed by the Khmer Rouge.

</section>

<section>

## Conclusion

The appropriated archive continuum model presented in this paper brings a new light on one of the most macabre archives of the 20th century. While there is no doubt about the authenticity of the content of the records that it contains and their usefulness as instruments of memory, witnessing and education, looking at the archive from a continuum perspective brings attention back to the re-creation of the archive as a politically-motivated act of appropriation of the records and to their reinterpretation as the antithesis of what they had been created for. A close examination of the processes that took place in the time/space dimension when the archive was appropriated and transformed into a new archive raises questions about the way the metadata that linked the records back to their context was preserved and therefore about their ability to act as evidence. Further research is needed to ascertain whether this model could be applied to other cases of politically appropriated archives in conflict zones or in periods of political turmoil. In the case of Cambodia, it brings attention to a period of time (the early 1980s), which has received little scholarly attention, and highlights the need for more research on the archival processes that took place at that time. From an archival perspective, the model demonstrates that continuum thinking and continuum modelling are amenable to the development of new continuum models to better represent different recordkeeping contexts.

</section>

<section>

## Acknowledgements

I am grateful to Sue McKemmish for her comments on an earlier version of the model presented in this paper, which enabled me to further refine the model.

It is with sadness that I heard of the passing of Michael Vickery when I was writing this paper. His ground-breaking work on Cambodia, as well as that of David Chandler, with whom I worked in the early 1990s, greatly influenced my understanding of Cambodian history and politics and my decision to carry research in that area.

</section>

<section>

## <a id="author"></a>About the author

**Viviane Frings-Hessami** is a lecturer in the Centre for Organisational and Social Informatics at Monash University, Australia. Over the past two years, she has been involved in teaching the Archives and Recordkeeping units and in redeveloping the curriculum for the Information and Knowledge Management specialisation. She holds a PhD in Political Science from Monash University and an MA in South East Asian Studies from the University of Kent at Canterbury. In the 1990s, she carried out doctoral research on Cambodian history and political economy. She can be contacted at [viviane.hessami@monash.edu](mailto:viviane.hessami@monash.edu).

</section>

<section>

## References

<ul> 
<li id="ada98"> Adam, D. (1998). The Tuol Sleng archives and the Cambodian genocide. <em>Archivaria, 45</em>, 5-26.</li>
<li id="bar80"> Barnett, A. (1980, May 2). Inside S 21. <em>New Statesman</em>, 671.</li>
<li id="bou80"> Boua, C. Kiernan, B. &amp;amp; Barnett, A. (1980, May 2). Bureaucracy of Death. <em>New Statesman</em>, 668-676.</li>
<li id="cam17"> Cambodian Genocide Program. (2017). <a href="http://www.webcitation.org/6uQXoQKYB"><em>Introduction to the Cambodian Genocide Databases</em></a>. Retrieved from http://gsp.yale.edu/case-studies/cambodian-genocide-program/cambodian-genocide-databases-cgdb/introduction-cambodian (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQXoQKYB)</li>
<li id="cas10"> Caswell, M. (2010). Khmer Rouge archives: accountability, truth and memory in Cambodia. <em>Archival Science, 10</em>, 25-44.</li>
<li id="cas12"> Caswell, M. (2012). Using classification to convict the Khmer Rouge. <em>Journal of Documentation, 68</em>(2), 162-184.</li>
<li id="cas14"> Caswell, M. (2014). <em>Archiving the unspeakable: silence, memory and photographic record in Cambodia</em>. Madison, WI: The University of Wisconsin Press.</li>
<li id="cha86"> Chanda, N. (1986). <em>Brother enemy: the war after the war</em>. San Diego, CA: Harcourt Brace Jovanovich.</li>
<li id="cha83"> Chandler, D. (1983). Revising the past in Democratic Kampuchea: when was the birthday of the party? <em>Pacific Affairs, 56</em>(2), 288-300.</li>
<li id="cha91"> Chandler, D. (1991). <em>The tragedy of Cambodian history: politics, war and revolution since 1945</em>. New Haven, CT: Yale University Press.</li>
<li id="cha92"> Chandler, D. (1992). <em>Brother Number One</em>. St Leonards, NSW: Allen &amp; Unwin.</li>
<li id="cha99"> Chandler, D. (1999). <em>Voices from S-21: terror and history in Pol Pot's secret prison</em>. Berkeley, CA: University of California Press.</li>
<li id="cha08a"> Chandler, D. (2008a). <em>A history of Cambodia</em> (4th ed.). Boulder, CO: Westview Press.</li>
<li id="cha08b"> Chandler, D. (2008b). Cambodia deals with its past: collective memory, demonization and induced amnesia. <em>Totalitarian Movements and Political Religions, 9</em>(2-3), 355-369.</li>
<li id="doc11"> Documentation Centre of Cambodia. (2011). <a href="http://www.webcitation.org/6uQatGYUr"><em>Fact sheet: Pol Pot and his prisoners at secret prison S-21</em></a>. Retrieved from http://www.d.dccam.org/Archives/Documents/Confessions.htm (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQatGYUr)</li>
<li id="doc17a"> Documentation Centre of Cambodia. (2017a). <a href="http://www.webcitation.org/6uQbLCuIv"><em>DC-Cam Khmer Rouge history database</em></a>. Retrieved from http://d.dccam.org/Database/Index.htm (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQbLCuIv)</li>
<li id="doc17b"> Documentation Centre of Cambodia. (2017b). <a href="http://www.webcitation.org/6uQYeOisS"><em>Documenting, disseminating and preserving records of past atrocities</em></a>. Retrieved from http://d.dccam.org/Projects/Document_Projects/Documentation.htm (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQYeOisS)</li>
<li id="doc17c"> Documentation Centre of Cambodia. (2017c). <a href="http://www.webcitation.org/6uQYvBB41"><em>Photographic database</em></a>. Retrieved from http://d.dccam.org/Database/Photographic/Cts.php (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQYvBB41)</li>
<li id="eva17"> Evans, J., McKemmish, S. &amp;amp; Rolan, G. (2017). <a href="http://www.webcitation.org/6uQZ8OIEr">Critical archiving and recordkeeping research and practice in the continuum</a>. <em>Journal of Critical Library and Information Studies, 1</em>(2). Retrieved from http://libraryjuicepress.com/journals/index.php/jclis/article/view/35 (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQZ8OIEr)</li>
<li id="fri97"> Frings, V. (1997). Rewriting Cambodian history to 'adapt' it to a new political context: the Kampuchean People's Revolutionary Party's historiography (1979-1991). <em>Modern Asian Studies, 31</em>(4), 807-846.</li>
<li id="gib15"> Gibbons, L. (2015). <a href="http://www.webcitation.org/6uQbcbJFp">Continuum informatics and the emergent archive</a>. In L. Stillman, T. Denison and M. Anwar (Eds.), <em>Privilege, Information, knowledge &amp; power: an endless dilemma? 12th Prato CIRN Conference 9-11 November 2015, Monash Centre, Prato, Italy</em> (pp. 82-94). Caulfield, VIC: Monash University, Centre for Organisational and Social Informatics. Retrieved from https://cirn.wikispaces.com/Conference+2015 (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQbcbJFp)</li>
<li id="got03"> Gottesman, E. (2003). <em>Cambodia after the Khmer Rouge: inside the politics of nation building</em>. New Haven, CO: Yale University Press.</li>
<li id="haw86"> Hawk, D. (1986). The Tuol Sleng Extermination Center. <em>Index on Censorship, 15</em>(1), 25-31.</li>
<li id="kie85"> Kiernan, B. (1985). <em>How Pol Pot came to power: a history of communism in Kampuchea, 1930-1975</em>. London: Verso.</li>
<li id="kie89"> Kiernan, B. (1989). <a href="http://www.webcitation.org/6uQc1Oskj">The American bombardment of Kampuchea, 1969-1973</a>. <em>Vietnam Generation 1</em>(1), 4-41. Retrieved from http://digitalcommons.lasalle.edu/vietnamgeneration/vol1/iss1/3/ (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQc1Oskj)</li>
<li id="kie96"> Kiernan, B. (1996). <em>The Pol Pot regime: race, power and genocide in Cambodia under the Khmer Rouge, 1975-79</em>. New Haven, CT: Yale University Press.</li>
<li id="kie08"> Kiernan, B. (2008). <em>Genocide and resistance in Southeast Asia: documentation, denial &amp;amp; justice in Cambodia &amp; East Timor</em>. New Brunswick, NJ: Transactions Publishers.</li>
<li id="led97"> Ledgerwood, J. (1997). The Cambodian Tuol Sleng Museum of Genocidal Crimes: national narrative. <em>Museum Anthropology, 21</em>(1), 82-98.</li>
<li id="mck94"> McKemmish, S. (1994). Are records ever actual? In S. McKemmish and M. Piggott (Eds.), <em>The records continuum: Ian Maclean and Australian Archives first fifty years</em> (pp. 187-203). Clayton, VIC: Ancora Press.</li>
<li id="mck17"> McKemmish, S. (2017). Recordkeeping in the continuum: an Australian tradition. In A.J. Gilliland, S. McKemmish &amp;amp; A.J. Lau (Eds.), <em>Research in the archival multiverse</em> (pp. 122-160), Clayton, VIC.: Monash University Publishing.</li>
<li id="mck10"> McKemmish, S., Upward, F. H. &amp;amp; Reed, B. (2010). Records continuum model. In M.J. Bates &amp;amp; M. Niles-Maack (Eds.), <em>Encyclopedia of Library and Information Sciences</em> (3rd ed.; pp. 4447-4459). New York, NY: Taylor &amp; Francis.</li>
<li id="rol17"> Rolan, G. (2017). Agency in the archive: a model for participatory recordkeeping. <em>Archival Science, 17</em>(3), 195-225.</li>
<li id="tay06"> Taylor, O. (2006, October 12). <a href="http://www.webcitation.org/6uQZwxKPN">Bombs over Cambodia</a>. <em>The Walrus</em>. Retrieved from https://thewalrus.ca/2006-10-history/ (Archived by WebCite&amp;reg; at http://www.webcitation.org/6uQZwxKPN)</li>
<li id="upw96"> Upward, F. (1996). Structuring the records continuum Part 1: Post custodial principles and properties. <em>Archives and Manuscripts, 24</em>(2), 268.</li>
<li id="upw97"> Upward, F. (1997). Structuring the records continuum Part 2: Structuration theory and recordkeeping. <em>Archives and Manuscripts, 25</em>(1), 10-35.</li>
<li id="upw00"> Upward, F. (2000). Modelling the continuum as paradigm shift in recordkeeping and archiving processes, and beyond: a personal reflection. <em>Records Management Journal, 10</em>(3), 115-139.</li>
<li id="upw05a"> Upward, F. (2005a). Continuum mechanics and memory banks. Part 1, Multi-polarity. <em>Archives and Manuscripts, 33</em>(1), 84-109.</li>
<li id="upw05b"> Upward, F. (2005b). Continuum mechanics and memory banks. Part 2, The making of culture. <em>Archives and Manuscripts, 33</em>(2), 18-51.</li>
<li id="vic86"> Vickery, M. (1986). <em>Kampuchea: politics, economics and society</em>. London: Pinter Publishers.</li>
<li id="vic99"> Vickery, M. (1999). <em>Cambodia 1975-1982</em>. Chiang Mai, Thailand: Silkworm Books. </li>
</ul>

</section>

</article>