#### Vol. 12 No. 2, January 2007

* * *

# Information seeking in organizations: epistemic contexts and contests

#### [Chun Wei Choo](mailto:cw.choo@utoronto.ca)  
Faculty of Information Studies,
University of Toronto,
140 St. George St. Toronto,
Ontario, Canada, M5S 3G6

#### Abstract

> **Introduction.** Organizations seek and use information to understand and enact their worlds. Information constitutes what the organization 'knows' about its environment and its tasks, and thus creates a basis for action. However, the link between information, knowledge, and action is problematic and not well understood.  
> **Method.** We consider three case studies that highlight different aspects of the interaction between information and knowledge in organizations. The first case compares information seeking and use in the due diligence processes of a government agency and a venture capital firm. The second case surfaces the epistemic properties of information seeking in the preparation of country studies by the staff of a German bank. The third case recollects the experience of large-scale information sharing among copier technicians of a multinational office products company.  
> **Analysis.** These case studies suggest that organizations behave as epistemic communities with distinct epistemic and information cultures that influence how information is sought and utilized, and how beliefs are formed and re-formed.  
> **Conclusions.** To understand information behaviour in organizations is to understand how organizations are simultaneously information-seeking and belief-forming social systems, where information is shaped by epistemic practices as much as beliefs are the outcomes of information seeking and use.

## Introduction

Information seeking and use has always been an intrinsic and important component of the theorizing in organization science about decision making, innovation, organizational sense-making, and knowledge creation. In general terms, we may say that organizations seek and use information in order to understand and enact their worlds ([Choo 2006](#cho06)). Information constitutes what the organization 'knows' about its environment and its tasks, and thus creates a basis for action. However, the link between information, knowledge, and agency is problematic and not well understood.

In this paper we introduce three case studies that highlight different aspects of the interaction between information and knowledge in organizations. The first case compares information seeking and use in the due diligence processes of a government department and a venture capital firm. The second case reveals the epistemic properties of information seeking in the preparation of country studies by the staff of a German bank. The third case recollects the experience of large-scale information sharing among copier technicians of a multinational office products company. Our analysis of these cases leads us to conclude that organizations behave as _epistemic communities_ in which _information-seeking_ and _belief-forming_ are closely interlaced. We discuss the epistemic styles and information cultures of organizations, and examine how epistemic contexts influence the seeking and use of information.

## Case Study 1\. Due diligence process in two organizations

Moldoveanu ([2002](#mol02)) compared due diligence processes in a US-based venture capital firm and a Canadian government agency. Both processes evaluated the application for funding from the same start-up firm, Highline Inc. Both processes ended in approval decisions, albeit for different reasons. Highline Inc. was a private Canadian high-technology start-up company designing chips and systems for fixed broadband wireless access. Highline Inc. submitted the same business model, marketing plan, financial projections and technology strategy map to both the government agency and the venture capital firm. The plan called for the development of a broadband wireless system that could offer Internet service providers and local telecommunication carriers with a _last-mile_ infrastructure that would outperform existing technologies. Using data from well-known market research firms, Highline Inc. projected revenues of \$10 million in the first 16 months of operation, rising to \$500 annually in the fifth year. Highline Inc. was hoping to raise \$15 million from venture capital firms and another \$7.5 million from the federal agency.

Managers of the venture capital firm and the federal agency had their own sets of beliefs and theories, which formed their working knowledge. They had proprietary information about the industry that Highline Inc. was targeting; knowledge of alternative business scenarios that would be compatible with the data from Highline Inc.; and intelligence about the sources on which Highline Inc. was basing its forecasts, but which may not be known to Highline Inc. In addition, both organizations relied on information about comparable deals that each had done in the past, as well as the outcomes of those deals.

The federal agency had codified its knowledge about the outcomes of past dealings with high-tech start-ups in the form of a set of guidelines and rules of thumb that it used to evaluate the claims made by Highline Inc. managers. These guidelines were held as irrefutable axioms during the due diligence process. The federal agency viewed the Highline Inc. business plan as data that either confirmed or disconfirmed its set of guidelines and heuristics (Figure 1). Thus, if there was a disparity between the company's business model or sales projections and the federal agency's guidelines, then the decision to reject a particular claim (or to ask for a modification of the model in order to fit the guidelines) was automatic. The main concern of the federal agency's managers seemed to be the _justification_ of the internally developed guidelines for regulating investment decisions:

> The government agency displayed a prototypically justificationist approach to the due diligence process. Its model or theory was the set of internally derived guidelines about what constitutes a viable opportunity. Only viable opportunities are to be approved. Once the opportunity passed an initial set of hurdles, the process of due diligence turned into one of finding reasons for investing in the firm from among the data supplied by the company. The credibility of the data seemed to be taken for granted, possibly because of the desire to fund the company once the initial guidelines were met. ([Moldoveanu 2002](#mol02): 419)

<figure>

![Figure 1](../p298fig1.jpg)

<figcaption>

**Figure 1: Government agency due diligence process (adapted from [Moldoveanu 2002](#mol02))**</figcaption>

</figure>

For the venture capital firm, the due diligence process started with the model derived from the business plan presented by Highline Inc., treating it as the theory to be tested. The inquiry process searched for reasons against accepting the firm's business model. Testable implications of the model were checked against market data as well as private information from potential customers, competitors, and other members of the venture fund. Since Highline Inc.'s managers were making claims about their technology and the market, their own credibility was evaluated as part of the process. Extensive reference checks were done using sources suggested by Highline Inc. managers as well as other sources. Negative references and discrepancies between the business model and data collected were not assumed to be conclusive. Instead, Highline Inc.'s managers were asked to respond to the discrepancies, and these comments and rebuttals were then used to revise the business model, which was submitted again for critical testing. Both the evidence generated by the venture firm's own process and the claims and propositions by the start-up were regarded as fallible statements that were subjected to further testing and criticism. Moldoveanu characterizes the venture firm's due diligence process as a _falsificationist_ approach that focuses on seeking evidence against a particular model, taking that evidence as fallible, and looking for more information to support, challenge or qualify new observation statements (Figure 2).

<figure>

![Figure 2](../p298fig2.jpg)

<figcaption>

**Figure 2: Venture capital firm due diligence process (adapted from [Moldoveanu 2002](#mol02))**</figcaption>

</figure>

Based on his comparative case study, [Moldoveanu (2002)](#mol02) identifies three epistemic styles:

*   A _justificationist_ organization holds its beliefs as being irrefutable and looks for evidence that supports its beliefs. It might ignore information that contradicts its premises, or it might strengthen its theories in order to account for the contrary evidence. A justificationist organization will rarely abandon its theory in favour of another.
*   A _dogmatist_ organization holds a belief for a reason (or a chain of reasons) that is held with absolute certainty, without the need for further justification. Thus the dogmatist might assert 'I just know it is so'.
*   A _falsificationist_ organization holds its beliefs provisionally, in the absence of reasons against holding them, while looking for arguments against those beliefs. Its attention is directed at finding evidence against a model, belief or theory. A model or belief is robust when it has survived repeated attempts at refutation rather than the degree to which it has been supported with different reasons for accepting it. Moreover, the falsificationist evaluates evidence as 'provisional' statements that may be challenged by information about other theories, or about the credibility of the sources involved.

## Organizational epistemic styles

The first case study raises the possibility that if organizations (or organizational processes) may be characterized by distinctive epistemic styles, then these approaches constitute an important aspect of the information seeking context of organizations. Thus, the epistemic style both shapes and is shaped by the information seeking and use practices that are part of the organization's belief-forming process. For a specific situation, an organization chooses among competing causal models through the belief selection strategy that it adopts. The epistemological style of the organization—be it dogmatist, justificationist or some other approach—may be revealed in the organization's information seeking and use practices, particularly its deliberation and advocacy activities. For example, an organization that actively encourages vigorous criticism of new proposals and theories, followed by a careful selection of the model that has best survived the strenuous examination, may be said to pursue a falsificationist approach in its belief selection strategy. In contrast, an organization that avoids open dialogue and adheres to familiar precedents may be displaying a dogmatist or justificationist style.

Justificationism is most closely associated with a rule-based view of organizations which sees knowledge as embedded in the rules and routines of organizations ([Cyert and March 1992](#cye92) and [Nelson and Winter 1982](#nel82)). Organizations with routines that cannot change even in the face of environmental forces that require change may be following a dogmatic or justificationist approach in belief selection where 'the way we do things around here' cannot be challenged. A dogmatic commitment to a set of beliefs may lead to the organization ignoring new information from the environment altogether. On the other hand, a falsificationist approach to belief corroboration is more likely to incorporate new information into its beliefs—using these signals to reject or reinforce current beliefs while at the same time considering their evidential authority.

## Case Study 2\. Country studies at a German bank

Knorr-Centina and Preda ([2001](#kno01)) examined the analysis of countries as 'epistemically rendered economic objects' in the Department of Eastern European Development of one of the largest banks in Germany. The department supported the bank's expansion of investments and subsidiaries in Eastern Europe by identifying new possibilities for further expansion. A prominent function of the department was conducting and providing studies of Eastern European 'countries' into which the bank had expanded after the fall of the Berlin Wall. These studies supported the investment decisions of the bank's management committee, and the department was seen as being of strategic importance, reporting directly to the CEO. These country studies were significant research projects: each study was about sixty to eighty pages and took months or longer to complete. A country was represented by a computer file and a cardboard file of papers. The files initially had a number of empty chapter slots and titles that were standard across all country studies. These included slots for sections on the banking laws of the country being studied, the interbank market, the economic competition in the country, economic projects conducted there, strategy and action plans and a slot for a profit and loss statement. Mapping the country, then, meant assembling information on these topics.

Knorr-Centina and Preda ([2001](#kno01)) highlighted a number of significant features of the epistemic approach used by the bank to construct these country studies (Table 1). One feature, _'specular epistemology,'_ refers to strategies of observation at a distance that were used to keep track of the state and development of economic objects (the countries, their markets, companies, and so on). Analysts and other observers continually watched particular segments of economic reality, but this observation was mostly accomplished from well beyond the boundaries of the observed object. The information sources used by the analysts included documents and statistics issued by the observed units; other government and transgovernmental agencies' analyses and reports (e.g., OECD reports); commercial information providers' public and intranet materials; the ideas they obtained when meeting someone or encountering something connected with the observed entity; and items gathered from the daily news.

<table><caption>

**Table 1: Epistemic features of information seeking in the construction of country reports**</caption>

<tbody>

<tr>

<td>

**"Specular" epistemology**</td>

<td>Observation is at a distance—from well beyond the boundaries of the observed object.  
A country becomes construed as a 'shared sight'—hence a spectacle.</td>

</tr>

<tr>

<td>

**Use of impure and heterogeneous data**</td>

<td>'One appears hospitable and receptive towards all sources of information, provided the source produces no outright lies'.</td>

</tr>

<tr>

<td>

**Perspectival validation**</td>

<td>The authority of recognition of the research results rests with the receiving agents who are higher ranking managers and sometimes clients of the firms.</td>

</tr>

</tbody>

</table>

The term 'specular epistemology' is intended to _'capture the observational optics of this approach through which the object becomes construed as a shared "sight" rendered transparent through a variety of tools'_ ([Knorr-Centina and Preda 2001](#kno01): 34). Thus, the epistemology is _specular_ in that observation is at a distance and a shared _sight_ of a country (hence a _spectacle_) is constructed from the use of information and tools. The authors note that this epistemic approach contrasts with the intervening epistemology of most natural sciences that manipulates materials in the laboratory, sometimes based upon protocols, in order to extract from laboratory _reactions_ specific, knowledge-generative effects:

> Objectivity, in these cases, is not based on the stand-offish attitude of the social sciences, which suggests that only a 'blind', non-interfering strategy that lets experimental subjects do as they would in a 'natural' environment leads to representationally valid results. ([Knorr-Centina and Preda 2001](#kno01): 34)

A second feature is that of _the use of _impure_ and heterogeneous data_. This also contrasts with the procedure of many natural sciences in which the laboratory itself is the main information source. For example, in molecular biology, nearly all data result from internally processed materials and great care is taken against contaminants and pollutants in practically all steps of laboratory work, including protecting against the unwanted interference of the entities (viruses, bacteria) they are studying. Similarly, in high energy physics, physicists battle continuously against an overwhelming background of unwanted signals which were the residue of last year's or even last decade's work. In contrast, the specular epistemology in the preparation of country studies shows few of those concerns. Knorr-Centina and Preda note that _'[o]ne appears hospitable and receptive towards all sources of information, provided the source produces no outright lies (there are sanctions against someone planting false information).'_ ([Knorr-Centina and Preda 2001](#kno01): 34)

Finally, there is what the authors labelled the phenomenon of _perspectival validation_. This refers to how the authority of recognition of the research results rests with the information end-users who are higher ranking managers and sometimes clients of the firms that produce the results. Moreover, several managers and management levels were routinely involved in the studies, introducing frames of reference and perspectives that differed from those of the researchers, resulting in various forms of interference with the progress of the research. Here is one example:

> During several briefings, the department head insisted that the profit and loss estimate for the planned subsidiary in country Y should be done after that done for country X, because 'they [the countries] resemble one another'. But country X was the place were the bank had undertaken the costly [building] restoration, and it was more than unlikely that the bank would spend another US$12 million in country Y on a similar project. Reconsidering, the department's head said the profit and loss estimate should be modeled rather after one done five years earlier for another subsidiary, because the countries 'resembled each other' too. Finally, she decided that the person in charge should produce a mix of the two, because the bank's activities would be in the future very similar to those in the other two countries. ([Knorr-Centina and Preda 2001](#kno01):40)

The second case study raises the question of how common are information practices in organizations characterized by distantiated observation, dubious data quality and privileged perspectives. More generally, the case underscores two important features of information behaviour in organizations: (1) organizations evolve their own culture of information seeking and use; (2) they accept a satisficing approach to truth-seeking, and an instrumental approach to information use.

## Culture of information seeking and use

Organizations develop their own culture of information seeking and use, establishing values and norms about, for example, how accurate the data should be, how much search is necessary, what kind of editing and manipulation is permissible and so on.

The use of distantiated observation ('specular epistemology') and the acceptance of 'impure' data in the case suggests a _satisficing_ approach ([Simon 1997](#sim97)) where search is satisfied with sufficiently good or acceptable results, without a requirement to maximize or optimize on search criteria such as accuracy or quality. Information use is _instrumental_ when the information is made to serve as the means to an end. This may be more likely to occur during perspectival validation, as in the country Y example above where managers influenced the assembling and selection of information. Knorr-Centina and Preda provide an interesting example of satisficing in information seeking and use when they described how the bank tinkered and made do with incomplete, inconsistent information in order to arrive at a report that was 'good enough':

> Moreover, there were clearly different understandings about what the 'interbank market' is. When a first report on that market was faxed in from the capital of country Y, it was called 'Three Interbank markets of [local currency name] in country Y' and had four parts, of about equal length. The first was 'the white market', then came 'the grey market', 'the black market' and 'outside markets of [local currency name]'. Each was given a percentage estimate of the total market, as well as a volume estimate. The working of each market was also described. When one of us noticed that the percentages added up to about 140 percent, a member of the department replied: 'We cannot change them, we have no better figures. We have to take this, they have no value, of course, but... We can write "gross estimate" on the top.' Nonetheless, changes were introduced several times during draft preparations. First, the description of the black market (which was not only statistical, but also procedural) was reduced, so that only one sentence acknowledging its existence remained in the final draft. The description of the grey market encountered the same treatment, although it survived for a longer period in various drafts. Contradictory statements were eliminated step by step. Having noticed that several descriptions coming from the representative office at the same time confirmed and denied the existence of secondary markets for securities, the department tried to tinker a coherent description by asking for additional reports and, when these were not forthcoming, they simply eliminated the contradictory passages. In addition, the final draft was edited so as to fit descriptions of interbank markets from previous studies. ([Knorr-Centina and Preda 2001](#kno01): 41-42)

## Case study 3\. The Eureka Project at Xerox

The work of service technicians is vital to the photocopier business of Xerox. Field service is an important point of contact between the firm and its customers, and a primary means of maintaining customer satisfaction and brand loyalty. In the early 1980s, with a shortage of technicians, Xerox decided to use less skilled and experienced service staff by moving towards _directive_ repair and adjustment procedures. Service instructions would be documented in the form of a decision tree, and technicians needed only be trained in using the documentation to be able to diagnose and repair machine failure. Xerox management did not believe that there was much value in what the technicians learned on their own in the field: it was more important to ensure that technicians followed the manual than to support them in creating new knowledge. ([Bobrow and Whalen 2002](#bob02))

Product engineers believed that all the important knowledge about the technical operation and failures of the copiers had been externalized and codified in the service manual. Diagnostic instructions in the manual were produced by inserting faults in the machines in a laboratory, and then recording the symptoms. Thus the laboratory was their information source. They had a mental model of the copier as an abstract machine whose operation could be described in engineering and statistical terms. Their role was to induce and investigate patterns of machine breakdowns in the laboratory, and codify repair actions in the manual.

Scientists at PARC had a strong background in artificial intelligence, and they initially built an expert system to support service repair work based on the documentation. When shown the system, the technicians were politely impressed but did not think it would be useful in the field ([Bobrow and Whalen 2002](#bob02)). (As discovered later, service technicians frequently encountered machine problems in the field that were not covered in the manual. They had to invent new solutions on the spot, through intuition, improvisation, trial-and-error.) Surprised by the response, PARC scientists decided to observe what service technicians actually did in the field by accompanying them on their service calls.

Their ethnographic study found that technicians carried two sets of books with them: an official repair manual that was usually in pristine condition; and a personal notebook that was worn and dog-eared. Technicians seemed to spend a lot of time talking to each other (in the café, near the parts depot, over their two-way radios). Initially, this behaviour raised a number of concerns. Were technicians ignoring or bypassing official manuals? Was too much time spent in small talk? A closer analysis revealed that technicians were writing down in their notebooks the solutions they had discovered to deal with hard problems that were not in the official manuals. The technicians took pride in their work, especially in solving problems that have stumped their peers. Whenever they got together they would tell stories of the problems they had run into where the answers were not documented. Unfortunately, while these war stories might get told to the ten people around the water cooler, they would not get shared among the 20,000 service technicians that Xerox had worldwide. What was needed was a way to share these stories and discoveries quickly, so that when someone found a solution, it would soon be available to the rest of the technicians.

To fill this need, Xerox scientists, together with the service technicians and engineers, developed a community-based mode of 'knowledge sharing', which they called Eureka ([Bobrow and Whalen 2002](#bob02)). Eureka worked as follows: technicians who had found new ways of solving difficult repair problems would submit their solutions to a pending tips database. Pending tips were vetted by validators, senior field technicians who were recognized as experts for those particular products. Technicians enjoyed the validation process as having an interesting conversation with a respected peer who shared a deep understanding of the machine in question. After validation, a tip entered formally into the 'knowledge base' where it would be viewed by everyone who subscribed to that product database. Names of authors and validators were displayed with each tip. When a technician tried to use a tip, he was asked to vote on whether it helped.

<figure>

![Figure 3](../p298fig3.jpg)

<figcaption>

**Figure 3\. Eureka Knowledge Sharing Process (adapted from [Choo 2006](#cho06))**</figcaption>

</figure>

Eureka was deployed in France in 1995, Canada in 1996, and the USA in 1998\. A recent report indicated that 15,000 customers service technicians are active users with more than 15,000 tips in the database ([Bobrow and Whalen 2002](#bob02)). The use of Eureka resulted in 10% reduction in service time and parts used, as well as fewer long or broken calls, increasing customer satisfaction. But perhaps the most important result of the Eureka project was the change in the organization's beliefs about the nature of field service work. The initial belief was that there was not much value in what technicians learned on their own in the field, and that all knowledge about repairs could be extracted in the laboratory and documented. Through a series of unconventional interventions, Eureka revised these beliefs to the new understanding that technicians were discovering useful knowledge in the field that was key to improving customer service, and that this new knowledge needed to be shared quickly and broadly.

## Epistemic contests

The Eureka experience suggests that organizations are arenas where epistemic styles and perspectives collide. For the _managers_, their epistemic style is based on introspection, vicarious learning, and personal beliefs. Moreover, there is a social expectation for managers to be confident, consistent, and to be able to reduce uncertainty so that timely action is possible. A consequence may be that managers are predisposed to a justificationist or dogmatist style that favours finding reasons for maintaining their beliefs over seeking contradictory evidence that requires them to question their assumptions. For the _engineers_, their epistemic style relies on measurement, experiments, testing, analysis and diagnosis—an empiricism that is part of their professional training. For service _technicians_, their epistemic stance is pragmatic, focusing on what works in the field, without necessarily knowing the reasons why something does or does not work. Their epistemology is based on trial-and-error, improvisation—a form of cognition-in-the-wild. For the computer and social _scientists_, their epistemic approach applies an open-minded inquiry that involved noticing and confronting breakdowns of their assumptions and beliefs, and designing new interventions that would allow them to re-perceive the world differently. Their approach is thus closer to falsificationism: holding beliefs provisionally, while looking for reasons that refute them.

<table><caption>

**Table 2: Contrasting epistemic contexts in the Eureka case**</caption>

<tbody>

<tr>

<th>Group</th>

<th>Epistemic context</th>

<th>Information seeking</th>

</tr>

<tr>

<td>Managers</td>

<td>Justificationist Dogmatist</td>

<td>Personal experience Hype Imitation  
</td>

</tr>

<tr>

<td>Engineers</td>

<td>Empiricist Codification</td>

<td>Lab as information source Testing Experiments  
</td>

</tr>

<tr>

<td>Technicians</td>

<td>Pragmatic  
Epistemology-in-the-wild</td>

<td>Peers  
Stories, notebooks  
Improvisation  
</td>

</tr>

<tr>

<td>Scientists</td>

<td>Falsificationist  
Open inquiry</td>

<td>Technicians  
Field observation  
Pilot projects  
</td>

</tr>

</tbody>

</table>

Given these divergent styles and perspectives, how are organizations able to learn collectively? How can they develop new understandings and initiate innovative action? The Eureka case suggests that there may be a role for epistemic change agents who are able to negotiate the boundaries that divide knowledge communities in an organization. The PARC scientists acted as epistemic boundary spanners who worked closely with individual technicians and managers to reveal the importance of information sharing in field service work. Eureka was jointly developed with the help of technicians and managers who acted as local champions, trainers, and executive sponsors. Through progressively larger scale projects, quantitative and qualitative evidence was collected to demonstrate the use and usefulness of the system. Eureka was rolled out in stages, starting in the more supportive environments in France and Canada, before its introduction in the USA. In recognition of the project's unexpected success, one scientist received a plaque captioned '_Despite the resistance of Worldwide Customer Service_', admitting the lack of support from the corporate service organization during the project.

## A tentative synthesis

### Organizations as epistemic communities

In epistemology, a prominent, traditional view is that propositional knowledge has three individually necessary and jointly sufficient conditions: justification, truth, and belief ([Audi 1998](#aud98)). The belief condition requires that a person who knows that a proposition is so, believes that proposition. The truth condition requires that a person knows that a proposition is so only if that proposition is in fact the case. The justification condition requires that a person must have adequate indication or evidence that a proposition is true: epistemic justification means 'evidential support' of a certain sort. Knowledge is not simply true belief—the justification condition disqualifies true beliefs that are formed by blind trust in a popular expert or that are supported only by lucky guesswork. An important branch of epistemology, naturalistic epistemology, views the human subject as a natural phenomenon and uses empirical science to study epistemic activity. Whereas many theories of justified belief focus on logical or probabilistic relations between evidence and hypothesis, naturalistic theories focus on the cognitive processes causally responsible for the belief. Goldman ([1999](#gol99)) argues that a belief can be justified only if it is generated by belief-forming (cognitive) processes that have the reliable capability to arrive at true or correct answers to questions of interest. Examples of belief-forming processes include reasoning processes, memory processes, and perceptual processes. Naturalistic epistemology is not confined to individual agents, it can also study communities of agents, when it must analyze the impact of social practices or social influences on the attainment of cognitive ends.

If organizations are defined by and act on their beliefs about identity and agency, and if these beliefs are based on some form of evidential support, then we may suggest that organizations are _epistemic communities_. Such communities would be characterized by their explicit or implicit methods of belief-forming and belief-testing. The term epistemic communities was proposed by Haas ([1992](#haa92)) in his work on how new policies were developed by government agencies (in the areas of monetary reform or environmental protection, for example). For Haas, an epistemic community would share (1) norms and values that provide a rationale for social action; (2) causal beliefs about policy actions and outcomes; (3) criteria for evaluating information; and (4) professional practices for problem solving.

If knowledge is justified true belief, and if a belief qualifies as justified when it is produced by reliable belief-forming processes, then we may ask: How reliable must belief-forming processes be? Although perfect reliability is not required, we should be mindful of the limitations and vulnerabilities of the belief-forming methods that are commonly observed in organizations. For example, many organizational processes seem to favour justificationist approaches. Justificationist approaches based on rule-following provide control and consistency, but they can also encumber new learning. Falsificationist approaches are thought to be more open and rigorous ([Moldoveanu 2002](#mol02)), but the process can be time-consuming and demands a relatively high level of discipline. We need more research that examines the belief-forming processes of organizations. What counts as evidence? What happens to signals and information that contradict existing beliefs? How do decision rules and premises act as organizational models and theories? What are the organizational defenses that obstruct open and critical self-examination?

### Organizations as cultures of information seeking and use

While there is substantial research on how the culture of an organization shapes the general behaviour of its participants ([Martin 2001](#mar01)), very little has been done to examine the impact of organizational culture on information behaviour. In our discussion here, we may refer to those aspects of an organization's values, norms, and practices that influence the seeking, evaluation and use of information as _information culture_ ([Choo 2006](#cho06)). Values are the deeply held beliefs about organizational identity and agency, including the role and significance of information and information work. Norms are based on values, but have a more direct impact on information behaviour. Norms are rules or socially accepted conventions about what is normal or expected behaviour in an organization. Thus, an organization may have norms or expectations about how much effort to put in seeking information, about the acceptability of using information for personal advantage, about the authority and authenticity of particular sources, and so on. Practices are repeated patterns of behaviour that affirm organizational roles, structures, and forms of interaction. Information practices are revealed in the activities by which people find, use and share information to do their work and sustain their identities.

The link between norms and information behaviour is made clear in the analysis of the social and cultural context of information seeking developed by Chatman. Drawing upon her studies of the information behaviour of the working poor, elderly women, prison inmates, and others, Chatman proposes a theory of Normative Behaviour:

> Normative behavior is that behaviour which is viewed by inhabitants of a social world as most appropriate for that particular context. Essentially driven by mores and norms, normative behavior provides a predictable, routine, and manageable approach to everyday reality. Aspects of interest are those things which serve to legitimize and justify values, which embody social existence." ([Chatman 2000](#cha00): 13)

Chatman's analysis may be extended to the social worlds of organizational actors. Chatman's theory is built on four concepts: social norms, worldview, social types, and information behaviour. Social norms create standards to judge what is right or wrong in social appearances. Norms give people a way to gauge what is normal in a specific context and at a specific time: they point the way to acceptable standards and codes of behaviour. Worldview is a collective perception by members of a social world regarding those things that are deemed important and unimportant. Worldview provides a collective approach to assess the importance of information. Social types are _'the absolute definitions given to members of a social world'_. They classify persons and in doing so _'members of a small world have sensible clues to the ways in which to behave, converse, and share information'._ ([Chatman 2000](#cha00): 12)

The empirical research of Marchand _et al._ ([2001](#mar01)) identifies six types of information behaviour and values that can predict an organization's capacity to use information effectively. These values are: Integrity (using information in a trustful and principled way); Transparency (openness and reporting of errors); Control (availability of information about performance); Sharing; Proactiveness (seeking and using new information to respond quickly to change); and Formality (use and trust of formal and informal sources). In their case study of a large law firm, Choo _et al._ ([2007](#choIP)) found that the values of sharing, proactiveness, transparency and integrity were significant factors in explaining information use outcomes.

More research is necessary to articulate the concept of information culture, and to observe how an organization's infomation culture can alter information behaviour. We would be interested to know if, for example, an information culture vested with values of vigilance, openness, and trust would encourage open inquiry and learning. Information culture probably needs to be considered at multiple levels. At the level of the organizaton, we might investigate how information norms and values are specified by the rules and routines that determine what information is needed, what sources to use, and what information to attend to. To what degree does the structuring of search and decision making activities in an organization sanction a particular set of information standards and expectations? At the level of the group and indvidual, information seeking and use are the learned outcomes of professional training, peer influence, and individual experience. To what degree do professional and group identities instill a view of the world that selects salience and significance in the information encountered? The research of Marchand and others is tantalizing in suggesting the possibility that a phenomenon as complex as information culture may be described parsimoniously by a small number of dimensions and values that have a major effect on information behaviour and outcomes.

### Organizations as arenas of contest, cooperation and change

Organizations are made up of disparate groups with contrasting epistemic styles and traditions. This is natural and necessary, since organizations exist as social arenas for groups to combine and coordinate their knowledge. At the same time, it is common for certain epistemic positions to become privileged by virtue of organizational power or authority, and for epistemic territories to become the locus of entrenched interests and beliefs. Given this diversity and the tendency for epistemic isolationism, how then can organizations learn—how can groups revise beliefs and develop shared understandings that enable collective action? How might boundaries that impede new learning be navigated? Our Eureka case study found that an alliance of scientists, technicians and managers was able to induce the epistemic change that led to new organizational beliefs about the value of front line work and the importance of information sharing. Is radical organizational learning stimulated by individuals or groups who act as epistemic change agents: agents who create the kind of information and insight that requires the organization to reflexively examine its methods of knowing? We know little about this aspect of information use in support of new learning. We do know that a number of communication roles are pivotal in the introduction and evaluation of new information: boundary spanners, devil's advocates, early adopters, gatekeepers, opinion leaders, and so on. Do these well-known communication roles also serve an epistemic function? What other epistemic roles might we observe in an organization? What are the implications for information services and information practice?

MacIntosh-Murray and Choo [(2005)](#mac05) discuss the role of an 'information/change agent' who supported the workplace learning of a group of nurses in a large tertiary care hospital in Canada. The change agent (the Practice Leader/Educator in the hospital) helped to reveal latent information needs; encouraged reflection about practical problems; and intervened to repair broken routines that might have led to adverse outcomes. The functions of the 'information/change agent' included:

*   acting as a boundary spanner bridging three types of gaps: (1) between the nurses' patient-level of focus and the system and process levels, (2) between the front-line and management, and (3) between the nurses on the unit and resources outside the unit;
*   recognizing patient safety issues and acting as an information seeker for front-line nurses, identifying their needs and seeking appropriate information;
*   using the information (research, policies, procedures, standards) as a knowledge translator to explain to the nurses how it applies to their practice; and
*   intervening actively as a change champion with just-in-time education, change initiatives, and ongoing coaching. ([Macintosh-Murray and Choo 2005](#mac05): 1343)

We have so far considered the roles of individuals or groups, but what might be said about the effects of the information itself? Are there for example, certain forms or types of information that can facilitate the sharing of meaning and knowledge between epistemic groups? Bowker and Star ([1999](#bow99)) describe _boundary objects_ as those information objects that both inhabit several communities of practice and satisfy the informational requirements of each of them. In working practice, they are objects that are able to travel across borders and maintain some sort of constant identity. This is achieved by allowing the objects to be weakly structured in common use, while imposing stronger structures in tailored use at individual sites. They are thus both ambiguous and constant; and they may be abstract or concrete. Boundary objects embody and represent essential knowledge that can be shared across domains and levels of expertise. For example, experts produce prototypes or sketches of products as a way of conveying their thoughts about how a product might work and how it should be designed. The prototype has extensive tacit knowledge embedded within it and can serve as a basis for communication, discussion, and elaboration. Through boundary objects, people can see for themselves the way that knowledge is represented and endowed with shared meanings. The Eureka system may be seen as a kind of boundary object that had different epistemic significance for its various constituencies (managers, engineers, technicians, scientists) while at the same time conveying a set of common, consistent messages which changed the organization's perceptions about the nature and value of field service work.

Information use in organizations is not innocent. Information is deployed in organizations to advance interests, to preserve the status quo, as well as to instigate open inquiry and new learning. If information seeking and use is the basis of constructing beliefs—beliefs that form the foundation of organizational choice and action—then we need to grasp more fully the epistemic dimensions of information behaviour as a precursor of organizational behaviour.

## Coda

Our discussion here raises more questions than answers. Assuming that organizations act on their beliefs, and that beliefs are based on evidential support, then research is needed so that we are better able to describe and discuss the methods by which organizations form and justify their beliefs. This paper draws a tentative sketch of how organizations act as epistemic communities with distinct epistemic and information cultures that influence how information is sought and used, and how beliefs are formed and re-formed. To understand information behaviour in organizations is to understand how organizations are simultaneously information-seeking and belief-forming social systems, where information is constructed through epistemic practices as much as beliefs are the outcomes of information seeking and use.

## References

*   <a id="aud98"></a>Audi, R. (1998). _Epistemology: a contemporary introduction to the theory of knowledge._ New York, NY: Routledge.
*   <a id="bob02"></a>Bobrow, D.G. & Whalen, J. (2002). Community knowledge sharing in practice: the eureka story. _Reflections, Journal of the Society for Organizational Learning_, **4**(2), 47-59.
*   <a id="bow99"></a>Bowker, G. & Star, S.L. (1999). _Sorting things out: classification and practice._ Cambridge, MA: MIT Press.
*   <a id="cha00"></a>Chatman, E.A. (2000). Framing social life in theory and research. _New Review of Information Behaviour Research_, **1**, 3-18.
*   <a id="cho06"></a>Choo, C.W. (2006). _The knowing organization: how organizations use information to construct meaning, create knowledge, and make decisions_ (2nd ed.). New York, NY: Oxford University Press.
*   <a id="choIP" name="choIP"></a>Choo, C.W., Furness, C., Paquette, S., van den Berg, H., Detlor, B., Bergeron, P., _et al._ (2007). Working with information: information management and culture in a professional services organization. _Journal of Information Science_ **32**(6), 3-22.
*   <a id="cye92"></a>Cyert, R.M. & March, J.G. (1992). _A behavioral theory of the firm_ (2nd ed.). Oxford: Blackwell.
*   <a id="gol99"></a>Goldman, A.I. (1999). _Knowledge in a social world_. Oxford: Oxford University Press.
*   <a id="haa92"></a>Haas, P.M. (1992). Banning chlorofluorocarbons: epistemic community efforts to protect stratospheric ozone. In P.M. Haas (Ed.), _Knowledge, power, and international policy oordination_ (pp. 187-224). Columbia, South Carolina: University of South Carolina Press.
*   <a id="kno01"></a>Knorr-Cetina, K. & Preda, A. (2001). The creation and incorporation of knowledge in economic activities: epistemization of economic transactions. _Current Sociology_, **49**(4), 27-44.
*   <a id="mac05"></a>MacIntosh-Murray, A. & Choo, C.W. (2005). Information behaviour in the context of improving patient safety. _Journal of the American Society of Information Science and Technology_, **56**(12), 1332-1345.
*   <a id="mar01"></a>Marchand, D., Kettinger, W. & Rollins, J. (2001). _Information orientation: the link to business performance_. New York, NY: Oxford University Press.
*   <a id="mart01"></a>Martin, J. 2001\. _Organizational culture: mapping the terrain_. Thousand Oaks, CA: Sage Publications.
*   <a id="mol02"></a>Moldoveanu, M. (2002). Epistemology in action: a framework for understanding organizational due diligence processes. In C.W. Choo & N. Bontis (Eds.), _The strategic management of intellectual capital and organizational knowledge._ (pp. 403-420). New York, NY: Oxford University Press.
*   <a id="nel82"></a>Nelson, R. & Winter, S. (1982). _An evolutionary theory of economic change_. Cambridge, MA: Belknap Press.
*   <a id="sim97"></a>Simon, H.A. (1997). _Administrative behavior: a study of decision-making processes in administrative organizations_. 4th ed. New York, NY: The Free Press.