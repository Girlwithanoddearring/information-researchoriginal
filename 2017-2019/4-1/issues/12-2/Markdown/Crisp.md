# Individual Differences and Internet Shopping Attitudes and Intentions

#### [C. Brad Crisp](mailto:brad.crisp@acu.edu)  
Abilene Christian University, Department of Management Sciences, ACU Box 29353, MBB 254, Abilene, TX 79699-9353 Telephone: 325-674-2077 Fax: 325-674-2507

#### [Sirkka L. Jarvenpaa*](mailto:sjarvenpaa@mail.utexas.edu)  
University of Texas at Austin, Center for Business, Technology and Law, CBA 6.480, Austin, TX 78712 Telephone: 512-471-1751 Fax: 512-471-0587

#### [Peter A. Todd](mailto:peter.todd@mcgill.ca)  
McGill University, Desautels Faculty of Management, Bronfman Building, 1001 Sherbrooke Street West, Montreal, Quebec H3A 1G5 Telephone: 514-398-4001 Fax.: 514-398-3876

#### Abstract

> The study explored the effect of various individual difference factors on the consumers' beliefs, attitudes, and intentions toward Internet shopping using a sample of 220 consumers. The sample was drawn from affluent sections of a southwestern metropolitan area and from the university community. The study involved an experiential survey; all participants were brought to a university computer laboratory where they browsed through electronic malls on the worldwide web and completed survey instruments comprised of items obtained from the literature. The study results indicate that attitudes towards computers affect attitudes towards Internet shopping. Prior Internet experience, attitudes towards computers, age, household size, and frequency of shopping via direct marketing channels affect intentions towards Internet shopping. Depending on their prior experience with the Internet, the consumers placed somewhat different emphasis on various beliefs about Internet shopping. Overall, however, the retail patronage model that we had developed in an earlier paper seem to hold for both inexperienced Web users as well as those with Web experience.

## Introduction

The Internet's World Wide Web (or Web) provides the promise of a new direct interactive shopping channel that is not bounded by time nor geography. Online store fronts are being constructed to market goods and services over the Internet. These store fronts can be established at low cost, and the product and service offerings can be updated on demand. Consumers around the world can have nearly instantaneous, 24 hour a day access to these store fronts using Web browsers. They can search for information about products and services, place an order for a product, receive follow-up service and in some cases, such as for information based products, actually take delivery over the Web. Individual retail store fronts can be grouped to form clusters of electronic malls.

Web retailing is an innovation that many predict will experience phenomenal growth, with total sales ranging from $2 to $20 billion by Year 2000 (Hoffman and Novak, 1996; Ives, 1997). Given these expectations, many businesses feel compelled to develop a Web presence (Champy et al, 1996; Economist, 1997). Others, however, are less enthusiastic about the commercial potential of the Web, pointing out the many challenges that the new retail form faces in terms of user acceptance (Alba et al, 1996).

Past forecasts on many other home information technology and services targeted at supporting retailing have suffered from over-optimism, technology determinism, and faulty assumptions of potential user communities (Klopfenstein, 1989). Thus, while it is clear that many see significant potential in the Web as a sales and marketing tool, much more needs to be understood about what drives commercial activity on the Web. In particular, "the need to understand Internet consumers is vital for both existing businesses and those who are considering a shop in cyberspace" (Fram and Grady, 1995, p. 63).

Despite the need to understand what drives Internet consumers, much of the empirical research of the Web as a retail channel has been descriptive, focusing on estimating the current Internet and Web user base, understanding what the users are buying, and how much they are spending (e.g., Georgia Institute of Technology, 1995; Gupta and Chatterjee, 1997). Emerging from this is a somewhat homogeneous picture of Web users and their buying behavior. For example, Hoffman, Kalsbeek and Novak (1996) found that those who actually buy on the Web use the Internet at least once a week and are frequent Web users. Further, retailers are encouraged to target stereotypical Web users: "college educated white males in their early thirties, earning higher than average incomes and employed in the computer, education, and other professional fields" (Quelch and Klein, 1996; p. 61). Many merchants seem to have taken this stereotype to heart and target these high-tech professionals as evidenced, for example, by the fact that the bulk of advertising and retail offerings on the Internet are aimed at a high-tech user (Information Week, August 5, 1996).

However, the attention focused on this high-tech male user tends to be somewhat limiting and ignores other potentially important user segments. First, the Web population doubles every six months suggesting that, at a given point in time, nearly half of the users are inexperienced (Borenstein, 1996). Second, recent reports report a steadily growing female Internet population. It was noted that only 21% of the Web user base was female in 1995 (Fram and Grady 1995), 27% was estimated to be female in 1996 (Hoffman, Kalsbeek and Novak 1996), and that by 1997 41% of the Web population are women (Business Week, May 1997). Clearly women represent an important and growing segment of the Web user base. They are disproportionately more important in terms of potential retail activity. Women do the majority of shopping for most households and are the primary buying decision maker in a household (Dholakia et al, 1995).

In addition to a changing gender mix, the web user base is evolving in other ways that require the attention of marketers. It is attracting more mature individuals with 45% of the Web population being 40 or over. The Web population is more affluent and better educated than the population at large. More than 42% of the households who use the Web have incomes greater than $50,000 (versus 33% of the population at large), and 73% of the Web users have attended college (vs. 46% of the total population) (<u>Business Week</u>, 1997). Thus, in many ways, the Web user base has transformed itself significantly from the stereotypical view of the 18-24 year old male computer nerd that dominated only a few years ago. The increased diversity of Web users suggests different market segments which may respond to commercial opportunities on the Web in various ways. Thus, it is important to explore how individual differences between users may influence consumer attitudes, intention and acceptance of Web-based retailing.

To begin this process, this paper reports on a study which explored the effect of individual differences on the Internet shopping beliefs, attitudes, and intentions using a sample of 220 individuals, of whom 184 were female. The average participant was 43 years of age. The results suggest that attitude towards shopping on the Web was significantly influenced by attitude towards computers. Intention to shop on the Net was significantly influenced by prior Web experience, attitude towards computers, frequency of shopping in direct marketing channels, household size, and age, explaining 22% of the variability in shopping intention. Prior Web experience also seemed to affect the salience of some of the beliefs related to Internet shopping beliefs, although the Internet retail patronage model developed in an earlier paper seemed to hold well for both inexperienced and experienced users.

The next section reviews the theory of technology acceptance and the role of individual differences in the theory. We will advance hypotheses that integrate empirical findings from information systems and marketing on individual differences. The third section describes the methodology, and the fourth section reports on the results. The last two sections discuss the results and conclude the paper with recommendations for practice and theory.

## Conceptual Background

The current study will explore how individual difference factors influence beliefs, attitude and intention toward Internet shopping. Individual differences have been theorized and found to be associated with the acceptance of new information technology (Zmud, 1979; Nelson, 1990) and new forms of retail marketing and shopping (Akaah, Korgaonkar, and Lund, 1995; Gehrt and Carter, 1992). We will place the individual difference factors in the context of attitude behavior models such as the Theory of Reasoned Action (TRA) (Fishbein and Ajzen, 1975; Ajzen and Fishbein 1980) and the Technology Acceptance Model (TAM) (Davis, 1989; Davis et al, 1989). To do this we employ an integrated model of Internet shopping behavior developed in Jarvenpaa and Todd (1997).

The model, shown in Figure 1, suggests that four factors _product value, shopping experience, service quality_ and _risk_ will, together, influence attitude and intention towards shopping on the Internet. These factors were derived from the literature on retail patronage and catalog shopping and then empirically examined. Each of the factors has a set of more detailed antecedent beliefs which together form an integrated Internet patronage model. The detailed antecedent belief factors are defined in Table 1 (see Jarvenpaa and Todd, 1997). The model shows how the beliefs related to value, experience, service and risk combine to influence attitude and intention towards Internet shopping. This set of relationships is consistent with the TRA which suggests that beliefs about the consequences of the behavior are key to the formulation of attitude towards the behavior (Fishbein and Ajzen 1975; Ajzen and Fishbein 1980).

The model was useful in helping to understand the determinants of attitude and intention towards Internet shopping. Specifically, attitude, product value and service quality all had significant influences on intention to shop on the Internet. Product value, shopping experience and perceived risk all had significant effects on attitude. The model explained approximately 50% of the variance in attitude and intention towards Internet shopping. Further the detailed belief factors accounted for a significant portion of the variance in the four retail patronage factors. Perceived product value was significantly influenced by price, quality and variety. The shopping experience was significantly influenced by effort, playfulness and compatibility but not sociability. Service quality was significantly influenced by responsiveness, tangibility and reliability but not by empathy. Finally, perceived overall risk was significantly influenced by performance, personal, privacy and economic risks but not by social risks. Thus, overall the model appears to provide significant insight into attitude and intention towards shopping on the Internet.

To explore the relationships in the model further, this paper examines how individual difference factors might moderate the relationships found and act as determinants to the basic beliefs. Hence, this paper extends our earlier work on online retail patronage model to consider individual differences. In particular we examine how prior web experience, general attitudes towards computers and shopping, as well as demographic factors such as age, income, gender, education and household size affect beliefs, attitudes and intentions towards shopping on the Internet. The next section will present detailed hypotheses for the relationship of these individual difference factors to beliefs, attitude and intention.

<table><caption>

**Table 1: Definitions of Shopping Factors**</caption>

<tbody>

<tr>

<th>Shopping Factor</th>

<th>Each factor defines the degree to which consumers perceive that...</th>

</tr>

<tr>

<th colspan="2">Product Perceptions</th>

</tr>

<tr>

<td>price</td>

<td>...the Web provides competitively priced merchandise and attractive promotions and deals.</td>

</tr>

<tr>

<td>quality</td>

<td>...the Web is a source of high quality goods and services, which meet consumer expectations.</td>

</tr>

<tr>

<td>variety</td>

<td>...the Web provides a wide range of goods and services including those that consumers are not able to get elsewhere.</td>

</tr>

<tr>

<th colspan="2">Shopping Experience</th>

</tr>

<tr>

<td>compatibility</td>

<td>...the Web fits consumer lifestyles and the way they like to shop.</td>

</tr>

<tr>

<td>effort</td>

<td>...the Web saves time and makes shopping easy.</td>

</tr>

<tr>

<td>playfulness</td>

<td>...shopping on the Web allows the consumer to have fun.</td>

</tr>

<tr>

<td>social</td>

<td>...shopping on the Web allows the consumer to interact with people.</td>

</tr>

<tr>

<td colspan="2">

**Customer Service**</td>

</tr>

<tr>

<td>responsiveness</td>

<td>... merchants provide the necessary information in a form that allows the consumer to conduct pre-purchase search, make a selection, place an order, make a payment, take delivery, and receive support after the sale.</td>

</tr>

<tr>

<td>reliability</td>

<td>... merchants can be counted on to deliver on their promises.</td>

</tr>

<tr>

<td>tangibility</td>

<td>... goods and services are displayed in a visually appealing way</td>

</tr>

<tr>

<td>empathy</td>

<td>... merchants understand and accommodate their individualized needs, such as providing universal access to services, linguistic or currency translation, audio rather than text-based interaction.</td>

</tr>

<tr>

<td>assurance</td>

<td>... merchants provide information to reduce the uncertainty experienced by the consumer about the reputation of the merchant and the quality of products and services.</td>

</tr>

<tr>

<td colspan="2">

**Consumer Risks**</td>

</tr>

<tr>

<td>economic risk</td>

<td>...using the Web to shop will lead to monetary losses through poor purchase decisions.</td>

</tr>

<tr>

<td>social risk</td>

<td>... shopping on the Web will be perceived as imprudent or socially unacceptable</td>

</tr>

<tr>

<td>performance risk</td>

<td>...goods and services bought on the Web will not meet consumers' expectations</td>

</tr>

<tr>

<td>personal risk</td>

<td>...the process of shopping will result in harmful personal consequences to the consumer.</td>

</tr>

<tr>

<td>privacy risk</td>

<td>...the process of shopping on the Web puts the consumers' privacy in jeopardy</td>

</tr>

</tbody>

</table>

## Hypotheses: The Effect of Individual Differences

Attitude-behavior models such as the Theory of Reasoned Action and the Technology Acceptance Model, upon which the Integrated model of Internet Shopping is based, typically assert that the influence of external factors, such as individual differences, will be mediated through the belief, attitude and intention components of the model. Among the factors that have been considered in this regard are prior experience with related behaviors, attitude towards objects related to the action and demographic variables such as age, income, education, and gender. We explore each of these groups of factors in turn.

### Prior Web Experience

One of the key sources of information used to shape beliefs is the person's own direct experience with a similar situation. The literature on TAM specifically and in the information systems literature in general suggests that an individual's prior computer usage impacts their beliefs about related systems and technology (Venkatesh and Davis, 1996; DeLone, 1988; Fuerst and Cheney, 1986; Igbaria et al, 1989; Lee, 1986; Igbaria et al, 1995). Taylor and Todd (1996) found the TAM model to predict intention and behavior for both experienced and inexperienced users although there were differences in the relative influence of the belief factors on intention and usage. Taylor and Todd in particular found that the relationship between intention and usage was stronger for the experienced than for inexperienced people, suggesting the users employ the knowledge accumulated from past experiences to form their intentions. Similarly, Thompson et al (1994) found that prior experience with computers not only had a direct effect on beliefs, attitude and intention, but also that the person's level of experience with the particular technology moderated the strength of the relationships between beliefs, attitude, and intention (Thompson et al, 1994). These findings on TAM are consistent with the arguments of the Theory of Reasoned Action (Ajzen and Fishbein, 1980; Fishbein and Ajzen, 1975; Triandis, 1979). Past experience generates objective consequences from engaging in the behavior. This knowledge will help to reinforce behavior and shape and moderate beliefs, future intention and behavior (Fishbein and Ajzen, 1975).

In the retail literature, consumers' knowledge as well as past experience has also been found to influence their attitude and intention (Smith and Swinyard, 1983; Akaah et al, 1995; Korgaonkar, Lund, and Price, 1985). For example, in the early days of "computing in the home," the purchase of home computers was found to be a function of a person's prior computing experience (Venkatesh and Vitalari, 1987). Moreover, past consumer research has shown that the more positive the person's past experience, the more positive their attitude and more likely they are to engage in the behavior again (Bagozzi and Warshaw, 1990). For example, past favorable experiences with catalog sales have been shown to increase a person's likelihood of shopping via catalog again (Shim, 1992). The adoption of videotext based shopping has similarly been found to be a function of prior experience with mail-order and catalog shopping (Eastlick, 1993). Hence, the first hypothesis is that:

_**H1: Prior Web experience will positively affect as well as moderate beliefs, attitudes and intentions towards Internet shopping.**_

### Computer Attitudes

Prior studies on videotext based shopping have found the predisposition toward computers and high-tech equipment in general to be a significant determinant of adoption and use of videotext shopping (Korgaonkar and Moschis, 1987; Korgaonkar and Smith, 1986; Shim and Drake, 1990; Shim and Mahoney, 1991). Although most of the past studies have found a consumers' computer orientation to influence attitudes toward videotext in a positive manner, Eastlick (1993) found a more ambiguous relationship between consumers' computer orientation and their attitudes toward videotext shopping. Her findings might suggest that those with favorable attitudes towards computers have higher expectations of electronic based shopping leading them to be both critical and positive at the same time. Research on new technology adoption has consistently found the attitudes towards computers to be a significant determinant of adoption and use although some have found the relationship to weaken as users gain experience with the technology (Thompson et al, 1994). Hence, we hypothesize that:

_**H2: Attitudes towards computers will affect beliefs, attitude and intention toward Internet shopping, especially for less experienced Web users.**_

### Shopping Orientation and Direct Shopping Frequency

Shopping orientation refers to the degree to which an individual sees themselves as a shopper and takes pleasure or personal satisfaction from the act of shopping. Shopping orientation has been found to be among the most influential predictors of consumer patronage behavior (Darden and Howell, 1987). For some customers, shopping is a pleasurable activity and an important part of the person's life. However, others do not enjoy shopping. In the current study, we associate shopping orientation with shopping enjoyment which is the most important dimension of the shopping orientation (Bellenger and Korgaonkar, 1980; Carlson 1978).

Some past research suggests that those who generally do not have positive shopping orientation will have more positive attitudes towards direct shopping modes, such as catalog shopping. Direct shopping is associated with reduction in time spent on shopping, saving physical effort, flexibility in the timing of shopping, and reduced levels of aggravation (e.g., Jasper and Lan, 1992; Darian, 1987). By contrast, others have found the opposite: those who shop via catalog also have a high propensity to shop in general (Shim, 1992).

The above contradictory results were partly explained by Gehrt and Carter (1992) who suggested that while at times, shoppers seek for efficiency in catalog shopping; at other times shoppers seek recreational or enjoyment value. In a study of French catalog patrons, the recreational orientation actually dominated perhaps because of the newness of the innovation (Gehrt et al, 1992). Given that the Web is a new form of direct marketing, we expect that those with high levels of shopping orientation (i.e., shopping enjoyment) also exhibit higher levels of attitudes and intention to shop on the Internet.

_**H3: Those with more favorable attitudes towards shopping in general will have more favorable attitude and intention towards Internet shopping.**_

We also expect those who currently use direct marketing channels to have more positive attitude and intention towards Internet shopping than those who do not utilize direct marketing channels such as catalogs. In many respects, the Web is much like the other direct marketing channels, with most merchants' web sites being nothing more than electronic versions of their catalogs (Alba et al 1996). Merchants rarely take advantage of the Web technologies' interactive and multimedia features and even more rarely customize their interfaces to a particular customer. Moreover, as is the case with catalogs, electronic storefronts make it difficult for consumers to examine non-information based products before purchase. Further, because of its novelty, the electronic shopping channel comprises many merchants with whom the shopper has had no prior experience. Hence, we expect that:

_**H4: Those who more frequently use other direct shopping modes will have more favorable attitude and intention toward Internet shopping.**_  

### Demographics: Income, age, education, gender, and household size

Past literature suggests that the new modes of direct retailing are patronized more by high income, educated, younger shoppers than by low income, less educated, and older shoppers (Cunningham and Cunningham, 1973; Reynolds, 1974; Feldman and Star, 1968; Berkowitz et al, 1979; Gillette, 1976; Peterson et al, 1989; Fields and Greco, 1988; Shim and Drake, 1990; Korgaonkar and Smith, 1986; Eastlick, 1993). One explanation for these effects is that this particular profile of shopper places a higher value on convenience, is less price conscious, and is more tolerant of the risk associated with new retail innovations (Reynolds, 1974; Peterson et al, 1989). These consumers also are found to seek a more unique merchandise assortment that the new direct retailing forms often provide (Reynolds, 1974; Gehrt and Carter, 1992). These results apply to catalog, mail-order, and videotext shopping. With time, demographic effects, except for income, reduce as the novelty of the new mode wears off (Kilsheimer and Goldsmith, 1991; Shim, 1992; Akaah et al, 1995).

The effect of age has been found to be transitional in retail patronage studies. For example, as catalog shopping has become more commonplace, the older shoppers have been found to be as likely and sometimes even more likely to shop using catalogs than their younger counterparts (Lumpkin, Greenberg, and Goldstucker, 1985; Jasper and Lan, 1992). However, with <u>new</u> retail innovations, age has been found to be significant. Similarly, the technology innovation literature suggests an inverse relationship between age and acceptance of new technologies (e.g., Harrison and Rainer, 1992; Nickell and Pinto, 1986; Gattiker, 1992). Older individuals are more conditioned to their customary way of behaving (i.e., have routinized more of their behavior). This might suggest that older individuals have to exert more cognitive and emotional effort to learn new behaviors and disassociate themselves from their daily routines. For example, Zeithaml and Gilly (1987) found that those elderly people who did not adapt to using bank ATM's emphasized their old way of doing and chatting with tellers. Also, those with less education have been found to exhibit more computer anxiety and more negative attitudes towards computers (Igbaria and Parasuraman, 1989).

Relatively little is known about the effect of gender on direct marketing patronage behavior because most studies are dominated by female shoppers reflecting the fact that women are still the primary shoppers as well as the primary purchasing decision makers (Rodkin, 1991). Some studies that have involved both genders have found no gender difference on patronage behavior (Kilsheimer and Goldsmith, 1991), whereas others have found females to be more frequent users of catalogs than males, particularly if the individual is over 40 years of age (Lumpkin and Hawes, 1985; Darian, 1987). Males and females have also been found to differ in terms of their beliefs and motives affecting purchase behavior. Females are more motivated by convenience, time savings, and effort, whereas males are motivated by the merchandise assortment and service (Eastlick and Feinberg, 1994). For example, a study by Dholakia et al (1995) found that upscale males acknowledged the convenience of the catalog and computer-based shopping modes but preferred in-store shopping because they perceived the alternative forms as too expensive and as less fun and rewarding. The literature on new information technology innovations shows little evidence of significant differences between the sexes in the use of technology and attitudes towards technology-mediated activities (Gattiker, 1994) although some studies have found women to possess less positive attitudes toward technology than men (Rafaeli, 1986). Also, early work on women and information technology found women to be less optimistic about the benefits of computer technology than men (Gutek and Bikson, 1985). Hence, we hypothesize that:

_**H5: Higher income, younger, more educated, and single male shoppers will have more positive attitude and intention toward Internet shopping.**_

## Methodology

Two hundred twenty household shoppers participated in the experiential survey study where they spent 1.5 hours in a university computer laboratory engaged in a variety of World Wide Web shopping activities and completed several questionnaires on individual differences factors, beliefs, attitudes and intentions toward shopping on the Internet. Each participant was paid $20\. The computer lab was equipped with Pentium workstations, 17 inch monitors, and high speed Ethernet access to the university backbone network. Since even among World Wide Web users electronic shopping activity is relatively low, this approach ensured that respondents had a minimum level of exposure to shopping on the World Wide Web, so that they could provide informed input.

### Sampling Strategy.

The sample was targeted to match the demographics of the Web population along dimensions such as income, age, and education level; while targeting primary household shoppers, who are predominantly female (Blaylock and Smallwood, 1987; Dholakia et al, 1995). This was thought to be important since it has been argued that one of the key challenges of Web-based shopping is the male dominated user base (Burstein and Kline 1995).

Participants were solicited through two sources. A mail solicitation asking the primary shopper to participate in the study was sent to a random sample of 1500 local area households which met the demographic characteristics of World Wide Web users reported by O'Reilly and Associates (1995) (i.e., median household income between $50,000 and $75,000, professional occupations, and median age between 35 and 55). To ensure a reasonable proportion of World Wide Web users in the sample, 500 additional solicitations were sent to e-mail users among the non-academic staff at a Southwestern university. The mailing generated over 300 responses from interested participants (a 15% response rate). Due to budget constraints we invited and scheduled 250 participants. Over the course of the sessions 30 people canceled yielding a final sample of 220, which includes 70 university employees.

The demographic profile of the participants is summarized in Table 2\. Aside from gender, which was deliberately skewed, the sample closely matches the demographic profile of Internet users reported by O'Reilly and Associates (1995). The sample's World Wide Web experience is summarized in Table 3\. Only 18 participants (8%) reported previously shopping through the Internet. This is consistent with estimates of the proportion of Internet users who had made purchases at the time of the study (Advertising Age, 1995).

<table><caption>

**Table 2: Demographic Profile of Study Participants**</caption>

<tbody>

<tr>

<td>

**Sex**</td>

<td colspan="2">184 female; 36 male</td>

</tr>

<tr>

<td>

**Average age**</td>

<td colspan="2">43 years(range 18 to 72)</td>

</tr>

<tr>

<td>

**Average employment experience**</td>

<td colspan="2">14 years (range 0.5 to 46)</td>

</tr>

<tr>

<td>

**Household income**</td>

<td>less than$24,999</td>

<td>11.4%</td>

</tr>

<tr>

<td></td>

<td>$ 25,000 - $ 49,999</td>

<td>23.7%</td>

</tr>

<tr>

<td></td>

<td>$ 50,000 - $ 74,999</td>

<td>19.2%</td>

</tr>

<tr>

<td></td>

<td>$ 75,000 - $ 99,999</td>

<td>19.2%</td>

</tr>

<tr>

<td></td>

<td>$100,000 -$124,999</td>

<td>11.9%</td>

</tr>

<tr>

<td></td>

<td>more than $125,000</td>

<td>14.6%</td>

</tr>

<tr>

<td>

**# people in household**</td>

<td colspan="2">3(range 1 to 8)</td>

</tr>

<tr>

<td>

**Educational experience**</td>

<td>high school</td>

<td>4.1%</td>

</tr>

<tr>

<td></td>

<td>2 or less years of college</td>

<td>17.4%</td>

</tr>

<tr>

<td></td>

<td>4 year college degree</td>

<td>48.9%</td>

</tr>

<tr>

<td></td>

<td>graduate degree</td>

<td>29.7%</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

</tr>

</tbody>

</table>

<table><caption>

**Table 3: Degree of Internet Experience Among Study Participants**</caption>

<tbody>

<tr>

<td>

**% with Internet Access**</td>

<td> </td>

<td>69%</td>

</tr>

<tr>

<td>

**% Accessing Internet from...**</td>

<td>home</td>

<td>47%</td>

</tr>

<tr>

<td></td>

<td>office</td>

<td>42%</td>

</tr>

<tr>

<td></td>

<td>other</td>

<td>8%</td>

</tr>

<tr>

<td></td>

<td colspan="2"></td>

</tr>

<tr>

<td>

**Prior use of Web browsing software**</td>

<td colspan="2">53%</td>

</tr>

<tr>

<td></td>

<td colspan="2"></td>

</tr>

<tr>

<td>

**Average months of prior use**</td>

<td colspan="2">5 months(range of 0 - 48 months)</td>

</tr>

<tr>

<td></td>

<td colspan="2"></td>

</tr>

<tr>

<td>

**Prior purchases through the World Wide Web**</td>

<td colspan="2">8%</td>

</tr>

<tr>

<td></td>

<td colspan="2"></td>

</tr>

</tbody>

</table>

### Experiential Tasks.

Web pages were created that outlined the shopping tasks and provided links to various shopping sites. Pointers to the shopping sites and instructions for study participants were available at Jarvenpaa's Website at the University of Texas. These included a link to the SHOPPER, a web page that indexes over 350 shopping sites, plus pointers to 5 electronic malls rated highly by various services such as Point Communications Corporation. The pages also contained pointers to catalogue retailers (JCPenney, LLBean and Land's End) and participants could access NetSearch facilities to search for particular products, services or shops. The intent was to create a selection of sites that were of good quality.

The participants performed three shopping activities. The first was a familiarization task, where participants browsed among different shopping sites. The second was a gift shopping exercise, to try to find a gift for a friend or relative. The third task was a personal shopping exercise, where participants tried to find something to buy for themselves. During each of these activities participants were told that they were not required to make a purchase and were personally responsible for any purchases they did make. The tasks were meant to be typical of those that the consumer might routinely perform. Their primary purpose was to provide the participants with a focus for their shopping activities. Table 4 summarizes the products and services that the consumers in our study most frequently considered buying as they completed the required shopping tasks.

<table><caption>

**Table 4: Products/Services Consumers Most Frequently Considered (% of shoppers who visited related sites during the three shopping activities)**</caption>

<tbody>

<tr>

<td>

**Product/Service**</td>

<td>

**Current Study:**</td>

</tr>

<tr>

<td>

*   Clothing
*   Home (e.g., hardware, appliances)
*   Books
*   Sporting goods and toys
*   Food
*   Travel
*   Accessories (e.g., jewelry, perfume)
*   Music (e.g., CD's)
*   Home electronics (not including computers)

</td>

<td>

*   40%
*   32%
*   21%
*   20%
*   18%
*   18%
*   17%
*   14%
*   10%

</td>

</tr>

</tbody>

</table>

### Procedures

Participants first completed a demographic questionnaire and a consent form. This was followed by a 5 minute overview of the study and introduction to World Wide Web and Netscape. Next, participants engaged in the familiarization task for 10 minutes, followed by the 20 minute gift shopping exercise. Next, they spent 10 minutes filling out an open ended survey about the shopping experience to that point. Then the personal buying activity lasted another 20 minutes, after which the participants completed a structured questionnaire. This was followed by an optional 30 minute focus group session which approximately half the participants attended. For all sessions, lab assistants were present to answer questions, help the participants find specific sites, and assist in navigation over the Web. The data were collected in groups of 10 to 24 over 5 days in January 1996.

### Measures

The items used to measure prior Web experience, attitude towards computers, shopping enjoyment, direct shopping experience were constructed on the basis of past literature. These items are reported in [Appendix A](#appa) along with the items for the demographic factors. Scales to measure each of the beliefs were developed based on previous literature and existing scales where possible (see Jarvenpaa and Todd, 1997b). Measures related to service quality were based on the SERVQUAL scales of Parasuraman, Zeithaml and Berry (1988; 1994a; 1994b), scales related to the shopping experience were based on prior work by Davis (1989) for effort, Webster et al (1991) for playfulness and Moore and Benbasat (1991) for compatibility. Measures of attitude and intention were based on the suggestions of Fishbein and Ajzen (1975).

### Analysis Strategy

The data were analyzed using correlation and regression techniques. The structural equation modeling technique was not used because of the sample size and the relationships being investigated were exploratory as they have not been previously investigated in the Web shopping context.

## Results

Results are presented in terms of correlation and regression analyses which look at the effects of the individual difference factors on beliefs, attitudes and intentions toward shopping on the Internet. Factor analysis was used to confirm the individual difference constructs of prior Web experience, computer attitude, shopping enjoyment, and direct shopping frequency ([Appendix B](#appb)). The constructs for beliefs, attitude, and intention were developed in an earlier paper (see Jarvenpaa and Todd, 1997). The criteria used to identify and interpret factors were that a given item should load .50 or higher on a specific factor and have a loading no higher than .35 on other factors. In general, items mapped closely with their intended factors. Table 5 provides a summary of descriptive statistics, intercorrelations, and reliability coefficients for the difference constructs, demographics, attitude, and intention. The reliability coefficients ranged in size from .61 for shopping enjoyment to .89 for prior Web experience. While some of the coefficients could be higher, they appear quite adequate given the exploratory nature of the study (Nunnally, 1967). Furthermore, the sizes of the coefficients are comparable with others reported in the literature (e.g., Korgaonkar, Lund and Price, 1985).

### Intercorrelations Among Study Variables

Table 5 presents the intercorrelations among the individual difference constructs, demographic factors, attitude, and intention. As noted, attitude towards internet shopping is correlated with computer attitude (_r_=0.26, _p_< 0.01) and prior Web experience (_r_=0.21, _p_ <0.01). Intention towards internet shopping is correlated with attitude (_r_=0.65, _p_ <0.01), prior Web experience (_r_=0.32, _p_ <0.01), computer attitude (_r_=0.27, _p_ <0.01), and gender (_r_=0.16, _p_ <0.05).

Table 6 presents the intercorrelations between the individual difference factors and the consumer's beliefs. Prior Web experience is positively correlated with compatibility (_r_=0.15, _p_ <0.05) and social risk (_r_=0.16, _p_ <0.05). Shopping enjoyment is negatively correlated with compatibility (_r_=-0.25, _p_ <0.01). Direct shopping frequency is negatively correlated with price (_r_=-0.29, _p_ <0.01), privacy (_r_=-0.18, _p_ < 0.05), and performance risk (_r_=-0.15, _p_ <0.05). It is positively correlated with product value, quality, service, social, effort, and compatibility, ranked from most significant to least (range of _r_=0.32 to 0.21, _p_ <0.01), and with shopping experience (_r_=0.16, _p_ <0.05) and performance (_r_=0.15, _p_ < 0.05). Computer attitudes is positively correlated with shopping experience (_r_=0.31, _p_ <0.01), compatibility (_r_=0.22, _p_ <0.01), empathy (_r_=0.18, _p_ <0.05), playfulness (_r_=0.17, _p_ <0.05), and product variety (_r_=0.16, _p_ <0.05). Gender is correlated negatively with personal risk (_r_=-0.21, _p_ <0.01) and positively with social experience (_r_=0.19, _p_ <0.01). Work tenure is positively correlated with service (_r_=0.19, _p_ <0.01), and education is negatively correlated with effort (_r_=-0.15, _p_ <0.05) andtangibility (_r_=-0.15, _p_< 0.05).

<table><caption>

**Table 5: Means, Standard Deviations, and Correlations**  
Correlations greater than or equal to 0.15 are significant at _p_ < .05; for _r_ > 0.19, _p_ < .01.  
Amounts in parentheses indicate the internal reliability (Cronbach's Alpha) for the construct.</caption>

<tbody>

<tr>

<td>

**Variable**</td>

<td>

**Mean**</td>

<td>

**s.d.**</td>

<td>

**1**</td>

<td>

**2**</td>

<td>

**3**</td>

<td>

**4**</td>

<td>

**5**</td>

<td>

**6**</td>

<td>

**7**</td>

<td colspan="2">

**8**</td>

<td colspan="2">

**9**</td>

<td>

**10**</td>

<td>

**11**</td>

<td>

**12**</td>

</tr>

<tr>

<td>

**<u>Difference Constructs</u>**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td colspan="2"> </td>

<td colspan="2"> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**1\. Prior Web experience**</td>

<td>1.88</td>

<td>0.71</td>

<td>(0.89)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td colspan="2"></td>

<td colspan="2"></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**2\. Shopping enjoyment**</td>

<td>4.72</td>

<td>1.47</td>

<td>-0.07</td>

<td>(-0.81)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td colspan="2"></td>

<td colspan="2"></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**3\. Direct shopping frequency**</td>

<td>1.54</td>

<td>0.99</td>

<td>-0.03</td>

<td>0.14</td>

<td>(0.61)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td colspan="2"></td>

<td colspan="2"></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**4\. Computer attitudes**</td>

<td>4.96</td>

<td>1.55</td>

<td>0.39</td>

<td>-0.07</td>

<td>-0.02</td>

<td>(0.63)</td>

<td> </td>

<td> </td>

<td> </td>

<td colspan="2"></td>

<td colspan="2"></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**<u>Demographics</u>**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td colspan="2"> </td>

<td colspan="2"> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**5\. Gender**</td>

<td>1.17</td>

<td>0.38</td>

<td>0.19</td>

<td>-0.23</td>

<td>0.04</td>

<td>0.11</td>

<td>(n/a)</td>

<td></td>

<td></td>

<td colspan="2"></td>

<td colspan="2"></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>

**6\. Age**</td>

<td>43.37</td>

<td>10.44</td>

<td>-0.32</td>

<td>-0.07</td>

<td>0.04</td>

<td>-0.11</td>

<td>0.02</td>

<td>(n/a)</td>

<td></td>

<td colspan="2"></td>

<td colspan="2"></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>

**7\. Work tenure**</td>

<td>13.78</td>

<td>9.91</td>

<td>-0.07</td>

<td>-0.10</td>

<td>0.01</td>

<td>0.10</td>

<td>0.15</td>

<td>0.55</td>

<td>(n/a)</td>

<td colspan="2"></td>

<td colspan="2"></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**8\. Income**</td>

<td>4.35</td>

<td>1.76</td>

<td>-0.20</td>

<td>0.10</td>

<td>-0.02</td>

<td>0.07</td>

<td>-0.10</td>

<td>0.24</td>

<td>0.19</td>

<td colspan="2">(n/a)</td>

<td colspan="2"></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**9\. Household size**</td>

<td>2.78</td>

<td>1.23</td>

<td>-0.16</td>

<td>-0.03</td>

<td>-0.01</td>

<td>-0.01</td>

<td>0.06</td>

<td>0.12</td>

<td>0.06</td>

<td colspan="2">0.44</td>

<td colspan="2">(n/a)</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**10.Education**</td>

<td>3.07</td>

<td>0.82</td>

<td>-0.03</td>

<td>-0.16</td>

<td>-0.21</td>

<td>0.10</td>

<td>0.05</td>

<td>0.18</td>

<td>0.05</td>

<td colspan="2">0.33</td>

<td colspan="2">0.08</td>

<td>(n/a)</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**<u>Outcomes</u>**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td colspan="2"></td>

<td colspan="2"></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**11\. Attitude**</td>

<td>5.03</td>

<td>1.60</td>

<td>0.21</td>

<td>-0.10</td>

<td>0.13</td>

<td>0.26</td>

<td>0.11</td>

<td>-0.06</td>

<td>0.10</td>

<td colspan="2">-0.01</td>

<td colspan="2">-0.01</td>

<td>-0.07</td>

<td>(0.92)</td>

<td> </td>

</tr>

<tr>

<td>

**12.Intention**</td>

<td>3.43</td>

<td>1.38</td>

<td>0.32</td>

<td>-0.06</td>

<td>0.13</td>

<td>0.27</td>

<td>0.16</td>

<td>0.12</td>

<td>0.10</td>

<td colspan="2">0.13</td>

<td colspan="2">0.12</td>

<td>0.03</td>

<td>0.65</td>

<td>(0.86)</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 6: Correlations Between Models**
** Correlation is significant at the 0.01 level (2-tailed).  
\* Correlation is significant at the 0.05 level (2-tailed).  
Listwise N=183</caption>

<tbody>

<tr>

<td colspan="11"></td>

</tr>

<tr>

<td colspan="11">

<sup>**a**</sup></td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td></td>

<td> </td>

<td> </td>

<td> </td>

<td></td>

<td></td>

</tr>

<tr>

<td></td>

<td colspan="4">

**Difference Constructs**</td>

<td colspan="6">

**Demographics**</td>

</tr>

<tr>

<td></td>

<td>

**Prior Web Experience**</td>

<td>

**Shopping Enjoyment**</td>

<td>

**Direct Shopping**</td>

<td>

**Computer Attitude**</td>

<td>  

**Gender**</td>

<td>  

**Age**</td>

<td>

**Work Tenure**</td>

<td>  

**Income**</td>

<td>

**Household Size**</td>

<td>  

**Education**</td>

</tr>

<tr>

<td>

**<u>Product Value</u>**</td>

<td>.127</td>

<td>-.071</td>

<td>.319(**)</td>

<td>.081</td>

<td>.130</td>

<td>-.001</td>

<td>.083</td>

<td>-.027</td>

<td>-.083</td>

<td>-.052</td>

</tr>

<tr>

<td>

**Price**</td>

<td>.001</td>

<td>-.044</td>

<td>-.293(**)</td>

<td>-.019</td>

<td>-.027</td>

<td>-.064</td>

<td>-.073</td>

<td>-.081</td>

<td>.098</td>

<td>.091</td>

</tr>

<tr>

<td>

**Quality**</td>

<td>-.007</td>

<td>.019</td>

<td>.256(**)</td>

<td>.047</td>

<td>.125</td>

<td>.129</td>

<td>.125</td>

<td>.004</td>

<td>-.077</td>

<td>-.085</td>

</tr>

<tr>

<td>

**Variety**</td>

<td>.013</td>

<td>-.120</td>

<td>.142</td>

<td>.164(*)</td>

<td>.077</td>

<td>.031</td>

<td>.073</td>

<td>-.021</td>

<td>-.118</td>

<td>-.007</td>

</tr>

<tr>

<td>

**<u>Shopping Experience</u>**</td>

<td>.085</td>

<td>-.057</td>

<td>.160(*)</td>

<td>.312(**)</td>

<td>.035</td>

<td>-.033</td>

<td>.037</td>

<td>.022</td>

<td>-.085</td>

<td>-.069</td>

</tr>

<tr>

<td>

**Compatibility**</td>

<td>.148(*)</td>

<td>-.245(**)</td>

<td>.205(**)</td>

<td>.216(**)</td>

<td>.132</td>

<td>.001</td>

<td>.092</td>

<td>.000</td>

<td>-.033</td>

<td>-.069</td>

</tr>

<tr>

<td>

**Effort**</td>

<td>-.057</td>

<td>.068</td>

<td>.212(**)</td>

<td>-.031</td>

<td>-.066</td>

<td>-.031</td>

<td>-.064</td>

<td>-.002</td>

<td>-.097</td>

<td>-.154(*)</td>

</tr>

<tr>

<td>

**Playfulness**</td>

<td>.047</td>

<td>.087</td>

<td>.066</td>

<td>.170(*)</td>

<td>.030</td>

<td>-.027</td>

<td>-.001</td>

<td>.016</td>

<td>-.066</td>

<td>-.087</td>

</tr>

<tr>

<td>

**Social**</td>

<td>-.061</td>

<td>.018</td>

<td>.218(**)</td>

<td>-.010</td>

<td>.194(**)</td>

<td>.017</td>

<td>.052</td>

<td>-.122</td>

<td>.021</td>

<td>-.045</td>

</tr>

<tr>

<td>

**<u>Customer Service</u>**</td>

<td>.057</td>

<td>-.055</td>

<td>.233(**)</td>

<td>.087</td>

<td>.078</td>

<td>.067</td>

<td>.194(**)</td>

<td>-.064</td>

<td>-.071</td>

<td>-.093</td>

</tr>

<tr>

<td>

**Empathy**</td>

<td>.143</td>

<td>.017</td>

<td>.120</td>

<td>.180(*)</td>

<td>-.102</td>

<td>.013</td>

<td>.031</td>

<td>.043</td>

<td>-.010</td>

<td>-.146(*)</td>

</tr>

<tr>

<td>

**Reliability**</td>

<td>.001</td>

<td>-.089</td>

<td>.053</td>

<td>-.018</td>

<td>.093</td>

<td>-.036</td>

<td>.010</td>

<td>.042</td>

<td>-.005</td>

<td>.071</td>

</tr>

<tr>

<td>

**Responsiveness**</td>

<td>.022</td>

<td>.015</td>

<td>.156(*)</td>

<td>.027</td>

<td>.005</td>

<td>-.094</td>

<td>.049</td>

<td>-.062</td>

<td>-.105</td>

<td>-.112</td>

</tr>

<tr>

<td>

**Tangibility**</td>

<td>.038</td>

<td>-.063</td>

<td>.097</td>

<td>.138</td>

<td>-.019</td>

<td>-.008</td>

<td>.106</td>

<td>-.013</td>

<td>-.037</td>

<td>-.148(*)</td>

</tr>

<tr>

<td>

**<u>Risk</u>**</td>

<td>.096</td>

<td>.090</td>

<td>-.124</td>

<td>.011</td>

<td>-.070</td>

<td>-.074</td>

<td>-.035</td>

<td>.018</td>

<td>-.004</td>

<td>.138</td>

</tr>

<tr>

<td>

**Economic**</td>

<td>-.029</td>

<td>.094</td>

<td>-.080</td>

<td>.029</td>

<td>.122</td>

<td>-.076</td>

<td>.094</td>

<td>.032</td>

<td>-.032</td>

<td>.035</td>

</tr>

<tr>

<td>

**Performance**</td>

<td>.042</td>

<td>.128</td>

<td>-.150(*)</td>

<td>-.036</td>

<td>-.003</td>

<td>-.007</td>

<td>-.096</td>

<td>.013</td>

<td>.013</td>

<td>.108</td>

</tr>

<tr>

<td>

**Personal**</td>

<td>-.004</td>

<td>.078</td>

<td>-.044</td>

<td>-.001</td>

<td>-.212(**)</td>

<td>-.052</td>

<td>-.026</td>

<td>.037</td>

<td>.123</td>

<td>.044</td>

</tr>

<tr>

<td>

**Privacy**</td>

<td>.089</td>

<td>.080</td>

<td>-.176(*)</td>

<td>.065</td>

<td>-.128</td>

<td>-.097</td>

<td>-.055</td>

<td>.065</td>

<td>.113</td>

<td>.136</td>

</tr>

<tr>

<td>

**Social Risk**</td>

<td>.157(*)</td>

<td>.013</td>

<td>-.060</td>

<td>-.081</td>

<td>-.033</td>

<td>-.077</td>

<td>-.033</td>

<td>-.070</td>

<td>-.086</td>

<td>.038</td>

</tr>

</tbody>

</table>

### Results on Hypothesis Tests

Table 7 presents the regression results. The individual difference factors including the demographic variables explained 22% of the variance in intention, but only 6% of the variance in attitude. Examination of the model results for intention indicates that five of the individual difference factors yielded statistically significant standardized coefficient estimates - prior Web experience, age, household size, computer attitude, and direct shopping frequency. Thus, the model results support the influence of these variables as predictor's of the consumer's intention to shop on the Internet. Examination of the model results for attitude indicates that only one of the individual difference factors yielded a statistically significant coefficient estimate - computer attitude (_b_=0.26, _t_=3.66).

<table><caption>

**Table 7: Regression Results on Intention**</caption>

<tbody>

<tr>

<td>

_**Independent Variable**_</td>

<td>

_**Standardized b**_</td>

<td>

_**Significance level**_</td>

<td>

_**Cumulative R2**_</td>

</tr>

<tr>

<td>Prior Web Experience</td>

<td>.36</td>

<td>.000</td>

<td>.099</td>

</tr>

<tr>

<td>Age</td>

<td>.23</td>

<td>.001</td>

<td>.153</td>

</tr>

<tr>

<td>Household size</td>

<td>.25</td>

<td>.025</td>

<td>.177</td>

</tr>

<tr>

<td>Computer Attitude</td>

<td>.25</td>

<td>.030</td>

<td>.198</td>

</tr>

<tr>

<td>Direct shopping frequency</td>

<td>.13</td>

<td>.048</td>

<td>.216</td>

</tr>

</tbody>

</table>

Hypothesis 1 predicted that prior Web experience will positively affect as well as moderate beliefs, attitude, and intention toward Internet shopping. The regression results noted above suggest that prior Web experience had a significant impact on intention to shop, but it did not affect attitude towards Internet shopping. In addition, prior Web experience was found to moderate shopping beliefs, attitude, and intention in several ways. Table 8 shows the beta coefficients for beliefs, attitude, and intention for two groups of consumers categorized on the basis of prior Web experience. Those with 'no experience' had never before used the Web prior to the shopping study.

<table><caption>

**Table 8: Moderating Impact of Prior Web Experience on Beliefs, Attitude and Intention**  
\* Significant at _p_ <0.05    ** Significant at _p_ <0.01</caption>

<tbody>

<tr>

<th rowspan="2">Variable</th>

<th colspan="2">Experience Level</th>

</tr>

<tr>

<th>None  
n=98</th>

<th>Some Experience  
n=121</th>

</tr>

<tr>

<td>

**Value**</td>

<td>(.75)</td>

<td>(.66)</td>

</tr>

<tr>

<td>

**Price**</td>

<td>-.542**</td>

<td>-.398**</td>

</tr>

<tr>

<td>

**Quality**</td>

<td>.353**</td>

<td>.367**</td>

</tr>

<tr>

<td>

**Variety**</td>

<td>.094</td>

<td>.218**</td>

</tr>

<tr>

<td>

**Experience**</td>

<td>(.49)</td>

<td>(.48)</td>

</tr>

<tr>

<td>

**Compatibility**</td>

<td>.431**</td>

<td>.479**</td>

</tr>

<tr>

<td>

**Effort**</td>

<td>.305**</td>

<td>-.021</td>

</tr>

<tr>

<td>

**Playful**</td>

<td>.277**</td>

<td>.341**</td>

</tr>

<tr>

<td>

**Social**</td>

<td>-.050</td>

<td>.058</td>

</tr>

<tr>

<td>

**Service**</td>

<td>(.69)</td>

<td>(.57)</td>

</tr>

<tr>

<td>

**Empathy**</td>

<td>.080</td>

<td>-.027</td>

</tr>

<tr>

<td>

**Reliability**</td>

<td>.216**</td>

<td>.257**</td>

</tr>

<tr>

<td>

**Responsibility**</td>

<td>.542**</td>

<td>.491**</td>

</tr>

<tr>

<td>

**Tangibility**</td>

<td>.182*</td>

<td>.237**</td>

</tr>

<tr>

<td>

**Risk**</td>

<td>(.61)</td>

<td>(.68)</td>

</tr>

<tr>

<td>

**Economic**</td>

<td>-.097</td>

<td>.067</td>

</tr>

<tr>

<td>

**Perform**</td>

<td>.424**</td>

<td>.427**</td>

</tr>

<tr>

<td>

**Personal**</td>

<td>.313**</td>

<td>.382**</td>

</tr>

<tr>

<td>

**Privacy**</td>

<td>.248**</td>

<td>.291**</td>

</tr>

<tr>

<td>

**Social Risk**</td>

<td>.043</td>

<td>.007</td>

</tr>

<tr>

<td>

**Attitude**</td>

<td>(.47)</td>

<td>(.49)</td>

</tr>

<tr>

<td>

**Value**</td>

<td>.205*</td>

<td>.278**</td>

</tr>

<tr>

<td>

**Experience**</td>

<td>.269**</td>

<td>.395**</td>

</tr>

<tr>

<td>

**Service**</td>

<td>.152</td>

<td>-.074</td>

</tr>

<tr>

<td>

**Risk**</td>

<td>-.236*</td>

<td>-.268**</td>

</tr>

<tr>

<td>

**Intention**</td>

<td>(.50)</td>

<td>(.48)</td>

</tr>

<tr>

<td>

**Value**</td>

<td>.086</td>

<td>.209*</td>

</tr>

<tr>

<td>

**Experience**</td>

<td>.126</td>

<td>-.067</td>

</tr>

<tr>

<td>

**Service**</td>

<td>.165</td>

<td>.134</td>

</tr>

<tr>

<td>

**Risk**</td>

<td>-.237*</td>

<td>.004</td>

</tr>

<tr>

<td>

**Attitude**</td>

<td>.275**</td>

<td>.535**</td>

</tr>

</tbody>

</table>

While the beta values for beliefs change with experience, the prior shopping model is still useful in explaining Internet shopping beliefs, attitude and intention (adjusted R<sup>2</sup> ranging from .47 to .75). As prior web experience increases: attitude becomes more significant in explaining intention to shop; shopping experience has a greater impact on attitude; personal risk and privacy have a larger influence on risk; and playfulness is a greater determinant of shopping experience. At the same time, effort and price have a larger impact on shopping experience and value, respectively, for less experienced shoppers. Attitude becoming more significant with experience is consistent with the literature and other technologies. Once experience is gained, consumers want merchants to offer a fun experience with low risk.

Hypothesis 2 predicted that a consumer's attitude towards computers will affect beliefs, attitude and intention toward Internet shopping. Consistent with this hypothesis, the regression results in Table 7 suggest that computer attitude has a significant impact on attitude and intention toward Internet shopping. Also, while not presented, computer attitude was significant in regression analysis in explaining several beliefs, including shopping experience.

Hypothesis 3 predicted that those with more favorable attitudes toward shopping in general will have a more favorable attitude and intention towards Internet shopping. This hypothesis was not supported by the regression analysis. Shopping orientation (i.e., enjoyment) had neither positive nor negative effect on attitude and intention toward Internet shopping.

Hypothesis 4 predicted that those who more frequently use other direct shopping modes will have a more favorable attitude and intention towards Internet shopping. As indicated in Table 7, direct shopping frequency did have a significant positive impact on intention towards Internet shopping.

Hypothesis 5 predicted that higher income, younger, more educated, and single male shoppers will have more positive attitudes and intentions towards Internet shopping. The regression results in Table 7 show that this hypothesis is not supported. Age did have an impact on intention, but the coefficient is positive not negative (i.e., intention increases with age). Also, household size did have a significant impact on intention; however, it increases with household size.

## Discussion and Implications

The study investigated the effect of individual difference factors on beliefs, attitude and intention to shop on the Web. Overall, the individual difference variables explained only 22% of the variance in intention and less than 10% in attitude. The shopping beliefs explained about 50% of the variance in attitude and 52% in intentions (Jarvenpaa and Todd, 1997). Therefore, the beliefs should be directly related to the design characteristics of the online store fronts. The current results suggest that improving the store fronts, and thereby consumers' beliefs about Web shopping, should be a greater concern for retailers than simply waiting for Internet shopping (and therefore customer attitude and intention) to mature.

However, the maturing of this shopping mode and its users will have a significant impact. Our study also suggests that beliefs that impact attitude and intention to shop are likely to change somewhat from the time of initial adoption to frequent utilization of the new media. One can hypothesize that as access and exposure to the Web increases, people who have positive encounters with computer technology will develop more positive attitudes towards shopping on the Web.

The current study also suggests that prior Web experience makes a difference in favorable intention and favorable attitude towards computers make a difference in both attitude and intention toward Web shopping. Since among the general public, a vast majority has no knowledge of the Web, it seems important that the public be educated about Web shopping and understand that it is not a trivial task. Overall, their attitude and behavior will depend on their past experiences and their attitudes towards computers. Internet shopping is likely to experience negative reactions from consumers if it is aggressively hyped. Further, marketing that does not take into account factors relating to the time and effort required to navigate on the Web may not be well received. An informal analysis of the open-ended comments on the most liked and least liked site indicated that the study's consumers found it effortful not only to browse a site once it was located but to locate appropriate sites in the first place (Jarvenpaa and Todd, 1996-1997).

The significant effects of individual differences also suggest that the potential market of Internet shopping might need to be segmented. The study found attitude and intention differences depending on the prior Web experience, computer attitudes, age, household size, and frequency of direct shopping. Merchants need to recognize these differences and identify methods to tailor their designs on the Web to these different individuals.

The study also discovered some unexpected effects. Affluent women, many with advanced degrees, were as likely to develop favorable attitude and somewhat even greater intention to shopping on the Web as their male counterparts. Also, shoppers from larger households were more likely to develop a favorable intention to shop on the Web. This last finding is similar to that of Lumpkin and Hawes (1985) who found the number of children living at home to be positively related to the catalog usage. This likely reflects the time pressures facing shoppers in these households and their view that the Web may better help them to manage their time.

This research should be extended to more clearly relate the views of different potential Internet market segments. Using the attitude-behavior model as a framework, it would be useful to examine more carefully how the importance of different factors in the models change as a function of various individual differences. Calibration of such models would be useful for developing an integration of the attitude behavior models and the individual difference literature. More applied research might also begin to look at how different segments of the user population might respond to different site design strategies. For example, those with a strong positive orientation towards shopping in general may be most attracted to Web based sites that embed the shopping experience in a rich community of interest, while those with a negative shopping orientation may be looking for sites that emphasize ease, efficiency and value. Studies which look at how different user segments respond to alternative site design strategies will be important to ensuring the long run development of the Internet based retailing environment.

## Conclusion

The Internet marketing has many problems requiring corrective action. For corrective action to succeed, it must be based on knowledge of potential customers' attitudes and intentions toward the new mode and the antecedents that underlie those attitudes. The objective of the current study was to examine the influence of demographic factors, prior Internet experience, computer attitudes, shopping orientation, and frequency of shopping in direct marketing channels as determinants of consumers' attitude toward Internet marketing and intention to patronize Internet online stores. The study involved an experiential survey; all participants were brought to a university computer laboratory where they browsed through electronic malls for 1.5 hours on the worldwide web and completed survey instruments comprised of items obtained from the literature. The study results indicate that attitudes towards computers affect attitudes towards Internet shopping. Prior Internet experience, attitudes towards computers, age, household size, and frequency of shopping via direct marketing channels affect intentions towards Internet shopping. Depending on their prior experience with the Internet, the consumers placed somewhat different emphasis on various beliefs about Internet shopping. Overall, however, the retail patronage model that we had developed in an earlier paper seems to hold for both inexperienced Web users as well as those with Web experience.

In conclusion, it is important to acknowledge the exploratory character of the results due to the immature and changing technology as well as the study limitations. The study's procedural and task limitations are discussed elsewhere (Jarvenpaa and Todd, 1997). Among the limitations that impact the relative effects of the individual differences variables are the following. Perhaps those users with experience with the Web but with negative attitudes towards Web shopping declined the invitation to participate in the study. The study had a limited number of males. The general attitude towards shopping was high and hence those that disliked shopping may have declined the invitation. Finally, our consumers were either from affluent township or from the university community and so the results may not reflect the full diversity of beliefs and attitude toward shopping.

* * *

## Acknowledgements

This work has been supported by grants from the Graduate School of Business, University of Texas at Austin and the Natural Sciences and Engineering Research Council of Canada. We are grateful to Dave Hannah, Tom Shaw, Connie Todd, and Joe Wiseman for their assistance with the collection and coding of the data reported in this study.

## Notes

\* Contact person for correspondence

Originally published June 1997 - contact information for authors updated January 2007

This paper was originally available at the home page of C. Brad Crisp at the University of Texas, Austin, who has made the paper available to Information Research, with the agreement of his co-authors.

## References

*   _Advertising Age_, Web Lacks Transactions, But Influences Decisions. (April 13, 1995): 26.
*   Akaah, I.P., Korgaonkar, P.K., Lund, D., Direct Marketing Attitudes. _Journal of Business Research_ 34 (1995): 211-219\.
*   Alba, J., Lynch, J., Weitz, B., Janiszewski, C., Lutz, R., Sawyer, A., and Wood, S., Interactive Home Shopping and the Retail Industry. Working Paper, University of Florida (December 1996).
*   Ajzen, I. and M. Fishbein, _Understanding Attitudes and Predicting Social Behavior_, Prentice-Hall, Inc., Englewood Cliffs, NJ. 1980\.
*   Associated Press, More Go On Line in U.S., poll finds. _Austin American Statesman_ (Tuesday, October 17, 1995): F1, F3\.
*   Bagozzi, R.P., and Warshaw, P.R., Trying to Consume. _Journal of Consumer Research_ 17 (1990): 127-140.
*   Bellenger, D.N. and Korgaonkar, P., Profiling the Recreational Shopper. _Journal of Retailing_ 56 (Fall 1980): 77-92\.
*   Berkowitz, E.N., Walton, J.R., and Walker, O.C., In-home Shoppers: The Market for Innovative Distribution Systems. _Journal of Retailing_ 55 (Summer 1979): 15-33.
*   Blaylock, James R., and Smallwood, David M., Intrahousehold Time Allocation: The Case of Grocery Shopping. _Journal of Consumer Affairs_. 21(2) (Winter 1987): 183-201.
*   Borenstein, N.S., Perils and Pitfalls of Practical Cybercommerce. _Communications of the ACM_ (June 1996): 36-44\.
*   Burstein, D., and Kline, D., _Road Warriors_, Dutton Book, New York, NY. 1995.
*   _Business Week,_ A Census in Cyberspace.Special Report, (May 5, 1997): 84-85.
*   Carlson, D.D., Shopping Orientations: Comment and Findings_. Proceedings of the Southwestern MarketingAssociation_ (1978).
*   Champy, J., Buday,R., and Nohria, N., The Rise of The Electronic Community_. CSC Index Insights_ (Spring 1996): 1-15.
*   Cunningham, I., and Cunningham, W.H., The Urban In-Home Shopper: Socioeconomic and Attitudinal Characteristics. _Journal of Retailing_ 49 (Fall 1973): 42-50.
*   Darden, W.R., and Howell, R.D., Socialization Effects of Retail Work Experience on Shopping Orientations. _Journal of the Academy of Marketing Science_ 15, 3 (Fall 1987): 52-63.
*   Darian, J.C., In-Home Shopping: Are There Consumer Segments? _Journal of Retailing_ 63/2 (Summer 1987): 163-186.
*   Davis, F.D., Perceived Usefulness, Perceived Ease of Use, and User Acceptance of Information Technology. _MIS Quarterly_ 13 (September 1989): 319-339.
*   Davis, F.D., Bagozze, R. P., and Warshaw, P.R., User Acceptance of Computer Technology: A Comparison of Two Theoretical Models. _Management Science_ 35 (August 1989): 982-1003.
*   DeLone, W.H., Determinants of Success for Computer Usage in Small Business, _MIS Quarterly_ 12, 1 (1988): 51-61.
*   Dholakia, R.R., Pederson, B., and Hikmet, N., Married Males and Shopping: Are They Sleeping Partners. _International Journal of Retail and Distribution Management_ 23, 3 (1995): 27-33.
*   Eastlick, M. A., Predictors of Videotext Adoption. _Journal of Direct Marketing_ 7, 3 (Summer 1993): 66-76.
*   Eastlick, M.A. and Feinberg, R.A., Gender Differences in Mail-Catalog Patronage Motives. _Journal of Direct Marketing_ 8, 7 (Spring 1994): 37-44.
*   _Economist_, Electronic Commerce: In Search of the Perfect Market. Special Survey, (May 10th, 1997): 3-18.
*   Feldman, J.M., and Star, A., Racial Factors in Shopping Behavior, in _A New Measure of Responsibility for Marketing._ Cox, K. and Ennis, B. eds., American Marketing Association, Chicago. 1968, pp. 216-226.
*   Fields, D.M., and Greco, A.J., Acceptance of the Idea of In-Home Video Shopping, in _Retailing: Its Past and Future_. King, R.L., ed., The Academy of Marketing Science, Charleston, S.C. 1988, pp. 70-75.
*   Fishbein, M., and Ajzen, I., _Belief, Attitude, Intention and Behavior: An Introduction to Theory and Research_. Addison-Wesley, Reading, MA. 1975.
*   Fram, E.H., and Grady, D.B., Internet Buyers: Will the Surfers Become Buyers. _Direct Marketing_ 58, 6 (October 1995): 63-85\.
*   Fuerst, W.L., and Cheney, P.H., Factors Affecting the Perceived Utilization of Computer-Based Decision Support Systems. _Decision Sciences,_ 17, 3 (1986): 329-356.
*   Gattiker, U. E., Computer Skills Acquisition: A Review and Future Directions for Research, _Journal of Management_ 18, 3 (1992): 547-574.
*   Gattiker, U.E., _Women and Technology_, de Gruyter Publishers, Hawthorne, New York, 1994.
*   Gehrt, K.C., and Carter, K., An Exploratory Assessment of Catalog Shopping Orientations. _Journal of Direct Marketing_ 6, 1 (Winter 1992): 29-39.
*   Gehrt, K.C., Alpander, G.G., and Lawson, D., A Factor-Analytic Examination of Catalog Shopping Orientations in France. _Journal of Euromarketing_ 2(2) (1992): 49-69.
*   Gillett, P.L., In-Home Shoppers - An Overview. _Journal of Marketing_ 10 (1976): 81-88.
*   Gupta, S. and Chatterjee, R., Consumer and Corporate Adoption of the World Wide Web as a Commercial Medium, in _Electronic Marketing and Consumer._ Peterson, R.A. ed., Sage, Thousand Oaks, CA. 1997.
*   Georgia Institute of Technology. _Graphic, Visualization, & Usability Center_. [GVU's 4th WWW User Survey, December 1995](http://www.webcitation.org/5LcmJYx5Z). Retrieved 3 January, 2006 from http://www-static.cc.gatech.edu/gvu/user_surveys/survey-10-1995/
*   Gutek, B., and Bikson, T.K., Differential Experiences of Men and Women in Computerized Offices's Sex Roles. Sex Roles 13, 3-4 (1985): 123-136.
*   Harrison, A.W., and Rainer, R.K. Jr., The Influence of Individual Differences on Skill in End-User Computing. _Journal of Management Information Systems_ 9, 1 (1992): 93-111.
*   Hoffman, D.L., Kalsbeek, S.M., and Novak, T.P., Internet and Web Use in the United States: Baselines for Commercial Development, in _Communications of the ACM_, ACM, 39, 12 (December 1996), pp. 36-46.
*   Hoffman, D.L., and Novak, T.P., Marketing in Hypermedia Computer-Mediated Environments: Conceptual Foundations._Journal of Marketing_ 60 (July 1996): 50-68.
*   Igbaria, M., Guimaraes, T., and Davis, G.B., Testing the Determinants of Microcomputer Usage via a Structural Equation Model. _Journal of Management Information Systems_ 11, 4 (Spring 1995): 87-114.
*   Igbaria, M. and Parasuraman, S., A Path Analytic Study of Individual Characteristics, Computer Anxiety and Attitudes Towards Microcomputers. _Journal of Management_ 15, 3 (1989): 373-388.
*   Igbaria, M., Pavri, F., and Huff, S., Microcomputer Application: An Empirical Look at Usage. _Information and Management_ 16, 4 (1989): 187-196.
*   _Information Week_, Electronic Commerce --- The Web 'Ads' Up. cover story. (August 5, 1996): 40.
*   Ives, B., Mondex International: Reengineering Money. A Case Study, London School of Business, 1997.
*   Jarvenpaa, S.L., and Todd, P.A., Consumer Reactions to Electronic Shopping on the World Wide Web. _Journal of Electronic Commerce_ 1, 2 (Winter 1996-97): 59-88.
*   Jarvenpaa, S.L., and Todd, P.A., An Empirical Study of Determinants of Attitude and Intention Towards Internet Shopping. Working paper, University of Texas/Queens University (February 1997).
*   Jasper, C.R. and Lan, R.R., Apparel Catalog Patronage: Demographic, Lifestyle and Motivational Factors. _Psychology and Marketing_ 9/4 (July-August 1992): 275-296\.
*   Kilsheimer, J. C., and Goldsmith, R.E., NonStore Shopping Among Senior Citizens. _Acros Business and Economic Review_ 22, 2 (Summer 1991): 203-213..
*   Klopfenstein, B.C., Forecasting Consumer Adoption of Information Technology and Services--Lessons from Home Video Forecasting. _Journal of the American Society for Information Science_ 40/1 (January 1989): 17-26.
*   Korgaonkar, P.K., Lund, D., and Price, B., A Structural-Equations Approach Toward Examination of Store Attitude and Store Patronage Behavior. _Journal of Retailing_ 61/2 (Summer 1985): 39-60.
*   Korgaonkar, P.K., and Smith, A.E., Psychographic and Demographic Correlates of Electronic In-Home Shopping and Banking Service, in _1986 American Marketing Assocation Proceedings_. Shimp, T.A., John, G., Lingren, J.H., Gardner, M.P, Sharma, S., Quelch, J.A., Dillon, W., and Dyer, R.E., (eds.), American Marketing Association, Chicago 1986, pp. 167-169.
*   Korgaonkar, P.K., and Moschis, G.P., Consumer Adoption of Videotext Services. _Journal of Direct Marketing_ 1 (1987): 63-71\.
*   Lee, D.S., Usage Patterns and Sources of Assistance to Personal Computer Users. _MIS Quarterly_ 10, 4 (1986): 313-325.
*   Lumpkin, J.R., Greenberg, B.A., and Goldstucker, J.L., Marketplace Needs of the Elderly: Determinant Attributes and Store Choice. _Journal of Retailing_ 61/2 (Summer 1985): 75-105.
*   Lumpkin, J.R., and Hawes, J.M., Retailing Without Stores: An Examination of Catalog Shoppers. _Journal of Business Research_ 13 (1985): 139-151.
*   Moore, G.C., and Benbasat, I., Development of an Instrument to Measure the Perceptions of Adopting an Information Technology Innovation. _Information Systems Research_ 2, 3 (September 1991): 192-222.
*   Nelson, D.L., Individual Adjustment to Information Driven Technologies: A Critical Review. _MIS Quarterly_ 14, 1 (1990): 79-100.
*   Nickel, G.S., and Pinto, J.N., The Computer Attitude Scale. _Computers in Human Behavior_ 2 (1986): 301-306.
*   Nunnally, J.C., _Psychometric Theory_, McGraw-Hill, New York. 1967, pp. 172-235.
*   O'Reilly and Associates, _Survey of Internet Usage_.1995 (http://www.ora.com/www/info<wbr>/research/users/index.html)[Ed. No longer available at this site]
*   Parasuraman, A, Zeithaml, V.A., and Berry, L.L., SERVQUAL: A Multiple Item Scale for Measuring Consumer Perceptions of Service Quality. _Journal of Retailing_ 64/1 (1988): 12-40.
*   Parasuraman, A, Zeithaml, V.A., and Berry, L.L., Alternative Scales for Measuring Service Quality: A Comparative Assessment Based on Psychometric and Diagnostic Criteria. _Journal of Retailing_ 70/3 (1994a): 201-230.
*   Parasuraman, A, Zeithaml, V.A., and Berry, L.L., Moving Forward in Service Quality Research: Measuring Different Customer-Expectation Levels, Comparing Alternative Scales, and Examining the Performance-Behavioral Intentions Link. _Marketing Science Institute Working Paper_, Report No. 94-114 (1994b).
*   Peterson, R.A., Albaum, G., and Ridgway, N.M., Consumers Who Buy From Direct Sales Companies. _Journal of Retailing_ 65/2 (Summer 1989): 273-286.
*   Quelch, J.A., and Klein, L.R., The Internet and International Marketing_. Sloan Management Review_ (Spring 1996): 60-75.
*   Reynolds, F.D., An Analysis of Catalog Buying Behavior. _Journal of Marketing_ 38 (July 1974): 47-51.
*   Rodkin, D., Marketing to Men - A Manly Sport: Building Loyalty. _Advertising Age_ 62, 16 (April 15, 1991): S1, S12.
*   Shim, S., and Mahoney, M.Y., The Elderly Mail-Order Catalog User of Fashion Products. _Journal of Direct Marketing_ 6/1 (Winter 1992): 49-58.
*   Shim, S., and Drake, M.F., Consumer Intention to Ulitilize Electronic Shopping. _Journal of Direct Marketing_ 4 (Summer 1990): 22-33.
*   Shim, S., and Mahoney, M., Electronic Shoppers and Nonshoppers among Videotext Users_. Journal of Direct Marketing_ (Summer 1991): 29-38\.
*   Smith, R., and Swinyard, W.R., Attitude-Behavior Consistence: The Impact of Product Trial vs. Advertising. _Journal of Marketing Research_ 20 (August 1983): 257-267.
*   Taylor, S., and Todd, P., Assessing IT Usage: The Role of Prior Experience. _MIS Quarterly_ 19(4) (December 1995): 561-570.
*   Thompson, R.L., Higgins, C.A., and Howell, J.M., Influence of Experience on Personal Computer Utilization: Testing a Conceptual Model. _Journal of Management Information Systems_ 11, 1 (Summer 1994): 167-187.
*   Venkatesh, V, and Davis, F.D., A Model of the Antecedents of Perceived Ease of Use: Development and Test_. Decision Sciences_ 27, 3 (Summer 1996): 451-481.
*   Venkatesh, A., and Vitalari, N., A Post-Adoption Analysis of Computing in the Home. _Journal of EconomicPsychology_ 8 (1987): 161-180.
*   Webster, J., Trevino, L.K., and Ryan, L., The Dimensionality and Correlates of Flow in Human Computer Interactions. _Computers in Human Behavior_ 9/4 (1993): 411-426.
*   Zeithaml, V.A., and Gilly, M.C., Characteristics Affecting the Acceptance of Retailing Technologies: A Comparison of Elderly and Nonelderly Consumers. _Journal of Retailing_ 63/1 (Spring 1987): 49-68.
*   Zmud, R.W., Individual Differences and MIS Success: A Review of the Empirical Literature. _Management Science_ 25, 10 (1979): 966-979.

## <a id="appa"></a>Appendix A

Prior Web experience

Do you have access to the Internet ___ Yes ____ No

If yes, do you access the Internet from ___ Home ____ Office ____ Other, specify ____

Before today, have you ever used browsing software (e.g., Mosaic, Netscape or Gopher) to look at information on the Internet ___ Yes ___ No

If Yes, when did you first start using this type of software (please provide month and year) ____ month ____ year

If yes, would you describe your use of browsing software as: (a) done it once, (b) done it a few times, (c) about once a month, (d) a few times a month, (e) once a week, (f) a few times a week, (g) every day.

Have you bought goods or services through the Internet ___ Yes ____ No

If yes, how frequently have you bought goods and services through the Internet: (a) done it once, (b) done it a few times, (c) about once a month, (d) a few times a month, (e) once a week, (f) a few times a week, (g) every day.  

Attitudes towards Computers (seven point scales: never.... every day)

I enjoy interacting with computers

I use computers at home  

Shopping enjoyment (7 point scales: strongly disagree.... strongly agree)

I dislike shopping

I would prefer someone else to do my shopping

I view shopping as an important leisure activity

For me, shopping is a pleasurable activity  

Direct shopping experience (7 point scales: strongly disagree... strongly agree)

I frequently buy products from printed catalogs

I frequently buy products through television shopping channels

I frequently watch infomercials on television  

Income

Household income: (a) less than \$10,000, (b) \$10,000-\$24,999, (c) \$25,000-\$49,999, (d) \$50,000-\$74,999, (e) \$75,000-\$99,000, (f) \$100,000-\$124,999, (g) \$125,000-\$149,999, (h) \$150,000-\$200,000, (I) over \$200,000  

Age

Age. ____ years  

Gender

Sex. ___ Female _____ Male  

Education Level

Highest level of education you have attained: (a) high school, (b) 2 or less years of college, (c) 4 year college degree, (d) graduate degree, (e) Ph.D.  

Employment tenure

Number of years of employment: ____ years  

1 This open-ended questionnaire asked respondents to say what they did and did not like about particular shopping sites they had seen on the Internet and to suggest reasons why the would and would not shop on the Internet. The responses were categorized using the factors in the model. The results of this coding (reported in detail in Jarvenpaa and Todd 1996-1997) suggest that the factors identified through the literature were indeed salient to the respondents in our sample.  

* * *

## <a id="appb"></a>Appendix B: Factor Loadings

<a name="0.1_table09"></a>

<table>

<tbody>

<tr>

<td>

**<u>Construct</u>**</td>

<td>

**<u>Item</u>**</td>

<td>

**<u>Item Loading</u>**</td>

</tr>

<tr>

<td>

**Prior Web Experience**</td>

<td>

*   Do you have access to the Internet and where?
*   Before today, have you ever used browsing software and when did you first start using this type of software?
*   Would you describe your use of browsing software as no experience, up to a few times a month, or at least once a week?

</td>

<td>.736

.888  

.858

</td>

</tr>

<tr>

<td>

**Shopping Enjoyment**</td>

<td>

*   I dislike shopping.
*   I would prefer someone else to do my shopping.
*   I view shopping as an important leisure activity.
*   For me, shopping is a pleasurable activity.

</td>

<td>-.735

-.481

.743

.957

</td>

</tr>

<tr>

<td>

**Direct Shopping Experience**</td>

<td>

*   I frequently buy products through television shopping channels.
*   I frequently watch infomercials on television.

</td>

<td>.790

.603

</td>

</tr>

<tr>

<td>

**Computer Attitude**</td>

<td>

*   I enjoy interacting with computers.
*   I use computers at home (never/every day).

</td>

<td>.911

.487

</td>

</tr>

</tbody>

</table>