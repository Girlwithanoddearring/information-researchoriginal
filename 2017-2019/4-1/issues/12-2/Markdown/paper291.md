#### Vol. 12 No. 2, January 2007

* * *

# Social spaces, casual interactions, meaningful exchanges: 'information ground' characteristics based on the college student experience.

#### [Karen E. Fisher](mailto:fisher@u.washington.edu), [Carol F. Landry](mailto:cflandry@u.washington.edu) and [Charles Naumer](mailto:c.naumer@u.washington.edu)  
The Information School, University of Washington Box 352840, Seattle, Washington, USA

#### Abstract

> **Introduction.** In the late 1990s Fisher (writing as Pettigrew) proposed 'information grounds' to describe social settings in which people share everyday information while attending to a focal activity.  
> **Method.** This study was conducted at a major research university, home to 45,000 students. Data were collected by seventy-two Master of Library and Information Science (MLIS) students as part of an information behaviour class. Trained in interviewing techniques, each MLIS student interviewed ten students in public places, including the campus and the university commercial district. The survey, comprising twenty-seven primarily open-ended questions, was conducted from October 14-21, 2004\. Data were collected from 729 college students and entered, along with extensive field notes, into an in-house Web form.  
> **Analysis.** Qualitative and quantitative analyses were supplemented by mini-reports prepared by the student researchers along with full-team debriefings.  
> **Results.** Using a people, place and information-related trichotomy, characteristics are discussed in terms of how they can be manipulated to optimize information flow in social settings.  
> **Conclusions.** By understanding better the characteristics of 'information grounds' and the interactions among these characteristics, we may be able to develop social spaces in support of information flow and human interaction. Our college student and other studies suggest that 'information grounds' play an intrinsic role in facilitating communication among people and that by building an in-depth 'information grounds' typology, beginning with basic categorical characteristics, we may develop new methods for facilitating information exchange.

## Introduction

Since the 1990s _context_ has been a foundational concept in information behaviour research, a paradigmatic cornerstone for capturing holistic perspectives and nuances. However, in our efforts to understand information behaviour phenomena from the perspectives of different actors or stakeholders (including organizational), the ambient role of _place_ has been subsumed within the broader big picture, meaning little attention has been paid to understanding the specific effects of social settings on information flow. Notwithstanding the work of Chatman (e.g. [1992](#cha92), [1996](#cha96)), whose ethnographic approaches subsumed the effects of setting, the majority of researchers only include shades of setting as ambient factors in the study of overall context ([Leckie and Hopkins 2002](#lec02); [Shill and Tonner 2003](#shi03); [Wiegand 2003](#wie03)). Indeed, the need for greater consideration of place was most recently witnessed by the submission of over forty manuscripts to a special issue of _The Library Quarterly_ on 'The library as place'

Within library and information science, Fisher and colleagues developed a research programme on the role of social settings in everyday information behaviour, known as 'information grounds', which grew from Pettigrew's ([1998](#pet98), [1999](#pet99), [2000](#pet00)) study of everyday information sharing among nurses and the elderly at community foot clinics in Canada. By applying Tuominen and Savolainen's ([1997](#tuo97)) social constructionist approach, she defined 'information grounds' as synergistic 'environment[s] temporarily created when people come together for a singular purpose but from whose behaviour emerges a social atmosphere that fosters the spontaneous and serendipitous sharing of information' ([Pettigrew 1999](#pet99): 811). To this were added the following propositions:

1.  People gather at 'information grounds' for a primary, instrumental purpose other than information sharing.
2.  Information grounds are attended by different social types, most if not all of whom play expected and important, albeit different roles in information flow.
3.  Social interaction is a primary activity at 'information grounds' such that information flow is a by-product.
4.  People engage in formal and informal information sharing, and information flow occurs in many directions.
5.  Information grounds can occur anywhere, in any type of temporal setting and are predicated on the presence of individuals.
6.  People use information obtained at 'information grounds' in alternative ways, and benefit along physical, social, affective and cognitive dimensions.
7.  Many sub-contexts exist within an 'information ground' and are based on people's perspectives and physical factors; together these sub-contexts form a grand context. ([Fisher _et al._ 2004](#fismar04))

As part of the information ground theory building process, these propositions were tested in different field studies using qualitative and/or quantitative approaches across varied populations, including new immigrants in Queens, New York ([Fisher _et al._ 2004](#fismar04)), the general public in King County, Washington ([Fisher _et al._ 2005](#fis05)), migrant Hispanic farm workers ([Fisher _et al._ 2004](#fismar04)), and baby story times in Canadian public libraries ([McKechnie and McKenzie 2004](#mck04)). As discussed by Fisher and Naumer ([2006](#fis06)), these studies supported the information ground propositions. Broad findings included that most everyone has at least one information ground, people's top information grounds are places of worship, the workplace and activity groups (e.g. fitness clubs or playgrounds), some information grounds qualify as _hostage phenomena_, i.e., settings in which one has little choice but to be present, e.g., medical offices, self-service laundries, bus stops, and store queues. While the studies theoretically supported information grounds, substantial work is required to understand the in-depth nature of information grounds, as well as how they may be engineered to facilitate everyday information flow. How, for example, might an existing information ground be made more conducive (or a new information ground created) to foster information flow about sexuality among teenagers or about testicular cancer among men?

In this paper, we draw upon findings from the most intensive information ground study to-date, to identify categorical characteristics, which we share following an overview of the study's population and methodology.

## The everyday life information behaviour of college students

College students have been studied intensively regarding their academic information behaviour; however, little research has addressed them in everyday contexts, aside from Given's ([2002](#giv02)) work on the overlap of academic and everyday information-seeking of mature students, and Jeong's inquiry ([2004](#jeo04)) regarding the influence of churches on the everyday information behaviour of American-Korean graduate students. Our study was guided by the following research questions:

1.  What are students' information grounds?
2.  What types of information do students obtain at their information grounds?
3.  What makes these information grounds opportune for information flow?
4.  How might these information grounds be explained using an emergent typology based on previous information ground studies?

Funded by the Institute of Museum and Library Services, this study was conducted at a major research university, which is home to 45,000 students. Data were collected by seventy-two Master of Library and Information Science (MLIS) students as part of an information behaviour class. Trained in interviewing techniques, each student interviewed ten students in public places, including the campus and the university commercial district. Interviewees were given computer screen cleaning brushes as incentives for participating. The survey, comprising twenty-seven, primarily open-ended, questions, was conducted from October 14-21, 2004\. Data were collected from 729 college students and entered, along with extensive field notes, into an in-house Web form. Qualitative and quantitative analyses were supplemented by mini-reports prepared by the student researchers along with full-team debriefings.

### The students and their information grounds

Of the 729 students surveyed, 55% were female; 72% were undergraduates, 14.7% were Masters students, 6.6% were PhD students, and 6.2% were non-degree seeking students. On average, students attended the University for 23.5 months and their mean age was approximately 24 years.

'Information grounds' were explained to the participants as places people go 'for a particular reason such as to eat, get a haircut, get exercise..., but end up sharing information just because other people are there and you start talking'. When asked if such a place came to mind, students identified 2.25 information grounds each. The most frequent information ground was the _campus_ (22%) coded to include such common areas as hallways, studios, student lounges, rehearsal areas, classrooms, Red Square (large, outdoor gathering area), and study centres (Table 1). As a nineteen-year-old female pointed out '_study groups and meeting with students before and after class_' comprised her information grounds, while a twenty-year-old male claimed the '_football wall where the football players always gather and talk_ was his. The Union Building (TUB) was excluded from the _campus_ category because it encompasses a variety of restaurants, recreational sites and other services, which were coded separately (e.g., 'restaurant at TUB' was coded as _restaurant_). The second most common information ground was restaurants or coffee shops, followed by group social gatherings and the workplace.

<table><caption>

**Table 1: College students' information grounds**</caption>

<tbody>

<tr>

<th rowspan="3">Type of search</th>

</tr>

<tr>

<th colspan="2">Most common information grounds</th>

<th colspan="2">Best information grounds</th>

</tr>

<tr>

<th>No.</th>

<th>%</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>School or campus</td>

<td>372</td>

<td>22.6</td>

<td>105</td>

<td>14.4</td>

</tr>

<tr>

<td>Restaurant, coffee shop, bar</td>

<td>287</td>

<td>17.5</td>

<td>202</td>

<td>27.7</td>

</tr>

<tr>

<td>Group social gatherings</td>

<td>260</td>

<td>15.8</td>

<td>68</td>

<td>9.3</td>

</tr>

<tr>

<td>Workplace</td>

<td>224</td>

<td>13.6</td>

<td>83</td>

<td>11.4</td>

</tr>

<tr>

<td>Shopping</td>

<td>71</td>

<td>4.3</td>

<td>41</td>

<td>5.6</td>

</tr>

<tr>

<td>Church, synagogue or temple (religious)</td>

<td>69</td>

<td>4.2</td>

<td>42</td>

<td>5.8</td>

</tr>

<tr>

<td>Working out: gym, rock climbing, yoga, etc</td>

<td>52</td>

<td>3.2</td>

<td>19</td>

<td>2.6</td>

</tr>

<tr>

<td>Dormitory, home or apartment</td>

<td>52</td>

<td>3.2</td>

<td>40</td>

<td>5.5</td>

</tr>

<tr>

<td>Online</td>

<td>50</td>

<td>3.0</td>

<td>25</td>

<td>3.4</td>

</tr>

<tr>

<td>Hair salon, tatoo parlour</td>

<td>49</td>

<td>3.0</td>

<td>22</td>

<td>3.0</td>

</tr>

<tr>

<td>Library</td>

<td>46</td>

<td>2.8</td>

<td>28</td>

<td>3.8</td>

</tr>

<tr>

<td>Bus stop or bus</td>

<td>40</td>

<td>2.4</td>

<td>9</td>

<td>1.2</td>

</tr>

<tr>

<td>Other</td>

<td>36</td>

<td>2.2</td>

<td>25</td>

<td>3.4</td>

</tr>

<tr>

<td>One-on-One meetings</td>

<td>35</td>

<td>2.1</td>

<td>27</td>

<td>3.7</td>

</tr>

<tr>

<td>Total</td>

<td>1643</td>

<td>100</td>

<td>763</td>

<td>100</td>

</tr>

</tbody>

</table>

Anticipating that students would have more than one information ground, we asked which one was 'best' for encountering information and why (discussed later). Students pointed to restaurants or coffee shops (27.7%) with fewer considering the campus (14.4%) and workplace (11.4%). However, a few identified hair salons, shopping venues, religious organizations, homes, library, gym, online or the bus stop and bus.

## The people-place-information trichotomy

We propose that information grounds be understood using fifteen categorical factors grouped under three headings: people-related, place-related, and information-related (Figure 1). These factors are defined and illustrated using findings from the college student study as well as by drawing upon relevant literature. While past information ground studies generated lists and descriptions of information grounds, in the current investigation we sought to further the information ground research programme by organizing these broad findings into categorical factors, viewed connectedly as a people-place-information trichotomy. The following scheme thus represents a first step at organizing information ground attributes for the purpose of informing system design and optimizing information ground settings.

<figure>

![Figure 1](../p291fig1.gif)

<figcaption>

**Figure 1: Information ground people-place-information trichotomy**</figcaption>

</figure>

### Information ground people characteristics

#### Membership size

The size of an information ground influences the way information is created and exchanged as it affects the degree of intimacy as well as degree of access to broad information types. For more than two-thirds of the college students, the information grounds were typically small to medium in size (Table 2).

<table><caption>

**Table 2: Characteristics of college students' information grounds***  
(*n may differ for each characteristic; counts for 'Place' characteristics excluded  
because they were generated using exploratory open questions)</caption>

<tbody>

<tr>

<th>Information ground  
membership size</th>

<td> </td>

<th>Information ground  
information significance</th>

<td> </td>

</tr>

<tr>

<td>(n=685)</td>

<td> </td>

<td>(n=722)</td>

<td> </td>

</tr>

<tr>

<td>Small (2-10 people)</td>

<td>42.9%</td>

<td>Information very useful</td>

<td>49.9%</td>

</tr>

<tr>

<td>Medium (11-25 people)</td>

<td>25.1%</td>

<td>Information somewhat useful</td>

<td>37.4%</td>

</tr>

<tr>

<td>Large (26-50 people)</td>

<td>13.4%</td>

<td>Can't do without information</td>

<td>6.1%</td>

</tr>

<tr>

<td>Extra large (51+ people)</td>

<td>18%</td>

<td>Information not useful</td>

<td>5.0%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Information not applicable</td>

<td>1.7%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>-------------------</td>

<td>-----</td>

</tr>

<tr>

<th>Membership type</th>

<td> </td>

<td>(n=724)</td>

<td> </td>

</tr>

<tr>

<td>(n=729)</td>

<td> </td>

<td>Make trivial decisions</td>

<td>35.9%</td>

</tr>

<tr>

<td>Open</td>

<td>70.4%</td>

<td>Make big decisions</td>

<td>22.2%</td>

</tr>

<tr>

<td>Closed</td>

<td>29.6%</td>

<td>Make trivial _and_ big decisions</td>

<td>37.6%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Make small decisions</td>

<td>2.5%</td>

</tr>

<tr>

<th>Information ground  
familiarity and relational</th>

<td> </td>

<td>Other</td>

<td>1.8%</td>

</tr>

<tr>

<td>(n=725)</td>

<td> </td>

<th>How information is created and shared</th>

<td> </td>

</tr>

<tr>

<td>Visit information ground daily</td>

<td>53.1%</td>

<td>(n=1186)</td>

<td> </td>

</tr>

<tr>

<td>Visit information ground weekly</td>

<td>39.4%</td>

<td>Talk to non-employee</td>

<td>37.9%</td>

</tr>

<tr>

<td>Visit information ground monthly</td>

<td>7.0%</td>

<td>Talk to employee</td>

<td>22.6%</td>

</tr>

<tr>

<td>Visit information ground yearly</td>

<td>0.4%</td>

<td>Overhear conversation</td>

<td>14.5%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Read posted material</td>

<td>8.7%</td>

</tr>

<tr>

<td>------------------</td>

<td>------</td>

<td>Media</td>

<td>5.8%</td>

</tr>

<tr>

<td>(n=724)</td>

<td> </td>

<td>Observing people</td>

<td>4.2%</td>

</tr>

<tr>

<td>Don't recognize people</td>

<td>10.2%</td>

<td>Read printed material</td>

<td>4.2%</td>

</tr>

<tr>

<td>Recognize people</td>

<td>18.8%</td>

<td>Lecture</td>

<td>1.8%</td>

</tr>

<tr>

<td>Know first names</td>

<td>25.6%</td>

<td>Other</td>

<td>0.3%</td>

</tr>

<tr>

<td>Know people well</td>

<td>45.4%</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>---------------------</td>

<td>-------</td>

<th>Information topics at information grounds</th>

<td> </td>

</tr>

<tr>

<td>(n=954)</td>

<td> </td>

<td>(n=1365)</td>

<td> </td>

</tr>

<tr>

<td>Activity in common</td>

<td>13.4%</td>

<td>Things need to learn more about</td>

<td>28.0%</td>

</tr>

<tr>

<td>Background in common</td>

<td>35.4%</td>

<td>What's happening in the area</td>

<td>19.8%</td>

</tr>

<tr>

<td>Characteristics in common</td>

<td>12.4%</td>

<td>What's happening in the world</td>

<td>18.5%</td>

</tr>

<tr>

<td>Interests in common</td>

<td>38.8%</td>

<td>Who is doing what</td>

<td>17.4%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Things about places</td>

<td>8.8%</td>

</tr>

<tr>

<th>Information ground roles</th>

<td> </td>

<td>Things to apply to daily living</td>

<td>2.0%</td>

</tr>

<tr>

<td>(n=790)</td>

<td> </td>

<td>Things for self-improvement</td>

<td>2.0%</td>

</tr>

<tr>

<td>Customer</td>

<td>25.4%</td>

<td>Other</td>

<td>2.0%</td>

</tr>

<tr>

<td>Student</td>

<td>20.1%</td>

<td>Others' thoughts & opinions</td>

<td>1.6%</td>

</tr>

<tr>

<td>Staff</td>

<td>19.9%</td>

<td>--------------------</td>

<td>-----</td>

</tr>

<tr>

<td>Member</td>

<td>16.2%</td>

<td>(n=1062)</td>

<td> </td>

</tr>

<tr>

<td>Information giver</td>

<td>12.4%</td>

<td>Information (short term)</td>

<td>24.1%</td>

</tr>

<tr>

<td>Resident</td>

<td>4.1%</td>

<td>Knowledge</td>

<td>22.5%</td>

</tr>

<tr>

<td>Other</td>

<td>1.9%</td>

<td>People</td>

<td>15.9%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Issues/Ideas</td>

<td>12.3%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Events</td>

<td>11.1%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Opinion</td>

<td>10.6%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>None</td>

<td>3.1%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Other</td>

<td>0.3%</td>

</tr>

</tbody>

</table>

#### Familiarity and relational dynamics.

Fisher _et al._ ([2005](#fis05)) determined that the social nature of information exchange transcends the sense of satisfaction engendered by the fulfilment of an information need. College students liked making connections with people at their information grounds. As this 25-year old graduate student explained: _'you can say what you like; you can talk and people listen'_, while this 20-year old undergraduate enjoyed being able to _'interact with a lot people at once'_. This would suggest that some type of relationship existed among people present. In their study of social relationships and public places, Morrill and Snow ([2005](#mor05)) established the existence of fleeting and anchored relationships. Fleeting relationships represent one-time events where an emotional connection can be made such as chatting with a seatmate on an airplane. One undergraduate male enjoyed the ephemeral nature of his information grounds as it was not necessary to worry about '_what other people think because you never have to see them again_'. Anchored relationships, however, have evolved over time and are tied to a specific place. For example, a 19-year old male undergraduate said of his favourite coffee shop, '_they don't mind me hanging out for hours and they know my drink_'. The majority of students knew the people present at their information grounds either quite well or at least they knew their first names (Table 2). Furthermore, just over half of the students stopped by their information grounds daily, and approximately 75% of the participants have been going to their information grounds for more than a year.

Intertwined with familiarity is homogeneity. Many students identified homogeneity as being conducive to information exchange because of commonalities of interest, background and situation. Shared bonds (Table 2) create a connection with people that prompted continued participation in information grounds. Remarks such as '_we are all minorities_', and '_common frustrations with work_', are representative of these common links. Others acknowledged heterogeneity as a positive aspect and claimed new ideas and perspectives were gained from their information grounds. Reminiscent of Putnam's ([2000](#put00)) notion of bonding social capital as well as Granovetter's ([1973](#gra73), [1982](#gra82)) strength of weak ties, highly homogenous groups comprise strong ties and serve to reinforce or strengthen existing bonds and largely provide emotional support as opposed to high levels of new information; highly heterogeneous groups, on the other hand, feature weak ties and bring together otherwise unconnected people and new information, especially. In general, we identified four types levels of familiarity: (1) a close friend or strong tie—someone you would call to meet specifically; (2) an acquaintance or weak tie—someone you would say hello to and possibly meet as part of a bigger group, (3) someone recognizable—a person you don't know, but associated with someone else in the common group, and (4) a complete stranger—someone you are meeting or seeing for the first time. Overall, our students indicated that diversity of people and ideas make information grounds a good place for information.

#### Actor roles and social types.

Defined as the distinct function that a person occupies at a place, an actor role is his or her primary reason for being there. Actor roles are significant as they reflect the identity that participants assume and affect their role in information flow. In terms of students' information grounds, seven basic actor roles were identified with the role of customer predominating, followed by student, and staff (Table 2).

Subtly different from actor roles, _social types_ are '_constructs that fall, conceptually, somewhere between an individual, idiosyncratic behaviour on the one side and formal or informal role behaviour on the other side_' ([Lofland and Lofland 1995](#lof95): 106), or an ideal that indicates '_broad but typical social actions... [that] are not intended to convey an actual person but the culmination of exhibited behavior that forms a specific perspective_' ([Pendleton and Chatman 1998](#pen98): 737) A concept that has received little attention in the information behaviour literature (with the notable exception of Chatman), social types play important roles at information grounds because they indicate your position in the information food chain, or, as Chatman ([2000](#cha00): 8) writes in reference to a prison setting: '_how you are classified determines both your access to information and your ability to use it_'. They can, for example, enable unique access to everyday information because they represent a weak tie (e.g., acquaintance of an acquaintance) or provide emotional support and legitimacy as a strong tie (e.g., kith and kin) in social networking terms (c.f., [Granovetter 1973](#gra73), [1982](#gra82); [Pettigrew 1999](#pet99), [2000](#pet99)). When asked who they would turn to find something out, ironically students showed a marked preference for the people with whom they have strong relationship. Readily available social types from the popular literature that are said to significantly affect information flow include Gladwell's ([2000](#gla00)) connectors, mavens, and salesmen; from the information behaviour literature, they include members, mentors, managers, and moguls ([Turner and Fisher, in press](#tur)), monitors and blunters (c.f., [Baker 1996](#bak96); [Baker and Pettigrew 1999](#bak99)), insiders and outsiders ([Chatman 1996](#cha96)), and bitch guards, brides and studs, in Chatman's study of a maximum security prison for women ([Chatman 1999](#cha99), [2000](#cha00); [Pendelton and Chatman 1998](#pen98)). While college students (and informants in past information ground studies) intimated that social types exist, substantial more research is required.

#### Motivation.

The motivation aspect reflects the voluntary or compulsory reasons for going to a place. Not all information ground attendance is voluntary; sometimes it is compelled, thereby creating a _hostage_ information ground setting. While most respondents said they freely attend their information grounds, students relying on the bus for transportation viewed it as a _hostage_ occurrence. Similarly, the classroom and hallways before and after class were considered _hostage_ grounds by some, since class attendance was needed to meet degree requirements. Exemplifying this situation, one 18-year old student claimed, '_Information is forced upon me_'. Closely related to motivation is the difference between engaging in purposive and non-purposive information seeking. Purposive information seeking occurs when someone voluntarily visits an information ground with the purpose of obtaining information. Non-purposive information seeking occurs when someone serendipitously encounters information without prior intent ([Erdelez 2005](#erd05); [Foster and Ford 2003](#fos03)). Hostage information grounds may facilitate this latter type, thus having implications to practice in fields such as public health and social services in which the target population of information dissemination efforts may not be actively seeking information regarding a problem. For example, studies regarding diabetes show people ignore and avoid helpful information; health information dissemination campaigns in hostage information grounds could counteract this behaviour

### Information ground place characteristics

#### Focal activities

The reason(s) people go to an information ground can be as diverse as eating, working out or attending class but does not primarily involve information sharing. Such activities are important for their abilities to bring people together in a social setting. Students identified food and beverage consumption, socializing, obtaining personal services, shopping, research, communication, employment, transportation, physical activities, habitation, or worship as their information ground focal activities. As a female undergraduate noted, '_I can eat and talk to my friends at the same time_'. Multiple activities can occur simultaneously, which may or may not always be conducive to promoting information behaviour

#### Conviviality

A convivial atmosphere often includes food or drink and is associated with good company and a festive mood fostering interaction among people. Conviviality is often associated with other factors related to the atmosphere of an information ground. A 32-year old, non-degree seeking student, described what she liked best about her coffee shop, '_the owner knows a lot and there is an atmosphere that invites topical discussion_'.

#### Creature comforts

Such environmental factors as shelter, comfortable chairs and sofas, lighting, music, bathrooms, scenic qualities such as views or art, and temperature create a relaxing climate for information exchange. These factors were mentioned by college students as being an extremely important part of the general ambience of an information ground.

#### Location and permanence

The location of an information ground is important for how convenient it is to access. This characteristic also influences other factors such as the degree of familiarity with the other people attending the information ground or a sense of comfort from being close to home. Permanence brings into question the notion of information ground life cycles: how are they created and sustained, what causes them to disappear or transform and how are people affected? information grounds may form in a wide variety of physical places and may be tied to them or move to new locations. A few students indicated that their group meets at different coffee shops, while others indicated that the dynamics of their information grounds were closely tied to a specific location. For instance, a student naming a particular bar as his information ground might associate with people frequenting only that place. Thus well established locations may foster different types of information exchange than information grounds that are less well established or transient.

#### Privacy

The level of perceived privacy affects people's preference for particular information grounds. Places that include private areas for talking or tables that far enough away from each other foster conversations that may be personal. Conversely, information grounds might be attractive because they enable eavesdropping, which may contribute to the overall richness of the place.

#### Ambient noise

The ambient noise of an information ground may (or may not) facilitate conversation and thus information behaviour. Locations such as loud restaurants or bars may detract from conversation and information exchange, while quiet background music can make people more relaxed or television programming can spur conversational topics. For example, while a noisy bar might make it difficult for group conversation, its close proximity may encourage one-on-one conversations or the use of other media

### Information ground information characteristics

#### Significance

Regarding the usefulness of obtained information, not all information is considered equal. information grounds may provide information deemed _important_ for decision making, while other information might be considered interesting but not vital. Regardless of how information is learned, the consequences benefit people at different levels of magnitude. Nearly half of the students suggested that the everyday information they received at their information grounds was very useful, while 37% considered it to be somewhat useful (Table 2). Such findings warranted further exploration. What outcomes resulted from learned information? When asked, most students believed the information could be used for making both trivial and big decisions or simply trivial decisions. However, some judged it of sufficient quality to make important decisions, while a small portion considered the information to be good for making small decisions.

#### Frequency discussed

Findings indicate that information grounds can be valued for the frequency with which particular topics occur. For example, some students appreciated information grounds associated with topics concerning a particular academic class, whereas others suggested that topic variety was conducive to new ideas and issues not previously considered.

#### How created and shared

How needs are expressed or recognized, and information created and given can encompass social interactions like conversations or passive observation. information grounds and their inherent social contact thus influence the social construction of information needs and information itself. While particular focal activities drew college students to different information grounds, information was typically shared and acquired in an assortment of ways. When asked how they obtained everyday information, the most prevalent means was "searching it out with conversation"-in the words of a 38 year-old PhD student. Respondents liked to talk to people not employed by the establishment as well as converse with ones who were. Conversely, some students encountered information simply by overhearing others, reading posted material, or using other media. Of the information learned, most students believed that 50-100% of it was attained serendipitously. Also of note but requiring further investigation is the multiplexity of communication modes or the specific varied media. While face-to-face communication was the predominant mode at the students' information grounds (as well as at those of other populations studied), the role and impact of other modes such as online chat, instant messaging, cell phones, notice boards, newspapers, etc., need be considered, especially for whether they negatively affect or are conducive to information flow and social interaction.

#### Topics: personal vs. local vs. world

To understand the types of information acquired at information grounds, students were asked about the specific kinds of things they learned. Responses ranged from topics they needed to know more about or addressed issues occurring locally, globally or in people's lives. A few students, however, picked-up information concerning self-improvement, other people's opinions as well as things that could be applied to daily living (Table 2). A 20-year old male student learned '_how to cook a turkey_', while another discovered '_what kind of person **not** to be_'. Although most information encountered at information grounds was accidental, students indicated that they were also interested in purposeful information. Almost one quarter of respondents pointed out that information related to short term needs such as '_information that would make me laugh or scream_' was worthwhile for one 24-year old graduate student, whereas others stated they would value information relevant to building a knowledge base. For one 21-year undergraduate student, this would encompass '_deeper philosophical knowledge_'. Other areas of concern were people-related, issue-oriented, event-related or information associated with others' perspectives. Curiously, a small segment of participants expressed no interest in obtaining any information, as exemplified by this graduate student, '_[I have] no information agenda, not looking for it_'.

### Relevance of information grounds and the trichotomy of characteristics.

Information grounds are a social construct rooted in an individual's combined perceptions of place, people and information. A place, such as a coffee shop, may be an entirely different type of information ground for different people. A stylist of a hair salon or tattoo parlour artist may value the site for their close relationships and information exchanges with fellow staff; a customer might consider the same shop an information ground for very different reasons.

The proposed categorical factors and trichotomy can help frame our understanding of how people's experiences may differ with regard to social spaces. Moreover, identifying salient factors can point to ways of fostering information flow. We coded students' responses regarding why they liked particular information grounds best using the same people-place-information trichotomy. _Information-related_ (47%) factors were the most important reason for making an information ground a _best place_ according to such participant-supplied criteria as relevance, quality, abundance, availability, and the unanticipated nature of obtaining information. For example, a student said her church provided information that was '_pertinent to life_'. Another said of a café, _'[It] has informal conversation that gives you information you don't expect'_. Referring to the campus, a different student claimed that _'my friends are smart and intelligent people, I don't have stupid friends'_. Second to _information-related_ factors were _place-related_ (28%), as students favoured information grounds based on their familiarity, comfort and convenience. Illustrating this point, one stated that _'I can relax at a friend's house'_. Finally _people-related_ (25%) factors were based upon trustworthiness, diversity, similar beliefs and opinions, common interests and the helpfulness of the people. One person appreciated the bus for its _'very diverse population, cross-cultural, cross-generational'_ riders, while another considered class peers to have _'similar ideas or opinions of information that I am seeking'_.

In response to an open question regarding what would make it easier to share information at their information grounds, students' replies contradicted previous patterns. In these instances they ranked information factors highest (33%) with such suggestions as technical improvements, more resources, the sharing information electronically by way of chat, e-mail or bulletin boards, more structure, lower prices and self-articulation. For the _people_ component (20%), community building by way of game playing and more people were most important, followed more focus on same-age colleagues, and smaller group size were believed to facilitate information sharing. _Place_ factors (15%) focused on room characteristics such as furnishings, better lighting or larger spaces, along with the availability of food and drink, and time investment: one student was concerned with _'not having to worry about being overheard'_ as colleagues were considered too _'nosey'_. Conversely, 25% of students either had no suggestions or considered their information grounds to be just fine for information sharing. From a systems perspective, our grouping of all information ground variables into the three broad groups of people-related, information-related and group-related can be further developed as a typology to optimize information flow at information grounds by providing specific guidelines for information architects.

### Future research

Beyond developing an information ground typology for the purpose of system design and promoting information flow, the following areas require further investigation.

#### Ranges of information grounds

What are the information grounds of specific populations, and under what conditions do they exist? A better understanding of the information grounds of subpopulations may inform information challenges faced by multiple disciplines such as public health, social services, and education.

#### The life cycle of information grounds

An information ground may appear briefly and disband as with a bus stop, or it might attain a permanence that is evident with long established bars and coffee houses. Same interests or shared characteristics are commonalities that help to sustain information grounds. Additionally, these commonalities help to create bonds among members that encourage social interaction beyond the confines of their information grounds. The life cycle of information grounds merits study from the perspective of how they emerge, how they are sustained, and how they might cease to exist. Consideration of the information ground life cycle can have considerable implications for improving people's lives such as when people are laid off from their jobs—thus also losing their primary information ground—and are in need of temporary or new information grounds. Case study research on can play an important role in better understanding information ground life cycles.

#### Social exchange of information

While the current study expanded our knowledge of information grounds, research is needed to understand how information is socially constructed among different actors, as well as how people's perceptions and participation change over time. Our findings suggest that information grounds are fertile territory for non-purposive information behaviour (or information encountering), and facilitate the social construction of information needs and, thus, information itself. Field research is needed on how people re-conceptualize their everyday life situations and redefine their information needs within social settings. The social construction of information need and non-purposive information behaviour have implications for human service outreach efforts that are concerned with informing the public and changing behavioural patterns that may be detrimental to people's health and well-being.

#### Affective factors

Additionally, our findings suggest that information grounds encompass a strong affective component where many information ground factors elicit emotional responses. Affect has been shown by Nahl ( [2005](#nah05)) and others to have significant impact on information behaviour. Information ground factors that may be tied to emotional response include lighting, music, and mood of the place, including feelings or relaxation and safety. Emotional response may also be associated with or attached to the type of people frequenting the information ground, such as degree of understanding, supportiveness, familiarity, or shared interests.

#### Nomenclature

Scholarly debate is also needed on how we to distinguish such related (and, currently confounded) terms as space, place, setting, context, etc. To date, such discussions have been limited to cognate fields such as human geography. In-depth discussion is necessary by the information behaviour community in order to understand and operationalize these concepts more accurately.

For information grounds, by better understanding their characteristics and how those characteristics interact, we may better develop social spaces in support of information flow and human interaction. Our college student and other studies suggest that information grounds play an intrinsic role in facilitating communication among people and that, by building an in-depth information ground typology, beginning with basic categorical characteristics, we may develop new methods for facilitating information exchange.

## Acknowledgements

_This research was supported by a grant from the U.S. [Institute of Museum and Library Services](http://www.imls.gov/) ._

## References

*   <a id="alm98"></a>Almog, O. (1998). [The problem of social type: a review.](http://www.Webcitation.org/5KLXfnBZM) _Electronic Journal of Sociology,_ , **3**(4), Retrieved 14 November, 2006 from http://www.sociology.org/content/vol003.004/almog.html
*   <a id="bak96"></a>Baker, L.M. (1996). A study of the nature of information needed by women with multiple sclerosis. _Library and Information Science Research,_ **18**(1), 67-81.
*   <a id="bak99"></a>Baker, L.M., & Pettigrew, K. E. (1999). Theories for practitioners: two frameworks for studying consumer health information-seeking behaviour. _Bulletin of the Medical Library Association_, **87**(4), 444-50.
*   <a id="bus06"></a>Buschman, J.E. & Leckie, G.J. (Eds.). (2006). _The library as place: history, community, and culture._ Westport, CT: Libraries Unlimited.
*   <a id="cha92"></a>Chatman, E.A. (1992). _The information world of retired women_. New York, NY: Greenwood Press.
*   <a id="cha96"></a>Chatman, E.A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science and Technology_, **47**(3), 207-217.
*   <a id="cha99"></a>Chatman, E.A. (1999). A theory of life in the round. _Journal of the American Society for Information Science and Technology_. **50**(3), 207-217.
*   <a id="cha00"></a>Chatman, E.A. (2000). Framing social life in theory and research. _The New Review of Information Behaviour Research_, **1**, 3-17.
*   <a id="cre04"></a>Creswell, T. (2004). _Place: a short introduction._. Malden, MA: Blackwell.
*   <a id="erd05"></a>Erdelez, S. (2005). Information encountering. In K.E. Fisher, S. Erdelez & L. McKechnie (Eds.). _Theories of information behavior._ (pp. 179-184). Medford, NJ: Information Today.
*   <a id="fel96"></a>Feld, S. & Basso, K.H. (Eds.) (1996). _Senses of place_. Santa Fe, NM: School of American Research Press.
*   <a id="fisdur04"></a>Fisher, K.E., Durrance, J.C., & Hinton, M.B. (2004). Information grounds and the use of need-based services by immigrants in Queens, NY: a context-based, outcome evaluation approach._Journal of the American Society for Information Science & Technology_, **55**(8), 754-766.
*   <a id="fismar04"></a>Fisher, K.E., Marcoux, E., Miller, L.S., Sanchez, A. & Cunningham, E.R. (2004). [Information behaviour of migrant Hispanic farm workers and their families in the Pacific Northwest.](http://www.Webcitation.org/5KHI7M8sx) _Information Research_. **10**(1), paper 199\. Retrieved January 6, 2006 from http://informationr.net/ir/10-1/paper199.html
*   <a id="fis06"></a>Fisher, K.E. & Naumer, C.M. (2006). Information grounds: theoretical basis and empirical findings on information flow in social settings. In A. Spink & C. Cole (Eds.),_New directions in human information behavior_, (pp. 93-111). Amsterdam: Kluwer.
*   <a id="fis05"></a>Fisher, K.E, Naumer, C., Durrance, J., Stromski, L. & Christiansen, T. (2005). [Something old, something new: preliminary findings from an exploratory study about people's information habits and information grounds.](http://www.Webcitation.org/5KHIbpXLI) _Information Research,_ **10**(2), paper 223\. Retrieved 6 January, 2006 from http://informationr.net/ir/10-2/paper223.html
*   <a id="kat92"></a>Fisher, K.E., Saxton, M.L., Edwards, P.M., & Mai, J-E. (In Press). Seattle Public Library as place: reconceptualizing space, community, and information at the central library. In J.E. Buschman & G.J. Leckie (Eds.). _The library as place_. Westport, CT: Libraries Unlimited.
*   <a id="fos03"></a>Foster, A. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation,_ **59**(3), 321-340.
*   <a id="giv02"></a>Given, L.M. (2002). The academic and the everyday: investigating the overlap in mature undergraduates' information-seeking behaviours. _Library & Information Science Research,_ **24**(1), 17-19.
*   <a id="gla00"></a>Gladwell, M. (2000). _The tipping point: how little things can make a big difference._ Boston, MA: Little, Brown.
*   <a id="gof63"></a>Goffman, E. (1963). _Behaviour in public places: notes on the social organization of gatherings._ New York, NY: The Free Press.
*   <a id="gof67"></a>Goffman, E. (1967). _Interaction ritual: essays on face-to-face behavior._ New York, NY: Pantheon Books.
*   <a id="gra73"></a>Granovetter, M.S (1973). The strength of weak ties. _American Journal of Sociology,_ **78**(6), 1360-1380.
*   <a id="gra82"></a>Granovetter, M.S. (1982). The strength of weak ties: a network theory revisited, In Peter V. Marsden and Nan Lin, (Eds.) _Social structure and network analysis,_ (pp. 105-130). Beverly Hills, CA: Sage Publications.
*   <a id="hei03"></a>Heinström, J. (2003). [Five personality dimensions and their influence on information behaviour](http://www.Webcitation.org/5KLUg1rqR). _Information Research_ **9**(1), paper 165\. Retrieved 12 June, 2004 from http://informationr.net/ir/9-1/paper165.html
*   <a id="jeo04"></a>Jeong, W. (2004). Unbreakable ethnic bonds: information-seeking behaviour of Korean graduate students in the United States. _Library & Information Science Research_, **26**(3), 384-400.
*   <a id="kla58"></a>Klapp, O. (1958). Social types. _American Sociological Review_, **23**(6), 673-681.
*   <a id="lec02"></a>Leckie, G. J., & Hopkins, J. (2002). The public place of central libraries: findings from Toronto and Vancouver._Library Quarterly_, **72**(3), 326-372.
*   <a id="lip97"></a>Lippard, L. (1997). _Lure of the local: senses of place in a multicentered society._ New York, NY: The New Press.
*   <a id="lof95"></a>Lofland, J. & Lofland, L.H. (1995). _Analyzing social settings: a guide to qualitative observation and analysis._ Belmont, CA: Wadsworth.
*   <a id="mck04"></a>McKechnie, L., & McKenzie, P. (2004). _The young child/adult caregiver storytime program as information ground_. Paper presented at the Library Research Seminar III, Kansas City, KA, October 15, 2004.
*   <a id="mor05"></a>Morrill, C. & Snow, D.A. (2005). The study of personal relationships in public places. In C. Morrill, D.A. Snow & C.H. White (Eds.). _Together alone: personal relationships in public places._ (pp. 1-22). Berkeley, CA: University of California Press.
*   <a id="nah05"></a>Nahl, D. (2005). Affective load. In K.E. Fisher, S. Erdelez & L. McKechnie (Eds.). _Theories of information behavior_, (pp. 39-43). Medford, NJ: Information Today.
*   <a id="old99"></a>Oldenburg, R. (1999). _The great good place: cafes, coffee shops, bookstores, bars, hair salons, and other hangouts at the heart of a community._ New York, NY: Marlowe.
*   <a id="pen98"></a>Pendleton, V.E.M. & Chatman, E.A. (1998). Small world lives: implications for the public library. _Library Trends,_ **46**(4), 732-751.
*   <a id="pet98"></a>Pettigrew, K.E. (1998). _[The role of community health nurses in providing information and referral to the elderly: a study based on social network theory](http://www.webcitation.org/5KORcl3km)_. Unpublished doctoral dissertation, University of Ontario, London, Ontario, Canada. Retrieved 14 November, 2006 from http://projects.ischool.washington.edu/fisher/dissertation/TP&TOC.pdf
*   <a id="pet99"></a>Pettigrew, K.E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behavior among attendees at community clinics. _Information Processing & Management,_ **3**(6), 801-817.
*   <a id="pet00"></a>Pettigrew, K.E. (2000). Lay information provision in community settings: how community health nurses disseminate human services information to the elderly. _Library Quarterly,_ **70**(1), 47-85.
*   <a id="put00"></a>Putnam, R.D. (2000). _Bowling alone: the collapse and revival of American community._ New York, NY: Simon & Schuster.
*   <a id="rel76"></a>Relph, E. (1976). _Place and placelessness._ London: Pion.
*   <a id="shi03"></a>Shill, H.B. & Tonner, S. (2003). Creating a better place: physical improvements in academic libraries, 1995-2002\. _College & Research Libraries,_ **64**(6), 431-466.
*   <a id="tuo97"></a>Tuominen, K. & Savolainen, R (1997). A social constructionist approach to the study of information use as discursive action. In P.Vakkari, R. Savolainen, & B. Dervin (Eds.). _Information seeking in context. Proceedings of an international conference on research in information needs, seeking and use in different contexts, 14-16 August 1996, Tampere, Finland._ (pp. 81-96). London: Taylor Graham.
*   <a id="tur"></a>Turner, T.C. & Fisher, K.E. (in press). Social types in technical newsgroups: implications for information flow. _International Journal of Communications, Law and Policy._
*   <a id="wie03"></a>Wiegand, W.A. (2003). To reposition a research agenda: what American studies can teach the LIS community about the library in the life of the user. _Library Quarterly,_ **73**(4), 369-382.