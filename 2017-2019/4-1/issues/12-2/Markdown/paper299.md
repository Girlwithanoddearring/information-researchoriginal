#### Vol. 12 No. 2, January 2007

* * *

# Information professionals in Brazil: core competencies and professional development

#### [Flávia Ferreira](mailto:flaviaccfc@yahoo.com.br), [Joanice Nascimento Santos](mailto:misticanice@yahoo.com.br), [Lucyana Nascimento](mailto:lucyana.n@ibest.com.br), [Ricardo Sodré Andrade](mailto:ricsodre_br@yahoo.com.br), [Susane Barros](mailto:susanesb@ufba.br), [Jussara Borges](mailto:jussaraborges2003@yahoo.com.br), [Helena Pereira da Silva](mailto:helenaps@ufba.br), [Othon Jambeiro](mailto:othon@ufba.br)  
Universidade Federal da Bahia,
Instituto de Ciências da Informação,
Av Reitor Miguel Calmon,
s/n Vale do Canela,
40110-100 Salvador- BA, Brazil

[Fábio Ferreira](mailto:fabioferreira@mail.utexas.edu)  
The University of Texas at Austin,
College of Communication 1 University Station A0900,
Austin, TX 78712, USA  

[Bethany Lynn Letalien](mailto:letalieb@mail.utexas.edu)  
The University of Texas at Austin,
School of Information 1 University Station D7000,
Austin, TX 78712, USA

#### Abstract

> **Introduction**. We discuss the concept of core competencies applied to policies for teaching and training information professionals, particularly librarians.  
> **Method.** Sixty graduates of the Institute were employed as information professionals. These sixty were asked to attribute degrees of importance to specific items associated with knowledge and skills that, within the scope of this research, were considered core competencies for meeting the demands of their jobs. Participants were also asked to cite knowledge they acquired in school and knowledge they use in exercising their profession, the skills that they consider necessary but that they did not gain in school, and the difficulties they encounter in exercising their profession and for which they were not sufficiently well prepared.  
> **Analysis.** Both quantitative and qualitative data analyses were performed. The data were tabulated using Access and several reports and cross-tabulations were generated.  
> **Results.** The results suggest a gulf between knowledge and skills acquired in library school and those that are required by the job market. In particular, participants lacked the skills they needed to work with information and communication technologies.  
> **Conclusion.** The concept of core competencies is increasingly taken into account by the productive sector of the economy. The educational system ought to keep up with this change. The empirical research described shows that there is a need to establish advanced and modern policies for the education of librarians, participants in the market for information professionals.

## Introduction

Contemporary society is often called a knowledge society. Despite the use of countless related terms, it seems that this general label is well accepted, due to the perception that knowledge has taken on increasing importance in socioeconomic processes. Drucker ([1994](#drucker94): 16) characterizes this society as a society of organizations. According to him, to survive, an individual depends on his/her association with others, based on goals defined by organizations and on the knowledge that these organizations accumulate collectively to reach their objectives. In this way, individuals and organizations coexist increasingly in an interdependent relationship.

An organization—a simple association of individuals, a conglomerate, or a holding—with specific missions and objectives collects and manipulates strategic information. It also builds knowledge, using this knowledge to establish networks of relationships and production networks. These networks are characteristic of contemporary society and their importance grows daily, strengthened through the use of information and communication technologies.

Besides increasing the potential of networks, information and communication technologies, since they allow simultaneous interactivity and information exchange and use, stimulate organizations to rethink traditional business formats and to create new ones. In this way, they stimulate the acceptance of the notion that associations of individuals and groups of all kinds should become information and knowledge organizations. In short, people are becoming increasingly aware of the importance of information and knowledge as basic building blocks for their organizations.

One consequence of these changes has been an alteration in attitude toward information processes. Now, the collection, organization, recording and distribution of information, traditionally within the purview of librarianship and archival studies, are now skills demanded from any professional in an organization, particularly regarding digital information. Familiarity with new processes and technologies on the part of all participants in an organization is a _sine qua non_ for generating the knowledge necessary for the survival and development of the organization.

Information and knowledge become, therefore, an input in the provision of any product or service. When there is a change in the sources (inputs), there are, in consequence, changes in the society's organization and in work relationships. In this sense, information and knowledge are changing organizations and the world of work. As Arruda _et al._ ([2000](#arrudaetal00): 15) put it, these changes bring to the fore connections between work and education.

Specifically in the field of information, these considerations lead to a reflection on policies regarding the education of information professionals. Since today people of different professional backgrounds work in the field of information, it is difficult to determine where educational policies should be applied. But we must start somewhere; perhaps a good place is the schools that teach librarianship and archival science, from which the teaching of new informational skills could later be expanded.

The present text suggests some of the areas in which current teaching practices in librarianship and closely related fields are increasingly inadequate for the formation of information professionals. The authors point to skills that, if these were more commonly taught in library, archival, or information studies courses, could help bridge the gap between the skills acquired in classrooms and those required by labor markets. Attention to these skills may help librarians, archivists, records managers and others like them to maintain their significance to an ever-changing and knowledge-intensive world, along with the professional prestige and respect that these overwhelmingly female professionals, in Brazil and elsewhere, have long fought to gain.

It is very important to note that in Brazil, the profession of librarianship is learned in undergraduate courses, such as Journalism, Economy, Pharmacy, Psychology or Medicine, among others. There is also a regulatory act from the National Congress that gives librarians the privilege of being the only people allowed to be responsible for libraries. A professional corporatist institution called the Conselho Nacional de Biblioteconomia (National Council of Librarianship), linked to the central government and managed exclusively for librarians, enforces the regulation to avoid the invasion of their labour force by other professionals. By Brazilian law _illegal exercising of a profession_ is a crime punishable by a prison sentence.

Below, we first discuss concept of core competencies. Next, we present an empirical study, in which a survey was administered to recent graduates of the Institute of Information Science, of the Federal University of Bahia, Brazil, between March and October of 2004\. The students had graduated between 2000 and 2003\. The interviewees were asked to attribute a degree of importance to specific items relative to knowledge and skills that were considered to be components of the core competencies necessary to their work. Participants were also asked to cite the kinds of knowledge they had acquired over the course of their undergraduate careers and those that they used in exercising their profession; the skills they considered necessary and which they had not acquired in school; and the challenges they faced in exercising their profession for which they had not been sufficiently well prepared. The theoretical basis, methodology and results are likewise discussed.

## Organizations, competencies and work

Globalization, fast technological development and new organizational paradigms have had a profound impact on the workplace in many countries and all of them are in search of productivity and competitiveness ([Vargas Zúñiga 2000](#vargaszuniga00): 1). This new context restructures organizations, eliminating intermediate levels and promising to promote workers' empowerment, workers' participation in decision making and interaction among the several organizational hierarchical levels. It reorganizes production with more flexible processes, prompts constant innovation, increases the speed at which products become obsolete and brings special concerns to quality management and services. The standardization of production techniques, meanwhile, forces companies to differentiate themselves in terms of customer service, quality and distribution.

To achieve differentiation in the provision of services, organizations should develop relationships in which they understand their customers' needs and expectations. In this sense, maintaining direct and intense relationships with clients demands the presence of professionals who understand users' desires and needs ([Zarifian 1998b](#zarifian98b)).

Furthermore, the meaning of work has undergone significant changes. The restricted workstation concept gave way to the idea of occupation, understood as an activity that is not limited to a group of specific operational tasks or functions. Occupation should be conceptualized broadly, evoking extensive knowledge of an area, with the possibility of horizontal development of an individual's career.

Those changes, as well as the need to increase productivity, innovate constantly and maintain competitiveness, demand that organizations and their employees develop what we are calling here core competencies. Basically, core competencies are related to knowledge, communication skills and the development of relationships, which, according to Vargas Zúñiga ([2000](#vargaszuniga00): 13), constitute the leitmotif of the current programmes of study.

The concept of core competencies, elabourated by Mertens (1974, cited by [Vargas Zúñiga 2000](#vargaszuniga00)), involves: knowledge, capacities and abilities, which mean much more than simply on-the-job learning or training. Core competencies are understood as: a proper attitude and capacity to exercise a wide number of functions or occupy various positions, even simultaneously, which requires flexibility. Additionally, a proper attitude to handle constant change in the environment is expected and this also requires constant learning. In this way, the workers' qualities, such as discipline, punctuality and creativity come to be considered their core competencies. Knowledge, team work, communication skills, the ability to work with information and technology, systematic thought, the ability to generate creative solutions to problems, self-esteem, self-confidence and tolerance for frustration are some of the elements of the concept of core competencies ([Arruda _et al._ 2000](#arrudaetal00); [Vargas Zúñiga 2001](#vargaszuniga01)).

The notion of core competencies is not restricted to the performance of employees. Organizations themselves have begun to apply these concepts as well, in attempts to develop dynamic and visionary leadership from top-level administrators, the courage to take risks, trust in employees' competencies, shared visions, clear missions and objectives, the execution of plans, and commitment from top-level managers to change and innovation.

With these characteristics, organizations begin to implement a different style of administration that demands new competencies on the part of workers. Organizations that value the following can be considered _learning organizations_: versatility of their workers; horizontal relationships rather than vertical, hierarchical ones; the reduction of barriers to integration between lower and upper levels; workers' participation and responsibility for their own work (empowerment); and innovation as opposed to repetition (Machado 1996, cited by [Arruda _et al._ 2000](#arrudaetal00); [Vargas Zúñiga 2000](#vargaszuniga00)).

For Prahalad ([1999](#prahalad99): 44), the greatest challenge faced by organizations is to develop a solid set of core competencies, considering people as part of a team, emphasizing speed and constant re-evaluation of the organizations' business models. Organizations should develop into true portfolios of competencies, combining technologies, both hard and soft, collective learning, multi-level and multi-functional capabilities and the capacity to interact with the environment beyond their borders. Prahalad reinforces the arguments of Vargas Zúñiga ([2001](#vargaszuniga01)), affirming that these competencies should be developed in the face of a competitive environment constituted by eight fundamental changes: 1) globalization, which helps to form a competitive market economy; 2) deregulation and privatization, which have implications at the macro and microeconomic levels; 3) market volatility; 4) business mergers and the convergence of technologies; 5) difficulty in identifying the sources of competition; 6) standardization of production activities; 7) less intermediation; 8) ecological concerns, which also requires specific strategies.

Core competencies appear to be a broad combination of individual and organizational competencies and they involve, according to Santos (in a seminar presentation, 2000), knowledge, abilities and attitudes that result in measurable behaviors. Zarifian ([2001](#zarifian01)) describes the notion of competencies as the search of accuracy, quick response, language skills and technology literacy. The development of these abilities, attitudes and competencies is essential to be part of a competitive market.

Zarifian ([1998a](#zarifian98a)) presents two elements that are associated with core competencies: responsibility and initiative. For him, competence means holding responsibility for the success of an individual or a team in a work context. If responsibility means to be accountable for the consequences of a decision, then initiative involves the ability to act assertively when necessary. A person will be competent when she or he takes the initiative to solve a problem when challenged to do so and takes the responsibility for her or his decisions. Asking for help and cooperation on a particular task and having the ability to use personal social networks are important elements in defining an individual as competent. Teamwork is always also individual, since individuals comprise teams and mobilize them to act.

It is clear that improved education and training are necessary to assist individuals in developing the competencies they will need on the job. The concept of core competencies involves knowledge, which should be acquired through formal education and training; skills or abilities, which are acquired through practice; and attitudes, which involve emotional and social aspects. A 1999 study of the Serviço Nacional do Comércio (SENAC) that sought to identify new occupations for the twenty-first century concluded that workers should demonstrate at least creativity, versatility and initiative in order to find employment. SENAC is an organization associated with the Brazilian Commerce National Union and it devoted to improving Brazil's workforce for the performance of commercial and service activities ([Serviço Nacional do Comércio 1999](#senac99)).

## Information Professionals and core competencies

The professional profile demanded during these new times of intense use of information and communication technologies is based on three elements: knowledge, skills or abilities and attitudes. Such a profile is not restricted to the new information professions, nor is it restricted to the traditional ones, such as librarianship; rather, it is related to all kind of occupations. The demand for professionals with those new profiles is, nonetheless, stronger in the information industry than in some other industries. With the increased value given to information, pushed by information and communication technologies, professionals with degrees in areas other than traditional Librarianship and Documentation are acquiring the competencies to work with information processes, also adopting and appropriating the language of information professionals. Meanwhile, other professions that have long had information as central to their work are increasingly recognizing the value of information and information work.

At the same time, the traditional academic training of librarians has changed little in Brazil, concentrating only on those environments that have long been typical of such professionals: libraries and archives, considered, as they have long been, an area restricted to information professionals. However, as Targino ([2000](#targino00)) observes, the expression _information professional_ reached wide currency only in the 1990s, circulated by scholars such as Mason ([1990](#mason90)) and Ponjuan ([1993](#ponjuan93), [1995](#ponjuan95)). The use of the expression was later reinforced by the International Federation of Information and Documentation, which created a study group dedicated to the Modern Information Professional. _Modern information professional_ characterizes the emphasis given to information and the access to information well and it would be a good indicator of what can be considered as the fourth sector of the economy; the information industry ([Targino 2000](#targino00): 1).

The information industry includes a vast range of activities that embrace the creation, production, distribution, recovery and use of information. It also incorporates several functions for which new competencies are necessary, but that do not depend on a specific or formal college education, especially because universities are still adjusting to the new demands. Tarapanoff ([1999](#tarapanoff99)) and Targino ([2000](#targino00)) illustrate some of the entities and services activities of the information industry: (1) content providers, which involves, among others, print and electronic editorial business; multimedia; media producers; conventional, digital and virtual information services; information agencies; and interactive electronic services; (2) data, information, communication and distribution services, encompassing telecommunication networks, subscription services of information and entertainment by cable and satellite, radio, television, newspapers, magazines and educational systems; and (3) data and information processing services, including software industries and computer and telecommunications equipment.

Teixeira Filho ([1999](#teixeirafilho99)) states that occupations based on information grow endlessly. Le Coadic ([1996](#lecoadic96)) notes that this growth and diffusion of professions related to information does not allow for the development of a precise definition of _information professional._ This raises the question of who is an information professional. Since the Internet was introduced decades ago and the World Wide Web more than ten years ago, this question remains with no definitive answer, particularly among librarians.

Witter (1999, cited by Targino ([2000](#targino00)) conducted a survey of professionals enrolled in a specialization course on strategic management, finding the following majors among those surveyed: librarianship, law, literature, data processing, business, education, zoology, economics and civil engineering. The objective of the study was to learn what the students understood an information professional to be. Thirty participants provided some ninety-one different answers. The participants demonstrated a clear perception of the three elements knowledge, skills and attitudes and they defined information professionals as those who work with information, provide information services, remain up-to-date, care about users' needs and satisfaction, work with a variety of media, are researchers, constitute links between users and information, are information facilitators, are competent, manage information, are critical, are patient, are understanding, are dynamic, are kind and polite, act responsibly, are active, are sociable and so on ([Targino 2000](#targino00): 5). This list demonstrates how even a small number of people can generate many different understandings of what is purportedly the same concept.

Guimarães (1997; 1998, cited by [Targino, 2000](#targino00)) believes that it is essential for modern information professionals to have management skills, analytical skills and creativity, as well as to keep themselves up-to-date. She notes the importance of constant education, throughout undergraduate education and afterwards. Constant updating is considered a factor of competitiveness, meaning, according to Franco ([1997](#franco97); 9), that professional competence involves _'new forms of thinking, new forms of interacting and new forms of living'_. Thus, research and internships at the undergraduate level are beneficial to the future of the professional.

In the case of librarians, who, traditionally, were considered the principal professionals in the field of information, it is necessary to face this new situation, in which they do not have exclusive dominance, in a very positive and confident manner. Being positive means seeking out fast and efficient ways of acquiring necessary core competencies. This understanding appears to permeate the thought of some authors who work with the question of the education of librarians, as well as that of other professions.

Librarians have no choice but to be immersed in the environment of globalization and new technologies. As Targino ([2000](#targino00): 9) puts it, in parallel with the traditional responsibilities of information depositories, libraries and archives have traditionally used models in which _just-in-case_ and manual browsing approaches prevail. However, these institutions are developing new management practices, such as _just-in-time_ and virtual browsing, focusing on accessibility and sharing of information with other information units. With this change, librarians and archivists should be attentive to the shift from the simple collection of information to the provision of access to information. With virtual networks, this is no longer simply a Utopia, but an increasingly concrete reality. It is necessary, therefore, to act with creativity, dynamism and interdisciplinary vision, developing, aside from clear proficiency in the use of information and communication technologies, abilities to synthesize information and knowledge regarding information management and policies.

Policies regarding the education and training of information professionals must take core competencies into consideration in order to allow those professionals to act in a wide array of new information spaces. It is equally important that, independent of the _invasion_ of the information sector by professionals from the most diverse of origins, librarians have an important role to carry out in that field. Even in their traditional spaces—libraries and archives— a new kind of professional will be necessary. Furthermore, the fact that libraries and archives are considered to constitute an extension of the educational system, the importance of those professionals becomes particularly clear. It is vital to recall that this society can be considered not only an information one, but also as an educational society ([International Commission... 1996](#unesco96)).

## Methods

The methodology used in this survey consisted of four steps:

First, construction of the theoretical framework to define core competencies, which comprised of three things: knowledge, skills and attitudes. Although the research instrument addressed attitudes, in this particular work, only knowledge and skills are considered. The following elements, suggested by the literature, were explored:

1.  Knowledge of:
    1.  Theoretical themes that are relevant to and growing in the field of information.
    2.  Bibliometrics.
    3.  Scientometrics.
    4.  Trends in the field of information.
    5.  Relationships between the field of information and other fields.
    6.  Societal changes and trends.
    7.  Development of information and communication technologies.
    8.  The provision of new information products and services through the use of information and communication technologies.
    9.  Information management.
    10.  Knowledge management.
    11.  Languages other than Portuguese.
2.  Skills:
    1.  To communicate with superiors, co-workers and users.
    2.  To maintain good relations with superiors, co-workers, and users.
    3.  To develop and participate in team work.
    4.  To identify problems.
    5.  To provide solutions for problems.
    6.  Creativity.
    7.  To make good use of intuition at work.
    8.  To search for innovation and to be a change agent.
    9.  Flexibility in the face of change and lifelong learning.
    10.  General management.
    11.  Leadership.
3.  Attitudes were not part of the study because of the extreme difficulty associatied with measuring attitudes and because we considered one telephone call and one face-to-face interview for participant to be insufficient for us to be able to make any important conclusions about attitudes. To study attitudes in this particular case, an in-depth, ethnographic approach would have been more appropriate and such an approach was neither our intention nor practical at the time.

Secondly, the undergraduate programme in librarianship and archival studies supplied a list with the telephones numbers of the students who had graduated between 2000 and 2003\. There were a total of 199 names. The graduates were assigned to groups according to their semester of graduation and researchers were sent to interview specific groups.

Thirdly, we first attempted to contact each of the graduates by telephone and were able to locate and contact 126 of them. The others demonstrated unwillingness to be contacted by the interviewers or had moved to unknown locations. The research objectives were explained to each individual who was contacted. They were asked to take part in the survey by answering questions regarding their socioeconomic status and about their membership in the workforce, which allowed us to know if they were employed as information professionals. From them, sixty were so employed (50 women and 10 men). As the research objective was to know their acquisition and application of professional knowledge and skills, only those who were working in the field were then asked to participate in a personal interview.

Finally, we performed face-to-face interviews. Besides a number of questions that are not addressed in this paper, we asked:

1.  What knowledge that you gained in your undergraduate career do you use in your activities?
2.  What kind of skills do you consider necessary but are not gained through your undergraduate programme?
3.  What difficulties have you encountered over the course of your professional career for which you have not felt prepared?

For each question, the participants were asked to attribute a degree of importance to each item.

## Results

As described in the Introduction, the objective of the research is to contribute to the wider discussion of the notion of core competencies, which encompasses three other concepts: knowledge, skills and attitudes. The analysis that follows takes into account the parameters discussed above.

Regarding knowledge acquired through academic education and training and the use of this knowledge in the exercise of professions (Table 1, below), the items that were most mentioned, cataloguing, classification, indexing, reveal a perfect fit of librarians to the core of their profession. At the same time, however, these items lead us to believe that the curriculum to which these individuals were exposed emphasizes traditional techniques and opens few doors for acquiring skills to match newer, more contemporary needs.

This is in complete contradiction to Targino ([2000](#targino00)), for instance, who affirms that the formation of information professionals must have as a priority the stimulation of a wide and critical vision of the world, going against the grain of the traditional approach, which focuses on the specialized aspects of the profession. Additionally, Araripe ([1998](#araripe98)) points to the necessity of redesigning information professions, considering information and communication technologies and the changing context of their field.

<table><caption>

**Table 1: Main kinds of knowledge acquired in college and used in professional activities**</caption>

<tbody>

<tr>

<th>Kind of Knowledge</th>

<th>Frequency</th>

</tr>

<tr>

<td>Cataloguing</td>

<td>34</td>

</tr>

<tr>

<td>Classification</td>

<td>31</td>

</tr>

<tr>

<td>Indexing</td>

<td>26</td>

</tr>

<tr>

<td>Technical</td>

<td>24</td>

</tr>

<tr>

<td>Reference services</td>

<td>13</td>

</tr>

<tr>

<td>Standards</td>

<td>10</td>

</tr>

<tr>

<td>Information dissemination</td>

<td>8</td>

</tr>

<tr>

<td>Acquisitions</td>

<td>5</td>

</tr>

<tr>

<td>Planning</td>

<td>5</td>

</tr>

<tr>

<td>User studies</td>

<td>4</td>

</tr>

<tr>

<td>Organization of collections</td>

<td>4</td>

</tr>

<tr>

<td>Automation</td>

<td>3</td>

</tr>

<tr>

<td>Methodology</td>

<td>3</td>

</tr>

<tr>

<td>Other</td>

<td>33</td>

</tr>

</tbody>

</table>

These results become clearer when we analyse the answers to the questions regarding skills considered necessary but not acquired through participants' undergraduate work (Table 2, below). The items with the highest scores were: relationship to information and communication technologies; interpersonal relations; and management of information units. The first item is a subcategory of _skills,_ while the remaining two are subcategories of _knowledge._ The three items are, for any conceptual understanding of professional education, considered essential elements in the constitution of curricula for librarianship and archival studies. Thus, if such knowledge was in some manner included in the curricula of the participants, it was insufficient to ready the students for their professional roles.

<table><caption>

**Table 2: Main skills considered necessary but not acquired in college**</caption>

<tbody>

<tr>

<th>Skill</th>

<th>Frequency</th>

</tr>

<tr>

<td>Relationship to information and communication technologies</td>

<td>32</td>

</tr>

<tr>

<td>Interpersonal relations</td>

<td>15</td>

</tr>

<tr>

<td>Management of information units</td>

<td>9</td>

</tr>

<tr>

<td>Knowledge of languages other than Portuguese</td>

<td>7</td>

</tr>

<tr>

<td>Technical knowledge</td>

<td>4</td>

</tr>

<tr>

<td>Research methodology</td>

<td>3</td>

</tr>

<tr>

<td>Preparation and guidance for the workforce</td>

<td>3</td>

</tr>

<tr>

<td>Preservation</td>

<td>3</td>

</tr>

<tr>

<td>Entrepreneurial vision</td>

<td>3</td>

</tr>

<tr>

<td>Leadership</td>

<td>2</td>

</tr>

<tr>

<td>Other</td>

<td>14</td>

</tr>

</tbody>

</table>

Asked about the difficulties they encounter in exercising their professions and for which they have not felt prepared (Table 3, below), participants mentioned most frequently: command of information and communication technologies, relationship between theory and practice and foreign language skills. Such answers can be analysed in at least two ways: first, they confirm the librarians' need to gain, through their undergraduate training, strong knowledge about information and communication technologies. Second, eight participants noted that they had encountered difficulty in relating theory to practice. This could mean that even regarding cataloguing, classification and indexing the ICI/UFBA graduates lack sufficient education and training. Since, for example, the vast majority of Web pages are in English, even given Brazil's substantial presence on the Web, improving foreign language skills should likewise be prioritized.

Nonetheless, it is important to note that ten participants said they felt completely prepared to exercise their chosen professions. This may mean a high level of satisfaction with their skills or, alternatively, low demands for skills on the part of their employers.

<table><caption>

**Table 3: Main difficulties professionals encounter in the workforce and for which they do not feel prepared**</caption>

<tbody>

<tr>

<th>Difficulty</th>

<th>Frequency</th>

</tr>

<tr>

<td>Lack of command of information and communication technologies</td>

<td>16</td>

</tr>

<tr>

<td>None</td>

<td>10</td>

</tr>

<tr>

<td>Relationship between theory and practice</td>

<td>8</td>

</tr>

<tr>

<td>Knowledge of a second language</td>

<td>7</td>

</tr>

<tr>

<td>Lack of technical knowledge</td>

<td>5</td>

</tr>

<tr>

<td>Lack of professional recognition</td>

<td>5</td>

</tr>

<tr>

<td>Working with equipment</td>

<td>5</td>

</tr>

<tr>

<td>Management of the information unit</td>

<td>3</td>

</tr>

<tr>

<td>Finding solutions for everyday problems</td>

<td>2</td>

</tr>

<tr>

<td>Requirements of the market</td>

<td>2</td>

</tr>

<tr>

<td>Planning for an information unit</td>

<td>2</td>

</tr>

<tr>

<td>Psychology</td>

<td>2</td>

</tr>

<tr>

<td>Responsibilities and requirements</td>

<td>2</td>

</tr>

<tr>

<td>Time table for services rendered</td>

<td>2</td>

</tr>

<tr>

<td>Other</td>

<td>10</td>

</tr>

</tbody>

</table>

The participants were later asked to attribute a degree of importance to a list of items related to knowledge and skills.

Regarding the knowledge necessary to exercise their professions, as considered in the literature of the field, they prioritized: development of information and communication technologies, knowledge of new products and services mediated by these technologies, information management and knowledge management. The degrees of importance in Table 4 represent the number of participants who gave the respective kind of knowledge a score of 5, the highest possible score.

Despite their educational deficiencies, the interviewees clearly are aware of what kinds of knowledge would be necessary for them exercise their professions well. Furthermore, there is considerable agreement between the knowledge they claim to lack and that which they consider essential. For example, since the concept of knowledge management includes that of the management of people, even items that at first seem not to fit, such as psychology, interpersonal relations and team work, mentioned among the difficulties for which they lacked the necessary skills (Table 2), are entirely consistent with the interviewees' answers.

<table><caption>

**Table 4: Degree of importance attributed to kinds of knowledge participants consider necessary to exercise their professions**</caption>

<tbody>

<tr>

<th>Knowledge of:</th>

<th>Degree of Importance</th>

</tr>

<tr>

<td>Development of information and communication technologies</td>

<td>46</td>

</tr>

<tr>

<td>New information products and services</td>

<td>43</td>

</tr>

<tr>

<td>Information management</td>

<td>42</td>

</tr>

<tr>

<td>Knowledge management</td>

<td>38</td>

</tr>

<tr>

<td>Relationship between information studies and other fields</td>

<td>34</td>

</tr>

<tr>

<td>Societal changes and trends</td>

<td>33</td>

</tr>

<tr>

<td>Trends in the field of information</td>

<td>31</td>

</tr>

<tr>

<td>Other languages</td>

<td>31</td>

</tr>

<tr>

<td>Theoreticians in the field of information studies</td>

<td>25</td>

</tr>

<tr>

<td>Scientometrics</td>

<td>11</td>

</tr>

<tr>

<td>Bibliometrics</td>

<td>7</td>

</tr>

</tbody>

</table>

Regarding the list of skills (Table 5), the librarians who graduated from the ICI/UFBA prioritized the following: communication with co-workers, superiors and users; flexibility in the face of change and continuous learning; provision of solutions to problems; good relationship with co-workers, superiors and users; working in teams; seeking out innovation and promoting change; and leadership. The degrees of importance in Table 5 again represent the number of participants who gave the respective answer a score of 5, the highest possible score.

In the case of skills, there is, again, coherence between the skills participants considered necessary and the problems they encountered at work. When they discussed the difficulties they encountered and for which they were not prepared, the most cited skill was command of information and communication technologies; when they discussed necessary skills they lacked, the most mentioned was, again, their relationship with new technologies. It is, then, perfectly reasonable to suggest that students of the librarianship undergraduate programmes of ICI/UFBA can graduate without having acquired in their studies, the necessary set of skills and knowledge regarding the understanding and use of the new information and communication technologies.

<table><caption>

**Table 5: Degree of importance attributed to skills participants consider necessary to exercise their professions**</caption>

<tbody>

<tr>

<th>Skill</th>

<th>Degree of Importance</th>

</tr>

<tr>

<td>Communication with superiors, co-workers, users</td>

<td>49</td>

</tr>

<tr>

<td>Flexibility in the face of change, continuous learning</td>

<td>48</td>

</tr>

<tr>

<td>Seek out solutions to problems</td>

<td>45</td>

</tr>

<tr>

<td>Maintain good relations with superiors, co-workers, users</td>

<td>41</td>

</tr>

<tr>

<td>Perform work in teams</td>

<td>39</td>

</tr>

<tr>

<td>Seek out innovation and be a change agent</td>

<td>38</td>

</tr>

<tr>

<td>Leadership</td>

<td>38</td>

</tr>

<tr>

<td>Identification of problems</td>

<td>34</td>

</tr>

<tr>

<td>Creativity</td>

<td>33</td>

</tr>

<tr>

<td>General management</td>

<td>32</td>

</tr>

<tr>

<td>Use intuition in performing work duties</td>

<td>17</td>

</tr>

</tbody>

</table>

## Discussion

The results of this research point to a clear lack of a mastery of information and communication technologies by the librarians interviewed and not only in their operational aspects. The transformation of the field by the several new digital ways of capturing, producing and organizing information indeed implies a fundamental change. There is, then, a need for information professionals to learn how to understand the new configuration of the world and the role information and communication technologies and information professionals have in this changing environment.

Even cataloguing, classification and indexing, which are traditional matters in the education of librarians, have been changing. Actually, the whole productive network of information has already changed with the introduction of automated templates, directly linked to databases and to mechanisms of bibliographic control.

Globalization, fast technological developments and new organizational patterns have had a profound impact on the workplace in many countries. Nonetheless, the interviews demonstrate that, at the ICI/UFBA, teaching practices in librarianship have been inadequate for the development of information professionals. It is clearly evident that there exists a meaning gap between the skills acquired by library students in classrooms and those required by labour markets. Graduates seem to have not so much as understood that the restricted workstation concept (a group of specific operational tasks or functions) has given way to the idea of occupation, understood broadly, evoking extensive knowledge of an area, with the possibility of horizontal development of an individual's career.

Thus, the ICI/UFBA should adopt at least some aspects of the concept of core competencies, as they are presented by the literature on this topic (Mertens 1974, cited by [Vargas Zúñiga 2001](#vargaszuniga01); [Vargas Zúñiga 2001](#vargaszuniga01); [Arruda _et al._ 2000](#arrudaetal00); [Guimarães 1997](#guimaraes97); [Franco 1997](#franco97); [Targino 2000](#targino00), among others). Librarianship course schedules should be prepared to provide students with attitudes and the capacity to exercise a wide number of functions or occupy various positions, even simultaneously. Such preparation would result in, above all, communication skills and the ability to work with information and technology. Students should also hone their creativity, versatility and initiative, in order to find employment. Data collected in the interviews demonstrate that librarians who graduated from that Institute were not given these kind of competencies in their courses. In fact, when they discussed the difficulties they encountered and for which they were unprepared, the most cited skill was a command of information and communication technologies; when they discussed the necessary skills they lacked, the most mentioned was, again, their relationship with new technologies.

This lack of core competencies is clearer still in the answers participants gave when asked about the difficulties they encountered in exercising their professions and for which they felt unprepared. As we have seen, interviewees mentioned most frequently: command of information and communication technologies, relationship between theory and practice and foreign language skills. The interviewees were, then, aware of what kinds of knowledge would be necessary for them to exercise their professions. The knowledge they claimed to lack was consistent with that which they considered essential. Likewise, the skills they considered necessary and the problems they encountered at work were consistent with each other.

The data analysis of the interviews leads us to argue that the curriculum to which interviewees were exposed emphasizes traditional techniques and opens few doors for acquiring skills to match newer, more contemporary needs. This conclusion is consistent with what is said about Brazilian libraries and archives, where manual browsing approaches prevail. The traditional academic training of librarians, then, has changed little in ICI/UFBA. New graduates are thus put at risk, since librarians are no longer considered the principal professionals in the field of information. Thus, if they do not acquire the necessary core competencies of information professionals, they will be confined to a narrow work space. In fact, as the interviews demonstrated, they have been graduating without having acquired those competencies, skills and knowledge, including regarding the understanding and use of new information and communication technologies.

Had it been possible to interview graduates who were unemployed or working in occupations outside the information professions, we might have gained answers to a number of crucial questions: were graduates unemployed or otherwise employed because they had been unable find jobs in the field, because they had encountered other, more attractive paths, or for some other reason? What other paths did they take and how, if at all, did their education contribute to their ability to perform their professional duties or achieve their goals? What, if anything, did they gain from their participation, however temporary, in the field, either while still students or between graduating and becoming unemployed or changing careers? Was a lack of preparation in a particular area within information studies to blame for their not working in the field? Did they regret leaving the field, and how might they be aided in reentering it, should they desire to do so? Future studies should examine both the ways in which changes to the curricula affect graduates who subsequently become information professionals and also relationships between other graduates who do not become information professions and their education in the field.

Our hope is that research like this can help librarianship courses take core competencies into consideration, in their education and training programmes. This way they can maintain the professional prestige and respect that librarian have long achieved. As many authors have been writing, librarians have meaningful importance to the educational system, in Brazil and elsewhere. They must be given the best education and training, in terms of skills, knowledge and attitudes.

## Conclusion

The core competencies model appears to be prevalent in organizations and, as a consequence, to affect individuals and countries. In terms of organizations, this growth in popularity is a consequence of growing pressure to maintain competitiveness in a global economy. In terms of individuals, remaining employable is the main concern. Regarding countries, a commitment to social development demands the formulation of information policies linked to education, in order to stimulate the growth of these competencies.

The complex relationship among knowledge, skills and attitudes, summarized in this text under the notion of competencies, will depend fundamentally on the use of those resources in specific contexts. This relationship demands, therefore, a capacity for constant learning and permanent adaptation. Flexibility, key to all of these considerations, forms the basis for the new modes of production enabled by the presence and use of information and communication technologies.

Individuals are responsible for the development of their own competencies and must use them to guarantee their jobs; formal education is growing and is stimulated by the organizations. This situation can be verified by the fast growth of master's degrees in Brazil. According to Arruda ([2000](#arruda00): 31), great efforts are being expended to ensure theoretical education, led by entrepreneurs and workers through their unions.

In fact, the understanding that in the knowledge era education should permeate all of society is practically unanimous and some authors, such as Drucker ([1993](#drucker93): 154), defend the point of view that schools must work in partnership with employers and their organizations. However, all agree that universal education of high quality is the top priority. Without such an education, a society would not be capable of becoming competitive. Providing students with the knowledge and abilities necessary to accomplish personal and professional success is a contribution to society and also an obligation of any educational system.

Thus, the relationship between work and education should be part of the conceptions and pedagogic plans of the teaching institutions. Colleges are responsible for facilitating scientific, technological and organizational development; for illuminating paths; and for stimulating openness, flexibility and lifelong learning. In other words, they are responsible for stimulating the development of core competencies in individuals.

It is important to reaffirm here the distinction between training and education. Training provides tangible skills— the ability to provide an item with subject headings, database management skills and the like—while education helps students grow able to acquire new skills on their own or repurpose the ones they have already acquired. In the current context, graduates need specific skill sets in order to obtain jobs (i.e., they need training), but they also need the ability to acquire new skills, a willingness to work under ever-changing conditions, a commitment to lifelong learning, and ways in which to grow personally to help them in their professional and personal lives (i.e., they need education).

Academic programmes cannot offer full sets of tangible, ephemeral skills to their students, but they must offer enough of such skills for graduates to be able to secure jobs that they can exercise with confidence and competence. Nor can schools prepare every student to encounter any imaginable problem in life, but they must help students grow and learn to continue to grow. The challenge for academic programmes is to provide training while preparing students to continue to learn well past their graduation dates and to make creative use of the skills—tangible or otherwise—that they acquire in school and elsewhere, including on the job. Information studies programmes, whether they offer _library science,_ _archival science,_ _information science,_ _information architecture,_ or any number of other degrees or certificates, at the undergraduate or graduate level, throughout the world, must continue to work to find appropriate balances between training and education.

The concept of core competencies can help us understand where these balances might best be struck. Simply put, schools cannot and should not prepare students perfectly with all of the skills they will need on the job and in life, but we can do a better job of choosing the skills that we prioritize and in helping students become capable of, and confident in, acquiring skills not taught to them in school. And although most of the cited authors point to this need, undergraduate programmes still are not considering it in any effective way. As emphasized by Vargas Zúniga ([2000](#vargaszuniga00)) and Arruda _et al._ ([2000](#arrudaetal00)), the world of work is changing radically, due to the new possibilities brought about by the information and communication technologies.

Likewise, the concept of core competencies widens the possibilities of workers. The best contribution that undergraduate programmes for information professionals can offer to students is teachers who help them to obtain the core competencies to exercise the professions they choose and a conscious understanding of the continuous widening of their professional occupations.

This research brings to the fore evidence of the need to establish new curricula for librarians. The flaws mentioned by the interviewees are substantial and they indicate a high degree of disconnection between training and current work demands. Worse than this, they denote a strong incoherence between the theoretical reflections offered in specialized journals in the field of information studies regarding the profile expected of professionals and the training and education actually received by these professionals.

## Acknowledgements

We would like to thank the more than one hundred individuals who graciously agreed to be interviewed for this research and particularly the sixty who were selected. We also wish to acknowledge the suggestions of the anonymous referees.

## References

*   <a id="araripe98"></a>Araripe, F.M.A. (1998). [Bibliotecário - profissional da informação. (Re) desenhando o perfil a partir da realidade brasileira: proposta para os países do mercosul.](http://www.webcitation.org/5LFMfYkha) [Librarian - information professional. (Re)-designing the profile in today's Brazil: proposal for the countries of the Mercosul. ] _Tercer Encuentro de Directores y Segundo de Docentes de das Escuelas de Bibliotecología del Mercosur 1998._ Santiago: Universidad Tecnológica Metropolitana. Retrieved 10 April, 2004 from http://www.utem.cl/deptogestinfo/20.doc
*   <a id="arruda00"></a>Arruda, M.C.C. (2000). [Qualificação versus competência.](http://www.webcitation.org/5LFMzE74V) [Qualification versus competency] _Boletín Cinterfor_, No. 149, 25-39\. Retrieved 19 December, 2006 from http://www.cinterfor.org.uy/public/spanish/region/ampro/cinterfor/publ/boletin/149/pdf/calmon.pdf
*   <a id="arrudaetal00"></a>Arruda, M.C.C., Marteleto, R.M., & Souza, D.B. (2000). Educação, trabalho e o delineamento de novos perfis profissionais: o bibliotecário em questão. [Education, work and the descriptioin of new professional profiles: the librarian in question.] _Ciência da Informação_, **9**(4) 14-24.
*   <a id="drucker93"></a>Drucker, P.F. (1993). _Sociedade pós-capitalista._ 3.ed. [Post-capitalist society.] São Paulo: Pioneira.
*   <a id="drucker94"></a>Drucker, P.F. (1994, November). [The age of social transformation.](http://www.webcitation.org/5LFNmBhR4) _The Atlantic Monthly_, **274**(5), 53-80\. Retrieved 19 December, 2006 from http://www.providersedge.com/ehdocs/transformation_articles/Age_of_Social_Transformation.pdf
*   <a id="franco97"></a>Franco, M.A. (1997). Internet: reflexões filosóficas de um informata. [The Internet: philosophical reflections of an information scientist.] _Revista Transinformação_, **9**(2), 37-48.
*   <a id="guimaraes97"></a>Guimarães, J.A.C. (1997). [Moderno profissional da informação: elementos para sua formação no Brasil.](http://www.webcitation.org/5LFOQDlyZ) [The modern information professional: elements of her training in Brazil.] _Revista Transinformação_, **9**(1), 124-140.
*   <a id="lecoadic96"></a>Le Coadic, Y.A. (1996). _Ciência da informação_ [Information science]. Brasília: Briquet de Lemos.
*   <a id="mason90"></a>Mason, R.O. (1990). What is an information professional? _Journal of Education for Library and Information Science_, **31**(2), 122-138.
*   <a id="nisembaum00"></a>Nisembaum, H. (2000). _A competência essencial_. [The essential competency.] Paulo: Infinito.
*   <a id="ponjuan93"></a>Ponjuan, G. (1993, March). Does the modern information professional have a life cycle? _FID News Bulletin_, **43**(3), 61.
*   <a id="ponjuan95"></a>Ponjuan, G. (1995). _La nueva postura del profesional de información_. Paper presented at the third Congresso de Biblioteconomia, Documentação e Ciência da Informação, São Paulo.
*   <a id="prahalad99"></a>Prahalad, C.K. (1999). O reexame das competências. [The re-examination of competencies] _HSM Management_, **17**(3), 40-46.
*   <a id="senac99"></a>Serviço Nacional do Comércio. _Departamento Nacional_. _Centro de Análises, Estudos e Pesquisas_. (1999). _Século XXI: as novas ocupações_. [The 21st century: new occupations.] Rio de Janeiro: SENAC.
*   <a id="tarapanoff99"></a>Tarapanoff, K. (1999). O profissional da informação e a sociedade do conhecimento: desafios e oportunidades. [The information professional and the knowledge society: challenges and opportunities.] _Revista Transinformação_, **11**(1), 27-38.
*   <a id="targino00"></a>Targino, M.G. (2000). _[Quem ê o profissional da informação?](http://www.webcitation.org/5LFPgVFZK) _[What is the information professional?] Maceió, Brazil: Universidade Federal de Alagoas. Retrieved 19 December, 2006 from http://www.decos.ufal.br/cienciadainformacao/evento2.htm
*   <a id="teixeirafilho99"></a>Teixeira Filho, J.T. (1999). _[Qual é o futuro dos profissionais da informação?](http://www.webcitation.org/5LFYgLhGY)_ Retrieved 19 December, 2006 from http://tinyurl.com/yb65jq
*   <a id="unesco96"></a>International Commission on Education for the Twenty-first Century.. (1996). _[Learning: the treasure within](http://www.webcitation.org/5LFZJea8T)_ (The Delors report). Paris: UNESCO. Retrieved 19 December, 2006 from http://www.unesco.org/delors/delors_e.pdf
*   <a id="vargaszuniga00"></a>Vargas Zúñiga, F. (2000). [De las virtudes labourales a las competencias clave: un nuevo concepto para antiguas demandas.](http://www.webcitation.org/5LFZzvNfj) [From work abilities to key competencies: a new concept for old needs.] _Boletin Cinterfor_, **149**, 9-23\. Retrieved 19 December, 2006 from http://tinyurl.com/yejz6a
*   <a id="vargaszuniga01"></a>Vargas Zúñiga, F. (2001). _[La formación por competencias. Instrumento para incrementar la empleabilidad](http://www.webcitation.org/5LFaNiDCv)_. [Training for competencies. A tool to increase employability.] Retrieved 19 December, 2006 from http://tinyurl.com/yebbcj
*   <a id="zarifian98a"></a>Zarifian, P. (1998a). _[El modelo de la competencia y sus consecuencias sobre el trabajo y los oficios profesionales](http://www.webcitation.org/5LFarl81q)_. [A model of competency and its consequences for work and professional occupations.] Paper presented at the Centro Internacional para a Educação, Trabalho e Transferência de Tecnologia, Rio de Janeiro. Retrieved 19 December, 2006 from http://tinyurl.com/ym5rtg
*   <a id="zarifian98b"></a>Zarifian, P. (1998b). _[Mutación de los sistemas productivos y competencias profesionales: la producción industrial de servicio](http://www.webcitation.org/5LFarl81q)_. [Change in productive systems and professional competencies: the industrial production of service.] Paper presented at the seminar Reestruturação produtiva, flexibilidade do trabalho e novas competências profissionais, Coordenação dos Programas de Pós-graduação de Engenharia, Universidade Federal do Rio de Janeiro. Retrieved 19 December, 2006 from http://tinyurl.com/ym5rtg
*   <a id="zarifian01"></a>Zarifian, P. (2001). _Objetivo competência: por uma nova lógica_. [Objective competency: for a new logic.] São Paulo: Atlas.