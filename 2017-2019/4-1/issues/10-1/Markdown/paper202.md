#### Vol. 10 No. 1, October 2004

* * *

# 'Whoever increases his knowledge merely increases his heartache.'  
Moral tensions in heart surgery patients' and their spouses' talk about information seeking

#### [Kimmo Tuominen](mailto:kimmo.tuominen@helsinki.fi)  
Arts Faculty Library, University of Helsinki  
FIN-00014 Helsinki, Finland

#### **Abstract**

> The paper analyses accounts of information behaviour that are produced by 20 heart surgery patients and their spouses. It is shown that patients and their significant others have to act in a context in which health ideologies stressing self sufficiency and patient compliance play a strong role. Thus, the analysed accounts and narratives of information seeking reflect moral demands that ill persons and their significant others are facing in contemporary society. The author uses social constructionist discourse analysis to examine how the interviewees have to relate their descriptions of information practices to existing moral presuppositions on how rational individuals should behave.

## Introduction

In recent years, the Library and Information Science (LIS) field has witnessed an increased interest in social constructionism (in short: constructionism) as a metatheory ([Tuominen & Savolainen, 1997](#tuominen1997b); [Tuominen, _et al._, 2002](#tuominen2002)) and as a methodological tool for analysing interviews ([Talja, 1999](#talja1999a)), studying identity construction ([Julien & Given, 2003](#julien)) and doing ethnography ([Pettigrew, 1999](#pettigrew)). Even though many constructionist studies have analysed and sometimes criticised standard metatheoretical approaches and methodologies in LIS (cf., [Tuominen, 1997](#tuominen1997a)), few studies have applied constructionist insights in empirical information behaviour (IB) research (e.g., [Given, 2002](#given); [McKenzie, 2002](#mckenzie2002); [Pettigrew, 1999](#pettigrew)). This paper contributes to the emerging research area of constructionist information behaviourstudies ([McKenzie, 2004](#mckenzie2004); [Tuominen, 2001](#tuominen2001)). According to McKenzie ([2002](#mckenzie2002): 44), studying individuals' accounts of every day life information seeking from the constructionist perspective can, 'alert researchers to the subtle ways in which information seeking is discursively bound up in descriptions of appropriate or inappropriate behaviour'. Similarly, Talja and her colleagues ([1999](#talja1999b)) note that accounts and narratives of information seeking contain powerful moral features. Thus far, however, the analysis of these features have been left somewhat aside in information behaviour research.

Kuhlthau ([1993](#kuhlthau)) has found out that anxieties and uncertainties are an integral part of an information search process. Moreover, according to some research results ([Baker, 1997](#baker); [Eriksson-Backa, 2003](#eriksson-backa): 125; [Tuominen, 1992](#tuominen1992): 84-85), health knowledge is not always actively pursued for: information seekers can attempt to reject or avoid information. The present paper sheds more light on the moral side of this phenomenon of anxiety and information avoidance. Individuals seeking information have to relate their talk and other activities to existing moral presuppositions on how rational and self-controlling individuals should behave. The moral context of information and health behaviour is visible in the way seriously ill people and their significant others construct accounts of information practices. Thus, concentrating on the moral aspects of information seekers' talk and action—understanding the context of information seeking as a 'socially defined moral context'—can provide new insights for information behaviour studies.

## Methods

The empirical data consists of 20 interviews of patients awaiting heart surgery, and their spouses. The interviews were carried out and transcribed in full in spring 1995\. Of the interviewed patients 17 were male and three female. Most of the patients were interviewed at their homes; only one interview was undertaken in a hospital. The age of the interviewees ranged from 42 to 75 years, and the average age was 61 years. The spouses were present in thirteen of the interviews; five interviewees lived alone, and two spouses were unable to attend the interview. The educational level of the interviewees ranged from low to medium. Contacts with the interviewees were made through a university hospital or through a support group for heart patients.

The interviews were informal in nature, taking from an hour and a half to two hours and a half. The themes of the interviews were set a priori as follows: present life situation of the interviewees, the events that had lead to the heart surgery, attitude towards the heart surgery, future life plans after the operation. The primary aim was not to discuss information practices separately from the concerns of the interviewees. Most accounts of information behaviour arose spontaneously during the course of the interviews. At the end of each interview, however, the interviewer posed a few direct questions about the interviewees' information needs and information seeking habits.

The data were analysed by applying constructionist discourse analysis ([Potter & Wetherell, 1987](#potter)). Constructionism is a metatheoretical perspective concentrating on the form-giving and productive power of language and on the dialogic nature of knowledge ([Tuominen, _et al._, 2002](#tuominen2002)). According to McKenzie, a pursuit for more inclusive understanding of 'information seeking within broader social practices invites a constructionist analytical paradigm' ([McKenzie, 2004](#mckenzie2004): 685). Through this paradigm the information behaviour research field may be able to gain 'a deeper understanding of the information practices that make up everyday life information seeking' ([McKenzie, 2004](#mckenzie2004): 693).

As a constructionist perspective on research data, discourse analysis focuses on the fluid and subtle ways speakers and writers use rhetorical and cultural resources to construct variable versions of states-of-things and events. This kind of variability in talk and texts is not random: it can be explained by scrutinizing the contextual and ideological functions of versions presented in written text or in face-to-face encounters. For example, people argue, accuse, and question things and issues, and present morally accountable explanations to their acts and deeds in discourse.

When applying discourse analysis, the guiding methodological principle of data analysis is that researchers should distance themselves from the common perspective to language as a transparent window to reality depicting events and states-of-things as such. In discourse analysis, the attention is turned to discursive social practices. Instead of using traditional explanatory concepts, such as individuals' goals or their mental processes, the researcher focuses on organised patterns of action, on the discursive meaning making that is by nature not individual and unique but social and shared. Distinctive from many other data analysis methods, discourse analysis does not try to reach beyond talk and text but focuses on meaningful action as it is manifested in verbal communication. Thus, discourse analysts place the focus on what happens between the speakers, not on what happens inside the speakers' heads. From the methodological perspective of discourse analysis, the interview talk of individuals can be approached as "a cultural and collective phenomenon" ([Talja, 1999](#talja1999a): 461).

### Interpretative repertoires and subject positions

For making sense of the data, I use the analytical concept of interpretative repertoires ([Potter & Wetherell, 1987](#potter)). These repertoires are abstractions or ideal-types captured from the on-going flow of interaction between the interviewer and the interviewees. The analyst should build the repertoires by examining contrasts and deviations between different versions of the same object or event ([Parker, 1992](#parker): 12-13; 126). Thus, the analysis proceeds by identifying significant patterns of consistency and variation in the data, and concentrates especially on incompatible points and interpretative conflicts ([Talja, 1999](#talja1999a): 466). The main goal of discourse analysis is to present a comprehensive picture of all different interpretative repertoires - or meaning systems - in the existing data, as well as to show the contextual functions of these repertoires in social interaction. The comprehensive picture of all the found interpretative repertoires and their functions is presented elsewhere ([Tuominen, 2001](#tuominen2001)). Here, I concentrate on the actual and potential conflicts between two of the repertoires that emerged from the interview data, that is, the virtue repertoire and the anxiety repertoire. These two repertoires are closely connected to the moral nature of the interviewees' talk and, especially, their accounts of information behaviour.

Both of these repertoires offer, or persuade the speaker to accept, a specific identity, a subject position. Although not automatically determining the speakers' identities these repertoires have rhetorical force to present themselves as if they were natural and obvious. Therefore, the speakers can not escape taking a stand or relating their discursive actions to the offered subject position. In some situations, the speakers accept the subject position uncritically. In other situations the speakers can, in turn, consider their relationships to the subject position and even contextually modify this position ([Fairclough, 1992](#fairclough): 45). In this sense, interpretative repertoires are not closed and rigid meaning systems and they do not act as sole determinants of social practices. If these systems seem to be rigid or closed, this stems from the fact that the social practices and institutions forming or sustaining them are so forceful. In diverse contexts of talk, however, the identity of one and the same person may be produced very differently (cf., [McKenzie, 2004](#mckenzie2004)).

### Virtue and anxiety repertoires

Reasonability and rational mastery of life are the main features defining the virtue repertoire. This repertoire portraits the subject position of an _ideal patient_ or an _ideal ill_: the person waiting for heart surgery and his or her significant others are produced from this position as collaborators with doctors and hospital staff. Together with health care providers the speakers fight against the disease and, even when facing adversary circumstances, do not give up. Indeed, they make all the necessary adjustments in coping with the heart disease, and, for example, try to take part in working life as much as they can and emphasize how eager they are to return to their professional duties after the surgery. They are optimistic and trustful about the future, and try to hold away anxieties and frustrations. From the virtue repertoire's perspective, medical compliance is a norm from which one should not deviate. Furthermore, the subject position offered by the virtue repertoire is that of a believer in the cognitive authority ([Wilson, 1983](#wilson)) of medical knowledge and in the effectiveness of medical treatments.

Billig and his associates ([1988](#billig)) have analysed the ideological dilemmas facing chronically ill people. These people have to act simultaneously in the worlds of both health and illness and, because the world of health ideologically dominates to that of illness, they have to be as healthy as they can. At the same time, however, they should not to overestimate or overdo their healthfulness, because this would be medically and morally unacceptable behaviour. Finding the balance between the demands of the world of health and the limitations of the sick body is a continuous and difficult task. The virtue repertoire presents an ideal position in which this balance seems to be found.

The virtue repertoire has connections to the moral norms and rules concerning healthy behaviour, responsible eating habits, and positive attitude and life style ([Radley, 1994](#radley)). 'Maintaining the health of the body in every circumstance' has become an almost self-evident maxim for us all. However, the moral pressure of health ideologies focuses most strongly on people who are double citizens of the worlds of health and illness. They are morally accountable for their living habits and health behaviour. In fact, they should try to use all the means available for finding—regardless of the malfunctions of their bodies—suitable roles as productive and useful members of society. As crystallized by Silverman ([1987](#silverman1987): 205 ), 'the treatment of illness inevitably occurs within a moral framework'.

The anxiety repertoire does not picture the chronically ill and their significant others as persons who are in control with the situation they are facing. On the contrary, the anxiety repertoire deals with serious biographical and identity disruptions ([Bury, 1981](#bury1981)). When the interviewees start to use this repertoire and adopt its subject position uncritically, the optimistic attitude of the virtue repertoire is washed away and they describe difficulties in psychological coping with the situation. It is hard to keep the emotional impulses in control and, thus, the terminology characterizing this vocabulary comprises words like _depression_, _giving up_, _angriness_, _fear_, _panic_ and _despair_. The anxiety repertoire is not directed towards the future, it clings on to the past and present difficulties and on to the horror from which there seems to be no way out. From the perspective of the virtue repertoire, the kind of behaviour and talk characterising the anxiety repertoire is improper, for the _dictatorship of emotions_ in the patient's mind might lead to negative consequences like medical non-compliance. The anxiety repertoire, in turn, views the virtue repertoire as a kind of hypocrisy that easily leads to pretending to one's own essential self and to others. The interviewees justify their adoption of the subject position of the anxiety repertoire by stressing the authenticity of their emotions and fears.

While medical knowledge and practices are seen as neutral and useful in the virtue repertoire, the reifying of the individual's body in actual medical operations raises opposition in the anxiety repertoire. When the interviewees adopt the subject position offered by the anxiety repertoire, they use emotional and imaginative metaphors to describe the seemingly grotesque or brutal side of the opening of their own chest in heart surgery. The power of these metaphors lies in the contrast between the impersonal medical operations and the subjective lived experience of the individual's own body. In the anxiety repertoire, the world of hospitals and medical practices seem alien, and even hostile. However, when the speakers use the virtue repertoire, they connect this same impersonality of medical operations to images of effectiveness, professional skill and safety.

Quite often (but not always) the anxiety repertoire is not adopted as such but it is a target of critique in statements presented from the viewpoint of the virtue repertoire. In these instances, someone else, possibly a fellow patient, suffers from irrational affective impulses, and the interviewee feels compassion for this kind of behaviour, or condemns it. The discursive function of this kind of talk is to show how far the subject position of the anxiety repertoire is from the speaker's own identity. As the virtue repertoire approaches the situation of the ill persons and their significant others from the perspective of the world of health, the anxiety repertoire keeps distance from this ideologically dominant standpoint and withdraws to the world of sickness.

The virtue repertoire and the anxiety repertoire have opposing functions in the analysed data. The virtue repertoire is often utilized to show how the individual struggles to behave and act in an accepted manner or _like a normal person_. In contrast, the anxiety repertoire uses the rhetoric of authentic experiences and uncontrollability of emotions to justify the kind of behaviour that does not fit to the ideological demands of the world of health. As we will see below, the interviewees' accounts of information behaviour are closely connected to the antagonism of these two repertoires and to their different ideological and rhetorical functions.

### The moral context of the interviews

Analysing people's ways of talking to each other in interviews enables us to examine the content of culturally dominant moral assumptions ([Silverman, 1993](#silverman1993): 108). In this paper, I approach interview data as verbal interaction and as a negotiation of meanings that takes place between the interviewer and the interviewees. The ideological context of this negotiation has significant influences on the way the interviewees produce their accounts of information behaviour. Even when facing adversary circumstances, like serious illnesses, people still are members of their society. Therefore, the interviewees have to relate their discursive and other actions to the norms of health behaviour in the Western countries.

To act properly in the _world of health_, the individual should be physically, mentally and economically self-sufficient. Because of this kind of normative or ideological demand, the ill person and his or her significant others have to continuously prove to potentially critical or sceptical spectators—relatives and friends as well as formal authorities—their will to get better and to live as normally and independently as possible. They are morally accountable for their past and present behaviour and coping strategies, and they have to use discursive and narrative cultural resources to prevent false associations and potential critique even in situations where no one has explicitly made any moral judgments ([Lawton, 2003](#lawton)). The central arenas in which the meanings of illness experience are negotiated are family and working lives as well as encounters with the health care providers. For the ill person, the reciprocity of meanings and interpretations is very important. However, as the ill person can not be sure that his or her own interpretations are similar to those made by others, he or she has to repeatedly test the acceptability of the constructed illness narratives and accounts in various contexts ([Bury, 1988](#bury1988)).

The chronically ill and their significant others use narratives and accounts as kinds of coping strategies in the world of health. Through these means they try to survive mentally and socially from the biographical disruption ([Bury, 1981](#bury1981)) they have faced and attempt a 'narrative reconstruction' ([Williams, 1984](#williams)). These kinds of discursive coping strategies have a clear moral role and function. They are used in discussing and arguing on, for example, such central issues as who is morally responsible for the onslaught of an illness or the delay of a formal medical diagnosis.

The control of body functionality and well-being has been internalised, through changes in health practices and ideologies in the recent decades, as a proper mission of every individual citizen ([Radley, 1994](#radley)). The individual, especially one who is chronically ill, has to continuously position himself or herself as an object of decentralized control, according to the kind of health behaviour in which he or she engages. The chronically ill person is, nowadays, a speaking subject making _confessions_ and this kind of speech is a nuanced part of the power-knowledge machinery of the present-day society ([Foucault, 1981](#foucault)). In this sense, the analysed interview extracts can be interpreted in the light of moral demands of control and self-sufficiency that characterise rationalistic production processes and power-structures of the contemporary society ([Tuominen, 2001](#tuominen2001)).

## Accounts of information behaviour

The virtue repertoire views information seeking, ideally, as a part of the patients' medically compliant health behaviour that is directed to effective coping with the illness and, eventually, recovering from it. The extract from the interview data below shows how closely the interviewees' accounts of information behaviour often fit with the virtue repertoire.

> 'I'm not at all worried about the delay with the surgery operation because I've coped quite well anyway. I've had a healthy diet for at least three years and haven't smoked since going to the secondary school, at that time when all the boys had to try to, but I quit quite quickly... I'm not even overweight, just normal... and, as a hobby, I've been following the Heart Association's journal for some twenty years. You know, at work somebody said that why don't you join in supporting this... and it was quite cheap. I didn't read the journal at first, but started to read it after getting high blood pressure... and I begun to follow the nutrition guidelines often given in the journal and elsewhere.' (John)

When describing his information practices John uses the virtue repertoire. In this repertoire, the concern for mental and physical balance, non-smoking, and healthy diet are a natural and self-evident way of fighting against the disease. In the virtue repertoire, seeking and receiving information play a substantial part in the larger struggle against the disease. Scanning the Heart Association's journal and internalising relevant nutritional information are seen as important tasks for an individual suffering from coronary artery disease.

> Interviewer: ;So, you said that you've got quite familiar with the literature dealing with your illness?'  
>   
> John: 'Well, not quite with such literature as medical books but rather with guidelines and leaflets and such. There is one called _Coronary bypass surgery_ and there are good articles in the Heart Association's journal... But I'm not of the type to buy a high quality medical handbook to examine. That would be totally useless, because that's the doctors' task..., though, of course, these books are commercial stuff and attempt to popularise, but mostly one gets more confused and may even find symptoms of other diseases and start to feel unnecessary anxiety. I'm not interested in that... but I do follow such real information that promotes health, that is related to eating and life habits... and to stress, too. Stress is one risk factor in coronary artery disease and high blood pressure...'

John takes a negative, even dismissing attitude towards the interviewer's expression _literature_. He determines 'examining medical handbooks' as unaccepted behaviour because it can cause concerns and even hypochondria as the ill person gets to know more than what is enough for him or her. At worst, seeking and using wrong kind of medical information might lead the patient to adopt the anxiety repertoire's subject position. As behaviour of this kind is unacceptable from the viewpoint of the virtue repertoire, it is better to leave the medical handbooks untouched. Absorbing medical knowledge as such is, from the virtue repertoire's perspective, unsuitable behaviour for patients. Reading leaflets and brochures, in turn, is acceptable for getting hold of new nutritional information and eliminating risk factors of different kinds.

It is interesting to notice that in the virtue repertoire studying _books_ is the opposite of reading _leaflets and guidelines_. John seems to consider books as those information sources that aim at increasing medical knowledge as such, whereas leaflets and guidelines and the Heart Association's journal contain information written by professionals in such a form that is easy for lay persons to understand. From the perspective of the virtue repertoire, the expert should act as the mediator of suitable and appropriate medical facts for the patients. Thus, lay patients should be more like receivers than active sense-makers and seekers of medical information.

> Kathryn: 'But usually the reading of literature, especially medical handbooks, is at least according to doctors...'  
> Paul: 'Yes... in their opinion, yes, yes...'  
> Kathryn: 'In their opinion a bit of risky business, so, that, it doesn't pay off to dig deep into it, as one can get...'  
> Paul: 'Yes, but...'  
> Kathryn: 'Easily lost...'  
> Paul: 'But the book... [called] _The Heart_...'  
> Kathryn: Yes, yes, that one.  
> Paul: 'I must confess that it was a good one.  
> Kathryn: Yes... but it's a different matter... it tells patients about the operation... nothing else.

Kathryn, the spouse, gets involved in the discussion when Paul talks about the book he is reading. According to Kathryn, the opinion of the doctors is that patients should not read medical literature dealing with their illness. Paul seems to be aware of this kind of criticism of patients' potentially harmful reading habits even before Kathryn has ended her sentence. Kathryn and Paul do not use the anxiety repertoire explicitly in the excerpt presented above, but this repertoire is hidden in the argumentative context ([Billig, _et al._, 1988](#billig)) of their conversation. Both Kathryn and Paul speak using the virtue repertoire and indicate that Paul's information activities should aid his recovery from the illness. In this context, reading medical handbooks and scientific literature seems to be somewhat doubtful. Getting involved in excessive use of medical knowledge might, from the virtue repertoire's point of view, lead lay persons to _pictures of clinical horror_ and, thus, cause needless worries and concerns. As untrained minds, the lay patients not using the popularised but expert form of medical knowledge seem to be in danger to, eventually, get entrapped into the anxiety repertoire's subject position. The interviewees make Paul's reading habits morally accountable by stressing that _The Heart_ is not a scientific book, but describes the functioning of the heart and the surgery operation in popular terms.

Getting deeper into medical information sources can be seen as an attempt to get rid off the role of a lay person. Both John and Paul explain their choices of information sources with the same argument: medical professionals have intended the received or found information for the heart patients and not, for example, to their peers. In this sense, the interviewees present their information seeking as a sign of the virtue repertoire. The interviewees stress that they are not searching for or receiving detrimental information and that they are not trying to become medical experts: their information seeking is part of their struggle with heart disease.

### A deviant case analysis

From the viewpoint of the virtue repertoire, disregarding medical information that is not directed for heart patients seems to be as important as it is to gather correct and suitable information. The virtue repertoire holds that the ill person should not seek for or receive more information than is enough for being a co-operative patient. The interviewees often stress that even if there might be people with a yearning for knowledge about the particularities of the disease and the surgery operation, they themselves do not belong to this group. There was, however, one clear exception, a deviant case, which deserves to be analysed in detail. Wayne emphasised repeatedly his active use of medical literature. Moreover, Wayne proved to be aware that from the perspective of the virtue repertoire it is not necessarily purposeful to present oneself as an active seeker of medical facts.

> Wayne: 'And then about this anxiety [after the operation], they don't talk about it... because there might be psychological effects...'  
> Interviewer: 'So, this is an issue that you've worked out by yourself?'  
> Wayne: 'Yes. They don't tell... I just tried to say to the assistant doctor that I know something about it. He said that I'll be fine, that everyone doesn't feel anxiety, that it is of course better that the patient doesn't think about these issues, and I don't, but I just want to know and have a look at the books.'

Wayne indicates that medical literature does not make him anxious, or take him too deep into his health problems. He argues that his information behaviour, which is not in line with the virtue repertoire's standard subject position of a co-operative patient, is caused by his desire for more medical information. A few minutes later he explained his atypical information practices in the following way:

> 'So, the assistant doctor said that possibly the surgeons will tell before anaesthesia... that they'll tell that we'll do now so and so [laughs] and he said that usually they don't even talk about the operation because, of course, all patients are not so... but I've been building and planning big house complexes all my life, so I'd think that I'll understand something about medicine as well when reading... though, of course, I know nothing about the details. They are the best men in this country and surely women too, who'll operate me, so, in this sense there is no need to discuss about these issues.'(Wayne)

Wayne defends his use of medical books as information sources with his own expertise: because, on the basis of his professional education and experience, he masters house construction, he maintains to have the ability to comprehend _raw_ medical information as well. In the interview context, Wayne produces himself as a person who can take almost an expert attitude to medical facts and practices even though he does not have formal education in this field. Furthermore, he makes a difference between himself and some other patients: everyone does not have the capability to take a neutral and technical attitude to the biomedical discourse. At the end of the extract, however, Wayne reacts against potential critique of him as a fake medical expert. He admits that he cannot understand every nuance and detail of what he reads, and affirms the superior ability of the physicians to comprehend medical knowledge and perform treatment procedures.

Wayne makes an exception in the research data by stressing the role of _raw_ medical material in his information world ([Chatman, 1992](#chatman)). As with the other interviewees, however, Wayne's descriptions of his information practices have still a link to the tension between the virtue repertoire and the anxiety repertoire. Wayne makes his reading of medical literature morally accountable by producing himself as a technical expert and arguing that his expertise protects him from taking medical information too subjectively and becoming distressed. Furthermore, Wayne makes it clear that he does not attempt to compete in expertise with the doctors but, instead, shows trust in their superior professional capabilities. His denial of this kind of competing position is in line with the virtue repertoire's ideal picture of a co-operative surgical patient.

## Conclusion

The virtue repertoire forms the generally accepted moral foundation in heart surgery patients' and their significant others' accounts of information behaviour. These accounts can be studied as a kind of confessional talk that is based on the morally obliging ideology of individuals' correct health behaviour. This ideology stands in close relation with the standard model of providing health information and health promotion material in a society. The role of the medical expert in this model is to choose relevant health information for lay persons and to present this information in a form that is suitable for them. Most of the analysed accounts of information behaviour explain avoiding wrong kind of expert information as a way to prevent oneself from getting entrapped in the anxiety repertoire's subject position. Table 1 summarizes the actual and potential conflicts and contrasts between the virtue repertoire and the anxiety repertoire.

**Table 1: The mutual relations between the virtue and the anxiety repertoires**

<table>

<tbody>

<tr>

<th>Virtue repertoire</th>

<th>Anxiety repertoire</th>

</tr>

<tr>

<td>Reason controls affects and emotions</td>

<td>Affects and emotions control reason</td>

</tr>

<tr>

<td>Medical compliance</td>

<td>Medical non-compliance</td>

</tr>

<tr>

<td>Mental attitude directed towards the future</td>

<td>Mental attitude entrapped with the past or present</td>

</tr>

<tr>

<td>Trust, will-of-life</td>

<td>Fear</td>

</tr>

<tr>

<td>Fighting or winning the disease, adjustment and coping, staying in control</td>

<td>Unforeseen course of the illness, crisis, loss of control</td>

</tr>

<tr>

<td>Morally correct, accepted behaviour</td>

<td>Unaccepted behaviour</td>

</tr>

<tr>

<td>Waiting for returning to normal working, family, and social roles after the surgery operation</td>

<td>Thoughts centred solely on the surgery operation or illness, or both</td>

</tr>

<tr>

<td>The surgery operation as a necessary and effective process</td>

<td>The surgery operation as an inhuman act destroying the subjective experience of the lived body</td>

</tr>

<tr>

<td>Information seeking seen as a part of the healing process; searching for right kind of (popularised) information and avoiding medical scientific publications and "raw" or unprocessed biomedical facts</td>

<td>Information avoidance; wrong kind of (expert) information might lead to deeper depression</td>

</tr>

</tbody>

</table>

The analysis raises an interesting insight into the information world of lay heart patients and their significant others, and into the moral assumptions characterizing this world and its boundaries. In this world, the patients should not behave as active sense-makers and judicious searchers for all kinds of information. In fact, someone else, that is, the expert, should construct the meaning of information for the lay person who should act as a recipient of 'the information brick' ([Dervin, 1983](#dervin)) which the cognitive authority has manufactured for him or her. As was shown in the detailed analysis of the deviant case in the data, the individual is morally accountable for spanning the boundaries of this lay information world and he or she must give suitable explanations for _wanting to know more than one needs to_. It seems that a thorough analysis of the information world of heart surgery patients and their spouses would demand a serious consideration of the roles of medical expertise and knowledge in society as well.

Further empirical research is needed into information behaviour as a moral practice in general, and into morality of health information seeking in particular. Thus far, psychological coping and learning theories have provided insights into how individuals' information seeking attitudes and styles vary. Psychologically oriented studies have shown, for example, that chronically ill individuals can be classified as active seekers or avoiders of information. The constructionist approach as used in the present study can provide more nuanced picture on this issue by revealing how thorough social individuals' accounts of information practices are, and how accounts of both information seeking and avoidance reflect compelling social norms and values. The information world of heart surgery patients seems to be a social construct that has powerful effects on actual information seeking styles and attitudes of the patients and their significant others.

It is important to scrutinize the moral nature of information seeking not only from the viewpoint of constructionist discourse analysis but also by applying the traditional naturalistic user-oriented perspective of information behaviourstudies. Methodologically, this would demand new and innovative research strategies because the ill persons' and their significant others' accounts of information behaviour are at least as much moral stories and interactional accomplishments as they are transparent representations of the individuals' attitudes or their previous information seeking processes. In addition to using interview data, we should complement the picture of the ill persons' information practices by examining actual information seeking encounters and incidents. In my opinion, there is an increasing demand for ethnographic and conversation analytic methods in information behaviourresearch. We need to find out the ways in which people relate their talk as well as _real-life_ information searching strategies and tactics to the context of appropriate and inappropriate information behaviour.

## Acknowledgements

The author would like to thank Maija-Leena Huotari, the members of the REGIS group, and the anonymous referees for helpful comments on previous versions of this article.

## References

*   <a id="baker"></a>Baker, L.M. (1997). Preference for physicians as information providers by women with multiple sclerosis: a potential cause for communication problems? _Journal of Documentation_, **53**(3), 251-262.
*   <a id="billig"></a>Billig, M., Condor, S., Edwards, D., Gane, M., Middleton, D. & Radley, A. (1988). _Ideological dilemmas: the social psychology of everyday thinking._ London: Sage.
*   <a id="bury1981"></a>Bury, M. (1981). Chronic illness as a biographical disruption. _Sociology of Health and Illness_, **4**(2), 167-182.
*   <a id="bury1988"></a>Bury, M. (1988). Meanings at risk: the experience of arthritis. In R. Anderson & M. Bury (Eds.), _Living with chronic illness: the experience of patients and their families_ (pp. 89-116). London: Unwin Hyman.
*   <a id="chatman"></a>Chatman, E.A. (1992). _The information world of retired women._ Westport, CT: Greenwood Press.
*   <a id="dervin"></a>Dervin, B. (1983). Information as user construct: the relevance of perceived information needs to synthesis and interpretation. In A.W. Spencer & L.J. Reeds (Eds.), _Knowledge structure and use: implications for synthesis and interpretations_ (pp. 153-183). Philadelphia, PS: Temple University Press.
*   <a id="eriksson-backa"></a>Eriksson-Backa, K. (2003). _In sickness and in health: how information and knowledge are related to health behaviour./em> Åbo (Turku), Finland: Åbo Akademi University Press._
*   <a id="fairclough"></a>Fairclough, N. (1992). _Discourse and social change._ Cambridge: Polity Press.
*   <a id="foucault"></a>Foucault, M. (1981). _The history of sexuality. Volume 1: an introduction._ Harmonsworth, Middlesex: Penguin.
*   <a id="given"></a>Given, L.M. (2002). Discursive constructions in the university context: social positioning theory and mature undergraduates' information behaviour. _The New Review of Information Behaviour Research_, **3**, 127-142.
*   <a id="julien"></a>Julien, H., & Given, L.M. (2003). Faculty-librarian relationship in the information literacy context: a content analysis of librarians' expressed attitudes and experiences. _The Canadian Journal of Information and Library Science_, strong>27(3), 65-87.
*   <a id="kuhlthau"></a>Kuhlthau, C.C. (1993). _Seeking meaning: a process approach to library and information services._ Norwood, NJ: Ablex.
*   <a id="lawton"></a>Lawton, J. (2003). Lay experience of health and illness: past research and future agendas. _Sociology of Health and Illness_, **25**(1), 23-40.
*   <a id="mckenzie2002"></a>McKenzie, P.J. (2002). Communication barriers and information-seeking counterstrategies in accounts of practitioner-patient encounters. _Library & Information Science Research_, **24**(1), 31-47.
*   <a id="mckenzie2004"></a>McKenzie, P.J.(2004). Positioning theory and the negotiation of information needs in clinical midwifery setting. _Journal of the American Society for Information Science and Technology_, **55**(8), 685-694.
*   <a id="parker"></a>Parker, I. (1992). _Discourse dynamics: critical analysis for social and individual psychology._ London: Routledge.
*   <a id="pettigrew"></a>Pettigrew, K.E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behavior among attendees at community clinics. _Information Processing & Management_, **35**(6), 801-817.
*   <a id="potter"></a>Potter, J., & Wetherell, M. (1987). _Discourse and social psychology: beyond attitudes and behaviour._ London: Sage.
*   <a id="radley"></a>Radley, A. (1994). _Making sense of illness: the social psychology of health and disease._ London: Sage.
*   <a id="silverman1987"></a>Silverman, D. (1987). _Communication and medical practice: social relations in the clinic._ London: Sage.
*   <a id="silverman1993"></a>Silverman, D. (1993). _Interpreting qualitative data: methods of analysing talk, text and interaction._ London: Sage.
*   <a id="talja1999a"></a>Talja, S. (1999). Analyzing qualitative interview data: the discourse analytic method. _Library & Information Science Research_, **21**(4), 459-477.
*   <a id="talja1999b"></a>Talja, S., Keso, H., & Pietil?inen, T. (1999). The production of context in information seeking research: a metatheoretical view. _Information Processing & Management_, **35**(6), 751-763.
*   <a id="tuominen1992"></a>Tuominen, K. (1992). _Arkielämän tiedonhankinta: Nokia Mobile Phones Oy:n työntekijöiden arkielämän tiedontarpeet, -hankinta ja -käyttö. [Everyday life information seeking: non-work information needs, seeking and use of the workers in Nokia Mobile Phones Ltd.]_ Tampere, Finland: University of Tampere.
*   <a id="tuominen1997a"></a>Tuominen, K. (1997). User-centered discourse: an analysis of the subject positions of the user and the librarian. _The Library Quarterly_, **67**(4), 350-371.
*   <a id="tuominen2001"></a>Tuominen, K. (2001). _[Tiedon muodostus ja virtuaalikirjaston rakentaminen: konstruktionistinen analyysi](http://acta.uta.fi/pdf/951-44-5112-0.pdf) [Knowledge formation and digital library design: a constructionist analysis]._ Tampere, Finland: University of Tampere. (Acta Electronica Universitatis Tamperensis 113) Retrieved 30 June, 2004 from http://acta.uta.fi/pdf/951-44-5112-0.pdf
*   <a id="tuominen1997b"></a>Tuominen, K. & Savolainen, R. (1997). Social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information Seeking in Context: Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts, 14-16 August, 1996, Tampere, Finland_ (pp. 81-96). London: Taylor Graham.
*   <a id="tuominen2002"></a>Tuominen, K., Talja, S. & Savolainen, R. (2002). Discourse, cognition and reality: towards a social constructionist metatheory for Library and Information Science. In H. Bruce, R. Fidel, P. Ingwersen & P. Vakkari (Eds.), _Emerging frameworks and methods, COLIS 4: proceedings of the fourth international conference on conceptions of Library and Information Science_ (pp. 271-283). Greenwood Village: Libraries Unlimited.
*   <a id="williams"></a>Williams, G. (1984). The genesis of chronic illness: narrative reconstruction. _Sociology of Health and Illness._ **6**(2), 175-200.
*   <a id="wilson"></a>Wilson, P. (1983). _Second-hand knowledge: an inquiry into cognitive authority._ Westport: Greenwood press.