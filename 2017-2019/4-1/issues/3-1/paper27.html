<!DOCTYPE html>
<html lang="en">

<head>
	<title>Beyond the on-line library - the Learning Environment</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="keywords"
		content="networked learner support, on-line learning, online, distance education, telematics, university, academic, library, information literacy">
	<meta name="description"
		content="This paper describes a collaborative project which has defined requirements and a model for holistic networked learner support. Based on these requirements, a working prototype for a Learning Environment has been developed. Technical, organisational and business issues which have arisen are discussed. ">
	<meta name="rating" content="Mature">
	<meta name="robots" content="All">
	<meta name="VW96.objecttype" content="Document">
	<link rel="schema.vw96" href="http://vancouver-webpages.com/META/VW96-schema.html">
	<meta name="DC.Title" content="Beyond the on-line library - the Learning Environment">
	<meta name="DC.Creator" content="Bob Banks">
	<meta name="DC.Subject" content="Beyond the on-line library - the Learning Environment">
	<meta name="DC.Description"
		content="This paper describes a collaborative project which has   defined requirements and a model for holistic networked learner support. Based   on these requirements, a working prototype for a Learning Environment has been   developed. Technical, organisational and business issues which have arisen are  discussed. ">
	<meta name="DC.Publisher" content="Information Research">
	<meta name="DC.Coverage.PlaceName" content="Global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4>Information Research, Vol. 3 No. 1, July 1997</h4>
	<h1>Beyond the on-line library - the Learning Environment</h1>
	<h4><a href="mailto:bbanks@fdgroup.co.uk">Bob Banks</a></h4>
	<p>Fretwell Downing Informatics<br>
		Sheffield, U.K.</p>
	<h4>Abstract</h4>
	<blockquote>
		<p>This paper describes a collaborative project which has defined requirements and a model for holistic
			networked learner support. Based on these requirements, a working prototype for a Learning Environment has
			been developed. Technical, organisational and business issues which have arisen are discussed.</p>
	</blockquote>
	<h2>Introduction</h2>
	<blockquote>
		<p><em>&quot;The library, as we know it, is obsolescent ... The university, as we know it, is obsolescent ...
				Your kind [ Librarians], and mine [Universities], are clearly at some kind of fork in our road ... We
				must either lead, follow, or get out of the way!&quot;</em> :<br>
			N. Langenburg, (1)</p>
	</blockquote>
	<p>In the brave new on-line world facing libraries and education, an important component is the availability of
		learning materials and IT support for access to these materials. However, to fully support learners, yet more is
		needed.</p>
	<p>In general terms, the &quot;more&quot; refers to the structure within which learning takes place for each learner
		- commonly known as the curriculum - and support for each student as he or she progresses through their
		learning. How best to provide this holistic concept of networked learner support is a complex question with
		educational, organisational and technical components, to which we do not yet have the answers.</p>
	<p>To help derive some answers the Learning Environment Club was formed. The club consisted of eleven Further
		Education colleges, together with a software supplier and some associate institutions (2). Whilst the focus was
		on FE, the principles of the Club's work are applicable to all sectors of education. The remit was to:</p>
	<ul>
		<li>Ascertain key requirements for successful online support of the entire learning process.</li>
		<li>Develop and evaluate prototype Learning Environment software which fulfils these requirements.</li>
		<li>Identify the best ways of fitting this kind of technology into the institutions' framework to maximise
			benefits and minimise problems.</li>
	</ul>
	<p>The Club has run from July, 1995 to May, 1997, and trialling is continuing through to 1998.</p>
	<p>There has been a fair amount of other work addressing various aspects of online learning. The main
		differentiating feature of the Learning Environment collaboration has been our holistic approach, with the
		online resource base at the centre of the model.</p>
	<h2>Key Requirements which were Identified</h2>
	<p>These requirements were derived in a series of workshops with Club members (a mixture of information specialists
		(librarians), tutors, senior college managers, IT support people and curriculum planners):</p>
	<ol>
		<li>Full support for a resource-based, student-centred open learning model. This implies:
			<ol type="a">
				<li>Making learning materials available to learners in two ways:
					<ul>
						<li>Through research. The learner locates them him/herself - through searching and browsing -
							with the
							resource itself available online - not just the catalogue record (as well as retaining
							access to books,
							videos etc. which are not held online).</li>
						<li>Guided learning - structured direct access to the learning materials which are at the core
							of each
							learner's programme of study. This includes both standard materials for the programme of
							learning which
							the learner is pursuing, and materials added in by a tutor to meet the individual needs of a
							learner.</li>
					</ul>
				</li>
				<li>Further extending support to the entire learning process, which was defined for our purposes in
					figure 1.
					(See also (3).<br>
					<img src="bnkfig1.gif" alt="Figure 1"><br>
					<strong>Fig. 1 - Learning model for the individual learner</strong><br>
					In this model, planning and reflection, with the student taking responsibility for his or her own
					learning are
					seen as integral to learning.</li>
				<li>
					Embedding online communication / discussion within the core structure of the learning.</li>
			</ol>
		</li>
		<li>Flexibility is of the essence, both in meeting the individual needs of learners, and adapting the
			institution's offering rapidly as needs change. This means meeting the needs of individual learners by,
			for example, support for individually tailored learning programmes, with the capability to provide the
			learning resources that are just right for a particular individual, and to change these immediately as
			the needs of the individual (or group) change.<br>
			It also implies adapting the institution's offering as needs change - not just on a fixed annual cycle.
			So, for example, existing courses can quickly be adapted to meet the needs of a customer organisation
			for a particular kind of training for it's workforce.</li>
		<li>
			To achieve this, support for the learner is likely to be provided by a team which may comprise (for
			example) a curriculum area specialist, a resource centre staff member (or librarian / information
			specialist), a personal tutor, and a workplace training manager. There is hope that a new, predominantly
			online, support regime may work better than traditional arrangements, through:
			<ul>
				<li>One-to-one asynchronous access to the right support person for the student the moment questions
					arise
					through (for example) integrated e-mail. Rather than having to wait for next week's tutorial with
					the one
					member of staff who is assigned to him or her, the student can direct a question at the appropriate
					person(s) - e.g. curriculum specialist, resource centre staff, or (importantly!) fellow students,
					and hope
					to have a response within a day or two.</li>
				<li>Availability of up-to-the-minute information on student progress to the team of tutors, with the
					automated
					ability to focus in on problem areas - e.g. overdue assignments - enabling tutors to spot any
					problems as
					soon as they arise. Effective support for individual student tracking (4) was seen as very
					important.</li>
			</ul>
		</li>
		<li>
			None of this is possible without the right learning materials being available. Generally speaking, it will
			be impossible for staff to develop the bulk of the materials for a course themselves. The model has to be
			sharing and re-use of materials, both within an institution, and between institutions. The learning
			institution, and its library, should be playing a central role of broker of learning objects.<br>
			<strong>Key to this are:</strong>
			<ul>
				<li>Effective and flexible search facilities for locating the right materials. (Also, ideally, automatic
					notification facilities when new relevant materials become available.)</li>
				<li>The added value obtained by cataloguing materials for optimum accessibility. This principle extends
					to
					external resources - such as Web pages etc.</li>
				<li>The design of materials for re-use. This encompasses both adaptation of materials for new purposes
					and also
					use of materials in a number of contexts. For example, a resource may relate to a number of
					curriculum areas
					and also to key skills in number, communication, etc..</li>
				<li>The appropriate level of quality assurance for an institution's materials.<br>
					The ability to automatically monitor usage of resources to evaluate their effectiveness and help
					plan future
					acquisitions is important.</li>
			</ul>
		</li>
		<li>Support for students (and staff) working from a variety of locations, such as home, the workplace, community
			centres, or open learning centres in the institution.</li>
	</ol>

	<h2>Learning Environment Architecture and Implementation</h2>
	<p>To fulfil the requirements for integrated support for the entire learning process, a prototype online Learning
		Environment was designed and built in a number of phases, with feedback from club members at each stage. The
		starting point was OLIB(5), an existing commercial library / online resource management system</p>
	<p>An <strong>information model</strong> was developed (figure 2), validated with members of the club, and
		implemented. Because of the methodology and software toolset used (6), rapid incremental development was
		possible, with comments from users feeding back quickly into the model and the software. In the model, the ovals
		denote the main kinds of information held (referred to as &quot;domains&quot;). The lines denote links between
		them.</p>
	<p><img src="bnkfig2.gif" alt="Figure 2"><br>
		<strong>Figure 2 - Learning Environment Information Mode</strong></p>
	<p>Some significant points are:</p>
	<ul>
		<li>
			The curriculum model is split into delivery and assessment strands, following recommended practice in FE
			(7, 8) enabling greater flexibility in meeting students' needs.
		</li>
		<li>
			The model supports active learning, through &quot;activities&quot; which each student undertakes
			(individually or in a team), and which are supported by learning resources.
		</li>
		<li>
			The model supports competency based assessment (as well as other forms). The units can be broken down
			into sub-units, sub-sub-units etc., all of which can be marked as achieved as appropriate. This will be
			when the appropriate evidence (either activities or evidence for Accreditation of Prior Learning) is
			successfully completed and / or when all the sub-units have been achieved. In the current model there is
			a basic degree of automation of this process of marking chunks of assessment as achieved. This could be
			extended to provide fairly comprehensive automation.
		</li>
		<li>
			Each activity may provide evidence for any number of chunks of assessment. For example, a survey of
			patient needs could provide evidence for the achievement of elements in a mandatory Health and Social
			Care unit, but also for elements of key skills units in number and IT.
		</li>
		<li>
			When students enrol on a programme (standard or individualised), individual &quot;copies&quot; of the
			programme, delivery modules, assessment units and activities are automatically made. This means that:
			<ul>
				<li>
					The progress of each student is recorded individually
				</li>
				<li>
					Each student has their own copy of each activity, which they can complete online as appropriate.
				</li>
				<li>
					Each student's programme and modules can be individually tailored - e.g. providing extra activities
					to
					reinforce areas where the student has difficulties, or to challenge and extend able students.
				</li>
				<li>
					Re-use of resources (including activities) across the whole range of the curriculum is supported.
				</li>
			</ul>
		</li>

	</ul>
	<p><strong>The architecture</strong> incorporates the concept of brokerage of learning objects (9) (fig. 3).
		Libraries and librarians have always acted as information brokers between learners, information repositories and
		information providers. There is a current move to extend this concept (10) to the educational institution as a
		whole, particularly in the context of electronic information. Rather than being imparters of a fixed body of
		knowledge to a set of students, learning support staff are being seen as facilitators of learning who
		responsively direct students to appropriate learning resources and monitor their progress.</p>
	<p>This brokerage adds value to the information by:</p>
	<ul>
		<li>locating it in the context of an individual student's (or group's) programme of study and progression, using
			the expertise of the tutor.</li>
		<li>linking it to assessment and accreditation for each student.</li>
		<li>supporting dialogue with the student to help understanding.</li>
	</ul>
	<p><img src="bnkfig3.gif" alt="Figure 3"><br>
		<strong>Figure 3: Learning Environment as Learning Resource broker</strong></p>
	<p>In the Learning Environment, there are currently two prime routes to resources held outside of the institution's
		boundary, via World Wide Web URLs, or via Z39.50. The latter is an international standard for information
		exchange between heterogeneous information bases, which allows remote information bases to be queried, and
		result sets returned in standard format. This capability is being extended in the Learning Environment in two
		European-funded projects: Renaissance and Universe (11). A third related project, DALI (11), has investigated
		and implemented a charging model for document delivery from remote sites - a crucial component if this kind of
		flexible remote access to resources is to become widespread.</p>
	<p>The <strong>User interface</strong> for students, tutors and information managers is based around three modes of
		interaction in line with requirement 1a above.</p>
	<ul>
		<li>Structured mode, through the users' &quot;workspace&quot; where the core information they will most commonly
			be working with is presented to them. (figure 4).</li>
		<li>Research mode - where the user can formulate queries and explore freely (if they have permissions) the
			entire information base (figure 5).</li>
		<li>Online communication in context. This is currently is via integrated e-mail, but video-conferencing, etc.
			could equally be employed. Also, folders of resources can be created and shared for collaborative learning.
		</li>
	</ul>
	<p><a href="http://InformationR.net/ir/bnkfig4.gif"><img src="bnkfig41.gif" alt="Figure 4"></a><br>
		<strong>Figure 4: &quot;Structured mode&quot; - typical initial view of a learner's workspace</strong></p>
	<p><a href="http://InformationR.net/ir/bnkfig5.gif"><img src="bnkfig51.gif" alt="Figure 5"></a><br>
		<strong>Figure 5: &quot;Research mode&quot; - a query of the database</strong></p>
	<p><strong>Resources</strong> can be located within or outside the LE database. For example, they can be:</p>
	<ul>
		<li>Word-processed documents - either in read-only or (for activities) in updateable mode.</li>
		<li>Adobe Acrobat documents. These allow the embedding of multimedia components, and hyperlinks. (As indeed -
			though to an arguably lesser extent to most modern word-processors.)</li>
		<li>Multimedia learning resources - which may have been authored using a variety of packages.</li>
		<li>World Wide Web pages anywhere on the Internet. The same considerations that apply to any educational use of
			the Web obviously apply here, and must be addressed in determining an institution's Internet policy.</li>
		<li>Web pages on the institution's internal intranet (some of which may be copies of pages obtained from the
			global Internet - but held locally to save on the phone bill, and to avoid the perceived dangers to innocent
			students lurking out there on the Internet.</li>
		<li>Etc. etc.</li>
	</ul>
	<p>In terms of physical location, resources may be held, for example:</p>
	<ul>
		<li>Within the LE database</li>
		<li>On a local area network server (which could be a dedicated document server). This may provide advantages in
			speed of access, but may limit the visibility of the resources (e.g. to users connected over a local area as
			opposed to wide area network.)</li>
		<li>On the Internet or intranet - referenced in standard Web fashion via a URL (Universal Resource Locator)</li>
		<li>On a CD-ROM, distributed to the students enrolled on a particular programme, and inserted into their own
			CD-ROM drive. This provides advantages of fast loading for multimedia resources without the expense of high
			bandwidth network connections. The obvious disadvantage is the inability to update resources without sending
			out new copies of the CD-ROM, but this may not matter.</li>
		<li>On CD-ROMs mounted on the local network on a CD-ROM jukebox.</li>
	</ul>
	<p>The choice of the mixture of strategies to be adopted for mounting resources is one of the major strategic
		questions an institution faces in developing online learning.</p>
	<p>The provision for online <strong>communication / discussion</strong> with tutors and with fellow learners may
		depend on learning style, educational model, confidence of students, degree of support required, etc. Examples
		of a structure would be:</p>
	<ul>
		<li>At the end of each module of delivery - e.g. e-mailing the tutor(s) to comment on the learning which has
			taken place in the module being the final activity in the module.</li>
		<li>Ad hoc - when problems or inspirations arise. Almost certainly, this will be important.</li>
		<li>Once a week (This is traditional but perhaps rarely appropriate!)</li>
	</ul>
	<p>Incorporating discussion with fellow learners in the process may be as important as (or more important than!)
		discussion with tutors.</p>
	<p>The Learning Environment was implemented in a client - server environment, supporting access over local or wide
		area networks (tcp-ip). An Oracle database on the central server(s) holds the information. The client runs on
		Windows PCs. In principle, the application could equally run over the World Wide Web, with a web browser
		providing the client, and (for example) Oracle Web Server providing access to the database. Extension to this
		environment is under consideration.</p>
	<h2>Strategies for Networked Learner Support</h2>
	<p>In the Learning Environment Club, there have typically been three overall roles involved in Learner Support:</p>
	<ul>
		<li>Subject Tutor - the specialist in a curriculum area</li>
		<li>Progression Tutor - the tutor with an overall view of the student's learning needs and progress, who
			provides counselling around this, etc.</li>
		<li>Information Specialist - also referred to in this paper as &quot;Resource Centre Staff&quot;. Providing
			support around information / resource needs.</li>
	</ul>
	<p>Often, a single person fulfils more than one of these roles, in part or fully.</p>
	<p>Each of these are involved in the provision of three types of support:</p>
	<ul>
		<li>Responsive. Providing information and advice in response to students' requests for support.</li>
		<li>Proactive. Providing input for students, either in a scheduled fashion, or as a result of the way a student
			is progressing (falling behind, showing misunderstanding in assignments, not being stretched, etc.)</li>
		<li>Structural. This is the traditional role of a librarian, but extended into an online context and with more
			focus on how resources fit into the curriculum. This covers the management of resources (including delivery
			modules, assessment units etc.) to ensure optimal accessibility. In our case, it also covers the development
			of new learning programmes; a role which may well be performed by a different person.</li>
	</ul>
	<p>The way that individual staff fit into this matrix will differ between institutions. The description of typical
		uses of the learning environment, below will make it clear how these types of support operate.</p>
	<h2>Typical Uses of the Learning Environment</h2>
	<p>These are described for the four typical types of user shown in figure 3.</p>
	<ol>
		<li>
			<strong>By a Learner</strong>
			<p>Tony is enrolled on a modular degree course. He generally works from home - in the evenings or whilst his
				children are at nursery. However, fellow students on the course who have no computer at home work at a
				number of
				open learning centres, on or off campus, or at &quot;Electronic Village Halls&quot; near their homes.
			</p>
			<p>Before starting the course, Tony had a number of meetings with tutors to:</p>

			<ul>
				<li>Identify his learning needs.</li>
				<li>Decide on his learning goals .</li>
				<li>Formulate an action plan (12) for his learning, and decide on the programme / modules on which he is
					going to enrol.
				</li>
			</ul>
			<p>This information is maintained (as word-processed documents) within the learning environment, and can be
				periodically revisited and reassessed.</p>
			<p>In a typical learning session, Tony may connect in, and initially check his mail, for example picking up
				a reply
				from his tutor about a problem he had yesterday, a couple of responses to points he made in group
				discussion,
				and a pointer to a resource from a resource centre staff member in response to a query. He then looks at
				his
				learning environment workspace, where the list of activities for the module he is currently working on
				are shown
				to him (fig. 4). He has just finished one activity, and starts on the next, which is marked as current.
				It may
				be, for example, a word processed activity sheet, which Tony completes with help from the list of
				learning
				resources attached. These could be, for example, a web page, a multimedia Acrobat document, and an
				online video.
				Postits, supplied by the module tutor, and visible just to all the students in this group, explain how
				each
				resource might best be used in this context.</p>
			<p>In this case, his tutor has added an additional learning resource to help him with the statistical
				analysis of
				the activity results, which he has been finding difficult. A &quot;postit&quot; against the resource
				explains
				how to use it. (His tutor found it by a search of the college resource bank held on the Learning
				Environment.)
			</p>
			<p>When Tony has a problem, he can instantly e-mail his tutor, or others in his support team, such as the
				resource
				centre staff. To do this, he clicks on the right mouse button over the activity, which brings up the
				action
				menu. He selects &quot;e-mail tutor&quot; which brings up his e-mail application, with the
				&quot;To&quot; box
				automatically populated with the address of the tutor for this module, and the &quot;About&quot; box
				populated
				with the name of the activity.</p>
			<p>When the activity is completed, Tony clicks on &quot;Completed&quot; on the action menu, and the next
				activity
				automatically becomes current, whilst this one appears in the tutor's folder of activities for marking.
			</p>
			<p>Tony can also freely research the resource base to explore other items that might be relevant.</p>
			<p>In this example, Tony is pursuing a fairly standard college course, although there may be a good deal of
				variation between course members in which optional modules they choose. However, learning programmes may
				equally
				be totally individual, on a &quot;pick and mix&quot; basis. There are many possible variations between
				the
				extremes of standard college courses and total individuality. For example, many colleges give students
				diagnostic tests in key skills when they enrol. These can be presented as activities, marked on-line,
				and based
				on the results, supplementary chunks of work can be dropped into the student's programme, to catch up on
				areas
				of deficiency.</p>
			<p>From time to time Tony (and his tutors) will wish to see how he is progressing with the assessment side
				of
				things, which is visible to both, and is automatically updated as relevant assignments are marked as
				successful.
			</p>
		</li>
		<li>
			<p><strong>By the learning support staff (tutors, resource centre staff etc.)</strong></p>
			<p>The student's support team monitors progress via</p>
			<ul>
				<li>direct communication with the student (and each other) - e.g. via integrated e-mail.</li>
				<li>a window onto each student's progress and work in their own workspaces. This may, for example,
					provide
					an automatically updated list of overdue assignments, allowing the tutor to home in immediately on
					problems as they arise, and respond and resolve the problem before the student become disillusioned,
					drops out etc.</li>
			</ul>
			<p>Also, they may locate and add in resources to meet individual student's needs, using the capabilities
				described
				in (3) below.</p>
		</li>
		<li>
			<p><strong>By course designers</strong></p>
			<p>Location of appropriate materials, and linking them together to provide structured chunks of curriculum
				is
				supported by:</p>
			<ul>
				<li>A wide range of search types, with access via date, subject headings, full text retrieval,
					curriculum
					area, author, etc..</li>
				<li>Cross-referencing and browsing capabilities.</li>
				<li>Resource sharing via static and dynamic folders.</li>
				<li>Drag and drop authoring and structuring capabilities for developing the catalogue and authoring
					modules
					of curriculum.</li>
			</ul>
		</li>
		<li>
			<p><strong>By resource / information managers</strong></p>
			<p>The management of this information is supported online by the standard mechanisms one would expect in a
				library
				management system. Authority controlled subject headings, standard classification schemes, etc. can be
				used to
				manage and increase accessibility of information.</p>
		</li>
	</ol>

	<h2>Key Issues for the future</h2>
	<p>These are largely organisational and cultural rather than technical, for example:</p>
	<p>The sourcing, management and sharing of learning resources. Some things that may help are:</p>
	<ul>
		<li>Arrangements for sharing between institutions.</li>
		<li>Third Party Brokers. In many cases, libraries are ideally place to play this role. Another example is
			the Web Site Unit Exchange project - a QUILT initiative to make available Open College Federation
			accreditation units developed across U.K. colleges (13).</li>
		<li>A &quot;component architecture&quot; for resources to facilitate their re-use.</li>
		<li>A clear analysis of the economics of buying in materials versus developing them internally, the
			investment required and the potential returns. Clearly educational institutions may play the role of
			resource providers (publishers) as well as consumers, but a clear analysis of how this fits the
			institution's core business is required.</li>
	</ul>
	<p>How to &quot;migrate&quot; new approaches and technology into an institution. Some critical success factors may
		be:</p>
	<ul>
		<li>An effective high-level champion combined with nurturing for staff at the grass-roots.</li>
		<li>Catalyst team or staff member, with sufficient resources to support their role.</li>
		<li>Demonstration of early success in an area that is small enough to be achievable, but significant enough
			to excite enthusiasm.</li>
		<li>Buy-in from key stakeholders.</li>
		<li>Perception that it will make participants' jobs better in some way.</li>
		<li>Appropriate focus.</li>
		<li>Defined migration paths from initial small-scale success to later widespread achievements.</li>
		<li>Defined mechanisms for taking feedback and adapting the introduction strategy based on successes and
			problems.</li>
		<li>A good fit with the institution's (evolving) culture.</li>
		<li>Technology that is reliable, has a good user interface and evokes confidence.</li>
		<li>Good provision for staff training.</li>
	</ul>

	<p>Forthcoming evaluation of the pilot use of the Learning Environment in colleges will provide further insights.
	</p>
	<h2>References</h2>
	<ul>
		<li>
			<p>Langenburg, D.N (1993) from New Technologies &amp; New Directions - Proceedings from the Symposium on
				Scholarly Communication; ed. Boynton &amp; Creth; Meckler, [pp 39-40]</p>
		</li>
		<li>
			<p>David Kay (1996) <strong>'The Learning Environment Club: Year 1 Report'</strong>: Fretwell Downing
				Informatics Internal Document</p>
		</li>
		<li>
			<p>The members of the club were Fretwell Downing Informatics along with Broxtowe College, Clarendon College,
				Doncaster College, Manchester College of Arts and Technology, Newark &amp; Sherwood College, Sheffield
				College, Solihull College, Thomas Rotherham College, Trowbridge College, West Cheshire College, Wirral
				Metropolitan College, together with some associates: The National Council for Educational Technology
				(NCET), South Yorkshire Open College Federation, Sheffield Hallam University.</p>
		</li>
		<li>
			<p>Cowham T. (1997) <strong>'Information and Learning Technology: A Development Handbook'</strong> FE
				Matters 1(15) FEDA (Further Education Development Agency, Coombe Lodge, Blagdon, Bristol BS18 6RG, U.K.
				)</p>
		</li>
		<li>
			<p>Donovan K. (1996) <strong>'Student Tracking'</strong> Developing FE 1(1). FEDA</p>
		</li>
		<li>
			<p>Fretwell Downing Informatics (1997) The OLIB Library Management System</p>
		</li>
		<li>
			<p>Banks R. et.al. (1996) <strong>'Support for co-operative work at the heart of the changing
					enterprise'</strong> in &quot;IT Support in the Productive Workplace&quot; Chapman J. (ed.). Stanley
				Thornes / UNICOM.</p>
		</li>
		<li>
			<p>FEU (1995) <strong>'A framework for credit: a common framework for post-14 education and training for the
					21st century.</strong> FEU / FEDA</p>
		</li>
		<li>
			<p>Fforwm (1995) <strong>'Student Tracking Project, 1995 Report'.</strong> Ffforwm</p>
		</li>
		<li>
			<p>Gray J. (1997) - Presentation at QUILT launch (FEDA, Feb., 1997)</p>
		</li>
		<li>
			<p>Hamalainen (1996) <strong>'The Course Broker Model'</strong> in 'The Road to the Information Society',
				European Union.</p>
		</li>
		<li>
			<p>Reports available from: Fretwell Downing Informatics, Brincliffe House, 861 Ecclesall Rd., Sheffield S11
				7AE, U.K.</p>
		</li>
		<li>
			<p>FEU (Now FEDA) (1994) <strong>'Maximising Potential through Individual Action Planning'.</strong> FEU /
				FEDA</p>
		</li>
		<li>
			<p>Web Site Unit Exchange Project. NEMAP, SE Derbyshire College, Long Eaton, Nottingham, NG10 4QN, U.K.</p>
		</li>
	</ul>
	<h5>Paper presented at the 2nd International
		Symposium on Networked Learner Support 23rd-24th June 1997, Sheffield, England</h5>
	<p><strong>New services, roles and partnerships for the on-line learning environment</strong></p>
</body>

</html>