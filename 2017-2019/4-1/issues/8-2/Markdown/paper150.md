#### Information Research, Vol. 8 No. 2, January 2003

# Freshmen's use of library electronic resources and self-efficacy

#### [Micaela Waldman](mailto:Micaela_Waldman@baruch.cuny.edu)  
Baruch College Library  
The City University of New York  
New York, USA

#### **Abstract**

> To encourage students' use of the library, and in particular of its electronic resources, we need to understand what factors encourage students to seek out information in the library setting. Research has shown that self-efficacy influences academic achievement. This paper looks at the role self-efficacy plays in their search for information and use of the library's electronic resources, by surveying a class of freshmen at Baruch College. Their library and computer use were analyzed and correlated with their self-efficacy scores. Through statistical analysis, we found that use of the library correlated to the students' use of the library's electronic resources. We also found out that students who express an interest in learning about the library's electronic resources will be more likely to have higher self-efficacy.

## Introduction.

Information professionals have long sought to comprehend what factors are relevant in encouraging a person to seek out information. More recently, a particular focus of inquiry has been on those factors that play a role in deciding to use the library and its resources as a place to seek information (whether physically or virtually) as opposed to just surfing the Internet. These inquiries assume an even greater importance in light of the fact that more people are using the Internet to find information they need, information that is unmediated by the library ([Kibirge, 2000](#kib00)).

Informed library users know that libraries have resources that are more comprehensive and scholarly than most Web sites provide. Libraries provide access to scholarly literature that, as a rule, is not freely available on the Web, or may not be online at all. Often, it is in college that users become aware of libraries' resources, usually while having to write research papers. Assuming that on average most students face the same number and type of papers and assignments during their college career, it is critical to understand what makes one student use the library's resources (print and/or electronic) while another will not think of the library as a place to find specialized resources for their papers, especially in the early parts of their college careers.

One obstacle to the use of a library's resources, and in particular its electronic resources, is that they are not seen as being straightforward. In contrast to an Internet search engine, where a single keyword search will usually result in thousands of hits, no matter what the topic, in the library, students have to choose a particular database and be more selective in the search words they use. Moreover, database subjects often overlap, with differences in dates, journal and subjects covered, and whether the material is full-text or not. In addition, the library may have a print subscription to a certain title that is not full-text electronically, or the title may be accessible full-text through another database than the one originally searched. Therefore, not only do students have to find the relevant citations, but they also have to know how to locate the article after that. This means juggling many screens, many technologies, multi-tasking electronic jobs, and of course, knowing where to look for all this necessary information. Lastly, there is the additional confusion that more and more library databases use Web-based technologies. Because the interface is seamless there does not seem to be a visible, on the screen, difference between Web-based library resources and general Web-based resources. All of the above also assumes the student is proficient in the use of computers. It is quite clear that searching for information has become "inexorably linked to computer technology." ([Jacobson, 1991: 268](#jac91)).

Understanding how students navigate this maze of resources is important in helping us to develop and assess pedagogy designed to instruct our students in library usage. Students are more and more Web-savvy ([Kibirge, 2000](#kib00)), many of them having been brought up around computers and the Internet. However, they matriculate with a diversity of computer and Web-searching skills and experience. Students may not have been exposed to library resources, or not be aware of which resources a library might have, or how to make use of them. It is therefore of interest to us to try and understand what characteristics will make one student branch out and explore library resources, while another one might not.

A study of undergraduates showed that they looked for the fastest way that would lead to satisfactory results when doing research, going for electronic information sources first ([Valentine, 1993](#val93)). These students felt uncomfortable, however, asking for help in using the library and spent frustrating hours trying to find information. Currently, with the explosion of full-text resources, it would seem even easier for the student to find a full-text database and select the articles, regardless of whether they would have been the most appropriate for their research. Not all students take this route, however.

In this study we will examine some factors that correlate with students' usage of the library's electronic resources. While we will examine the relationship of age and gender to usage, we will also focus on the concept of self-efficacy, a person's belief in their ability to attain desired outcomes ([Bandura, 1997](#ban97)).

## Background Research

This section is an overview of research conducted on usage of library's electronic resources and examines self-efficacy as applied to academic settings.

### Students' Demographics

Demographics often yield important clues as to what factors contribute to undergraduates' use of electronic resources. [Whitmire](#whi01a) (2001a) found that there was a correlation between background characteristics such as gender, race, and initial critical thinking scores and library use during the freshmen and sophomore years, although these played less of an important role in the junior year. An interesting predictor of college library use was the degree to which these students used the library while in high school ([Whitmire, 2001a](#whi01a)).

#### Age

Age is one variable that correlates with comfort with computers and use of electronic resources. Younger generations have been brought up with computers; many do not even remember a time when computers were not around. Older and returning students may not have had as much exposure to computers, resulting in increased computer anxiety. For example, Laguna and Babcock found that, "...there were significant age differences on the computer task, as measured by older adults making fewer correct decisions and taking longer to make their decisions than younger adults." ([Laguna and Babcock, 1997:323](#lag97)). Studies have also found that the more computer experience people have, the less anxious they are about using computers. [Dyck and Smither, 1994](#dyc94) found this true for younger and older subjects (recruited from senior citizens courses and continuing education courses). Therefore, given similar computer experience, age does not seem to make a difference in people's comfort levels with computers. Medical doctors, who we assume have similar educational backgrounds, showed no difference in usage of electronic resources in terms of sex or age ([Laerum, 2001](#lae01)).

#### Gender

Gender is another relevant factor in examining use of electronic databases. In a study of high school students, it was found that their attitudes towards computers and their computer use tended to vary by gender. This difference, howeve,r tended to diminish with computer experience. ([Sacks, 1993/94)](#sac93). Other studies have found similar results: Dyck and Smither, for example, found that "...when the effects of computer experience were controlled, there were no gender effects." ([Dyck and Smither, 1994: 246](#dyc94)). In a study of 60 college students, [Koohang](#koo86) (1986) found that neither age nor gender was strongly correlated to computer anxiety, computer confidence or liking, but that computer experience was.

While the gender gap relating to computer use seems to be shrinking, several studies have found that there is a gender gap when considering use of the Internet, and that gender is a major predictor of Internet use and attitudes; males seem to enjoy browsing on the Internet for enjoyment while females tend to only use it for work-related purposes ([Ford and Miller, 1996:188](#for96)). In a study of highly successful students, Ford, _et al._ found that "...females tended to experience more difficulty finding information on-line, to feel less competent and comfortable using the Internet, to use the Internet less frequently than males, and to make use of a less varied set of Internet applications." ([Ford, _et al._, 2001: 1061](#for01)). [Majid](#maj99) (1999) found a similar result in studying faculty members: while males tended to have better computing skills than females, age and year of obtaining highest educational qualifications were also important factors in establishing computer skills.

### Faculty influence

A study of faculty's use of electronic resources found that it was "influenced by such factors as computing skills of academics, their age and gender." ([Majid, 1999: 110](#maj99)). They found an especially significant relationship between computing skills and use of electronic resources in the library, including the Online Public Access Computer or OPAC, the library's online catalogue ([Majid, 1999](#maj99): 9). Faculty with higher computing skills were not only more likely to use and be familiar with their library's electronic resources, including the OPAC, but also tended to use the Internet more frequently ([Majid, 1999](#maj99)).

This can have implications for how much encouragement students will get from their professors when completing assignments. Anyone having worked at the reference desk can attest that professors will send students to use those resources with which they themselves are familiar, regardless of whether the library still has that particular resource, or whether it is still the most appropriate one.

### Frequency of computer use

Computer anxiety is another contributing reason that discourages users from taking advantage of library resources and services ([Brosnan, 1998](#bro98)). Because much of today's information technology makes use of computers, it is important to investigate the relationship between computer use by students and their use of electronic resources in the library. For example, a study of faculty at Western Michigan University showed that frequent users of the library tended to make greater use of computer applications and the library's databases than infrequent users, and also reported higher expertise in using these applications ([Meer, 1997](#mee97)). Moreover, the level of computing and Internet experience with which students enter higher education might influence whether or not they will use the library's electronic resources ([McGuigan, 2001](#mcg01)).

### Library use

Students' library use is another variable influencing the use of electronic resources. It is reasonable to assume that the more an undergraduate uses the library, the more familiar the student will be with its resources, including its electronic resources. However, if students use the library primarily as a quiet and convenient place to study, they may not be aware of its resources at all, as compared to the student who never puts a foot in the library. Several studies have shown that undergraduates use the library mostly as a place to study and make photocopies, but do not make great use of some of the available library services, such as interlibrary loan and the reference desk ([Whitmire, 2001b](#whi01b)).

A longitudinal study of undergraduates by Whitmire ([2001a,](#whi01a) [2001b](#whi01b)) shows that library use by undergraduates changed according to their status. In particular she found that library use increased from freshmen to junior years, especially in the use of computers for research, although this might also be because juniors have more assignments where they need to do research.

### Self-efficacy

The degree of self-efficacy is another variable that appears to influence use of the library's electronic resources. Self-efficacy has been used in a variety of fields since its introduction in 1977 by Alberto Bandura,

According to Bandura, self-efficacy is part of an individual's "beliefs in one's capability to organize and execute the courses of action required to manage prospective situations." ([Bandura, 1997: 2](#ban97)). Individuals get their self-efficacy beliefs in several ways. The first and most significant way is by interpreting what they did; self-efficacy is not a static concept, it is continually being actualized in an individual's mind, through what Bandura called "mastery experiences": "Outcomes interpreted as successful raise self-efficacy; those interpreted as failures lower it." ( [Pajares, 1997](#paj97))

Individuals also develop self-efficacy beliefs through "vicarious experiences", by observing how peers have dealt with certain experiences, and from there inferring how they would handle a similar experience. However, this is a less significant way of establishing self-efficacy. A still weaker way self-efficacy beliefs develop is through "verbal persuasions", when someone gives oral encouragement or communicates their confidence that someone else will be able to succeed. Interestingly, "it is usually easier to weaken self-efficacy beliefs through negative appraisals than to strengthen such beliefs through positive encouragement" ([Pajares, 1997](#paj97)). Individuals form their self-efficacy beliefs by incorporating and weighing these factors.

Bandura makes clear, however, that self-efficacy beliefs are beliefs, they are not related to actual skill level, but are relevant in determining how long an individual perseveres in a task, and whether the task will be engaged in at all. In academic settings,

> Students confident in their academic skills expect higher marks on exams and expect the quality of their work to reap benefits. The opposite is also true of those who lack such confidence. Students who doubt their academic ability envision low marks before they begin an exam. ([Pajares, 1997](#paj97)).

Self-efficacy beliefs contribute to effective performance by increasing motivation, task focus, and effort and decreasing anxiety and self-defeating negative thinking ([Bandura, 1997](#ban97)). [Schunk](#sch94) (1994) demonstrated that "When self-efficacy is too low, students will not be motivated to learn" ([Schunk, 1994: 87](#sch94)), therefore different strategies will need to be devised to reach such students.

Much research on self-efficacy has focused on the relationship between self-efficacy and academic achievement. [Pintrich and Garcia](#pin91) (1991) showed that students with higher self-efficacy "use more cognitive and metacognitive strategies and persist longer than those who do not" ([Pajares, 1997](#paj97)).

Of interest to us in this study is research related to academic motivation and self-regulation, and computer use ([Pintrich and Schunk, 1995;](#pin95) [Compeau, 1995](#com95)). The studies focusing on students have explored the relationship of self-efficacy with the choice of college major and career, and with students' performances and achievements ([Pajares, 1997](#paj97)).

It is also important to note that academic self-efficacy beliefs vary according to subject matter, in that students may have high self-efficacy in one subject but not in another: mathematics self-efficacy is independent of writing self-efficacy, depending on their mastery and vicarious experiences in each subject.

> …researchers have reported that students' self-efficacy beliefs are correlated with other motivation constructs and with students' academic performances and achievement. Constructs in these studies have included attributions, goal setting, modeling, problem solving, test and domain-specific anxiety, reward contingencies, self-regulation, social comparisons, strategy training, other self-beliefs and expectancy constructs, and varied academic performances across domains. ([Pajares, 1997](#paj97))

Because "People are generally more interested in performing activities in which they have high self-efficacy" ([Ren, 2000: 323](#ren00)), we can imagine that students with high self-efficacy will be more likely to take advantage of what is around them. That is, if they are familiar and feel comfortable with computers, they will use them, and if they feel that learning about a library's resources will enhance their academic performance, they will learn about them. Research has demonstrated that college students' " computer use and their technology acceptance are positively related to computer self-efficacy. Computer self-efficacy has a direct effect on a person's perception of the ease of computer use, which, in turn, affects the frequency and time of computer use." ([Ren, 2000: 324](#ren00)). Studying self-efficacy in relation to learning Internet skills, [Nahl and Tenopir](#nah96) (1996) also found that higher self-efficacy translated in higher search efficiency, success and satisfaction. In a study of library instruction and self-efficacy, [Ren](#ren00) (2000) also showed a positive correlation between students' self-efficacy and the frequency of use of the library electronic resources.

In studying working adults' information seeking behaviour, [Brown, Challagalla, and Ganesan](#bro01) (2001) found that "employees with high self-efficacy effectively seek, integrate, and use information to increase role clarity and performance, whereas employees with low self-efficacy do not." ([Brown, _et al._, 2001: 1049](#bro01)). It seems reasonable to assume that the same would true of students' information-seeking behaviour.

Bandura also looked at how self-efficacy relates to computer use, linking computer use with educational achievement, especially since computers provide "a ready means for self-directed learning. Disparities in computer skills can create disparities in educational development" ([Bandura, 1997: 434](#ban97)). He goes on to note "Belief in one's efficacy to master computers predicts enrollment in computer courses independently of beliefs about the instrumental benefits of knowing how to use them" ([Bandura, 1997: 435](#ban97)). We might infer therefore that students with a high self-efficacy regarding computers would also be more likely to explore new technologies, software or databases. They would be more likely, for example, to explore a library's Website and find that the library has specialized resources, and they might even try some searches on those resources, without, or with less, prompting from professors and/or librarians and without necessarily taking library workshops.

A "problem" is that the Internet is so easy to use. Indeed "a brief introduction is sufficient for hypertext users to be able to apply minimal search system features to find information." ([Lazonder, _et al._, 2000: 577](#laz00)). However [Hill and Hannafin](#hil97) (1997) showed that search strategies on the Internet tended to be primitive if there was little Internet familiarity, even if there was great subject expertise.

One reason users may choose to investigate library resources might be that:

> The ways in which users choose and move between information services to meet their information needs are expressions of their views about the best information strategy given those information needs. Each user's strategy is formed on the basis of such factors as his or her perceptions of the value of the products delivered by each service, the likelihood of success in using each service, knowledge that the services exists, and the logical dependencies between one service and the other. ([Heine, 2000: 233](#hei00)).

Although there is a correlation between computer use and self-efficacy, and there are some gender differences in computer use, the study by Ford _et al._ found "... no link between female gender and low self-efficacy" ([Ford, _et al._, 2001: 1060](#for01)), that is. it is not the gender variable that determines the degree of self-efficacy.

In this study it is hypothesized that:

1.  <a id="hyp1"></a>Students who have more computer experience will be more likely to use the library's electronic resources and have higher self-efficacy.
2.  <a id="hyp2"></a>Students who use the library's electronic resources from home have higher self-efficacy.
3.  <a id="hyp3"></a>Students who frequent the library more often are more likely to use the library's electronic resources.
4.  <a id="hyp4"></a>Students who use the library's electronic resources will be able to discriminate them from information from the Internet.
5.  <a id="hyp5"></a>Students who express interest in learning about the library's electronic resources will have higher self-efficacy.

## Methods

We surveyed a class of students taking "Introduction to Psychology" at Baruch College, City University of New York. Our data were collected in the Fall semester of 2001, during the last class before examinations. This is a large lecture class with about 500 students, and attracts mostly freshmen. Although the class did not have a library component, students were encouraged to use technology throughout the semester by participating in Blackboard, a distance-learning course management system.

A pilot survey was conducted in the Spring semester of 2001\. That survey had a number of open-ended questions, that were used to determine the close questions we asked in the Fall survey. Although helpful in some areas (for example, to determine what students came to the library for), some of the open-ended questions had a very low answering rate and therefore were not helpful in focusing our final survey's questions (in particular, students did not answer open-ended questions relating to which electronic resources they used.)

Our Fall 2001 survey contained two distinctive parts: a self-efficacy standard measuring instrument and a library component. The survey has been included in [Appendix A](#app).

The self-efficacy survey made use of a Likert scale. Seventeen statements were presented to the students, and they could agree or disagree with them on a five point scale, from 1=Strongly Disagree to 5=Strongly Agree. Thus. a total numerical value can be calculated from all the responses.

The library section consisted of 35 questions. Questions one through eight were concerned with gathering demographic data. Questions nine to 16 focused on whether or not the student used the Internet and what for. Questions 17 to 31 dealt with how the students used the library and its electronic resources. Questions 31 to 35 focused on the how students gather necessary information for their research. Most questions were closed-ended, although some were open-ended.

Survey responses were coded and input into the SPSS statistical package for analysis and the hypotheses presented above were tested using analysis of variance (ANOVA). The ANOVA is used to uncover the effects of independent variables on an interval dependent variable. This procedure employs the statistic (F) to test the statistical significance of the differences among the obtained means of two or more random samples from a given population. The statistic (F) is a ratio, which, if sufficiently larger than 1, indicates that the observed differences among the obtained means are statistically significant. It is important to note here, however, that the samples were not random, which reduces the generalization of our results.

## Results and Discussion

### Demographics

Data from 340 usable surveys showed that the class was composed of 90% freshmen, 54% female students, 77% who were between the ages of 17 and 21, and 47% were working part-time.

This Baruch College class, characteristically, was composed of a rich diversity of students in that 34% of the respondents were White, 18% Asian, 13% Hispanic, and 10% African-American.

This freshman class presents a similar make-up as the Baruch undergraduate population as a whole for the Fall 2001 semester concerning gender, with a preponderance of female students. Out of an enrollment of 12,969 undergraduates, 42.9% were males and 57.1% were females. Baruch's undergraduate population was also very diverse, although the proportions were slightly different: 28.5% White Non-Hispanic, 25.1% were Asian or Pacific-Islander, 18.7% were Black Non-Hispanic, 18.5% Hispanic and 0.1% American Indian or Alaskan Native ( [National Center for Education Statistics, 2001](#nat01)).

### Computer and Internet use

Computers are definitively present in students' lives in that 94% reported having access to a personal computer at home, with 93% saying they own one. For this population of incoming students there is no digital divide. Perhaps because so many students have access to a computer at home, they do not seem fully aware of the computing resources available to them: only 34% said they had access to computers at school, although there are student computer laboratories on campus.

Our incoming students are also very familiar with the Internet: 73% say they access the Internet daily, and an additional 25% at least once a week. In other words, over 97% of students access the Internet weekly or more often. When surfing, 44% look for educational information, 42% for entertainment information, 30% for news, 25% check for sports information, 9% for health information, and 70% report using e-mail often.

While it is rewarding to see that so many students use the Internet for educational purposes, it is important to clearly understand what they deem as educational. Is it Blackboard usage, researching term papers, or word processing?

It is also interesting to note that so few search for health information on the Internet, when other research has shown that a great number of people use the Internet to search for health information ([Lewis and Behana, 2001](#lew01)). Perhaps searching for health information online might be linked to age of the researcher.

### Library use.

Data revealed that 67% visit the library weekly, and only 1% has never been to the library [Table 1]. Asked what they did at the library, 80% reported studying, 38% to do research, 33% to sleep, 30% to socialize, 24% to use the library's electronic resources, 22% to check books out and 21% to e-mail or chat [Table 2]. Even though some of these areas overlap (research and use of electronic resources, for example), it is clear that these students are mostly using the library as a place, not to make use of the library's resources or services.

<figure>

![Figure 1](../p150fig1.gif)</figure>



<figure>

![Figure 2](../p150fig2.gif)</figure>

It is interesting to note that although only 24% responded that they use the library's electronic resources when at the library, 81% responded that they have used those resources, and 30% said they use them at least weekly.

### Use of electronic resources

We then asked students which electronic resources they were familiar with. During the earlier pilot testing students did not reply to open questions on the topic and, therefore, the resources available at the Baruch College Newman library were listed, including some more specialized databases we doubted freshmen would have used. The results are in the table below.

<table><caption>

**Table 3: Students' use of the library's electronic resources**</caption>

<tbody>

<tr>

<th>Library Electronic Resources</th>

<th>% of Students</th>

</tr>

<tr>

<td>Lexis/Nexis</td>

<td>44%</td>

</tr>

<tr>

<td>DPAC</td>

<td>36%</td>

</tr>

<tr>

<td>Academic Search Premier</td>

<td>29%</td>

</tr>

<tr>

<td>Ebsco Host</td>

<td>26%</td>

</tr>

<tr>

<td>DPER</td>

<td>9%</td>

</tr>

<tr>

<td>Dow Jones Interactive</td>

<td>8%</td>

</tr>

<tr>

<td>Book Review Index</td>

<td>6%</td>

</tr>

<tr>

<td>Business Source Premier</td>

<td>4%</td>

</tr>

<tr>

<td>Literature Resource Center</td>

<td>4%</td>

</tr>

<tr>

<td>PsycInfo</td>

<td>3%</td>

</tr>

<tr>

<td>America: History and Life</td>

<td>2%</td>

</tr>

<tr>

<td>CQ Researcher</td>

<td>2%</td>

</tr>

<tr>

<td>Ethnic Newswatch</td>

<td>2%</td>

</tr>

<tr>

<td>Sociological Abstracts</td>

<td>2%</td>

</tr>

<tr>

<td>JSTOR</td>

<td>0.3%</td>

</tr>

</tbody>

</table>

Some of these numbers were thought-provoking. We were surprised to see how many students claimed to have used _Lexis/Nexis_, since it contains newspaper articles and law information, although they might have used to it find current information on controversial topics, which are common paper or speech topics. On the other hand, perhaps Lexis/Nexis has great name recognition, having been around for a while.

We were glad to see that 36% of the students surveyed had used or at least recognized _DPAC_, our online catalogue; since only 21% used the library to check books out. It is possible that DPAC use comes from checking course reserves.

We were not very surprised to see that students made use of _Academic Search Premier_ and _Ebsco Host_ since they are good general databases, with a number of full-text sources. Although _Academic Search Premier_ is one of the databases included in _Ebsco Host_, we decided to give students both choices since we provide separate access to them from the library's electronic resources Web page.

While 88% knew they could access the library's electronic resources from home, 50% reported using them from home. This demonstrates that our freshmen are quite sophisticated in an online environment, and that the library has been doing a good job in terms of letting students know they can access resources from home. However, the fact that freshmen students are using _DPER_ (a magazine and journal index) is not a reassuring sign. Indeed _DPER_ has not been supported in the library since December 1999 (nor has it been updated since then) and was replaced by a Web-based _First Search_ Wilson index. It is possible therefore that professors are directing their students to use _DPER_.

This would be consistent with the fact that 28% report learning about electronic resources from their professors, compared to 30% who learn from library workshops. Since our workshop count is not that high, and tends to reflect more senior students, it is possible that students are including course-related lectures given by the library faculty, in particular once consisting of two sessions, given to freshmen as part of a new initiative on information literacy.

Sixty-one percent of the students reported that using the library's electronic resources is somewhat easy or very easy. However 50% of them realize there is a difference between the library's electronic resources and Internet resources. This might explain why 77% start their research through the Internet, not the library's resources (electronic or otherwise). Sixty-seven percent also report that they find most information for their papers through the Internet, while only 27% report using the library's electronic resources and 20% report using non-electronic resources.

We then tested our hypotheses using an analysis of variance (ANOVA). While age or gender did not have any effect on students' use of the library's electronic resources, self-efficacy did. By dividing the self-efficacy scores into high self-efficacy (top 20 percentile) and low self-efficacy (bottom 20 percentile), we were able to create two groups and compare them with library use.

We found that students who use the library more frequently (monthly, weekly, or daily) were more likely to report using the library's electronic resources than those who rarely used the library (never, once/twice), **F**(1, 336) = 16.416, p<.01\. This is a very interesting fact since most students do not spend their time using the library's resources, but using the library as a place to study, sleep, or socialize. It seems that being in the library environment influences students' awareness of the resources available to them, even if they do not make use of them.

This might also be related to the fact that students who visit the library more frequently (monthly, weekly, or daily) had higher self-efficacy scores than those who reported using the library less often (never, once/twice), **F**(1, 112) = 3.833, p<.05\. That students with higher self-efficacy tend to use the library more often correlates with other research on self-efficacy, in that self-efficacious students tend to be more active academically and use the resources available to them. This also explains why students who use the library more often use the library's electronic resources more frequently.

Students who found the library's electronic resources easy to use had higher self-efficacy scores as compared to those who found the electronic resources difficult to use, **F**(1, 18) = 7.004, p<.05\. This result correlates with other self-efficacy research in that students with higher self-efficacy tend to believe in their abilities and will work harder to learn what they don't know. This is true even if they do not have the skills to use the library's electronic resources for they will tend to work at developing them.

Finally, students who report being highly motivated to learn about the library's electronic resources have higher self-efficacy scores when compared to those who are unmotivated to learn, **F**(1, 58) = 4.814, p<.05\. This result also relates to other research on self-efficacy since students with higher self-efficacy tend to believe in their abilities and want to expand them, so they would want to learn about resources they think would be helpful to their academic careers.

## Conclusion

In this study we found significant support for [hypothesis 3](#hyp3): that students who frequent the library more often are more likely to use the library's electronic resources; and for [hypothesis 5](#hyp5): that students who express an interest in learning about the library's electronic resources will be more likely to exhibit higher self-efficacy. We also have evidence (non-significant) demonstrating that students who have more computer experience will be more likely to use the library's electronic resources and have higher self-efficacy ([hypothesis 1](#hyp1)), that students who use the library's electronic resources from home have higher self-efficacy ([hypothesis 2](#hyp2)), and that students who use the library's electronic resources will be able to discriminate them from information from the Internet ([hypothesis 4](#hyp4)).

In this paper we have tried to determine some students' characteristics that would lead them to use the library's electronic resources. Through a survey administered to a class of freshmen we found that while age and gender were not related to use of electronic resources, self-efficacy was. Self-efficacy, the belief in one's capacity to act in order to achieve one's goals, was related in our research to a higher use of both the library and of electronic resources.

Although we are not be able to generalize from our results to the entire freshman population, since our sample was not random, our results will be helpful in designing new initiatives that could enhance freshmen's use of the library's resources. Knowing that most of our students are basically computer literate and somewhat competent in using the Web, we can create programmes that will build on those skills.

This paper also points to several areas that need further research:

1.  Getting more detailed information on what, exactly, freshmen are accessing when they speak of accessing the Internet for educational purposes.
2.  Examining what, specifically, freshmen mean when they speak of doing research in the library.
3.  Ascertaining more information on which databases students use to search for which kinds of information.
4.  Determining why so many freshmen are using _Lexis/Nexis_.

An additional line of inquiry would be to understand how students who use the library as a place to study develop more awareness of the library's resources. If a library understood how students gained that awareness (through interacting with librarians at the reference desk, or through following signs, or by observing other students, for example), the library could make further efforts to reach students in an efficient way. Also, if the library knew how students absorb this kind of information, it could be the basis for developing sound pedagogical methods to transmit information to our students.

Finally, research could also explore, in more depth, what makes students decide to use the Internet as opposed to the library's electronic resources, and what kinds of incentives would encourage students to use more of the library's resources.

## [Appendix](#app)

## References:

*   <a id="ban97"></a>Bandura, Albert. (1997). _Self-efficacy: the exercise of control._ New York, NY: W.H. Freeman.
*   <a id="bro98"></a>Brosnan, M.J. (1998). "The impact of computer anxiety and self-efficacy upon performance."  _Journal of Computer Assisted Learning,_ **14**(3), 223-235.
*   <a id="bro01"></a>Brown, P., Challagalla, G., & Ganesan, S. (2001).  "Self-efficacy as a moderator of information seeking effectiveness." _Journal of applied psychology,_ **86**(5), 1043-1051.
*   <a id="com95"></a>Compeau, D.R. & Higgins, C.A. (1995). "Computer self-efficacy: development of a measure and initial test." _MIS Quarterly,_ **19**, 189-211.
*   <a id="dyc94"></a>Dyck, J.L. & Smither, J.A. (1994). "Age differences in computer anxiety: the role of computer experience, gender and education."  _Journal of educational computing research,_ **10**(3), 239-248.
*   <a id="for96"></a>Ford, N. & Miller D. (1996) "Gender differences in internet perception and use." In: Collier, M. & Arnold, K. (eds.), _Electronic Library and Visual Information Research. ELVIRA 3: Papers from the Third ELVIRA conference, 30 April-2 May 1996_, pp. 87-100 London: ASLIB
*   <a id="for01"></a>Ford, N., Miller, D, & Moss, N. (2001) "The role of individual differences in Internet searching: an empirical study." _Journal of the American Society for Information Science and technology,_ **52**(12), 1049-1066
*   <a id="jac91"></a>Jacobson, F.F. (1991). "Gender differences in attitudes toward using computers in libraries: an exploratory study." _Library and Information Studies Research,_ **13**, 267-279.
*   <a id="hei00"></a>Heine, M., Winkworth, I., & Ray, K. (2000). "Modeling service-seeking behavior in an academic library: a methodology and its application." _Journal of Academic Librarianship,_ **26**(4), 233-247.
*   <a id="hil97"></a>Hill, J.R., & Hannafin, M.J. (1997).  "Cognitive strategies and learning from the World Wide Web." _Educational Technology Research and Development,_ **45**(4), 37-64.
*   <a id="kib00"></a>Kibirge, H.M. & DePalo, L. (2000). "The Internet as a source of academic research information: findings of two pilot studies." _Information Technology and Libraries_, **19**, 11-16.
*   <a id="koo86"></a>Koohang, A.A. (1986). "Effects of age, gender, college status, and computer experience on attitudes toward library computer systems (LCS)".  _Library and Information Science Research_, **8**, 349-355.
*   <a id="lag97"></a>Laguna, K. & Babcock, R.L. (1997).  "Computer anxiety in young and older adults: implications for human-computer interactions in older populations." _Computers in Human Behavior_, **13**, 317-326.
*   <a id="lae01"></a>Laerum, H., Ellingsen, G. & Faxvaag A. (2001). "Doctors' use of electronic medical records systems in hospitals: cross sectional survey." _British Medical Journal_, **323**(7323), 1344-1348
*   <a id="laz00"></a>Lazonder, A.W., Biemans, H.J.A., & Wopereis, I.G.J.H. (2000). "Differences between novice and experienced users in searching for information on the World Wide Web." _Journal of the American Society for Information Science,_ **51**(6), 576-581
*   <a id="lew01"></a>Lewis, B. & Behana, K. (2001).  "The Internet as a resource for consumer healthcare." _Disease Management and Health Outcomes,_ **9**(5), 241-248.
*   <a id="maj99"></a>Majid, S. & Abazova, A.F. (1999).  "Computer literacy and use of electronic information sources by academics: a case study of International Islamic University Malaysia." _Asian Libraries_, **8**(4), 100-111.
*   <a id="mcg01"></a>McGuigan, G.S. (2001).  "Databases versus the Web: a discussion of teaching the use of electronic resources in the library instruction setting." _Internet Reference Services Quarterly_, **6**(1), 39-47.
*   <a id="mee97"></a>Meer, V., Fravel, P., Poole, H. & Van Valey, T. (1997). ["Are library users also computer users? A survey of faculty and implications for services."](http://info.lib.uh.edu/pr/v8/n1/vand8n1.html)  _The Public-Access Computer Systems Review,_ **8**(1). http://info.lib.uh.edu/pr/v8/n1/vand8n1.html (26 January 2002)
*   <a id="nah96"></a>Nahl, D. & Tenopir, C. (1996). "Affective and cognitive searching behavior of novice end-users of a full-text database." _Journal of the American Society for Information Science,_ **47**(4), 276-86.
*   <a id="nat01"></a>National Center for Education Statistics (2001).  [IPEDS College opportunities on-line: CUNY Bernard M Baruch College, IPEDS ID: 19051](http://nces.ed.gov/ipeds/cool/Enrollment.asp?UNITID=190512). Washington, DC: National Center for Education Statistics. http://nces.ed.gov/ipeds/cool/Enrollment.asp?UNITID=190512\. (15 October 2002)
*   <a id="paj97"></a>Pajares, F. (1997).  ["Current directions in self-efficacy research",](http://www.emory.edu/EDUCATION/mfp/effchapter.html) In: M. Maehr & P. R. Pintrich (eds.) _Advances in motivation and achievement. Volume 10_, pp. 1-49\. Greenwich, CT: JAI Press. http://www.emory.edu/EDUCATION/mfp/effchapter.html (26 January 2002)
*   <a id="pin91"></a>Pintrich, P.R. & Garcia, T. (1991)  "Student goal orientation and self-regulation in the college classroom", In: M. Maehr & P. R. Pintrich (eds.) _Advances in motivation and achievement: goals and self-regulatory processes,_ pp. 371-402\. Greenwich, CT: JAI Press.
*   <a id="pin95"></a>Pintrich, P.R., & Schunk, D.H. (1995). _Motivation in education: theory, research and applications._ Englewood Cliffs, NJ: Prentice-Hall.
*   <a id="ren00"></a>Ren, Wen-Hua (2000).  "Library instruction and college student self-sufficiency in electronic information searching." _Journal of Academic Librarianship,_ **26**(5), 323-328.
*   <a id="sac93"></a>Sacks, C.H., Bellissimo, Y & Mergendoller, J.R. (1994). "Attitudes toward computers and computer use: the issue of gender." _Journal of research on computing in education,_ **26**(2), 256-269.
*   <a id="sch94"></a>Schunk, D.H. (1994)  "Self-regulation of self-efficacy and attributions in academic settings", In: D.H. Schunk, and B.J.Zimmerman, _Self-regulation of learning and performance: issues and educational applications,_ pp.75-99\. Hillsdale, NJ: Lawrence Erblaum.
*   <a id="val93"></a>Valentine, B. (1993). "Undergraduate research behavior: using focus groups to generate theory" _Journal of Academic Librarianship_, **19**(5), 300-304.
*   <a id="whi01a"></a>Whitmire, E. (2001a).  "The relationship between undergraduates' background characteristics and college experiences and their academic library use." _College and Research Libraries,_ **62**(6), 528-540.
*   <a id="whi01b"></a>Whitmire, E. (2001b).  "A longitudinal study of undergraduates' academic library experiences." _Journal of Academic Librarianship,_ **27**(5), 379-385.

## <a id="app"></a>Appendix A: Survey

Each of the following statements is trying to describe you. Indicate the extent to which you agree or disagree with each statement. To complete an item, simply circle the responses that best describe you. Please mark only one answer per item.

<table>

<tbody>

<tr>

<td> </td>

<td>Strongly Disagree</td>

<td>Disagree</td>

<td>Neither</td>

<td>Agree</td>

<td>Strongly Agree</td>

</tr>

<tr>

<td>

1\. When I make plans, I am certain I can make them work.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

2\. One of my problems is that I cannot get down to work when I should.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

3\. If I can't do a job the first time, I keep trying until I can.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

4\. When I set important goals for myself, I rarely achieve them.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

5\. I give up on things before completing them.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

6\. I avoid facing difficulties.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

7\. If something looks too complicated, I will not even bother to try it.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

8\. When I have something unpleasant to do, I stick to it until I finish it.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

9\. When I decide to do something, I go right to work on it.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

10\. When trying to learn something new, I soon give up if I am not initially successful.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

11\. When unexpected problems occur, I don't handle them well.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

12\. I avoid trying to learn new things when they look too difficult to me.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

13\. Failure just makes me try harder.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

14\. I feel insecure about my ability to do things.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

15\. I am a self-reliant person.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

16\. I give up easily.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

<tr>

<td>

17\. I do not seem capable of dealing with most problems that come up in life.</td>

<td>1</td>

<td>2</td>

<td>3</td>

<td>4</td>

<td>5</td>

</tr>

</tbody>

</table>

1\. Gender: _________

2\. Age:  
a. up to 18  
b. 18 to 21  
c. 22 to 25  
d. 26 to 30  
e. 31 to 35  
f. 36 and up

3\. Ethnicity:  
a. White  
b. African-American  
c. Hispanic  
d. Native American  
e. Asian  
f. Other (Please specify): _____________________

4\. Status:  
a. Freshman  
b. Sophomore  
c. Junior  
d. Senior  
e. Graduate Student

5\. Are you involved in the Block programming?  
a. Yes  
b. No

6\. What is your Grade Point Average: _______

7\. I am presently:  
a. Working full-time  
b. Working part-time  
b. Unemployed  
c. Other (Please specify): _________________

8\. This year my yearly income is about:  
a. I don't work  
b. $0-$2,500  
c. $2,501-$5,000  
d. $5,001-$10,000  
e. $10,001-$15,000  
f. $15,001-$20,000  
g. $20,001-$30,000  
h. $30,001-$40,000  
i. $40,001-$50,000  
j. $50,001-$60,000  
k. Over $60,001

9\. Do you own a personal computer?  
a. Yes  
b. No

10\. Do you have access to a computer?  
a. At home  
b. At school  
c. At work  
d. Friend  
e. Other (Please specify): ___________________

11\. Do you use the Internet?  
a. Daily  
b. At least once a week  
c. At least once a month  
d. Less than once a month

12\. If you use the Internet, do you use it to check for?  
a. Sports  
b. Entertainment  
c. Education  
d. News  
e. Health  
f. Other (Please specify): ________________

13\. Do you use search engines?  
a. Yes  
b. No  
If you answered yes to question 13, please answer the following 2 questions:

14\. How often do you use search engines:  
a. Never  
b. Once or twice  
c. Monthly  
d. Weekly  
e. Daily

15\. Which search engine do you use?  
a. Google  
b. Yahoo  
c. Altavista  
d. Excite  
e. MSN  
f. AskJeeves  
g. Other (Please specify): _______________________

16\. Do you use e-mail to communicate?  
a. Often  
b. Sometimes  
c. Rarely

17\. How often did you visit the Newman library this semester?  
a. Never  
b. Once or twice  
c. Monthly  
d. Weekly  
e. Daily

18\. At the library, do you:  
a. Study  
b. Sleep  
c. Socialize  
d. Check out books  
e. Research  
f. Use the library's electronic resources  
g. E-mail or chat  
h. Other (Please specify): ___________

19\. Have you ever used the library's electronic resources?  
a. Yes  
b. No

If you checked yes in question 19, please answer the following 3 questions:

20\. Do you use the library's electronic resources?  
a. Daily  
b. At least once a week  
c. At least once a month  
d. Less than once a month

21\. Which of the following resources are you familiar with?  
a. Academic Search Premier  
b. Lexis/Nexis  
c. America: History and Life  
d. Dow Jones Interactive  
e. Book Review Index  
f. Business Source Premier  
g. CQ Researcher  
h. EBSCO Host  
i. Ethnic Newswatch  
j. JSTOR  
k. Literature Resource Center  
l. DPAC  
m. DPER  
n. Sociological Abstracts  
o. PsycInfo  
p. Other (Please specify): _________________________

22\. Do you use the library's resources from home?  
a. Yes  
b. No

23\. If you answered no to question 22, Did you know you can access the library's resources from home:  
a. Yes  
b. No

24\. Knowing that you can access the library's resources from home, will you access these resources from home in the future?  
a. Yes  
b. No

25\. Is using the library's electronic resources?  
a. Very hard  
b. Somewhat hard  
c. Somewhat easy  
d. Very easy

26\. In using the library's electronic resources, do you find what you're searching for?  
a. always  
b. sometimes  
c. never

27\. If you use the library's electronic resources, Please describe in detail what steps you take when conducting research:

28\. To find information do you usually start your research looking through?  
a. Internet at home  
b. Internet on campus  
c. Printed books  
d. Electronic books  
e. Printed journals and magazines  
f. Electronic databases  
g. Electronic journals  
h. Other (Please specify): ________________

29\. How do you learn about the library's electronic resources?  
a. Teach myself  
b. From the Reference desk  
c. From friends  
d. From the library's Websites  
e. From workshops  
f. From Professors

30\. "I can find the information I need for my papers through the Internet?" What do you think about this statement?  
a. Strongly disagree  
b. Disagree  
c. Agree  
d. Strongly agree

31\. "I can find the information I need for my papers through the library's electronic resources?" What do you think about this statement?  
a. Strongly disagree  
b. Disagree  
c. Agree  
d. Strongly agree

32\. Do you find most of your information for your school papers:  
a. Through the internet  
b. Through the library's electronic resources

33\. Do you feel there is a difference between what you find through the Internet or through the library's electronic resources?  
a. Yes  
b. No

Please explain in detail:

34\. How motivated are you to learn how to use the library's electronic resources?  
a. Very motivated  
b. Somewhat motivated  
c. Not motivated

35\. Have you attended any of the library's workshops?  
a. Yes  
b. No