<!doctype html>
<html lang="en">
<head>
<title>The autonomous turn in information behaviour</title>
<meta charset="utf-8" />
<link href="../../IRstyle4.css" rel="stylesheet" media="screen" title="serif" />

<!--Enter appropriate data in the content fields-->
<meta name="dcterms.title" content="The autonomous turn in information behaviour" />

<meta name="citation_author" content="Ridley, Michael" />

<meta name="dcterms.subject" content="The aim of this paper is to explore the extent to which concepts of information behaviour have been adopted within other disciplines." />
<meta name="description" content="The aim of this paper is to explore the extent to which concepts of information behaviour have been adopted within other disciplines, to the extent allowed by quantitative analysis of Web of Science data.  Searches were carried out in Web of Science in each decade from 1960 to the present day and the results analysed by the journals publishing related papers and by the research areas of these journals.  The 'Analyze Results' feature of Web of Science was used to provide quantitative analysis of the results, by journal title and by research area.  While papers on information behaviour appear in more than one hundred disciplinary areas, the distribution is concentrated in a very limited number of areas and is otherwise thinly spread over the remaining disciplines. Scholars in many disciplines have explored the information needs and information behaviour of those working in the discipline, or whom the disciplines serves. However, the concentration of interest is found in the health and medical sciences, computer science and information systems, communication and media studies, and psychology." />
<meta name="keywords" content="information behaviour, information needs, information use, information technology," />

<!--leave the following to be completed by the Editor-->
<meta name="robots" content="all" />
<meta name="dcterms.publisher" content="University of Borås" />
<meta name="dcterms.type" content="text" />
<meta name="dcterms.identifier" content="ISSN-1368-1613" />
<meta name="dcterms.identifier" content="http://InformationR.net/ir/24-1/isic2018/isic1832.html" />
<meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/24-1/infres241.html" />
<meta name="dcterms.format" content="text/html" />  <meta name="dc.language" content="en" />
<meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
<meta  name="dcterms.issued" content="2019-03-15" />
<meta name="geo.placename" content="global" />

</head>

<body>

<header>
<img height="45" alt="header" src="../../mini_logo2.gif"  width="336" /><br />
<span style="font-size: medium; font-variant: small-caps; font-weight: bold;">published quarterly by the University of Bor&aring;s, Sweden<br /><br />vol. 24  no. 1, March, 2019</span>
<br /><br />
<div class="button">
<ul>
<li><a href="isic2018.html">Contents</a> |  </li>
<li><a href="../../iraindex.html">Author index</a> |  </li>
<li><a href="../../irsindex.html">Subject index</a> |  </li>
<li><a href="../../search.html">Search</a> |  </li>
<li><a href="../../index.html">Home</a> </li>
</ul>
</div>
<hr />
Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.
<hr />
</header>
<article>
  <h1>The autonomous turn in information behaviour</h1>  <br />

  <h2 class="author"><a href="#author">Michael Ridley</a>
  </h2>

  <br />

  <blockquote>
  <strong>Introduction</strong>. Autonomous, intelligent information systems and agents are becoming ubiquitous and appear to engage in information behaviours that constitute a new model or framework.<br />
  <strong>Methods</strong>. By examining the nature of artificial intelligence systems and their algorithmic basis, a description of autonomous information behaviour is proposed.<br />
  <strong>Analysis</strong>. While autonomous and human information behaviours utilize both similar and different strategies, they share the same objectives with respect to information need and use.<br />
  <strong>Results</strong>. A preliminary model of autonomous information behaviour is proposed, and selected features of that model are compared with existing human behaviour models.<br />
  <strong>Conclusion</strong>. Autonomous, intelligent agents and systems engage in new forms of information behaviour. Creating information behaviour models or frameworks from these systems will not only contribute to their understanding but also facilitate the development of more effective and equitable systems.
  </blockquote>

  <br />

  <section>
    <h2>Introduction</h2>
    <p>Autonomous, intelligent systems or agents (artificial intelligence) are challenging notions of information behaviour. Models of information behaviour or practice are typically human-centric (<a href="#bat10">Bates, 2010</a>; <a href="#cas16">Case and Given, 2016</a>; <a href="#fis05">Fisher, Erdelez and McKechnie, 2005</a>; <a href="#wil10">Wilson, 2010</a>). The <em>autonomous turn</em> recognizes that artificial intelligences (or more specifically autonomous, intelligent algorithms) share the same characteristics as human information behaviour models. As with humans, artificial intelligences &lsquo;<em>need, seek, manage, give, and use information in different contexts</em>&rsquo;.</p>
    <p>Autonomous information behaviour describes the manner in which artificial intelligence engage in a relevant information space. The preliminary considerations presented here attempt to outline how autonomous and human information behaviours are both similar and different.</p>
    <p>The <em>user</em><em> turn</em> in information behaviour research shifted focus from information systems to &lsquo;<em>human</em><em> beings as actively constructing rather than passively processing information</em>&rsquo; (<a href="#der86">Dervin and Nilan, 1986</a>). This user focus, and the socially constructed view of information behaviour that followed, rebalanced the field integrating people, social and contextual concerns, systems, and information (<a href="#sav95">Savolainen, 1995</a>). A <em>digital turn</em> occurred as researchers explored the impact of digital technologies and tools (<a href="#jae10">Jaeger and Burnett, 2010</a>; <a href="#sav04">Savolainen and Kari, 2004</a>). The <em>cognitive turn</em>, particularly as represented by the Mediator Model (<a href="#ing92">Ingwersen, 1992</a>; <a href="#ing05">Ingwersen and J&auml;rvelin, 2005</a>), explored the idea of an intelligent agent in information behaviour, albeit restricted to the information retrieval domain and never implemented as an operational system.</p>
    <p>Autonomous information behaviour is neither a return to a system focus nor a privileging of the digital format. While an artificial intelligence is clearly a digital information system, its information behaviour is unique because of its autonomous capacity, thereby realizing in a wider domain Ingwersen&rsquo;s concepts. Autonomous information behaviour acknowledges the agency of artificial intelligence (<a href="#lat05">Latour, 2005</a>; <a href="#mal13">Malafouris, 2013</a>). They are, in Floridi&rsquo;s assessment, <em>&lsquo;inforgs&rsquo;</em> (informational organisms) (<a href="#flo11">Floridi, 2011</a>). These characteristics cause researchers to regard artificial intelligences not as systems or tools but as agents, partners, colleagues, and for libraries, even &lsquo;<em>a</em><em> new kind of patron</em>&rsquo; (<a href="#bou17">Bourg, 2017</a>; <a href="#nas97">Nass, Moon, Morkes, Kim and Fogg, 1997</a>).</p>
    <p>As autonomous, intelligent systems proliferate, formulating an autonomous information behaviour model is important for understanding of how these systems operate and impact society. More urgently, there is concern and evidence that these algorithms exhibit bias, inequity, and lack of fairness that compromise their effectiveness (<a href="#eub18">Eubanks, 2018</a>; <a href="#nob18">Noble, 2018</a>; <a href="#one16">O&rsquo;Neil, 2016</a>; <a href="#pas15">Pasquale, 2015</a>). Developing and validating an autonomous information behaviour model will further attempts to create more equitable artificial intelligences.</p>
  </section>

  <section>
  <h2>The autonomous model</h2>
  <figure class="centre">
  <img src="../../figs/isic1832fig1.png" width="697" height="328" alt="Figure 1: Autonomous information behaviour model
  " />
  <figcaption><br />Figure 1: Autonomous information behaviour model</figcaption>
  </figure>
  <p>This simplified schematic of a machine learning model identifies the key components and their interrelationships and represents a preliminary description of autonomous information behaviour. <em>Training data </em>is collected in as much volume as possible with ~30% of that data set aside as <em>test data </em>to be used later for validation. The <em>learner</em>, with its goals implemented as features, weights, biases, and rewards, ingests and analyses the <em>training data</em>. The <em>classifier </em>represents the various policies or models that operate on the learned data and determines the <em>outcome (prediction).</em></p>
  <p><em>Test data </em>is used in error correction. Backpropagation explores the error rates between the <em>training</em><em> data </em>and the <em>test data </em>with respect to the <em>outcome (prediction)</em><strong>. </strong>Reducing the error rate (i.e. getting closer to optimal performance) is accomplished by a combination of ingesting more data, tuning the <em>learner </em>through the <em>hand engineering</em> (i.e. human intervention) of features, weights, and biases, and by increasing the size of the <em>classifier </em>(e.g. a larger neural network). When the error rate is deemed acceptable, the model is annealed and used with <em>new data </em>to generate outcomes.</p>
  <p>For supervised and unsupervised learning, the model is fixed at this point. A significant difference in reinforcement learning is that the <em>learner </em>and the <em>classifier</em> are combined into one model (with limited or no hand engineering) and tasked with continuous learning and prediction. Lastly, since both unsupervised learning and reinforcement learning are opaque, an <em>explanation </em>is often needed to explain the outcome or prediction to a human user/subject.</p>
  <p>As with the processes of human information behaviour, the mechanisms or mechanics of autonomous information behaviour (i.e. the <em>learners</em> and the <em>classifiers</em>) are both contextual and evolving. The specific prediction and classifier models used by artificial intelligence (e.g. artificial neural networks, convolutional neural networks, generative adversarial networks, decision trees, rules, and many others) have individual strengths and weaknesses, and are selected and <em>tuned</em> for particular tasks (e.g. image analysis, speech recognition, text comprehension) (<a href="#goo16">Goodfellow, Bengio and Courville, 2016</a>). These different contexts engage specific models or even <em>ensembles</em> where multiple approaches are used. Information seeking and use models differ for such objectives as game tactics, drug discovery or recommendation systems.</p>
  <p>These models are clustered into two general strategies: supervised learning, where training data and defined outcomes direct the model (e.g. show the model lots of cats so it can recognize a cat in the future), and unsupervised learning, where a <em>trial and error</em> approach allows the model to explore the data more independently (e.g. give the model only basic rules or guidance and allow it to explore and test options on its own).</p>
  </section>

  <section>
  <h2>Need and opacity</h2>
  <p>Human information behaviour models typically originate with some sort of gap, uncertainty or need (<a href="#bel80">Belkin, 1980</a>; <a href="#der03">Dervin, 2003</a>; <a href="#kuh91">Kuhlthau, 1991</a>). In autonomous systems, gaps, uncertainties or needs are best understood as problem definitions provided explicitly by the system designers for the particular requirement. While in this example humans are integral features of the overall autonomous information behaviour (in their role as establishing the need and receiving the prediction or outcome), increasingly artificial intelligences are defining their own need or gap, with the output of one artificial intelligence providing the input to another (<a href="#gil14">Gil, Greaves, Hendler and Hirsh, 2014</a>).</p>
  <p>The opacity of advanced artificial intelligence models such as deep learning, a form of neural network (<a href="#lec15">LeCun, Bengio and Hinton, 2015</a>), presents a significant barrier to understanding and defining autonomous information behaviour. As Geoffrey Hinton notes: &lsquo;<em>a deep-learning system doesn&rsquo;t have any explanatory power. The more powerful the deep-learning system becomes, the more opaque it can become&rsquo;</em> (<a href="#muk17">Mukherjee, 2017</a>, p. 53). A deep learning system can be inscrutable, and resist attempts at transparency and explainability. Research on <em>explainable</em> artificial intelligence is attempting to develop processes and strategies to make algorithmic systems interpretable and to provide methods for human-level explanations (<a href="#dar16">DARPA, 2016</a>).</p>
  </section>

  <section>
  <h2>Overfitting and dimensionality</h2>
  <p>To better understand the nature of autonomous information behaviour, it is useful to examine two typical aspects of human models and identify comparators in an autonomous model. Concluding an information search prematurely is a feature of human models. This <em>rush to judgement</em> is an inadequate exploration of the information space resulting in a conclusion that precludes the use of potentially relevant information. A similar characteristic appears in an autonomous model. Overfitting is the term used to describe artificial intelligences that make a decision or prediction based on a limited amount of data (<a href="#bud17">Buduma, 2017</a>). Further exploration of the information space would have (or might have) identified additional information that would have altered the prediction. Autonomous systems use various randomizing techniques and feature weightings as means to prevent overfitting.</p>
  <p>Another feature of human information behaviour is information overload. In this case, too much information is gathered (or is available) resulting in an inability to synthesize and come to a satisfactory conclusion. In autonomous models, this situation is characterized as an exponentially expanding information space and is referred to as the <em>curse</em> of dimensionality (<a href="#dom15">Domingos, 2015</a>). Even with advanced computational power, the size and complexity of the space is too large to manage in a reasonable timeframe. AlphaGo Zero navigated the Go information space (very large and complex) but it required 40 days and 29 million training games to do so (<a href="#sil17">Silver <em>et al</em>., 2017</a>). Autonomous systems manage this by constructing higher level models (approximations), auditing features with low value, and techniques like <em>chunking</em> and <em>pruning</em> which penalize overly complex models.</p>
  <p>The most obvious characteristics that differentiate autonomous from human information behaviours are the underlying statistical models and computational substrates required to process them. Artificial intelligence systems are statistically driven (<a href="#dom15">Domingos, 2015</a>; <a href="#goo16">Goodfellow <em>et al.</em>, 2016</a>). The features of the data elements used in learning and classifying are essentially weights, values, and computations of them. Understanding information behaviour as a statistical process is at the core of the autonomous model.</p>
  </section>

  <section>
  <h2>Conclusion</h2>
  <p>A crucial divide in the artificial intelligence community is whether the overarching objective is to replicate human intelligence or create a different type of intelligence (<a href="#bod16">Boden, 2016</a>). In assessing autonomous information behaviour there is tendency to establish human performance as the appropriate benchmark even when <em>greater than</em> or <em>different than</em> human performance may be indicated or achievable. Much of artificial intelligence is modelled on human cognition (artificial neural networks were inspired by human neurology), but performance levels are in some cases greater. Fazi suggests that autonomous machines are capable of new ideas not by imitating human cognition (i.e. simulation) but by allowing machines to fully exploit their computational advantage (even if that results in <em>&lsquo;alien thought&rsquo;</em>) (<a href="#faz18">Fazi, 2018</a>). While the similarities between autonomous and human information behaviours is instructive, it is the differences that may point towards new methods of information use. As DeepMind founder Demis Hassabis noted, in its game play AlphaGo Zero was &lsquo;<em>creative&rsquo;</em> and &lsquo;<em>no longer constrained by the limits of human knowledge&rsquo;</em> (<a href="#kna17">Knapton, 2017</a>).</p>
  <p>There is considerable debate, within the artificial intelligence community and beyond, about the generalizability of a system&rsquo;s autonomy and intelligence (<a href="#mar18">Marcus, 2018</a>). Performance levels at or beyond those of humans are typically in very specific and restricted knowledge domains. Much of what is called artificial intelligence is often conventional statistical manipulation with little or no autonomy (<a href="#bog17">Bogost, 2017</a>). However, even with those caveats, it is clear that artificial intelligence can &lsquo;<em>need,</em><em> seek, manage, give, and use information in different contexts&rsquo;</em> and thereby reflect distinct information behaviours. While intentionality may remain in question, the autonomous systems in operation and in development open new avenues for investigation and mark an autonomous turn in information behaviour. Future research needs to examine more closely the various <em>learner </em>and <em>classifier </em>models to determine if differences among them constitute different behaviour models or if they can be integrated into a single framework.</p>
  </section>

  <section>
  <h2>Acknowledgements</h2>
  <p>The author thanks the two anonymous reviewers for helpful and probing comments, and valuable recommendations.</p>
    </section>

    <section>
  <h2 id="author">About the author</h2>

  <p><strong>Michael Ridley</strong> is a PhD student, Faculty of Information and Media Studies (FIMS), Western University. His research interests are artificial intelligence, information behaviour, and literacy. He can be contacted at  <a href="mailto:mridley@uoguelph.ca">mridley@uoguelph.ca</a> on <a href="http://www.MichaelRidley.ca">www.MichaelRidley.ca</a>, and @mridley</p>

  </section>

  <section class="refs">

  <h2>References</h2>
  <ul class="refs">
  <li id="bat10">Bates, M.J. (2010). Information behavior. In M.J. Bates &amp; M.N. Maack (Eds.), <em>Encyclopedia of library and information sciences </em>(3rd ed., pp. 2381&ndash;2391). Boca Raton, FL: CRC Press.</li>

  <li id="bel80">Belkin, N.J. (1980). Anomalous states of knowledge as a basis for information retrieval. <em>Canadian Journal</em><em> of Information Science</em>, <em>5</em>, 133&ndash;143.</li>

  <li id="bod16">Boden, M.A. (2016). <em>AI:</em><em> Its nature and future</em>. Oxford: Oxford University Press.</li>

  <li id="bog17">Bogost, I. (2017). <a href="http://www.theatlantic.com/technology/archive/2017/03/what-is-artificial-intelligence/518547/">&ldquo;Artificial intelligence&rdquo; has become meaningless</a>. <em>The Atlantic</em>. Retrieved from www.theatlantic.com/technology/archive/2017/03/what-is-artificial-intelligence/518547/</li>

  <li id="bou17">Bourg, C. (2017, March 17). <a href="https://chrisbourg.wordpress.com/2017/03/16/what-happens-to-libraries-and-librarians-when-machines-can-read-all-the-books/"><em>What happens to libraries and librarians when machines can read all the books?</em></a> Retrieved from https://chrisbourg.wordpress.com/2017/03/16/what-happens-to-libraries-and-librarians-when-machines-can-read-all-the-books/</li>

  <li id="bud17">Buduma, N. (2017). <em>Fundamentals of deep learning: designing next-generation machine intelligence algorithms</em>. Sebastopol, CA: O&rsquo;Reilly.</li>

  <li id="cas16">Case, D.O. &amp; Given, L.M. (2016). <em>Looking for information: a survey of research on information seeking, needs and behavior </em>(4th ed.). Bingley, UK: Emerald Group Publishing.</li>

  <li id="dar16">DARPA (2016). <a href="http://www.darpa.mil/attachments/DARPA-BAA-16-53.pdf"><em>Explainable artificial intelligence (XAI)</em></a>. Arlington, VA: DARPA. Retrieved from http://www.darpa.mil/attachments/DARPA-BAA-16-53.pdf</li>

  <li id="der03">Dervin, B. (2003). <em>Sense-making</em> <em>methodology</em> <em>reader: selected writings of Brenda Dervin</em>. Cresskill, N.J.: Hampton Press.</li>

  <li id="der86">Dervin, B. &amp; Nilan, M. (1986). Information needs and uses. <em>Annual Review of</em><em> Information Science and Technology, 21</em>, pp. 3&ndash;33.</li>

  <li id="dom15">Domingos, P. (2015). <em>The master</em><em> algorithm: how the quest for the ultimate learning machine will remake our world</em>. New York, NY: Basic Books.</li>

  <li id="eub18">Eubanks, V. (2018). <em>Automating inequity: how high-tech tools profile, police, and punish the poor</em>. New York, NY: St. Martin&rsquo;s Press.</li>

  <li id="faz18">Fazi, M. B. (2018). Can a machine think (anything new)? Automation beyond simulation. <em>AI &amp; Society</em>, 1&ndash;12. doi:10.1007/s00146-018-0821-0</li>

  <li id="fis05">Fisher, K.E., Erdelez, S. &amp; McKechnie, L. (Eds.). (2005). <em>Theories of information behavior</em>. Medford, NJ: American Society for Information Science and Technology.</li>

  <li id="flo11">Floridi, L. (2011). <em>The philosophy of information</em>. Oxford: Oxford University Press.</li>

  <li id="gil14">Gil, Y., Greaves, M., Hendler, J. &amp; Hirsh, H. (2014). Amplify scientific discovery with artificial intelligence. <em>Science</em>, <em>346</em>(6206), 171&ndash;172. doi: 10.1126/science.1259439</li>

  <li id="goo16">Goodfellow, I., Bengio, Y. &amp; Courville, A. (2016). <em>Deep learning</em>. Boston, MA: MIT Press.</li>

  <li id="ing92">Ingwersen, P. (1992). <em>Information</em> <em>retrieval</em> <em>interaction</em>. London: Taylor Graham.</li>

  <li id="ing05">Ingwersen, P. &amp; J&auml;rvelin, K. (2005). <em>The turn: integration of information seeking and retrieval in context</em>. Dordrecht, Germany: Springer.</li>

  <li id="jae10">Jaeger, P.T. &amp; Burnett, G. (2010). <em>Information worlds: social context, technology, and information behavior in the age of the Internet</em>. New York, NY: Routledge.</li>

  <li id="kna17">Knapton, S. (2017, October 18). <a href="http://www.telegraph.co.uk/science/2017/10/18/alphago-zero-google-deepmind-supercomputer-learns-3000-years/amp/">AlphaGo Zero: Google DeepMind supercomputer learns 3,000 years of human knowledge in 40 days.</a> <em>The Telegraph</em>. Retrieved from http://www.telegraph.co.uk/science/2017/10/18/alphago-zero-google-deepmind-supercomputer-learns-3000-years/amp/</li>

  <li id="kuh91">Kuhlthau, C.C. (1991). Inside the search process: Information seeking from the user&rsquo;s perspective. <em>Journal of the American Society for Information Science</em>, <em>42</em>(5), 361&ndash;371.</li>

  <li id="lat05">Latour, B. (2005). <em>Reassembling</em> <em>the</em> <em>social:</em> <em>An</em> <em>introduction</em> <em>to actor-network-theory</em>. Oxford: Oxford University Press.</li>

  <li id="lec15">LeCun, Y., Bengio, Y. &amp; Hinton, G. (2015). Deep learning. <em>Nature</em>, <em>521</em>, 436&ndash;444. doi: 10.1038/nature14539</li>

  <li id="mal13">Malafouris, L. (2013). <em>How things shape the mind: a theory of material engagement</em>. Cambridge, MA: MIT Press.</li>

  <li id="mar18">Marcus, G. (2018). <a href="http://arxiv.org/abs/1801.00631"><em>Deep</em><em> learning: </em><em>a critical appraisal</em>.</a> Retrieved from http://arxiv.org/abs/1801.00631</li>

  <li id="muk17">Mukherjee, S. (2017, April 3). The algorithm will see you now. <em>The New Yorker</em>, 46&ndash; 53.</li>

  <li id="nas97">Nass, C.I., Moon, Y., Morkes, J., Kim, E.-Y. &amp; Fogg, B. J. (1997). Computers are social actors: a review of current research. In B. Friedman (Ed.), <em>Human values and the design of computer technology </em>(pp. 137&ndash;162). New York, NY: Cambridge University Press.</li>

  <li id="nob18">Noble, S.U. (2018). <em>Algorithms of oppression: How search engines reinforce racism</em>. New York, NY: New York University Press.</li>

  <li id="one16">O&rsquo;Neil, C. (2016). <em>Weapons of math destruction: how big data increases inequality and threatens democracy</em>. New York, NY: Crown.</li>

  <li id="pas15">Pasquale, F. (2015). <em>The black box society: the secret algorithms that control money and information</em>. Cambridge, MA: Harvard University Press.</li>

  <li id="sav95">Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of &ldquo;way of life.&rdquo; <em>Library and Information Science Research</em>, <em>17</em>(3), 259&ndash;294. doi: 10.1016/0740-8188(95)90048-9</li>

  <li id="sav04">Savolainen, R. &amp; Kari, J. (2004). <a href="https://doi.org/10.1177/0165551504044667">Conceptions of the internet in everyday life information seeking.</a> <em>Journal of Information Science</em>, <em>30</em>(3), 219&ndash;226. https://doi.org/10.1177/0165551504044667</li>

  <li id="sil17">Silver, D., Schrittwieser, J., Simonyan, K., Antonoglou, I., Huang, A. &amp; Guez, A. (2017). Mastering the game of Go without human knowledge. <em>Nature</em>, <em>550</em>, 354&ndash;359. doi: 10.1038/nature24270</li>

  <li id="wil10">Wilson, T.D. (2010). Information behavior models. In M.J. Bates &amp; M.N. Maack (Eds.), <em>Encyclopedia</em><em> of library and information sciences </em>(3rd ed., pp. 2391&ndash;2400). Boca Raton, FL: CRC Press.</li>

  </ul>
  </section>
  
  <section>
  <hr />
  <h4 style="text-align:center;">How to cite this paper</h4>
  <div class="citing">Ridley, M. (2019). The autonomous turn in information behaviour In <em>Proceedings of ISIC, The Information Behaviour Conference, Krakow, Poland, 9-11 October: Part 2. Information Research, 24</em>(1), paper isic1832. Retrieved from http://InformationR.net/ir/24-1/isic2018/isic1832.html (Archived by WebCite® at http://www.webcitation.org/76lZKt9z0)</div>

  </section>

</article>
<br />
<section>

<table class="footer" style="border-spacing:10px;">
<tr>
<td colspan="3" style="text-align:center; background-color: #5E96FD; color: white; font-family: verdana; font-size: small; font-weight: bold;">Find other papers on this subject</td></tr>
<tr><td style="text-align:center; vertical-align:top;"><form method="get" action="http://scholar.google.com/scholar" target="_blank">
<table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"> <input type="hidden" name="q" value="information behaviour" /><br />
<input type="submit" name="sa" value="Scholar Search"  style="font-size: small; font-family: Verdana; font-weight: bold;" /><input type="hidden" name="num" value="100" />  </td>  </tr></table></form></td>
<td style="vertical-align:top; text-align:center;">  <!-- Search Google --><form method="get" action="http://www.google.com/custom" target="_blank">
<table class="footer">
<tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="information behaviour" /><br />
<input type="submit" name="sa" value="Google Search" style="font-family: Verdana; font-weight: bold; font-size: small;" /><input type="hidden" name="client" value="pub-5081678983212084" /><input type="hidden" name="forid" value="1" /><input type="hidden" name="ie" value="ISO-8859-1" /><input type="hidden" name="oe" value="ISO-8859-1" /><input type="hidden" name="cof" value="GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LGC:FF9900;LC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;FORID:1;" /><input type="hidden" name="hl" value="en" /></td></tr>
</table></form></td>
<td style="vertical-align:top; text-align:center;"><form method="get" action="http://www.bing.com" target="_blank">
<table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="information behaviour" /> <br /><input type="submit" name="sa" value="Bing"  style="font-size: small; font-family: Verdana; font-weight: bold;" /> <input type="hidden" name="num" value="100" /></td></tr>
</table></form></td></tr>
</table>

<div style="text-align:center;">Check for citations, <a href="http://scholar.google.co.uk/scholar?hl=en&amp;q=http://informationr.net/ir/24-1/isic2018/isic1832.html&amp;btnG=Search&amp;as_sdt=2000">using Google Scholar</a></div>
<br />
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_inline_share_toolbox" style="text-align:center;"></div>
<hr />
</section>

<table class="footer" style="padding:10px;"><tr><td style="text-align:center; vertical-align:top;">
<br />
<div><a href="https://www.digits.net" target="_blank"><img src="https://counter.digits.net/?counter={c85388cb-161b-8aa4-c950-66b03f5b3b36}&amp;template=simple" alt="Hit Counter by Digits" /></a></div>
</td>
<td class="footer" style="text-align:center; vertical-align:middle;">
<div>  &copy; the author, 2019. <br />Last updated: 1 March, 2019 </div></td>

<td style="text-align:center; vertical-align:middle;">&nbsp;

</td></tr>  </table>

<footer>

<hr />
<table class="footer"><tr><td>
<div class="button">
<ul style="text-align: center;">
<li><a href="isic2018.html">Contents</a> | </li>
<li><a href="../../iraindex.html">Author index</a> | </li>
<li><a href="../../irsindex.html">Subject index</a> | </li>
<li><a href="../../search.html">Search</a> | </li>
<li><a href="../../index.html">Home</a></li>
</ul>
</div></td></tr></table>
<hr />
</footer>
<script src="http://www.google-analytics.com/urchin.js">  </script>  <script>  _uacct =
"UA-672528-1"; urchinTracker();
</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5046158704890f2e"></script>
</body>
</html>
