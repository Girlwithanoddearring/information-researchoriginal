<!doctype html>
 <html lang="en">
 <head>
 <title>The information mapping board game: a collaborative investigation of asylum seekers and refugees’ information practices in England, UK</title>
<meta charset="utf-8" />
 <link href="../../IRstyle4.css" rel="stylesheet" media="screen" title="serif" />

 <!--Enter appropriate data in the content fields-->
 <meta name="dcterms.title" content="The information mapping board game: a collaborative investigation of asylum seekers and refugees’ information practices in England, UK" />

<meta name="citation_author" content="Le Louvier, Kahina" />
<meta name="citation_author" content="Innocenti, Perla" />


 <meta name="dcterms.subject" content="This paper discusses the use of an information mapping board game for collaboratively identifying information practices of a small group of asylum seekers and refugees in the North East of England, UK." />
 <meta name="description" content="This paper discusses the use of an information mapping board game for collaboratively identifying information practices of a small group of asylum seekers and refugees in the North East of England, UK. Drawing on participatory visual methods, an original information mapping board game was designed. Qualitative results are discussed and analysed using grounded theory, constant comparative analysis, and situational mapping. The use of an information mapping board game allows participants going through the asylum process to become actively involved in mapping and sharing their own information practices, sources and barriers within a playful collaborative environment. It enables participants to become aware of their acquired information literacy by sharing knowledge, and to adapt the game to reflect their needs and knowledge. This study indicates that participatory techniques such as the information mapping board game have the potential to engage hard to reach populations in the research process, to foster their agency, confidence, and capacities, and to inform actions at a local level." />
 <meta name="keywords" content="action research, information mapping board game, asylum seekers, refugees, everyday life, information practices, information sharing," />

 <!--leave the following to be completed by the Editor-->
 <meta name="robots" content="all" />
 <meta name="dcterms.publisher" content="University of Borås" />
 <meta name="dcterms.type" content="text" />
 <meta name="dcterms.identifier" content="ISSN-1368-1613" />
 <meta name="dcterms.identifier" content="http://InformationR.net/ir/24-1/isic2018/isic1835.html" />
 <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/24-1/infres241.html" />
 <meta name="dcterms.format" content="text/html" />  <meta name="dc.language" content="en" />
 <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
 <meta  name="dcterms.issued" content="2019-03-15" />
  <meta name="geo.placename" content="global" />

</head>

 <body>

<header>
 <img height="45" alt="header" src="../../mini_logo2.gif"  width="336" /><br />
<span style="font-size: medium; font-variant: small-caps; font-weight: bold;">published quarterly by the University of Bor&aring;s, Sweden<br /><br />vol. 24  no. 1, March, 2019</span>
<br /><br />
<div class="button">
 <ul>
 <li><a href="isic2018.html">Contents</a> |  </li>
 <li><a href="../../iraindex.html">Author index</a> |  </li>
 <li><a href="../../irsindex.html">Subject index</a> |  </li>
 <li><a href="../../search.html">Search</a> |  </li>
 <li><a href="../../index.html">Home</a> </li>
 </ul>
 </div>
 <hr />
  Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.
 <hr />
 </header>
 <article>
 <h1>The information mapping board game: a collaborative investigation of asylum seekers and refugees’ information practices in England, UK
</h1>  <br />

 <h2 class="author"><a href="#author">Kahina Le Louvier</a> and <a href="#author">Perla Innocenti</a></h2>

 <br />

 <blockquote>
 <strong>Introduction</strong>.This paper discusses the use of an information mapping board game for collaboratively identifying information practices of a small group of asylum seekers and refugees in the North East of England, UK.<br />
<strong>Methods</strong>. Drawing on participatory visual methods, an original information mapping board game was designed.<br />
<strong>Analysis</strong>. Qualitative results are discussed and analysed using grounded theory, constant comparative analysis, and situational mapping.<br />
<strong>Results</strong>. The use of an information mapping board game allows participants going through the asylum process to become actively involved in mapping and sharing their own information practices, sources and barriers within a playful collaborative environment. It enables participants to become aware of their acquired information literacy by sharing knowledge, and to adapt the game to reflect their needs and knowledge.<br />
<strong>Conclusion</strong>. This study indicates that participatory techniques such as the information mapping board game have the potential to engage hard to reach populations in the research process, to foster their agency, confidence, and capacities, and to inform actions at a local level.
 </blockquote>

 <br />

 <section>

 <h2>Introduction</h2>

 <p>This paper discusses the use of the <em>information mapping board game</em> in research involving refugees and asylum seekers. This novel technique was designed as part of a pilot study investigating the information practices of applicants going through the asylum process in the North East of England, UK.</p>

 <p>From the Brexit campaign to the implementation of &lsquo;<em>hostile environment</em>&rsquo; policies (<a href="#kir12">Kirkup and Winnett, 2012</a>), the hospitality crisis in the United Kingdom led to a need to rethink the social inclusion of refugees in the British context. In the field of Library and Information Science, social inclusion has been defined as an information problem (<a href="#cai05">Caidi and Allard, 2005</a>), whereby migrants and refugees in particular are confronted with a &lsquo;<em>fractured information landscape</em>&rsquo; (<a href="#llo17">Lloyd, 2017</a>) that prevents them from taking an active part in society. A number of studies have sought means to foster inclusion by identifying the information practices of refugees in Australia (<a href="#llo14">Lloyd, 2014</a>), Canada (<a href="#qui12">Quirke, 2012</a>), Sweden (<a href="#llo17b">Lloyd, Pilerot and Hultgren, 2017</a>) and more recently Scotland (<a href="#odu17">Oduntan and Ruthven, 2017</a>). This pilot study brings this investigation to England with two aims:</p>

 <ol>
 <li>Draw an initial overview of the information landscape experienced by people who have been resettled in North East England as part of the asylum dispersal programme.</li>
 <li>Allow those who experienced the UK asylum process to exercise their agency and share their own information practices by experimenting with participatory methods via the vehicle of an information mapping board game.</li>
 </ol>
</section>
<section>

<h2>Literature review</h2>

<p>In information science, social inclusion is defined as an information problem (<a href="#cai05">Caidi and Allard, 2005</a>): without the capacity to access and make sense of information, newcomers cannot play an active part in society. Consequently, social inclusion requires understanding how migrants interact with information. Various studies have sought to identify the information practices of refugees, highlighting the following needs:</p>

<ul>
<li>Digital technology (<a href="#ala15">Alam and Imran, 2015</a>; <a href="#gif13">Gifford and Wilding, 2013</a>; <a href="#dia16">D&iacute;az Andrade and Doolin, 2016</a>).</li>
<li>Health information (<a href="#all04">Allen, Matthew and Boland, 2004</a>).</li>
<li>Compliance information, such as societal rules, policies, asylum refusal, legal rights and representation (<a href="#ken11">Kennan, Lloyd, Qayyum and Thompson, 2011</a>; <a href="#odu17">Oduntan and Ruthven, 2017</a>; <a href="#llo17b">Lloyd <em>et al.</em>, 2017</a>).</li>
<li>Everyday information, such as education, employment, health, daily living, communication with family and friends, language, social life and leisure activities (<a href="#ken11">Kennan <em>et al</em>., 2011</a>; <a href="#odu17">Oduntan and Ruthven, 2017</a>; <a href="#llo17b">Lloyd <em>et al.</em>, 2017</a>; <a href="#qui15">Quirke, 2015</a>).</li>
</ul>

<p>Studies identified interpersonal interactions as the preferred sources of information for most refugees (see <a href="#old99">Olden, 1999</a>; <a href="#ken11">Kennan <em>et al.</em>, 2011</a>; <a href="#sha16">Shankar <em>et al.</em>, 2016</a>). These include interactions with friends and family (<a href="#qui12">Quirke, 2012</a>; <a href="#llo17b">Lloyd <em>et al.</em>, 2017</a>; <a href="#odu17">Oduntan and Ruthven, 2017</a>), or service providers, caseworkers and volunteers (<a href="#ken11">Kennan <em>et al.</em>, 2011</a>; <a href="#qui12">Quirke, 2012</a>; <a href="#llo17b">Lloyd <em>et al.</em>, 2017</a>; <a href="#odu17">Oduntan and Ruthven, 2017</a>; <a href="#qay14">Qayyum, Thompson, Kennan and Lloyd, 2014</a>). Further information sources include digital sites such as social media (<a href="#llo16">Lloyd and Wilkinson, 2016</a>; <a href="#llo17b">Lloyd <em>et al.</em>, 2017</a>), leisure activities (<a href="#qui15">Quirke, 2015</a>; <a href="#llo16">Lloyd and Wilkinson, 2016</a>) and libraries (<a href="#var14">Varheim, 2014</a>). These sites are described as <em>information grounds</em> (see <a href="#pet99">Pettigrew, 1999</a>) that facilitate the spontaneous exchange of information and the building of social capital through which refugees acquire information literacy (<a href="#var14">Varheim, 2014</a>; <a href="#qui15">Quirke, 2015</a>; <a href="#llo16">Lloyd and Wilkinson, 2016</a>; <a href="#llo17b">Lloyd <em>et al.</em>, 2017</a>). They also allow newcomers to develop collective coping strategies such as information pooling (<a href="#llo14">Lloyd, 2014</a>; <a href="#llo15">Lloyd, 2015</a>; <a href="#llo17b">Lloyd <em>et al.</em>, 2017</a>).</p>

<p>The pilot contributes to such body of work by using an original technique that allow participants to express and map their own information practices.</p>

</section>

<section>

<h2>Method</h2>

<p>The pilot study used three focus groups employing qualitative participatory action research (<a href="#kin07">Kindon, Pain and Kesby, 2007</a>) to identify the information practices of refugees and asylum seekers settled in a city of the North East of England (Fig.1). This approach addresses the ethical implications of researching marginalised groups whose agency is often denied. Although participation was limited to a collaborative mode (<a href="#big89">Biggs, 1989</a>), whereby the researcher designed the research questions and activities, the aim was to experiment on research techniques that are non-coercive, provide mutual benefits and allow participants to reflect on how to improve their situation. Participatory approaches are increasingly used in Library and Information Science research for they allow socially excluded groups to have their voice heard (<a href="#hic18">Hicks and Lloyd, 2018</a>), however they rely on a high level of participant engagement. Moreover, asylum seekers include people with diverse English language skills and education, precluding the use of traditional research methods. Visual methods such as photovoice have been suggested as powerful alternatives as they allow mitigation of language and literacy levels, and give participants an opportunity to be in control of the information they provide (<a href="#hic18">Hicks and Lloyd, 2018</a>). While the use of photovoice was considered for this pilot, it was judged as too time-consuming for participants.</p>

<figure class="centre">
<img src="../../figs/isic1835fig1.png" width="650" height="275" alt="Figure 1: Autonomous information behaviour model
" />
<figcaption><br />Figure 1: Research design</figcaption>
</figure>

<p>Another visual method was needed to investigate information practices, respecting participatory principles whilst adapting to the players. The information mapping board game was thus designed and composed of five different elements: a detailed street map of the urban area, a progression board, an eight-sided dice, green, yellow and pink flags, and coloured pieces (Fig.2). At the beginning of the game, each player placed their piece on the progression board. The colour of the square on which their piece stood determined their possible actions:</p>

<ul>
<li><strong><em>Yellow </em></strong>&ndash; Players indicate a type of information they need in their everyday life and the source of information used to seek it on a yellow flag, and place it on the map.</li>
<li><strong><em>Green </em></strong>&ndash; Players indicate a type of information they share in their everyday life and the medium they used to share it on a green flag, and place it on the map.</li>
<li><strong><em>White </em></strong>&ndash; Players throw a die showing different information media (radio, official sources, printed media, TV, computer, phone, people and observation). They indicate information they either seek or share using the medium visible on the appropriate flag and place it on the map.</li>
<li><strong><em>Pink </em></strong>&ndash; After each turn, players can put a pink obstacle flag next to the flag just placed to indicate a barrier to that information activity.</li>
</ul>

<figure class="centre">
<img src="../../figs/isic1835fig2.png" width="310" height="300" alt="Figure 1: Autonomous information behaviour model
" />
<figcaption><br />Figure 2: Information Mapping Board Game: Progression board, information sources die, city map.
</figcaption>
</figure>

<p>Each action was explained and discussed with the participants. Players moved two squares forward for each green and yellow flag they placed, one square forward for a pink flag, and one square backward when being stopped. The session lasted about two hours; participants had to leave before anybody won. The competing and playful nature of the game meant that participants joked whilst arguing their case. It may be considered that this game did not facilitate a deep investigation of each information activity. However, the rules of the game, as well as the possibility to break them, promoted insightful data capture, enabling participants to become aware of their capacities.</p>

<p>Data was analysed during the session with a grounded theory method and between each session using constant comparative analysis (<a href="#gla67">Glaser and Strauss, 1967</a>). At the end of the three sessions situational mapping (<a href="#cla05">Clarke, 2005</a>) was used to account for the complexities of participants&rsquo; situation, highlighting both the structure in which they are immersed and the way they experience it.</p>
</section>

<section>

<h2>Findings and discussion</h2>

<p>This original information mapping board game was designed to address three identified challenges:</p>

<ul>
<li><em>Mutual benefits.</em> The game provided a space for participants to practice English in a friendly and non-judgmental environment. Participants played in turns, which enabled equal contributions. The game did require speaking as well as multisensory engaging by observing, listening and moving things around. It was perceived as a diverting time and allowed everybody to actively take part.</li>
<li><em>Horizontality and agency.</em> The game structure allowed the researcher to act as a facilitator rather than as a leader, setting the rules at the beginning of the session before letting the participants appropriate them. The playfulness fostered a positive group dynamic that increased <em>participants&rsquo; engagement</em>.</li>
<li><em>Collaborative information mapping.</em> The game built on the information horizons mapping technique, which graphically represents individual&rsquo;s information seeking behaviour (<a href="#son01">Sonnenwald, Wildemuth and Harmon, 2001</a>). However, it shifted the focus from information seeking to everyday information practices, thus including information sharing activities. Moreover it used a real city map which participants collaboratively interacted with, helping each other navigate the city.</li>
</ul>

<p>Participants indicated functional needs, such as food, English language and IT skills, as well as leisure activities. Their main barrier to information was lack of money, preventing internet access, or affording bus tickets between the accommodation provided by authorities and the city centre, where most flags were located. Social interactions &ndash; as suggested by previous works - appeared to be their main source of information. Social networks were developed through the activities and services provided by local charities and community groups, which allowed them to meet their needs and represented most of the locations flagged. When reflecting on this finding, participants concluded that there were sufficient services to help them, since they had managed to collectively map them, but that these were difficult to find for a newcomer alone because the charities providing them lacked visibility, due to their lack of resources. To mitigate this gap, participants discussed the creation of a website where all information necessary for new asylum seekers could be centralised, although internet connection and digital literacy would limit its access.</p>

<p>The most interesting aspect of the information mapping board game was an unexpected one: the possibility to break its rules. In the middle of the session, participants took over the game and, rather than indicating instances when they sought or shared information in the past, they started sharing information with their co-players directly during the game. They shared tips on where to find free food, courses, leisure activities, and cheap mobile phone companies. Thus, they transformed the game into an information ground and displaced the competition to who knew the city best and could make the most of their situation. As they became aware of how they had managed to make sense of the system and make the most of it, they seemed to gain confidence. Sharing information thus appeared more important than seeking information, for it meant appropriating the city and showcasing expertise.</p>

</section>

<section>

<h2>Conclusion and next steps</h2>

<p>The information mapping board game provides a novel exploratory technique for the identification of refugees&rsquo; and asylum seekers&rsquo; information practices on a city scale. The game&rsquo;s playfulness allows engaging people with different language and education levels, and fosters participants&rsquo; agency by providing them with tools to play with and the possibility to take over the game. Its friendly competition allows participants to work collaboratively and become aware of their acquired information literacy by sharing their knowledge with others. The contextual nature and limited volume of data collected from the game means that findings cannot be generalized and would not be sufficient to enable a satisfactory analysis as a stand-alone technique. Subsequent data collection will further address this. A cross-analysis of results emerging from different groups may provide a rich picture of information affordability and barriers to inform local community-based actions and policies.</p>

</section>

<section>

<h2>Acknowledgements</h2>

<p>This pilot study is part of a research project funded by the University of Northumbria. We would like to thank the participants for playing along. We are grateful to Gobinda Chowdhury and Ed Hyatt for their comments on earlier versions of this paper and inputs to this research, and to the reviewers for their helpful suggestions and observations.</p>
</section>


<section>

<h2 id="author">About the authors</h2>

<p><strong>Kahina Le Louvier</strong> is a Ph.D. candidate in the department of Computer and Information Science at the iSchool of the University of Northumbria, Newcastle-upon-Tyne, UK. Her research focuses on the information and heritage practices of people going through the British asylum system. She can be contacted at <a href="mailto:kahina.lelouvier@northumbria.ac.uk">kahina.lelouvier@northumbria.ac.uk.</a><br />
<strong>Perla Innocenti</strong> is a Senior Lecturer in Information Sciences at the iSchool and NorSC Lab of the University of Northumbria, Newcastle-upon-Tyne, UK. Her research focuses on preserving, curating and making accessible cultural heritage. She is currently investigating articulations between technology and intangible heritage practices, together with ways in which cultural institutions might support the social inclusion of marginalised groups. She can be contacted at <a href="mailto:perla.innocenti@northumbria.ac.uk">perla.innocenti@northumbria.ac.uk</a>.</p>

</section>
<section class="refs">

<h2>References</h2>
<ul class="refs">
<li id="ala15">Alam, K. &amp; Imran, S. (2015). The digital divide and social inclusion among refugee migrants. <em>Information Technology &amp; People</em>, <em>28</em>(2), 344&ndash;365.</li>

<li id="all04">Allen, M., Matthew, S. &amp; Boland, M.J. (2004). Working with immigrants and refugee populations: issues and Hmong case study. <em>Library Trends</em>, <em>53</em>(2), 301&ndash;328.</li>

<li id="big89">Biggs, S. (1989). <em>Resource-poor farmer participation in research: a synthesis of experiences from nine national agricultural research systems</em>. The Hague: International Service for National Agricultural Research.</li>

<li id="cai05">Caidi, N. &amp; Allard, D. (2005). Social inclusion of newcomers to Canada: an information problem? <em>Library and Information Science Research</em>, <em>27</em>(3), 302&ndash; 324.</li>

<li id="cla05">Clarke, A.E. (2005). <em>Situational analysis: grounded theory after the postmodern turn</em>. Thousand Oaks, CA: Sage.</li>

<li id="dia16">D&iacute;az Andrade, A. &amp; Doolin, B. (2016). Information and communication technology and the social inclusion of refugees. <em>MIS Quarterly</em>, <em>40</em>(2), 405&ndash;416.</li>

<li id="gif13">Gifford, S. M. &amp; Wilding, R. (2013). Digital escapes? ICTs, settlement and belonging among Karen youth in Melbourne, Australia. <em>Journal of Refugee Studies</em>, <em>26</em>(4), 558&ndash;575.</li>

<li id="gla67">Glaser, B.G. &amp; Strauss, A.L. (1967). <em>The discovery of grounded theory: strategies for qualitative research</em>. New York, NY: Aldine Publishing.</li>

<li id="hic18">Hicks, A. &amp; Lloyd, A. (2018). Seeing information: visual methods as entry points to information practices. <em>Journal of Librarianship and Information Science</em>, <em>50</em>(3), 229-238.</li>

<li id="ken11">Kennan, M.A., Lloyd, A., Qayyum, A. &amp; Thompson, K. (2011). Settling in: the relationship between information and social inclusion. <em>Australian Academic &amp; Research Libraries</em>, <em>42</em>(3), 191&ndash;210.</li>

<li id="kin07">Kindon, S., Pain, R. &amp; Kesby, M. (Eds.). (2007). <em>Participatory action research approaches and methods</em>. London, New York, NY: Routledge.</li>

<li id="kir12">Kirkup, J. &amp; Winnett, R. (2012, May 25). <a href="https://www.telegraph.co.uk/news/uknews/immigration/9291483/Theresa-May-interview-Were-going-to-give-illegal-migrants-a-really-hostile-reception.html">Theresa May interview: we're going to give illegal immigrants a really hostile reception.</a> <em>The Telegraph.</em> Retrieved from https://www.telegraph.co.uk/news/uknews/immigration/9291483/Theresa-May-interview-Were-going-to-give-illegal-migrants-a-really-hostile-reception.html.</li>

<li id="llo14">Lloyd, A. (2014). Building information resilience: how do resettling refugees connect with health information in regional landscapes &ndash; implications for health literacy. <em>Australian Academic &amp; Research Libraries</em>, <em>45</em>(1), 48-66.</li>

<li id="llo15">Lloyd, A. (2015). Stranger in a strange land; enabling information resilience in resettlement landscapes. <em>Journal of Documentation</em>, <em>71</em>(5), 1029&ndash;1042.</li>

<li id="llo17">Lloyd, A. (2017). Researching fractured (information) landscapes. <em>Journal of Documentation</em>, <em>73</em>(1), 35&ndash;47.</li>

<li id="llo17b">Lloyd, A., Pilerot, O. &amp; Hultgren, F. (2017). <a href="http://InformationR.net/ir/21-1/paper764.html">The remaking of fractured landscapes: supporting refugees in transition (SpiRiT)</a>. <em>Information Research</em>, <em>22</em>(3), paper 764. Retrieved from http://InformationR.net/ir/21-1/paper764.html (Archived by WebCite&reg; at http://www.webcitation.org/6tTRmq2S2).</li>

<li id="llo16">Lloyd, A. &amp; Wilkinson, J. (2016). Knowing and learning in everyday spaces (KALiEds): mapping the information landscape of refugee youth learning in everyday spaces. <em>Journal of Information Science</em>, <em>42</em>(3), 300&ndash;312.</li>

<li id="odu17">Oduntan, O. &amp; Ruthven, I. (2017). Investigating the information gaps in refugee integration. <em>Proceedings of the Association for Information Science and Technology,</em><em> 54</em>(1), 308-317.</li>

<li id="old99">Olden, A. (1999). Somali refugees in London: oral culture in a Western information environment. <em>Libri</em>, <em>49</em>(4), 212&ndash;224.</li>

<li id="pet99">Pettigrew, K.E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behaviour among attendees at community clinics. <em>Information Processing and Management</em>, <em>35</em>, 801&ndash;817.</li>

<li id="qay14">Qayyum, M.A., Thompson, K.M., Kennan, M.A. &amp; Lloyd, A. (2014). <a href="http://InformationR.net/ir/19-2/paper616.html">The provision and sharing of information between service providers and settling refugees</a>. <em>Information Research</em>, <em>19</em>(2), paper 616. Retrieved from http://InformationR.net/ir/19-2/paper616.html</li>

<li id="qui12">Quirke, L. (2012). Information practices in newcomer settlement: a study of Afghan immigrant and refugee youth in Toronto. In <em>Proceedings of the 2012 iConference </em>(pp<em>.</em> 535&ndash;537). New York, NY: ACM.</li>

<li id="qui15">Quirke, L. (2015). Searching for joy: the importance of leisure in newcomer settlement. <em>Journal of International Migration and Integration</em>, <em>16</em>(2), 237&ndash;248.</li>

<li id="sha16">Shankar, S., O&rsquo;Brien, H.L., How, E., Lu, Y., Mabi, M. &amp; Rose, C. (2016). The role of information in the settlement experiences of refugee students. <em>Proceedings of the Association for Information Science and Technology</em>, <em>53</em>(1), 1&ndash;6.</li>

<li id="son01">Sonnenwald, D.H., Wildemuth, B.M. &amp; Harmon, G.L. (2001). A research method to investigate information seeking using the concept of information horizons: an example from a study of lower socio-economic students&rsquo; information seeking behavior. <em>Association of Library and Information Science Education (ALISE)</em>, <em>2</em>, 65&ndash;86</li>

<li id="var14">Varheim, A. (2014). Trust and the role of the public library in the integration of refugees: the case of a Northern Norwegian city. <em>Journal of Librarianship and Information Science</em>, <em>46</em>(1), 62&ndash;69.</li>


</ul>
</section>
<section>
<hr />
 <h4 style="text-align:center;">How to cite this paper</h4>
   <div class="citing">Le Louvier, K. and Innocenti, P. (2019). The information mapping board game: a collaborative investigation of asylum seekers and refugees’ information practices in England, UK In <em>Proceedings of ISIC, The Information Behaviour Conference, Krakow, Poland, 9-11 October: Part 2. Information Research, 24</em>(1), paper isic1835. Retrieved from http://InformationR.net/ir/24-1/isic2018/isic1835.html (Archived by WebCite® at http://www.webcitation.org/76lZjf4h4)</div>

</section>

<hr />

</article>
<br />
<section>

 <table class="footer" style="border-spacing:10px;">
 <tr>
 <td colspan="3" style="text-align:center; background-color: #5E96FD; color: white; font-family: verdana; font-size: small; font-weight: bold;">Find other papers on this subject</td></tr>
 <tr><td style="text-align:center; vertical-align:top;"><form method="get" action="http://scholar.google.com/scholar" target="_blank">
 <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"> <input type="hidden" name="q" value="information behaviour, refugees" /><br />
<input type="submit" name="sa" value="Scholar Search"  style="font-size: small; font-family: Verdana; font-weight: bold;" /><input type="hidden" name="num" value="100" />  </td>  </tr></table></form></td>
 <td style="vertical-align:top; text-align:center;">  <!-- Search Google --><form method="get" action="http://www.google.com/custom" target="_blank">
 <table class="footer">
 <tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="information behaviour, refugees" /><br />
 <input type="submit" name="sa" value="Google Search" style="font-family: Verdana; font-weight: bold; font-size: small;" /><input type="hidden" name="client" value="pub-5081678983212084" /><input type="hidden" name="forid" value="1" /><input type="hidden" name="ie" value="ISO-8859-1" /><input type="hidden" name="oe" value="ISO-8859-1" /><input type="hidden" name="cof" value="GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LGC:FF9900;LC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;FORID:1;" /><input type="hidden" name="hl" value="en" /></td></tr>
  </table></form></td>
 <td style="vertical-align:top; text-align:center;"><form method="get" action="http://www.bing.com" target="_blank">
 <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="information behaviour, refugees" /> <br /><input type="submit" name="sa" value="Bing"  style="font-size: small; font-family: Verdana; font-weight: bold;" /> <input type="hidden" name="num" value="100" /></td></tr>
 </table></form></td></tr>
 </table>

 <div style="text-align:center;">Check for citations, <a href="http://scholar.google.co.uk/scholar?hl=en&amp;q=http://informationr.net/ir/24-1/isic2018/isic1835.html&amp;btnG=Search&amp;as_sdt=2000">using Google Scholar</a></div>
 <br />
 <!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_inline_share_toolbox" style="text-align:center;"></div>
<hr />
</section>

<table class="footer" style="padding:10px;"><tr><td style="text-align:center; vertical-align:top;">
<br />
<div><a href="https://www.digits.net" target="_blank"><img src="https://counter.digits.net/?counter={c37e4719-b70a-b784-5d6e-c5e88ef7bb24}&amp;template=simple" alt="Hit Counter by Digits" /></a></div>
 </td>
<td class="footer" style="text-align:center; vertical-align:middle;">
  <div>  &copy; the authors, 2019. <br />Last updated: 1 March, 2019 </div></td>

 <td style="text-align:center; vertical-align:middle;">&nbsp;

  </td></tr>  </table>

 <footer>

<hr />
 <table class="footer"><tr><td>
<div class="button">
 <ul style="text-align: center;">
	<li><a href="isic2018.html">Contents</a> | </li>
	<li><a href="../../iraindex.html">Author index</a> | </li>
	<li><a href="../../irsindex.html">Subject index</a> | </li>
	<li><a href="../../search.html">Search</a> | </li>
	<li><a href="../../index.html">Home</a></li>
</ul>
</div></td></tr></table>
 <hr />
</footer>
 <script src="http://www.google-analytics.com/urchin.js">  </script>  <script>  _uacct =
 "UA-672528-1"; urchinTracker();
 </script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5046158704890f2e"></script>
</body>
 </html>
