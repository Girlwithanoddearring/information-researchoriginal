<!doctype html>
 <html lang="en">
 <head>
 <title>Semiotics based discursive communities in online book reviews</title>
<meta charset="utf-8" />
 <link href="../../IRstyle4.css" rel="stylesheet" media="screen" title="serif" />

 <!--Enter appropriate data in the content fields-->
 <meta name="dcterms.title" content="Semiotics based discursive communities in online book reviews" />
 
<meta name="citation_author" content="Yoon, Kyunghye" />
 <meta name="citation_author" content="Bezdicek, Adam" />

 <meta name="dcterms.subject" content="The aim of this paper is to explore the extent to which concepts of information behaviour have been adopted within other disciplines." />
 <meta name="description" content="The aim of this paper is to explore the extent to which concepts of information behaviour have been adopted within other disciplines, to the extent allowed by quantitative analysis of Web of Science data.  Searches were carried out in Web of Science in each decade from 1960 to the present day and the results analysed by the journals publishing related papers and by the research areas of these journals.  The 'Analyze Results' feature of Web of Science was used to provide quantitative analysis of the results, by journal title and by research area.  While papers on information behaviour appear in more than one hundred disciplinary areas, the distribution is concentrated in a very limited number of areas and is otherwise thinly spread over the remaining disciplines. Scholars in many disciplines have explored the information needs and information behaviour of those working in the discipline, or whom the disciplines serves. However, the concentration of interest is found in the health and medical sciences, computer science and information systems, communication and media studies, and psychology." />
 <meta name="keywords" content="discourse analysis, semiotics, textual analysis, virtual communities," />

 <!--leave the following to be completed by the Editor-->
 <meta name="robots" content="all" />
 <meta name="dcterms.publisher" content="University of Borås" />
 <meta name="dcterms.type" content="text" />
 <meta name="dcterms.identifier" content="ISSN-1368-1613" />
 <meta name="dcterms.identifier" content="http://InformationR.net/ir/24-1/isic2018/isic1838.html" />
 <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/24-1/infres241.html" />
 <meta name="dcterms.format" content="text/html" />  <meta name="dc.language" content="en" />
 <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
 <meta  name="dcterms.issued" content="2019-03-15" />
  <meta name="geo.placename" content="global" />

</head>

 <body>

<header>
 <img height="45" alt="header" src="../../mini_logo2.gif"  width="336" /><br />
<span style="font-size: medium; font-variant: small-caps; font-weight: bold;">published quarterly by the University of Bor&aring;s, Sweden<br /><br />vol. 24  no. 1, March, 2019</span>
<br /><br />
<div class="button">
 <ul>
 <li><a href="isic2018.html">Contents</a> |  </li>
 <li><a href="../../iraindex.html">Author index</a> |  </li>
 <li><a href="../../irsindex.html">Subject index</a> |  </li>
 <li><a href="../../search.html">Search</a> |  </li>
 <li><a href="../../index.html">Home</a> </li>
 </ul>
 </div>
 <hr />
  Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.
 <hr />
 </header>
 <article>
 <h1>Semiotics based discursive communities in online book reviews
</h1>  <br />

 <h2 class="author"><a href="#author">Kyunghye Yoon</a> and <a href="#author">Adam Bezdicek</a>
</h2>

 <br />

 <blockquote>
 <strong>Introduction</strong>.This paper discusses semiotics as a methodological framework for analysing user generated online book reviews (UGOBR) to identify shared collateral experiences conditional for communities of discourse to arise.<br />
<strong>Methods</strong>. Purposely selected UGOBR text from amazon.com were coded and analysed with a triadic semiotic model as a guiding framework.<br />
<strong>Analysis</strong>. Coding categories were analysed for common interpretive frames comprised of similar expressed experiences.<br />
<strong>Results</strong>. Groupings of collateral experience among reviewers emerged about a book, which seemed to indicate a common interpretive frame.<br />
<strong>Conclusion</strong>. The result suggested that reviewers’ meaning created from reading the same book can be identified to form a basis for collateral experience.
 </blockquote>

 <br />

 <section>

 <h2>Introduction</h2>

<p>Inquiry on information meaning is one of the central foci of LIS research. Any information activities assume an understanding of the meaning of information for the producer, the user and a processor. Information organization such as indexing or cataloguing involves a process of denoting and interpreting what it means to represent and create a surrogate for retrieval. Information users make meaning of information according to their need and use context. Meaning is embedded in information phenomena during its production, organization, retrieval, use and any intersection of these activities.</p>

<p>The rise of user generated content (UGC) and big data have resulted in an increased focus within LIS on machine processing, natural language recognition, and <em>semantic textuality</em> (<a href="#xia17">Xiao and Conroy, 2017</a>). Establishing a methodological framework for identifying communities of meaning within UGC would be beneficial to the identification of proto-communities of users in UGC contexts where formal communities and community building features may be lacking. In this paper, we will discuss semiotics as a useful methodological framework for interpretative analysis of user meaning in UGC, using UGOBR as a case study.</p>

</section>
<section>

<h2>Semiosis and community</h2>

<p>According to Peirce, &lsquo;<em>A sign is something, A, which brings something, B, its interpretant sign determined or created by it, into the same sort of correspondence with something, C, its object, as that in which itself stands to C</em>&rsquo; (<a href="#hua09">Huang and Chuang, 2009</a>, p. 342). The process by which people negotiate between these elements to create meaning is dynamic, ongoing, and generative of further signs and semiosis. The end result of the semiosis, or the interpretant, takes the form of a new <em>sign</em> generated from a meaningful interpretation of the relationship between the form a sign takes (representamen) and the concepts/things it represents (object) (<a href="#mai01">Mai, 2001</a>; <a href="#the03">Thellefsen, Brier and Thellefsen, 2003</a>; <a href="#the18">Thellefsen, Thellefsen and Sorensen, 2018</a>). As such, any new interpretant always bares the possibility that it could in turn become a representamen for a new process of semiosis, potentially stretching into infinity. Peirce grounded these distinctions with a hierarchical phenomenology outlining the varying ways in which a sign&rsquo;s triadic elements are manifested through human perception, sensation, and cognition. Based on this, he developed 10 sign classes (<a href="#mai01">Mai, 2001</a>).</p>

<p>He acknowledged that a single sign can mean different things to different people based upon their past experiences with the sign. Via his conception of legisign, or signs as <em>generalizable laws</em>, Peirce acknowledged that the only way that language can function and discourse can arise is via shared experience with signs (<a href="#pei55">Peirce, 1955</a>, p. 11). This invites the possibility of the formation of <em>semiotic communities</em>, or communities of individuals defined by a shared means of interpreting signs. Several within the field of ILS have begun to think of ways that semiotics might be applied to the study of communities (<a href="#bri96">Brier, 1996</a>; <a href="#fri13">Friedman and Smiraglia, 2013</a>; <a href="#sor16">Sorenson, Thellefsen and Thellefsen, 2016</a>). Schreiber (<a href="#sch13">2012</a>) adapts Irvine Goffman&rsquo;s notion of frame to study of individual users of collaborative learning software employ different framing devices for the interpretation of meaning.</p>

<p>Thellefsen and Thellefsen (<a href="#the13">2013</a>) supplement this conception of semiotic framing devices by distinguishing between the roles of <em>collateral experience </em>and <em>discursive communities </em>in semiotic processes. They use the concept of <em>collateral experience </em>or the <em>&lsquo;actual knowledge shared by the utterer and interpreter about concepts or phenomena in a communication process&rsquo; </em>(<a href="#the13">Thellefsen and Thellefsen, 2013</a>, p. 1739). The more experience that an individual has with the interpretation of a given representamen, the more limited and defined their interpretations will be. Individuals with shared collateral experiences are better able to arrive at a common understandings of the meaning of a sign, opening up a space where communities of discourse can arise. While discourse communities refer to norms for discussing a given topic, be they ad hoc or highly structured and specialized, <em>collateral experience </em>refers to shared experiences with a given sign that allow communities of discourse to arise.</p>

<p>This distinction between <em>discourse community </em>and communities of <em>collateral experience </em>is an important one to the study of ad hoc discursive communities that may arise along with UGC. While all processes of semiosis will be contextualized within some sort of discursive community, examining book reviews in terms of communities of <em>collateral experience</em> will help to identify common features within individuals that may lead to certain types of interpretations, even in the absence of any formalized community structures.</p>

</section>

<section>

<h2>Semiosis and reading</h2>

<p>Peirce&rsquo;s conception of semiosis provides a methodological framework for unpacking and differentiating between the complex relationship between individual perception and emotion, informational content, and socio-cultural context involved in the process of reading books, writing about books, and understanding the multiplicity of meanings that those books can have for individuals.&nbsp; Peirce allows us to begin to separate the different semiotic processes that are involved in the review writing process: (1) the semiosis of the book&rsquo;s author, (2) the semiosis of the reader who writes the book review, and (3) the semiosis of researcher analysing the review.</p>

<p>Applied to Peirce&rsquo;s triadic model of the sign, the text of the book can be viewed as a collection of signs each of which has a representamen, object, and interpretant.&nbsp; However, the exact nature of each of these triadic elements changes depending upon whether it is placed within the context of the book&rsquo;s author, the book&rsquo;s reader, or the researcher&rsquo;s analysis of UGOBR.&nbsp; To the author, the text of the book, as the end result of a semiotic process, can be viewed as the interpretant. The author&rsquo;s own semiosis, in writing the book, involves interpreting a host of signs and synthesizing this interpretation into collections of rhemes, dicent statements, and arguments. To the reader, the book&rsquo;s text constitutes a collection of representamen, a host of signs and also an object, what is signified by the book content, the idea that is conveyed in the book by the author. For the reader, the semiotic processes through which they interpret and make sense of the book, as a representamen, involve a complex interplay between their previous collateral experiences and their emotional and cognitive experiences of reading the book. As researchers, we analyse the text of the OBR as the reader&rsquo;s interpretant (or representamen, from our perspective), employing semiosis as a framework for interpretation and analysis.</p>

</section>

<section>

<h2>UGOBR text analysis</h2>

<p>The primary objective of this research was to explore using semiotics as not only a theoretical but also a methodological framework for empirically identifying the underlying experiential basis for shared semiotic frames that result in similar interpretation of signs among sets of individuals that do not necessarily self-identify with a common community. We employed it in the analysis of OBR text for examining a shared collateral experience of book reading. In this paper, we discuss how we used the triadic sign model as a guiding ground for coding and analysis of review text in order to see if and to what degree the collateral experience can be identified in user generated book reviews.</p>

<p>A corpus of 200 reviews was purposively gathered from Amazon (amazon.com) by selecting the top 20 <em>most helpful</em> user reviews for 10 popular science books. The body of the reviews along with relevant metadata were archived coded and analysed.</p>

<p>Coding and analysis were done inductively and iteratively with an interpretative approach, allowing categories to evolve through the iterative process. The analysis started with an individual review to generate a researcher&rsquo;s re-creation (i.e., researcher&rsquo;s interpretant) of the meaning combining with the multiple layers of coding categories, then moved across all reviews for a given book, grouping them together by their similarities in meaning and sentiment.</p>

<p>The initial phase focused on categorizing claims by topic to capture the researcher&rsquo;s meaning of the content as a relationship between the sign vehicle (i.e., review text) and the sign object (i.e., reviewer&rsquo;s meaning) of Peirce&rsquo;s triadic sign. A claim was defined as any chunk of a text representing a meaningful assertion and constituting a core unit of linguistic meaning (<a href="#yoo12">Yoon, 2012</a>; <a href="#yoo15">Yoon and Bezdicek, 2015</a>). It has yielded the following four categories; (1) the <em>book </em>itself and its author pertaining the book value; (2) the <em>book content</em> such as subject topics, theses, themes and summaries; (3) the <em>reviewer</em>&rsquo;s own personal experience, knowledge, preferences, expectations, and other aspects of their personal lives (<a href="#bez16">Bezdicek and Yoon, 2016</a>); and (4) the <em>recommendation</em>.</p>

<p>The second phase of coding and analysis involved shifting focus from our own semiotic processes as researchers to that of the reviewers. At this stage analysis focused on the reviews as interpretants, or the end result of the reviewer&rsquo;s semiotic processes. Within each of the 4 categories identified in stage 1, subcategories were developed using Peirce&rsquo;s typology of symbolic legisigns (rheme, dicent, and argument). Where appropriate, sentiment was coded using a combination of star ratings and, at the level of argument, whether the specific claim expressed a positive or negative disposition toward the book</p>

<p>The third phase analysed the reviewer&rsquo;s meanings across the reviews within a book, where the grouping of reviewers emerged based on the different meanings they created. Reviewer&rsquo;s personal statements were analysed across reviews to identify shared experiences constitutive of groupings of shared collateral experiences. Then, content statements and critiques were compared against personal statements to identify common interpretative frames among the reviewers. Pairings of similar content and personal statements were analysed and coded one last time to identify common interpretive <em>frames</em> comprised of similar expressed experiences that tended to appear with similar statements about a book&rsquo;s meaning. Frames were categorized at the level of content/personal pairings rather than at the level of the reviewer to detect instances where multiple frames might be employed within a single review.</p>

<p>Analysis of coding at multiple levels yielded a rich understanding of user meaning within the individual <em>frame</em>. It produced grouping of reviewers among which the <em>collateral experience</em> seems to play a critical role. Table 1 shows a breakdown of different sources of framing that were identified within the reviewer/personal category, along with descriptions and examples of the types of arguments deployed within each frame.</p>


<table class="center" style="width:60%;">
<caption><br />Table 1: Groupings of collateral experience among reviewers with Sample review text</caption>
<tbody>
<tr><th>Reviewer/Personal sources of framing</th><th>Description and examples</th>
</tr><tr><td rowspan="2">Argument positive: e.g., agreement or compliment with the author, book and content that seems to be in relation to or confirm with own knowledge and experience <br /></td><td style='text-align:left;'>Accept the author’s point based on logical thinking <br />E.g., ‘because there has been prior training, study, rehearsal that such wise snap judgement is possible’.(Z6 Review 9) <br />E.g., ‘All of us make our judgements based on our experience--experience that becomes so ingrained that we can become ultimately unaware of why we know what we know’. (Z6 Review 10) </td>
</tr><tr><td style='text-align:left;'>Argue against based on personal experience <br />E.g., ‘My credentials match the author's and I'm at least as well researched as she is, so I feel confident when I say this book muddies the waters more than parts them on the subject of introversion/extroversion’. (Z14 Review 11) <br />E.g., ‘And if, like me, you're an introvert who has been criticized for not liking small talk or socializing activities or been told that you think too much, this book will come as a great relief’. (Z14 Review 12) </td>
</tr><tr><td rowspan="3">Argument negative: e.g., disagreement with or objection to the author, book and content based on reviewer’s own knowledge, experience and world view <br /></td><td style='text-align:left;'>Criticizing logical weakness <br />E.g., ‘the book is a little too anecdotal... Too often conclusions are made on basic handwaving’, ‘the whole thing derails into examples that just don’t seem appropriate for the topic’. (Z6 Review 1) <br />E.g., ‘doesn’t arrive at a coherent and usable conclusion’. (Z6 Review 14) </td>
</tr><tr><td style='text-align:left;'>Didn’t apply to one’s life or personal experience <br />E.g., ‘As an introvert myself I think it missed part of the whole picture’. (Z14 Review 4) <br />E.g., ‘My credentials match the author's and I'm at least as well researched as she is, so I feel confident when I say this book muddies the waters more than parts them on the subject of introversion/extroversion’. (Z14 Review 11) </td>
</tr><tr><td style='text-align:left;'>Didn’t meet one’s expectation <br />E.g., ‘As I read through ‘Part one: the extrovert ideal’, I thought I had a winner. I thought I had a book that I was going to be able to say to all of you out there that are introverts and writers (because most actual writers are introverts (and I say it that way to exclude people (celebrities) who write books but don't really do the actual writing)), ‘You need to read this book’! But, as it turns out, I was wrong’. (Z14 Review 13)<br />E.g., ‘This is surprisingly patronizing, given its title. I hoped for a manifesto. What I got was pablum’. (Z14 Review 14) </td>
</tr><tr><td>Argument ambivalent or neutral <br /></td><td style='text-align:left;'>Split views on different aspects <br />E.g., ‘An excellent theory harmed by very poor examples’. (Z6 Review 13)<br /><br />Reviewer followed and took author’s points, still disappointed because it didn’t meet expectation<br />E.g., ‘Sound decision making is borne of a balance between instinctive, reactive realization and deliberate, studied conclusions. I think he pretty well proves the point, but he doesn’t really give us a road map for finding that balanced place’. (Z6 Review 11) <br />E.g., ‘with experience comes intuition’. ‘hoping ...a discussion on how to hone yourself to make better ‘blink’ decisions outside your field of expertise. The author does not address this at all...’. (Z6 Review 15) </td>
</tr></tbody>
</table>


</section>

<section>

<h2>Discussion and conclusion</h2>

<p>This paper explored using semiotics as a methodology for identifying shared interpretative framing devices deployed by writers of UGOBRs who expressed similar collateral experiences. Semiotics provided a methodological basis for bridging the conceptual relations of signs with empirical data analysis and presented semiotics as a useful methodological framework for untangling the distinct semiotic processes of the authors of the books being reviewed, the reviewers writing reviews of the book, and the researchers&rsquo; own processes of interpreting the OGBRs. The analysis suggested that reviewer&rsquo;s meaning created from reading, more specifically based on his or her own knowledge and experience, can be identified as a <em>frame</em> to interpret the sign object. These frames are grounded in similar collateral experiences and may be deployed to identify proto-communities of users in UGC contexts where formal communities and community building features may be lacking.</p>

</section>


<section>

<h2 id="author">About the authors</h2>

<p><strong>Kyunghye Yoon</strong> Associate Professor, in the MLIS Program at St. Catherine University, 2004 Randolph Ave. St. Paul, MN 55105. She received her PhD from Syracuse University. Her research interests include information seeking behavior, human information interaction, accessibility and user-experience design. She can be contacted at <a href="mailto:kyoon@stkate.edu">kyoon@stkate.edu</a>.<br />
<strong>Adam Bezdicek</strong> Librarian at Pine Technical and Community College, 400 4<sup>th</sup> St. SE, Pine City, MN 55063. He received his MA in English Literature from the University of St. Thomas and MLIS from St. Catherine University.&nbsp; His research interests include data librarianship, the digital humanities, and information literacy instruction. He can be contacted at <a href="mailto:adam.bezdicek@pine.edu">adam.bezdicek@pine.edu</a>.</p>

</section>
<section class="refs">

<h2>References</h2>
<ul class="refs">
<li id="bez16">Bezdicek, A. &amp; Yoon, K. (2016). <a href="http://hdl.handle.net/2142/89334">Investigating reading appeal in user generated online book reviews: reading as a meaning making process.</a> In <em>Proceedings of the iConference 2016. </em>Retrieved from http://hdl.handle.net/2142/89334</li>

<li id="bri96">Brier, S. (1996). Cybersemiotics: a new interdisciplinary development applied to the problems of knowledge organization and document retrieval in information science. <em>Journal of Documentation</em>, <em>52</em>(3), 296-344.</li>

<li id="fri13">Friedman, A. &amp; Smiraglia, R.P. (2013). Nodes and arcs: concept map, semiotics, and knowledge organization. <em>Journal of Documentation, 69</em>(1), 27-48.</li>

<li id="hua09">Huang, A. W.-C. &amp; Chuang, T.-R. (2009). Social tagging, online communication, and Peircean semiotics: a conceptual framework. <em>Journal of Information Science, 35</em>(3) 340&ndash;357.</li>

<li id="mai01">Mai, J. (2001). Semiotics and indexing: an analysis of the subject indexing process. <em>Journal of Documentation</em>, <em>57</em>(5), 591-622.</li>

<li id="pei55">Peirce, C.S. (1955). <em>Philosophical writings of Peirce</em>. New York, NY: Dover Publications.</li>

<li id="sch13">Schreiber, C. (2013). Semiotic processes in chat-based problem-solving situations. <em>Educational Studies in Mathematics</em>, <em>82</em>(1), 51-73.</li>

<li id="sor16">Sorenson, B., Thellefsen, T. &amp; Thellefsen, M. (2016). The meaning creation process, information, emotion, knowledge, two objects, and significance-effects: some peircean remarks. <em>Semiotica, </em>208, 21-33.</li>

<li id="the03">Thellefsen, T., Brier, S. &amp; Thellefsen, M. (2003). Problems concerning the process of subject analysis and the practice of indexing. <em>Semiotica, </em>144, 177-218.</li>

<li id="the13">Thellefsen, T. &amp; Thellefsen, M. (2013). Emotion, information, and cognition, and some possible consequences for library and information science. <em>Journal of the American Society for Information Science and Technology</em>, <em>64</em>(8), 1735-1750.</li>

<li id="the18">Thellefsen, M., Thellefsen, T. &amp; Sorensen, B. (2018). Information as signs: a semiotic analysis of the information concept, determining its ontological and epistemological foundations. <em>Journal of Documentation, 74</em>(2), 372-382.</li>

<li id="xia17">Xiao, L. &amp; Conroy, N. (2017). Discourse relations in rationale-containing text-segments. <em>Journal of the Association for Information Science and Technology</em>, <em>68</em>(12), 2783-2794.</li>

<li id="yoo12">Yoon, K. (2012). Conceptual syntagmatic associations in user tagging. <em>Journal of American Society for Information Science and Technology</em>, <em>63</em>(5), 923&ndash;935.</li>

<li id="yoo15">Yoon, K. &amp; Bezdicek, A. (2015). Using syntagmatic relations to examine semantic communities in user generated book reviews. <em>Proceedings of the 78th Association of Information Science and Technology Annual Meeting, 52</em>(1), 1-3.</li>

</ul>
</section>
<section>
<hr />
 <h4 style="text-align:center;">How to cite this paper</h4>
   <div class="citing">Yoon, K. and Bezdicek, A. (2019). Semiotics based discursive communities in online book reviews In <em>Proceedings of ISIC, The Information Behaviour Conference, Krakow, Poland, 9-11 October: Part 2. Information Research, 24</em>(1), paper isic1838. Retrieved from http://InformationR.net/ir/24-1/isic2018/isic1838.html (Archived by WebCite® at http://www.webcitation.org/76la6hqlK)</div>

</section>

<hr />

</article>
<br />
<section>

 <table class="footer" style="border-spacing:10px;">
 <tr>
 <td colspan="3" style="text-align:center; background-color: #5E96FD; color: white; font-family: verdana; font-size: small; font-weight: bold;">Find other papers on this subject</td></tr>
 <tr><td style="text-align:center; vertical-align:top;"><form method="get" action="http://scholar.google.com/scholar" target="_blank">
 <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"> <input type="hidden" name="q" value="discourse analysis, textual analysis, virtual communities" /><br />
<input type="submit" name="sa" value="Scholar Search"  style="font-size: small; font-family: Verdana; font-weight: bold;" /><input type="hidden" name="num" value="100" />  </td>  </tr></table></form></td>
 <td style="vertical-align:top; text-align:center;">  <!-- Search Google --><form method="get" action="http://www.google.com/custom" target="_blank">
 <table class="footer">
 <tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="discourse analysis, textual analysis, virtual communities" /><br />
 <input type="submit" name="sa" value="Google Search" style="font-family: Verdana; font-weight: bold; font-size: small;" /><input type="hidden" name="client" value="pub-5081678983212084" /><input type="hidden" name="forid" value="1" /><input type="hidden" name="ie" value="ISO-8859-1" /><input type="hidden" name="oe" value="ISO-8859-1" /><input type="hidden" name="cof" value="GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LGC:FF9900;LC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;FORID:1;" /><input type="hidden" name="hl" value="en" /></td></tr>
  </table></form></td>
 <td style="vertical-align:top; text-align:center;"><form method="get" action="http://www.bing.com" target="_blank">
 <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="discourse analysis, textual analysis, virtual communities" /> <br /><input type="submit" name="sa" value="Bing"  style="font-size: small; font-family: Verdana; font-weight: bold;" /> <input type="hidden" name="num" value="100" /></td></tr>
 </table></form></td></tr>
 </table>

 <div style="text-align:center;">Check for citations, <a href="http://scholar.google.co.uk/scholar?hl=en&amp;q=http://informationr.net/ir/24-1/isic2018/isic1838.html&amp;btnG=Search&amp;as_sdt=2000">using Google Scholar</a></div>
 <br />
 <!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_inline_share_toolbox" style="text-align:center;"></div>
<hr />
</section>

<table class="footer" style="padding:10px;"><tr><td style="text-align:center; vertical-align:top;">
<br />
<div><a href="https://www.digits.net" target="_blank"><img src="https://counter.digits.net/?counter={483df947-d5e6-2de4-79d8-61379b78368b}&amp;template=simple" alt="Hit Counter by Digits" /></a></div>
 </td>
<td class="footer" style="text-align:center; vertical-align:middle;">
  <div>  &copy; the authors, 2019. <br />Last updated: 1 March, 2019 </div></td>

 <td style="text-align:center; vertical-align:middle;">&nbsp;

  </td></tr>  </table>

 <footer>

<hr />
 <table class="footer"><tr><td>
<div class="button">
 <ul style="text-align: center;">
	<li><a href="isic2018.html">Contents</a> | </li>
	<li><a href="../../iraindex.html">Author index</a> | </li>
	<li><a href="../../irsindex.html">Subject index</a> | </li>
	<li><a href="../../search.html">Search</a> | </li>
	<li><a href="../../index.html">Home</a></li>
</ul>
</div></td></tr></table>
 <hr />
</footer>
 <script src="http://www.google-analytics.com/urchin.js">  </script>  <script>  _uacct =
 "UA-672528-1"; urchinTracker();
 </script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5046158704890f2e"></script>
</body>
 </html>
