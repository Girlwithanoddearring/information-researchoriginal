#### Information Research, Vol. 7 No. 2, January 2002,

# Critical realism and information systems research: why bother with philosophy?

#### [Philip J. Dobson](mailto:p.dobson@ecu.edu.au)  
School of Management Information Systems  
Edith Cowan University  
Churchlands, Western Australia

#### **Abstract**

> Garcia and Quek point out the difficulties in defining the actual object of information systems research - 'Is the object of research in information systems of a technological or social nature? Is it the organization, an information system or a social system?' ([Garcia & Quek, 1997](#gar97): 450). A recent IFIP conference on Organisational and Social Perspectives on Information technology examines some of the social and organisational issues involved in IS Research (IFIP TC8 WG8.2, 2000). At this conference [Jones](#jon00) (2000) examines the number of citations in the IFIP WG8.2 conference literature that refer to social theorists as a means to determine the level of interest in the use of social theory within IS. Over the period 1979-1999 57% of papers had references to social theorists - the four major theorists being Giddens, Habermas, Foucault and Latour. This paper suggests that it would be useful to widen the range of social theorists used in IS to include that of Bhaskar who proposes a critical realist approach to social research. In order to highlight the main aspects of the philosophy the paper will critique a framework developed to suggest IS research approaches.

## Introduction

Philosophy can be defined as "the critical examination of the grounds for fundamental beliefs and an analysis of the basic concepts employed in the expression of such beliefs" (Encyclopaedia Britannica). As the information systems (IS) research arena matures it is no surprise that a number of IS researchers in recent years have called for a clearer definition of the underlying philosophy and assumptions of IS research (e.g. [Mumford, _et al._](#mum85), 1985; [Banville & Landry](#ban89), 1989; [Iivari](#iiv91), 1991; [Orlikowski & Baroudi](#orl91), 1991; [Nissen, _et al._](#nis91), 1991; [Hirschhein _et al._, 1995](#hir95), [1996](#hir96); [Iivari & Hirschheim](#iiv96), 1996; [Winder _et al._](#win97), 1997; and [Mingers & Stowell](#min97), 1997).

[Garcia and Quek](#gar97) (1997) call for a greater critical awareness of the underlying assumptions implied by the use of particular theories in the IS field. They argue that the relative immaturity of the information systems field has lead to the "borrowing" of a number of theoretical approaches and methods from other subject areas, often with little regard for the associated baggage of underlying assumptions. IS research is an applied field in that it is heavily oriented towards the application of information systems in business. They suggest that this has resulted in research in the area to have a greater concentration on the outcomes and practical or methodological issues rather than the ontological and philosophical reasoning behind a particular research approach.

This practitioner focus is exemplified by Ormerod when he discusses organisational intervention from an operational research perspective:

> Any choice of mechanism should, in my view, be rooted in practical requirements rather than in theoretical considerations with which very few practitioners could feel at home. In simple terms the approach (methods and their theories) chosen must support a process of intervention (practice) in a particular context to achieve the desired outcome.

and

> If a philosophical justification for action is needed it can be found in American pragmatism, Bhaskar's transcendental realism, Feyerabend's iconoclastic critique of philosophical positions or postmodernism. ([Ormerod](#orm97), 1997: 421)

Ormerod's view of the role of philosophy neglects the useful role that philosophy can play as "underlabourer" to research and practice - the term under-labouring is taken from [Locke](#loc94) (1894: 14) as "clearing the ground a little... removing some of the rubbish that lies in the way of knowledge". Bhaskar's philosophy of critical (or transcendental) realism is presented as a useful under-labourer to research and practice: it suggests that philosophy plays an integral and important role in the social situations involved in research and practice; its continued role being contingent on the ultimate success of the resultant research or practice.

## Why bother with philosophy?

Collier asks the question "why philosophy" and answers it thus:

> A good part of the answer to the question "why philosophy?" is that the alternative to philosophy is not **no** philosophy, but **bad** philosophy. The "unphilosophical" person has an unconscious philosophy, which they apply in their practice - whether of science or politics or daily life. ([Collier, 1994](#col94): 17)

As an example of this situation, Ormerod's quotation detailed above reflects a particular practitioner focussed philosophy that has continued ongoing ramifications on the development process - these effects need to be made clear. As Gramsci argues "...everyone is a philosopher, though in his own way and unconsciously, since even in the slightest manifestation of any intellectual activity whatever, in 'language' there is contained a specific conception of the world" ([Gramsci](#gra71), 1971: 323, cited in [Collier, 1994](#col94): 17).

The "bothering" with philosophy can also provide the potential for emancipation from domination by one's social or academic grouping:

> ...is it better to take part in a conception of the world mechanically imposed by the external environment, i.e., by one of the many social groups in which everyone is automatically involved from the moment of his entry into the conscious world... Or, on the other hand, is it better to work out consciously and critically one's own conception of the world and thus, in connection with the labour's of ones own brain, choose one's sphere of activity, take an active part in the creation of the history of the world, be one's own guide, refusing to accept passively and supinely from outside the moulding of one's personality ([Gramsci, 1971](#gra71): 323-324 cited in [Collier, 1994](#col94): 17)

The confidence provided by understanding different philosophical positions provides the researcher and the practitioner with the power to argue for different research approaches and allows one confidently to choose one's own sphere of activity. The emancipatory potential of such knowledge is a powerful argument for "bothering with philosophy".

The importance of defining the philosophical position from which the researcher derives is also emphasised by [Walsham](#wal95) (1995: 80) who wishes to encourage IS case study researchers to reflect on the basis, conduct and reporting of their work. He encourages the adoption of multiple perspectives and feels that researchers need to reflect on their philosophical stance and explicitly define their stance when writing up their work. Both Walsham, and Garcia and Quek argue that a more coherent (i.e., consistent, rational, and logical) research process can be achieved through such philosophical reflection.

## The object of research

Garcia and Quek point out the difficulties in defining the actual object of information systems research - "_Is the object of research in information systems of a technological or social nature? Is it the organization, an information system or a social system?_" ([Garcia & Quek](#gar97), 1997: 450). A recent IFIP conference on Organisational and Social Perspectives on Information technology examined some of the social and organisational issues involved in IS Research (IFIP TC8 WG8.2, 2000). At this conference Jones (2000) examined the number of citations referring to social theorists in the IFIP WG8.2 conference literature to determine the level of interest in the use of social theory in IS. Over the period 1979-1999, 57% of papers had references to social theorists, the four major theorists being Giddens, Habermas, Foucault and Latour. This paper suggests that it would be useful to widen the range of social theorists used in IS to include that of [Bhaskar](#bha78) (1978, 1979, 1986).

Bhaskar's brand of realism (referred to by [Searle](#sea95) (1995) as external realism) argues that there exists a reality totally independent of our representations of it; the reality and the "representation of reality" operating in different domains - roughly a transitive epistemological dimension and an intransitive ontological dimension. For the realist, the most important driver for decisions on methodological approach will always be the intransitive dimension, the target being to unearth the real mechanisms and structures underlying perceived events. Critical realism acknowledges that observation is value-laden as Bhaskar pointed out in a recent interview:

> ...there is no conflict between seeing our scientific views as being about objectively given real worlds, and understanding our beliefs about them as subject to all kinds of historical and other determinations. ([Norris](#nor99), 1999)

The critical realist agrees that our knowledge of reality is a result of social conditioning and, thus, cannot be understood independently of the social actors involved in the knowledge derivation process. However, it takes issue with the belief that the reality itself is a product of this knowledge derivation process. The critical realist asserts that "real objects are subject to value laden observation"; the _reality_ and the value-laden _observation of reality_ operating in two different dimensions, one intransitive and relatively enduring; the other transitive and changing. Bhaskar argues that a lack of recognition of this division is a fundamental error of much of postmodernist work. This so-called epistemic fallacy assumes that statements about being (ontological statements) can be analysed in terms of statements about knowledge of that being (epistemological statements). An example of this ambiguity is provided by [Bhaskar](#bha91) (1991:. 10) when he addresses Kuhn's proposal regarding the incommensurability of paradigmatic positions: "though the world does not change with a change of paradigm, the scientist afterward works in a different world" ([Kuhn, 1970](#kuh70): 121). In Bhaskar's view Kuhn's statement is ambiguous in that it does not recognise the existence of two worlds, an intransitive world that is natural and (relatively) unchanging and a transitive world that is social and historical. This recognition allows the re-phrasing of the statement in an unremarkable and non-paradoxical manner: "Though the (natural (or object)) world does not change with the change of paradigm, the scientist afterward works in a different (social (or cognitive)) world".

In contrast to those approaches that adopt philosophical reflection after the event [Bhaskar's](#bha78) (1978, 1979, 1989) philosophy of critical realism sees philosophy as operating at the same level as methodological issues. Philosophical considerations are an integral part of the research process and the continued success of a philosophy is considered by [Bhaskar](#bha78) (1978) to be conditional on its success as an under-labourer to the research process. Philosophy is seen to be a social institution ([Irwin](#irw97), 1997) that has an important role to play in research, not as a permanent statement of position, but as conditional and intimately related to the outcomes and practice of research. This view of philosophy encourages a coherency in research, in that it sees philosophical suppositions concerning the nature of the world under study as an integral and important part of the research process.

Archer argues, from within a critical realist perspective, that:

> ...the nature of what exists cannot be unrelated to how it is studied ... the social ontology endorsed does play a powerful regulatory role vis-à-vis the explanatory methodology for the basic reason that it conceptualises social reality in certain terms, thus identifying what there is to be explained and also ruling out explanations in terms of entities or properties which are deemed non-existent" ([Archer](#arc95), 1995: 16-17).

There is thus a strong argument that ontology and methodology should be closely linked:

> Once social analysts have been assured that ontology and methodology are separate issues, why should they not conclude that they can merely select the methodology which pragmatically seems most useful to them (thus sliding rapidly into instrumentalism), because if ontology is a separate concern, then it need to be no concern of theirs. Equally, once social theorists have been persuaded of the separation, what prevents an exclusive preoccupation with ontological matters, disregarding their practical utility and effectively disavowing that acquiring knowledge about the world does and should affect conceptions of social reality? This is a recipe for theoretical sterility. An ontology without a methodology is deaf and dumb; a methodology without an ontology is blind. Only if the two go hand in hand can we avoid a discipline in which the deaf and the blind lead in different directions, both of which end in _cul de sacs_. ([Archer](#arc95), 1995, p.28)

As [Arche](#arc95)r (1995: 17) points out _"What social reality is held to **be** also **is** that which we seek to explain"_.

## The transitive/intransitive divide

From within an interpretivist perspective [Trauth](#tra01) (2001) discusses the important factors in selecting research approaches considers the important factors that affect researcher decisions regarding the research approach to adopt. She argues that the following factors are important in the decision to use qualitative approaches and in the actual choice of qualitative methods:

*   The research problem
*   The researcher's theoretical lens
*   The degree of uncertainty surrounding the phenomena
*   The researcher's skills
*   Academic politics

It is useful to present a critical realist perspective on her argument. She suggests that the most important factor may well be the research problem: _"what one wants to learn suggests how one should go about it"_ ([Trauth](#tra01), 2001: 4). A particular example is given of her own study of Ireland's information economy where she wished to uncover the _"story behind the statistics"_. This purpose led her to select an interpretative approach as the most appropriate means of examining such a topic. The researcher's theoretical lens is also suggested as playing an important role in the choice of methods because the underlying belief system of the researcher largely defines the choice of method. In fact, in many cases, she suggests that the choice of lens is often driven by a desire to avoid the shortcomings of positivism. Another important factor in the decision to adopt qualitative approaches may be the high levels of uncertainty in the problem situation, which could lead to difficulties in positivistic measurement. Researchers' skills can also define what methods are to be used, as can academic politics.

For the critical realist, most of the factors suggested by Trauth can be seen to be primarily concerned with the transitive epistemological aspects of the research process rather than the intransitive ontological aspects, as Table 1 shows:

<table><caption>

**Table 1: Epistemological and ontological concerns**</caption>

<tbody>

<tr>

<th>Primarily epistemological concerns</th>

<th>Primarily ontological concerns</th>

</tr>

<tr>

<td>The researcher's theoretical lens</td>

<td>The research problem</td>

</tr>

<tr>

<td>The researcher's skills</td>

<td>The degree of uncertainty surrounding the phenomena</td>

</tr>

<tr>

<td>Academic politics</td>

<td> </td>

</tr>

</tbody>

</table>

The interpretativist does not make this distinction, as Orlikowski and Baroudi suggest:

> Interpretivism asserts that reality, as well as our knowledge thereof, are social products and hence incapable of being understood independent of the social actors (including the researchers) that construct and make sense of that reality. ([Orlikowski & Baroudi](#orl91), 1991: 13)

Critical realism would suggest that this argument reflects the epistemic fallacy in that it confuses the transitive and intransitive dimensions. For the realist, reality can never be a social product since it pre-exists the transitive, changing social analysis of it. Our perceptions of reality change continually but the underlying structures and mechanisms constituting that reality are "relatively enduring". The aim of realist research is to develop a better understanding of these enduring structures and mechanisms. Ontological factors, therefore, must be the primary factor in defining research approaches; this requirement necessarily forcing a strong philosophical commitment. For the realist, academic politics and traditional researcher skills and background should not define research approaches; the nature of what is to be investigated is the primary concern.

## Critiquing an interpretativist framework

[Galliers](#gal92) (1992) proposes a taxonomy for providing guidance in selecting information system research approaches ([see Table 2](../p112tab2.gif)). The framework suggests that by selecting the object of one's research (society, group or individual) or the purpose of the research (theory testing, theory building or theory extension) one can get a feeling for which research approach would be most suitable. Although not stated it is clear that this framework is grounded in an interpretative perspective - an analysis from a critical realist perspective provides some interesting contrasts.

The first observation critical realism would make is that one cannot concentrate solely on a single level investigation of the society, group or individual: critical realism argues for a relational perspective, seeing society as _"an ensemble of structures, practices and conventions that individuals reproduce or transform"_ ([Bhaskar](#bha91), 1991: 76). Society is a _"skilled accomplishment of active agents"_ (p. 4) and the flat ontology suggested by the concentration on a single aspect of social situations (society, group or individual) would restrict explanatory power ([Reed](#ree97), 1997). Critical realism would suggest (as in [Layder](#lay93), 1993) that in order to practically investigate a social situation one may need to examine each level in turn but the interactions between each structure identified at the different levels cannot be ignored.

Another issue with Galliers's two-dimensional framework is that the "approach" is compared against the object and the purpose of the study: this must neglect, therefore, the important relationship between the purpose and the object. As [Sayer](#say92) (1992: 4) argues, the choice of method or approach must be _"appropriate to the nature of the object under study and the purpose and expectation of the study"_. Galliers's model does not reflect this aim (and being two-dimensional obviously cannot do so without a deal of difficulty). As detailed in Figure 1, for the realist the purpose of the study (intensive, abstract or generalisable research) helps to define the particular underlying aspects of reality upon which the realist researcher should concentrate. For example, abstract, theoretical research does not specifically deal with events apart from as possible outcomes, whereas if generalisation is the target then the researcher would need consider how events may be related across different settings, seeking regularities and common properties at this level. Intensive research for the realist involves the consideration of particular contexts and combinations of isolated structures, mechanisms and actual events. The purpose of the study largely defines at what level of reality the researcher should operate.

The primary focus for the realist is first, to identify the underlying objects of research, which then helps to define the approaches that should be adopted; the approach is secondary. For the realist it is implied that, once we know what we are looking for, any number of approaches can be adopted and applied in different, novel ways; the target being to unearth the real structures and mechanisms within a particular research situation. Galliers's model is more interpretative in focus, concentrating more on the interpretative power of differing approaches rather than on the underlying purpose and object.

Galliers's framework implies that the methods of the sciences **can** be extended to social investigation in that, for example, it allows for the use of laboratory experiment and theorem proof to investigate social situations. This is in line with the view of the critical realist who believes that the methods of the sciences can be (carefully/critically) extended to the study of the natural or social sciences. The realist, however, would suggest that the inability to create "closure" in the social arena fundamentally affects the conclusions reached and the applicability of the different scientific approaches. For the realist, the inability to create experimental closure requires that the primary aim of research must be explanatory (the equivalent of Galliers's theory building?) rather than prediction (Galliers's theory extension?) or falsification (Galliers's theory testing). Predictive use of theory and theory testing is limited within the critical realist approach because of the inability to create closure in social situations. This necessitates that any derived theory from social investigation can only indicate "tendencies" rather than provide clear prediction. Similarly, falsification on the basis of social observation is never fully possible.

Consistency is a major aim of realist research, the tripartite connections between ontology, methodology and practical theory being most important. As Archer argues:

> ...the social ontology endorsed does play a powerful regulatory role vis-à-vis the explanatory methodology for the basic reason that it conceptualises social reality in certain terms. Thus identifying what there is to be explained and also ruling out explanations in terms of entities or properties which are deemed non-existent. Conversely, regulation is mutual, for what is held to exist cannot remain immune from what is really, actually or factually found to be the case. Such consistency is a general requirement and it usually requires two-way adjustment. ([Archer](#arc95), 1995: 17)

This two-way adjustment requires a contingent ontology in order to work, since if a particular non-consistent theory works very well it may well raise ontological and philosophical questions that would need to be addressed.

<figure>

![Figure 1](../p112fig1.gif)

<figcaption>

**Figure 1: Structures, mechanisms and events (based on [Sayer](#say92), 1992: 11)**</figcaption>

</figure>

## Conclusion

[Mingers](#min00) (2000) suggests that critical realism can be useful as the underpinning philosophy for operations research and management science and systems. This paper suggests that critical realism may also provide a useful grounding for information systems research in general by elevating the importance of philosophical issues and thus allowing for a more consistent approach to research. Its recognition of a transitive and intransitive dimension to reality provides a useful basis for bridging the dualism between subjective and objective views of reality: "real objects are subject to value-laden observation". As detailed above, critical realism can provide useful guidance in the selection of methodological approaches and is useful in providing consistency in research approach.

## Acknowledgements

My thanks to the anonymous referees whose comments helped considerably in the preparation of this paper for publication.

## References

*   <a id="arc95"></a>Archer, M. (1995). _Realist social theory: the morphogenetic approach_. Cambridge: Cambridge University Press.

*   <a id="ban89"></a>Banville, C. and Landry, M., (1989). "Can the field of MIS be disciplined?" _Communications of the ACM_. **32**(1), 48-60

*   <a id="bha78"></a>Bhaskar, R. (1978) _A realist theory of science._ Hassocks: Harvester Press.

*   <a id="bha79"></a>Bhaskar, R. (1979) _The possibility of naturalism._ Brighton: Harvester Press.

*   <a id="bha86"></a>Bhaskar, R. (1986) _Scientific realism and human emancipation._ London: Verso .

*   <a id="bha89"></a>Bhaskar, R. (1989)._Reclaiming reality: a critical introduction to contemporary philosophy_. London: Verso.

*   <a id="bha91"></a>Bhaskar, R. (1991). _Philosophy and the idea of freedom._ Oxford: Blackwell.

*   <a id="col94"></a>Collier, A. (1994). _Critical realism: an introduction to the philosophy of Roy Bhaskar_. London: Verso.

*   <a id="cra92"></a>Craib, I. (1992) _Modern social theory: from Parsons to Habermas_. Hemel Hempstead: Harvester Wheatsheaf

*   <a id="gal92"></a>Galliers, R.D. (1992). "Choosing information systems research approaches", in R.D. Galliers, ed., _Information systems research: issues, methods, and practical guidelines._ . pp.144-162\. Oxford: Blackwell Scientific Publications.

*   <a id="gar97"></a>Garcia, L. & Quek, F. (1997). "Qualitative research in information systems: time to be subjective?" in A.S. Lee, J. Liebenau and J.I. DeGross, eds. _Information systems and qualitative research_. pp.542-568\. London: Chapman and Hall.

*   <a id="gra71"></a>Gramsci, A. (1971). _Prison notebooks_. London: Lawrence & Wishart.

*   <a id="hir95"></a>Hirschheim, R., Klein H.K. and Lyytinen, K., (1995). _Information systems development and data modeling: conceptual and philosophical foundations_. Cambridge: Cambridge University Press.

*   <a id="hir96"></a>Hirschheim, R., Klein H.K. and Lyytinen, K., (1996). "Exploring the intellectual foundations of information systems", _Accounting , Management and Information Technologies_. **6**(1/2), 1-64

*   <a id="iiv91"></a>Iivari, J., (1991). "A paradigmatic analysis of contemporary schools of IS development", _European Journal of Information Systems_. **1**, (4), 249-272

*   <a id="iiv96"></a>Iivari, J. and Hirschheim, R., (1996). "Analyzing information systems development: a comparison and analysis of eight IS development approaches", _Information Systems_. **21**(7), 551-575

*   <a id="irw97"></a>Irwin, L. (1997). _[The WSCR Glossary](http://www.raggedclaws.com/criticalrealism/glossary/index.html)_. Available at http://www.raggedclaws.com/criticalrealism/glossary/index.html, [Accessed 13th September, 2000]

*   <a id="jon00"></a>Jones, M. (2000). "The moving finger: the use of social theory in WG 8.2 Conference Papers, 1975-1999", in: Richard Baskerville, Jan Stage, & Janice I. DeGross., _eds_. _Organizational and social perspectives on information technology_. Boston: Kluwer Academic Publishers.

*   <a id="kuh70"></a>Kuhn, T. (1970) _The structure of scientific revolutions_. 2nd ed. Chicago: University of Chicago Press.

*   <a id="lay93"></a>Layder, D. (1993). _New strategies in social research: an introduction and guide_. Cambridge: Polity Press.

*   <a id="loc94"></a>Locke, J. (1894). _An essay concerning human understanding._ A.c. Fraser (ed.): Vol. 1, Clarendon, Oxford.

*   <a id="mil94"></a>Miles, M. and Huberman, M. (1994). _Qualitative data analysis_. Thousand Oaks, CA: Sage.

*   <a id="min00"></a>Mingers, J. (2000). The contribution of critical realism as an underpinning philosophy for OR/MS and systems, _Journal of the Operational Research Society_. **51**(11), 1256-1270.

*   <a id="min97"></a>Mingers, J. and Stowell, F., _eds_. (1997). _Information systems: an emerging discipline?_. London: McGraw-Hill.

*   <a id="mum"></a>Mumford, E., Hirschheim, R., Fitzgerald, G. and Wood-Harper, A.T., eds. (1985). _Research methods in information systems_. Amsterdam: North-Holland

*   <a id="nis91"></a>Nissen, H.-E., Klein, H.K. and Hirschheim, R. (eds.). (1991). _Information systems research: contemporary approaches & emergent traditions_. Amsterdam: North-Holland.

*   <a id="nor99"></a>Norris, C. (1999). Bhaskar Interview, _The Philosophers' Magazine._ No. 8, Autumn, 34.

*   <a id="orl91"></a>Orlikowski W.J. and Baroudi, J.J., (1991). "Studying information technology in organizations: research approaches and assumptions", _Information Systems Research_. **2**(1), 1-28

*   <a id="orm97"></a>Ormerod, R.J. (1997). The design of organisational intervention: choosing the approach. _Omega_. **25**(4), 415-435.

*   <a id="ree97"></a>Reed, M.I. (1997). In praise of duality and dualism: rethinking agency and structure in organisational analysis, _Organisation Studies_. **18**(1), 21-42.

*   <a id="row95"></a>Rowland, G. (1995). Archetypes of systems design, _Systems Practice_. **8**(3), 277-289.

*   <a id="say92"></a>Sayer, R.A. (1992). _Method in social science: a realist approach_. London: Routledge.

*   <a id="sea95"></a>Searle, J.R. (1995). _The construction of social reality_. New York, NY: Free Press.

*   <a id="tra01"></a>Trauth, E. M., _ed._ (2001). _Qualitative research in information systems: issues and trends._ Hershey, PA: Idea Group Publishing.

*   <a id="tso92"></a>Tsoukas, H. (1992) Panoptic reason and the search for totality: a critical assessment of the critical systems perspective, _Human Relations_. **45**(7), 637-657

*   <a id="wal93"></a>Walsham, G., (1993) _Interpreting information systems in organisations_. Chichester: Wiley.

*   <a id="wal95"></a>Walsham, G., (1995). interpretative case studies in IS research: nature and method, _European Journal of Information Systems_, **4**, 74-81\.

*   <a id="win97"></a>Winder,R.L., Probert, S.K., & Beeson, I.A., _eds_. 1997, _Philosophical aspects of information systems_. London: Taylor & Francis.