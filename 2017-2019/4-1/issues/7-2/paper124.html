<!DOCTYPE html>
<html lang="en">

<head>
	<title>Critical realism and information systems research: why bother with philosophy?</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="keywords"
		content="information systems, realism, philosophy, research framework, critical realism, social theory, social research">
	<meta name="description"
		content="Garcia and Quek (1997) point out the difficulties in defining the actual object of information systems research - 'Is the object of research in information systems of a technological or social nature? Is it the organization, an information system or a social system?' (p. 450). A recent IFIP conference on Organisational and Social Perspectives on Information technology examines some of the social and organisational issues involved in IS Research (IFIP TC8 WG8.2, 2000). At this conference Jones (2000) examined the number of citations in the IFIP WG8.2 conference literature that refer to social theorists to determine the level of interest in the use of social theory in information systems research. Over the period 1979-1999 57% of papers had references to social theorists - the four major theorists being Giddens, Habermas, Foucault and Latour. This paper suggests that it would be useful to widen the range of social theorists used in IS to include that of Bhaskar who proposes a critical realist approach to social research. In order to highlight the main aspects of the philosophy the paper will critique a framework developed to suggest IS research approaches.">
	<meta name="rating" content="Mature">
	<meta name="VW96.objecttype" content="Document">
	<meta name="ROBOTS" content="ALL">
	<meta name="DC.Title" content="Critical realism and IS Research - Why bother with philosophy?">
	<meta name="DC.Creator" content="Philip J. Dobson">
	<meta name="DC.Subject"
		content="information systems, realism, philosophy, research framework, critical realism, social theory, social research">
	<meta name="DC.Description"
		content="Garcia and Quek (1997) point out the difficulties in defining the actual object of information systems research - 'Is the object of research in information systems of a technological or social nature? Is it the organization, an information system or a social system?' (p. 450). A recent IFIP conference on Organisational and Social Perspectives on Information technology examines some of the social and organisational issues involved in IS Research (IFIP TC8 WG8.2, 2000). At this conference Jones (2000) examined the number of citations in the IFIP WG8.2 conference literature that refer to social theorists to determine the level of interest in the use of social theory in information systems research. Over the period 1979-1999 57% of papers had references to social theorists - the four major theorists being Giddens, Habermas, Foucault and Latour. This paper suggests that it would be useful to widen the range of social theorists used in IS to include that of Bhaskar who proposes a critical realist approach to social research. In order to highlight the main aspects of the philosophy the paper will critique a framework developed to suggest IS research approaches.">
	<meta name="DC.Publisher" content="Information Research">
	<meta name="DC.Coverage.PlaceName" content="Global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="information-research-vol-7-no-2-january-2002">Information Research, Vol. 7 No. 2, January 2002,</h4>
	<h1 id="critical-realism-and-information-systems-research-why-bother-with-philosophy">Critical realism and
		information systems research: why bother with philosophy?</h1>
	<h4 id="philip-j-dobson"><a href="mailto:p.dobson@ecu.edu.au">Philip J. Dobson</a></h4>
	<p>School of Management Information Systems<br>
		Edith Cowan University<br>
		Churchlands, Western Australia</p>
	<h4 id="abstract"><strong>Abstract</strong></h4>
	<blockquote>
		<p>Garcia and Quek point out the difficulties in defining the actual object of information systems research -
			'Is the object of research in information systems of a technological or social nature? Is it the
			organization, an information system or a social system?' (<a href="#gar97">Garcia &amp; Quek, 1997</a>:
			450). A recent IFIP conference on Organisational and Social Perspectives on Information technology examines
			some of the social and organisational issues involved in IS Research (IFIP TC8 WG8.2, 2000). At this
			conference <a href="#jon00">Jones</a> (2000) examines the number of citations in the IFIP WG8.2 conference
			literature that refer to social theorists as a means to determine the level of interest in the use of social
			theory within IS. Over the period 1979-1999 57% of papers had references to social theorists - the four
			major theorists being Giddens, Habermas, Foucault and Latour. This paper suggests that it would be useful to
			widen the range of social theorists used in IS to include that of Bhaskar who proposes a critical realist
			approach to social research. In order to highlight the main aspects of the philosophy the paper will
			critique a framework developed to suggest IS research approaches.</p>
	</blockquote>
	<h2 id="introduction">Introduction</h2>
	<p>Philosophy can be defined as &quot;the critical examination of the grounds for fundamental beliefs and an
		analysis of the basic concepts employed in the expression of such beliefs&quot; (Encyclopaedia Britannica). As
		the information systems (IS) research arena matures it is no surprise that a number of IS researchers in recent
		years have called for a clearer definition of the underlying philosophy and assumptions of IS research (e.g. <a
			href="#mum85">Mumford, <em>et al.</em></a>, 1985; <a href="#ban89">Banville &amp; Landry</a>, 1989; <a
			href="#iiv91">Iivari</a>, 1991; <a href="#orl91">Orlikowski &amp; Baroudi</a>, 1991; <a
			href="#nis91">Nissen, <em>et al.</em></a>, 1991; <a href="#hir95">Hirschhein <em>et al.</em>, 1995</a>, <a
			href="#hir96">1996</a>; <a href="#iiv96">Iivari &amp; Hirschheim</a>, 1996; <a href="#win97">Winder <em>et
				al.</em></a>, 1997; and <a href="#min97">Mingers &amp; Stowell</a>, 1997).</p>
	<p><a href="#gar97">Garcia and Quek</a> (1997) call for a greater critical awareness of the underlying assumptions
		implied by the use of particular theories in the IS field. They argue that the relative immaturity of the
		information systems field has lead to the &quot;borrowing&quot; of a number of theoretical approaches and
		methods from other subject areas, often with little regard for the associated baggage of underlying assumptions.
		IS research is an applied field in that it is heavily oriented towards the application of information systems in
		business. They suggest that this has resulted in research in the area to have a greater concentration on the
		outcomes and practical or methodological issues rather than the ontological and philosophical reasoning behind a
		particular research approach.</p>
	<p>This practitioner focus is exemplified by Ormerod when he discusses organisational intervention from an
		operational research perspective:</p>
	<blockquote>
		<p>Any choice of mechanism should, in my view, be rooted in practical requirements rather than in theoretical
			considerations with which very few practitioners could feel at home. In simple terms the approach (methods
			and their theories) chosen must support a process of intervention (practice) in a particular context to
			achieve the desired outcome.</p>
	</blockquote>
	<p>and</p>
	<blockquote>
		<p>If a philosophical justification for action is needed it can be found in American pragmatism, Bhaskar's
			transcendental realism, Feyerabend's iconoclastic critique of philosophical positions or postmodernism. (<a
				href="#orm97">Ormerod</a>, 1997: 421)</p>
	</blockquote>
	<p>Ormerod's view of the role of philosophy neglects the useful role that philosophy can play as
		&quot;underlabourer&quot; to research and practice - the term under-labouring is taken from <a
			href="#loc94">Locke</a> (1894: 14) as &quot;clearing the ground a little... removing some of the rubbish
		that lies in the way of knowledge&quot;. Bhaskar's philosophy of critical (or transcendental) realism is
		presented as a useful under-labourer to research and practice: it suggests that philosophy plays an integral and
		important role in the social situations involved in research and practice; its continued role being contingent
		on the ultimate success of the resultant research or practice.</p>
	<h2 id="why-bother-with-philosophy">Why bother with philosophy?</h2>
	<p>Collier asks the question &quot;why philosophy&quot; and answers it thus:</p>
	<blockquote>
		<p>A good part of the answer to the question &quot;why philosophy?&quot; is that the alternative to philosophy
			is not <strong>no</strong> philosophy, but <strong>bad</strong> philosophy. The &quot;unphilosophical&quot;
			person has an unconscious philosophy, which they apply in their practice - whether of science or politics or
			daily life. (<a href="#col94">Collier, 1994</a>: 17)</p>
	</blockquote>
	<p>As an example of this situation, Ormerod's quotation detailed above reflects a particular practitioner focussed
		philosophy that has continued ongoing ramifications on the development process - these effects need to be made
		clear. As Gramsci argues &quot;...everyone is a philosopher, though in his own way and unconsciously, since even
		in the slightest manifestation of any intellectual activity whatever, in 'language' there is contained a
		specific conception of the world&quot; (<a href="#gra71">Gramsci</a>, 1971: 323, cited in <a
			href="#col94">Collier, 1994</a>: 17).</p>
	<p>The &quot;bothering&quot; with philosophy can also provide the potential for emancipation from domination by
		one's social or academic grouping:</p>
	<blockquote>
		<p>...is it better to take part in a conception of the world mechanically imposed by the external environment,
			i.e., by one of the many social groups in which everyone is automatically involved from the moment of his
			entry into the conscious world... Or, on the other hand, is it better to work out consciously and critically
			one's own conception of the world and thus, in connection with the labour's of ones own brain, choose one's
			sphere of activity, take an active part in the creation of the history of the world, be one's own guide,
			refusing to accept passively and supinely from outside the moulding of one's personality (<a
				href="#gra71">Gramsci, 1971</a>: 323-324 cited in <a href="#col94">Collier, 1994</a>: 17)</p>
	</blockquote>
	<p>The confidence provided by understanding different philosophical positions provides the researcher and the
		practitioner with the power to argue for different research approaches and allows one confidently to choose
		one's own sphere of activity. The emancipatory potential of such knowledge is a powerful argument for
		&quot;bothering with philosophy&quot;.</p>
	<p>The importance of defining the philosophical position from which the researcher derives is also emphasised by <a
			href="#wal95">Walsham</a> (1995: 80) who wishes to encourage IS case study researchers to reflect on the
		basis, conduct and reporting of their work. He encourages the adoption of multiple perspectives and feels that
		researchers need to reflect on their philosophical stance and explicitly define their stance when writing up
		their work. Both Walsham, and Garcia and Quek argue that a more coherent (i.e., consistent, rational, and
		logical) research process can be achieved through such philosophical reflection.</p>
	<h2 id="the-object-of-research">The object of research</h2>
	<p>Garcia and Quek point out the difficulties in defining the actual object of information systems research -
		&quot;<em>Is the object of research in information systems of a technological or social nature? Is it the
			organization, an information system or a social system?</em>&quot; (<a href="#gar97">Garcia &amp; Quek</a>,
		1997: 450). A recent IFIP conference on Organisational and Social Perspectives on Information technology
		examined some of the social and organisational issues involved in IS Research (IFIP TC8 WG8.2, 2000). At this
		conference Jones (2000) examined the number of citations referring to social theorists in the IFIP WG8.2
		conference literature to determine the level of interest in the use of social theory in IS. Over the period
		1979-1999, 57% of papers had references to social theorists, the four major theorists being Giddens, Habermas,
		Foucault and Latour. This paper suggests that it would be useful to widen the range of social theorists used in
		IS to include that of <a href="#bha78">Bhaskar</a> (1978, 1979, 1986).</p>
	<p>Bhaskar's brand of realism (referred to by <a href="#sea95">Searle</a> (1995) as external realism) argues that
		there exists a reality totally independent of our representations of it; the reality and the
		&quot;representation of reality&quot; operating in different domains - roughly a transitive epistemological
		dimension and an intransitive ontological dimension. For the realist, the most important driver for decisions on
		methodological approach will always be the intransitive dimension, the target being to unearth the real
		mechanisms and structures underlying perceived events. Critical realism acknowledges that observation is
		value-laden as Bhaskar pointed out in a recent interview:</p>
	<blockquote>
		<p>...there is no conflict between seeing our scientific views as being about objectively given real worlds, and
			understanding our beliefs about them as subject to all kinds of historical and other determinations. (<a
				href="#nor99">Norris</a>, 1999)</p>
	</blockquote>
	<p>The critical realist agrees that our knowledge of reality is a result of social conditioning and, thus, cannot be
		understood independently of the social actors involved in the knowledge derivation process. However, it takes
		issue with the belief that the reality itself is a product of this knowledge derivation process. The critical
		realist asserts that &quot;real objects are subject to value laden observation&quot;; the <em>reality</em> and
		the value-laden <em>observation of reality</em> operating in two different dimensions, one intransitive and
		relatively enduring; the other transitive and changing. Bhaskar argues that a lack of recognition of this
		division is a fundamental error of much of postmodernist work. This so-called epistemic fallacy assumes that
		statements about being (ontological statements) can be analysed in terms of statements about knowledge of that
		being (epistemological statements). An example of this ambiguity is provided by <a href="#bha91">Bhaskar</a>
		(1991:. 10) when he addresses Kuhn's proposal regarding the incommensurability of paradigmatic positions:
		&quot;though the world does not change with a change of paradigm, the scientist afterward works in a different
		world&quot; (<a href="#kuh70">Kuhn, 1970</a>: 121). In Bhaskar's view Kuhn's statement is ambiguous in that it
		does not recognise the existence of two worlds, an intransitive world that is natural and (relatively)
		unchanging and a transitive world that is social and historical. This recognition allows the re-phrasing of the
		statement in an unremarkable and non-paradoxical manner: &quot;Though the (natural (or object)) world does not
		change with the change of paradigm, the scientist afterward works in a different (social (or cognitive))
		world&quot;.</p>
	<p>In contrast to those approaches that adopt philosophical reflection after the event <a
			href="#bha78">Bhaskar's</a> (1978, 1979, 1989) philosophy of critical realism sees philosophy as operating
		at the same level as methodological issues. Philosophical considerations are an integral part of the research
		process and the continued success of a philosophy is considered by <a href="#bha78">Bhaskar</a> (1978) to be
		conditional on its success as an under-labourer to the research process. Philosophy is seen to be a social
		institution (<a href="#irw97">Irwin</a>, 1997) that has an important role to play in research, not as a
		permanent statement of position, but as conditional and intimately related to the outcomes and practice of
		research. This view of philosophy encourages a coherency in research, in that it sees philosophical suppositions
		concerning the nature of the world under study as an integral and important part of the research process.</p>
	<p>Archer argues, from within a critical realist perspective, that:</p>
	<blockquote>
		<p>...the nature of what exists cannot be unrelated to how it is studied ... the social ontology endorsed does
			play a powerful regulatory role vis-à-vis the explanatory methodology for the basic reason that it
			conceptualises social reality in certain terms, thus identifying what there is to be explained and also
			ruling out explanations in terms of entities or properties which are deemed non-existent&quot; (<a
				href="#arc95">Archer</a>, 1995: 16-17).</p>
	</blockquote>
	<p>There is thus a strong argument that ontology and methodology should be closely linked:</p>
	<blockquote>
		<p>Once social analysts have been assured that ontology and methodology are separate issues, why should they not
			conclude that they can merely select the methodology which pragmatically seems most useful to them (thus
			sliding rapidly into instrumentalism), because if ontology is a separate concern, then it need to be no
			concern of theirs. Equally, once social theorists have been persuaded of the separation, what prevents an
			exclusive preoccupation with ontological matters, disregarding their practical utility and effectively
			disavowing that acquiring knowledge about the world does and should affect conceptions of social reality?
			This is a recipe for theoretical sterility. An ontology without a methodology is deaf and dumb; a
			methodology without an ontology is blind. Only if the two go hand in hand can we avoid a discipline in which
			the deaf and the blind lead in different directions, both of which end in <em>cul de sacs</em>. (<a
				href="#arc95">Archer</a>, 1995, p.28)</p>
	</blockquote>
	<p>As <a href="#arc95">Arche</a>r (1995: 17) points out <em>&quot;What social reality is held to <strong>be</strong>
			also <strong>is</strong> that which we seek to explain&quot;</em>.</p>
	<h2 id="the-transitiveintransitive-divide">The transitive/intransitive divide</h2>
	<p>From within an interpretivist perspective <a href="#tra01">Trauth</a> (2001) discusses the important factors in
		selecting research approaches considers the important factors that affect researcher decisions regarding the
		research approach to adopt. She argues that the following factors are important in the decision to use
		qualitative approaches and in the actual choice of qualitative methods:</p>
	<ul>
		<li>The research problem</li>
		<li>The researcher's theoretical lens</li>
		<li>The degree of uncertainty surrounding the phenomena</li>
		<li>The researcher's skills</li>
		<li>Academic politics</li>
	</ul>
	<p>It is useful to present a critical realist perspective on her argument. She suggests that the most important
		factor may well be the research problem: <em>&quot;what one wants to learn suggests how one should go about
			it&quot;</em> (<a href="#tra01">Trauth</a>, 2001: 4). A particular example is given of her own study of
		Ireland's information economy where she wished to uncover the <em>&quot;story behind the statistics&quot;</em>.
		This purpose led her to select an interpretative approach as the most appropriate means of examining such a
		topic. The researcher's theoretical lens is also suggested as playing an important role in the choice of methods
		because the underlying belief system of the researcher largely defines the choice of method. In fact, in many
		cases, she suggests that the choice of lens is often driven by a desire to avoid the shortcomings of positivism.
		Another important factor in the decision to adopt qualitative approaches may be the high levels of uncertainty
		in the problem situation, which could lead to difficulties in positivistic measurement. Researchers' skills can
		also define what methods are to be used, as can academic politics.</p>
	<p>For the critical realist, most of the factors suggested by Trauth can be seen to be primarily concerned with the
		transitive epistemological aspects of the research process rather than the intransitive ontological aspects, as
		Table 1 shows:</p>
	<table>
		<caption>
			<strong>Table 1: Epistemological and ontological concerns</strong>
		</caption>
		<tbody>
			<tr>
				<th>Primarily epistemological concerns</th>
				<th>Primarily ontological concerns</th>
			</tr>
			<tr>
				<td>The researcher's theoretical lens</td>
				<td>The research problem</td>
			</tr>
			<tr>
				<td>The researcher's skills</td>
				<td>The degree of uncertainty surrounding the phenomena</td>
			</tr>
			<tr>
				<td>Academic politics</td>
				<td> </td>
			</tr>
		</tbody>
	</table>
	<p>The interpretativist does not make this distinction, as Orlikowski and Baroudi suggest:</p>
	<blockquote>
		<p>Interpretivism asserts that reality, as well as our knowledge thereof, are social products and hence
			incapable of being understood independent of the social actors (including the researchers) that construct
			and make sense of that reality. (<a href="#orl91">Orlikowski &amp; Baroudi</a>, 1991: 13)</p>
	</blockquote>
	<p>Critical realism would suggest that this argument reflects the epistemic fallacy in that it confuses the
		transitive and intransitive dimensions. For the realist, reality can never be a social product since it
		pre-exists the transitive, changing social analysis of it. Our perceptions of reality change continually but the
		underlying structures and mechanisms constituting that reality are &quot;relatively enduring&quot;. The aim of
		realist research is to develop a better understanding of these enduring structures and mechanisms. Ontological
		factors, therefore, must be the primary factor in defining research approaches; this requirement necessarily
		forcing a strong philosophical commitment. For the realist, academic politics and traditional researcher skills
		and background should not define research approaches; the nature of what is to be investigated is the primary
		concern.</p>
	<h2 id="critiquing-an-interpretativist-framework">Critiquing an interpretativist framework</h2>
	<p><a href="#gal92">Galliers</a> (1992) proposes a taxonomy for providing guidance in selecting information system
		research approaches (<a href="p112tab2.gif" target="_blank">see Table 2</a>). The framework suggests that by selecting the
		object of one's research (society, group or individual) or the purpose of the research (theory testing, theory
		building or theory extension) one can get a feeling for which research approach would be most suitable. Although
		not stated it is clear that this framework is grounded in an interpretative perspective - an analysis from a
		critical realist perspective provides some interesting contrasts.</p>
	<p>The first observation critical realism would make is that one cannot concentrate solely on a single level
		investigation of the society, group or individual: critical realism argues for a relational perspective, seeing
		society as <em>&quot;an ensemble of structures, practices and conventions that individuals reproduce or
			transform&quot;</em> (<a href="#bha91">Bhaskar</a>, 1991: 76). Society is a <em>&quot;skilled accomplishment
			of active agents&quot;</em> (p. 4) and the flat ontology suggested by the concentration on a single aspect
		of social situations (society, group or individual) would restrict explanatory power (<a href="#ree97">Reed</a>,
		1997). Critical realism would suggest (as in <a href="#lay93">Layder</a>, 1993) that in order to practically
		investigate a social situation one may need to examine each level in turn but the interactions between each
		structure identified at the different levels cannot be ignored.</p>
	<p>Another issue with Galliers's two-dimensional framework is that the &quot;approach&quot; is compared against the
		object and the purpose of the study: this must neglect, therefore, the important relationship between the
		purpose and the object. As <a href="#say92">Sayer</a> (1992: 4) argues, the choice of method or approach must be
		<em>&quot;appropriate to the nature of the object under study and the purpose and expectation of the
			study&quot;</em>. Galliers's model does not reflect this aim (and being two-dimensional obviously cannot do
		so without a deal of difficulty). As detailed in Figure 1, for the realist the purpose of the study (intensive,
		abstract or generalisable research) helps to define the particular underlying aspects of reality upon which the
		realist researcher should concentrate. For example, abstract, theoretical research does not specifically deal
		with events apart from as possible outcomes, whereas if generalisation is the target then the researcher would
		need consider how events may be related across different settings, seeking regularities and common properties at
		this level. Intensive research for the realist involves the consideration of particular contexts and
		combinations of isolated structures, mechanisms and actual events. The purpose of the study largely defines at
		what level of reality the researcher should operate.</p>
	<p>The primary focus for the realist is first, to identify the underlying objects of research, which then helps to
		define the approaches that should be adopted; the approach is secondary. For the realist it is implied that,
		once we know what we are looking for, any number of approaches can be adopted and applied in different, novel
		ways; the target being to unearth the real structures and mechanisms within a particular research situation.
		Galliers's model is more interpretative in focus, concentrating more on the interpretative power of differing
		approaches rather than on the underlying purpose and object.</p>
	<p>Galliers's framework implies that the methods of the sciences <strong>can</strong> be extended to social
		investigation in that, for example, it allows for the use of laboratory experiment and theorem proof to
		investigate social situations. This is in line with the view of the critical realist who believes that the
		methods of the sciences can be (carefully/critically) extended to the study of the natural or social sciences.
		The realist, however, would suggest that the inability to create &quot;closure&quot; in the social arena
		fundamentally affects the conclusions reached and the applicability of the different scientific approaches. For
		the realist, the inability to create experimental closure requires that the primary aim of research must be
		explanatory (the equivalent of Galliers's theory building?) rather than prediction (Galliers's theory
		extension?) or falsification (Galliers's theory testing). Predictive use of theory and theory testing is limited
		within the critical realist approach because of the inability to create closure in social situations. This
		necessitates that any derived theory from social investigation can only indicate &quot;tendencies&quot; rather
		than provide clear prediction. Similarly, falsification on the basis of social observation is never fully
		possible.</p>
	<p>Consistency is a major aim of realist research, the tripartite connections between ontology, methodology and
		practical theory being most important. As Archer argues:</p>
	<blockquote>
		<p>...the social ontology endorsed does play a powerful regulatory role vis-à-vis the explanatory methodology
			for the basic reason that it conceptualises social reality in certain terms. Thus identifying what there is
			to be explained and also ruling out explanations in terms of entities or properties which are deemed
			non-existent. Conversely, regulation is mutual, for what is held to exist cannot remain immune from what is
			really, actually or factually found to be the case. Such consistency is a general requirement and it usually
			requires two-way adjustment. (<a href="#arc95">Archer</a>, 1995: 17)</p>
	</blockquote>
	<p>This two-way adjustment requires a contingent ontology in order to work, since if a particular non-consistent
		theory works very well it may well raise ontological and philosophical questions that would need to be
		addressed.</p>
	<figure>
		<p><img src="p112fig1.gif" alt="Figure 1"></p>
		<figcaption>
			<p><strong>Figure 1: Structures, mechanisms and events (based on <a href="#say92">Sayer</a>, 1992:
					11)</strong>
		</figcaption>
		</p>
	</figure>
	<h2 id="conclusion">Conclusion</h2>
	<p><a href="#min00">Mingers</a> (2000) suggests that critical realism can be useful as the underpinning philosophy
		for operations research and management science and systems. This paper suggests that critical realism may also
		provide a useful grounding for information systems research in general by elevating the importance of
		philosophical issues and thus allowing for a more consistent approach to research. Its recognition of a
		transitive and intransitive dimension to reality provides a useful basis for bridging the dualism between
		subjective and objective views of reality: &quot;real objects are subject to value-laden observation&quot;. As
		detailed above, critical realism can provide useful guidance in the selection of methodological approaches and
		is useful in providing consistency in research approach.</p>
	<h2 id="acknowledgements">Acknowledgements</h2>
	<p>My thanks to the anonymous referees whose comments helped considerably in the preparation of this paper for
		publication.</p>
	<h2 id="references">References</h2>
	<ul>
		<li>
			<p><a id="arc95"></a>Archer, M. (1995). <em>Realist social theory: the morphogenetic approach</em>.
				Cambridge: Cambridge University Press.</p>
		</li>
		<li>
			<p><a id="ban89"></a>Banville, C. and Landry, M., (1989). &quot;Can the field of MIS be disciplined?&quot;
				<em>Communications of the ACM</em>. <strong>32</strong>(1), 48-60</p>
		</li>
		<li>
			<p><a id="bha78"></a>Bhaskar, R. (1978) <em>A realist theory of science.</em> Hassocks: Harvester Press.</p>
		</li>
		<li>
			<p><a id="bha79"></a>Bhaskar, R. (1979) <em>The possibility of naturalism.</em> Brighton: Harvester Press.
			</p>
		</li>
		<li>
			<p><a id="bha86"></a>Bhaskar, R. (1986) <em>Scientific realism and human emancipation.</em> London: Verso .
			</p>
		</li>
		<li>
			<p><a id="bha89"></a>Bhaskar, R. (1989).<em>Reclaiming reality: a critical introduction to contemporary
					philosophy</em>. London: Verso.</p>
		</li>
		<li>
			<p><a id="bha91"></a>Bhaskar, R. (1991). <em>Philosophy and the idea of freedom.</em> Oxford: Blackwell.</p>
		</li>
		<li>
			<p><a id="col94"></a>Collier, A. (1994). <em>Critical realism: an introduction to the philosophy of Roy
					Bhaskar</em>. London: Verso.</p>
		</li>
		<li>
			<p><a id="cra92"></a>Craib, I. (1992) <em>Modern social theory: from Parsons to Habermas</em>. Hemel
				Hempstead: Harvester Wheatsheaf</p>
		</li>
		<li>
			<p><a id="gal92"></a>Galliers, R.D. (1992). &quot;Choosing information systems research approaches&quot;, in
				R.D. Galliers, ed., <em>Information systems research: issues, methods, and practical guidelines.</em> .
				pp.144-162. Oxford: Blackwell Scientific Publications.</p>
		</li>
		<li>
			<p><a id="gar97"></a>Garcia, L. &amp; Quek, F. (1997). &quot;Qualitative research in information systems:
				time to be subjective?&quot; in A.S. Lee, J. Liebenau and J.I. DeGross, eds. <em>Information systems and
					qualitative research</em>. pp.542-568. London: Chapman and Hall.</p>
		</li>
		<li>
			<p><a id="gra71"></a>Gramsci, A. (1971). <em>Prison notebooks</em>. London: Lawrence &amp; Wishart.</p>
		</li>
		<li>
			<p><a id="hir95"></a>Hirschheim, R., Klein H.K. and Lyytinen, K., (1995). <em>Information systems
					development and data modeling: conceptual and philosophical foundations</em>. Cambridge: Cambridge
				University Press.</p>
		</li>
		<li>
			<p><a id="hir96"></a>Hirschheim, R., Klein H.K. and Lyytinen, K., (1996). &quot;Exploring the intellectual
				foundations of information systems&quot;, <em>Accounting , Management and Information Technologies</em>.
				<strong>6</strong>(1/2), 1-64</p>
		</li>
		<li>
			<p><a id="iiv91"></a>Iivari, J., (1991). &quot;A paradigmatic analysis of contemporary schools of IS
				development&quot;, <em>European Journal of Information Systems</em>. <strong>1</strong>, (4), 249-272
			</p>
		</li>
		<li>
			<p><a id="iiv96"></a>Iivari, J. and Hirschheim, R., (1996). &quot;Analyzing information systems development:
				a comparison and analysis of eight IS development approaches&quot;, <em>Information Systems</em>.
				<strong>21</strong>(7), 551-575</p>
		</li>
		<li>
			<p><a id="irw97"></a>Irwin, L. (1997). <em><a
						href="http://www.raggedclaws.com/criticalrealism/glossary/index.html">The WSCR
						Glossary</a></em>. Available at http://www.raggedclaws.com/criticalrealism/glossary/index.html,
				[Accessed 13th September, 2000]</p>
		</li>
		<li>
			<p><a id="jon00"></a>Jones, M. (2000). &quot;The moving finger: the use of social theory in WG 8.2
				Conference Papers, 1975-1999&quot;, in: Richard Baskerville, Jan Stage, &amp; Janice I. DeGross.,
				<em>eds</em>. <em>Organizational and social perspectives on information technology</em>. Boston: Kluwer
				Academic Publishers.</p>
		</li>
		<li>
			<p><a id="kuh70"></a>Kuhn, T. (1970) <em>The structure of scientific revolutions</em>. 2nd ed. Chicago:
				University of Chicago Press.</p>
		</li>
		<li>
			<p><a id="lay93"></a>Layder, D. (1993). <em>New strategies in social research: an introduction and
					guide</em>. Cambridge: Polity Press.</p>
		</li>
		<li>
			<p><a id="loc94"></a>Locke, J. (1894). <em>An essay concerning human understanding.</em> A.c. Fraser (ed.):
				Vol. 1, Clarendon, Oxford.</p>
		</li>
		<li>
			<p><a id="mil94"></a>Miles, M. and Huberman, M. (1994). <em>Qualitative data analysis</em>. Thousand Oaks,
				CA: Sage.</p>
		</li>
		<li>
			<p><a id="min00"></a>Mingers, J. (2000). The contribution of critical realism as an underpinning philosophy
				for OR/MS and systems, <em>Journal of the Operational Research Society</em>. <strong>51</strong>(11),
				1256-1270.</p>
		</li>
		<li>
			<p><a id="min97"></a>Mingers, J. and Stowell, F., <em>eds</em>. (1997). <em>Information systems: an emerging
					discipline?</em>. London: McGraw-Hill.</p>
		</li>
		<li>
			<p><a id="mum"></a>Mumford, E., Hirschheim, R., Fitzgerald, G. and Wood-Harper, A.T., eds. (1985).
				<em>Research methods in information systems</em>. Amsterdam: North-Holland</p>
		</li>
		<li>
			<p><a id="nis91"></a>Nissen, H.-E., Klein, H.K. and Hirschheim, R. (eds.). (1991). <em>Information systems
					research: contemporary approaches &amp; emergent traditions</em>. Amsterdam: North-Holland.</p>
		</li>
		<li>
			<p><a id="nor99"></a>Norris, C. (1999). Bhaskar Interview, <em>The Philosophers' Magazine.</em> No. 8,
				Autumn, 34.</p>
		</li>
		<li>
			<p><a id="orl91"></a>Orlikowski W.J. and Baroudi, J.J., (1991). &quot;Studying information technology in
				organizations: research approaches and assumptions&quot;, <em>Information Systems Research</em>.
				<strong>2</strong>(1), 1-28</p>
		</li>
		<li>
			<p><a id="orm97"></a>Ormerod, R.J. (1997). The design of organisational intervention: choosing the approach.
				<em>Omega</em>. <strong>25</strong>(4), 415-435.</p>
		</li>
		<li>
			<p><a id="ree97"></a>Reed, M.I. (1997). In praise of duality and dualism: rethinking agency and structure in
				organisational analysis, <em>Organisation Studies</em>. <strong>18</strong>(1), 21-42.</p>
		</li>
		<li>
			<p><a id="row95"></a>Rowland, G. (1995). Archetypes of systems design, <em>Systems Practice</em>.
				<strong>8</strong>(3), 277-289.</p>
		</li>
		<li>
			<p><a id="say92"></a>Sayer, R.A. (1992). <em>Method in social science: a realist approach</em>. London:
				Routledge.</p>
		</li>
		<li>
			<p><a id="sea95"></a>Searle, J.R. (1995). <em>The construction of social reality</em>. New York, NY: Free
				Press.</p>
		</li>
		<li>
			<p><a id="tra01"></a>Trauth, E. M., <em>ed.</em> (2001). <em>Qualitative research in information systems:
					issues and trends.</em> Hershey, PA: Idea Group Publishing.</p>
		</li>
		<li>
			<p><a id="tso92"></a>Tsoukas, H. (1992) Panoptic reason and the search for totality: a critical assessment
				of the critical systems perspective, <em>Human Relations</em>. <strong>45</strong>(7), 637-657</p>
		</li>
		<li>
			<p><a id="wal93"></a>Walsham, G., (1993) <em>Interpreting information systems in organisations</em>.
				Chichester: Wiley.</p>
		</li>
		<li>
			<p><a id="wal95"></a>Walsham, G., (1995). interpretative case studies in IS research: nature and method,
				<em>European Journal of Information Systems</em>, <strong>4</strong>, 74-81.</p>
		</li>
		<li>
			<p><a id="win97"></a>Winder,R.L., Probert, S.K., &amp; Beeson, I.A., <em>eds</em>. 1997, <em>Philosophical
					aspects of information systems</em>. London: Taylor &amp; Francis.</p>
		</li>
	</ul>

</body>

</html>