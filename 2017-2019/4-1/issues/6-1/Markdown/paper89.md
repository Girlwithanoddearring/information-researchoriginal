#### Information Research, Vol. 6 No. 1, October 2000

# Designing Internet research assignments: building a framework for instructor collaboration

<table>

<tbody>

<tr>

<td>

[David Ward](mailto:dh-ward@uiuc.edu)  
Undergraduate Library</td>

<td>

[Sarah Reisinger](mailto:sreising@uiuc.edu)  
Reference Library</td>

</tr>

<tr>

<td colspan="2">University of Illinois  
Urbana, IL, 61801</td>

</tr>

</tbody>

</table>

#### **Abstract**

> Internet knowledge is increasing steadily among instructors in the academic world. As courses incorporate more instructional technology, traditional undergraduate research assignments are adapting to reflect the changing world of information and information access. New library assignments reflect this shift as well, with term papers and research projects asking students to use Web sites as an information resource, in addition to the standard literature of periodicals and monographs. But the many pitfalls the library profession has learned in its own metamorphosis during the past decade are often repeated in these newer course assignments.  
> The authors in this paper present a framework for librarians to interact with instructors to incorporate Internet resources into traditional term paper and research assignments. They suggest a framework for creating sample assignments librarians can take to campus instructional units, to show the teaching community at large what the library profession has learned from first-hand experience.

## Introduction

Internet information resources are fast seeping into the common consciousness of academia, taking their place alongside traditional academic resources in core research assignments. Instructors at all course levels incorporate the Internet through such techniques as posting course syllabi and readings on the Web, using interactive course design packages such as WebCT and FirstClass, and using communication tools like chat software and Web bulletin boards. The eagerness with which many instructors approach integrating Web research tools and technology into their courses provides a significant opportunity for collaboration with librarians. While many instructors are new to Internet searching and evaluation techniques, the library profession has already devoted significant time in investigating the pitfalls of Internet searching, and the nature of information found on the Web.

Librarians' expertise in the world of information can only go so far, however: "it is the writing faculty who actually provide the incentive (assignments) for students to use the skills of gathering, evaluating, and integrating information into their writing." ([Gauss & King, 1998](#ref1)) When students have a definite task at hand, the relevance of library instruction becomes readily apparent. Often, however, librarians only find out about assignments once students have started to arrive at the Reference desk with questions. The goal of this paper is to provide a framework for librarians to interact with instructors, and prepare for three common types of research assignments which necessitate the incorporation of Internet research and evaluation skills.

We will first summarize some of the familiar pitfalls of Internet searching, and show how they relate to our three types of assignments. Next, we will describe some of the key points to communicate to both instructors and students about each type of assignment. Finally, we will provide sample assignments, as tools for building collaborative efforts between course instructors and the library. The goal of these assignments is to show students and instructors alike some basic tips for effectively exploring the Internet and evaluating Web sites.

## The Big Picture

The use of Internet sites as sources for research is just a small part of the greater information literacy model. Often, we find that students and even faculty are facing this whole new ideology of information without the appropriate tools to discern facts from fictions. Without publishers and editors as filters, Web information can be served up quickly and often with an apparently validating beauty, with tabloids appearing on equal ground with encyclopedias. "Don't believe everything you read" has never rung truer. Though we will not attempt to tackle solutions for the wide-spread need for information literacy, it is hoped that this framework for creating guides and assignments for college-level library and term paper assignments will serve as an introduction for students and faculty alike to some of these concepts. The explanations and examples included in this paper can be used as either pieces or modules of a broader library instruction programme, or as stand-alone assignments designed around particular classes or curricula.

## Methodology

Library literature includes much on the problems of using the Internet for academic research ([Cornell, 1999](#ref1); [Janes, 1999](#ref1)) and the fallibility of current search engine technology ([Notess, 2000](#ref1)), as well as the importance of building strong librarian/instructor relationships ([Kotter, 1999](#ref1); [Stebelman, 1999](#ref1)). Based on the issues addressed in the literature, as well as our own experiences with real library Internet-based assignments, our goal is to examine these practical problems, and suggest solutions by way of actual hands-on assignments that librarians and instructors can either use immediately, or easily modify to suit their own particular needs.

Our approach to designing these assignments centered on defining appropriate goals - for both librarians, instructors, and students - for teaching Internet research skills. For librarians, the goals are three-fold: increase patron knowledge of how to retrieve and interpret information from Web sites, build a bridge to the teaching faculty to make them aware of how the library can help raise the level of their classes' critical thinking skills, and encourage participation on their part in developing Information Literacy.

For instructors the goal we had in mind was to devise assignments that covered basic Internet research skills, yet provided flexibility in terms of the classroom assignments they might need to teach these skills. Lastly, for students the assignments needed to be easy to understand, and immediately relevant to term papers or other projects that their instructors were regularly assigning.

## Common Internet Pitfalls

The basic concerns with using "The Internet" as a research tool are well known, and fall roughly into two categories: defects and inconsistencies in the searching tools, and the complexity of interpreting and evaluating results. Well-constructed assignments will also address the universal concern that "students and teachers need to recognize the current limitations of information found on the Internet." ([Schrock, 1999](#ref1)) The first step towards building good Internet related assignments is to recognize relevant pitfalls in searching and evaluation.

## Pitfalls of Searching

*   _Students do not know where to start._ The first step to finding a web page is deciding how to search the web itself. Students often do not know the difference between a directory (like Yahoo), a search engine (like Infoseek), or a meta-search engine (like Dogpile).

*   _Search engines do not search the whole web._ Once students have found a search tool they like, they often do not realize that it only covers a small portion of the whole Web, which may be different from other search tools or directories. Many studies have been done on the percentage of the web covered by individual search engines and group search engines, and most show that less than 50% of the web is covered by all of these tools put together. (e.g., [Feldman, 1999](#ref1))

*   _Users do not know how (or care how) to use an individual search engine's full searching syntax._ This problem is also common to OPAC and article database searching patterns, where students either do not use Boolean operators, or do not understand their proper use. A study of over 18,000 Excite users performing over 50,000 searches showed that Boolean Operators were used in only 8% of search statements, and +/- operators in only 7%. ([Jansen _et al._, 1998](#ref1))

*   _Search engines return irrelevant and multitudinous results._ Especially for multi-keyword searches, the different ways search tools rank results often means that the most scholarly or assignment-appropriate hits will not be displayed first. Jansen's study also showed that 80% of users only looked at the first two pages of results. ([Jansen _et al._, 1998](#ref1))

*   _Spam._ Many tricky or innovative (depending on your perspective) Webmasters have discovered ways to spam search engines, loading their pages with keywords in META tags or elsewhere so that their pages are returned for searches totally irrelevant to the actual content of the site.



## Pitfalls of Evaluation

*   _Authority_. Many pages do not have an author listed, or if one is listed, there are no credentials or affiliations given to indicate whether the person is an expert, a journalist, or just an ordinary person with a casual interest in the Enlightenment.
*   _Depth_. Internet sites often only provide an overview of a subject, without the expanse of research found in academic journal articles or books.
*   _Bias_. Many sites are often constructed with a specific agenda, and may not attempt to present an objective assessment of their subject.
*   _Currency_. Students often assume that because information is on the computer, it is the most current information available. For research topics where currency is required, it is important to be aware of when a site was mounted, and whether it has been updated since.
*   _Sources_. While some sites list references and have citations just like scholarly books and articles, most provide information without mentioning its origin.
*   _Stability_. Web sites seem to disappear almost as fast as they appear.
*   _Look and Feel_. Grammar and spelling errors, little ease of navigation, broken links, etc. all contribute to the probable lack of dependability of the site.
*   _Audience_. Often, Web pages will be targeted at a specific constituency that might affect the writer's bias, the level of depth with which the topic is pursued, etc. Sites utilized as sources for academic papers should be written at the appropriate level of scholarship required of all other sources for the class and assignments.

## Defining the Types of Assignments

By taking stock of Internet-related assignments that we regularly encounter in our library and the ones we have located from other institutions, we have grouped the assignments into three types. The Type I assignment involves basic Web searching skills. The Type II usually handles retrieval of information from pre-selected Internet sites. The Type III assignment is the one that we are all moving towards and hoping for: a complete integration of electronic information (including Web sources) into the standard writing assignment.

These three often reflect the various levels of instructor Internet knowledge, and/or a difference in educational goals. Also, because student levels of technology and information literacy vary between schools, libraries may see more of the introductory Type I and II, than Type III assignments. In the past year, we have seen significant increase in the Net-awareness of our incoming classes, usually reflecting their increased access to technology in the high school setting. Because of this, we expect to see many more of the integrated Type III assignments in the near future.

#### Type I - Searching for Sites

[Type I assignments](#type-i) are usually designed to introduce students to searching on the Internet. They often ask students to use the Web, find a site on a topic, and report back on how they found it. These assignments focus on web-searching skills, and tend toward an attitude of welcoming the Web as a free source of information - perhaps advocating the idea of flea-market shopping: "It's all there, you just have to find it!" These assignments may include introduction to a few search engines and directories - though often there is not much distinction made between the two. With inexperienced classes, it is important to provide specific guidelines or worksheets to illustrate good search strategies, rather than just turning students loose and hoping for the best. We shall discuss how to encourage instructors along these lines in the next section.

#### Type II - Searching Sites for Information

For [Type II assignments](#type-ii), faculty will often prepare a course-related list of Web sites for their students to use to answer questions, or they may instruct them to search a specific one for information - i.e., "Visit the www.irs.gov and find information on the new education tax credits for families."

This second assignment goes a little further in depth. The instructor is aware of some of the difficulties of finding good information on the Internet, and limits the students to certain sites, often resulting in the equivalent of a "library scavenger hunt" with these pages. Though this type of assignment can be good for introducing students to the concepts of site evaluation and what academic Web sites should look like, often it is little more than a lesson in navigating a page and sifting through information for answers, much as one would do with a traditional journal article handed out in class.

#### Type III - Researching and Incorporating Internet Information

[This assignment](#type-iii) addresses what Gauss and King call "all three stages of true information literacy: gathering, evaluating, and integrating information." ([Gauss & King, 1998](#ref1)) Students are asked to first search for sources for their topic, often as part of a traditional term paper assignment, and then (often) evaluate the sources that they find. In the list of allowed sources, electronic information and Web sites are permitted. This instructor realizes the value of electronic information, perhaps even uses e-journals in their own publications, and so desires to allow and encourage his or her students to do the same. In the best cases, this is a good assignment, and something that we as forward-looking librarians would want to encourage. However, this is perhaps the most difficult one for the students, with the pitfalls of both searching and evaluation to be concerned about.

## Developing Communication with Instructors

If an instructor comes to you with one of these assignment types, and some of the before mentioned pitfalls are evident, how best can you guide them to surer footing? Some of the answers regarding collaboration with instructors are the same regardless of whether we are working with an Internet related piece or another traditional library assignment. Here we suggest common types of questions to ask instructors, and possible solutions for improvement of their Internet-related assignments.

#### Type I Assignment Guides

For the individual giving an assignment like Type I, find out the state of their students knowledge of the technology. Do they have a purpose in encouraging a "free-grazing" type of approach to the Web? If they are just introducing the technology, assist them in preparing a hand-out to point out the basic differences between search engines and directories. You also might give them some statistics regarding the percentage of the Web that is covered by any given search engine (see [Search Engine Watch](http://www.searchenginewatch.com/)) You also can attempt to get them to include a short evaluation form in their assignment. (See [Examples of Type I assignments](#type-i).) This will encourage preliminary critical thinking in their students' relationship with these Internet materials.

Ask the instructor if s/he plans to introduce the students to the library's electronic resources that are not open to the general public - i.e., the article databases, or electronic journals. Do not cloud the assignment too much by expecting that the instructor will deal in-depth with either of these areas of evaluation or electronic resources. This is an entry-level assignment and one that many of our college freshmen would have no trouble with. However, the evaluation portion can get them thinking along relevant lines in advance of their need for electronic sources for a paper or project.

#### Type II Assignment Guides

For the second type of assignment, the instructor has done some independent research to prepare the material for their class. Again, make sure you know what the instructor wants to accomplish. Here you might suggest that he or she adds some questions to their "scavenger hunt" that are more than simple regurgitation of information.

If the sites they are limiting their students to for the assignment are well known government or education sites, suggest some other non-official sites to use for comparative exercises. This assignment is also very useful for comparing free and fee Internet resources, such as article databases to which libraries subscribe. Encourage the instructor to have his or her students locate a piece of information that looks questionable, or identify a discrepancy in information between two sites.

#### Type III Assignment Guides

The Type III assignment presents the most challenges. Here, we often find the instructor has either limited the students too stringently, or has given no guidance and assumed the ability and knowledge of their students includes all areas of site appropriateness and evaluation.

Take a look at the instructor's guidelines for the paper. Has he or she provided samples of acceptable sources? Are they limiting the students to on-line scholarly journals? Government and education sites? This is a fairly quick way to weed out disreputable sites, but it does not hit all of them, and it limits the students from using other sources that might be very appropriate. Ultimately, it is up to the instructor to decide what they will and will not accept for valid sources in a paper or project. Careful diplomacy may be needed at times to make a point, or to suggest a source that is out of the range of allowed information types.

For example, one assignment we have seen allows the students to use Internet sources for their papers as long as they have also been published in traditional print form. This kind of restriction avoids a basic analysis and evaluation of Internet resources. Although this is usually a safe strategy from the point of view of the dependability of sources, the instructor unknowingly robs students of a wealth of information that is _only_ published on the Web - electronic journal, education, government, or otherwise. Come to them with some examples of helpful sites in their subject area. If you are successful in getting them to allow original Internet sources, then give them a simple evaluation packet to pass on to their students, such as the [Term Paper Worksheet](#wsheet) included in our sample assignments. Encourage them to edit it as they see fit, emphasizing what they find useful.

For the term paper assignment with no limits regarding Internet resources, it is important to encourage the instructor to at least have a discussion with students about the different types of information that they can access on the Web. Ask the instructor what types of Internet sources they will accept for the paper. Your library might provide a suggested handout or two, or perhaps a Web site that you have prepared with examples of scholarly journals your school pays for, free journals on the Web and educational, government and non-profit organization sites. Also include some personal sites that may be of interest - but point out the possible lack of trustworthiness. If you have the time, personalizing it to the subject area of the class will be the most helpful. Depending on the number of classes that you handle per year, even preparing a separate set of examples for the sciences and humanities will be valuable.

Lastly, make sure that both the instructor and the students understand the difference in citation format for Internet resources. Many still cite electronic copies of traditionally printed journals as if they were citing the paper journal itself. Others do the same with on-line journals. Make it easy for them by giving them the sites for the [MLA](http://www.mla.org/style/sources.htm) and [APA](http://www.apa.org/journals/webref.html) specifications.

## Damage Control

Often librarians do not have the opportunity to catch a "bad" Internet assignment before a student walks in with it in the middle of the semester. This results in another special function of librarians - damage control experts! The best way to meet these challenges is by some careful preparation. Here are some simple steps to aid you in your task.

#### 1\. Be prepared.

In the same way that you will be prepared to discuss assignments with instructors before the semester begins, prepare for the surprise assignments. Create a general web search engine overview page for the Type I assignment. Have evaluation handouts printed out for assignment Types II and III. Mount a Web site for the Type III assignment suggesting ways to access scholarly material on the Web.

Additionally, prepare a more in-depth Internet Research Web-based tutorial to which you can refer any students. For example, this [University of Illinois site](http://www.library.uiuc.edu/rex/instruction/Internet/internetresearch.htm) includes basic a basic introduction, and then goes into more advanced material, suggesting search strategies and covering evaluation and citation in detail. As with anything on the subject of the Internet, the page can become dated rather quickly, so frequent updating is essential. You will know best how to meet the needs of your student population - being prepared for the one assignment type that you see the most will save a lot of time down the road

#### 2\. Recognize Assignment Types:

As with most assignments encountered at the Reference Desk, a proper line of questioning is the best starting point for analyzing assignments that may involve Internet resources. Perhaps an Internet related assignment will be very obvious - for Type I or II, this will most likely be the case. But paper assignments requiring Web sites can be more difficult to recognize. Often it will not be readily apparent whether the student will be allowed to use Internet resources in their work. Even if they are at the Reference desk to find a traditional source, be sure to ask to see a copy of their assignment. If the assignment utilizes or welcomes the Internet sources, check to see if the student has been given any evaluation or selection direction from their instructor. Once you decide if it is a Type I, II or III (or a mixture) you will know what sorts of handouts and assistance are most appropriate to give the patron.

#### 3\. Provide Patron Assistance:

Chances are, the patrons will be at your Reference desk looking for help on another part of the assignment. As you assist them with finding the pertinent resources, particularly with term papers, be prepared to offer assistance for the Internet portion of their research. Make sure to communicate the value of electronic resources that the library subscribes to, emphasizing that _only_ your school affiliates are permitted to use them. Show them your library's Web site evaluation sheet and discuss some of the pitfalls that we have mentioned in this article. Refer them to the helps that are appropriate for their type of assignment. After you have assisted them in finding good quality, reputable sources, encourage them to refer their fellow students to the Reference desk for help with the Internet resources. Even if you are unable to speak to the whole class, the assistance that you give the students that come into the library will go a long way. Remember: satisfied patrons are our best publicity!

#### 4\. Make Instructor Contact:

If it is early in the time-line of the assignment, attempt to contact the instructor. See if you can visit the class or suggest resources to them as a group. This response is the same as one you would use for any unwieldy library related assignment that shows up at your door. If it is late in the semester, find out who the instructor is and contact them prior to the following semester with handouts, web helps, and general suggestions for improving the resource gathering experience of their students.

#### 5\. Make Staff Contact:

As with any unexpected library assignment, communicate with other staff. Make sure that no one is left to redo the work that you have just done by letting others know what type of assignment you have encountered, and what the needs are for the students that will be coming in to your library. Let other librarians know what "damage control" you have already taken, so that when they meet up with the same assignment, they are prepared to do the same.

## Conclusion

The use of Internet resources in the academic environment is only going to grow, as developments like  [Internet-2]('http://www.internet2.edu/'), [Next Generation Internet]('http://www.ngi.gov/'), and higher speed access for both homes and libraries make information dissemination faster and easier (see [Johnson, 2000](#ref1); [McLoughlin, 1999](#ref1); [Preston, 1999](#ref1); [Sheehan 1999](#ref1)). Anticipation and preparation are the key elements for any collaborative efforts between librarians and course instructors. Hopefully these suggestions will enable you to prepare your own assignments (and addendum to instructors' assignments) with relative ease. With effective and relevant hands-on Internet assignments in hand, a library can have a package to present to departments and instructors to complement their own instruction missions.

<a id="ref1"></a>

## References

*   Connell, Tschera Harkness and Tipple, Jennifer E. (1999) "Testing the accuracy of information on the World Wide Web using the AltaVista search engine." _Reference and User Services Quarterly,_ _Summer,_ 38(4), 360-8\.
*   Feldman, Susan. (1999) "New Study of Web Search Engine Coverage Published." _Information Today_, _September_, 16(8), 29.
*   Gauss, Nancy Venditti and King, William E. (1998) "Integrating Information Literacy into Freshman Composition: Beginning a Long and Beautiful Relationship." _Colorado Libraries,_ _Winter,_ 24(4), 17-20.
*   Janes, Joseph and McClure, Charles R. (1999) "The Web as a reference tool: comparisons with traditional sources." _Public Libraries,_ _Jan/Feb,_ 38(1), 30-3+.
*   Jansen, Bernard J., Amanda Spink, Judy Bateman, and Tefko Saracevic. (1998) "Real Life Information Retrieval: a Study of User Queries on the Web." _SIGIR Forum_, 32(1), 5-17.
*   Johnson, Dave. (2000) "Broadband in Your Future: Home Networks Meet High-Speed Pipes." _Home Office Computing,_ _March_, 18(3), 43.
*   Kotter, Wade R. (1999) "Bridging the great divide: improving relations between librarians and classroom faculty." _The Journal of Academic Librarianship,_ _July,_ 25(4), 294-303\.
*   McLoughlin, Glenn J. (1999) "Next Generation Internet and Related Initiatives." _Journal of Academic Librarianship,_ _May,_ 25(3), 226-229.
*   Notess, Greg R. (2000) "The never-ending quest: search engine relevance." _Online,_ _May/June,_ 24(3), 35-40\.
*   Preston, Cecilia M. (1999) "Internet 2 and the Next Generation Internet: a Realistic Assessment." _Searcher,_ _January,_ 7(1), 66-70.
*   Schrock, Kathleen. (1999) "Producing Information Consumers: Critical Evaluation and Critical Thinking." _Book Report_, _January/February,_ 17(4), 47-48.
*   Sheehan, Mark. (1999) "FASTER, FASTER! Broadband Access to the Internet." _Online_, _July/August,_ 23(4), 18-20+.
*   Stebelman, Scott, Jack A. Siggins, and David J. Nutty. (1999) "Improving library relations with the faculty and university administrators: the role of the faculty outreach librarian." _College and Research Libraries_, _March,_ 60(2), 121-30\.

## Additional reading

*   Babione, Alexandra (2000) _[Evaluating a Web Site](http://www.siue.edu/WRITE/lib3.htm)_ Available at: http://www.siue.edu/WRITE/lib3.htm [Accessed 11 May 2000]
*   Courtouis, Martin P. and Berry, Michael W. (1999) "Results-Ranking in Web Search Engines." _Online, May/June,_ 23(3), 39-40+.
*   Harris, Robert. (1997) _[Evaluating Internet Research Sources](http://www.sccu.edu/faculty/R_Harris/evalu8it.htm)_ Available at: http://www.sccu.edu/faculty/R_Harris/evalu8it.htm [Accessed 11 May 2000]
*   Higgins, Carla and Cedar Face, Mary Jane. (1998) "Integrating Information Literacy Skills." _Reference Services Review, Fall/Winter,_ 26(3/4), 17-32.
*   Junion-Metz, Gail. (1998) "The Art of Evaluation." _School Library Journal, May,_ 44(5), 57.
*   Mosley, Pixey Anne. (1998) "Creating a Library Assignment Workshop for University Faculty" _Journal of Academic Librarianship, January ,_24(1), 33-41.
*   Notess, Greg R. (1996) "Rising Relevance in Search Engines." _Online, May/June,_ 23(3), 84-86.
*   Repman, Judi and Carlson, Randal D. (1999) "Surviving the Storm: Using Metasearch Engines Effectively." _Computers in Libraries, May,_ 19(5), 50-55.
*   Smalley, Topsey N. (1998) "Partnering with Faculty to Interweave Internet Instruction into college coursework (at Cabrillo College)." _Reference Services Review_, _Summer,_ 26(2), 19-27.
*   Steenbock Memorial Library, University of Wisconsin-Madison (1999) _[Creating Effective Library Assignments](http://www.library.wisc.edu/libraries/Steenbock/bipage/libassig.htm)_ Available at: http://www.library.wisc.edu/libraries/Steenbock/bipage/libassig.htm [Accessed 11 May 2000]
*   Wills, Deborah. (1997) "Internet Tutorials for Faculty; Meeting Academic Needs" _RQ, Spring,_ 36, 360-367\.
*   Young, Rosemary and Harmony, Stephen A. (1999) _Working with Faculty to Design Undergraduate Information Literacy Programs._ New York: Neal-Schuman.

# Appendix - Example Assignments

#### A Proviso

The operating principle in the following assignments is that introductions to the Internet environment need simple, clear instructions, not an in-depth "library speak" analysis of information skills. With this in mind, the assignments presented below use a "bare bones" approach, providing students with basic instruction for getting started with searching and evaluating Internet resources, without overwhelming them with the accumulated knowledge of the field of librarianship.

* * *

## <a id="type-i"></a>Type I Assignment (A)

**"Introduction to Internet Searching"**

**Goal** - To learn how to use various search tools to research a topic, and discern the differences in the results they return.

**Step 1 - Choose Your Topic**

Choose a current events related topic to search for. Possible topics include cloning, gun control, satanic cults, Serbia, alternative medicine, etc. Write your topic here: ______________________________________________________

**Step 2 - Searching**

Using what you wrote in Step 1 as your search terms, try your search in three different search tools - a directory (**_Yahoo_**), a search engine (**_Google_**), and a meta-search engine (**_Metacrawler_**). Find two sites that look appropriate to you from each tool and write down their URLs (the address written in the "Location" bar near the top of your browser) and a brief description.

**_Yahoo - http://www.yahoo.com_**

Site 1  _________________________________________

Site 2  _________________________________________

**_Google - http://www.google.com_**

Site 1  _________________________________________

Site 2  _________________________________________

**_Metacrawler- http://www.metacrawler.com_**

Site 1  _________________________________________

Site 2  _________________________________________

**Step 3 - Comparison**

Did you find the same site in more than one search tool?  
(List Here): _________________________________________


How many pages of results did you look through for each site? ___________

Which tool was the easiest to use?  
Why? _______________________________________________________________________ 


Which, in your opinion, gave the best results?  
Why?________________________________________________________________________ 


* * *

## Type I Assignment (B)

**"Difference in Searching Tools"**

**Goal:** To understand the differences in electronic research tools and the types of material they retrieve.

In order to best undertake your research in the future, it is important to understand what types of electronic resources you have access to and what they can do for you.

There are two basic types of search tools on the Web: the Search Engine and the Web Directory. There are also Electronic Databases that you have access to as a University student (in other words, the general public can get to the first two tools, but not this last one.) Some of these databases will link you to the full text of a journal article while others will only give you information about the author and title of the article.

Using the list of these three types of resources, choose one from each. As you browse the site, try to answer the following questions (hint: try some searches in each tool to get a feel for how they work, then visit their help or general information sections to find out behind the scenes information.):  
_(Instructor: insert three lists of the tools you prefer them to choose from)_

1\. What is one difference between Search Engines and a Web Directories?

2\. Is there a reason why I would choose to use one over the other?

3\. When might it be helpful for me to use an Electronic Database as a search tool?

4\. How could I get something that I had written into a Search Engine?

5\. A Web directory?

6\. An Electronic Database?

7\. What does this tell me about the reputability of the information in these tools?

8\. In your opinion, is it easy to find good information for a research paper using these tools? Which one would you use first?

**Extra Credit**

Find out from one of the librarians how much it costs the University a year to subscribe to the Electronic database that you used!

* * *

## <a id="type-ii"></a>Type II Assignment (A)

**"The Internet Assignment"**

**Goal:** To understand the basic principles for evaluating Web sites and determining their appropriateness for academic research, and their relation to traditional print resources.

**Step 1** - Choose a topic related to the history of France/Gaul in the time of the Roman Empire. Write your topic here: ___________________________________

**Step 2** - Research your site using the <u>Internet Public Library</u> and a full-text library database, **_SearchBank_** (linked off of the library's home page). Choose one site from the Internet Public Library and one article from SearchBank and answer the following questions about each of them:

1.  Who is the work's author? Where is the author listed? What credentials are given for the author?
2.  When was the work written?
3.  Are there references or citations in the work? Give examples, if possible.
4.  Is there a bias to the author's writing?
5.  Which work did you like better? Why?

* * *

## Type II Assignment (B)

### Page Comparisons

**Goal:** To compare assignment site with other sites on the Web.

Now that you have finished working on the assignment using the pages that your instructor prepared for you ahead of time, use a Search Engine to find more Web sites on your topic.

1\. Use a Search Engine or Directory and locate three Web sites that discuss your subject area.

List them here:__________________________________
\________________________________________________ 

2\. What kind of sites did you find? (i.e. .edu, .com, etc.)

3\. Do any of the sites address the same issues that your assignment sites did?

4\. Do they agree on the information?

5\. Make a case to your instructor for including or not including one of your new sites in their assignment next time around. Use the following list of considerations as appropriate.

### Web Site Evaluation Suggestions

**A. Author**- Who wrote the page? Can you tell? Is it easy to see if they are affiliated with a larger institution? Does this make him/her qualified to write on the subject? Is there contact information for the author (e-mail as well as regular mailing address)?  

**B. Publisher**- In the print world, the publisher's name can be an indication of the quality of the information provided. Most people would be more willing to believe information in a book published by a university press than information found in a supermarket tabloid. In the Web world, the web server is the equivalent to a publisher. The web server's domain can help in assessing the reliability of the information. Here are a few of them:  
_.edu_ an educational or research institution  
_.com_ a commercial enterprise, including companies, newspapers, commercial Internet Service Providers  
_.gov_ a governmental body  
_.mil_ a military body  
_.org_ a not-for-profit organization  

**C. Intended Audience** - Is it apparent who the page was created for? For students? For scientists? For consumers? How might this affect the level and/or reliability of the information?  

**D. Background Sources**- Does the author cite any other material? Do they give full references for it? Would you have any way of checking to see if the things that they cite are indeed real articles or books?  

**E. Currency** - When was this page put up on the Web? Has it been updated since? Is the information current enough for your needs on this topic?  

**F. Accuracy** - Are there any grammatical, spelling, or typographical errors? If there are, there may be problems with the dependability of the content of the page as well.

* * *

## <a id="type-iii"></a>Type III Assignment

### <a id="wsheet"></a>"Term Paper Worksheet"

Purpose - Use this worksheet to help search for and evaluate Internet sites for your term paper research assignment.

**Topic** - Write your topic here:


\________________________________________________________________
\________________________________________________________________


**Searching** - You can use subject links from your class homepage, as well as the search tools listed on the library's <u>Searching The Internet</u> page. Write down all of the different phrases or search terms you use here.

**Evaluation** - For each Web page that you decide to use for your term paper, write down the following information about it:

*   Title and URL (http:// etc.)
*   Author, and author's credentials
*   Date material was written
*   Brief description of site's content
*   A few sentence evaluation of the site's quality. Address the **depth** of coverage, any **bias** you can detect, and the **audience** the site was written for.

_Paper hint: For your own sake, due to the rapid turnover of information on the Internet, you may want to print out and keep a copy of the Web information that you cite in your paper. Even good information can disappear due to problems with servers or movement from one server to another._