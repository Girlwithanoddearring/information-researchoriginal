<header>

#### vol. 19 no. 1, March, 2014

</header>

<article>

# Open for business? An historical, comparative study of public access to information about two controversial coastal developments in North-east Scotland

#### [Graeme Baxter](#author)  
Department of Information Management, Aberdeen Business School, The Robert Gordon University,  
Garthdee Road, Aberdeen, AB10 7QE, UK

<section>

#### Abstract

> **Introduction.** This paper compares public access to information about two controversial coastal developments in North-east Scotland: the construction of a gas terminal by the British Gas Council and Total in the 1970s, and the current development of 'the world's greatest golf course' by the tycoon Donald Trump.  
> **Method.** Data has been collected from a range of sources, including: the records of local and national government, the developers, and environmental interest groups; academic and other literature; the press; and interviews and correspondence with key figures involved in the two developments.  
> **Analysis.** The content of these sources was analysed in order to identify what information was, and was not, made publicly available during the two projects, and to explore what impact this may have had on citizens' engagement in the planning and decision-making processes.  
> **Results.** The provision of information, and of opportunities for participation, has been more extensive in the case of the golf course, where freedom of information legislation has also played an important role. Regional newspaper coverage of the Trump course has been less balanced than that of the gas terminal.  
> **Conclusions.** While the Trump development has been responsible for more voluminous information, questions remain over its comprehensiveness and reliability.

## Introduction

This paper discusses an historical, comparative study, currently in progress, of public access to information relating to two controversial coastal building developments in North-east Scotland: the proposed construction of a North Sea gas reception terminal by the British Gas Council and the French exploration company, Total Oil Marine, at Crimond, Aberdeenshire, in the early 1970s; and the current development of the 'world's greatest golf course' by the American property tycoon, Donald Trump, at Menie, Aberdeenshire, around 30 miles south of Crimond. Figure 1 shows the location of the two developments.

<figure>

![Location of gas terminal and golf course developments in North-east Scotland](../p603fig1.jpg)

<figcaption>Figure 1: Location of gas terminal and golf course developments in North-east Scotland (© Crown Copyright/database right 2013\. An Ordnance Survey/EDINA supplied service)</figcaption>

</figure>

These two developments have much in common: both have had potential or actual impacts on environmentally sensitive sites; both were responsible for significant levels of public participation in the planning processes; both resulted in the creation of new groups with the primary aim of opposing the proposed developments; and each has also been affected by plans for other structures in their immediate vicinity. However, separated by over thirty years, they have taken place during periods when, theoretically at least, public access to information and the citizenry's influence on government decision making have been significantly different. The terminal application was made in the midst of the North Sea oil and gas exploration boom, at a time when there was little history of public participation in planning processes in Scotland. It was also a period when environmental impact procedures were not yet widely accepted; when government secrecy was still the norm; and, of course, before the appearance of the World Wide Web. In contrast, the Trump development has taken place at a time when public input into major planning decisions is now taken for granted; when European legislation has made environmental assessments a key element of many large development proposals; when freedom of information (freedom of information) law has supposedly led to more open government; and when planning documentation is readily accessible online.

With these points in mind, the main aim of the study has been to identify what information was, and was not, made publicly available during each of the two projects — by the developers, by government departments and agencies, and by environmental and business interest groups — and to explore what impact this may have had on the public's engagement in the planning and decision-making processes in each case. The study is also investigating the role of the press in the dissemination of information about the two developments. It is exploring, for example, the extent, accuracy and objectivity of the information presented, and the potential influence this might have had on public perception of the two developments. In this regard, the study is also examining what impact the emergence of online citizen journalism has had in the case of the Trump development.

This paper will firstly outline the data collection methodologies being employed in the study, before providing an account of the building of the gas terminal and the associated information issues. It will then provide an overview of the Trump development to date, comparing and contrasting the information provision with that occurring over thirty years earlier. Throughout, it will make some initial observations on what opportunities, and barriers, the public at large has faced in obtaining information about the two projects, and in making a meaningful contribution to planning procedures.

## Method

During the course of the research, data has been collected from a wide range of primary and secondary sources. These have included:

**Government records.** In the case of the gas terminal, primary sources were examined at the National Archives in Kew, the National Records of Scotland in Edinburgh, and Aberdeen City and Aberdeenshire Archives. These have included records of the Ministry of Defence, the Department of Trade and Industry, the Scottish Development Department, and Aberdeen County Council. Many of these documents have only recently become publicly available, because of the UK Government's 30-year access rule for historical records. For the Trump project, electronic versions of primary documentation have generally been available on the Websites of [Aberdeenshire Council](http://www.aberdeenshire.gov.uk), the [Scottish Parliament](http://www.scottish.parliament.uk) and the [Scottish Government](http://www.scotland.gov.uk). With both developments, however, the current author has also had to make freedom of information requests in order to access specific records.

**Developers' records.** Some Total and British Gas Council records from the 1970s are held, respectively, by the University of Aberdeen's Special Collections Centre and by the National Gas Archive in Warrington, England. Meanwhile, the [Trump International Golf Links, Scotland](http://www.trumpgolfscotland.com) Website has been a source of information relating to the golf resort.

**Interest group records.** Material concerning the gas terminal was obtained from the archives of various environmental groups, including the Royal Society for the Protection of Birds (RSPB) and the Scottish Ornithologists' Club, as well as from the personal files of surviving interest group members. With the Trump development, most of the groups opposing or supporting the resort have had some form of Web presence that has been used to disseminate information.

**Print press coverage.** Newspaper coverage of the gas terminal proposals has been examined for the period from November 1972 (just before the planning application was formally announced) to September 1974 (when construction was well under way). This includes two national, Scottish newspapers, the _Scotsman_ and the _Herald_; two regional papers, the _Evening Express_ and the _Press and Journal_; and two weekly local titles, the _Fraserburgh Herald_ and the _Buchan Observer_ (Buchan is the historic name for the area surrounding the town of Peterhead - see Figure 1). With the Trump development, the press coverage being analysed dates from January 2006 (again, just before the plans were announced officially). However, due to current question marks over the completion of the entire resort (of which more is discussed later), this element of the data collection is ongoing. The same regional and national newspapers are being examined here (plus the two nationals' Sunday equivalents, _Scotland on Sunday_ and the _Sunday Herald_, which did not exist in the 1970s); but the two local papers being analysed are the _Ellon Advertiser_ and the _Ellon Times_, to reflect the more southerly location of Trump's golf course. In addition, the content of the online citizen newspaper, _Aberdeen Voice_, is being examined.

**Broadcast press coverage.** For an indication of radio and television news coverage of the gas terminal development, the Programme as Broadcast records in the Glasgow archives of the British Broadcasting Corporation (BBC) were consulted. Unfortunately, no such records are extant for the country's main independent broadcaster, Scottish Television. For the golf course, the [BBC](http://www.bbc.co.uk/news) and [STV](http://news.stv.tv) news Websites have been used as approximate indicators of national broadcast news coverage.

**Other secondary sources.** Extensive searches have been conducted for academic and other literature on the two developments.

**Interviews and other personal correspondence.** As the gas terminal application was made over forty years ago, many of the key figures involved in the development are no longer with us, or have proved impossible to locate. However, the author has interviewed, or corresponded with, a small number of the individuals involved in opposing the original plans. There are also plans to interview some of the prominent individuals involved in both sides of the Trump resort saga.

## 'Gas versus geese': public information provision during the terminal development

Throughout November 1972, the _Fraserburgh Herald_ reported that the area was awash with rumours about some form of oil or gas development being planned near Crimond ("We don't know" [1972](#wed72)). Then, on 27 November, BBC Scotland broke the news that an application for planning permission for a gas reception and treatment plant at a disused airfield at Crimond was to go before Aberdeen County Council's planning committee on 1 December. This application had been submitted jointly by the British Gas Council (a public body) and Total Oil Marine, who, at this stage, were not particularly forthcoming with details of the plans. All that they would reveal was that the terminal would handle gas piped from the Frigg field, 200 miles away in the North Sea, that it would be built on a 500-acre site, would cost several hundred million pounds, and would result in fifty to sixty permanent jobs, although the pipelaying and construction stages would employ hundreds.

Over the next few days, the regional and national newspapers were full of speculation about the extent and impact of the terminal, and two main issues became immediately apparent. Firstly, the proposed site was adjacent to the Loch of Strathbeg, Britain's largest coastal dune lake, recently designated a Site of Special Scientific Interest because of its wildlife, geology and limnology. Strathbeg had long been recognised as an internationally important site for wintering wildfowl. Indeed, when the terminal plans were announced, the RSPB was in the process of trying to secure land around the loch to create a new nature reserve.

The second main concern was that the Ministry of Defence (hereafter, the Ministry) had already received clearance to build an important radio station on the same airfield site, with construction expected to start in 1973\. When plans for the station were first announced, in 1971, the RSPB and the Nature Conservancy (the UK Government's environmental watchdog at the time) claimed that the resultant network of aerials and cables would lead to '_an appalling holocaust_' amongst geese and ducks flying in mist or darkness to the loch ([Decision soon, 1971](#dec71)). However, the Ministry had since agreed to the '_fixing of lights on their radio masts and on other measures_' to lessen the danger of collisions ([Dunn, 1972a](#dun72a)).

At its meeting on 1 December, the Council's planning committee decided to defer its decision until 22 December, so that the proposals could be advertised in the press and to allow objectors to express an opinion. Reporters did observe, though, that the committee appeared generally in support of the scheme; with one member proclaiming, 'If I had a choice between a flock of ducks and this development, I know where my allegiances would lie' ([Decision before Christmas, 1972](#dec72)). A public notice subsequently appeared in the regional newspapers on 5 December, stating that a site plan could be inspected at council offices, and giving a period of 14 days for any interested parties to make representations. These efforts at public consultation caused immediate outrage amongst local environmentalists, who expressed concern both about the short time frame in which to respond and the lack of detail on the plan. One of them pointed out that:

> The plan consists simply of a map of the old airfield at Crimond, coloured red, a blue road leading into it from the west and a "corridor" one-third of a mile wide from the airfield through the loch itself and the coastal dunes leading to the sea labelled "possible pipeline corridor"... How can anyone be expected to make sensible representations…on the basis of such meagre information… How, indeed, can any responsible planning authority give planning permission in principle, on such data?' ([Dunnet, 1972](#dug72))

The Gas Council fuelled further anger by stating that 'for some time now' they had been holding meetings about the development with various environmental groups, including the RSPB ("Gas plant" [1972](#gas72)), a claim challenged by local conservationists ([Bourne, 1972](#bou72)). Indeed, the RSPB later revealed that it had not met with the Gas Council until 13 December, more than a week after the appearance of the public notice; and that it '_could hardly be called consultation, but generally a loose indication on both sides of what we are trying to do_' ([Bird society, 1973](#bir73)).

Such an unsystematic approach to consultation was far from unusual at that time. Burton and Johnson ([1976](#bur76)), reviewing public participation in planning in Scotland, observed that the existing legislation, the Town and Country Planning (Scotland) Act 1969, referred only to '_adequate publicity_' and entitlement to '_an opportunity of making representations_', and that, consequently, much confusion existed amongst planning authorities as to what exactly was expected of them. Participation in planning, they concluded, was still at an experimental stage.

Despite there being just a fourteen-day consultation period, the application received a '_record number_' of 128 objections, including a 2,766-signature petition ([Dunn, 1972b](#dun72b)). Remarkably, at the next planning committee meeting on 22 December, the developers were still not in a position to provide further details of their plans, and a decision was deferred again. This second deferral allowed some organised opposition to begin, and the next few weeks saw the creation of two groups with broadly similar aims: to prevent the terminal being built next to the Loch of Strathbeg, and to appeal for a more careful approach to industrial development across the North-east. The first of these was the North-east Environmental Liaison Group, consisting of academics from the botany, geography and zoology departments of Aberdeen University, and of representatives of various conservation groups, including the RSPB, the Scottish Ornithologists Club, and the Scottish Wildlife Trust. The second was the Buchan Action Group, chaired by a local Episcopalian minister, with a committee of people 'from all ranks of life' ("Buchan conservation" [1973](#buc73)). The individuals behind the Buchan Action Group also organised a public meeting, held in Crimond on 6 February 1973, at which Gas Council and Total representatives appeared to explain their choice of the airfield site.

The press were generally sympathetic to the arguments of the two groups and gave them significant column inches in which to present their case. As one _Evening Express_ journalist observed:

> 'It is not good enough that the people of Buchan have so little information about the gas terminal planned for Crimond…It is not good enough when "little man" action groups are called upon to prove big business wrong with little expertise and less money.' ([Ogilvie, 1973)](#ogi73)

The Environmental Liaison Group came to play a crucial role in disseminating information on the potential impact of building the terminal next to Strathbeg. Members had an article published in the journal _Nature_ ([Bourne, Gimingham, Morgan and Britton, 1973](#bou73)), thus ensuring that the case became known internationally. And the County Council, perhaps realising the lack of environmental expertise within its own ranks, invited the group to adopt a semi-official advisory role and take part in a series of meetings with the developers ([Dunnet, 1974](#dug74)).

While most of these dramas were being played out in public, much was also going on privately, in the corridors of power in central government. Correspondence ([Scottish Development Department 1973](#sdd73)) between the Department for Trade and Industry and the Scottish Development Department reveals that civil servants became aware of the forthcoming gas terminal application in late October 1972, and that they were already resigned to the fact that '_a major battle is going to brew up_' between two public bodies — the Ministry and the Gas Council — over the same piece of land. It was also believed that the Ministry, while having powers to pursue compulsory purchase orders for projects crucial to national security, would be reluctant to resort to these in peacetime, as '_politically it is almost impossible to use them_'.

Scottish Development Department officials were also critical of the lack of detail in the terminal plans submitted for public examination, describing these as being 'in the most outline of outline form'. They therefore arranged a private meeting in Edinburgh on 28 December 1972, where Gas Council and Total representatives were asked to provide further information about their proposals. At this meeting, the developers explained that the pipeline landfall had to be made near Fraserburgh for economic reasons, and that the coast opposite the Loch of Strathbeg provided a suitable, hazard-free approach line for the pipes. Despite the developers having already publicly proclaimed that the Crimond site was the one '_most suitable for our needs_' ([MacDonald, 1972](#mac72)), Total admitted that there was a four to five mile stretch of coastline further south that had not yet been surveyed by themselves or by any other oil and gas company. Total acknowledged that they would have to survey that area as a matter of urgency, should they need to convince any future public inquiry that they had explored all alternative landfalls.

The Gas Council and Total also complained about a lack of detailed information on the Ministry's radio station requirements, to establish the compatibility of the two projects. As one Scottish Development Department official wrote after the meeting:

> 'Clearly there is a need for improved communication and better understanding between the two…it seems essential that discussions start forthwith on the possibility of compatible use… Both requirements may be of national importance… Neither requirement will go away of its own accord and we must try to see whether both can be accommodated.' ([Scottish Development Department, 1973](#sdd73))

A further private meeting was therefore convened, in London on 23 January 1973, where Gas Council and Total representatives met with Ministry and other government officials, to discuss their respective requirements for the Crimond site. At this meeting, it was concluded that the two projects could not be immediately co-located, for safety reasons. It also became evident, however, that 'both parties still see serious difficulties in considering an alternative site and seem determined to persevere in their efforts to acquire this one' ([Scottish Development Department 1973](#sdd73)).

Anxious to avoid a protracted public fight over the land, Scottish Development Department officials privately aimed for a solution that would involve the Ministry having priority and the Gas Council and Total being told to look elsewhere. They therefore arranged '_a confidential talk with someone in the Nature Conservancy who knows the area and its ecology_' in the hope that they would advise that a more southerly landfall site for the pipelines would be preferable to the one proposed. The Nature Conservancy representative, they had been assured, would be instructed '_to keep his trap shut_' ([Scottish Development Department 1973](#sdd73)). These plans, however, were overtaken by other events. At a private meeting with County Council and Scottish Development Department officials on 21 February 1973, the Gas Council revealed that another site had been found for the terminal, a few miles away at St. Fergus. Confidential meetings to discuss these new proposals then took place with the Environmental Liaison Group on 6 March and 16 April. Towards the end of April 1973, speculation about St. Fergus grew in the regional press, and the news was confirmed publicly following a Council planning committee meeting on 27 April. As the committee's vice-convener put it:

> It looks as if we are going to get the best of both worlds. We are still going to get a gas terminal. The Ministry is going to get its aerial farm and the conservationists are going to keep their ducks. ([Big gas plant, 1973](#big73))

While the Environmental Liaison Group was, indeed, pleased that the threat to Strathbeg had been lifted, it was less impressed with the developers' refusal to concede victory to the conservationists. The Gas Council and Total insisted that the decision was driven solely by the Ministry's determination to proceed with the radio station. This, the Liaison Group believed, did '_much less than justice to the ecologists_' case' ([North-eastern environmental, 1973](#nor73)). In subsequent years, British Gas continued to maintain that the '_implacable stance_' of the Ministry was the sole reason for the abandonment of their Crimond plans and that '_solutions acceptable to all parties could have been implemented_' should the terminal have been built next to Strathbeg ([Dean, 1981](#dea81)). This claim was echoed by the Aberdeen County Council convener, Maitland Mackie, who also argued that the public was now having too great a say in the planning process, making local government '_more difficult_'. Should this sound undemocratic, he said, voters could remove him from office at the next election ([Huxley, 1974](#hux74)). Just a few months later, Mackie failed to win a seat in the new Grampian Regional Council; a defeat he later put down to perhaps being a '_bit too conceited_' ([Mackie, 1992, p. 189](#mac92)).

So, what was the eventual outcome in the gas terminal case? A new planning application for the St. Fergus site was submitted in May 1973, against which no objections were lodged. Planning permission in principle was granted in July 1973, subject to conditions; most significantly that the shoreline dunes be returned to their former state once the pipelines had been laid through them. Outline planning permission was granted in October 1973, with detailed plans approved in March 1974\. And while the Gas Council (by now the Gas Corporation) fell foul of the authorities for persistently allowing its contractors to bypass and ignore statutory planning procedures during the early days of its construction ([Wapshott, 1974](#wap74)), the terminal was eventually opened in May 1978, and is still operating today. Perhaps ironically, the terminal became something of a haven for birdlife: a flat-roofed building on the site housed Britain's first roof-nesting common terns; while the security fence surrounding the terminal prevented four-legged predators such as foxes entering the area, thus allowing ground-nesting birds to flourish ([Tomlinson, 1990](#tom90)).

The RSPB's new nature reserve at Strathbeg opened officially in June 1977\. And the Ministry opened its radio station in July 1978, although high frequency transmissions did not begin until March 1979, following two Health & Safety Executive investigations into gas ignition hazards at the St. Fergus terminal. Despite the conservationists' earlier concerns about wildfowl mortality at the radio station, this was found to be '_small, of the order of dozens_' and even less after the RSPB provided an alternative roosting site at the reserve ([Bourne, 2005](#bou05)). The gas terminal story, therefore, ended relatively satisfactorily for all concerned. The extent to which the same can be said about the Trump golf resort development will be explored in the next part of this paper.

## 'Birds or birdies?': public information provision during the Trump golf resort development

The golf course story began in a similar fashion to that of the gas terminal, when, in January 2006, the press became aware of Donald Trump planning a '_major new leisure development_' somewhere in Aberdeenshire ([Is Trump teeing, 2006](#ist06)). Then, in March 2006, Trump announced plans to build '_the greatest golf course anywhere in the world_' across an 800-acre stretch of dunes on the Menie Estate. The £300 million project — Trump International Golf Links, Scotland (hereafter, Trump International) — would include two championship courses, a five-star hotel, and a mixed residential development ([Kirk, 2006](#kir06)). This news was immediately welcomed by business and tourism groups, who believed that the course would bring enormous economic benefits to the area. In complete contrast, concerns were raised by Scottish Natural Heritage (the Scottish Government's environmental watchdog, and a successor of the Nature Conservancy) about the potential impact on the dynamic, shifting dune system on the Menie Estate, designated a Site of Special Scientific Interest. Crucially (as will be seen later), Trump also warned that he would abandon his plans if proposals for a nearby thirty-three-turbine offshore wind farm were to proceed, arguing that this would spoil the sea views from his resort ([Johnston and Smith, 2006](#joh06)).

In November 2006, Trump submitted his outline planning application to Aberdeenshire Council, revealing that it would now cost between £600 million and £1 billion, and would include the two courses, a clubhouse, a 450-room hotel, a golf academy and turf research centre, an accommodation block for 400 employees, almost 1,000 holiday homes, and 500 residential properties. The full application, including the now mandatory environmental impact assessment (EIA) and separate economic and transport assessments, was not submitted until March 2007\. The impact assessment acknowledged that there would be '_significant adverse changes_' to the Site of Special Scientific Interest, but promised various '_mitigation measures_' to '_maintain and enhance as much natural interest as possible_' ([Ironside Farrar, 2007a](#iro07a)). Meanwhile, the economic survey, produced by Deloitte MCS Limited ([2007](#del07)), estimated that the resort would create over 6,000 construction jobs and over 1,200 permanent jobs locally, and would be worth almost £50 million annually to the North-east economy. The full Deloitte report was never made publicly available, and Trump subsequently commissioned the University of Strathclyde (see [Dunlop 2008](#dun08)) to conduct another economic impact study, which resulted in some significantly different figures (e.g., between 1,443 and 2,165 Full Time Equivalent local jobs during construction; and between £22.3 million and £33.5 million Gross Value Added annually to the local economy). These estimates have been challenged in various quarters, not least in a 2011 documentary film, _You've Been Trumped_, where the Strathclyde figures were described as '_wildly optimistic_' (see [Ward, 2011](#war11)).

In complete contrast to the Crimond gas terminal application in the 1970s, most of the supporting documentation for the golf resort proposals was made readily available to the public; online, at council offices and public libraries, and at a series of public meetings held throughout the area. The full planning application by Trump International was subject to the by now standard 28-day public consultation period; but, owing to its 'complexities', conservation bodies including the RSPB, SNH, and the SWT were allowed more time in which to respond ([Urquhart 2007](#urq07)). The environmental concerns expressed in these organisations' representations resulted in a response document from Trump International, in July 2007, in which they reaffirmed their intention to build part of the course on the Site of Special Scientific Interest:

> 'The very special qualities which the SSSI possesses…include the scale of the dunes, proximity to the sea, and the vistas which are on offer… Without the ability to form the course in this location, there would be no basis for the resort, and it would not proceed.' ([Ironside Farrar 2007b: 10](#iro07b))

Aberdeenshire Council invited the key environmental groups to submit further responses to this document, and indeed continued to accept representations from the wider public until November 2007, when the Council was due to consider Trump's application. This effectively meant that the consultation period had been eight months long. By 21 November 2007, the Council had received 2,999 letters of representation, with 65% in support of the resort. A 766-name petition objecting to the application was also received ([Aberdeenshire Council. _Planning and Environmental Services_, 2007](#acp07)).

It was at this stage of the proceedings that the drama really began to unfold. In September 2007, Aberdeenshire Council's planners had recommended the approval of Trump's plans, arguing that '_this is an occasion where the social and economic benefits are of national importance and that these do override the adverse environmental impacts_' ([Aberdeenshire Council. _Development Management & Building Standards_. 2007: paragraph 6.55](#acd07)). Then, on 20 November 2007, the Council's Formartine area committee backed the plan. But just a few days later, on 29 November, the more powerful infrastructure services committee refused planning permission, on the chairman Martin Ford's casting vote, largely because of the adverse environmental impact and concerns about the inclusion of commercial housing on land not allocated for that purpose in the local development plan ([Aberdeenshire Council. _Infrastructure Services Committee_, 2007](#aci07)).

There was a predictable response to this outcome. The environmental lobby praised the committee's decision; while business and tourism groups were outraged. The regional newspapers were equally incensed, with the Evening Express editorial branding the seven councillors who voted against the resort as 'traitors to the North-east' and demanding their resignation ("Betrayed" [2007](#bet07)). Trump, meanwhile, suggested that he would abandon his Menie plans, saying that he was 'considering doing something very spectacular in another location. Sadly, it will not be in Scotland' ([Smith 2007](#smi07)).

Following this outcry, a few days later on 4 December 2007, the Scottish Government took the apparently unprecedented step of 'calling in' the planning application before any appeal had been made by Trump International, stating that it 'raises issues of importance that require consideration at a national level' ([Scottish Government 2007](#sco07)). As a result, a four-week public inquiry was held in Aberdeen in the summer of 2008, where public participation was again encouraged. Indeed, amongst the evidence, the inquiry reporters considered some 2,700 letters or emails supporting the resort, and 2,084 letters or emails, plus four petitions, opposing the development ([Scottish Government 2008a](#sco08a)).

Then, on 3 November 2008, the Scottish Government granted outline planning permission for the resort, subject to 46 conditions, indicating that 'there was significant economic and social benefit to be gained from this project' ([Scottish Government 2008b](#sco08b)). Again, the response to this decision was predictably polarised. Industry and tourism leaders declared that it demonstrated that the North-east, and Scotland more widely, was 'open for business' ([Whitaker, 2008a](#whi08a)); while environmentalists feared that it would set a 'precedent which will undermine the whole protected-sites network in Scotland' ([Urquhart, 2008](#urq08)). That was far from being the end of the golf resort story, however. It has taken many twists and turns since receiving Scottish Government approval; and, in the process, has provided enough material for several academic studies. This paper will continue by discussing just some of the issues that have emerged, focusing on those relating to information provision and communication.

In May 2009, it emerged that Trump International, in preparing its detailed resort masterplan, had advised Aberdeenshire Council that it would be lodging further applications for outline planning permission, but for eight parcels of land not currently owned by the organisation. These included four private homes that Trump International was hoping to acquire through 'amicable' discussion. It also emerged, however, that Aberdeenshire Council had prepared a briefing note for councillors which raised the prospect of compulsory purchase orders being used to secure the land ([Ross 2009](#ros09)), although Trump International insisted that this would only be pursued as an 'absolute last resort' ([Urquhart 2009](#urq09)). Given that compulsory purchase orders are normally used only for major public infrastructure projects, this prospect incurred the wrath of Trump's opponents. Within a few days, a new protest group, Tripping Up Trump, had been formed, with the primary aim of opposing compulsory purchase orders; and by the end of September 2009 had raised a 15,000-signature petition against compulsory purchase ([Bell, 2009a](#bel09a)). In October 2009, Tripping Up Trump and the Scottish Green Party jointly commissioned an opinion poll which established that 74% of Scots were opposed to compulsory purchase orders at Menie ([Bell, 2009b](#bel09b)). And in May 2010, Tripping Up Trump bought an acre of land from one of the residents threatened with eviction, dubbed it 'The Bunker', and placed multiple names on the title deeds, to create a legal headache for Trump International and Aberdeenshire Council should compulsory purchase orders be pursued ([Gilmartin, 2010](#gil10)). Eventually, in January 2011, Trump announced that he would not be seeking compulsory purchase orders for the residential properties; that he would effectively build his course around them. However, Trump also claimed that 'we have consistently said that we have no interest in compulsory purchase and have never applied for it' ([Urquhart, 2011](#urq11)). This was despite the existence of a letter from early 2009, released under freedom of information, in which his legal representatives had clearly asked Aberdeenshire Council '_to exercise its powers of compulsory purchase…to acquire the eight plots of land on behalf of TIGLE_'. That letter has become widely available online (see, for example, [Scottish Green Party, 2011](#sco11)).

The letter, and other related correspondence, had been released by Aberdeenshire Council under the Freedom of Information (Scotland) Act 2002, which came into force at the beginning of 2005 and which gives people the basic right to see information held by Scottish public authorities. The entire Trump golf course saga has, therefore, taken place under this freedom of information regime; and freedom of information requests relating to the resort — largely, it would appear, from journalists — have become a common occurrence over the last eight years. Indeed, a special report to the Scottish Parliament by the Scottish Information Commissioner, who is responsible for enforcing and promoting Scotland's freedom of information laws, highlighted the '_wealth of information_' relating to the Menie development released under the new Act by the Scottish Government in 2008 ([SIC 2012, p. 7](#sic12)). Although close inspection of these documents (see [Scottish Government, 2008c](#sco08c)) will reveal that 20% were the subject of considerable redaction. Many of the freedom of information requests have related to meetings and correspondence between Trump and his representatives and the two First Ministers of Scotland in post during the planning of the golf course: Jack McConnell of the Scottish Labour Party (First Minister between 2001 and May 2007), and his successor and the current incumbent, the Scottish National Party leader, Alex Salmond. Of particular interest were records of two meetings held immediately prior to the Scottish Government calling in the planning application on 4 December 2007: one involving Salmond and representatives of Trump International on 3 December; the other involving Scotland's Chief Planner and the same Trump International representatives on 4 December.

News of these meetings broke a few days later ([Salmond's Trump, 2007](#sal07)) leading to allegations of '_sleaze_' by the Scottish Liberal Democrat leader, Nicol Stephen ([Scottish Parliament, 2007](#scp07)), and ultimately resulting in a parliamentary inquiry into the Menie planning application, where Salmond, although cleared of any wrongdoing, was described as '_cavalier_' in his actions ([Scottish Parliament. _Local Government and Communities Committee_, 2008](#spl08)). Not everyone in the Scottish political arena was happy with this flurry of information provision, however. On discovering that civil servants had spent more than 2,000 hours over a four-month period, at a cost of over £50,000, answering 175 parliamentary questions and 130 freedom of information requests relating either to the Trump resort or to another controversial planning application (for the proposed expansion of the Aviemore Highland Resort), one Scottish National Party Member of the Scottish Parliament (MSP), laid the blame firmly at the door of opposition parties. He claimed that this money had been 'wasted' in an effort to '_score political points and to try and embarrass SNP ministers_' ([Whitaker, 2008b](#whi08b)).

It has not only been the Scottish Government and Aberdeenshire Council that have received freedom of information requests relating to the golf resort. Information has been sought from a range of public bodies with varying degrees of involvement with the project. For example, in 2006, Scotland's national economic development agency, Scottish Enterprise, and its international arm, Scottish Development International, released documents that showed the extent to which agency officials were 'courting' Trump and his representatives when interest in investing in Aberdeenshire was first expressed. This included arranging a New York meeting between Trump and Jack McConnell, thereby offering him '_a direct line into the government in Scotland_' ([Barnes and Watson, 2006](#bar06)). In 2011, the local police force released over 200 pages of documents and emails which suggested that Trump International had '_unrealistic expectations_' that the local police would behave like the New York Police Department in providing security at the resort ([Edwards, 2011](#edw11)).

At times, however, public bodies have appeared unwilling to release information. At the time of writing (December 2013), the Scottish Information Commissioner has completed six investigations into freedom of information applications for information relating to the golf resort, all of which found that the public bodies concerned had failed to comply fully with the requirements of the Scottish Act or the Environmental Information (Scotland) Regulations 2004\. These examples of non-compliance included: a failure to meet the timescales set down in the legislation ([Scottish Information Commissioner, 2006](#sic06)); a failure to provide appropriate advice and assistance to the applicant ([Scottish Information Commissioner, 2008a](#sic08a)); and the withholding of information that the applicant was entitled to receive ([Scottish Information Commissioner, 2008b](#sic08b)). On this last point, it is fair to say that some of Trump's opponents remain sceptical that all pertinent information relating to the development has found its way into the public domain (see, for example, [Kelly, 2013a](#kel13a)).

Opponents of Trump have also been critical of the role of the regional newspapers — the _Evening Express_ and the _Press & Journal_ — during the golf course development. While these two papers provided an impartial account of proceedings during the gas terminal development in the 1970s, the same could not be said about their more recent coverage of the Trump resort. When Trump's proposals were first announced, both papers immediately expressed their support, with the _Evening Express_ ([Crucial we give..., 2006](#cru06)) advising its readers that 'it is crucial we embrace' the plans. Since then, both papers have maintained an obvious pro-Trump stance, which has manifested itself in a variety of ways. For example, environmental opponents of the golf course have been described variously, by editors and columnists, as '_wrecking crews_' ([Seize the day..., 2007](#sei07)), '_naysayers and Nimbys_' ([Nimby opposition, 2007](#nim07)), '_shiny-eyed saviours of a bit of sand_' ([Why we can't..., 2007](#why07)), and '_a bunch of whinging, squabbling bairns [children] more worried about birds than people_' ([Let's not..., 2007](#let07)). The seven councillors in the infrastructure services committee who voted against the resort received similar treatment, being branded as '_misfits_', '_no-hopers_', '_small-minded numpties [fools]_' and '_buffoons in woolly jumpers_' ([Betrayed..., 2007](#bet07)). The chair of the committee, Martin Ford, whose casting vote had originally rejected Trump's proposals, was singled out for particular criticism. Indeed, Ford has since provided his own account of his portrayal in the regional press ([Ford, 2011](#for11)).

While other newspapers have been asking fundamental questions about planning policy, government openness, and ethical business practices, the _Evening Express_ has often focused on the more trivial aspects of Trump's lifestyle. Readers have learned, for example, how he spends his Christmas ([Ewen 2009](#ewe09)), and how he and his family enjoy a 'hearty Scottish breakfast' during their visits to the North-east ([Rennie 2011](#ren11)). Meanwhile, in December 2009, the _Press & Journal_ ceased all coverage of Tripping Up Trump group, effectively because key figures in the group did not originate from, live, work, or study in, the North-east of Scotland. As the paper's editorial declared:

> 'It is now apparent that the opposition has been orchestrated by an organisation which has little interest in the area it claims to be trying to protect… This newspaper has given a voice to all those who have wished to become involved in the debate about Donald Trump's plans. That courtesy was extended to Tripping Up Trump in the belief that it was a bona fide group of local environmentalists. Today, it has been withdrawn.' ([Tucker, 2009](#tuc09))

Both newspapers have also provided negligible coverage of the aforementioned documentary, _You've Been Trumped_, which does not portray Trump International in a particularly favourable light. Despite having won several international film festival awards, it has rarely received a mention in the regional press, a pattern noted by its director (see [Kelly, 2011](#kel11)).

Overall, then, readers of the two titles have received a rather slanted, one-sided view of the golf course development. Why this should be the case has been debated by opponents of the course, who suggest that the papers may have been seduced by Trump's wealth and fame, or that they have perhaps been more interested in seeking advertising revenue from Trump International than in providing any objective accounts of the resort's development ([Kelly, 2012](#kel12)). Certainly, in this last regard, the _Press & Journal_ and the _Evening Express_, in common with most other newspaper titles in Scotland, have been faced with falling circulation figures in difficult economic times. Indeed, Table 1 provides recent figures for the two papers, comparing these with circulations in 1973, when the gas terminal controversy was at its peak. As can be seen, the readership of each title has fallen dramatically over the last forty years.

<table><caption>

Table 1: Circulation figures for _Press & Journal_ and _Evening Express_: comparison between 1973 and 2012  
<small>Sources: 1973 figures ([Nicoll, 1973](#nic73)); 2012 figures (Joint Industry Committee for Regional Media Research, see [http://jiab.jicreg.co.uk)](http://jiab.jicreg.co.uk)</small></caption>

<tbody>

<tr>

<th>Title</th>

<th>1973</th>

<th>2012</th>

<th>% fall</th>

</tr>

<tr>

<td>

_Press & Journal_</td>

<td>107,910</td>

<td>66,679</td>

<td>-38.2</td>

</tr>

<tr>

<td>

_Evening Express_</td>

<td>75,819</td>

<td>40,722</td>

<td>-46.3</td>

</tr>

</tbody>

</table>

Perhaps other factors have contributed to the newspapers' pro-Trump stance. For example, it recently emerged that the current editor of the _Press & Journal_ (and editor of the _Evening Express_ between April 2006 and March 2011) had married the Executive Vice President of Trump International. Although the newspapers themselves have not mentioned the event, other sources, including the satirical magazine _Private Eye_ ([Street of shame, 2013](#str13)), and the online citizen newspaper _Aberdeen Voice_ ([Kelly, 2013b](#kel13b)), have certainly done so and have drawn their own conclusions as to the impact on the papers' objectivity. _[Aberdeen Voice](http://www.aberdeenvoice.com)_ was launched in June 2010, in an effort to _'give a voice to the general public in the North East and to promote inclusion in affairs affecting the region_'. Since then, it has contained several (mostly critical) articles on the Trump development. While many of these articles could not themselves be regarded as being entirely impartial, and while its readership figures are unclear, it might be argued that the emergence of _Aberdeen Voice_ has gone some way to addressing the imbalance caused by the coverage of the two regional newspapers.

So, what is the current situation with Trump's golf resort? The first championship course, together with a temporary clubhouse, opened in July 2012; although, in a situation reminiscent of that which took place during the construction of the gas terminal, Trump International has been criticised by Aberdeenshire Council for the number of retrospective planning applications it has made in order to remedy '_breaches of planning control_' ([Edwards, 2013](#edw13)). The second golf course is at the planning stage, and construction work is expected to commence shortly on a permanent clubhouse. However, the rest of the resort — the hotel, housing, etc. — has been put on hold because, in March 2013, the Scottish Government finally granted consent for the development of the nearby offshore wind farm, by now an eleven-turbine project. Trump immediately vowed to mount a legal challenge against the decision, saying '_We will spend whatever monies are necessary to see to it that these huge and unsightly industrial wind turbines are never constructed_' ([Crighton, 2013](#cri13)). A year earlier, Trump had told a Scottish Parliamentary inquiry into renewable energy targets that both First Ministers McConnell and Salmond had given him assurances that the windfarm would never be built (a claim both men deny), leaving him feeling '_betrayed_' ([Scottish Parliament, _Economy, Energy and Tourism Committee_, 2012: column 1341](#scp12)). It would appear, then, that there is no immediate end in sight to the controversy surrounding the golf course.

## Conclusions

This paper has explored and compared public access to information about two controversial coastal building developments in North-east Scotland: the proposed construction of a gas terminal at Crimond by the British Gas Council and Total in the 1970s, and the current development of a golf resort by Donald Trump at Menie. The Trump project has taken place during a period of supposed government transparency, when information relating to planning applications is theoretically more readily available than in the past. Using these two projects as examples, can it really be said that today's North-east citizens are better informed than their counterparts of 40 years ago?

Certainly, in terms of the official provision of information about the planned developments, and of opportunities for the public to respond to these plans, then those with an interest in Trump's golf course have been better placed than those who wished to have their say on the initial gas terminal proposals four decades earlier. Planning permission to build a gas terminal at Crimond was sought within just fourteen days and was based on 'the sketchiest of information' ([Dunnet 1974, p. 14](#dug74)); and the one public meeting that took place was arranged at the behest of concerned residents and conservationists rather than by government officials or the developers. In contrast, the full golf resort planning application was effectively subject to an eight-month consultation period, and was supported by lengthy and widely available documentation (both online and offline), together with a programme of public meetings, which detailed the plans and set out their anticipated environmental, economic and transport impacts. This allowed around 3,000 organisations and individuals to have their say on Trump's proposals before the Aberdeenshire Council infrastructure services committee initially refused planning permission. The subsequent public inquiry allowed thousands more to offer their opinions on the Menie resort.

However, in both cases, the information emanating from the developers has been peppered with misleading or questionable statements and data. With the gas terminal proposals, for example, the Gas Council and Total appeared reluctant to reveal publicly that the entire North-east coastline had not yet been surveyed for suitable pipeline landfall locations, implying that the Crimond site was their only option. They also made dubious claims about having conducted extensive consultation with environmental bodies. With the golf resort, the economic benefits forecast by Trump International and their consultants continue to be challenged ("Trouble with Trump" [2013](#tro13)); their claim that compulsory purchase orders had never been pursued can certainly be disputed; while Trump's allegations that two of Scotland's First Ministers had each given him verbal assurances that the offending offshore windfarm would never be built have been vigorously denied by both men. The 21st century North-east citizen may well have received more extensive and detailed information from the developers than his 1970s equivalent, but it might be argued that much of it should be treated with caution.

Perhaps the same could be said about the information emanating from the two regional newspapers, the Press & Journal and the Evening Express. Forty years ago, these two papers provided a reasonably balanced and objective account of the 'battle over the Loch of Strathbeg' ([Navy base..., 1972](#nav72)). However, despite a recent claim that they 'provide one of the few platforms for the little man to take on the big institutions' ([Forsyth, 2012](#for12)), the same two titles have displayed a distinct lack of impartiality during the Trump golf resort saga. The current author would argue, therefore, that those North-east citizens who rely on the regional press as their main information source are less well-informed than their 1970s predecessors; and that the one-sided editorial direction of the two papers may well have had a skewed impact on public awareness and opinion during the planning and construction stages of the Trump development.

Finally, what of the impact of freedom of information legislation in the 21st century? As has been seen, with the gas terminal proposal in the 1970s, many of the meetings involving politicians, civil servants and the developers took place behind closed doors, with the associated documentation and other relevant correspondence being kept hidden from the public eye for the next 30 years. Similar private meetings have clearly taken place in relation to the Trump International golf course, but the existence of the freedom of informationSA has ensured that at least some details of these meetings, and other previously secret correspondence, have become publicly available. It can hardly be said, however, that all government officials, political figures and public bodies have fully embraced the spirit of openness and transparency. A significant proportion of the material obtained under the freedom of informationSA has been censored; and much of the potentially controversial information has been released almost under duress, following SIC investigations, rather than being provided as a matter of course. One MSP also made the questionable claim that freedom of information requests relating to the golf course had largely been made by elected members for political point scoring purposes, rather than by investigative journalists and other interested parties with a view to seeking the truth and serving the public good. So, shall the public ever learn the full story behind the decision-making processes concerning the Trump International golf resort? Interestingly, the Scottish Government is in the process of relaxing the 30-year rule for historical records, making many public bodies' files available after fifteen years ([Scottish Parliament. _Information Centre_, 2012](#spi12)). Perhaps, then, in 15-20 years' time, we will finally gain access to all, unredacted, official records relating to the Menie project, and find evidence, or otherwise, of any political machinations surrounding the '_world's greatest golf course_'.

## <a id="author"></a>About the author

Graeme Baxter is a Research Assistant in the Department of Information Management at the Aberdeen Business School, Robert Gordon University, UK. His research interests include: the provision of, need for, and use of government, parliamentary and citizenship information; freedom of information; the use of the Internet by political parties and electoral candidates; and public participation in government consultation exercises. He can be contacted at: [g.baxter@rgu.ac.uk](mailto:g.baxter@rgu.ac.uk)

## Note

This paper is based on a presentation at the Information: Interactions and Impact (i<sup>3</sup>) Conference. This biennial international conference is organised by the Department of Information Management and Research Institute for Management, Governance and Society (IMaGeS) at Robert Gordon University. i<sup>3</sup> 2013 was held at Robert Gordon University, Aberdeen, UK on 25-28 June 2013\. Further details are available at [http://www.i3conference.org.uk/](http://www.i3conference.org.uk/)

</section>

<section>

## References

<ul>

<li id="acd07">Aberdeenshire Council. <em>Development Management &amp; Building Standards</em>. (2007). <a
href="http://www.Webcitation.org/6Lw7hduVN"><em>Formartine Area Committee report - 18 September
    2007.</em></a> Retrieved from
http://www.aberdeenshire.gov.uk/committees/files_meta/802572870061668E80257352004E6138%5C2006-4605.pdf
(Archived by WebCite&reg; at http://www.Webcitation.org/6Lw7hduVN)</li>
<li id="aci07">Aberdeenshire Council. <em>Infrastructure Services Committee</em>. (2007). <a
href="http://www.Webcitation.org/6Lw7yGyGL"><em>Infrastructure Services Committee, Woodhill House,
    Aberdeen, 29 November, 2007.</em></a> Retrieved from
http://www.aberdeenshire.gov.uk/committees/files_meta/802572870061668E802573C50042E4EF/291107isc.pdf
(Archived by WebCite&reg; at http://www.Webcitation.org/6Lw7yGyGL)
</li>
<li id="acp07">Aberdeenshire Council. <em>Planning and Environmental Services</em>. (2007). <a
href="http://www.Webcitation.org/6Lw8E45ua">Infrastructure Services Committee - 29 November 2007. </a>
Retrieved from
http://www.aberdeenshire.gov.uk/committees/files_meta/802572870061668E8025739A005DF53D%5C%2808%29%20APP-2006-4605-Menie.pdf
(Archived by WebCite&reg; at http://www.Webcitation.org/6Lw8E45ua)
</li>
<li id="bar06">Barnes, E. &amp; Watson, J. (2006, May 14). How Jack of clubs came up trumps for Donald.
<em>Scotland on Sunday</em>, p.12.
</li>
<li id="bel09a">Bell, G. (2009a, September 30). Compulsory purchase orders set to be blocked. <em>Press and
Journal</em>, p.3.
</li>
<li id="bel09b">Bell, G. (2009b, November 16). 'Hammer blow' to Trump project. <em>Press and Journal</em>,
p.12.
</li>
<li id="bet07">Betrayed by stupidity of seven. (2007, November 30). <em>Evening Express</em>, p.6.
</li>
<li id="big73">Big gas plant goes to St Fergus. (1973, April 27). <em>Evening Express</em>, p.1.
</li>
<li id="bir73">Bird society still in dark over Crimond. (1973, January 5). <em>Press and Journal</em>, p.3.
</li>
<li id="bou72">Bourne, W.R.P. (1972, December 26). The Gas Council v the conservationists [Letter to the
editor]. <em>Press and Journal</em>, p.6.
</li>
<li id="bou05">Bourne, W.R.P. (2005). <a href="http://www.Webcitation.org/6Lw8U6sly">Windfarms a threat? -
or can birds adapt? </a> <em>Scottish Bird News</em>, <strong>77</strong>(16) Retrieved from
http://www.the-soc.org.uk/docs/SBN77.pdf (Archived by WebCite&reg; at
http://www.Webcitation.org/6Lw8U6sly)
</li>
<li id="bou73">Bourne, W.R.P., Gimingham, C.H., Morgan, N.C. &amp; Britton, R.H. (1973). The Loch of
Strathbeg. <em>Nature</em>, <strong>242</strong>(5393), 93-95.
</li>
<li id="buc73">Buchan conservation (1973, February 16). <em>Fraserburgh Herald</em>, p.5.
</li>
<li id="bur76">Burton, A.W. &amp; Johnson, R. (1976). <em>Public participation in planning: a review of
experience in Scotland</em>. Glasgow: The Planning Exchange.
</li>
<li id="cri13">Crighton, R. (2013, March 27). Offshore windfarm plans facing six-year court battle.
<em>Press and Journal</em>, p.10.
</li>
<li id="cru06">Crucial we give Trump fair hearing. (2006, April 15). <em>Evening Express</em>, p.6.
</li>
<li id="dea81">Dean, F.E. (1981). St Fergus: visual and design considerations. In: W.J. Cairns &amp; P.M.
Rogers (Eds). <em>Onshore impacts of offshore oil</em> (pp. 145-153). Barking: Applied Science Publishers
Ltd.
</li>
<li id="dec72">Decision before Christmas on who gets Crimond airfield. (1972, December 2). <em>Press and
Journal</em>, p.1.
</li>
<li id="dec71">Decision soon on radio mast site (1971, September 28). <em>Press and Journal</em>, p.7.
</li>
<li id="del07">Deloitte MCS Limited (2007). <a href="http://www.Webcitation.org/6Lw8hkLQz"><em>Trump
    International Golf Club Scotland Ltd: Menie Estate development: economic impact assessment &amp;
    financial review: executive summary.</em> </a> Retrieved from
http://www.scotland.gov.uk/Resource/Doc/216107/0057846.pdf (Archived by WebCite&reg; at
http://www.Webcitation.org/6Lw8hkLQz)
</li>
<li id="dun08">Dunlop, S. (2008). <a href="http://www.Webcitation.org/6Lw8qTxtp">The Trump development in
Aberdeenshire: what are the issues? </a> <em>Fraser Economic Commentary</em>, <strong>32</strong>(2),
47-51. Retrieved from
http://www.strath.ac.uk/media/departments/economics/fairse/fecs/Final_FEC_Vol_32_No_2.pdf (Archived by
WebCite&reg; at http://www.Webcitation.org/6Lw8qTxtp)
</li>
<li id="dun72a">Dunn, H. (1972a, November 30). Bird-lovers and defence chiefs oppose oil firm.
<em>Scotsman</em>, p.11.
</li>
<li id="dun72b">Dunn, H. (1972b, December 23). Fears over 'monster' gas plant. <em>Scotsman</em>, p.1.
</li>
<li id="dug72">Dunnet, G.M. (1972, December 9). Crimond site rush deplored [Letter to the editor]. <em>Press
and Journal</em>, p.6.
</li>
<li id="dug74">Dunnet, G.M. (1974). <a href="http://www.Webcitation.org/6Lw8w6eRL">Impact of the oil
industry on Scotland's coasts and birds. </a> <em>Scottish Birds</em>, <strong>8</strong>(1), 3-16.
Retrieved from http://www.the-soc.org.uk/docs/scottish-birds/sb-vol08-no01.pdf (Archived by WebCite&reg;
at http://www.Webcitation.org/6Lw8w6eRL) </li>
<li id="edw11">Edwards, R. (2011, October 2). Trump accused of using police as his private security force.
<em>Sunday Herald</em>, p.24.
</li>
<li id="edw13">Edwards, R. (2013, June 16). Trump criticised for planning breaches. <em>Sunday Herald</em>,
p.16.
</li>
<li id="ewe09">Ewen, D. (2009, December 26). 'I work on holidays &hellip; it helps me relax'. <em>Evening
Express</em>, p.18.
</li>
<li id="for11">Ford, M.A. (2011). Deciding the fate of a magical, wild place. <em>Journal of Irish and
Scottish Studies</em>, <strong>4</strong>(2), 33-73.
</li>
<li id="for12">Forsyth, T. (2012, November 30). Time to pause and take stock. <em>Press and Journal</em>,
p.30.
</li>
<li id="gas72">Gas plant 'won't ruin Crimond wildlife'. (1972, December 15). <em>Evening Express</em>, p.5.
</li>
<li id="gil10">Gilmartin, K. (2010, May 26). Trump resort plans are hit by land grab. <em>Herald</em>, p.5.
</li>
<li id="hux74">Huxley, J. (1974, January 7). We can't hold on to all Scotland's scenery&hellip;says Aberdeen
County Council convener. <em>Evening Express</em>, p.6.
</li>
<li id="iro07a">Ironside Farrar (2007a). <a href="http://www.Webcitation.org/6Lw977juK"><em>Golf &amp;
    leisure resort, Menie Estate, Balmedie, Aberdeenshire: environmental statement non-technical
    summary.</em> </a> Retrieved from
http://www.ukplanning.com/aberdeenshire/doc/Other-4210355.pdf?extension=.pdf&amp;id=4210355&amp;location=VOLUME4&amp;contentType=application/pdf&amp;pageCount=1
(Archived by WebCite&reg; at http://www.Webcitation.org/6Lw977juK) </li>
<li id="iro07b">Ironside Farrar (2007b). <a href="http://www.Webcitation.org/6Lw9DQz9c"><em>Golf &amp;
    leisure resort, Menie Estate, Balmedie, Aberdeenshire: response to Aberdeenshire Council &amp;
    statutory consultations. </em></a> Retrieved from
http://www.ukplanning.com/aberdeenshire/doc/Other-4601593.pdf?extension=.pdf&amp;id=4601593&amp;location=VOLUME4&amp;contentType=application/pdf&amp;pageCount=50
(Archived by WebCite&reg; at http://www.Webcitation.org/6Lw9DQz9c)
</li>
<li id="ist06">Is Trump teeing up for new Scots links course? (2006, January 12). <em>Press and
Journal</em>, p.1.
</li>
<li id="joh06">Johnston, J. &amp; Smith, G. (2006, April 1). Wind may be hazard in Trump's plans to build
&pound;300m golf resort. <em>Herald</em>, p.3.
</li>
<li id="kel11">Kelly, S. (2011, June 24). <a href="http://www.Webcitation.org/6Lw9ddEmO">In conversation
with Anthony Baxter. </a> <em>Aberdeen Voice</em>. Retrieved from
http://aberdeenvoice.com/2011/06/in-conversation-with-anthony-baxter/ (Archived by WebCite&reg; at
http://www.Webcitation.org/6Lw9ddEmO)
</li>
<li id="kel12">Kelly, S. (2012, October 24). <a href="http://www.Webcitation.org/6Lw9n26qH">Bully for you:
Trump threatens suit against filmmaker. </a> <em>Aberdeen Voice</em>. Retrieved from
http://aberdeenvoice.com/2012/10/bully-for-you-trump-threatens-suit-against-filmmaker/ (Archived by
WebCite&reg; at http://www.Webcitation.org/6Lw9n26qH)
</li>
<li id="kel13a">Kelly, S. (2013a, June 10). <a href="http://www.Webcitation.org/6Lw9w70p2">Scottish
Enterprise, Trump and Menie - business as usual. </a> <em>Aberdeen Voice</em>. Retrieved from
http://aberdeenvoice.com/2013/06/scottish-enterprise-trump-and-menie-business-as-usual/ (Archived by
WebCite&reg; at http://www.Webcitation.org/6Lw9w70p2)
</li>
<li id="kel13b">Kelly, S. (2013b, February 5). <a href="http://www.Webcitation.org/6LwA1Odf4">Trump Vice
President weds Journals ed - joining the dots. </a> <em>Aberdeen Voice</em>. Retrieved from
http://aberdeenvoice.com/2013/02/trump-exec-vp-weds-journals-ed-joining-the-dots/ (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwA1Odf4)
</li>
<li id="kir06">Kirk, T. (2006, March 31). Trump reveals &pound;300m north-east golf dream. <em>Press and
Journal</em>, p.1.
</li>
<li id="let07">Let's not ruin chance. (2007, October 9). <em>Evening Express</em>, p.20.
</li>
<li id="mac72">MacDonald, G. (1972, November 29). Multi-million plan to pipe North Sea gas. <em>Herald</em>,
p.1.
</li>
<li id="mac92">Mackie, M. (1992). <em>A lucky chap: orra loon to Lord Lieutenant</em>. Buchan: Ardo
Publishing Company.
</li>
<li id="nav72">Navy base wins Crimond battle. (1972, December 14). <em>Evening Express</em>, p.1.
</li>
<li id="nic73">Nicoll, J. (1973). <em>Aberdeen: official handbook and industrial review</em>. Cheltenham:
Ed. J. Burrow &amp; Co. Ltd, pp.27-29.
</li>
<li id="nim07">Nimby opposition to Trump is unsustainable. (2007, September 25). <em>Evening Express</em>,
p.20.
</li>
<li id="nor73">North-eastern environmental liaison group now call for environmental alertness (1973, May
11). <em>Fraserburgh Herald</em>, p.3.
</li>
<li id="ogi73">Ogilvie, G. (1976, February 9). People's lives matter: tell us what is going on. <em>Evening
Express</em>, p.8.
</li>
<li id="ren11">Rennie, A. (2011, June 23). 'I can escape the concrete jungle of New York to the beautiful
scenery here'. <em>Evening Express</em>, p.4.
</li>
<li id="ros09">Ross, C. (2009, May 9). Unusual Trump plan considered. <em>Press and Journal</em>, p.4.
</li>
<li id="sal07">Salmond's Trump golf team meeting. (2007, December 9). <a
href="http://www.Webcitation.org/6LwAALzYf"><em>BBC News.</em> </a> Retrieved from
http://news.bbc.co.uk/1/hi/scotland/north_east/7135266.stm (Archived by WebCite&reg; at
http://www.Webcitation.org/6LwAALzYf) </li>

<li id="sdd73">Scottish Development Department. (1973). <em>Gas industry 1972-73</em>. Edinburgh: National
Records of Scotland. (SEP 4/2370) </li>

<li id="sco07">Scottish Government. (2007). <a href="http://www.Webcitation.org/6LwAKtqvx"><em>Proposed golf
    course at Menie estate.</em> News release, 4 December. </a> Retrieved from
http://www.scotland.gov.uk/News/Releases/2007/12/05082758 (Archived by WebCite&reg; at
http://www.Webcitation.org/6LwAKtqvx) </li>

<li id="sco08a">Scottish Government. (2008a). <a href="http://www.Webcitation.org/6LwASeZtp"><em>Decision on
    the Menie Estate planning application by Trump International Golf Links, Scotland.</em> </a> Retrieved
from http://www.scotland.gov.uk/Resource/Doc/212607/0067709.pdf (Archived by WebCite&reg; at
http://www.Webcitation.org/6LwASeZtp) </li>

<li id="sco08b">Scottish Government. (2008b). <a href="http://www.Webcitation.org/6LwAnFLTf"><em>Trump golf
    resort gets go ahead.</em> News release, 3 November. </a> Retrieved from
http://www.scotland.gov.uk/News/Releases/2008/11/03123709 (Archived by WebCite&reg; at
http://www.Webcitation.org/6LwAnFLTf) </li>

<li id="sco08c">Scottish Government. (2008c). <a href="http://www.Webcitation.org/6LwAvqngo"><em>Information
    regarding the Menie Estate planning application by Trump International Golf Links, Scotland. </em></a>
Retrieved from
http://www.scotland.gov.uk/About/Information/freedom of information/Disclosures/2008/03/Trump (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwAvqngo)
</li>
<li id="sco11">Scottish Green Party. (2011). <a href="http://www.Webcitation.org/6LwB1k0EG"><em>Trump
    climbdown cautiously welcomed.</em> News release, 31 January. </a> Retrieved from
http://www.scottishgreens.org.uk/news/show/6472/trump-climbdown-cautiously-welcomed (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwB1k0EG) </li>

<li id="sic06">Scottish Information Commissioner. (2006). <a
href="http://www.Webcitation.org/6LwBh2a5Y"><em>Decision 197/2006: Mr Tom Gordon and the Scottish
    Executive.</em> </a> Retrieved from
http://www.itspublicknowledge.info/ApplicationsandDecisions/Decisions/2006/200601313.aspx (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwBh2a5Y)
</li>
<li id="sic08a">Scottish Information Commissioner. (2008a). <a
href="http://www.Webcitation.org/6LwBpWLHr"><em>Decision 083/2008: Mr Ellis Thorpe and Scottish
    Enterprise. </em></a> Retrieved from
http://www.itspublicknowledge.info/applicationsanddecisions/Decisions/2008/200701251.aspx (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwBpWLHr) </li>

<li id="sic08b">Scottish Information Commissioner. (2008b). <a
href="http://www.Webcitation.org/6LwBwGDjb"><em> Decision 139/2008: Mr Rob Edwards and the Scottish
    Ministers.</em> </a> Retrieved from
http://www.itspublicknowledge.info/applicationsanddecisions/Decisions/2008/200800605.aspx (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwBwGDjb) </li>

<li id="sic12">Scottish Information Commissioner. (2012). <a
href="http://www.Webcitation.org/6LwC7BJtd"><em> Informing the future: the state of freedom of
    information in Scotland.</em> </a> Retrieved from
http://www.itspublicknowledge.info/nmsruntime/saveasdialog.aspx?lID=5266&amp;sID=5972 (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwC7BJtd) </li>

<li id="scp07">Scottish Parliament. (2007). <a href="http://www.Webcitation.org/6LwBAlPV9"><em>Official
    report debate contributions: plenary, 13 December.</em> </a> Retrieved from
http://www.scottish.parliament.uk/parliamentarybusiness/28862.aspx?r=4762&amp;mode=html (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwBAlPV9) </li>

<li id="scp12">Scottish Parliament. <em>Economy, Energy and Tourism Committee</em>. (2012). <a
href="http://www.Webcitation.org/6LwBHosAF"><em>Official report, Wednesday 25 April 2012. </em></a>
Retrieved from http://www.scottish.parliament.uk/parliamentarybusiness/28862.aspx?r=7122&amp;mode=pdf
(Archived by WebCite&reg; at http://www.Webcitation.org/6LwBHosAF) </li>

<li id="spi12">Scottish Parliament. <em>Information Centre</em>. (2012). <a
href="http://www.Webcitation.org/6LwBPNBvu">Freedom of Information (Amendment) (Scotland) Bill.
</a><em>SPICe Briefing 12/54. </em> Retrieved from
http://www.scottish.parliament.uk/ResearchBriefingsAndFactsheets/Factsheets/SB_12-54.pdf (Archived by
WebCite&reg; at http://www.Webcitation.org/6LwBPNBvu) </li>

<li id="spl08">Scottish Parliament. <em>Local Government and Communities Committee</em>. (2008). <a
href="http://www.Webcitation.org/6LwBZpJTJ"><em>5th report, 2008: Planning application processes (Menie
    Estate).</em> </a> Retrieved from
http://archive.scottish.parliament.uk/s3/committees/lgc/reports-08/lgr08-05.htm (Archived by WebCite&reg;
at http://www.Webcitation.org/6LwBZpJTJ) </li>

<li id="sei07">Seize the day - for region's sake. (2007, March 31). <em>Press and Journal</em>, p.44. </li>

<li id="smi07">Smith, G. (2007, November 30). Opponents trump Donald's dreams. <em>Herald</em>, p.5. </li>

<li id="str13">Street of shame. (2013, February 22). <em>Private Eye</em>, no. 1334, 6-7. </li>

<li id="tom90">Tomlinson, D. (1990). Terns for the better. <em>Country Life</em>, <strong>184</strong>(27),
131. </li>

<li id="tro13">Trouble with Trump (2013, July 13). <a href="http://www.Webcitation.org/6LwCCgZYR"><em>BBC
    News.</em> </a> Retrieved from http://www.bbc.co.uk/programmes/n3csvf4s (Archived by WebCite&reg; at
http://www.Webcitation.org/6LwCCgZYR) </li>

<li id="tuc09">Tucker, D. (2009, December 12). Protest group's credibility gap. Press and Journal, p.38.
</li>

<li id="urq07">Urquhart, F. (2007, May 3). Protests muted as Trump's golf plans attract just 60 comments.
<em>Scotsman</em>, p.8. </li>

<li id="urq08">Urquhart, F. (2008, November 4). 'The time is right. I want to get started'.
<em>Scotsman</em>, p.1. </li>

<li id="urq09">Urquhart, F. (2009, May 9). Trump accused of 'Highland Clearances' after bid to take over
neighbours' land. <em>Scotsman</em>, p.11. </li>

<li id="urq11">Urquhart, F. (2011, February 1). Scepticism as Trump claims no evictions over Menie.
<em>Scotsman</em>, p.22. </li>

<li id="wap74">Wapshott, N. (1974, September 9). Planning rumpus at St Fergus. <em>Scotsman</em>, p.4. </li>

<li id="war11">Ward, B. (2011, September 13). <a href="http://www.Webcitation.org/6LwCK5ma4">You've Been
Trumped: film reveals tycoon's ruthless efforts to build Scottish golf resort. </a> [Web log entry].
<em>Guardian Environment Blog</em>. Retrieved from
http://www.theguardian.com/environment/blog/2011/sep/13/youve-been-trumped-scotland-golf-course?CMP=twt_fd
(Archived by WebCite&reg; at http://www.Webcitation.org/6LwCK5ma4) </li>

<li id="wed72">We don't know what ? but it's coming for a' that. (1972, November 17). <em>Fraserburgh
Herald</em>, p.2. </li>

<li id="whi08a">Whitaker, A. (2008a, November 4). First Minister hails decision as 'victory' for Evening
Express. <em>Evening Express</em>, p.4. </li>

<li id="whi08b">Whitaker, A. (2008b, April 17). Row over cash spent on probe into golf plan. <em>Evening
Express</em>, p.4. </li>

<li id="why07">Why we can't look this gift birdie in the mouth. (2007, November 20). <em>Evening
Express</em>, p.20. </li>

</ul>

</section>

</article>