<header>

#### vol. 19 no. 1, March, 2014

</header>

<article>

# Creating possible selves: information disclosure behaviour on social networks

#### [Jenny Bronstein](#author)  
Department of Information Science, Bar-Ilan University, Ramat-Gan 52900, Israel

#### Abstract

> **Introduction.** This study investigates the creation of alternative identities or possible selves on social networks by examining self-presentation and self-disclosure as elements of the information disclosure behaviour of Facebook users.  
> **Method.** An online questionnaire was distributed amongst library and information science students at Bar-Ilan University, Israel, and 152 students who have a profile on Facebook particpated in the study.  
> **Analysis.** The data analysis consisted of two phases. In the first phase quantitative responses received from the survey were compiled in a spreadsheet and analyzed using descriptive and correlative statistics. The second phase involved a thematic analysis of the participants' textual answers which provided insight into the participants' quantitative responses.  
> **Results.** Findings show that the majority of participants were women who identified themselves using their real name, and who selectively posted personal photographs on their profile. The majority wanted to create a positive or socially acceptable possible self on their profiles and therefore they refrained from disclosing information that is highly personal or embarrassing, did not let their defenses down, and if they did disclose personal information, did so consciously.  
> **Conclusions.** Findings suggest participants do not create their possible selves solely in relation to their social environment, either to impress their social connections or as a reaction to their feedback. Rather, the creation of possible selves is a reflective process that is motivated by the need for self-enhancement and not only by social or communication needs.

<section>

## Introduction

Social networks have become an integral part of the lives of millions of Internet users around the world and like other Web, 2.0 technologies, allow people without any particular technical knowledge to create an online profile and to communicate and share information with others. According to Boyd and Ellison ([2007, p. 2](#boy07)), social networks are '_web-based services that allow individuals to: (1) construct a public or semi-public profile within a bounded system, (2) articulate a list of other users with whom they share a connection, and (3) view and traverse their list of connections and those made by others within the system_'. Several studies have recognized both self-disclosure ([Acquisti and Gross, 2006](#acq06); [Debatin, Lovejoy, Horn and Hughes, 2009](#deb09); [Nosko, Wood and Molema, 2010](#nos10); [Schrammel, Kiffel and Tscheligi, 2009](#sch09)) and social connections ([Brandtzaeg, Luders and Sjetke, 2010](#bra10); [Ellison, Heino and Gibbs, 2006](#ell06); [Zweir, Araujo, Boukes and Willemsen 2011](#zwe11)) as core behaviour on social networks. Users choose how to present themselves on their profiles and the information disclosed can be very limited (e.g., only user name and password) or very extensive (e.g., personal data, hobbies, photographs). The social mechanisms in play reward the provision of information and entice users to disclose and share information ([Schrammel, Kiffel and Tscheligi, 2009](#sch09)).

Information disclosure behaviour relates to the disclosure of personal or intimate information through self-presentation and self-disclosure facilitated by the Internet as well as the avoidance or suppression of disclosure of information ([Bortree 2005](#bor05); [Bronstein 2012](#bro12); [Trammell, Tarkowski, Hofmokl and Sapp 2006](#tra06); [Viegas 2005](#vie05)). Self-presentation refers to the variety of ways in which people present themselves, tailoring the images they convey to others according to situational demands ([Goffman 1959](#gof59)). Self-disclosure has been defined as an individual's propensity for revealing personal information to others ([Archer 1980](#arc80)). According to Schau and Gilly ([2003](#sch03)) the propensity for self-disclosure relates to the content of the self-presentation they choose to display. Strategies of self-presentation often revolve around repressing personal information or supplanting it with modified details that reveal a more desired self. The different ways by which people present themselves and the information they disclose are directly connected to their motivation to create and maintain online social connections. Christofides, Muise and Desmarais ([2009](#chr09)) observe that there is a reciprocal relationship between trust and self-disclosure in online communication: as information disclosure increases the impression of trustworthiness, it results in reciprocal information disclosure on the part of the conversation partner.

Identity is an important element of communication. While message content is important to the communication process, knowing the sender's identity can be equally important in supplying credibility and motivations. According to Marx ([1999](#mar99)), anonymity refers to a state where a person is not identifiable. The degree of anonymity can range from being totally anonymous to lacking anonymity all together, being totally identifiable. Several studies have investigated the extent of self-presentation and self-disclosure on anonymous online settings ([Chesney and Su, 2010](#che10); [Papacharissi 2002](#pap02); [Qian and Scott, 2007](#qia07); [Rheingold, 1995](#rhe95); [Turkle, 1995](#tur95)). These studies emphasize the anonymous and textual nature of the Internet that facilitates self-expression and enables individuals to express themselves freely and behave in ways not permissible or acceptable in their usual social sphere ([Papacharissi, 2002](#pap02)). Other studies have examined information disclosure behaviour in what Zhao, Grasmuck and Martin ([2008](#zha08)) coined _nonymous_ (i.e., the opposite of anonymous) settings such as Internet dating sites ([Ellison _et al._ 2006](#ell06); [Whitty, 2008](#whi08)). These studies found that people act differently according to the degree of nonymity (or anonymity) of the setting; that is, self-presentation and self-disclosure varied according to the nature of the environment. The present study seeks to extend the existing research on different patterns of self-presentation and self-disclosure in a nonymous online environment such as social networks and to examine the influence that online social connections may have on these two information disclosure types.

## Literature survey

According to Markus and Nurius's ([1986](#mar86)) concept of possible selves, an individual's identity is composed of many different self-conceptions, or ideas about the person he or she thinks that he or she is. Self-conceptions can be divided into now selves, which 'describe the self as it presently is perceived by the individual,' and possible selves, which are '_images of the self that have not yet been realized_' ([Markus and Nurius, 1986, p. 957](#mar86)). An individual is free to create any variety of possible selves, yet the pool of alternative identities derives from the categories made salient by the individual's particular socio-cultural and historical context and they reflect the extent to which the self is socially determined and constrained. Therefore, the construction of an identity is an ongoing process in which people handle certain behaviour in order to present a favorable and appropriate image that would influence the way others perceive them ([Goffman, 1959](#gof59)). The Internet is particularly suited to the creation of different types of possible selves. The lack of non-verbal elements and the removal of physical '_gating features_' (e.g., stigmatized appearance, stuttering, shyness etc.) allows individuals to have more control over the information they disclose and to be more inventive in the way they present themselves online ([Bargh, McKenna and Fitsimons, 2002](#bar02); [McKenna and Bargh, 2000](#mck00)).

The issue of self-disclosure is closely related to the creation of possible selves in online communication. Since people tend to be heavily invested in impression management, self-disclosure plays an important role in interpersonal communication. Qian and Scott ([2007](#qia07)) explain that when disclosing one's inner world where typically there are socially embarrassing or unspeakable facts, one runs the risk of jeopardizing other people's impressions. Thus, people need to control the desired nature and degree of self-disclosure that will match the possible selves they want to present on their online profile.

Unlike anonymous settings in which users feel free to present any version of their possible selves they wish, nonymous settings place constraints on the freedom of the identities displayed. The possible selves that can be created on social networks are limited, since a person's offline social connections such as family members, friends and colleagues also communicate with them over the Internet ([Zhao _et al._ 2008](#zha08)). Zhao _et al._ ([2008](#zha08)) called this type of offline-based online relationships ''. The level of anchorage varies depending on the degree to which online partners are identifiable and locatable offline. Ellison _et al._ ([2006](#ell06)) posit that the anticipation of possible face-to-face encounters in anchored relationships narrows the discrepancy between the now selves and the possible selves. Greene, Derlega and Mathews ([2006](#gre06)) further describe an existing tension between the desire to present an authentic version of one self and the need for impression management that is inherent in many aspects of self-disclosure. In making decisions about what and when to self-disclose, individuals often struggle to reconcile opposing needs. For example, Yurchisin, Watchravesringkan and McCabe ([2005](#yur05)) demonstrated that users of Internet dating services present their possible selves or '_hope for selves_' on their profiles by '_stretching the truth_' a bit in order to present a more desirable self. However, this stretching of truth was limited to displaying selected photos to cover up undesirable features or to writing positive self-descriptions. Ellison _et al._ ([2006](#ell06)) found that the identities produced on Internet dating sites were quite honest and realistic as users wanted to avoid unpleasant situations in subsequent face-to-face encounters.

If the creation of online social connections (called _friending_) can be partially based on the possible self and the self-disclosure of the user presented, the question arises as to the meaning of friendship in social networking. In general, friends are defined as 'a subset of peers who engage in mutual companionship, support and intimacy' ([Kim and Lee, 2011, p. 360](#kim11)). However, the concept of friendship has changed in the context of social networking since members do not need to invest great efforts to become friends with other members and once formed, this type of online social connections do not need strong attachment or close relations ([Vanden Boogart, 2006](#van06)). Ellison _et al._ ([2006](#ell06)) posit that since friending has been shown to be one of the main activities of Facebook, a large number social weak ties via Facebook becomes a source of social capital. In sum, the nonymity of social networks presents an ideal situation for the investigation of the construction of possible selves in online environments where social connections are anchored in offline relationships. The current study will focus on the information behaviour of users by examining strategies of self-presentation and self-disclosure used in creating a possible-self related to online social networks connections.

## Methods

### Research questions

The study examined the following research questions regarding self-presentation and self-disclosure patterns of social network site users:

1.  How do participants self-present on their profiles and what do these patterns of self-presentation mean to the possible selves created?
2.  Which patterns of self-disclosure are used by participants to create possible selves on their profiles?
3.  What impact does a profile have on the social connections of participants?
4.  Are participants aware of the level of exposure of the information disclosed on their profiles and how this exposure influences their self-presentation and self-disclosure?

The study was conducted over one month (1-30 June, 2011) using an online survey that was developed specifically for the current study. It was short, simply designed, and easy to answer in order to reduce many of the drawbacks of online questionnaires. It was written in Hebrew, since it was directed at a Hebrew-speaking population ([Baron and Siepman, 2000](#bar00)) and was divided into four main sections:

1.  Students' demographic data: questions addressing the students' personal data such as age, gender, and the frequency with which they update their profile.
2.  Self presentation on the profile: questions about different elements of self- presentation such as the type of photographs displayed.
3.  Disclosure of personal information: questions addressing various elements of self-disclosure.
4.  Open ended questions: questions requesting that participants explain in their own words their answers to the following five items in the questionnaire:
    1.  The kind of photographs they would not disclose on their profile.
    2.  The impact the profiles of participants have on the their online social connections.
    3.  Whether participants have disclosed information on their profile that they would not disclose face-to-face
    4.  What kind of information they have regretted disclosing on their profile.

### Data analysis

The data analysis consisted of two phases. In the first phase quantitative responses received from the survey were compiled in a spreadsheet and analyzed using descriptive and correlative statistics. This phase yielded quantifiable demographic data about the participants and the information they provided or presented in their profiles, about different elements of self-presentation and self-disclosure, and about the awareness that participants have of the level of exposure of the information disclosed in their profiles. In addition, Spearman correlation and chi-square tests were performed to look for correlations between age and gender and different elements of self-presentation and self-disclosure. For ordinal variables, Mann-Whitney test was performed in order to look for differences between different patterns of self-disclosure.

The second phase involved a thematic analysis of the participants' textual answers which provided insight into the participants' quantitative responses. Thematic content analysis is a descriptive presentation of qualitative data that identifies common themes 'in order to give expression to the communality of voices across participants' ([Anderson, 2007, p. 1](#and00)). Thematic analysis facilitates identifying, analyzing, and reporting patterns or themes within data. A theme is a pattern found in the information that describes and organizes the possible observations ([Boyatzis, 1998, p. 4](#boy98)).

Thematic analysis involves the creation and application of codes to data and the data being analyzed might take any number of forms: an interview transcript, field notes, policy documents, photographs, video footage, and other formats as well. Following these principles of thematic content analysis, phrases from the participants' textual answers were selected as the minimal information unit. The phrases were identified and coded into their respective categories by constantly comparing these phrases to the properties of the emerging category until the category was developed and saturated. Each textual answer was coded separately, and the results of the coding for each question are presented independently along with the relevant quantitative analysis of the data. The qualitative data supplements the survey findings with a deeper insight into the participants' experiences and intentions.

### Participants

The study population consisted of BA and MA students at the Information Science Department at Bar-Ilan University in Israel. Of the, 281 students who were invited to participate in the study, 152 students who had a profile on a social networking site answered the questionnaire. The descriptive statistical analysis of the demographic data revealed that, 29.6% (n=45) of participants were men and 70.4% (n=107) were women. The mean of the participants' age is 30.5 (sd=9.0) in a range of, 20-61\. Regarding the different social networks represented in the sample, 88.8% (n=135) of participants reported having a profile on Facebook, while 9.9% (n=15) reported having a profile on Facebook and on one other social network, and 1.3% (n=2) reported having an account on Twitter.

Regarding the frequency with which participants updated their profile, almost half (45.4% n=69) reported updating their profile occasionally, 6.6% (n=10) reported updating once and twice a month, 15.1% (n=23) once a week, 13.20% (n=20) at least three times a week, 5.3% (n=8) once a day and 7.9% (n=12) a few times a day.

<figure>

![Frequency of profile updating](../p609fig1.png)

<figcaption>Figure 1: Frequency of profile updating</figcaption>

</figure>

## Results

### Self-presentation

The first research question examined the different elements of self-presentation that participants use to create possible selves on their profiles. The majority of participants (92.8% n=141) presented themselves using their real name, 3.3% (n=5) used a variation of their name and 3.9% (n=6) used a pseudonym.

<figure>

![Self-presentation patterns](../p609fig2.png)

<figcaption>Figure, 2: Self-presentation patterns</figcaption>

</figure>

Posting visual elements such as photographs is an important element of self-presentation since it provides additional information about the owner of the profile. Participants were asked if they posted photographs on their profile, 75% (n=114) of participants answered affirmatively and, 25% (n=38) answer negatively. The thematic analysis of the participants' textual responses reveal four different motivations for posting photographs: sharing information, self-presentation, documenting life experiences, and creating and maintaining online social connections. The following statements present examples of the participants' textual answers on these four themes.

_Sharing information_. Participants posted photographs to share information about their lives with their social connections on the social networks.

> To share my life experiences with the people close to me, especially with those whom I do not see too often.  
>   
> I want to share my life, my trips and my pastimes with my friends and this [posting photographs on my profile] is a great way to do it.  
>   
> Photographs represent little moments of joy in my life that I'm very happy to share.  
>   
> When blogging there is no need to deal with embarrassing situations or with undesirable reactions from other people.  
>   
> Maybe because people that are close to you tend to be more judgmental than strangers that don't know you.

_Self-presentation._ Photographs are an important element of the self-presentation strategy of participants that helps create their 'possible selves'.

> I post photographs so people would know that it is me.  
>   
> I post photographs on my profile to let people know that I'm always having fun.  
>   
> Photographs are an important visual tool that adds information to my profile.

_Documenting life experiences._ Participants describe their profile as a documenting tool; they posted photographs in order to preserve the information online.

> I post a lot of photos from my trips and from family reunions; Facebook for me is sort of a family album.  
>   
> I use Facebook as an online storage tool to preserve my photos.

_Creating and maintaining online social connections._ Because photographs add to the personal information posted on the profile, participants feel that this element helps them with their social connections on the site.

> Social networks are a way for me to create and maintain social relationships and my photos are part of me. This is why I like to post photos on my profile.  
>   
> Facebook for me is like a bridge to get to know people from Latin America, to assist me in my work at the library, posting photos make this bridge more real and true.

_Disclosure of deep-seated feelings._ Although bloggers reported disclosing personal information in their blogs, there were certain feelings they were not willing to blog about.

> I have never written about being of Georgian descent and what it means for me to be a part of that community.  
>   
> I have never written about the scar my grandmother's death left in me. In fact, I never write about the subject of death, it is too painful.

Almost all participants (98.7% n=150) said that there were some photographs that they would not post on their profile. Thirty three percent of female participants regretted having posted some photographs on their profile in comparison to 14% of male participants. Participants were asked to describe the kind of photographs they would not post on their profiles and the thematic analysis of the participants' textual responses reveal three reasons for not posting a photograph: photographs that may be a cause for embarrassment, photographs that may represent an intrusion on their personal privacy, and photographs that may represent an intrusion on other people's privacy. The following statements present examples of the participants' textual answers on the three themes:

_Photographs that may be a cause for embarrassment._ Participants were concerned that photographs they perceive as embarrassing would harm the 'possible selves' they wanted to portray.

> There are some photographs that might catch you in a bad moment that is not representative of who you really are. If you post such a photograph people might think you always behave like that.  
>   
> [I would not post] embarrassing photos; like photographs of me drunk.  
>   
> [I would not post] unsuccessful photos of myself in embarrassing situations, like me making faces or behaving inappropriately.

_Photographs that may represent an intrusion on their personal privacy._ Participants believe that there are some photographs that are too personal to be posted on their profile and by posting them they might harm their own privacy.

> [I would not post] photos that are too personal - there are some people from work in my list of friends, and I do not want them to be exposed to some aspects in my private life.  
>   
> [I would not post] photos that are too revealing, like photographs of me wearing a bathing suit.  
>   
> [I would not post] photos that are not flattering, or are improper, nude or intimate photos. In general, there are some photographs that should not be made public.

_Photographs that may represent an intrusion into other people's privacy._ Participants were concerned about harming other people's privacy by posting photographs of them.

> I don't want to post photographs of myself with other people, for example photos with my family or friends, especially if I know they don't use social networks. I feel I don't have the right to post someone else's photographs without their permission.  
>   
> [I would not post] photos that might harm the privacy of others.  
>   
> [I would not post] a family photo. People on Facebook don't need to know about my family, especially about my children.

### Disclosure of personal information

The second research question examined different patterns of self-disclosure such as the disclosure of personal, embarrassing or sensitive information. Table 1 presents various aspects of self-disclosure that participants were asked to rate on a 1-7 Likert scale (1 lowest value to 7 highest value).

<table><caption>Table 1: Characteristics of self-disclosure - percent of total</caption>

<thead>

<tr>

<th>Question</th>

<th>1(-)</th>

<th>2</th>

<th>3</th>

<th>4</th>

<th>5</th>

<th>6</th>

<th>7(+)</th>

</tr>

</thead>

<tbody>

<tr>

<td>How personal is the information you disclose on your profile?</td>

<td>19.7</td>

<td>23.7</td>

<td>25.0</td>

<td>20.4</td>

<td>7.9</td>

<td>3.3</td>

<td>0</td>

</tr>

<tr>

<td>How frequently do you disclose personal information on your profile?</td>

<td>17.8</td>

<td>45.4</td>

<td>15.1</td>

<td>14.5</td>

<td>5.3</td>

<td>2.0</td>

<td>0</td>

</tr>

<tr>

<td>To what extent do you consciously disclose personal information on your profile?</td>

<td>9.2</td>

<td>10.5</td>

<td>9.2</td>

<td>7.9</td>

<td>7.9</td>

<td>23.7</td>

<td>31.6</td>

</tr>

<tr>

<td>To what extent do you disclose embarrassing or frightening information in your profile</td>

<td>53.3</td>

<td>16.4</td>

<td>9.9</td>

<td>8.6</td>

<td>6.6</td>

<td>5.3</td>

<td>0</td>

</tr>

<tr>

<td>To what extent do you let your defenses down on your profile</td>

<td>47.4</td>

<td>22.4</td>

<td>10.5</td>

<td>8.6</td>

<td>5.3</td>

<td>2.6</td>

<td>3.3</td>

</tr>

<tr>

<td>To what extent does your profile reflect who you really are</td>

<td>9.2</td>

<td>10.5</td>

<td>15.1</td>

<td>25</td>

<td>13.2</td>

<td>15.1</td>

<td>11.8</td>

</tr>

</tbody>

</table>

Table 1 shows that the majority of participants (68.4%) responded that the information they disclose on their profile was not highly personal and 78.3% described their disclosure of personal information as infrequent (chose 1-3 on the Likert scale). Yet interestingly, 63.2% of participants reported that when they do disclose personal information they do it consciously (chose 5-7 on the Likert scale). A weak significant Spearman correlation was found between the conscious disclosure of information and the age of participants (rs=.18, p=.023). That is, older participants were slightly more inclined to disclose personal information consciously.

Moreover, findings show that participants' strategies of self-disclosure are related to the socially acceptable and restrained possible self they wanted to present. The majority (79.6%) of participants do not tend to disclose embarrassing information. Similarly, 80.3% of participants responded that they do not let their defences down on their profile. A weak significant Spearman correlation was found between age and the tendency of participants to let their defences down on their profile (rs= .17, p=.035). That is, young participants were more inclined to let their defences down on their profiles. Interestingly, only, 26.9% of participants described their profile as a true reflection of who they are, that is, they felt they presented their now selves. In contrast, the majority (73.1%) of participants admitted that their profile on the social networks does not portrait who they really are but rather a possible self they created.

To further understand self-disclosure patterns a Spearman correlation test was performed to look for correlations between six patterns of self -disclosure.

<table><caption>Table 2: Nonparametric statistically significant correlations between characteristic of self-disclosure</caption>

<tbody>

<tr>

<th colspan="2">z</th>

<th>Frequency of disclosure</th>

<th>Disclosure of embarrassing information</th>

<th>Letting defenses down</th>

<th>Disclosure of true self</th>

</tr>

<tr>

<td rowspan="3">Rate of disclosure of personal information</td>

<td>Correlation coefficient</td>

<td>0.721**</td>

<td>0.614**</td>

<td>0.439**</td>

<td>0.362**</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td>0.000</td>

<td>0.000</td>

<td>0.000</td>

<td>0.000</td>

</tr>

<tr>

<td>N</td>

<td>152</td>

<td>152</td>

<td>152</td>

<td>152</td>

</tr>

<tr>

<td rowspan="3">Frequency of disclosure</td>

<td>Correlation coefficient</td>

<td> </td>

<td>0.593**</td>

<td>0.499**</td>

<td>0.358**</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td> </td>

<td>0.000</td>

<td>0.000</td>

<td>0.000</td>

</tr>

<tr>

<td>N</td>

<td> </td>

<td>152</td>

<td>152</td>

<td>152</td>

</tr>

</tbody>

</table>

Table 2 shows that several significant correlations were found between the different patterns of self-disclosure. A strong significant correlation was found between the rate of disclosure of personal information and the frequency of disclosure. That is, participants who refrain from disclosing highly personal information updated their profile infrequently. Connected to this, a strong significant correlation was found between the rate of disclosure of personal information and the disclosure of embarrassing information, namely, participants who did not disclose highly personal information also refrained from disclosing embarrassing information. A medium significant correlation was found between frequency of disclosure and the pattern letting your defences down. That is, participants who updated their profile more frequently tended to let their defences down. Moreover, participants who tended to let their defences down they also tended to disclose highly embarrassing information.

To gain a deeper understanding of the patterns of self-disclosure on social networks, participants were asked if they have revealed something about themselves on their profiles that they have yet to reveal to someone face-to-face; 90.1% answered negatively. A Mann Whitney test was performed and a significant correlation was found between the disclosure of highly personal information and the tendency to disclose information on the profile that has yet to be disclosed face-to-face (U= 564.5, p=.003). That is, those who disclose pieces of information on their social network profile and not face-to-face reported disclosing information of a more personal nature then those participants who have yet to disclose personal information on their profile without disclosing it face-to-face (M=2.72, sd-1.30, median=3).

When asked if they have ever regretted disclosing personal information on their profile, 27.4% of participants answered affirmatively. A significant Spearman correlation was found between gender and regret of disclosure (?<sup>2</sup> (1) = 5.74, p=0.017, n=151). Similarly to the data related to the disclosure of photographs, 33% of female participants regretted disclosing personal information in comparison to only 14% of male participants. A Spearman correlation was performed between age and regret of disclosure of personal information and a weak significant correlation was found (rs=0.20, p=0.016). Namely, the younger the participants were the more they regretted disclosing personal information on their profiles. This might be the result of young people disclosing personal information more frequently.

Participants were asked to describe in their own words when they have regretted disclosing personal information. The thematic analysis of the participants' textual responses revealed two themes: the disclosure of information perceived as too personal and disclosure of information that was misinterpreted. The following statements present examples of the participants' statements:

_Disclosure of information perceived as too personal._ Participants regretted, disclosing information that was perceived retrospectively as too personal.

> I regret disclosing personal information such as feelings, disappointments and expectations. Also, I was too quick to update my status and people got hurt.  
>   
> I regretted posting a particular photo...because it was apparently too personal and some of my friends reacted very strongly to it.  
>   
> I regretted disclosing my age and my occupation.

_Disclosure of information that was misinterpreted._ Participant regretted disclosing information that was misunderstood by their friends on the social networking site.

> Once I wrote something that was supposed to be funny but people did not get it and some of them got hurt.  
>   
> I regret some of the comments I wrote to my friends' posts about issues such as animals, religion and religious leaders because some people misunderstood what I wanted to say.

_The impact of self-presentation and self-disclosure on online social connections._The third research question asked participants about the possible impact that their profiles might have on their personal relationships. Only 37.2% of participants reported that their profile have some influence on their online social connections. Participants that answered affirmatively were asked to describe this impact in their own words. The following statements present examples of the participants' textual answers on the impact that their online social connections have on their self-disclosure patterns.

> Through my membership in the social network I have been able to renew my relationship with people that I haven't been in touch for a while.  
> My profile helps me maintain a relationship with distant family, to strengthen my relationship with co-workers; it helps me to build relationships in general.  
> [Through my profile] I can see new sides of the people I know.

Participants were asked about the number of friends they have through their profiles and how many of those friends they knew personally.The mean number of friends reported was, 208.05 but the mean number of friends participants known personally was 158.44\. A Spearman correlation test was performed and a medium negative correlation was found between age and the number of friends participants know personally (rs=-0.47, p<0.001; rs=-0.43, p<0.001), i.e., the younger respondents have more friends on their social network site.

_Level of exposure._ The fourth research question investigated to what extent participants were aware of the exposure of the information disclosed on their profiles. In order to understand this issue, participants were asked about the privacy settings of their profile. The majority of participants (73.0%) reported that only their friends could access their profile, 15.8% reported that they allowed access to friends of their friends and only 11.2 % allowed their profile to be open to all. However, there appears to be a basic misunderstanding amongst participants about the meaning of these privacy settings because when they were asked if friends of their friends had access to their profile, 39.5% answered affirmatively, 48.0% answered negatively and 12.5% answered that they did not know. Nevertheless, almost all participants (90.1%) were concerned about the exposure of the personal information disclosed on their profile and 67.1 % of participants changed their passwords since opening their account in order to preserve their privacy. This finding parallels those of Joinson's ([2008](#joi08)) study that found that the majority of users claimed to have changed their privacy settings in Facebook. Participants' awareness of their profile's level of exposure could be influenced by the fact that 74.3% reported being contacted by a stranger through the social network. A Spearman correlation was performed between age and being contacted by a stranger on their profiles (rs=.28, p=.001). That is, young participants were contacted by strangers somewhat more frequently.

## Discussion

The present study investigated the information disclosure behaviour of participants on their social network site profiles with the purpose of creating alternative identities that Markus and Nurius ([1986](#mar86)) named "possible selves". These possible selves represent specific individually significant hopes, fears and fantasies that compose self-conceptions that individuals have about the person they think they are and about the image they want to portray to their online social connections. The first research question examines the participants' self-presentation patterns and found that the majority of participants identified themselves using their real name and posted personal photographs on their profile. Echoing the findings in Ellison _et al._ ([2006](#ell06)), we would like to suggest that this lack of anonymity affected participants' self-presentation, limiting the possible selves they could portray but allowing them to manage their online interactions more strategically.

According to Goffman ([1959](#gof59)) this impression management behaviour consists of expressions given (i.e., traditional forms of communication such as spoken communication) and expressions given off (i.e., nonverbal forms of communications such as pictures). Strano ([2008](#str08)) posited that photographs are a good example of expressions given off since they present an idealized image of socially accepted norms about desirable personal characteristics. The majority of participants posted personal photographs on their profile and described self-presentation as one of the motivations for posting them. The additional motivations reported in the participants' textual answers are similar to the motivations that bloggers have for writing personal blogs, such as keeping in touch with family and friends by sharing information and by receiving feedback and comments ([Bronstein, 2012](#bro12); [Mosel, 2005](#mos05); [Nardi, Schiano, Gumbrecht and Swartz, 2004](#nar04); [Walker 2000](#wal00)). In addition, participants explained in their textual answers that they used their Facebook profile as a way to document and safeguard the photographs that represented important aspects of their personal lives and their life experiences. This finding echoes results from Li's ([2007, p. 130](#li07)) study about blogs that noted that '_blogs function as a notebook, a tape recorder and a bookmark collection_'.

Participants identified photographs as important information elements representing the possible self they wanted to portray by allowing them to repress or to modify the negative or the less desirable elements of their identity they wanted to conceal. This could explain why the majority of participants said they would not post a photograph that is too personal or one that might embarrass them or other people. However, the data analysis revealed some gender differences regarding the disclosure of photographs. Findings show that female participants are especially concerned with the image photographs on their profile convey, since a third of them regretted having posted some photographs that apparently did not conform to the possible self they wanted to portray. This finding echoes Strano's ([2008](#str08)) claim that women are more likely to choose a profile image that makes them look attractive and Siibak's ([2007](#sii07)) finding that described how women using Internet dating sites are focused on displaying an idealized image of female beauty.

The second research question investigated the patterns of self-disclosure used by participants to create possible selves on their profiles. This question relates to what Goffman ([1959](#gof59)) called _expressions given_, which refers to the textual information disclosed by participants on their profiles. Findings show that the majority of participants wanted to create a positive or socially acceptable possible self on their profiles and therefore refrained from disclosing information that was highly personal or embarrassing, did not let their defenses down, and if they did disclose personal information they did it in a controlled manner. This finding parallels Nosko _et al._'s ([2010](#nos10)) study which found that users experienced discretion regarding personally revealing information, but differs from Young's ([2005](#you05)), Acquisti and Gross's ([2006](#acq06)) and Bronstein's ([2012](#bro12)) studies on blogging, which found that, despite privacy concerns, individuals disclose a great deal of personal information in online communication. The difference in the findings of these studies could lie in the expectation of offline encounters. In other words, unlike bloggers who might never meet their audiences face-to-face, social networking site users could anticipate future face-to-face interactions that may diminish participants' sense of visual anonymity, an important variable in many online self-disclosure studies ([Ellison _et al._ 2006](#ell06)).

As a result of these restricted self-disclosure patterns, less than a third of the participants said that their profile was a reflection of their true self. Hence, this finding could point to the fact that the majority of participants did not portray their true or 'now self' but rather they constructed alternative identities or possible selves that conform to the self-image they wanted to convey on their profiles. Brandtzaeg _et al._'s ([2010, p. 1023](#bra10)) study presented a similar finding and claimed that 'people on Facebook share only a part of themselves, without becoming too private or personal'. Hence, this selective information disclosure behaviour might be the result of 'increasing social pressure towards conformity' ([Brandtzaeg _et al._, 2010, p. 1023](#bra10)). When correlating age and self-disclosure patterns, this study found that younger participants were less restrictive about their self-disclosure and revealed personal and embarrassing information more frequently than older participants. These findings resemble those in Nosko _et al._'s ([2010](#nos10)) study which found that for younger participants disclosing personal information is part of everyday life and they feel more comfortable with online disclosure.

The third research question asks about the impact that information disclosure behaviour has on the participants' online social connections. These online social connections usually include people the user knows both offline and online which results in the need to coordinate their behaviour in both realms ([Clark, 1998](#cla98)). This is why by keeping private highly sensitive personal information, participants are not reinventing themselves but rather creating an online possible self that might help them bypass those '_gating features_' (i.e., stigmatized appearance, stuttering, shyness etc.) that they are unable to overcome in the offline world. Zhao _et al_. ([2008](#zha08)) posit that Facebook can help users enhance their overall self image and identity claims that might have a positive impact on their social connections in the offline world. A number of studies (e.g., [Christofides _et al_., 2009](#chr09); [Kim and Lee, 2011](#kim11); [Zhao _et al._, 2008](#zha08)) have suggested that feedback from anchored relationships concerning information posted about oneself on the Internet might have an impact on that person's identity. That is, identity is not an individual characteristic nor is it an innate expression but rather it is a social product created by the information disclosed and by the feedback from social connections. Thus, if identity is a social product then feedback from others would be an important part of the identity creation process. However, only a third of participants said that their profile has an impact on their social environment. We suggest that this is because participants do not create their possible selves solely in relation to their social environment, either to impress their social connections or as a reaction to their feedback. That is, the identity creation process becomes a reflective one in which the computer acts as a mirror in that it reflects back to participants leading them to private self-focus ([Joinson, 2001](#joi01)). This assertion supports Markus and Nurius's ([1986](#mar86)) claim that an individual's self-perception is composed by not only the present capacities and states but also by what they hope to become, what they plan to do, what they fear, and so on. It is our belief that participants constructed possible selves as a way to enhance their self-concept and their self-esteem in relation to others ([Sirgy, 1982](#sir82)). Another finding supports the reflective interpretation of the self-disclosure patterns. Almost all of the participants reported that they have not revealed details about themselves on their profiles that they have yet to reveal to someone face-to-face. This finding differs from Tidwell and Walther's ([2002](#tid02)) observation that online interactions generate more self-disclosures and foster deeper personal questions than face-to-face conversations, and from Bronstein's ([2012](#bro12)) study on personal blogs in which bloggers admitted disclosing intimate information on their blogs that they would not disclose to their offline connections.

Finally the study wanted to understand to what extent the participants were aware of the degree of exposure of the information disclosed on their profile and the possible influence this exposure might have on their self-presentation and self-disclosure patterns. Findings show that in general, participants felt that the degree of exposure of the information disclosed on their profiles was rather high. When asked about the privacy settings of their profile the majority of participants (73%) responded that only their friends can access their profile. This finding concurs with Debatin _et al._'s ([2009, p. 93](#deb09)) study in which the majority of respondents changed their default privacy setting to restrict access to their profile so that '_only friends can see it_'. However, some confusion on the meaning of privacy settings on social networks was revealed in this study: when asked if friends of their friends can access their profile almost 40% of participants answered affirmatively and 12.5% answered that they did not know. This finding could explain why almost all the participants were concerned about the exposure of the information disclosed on their profiles and the majority had changed their passwords since opening their accounts in order to preserve their privacy. This perception of insecurity was probably exacerbated by the fact that a large number of participants have been contacted by someone they did not know through their profile. This finding coincides with Debatin _et al._'s ([2009](#deb09)) claim that Facebook users were more likely to change their privacy setting if they experienced a personal invasion of their privacy. Furthermore, Gross and Acquisti ([2005](#gro05)) asserted that disclosure of information on large social networks presents a new form of intimacy that users have to get used to. Self-disclosure may be considered conducive for intimacy since intimacy resides in selectively disclosing information to certain individuals but not to others. We posit that participants found it difficult to get used to this new kind of intimacy which might be perceived as somewhat intrusive and intimidating and did not considered that the perceived benefit of disclosing personal information to strangers outweighed the perceived costs of possible privacy invasions. Consequently, their self-presentation and self-disclosure patterns were relatively restrictive and they only portrayed the possible self they felt comfortable with under this new kind of online privacy.

As in any other research based on self-reported behaviour, the perceptions people have of their own behaviour may differ from their actual behaviour, and therefore accuracy is difficult to verify. Unlike other studies where researchers accessed participants' social networking site profiles or conducted content analysis of the blogs, no external validation was conducted for this study. However, we believe that ensuring the anonymity of the participants enabled them to answer the survey in a more honest and open manner. An additional limitation is the type of population of the study that included only library and information science students who are probably more aware of some of the issues related to the use of social networks.

## Conclusions

According to Goffman ([1959, p. 28](#gof59)), what defines how an individual presents himself is not '_his show for the benefit of other people_' but rather '_the individual's own belief in the impression of reality that he attempts to engender in those among whom he finds himself_'. Findings from this study support this assertion and show how participants created alternative identities or possible selves by managing their self-presentation and their self-disclosure on their social networking site profiles. However, information disclosure patterns revealed in this study also suggest that identity construction could have a personal motive beyond the basic purpose of online interaction and communication. Namely, for participants in this study identity creation on a social networking site is a reflective process that may be motivated by the need for self-enhancement that would result in a possible self that communicates the best part of themselves to others, supporting Goffman's ([1959](#gof59)) assertion that individuals need to present themselves as an acceptable person to others. Furthermore, findings also show that the process of identity creation is dynamic, since participants reflected on the information disclosed resulting in some of them regretting a particular disclosure and changing the information on their profile. As Markus and Nurius ([1986](#mar86)) observed, given that possible selves are not well anchored in social experience they are most sensitive to situations that communicate new or inconsistent information about the self-conveyed. In sum, findings show that these profiles represent a way to create a desirable or socially acceptable online identity that conveys the way users would like to perceive themselves and to be perceived by others.

Further research is needed on the area of information disclosure behaviour in nonymous settings to investigate how different populations of users act in online settings where they can be identified. In addition, it will be interesting to compare the ways in which information behaviour of the same user vary in different settings and to understand the users' motivations for these behavioural changes.

## <a id="author"></a>About the author

Jenny Bronstein is a lecturer at the Information Science department at Bar-Ilan University. Her research interests are in library and information science education and professional development, self presentation and self disclosure on different social platforms and information seeking behavior and she has published in refereed information science journals. She teaches courses in information retrieval techniques, information behaviour, academic libraries and business information. In the past, Dr. Bronstein served as an academic librarian and an information professional in corporate information centers. She holds a PhD and a M.S. in Information Science from Bar-Ilan University and a B.A. in History and Linguistics from Tel-Aviv University She can be contacted at: [jenny.bronstein@biu.ac.il](mailto:jenny.bronstein@biu.ac.il)

## Note

This paper is based on a presentation at the Information: Interactions and Impact (i<sup>3</sup>) Conference. This biennial international conference is organised by the Department of Information Management and Research Institute for Management, Governance and Society (IMaGeS) at Robert Gordon University. i<sup>3</sup> 2013 was held at Robert Gordon University, Aberdeen, UK on 25-28 June 2013\. Further details are available at [http://www.i3conference.org.uk/](http://www.i3conference.org.uk/)

</section>

<section>

## References

<ul>
<li id="acq06">Acquisti, A. &amp; Gross, R. (2006). Imagined communities: awareness, information sharing, and privacy on the Facebook. In Danezi, G. &amp; Golle, P. (eds,) <em>Proceedings of the 6th Workshop on Privacy Enhancing Technologies,</em> (pp. 36-58). Cambridge: Robinson College.
</li>
<li id="and00">Anderson, R. (2007). <em><a href="http://www.webcitation.org/6KVdoUFDH">Thematic content analysis (TCA): descriptive presentation of qualitative data.</a></em> Retrieved from http://www.wellknowingconsulting.org/publications/pdfs/ThematicContentAnalysis.pdf (Archived by Webcite® http://www.webcitation.org/6KVdoUFDH).
</li>
<li id="arc80">Archer, J.L. (1980). To reveal or not to reveal: a theoretical model of anonymous communication. <em>Communication Theory, 8</em>(4), 381-401.
</li>
<li id="bar02">Bargh, J.A., McKenna, K.Y. &amp; Fitsimons, G.M. (2002). Can you see the real me? Activation and expression of the "True Self" on the Internet. <em>Annual Review of Information Science and Technology, 58</em>, 33-48.
</li>
<li id="bar00">Baron, J. &amp; Siepmann, M. (2000). Techniques for creating and using online questionnaires in research and testing. In M. H. Birnbaum, (Ed.), <em>Psychological experiments on the Internet,</em> (pp., 235-265). San Diego, CA: Academic Press.
</li>
<li id="bor05">Bortree, D.S. (2005). Presentation of self on the Web: an ethnographic study of teenage girls' Weblogs. <em>Education, Communication &amp; Information, 5</em>(1), 25-39.
</li>
<li id="boy98">Boyatzis, R. (1998). <em>Transforming qualitative information: thematic analysis and code development.</em> Thousand Oaks, CA: Sage Publications.
</li>
<li id="bra10">Brandtzaeg, P.B., Luders, M. &amp; Sjetke, J.H. (2010). Too many Facebook 'friends'? Content sharing and sociability versus the need for privacy in social network sites. <em>Journal of Human-Computer Interaction, 26</em>(11), 1006-1030.
</li>
<li id="boy07">Boyd, D.M. &amp; Ellison, N.B. (2007). Social network sites: definition, history, and scholarship. <em>IEEE Engineering Management Review, 38</em>(3), 16-31
</li>
<li id="bro12">Bronstein, J. (2012). Blogging motivations for Latin American bloggers: a uses and gratifications approach. In T. Dumova, (Ed.), <em>Blogging in the global society,</em> (pp., 200-215). Hershey, PA: Information Science Reference.
</li>
<li id="che10">Chesney, T. &amp; Su, D. K. (2010). The impact of anonymity on weblog credibility. <em>International Journal of Human-Computer Studies, 68</em>(10), 710-718.
</li>
<li id="chr09">Christofides, E., Muise, A. &amp; Desmarais, S. (2009). Information disclosure and control on Facebook: are they two sides of the same coin or two different process? <em>CyberPsychology &amp; Behavior, 12</em>(3), 341-345.
</li>
<li id="cla98">Clark, L.S. (1998). Dating on the net: teens and the rise of 'pure relationships'. In S.G. Jones (Ed.), <em>Cyberspace, 2.0: revisiting computer-mediated communication and community,</em> (pp. 159-183). Thousand Oaks, CA: Sage.
</li>
<li id="deb09">Debatin, B., Lovejoy, J.P., Horn, A.K. &amp; Hughes, B.N. (2009). Facebook and online privacy: attitudes, behaviors, and unintended consequences. <em>Journal of Computer-Mediated Communication, 15</em>(1), 83-108.
</li>
<li id="don04">Donath, J. &amp; Boyd, D. (2004). Public displays of connections. <em>BT Technology Journal, 22</em>(4), 71-82.
</li>
<li id="ell06">Ellison, N.H., Heino, R. &amp; Gibbs, J. (2006). Managing impressions online: self-presentation processes in the online dating environment. <em>Journal of Computer-Mediated Communication, 11</em>(2), article, 2.
</li>
<li id="gof59">Goffman, E. (1959). <em>Presentation of self in everyday life.</em> Garden City, NY: Anchor.
</li>
<li id="gre06">Greene, K., Derlega, V. L. &amp; Mathews, A. (2006). Self-disclosure in personal relationships. In A. Vangelisti &amp; D. Perlman (Eds.), <em>Cambridge handbook of personal relationships.</em> Cambridge: Cambridge University Press.
</li>
<li id="gro05">Gross, R. &amp; Acquisti, A. (2005). Information revelation and privacy in online social networks. In Atluri, V., De Capitani di Vimercati, S. &amp; Dingledine R. (Eds.)<em>WPES: ACM Workshop on Privacy in Electronic Society,</em> 7-9 November, Alexandria, Virginia (pp. 71-80). New York: ACM Press.
</li>
<li id="gun02">Gunn, H. (2002). <a href="http://www.webcitation.org/6KPPxkMDg">Surveys: changing the survey process.</a> <em>First Monday, 7</em>(12). Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/1014/935 (Archived by Webcite® at http://www.webcitation.org/6KPPxkMDg).
</li>
<li id="joi01">Joinson, A. (2001). Self-disclosure in computer-mediated communication: the role of self-awareness and visual anonymity. <em>European Journal of Social Psychology, 31</em>(2), 177-192.
</li>
<li id="joi08">Joinson, A. (2008). 'Looking at', 'Looking up or 'Keeping up with' people? In Czerwinski, M., Lund, A. &amp; Tan, D. (eds.) <em>26th annual SIGCHI conference on Human Factors in Computing Systems,</em> 5-10 April, Florence, Italy (pp. 1027-1036). New York: ACM Press.
</li>
<li id="kim11">Kim, J. &amp; Lee, J.E. (2011). The Facebook paths to happiness: effects of the number of Facebook friends and self-presentation on subjective well-being. <em>CyberPsychology, Behavior and Social Networking, 14</em>(6), 359-364.
</li>
<li id="li07">Li, D. (2007). <a href="http://www.webcitation.org/6KPTJDGKY">Why do you blog: a uses-and-gratifications inquiry into bloggers' motivations. Master's thesis.</a> Unpublished Master's thesis, Marquette University, Milwaukee, Wisconsin. Retrieved from http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.91.6790&amp;rep=rep1&amp;type=pdf (Archived by Webcite® at http://www.webcitation.org/6KPTJDGKY).
</li>
<li id="mar86">Markus, H. &amp; Nurius, P. (1986). Possible selves. <em>American Psychologist, 41</em>(9), 854-969.
</li>
<li id="mar99">Marx, G. T. (1999). What's in a name? Some reflections on the sociology of anonymity. <em>The Information Society, 15</em>(2), 99-112.
</li>
<li id="mck00">McKenna, K.Y.A. &amp; Bargh, J.A. (2000). Plan 9 from Cyberspace: the implications of the Internet from personality and social psychology. <em>Personality and Social Psychology Review, 4</em>(1), 57-75.
</li>
<li id="mos05">Mosel, S. (2005). <a href="http://www.webcitation.org/6KPS43U99"></a><em>Self directed learning with personal publishing and microcontent.</em> Paper presented at the Microcontent Conference, Innsbruck, Austria. Retrieved from http://www.microlearning.org/micropapers/MLproc_2005_mosel.pdf (Archived by Webcite® at http://www.webcitation.org/6KPS43U99)
</li>
<li id="nar04">Nardi, B.A., Schiano, D.J., Gumbrecht, M. &amp; Swartz, L. (2004). Why we blog. <em>Communications of the ACM, 47</em>(12), 41-46.
</li>
<li id="nos10">Nosko, A.W., Wood, E. &amp; Molema, S. (2010). All about me: disclosure in online social networking profiles. The case of Facebook. <em>Computers in Human Behavior, 26</em>(3), 406-418.
</li>
<li id="pap02">Papacharissi, Z. (2002). The presentation of self in virtual life: characteristics of personal home pages. <em>Journalism and Mass Communication, 79</em>(3), 643-660.
</li>
<li id="pew112">Pew Internet Research. (2011). <em><a href="http://www.webcitation.org/6KVemMK4j">Social media usage.</a></em> Washington, DC: Pew Research Center. Retrieved from http://www.pewinternet.org/Reports/2011/Social-Networking-Sites.aspx (Archived by Webcite® at http://www.webcitation.org/6KVemMK4j)
</li>
<li id="qia07">Qian, H. &amp; Scott, C.R. (2007). <a href="http://www.webcitation.org/6KPSnnWhz">Anonimity and self-disclosure on Weblogs.</a> <em>Journal of Computer-Mediated Communication, 12</em>(4), 1428-1451. Retrieved from http://jcmc.indiana.edu/vol12/issue4/qian.html (Archived by Webcite® at http://www.webcitation.org/6KPSnnWhz)
</li>
<li id="rhe95">Rheingold, H. (1995). <em>The virtual community: finding connection in a computerized world.</em> London: Secker &amp; Wargung.
</li>
<li id="sch03">Schau, H. J. &amp; Gilly, M.C. (2003). We are what we post? Self-presentation in personal Web space. <em>Journal of Consumer Research, 30</em>(3), 385-404.
</li>
<li id="sch09">Schrammel, J., Kiffel, C. &amp; Tscheligi, M. (2009). How much do you tell?: information disclosure behaviour indifferent types of online communities. In Carrol, J.M. (ed.) <em>Proceedings of the fourth international conference on Communities and technologies</em>, (pp., 275-284). New York: ACM Press.
</li>
<li id="sii07">Siibak, A. (2007). <a href="http://www.webcitation.org/6KVevGRkv">Reflections of RL in the virtual world.</a> <em>. Cyberpsychology: Journal of Psychosocial Research on Cyberspace, 1</em>(1), article 7. Retrieved from http://cyberpsychology.eu/view.php?cisloclanku=2007072301&amp;article=(search in Issues) (Archived by Webcite® at http://www.webcitation.org/6KVevGRkv)
</li>
<li id="sir82">Sirgy, M. (1982). Self-concept in consumer behavior: a critical review. <em>Journal of Consumer Research, 9</em>(4), 287-300.
</li>
<li id="str08">Strano, M. (2008). <a href="http://www.webcitation.org/6KVf2Tylt">User descriptions and interpretations of self-presentation through Facebook profile images.</a> <em>Cyberpsychology: Journal of Psychosocial Research on Cyberspace, 2</em>(2), article 5. Retrieved from http://cyberpsychology.eu/view.php?cisloclanku=2008110402 (Archived by Webcite® at http://www.webcitation.org/6KVf2Tylt).
</li>
<li id="tid02">Tidwell, L. &amp; Walther, J.B. (2002). Computer-mediated communication effects on disclosure, impressions, an interpersonal evaluation: getting to know one another a bit a time. <em>Human Communication Research, 13</em>(4), 531-549.
</li>
<li id="tra06">Trammell, K., Tarkowski, A., Hofmokl, J. &amp; Sapp, A. (2006). Rzeczpospolita blogów [Republic of Blog]: examining polish bloggers through content analysis. <em>Journal of Computer-Mediated Communication, 11</em>(13), 702-722.
</li>
<li id="tur95">Turkle, S. (1995). <em>Life on the screen: identity in the age of the Internet.</em> New York, NY: Simon &amp; Schuster.
</li>
<li id="van06">Vanden Boogart, M.R. (2006). <em><a href="http://www.webcitation.org/6KVfBR67L">Uncovering the social impacts of Facebook on campus.</a></em> Unpublished master's thesis, Kansas State University, Manhattan, Kansas. Retrieved from http://krex.k-state.edu/dspace/bitstream/handle/2097/181/MatthewVandenBoogart2006.pdf?sequence=1. (Archived by Webcite® at http://www.webcitation.org/6KVfBR67L).
</li>
<li id="vie05">Viegas, F.B. (2005). <a href="http://www.webcitation.org/6KPTxv7ux">Bloggers' expectations of privacy and accountability: an initial survey.</a> <em>Journal of Computer-Mediated Communication, 10</em>(3), article 12. Retrieved from http://jcmc.indiana.edu/vol10/issue3/viegas.html (Archived by Webcite® http://www.webcitation.org/6KPTxv7ux).
</li>
<li id="wal00">Walker, K. (2000). "It's difficult to hide it": the presentation of self on Internet home pages. <em>Qualitative Sociology, 23</em>(1), 99-120.
</li>
<li id="whi08">Whitty, M. T. (2008). Revealing the 'real' me, searching for the 'actual' you: presentations of self on an Internet dating site. <em>Computers in Human Behavior, 24</em>(4), 1707-1723.
</li>
<li id="you05">Young, S. (2005). Teenagers' perceptions of online privacy and coping behaviour: a risk-benefit appraisal approach. <em>Journal of Broadcasting &amp; Electronic Media, 49</em>(1), 86-110.
</li>
<li id="yur05">Yurchisin, J., Watchravesringkan, K. &amp; McCabe, D.B. (2005). An exploration of identity re-creation in the context of internet dating. <em>Social Behavior and Personality: an International Journal, 33</em>(8), 733-750.
</li>
<li id="zha08">Zhao, S., Grasmuck, S. &amp; Martin, J. (2008). Identity construction on Facebook: digital empowerment in anchored relationships. <em>Computers in Human Behavior, 24</em>(5), 1816-1836.
</li>
<li id="zwe11">Zweir, S., Araujo, T., Boukes, M. &amp; Willemsen, L. (2011). Boundaries to the articulation of possible selves through social networks: the case of Facebook profilers' social connectedness. <em>CyberPsychology, Behavior, and Social Networking, 14</em>(10), 571-576.
</li>
</ul>

</section>

</article>