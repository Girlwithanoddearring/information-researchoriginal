#### Vol. 9 No. 4, July 2004

# Web links as analogues of citations

#### [Alastair G. Smith](mailto:Alastair.Smith@vuw.ac.nz)  
School of Information Management  
Victoria University of Wellington, New Zealand

#### **Abstract**

> This exploratory study investigates the extent to which Web links are analogues to the citations in traditional print literature. A classification of Web links is developed, using the nature of the source and target pages, and the reasons for linking. Links to a sample of research oriented Websites (universities, professional institutes, research institutes, electronic journals, and individual researchers) were classified. Overall, 20% of the Web links in the study could be regarded as research links analagous to citations.

## Introduction

Citations in conventional print publications have traditionally been used as indicators of links between researchers, and it is tempting to regard Web links as analogues of citations. To what extent do hypertext links between Websites indicate research links? This exploratory study examines the nature of Web links made to research oriented Websites, to determine the extent to which the Web links are made for research-related reasons and are, therefore, analagous to citations.

One obvious difference is the nature of the documents that are linked. Citations in conventional print publications are generally between research publications, while Web links may be between a wide variety of publication types: personal home pages, subject resource guides, etc.

Why are citations and Web links made? Bibliometric literature contains many studies that examine the motivation behind citing. Egghe and Rousseau ([1990](#egghe)) review a number of studies of citation motivation. Garfield ([1964](#garfield)) identifies reasons that include paying homage, identifying methodology, providing background, correcting, criticising, substantiating or authenticating, alerting to forthcoming or poorly disseminated work, identifying original concepts, and disclaiming or disputing previous work. Kelland and Young ([1998](#kelland)) list several motivations, including acknowledging prior work, identifying methodology, providing background reading, correcting or criticising, substantiating claims, alerting readers to forthcoming work, authenticating data, identifying original publication of a term or concept, disclaiming work of others, and disputing priority claims. Case and Higgins ([2000](#case)) found that citations might be made because the cited work (1) was a 'concept-marker', (2) promoted the authority of the citing work or (3) deserved criticism. Oppenheim and Smith ([2001](#oppenheim)) studied citations in students' dissertations and found that while their motivations for citing were similar to academics, there was a trend towards citing Internet sources.

Studies of linking motivation on the Web, on the other hand, are more recent, and indicate that Web links are made for different reasons. Kim ([2000](#kim)) studied links from Web based scholarly publications and found a wide range of motivations for linking, some echoing citations, but also including: publicity, credit to an institution, providing an immediate access mechanism, to provide a graphical image, and an editorial policy of encouraging hyperlinks. Thelwall ([2003](#thelwall)) studied 100 Web links between university home pages, and classified them into 'ownership', 'social', 'general navigational' and 'gratuitous'; arguing that the majority of Web linking motivations were trivial compared with citing motivation. Chu ([2003](#chu)) analysed links to academic Websites, and found that 50% of links were to resource or directory information, while only 27% were motivated by research or teaching/learning. Because of these wider reasons for Web linking, and the more eclectic range of documents that Web links are made between, motivations are different for Web links and citations. Wilkinson _et al._ ([2003](#wilkinson)) found that less than 1% of links to university department Websites were formal research citations. Vaughan and Shaw ([2003](#vaughan)) studied bibliographic and Web citations to articles in LIS journals and found that many Web citations represented intellectual impact and that for most journals there was a correlation between Web citations and the Journal Impact Factor.

The increasing use of Web links in Webmetric research means that the nature of these links, and the extent to which they serve the same function as citations, is important. The current study undertook to classify links made to a sample of research oriented Websites, to test a trial classification and to estimate the extent to which the Web links were analagous to citations.

## Methodology

Fifteen research-oriented sites were studied, taking three examples of each of five types of sites. These were:

*   University Websites ([Victoria University of Wellington](http://www.vuw.ac.nz) , [Australian National University](http://www.anu.edu.au), and [MIT](http://www.mit.edu))
*   Professional institute Websites ([The Royal Society of New Zealand](http://www.rsnz.govt.nz/), [Institute of Professional Engineers of New Zealand](http://www.ipenz.org.nz/ipenz/), and the [New Zealand Law Society](http://www.nz-lawsoc.org.nz/) )
*   Research institute Websites ([National Institute of Water and Atmospheric Research](http://www.niwa.cri.nz/), [Institute of Veterinary, Animal and Biomedical Sciences](http://www.ivabs.massey.ac.nz), and the [New Zealand Institute for Economic Research](http://www.nzier.org.nz/))
*   Electronic Journals (_[British Medical Journal](http://www.bmj.com) _, _[Journal of Internet Law and Technology](http://elj.warwick.ac.uk/jilt/) _, and _[AtP (AntePodium)](http://www.vuw.ac.nz/atp/)_)
*   Individual researchers' Web pages (an NZ [artificial intelligence researcher](http://www.mcs.vuw.ac.nz/~pondy/), a US [library and information management researcher](http://web.simmons.edu/~schwartz/), and a US [mathematical statistician](http://www-stat.stanford.edu/~donoho/))

These sites were chosen as being representative of a range of ways in which research appears on the Web, and representing both academic and professional research. This approach to selection was appropriate for an exploratory study testing the classification and methodology.

Links to each of the target sites were determined by using the AltaVista command in advanced mode ([http://altavista.com/Web/adv](http://altavista.com/Web/adv)):

<pre>link:xxx and not host:xxx</pre>

where xxx is the target domain. This excluded Web links made within the site itself. Site collapse was off, and the search was set to be world wide, for documents in English (to avoid having to classify sites in languages the researcher was not familiar with). Every twentieth item on the list was examined, up to a total of ten linking sites, except where fewer than 200 linking sites were found, when every tenth site was examined. If a site or link was no longer valid, the next link on the results list was chosen. 150 links were studied in total. Searches were carried out in January 2003\. As this was an exploratory study, classification was carried out by the researcher without independent verification of the classifications.

Links were classified on three aspects:

*   Nature of source page, i.e., the page containing the link.
*   Nature of target page, i.e., the page linked to.
*   Reason for linking.

The classifications were derived from similar studies (for example Thelwall, [2003](#thelwall); Chu, [2003](#chu)) but were oriented to identifying features that would indicate the research nature of the link. Table 1 shows the classifications used for the Source and Target pages.

<table><caption>

**Table 1: Page types**</caption>

<tbody>

<tr>

<th>Code</th>

<th>Type of page</th>

</tr>

<tr>

<td>1</td>

<td>General information resource, not otherwise classified</td>

</tr>

<tr>

<td>2</td>

<td>Research information</td>

</tr>

<tr>

<td>3</td>

<td>Teaching resource</td>

</tr>

<tr>

<td>4</td>

<td>Administrative information</td>

</tr>

<tr>

<td>5</td>

<td>Student assignment</td>

</tr>

<tr>

<td>6</td>

<td>Links list</td>

</tr>

<tr>

<td>6.1</td>

<td>

_Bibliography/publication list_</td>

</tr>

<tr>

<td>6.2</td>

<td>

_Directory/ subject guide_</td>

</tr>

<tr>

<td>6.3</td>

<td>

_Related/useful links_</td>

</tr>

<tr>

<td>6.4</td>

<td>

_Events list_</td>

</tr>

<tr>

<td>7</td>

<td>Discussion list archive, Blog</td>

</tr>

<tr>

<td>8</td>

<td>Formal publication</td>

</tr>

<tr>

<td>8.1</td>

<td>

_Technical paper, report_</td>

</tr>

<tr>

<td>8.2</td>

<td>

_E-journal article_</td>

</tr>

<tr>

<td>8.3</td>

<td>

_Conference paper_</td>

</tr>

<tr>

<td>8.4</td>

<td>

_News item_</td>

</tr>

<tr>

<td>8.5</td>

<td>

_E-journal_</td>

</tr>

<tr>

<td>8.6</td>

<td>

_Conference_</td>

</tr>

<tr>

<td>8.7</td>

<td>

_News source_</td>

</tr>

<tr>

<td>9</td>

<td>Personal Web page</td>

</tr>

<tr>

<td>10</td>

<td>Main page of organisation</td>

</tr>

<tr>

<td>10.1</td>

<td>

_Hosted or subsidiary organisation_</td>

</tr>

<tr>

<td>11</td>

<td>Software source</td>

</tr>

<tr>

<td>12</td>

<td>'About' page</td>

</tr>

</tbody>

</table>

The first classes (1-5) are for general Web pages that act as information resources in research, teaching, and administration. Student Websites, created for assignments, are a significant group, and were given their own classification. A specifically Web phenomenon is the directory or links page (6) and these were subdivided into different types: a bibliography or publication list (generally referring to print publications), directory/subject guides (generally having some organised structure), related/useful links (generally an unstructured list that formed part of an individuals home page), or a list of events (usually including links to the events in question). Class 7 reflects the role of communication on the Web, with pages that are discussion lists, or Weblogs.

Class 8 was for Web pages that were formal publications analagous to print publications: e-journals, online conference papers, etc. This class distinguished between individual items (8.1-8.4) and home pages for the publications. The remaining pages were for uniquely Web documents: the personal Web page, the main page of an organisation (a subclass was created for subsidiary or hosted organisations), software sources (a common feature of pages in computer science, where software may be the 'output' of a research project), and the 'about' page, giving background information about a site – ownership, sponsorship, etc.

Reasons for linking were classified according to the schedule in Table 2:

<table><caption>

**Table 2: Reasons for linking**</caption>

<tbody>

<tr>

<th>Code</th>

<th>Reason for link</th>

</tr>

<tr>

<td>1</td>

<td>General information link</td>

</tr>

<tr>

<td>1.1</td>

<td>

_Teaching/learning_</td>

</tr>

<tr>

<td>1.2</td>

<td>

_Administrative_</td>

</tr>

<tr>

<td>1.3</td>

<td>

_Research funding_</td>

</tr>

<tr>

<td>1.4</td>

<td>

_Dissemination of Research_</td>

</tr>

<tr>

<td>1.5</td>

<td>

_Employment_</td>

</tr>

<tr>

<td>2</td>

<td>Formal research citation</td>

</tr>

<tr>

<td>4</td>

<td>Sponsor/acknowledgement of support</td>

</tr>

<tr>

<td>5</td>

<td>Self link to more information about creator</td>

</tr>

<tr>

<td>6</td>

<td>Related pages</td>

</tr>

<tr>

<td>6.1</td>

<td>

_Related individual_</td>

</tr>

<tr>

<td>6.2</td>

<td>

_Related organisation_</td>

</tr>

<tr>

<td>7</td>

<td>Information about geographic area</td>

</tr>

<tr>

<td>8</td>

<td>Advertising</td>

</tr>

<tr>

<td>9</td>

<td>Software download</td>

</tr>

</tbody>

</table>

Class 1 was for general informational reasons, subdivided into different types. Class 2 was for links where the reason was a formal research citation. Other classes were acknowledgements of various kinds: of support (4) or relationships (6). Providing contextual information was a reason: information about the creator (5) or the geographic area to which the page related (7). The advertising class (8) was used where there seemed to be a commercial motivation (including boosting an educational institution's enrolments). Dissemination of software (9) was a significant reason, particularly in the computer science area.

While there is some relationship between the classes for the reason for linking, and the classes for source and target pages, it is important that these are classified separately. In the case of a teaching resource page (source class 3) linking to a university Web page (target class 10), this could be for a variety of reasons: for teaching or learning (reason 1.1), because the university was a sponsor (reason 4) or because the creator of the page had a relationship with the university (reason 6.2).

## Results

Table 3 indicates the percentages of source (i.e., the page the link was made from to the research oriented site) and target (i.e., the page on the research oriented site that was linked to) pages that were in each classification. More detailed frequency data are given in the [Appendix](#app).

<table><caption>

**Table 3: Frequency of page types**</caption>

<tbody>

<tr>

<th>Code</th>

<th>Type of page</th>

<th>Source %</th>

<th>Target %</th>

</tr>

<tr>

<td>1</td>

<td>General information resource</td>

<td>9</td>

<td>5</td>

</tr>

<tr>

<td>2</td>

<td>Research information</td>

<td>4</td>

<td>2</td>

</tr>

<tr>

<td>3</td>

<td>Teaching resource</td>

<td>3</td>

<td>2</td>

</tr>

<tr>

<td>4</td>

<td>Administrative information</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>5</td>

<td>Student assignment</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>6</td>

<td>Links list</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>6.1</td>

<td>

_Bibliography/publication list_</td>

<td>9</td>

<td>1</td>

</tr>

<tr>

<td>6.2</td>

<td>

_Directory/subject guide_</td>

<td>48</td>

<td>11</td>

</tr>

<tr>

<td>6.3</td>

<td>

_Related/useful links_</td>

<td>3</td>

<td>0</td>

</tr>

<tr>

<td>6.4</td>

<td>

_Events list_</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>7</td>

<td>Discussion list archive, Weblog</td>

<td>2</td>

<td>0</td>

</tr>

<tr>

<td>8</td>

<td>Formal publication</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>8.1</td>

<td>

_Report_</td>

<td>2</td>

<td>4</td>

</tr>

<tr>

<td>8.2</td>

<td>

_E-journal article_</td>

<td>1</td>

<td>5</td>

</tr>

<tr>

<td>8.3</td>

<td>

_Conference paper_</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>8.4</td>

<td>

_News item_</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>8.5</td>

<td>

_E-journal_</td>

<td>1</td>

<td>13</td>

</tr>

<tr>

<td>8.6</td>

<td>

_Conference_</td>

<td>1</td>

<td>2</td>

</tr>

<tr>

<td>8.7</td>

<td>

_News source_</td>

<td>4</td>

<td>3</td>

</tr>

<tr>

<td>9</td>

<td>Personal Web page</td>

<td>5</td>

<td>10</td>

</tr>

<tr>

<td>10</td>

<td>Main page</td>

<td>2</td>

<td>36</td>

</tr>

<tr>

<td>10.1</td>

<td>

_Host/subsidiary_</td>

<td>1</td>

<td>3</td>

</tr>

<tr>

<td>11</td>

<td>Software source</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>12</td>

<td>'About' page</td>

<td>3</td>

<td>1</td>

</tr>

</tbody>

</table>

The most common source page overall was the directory or subject guide (page type 6.2) with 72 links (48% of all links) made from these. This reinforces the role of directory and subject guide in facilitating surfing on the Web. However, bibliography and publication lists (page type 6.1, which might be regarded as the more traditional analogues of Web directories), as well as general information sources (page type 1), were significant sources of links, with 13 (9% of all links) each. Formal publications (page types 8.1-8.7, combining technical papers, e-journals, conferences, and news sources) were also significant, with 13 links (9% of all links).

The most common target page type is the main page of an organisation (page type 10, with 36% of all links). This type, along with subsidiary/hosted organisation, page type 10.1, personal Web pages, page type 9, and e-journal home pages, page type 8.5) could be regarded as a wider class of 'Entry points'. These were the targets of 67% of total links. Directory/Subject guides (page type 6.2), together with publication lists (page type 6.1), were the target of 12% of total links, indicating that pages that point to further information play an important role in the research oriented Web, and in attracting links. On the other hand, specific formal publications (page types 8.1-8.4) were the targets of only 10% of total links. Target pages that could be regarded as 'information content' (page types 1-4, 8.1-8.4, and 11) as opposed to entry or directional, accounted for 20% of total links.

Table 4 shows the percentage of links for each 'reason for linking'.

<table><caption>

**Table 4: Frequency of reasons for linking**</caption>

<tbody>

<tr>

<th>code</th>

<th>reason for link</th>

<th>%</th>

</tr>

<tr>

<td>1</td>

<td>General information</td>

<td>39</td>

</tr>

<tr>

<td>1.1</td>

<td>

_teaching_</td>

<td>7</td>

</tr>

<tr>

<td>1.2</td>

<td>

_administrative_</td>

<td>0</td>

</tr>

<tr>

<td>1.3</td>

<td>

_Research funding_</td>

<td>1</td>

</tr>

<tr>

<td>1.4</td>

<td>

_Dissemination of Research_</td>

<td>7</td>

</tr>

<tr>

<td>1.5</td>

<td>

_Employment_</td>

<td>2</td>

</tr>

<tr>

<td>2</td>

<td>citation</td>

<td>10</td>

</tr>

<tr>

<td>4</td>

<td>sponsor</td>

<td>7</td>

</tr>

<tr>

<td>5</td>

<td>information about creator</td>

<td>1</td>

</tr>

<tr>

<td>6</td>

<td>Related pages</td>

<td>1</td>

</tr>

<tr>

<td>6.1</td>

<td>

_Related individual_</td>

<td>6</td>

</tr>

<tr>

<td>6.2</td>

<td>

_Related organisation_</td>

<td>17</td>

</tr>

<tr>

<td>7</td>

<td>geographic area</td>

<td>1</td>

</tr>

<tr>

<td>8</td>

<td>Advertising</td>

<td>0</td>

</tr>

<tr>

<td>9</td>

<td>Software download</td>

<td>1</td>

</tr>

</tbody>

</table>

The most common reason for linking was the provision of further information to amplify the content of the source page (link types 1-1.5 and 5, 57% of total links). Relationship links, for example to related individuals or organisations, comprised 23% of total links. There were 15 links for which the reason was formally citing research (10% of total citations). Of those links which might be considered analagous to print citations, 9 were to e-journals, 4 to individual researchers sites, and 2 to other kinds of university sites.

The targets of the links to related organisations (link type 6.2) were largely professional organisations and research institutes (88% of this link type). Interestingly, there were no related organisation links made to general university sites – this may reinforce the view of universities as 'ivory towers'. Not surprisingly the bulk of 'teaching and learning' links were made to universities (7 out of 11 links of link type 1.1).

A significant number of target pages for the researchers Websites were directory subject guides (10 out of 30 were page type 6.2) indicating that researchers may perform a valuable function in providing subject guides on the Internet. Most of the target pages at e-journals were to the e-journal as a whole, rather than specific articles (7 were page type 8.2 while 19 were page type 8.5). This reinforces the view that Web links to e-journals are not exactly equivalent to print citations.

## Discussion

The classification proved to be useful, although it has not been tested for consistency across different individuals acting as classifiers. The class 6.2 in the page types (directory and subject guides) proved to be broad, and could be broken down into overall subject guides (Yahoo! etc), specialised subject guides (e.g., SOSIG) and informal subject guides (researchers' personal lists of interesting sites). In the current study, any site listings that were organised in some way were classified as directory and subject guides, and this may have been too inclusive. Software sources (in this sample, only two links) were included as a classification since in computer science, software is often seen as a research output. However it would be useful to distinguish between software originating from the organisation, and software being redistributed.

To what extent were the Web links in this study analgous to citations, i.e., that they are citing a research source? A Web link might be regarded as being equivalent to a citation if the target page was a research source, or if the reason for linking was made for research type reasons.

Some classes of target pages that could be regarded as indicating substantive research links are:

*   Research information (page type 2)
*   Technical papers, reports, e-journal articles, and conference papers (page types 8.1-8.3)
*   Software sources (page type 11)

13% of total Web links fell into these classes. Not surprisingly, e-journals were the target of almost half of these links: 5% of total links.

Classes of reasons for Web linking that that could be regarded as indicating research links are:

*   Dissemination of research (link type 1.4)
*   Formal research citation (link type 2)
*   Software download (link type 9)

17% of total links were made for these reasons. While most of these were formal research citations, a significant number were general hypertext links that had the object of disseminating research: indicating that perhaps new forms of citation are evolving on the Web.

Table 5 shows the number and proportion of Web links that satisfied one or other of the definitions for being analagous to citations, i.e., were research-oriented links (target page type 2,8.1-8.3,11; or link type 1.4, 2, or 9). Percentages are of the possible links, e.g., 8 of the 30 links to universities (27%) were analagous. Over all the types of sites, 30 of the 150 links (20%) were analagous. University sites and electronic journals had relatively high proportions of links made to them that were analagous to citations; while the professional institute and research institute sites had relatively low proportions. However the sample in this exploratory study is too small to put much weight on these differences.

<table><caption>

**Table 5: Research-oriented links**</caption>

<tbody>

<tr>

<th> </th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>Universities</td>

<td>8</td>

<td>27</td>

</tr>

<tr>

<td>Professional institutes</td>

<td>3</td>

<td>10</td>

</tr>

<tr>

<td>Research institutes</td>

<td>4</td>

<td>13</td>

</tr>

<tr>

<td>Electronic Journals</td>

<td>9</td>

<td>30</td>

</tr>

<tr>

<td>Researchers</td>

<td>6</td>

<td>20</td>

</tr>

<tr>

<th>All sites</th>

<td>30</td>

<td>20</td>

</tr>

</tbody>

</table>

## Conclusion

This exploratory study addresses the question of the extent to which Web links are analagous to conventional citations. It proposes a classification for Web links, based on classifying source and target pages, as well as the reason for linking. The classification method was applied to links made to a small sample of research-oriented sites. Not surprisingly, a high proportion of links were made from directory or subject guides. Formal publications (technical reports, e-journal articles, conference papers) were noticeable as targets of links, but less so as sources. Formal research citations only amounted to 10% of all citations. However if a broader definition of research oriented Web links is used, 20% of the links in this sample could be regarded as analagous to citations.

This study reinforces the view that the nature of Web links are more varied than print citations. However in the case of links made to these research-oriented sites, a sizeable minority were analagous to citations.

## Note:

This article is based on a paper presented at ISSI 2003, 25-29 August 2003, Beijing. The author thanks the reviewers and participants of this conference for their useful comments.

## References:

*   Case<a id="case"></a>, D.O., & Higgins, G.M. (2000). How can we investigate citation behaviour? A study of reasons for citing literature in communication. _Journal of the American Society for Information Science,_ **51**(7), 635-645\.
*   Chu<a id="chu"></a>, H. (2003). _Reasons for sitation (hyperlinking): what do they imply for Webometric research?_ Paper presented at the International Conference on Scientometrics and Informetrics, 9th. 25-29 August 2003, Beijing.
*   Egghe<a id="egghe"></a>, L.L., & Rousseau, R. (1990). Citations and citers' motivations. In, Leo Egghe & R. Rousseau, (Eds.) _Introduction to informetrics: quantitative methods in library, documentation, and information science_ (pp. 211-227). Amsterdam: Elsevier Science Publishers.
*   <a id="garfield"></a>Garfield, E. (1965). Can citation indexing be automated? In, M.E. Stevens, V.E. Giuliano, & L.B. Heilprin (Eds.), _Statistical association methods for mechanized documentation, symposium proceedings, Washington, DC, 1964_ (pp. 189-192). Washington, DC: National Bureau of Standards. (NBS Miscellaneous Publication 269).
*   Kelland, J.L., & Young, A.P. (1998). Citation patterns and library use, In: A. Kent (Ed.), _Encyclopaedia of library and information science_, (Vol. 61, pp. 62-76). New York: Marcel Dekker.
*   Kim<a id="kim"></a>, H. J. (2000). Motivations for hyperlinking in scholarly electronic articles: a qualitative study. _Journal of the American Society for Information Science_, **51**(10), 887-899\.
*   Oppenheim<a id="oppenheim"></a>, C., & Smith, R. (2001). Student citation practices in an information science department. _Education for Information_, **19**(4), 299-323\.
*   Thelwall<a id="thelwall"></a>, M. (2003). [What is this link doing here? Beginning a fine-grained process of identifying reasons for academic hyperlink creation.](http://informationr.net/ir/8-3/paper151.html) _Information Research_, **8**(3) paper no. 151\. Retrieved 13 February, 2004 from http://informationr.net/ir/8-3/paper151.html
*   Vaughan<a id="vaughan"></a>, L., & Shaw, D. (2003). Bibliographic and Web citations: What is the difference? _Journal of the American Society for Information Science and Technology_, **54**(14), 1313-1322\.
*   Wilkinson<a id="wilkinson"></a>, D., Harries, G., Thelwall, M., & Price, L. (2003). Motivations for academic Web site interlinking: evidence for the Web as a novel source of information on informal scholarly communication. _Journal of Information Science_, **29**(1), 49-56\.

## <a id="app"></a>Appendix: frequencies of page and link types

*   Uni = University
*   Proff = Professional association
*   RI= Research Institute
*   Ej= Electronic Journal
*   Rschr= researcher home page
*   Src= source page
*   Targ= target page

<table><caption>

**Frequency of page types**</caption>

<tbody>

<tr>

<th>Code</th>

<th>Type of page</th>

<th>Uni src</th>

<th>Uni targ</th>

<th>Proff src</th>

<th>Proff targ</th>

<th>RI src</th>

<th>RI targ</th>

<th>Ej src</th>

<th>Ej targ</th>

<th>Rschr src</th>

<th>Rschr targ</th>

<th>Tot src</th>

<th>Tot targ</th>

</tr>

<tr>

<td>1</td>

<td>General information resource, not otherwise classified</td>

<td>3</td>

<td>4</td>

<td>5</td>

<td>2</td>

<td>4</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>13</td>

<td>7</td>

</tr>

<tr>

<td>2</td>

<td>Research information</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>4</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>6</td>

<td>3</td>

</tr>

<tr>

<td>3</td>

<td>Teaching resource</td>

<td>0</td>

<td>3</td>

<td>0</td>

<td>0</td>

<td>3</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>5</td>

<td>3</td>

</tr>

<tr>

<td>4</td>

<td>Administrative information</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>0</td>

</tr>

<tr>

<td>5</td>

<td>Student assignment</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>6</td>

<td>Links list</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>6.1</td>

<td>

_Bibliography/publication list_</td>

<td>1</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>6</td>

<td>0</td>

<td>4</td>

<td>2</td>

<td>13</td>

<td>2</td>

</tr>

<tr>

<td>6.2</td>

<td>

_Directory/ subject guide_</td>

<td>20</td>

<td>6</td>

<td>14</td>

<td>0</td>

<td>9</td>

<td>1</td>

<td>16</td>

<td>0</td>

<td>13</td>

<td>10</td>

<td>72</td>

<td>17</td>

</tr>

<tr>

<td>6.3</td>

<td>

_Related/useful links_</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>4</td>

<td>0</td>

</tr>

<tr>

<td>6.4</td>

<td>

_Events list_</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>0</td>

</tr>

<tr>

<td>7</td>

<td>Discussion list archive, Blog</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>3</td>

<td>0</td>

</tr>

<tr>

<td>8</td>

<td>Formal publication</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>8.1</td>

<td>

_Technical paper, report_</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>6</td>

</tr>

<tr>

<td>8.2</td>

<td>

_E-journal article_</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>7</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>8</td>

</tr>

<tr>

<td>8.3</td>

<td>

_Conference paper_</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>8.4</td>

<td>

_News item_</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>8.5</td>

<td>

_E-journal_</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>19</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>20</td>

</tr>

<tr>

<td>8.6</td>

<td>

_Conference_</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>1</td>

<td>3</td>

</tr>

<tr>

<td>8.7</td>

<td>

_News source_</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>2</td>

<td>2</td>

<td>1</td>

<td>3</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>6</td>

<td>4</td>

</tr>

<tr>

<td>9</td>

<td>Personal Web page</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>5</td>

<td>14</td>

<td>7</td>

<td>15</td>

</tr>

<tr>

<td>10</td>

<td>Main page of organisation</td>

<td>0</td>

<td>10</td>

<td>1</td>

<td>20</td>

<td>2</td>

<td>24</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>3</td>

<td>54</td>

</tr>

<tr>

<td>10.1</td>

<td>

_Hosted or subsidiary organisation_</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>3</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>2</td>

<td>4</td>

</tr>

<tr>

<td>11</td>

<td>Software source</td>

<td>0</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

</tr>

<tr>

<td>12</td>

<td>'About' page</td>

<td>0</td>

<td>0</td>

<td>4</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>5</td>

<td>1</td>

</tr>

</tbody>

</table>

<table><caption>

**Frequency of link types**</caption>

<tbody>

<tr>

<th>Code</th>

<th>Reason for link</th>

<th>Uni</th>

<th>Proff</th>

<th>RI</th>

<th>Ej</th>

<th>Reschr</th>

<th>Total</th>

</tr>

<tr>

<td>1</td>

<td>General information link</td>

<td>12</td>

<td>7</td>

<td>13</td>

<td>18</td>

<td>9</td>

<td>59</td>

</tr>

<tr>

<td>1.1</td>

<td>

_Teaching/learning_</td>

<td>7</td>

<td>0</td>

<td>1</td>

<td>1</td>

<td>2</td>

<td>11</td>

</tr>

<tr>

<td>1.2</td>

<td>

_Administrative_</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>1.3</td>

<td>

_Research funding_</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>1.4</td>

<td>

_Dissemination of Research_</td>

<td>4</td>

<td>2</td>

<td>2</td>

<td>0</td>

<td>2</td>

<td>10</td>

</tr>

<tr>

<td>1.5</td>

<td>

_Employment_</td>

<td>0</td>

<td>2</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>3</td>

</tr>

<tr>

<td>2</td>

<td>Formal research citation</td>

<td>2</td>

<td>0</td>

<td>0</td>

<td>9</td>

<td>4</td>

<td>15</td>

</tr>

<tr>

<td>4</td>

<td>Sponsor/acknowledgement of support</td>

<td>3</td>

<td>3</td>

<td>4</td>

<td>0</td>

<td>1</td>

<td>11</td>

</tr>

<tr>

<td>5</td>

<td>Self link to more information about creator</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>6</td>

<td>Related pages</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>6.1</td>

<td>

_Related individual_</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>9</td>

<td>9</td>

</tr>

<tr>

<td>6.2</td>

<td>

_Related organisation_</td>

<td>0</td>

<td>14</td>

<td>9</td>

<td>2</td>

<td>1</td>

<td>26</td>

</tr>

<tr>

<td>7</td>

<td>Information about geographic area</td>

<td>0</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>8</td>

<td>Advertising</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

</tr>

<tr>

<td>9</td>

<td>Software download</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1</td>

</tr>

</tbody>

</table>