# Generating and analysing data for applied research on emerging technologies: a grounded action learning approach

#### [Pak Yoong](mailto:Pak.Yoong@vuw.ac.nz) and [David Pauleen](mailto:David.Pauleen@vuw.ac.nz)  
School of Information management  
Victoria University of Wellington, New Zealand

#### **Abstract**

> One of the difficulties of conducting applied qualitative research on the applications of emerging technologies is finding available sources of relevant data for analysis. Because the adoption of emerging technologies is, by definition, new in many organizations, there is often a lack of experienced practitioners who have relevant background and are willing to provide useful information for the study. Therefore, it is necessary to design research approaches that can generate accessible and relevant data. This paper describes two case studies in which the researchers used a grounded action learning approach to study the nature of e-facilitation for face-to-face and for distributed electronic meetings. The grounded action learning approach combines two research methodologies, grounded theory and action learning, to produce a rigorous and flexible method for studying e-facilitation. The implications of this grounded action learning approach for practice and research will be discussed.

## Introduction

Electronic meeting facilitation (e-facilitation) continues to be a critical success factor in the use of information technology to support face-to-face and virtual collaborative work. Research has shown that these meetings can be productive ([Dennis & Gallupe 1993](#dennis); [Fjermestad & Hiltz 1999](#fjermestad); [Kayworth & Leidner 2001](#kayworth)) but they also show that the productivity is largely due to the skill of an e-facilitator ([Benbasat & Lim 1993](#benbasat); [Nunamaker _et al._ 1996](#nunamaker); [Duarte & Tennant-Snyder 1999](#duarte)). The skilled facilitator aids the group in using the technology, helps with group process and facilitates the building of relationships between members who may be separated by time and distance. Yet, because the adoption of e-facilitation is very new in many organizations, there is often a lack of experienced practitioners who have relevant background and are able to provide useful information. Therefore, researchers and less-experienced practitioners continue to struggle to understand the subtleties and difficulties in the application of meeting facilitation techniques in an _electronic_ context. What is needed, then, is a way for researchers and these less-experienced practitioners to learn more about the nature of e-facilitation and the learning processes associated with becoming a facilitator of e-meetings.

Through reference to two separate studies on e-facilitation ([Yoong 1996](#yoong96); [Pauleen 2001](#pauleen01)), this paper describes how the researchers used a grounded action learning approach, a synthesis of grounded theory and action learning methods, to produce a rigorous and flexible methodology, which accomplishes three key objectives when conducting applied research of emerging technologies. These objectives are attracting and educating research participants, collecting data, and developing theory. To illustrate the methodology, two cases are presented, which show how this research approach enabled the generation of easily accessible and relevant data related to e-facilitation, as well as giving the research participants/practitioners the opportunity to learn such skills. These two cases were researched and developed separately and are presented here together to demonstrate the robustness of the methodology, which the two cases shared in common. In the first case, the author conducted a study that examined the question of how facilitators of conventional meetings become facilitators of face-to-face electronic meetings ([Yoong 1996](#yoong96)) while the second explored the nature of virtual facilitation and relationship building in virtual teams ([Pauleen 2001](#pauleen01)). In both cases action learning was used, through an intensive training programme, as the means for data generation, while grounded theory methodology, was used as a means of data analysis and theory development.

The paper begins with a discussion on action learning and grounded theory. The second section presents each study and the application of grounded action learning. Finally, some implications of this research approach for practice and research are outlined.

## Action learning

The term action learning was coined by Revans ([1982](#revans): 626-627) and is defined as 'a means of development, intellectual, emotional or physical that requires its subjects, through responsible involvement in some real, complex and stressful problem, to achieve intended change to improve their observable behavior henceforth in the problem field'. Action learning has now been extended and applied in information systems education ([Avison 1989](#avison); [Jessup & Egbert 1995](#jessup); [Yoong & Gallupe 2001](#yoong01)) and organizational development ([Ramirez 1983](#ramirez); [Gregory 1994](#gregory); [Pauleen 2003](#pauleen03)). In these contexts, action learning is a group learning and problem-solving process whereby group members work on real issues and problems with an emphasis on self-development and learning by doing.

Marsick and O'Neil ([1999](#marsick)) identify three different _schools_ of thought on action learning: scientific, experiential and critical reflection. Table 1 provides a summary of the theoretical background to each school of thought.

<table><caption>

**Table 1: The Three 'Schools' of Action Learning (Adapted from Marsick and O'Neil, [1999](#marsick): 161-163)**</caption>

<tbody>

<tr>

<th>School</th>

<th>Influenced by</th>

<th>Theoretical background</th>

</tr>

<tr>

<td>Scientific</td>

<td>R.W. Revans (1982)</td>

<td>Action learning is viewed as a model of problem solving in three stages:

1.  System Alpha - design of a problem solving strategy including a situation analysis.
2.  System Beta - the negotiation of the strategy including survey, hypothesis, experiment, audit and review.
3.  System Gamma - the learning process associated with the strategy.

</td>

</tr>

<tr>

<td>Experiential</td>

<td>D. Kolb (1984)</td>

<td>Based on Kolb's experiential learning cycle, proponents of this school advocate that the starting point for learning is action followed by reflection on action, preferably with the support of other group members. Any further action should focus on changing previous patterns of behaviour.</td>

</tr>

<tr>

<td>Critical Reflection</td>

<td>J. Mezirow (1990, 1991)</td>

<td>Proponents of this school see 'reflection on action' as a necessary but insufficient condition for learning. They believe that participants should also go deeper and examine the assumptions and beliefs that influence their practice. Reflection at this deeper level focuses a participant's attention on the root of the problem and transform previously held perspectives of the same problem.</td>

</tr>

</tbody>

</table>

Marsick and O'Neil uncovered two themes that are common to all three schools of action learning and that is, the group participants: (a) meet on equal terms and (b) are engaged in solving unstructured problems where there are no one right solution. The group of four to six participants, known as the action learning _set_, meets regularly and provides the supportive and challenging environment in which members are encouraged to learn from experience, sharing that experience with others, having other members criticise and advise, taking that advice and implementing it, and reviewing with those members the action taken and the lessons that are learned ([Margerison 1988](#margerison)). Many learning sets require the assistance of a _learning coach_ and the role of the coach depends on (a) whether the learning set works on one project as a team or the participants work on individual projects and (b) the level of facilitation on group process ([Marsick & O'Neil 1999](#marsick)).

Action learning is closely linked to action research. The relationship between action learning and action research is described by Zuber-Skerritt ([1991](#zuber): 88) when she argues that action research is based on the 'fundamental concepts of action learning, adult learning and holistic dialectical thinking...' and that action learning '...is a basic concept of action research'. However, the use of action research is more prevalent in information systems research ([Lau 1997](#lau)) and typically involves the researcher's intervention in an organization's core business process in order to both improve that process and generate new and relevant knowledge from this experience ([Kock _et al._ 1997](#kock)). However, Lau's ([1997](#lau)) proposed information systems action research framework describes the _classical_ action research as having a focus on changing information systems related practice while the _emergent_ version has a focus of changing social practice with a socio-technical system or a technological innovation.

The need to relate theory and practice is also advocated by authors such as Brown, _et al._ ([1989](#brown)) and Lave and Wenger ([1991](#lave)). Brown _et al._ propose the concept of _situated learning_ which relates _knowing_ to _doing_ and suggest that knowledge is tightly linked to the situation in which learning takes place. Lave and Wenger introduced the concept of _legitimate peripheral participation_ in which a newcomer goes through a social learning process (e.g., observations and mentoring from experts) from the peripheral to becoming an expert. They also link situated learning with communities of practice in which members, through knowledge-sharing activities in regular informal group sessions, extend their domain knowledge and expertise. The concepts of community of practice and action learning are similar in that in both situations, learning and knowledge sharing take place in groups and that members meet on equal terms. However, action learning has an additional focus of solving unstructured problems from which practical knowledge may be generated.

### The appropriateness of action learning in the study of emerging technologies

The introduction and use of emerging technologies, such as e-meetings technologies, in organizations has been found to be a complex process ([Orlikowski 1992](#orlikowski93)). For example, the adoption of electronic meeting systems in a business organisation requires a technical as well as a human perspective. Essentially, for these e-meetings to be effective, there is often a reliance on a group facilitator to oversee the computer and human interactions. However, understanding one's own facilitation actions, intended or unintended, is possible only with attention to situational and contextual cues ([Friedman 1989](#friedman)) and with the recognition that much of what the facilitator does is active, spontaneous and flexible ([Anson 1990](#anson)). There is simply too little time to deliberate on their actions. If this situation is also coupled with the use of electronic meeting tools, the facilitation becomes very much more complex. How then do you design a research-focused training programme that not only deals with this complexity in the learning situation but also equips the learners to handle this complexity in _live_ situations?

To discover the personal experiences of the participant-facilitators in these situations, the researchers began to search for learning approaches which would not only explicitly acknowledge their roles as researchers, facilitators and trainers but also their intimate involvement in the study. This meant looking for a framework that would include them in the design of the study, by allowing them to take an active part in its implementation and in collecting, analyzing and interpreting the data. This is what Cunningham ([1993](#cunningham)) described as being _engaged_ in the problem as it evolves. In each of the two case studies, it was decided that action learning would be the appropriate and relevant approach for an e-facilitation training programme.

First, action learning focuses on tackling real 'live' organizational issues and "action learning problems are always based on real work" (Marsick & O'Neil, [1999](#marsick): 165). Learning to facilitate in electronic environments is real and 'live' for many organizations who have or are considering introducing internet-based collaborative work spaces. Second, action learning promotes learning in collaborative groups and that group process is important to participants' learning ([Marsick & O'Neil 1999](#marsick)). Third, action learning is suited to turbulent environments, which are experiencing conditions of uncertainty and unpredictability ([Ramirez 1983](#ramirez)). E-facilitation often occurs in turbulent environments where uncertainty and unpredictability are common phenomena. In these conditions, the e-facilitators, in consultation with team participants, are continually adapting the meeting agenda to accommodate to these unexpected changes ([DeSanctis & Poole 1994](#desanctis)). In this respect, action learning is appropriate for e-facilitation training as it encourages and promotes the practice of active flexible facilitation. Finally, action learning meets the requirement that these training programmes are tailored to a group of experienced facilitators who could bring their own expertise and who, by researching their own practice, would be able to improve their own facilitation practice in the e-meeting environment.

### Grounded theory

The grounded theory approach taken in these two studies relies on the constant comparative method developed by Glaser and Strauss ([1967](#glaser)). This method assumes the processes of data collection, coding, analysis, and theorizing to be simultaneous, iterative and progressive. For example, as the data are collected, they are coded into categories (as many as possible) so that subsequent coding either will confirm these categories or refine, extend and modify them to fit the new data. New categories may also emerge at this stage. This data collection procedure is governed by a process known as _theoretical sampling_ where the coding and analysis done at the initial stages determines the subsequent data to be collected. Theoretical sampling, unlike statistical sampling, is the process of collecting data for comparative analysis and it is especially useful to facilitate theory generation. Wilson compares theoretical sampling with statistical sampling:

> '... theoretical sampling rethinks statistical sampling standards in order to address interactive research questions. Theoretical samples are judged by the quality of the theory that emerges, whereas statistical samples are judged by their conformity to the rules of probability sampling theory. The analyst who uses theoretical sampling looks for variation, for situations that provide new properties of a process' ([Wilson 1985](#wilson): 418).

Therefore, theoretical sampling provides a flexible process that allows the researcher, unconstrained by a prescribed sample, to pursue theory development as new concepts emerge from the data.

The notion of _theoretical sensitivity_ is described by Strauss and Corbin as 'the attribute of having insight, the ability to give meaning to data, the capacity to understand, and capability to separate the pertinent from that which isn't' ([Strauss and Corbin 1990](#strauss): 42-43). This sensitivity can be achieved by a variety of approaches including extensive literature search in related fields of study and a series of reflections on personal and professional experience.

As the study progresses, any further data collection and analysis become more selective and are guided by the emerging theory and a process known as _theoretical saturation_. This means that the entire process continues until no additional data, coding, or sorting contribute to the extension of the theory ([Glaser & Strauss 1967](#glaser)).

### The appropriateness of grounded theory

Grounded theory as used in the two studies is inductive; that is, the concepts, insights, and understanding are developed from patterns in the data. It is this inductive process that provides the primary value because little is known about the nature of e-facilitation at the beginning of both studies. The choice of grounded theory is based on the belief that to document the human experience of participants in an intensive study which involves developmental processes of change over a sustained period of time, the researchers had to be close to the participants to capture these experiences as they happened. Specifically, the choice of grounded theory as the research method for the collection and analysis of data was guided by the following considerations:

*   Close tie to the data: the grounded theory approach provides the researcher with a structured method of making sure that the emerging theory is closely tied to and consistent with the empirical data ([Glaser & Strauss 1967](#glaser)). This approach is particularly important in these studies as they address the gap between research and practice and provides a detailed and rich account of the area under investigation ([Martin & Turner 1986](#martin)).
*   The focus on process analysis: one reason for choosing the grounded theory approach is that this study looks at human experience, interaction and change in a group setting ([Strauss & Corbin 1990](#strauss)). Grounded theory facilitates'the generation of theories of process, sequence, and change pertaining to organizations, positions, and social interaction' ([Glaser & Strauss 1967](#glaser): 114) and seems particularly suitable for a study aimed at understanding the process by which e-facilitators learned their craft.
*   The use of contextual interpretation: human experiences are complex and rich and so qualitative research studies that focus on human experiences must provide contextual interpretation of the research results. For example, Orlikowski ([1993](#orlikowski93): 311) suggests that 'to produce accurate and useful results, the complexities of the organizational context have to be incorporated into an understanding of the phenomenon, rather than be simplified or ignored.'

### Grounded action learning

Baskerville and Pries-Heje argue that the theory development component of action research can be made more rigorous by using theory development methodologies such as grounded theory. This combination of the two methods, known as grounded action research, is essentially an integration of certain grounded theory techniques in the different stages of action research. The authors suggest that this integration could be done in two ways: (1) using grounded theory notations, such as memos and diagrams, to illustrate the relationship between emergent theory and the raw data and (2) utilising grounded theory coding techniques during 'the evaluating, learning and diagnosis phases of action research' ([Baskerville and Pries-Heje 1999](#baskerville): 8). In a similar fashion, action learning faces the same challenge of theory development. The combination of grounded theory and action learning, grounded action learning, not only overcomes the dilemma of theory development for researchers, but also narrows the gap between the theory and practice of emerging technologies such as e-facilitation for both researchers and practitioners. Figure 1 illustrates this connection between action learning, which generates data based on participant-facilitator experiences, and grounded theory, which allows researchers to analyse and interpret the data.

<figure>

![Figure 1](../p195fig1.jpg)

<figcaption>

Figure 1: Data generation and theory development ([Yoong 1996](#yoong96))</figcaption>

</figure>

In the next two sections, the application of grounded action learning in both studies is described.

## The application of grounded action learning

Even though there were some differences between how grounded action learning was applied in the two studies, the approach to be described in this section is a generalized version of grounded action learning based on the researchers' collective experiences.

### An outline of the two case studies

#### Case study 1: Facilitation of face-to-face electronic meetings

This first study investigated the question: How do facilitators of conventional meetings make the transition to facilitating face-to-face electronic meetings? The study aimed to develop a model representing the learning processes and experiences of conventional meeting facilitators who were undergoing training in the facilitation of computer supported problem-solving meetings ([Yoong 1998](#yoong98)).

Three action learning programmes were conducted and five participant-facilitators took part in each programme. All the fifteen participants (six females and nine males) were group facilitators and worked for public and private organisations based in Wellington, New Zealand. They had between two and twenty years' facilitation experience in educational and commercial settings. Even the least experienced facilitator had conducted at least two meetings before the training programme began. ([Appendix 1](#app) provides a summary of the participants' facilitation and computing background).

Each training programme consisted of three sessions as shown in Table 2.

<table><caption>

**Table 2: The components of an electronic meetings facilitation training programme**</caption>

<tbody>

<tr>

<th>Session number</th>

<th>Title of session</th>

<th>Brief description of the session</th>

</tr>

<tr>

<td>1</td>

<td>The tools of electronic meetings</td>

<td>This session provides the necessary hands-on skills and knowledge of the e-meeting product (GroupSystems V or VisionQuest).</td>

</tr>

<tr>

<td>2</td>

<td>Planning and managing an electronic meeting</td>

<td>This session focuses on (a) how to plan and design an agenda for an electronic meeting, (b) how to balance human and computer interactions, and (c) the role of the facilitators in electronic meetings.</td>

</tr>

<tr>

<td>3</td>

<td>Putting it all together</td>

<td>This practical session provides opportunities for trainees to plan and facilitate 'live' electronic meetings.</td>

</tr>

</tbody>

</table>

Sessions 1 and 2 were conducted over three days. The practical component, 'Putting it all together', took place soon after the training. During this session the facilitators were expected to demonstrate the skills and knowledge acquired from the preceding training programme. They did this by planning, managing and facilitating a _live_ electronic meeting which lasted about three hours.

#### Case study 2: Facilitation of virtual teams

The second study investigated the question: How do facilitators of virtual teams build relationships with their virtual team members? A pilot programme involving one participant-facilitator and two action learning programmes, involving three participant-facilitators each, were conducted in this study. Each training programme was ten weeks long ([Appendix 2](#app2) provides an outline of the participants' background). The content of the programme covered virtual team issues and processes of concern to a facilitator (Table 3). During the training programmes, each participant planned for, evaluated the use of, or actually initiated and facilitated a virtual team within their own organizational context. The three facilitators and the combined trainer and researcher in each programme met every two weeks for two hours.

<table><caption>

**Table 3: Outline of virtual team action learning programme**</caption>

<tbody>

<tr>

<td>Session One</td>

<td>Virtual team implementation and project planning</td>

</tr>

<tr>

<td>Session Two</td>

<td>Developing virtual team purpose, communication  
strategies and protocols, and technology</td>

</tr>

<tr>

<td>Session Three</td>

<td>Developing team identity, building relationships  
and intercultural communication issues</td>

</tr>

<tr>

<td>Session Four</td>

<td>Preparing for and facilitating virtual meetings</td>

</tr>

<tr>

<td>Session Five</td>

<td>Concluding a virtual team and other training issues;  
Virtual teams in the organization</td>

</tr>

</tbody>

</table>

It should also be emphasized that there are significant differences between the grounded action research as espoused by Baskerville and Pies-Heje and the grounded action learning approach used in the two case studies. They suggest that because action research projects are informed by prior theories, it often

> ...means that every action research project begins, from a grounded theory perspective, with certain pre-defined categories and perhaps even a pre-defined core category... contradicting a grounded theory tenet that theory must be allowed to emerge from the open coding. ([Baskerville and Pries-Heje 1999](#baskerville): 18)

Because the researchers in these action learning studies were not working with the participants in solving their respective business problems, their concern was exclusively for the learning that the participants achieved and the unfettered emergence of the data.

### Collecting and analyzing data from the programme

This section begins with a discussion of some field work issues associated with both studies, followed by a description of the procedures used during the collection, analysis and interpretation of the research data.

#### Field work issues

The field work in both studies was divided into three blocks of activity. In the first study, there were three training programmes whilst the second study involved a pilot project and the two training programmes. Dividing the studies into blocks of activity proved to be a useful approach. The extended period between each block of fieldwork provided time for transcription and analysis of the interview data. Equally importantly, these in-between periods were used for reflection, interpretation and strategy building. These reflective periods, which are built into the action research cycle as well as the grounded theory method ( [Yoong 1996](#yoong96)) significantly influenced the way the next period of fieldwork was conducted. The following two examples from the second study are illustrative: the difficulties the researcher encountered in the pilot project working with a single individual, encouraged him to think strategically about his data collection methods and consequently to devise a training programme for several participants so as to ensure adequate data collection; the interim results from the first training programme helped him to determine the selection of the second programme participants based on the principle of theoretical sampling. Facilitators in the second training programme were selected because of their differences to those from the first training programme, both in their experience with virtual teams and in the global nature of their virtual teams and team projects. As a result, the researcher was able to compare and contrast the emerging theory with the data as prescribed by the constant comparative method.

#### Data collection

As discussed in an earlier section, the constant comparative method provides the researcher with an established set of procedures for conducting the data analysis. Several methods of data collection have been used in both studies primarily based on semi-structured interviews and discussions between the researcher and the facilitators and informal facilitator reports, the researcher's journal, participants' notes, and organizational documentation. Video-recordings of the _live_ electronic meetings were also collected in the first study while copies of electronic conversations (i.e., e-mail) were used in the second study. These different methods provided for the collection of diverse kinds of data and enhanced the use of the constant comparative method ([Glaser & Strauss 1967](#glaser)).

For example, during the first study semi-structured interviews were conducted with the facilitators after each of the following training events: briefing meeting, the training sessions and the _live_ electronic meeting (used only for the first study). The list of questions in the interview guide was flexible and 'not a tightly structured set of questions to be asked verbatim as written... it is a list of things to be sure to ask about when talking to the person being interviewed' (Lofland & Lofland, [1995](#lofland): 85). This flexible and guided conversation then flowed on from the initial leading questions. The grounded theory principle of theoretical sensitivity also guided this conversation as the researchers were looking for similarities, differences and density in the facilitators' account of their experiences.

#### Data analysis

As is common in qualitative research, a large volume of data was collected in both studies ([Gopal & Prasad 2000](#gopal)), and each researcher began to analyse the data by listening to and transcribing each recorded interview and discussion. Transcripts were returned to the participants for member checking and validation.

**Step 1**: The first step in the analysis of the data were to code all the transcripts and relevant documents such as facilitators' journals. Open coding techniques, a process of labelling the events and ideas represented in the data ([Baskerville & Pries-Heje 1999](#baskerville)) into concepts were used first. The goal of open coding is to create an initial list of conceptual codes, which are grounded in the data. Most of the data were coded using NVivo, a computer software program developed for use with qualitative research methods ([Richards & Richards 1994](#richards)). The transcripts were perused and each line, sentence or paragraph, were assigned one or more conceptual codes, most often in terms of properties and dimensions. As grounded theory researchers, the data were approached without any particular preconceived notion or framework ([Trauth & Jessup 2000](#trauth)) and simply assigned a descriptive label.

**Step 2**: At the end of the first action learning cycles, the researchers began looking for connections between conceptual codes through the use of several strategies. As suggested by Baskerville and Pries-Heje ([1999](#baskerville)) grounded theory notation such as memos and diagrams were used. A set of emergent models based on the codes and categories that were taking shape were created, as well as the researchers' intuition guided by increasing levels of theoretical sensitivity. Each researcher also wrote narrative, chronological case studies of each of the participants. This gave them another lens through which to view the data and to draw cross linkages between the experiences of each of the participants. These cases also provided a valuable way to engage in _member checking_ with the participants when they read through them and verified their experience.

**Step 3**: The data collection and analysis processes were repeated for the remaining training programmes, comparing emerging categories with those from the previous cycles. The notion of _theoretical sensitivity_ is particularly useful at these stages. It is 'the attribute of having insight, the ability to give meaning to data, the capacity to understand, and capability to separate the pertinent from that which isn't' ([Strauss & Corbin 1990](#strauss): 42-43). This sensitivity can be achieved by a variety of approaches including extensive literature search in related fields of study and a series of reflections on personal and professional experience. Any further data collection and analysis become more selective and are guided by the emerging theory and a process known as _theoretical saturation_. This means that the entire process continues until no additional data, coding, or sorting contribute to the extension of the theory.

## Research results

### Case study 1: Facilitation of group support systems

The major outcome of the first study is the grounded theory of reflective facilitation of group support systems facilitation. Reflective facilitation of group support systems describes the processes and issues associated with how facilitators of conventional meetings—the trainees who took part in this study—become facilitators of electronic meetings. The processes and issues were documented from the time the facilitators entered the programme until the time they had facilitated their first electronic meeting, a period defined as the transitional period. The research findings from this study, which occurred in one setting, are inductively derived from what took place in that setting.

Reflective facilitation of group support systems is a unifying framework for the two inter-related theoretical components: the stages of group support systems facilitation development and reflective practice. Figure 2 shows the inter-relationship between the two theoretical components.

<figure>

![Figure 2](../p195fig2.jpg)

<figcaption>

Figure 2: The stage theory of GSS facilitation development</figcaption>

</figure>

The first of these two theoretical components, the stages in the developoment of group support systems facilitation, describes the four stages that the trainees go through in the transition from being facilitators of conventional meetings to being facilitators of electronic meetings. The four stages are: wondering and exploring, familiarising, integrating, and balancing.

The second theoretical component, reflective practice, describes the trainees' ways of thinking about how to facilitate electronic meetings and the context in which these thoughts take place. Reflective practice itself consists of two components: active reflection and contextual factors.

### Case study 2: virtual facilitation

The major outcome of the second study is a grounded theory of virtual facilitation that investigated the issues facing virtual team facilitators as they implemented and led virtual team members ( [Pauleen 2003](#pauleen03), [2004](#pauleen04)). A specific outcome of the study was a model of how virtual team facilitators build relationships with their virtual team members. This model (Figure 3) includes a unifying framework of three inter-related theoretical steps: assessing conditions, targeting level of relationship, and creating strategies. This study was the first to identify the steps a virtual team leader undertakes when building relationships with virtual team.

<figure>

![Figure 3](../p195fig3.jpg)

<figcaption>

Figure 3: The three steps in facilitating virtual relationships (in an organizational context)</figcaption>

</figure>

## Discussion and implications

We believe there are a number of implications of the grounded action learning approach described in this paper. First, learning to facilitate electronic meetings is a complex and difficult experience. The action learning component of grounded action learning provides the facilitator with the means to combine both experience and reflection as the learning is taking place.

Secondly, the grounded theory component of grounded action learning provides the researcher with a lens to analyze and interpret the research data, that is, the trainees' experience. The selective use of different grounded theory techniques enables the researchers to directly link research data with participants' experience, thus ensuring a closer link between theory and practice. This process enables information systems researchers to minimize criticisms that findings from information systems research are seldom relevant to organizational practice.

Thirdly, the grounded action learning approach has potential beyond research into e-facilitation. Information systems researchers in general, might consider this approach when studying learning in complex situations involving emerging technologies. For example, this form of practice-focused research may very well become more pervasive as organizations learn to manage emerging technologies.

Finally, for future research into the adoption of internet-based collaborative systems in organisations, the activity theory framework ([Cole and Engestrom 1993](#cole), cited in Turner and Turner, [2002](#turner)) could be used to establish a set of conventions, norms and behaviour regarding e-facilitation. The framework could help identify the interactions between the behaviour of virtual team members and that of facilitators.

## Conclusions

It is our view that grounded action learning is a powerful approach in studying organisational usage of emerging technologies. Firstly, the action learning approach is a useful method to training users of emergent technologies. Secondly, the grounded theory provides an effective method for researchers to collect and analyse relevant data based on the users' experience. Finally, the grounded action learning approach offers the potential to develop emergent theories of new information technology applications that are based on actual practice. It is hoped that this paper will promote identification of and discussion about the many complex issues associated with the introduction of new technology in the workplace.

## References

*   Anson<a id="anson"></a>, R. (1990). _Effects of computer support and facilitator support on group processes and outcomes: an experimental assessment_. Unpublished doctoral dissertation, Indiana University, Bloomington, Indiana, USA.
*   Avison<a id="avison"></a>, D. (1989). Action learning for information systems teaching. _International Journal of Information Management_, **9**(1), 41-50.
*   Baskerville<a id="baskerville"></a>, R. & Pries-Heje, J. (1999). Grounded action research: a method for understanding IT in practice. _Accounting Management and Information Technologies_, **9**(1), 1-23.
*   Brown<a id="brown"></a>, J., Collins, A. and Duguid, P (1989).  Situated cognition and the culture of learning.  _Educational Researcher_, **18**(1), 32-42.
*   Benbasat<a id="benbasat"></a>, I. & Lim, L. (1993). The effects of groups, task, context, and technology variables on the usefulness of group support systems: a meta-analysis of experimental studies. _Small Group Research_, **24**(4), 430-462.
*   Cole<a id="Cole"></a>, M. and Engestrom, Y. (1993).  A cultural-historical approach to distributed cognition. In G. Salomon (Ed.), _Disitributed cognitions–psychological and educational considerations_. Cambridge: Cambridge University Press.
*   Cunningham<a id="cunningham"></a>, J. (1993). _Action research and organizational development_. Westport, CT: Praeger.
*   Dennis<a id="dennis"></a>, A.R. & Gallupe, R.B. (1993). GSS Past, present, and future. In L. Jessup and J. Valacich (Eds.), _Group support systems: new perspectives_ (pp. 59-77). New York, NY: Macmillan.
*   DeSanctis<a id="desanctis"></a>, G. & Poole, S. (1994). Capturing the complexity in advanced technology use: adaptive structuration theory. _Organization Science_, **5**(2), 121-147.
*   Duarte<a id="duarte"></a>, N. & Tennant-Snyder, N. (1999). _Mastering virtual teams: strategies, tools, and techniques that succeed_. San Francisco, CA: Jossey-Bass.
*   Fjermestad<a id="fjermestad"></a>, J. & Hiltz, S.R. (1999). An assessment of group support systems experiment research: methodology and results. _Journal of Management Information Systems_, **15**(3), 7- 15.
*   Friedman<a id="friedman"></a>, P. (1989). Upstream facilitation: a proactive approach to managing problem-solving groups. _Management Communications Quarterly_, **3**(1), 33-51.
*   Glaser<a id="glaser"></a>, B. & Strauss, A.L. (1967). _The discovery of grounded theory: strategies for qualitative research_. New York, NY: Aldine De Gruyter.
*   Glesne<a id="glesne"></a>, C. & Peshkin, A. (1992). _Becoming qualitative researchers: an introduction_. White Plains, N.Y: Longman.
*   Gopal<a id="gopal"></a>, A. & Prasad, P. (2000). Understanding GDSS in symbolic context: shifting the focus from technology to interaction. _MIS Quarterly_, **24**(3), 509-545.
*   Gregory<a id="gregory"></a>, M. (1994). Accrediting Work-Based Learning: Action Learning - A model for empowerment. _Journal of Management Development_, **13**(4), 41-52.
*   Kayworth<a id="kayworth"></a>, T.R. & Leidner, D.E. (2001). Leadership effectiveness in global virtual teams. _Journal of Management Information Systems_, **18**(3), 7-40.
*   Jessup<a id="jessup"></a>, L. & Egbert, J. (1995). Active learning in business education with, through, and about technology. _Journal of Information Systems Education_, **7**(3), 108-112.
*   Kock<a id="kock"></a>, N., McQueen, R. & Scott, J. (1997). Can action research be made more rigorous in a positivist sense? the contribution of an iterative approach. _Journal of Systems and Information Technology_, **1**(1), 1-24.
*   Lau<a id="lau"></a>, F. (1997). A review on the use of action research in information systems studies. In Lee, A.S., Liebenau, J. & DeGross, J.I. (Eds), _Information systems and qualitative research_ (pp. 31-68). London: Chapman and Hall, London.
*   Lave<a id="lave"></a>, J. and Wenger, E. (1991). _Situated learning: legitimate peripheral participation._ Cambridge: Cambridge University Press.
*   Lofland<a id="lofland"></a>, J. & Lofland, L. (1995). _Analyzing social settings: a guide to qualitative observation and analysis_. Belmont, CA: Wadsworth Pub.
*   Margerison<a id="margerison"></a>, C. (1988). Action learning and excellence in management development. _Journal of Management Development_, **7**(5): 43-55.
*   Marsick<a id="marsick"></a>, V. & O’Neil, J. (1999). The many faces of action learning. _Management Learning_, **30**(2), 159-176.
*   Martin<a id="martin"></a>, P. & Turner, B. (1986). Grounded theory and organizational research. _The Journal of Applied Behavioral Science_, **22**(2), 141-157.
*   Nunamaker<a id="nunamaker"></a>, J.F. Jr., Briggs, R.O. & Mittleman, D.D. (1996). Lessons from a decade of group support systems research. In _Proceedings of the 29th Annual Hawaii International Conference on System Sciences (HICSS), January 3-6, 1996, Maui, Hawaii. Volume 3: Collaboration Systems and Technology_, (pp. 418-427). Washington, DC: IEEE Computer Society
*   Orlikowski<a id="orlikowski92"></a>, W. (1992). The duality of technology: rethinking the concept of technology in organization. _Organization Science_, **3**(3), 398-427.
*   Orlikowski<a id="orlikowski93"></a>, W. (1993). Case tools as organizational change: investigating incremental and radical changes in systems development. _MIS Quarterly_, **17**(3), 309-340.
*   Pauleen<a id="pauleen01"></a>, D. (2001). _A grounded theory of virtual facilitation: building relationships with virtual team members_. Unpublished doctoral thesis, Victoria University of Wellington, Wellington, NZ.
*   Pauleen<a id="pauleen03"></a>, D. (2003) Leadership in a global virtual team: an action learning approach. _Leadership and Organizational Development Journal_, **24**(3), 153-162.
*   Pauleen<a id="pauleen04"></a>, D. (2004). An inductively derived model of leader-initiated relationship building with virtual team members. _Journal of Management Information Systems_, **20**(3), 227-256
*   Ramirez<a id="ramirez"></a>, R. (1983). Action learning: a strategic approach for organizations facing turbulent conditions. _Human Relations_, **36**(8), 725-742.
*   Revans<a id="revans"></a>, R. (1982). _The origins and growth of action learning_. Bromley, UK: Chartwell-Bratt.
*   Richards<a id="richards"></a>, T. & Richards, L. (1994). Using computers in qualitative research. In Lincoln, Y.S. & Denzin, N.K (Eds) _Handbook of qualitative research_ (pp. 445-463). Thousand Oaks, CA: Sage Publications.
*   Strauss<a id="strauss"></a>, A. L. & Corbin, J (1990). _Basics of qualitative research: grounded theory procedures and techniques_. Newbury Park, CA: Sage Publications.
*   Trauth<a id="trauth"></a>, E. M. & Jessup, L.M. (2000). Understanding computer-mediated discussions: positivist and interpretative analysis of group support system use. _MIS Quarterly_, **24**(1), 43-79.
*   <a id="turner"></a>Turner, P. and Turner, S. (2002). End-user perspectives on the uptake of computer supported cooperative working. _Journal of End User Computing_, **14**(2), 3-15.
*   Wilson<a id="wilson"></a>, H. (1985). _Research in nursing_. Menlo Park, CA: Addison-Wesley.
*   Yoong<a id="yoong01"></a>, P., & Gallupe, B. (2001). Action learning and groupware technologies: a case study in GSS facilitation research. _Information Technology and People_, **14**(1), 78-90.
*   Yoong<a id="yoong98"></a>, P. (1998). Training facilitators for face-to-face electronic meetings: an experiential learning approach. _Journal of Informing Science_, **1**(2), 31-36\.
*   Yoong<a id="yoong96"></a>, P. (1996). _A grounded theory of reflective facilitation: making the transition from traditional to GSS facilitation_. Unpublished doctoral thesis, Victoria University of Wellington, New Zealand.
*   Yoong<a id="yoong99"></a>, P. (1999). Making sense of gss facilitation: a reflective practice perspective. _Journal of Information Technology and People_, **12**(1), 86-112.
*   Zuber<a id="zuber"></a>-Skerritt, O. (1991). _Professional development in higher education: a theoretical framework for action research_, Brisbane: Griffith University.

## <a id="app"></a>Appendix 1

<table><caption>

**Case study 1: Summary of the participants' background by computing and facilitation experience**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="2">Level of experience as a facilitator of conventional meetings</th>

<th rowspan="2">Total</th>

</tr>

<tr>

<th>Level of experience with the use of computers</th>

<th>Medium</th>

<th>High</th>

</tr>

<tr>

<th>Medium</th>

<td>2 (13%)</td>

<td>6 (40%)</td>

<td>8 (53%)</td>

</tr>

<tr>

<th>High</th>

<td>2 (13%)</td>

<td>5 (34%)</td>

<td>7 (47%)</td>

</tr>

<tr>

<th>Total</th>

<td>4 (27%)</td>

<td>11 (73%)</td>

<td>15</td>

</tr>

</tbody>

</table>

## <a id="app2"></a>Appendix 2

<table><caption>

**Case study 2: Summary of the participants' background**</caption>

<tbody>

<tr>

<th>Participant/Positions</th>

<th>Organisation</th>

<th>Previous experience of virtual communication</th>

</tr>

<tr>

<td>DW - Managing Director</td>

<td>New Zealand-based advertising company - part of an international partnership</td>

<td>very familiar with use of virtual communication channels</td>

</tr>

<tr>

<td>BC - Senior Policy Analyst</td>

<td>New Zealand Government Department</td>

<td>familiar with use of virtual communication channels</td>

</tr>

<tr>

<td>SC - Independent contractor</td>

<td>New Zealand educational consulting company</td>

<td>familiar with use of virtual communication channels</td>

</tr>

<tr>

<td>RB - General Manager  
</td>

<td>New Zealand software and business development consulting company  
</td>

<td>familiar with use of virtual communication channels and underlying technology  
</td>

</tr>

<tr>

<td>RW - Managing Director  
</td>

<td>New Zealand-based political consultancy operating worldwide as a virtual organisation  
</td>

<td>very familiar with use of virtual communication channels and experience facilitating virtual teams</td>

</tr>

<tr>

<td>AR - Project Manager  
</td>

<td>New Zealand office of international consulting company  
</td>

<td>very familiar with use of virtual communication channels and experience facilitating virtual teams  
</td>

</tr>

<tr>

<td>JJ - Project Analyst  
</td>

<td>Australasian trading company  
</td>

<td>some experience as a student with use of virtual communication channels  
</td>

</tr>

</tbody>

</table>