#### Vol. 12 No. 4, October, 2007

* * *

# "Featuring the Future": Proceedings of the sixth international conference on conceptions of library and information science (CoLIS6)  

Preface

#### [Katriina Byström](mailto:Katriina.Bystrom@hb.se)  
Swedish School of Library and Information Science  
at Göteborg University and Högskolan i Borås  
501 90 Borås, Sweden  
#### [Ragnar Nordlie](mailto:Ragnar.nordlie@jbi.hio.no)  
#### [Nils Pharo](mailto:Nils.Pharo@jbi.hio.no)  
Oslo University College  
Dept. of journalism, library and information science  
Box 4, 0130 Oslo, Norway

CoLIS (Conceptions of Library and Information Sciences) is a series of international conferences which scrutinizes the underlying methodological ideas that advise the research efforts within the LIS discipline. One of the sustaining principles for CoLIS is its emphasis on interdisciplinary issues. It is under the CoLIS umbrella that different perspectives, theories, methods and both abstract and practical outcomes meet, as well as where disciplinary and sub-disciplinary boundaries and joint ventures are conferred.

The 6th CoLIS: Featuring the Future conference was arranged in Borås, Sweden, August 13 - 16, 2007, by the Swedish School of Library and Information Science. Borås as a CoLIS conference site follows Glasgow, UK, Seattle, United States, Dubrovnik in Croatia, Copenhagen in Denmark and Tampere, Finland, where the first CoLIS took place in 1991.

This year's conference theme focused on the ever burning questions of how to develop and enhance the prospects of library and information science. Important areas addressed at the conference were the efforts that are being made to increase the impact in, and of, LIS research on the knowledge society. It shed light on theoretical and empirical research trends in LIS relating to our understanding of the various roles, natures, uses and associated relationships of information, information systems, information processes, and information networks. More specifically, contributions were invited on the following four themes:

*   Reframing LIS from different perspectives
*   LIS in contemporary society
*   LIS versus new research and professional fields
*   New research methods in LIS

The contributions range widely in content, from the heavily theoretical to the forthrightly practical. They are all interesting in their own particular ways, but the main strength of the conference lies perhaps in the large number of papers that discuss, extend or take to task the major theories and assumptions in the LIS field, from the basic questions of the nature of knowledge, to theories of information behaviour or information description. The three keynote speakers addressed issues of LIS future from different positions, both inside and outside the discipline, and the submitted papers offer a variety of ways to understand, emphasise and value contemporary library and information scientific issues. Few, if any, other forums in the field offer this degree of focus on theory and methodology development.

CoLIS 6 had 110 registered participants from 16 countries and the main conference programme included 3 keynote presentations, 31 papers and two panels, as well as a poster session. In addition to the main conference, two pre-conference workshops took place. The "Educational Forum", was a workshop devoted to the international study of the status and needs of education in the LIS field and was organised for the first time under the aegis of CoLIS. The "Doctoral Forum" where doctoral candidates present and discuss their work with experienced mentors, is traditionally included in CoLIS arrangements.

In this specific issue of "Information Research" we present the proceedings of the conference in three sections: full paper, poster abstracts and educational forum contributions. The full papers have all been peer reviewed in a double blind review process, apart from the three invited keynote presentations. The poster abstracts have also undergone a peer review process, either as poster contributions or as doctoral forum presentations. The educational forum consisted of a mixture of invited and peer-reviewed papers, and the invited papers have been marked as such in the proceedings. A "best student paper" award was prize was awarded to Melanie Feinberg for her paper "Hidden bias to responsible bias"

We would like to characterise CoLIS 6 as a congenial academic forum where different views on library and information science did not stand in the way of lively discussion, but enriched it. This stimulating atmosphere was created by the contributions of a number of people; the engaged Scientific Committee, an insightful Programme Committee, inspiring speakers and a fantastic local team, generous sponsors as well as marvellous participants. Thank you!

We hope you will find the CoLIS 6 papers in this issue as inspiring as we have done.

October 2007  

Katriina Byström,  
Conference Chair  

Ragnar Nordlie  
Nils Pharo,  
Publication & Proceedings Co-Chairs