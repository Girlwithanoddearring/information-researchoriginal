#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Poster abstract

# Heterogeneity and homogeneity in library and information science research

#### [Fredrik Åström](mailto:fredrik.astrom@lub.lu.se)  
Lund University Libraries, Head Office,  
P.O. Box 134,  
SE-221 00 Lund,  
Sweden

## Introduction

Library and information science has been described as a field in crisis with a debated identity, lacking theoretical development and vulnerable to competition from other fields (e.g. ([Hjørland 2000](#hjo00), [Warner 2001](#war01)), a notion reflecting theories on 'fragmented adhocracies' ([Fuchs 1993](#fuc93), [Whitley 2000](#whi00)). To better understand and face the challenges, a discussion on the challenges and perceived crisis is suggested. The focus of the discussion is on library and information science origins, perceptions and definitions, as well as contextual relations, related to the concept pairs 'homogeneity - heterogeneity' and 'integration - fragmentation'.

## Method

The analysis is based on meta-analytical library and information science literature, with a certain emphasis on empirical investigations, where the findings of these previous studies are related to a set of theories on the organization of the sciences, primarily the framework developed by Whitley ([2000](#whi00)); and to ideas by Gibbons _et al._ ([2004](#gib04)) for analytical contrast.

## Results

The development of contemporary library and information science can be viewed from three different perspectives. The first perspective is related to a disciplinary based view on the organization of the sciences, where library and information science can be characterized as a 'fragmented adhocracy' ([Whitley 2000](#whi00)), heading for further fragmentation ([Fuchs 1993](#fuc93)). An interpretation strongly related to the notion of library and information science as a field in crisis ([Hjørland 2000](#hjo00), [Warner 2001](#war01)). This development can be seen in how the dual origin of the field has lead to great variations in terms of research orientations and scientific organization ([Åström 2006](#ast06), [Buckland 1996](#buc96), [Rayward 1996](#ray96)). Another aspect is the great variety of meta-studies, ([Åström 2006>](#ast06)), reflecting a diverse self-understanding, mirroring high levels of task uncertainty and low degrees of mutual dependency and reputational autonomy ([Whitley 2000](#whi00)). This has implications for the internal organization of the field, as well as opening up for external competition, making it easier for external fields to have an impact on library and information science research, as well as on definitions of the field and its purposes. This can be seen in e.g. the large 'import' of ideas ([Cronin and Pearson 1990](#cro90)), in competition from other fields on researching information related phenomena and in one extreme case: the use of the name information science for a department not including library and information science, but at the same university as a library and information science unit ([Åström](#ast06)).

There are, however, indications on an alternative line of development. Signs of library and information science research areas integrating can be seen, e.g., information retrieval and information seeking as well as information retrieval and informetrics ([Åström 2007](#ast07), [Ingwersen and Järvelin 2005](#ing05)), suggesting a process of homogenization in at least some parts of the field; and contradicting Fuchs' ([1993](#fuc93)) theories on the dynamics of research fields.

The development of library and information science can also be seen from an alternative point of view, reinterpreting many of the characteristics of library and information science as a field in crisis into traits closely connected to the development of the sciences since 1945, formulated in terms of 'Mode 2' research ([Gibbons et al 1994](#gib94)). From this point of view, the heterogeneous nature of library and information science does not signify fragmentation but interdisciplinarity, a trait further emphasized by an increase in cooperation between different information oriented fields in the form of e.g. information schools. Another aspect of the interdisciplinary trait is the shift of emphasis from competition and the protection of disciplinary boundaries to cooperation across disciplinary limits ([Åström 2006](#ast06)). One important aspect of the 'Mode 2' research is how the organization of research not only stretches across disciplinary borders, but involves non-academic participants to a higher degree and a stronger emphasis on applications oriented research ([Gibbons et al 1994](#gib94)). This makes library and information science characteristics such as the close connection to the field of practice, and a definition of the library and information science raison d'être oriented towards supporting the dissemination of relevant information, more valid as a basis for academic legitimacy.

## Conclusions

To understand the dynamics in current library and information science development and the notion of library and information science as a field in crisis, concept pairs such as 'homogeneity - heterogeneity' and 'integration - fragmentation' needs to be taken into account. Depending on what parts of the pairs are emphasized - and whether the development is seen from a disciplinary based or 'Mode 2' perspective on the organization of the science - how the development of library and information science is perceived differs significantly. The notion of library and information science in crisis can for instance be reinterpreted from the point of view of Gibbons et al [(1994)](#gib94), where an interdisciplinary and applications oriented organization of research where library and information science characteristics becomes a trait along the lines of modern scientific organization rather than a cause for concern in terms of academic legitimacy.

## References

*   <a id="ast06" name="ast06"></a>Åström, F. (2006). _The social and intellectual development of library and information science_. Umeå: Dept. of Sociology. Diss.
*   <a id="ast07" name="ast07"></a>Åström, F. (2007). Changes in the library and information science research front: timesliced co-citation analyses of library and information science journal articles, 1990-2004\. _Journal of the American Society for Information Science and Technology_, **58**(7), 947-957
*   <a id="buc96" name="buc96"></a>Buckland, M.K. (1996). Documentation, information science and library science in the U.S.A., _Information processing & management_, **32**(1), 63-76
*   <a id="cro90" name="cro90"></a>Cronin, B. & Pearson, S. (1990). The export of ideas from information science. _Journal of information science_, **16**, 381-391
*   <a id="fuc93" name="fuc93"></a>Fuchs, S. (1993). A sociological theory of scientific change. _Social forces_, **71**(4), 933-953
*   <a id="gib94" name="gib94"></a>Gibbons, M. _et al._ (1994). _The new production of scientific knowledge: the dynamics of science and research in contemporary society_. London: Sage
*   <a id="hjo00" name="hjo00"></a>Hjørland, B. (2000). Library and information science: practice, theory, and philosophical basis. _Information Processing & Management_, **36**, 501-531
*   <a id="ing05" name="ing05"></a>Ingwersen, P. & Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context_. Dordrecht: Springer
*   <a id="ray96" name="ray96"></a>Rayward, W.B. (1996). The history and historiography of information science: some reflections. _Information processing & management_, **32**(1), 3-17
*   <a id="war01" name="war01"></a>Warner, J. (2001). W(h)ither information science?/! _Library Quarterly_, **71**(2), 243-255
*   <a id="whi00" name="whi00"></a>Whitley, R. (2000). _The intellectual and social organization of the sciences_. Oxford: University Press