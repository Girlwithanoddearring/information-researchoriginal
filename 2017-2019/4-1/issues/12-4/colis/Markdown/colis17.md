#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# The public library: from information access to knowledge management: a theory of knowledge and knowledge categories

#### [Lars Qvortrup](mailto:rektor@db.dk)  
#### The Royal School of Library and Information Science, Copenhagen

#### Abstract

> How do you know what you know? Two hundred years ago you would answer that "I heard it from my neighbours" or that "the priest said so". Fifty years ago you would answer that "I read it in the newspaper" or "heard it in the radio" - or you might have learned it from visiting the public library. Today, many people would answer that "I have been on the net", or that "I have googled". This implies that the theory of public knowledge has entered a new phase. We need to know how knowledge should be defined and categorized in relation to what is increasingly called a "knowledge society".  
> However, the theory of knowledge in sociological contexts such as knowledge economy, knowledge management, knowledge sharing and knowledge organizations (including, of course, libraries) lacks a common definition and categorization. It is the ambition of the present paper (and of earlier publications, e.g., Qvortrup 2001, 2004 and 2006) to suggest a well-defined concept and categorization. In this paper knowledge is defined as confirmed observations, and it is suggested that knowledge can be classified into four categories based on a system of hetero- and self-observation, i.e. a system of knowledge of the environment and knowledge of knowledge. In my development of this definition and categories I refer primarily to sociological and organizational theory. It is, however, my assumption that in order to qualify the sociological definition of knowledge one can benefit from referring to the vast amount of philosophical analyses and theories of knowledge.  
> However, in order to understand the need for a current and updated theory of knowledge in relation to library theory the paper starts with a brief sketch of the development of modern libraries from the 17th century till today.

## From information access to knowledge management

In 1627, Gabriel Naudé published his famous book _Advis pour dresser une bibliothéque_ (Handbook in establishing a library). For him, the basic challenge of a modern library was to provide as much information as possible.

The background was the following: In the beginning of the 1620s the president of the parliament of Paris, Henri de Mesme, asked the young and gifted scholar, Gabriel Naudé, to establish a modern, public library in Paris. A library which could address the demands of the emerging modern society. After some years of consideration, Gabriel Naudé answered by providing both an institution and a book: The first modern library, and the first book about modern librarianship.

The basic principle was that the modern citizen should have access to as much information as possible. At that time it was a revolutionary statement, for at that time almost all libraries were private. Until the 17th century, in reality the basic function of libraries had been to prevent ordinary people from getting access to books. Possession of knowledge was potentially dangerous and should be limited to the educated few.

According to Naudé, this should be down away with. The new ideal - and the precondition for the emerging democratic society - was: Enlightenment.[1](#note1) In order to make this ideal a reality, public libraries should be established with the primary function to give all citizens access to all existing information. In order to fulfil this function, three basic principles for the modern library should be put into reality.

First, one should "...provide the library with all the greatest and most important authors, old and new." And, Naudé added: "One must not ignore all those books written by the biggest heretics."

Second, there must be public access. Not even "the poorest reader" must be prevented access to the collections, Naudé wrote.

Third, for every single library one should " ...find an educated, learned and bibliognostic man and give him together with the office a reasonable salary, title and rank as librarian." As his first duty, this librarian should organize the collection of books systematically and elaborate two catalogues: a subject catalogue and an alphabetic catalogue.

It is my assumption that today the situation is radically different. I assert that today the basic challenge in society is not to get access to as much information as possible, but to deal with a massive information overload. As a consequence of this assumption the basic ability of a librarian is to manage this information overload, i.e. to transform information into knowledge.[2](#note2) The fundamental function of the modern library is not to give access to all information available, but to provide knowledge management. Only in this way the current library can play its role as a central node in the knowledge society.

This, however, raises new demands for our theory of libraries. The reason is that although almost everybody talks about the so-called "knowledge society", in reality we know very little about knowledge in a current, sociological context. What is it? In which forms does it appear? Which forms of knowledge support which purposes? How do we facilitate the different forms of knowledge? And what qualifies a society into being named a "knowledge society"?

## What is knowledge? In search of a theory<sup>[3](#note3)</sup>

As already stated, we are in a paradoxical situation. On the one hand almost everybody seems to agree that knowledge is a basic phenomenon of and an adequate basic concept for our current society. Very few would dispute that we live in a "knowledge society", that the knowledge production of educational institutions is central for the reproduction of the wealth of our nations, and that it is important to develop institutions that can support the public access to knowleledge and the "knowledge circulation" of society.

On the other hand, we do not seem to know how knowledge should be defined within the context of a theory of society. Most of the current theories of knowledge society, knowledge production and knowledge institutions do not suggest an explicit concept of knowledge.

Consequently, a basic - and ambitious - current demand is to develop a theory of the knowledge society and its basic institutions in much the same way as Karl Marx did for the capitalist society. He stated that in order to understand capitalism, it was necessary to analyse and understand the basic element of this society: The commodity. Similarly, in order to understand the knowledge society, it is necessary to analyse and to understand the basic element of this society: Knowledge. Put slightly differently, in order to observe society as a knowledge society and to understand the functional mechanisms of an emerging knowledge society - or, as I would prefer: a knowing society (cf. [Qvortrup 2004](#qvo04)) - one should focus on the marked state of this society: knowledge, and thus implying that knowledge/non-knowledge is an adequate basic distinction.

But in order to test whether this initial distinction is adequate, the concept must be taken seriously. It must be defined in such a way that it is robust as the basis for further conceptual determinations. What is knowledge? Which knowledge categories can be identified?

## Existing knowledge theories

In most theories of the knowledge society, any explicit, sociologically relevant definition of knowledge is absent. As early as 1959, the English economist and organisation analyst Edith Penrose emphasised the growing importance of knowledge in economy, but in addition she admitted that the whole subject of knowledge is so "slippery" that it is impossible to get a firm grip of it ([Penrose 1959: 77](#pen59)). In 1969 Peter Drucker announced that knowledge has become the central capital, cost centre and basic resource of the economy ([Drucker 1969: ix](#dru69)). Still however he did not suggest how to appropriately define this basic resource. Approximately thirty years later, Luhmann correctly summarised: " ...was is Wissen? Wenn man von der Gesellschaftstheorie ausgeht und selbst wenn man die moderne Gesellschaft als 'Wissensgesellschaft' bezeichnet, findet man keinen brauchbaren Begriff des Wissens." ([Luhmann 2002: 97](#luh02))

However, in some of the theories subscribing to the knowledge society idea, definitions of knowledge have been suggested. Still, to my mind these definitions are not adequate.

Sometimes, often in relation to information and communication technologies (ICTs), knowledge is defined as an essence or substance, cf. for instance the OECD report from 2004, _Innovation in the Knowledge Economy_, which focuses on "implications for education and learning". Here, it is emphasised that it is important to have a clear idea of "...what it is that is passing through the electronic pipelines: knowledge, information or data?" ([OECD 2004: 18](#oec04)). However, the challenges of education and learning - why doesn't teaching automatically lead to adequate learning, if teaching is only a matter of transporting knowledge? - and of knowledge sharing - why is knowledge sharing actually most often _not_ happening automatically? - cannot be answered if it is assumed that knowledge is a substance that can easily be transported from one person to the other. It is well known that this is _not_ what happens in the classroom or in the knowledge-sharing organisation. Knowledge about something is a representation of something according to interpretation standards, which may change from person to person and from teacher to pupil. My knowledge is not equal to your knowledge, and it cannot be transported from me to you.

In other contexts, knowledge has been defined in a restricted way as certified knowledge. In his classical book about the post-industrial society Daniel Bell defined knowledge as "...a set of organised statements of facts or ideas, presenting a reasoned judgment or an experimental result, which is transmitted to others through some communication medium is some systematic form" ([Bell 1973: 175](#bel)). In his book about the network society, Manuel Castells has, as he says, "no compelling reason to improve on" this definition ([Castells 1996: 17](#cas96)). But certified knowledge is only one aspect of knowledge, as for instance Michael Polanyi has convincingly argued. Also, tacit knowledge - the knowledge of e.g., how to ride a bike - is knowledge, although it cannot be written down or "proved" and certified in any traditional scientific way.

In the 2004 OECD report this is reflected upon by making a distinction between on the one hand certified (tested) and practical (uncertified) knowledge, or in French: between "savoir" and "connaissance", and on the other hand between codified and tacit knowledge ([OECD 2004: 18ff](#oec04)), and although no systematic categorisation of knowledge forms is provided, at least it is made clear that the question of knowledge is complex.

Yet another systematisation has been suggested by Bengt-Aake Lundvall in the OECD 2000 report _Knowledge Management in the Learning Society_. Here he suggests a categorisation into four forms of knowledge:

*   Know-what that refers to knowledge about facts;
*   know-why that refers to knowledge about principles and laws governing facts;
*   know-how that refers to skills, i.e. abilities to do something with one's factual knowledge;
*   know-who that refers to the ability to trace knowledge providers across disciplines and specialisations ([OECD 2000: 14f](#oec00)).

While I agree to some of these categories, I think the fourth knowledge form, "know-who" falls outside the paradigm hidden behind the categories.

A different theory of knowledge and knowledge categories has been developed by Max H. Boisot ([1995](#boi95) and [1998](#boi98)). In a way that can be compared with the one that I am proposing, Boisot conceptualizes knowledge as "...a set of probability distributions held by an agent and orienting his or her actions." ([Boisot 1998: 12](#boi98)). Compare this with my definition of the function of knowledge in the next section. Boisot suggests a typology of knowledge depending on whether it is diffused/undiffused and codified/uncodified. This leads into four categories of knowledge: Personal knowledge (undiffused and uncodified), common-sense knowledge (diffused and uncodified), proprietary knowledge (undiffused and codified), and public knowledge (diffused and codified) ([Boisot 1995: 145-149](#boi95)). Based on these categories and adding a third dimension, i.e. abstraction, Boisot has suggested a description of the use and distribution of knowledge in organisations within the so-called Information-Space or just I-Space. In particular, a social learning cycle can be identified as a movement of knowledge - or information - within the I-Space (cf. [Boisot 1995: 184ff](#boi95)).

My conclusion on this brief review of existing sociological knowledge theories is that we must leave the model of knowledge as an essence, which can be transported from place from place, i.e. from the research laboratory to the enterprise. Similarly, we must give up the idea that knowledge as suggested by Bell and Castells can be defined only as certified knowledge. For me, the concept of knowledge is multidimensional, and it cannot be perceived as something, which is created and certified in the ivory tower of research and then - sometimes via the educational sector - transferred to society in general and to the business sector in particular.

## The mystery of knowledge

### The weird world of "mind vs. reality"

I hope that it is by now obvious that there is a job to do in order to unveil the mysteries of knowledge, and that this job is both important and demanding.

One of the problems of understanding knowledge and of developing a sociologically adequate concept of knowledge is that there is a mismatch between the understanding of society and the understanding of knowledge. While society is described in its current form as "post-industrial", "post-modern" etc., assuming that realities have changed during the latest hundred years, knowledge is still understood through classical epistemologies. The problem is a problem concerning theoretical a-synchronicity. I will briefly demonstrate that the understanding of knowledge is based on an epistemology developed by Descartes and classical philosophy, while the understanding of society is post-Cartesian: It is - although most often implicitly - based on 20th century sociological theories informed by Husserl, Heidegger and others.

According to Cartesian philosophy, the world can be divided into _res cogitans_ and _res extensa_. The thinking subject versus the external - not-thinking - world. Consequently, knowledge is the result of a correspondence between mind and world.

Thus, to know something is to establish a link or a correspondence between mind and reality. Just listen to the words: A "link". "Mind versus reality" - as if mind isn't reality, and as if thoughts are models "corresponding" to an external world. According to this world-view, to know something is to transport knowledge from the external world into the mind. Consequently, knowledge is the store of facts (in the computer age: the memory of information), and to share knowledge is to transfer knowledge from one file to another.

Still, most epistemological theory implicitly assumes that this weird world of correspondences, of minds outside reality and realities outside mind, constitutes the indisputable precondition for talking about knowledge. You may disagree about what comes first. You may be "realist" or "antirealist". Still, however, the very distinction between mind and reality is beyond discussion (for a recent example see [Klausen 2004](#kla04)).

In accordance with this theory, modern knowledge management theory defines knowledge as a substance. Knowledge management is equal to management of physical processes. It is a theory about how to file, to transport and to provide access to knowledge substances.

### Critique of "the ghost in the machine" paradigm

Inspired by e.g., Edmund Husserl's critique of the Cartesian philosophy, in 1949 the English language philosopher Gilbert Ryle reacted against this paradigm. He characterised the idea that there should exist a certain thinking device, which did not belong to the world, as "the ghost in the machine" paradigm. As a result of his criticism, Ryle concluded that knowledge cannot primarily be understood as knowledge of something. With his famous statement in _The Concept of Mind_ "knowing-that" presupposes "knowing-how" ([Ryle 1949](#ryl49)). In order to know that something is the case, one must know the conditions on which it is the case. Facts are not simply facts, but they are facts according to an attitude or a point of view, which could be otherwise. The innocence of pure knowledge has been lost.

With his critique of pure knowledge and with his addition of knowing-how to knowing-that, Ryle broke the curse of knowledge. Knowledge is more than knowing-that. He passed from first order to second order knowledge. i.e. from simple knowledge (knowledge of something) to reflexive knowledge (knowledge of knowledge). However, he did not reach the third or fourth orders of knowledge, i.e. the orders of knowledge in which the mystery of knowledge occurs: The orders in which new knowledge is created.

Recently, Claus Otto Scharmer has suggested that within knowledge management theories in addition to talking about explicit knowledge, which equals certified knowledge, one should include two additional categories: "processual" knowledge and "emerging" knowledge ([Scharmer 2001](#sch01)). Furthermore, the French philosopher Michel Serres had argued that it is not sufficient just to develop a categorisation of knowledge dimensions. No, the very "nature" of knowledge should be reconsidered. Knowledge cannot be understood as a fixed, centripetal field, such as it is assumed in the encyclopaedic tradition, in which one aimed at creating a finite, universal and all-inclusive file of knowledge. No, knowledge has to be understood as an unlimited, growing and dynamic polycentric system (cf. [Serres 1997](#ser97) [1991]).

## Definition of knowledge

However, before developing a system of categories of knowledge one has to suggest a definition of knowledge _per se_. What is knowledge? For me, a very simple, yet practical and applicable sociological definition of knowledge is that knowledge is confirmed observations. Observations may be confirmed over time or in society. When I observe something and then repeat my observation with the same result it becomes a confirmed observation and thus: personal knowledge. Similarly, when I observe something and another person can confirm this observation it becomes social knowledge.[4](#note4)

This implies that knowledge is not a quality of the world, but a quality of observing the world. Knowledge isn't something that we find "out there", but something that is created by observing the world and by comparing world observations over time and among different observers, bearing in mind, of course, that the observer is part of the observed world (cf. [von Foerster 1984](#foe84)). Thus, knowledge isn't created and re-created from moment to moment, but is always a matter of confirmation of observations through repeated self-observations and through communication of others' observations. Thus, although being relatively stable, knowledge systems are always dynamic and self-developing, and different mechanisms have been created to establish such stable, but dynamic systems. The mass media system is one such system, which from day to day confirms, modifies and challenges "yesterday's knowledge". One might even suggest that so-called consensus is the contingent result of mass media mediated knowledge production. The scientific system is another knowledge producing functional system in society. The scientific system has developed very explicit and specific criteria for knowledge production, i.e. for the confirmation of observations (so-called truth criteria) and - consequently - for what counts as scientifically confirmed knowledge.

Consequently, knowledge may change over time or between social systems: knowledge of one society or organisation may be different from the knowledge of another society or organisation. For instance our current knowledge about the system of planets is different from the knowledge of the system of planets in a traditional, pre-modern society. _We_ know that the sun is the centre of the planetary system, and that the universe is expanding. 600 years ago "_they_" knew that the earth was the centre of the planetary system, and that the universe had a fixed size, surrounded by a transparent shell with stars. In 200 years from now, yet new systems of observations will have been confirmed and thus transformed into knowledge. Thus, knowledge is contextual, which explains why knowledge sharing is not just a question of transmitting facts, but is also a question about the negotiation of a shared knowledge context.

Knowledge, then, is defined as confirmed observations. However, knowledge does not only concern observations. It also concerns actions. One may also confirm one's actions: if I do this or that, I know what will happen, because prior to the present situation this very action has been repeated by myself or by others with the same result. This type of knowledge might be termed "practical knowledge", or in the tradition of Polanyi: tacit knowledge. I would however suggest that it is named: skills. Skills are confirmed actions. Together, skills and knowledge constitute the totality of abilities.

One of the important implications of this definition of knowledge is that knowledge is always reflective. The knowing subject is not excluded from the extended world - the "res extensa" - but is always already included in this world. Consequently, knowledge is always both knowledge about something in the world and knowledge about itself. I fully agree with Ryle in emphasising that knowledge includes both knowing-that and knowing-how. One must always ask how one's knowledge about something is constituted the way it is.

This implies that knowledge is dynamic, and that the "dynamics" of knowledge has two sources: firstly knowledge may change, because the world changes. But secondly knowledge may change, because the way in which we observe the world changes. Furthermore, these two sources of change are interconnected: The observing or acting subject is him- or herself part of a changing world. As we shall soon see, this constitutes the theory of the categories of knowledge. Adding to know-that and know-how, one must also consider _why_ our knowledge is, as it is. We know what we know according to collective paradigms of knowledge, and again these paradigms change over time (cf. [Kuhn 1967](#kuh67)). Finally, one must consider the totality of what is known and can currently be known, i.e. our knowledge culture or knowledge horizon. Inspired by Gregory Bateson, this fourth form of knowledge can be characterised as an evolutionary fact.

## Function of knowledge

Knowledge can be defined as confirmed observations. But what is the function of knowledge? Why is knowledge developed?

Here, I would suggest an evolutionary approach. I understand the development of knowledge as a special case of the general "Morphogenese von Komplexität" ([Luhmann 1997: 415](#luh97)).

The basic hypothesis is that human beings and social systems survive and develop by reproducing the distinction between system and environment, that is by managing external complexity. But external complexity can only be managed through the development of internal complexity, cf. the general statement that "[o]nly complexity can reduce complexity" ([Luhmann 1995: 26](#luh95), [1984: 49]).

Thus, for me knowledge is a certain form of internal complexity. It is the sum of confirmed observations. According to this functional definition knowledge is a resource of all meaning-based systems, i.e. both psychic and social systems. The function of knowledge is to manage complexity.[5](#note5) Of course, in order to accumulate knowledge, other resources must be used (or developed), such as books, libraries, files etc. of the social system, and such as neural media of the psychic system. However, one does not find knowledge "in" books and files, or "in" the neurons (if this were the case, we would be back in an essential definition of knowledge), but structural couplings must be exercised between the psychic system and the brain, and between the social system and books, libraries etc.

According to the classical epistemology, a clear distinction could be made between the internal system of knowledge and the environment. According to this epistemology, the accumulation of knowledge could be understood as a closed and finite process, which happened during the "formative years" and primarily within the educational system. Here, according to this understanding, a fixed knowledge system was being built. This understanding has informed the industrial society, in which skills were accumulated through externally stimulated learning. This constitutes the learning-to-know paradigm.

However, according to a post-Cartesian epistemology, knowledge must be applied not only to the environment, but also to itself. Knowledge is used in order to manage external complexity, but it is also and simultaneously used in order to manage its own complexity, i.e. to manage "eigen-complexity". Thus, the system of knowledge is characterised by not only being complex, but by being hypercomplex (cf. [Qvortrup 2003](#qvo03)). Knowledge is not a stable resource, but a dynamic, self-developing resource, constituting the learning-to-learn paradigm, and confirming the well-known sentence: The more we know, the more we know that we do not know.[6](#note6)

## Categories of knowledge

Inspired by the phenomenological critique of the Cartesian paradigm, and particularly influenced by the American anthropologist and epistemologist Gregory Bateson, in my book, _The Knowing Society_, with the subtitle: "The mystery of knowledge, learning and culture" ([Qvortrup 2004](#qvo04)) I have systematised the categories of knowledge with a special focus on the third category of knowledge, creative knowledge.

In accordance with Ryle's critique of Cartesian dualism, in the 1960s Bateson suggested that learning and communication can be divided into four categories: first, second, third and fourth order learning ([Bateson 2000](#bat00) [1972])[7](#note7). Taking inspiration in Bateson's categorisation, from a post-Cartesian or - more specifically - a systems theoretical approach one can identify four forms of knowledge. First order knowledge is simple knowledge: Knowledge about something. Second order knowledge is knowledge about knowledge, i.e. reflective or situative knowledge. This category corresponds to Ryle's "knowing-how". Third order knowledge is knowledge about knowledge about knowledge, i.e. knowledge about the preconditions for reflective knowledge. Finally, one can identify a fourth category of knowledge, which represents the social evolution of knowledge, i.e. the collective and perhaps unconscious knowledge process and the total knowledge potential. This is closely related to what Edmund Husserl called the meaning horizon of society.

The theory presented in The Knowing Society builds on this idea. Its main claim is that a mundanised subject observing the world in order to know about the world - and all subjects are mundanised subjects - must make the following forms of observation:

*   it must observe the world as an object of observation
*   it must observe itself in the world
*   it must observe the world (including itself) as a precondition for observing the world.

In addition, this theory of knowledge presupposes that the world, including the subjects observing the world, exists as a knowledge horizon, i.e. as a totality of what can be known. The theory furthermore presupposes that this world is dynamic, i.e. that it changes in unforeseen directions (this is the result of its hypercomplexity, cf. [Qvortrup 2003](#qvo03)) and thus makes change of knowledge possible. It isn't just a knowledge world, but also a knowing world. Society can not adequately be characterised as a "knowledge society", but it should be termed a "knowing society". Finally, the theory assumes that changes in the world take place by virtue of the world itself. Drawing a distinction between the subject and the world, the mundanised subject changes the world through its observation, and it is changed by the world through its observation: It transforms the world and is transformed thereby, whether this last change is called socialisation or learning.

This implies that four categories of knowledge can be identified:

<table>

<tbody>

<tr>

<th>Knowledge category</th>

<th>Knowledge form</th>

<th>Knowledge designation</th>

</tr>

<tr>

<td>1st order or simple knowledge</td>

<td>Knowledge about something</td>

<td>Factual or object knowledge</td>

</tr>

<tr>

<td>2nd order or complex knowledge</td>

<td>Knowledge about the conditions of knowing</td>

<td>Recursive or situative knowledge</td>

</tr>

<tr>

<td>3rd order or hypercomplex knowledge</td>

<td>Knowledge about the conditions of the reflexive knowledge system</td>

<td>Reflective or creative knowledge</td>

</tr>

<tr>

<td>4th order knowledge</td>

<td>World knowledge or the knowledge horizon</td>

<td>Evolutionary or world knowledge</td>

</tr>

</tbody>

</table>

Where the first forms of knowledge represent observation-based forms of knowledge, i.e. relations between subject and world (including the subject's knowledge of itself as a subject in the world), the fourth form of knowledge is not knowledge about the world but the world as knowledge.

*   First order knowledge is knowledge about something. For instance, I know that from where I am sitting I can see a beautiful bed of rhododendrons, and I know that the large plant in the middle is a Rhododendron Cawtabiensis.
*   Second order knowledge is knowledge about knowledge, i.e. the capacity for self-observation. It is called "recursive knowledge", because it is knowledge applied to itself, and "situative knowledge", because it is the ability to use knowledge in specific situations. Often, this is called _competence_. Not only do I know that the shrub out there is a Rhododendron Cawtabiensis, but I also know that I know it because I had to elaborate my wishes to the owner of the nursery I bought it from. I also know that the fact that I consider it beautiful may be because I planted it myself. In other words, I am not only capable of categorising what I see, but I also have the ability to stand next to my own observation and consider it.
*   Third order knowledge is knowledge about knowledge about knowledge, i.e. knowledge about the system of knowledge that first order knowledge is based on. It is called reflexive knowledge, because it is knowledge about how to reflect the normally hidden assumptions of common knowledge. Often, this is called creativity, because it is the ability to change the assumptions or the "schemata" behind common knowledge. I know what I know according to a knowledge system or paradigm. It can, for instance, be knowledge of the botanical systematics, which lead to the designations of species that I employ. Or it can be knowledge of the aesthetic criteria for beauty, which make me find my garden beautiful. My aesthetic preferences could be caused by a predilection for English garden aesthetics rather than the more formal French or Italian classicist garden aesthetics.
*   Finally, fourth order knowledge is knowledge transcending the preconditions for the knowledge systematic. i.e. the totality of knowledge as an evolutionary fact. One sometimes says that it is represented by the entire cultural system, into which these knowledge forms and judgements of taste are embedded. Following Bateson's categories of learning and communication, this fourth order knowledge is a very particular form of knowledge, which cannot be contained within one person but resides in the social community of which the individuals are members.

Adding to these four knowledge categories a distinction should be made between codified and non-codified knowledge, or between skills and knowledge. Skills can be defined as tacit abilities, while knowledge can be defined as codified abilities. Again, however, skills can be divided into four categories:

*   ready-at-hand skills (the simple ability to e.g., use a hammer);
*   situative skills (the ability to solve problems by using an instrument, e.g., using a hammer as a bottle opener);
*   systemic skills (the ability to practically reflect on the use of e.g., different tools, which can be found among skilled practitioners);
*   the culture of skills (which is established in a workshop with different specialities, e.g., in an orchestra with specialised musicians).

The total system of abilities, divided into skills and knowledge forms, looks like this:

<table>

<tbody>

<tr>

<th>Forms of connaissance/skills</th>

<th>Forms of savoir/knowledge</th>

</tr>

<tr>

<td>Ready-at-hand skills</td>

<td>Factual knowledge</td>

</tr>

<tr>

<td>Situative skills</td>

<td>Situative knowledge</td>

</tr>

<tr>

<td>Systemic skills</td>

<td>Systemic knowledge</td>

</tr>

<tr>

<td>The culture of skills</td>

<td>The culture of knowledge</td>

</tr>

</tbody>

</table>

## Functions of the different knowledge categories

What functions do the different knowledge categories support? In order to provide a short answer to this question I would give an example. I would refer to one of the modern, knowledge-based enterprises in Denmark, ECCO Footwear. ECCO Footwear started in the 1960s as a footwear production enterprise in Denmark, employing hundreds of unskilled industrial workers. Currently, however, footwear is only designed and the total, global footwear production system is managed in Denmark, while the physical production of footwear is performed at factories in China, Indonesia and elsewhere. Consequently, within a Danish perspective, the ECCO Footwear company is a typical, knowledge-based enterprise.

What skills must those employed at a knowledge-heavy company such as ECCO Footwear possess? It would be easy to follow Richard Florida in saying that they should be "creative" workers. But although I agree with Florida, when he says that the concept of creative workers "...has a good deal more precision than existing, more amorphous definitions of knowledge workers, symbolic analysts or professional and technical workers" ([Florida 2004: 9](#flo04)), not only is his category of creative workers too narrow; his rather romantic idea of creativity as something emerging from social and cultural diversity is not appropriate. Yes, creativity is part of the competencies of an employee at ECCO Footwear, but there is more to it than just creativity. Here, the categories - and the systematics - presented above can prove their usefulness.

Employees at a knowledge-heavy company such as ECCO Footwear must have considerable _factual knowledge_, i.e. a whole series of technical and professional qualifications: Designers at ECCO have to have design knowledge, knowledge of materials, and be able to use advanced digital tools.

They must have considerable reflexive or _situative knowledge_. They must be able to work in teams, to handle unexpected situations with their colleagues, to improvise and empathise.

They must have _systemic knowledge_. They must constantly be able to rise above the I-you situation of the group and see things from the outside, identifying and re-interpreting even basic assumptions that are perhaps not so self-evident as they appear at first glance. This is a prerequisite for being able to act creatively: to be able - taking the design and production of footwear as an example - to understand that shoes are not just shoes but narratives about and self-stagings of the person wearing them. One is not designing a functional technology but a culture-historically based tool for identity-construction and -reflection.

Lastly, they must be part of what I have referred to as _metasystemic knowledge_, or _knowledge culture_. For instance, they must be able to adopt an attitude towards the company culture of which they are a part, yet only can play a role in and contribute to by being different: the value of modern knowledge workers presupposes that they contribute to the organisation by being different. They are included by being exclusive.

## How does one facilitate the different forms of knowledge?

Formerly, indeed, until quite recently, knowledge was thought of as an extended field that admittedly grew and grew, but which did so from a centre and that also hereby approached the boundary of possible knowledge. Therefore the concept of all knowledge - the 'encyclopedia' - is derived from the word 'circle', i.e. the form that is defined as having a centre. Similarly, the institution where new knowledge is produced is stilled referred to as a 'university' and not a 'multiversity', i.e. the place where one's gaze is in one direction, toward the source of all knowledge.

Today, though, according to Michel Serres, we know that knowledge does not have a fixed order and a centre, but that knowledge arises and exists by virtue of its "...momentum, the energy in its motion" ([Serres 1997: 38](#ser97)). We know that the person who believes himself to be standing at the centre of knowledge is blind, for he cannot see what he cannot see. "The person who considers himself perfect can neither see or feel a limit and therefore does not understand the ardent need to transcend an inaccessible boundary, whose position he is uncertain about." ([Serres 1997: 16](#ser97))

The consequence is that digital networks can not only be conceived as transport systems that, according to various principles, since they are capable of linking the individual nodes in the networks, can ensure the best possible transportation and distribution of certified knowledge. No, it is also possible to identify a structural relationship between forms of knowledge and forms of network.

Knowledge of the first order is created and exchanged via simple point-to-point networks. Factual knowledge or 'object-knowledge' of the one network node can be passed on to the second node via a simple transfer-relation between the two nodes; similarly, first order knowledge can be distributed in organisations via hierarchical one-way networks.

Second order knowledge is defined as knowledge about knowledge, for example, as knowledge concerning the ability to use factual knowledge for problem-solving purposes. Here, one not only has to know something but possess a methodic knowledge, i.e. a knowledge of how the object knowledge in question is to be used. To be able to create and exchange second order knowledge it is therefore necessary to establish a recursive relation between transfer and code or, in network terminology, between network transfer and network protocol.

Third order knowledge is defined as knowledge about knowledge about knowledge. For example, knowledge of the way in which the relation between object-knowledge and problem-solving knowledge is conditioned. Third order knowledge is knowledge about how codes - or in knowledge terminology: methods - are conditioned. In functionalist organisation theory one speaks of 'basic assumptions' (cf. Schein), i.e. what one does not know that one knows. In sociological terminology, third order knowledge is based on the distinction between codes and social system; in Castells' terminology we are dealing with the distinction between protocol and space of flows.

Finally, fourth order knowledge is defined as the totality of world knowledge: all that can be known. In Castell's terminology this would refer to the - invisible - distinction between networks and non-networks, i.e. the transcendental conditions for networks.

<table>

<tbody>

<tr>

<th>Knowledge category</th>

<th>Network category</th>

<th>Communication/observation category</th>

</tr>

<tr>

<td>1st order knowledge</td>

<td>Simple point-to-point coupling</td>

<td>Transfer/non-transfer - simple observation as indication/distinction</td>

</tr>

<tr>

<td>2nd order knowledge</td>

<td>Network/protocol</td>

<td>Transfer/code - the recursive observation, or the observation of codes as a precondition for transfer</td>

</tr>

<tr>

<td>3rd order knowledge</td>

<td>Protocol/space of flows</td>

<td>Code/social system - the observation of the emergence of codes through the operation of a social system</td>

</tr>

<tr>

<td>4th order knowledge</td>

<td>Network/non-network</td>

<td>Society/transcendentality - the observation of society from a position outside society (the transcendental observer)</td>

</tr>

</tbody>

</table>

## Conclusion: the library as a knowledge institution

The initial hypothesis of this article was that the basic function of a modern library is not to provide access to as much information as possible, but to provide knowledge management services. The basic function is to help the citizen with managing information overload.

Consequently, we must understand what "knowledge" is in the so-called knowledge society. Thus, the brief conclusion of the above is that in order to establish and organize a modern library we must know what knowledge is, in which forms it appears, and how the different forms of knowledge are best facilitated. In this article the basic principles and categories of such a theory have been suggested.

## Notes

1.  <a id="note1"></a>Some would say that this is an anachronistic statement. However, I refer to the theory that the basic change from traditional to modern society started during the renaissance. See e.g., [Cassirer 1927](#cass27) and many other renaissance studies. See also [Qvortrup 1996](#qvo96), [1998](#qvo98) and [2003](#qvo03).
2.  <a id="note2"></a>Cf. the national Danish library strategy to "turn information into knowledge".
3.  <a id="note3"></a>The following builds on [Qvortrup 2006](#qvo06).
4.  <a id="note4"></a>Through the process of confirming observations, as a matter of fact observations are condensed: Many observational operations are condensed into one operation, and this operation can be called: Knowledge. This is the way in which knowledge is defined by Niklas Luhmann, who suggests "...Wissen als Kondensierung von Beobachtungen zu bezeichnen". ([Luhmann 1990: 123](#luh90))
5.  <a id="note5"></a>Cf. Nico Stehr who emphasizes that knowledge is a model for reality, not a model of reality ([Stehr 2006: 31](#ste06))
6.  <a id="note6"></a>Concerning the dynamism of knowledge see: [Serres 1997](#ser97).
7.  <a id="note7"></a>See also the detailed review of Bateson's categories of learning in [Qvortrup 2001](#qvo01).

## References

*   <a id="bat00"></a>Bateson, G. (2000 [1972]). The logical categories of learning and communication. In: Gregory Bateson: _Steps to an ecology of mind_. Chicago: The University of Chicago Press.
*   <a id="bel73"></a>Bell, D. (1973). _The coming of post-industrial society_. New York: Basic Books.
*   <a id="boi95"></a>Boisot, M. H. (1995). _Information space. A framework for learning in organizations, institutions and culture_. London: Routledge.
*   <a id="boi98"></a>Boisot, M. H. (1998). _Knowledge assets. Securing competitive advantage in the information economy_. Oxford: Oxford University Press.
*   <a id="cass27"></a>Cassirer, E. (1927). _Individuum und Kosmos in der Philosophie der Renaissance_. Leipzig: B. G. Teubner.
*   <a id="cas96"></a>Castells, M. (1996). _The rise of the network society_. Malden, Ma.: Blackwell Publishers.
*   <a id="dru69"></a>Drucker, P.F. (1969). _The age of discontinuity; guidelines to our changing society_. London: Heinemann
*   <a id="flo04"></a>Florida, R. (2004) _The Rise of the Creative Class_. New York: Basic Books.
*   <a id="foe84"></a>Foerster, H. von (1984). _Observing systems_. Seaside, Ca.: Intersystem Publications.
*   <a id="kla04"></a>Klausen, S. H. (2004). _Reality lost and found. An essay on the realism-antirealism controversy_. Odense: University Press of Southern Denmark.
*   <a id="kuh67"></a>Kuhn, T. S. (1967). _Die Struktur wissenschaftlicher Revolutionen_. Frankfurt a. M.: Suhrkamp Verlag.
*   <a id="luh90"></a>Luhmann, N. (1990). _Die Wissenschaft der Gesellschaft_. Frankfurt a. M.: Suhrkamp Verlag.
*   <a id="luh95"></a>Luhmann, N. (1995). _Social systems_. Stanford, Ca.: Stanford University Press. [first German version 1984]
*   <a id="luh97"></a>Luhmann, N. (1997). _Die Gesellschaft der Gesellschaft_. Frankfurt a. M: Suhrkamp Verlag.
*   <a id="luh02"></a>Luhmann, N. (2002). _Das Erziehungssystem der Gesellschaft_. Frankfurt a. M.: Suhrkamp Verlag.
*   <a id="oec00"></a>OECD (2000). _Knowledge management in the learning society_. Paris: OECD.
*   <a id="oec04"></a>OECD (2004). _Innovations in the knowledge economy_. Paris: OECD.
*   <a id="pen59"></a>Penrose, E. (1959). _The theory of the growth of the firm_. Oxford: Oxford University Press.
*   <a id="qvo96"></a>Qvortrup, L. (1996). _Mellem kedsomhed og dannelse_ [between boredom and "bildung"]. Odense: Odense Universitetsforlag.
*   <a id="qvo98"></a>Qvortrup, L. (1998). _Det hyperkomplekse samfund_ [The hypercomplex society]. Copenhagen: Gyldendal.
*   <a id="qvo01"></a>Qvortrup, L. (2001). _Det lærende samfund_ [The learning society]. Copenhagen: Gyldendal.
*   <a id="qvo03"></a>Qvortrup, L. (2003). _The hypercomplex society_. New York: Lang Publishers.
*   <a id="qvo04"></a>Qvortrup, L. (2004). _Det vidende samfund_ [The knowing society]. Copenhagen: Unge Pædagoger.
*   <a id="qvo06"></a>Qvortrup, L. (2006). _Knowledge, Education and Learning - E-learning in the Knowledge Society_. Copenhagen: Samfundslitteratur Press.
*   <a id="ryl49"></a>Ryle, G. (1949). _The concept of mind_. London: Hutchinson.
*   <a id="sch01"></a>Scharmer, C. O. (2001). Self-transcending knowledge: Organizing around emerging realities. In: Ikujuru Nonaka and David Teece (eds.): _Managing industrial knowledge. Creation, transfer and utilization_. London: Sage Publications,
*   <a id="ser97"></a>Serres, M. (1997[1991]). _The troubadour of knowledge_. Ann Arbor: The University of Michigan Press.
*   <a id="ste06"></a>Stehr, N. (2006). Grenzenlose Wissenswelten? In: Gertraud Koch (ed.): _Internationalisierung von Wissen. Multidisziplinäre Beiträge zu neuen Praxen des Wissenstransfers_. St. Ingbert: Röhrig Universitätsverlag.