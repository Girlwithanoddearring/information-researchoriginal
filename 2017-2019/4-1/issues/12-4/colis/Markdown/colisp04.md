#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Poster abstract

# Human-agent interaction from the perspective of information behaviour and usability

#### [Mirka Grešková](mailto:mirka.greskova@fphil.uniba.sk)  
Department of Library and Information Science, 
Comenius University, 
Bratislava, 
Slovakia

## Introduction

The research project is aimed at analysis of human-agent interaction from the perspective of information behaviour, interactive aspects and system design with respect to usability contexts. We focus on the study of user aspects from socio-cognitive perspective. In our pilot study we studied interaction of users with information search agent Copernic. We would like to support interdisciplinary knowledge interchange and bridge user and system views. Design of systems which takes into account user behaviour and the needs supports efficiency of information search with the impact on increasing relevance of search results. We stated the following general research questions: Which processes are realized during human-agent interaction from the user's perspective? How do respondents verbalize and conceptualize human-agent interaction? Which solutions will support human-agent interaction?

## Theoretical framework

### The cognitive paradigm of information science

Cognitive sciences have been applied in information science since 1980s. De Mey introduced the cognitive view in the Workshop on Cognitive Viewpoint ([Ingwersen 1992](#ing92)). The cognitive _turn_ is characterized by three revolutions ([Robertson and Hancock-Beaulieu 1992](#rob92)): cognitive, relevance and interactive. Taylor's model of information need creation, the ASK framework, Allen's ([1991](#all91)) analysis of cognitive skills, processes and styles, poly-representation ([Ingwersen 1992](#ing92), [2005](#ing05)) - all these theories and conceptual frameworks contributed to the development of the cognitive paradigm of information science. Ingwersen and Järvelin ([2005](#ing05)) describe the turning points involved in the cognitive view bridging system-oriented and information seeking approaches. They mention a shift from individualism to the user-driven approach. Transfer and process aspects were emphasized in the 1980s. These approaches were transformed to the holistic cognitive view. Cognitive view is reflected in the conceptions of information behaviour, the interactive approach and system design. Our aim is to synthesize these conceptions and its outcomes in our research.

#### Information behaviour

Kuhlthau ([1993](#kuh93)) describes information behaviour as "seeking of the meaning" and Dervin ([1983](#der83)) understands it as 'sensemaking'. information behaviour foundations are embedded in the models of information seeking introduced by Ellis ([1989](#ell89)), Kuhlthau ([1991](#kuh91)), Wilson and Walsh ([1996](#wil96)). Nahl ([2001](#nah01)) distinguishes ACS (Affective - Cognitive - Sensomotoric) as the unit of the information behaviour and characterizes it in more detail. Wilson ([2006](#wil06)) introduces the activity theory as a framework relevant for information behaviour field and develops an inspiring process model of activity.

#### Interactive approach

The interactive approach has been developed since research in the area of database systems was conducted. Berrypicking ([Bates 1989](#bat89)), interactive processes and feedback ([Fidel 1991](#fid91)), collaborative information retrieval and task-based analysis created consistent bases for interactive research. Episodic interaction model introduced by Belkin ([1995](#bel95)), Saracevic's stratified model ([1997](#sar97)) or feedback model ([Spink 1997](#spi97)) are among the best-known interactive models. Ingwersen and Järvelin ([2005](#ing05)) present cognitive framework for information seeking and retrieval and analytical models representing relations of information objects, systems, interfaces and cognitive actors in the context.

#### Information systems design applications

Usability engineering, socio-technical design and cognitive system engineering frameworks bridge cognitive approach with system design. Fidel and Pejtersen ([2004](#fid04)) apply Rasmussen's framework of cognitive work analysis, which explains 'human-information interaction in the context of human work activities'. Conceptions of interface design, human-computer interaction and usability are relevant to our research problem.

## Methods

Our empirical research applies qualitative approach. Data collection was mediated by think-aloud method, semi-structured interviews and observations. Think-aloud protocols and interviews were recorded and accompanied by screen logging in our pilot study. We analysed the transcriptions, screen logs and observation notes. Synthesis resulted in tables, summarized model, process diagrams, description of usability aspects and further research proposal.

## Pilot study

The pilot study was conducted between February and the end of March, 2007\. Two students from the fourth grade of Department of Librarianship and Information Science, Comenius University were selected as a sample. Advanced experience in information search and English language skills were defined as the sampling criteria. Copernic Agent Professional, a personal search agent for information professionals, was used to study human-agent interaction. CamStudio software was used to create screen recordings. Respondents were trained to think aloud during interaction in preparatory phase. The research team consisted of three researchers. One of them interviewed the respondents and asked them to think aloud when completing tasks. Another two researchers conducted observations.

The initial interview was focused on identification of respondents. Then, respondents were asked to think aloud when solving tasks related to: searching, filtering, results sorting, grouping of results, summarization, annotating, adding to favourites, tracking of results. The concluding interviews were focused on the problems that emerged during human-agent interaction, on limitations of the interface, degree of satisfaction with the agent and solution proposals.

Collected data (recordings, observations) were transcribed in text form. Later on we analysed transcriptions following defined coding scheme. Coded concepts were transferred to tables. We grouped the concepts to categories and tried to sort them in more detail. As a result the following general model was developed based on the first task.

<figure>

![Figure 1\. Model of human-agent interaction during the information search.](../colisp04fig1.jpg)</figure>

Activities of respondents were featured in the diagrams. Affective components of information behaviour during interaction were also recorded. Our further work will be focused on more detailed analysis of fundamental concepts, interaction and interface design. We will compare human-agent interaction of novices and professionals in further empirical research. Results from analysis will lead to the concluding conception which will be aimed at solution of usability problems connected with the agent interface and interaction. The conception will provide information useful for user-oriented interface design rising from the cognitive and affective components of information behaviour during human-agent interaction.

## Acknowledgements

The author acknowledge the VEGA SR - research task Information Use by IB in Education and Science for funding this research. I would like to express my gratitude to Jela Steinerova, Eva Kristofieova, Jan Gondol from the Department of LIS, Comenius University at Bratislava and respondents.

## References

*   <a id="all91"></a>Allen, B. (1991). Cognitive research in information science: Implications for design. _Williams, Martha E., ed. ARIST_, **26**, 3-37.
*   <a id="bat89"></a>Bates, M. J. (1989). _[The Design of Browsing and Berrypicking Techniques for the Online Search Interface.](http://www.gseis.ucla.edu/faculty/bates/berrypicking.html)_ Retrieved 2 April, 2006 from http://www.gseis.ucla.edu/faculty/bates/berrypicking.html
*   <a id="bel95"></a>Belkin, N. J. et al. (1995). Cases, scripts and information seeking strategies: On the design of interactive information retrieval systems. _Expert Systems with Applications_, **9**, 379-395.
*   <a id="der83"></a>Dervin, B. (1983). _[An overview of sense-making research: concepts, methods and results to date.](http://communication.sbs.ohio-state.edu/sense-making/art/artdervin83.html)_ Paper presented at the International Communications Association Annual Meeting. Dallas, Texas, USA, May 1983\. Retrieved 2 April, 2006 from http://communication.sbs.ohio-state.edu/sense-making/art/artdervin83.html
*   <a id="ell89"></a>Ellis, D. (1989). A behavioural approach to information retrieval system design. _JoD_, **45** (3), 171-212.
*   <a id="fid91"></a>Fidel, R. (1991). Searchers' selection of search keys I-III. _Ingwersen, P., Järvelin, K.: The Turn: Integration of Information Seeking and retrieval in Context._ Springer, 2005, 218.
*   <a id="fid04"></a>Fidel, R., Pejtersen, A. M. (2004). [From information behaviour research to the design of information systems: the Cognitive Work Analysis framework.](http://InformationR.net/ir/10-1/paper210.html) _Information Research_, **10** (1), paper 210\. Retrieved 5 January 2006 from http://InformationR.net/ir/10-1/paper210.html
*   <a id="ing92"></a>Ingwersen, P. (1992). _[Information retrieval interaction.](http://www.db.dk/pi/iri/)_ London: Taylor Graham, 1992\. 246 p. Retrieved 4 February 2005 from http://www.db.dk/pi/iri/
*   <a id="ing05"></a>Ingwersen, P., Järvelin, K. (2005). _The Turn: Integration of Information Seeking and retrieval in Context._ Springer, 2005, 448 p.
*   <a id="kuh91"></a>Kuhltau, C. C. (1991). Inside the Information Search Process: information seeking from the user's perspective. _JASIS_, **42** (5), 361-371.
*   <a id="kuh93"></a>Kuhltau, C. C. (1993). _Seeking meaning: a process approach to library and information services._ Norwood, NJ: Ablex, 1993, 199 p.
*   <a id="nah01" name="nah01"></a>Nahl, D. (2001). [A Conceptual Framework for Explaining Information Behavior.](http://www.utpjournals.com/jour.ihtml?lp=simile/issue2/nahlfulltext.html) _Studies in Media & Information Literacy Education_, **1** (2). Retrieved 5 January 2006 from http://www.utpjournals.com/jour.ihtml?lp=simile/issue2/nahlfulltext.html
*   <a id="rob92"></a>Robertson, S. E., Hancock-Beaulieu, M. (1992). On evaluation of IR systems. _Ingwersen, P., Järvelin, K.: The Turn: Integration of Information Seeking and retrieval in Context._ Springer, 2005, 3-4.
*   <a id="sar97"></a>Saracevic, T. (1997). [The stratified model of information retrieval interaction: Extension and applications.](http://www.scils.rutgers.edu/~tefko/ProcASIS1997.doc) _Schwartz, C., Rorvig, M. ASIS'97: Proceedings of the American Society for Information Science._ **34**, Silver Spring (Maryland): ASIS, 1997, 313-327\. Retrieved 6 January 2006 from http://www.scils.rutgers.edu/~tefko/ProcASIS1997.doc
*   <a id="spi97"></a>Spink, A. (1997). Information science: a third feedback framework. _JASIS_, **48** (8), 728-740.
*   <a id="wil06"></a>Wilson, T. D. (2006). [A re-examination of information seeking behaviour in the context of activity theory.](http://InformationR.net/ir/11-4/paper260.html) _Information Research_, **11** (4) paper 260\. Retrieved 16 February 2007 from http://InformationR.net/ir/11-4/paper260.html
*   <a id="wil96"></a>Wilson, T. D. (1996). [Information behaviour: an interdisciplinary perspective.](http://informationr.net/tdw/publ/papers/1999JDoc.html) _Wilson, T. D. Models in information behaviour research. JoD. 55 [1999]_, **3**, Retrieved 20 April 2006 from http://informationr.net/tdw/publ/papers/1999JDoc.html