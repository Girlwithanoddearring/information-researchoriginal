#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# Revisiting the user-centred turn in information science research: an intellectual history perspective

#### [Sanna Talja](mailto:sanna.talja@uta.fi), Department of Information Studies, University of Tampere, Finland  
#### [Jenna Hartel](mailto:jhartel@ucla.edu), Department of Information Studies, University of California, Los Angeles, USA

#### Abstract

> **Introduction.** This article revisits the narrative of the user-centred turn. Descriptions of the evolution of the user-centred approach in information science tend to lean towards accepting the interpretation of the decontextualized and system-centred nature of traditional information needs and uses studies presented in the seminal 1986 review article by Brenda Dervin and Michael Nilan.  
> **Method.** A sample of early user studies and comprehensive reviews of user studies written in 1950s and 1960s were reviewed and compared to the interpretative context set by the Dervin and Nilan 1986 article.  
> **Analysis.** This comparison captures some of the varying meanings and interpretations given to concepts such as information systems, information sources and information needs in different decades.  
> **Results.** Dervin and Nilan's description of the nature of traditional user studies drew major insights from other contemporary articles working toward building a better general conceptualization of information for information science. Dervin and Nilan did not incorporate in their review early user studies conducted between 1950s and 1970s, which would have mandated different conclusions.  
> **Conclusions.** Despite the fact that early user studies usually did have an implicit or explicit aim to support the flow of scientific information through the design of information systems or products, scholarly communication and reading habits were in many early user studies explored quite user-sensitively in their natural contexts. Studies by, for instance, Menzel, Parker and Paisley, Allen and Gerstberger, and Garvey focused on information and communication practices conceived very broadly, incorporating not only information seeking but also information receiving and sharing, reading, writing, and publishing activities.

## Introduction

The 1986 _Annual Review of Information Science and Technology (ARIST)_ review article, "Information Needs and Uses," written by Brenda Dervin and Michael Nilan, argued for a radical reconceptualization of the information needs and uses research area. Dervin and Nilan ([1986](#der06)) in fact called for a comprehensive shift away from the traditional paradigm that guides _information science_ research. Dervin and Nilan's call for a paradigm shift was to become the most cited article in the information behaviour research community ([White and McCain 1998](#whi01)).

Dervin and Nilan's seminal article is quite often cited as the turning point that led away from the study of users categorized and analysed according to systems features and variables towards a more holistic study of users from the viewpoint of users. In a summary of the contributions of the first Information Seeking in Context conference, Pertti Vakkari ([1997](#vak01)) noted how a qualitative grounded-theory influenced research approach focusing on contextual aspects of the user's situation had, almost entirely, replaced quantitative studies and earlier metatheories:  

> The old system-centred paradigm is metaphorically speaking water under the bridge. A person-in-situation or action-centred metatheory has replaced it. In that way the directives put forward in the article by Dervin and Nilan ten years ago have been realized. ([Vakkari 1997](#vak01): 463)

In a comprehensive book on information seeking, needs and behaviour research, Donald Case ([2002](#cas01)) dates the beginning of the paradigm shift to early 1970s, and especially to the [1976](#der01) landmark article by Dervin that challenged ten "dubious assumptions" that had dominated past research on information needs and uses. According to Case:

> It was not until the 1970s that investigations begin to branch out beyond the focus on formal channels and task-oriented needs. The emphasis shifted away from the structured "information system" and toward the person as finder, creator, and user of information. ([Case 2002](cas01): 6)

A yet different view on the origin and history of user-centred research was presented in 2004 by Marcia Bates. In her memoirs of the early formulative phase of information science in the 1960s, Bates wrote that

> Many writers (...) cite Dervin and Nilan's 1986 literature review on information seeking as marking, essentially, the beginning of a modern user-centred orientation to information-seeking research. I have long been puzzled by this apparent blanking out of the rich body of information behaviour research by Paisley and many other excellent researchers (...) ([Bates 2004](#bat05): 692)

Bates ([2004](#bat05)) hence placed the beginning of user-centred research to the studies on scholarly communication conducted in the 1950s and 1960s. In Case's ([2002:](#cas01) 6) view, this older literature was not really about information seeking but artifacts and venues of information seeking. It focused on information sources and how they were used, rather than the individual users and their needs.

In the light of the variability of views concerning the history of user-centred research, there is clearly a need to revisit the user-centred turn. This article looks more closely into a sample of early user studies in the light of the characterization of those studies as system-centred. We seek to convey some of the meanings and interpretations given to information systems, information sources, and information activities, in different decades.

In addition to the original works that we were able to access, we rely on early reviews of user studies written by Bates ([1971](#bat01), [1973](#bat02)), Menzel ([1960](#men01), [1966](#men02)), Paisley and Parker ([1966](#par01)) and Paisley ([1968](#pai01)) that appear to be fairly comprehensive. The reviews naturally represent the viewpoint of the time in which they were written, however, this is exactly what we want to capture and compare to the interpretative context set by Dervin and Nilan's characterization of traditional user research.

The article proceeds by first introducing Dervin and Nilan's major arguments. We then discuss the historical context of the Dervin and Nilan article. Then we revisit the dichotomies presented by Dervin and Nilan by comparing them to ideas presented in the sources mentioned above. The article ends by presenting our conclusions.

## Characteristics of traditional user and use studies according to Dervin and Nilan

According to Dervin and Nilan ([1986](#der06): 10), traditional use and user studies primarily focused on individuals' use of information sources and their sociodemographic characteristics. Traditional studies mainly addressed questions such as "how often is system X used and by whom?" focusing on observable dimensions of behaviour ([ibid.](#der06): 10). Dervin and Nilan argued that traditional use and user studies constructed users mainly as passive and essentially homogeneous recipients of information (ibid.: 13).

Dervin and Nilan ([1986](#der06)) proposed a range of characteristics for a new alternative user-centred paradigm that were cast as contrasts to the features of the traditional approach. The contrasts outlined by Dervin and Nilan were:

1.  Objective versus Subjective Information. Traditional information needs and uses studies focused on objective information, that is, constructed "information" mainly as something that has some element of correspondence to reality. The alternative paradigm focuses on the uniqueness of the life-worlds of users who are constructed as actively seeking and making sense of information ([Dervin and Nilan 1986](#der06): 13).
2.  Mechanistic, Passive versus Constructivist, Active Users. Traditional user studies assumed that all information, document delivery and information system use is beneficial,"ignoring the strategies that people actually use for bridging their information needs" ([Dervin and Nilan 1986](#der06): 13-14). The user-centred approach focuses on questions such as "how do people define needs in different situations?" ([ibid.](#der06): 16).
3.  Trans-Situationality versus Situationality. Traditional studies attempted to describe elements of user behaviour that apply to all users across situations. There are few conceptual approaches dealing with situational variables. The situationality focus requires systems to make repetitive needs assessments [(ibid.](#der06):. 14).
4.  Atomistic versus Holistic Views of Experience. Traditional studies focused on user behaviour primarily in the context of user intersection with systems. The alternative paradigm calls for looking at information behaviours outside system contexts ([ibid.](#der06): 14)
5.  External behaviour versus Internal Cognitions. Traditional research focused on contacts with sources and systems as observable indicators of needs whereas the alternative approach sees information needs as internal, psychological, cognitive states ([ibid](#der06).: 15).
6.  Chaotic versus Systematic Individuality. Traditional studies considered it necessary to base systems and observations on orderly patterns of behaviour whereas the alternative paradigm argues for the right of the user to be different ([ibid.](#der06): 15).
7.  Qualitative versus Quantitative Research. Quantitative techniques were the most compatible with traditional assumptions whereas the alternative paradigm seeks to supplement these with inductive qualitative approaches ([ibid.](#der06):. 16).

## The historical context of the Dervin and Nilan review

As noted by Bates ([2004](bat05): 691), descriptions of the evolution of the information behaviour research generally lean towards accepting the interpretation of the de-contextualized and system-centred nature of traditional information needs and uses studies. She pointed out that Dervin and Nilan _had reviewed the literature from the year 1978 forward only_, as that was the date of the preceding ARIST review on information needs and uses literature. This seems, by and large, to have gone unnoticed ([Bates 2004](#bat05)), and the effects of this time limit on Dervin and Nilan's interpretations have not been discussed in an in-depth manner in earlier work.

Another important consideration is that Dervin's own research had focused on public library users and everyday information needs of general population adults ([Dervin 1976b](#der02), [Dervin and Zweizig 1977](#der07)). It is in this particular context that she advocated the idea that the library should not be the central object of study in itself, but information needs as they are experienced and faced by individuals in diverse life situations. In fact, Dervin's analysis of alternative conceptualizations of information and users originally encompassed a wide scope of literatures. It was developed as a critique of "social science literature relating to information transfer" ([Dervin 1983a](#der04): 160). Dervin discussed and analysed, for instance, the positioning of audiences in mass media research and public opinion studies.

Most importantly, the Dervin and Nilan [1986](#der06) ARIST call to arms incorporated major critical insights from other articles similarly aiming at strengthening the conceptual and theoretical foundations of information science. Some of the leading theorists cited by Dervin and Nilan were Dervin ([1980](#der03), [1983b](#der05)), Zweizig ([1976](#zwe01), [1979](#zwe02)), Belkin ([1978](#bel01)), Jarvelin and Repo ([1982](#jar01)), Taylor ([1984](#tay01)), and Wilson ([1981](#wil01), [1984](#wil02)). Dervin and Nilan ([1986](#der06): 9) explicitly state that they derived their context of looking at information needs and uses literature from this body of research expressing an ambition of developing a better general theory for the study of information-related human behaviour. They commented that without this context, it would have been difficult to make sense of the literature because

> On the one hand, _the brunt of the work looks much like work reviewed in prior ARIST chapters_. On the other, a small but significant portion of the work is going off in seemingly unrelated directions. ([Dervin and Nilan 1986](#der06): 9; italics added).

It is noteworthy that many of the best works in the period reviewed by Dervin and Nilan were influenced by theories of cognitive psychology (also called mental constructivism in distinction to social constructivism, ([Talja _et al._ 2005](#tal01)). This was the strongest theoretical movement at that time, allowing scholars to develop a theoretical foundation for the study of human information processing by shifting attention to cognitive knowledge structures and how meanings and what constitutes "information" is relative to particular situations. Dervin and Nilan were similarly more interested in the conceptualization of information at the level of metatheory than the history of information needs and uses research. They wanted to tackle underlying philosophical background assumptions about the nature of reality, knowledge, information and human beings. Dervin continued this research track in her later publications, resulting in some of the strongest theoretical work in the field of information studies.

## Conceptualizations of context

Dervin and Nilan called for a focus on contextual aspects of users' situations. As noted by Dalrymple ([2001](#dal01)), Paisley's [1968](#pai01) _ARIST_ review developed a sophisticated conceptualization of the layers of context or foci through which to study scholarly communication. These were: 1) the scientist within his culture, 2) the scientist within a political system, 3) the scientist within a membership group, 4) the scientist within a reference group, 5) the scientist within an invisible college, 6) the scientist within a formal organization, 7) the scientist within a work team, 8) the scientist within his own head, 9) the scientist within a legal/economic system, 10) the scientist within a formal information system. The first nine layers definitely "go beyond the system intersection" and are concerned with "what leads up to intersections with systems" ([Dervin and Nilan 1986](#der06): 15). [Dervin and Nilan](#der06) (1986: 17) argued, however, that "almost without exception "information needs" have not been defined as what users think they need but rather in terms that designate what it is in the information system that is needed."

It is interesting to look at the topics that Paisley ([1968](#pai01)) outlined as relevant for each of the ten layers in the light of this argument. For instance, Paisley noted that affective and psychological factors belong to the layer "scientists within his own head" but he then went on to review studies on the "curious filtering process" revealed by studies on _perceived relevance_. Citing Cuadra and Katter ([1967](#cua01)) he wrote that:

> The authors' discussion on the _situation-specific_ "preferential discriminatory response" (...) shows that we have made progress since the days when relevance was thought to be a property of documents. In fact, Cuadra and Katter have opened up a Pandora's box of relationships among judges, needs, documents, situations, and scales that may temporarily discourage the use of the relevance crition in system precision. ([Paisley 1968](#pai01): 8; italics added)

A major finding as summarized by Paisley ([1968](#pai01)) was that "information acceptances" and preferred aspects of relevance - interpretative, methodological, formulative, "loose-metaphor orientation," "hard-fact orientation" - seemed to be explained by group membership and research phase, and not by individual characteristics.

Paisley's ([1968](#pai01)) work is interesting also because he stressed that the effects of the cultural system of science are so pervasive that they are easily overlooked even by scholars themselves. Into the cultural system he not only counted the traditions of scholarly communities but also technology. Interpreted in contemporary terms, he did not see technology as a stand-alone system but as both shaping and being shaped by scholarly information and communication practices. Vis-a-vis information systems Paisley ([1968:](#pai01) 6) did not adopt a system-centred view but explicitly stated that there exists a marketplace of _competing_ information systems so that "much like commercial air service, a network coalesces from competitive elements."

In the same way as Dervin and Nilan, Parker and Paisley ([1966](#par01)) emphasized the need for psychological research into the nature of human information seeking and information-processing behaviour. What is different in their approach is that they do not call for more research on users' cognitive and psychological processes to build a general theory, but explicitly state that

> The task for psychological research in this context is to test the behavioural assumptions of systems designers. ([Parker and Paisley 1966](#par01): 1069)

Parker and Paisley saw the role of psychological research of human information processing as "providing explicit specifications and constraints for the design of information systems" ([Parker and Paisley 1966](#par01): 1069).

They stressed that information systems design for amplifying the efficient flow of information among scientists entails dealing with a great deal of "_apparent_ irrationality" on the part of scholars because such apparent irrationality nevertheless results in overall efficiency and productivity ([Parker and Paisley](#par01): 1069). Along similar lines, Scott ([1959](#sco01): 245) stated that "we had better take the scientist broadly as we find him and build our system of information storage around him." Swanson ([1966:](#swa03) 80) stressed that "we should (...) get on with the job of trying to change the services, not the customers." Swanson ([1966](#swa03): 83) suggested that a good line to take in designing better systems is the "systemic large-scale amplification of information activities already found to be useful by scientists themselves;" the "informal and haphazard information activities."

Although this user-centred design viewpoint was not consistent in early scholarly communication studies ([Bates 1971](#bat01)), it in fact predates even the many fine papers presented in the 1958 International Conference on Scientific Information (ICSI). Swank ([1945](#swa01)) studied the needs of English scholars analysing the topics and subjects they studied through a corpus of doctoral dissertations. His interest was in "whether the required materials are brought together under headings relevant for English scholars' needs," defining the rationale for his research as follows:

> This study is a criticism, from the point of view of the English scholar, of bibliographical devices which are, or can be, used for revelatory purposes. ([Swank 1945](#swa01): 50)

## Conceptualizations of systems

As noted by Bates ([2004:](#bat05) 691), the seminal articles by, for instance, Menzel ([1959](#men01), [1960](#men02)) and Parker and Paisley ([1966](#par01)) employ the term "systems" in a very different sense than information retrieval systems or library-centred studies. The word "system" in these studies refers to "_systems of scientific communication_," that is, scholarly communication practices. These were studied quite consistently in their social and scholarly domain contexts. As pointed out by Menzel ([1960](#men02): 1), the range of topics studied was really much wider than conveyed by the umbrella term "information needs and uses." The fundamental questions tackled in these early studies were:

*   given that a scholar has to perform such and such a task, how does he go about performing it? ([Menzel 1960](#men02))
*   how do scientists actually learn about work important to them? ([Parker and Paisley 1966](#par01))
*   on what grounds do researchers choose literature? ([Allen and Gerstberger 1967](#all01))
*   what is the range of literatures used? ([Metz 1983](#met01))
*   how can the total scientific communication process be aided and amplified? ([Swanson 1964](#swa02), [1966](#swa03)).

Answering these questions was understood to require studying and observing the daily activities of scholars ([Halbert and Ackoff 1959](#hal01); [Bates 1971](#bat01)), and not only their use of information systems and services.

For instance, the series of studies undertaken by the American Psychological Association (22 reports published between between 1963 and 1968) on scientific information exchange in psychology report on topics such as: the pace of migration of research results from oral discussions and conference reports to final publications, information use habits in the preparation for teaching, scholars' filing and note-taking systems, the role of conferences in the scholarly communication process, problems intrinsic to review-writing, patterns of collaboration, authors' experiences in submitting papers for publication ([Bates 1971](#bat01)). The overall approach of this work is nicely summarized in the end of Garvey's book _Communication: The Essence of Science_ ([1979](#gar01)):

> We have sought to portray scientific communication as a social system in which interactive communication among scientists is its most salient feature. ([Garvey and Gottfredson 1979](#gar02): 320)

They go on to emphasize that innovations or interventions into systems of scholarly communication (journal publishing, libraries, conferences, technologies) should themselves be done in interaction with scholarly communities. The book ends by saying that any reform to the communicative systems of science must consider the active scientific researcher not only as a consumer but also in his role as "producer-disseminator" of information ([Garvey and Gottfredson 1979](#gar02): 320).

The conceptualization of "users" in the dual role of producer and consumer of information yields a broad and active role to the user. Garvey and Gottfredson ([1979](#gar02): 320) not only assumed that users should be placed at the center of systems designed and planned by someone else, they stressed that innovations and interventions in communication systems must ultimately be designed in collaboration with, or within, scholarly communities themselves or they will not become efficient and effective.

## Conceptualizations of information seekers

Not all early user studies assumed that information system use is always beneficial, rather, Parker and Paisley ([1966](#par01)) note that

> Accumulating data have not been kind to normative assumptions of ways in which scientists _ought_ to use information. We have been forced to broaden our investigations to include informal (chiefly interpersonal) systems, "accidental" acquisition of useful information, "inefficient" and "irrational" information-seeking and so on. ([Parker and Paisley](#par01): 1061; emphasis original)

According to Dervin and Nilan ([1986](#der06): 13-14), in traditional studies "states of being informed or benefited have been assumed to ensure directly from document delivery with no intervening user behaviour." We may contrast this with what Menzel ([1960](#men02)) has to say about the accomplishments of the scientific information system:

> One must, however, remember the very common experience that the information sought is not found; that the information, once gained, finds use in quite unanticipated ways; that information is obtained which was not deliberately sought when one picked up a journal or entered a meeting room; and, finally, that scientific information is obtained on occasions which were not entered for the purpose of information-gathering at all. In the view of the frequency of such occurrences it does not seem satisfactory to restrict one's view of the accomplishments of the scientific information system and its elements to what the information-receiving scientist was aware of before he engaged in the activity concerned. ([Menzel 1960](#men02): 27)

Interestingly, Menzel ([1960](#men02)) preferred to talk about information-receiving acts rather than information-seeking behaviour. Although he used the term "information-gathering behaviour and experiences" he seemed to prefer the term "communication-receiving activity." This is explained by the fact that Menzel's ([1959](#men01)) own and studies he reviewed had already shown that much information of great importance comes through the scholars' informal information network or "accidentally." He stressed that "what appears as an accident from the point of view of the individual may be an expected occurrence from a larger point of view" ([Menzel 1960](#men02): 50-51).

Parker and Paisley ([1966](#par01)) especially praised the interview study by Herner ([1954](#her01)) in which "formal and informal communication systems of science" were explored jointly. Swanson ([1966](#swa03)) stressed that the planning of future information services ought to be based on the notion of the importance of small clusters of scientists who read and cite each others' papers and who communicate informally. He suggested that

> The mechanized library (...) is in a particularly good position to keep systematic historical records of the associations between users (at least who grant permission to do so) and works requested. This information could be used to discover groups of people with similar literature interests, or perhaps to discover books which share a high incidence of similar use and which grouping therefore ought to be reflected in the catalog. ([Swanson 1964:](#swa02) 120)

Swanson ([1964](#swa02): 120-121) suggested techniques for discovery of highly correlated clusters of books, correlated because sharing users, or, clusters of users whose correlation is determined by a high incidence of common book use. These suggestions predate automatic recommender systems such as those used by Amazon.com, and social classification and bookmarking systems. Based on results reached in empirical user studies, they still come across as valuable and up-to-date ([Twidale and Nichols 1998:](#twi01) 262).

## Conceptualizations of sources

According to Dervin and Nilan, the traditional paradigm categorized and analysed users and uses according to systems features and variables. On the relationship between studies of scholarly communication to libraries and research institutes, Menzel ([1960](#men02): 5) wrote that

> To be sure, many studies are carried out within one or a few establishments, but are designed to serve broader interests; and even those that are carried out primarily for guiding local policy can be of more far-reaching significance and interest. This is, however, least likely when the local studies are limited to descriptive statements, as opposed to examinations of functional inter-relationships between the communication behaviour and experiences of scientists and other factors.

One such local library study that has more far-reaching significance is Metz's ([1983](#met01)) _The Landscape of Literatures: Use of Subject Collections in a University Library_. It is true that Metz's study is library-centred and document-centred in that it focuses on library collections and observable dimensions of their use. Metz's simple question "who uses what?" continues to be valuable and interesting, however, and his overall approach effective.

Metz ([1983](#met01)) used statistical techniques to trace the landscape of interdisciplinary relationships. He focused on what literatures scholars in each discipline actually use, and how interdisciplinary or "ethnocentric" (endogenous, focused on own discipline) their reading is. He found that, for instance, mathematics and geography are dissimilar in showing, respectively, a very high and a very low degree of dependence on endogenous literature. Social scientists relied on widely scattered literature compared to the relative independence of the physical and life sciences. Overall, his library circulation data showed greater dependence on external literatures than citation studies.

Metz ([1983](#met01)) also reached interesting conclusions and hypotheses regarding differences in personal libraries/collections reading in comparison to academic library readings, differences in degrees of interdisciplinarity in monograph reading and journal reading, and differences in overall reading practices in comparison what eventually gets cited in publications. He also showed that specialists and nonspecialists approach subject literatures differently.

To be sure, the focus on domains and their subject literatures differs from a people-centred or users-in-problematic-situations approach. We would argue that this is, nevertheless, a fundamentally context-sensitive approach. It treats readers and literatures as inseparable entities. That is, when asking what is the range of literatures used in various specialties by specialists in knowledge production, it places documents and subject literatures at the center of attention without losing sight of essential contextual differences in "how work is conducted with and through texts" ([Davenport and Cronin 1998](#dav01)).

## Conceptualizations of information needs

Dervin and Nilan ([1986](#der06)) called for more studies on the everyday information needs of average citizens. Dervin and Nilan argued that earlier studies had been conducted within system confines, stuck on habitual behaviours, and had not attempted to deal systematically "with the chaos of individuality" [(ibid.](#der06): 21). There exists some early everyday life information needs studies that appear to be generally very little known, although they are important precursors of everyday life information seeking studies.

The term "life information" was coined by Bates in 1974\. According to Bates' succinct definition,

> By "life information" is meant information needed for successful living. The area of need ranges all the way from sheer survival (stay away from dogs that walk funny and foam from the mouth) to the most advanced forms of self-realization (where can I study ceramics or transcendental meditation?). The scope of information falling under this rubric is greater than may at first appear. It includes vast amounts of information about how to do many different things in one's culture that will be acceptable and lead to one's survival and emotional satisfaction. ([Bates 1974:](#bat04) 53)

The book involving Bates' ([1974](#bat04)) presentation of the life information research area, _Library and Information Service Needs of the Nation_, contained chapters on the special information needs of, for instance, the following groups:

*   labor,
*   agriculture,
*   creative and performing artists,
*   women, homemakers and parents,
*   young children,
*   young adults and students,
*   older people,
*   the geographically remote,
*   the economically and socially deprived,
*   the institutionalized person,
*   the mentally and physically handicapped,
*   the Mexican-American community. ([Cuadra and Bates 1974](#bat03))

Bates ([1973](#bat02)) _Review of Literature Relating to the Identification of User Groups and Their Needs_ additionally discussed a range of literature explicity or implicitly revealing specific needs of, for instance:

*   continuing education and open university students,
*   foreign language speaking people,
*   American Indians,
*   African-Americans,
*   women, and
*   the functionally illiterate.

The chapters in _Library and Information Service Needs of the Nation_, and some of the studies reviewed by Bates ([1973](#bat02)), discussed the special needs of various interest groups, and what could be done in terms of outreach activities, community information provision and library materials provision to meet those needs. Bates ([1973](#bat02)) discussed cultural differences and cultural heritage issues needing more active involvement from the part of the library. The emphasis in these works was firmly on "life information" and "public information provision" through public libraries. A study by Bates ([1974](#bat03)) identified information needed by women through analysing information requests (questions) made at a Women's Center.

The book chapters in _Library and Information Service Needs of the Nation_ ([1974](#bat04)) outlined the information needs of specific groups according to a common typology:

*   types of information needed in crisis situations (typical problem situations, e.g., medical, legal, financial),
*   types of information needed typically by the group (e.g, health, parenting, homemaking),
*   types of information needed by the group for self-enrichment, development, recreation, and leisure.

In involving leisure interests, this typology is broader in scope than the focus on problematic situations advocated Dervin and Nilan ([1986](#nil06)), among others. The typology may be viewed as an attempt to deal effectively with the seeming chaos of individual needs. The typology incorporated both habitual needs and specific needs of various groups. In terms of habitual needs, McNeal's ([1951](#mcn01)) study on reading interests of rural citizens appears interesting. Respondents wanted more materials on the broad areas of home management, health and health problems, hobbies, child care, gardening, sewing and dressmaking, poultry, food processing, and home furnishing ([Bates 1973:](#bat02) 53). Life information needs hence appear to remain fairly stable.

It could be argued that early library use and user studies were not all oblivious to the user's needs or to sociological contexts. Berelson's ([1949](#ber01)) work was on what public libraries can do develop their services, yet he did not necessarily assume that those not using the library would not have had other information sources and channels at their disposal. In the 1930s, Waples ([1931](#wap01)) launched a huge study to determine what people read in their lives. This is a fairly user-sensitive question.

## Conceptualizations of methodology

Dervin and Nilan ([1986](#der06)) emphasized qualitative methods as better suited for investigating people as purposive, self-controlling, sense-making beings. In the 1960s and 1970s, survey questionnaires were often perceived as the most effective method for following issues such as migration of results and ideas from informal to formal domains. Menzel ([1960](#men02)) discussed techniques of data gathering employed in the reviewed use studies, among them diaries, interviews, and observation. He stressed, however, that in-depth analysis that does not stay at the level of descriptive statements depends of the conceptualizations that the researcher chooses to work with, rather than data-gathering techniques:

> It is sometimes erroneously believed that the choice of data-gathering technique is the principal or even the only important issue of research methodology. (...) Generally speaking, there is no one-to-one relationship between data-gathering technique and the conceptualization of units of sampling and observation, although certain bounds to the latter are set by the choice of data-gathering technique. ([Menzel 1960:](#men02) 5)

Paisley ([1968:](#pai01) 21-22) continued the discussion on how to reach beyond pure description by underlining that statistical methods permit inferences to larger populations but "a small purposive sample generates maximum _insight_ rather than inference, per unit cost." He stressed the need for good middle range theories that can be formed through "successive approximations," that is, through forming testable hypotheses out of observations. Menzel ([1960:](#men02) 16) stated that researchers are in fact rarely interested in amounts of reading or frequencies information-receiving acts for their own sake: "such data only become useful when they enter into comparisons and into the examination of relationships."

Parker and Paisley ([1966](#par01)) noted that the reason why no coherent pattern of scientific information use emerged from earlier findings should not be attributed to methodological failures but the complexity of the problem itself (multiple interacting variables). In Menzel's ([1960](#men02)) work, however, we find sketched a research programmefor the study of scholarly communication practices with hypotheses that with some additional work could easily have reached the theory status.

## Discussion

This paper has pursued the line of study opened by Bates ([2004](#b%20at05)) and Dalrymple ([2001](#dal01)) who both questioned the narrative of the user-centred turn. In revisiting the history of user-centred research we have discovered a rich body of findings which are still of great value. Many studies conducted in the 1950s and 1960s pursued research topics and tracks that remain interesting and meriting further study, such as the role of conferences as communication forums, how authors choose the journals where they submit their work for publication, and the role of being familiar with scientists and literatures from other fields to innovation and performance. Much of this work has been somewhat forgotten and under-utilized, therefore, user-centred research may perhaps not have been as cumulative as it could otherwise have been.

In the studies by, for instance, Garvey, Menzel, Parker and Paisley, Swanson, and Allen and Gerstberger, the focus was placed on scholarly communication conceived very broadly, incorporating not only information seeking and use behaviours but also information receiving and sharing, reading, writing and publishing activities: the full arc of communication and information practices. These practices were studied quite user-sensitively in their natural context despite the fact that early user studies usually did have an implicit or explicit aim to support the flow of information through the design of information systems or products.

Today the information behaviour community tends to identify itself through the system-centredness versus user-centredness dichotomy. In other words, there is a tendency to define research foci through a contrast between people-centred questions and system-oriented questions. An example of a people-oriented question is, for example "How do the elderly learn about and cope with problems or opportunities that come up in their daily lives?" ([Case 2002:](#cas01) 7). An example of a system-oriented question is, in turn, "How does the public use a library for personal pleasure and growth; what they ask for, borrow, and read?" ([Case 2002:](#cas01) 7). As a consequence, site-specific, system-specific, or service-specific studies have not been incorporated in the _ARIST_ reviews of the information behaviour research area since the 1990s ([Case 2006](#cas02)). In this sense, the Dervin and Nilan 1986 article has worked as the most important identity-marker for the information behaviour research community ([Olsson 2007](#ols01)).

How we choose to draw the boundaries of a research field and what we choose to count in and out to a large extent depends what we see as the central issues to study. As noted by Case ([2002](#cas01)), the boundaries, concepts, methods and research foci of information needs and uses research have, since the earliest reviews ([Menzel 1966](#men03)), arosen as much dissatisfaction and dissenting voices than knit together a community of like-minded scholars. Case ([2002:](#cas01) 285) described information behaviour research as a field carrying a heavy legacy of fault-finding and a long history of complaint over low quality and general uselessness. One of the strongest concerns voiced over years has been that the user versus system dichotomy is potentially harmful for research programs in LIS ([Pettigrew _et al._ 2001:](#per01) 64; [Järvelin and Ingwersen 2005](#jar01)). This may indeed be a distinction that isolates interacting and inseparable actants - people, technologies, literatures, and documents - into separate domains of research.

In revisiting the distinction between system-centred and user-centred research in an attempt to understand continuity and changes in user studies, what struck us as surprising was the broad scope of interests tackled already in 1960s. The joint exploration of both oral and written communication and both reading and writing literature in the early studies may be argued to be even more extensive in scope than the focus on users' information needs advocated by Dervin and Nilan. Another aspect that seems important to consider is that it is quite possible to study information needs sociologically and in a user-sensitive manner also in the context of library-oriented, site-specific, and service-oriented research. Questions such as "who uses what," "what people read in their lives," and "what are the criteria for choosing literature" will remain relevant and worthy of study for times to come.

## Conclusions

Dervin and Nilan did not incorporate in their review pre1978 user studies. In grounding their call to arms on other critical pieces, their article was a forceful synthesis of the voices of change. Their criticisms focused on conceptualizations of information and users, not on studies of scholarly communication conducted between 1950s and 1970s. These would have mandated different conclusions. Reading the Dervin and Nilan article side by side with the studies on scientific information and communication practices reviewed above, it becomes apparent that Dervin and Nilan were not deeply familiar with this literature.

Like Belkin, Brookes and Wilson, Dervin and Nilan wanted to strengthen the theoretical foundations of information studies in general, and in that respect their article is unquestionably a milestone. It did not mark the beginning of user-centred research, however.

The powerful influence of the Dervin and Nilan article can be at least partly explained by how it captured and synthesized other observed problems in information research: the limited scope of library circulation studies, and shortcomings of studies that sought to present technical solutions to the problems of document retrieval without attacking the underlying philosophical issues about the nature of human knowing, language, and document relevance assessments.

One thing that also may explain the influence of Dervin and Nilan's article is that "systems-centredness" is easily associated with cold technology, indifference toward individual human beings and their problems. "User-centredness" easily appears as ethically sound, warm and compassionate, based on the ethics of caring about people and their problems. There are many traps in these images, however, the most important one perhaps being that there are many different streams of user-centred approaches. The time has come to probe deeper into the differences between diverse user-centred traditions. In this quest, we might as well start from the beginning of user-centred research.

## Acknowledgements

This work would not have been possible without the help of Professor Marcia J. Bates whose hints on where to look for interesting materials have been invaluable. Marcia also let us access her notes and difficult to access bibliographies on the early literature. We owe a major debt of gratitude to Marcia. For potential errors in this paper, we are solely responsible.

## References

*   <a id="all01"></a>Allen, T. J. & Gerstberger, P. G. (1967). _Criteria for selection of an information source._ Cambridge, MA: Alfred P. Sloan School of Management, Massachusetts Institute of Technology.
*   <a id="bat01"></a>Bates, M. J. (1971) _User studies: a review for librarians and information scientists_. East Lansing, MI: National Center for Research on Teacher Learning. ERIC Document Reproduction Service No. ED041138.
*   <a id="bat02"></a>Bates, M. J. (1973). Review of literature relating to the identification of user groups and their needs. In C.P. Bourne, V. Rosenberg, M.J. Bates & G. R. Perolman, _Preliminary investigation of present and potential library and information service needs. Final report_ (pp. 36-68). Washington, DC: U.S. National Commission on Libraries and Information Science, February 1973\. ERIC Document Reproduction Service No. ED073786.
*   <a id="bat03"></a>Bates, M. J. (1974). Library and information services for women, homemakers, and parents. In C. Cuadra, C. & M.J. Bates (Eds.) _Library and information service needs of the nation: proceedings of a conference on the needs of occupational, ethnic, and other groups in the United States_ (pp. 129-141). East Lansing, MI: National Center for Research on Teacher Learning. ERIC Document Reproduction Service No. ED101716.
*   <a id="bat04"></a>Bates, M. J. (1974) Speculations on the sociocultural context of public information provision in the seventies and beyond. In C. Cuadra, C. & M.J. Bates (Eds.) _Library and information service needs of the nation: proceedings of a conference on the needs of occupational, ethnic, and other groups in the United States_ (pp. 51-76). East Lansing, MI: National Center for Research on Teacher Learning. ERIC Document Reproduction Service No. ED101716.
*   <a id="bat05"></a>Bates, M.J. (2004) Information science at the University of California at Berkeley in the 1960s: a memoir of student days. _Library Trends,_ **52**(4), 683-701.
*   <a id="bel01"></a>Belkin, N. J. (1978). Information concepts for information science. _Journal of Documentation,_ **34**(1), 55-85.
*   <a id="ber01"></a>Berelson, B. (1949). _The library's public: a report of the public library inquiry._ New York: Columbia University Press.
*   <a id="cas01"></a>Case, D. O. (2002). _Looking for information: a survey of research on information seeking, needs, and behaviour._ London: Academic Press.
*   <a id="cas02"></a>Case, D. O. (2006). Information seeking. In B. Cronin (Ed.), _Annual Review of Information Science and Technology,_ Vol. 41, pp. 293-326\. Medford, NJ: Information Today.
*   <a id="cua01"></a>Cuadra, C. & Bates, M. J. (Eds.) (1974). _Library and information service needs of the nation: proceedings of a conference on the needs of occupational, ethnic, and other groups in the United States_. East Lansing, MI: National Center for Research on Teacher Learning. ERIC Document Reproduction Service No. ED101716.
*   <a id="cua02"></a>Cuadra, C. & Katter, R. (1967). Opening the black box of "relevance." _Journal of Documentation_, **23**(4), 291-303.
*   <a id="dal01"></a>Dalrymple, P. W. (2001). A quarter of a century of user-centred study: the impact of Zweizig and Dervin on library and information science research. _Library and Information Science Research_ **23**(2), 155-165.
*   <a id="dav01"></a>Davenport, E. & Cronin, B. (1998). Some thoughts on _just for you_ service in the context of domain expertise. _Journal of Education for Library and Information Science_, **39**(4), 264-274.
*   <a id="der01"></a>Dervin, B. (1976a) Strategies for dealing with human information needs: information or communication? _Journal of Broadcasting,_ **20**(3), 324-351.
*   <a id="der02"></a>Dervin, B. (1976b) The development of strategies for dealing with the information needs of urban residents: phase 1: Citizen study. Final report. Seattle, WA: School of Communications, University of Washington. ED 125640.
*   <a id="der03"></a>Dervin, B. (1980). Communication gaps and inequities: moving toward a reconceptualization. In: B. Dervin & M. Voight (Eds.), _Progress in Communication Sciences,_ Vol. 2, pp. 73-112\. Norwood, NJ: Ablex.
*   <a id="der04"></a>Dervin, B. (1983a). Information as a user construct: the relevance of perceived information needs to synthesis and interpretation. In S.A. Ward & L.J. Reed (Eds.) _Knowledge structure and use: implications for synthesis and interpretation_ (pp. 155-183). Philadelphia: Temple University Press.
*   <a id="der05"></a>Dervin, B. (1983b). _An overview of Sense-Making: concepts, methods, and results to date_. Paper presented at the International Communication Association Annual Meeting, Dallas, TX.
*   <a id="der06"></a>Dervin, B., & Nilan, M. (1986). Information needs and uses. In M. E. Williams (Ed.), _Annual Review of Information Science and Technology,_ Vol. 21, pp. 3-33\. White Plains, NY: Knowledge Industry Publications.
*   <a id="der07"></a>Dervin, B., & Zweizig, D. (1977). Public library use, users, uses: advances in knowledge of the characteristics and needs of the adult clientele of American public libraries. In M. J. Voigt & M. H. Harris (Eds.), _Advances in Librarianship,_ Vol 7, pp. 231-255\. New York: Academic Press.
*   <a id="gar01"></a>Garvey, W. D. (1979). _Communication: the essence of science_. Oxford: Pergamon Press.
*   <a id="nie03"></a>Garvey, W. D. & Gottfredson, S. D. (1979). Changing the system: innovations in the interactive social system of scientific communication. In W. D. Garvey, _Communication: the essence of science_ (pp. 300-321). Oxford: Pergamon Press.
*   <a id="hal01"></a>Halbert, M.H. & Ackoff, R. L. (1959). An operations research study on the dissemination of scientific information. In _Proceedings of the International Conference on Scientific Information_ (pp. 97-130). Washington, D.C.: National Academy of Sciences.
*   <a id="her01"></a>Herner, S. (1954). Information gathering habits of workers in pure and applied sciences. _Industrial and Engineering Chemistry,_ **46**(1)**,** 228-36.
*   <a id="jar01"></a>Järvelin, K., & Repo, A. J. (1982). Knowledge work augmentation and human information seeking. _Journal of Information_ _Science_, **5**(2-3), 79-86.
*   <a id="jar02"></a>Järvelin, K. & Ingwersen, P. (2005). Information seeking research needs extension towards tasks and technology. _Information Research_ **10**(1), paper 212\. Retrieved 18 June 2007 from http://InformationR.net/ir/paper212.html
*   <a id="mcn01"></a>McNeal, A. L. (1951). _Rural reading interests: needs related to availability_. PhD Dissertation, University of Chicago.
*   <a id="men01"></a>Menzel, H. (1959). Planned and unplanned scientific communication. In _Proceedings of the International Conference on Scientific Information_, Vol. 1, pp. 199-243\. Washington, D.C.: National Academy of Sciences, National Research Council.
*   <a id="men02"></a>Menzel, H. (1960). _Review of studies in the flow of information among scientists. Two volumes_. Bureau of Applied Social Research, New York: Columbia University.
*   <a id="men03"></a>Menzel, H. (1966). Information needs and uses in science and technology. In C. A. Cuadra (Ed.) _Annual Review of Information Science and Technology,_ Vol. 1, pp. 41-69.
*   <a id="met01"></a>Metz, P. (1983). _The landscape of literatures: use of subject collections in a university library_. Chicago: American Library Association.
*   <a id="ols01"></a>Olsson, M. (2007). Power/knowledge: the discursive construction of an author. _Library Quarterly,_ **77**(2), 219-240.
*   <a id="pai01"></a>Paisley, W. (1968). Information needs and uses. In C. A. Cuadra (Ed.), _Annual Review of Information Science and Technology,_ Vol, 3, pp. 1-30\. Chicago: Encyclopedia Britannica.
*   <a id="par01"></a>Parker, E.B., & Paisley, W.J. (1966). Research for psychologists at the interface of scientists and his information system. _American Psychologist_, **21** (November), 1061-1071.
*   <a id="pet01"></a>Pettigrew, K. E., Fidel, R., & Bruce, H. (2001). Conceptual frameworks in information behaviour. In M. E. Williams (Ed.), _Annual Review of Information Science and Technology,_ Vol. 35, pp. 43-76\. Medford, NJ: Information Today.
*   <a id="sco01"></a>Scott, C. (1959). The use of technical literature by industrial technologists, _Proceedings of the International Conference on Scientific Information_ (pp.245-66). Washington, DC: National Academy of Sciences, National Research Council, USA.
*   <a id="swa01"></a>Swank, R. (1945) The organization of library materials for research in English literature. _Library Quarterly,_ **15**(1), 49-74.
*   <a id="swa02"></a>Swanson, D. R. (1964). Dialogues with a catalog. _Library Quarterly,_ **34**(1)_,_ 113-125.
*   <a id="swa03"></a>Swanson, D. R. (1966). On improving communication among scientists. _Library Quarterly,_ **36**(2), 79-87.
*   <a id="tal01"></a>Talja, S., Tuominen, K. & Savolainen, R. (2005). "Isms" in information science: constructivism, collectivism, and constructionism._Journal of Documentation,_ **61**(1), 79-101.
*   <a id="tay01"></a>Taylor, R. S. (1984). Value-added processes in document-based systems: abstracting and indexing services. _Information Services and Use,_ **4**(3), 127-146\.
*   <a id="twi01"></a>Twidale, M. B., & Nichols, D. M. (1998). Computer supported cooperative work in information search and retrieval. In M.E. Williams (Ed.), _Annual Review of Information Science and Technology,_ Vol. 33, pp. 259-319\. Medford, NJ: Information Today.
*   <a id="vak01"></a>Vakkari, P. (1997). Information seeking in context: a challenging metatheory. In P. Vakkari, Savolainen, R. & Dervin, B. (Eds.), _Information seeking in context: p__roceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp. 451-464). London: Taylor Graham.
*   <a id="wap01"></a>Waples, D. (1931). _What people want to read about: a study of group interests and a survey of problems in adult reading_. Chicago: American Library Association and the University of Chicago Press.
*   <a id="whi01"></a>White, H., & McCain, K. W. (1998). Visualizing a discipline: an author co-citation analysis of information science. _Journal of the American Society for Information Science,_ **49**(4), 327-355.
*   <a id="wil01"></a>Wilson, T. D. (1981). On user studies and information needs. _Journal of Documentation,_ **37**(1), 3-15\.
*   <a id="wil02"></a>Wilson, T. D. (1984). The cognitive approach to information seeking behaviour and information use. _Social Science Information Studies,_ **4**(2/3), 197-204.
*   <a id="zwe01"></a>Zweizig, D. L. (1976). With our eye on the user: needed research for information and referral in the public library. _Drexel Library Quarterly,_ **12**(1/2), 48-58.
*   <a id="zwe02"></a>Zweizig, D. L. (1979). Approaches to the study of public library services and users: the recommendations of a study panel to the national center for education statistics. _Public Library Quarterly,_ **1**_,_ 258-266\.