#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science— "Featuring the Future". Poster abstract

# The retrievability of a discipline: a domain analytic view of classification

#### [Jan Larsson](mailto:jan.larsson@hb.se)  
Swedish School of Library and Information Science,  
University College of Borås  
Sweden

This poster presents some of the main findings in _A discipline in disguise: a domain analytic approach to History of science and ideas through the subject representation in LIBRIS: bibliography included [Den dolda disciplinen: en domänanalytisk ansats i relation till Idé- och lärdomshistoria genom ämnesrepresentationen i LIBRIS: med bibliografi]_ ([Alm and Larsson 2003](#alj03)). The thesis aimed at a deeper understanding of information systems and subject representation in relation to academic disciplines. This was carried out by comparing how a discipline, the History of Science and Ideas, views itself, compared with the subject representation of its dissertations in the Swedish National Academic information system, LIBRIS. The theoretical point of departure for the thesis is Hjørland’s domain analysis which states that the quality of information seeking is enhanced if subject representation takes into account the contexts in which documents are produced and used ([Hjørland 1997](#hjo97)). The importance of concepts such as domain and discipline is discussed.

The discipline of the History of Science and Idea’s view of itself was established in three ways; by studying articles written by its scholars, by examining how the discipline presents itself on the web sites of five university institutions and by examining the titles and keywords accorded to dissertations in the discipline. The information system’s representation of the discipline was investigated through the classification codes, subject headings and the index terms accorded to the relevant dissertations. This comparison was an attempt to discover possible discrepancies between the two representations ([Alm and Larsson 2003](#alj03)).

The study did not find any significant discrepancies between the two representations, as the subject representation confirmed the discipline’s view of itself as a discipline that studies the relations between man, society and nature. What it did find, however, was that the subject representation did not, to any greater extent, recognize the discipline as such, as there is little evidence of the context from which the dissertations originate ([Alm and Larsson 2003](#alj03)). As we will see this has consequences for the retrieval of documents from the discipline, and is connected to some earlier issues identified in librarianship and information science.

Hjørland and Albrechtsen ([Hjørland and Albrechtsen 1995](#hah95); [Hjørland 1997](#hjo97)) shows how the disseminating of information from knowledge producers to users can be divided into different levels. Alm and Larsson’s ([2003](#alj03)) study identified difficulties which seemed to be connected with the primary and second levels, identified by Hjørland in _Information Seeking and Subject Representation: an Activity Theoretical Approach to Information Science:_

> An obvious problem to investigate includes suboptimal utilization of information due to lack of visibility, for example, unsatisfactory coverage of the primary information in the secondary systems. ([Hjørland 1997](#hjo97), s.126)

The problems identified by Alm and Larsson ([2003](#alj03)) concerned how the primary documents, dissertations in the History of Science and Ideas, are represented and made retrievable on the second level, in the information system. It was shown that it was not unsatisfactory coverage which caused problems in retrieval; all the sought dissertations were catalogued and classified in LIBRIS, but the problem lay with, what Lancaster referred to as retrievability:

> For someone seeking information on a particular subject, the coverage of a database on that subject will be important, especially if a comprehensive search is required. Also important, of course, is retrievability; given that a database includes n items on a subject (which can be established through a study of coverage) how many of these is it possible to retrieve when searching the database? ([Lancaster 1998](#lan98), s. 137)

Lancaster defines retrievability as: ”How much of the literature on the topic, included in the database, can be found using ”reasonable” search strategies”([Lancaster 1998](#lan98), s. 127). This is related to the difficulties faced by Alm & Larsson in trying to retrieve dissertations in the History of Science and Ideas using classification codes representing the chosen domain, a strategy which seemed 'reasonable', and furthermore was recommended by academic librarians. This resulted, however, in low recall, probably due to the recommendations issued regarding the application of classification codes. There appears to be a conflict between the Swedish Classification System, which states that the discipline of the History of Science and Ideas should be scattered in the system due to its object of study ([Klassifikationssystem för svenska bibliotek 1997](#kla99)), and recommendations from the Swedish Royal Library, suggesting that the classification code should represent the discipline ([Gustavsson 1999](#gub99)). Unfortunately it is not clearly defined if this means the discipline which is the origin of the document or the discipline which is the object of study in the document.

As a result, Alm and Larsson did not succeed in collecting their empirical data through the information system, LIBRIS, but had to contact the relevant academic institutions to be able to find the requested dissertations needed for their work ([Alm and Larsson 2003](#alj03)). This had probably not been the case if one had fully considered disciplines as entities in knowledge organization, and knowledge domains accordingly reflected as such in information systems, as suggested by Hjørland and Albrechtsen (Hjørland and Albrechtsen [1995](#hah95);[1999](#hah99)).

<figure>

![Classification code in the Swedish classification scheme](../colisp08fig1.jpg)

<figcaption>

**Figure 1 Classification code in the Swedish classification scheme**</figcaption>

</figure>

<figure>

![Figure 2 Main categories from the Swedish classification scheme](../colisp08fig2.jpg)

<figcaption>

**Figure 2 Main categories from the Swedish classification scheme**</figcaption>

</figure>

## References

*   <a id="alj03"></a>Alm, M. & Larsson, J. (2003). _A discipline in disguise: a domain analytic approach to History of science and ideas through the subject representation in LIBRIS: bibliography included [ Den dolda disciplinen: en domänanalytisk ansats i relation till Idé- och lärdomshistoria genom ämnesrepresentationen i LIBRIS: med bibliografi]._ Borås, Sweden: Högskolan i Borås, Institutionen Bibliotekshögskolan.
*   <a id="gub99"></a>Gustavsson, B. (1999). _SAB:s klassifikationssystem : slutrapport 1999-01-15._ Stockholm, Sweden: Kungliga biblioteket. Retrieved 2003-05-12 from: http://www.kb.se/bus/slutrapport.htm (Unavailable from this source. Printed copy kept by the author of this paper)
*   <a id="hjo97"></a>Hjørland, B. (1997). _Information Seeking and Subject Representation: An Activity-Theoretical Approach to Information Science._ Westport, CT: Greenwood Press.
*   <a id="hah95"></a>Hjørland, B. & Albrechtsen, H. (1995). Toward a New Horizon in Information Science: Domain-Analysis. _Journal of the American Society for Information Science_ **46**(6), 400-425.
*   <a id="hah99"></a>Hjørland, B. & Albrechtsen, H. (1999). An Analysis of Some Trends in Classification Research. _Knowledge Organisation_ **26**(3), 131-139.
*   <a id="kla97"></a>Klassifikationssystem för svenska bibliotek (1997). 7\. ed. Lund, Sweden: Bibliotekstjänst.
*   <a id="lan98"></a>Lancaster F.W. (1998). _Indexing and Abstracting in Theory and Practice._ 2 ed. Champaign, Ill.: University of Illinois Graduate School of Library and Information Science Publications Office.