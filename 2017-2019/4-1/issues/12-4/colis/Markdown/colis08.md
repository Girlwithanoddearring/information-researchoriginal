#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# A synergistic alternative to readers' advisory services: managing customer knowledge

#### [Hsia-Ching Chang](mailto:carriehc@gmail.com) and [Terrence A. Maxwell](mailto:tamaxwell@hvc.rr.com)  
#### Department of Informatics, College of Computing and Information, 
#### University at Albany, State University of New York, 
#### Draper Hall, Room 113, 
#### 135 Western Avenue, Albany, NY 12222

#### Abstract

> **Introduction.** Readers' advisory services become a growing area in combining both advanced information technology and multi-channel interactions with patrons to automatically perform customized reading recommendations. Although readers' advisory services originated from public libraries, readers fall back on customer reviews and powerful tools offered by online bookstores as well as social cataloguing applications, which best use customer knowledge.  
> **Method.** A case study approach was chosen to demonstrate how two social cataloguing websites, LibraryThing and Shelfari, use customer knowledge through building online customized book cataloguing and social navigation services.  
> **Analysis.** A value analysis was used to evaluate specific customer knowledge flow between the customer and the service provider.  
> **Results.** This paper proposes a synergistic model that addresses managing customer knowledge flows and value drivers inside a customer's mind. Applying the conceptual focal points of this model to library context sheds light on an ideal readers' advisory service.  
> **Conclusions.** Our synergistic model suggests that readers' advisory services appeal to enhance customer knowledge flows and emotional value; also, adopting a value network analysis is workable for evaluation phases.

## Introduction

Information organizations cannot avoid participating in knowledge-based competition because customers' preferences are dynamic. Although readers' advisory services originated from public libraries, readers fall back on customer reviews and powerful tools offered by online bookstores as well as social cataloguing applications, which best use customer knowledge. Savvy organizations are taking advantage of this access to invaluable customer opinions and knowledge with respect to customer satisfaction or potential expectations. Data mining and customer profiling are methods for getting to know customers. However, active listening and shared experiences are indispensable to direct customer interaction. This paper first gives a brief overview of readers' advisory services, and the ideas of customer knowledge and customer value. To demonstrate a close association between them, the paper will then go on to discuss the potential for utilizing evolving web technologies (e.g., Web 2.0) based on two case studies in the business world. Finally, to facilitate libraries providing better quality readers' advisory services, we develop a synergistic model concerned with reader participation and social networks.

## Overview of readers' advisory services

RA service has been redefined as a patron-centered library service in pursuit of information seeking and leisure reading needs ([Saricks, 2005](#sar05)). Unlike the Reader Advisor of the early twentieth century, whose role was more like an educator bringing the "best books" to the reader, the objective of today's readers' advisory service is to provide books based on reader preference ([Watson, 2000](#wat00); [Saricks, 2005](#sar05)). A question such as "I just want a good book to read; what would you suggest?" initiates an readers' advisory interview. Generally, a librarian will inquire, "Tell me about the book you just finished," to identify the reader's interest and then prepare a booklist matching the reader's preference with reference resources. Main areas readers describe center around the book's story, characters, plot, theme or subject, and emotional effect. However, it is impossible for a librarian to read everything and know everything about books. To better connect books and readers, readers' advisors need to understand their special collections in the library, and know about all available print or electronic readers' advisory resources and the readers ([Schultz, 2000](#sch00)). Being able to identify the qualities of a book or genre that appeal to an individual reader is central to the process. It is challenging that the underlying reason people choose to read is highly tied to the philosophy of readers' advisory service: reading has intrinsic value and readers are autonomous ([Watson, 2000](#wat00)). Bridging the connection between books and readers not only means knowing about customers, but also about their reading experiences that drive each specific value.

Two components are critical to providing high quality readers' advisory service: 1) training in working with readers and understanding what makes a book appealing to a particular reader, and 2) access to and knowledge of the resources that are available for helping connect readers to books ([RA Committee, 2004](#rea04)). However, underestimating the significance of the following points can decrease the quality of readers' advisory services.

### Capturing knowledge about customers

The lack of training in readers' advisory services or failing to capture knowledge about customers can cause problems when assisting readers in selecting books. In such cases, librarians wrongly assume that they know what patrons want before they have gathered enough information. Instead, librarians should continue their interaction with the readers until the librarians understand what the patrons seek. For instance, such an activity involves asking the reader what he/she liked about the book that inspired the query and then asking if the reader is in the mood for something similar or different would be an improvement upon the process. These likes and dislikes constitute "appeal factors," such as a book's pacing, characterization, story line, and frame ([Chelton, 2003](#che03); [Saricks, 2005](#sar05)). Libraries ought to integrate such feedback services into their computer systems to profile each library card holder ([Moyer, 2005](#moy05)).

### The emotional aspects of readers' advisory service: customer reading experience

In addition, readers' advisory is one of the most intimate library services and often most satisfying to patrons when they know their advisor is responding with his or her own personal reading experience ([Johnson, 2001](#joh01)). The value of human contact in a reader/librarian interaction has been viewed as a reason libraries are still vital in communities. In particular, the social and emotional aspects of a reading group are valuable ([Chelton, 2001](#che01)). Those who experience strong emotions while reading a book want to share them with other members and find resonance in their feelings. The online bookstores ([Amazon](http://www.amazon.com/books-used-books-textbooks/b?ie=UTF8&node=283155) and [Barnes & Noble](http://www.barnesandnoble.com/)) excel in consolidating reviews and sample book chapters. New social cataloguing websites ([LibraryThing](http://www.librarything.com/) and [Shelfari](http://www.shelfari.com/)) further enable users to organize their book collections online and share with others. The serendipity between information and social connection broadens readers' book sharing experiences. These services might not analyze the appeal factors of particular books as traditional readers' advisory services do, but rather help gather readalikes ([Chelton, 2003](#che03)); some patrons prefer to seek advice from librarians for choosing books fitting their emotional needs or previous experience ([Ross, 2001](#ros01)). Therefore, successfully gathering and harnessing customer knowledge greatly complements a good readers' advisory service.

## Customer knowledge and customer value

One commonly accepted definition of customer knowledge is, "the dynamic combination of experience, value, scenario information and expertise insight which is needed, created and absorbed during the process of transaction and exchange between the customers and the organization" ([Gebert et al., 2003](#geb03)). Feng and Tian ([2005](#fen05)) further clarify the logical relationship among customer relations, customer value, and customer knowledge: customer knowledge is the logical starting point of a customer relationship, and customer knowledge management (CKM) is the essential core of customer relations management (CRM). To build long-term relationships with customers, a customer knowledge value chain leads to knowledge-based personalization ([Tiwana, 2001](#tiw01)). There is a need for better managing customer knowledge and much consideration for customer value. The dimensions of customer knowledge can be categorized as follows.

### Knowledge about customers

Automatically captured by systems and saved in databases, knowledge about a customer not only includes structured data (e.g., name, contact information, preferences), but also the information that can be derived from transactions with organizations and services. Many companies use data mining techniques to enhance marketing and determine a customer's value to them ([Reinartz & Kumar, 2002](#rei02)).

### Knowledge for customers

Knowledge for customers includes everything an organization prepares to meet its customers' information needs ([Gebert et al., 2003](#geb03); [Buren et al. 2004](#bur04)); knowledge for customers can be viewed as supporting knowledge to improve the customers' experience with products and services ([Desouza & Awazu, 2005](#des05)). Customers may discuss their questions regarding using experience or request supporting knowledge through different channels; customer communities, online chat rooms, and emails handle customer support queries very well.

### Knowledge from customers

Knowledge from customers can be defined as the ideas, thoughts, and information the organization receives from its customers regarding the preferences, creativity, or consumption experience of specific products or services ([Desouza & Awazu, 2005](#des05)). Without interacting with, gathering insights from, and learning from customers, organizations cannot reach customers' minds and take advantage of precious customer feedback to mine foreseeable trends that help make appropriate decisions for adjusting existing services. Understanding what customers know is an important but neglected part of an organization's knowledge ([Rowley, 2002](#row02)). Using online interactive toolkits is a fascinating mechanism to involve customers in innovating and customizing services to meet their particular needs, but only with organization of customer knowledge can collective wisdom be fully used.

### Defining customer value: a value analysis model

We believe that each customer knowledge flow between the customer and the service provider forms a value network making the relationship meaningful. As Allee ([2000](#all00)) defines it, "a value network is any web of relationships that generates tangible and intangible value through complex dynamic exchanges between two or more individuals, groups, or organizations." According to Allee's value exchange model (Table 1 and Figure 1), mechanisms or media that trigger transactions, in particular intangible deliverables, support any value exchange. A whole picture of mapping value networks can involve complicated value exchanges with all stakeholders in an organization network. To delve into value network analysis, [Open Value Networks](http://www.value-networks.com/), an open content online community, offers bountiful resources for researchers and practitioners.

<table><caption>

**Table 1: An example of analysis of value exchanges (adapted from [Allee 2000](#all00))**</caption>

<tbody>

<tr>

<th>Mechanism</th>

<th>Provides Value</th>

<th>Returns Value</th>

</tr>

<tr>

<td rowspan="3">Interactive Online Discussion Group</td>

<td>

**GOODS, SERVICE**
Moderated discussions  
Responses to questions</td>

<td>

**REVENUE**
Subscription fee</td>

</tr>

<tr>

<td>

**KNOWLEDGE**
Personally targeted news and offerings based on user preferences  
Product updates</td>

<td>

**KNOWLEDGE**
Feedback for product development  
Customer usage data</td>

</tr>

<tr>

<td>

**BENEFITS**
Sense of community</td>

<td>

**BENEFITS**
Customer loyalty</td>

</tr>

</tbody>

</table>

<figure>

![Figure 1](../colis08fig1.jpg)

<figcaption>

**Figure 1: An example of mapping the value exchanges (adapted from [Allee 2000](#all00))**</figcaption>

</figure>

We adopt a value exchange model to conduct our case studies to elaborate on the role of customer knowledge management mechanisms in supporting the delivery of customer-centric readers' advisory services and embedded value. The two cases of social cataloguing services are chosen because they best represent our envisioned customer knowledge management of readers' advisory services. In the following section we simply illustrate a portion of value network analysis on one mechanism for each case.

## Case study: LibraryThing and Shelfari

An analysis of [LibraryThing](http://www.librarything.com/) and [Shelfari](http://www.shelfari.com/) reveals how these sites use customer knowledge by building online customized book cataloguing and social navigation services, which engage e-patrons, help each other explore more books they may like, and have an adventure in reading or meeting book lovers.

### Case 1: LibraryThing

[LibraryThing](http://www.librarything.com/), a virtual bookshelf allowing readers to organize, catalogue, and review their books, was launched by Tim Spalding, who is also the founder of [SocialCatalogers](http://socialcatalogueers.ning.com/) network, on August 29, 2005\. LibraryThing has MARC data and its book search engine is connected to Amazon.com, the Library of Congress, and 70 other libraries ([Schubert, 2007](#sch07)). Individual users can sign up for free and list up to 200 books. Beyond a virtual book repository for individuals or groups, LibraryThing aggregates social information of readers and presents readers with who else among the members has the same collection ([Rutkoff, 2006](#rut06)). In doing so, readers can easily discover more read-alike lists from other readers and share their experiences with other book lovers. Online bookseller [Abebooks](http://www.abebooks.com/) has a 40% share in LibraryThing. LibraryThing has complete customized functions, such as personal tag clouds, personal author clouds, personal statistics pages, detailed profile settings, and RSS feeds to keep informed of any new information. Here, we only examine the social tagging function that allows readers to label the indexes to conduct a value analysis (Table 2 and Figure 2).

<table><caption>

**Table 2: An example of value exchange: social cataloguing (tagging) mechanism**</caption>

<tbody>

<tr>

<th>Featured Mechanism</th>

<th>Provides Value</th>

<th>Returns Value</th>

</tr>

<tr>

<td rowspan="3">Social Cataloging (Tagging)</td>

<td>

**SERVICE**
Tags created to describe the books by readers  
Keeping updating new personalized tagging tools for book management</td>

<td>

**REVENUE**
Charging \$10 (year) or \$25 (life) for over 200 books</td>

</tr>

<tr>

<td>

**KNOWLEDGE**
Grouping readers and books based on tag information  
Open content contributed by users</td>

<td>

**KNOWLEDGE**
Reader usage behavior  
More complete linkages regarding a book</td>

</tr>

<tr>

<td>

**INTANGIBLE BENEFITS**
Weaving readers' social network</td>

<td>

**INTANGIBLE BENEFITS**
Online consumer stickiness</td>

</tr>

</tbody>

</table>

<figure>

![Figure 2](../colis08fig2.jpg)

<figcaption>

**Figure 2: Mapping the value exchanges: social cataloguing mechanism**</figcaption>

</figure>

### Case 2: Shelfari

Founded by Kevin Beukelman, Josh Hug, and Mark Williamson in October 2006, [Shelfari](http://www.shelfari.com/) has been invested in by Amazon.com and links exclusively to Amazon's catalogue ([Cook, 2006](#coo06)). It is an online bookshelf where users are able, without limit, to organize the books they are reading and share with others. Similar to LibraryThing, Shelfari offers bloggers widgets to exhibit their bookshelves. However, unlike LibraryThing, Shelfari focuses more on supporting group features, which add a book to a group's shelf, send out group emails, and acquire new members. A newly added function that harnesses the wisdom of crowds is to "ask a question" for books and get feedback and advice from other readers. The following (Table 3 and Figure 3) is a value analysis on questions and answers mechanism.

<table><caption>

**Table 3: An example of value exchange: questions and answers mechanism**</caption>

<tbody>

<tr>

<th>Featured Mechanism</th>

<th>Provides Value</th>

<th>Returns Value</th>

</tr>

<tr>

<td rowspan="3">Questions and Answers</td>

<td>

**SERVICE**
Proposing questions to other readers  
Responses to inquiries</td>

<td>

**CONTENT**
Many facets of questions and answers</td>

</tr>

<tr>

<td>

**KNOWLEDGE**
A Knowledge base of Book Q&A</td>

<td>

**KNOWLEDGE**
Peer production of knowledge</td>

</tr>

<tr>

<td>

**INTANGIBLE BENEFITS**
Sense of community</td>

<td>

**INTANGIBLE BENEFITS**
Customer participations  
Enjoyment of helping others</td>

</tr>

</tbody>

</table>

<figure>

![Figure 3](../colis08fig3.jpg)

<figcaption>

**Figure 3: Mapping the value exchanges: questions and answers mechanism**</figcaption>

</figure>

## Learning from cases of managing customer knowledge

Patrons expect to find information, desired reading materials, and knowledgeable assistance at their local library. The essence of customer knowledge management is to coordinate the knowledge flows between customers and organizations. Based on literature reviews and case studies, we propose a synergistic model (Figure 4), which addresses managing customer knowledge flows and the value drivers inside a customer's mind. Applying the conceptual focal points of this model to the library context sheds light on an ideal readers' advisory service. We further explain each focal point as follows.

<figure>

![Figure 4](../colis08fig4.jpg)

<figcaption>

**Figure 4: A synergistic model of readers' advisory services**</figcaption>

</figure>

### Knowledge about customer completes reader profile

Information technologies, such as automation, database development, and data mining can facilitate capturing knowledge about patrons. Database storing of transaction data such as circulation, circulation statistics, and patron membership information is a useful tool for acquiring knowledge about library customers to determine who uses the library and which services and books they choose. Advisors need to understand more about patron interests, needs, and backgrounds than is typically necessary in reference service. It is essential for a library to know its customer base and to know who is using the readers' advisory service ([Boudreau & Manley, 2004](#bou04)).

### Knowledge for customer tailored by customer value

RA questions usually follow specific patterns and their focus is the taxonomy of wh- words (i.e. Who, What, When, Where, Why, and How). If the frequent asked questions (FAQ) in the readers' advisory service can be well organized, developing a question answering system will greatly facilitate users' self-service. In addition, librarians have to select website lists with good content and links to other sites (authors' or publishers' websites), offering reading guides on a particular subject ([Nordmeyer, 2001](#nor01)). The art of preparing readers' advisory service constituted the advisor's knowledge, based on experience, and a love of books and humanity ([Karetzky, 1982](#kar82)). As a result, it is difficult to decompose exactly the procedures of RA, other than to suggest that it is an incremental and cooperative process ([Luyt, 2001](#luy01)). The predominant characteristic of Web 2.0 is user-generated content that can facilitate libraries compiling good resources and links that match readers' interests.

### Knowledge from customer powered by resonance

To encourage patrons to actively participate, some libraries have listings of the most requested titles for the week and pages of their patrons' recommendations. Making the site interactive and inviting patrons to submit annotations, comments, or recommendations are practical approaches to outreach ([Nordmeyer, 2001](#nor01)). The increasing readers' advisory resources, including electronic databases, websites, online book discussion groups, reader clubs, and publishers, are substantial for establishing a readers' advisory knowledge base for libraries. Social cataloguing is a powerful online tool for sharing collective intelligence of books and enables people to gain knowledge reciprocity with others when needed.

### The value exchange multiplied by network effect

Through a value analysis, a library can evaluate readers' advisory services in terms of proposing and delivering the value customers desire and can make them perceive what a library devotes to realizing said value. As Johnson ([2001](#joh01)) emphasizes, public libraries have discovered the power of the Internet for readers' advisory services, but do not effectively combine web-related information and share these resources with each other. Instead of investing time and effort in restructuring readers' advisory services, libraries should consider harnessing collective intelligence to assist them in recommending books through interactive channels to elicit customer knowledge.

## Conclusion

Current readers' advisory services in libraries neither take advantage of database techniques to more deeply understand readers' needs, nor successfully connect their readers' advisory reference resources with customer knowledge or experience. Our synergistic model suggests that readers' advisory services appeal to enhance customer knowledge flows and emotional value; also, adopting a value network analysis is workable for evaluation phases. The emerging social cataloguing websites exemplify the evolution Internet technologies are now facing. Such websites, the products of Web 2.0, turn users into reader advisors and have shown that while users exchange their knowledge and express infectious enthusiasms, their emotional value affects each other. Hence, reciprocity drives knowledge sharing. Not only does customer knowledge management add value to customers and knowledge organizations, but it also creates an alternative to better readers' advisory services.

## References

*   <a id="all00"></a>Allee, V. (2000). Reconfiguring the value network, _Journal of Business Strategy_, **21**(4), 36-39.
*   <a id="bou04"></a>Boudreau, G. and Manley, C. (2004). Developing a customized database of users as a tool for marketing the library, _Information Outlook_, **8**(1), 44.
*   <a id="bur04"></a>Buren, A., Schierholz, R., Kolbe, L., and Brenner, W. (2004). Customer knowledge management - improving performance of customer relationship management with knowledge management, _Proceedings of the 37th Hawaii Conference on System Science_.
*   <a id="che01"></a>Chelton, M. K. (2001). When Oprah meets email: virtual book clubs, _Reference & User Services Quarterly_, **41**(1), 31-36.
*   <a id="che03"></a>Chelton, M. K. (2003). Readers advisory 101: a crash course in RA: common mistakes librarians make and how to avoid them, _Library Journal_, 11/1/2003\. Retrieved 19 June, 2007 from http://www.libraryjournal.com/article/CA329318.html.
*   <a id="coo06"></a>Cook, J. (2006). Shelfari an online meeting place for bibliophiles, _Seattle Post-Intelligencer_, October 11, 2006\. Retrieved 19 June, 2007 from http://seattlepi.nwsource.com/business/288229_shelfari11.html.
*   <a id="des05"></a>Desouza, K. C. and Awazu, Y. (2005). What do they know? (customer knowledge management), _Business Strategy Review_, **16**(1), 41-45.
*   <a id="fen05"></a>Feng, T.-X. and Tian, J.-X. (2005). Customer knowledge management and condition analysis of successful CKM implementation, _Proceedings of 2005 International Conference on Machine Learning and Cybernetics_. **4**, 2239-2244.
*   <a id="geb03"></a>Gebert, H., Geib, M., Kolbe, L., and Brenner, W. (2003). Knowledge-enabled customer relationship management: integrating customer relationship management and knowledge management concepts, _Journal of Knowledge Management_, **7**(5), 107-123.
*   <a id="gib02"></a>Gibbert, M., Leibold, M., and Probst, G. (2002). Five styles of customer knowledge management, and how smart companies use them to create value, _European Management Journal_, **20**(5), 459-469.
*   <a id="joh01"></a>Johnson, R. (2001). The global coversation about books, readers, and reading on the Internet, In Shearer, K.D. and Burgin, R. (Ed.), _The Readers' Advisor's Companion_ (pp. 191-206). Englewood, CO: Libraries Unlimited.
*   <a id="kar82"></a>Karetzky, S. (1982). _Reading Research and Librarianship: A History and Analysis._Westport, CA: Greenwood Press.
*   <a id="luy01"></a>Luyt, B. (2001). Regulating readers: the social origins of the readers advisor in the United States, _Library Quarterly_, **71**(4), 443-466.
*   <a id="moy05"></a>Moyer, J. E. (2005). Adult fiction reading: a literature review of readers' advisory services, adult fiction librarianship, and fiction readers, _Reference & User Services Quarterly_, **44** (3), 220-231.
*   <a id="nor01"></a>Nordmeyer, R. (2001). Readers' advisory web sites, _Reference & User Services Quarterly_, **41**(2), 139-143.
*   <a id="rea04"></a>Readers' Advisory Committee (2004). Recommended readers' advisory tools, _Reference & User Services Quarterly_, **43**(4), 294-305.
*   <a id="rei02"></a>Reinartz, W. and Kumar, V. (2002). The mismanagement of customer loyalty, _Harvard Business Review_, **80**(7), 86-94.
*   <a id="ros01"></a>Ross, C. (2001). What we know from readers about the experience of reading, In Shearer, K.D. and Burgin, R. (Ed.), _The Readers' Advisor's Companion_ (pp. 77-96). Englewood, CO: Libraries Unlimited.
*   <a id="row02"></a>Rowley, J. E. (2002). Reflections on customer knowledge management in e-business, _Qualitative Market Research_, **5**(4), 268-280.
*   <a id="rut06"></a>Rutkoff, A. (2006). Social networking for bookworms, _The Wall Street Journal Online_, 2006-06-27\. Retrieved 19 June, 2007 from http://online.wsj.com/public/article/SB115109622468789252-Bi4NTGvCqDjylkFiE9xJzb2LsYA_20070626.html.
*   <a id="sar05"></a>Saricks, J. (2005). _Readers' Advisory in the Public Library._ Chicago, American Library Association.
*   <a id="sch07"></a>Schubert, S. (2007). Beating Oprah at the book club game, _Business 2.0 Magazine_, April 5 2007\. Retrieved 19 June, 2007 from http://money.cnn.com/magazines/business2/business2_archive/2007/04/01/8403361/.
*   <a id="sch00"></a>Schultz, K. (2000). An overview of readers' advisory service with evaluations of related websites, _Acquisitions Librarian,_ **12,** 21-33.
*   <a id="tiw01"></a>Tiwana, A. (2001). _Knowledge-Enabled Customer Relationship Management: The Essential Guide to Knowledge Management: E-Business and CRM Applications,_ NJ: Prentice Hall PTR.
*   <a id="wat00"></a>Watson, D. (2000). Time to turn the page: library education for readers' advisory services, _Reference & User Services Quarterly_, **40**(2), 143-146.