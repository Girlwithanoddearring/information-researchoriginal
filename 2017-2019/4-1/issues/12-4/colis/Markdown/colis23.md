#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# The handicap principle: a new perspective for library and information science research

#### [Jeppe Nicolaisen](mailto:jni@db.dk) and [Tove Faber Frandsen](mailto:tff@db.dk)  
#### Royal School of Library and Information Science, Birketinget 6, DK-2300 Copenhagen S., Denmark

#### Abstract

> **Introduction.** The paper introduces a new perspective for library and information science research known as the handicap principle (a.k.a. the theory of costly signaling). Research has demonstrated that this perspective can explain prosocial behaviour - something which conventional cost-benefit theories traditionally have failed to accomplish.  
> **Method.** In order to exemplify the potentials of the handicap principle for library and information science research, the paper focuses on the practice of academic book reviewing.  
> **Analysis.** The paper analyses the book reviewing activity of authors in the field of economics (1985-2006).  
> **Results.** The paper demonstrates that conventional cost-benefit theories fail to explain the prosocial behaviour of academic book reviewing. Instead, an alternative explanation based on the handicap principle is tested with positive results.  
> **Conclusion.** The paper demonstrates the feasibility of the handicap principle as a theory for understanding the act of academic book reviewing - an act that is difficult to understand from traditional cost-benefit views such as the principle of least effort.

## Introduction

The last few decades have witnessed an increasing interest in evolutionary approaches to the study and understanding of human behaviour, a trend exemplified by monographs such as Richard Dawkins' _The Selfish Gene_ ([1976](#daw76)), Helena Cronin's _The Ant and the Peacock: Altruism and Sexual Selection from Darwin to Today_ ([1991](#cro91)), and Geoffry F. Miller's _The Mating Mind: How Sexual Choice Shaped the Evaluation of Human Nature_ ([2001](#mil00)). Social scientists are gradually beginning to recognize the potential of evolutionary theory for explaining prosocial behaviour - actions by an individual that enhance the well-being of members of his or her social group beyond his or her immediate kin. Phenomena such as altruism, cooperation, fairness, resource sharing, and waste have traditionally puzzled social scientists. Why do people engage in behaviour that benefits others at a (potential) cost or risk to themselves? Evolutionary theory seems to offer the solution. A solution or rather a principle that in recent years have helped to explain, among other things, inefficient foraging behaviour ([Miller 2000](#mil00)), the evolution of art ([Zahavi & Zahavi 1997](#zah97)), and the waste in advertising ([Ambler & Hollier 2004](#amb04)). The principle in question goes by the name of the handicap principle.

The handicap principle was first formulated by Amotz Zahavi ([1975](#zah75)). However, we first became acquainted with the handicap principle when reading Tor Nørretranders' book _The Generous Man: How Helping Others is the Most Sexiest Thing You Can Do_ ([2002](#nor02)). In this book, Nørretranders develops what he considers an overlooked strand of Darwinian thought. In addition to the efficiency and survival instincts essential to successful evolution or natural selection, Nørretranders argues that animals and humans alike must show their worth by doing something difficult in order to impress in terms of sexual selection. His proposal was inspired by the handicap principle that suggests that if an individual is of high quality and its quality is not known, it may benefit from investing a part of its advantage in advertising that quality, by taking on a handicap, in a way that inferior individuals would not be able to do, because for them the investment would be too high ([Zahavi 2003](#zah03)).

The handicap principle has not been discussed much in the library and information science literature. It was briefly mentioned by Kock and Davison ([2003](#koc03)) [[1]](#note1), but given a more thorough discussion by Nicolaisen ([2004](#nic04); [2007](#nic07)) who used it to explain honesty and deception in academic citation behaviour. This lack of awareness or interest is a great shame as the handicap principle appears to be a very fruitful theory for studying and understanding what goes on in all kinds of social systems including information systems. In the words of its founder:

> "I believe that in future years ethologists, sociologists and others trying to find the ultimate reasons for the workings of social systems and for the patterns and reliability of signals will benefit from taking into account the importance of the quest for and the effect of social prestige as a mechanism that explains much of what happens in social systems. I also predict that the handicap principle (or 'costly signalling', as some who do not wish to refer to the handicap principle prefer to call it) will be found to be an inherent component in all signals" ([Zahavi 2003: 862](#zah03)).

In reframing library and information science from different perspectives [[2]](#note2), it consequently appears obvious to focus on the handicap principle and dwell on how it compares with comparable perspectives found in the field, and how it can extend or influence the field. Consequently, we will introduce the handicap principle and contrast it to the well-known principle of least effort that has influenced different theories in library and information science. In order to exemplify the potentials of the handicap principle, we will focus on the practice of academic book reviewing. We will demonstrate that the principle of least effort fails to explain the prosocial behaviour of academic book reviewing and instead formulate and test an alternative explanation based on the handicap principle. In doing so, we hope to inspire more library and information science researchers to take up the handicap principle as a theory for their own work.

## Academic book reviewing

The practice of book reviewing in academia is as old as the scientific community itself. The earliest journals, commenced in the major European countries in the latter part of the seventeenth century, consisted for the most part of book notices; and Journal des Scavans, the first periodical to provide regular information on scientific matters, was in fact composed entirely of summaries of Scholarly and scientific works. Today, countless academic journals in all fields either contain a section devoted exclusively to book reviews or publish reviews of interest to those in the field from time to time. Some journals even operate exclusively as book reviewing journals.

Book reviews are sometimes referred to as "published peer reviews" (e.g., [Hyland 2000](#hyl00); [Schubert _et al._ 1984](#sch84)). They are widely used to help estimate the quality and importance of books published by academics, and are thus instrumental in decisions about hiring, promotions, and salary increases ([Glenn 1978](#gle78)). Librarians and information specialists rely to a great extent on book reviews for developing and maintaining library and information center collections, and various commentators (e.g., [Chen 1976](#che76); [Ingram & Mills 1989](#ing89); [Miranda 1996](#mir96); [Snizek & Fuhrman 1979](#sni79)) have furthermore called attention to the fact that book reviews are valuable academic tools, as they make it feasible for members of scientific communities to keep up with the latest professional progress despite the eternal growth and dissemination of recorded knowledge.

Although academic book reviews actually serve a number of vital functions, the quality and worth of the genre are often questioned. Academic book reviews are regularly charge with merely reflecting individual opinions, which, according to their critics, disqualify them entirely as scholarly contributions ([Sabosik 1988](#sab88)). The genre has even been branded "a second-class citizen of scientific literature" ([Riley & Spreitzer 1970](#ril70)), while others have noted that academic book reviews rarely receive the credit or attention they deserve ([Moxley 1992](#mox92)). To some extent these objections are supported by bibliometric analyses, which have revealed that the scholarliness of academic book reviews varies significantly between and within fields ([Nicolaisen 2002a](#nic02a)), and that book reviews are hardly ever cited ([Diodato 1984](#dio84)). Notice also that book reviews are not treated as citable units in the traditional journal impact factor published yearly by the Institute of Scientific Information.

The principle of least effort ([Zipf 1949](#zip49)) is a cost-benefit principle founded on the premise that "in performing tasks (e.g., writing or speaking) individuals adopt a course of action that will expend the probable least average of their work - the least effort" ([Case 2005](#cas05)). From this cost-benefit perspective, it is consequently strange to note that many scholars choose to review books. The net benefit of their behaviour appears to be close to nothing whereas the time and intellectual reflection required reading and reviewing books are quite extensive. Reviewing books also disobeys with a related cost-benefit view of scholarly behaviour formulated by Sandstrom ([1994](#san94); [1998](#san98); [1999](#san99); [2001](#san01); [2004](#san04)). Sandstrom's view of scholarly behaviour is inspired by the socio-ecological theory of optimal foraging (OFT) and its basic assumption that "organisms will behave as if they are optimizing some fitness-related currency or set of currencies" ([Sandstrom 1994](#san94) [citing [Kaplan & Hill 1992](#kap92)]). According to optimal foraging theory "a particular prey type will be included in the optimal diet only if its net energy return per unit handling time is greater than the average return rate (including search time) for all prey types of higher rank" ([Sandstrom 1994: 425](#san94) [citing [Smith 1983](#smi83)]). In other words, the foraging behaviour of any organism is thought to be a balance between physical cost and benefit. If the cost of including a particular prey type into an organism's diet exceeds the net benefit of doing so, the organism simply will avoid inclusion. Sandstrom believes that it is probable that scholars, too, choose among information resources according to the same basic principle: "Scholars rarely undertake an extensive, active hunting-style search for information if it can be avoided" ([Sandstrom 2004: 16](#san04)). "Scholars (both as readers and writers) are likely to maximize their interaction with an array of resources offering higher returns in terms of handling and to minimize their efforts in procuring the obscure ones" ([Sandstrom 1994: 428](#san94)). Although Sandstrom has focused most of her efforts on proving the applicability of OFT for the prediction and explanation of scholars' information seeking behaviour, the passage above reveals a deeper conviction - i.e. that OFT also can account for scholars' publication behaviour. However, the case of academic book reviewing seems to contradict Sandstrom's conviction. From a purely cost-benefit perspective, book reviewing appears to be a waste of time and energy - something that the theory of optimal foraging behaviour cannot handle.

## The handicap principle

The socio-ecological theory of optimal foraging has been challenged for more than two decades. A number of anthropological studies have revealed that human foragers often violate the cost-benefit principle underlying the theory. The anthropologist Eric Alden Smith, who was one of the leading figures of OFT during the 1980s, now grants that the theory of optimal foraging fails to explain much of human foraging behaviour. He actually acknowledged the many problems of OFT the year preceding Sandstrom's first paper on the subject ([Smith 1993](#smi93)). Like many others, E.A. Smith now subscribes to the handicap principle a.k.a. the theory of costly signaling (e.g., [Bliege Bird, Smith & Bird 2001](#bli01); [Smith, Bliege Bird & Bird 2003](#smi03)). Remarkably, Sandstrom seems completely unaware of this. Although E.A. Smith is among her top recitees, all of Sandstrom's references to his work predate 1993, and none of her publications deal with nor even mention the handicap principle.

In short the handicap principle states that waste indicates that an organism has resources to spare. By advertising waste the organism shows its superiority. Only resourceful organisms can afford to waste. Thus, waste makes advertising of superiority reliable. The handicap principle has proved useful for unraveling an array of anthropological puzzles, including what from an OFT perspective looks like inefficient foraging behaviour. In the early 1980s, female anthropologists showed that in most hunter-gatherer societies women provide most of the food, efficiently collecting plant foods and small game ([Dahlberg 1981](#dah81)). The men often fail to bring any meat back from the hunt and often rely on their female partners for day-to-day provisions. Trying to chase down large animals that have evolved to run away from predators much faster than man is simply not an efficient, reliable way of foraging. Miller ([2000](#mil00)) reports that anthropologists have found that in some tribes, men only have a 3 percent chance per day of successfully killing a large animal. Data from other tribes have shown slightly higher success rates, but rarely exceed 10 percent each day. Humans know perfectly well that their hunting success is much higher when they go after smaller, slower, weaker animals. Usually, the smaller the prey type, the more pounds of meat per day they bring home, and the less variable the amount of meat from one week to the next. Also, the smaller the animal, the more of its meat could be eaten before it goes rotten. Proponents of the handicap principle (e.g., [Bliege Bird, Smith & Bird 2001](#bli01); [Hawkes 1990](#haw90); [1991](#haw91); [1993](#haw93); [Smith, Bliege Bird & Bird 2003](#smi03)) suggest that men's contribution to subsistence may have evolved and may persist because men establish and maintain their relative social prestige by showing off their hunting skill. From an OFT perspective such foraging behaviour is not optimal. It clearly violates the basic premise of the theory, namely that if the cost of including a particular prey type into an organism's diet exceeds the net benefit of doing so, the organism simply will avoid inclusion. Yet, such wasteful behaviour fits well with Zahavi's handicap principle.

Prestige may be gained by investing in wasteful activities as well as by investing in so-called altruistic activities ([Zahavi 1995](#zah95)). Investments involved in the altruistic activities serve as honest signals of the ability of the individual to help the group and its quality as a collaborator. Zahavi ([1995: 2](#zah95)) argues that the handicap principle provides a general solution to the problem of altruism:

> "[T]he investment in the welfare of the group, or of its members, functions to advertise the quality and motivation of the helper. The advertiser gains from its investment by increasing its "social prestige". Helping may thus be considered as a simple selfish character". Academic book reviewing can be understood as an altruistic activity preferred by strong researchers, motivated by expected increase of social prestige. Like hunting, book reviews are showy and thus serve as effective signals for the ability to waste. Reading books and writing reviews take time, time that could be spent more efficiently on research and the writing of research papers. Book reviews are thus costly signals that the strong researcher can afford to a larger extent than the weak researcher. We consequently suggest understanding the act of scholarly book reviewing as an act of investment in the interests of academia for increased social prestige.

## Method

The following case study aims to examine empirically the potentials of the theory of the handicap principle as a framework for understanding and explaining scholarly behaviour exemplified by the act of book reviewing. The following hypothesis provides the focus of the study:

> Book reviews are costly signals primarily preferred by strong researchers.

In order to turn it into a testable hypothesis we need to operationalize some fundamental concepts.

First of all we need to determine how strength in scientific communities is to be measured. The legitimizing and control functions of the publication system in science confirm that analyses of the organization and operation of the communication system is central in analysing the communication in science ([Whitley 2000](#whi00)). Thus strength in scientific communities is highly related to scholarly research quality and thus to quality of scholarly publications. As stated by Moed ([Moed 2005: 25](#moe05)) research quality reflects objectivity only to a limited extent and it is not easily defined and measured. In order to assess the quality of research we have a choice between bibliometrics (including both publication and citation analysis) and peer review. It should be noted that the results achieved by using these three methods may be essentially divergent. On the level of the individual papers there seems to be a slight correlation between peer review and citation rates. Cole ([1989](#col89)) shows that even before receiving the Nobel Price publications by Nobel Laureates have a high citation rate. However, Nicolaisen ([2002b](#nic02b)) proves the correlation not to be linear but rather J-shaped. Publications of very high quality receive a high number of citations but that is also the case for publications of very low quality. Van Dalen and Henkens ([2001](#van01)) stress that the citation rate of an article is influenced by various factors such as publication in core journals, language, location of the article in the specific issue, position of the author and other factors. Strength in scientific communities is thus difficult to measure by citation analysis as we risk integration of a group of the weakest individuals into the group of the strongest individuals.

The alternative is peer review. But peer reviewing is not flawless either as pointed out by e.g. Wennerås and Wold ([1997](#wen97)). Gunn ([2004](#gun04)) states as well as being very labour intensive tying up an impressive oeuvre of experts for prolonged periods; this approach has led to many of the same controversies that seem to be the case of citation rates. In his opinion the results of the peer review process are in many cases very close to simply weighting by best journal of publication. Gunn wonders why expert assessment is not dramatically better and finds that fundamentally the two methods suffer from the same deficiency. The problem is that it is often not clear just how important a paper will turn out to be until many years have passed. Publication in a good journal infers at a minimum that the referees and editor thought at the time that the paper had value and it should not be a surprise that an expert panel often thinks the same. Following the arguments of Gunn, we measure strength by the number of publications accepted in peer reviewed journals.

However, we need to determine which publication types should be included and which should be excluded from the analysis. We include the so-called source items or citable units used in the calculation of the journal impact factor, which is the three following document types: Article, note, review [[3]](#note3). We also include the document type 'letter' as recommended by Christensen, Ingwersen and Wormell ([1997](#chr97)). Furthermore, we need to determine if all publications should be counted as one unit or if a scale measuring journal quality should be used. According to Oster ([1980](#ost80)) an author's choice of journal when publishing an article is affected by a number of factors. The motivation for publishing (e.g. prestige, large audience and fast publication process) will shape the optimal order of journals to submit to. Taylor and Harding ([1997](#tay97)) note that journals attract different papers according to their profile and therefore it is nonsensical to judge a poor paper in a good journal superior to a good paper in a poor journal. Drawing on these two arguments no scale is being used in the analysis other than the publications must be peer reviewed and indexed in the Social Sciences Citation Index.

Drawing upon the operationalizations above the testable hypothesis is defined as follows: Book reviews are primarily published by authors who have published several articles, notes, reviews or letters and the authors publishing book reviews continue to publish articles, notes, reviews or letters.

The analysis in the present paper is based on a selection of authors who wrote their PhD thesis within economics. The authors all belong to the same discipline as it is necessary to collect a rather homogeneous data set in order to keep the number of variables at a reasonable level. As we need to collect data on all their publications a fairly long publication window is needed, however, we also would like the analysis to be as up to date as possible and thus 1985 is the compromise chosen for the data sample. The authors who earned their PhD degree writing a thesis within economics in 1985 are determined using Dissertation Abstracts Online which is a guide to American dissertations accepted at accredited institutions. This results in a data sample consisting of 1374 authors and data on their publications (until 2006) is collected using Social Sciences Citation Index (SSCI). Some authors with common names cannot be positively identified in SSCI and these authors are excluded from the data collection. For every author in the data sample the number of articles, notes, reviews and letters per book review is recorded. In some cases it can be difficult to determine the chronology of publications which is necessary in order to establish the number of articles, reviews or notes per book review. If publications have the same publication year we use the chronology set by the SSCI, although it should be noted that this chronology is determined by the date of entering the database and not necessarily the date of publication. Examples of the data collection are available in table 1 (in the data sample the number of publications per book review is registered for all book reviews but in order to preserve an overview in the examples four book reviews is maximum).

<table><caption>

**Table 1: Examples of data collection**</caption>

<tbody>

<tr>

<td>

**Author id**</td>

<td>

**Number of publication per first book review**</td>

<td>

**Number of publication per second book review**</td>

<td>

**Number of publication per third book review**</td>

<td>

**Number of publication per fourth book review**</td>

</tr>

<tr>

<td>0924</td>

<td>5</td>

<td>4</td>

<td>3</td>

<td>2</td>

</tr>

<tr>

<td>0925</td>

<td>3</td>

<td>4</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>0926</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

</tbody>

</table>

As we can see in table 1 in the publication period author 0924 publishes 4 book reviews and 14 articles, reviews or notes (note that any publications after the fourth book review are not counted as there are no book reviews among). Author 0925 publishes 2 book reviews and 7 articles, reviews or notes. There is no data on author 0926 as this author does not publish book reviews in the publication period.

## Results

Before making any conclusions on the basis of the results form the analyses we have to evaluate the method in order to be able to keep the appropriate reservations in mind. Firstly, we must bear in mind that we focus on the average author and it could potentially add another perspective if we focused on the few very strong individuals investing in a huge handicap and thus who write numerous book reviews. Secondly, we cannot generalize these findings to all disciplines as we only focus on one discipline. Finally, it is important to remember that the theory of the handicap principle as a framework for understanding scholarly behaviour offers part of the explanation, but not necessarily all of the explanation. Free review copies or the opportunity of having ones name associated with a famous author or work are just two other probable motives that may help to explain the act of academic book reviewing. It should also be borne in mind that not all academic book reviews are typical exemplars of the genre. Sometimes authors use the genre for other purposes than straightforward presentation and evaluation of books. Noam Chomsky's review of B.F. Skinner's Verbal Behavior is a good example of such an atypical book review. In the 32 pages long review, Chomsky ([1959](#cho59)) challenged not just the book under review, but the entire behaviourist approach to the study of mind and language dominant in the 1950s. The review helped to spark the cognitive revolution in psychology and may thus be seen more as a traditional research article than a typical book review.

Although, the quality and worth of book reviews are often questioned a considerable share of authors choose to write book reviews. As we can see in table 2, 13.5 per cent of all authors write one book review, 5 per cent write two book reviews and almost 3 per cent write 3 or more book reviews.

<table><caption>

**Table 2: Share of authors publishing a book review**</caption>

<tbody>

<tr>

<td>

**Number of book reviews written**</td>

<td>

**1**</td>

<td>

**2**</td>

<td>

**3 or more**</td>

</tr>

<tr>

<td>

**Number of authors in sample**</td>

<td>185</td>

<td>70</td>

<td>38</td>

</tr>

<tr>

<td>

**Per cent of authors in sample**</td>

<td>13.5</td>

<td>5.1</td>

<td>2.8</td>

</tr>

</tbody>

</table>

Even though book reviews rarely receive the credit or attention they deserve 13.5 per cent of authors in this data sample choose to write book reviews. The remarkable high share of authors choosing to write book reviews strengthen us in believing that authors are driven by other incentives than the credit received by the scientific community on the basis of the book review itself.

Figure 1 illustrates the distribution of the number of publications before writing the first book review. As we can see 15 per cent of authors writing a book review write their first book review as their first publication. 22 per cent writes their first book review when they have written 1 article, note, letter or review. Writing a book review on the basis of 2, 3 and 4 articles, notes, letters or reviews decreases from 13 to 4 per cent. Finally, 32 per cent writes their first book review when they have written 5 or more articles, notes, letters or reviews.

<figure>

![Distribution](../colis23.jpg)

<figcaption>

**Figure 1: Distribution of the number of publications before writing the first book review**</figcaption>

</figure>

This means that a large share of authors do not write their first book review until having published a considerable amount of articles, notes, letters or reviews. Book reviews are not only written by weak individuals who are not able to get their articles, notes, letters or reviews published.

However, we would like to be able to differentiate further between the authors writing book reviews. Therefore we investigate the differences between the first and second book review. Is the pattern different when we look at the second book review or put in other words: who continues writing book reviews - the weak or the strong author? We use the terminology also used by Braun, Glänzel, and Schubert ([2001](#bra01)) to categorize authors according to their scientific production and especially the categories of 'terminators' and 'continuants' are of interest to this study. First book review terminators are authors who write just one book review. First book review continuants are authors continuing to publish two or more book reviews. Second book review terminators write exactly two book reviews and second book review continuants write three or more book reviews. Obviously, this can be extended with any subsequent book reviews. Table 3 is an overview of the differences between the authors who continue writing book reviews and the authors who cease writing book reviews.

<table><caption>

**Table 3: The number of articles, notes, letters or reviews in total at the first and second book review**</caption>

<tbody>

<tr>

<td></td>

<td>

**Terminators**</td>

<td>

**Continuants**</td>

</tr>

<tr>

<td>

**First book review**</td>

<td>4.7</td>

<td>6</td>

</tr>

<tr>

<td>

**Second book review**</td>

<td>6.7</td>

<td>7.5</td>

</tr>

</tbody>

</table>

The differences between the two rows are expected as the numbers can only increase and what should be noticed in this table are the differences between the columns. First book review terminators (authors who write only one book review) write a lower number of articles, notes, letters or reviews on average. They publish 4.7 articles, notes, letters or reviews on average before their 1st book review whereas the continuants (authors continuing to publish two or more book reviews) publish 6 articles, notes, letters or reviews. The pattern repeats itself when we look at the second book review. Second book review terminators (authors who write their second and last book review) write a lower number of articles, notes, letters or reviews than the group of authors continuing to publish three or more book reviews. The first group publishes on average 6.7 articles, notes, letters or reviews in total before their second book review whereas the latter publishes 7.5 articles, notes, letters or reviews. We are not able to look into the patterns of the third or more book review as the data material is too small to make any solid conclusions. However, table 3 clearly depicts a picture of book reviews being published by strong individuals especially when we look at those who choose to continue writing book reviews.

These results fit the basic idea of the handicap principle, i.e. that if an individual is of high quality it may benefit from investing a part of its advantage in advertising that quality, by taking on a handicap. Consequently, the results support our thesis that academic book reviews are costly signals that the strong researcher engages in to a larger extent than the weak researcher.

## Conclusion

The handicap principle is a powerful theory for explaining prosocial behaviour. It facilitates insight and understanding of what goes on in social systems and why. It is consequently also a very relevant theory for library and information science researchers studying the social mechanisms of information systems. We have shown its feasibility as a theory for understanding the act of academic book reviewing - an act that is difficult to understand from traditional cost-benefit views such as the principle of least effort. The handicap principle thus extends the horizon of the field and enables library and information science researchers to study and better comprehend phenomena that traditional theories have failed to explain.

## Acknowledgements

This work was partly financed by a generous grant from the Nordic Research School in Library and Information Science (NORSlibrary and information science). The authors would like to thank Robert M. Wilkes and two anonymous referees for commenting on an earlier draft of the paper.

## Notes

<a id="note1">1.</a> "It is not unreasonable to assume that publications in a select group of journals are seen by the IS research community as fitness indicators, especially since publishing in them is rather difficult to achieve, which fits well with Zahavi and Zahavi's handicap principle. Also, we cannot forget that we humans are also animals, and thus are likely to have the propensity to behave in general accordance with the handicap principle when we assess other people's abilities" (Kock & Davison ([2003: 523](#koc03)).

<a id="note2">2.</a> Reframing library and information science from Different Perspectives was one of the major conference themes at the Colibrary and information science 6 Conference [[http://www.hb.se/colis/themes/default.htm](http://www.hb.se/colis/themes/default.htm)]

<a id="note3">3.</a> According to ISI all full articles - original and review - are counted as source items. In addition, any shorter item with full author information and abstract may be counted, especially if cited references are included. Thus, in some instances, technical notes qualify as source items as may case notes. Editorials and commentaries are not counted as source items, nor are meetings abstracts. Letters are typically not counted; however there are room for exception in cases where they function as "articles" within a journal. Supplements are somewhat more problematic and are treated on a case-by-case basis ([O'Neill 2000](#one00)).

## References

*   <a id="amb04"></a>Ambler, T. & Hollier, E.A. (2004). The waste in advertising is the part that works. _Journal of Advertising Research_, **44**, 375-389\.
*   <a id="bli01"></a>Bliege Bird, R., Smith, E.A. & Bird, D.W. (2001). The hunting handicap: Costly signaling in human foraging strategies. _Behavioral Ecology and Sociobiology_, **50**, 9-19\.
*   <a id="bra01"></a>Braun, T., Glanzel, W. & Schubert, A., (2001). Publication and Cooperation Patterns of the Authors of Neuroscience Journals. _Scientometrics_, **51**(3), 499-510\.
*   <a id="cas05"></a>Case, D.O. (2005). Principle of least effort. In: K.E. Fisher and S. Erdelez (eds.) _Theories of Information Behavior_. Medford, NJ: Information Today, pp. 289-292\.
*   <a id="che76"></a>Chen, C.C. (1976). _Biomedical, Scientific and Technical Book Reviewing_. Metuchen, NJ: Scarecrow Press.
*   <a id="cho59"></a>Chomsky, N. (1959). Review of Verbal Behavior [book review]. _Language_, **35**, 26-58\.
*   <a id="chr97"></a>Christensen, F.H., Ingwersen, P. & Wormell, I. (1997). Online determination of the journal impact factor and its international properties. _Scientometrics_, **40**(3), 529-540\.
*   <a id="col89"></a>Cole, S. (1989). Citations and the evaluation of individual scientists. _Trends in Biochemical Sciences_, **14**, 9-13\.
*   <a id="cro91"></a>Cronin, H. (1991). _The Ant and the Peacock: Altruism and Sexual Selection from Darwin to Today_. Cambridge, UK: Cambridge University Press.
*   <a id="dah81"></a>Dahlberg, F. (ed.) (1981). _Woman the Gatherer_. New Haven, CN: Yale University Press.
*   <a id="daw76"></a>Dawkins, R. (1976). _The Selfish Gene_. Oxford, UK: Oxford University Press.
*   <a id="dio84"></a>Diodato, V. (1984). Impact and scholarliness in arts and humanities book reviews: A citation analysis. _Proceedings of the 47th ASIS Annual Meeting_, 217-220\.
*   <a id="gle78"></a>Glenn, N. (1978). On the misuse of book reviews. _Contemporary Sociology_, **7**(3), 254-255\.
*   <a id="gun04"></a>Gunn, A.J. (2004). [Damned statistics](http://www.bmj.com/cgi/content/full/329/7471/0-h). _BMJ, online comments_, October 19\. Retrieved June 12, 2007 from http://www.bmj.com/cgi/content/full/329/7471/0-h.
*   <a id="haw90"></a>Hawkes, K. (1990). Why do men hunt? Some benefits for risky choices. In: E. Cashdan (ed.) _Risk and Uncertainty in Tribal and Peasant Economies_. Boulder, CO: Westview Press, pp. 145-166\.
*   <a id="haw91"></a>Hawkes, K. (1991). Showing off: Tests of an hypothesis about men's foraging goals. _Ethology and Sociobiology_, **12**, 29-54\.
*   <a id="haw93"></a>Hawkes, K. (1993). Why hunter-gatherers work. _Current Anthropology_, **34**(4), 341-362\.
*   <a id="hyl00"></a>Hyland, K. (2000), _Disciplinary Discourses: Social Interactions in Academic Writing_. Harlow, UK: Longman.
*   <a id="ing89"></a>Ingram, H. & Mills, P.B. (1989). Reviewing the book reviews. _PS: Political Science and Politics_, **22**(3), 627-634\.
*   <a id="kap92"></a>Kaplan, H. & Hill, K. (1992). The evolutionary ecology of food acquisition. In E.A. Smith and B. Winterhalder (eds.) _Evolutionary Ecology and Human Behavior_. New York, NY: Aldine de Gruyter, pp. 167-202\.
*   <a id="koc03"></a>Kock, N. & Davison, R. (2003). Dealing with plagiarism in the information systems research community: A look at factors that drive plagiarism and ways to address them. _MIS Quarterly_, **27**(4), 511-532\.
*   <a id="mil00"></a>Miller, G. (2000). _The Mating Mind: How Sexual Choice Shaped the Evolution of Human Nature_. New York, NY: Doubleday.
*   <a id="mir96"></a>Miranda, E.O. (1996). On book reviewing. _Journal of Educational Thought_, **30**(2), 191-202\.
*   <a id="moe05"></a>Moed, H.F. (2005). _Citation Analysis in Research Evaluation_. Dordrecht, NL: Springer.
*   <a id="mox92"></a>Moxley, J.M. (1992). _Publish don't Perish: The Scholar's Guide to Academic Writing and Publishing_. Westport, CT: Greenwood Press.
*   <a id="nic02a" name="nic02a"></a>Nicolaisen, J. (2002a). The scholarliness of published peer reviews: A bibliometric study of book reviews in selected social science fields. _Research Evaluation_, **11**(3), 129-140\.
*   <a id="nic02b" name="nic02b"></a>Nicolaisen, J. (2002b). The J-shaped distribution of citedness. _Journal of Documentation_, **58**(4), 383-395\.
*   <a id="nic04"></a>Nicolaisen, J. (2004). _Social Behavior and Scientific Practice - Missing Pieces of the Citation Puzzle_. Copenhagen, DK: Royal School of Library and Information Science. PhD thesis.
*   <a id="nic07"></a>Nicolaisen, J. (2007). Citation analysis. _Annual Review of Information Science and Technology_, **41**, 609-641\.
*   <a id="nor02"></a>Nørretranders, T. (2002). _The Generous Man: How Helping Others is the Most Sexiest Thing You Can Do_. New York: Thunder's Mouth Press.
*   <a id="one00"></a>O'Neill, J. (2000). The significance of an impact factor: Implications for the publishing community. _Learned Publishing_, **13**(2), 105-109\.
*   <a id="ost80"></a>Oster, S. (1980). The optimal order for submitting manuscripts. _The American Economic Review_, **70**(3), 444-448\.
*   <a id="ril70"></a>Riley, L.E. & Spreitzer, E.A. (1970). Book reviewing in the social sciences. _American Sociologist_, **5**, 358-363\.
*   <a id="sab88"></a>Sabosik, P.E. (1988). Scholarly reviewing and the role of choice in the postpublication review process. _Book Research Quarterly_, **Summer**, 10-18\.
*   <a id="san94"></a>Sandstrom, P.E. (1994). An optimal foraging approach to information seeking and use. _Library Quarterly_, **64**(4), 414-449\.
*   <a id="san98"></a>Sandstrom, P.E. (1998), _Information Foraging among Anthropologists in the Invisible College of Human Behavioral Ecology: An Author Co-Citation Analysis_. Bloomington, IN: Indiana University. PhD thesis.
*   <a id="san99"></a>Sandstrom, P.E. (1999). Scholars as subsistence foragers. _Bulletin of the American Society for Information Science_, **25**(3), 17-20\.
*   <a id="san01"></a>Sandstrom, P.E. (2001). Scholarly communication as a socioecological system. _Scientometrics_, **51**(3), 573-605\.
*   <a id="san04"></a>Sandstrom, P.E. (2004). Anthropological approaches to information systems and behaviour. _Bulletin of the American Society for Information Science and Technology_, **30**(3), 12-16\.
*   <a id="sch84"></a>Schubert, A. _et al_. (1984). Quantitative analysis of a visible tip of the peer review iceberg: Book reviews in chemistry. _Scientometrics_, **6**(6), 433-443\.
*   <a id="smi83"></a>Smith, E.A. (1983). Anthropological applications of optimal foraging theory: A critical review. _Current Anthropology_, **24**(5), 625-651\.
*   <a id="smi93"></a>Smith, E.A. (1993). Comment on Hawkes K: Why hunter-gatherers work: An ancient version of the problem of public goods. _Current Anthropology_, **34**, 356\.
*   <a id="smi03"></a>Smith, E.A., Bliege Bird, R. & Bird, D.W. (2003): The benefits of costly signalling: Meriam turtle hunters. _Behavioral Ecology_, **14**(1), 116-126\.
*   <a id="sni79"></a>Snizek, W.E. & Fuhrman, E.R. (1979). Some factors affecting the evaluative content of book reviews in sociology. _American Sociologist_, **14**, 108-114\.
*   <a id="tay97"></a>Taylor, K. & Harding, G. (1997). Publishing in the PJ - why bother? _The Pharmaceutical Journal_, **258**, 269
*   <a id="van01"></a>Van Dalen, H. & Henkens (2001). What makes a scientific article influential? The case of demographers. _Scientometrics_, **50**(3), 455-482\.
*   <a id="wen97"></a>Wennerås, C. & Wold, A. (1997). Nepotism and sexism in peer-review. _Nature_, **400**, 341-343
*   <a id="whi00"></a>Whitley, R. (2000). _The Intellectual and Social Organization of the Sciences_. Oxford, UK: Oxford University Press. 2nd ed.
*   <a id="zah75"></a>Zahavi, A. (1975). Mate selection: Selection for a handicap. _Journal of Theoretical Biology_, **53**(1), 205-214\.
*   <a id="zah95"></a>Zahavi, A. (1995). Altruism as a handicap - the limitations of kin selection and reciprocity. _Journal of Avian Biology_, **26**(1), 1-3\.
*   <a id="zah03"></a>Zahavi, A. (2003). Indirect selection and individual selection in socio-biology: My personal views on theories of social behaviour. _Animal Behaviour_, **65**, 859-863\.
*   <a id="zah97"></a>Zahavi, A. & Zahavi, A. (1997). _The Handicap Principle_. New York, NY: Oxford University Press.
*   <a id="zip49"></a>Zipf, G.K. (1949). _Human Behavior and the Principle of Least Effort: An Introduction to Human Ecology_. Cambridge, MA: Addison-Wesley.