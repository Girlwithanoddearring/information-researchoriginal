#### Vol. 12 No. 4, October, 2007

* * *

# Concentration of Web users' online information behaviour

#### [Chun-Yao Huang](mailto:cyhuang@management.ntu.edu.tw)  
Department of Business Administration, National Taiwan University, Taipei, Taiwan  
[Yung-Cheng Shen](mailto:ycs@saturn.yzu.edu.tw)  
Department of Business Administration, Yuan Ze University, Taoyuan, Taiwan  
[I-Ping Chiang](mailto:ipchiang@mail.ntpu.edu.tw)  
Department of Information Management, National Taipei University, Taipei, Taiwan  
[Chen-Shun Lin](mailto:ericlin@pmiims.com)  
Department of Statistics, Renmin University of China, Beijing, China

#### Abstract

> **Introduction**. Focusing on Web users' behavioural concentration across Websites they have visited, we investigate heterogeneity in Web users' online information behaviour.  
> **Method.** The Gini coefficient is used to measure the degree of a Web user's online information behavioural concentration in terms of both page-views and visit duration. We explore how the behavioural dimensions of the number of sites visited, the number of page-views per site and the duration per page predict online information behavioural concentration.  
> **Analysis.** Data from an online panel are analysed using multiple regression models, which reveal that the three dimensions of online information behaviour predict more than three quarters of the variances in behavioural concentration.  
> **Results.** The number of sites visited and the number of page-views per site positively predict the degree of behavioural concentration (in terms of both page-views and visit duration), while the speed dimension of online information behaviour positively predicts the degree of behavioural concentration in terms of page-views but negatively predicts that in terms of visit duration. The relative importance of variables in the explanation of Web users' degree of behavioural concentration is also analysed.  
> **Conclusion.** The quantitative analytical framework presented herein gives insight into the heterogeneity of online information behaviour. This paper is a stepping-stone for a more comprehensive understanding of online information behaviour from a macro perspective.

## Introduction

Being a hyperlinked hypermedia information system ([Huberman __et al.__ 1998](#hub89); [Kari and Savolainen 2003](#kar03)), the World Wide Web (hereafter, the Web) is an interactive environment as well as a multi-channel communication tool ([Wikgren 2001](#wik01)). The Web can act as an instrument of communication, education, business, entertainment, finance, staying informed, passing time, relaxing, escape, socialization, work, surveillance, etc. ([Kari and Savolainen 2003](#kar03); [Song _et al._ 2004](#son04)). On the Web, users follow links, enter queries, and scan pages ([Pharo 2004](#pha04)). In everyday life, Web users search and retrieve, browse, monitor, unfold, exchange, dress, instruct, and publish content in various guises of information ([Hektor 2003](#hek03)). The Web, to borrow from Savolainen ([1995](#sav95)), has gradually rooted in its users a 'way of life' as well as a 'mastery of life'.

Although the literature on online information behaviour has been growing with the prevalence of the Internet, most published studies focus on micro-level behaviour confined to specific domains or tasks (e.g., [Koufaris 2002](#kou02); [D'Ambra and Wilson 2004](#dam04); [Hope and Li 2004](#hop04); [Jansen _et al._ 2005](#jan05); [Tombros _et al._ 2005](#tom05)), study a specific segment of Web users (e.g., [Hsieh-Yee 1998](#hsi98); [Rieh and Belkin 2000](#rie00); [Zhang _et al._ 2004](#zha04); [Andrews _et al._ 2005](#and05)), explore the motives of Web users (e.g., [Bilal 2002](#bil02); [Savolainen 2004](#sav04); [Song _et al._ 2004](#son04)), or propose conceptual models of online information seeking behaviour (e.g., [Hektor 2003](#hek03); [Kari and Savolainen 2003](#kar03); [McKenzie 2003](#mck03)). By its nature, the Web is able to keep every user's behavioural details and, therefore, is (at least potentially) a perfect platform for an in-depth analysis of traceable human information behaviour. In a commercial setting, descriptive statistics of visitors' online information actions (such as page-views, visit duration, visit sessions, etc.) at competing sites have been constantly reported. However, owing to the difficulty in accessing data that client-side software collects under which such cross-site reports are compiled, other than efforts sush as Toms _et al._ ([2003](#toms03)), Spink and Jansen ([2004](#spi04b)), and Jansen and Spink ([2006](#jan06)), which examine Web searching across multiple systems and domains, little research has been done so far by academics to empirically explore behavioural patterns of Web users' everyday Web usage in a systematic way.

Given the established literature as a foundation, this paper studies online information behaviour from the perspective of 'behavioural concentration' and aims to serve as a stepping-stone for further in-depth understanding of how the Web is used by heterogeneous users. In terms of behavioural concentration, we focus on how a Web user distributes his/her page-views and visit duration among a set of sites that s/he visited during a period of time. For both page-view and visit duration, the behavioural concentration is measured by the Gini coefficient. Coupling informetrics with an online panel in the empirical study, we are able to provide a holistic picture that parsimoniously quantifies how users consume information on the Web and how these users' behaviour differs from each other.

## Background

### Online information behaviour

According to Wilson _et al._ ([1999](#wil99)), information behaviour is behaviour engaged in by persons in relation to information sources and channels, which cover information seeking behaviours that may be either active or passive. [Spink and Cole (2004)](#spi04) further elaborate the concept of information behaviour as 'a broad term covering all aspects of information seeking, including passive or undetermined information behavior'. Therefore, online information behaviour, by definition, includes all activities that users conduct on the Web, be it goal-directed searching or just surfing without a specific purpose.

The literature also indicates that the multi-facet Web, to its heterogeneous users, is taken as an _information environment_ where hypermedia and hypertext documents are linked to one another ([Kari and Savolainen 2003](#kar03)), an _information space_ which the user has navigated before or can navigate ([Pharo 2004](#pha04)), an _information horizon_ which is an imaginary field or subjective map of source preferences in which information sources are given various positions ([Sonnewald 1999](#son99); [Savolainen and Kari 2004](#sav04)), and an _information world_ that individuals resort to when facing information needs ([Bruce _et al._ 2004](#bru04)). Therefore, the Web has rich cognitive and affective, as well as conatative implications for its multifarious users.

To analyse online information behaviour, various conceptual models have been proposed with a focus on behavioural or attitudinal typology. Hektor ([2003](#hek03)) suggests eight forms of information activities in a model of online information behaviour in everyday non-work life. Synthesizing Marchionini's ([1995](#mar95)) and Wilson's ([1997](#wil97)) typology of browsing modes and Ellis's ([1989](#ell89)) general information seeking model, Choo _et al._ ([1999](#cho99)) propose an integrated conceptual model of browsing and searching on the Web in the form of an analytical matrix that typifies behavioural modes and moves of information behaviour on the Web. Following the use and gratification paradigm, Song _et al._ ([2004](#son04)) attribute multiple gratifications that Web users seek online to the dichotomy of _process_ and _content_ gratifications. Empirically, most recent studies of online information behaviour focus on specific sites (e.g., [Bucklin and Sismeiro 2003](#buk03); [Mat-Hassan and Levene 2005](#mat05); [Zhang _et al._ 2004](#zha04)), unique contexts (e.g., [D'Ambra and Wilson 2004](#dam04); [Kim 2001](#kim01); [Rieh 2004](#rie04); [Savolainen and Kari 2004](#sav04); [Spink _et al._ 2004](#spi04c)), or segments of users (e.g., [Song _et al._ 2004](#son04); [Cole _et al._ 2005](#col05)) through experiments (e.g., [Kim 2001](#kim01); [Lin 2005](#lin05); [Tombros _et al._ 2005](#tom05)), surveys (e.g., [D'Ambra and Wilson 2004](#dam04); [Slone 2003](#slo03); [Song _et al._ 2004](#son04); [Tabatabai and Shore 2005](#tab05)), and clickstream data analysis focusing on the interaction at information aggregators (e.g., [Silverstein _et al._ 1999](#aasil); [Jansen _et al._ 2000](#jan00); [Anick 2003](#ani03); [Mat-Hassan and Levene 2005](#mat05); [Park _et al._ 2005](#par); [Agichtein _et al._ 2006](#agi06)) for a specific set of Websites.

The most relevant studies for our research in the growing body of online information behaviour literature are those focusing on observable browsing strategies and tactics ([Catledge and Pitkow 1995](#cat95); [Tauscher and Greenberg 1997](#tau97); [Huberman _et al._ 1998](#hub89); [Nicholas _et al._ 2003](#nic04); [Bucklin and Sismeiro 2003](#buk03); [Zauberman 2003](#zau03); [Johnson _et al._ 2003](#joh03); [Kari 2004](#kar04)). Most of this line of research looks at the micro-level online information behaviour. At the relatively early stage of the Web's development, Catledge and Pitkow ([1995](#cat95)) find that more than 90% of page downloads by users in front of a browser were made through hyperlink clicking and the _back_ function built in the browser, a finding empirically echoed almost a decade later by Kari ([2004](#kar04)). They also establish that users tend to navigate only a small area within a particular site in a _hub and spoke_ manner. Tauscher and Greenberg ([1997](#tau97)) support this very finding and further indicate that Web users use a few pages frequently and use short, repeated navigation paths. Huberman _et al._ ([1998](#hub89)) implement the inverse Gaussian distribution to model the _strong regularities_ found in Web surfing. Nicholas _et al._ ([2003](#nic04)) characterize online information behaviour as 'one of bouncing in which users seldom penetrate a site to any depth... and seldom return to sites they [visitors] once visited'. According to them, online information behaviour of Web users therefore is 'seemingly shallow, promiscuous and dynamic'. Bucklin and Sismeiro ([2003](#buk03)), Zauberman ([2003](#zau03)), and Johnson _et al._ ([2003](#joh03)) from various approaches support the hypotheses that online information behaviour is guided by cost-benefit tradeoffs and, therefore, people may lock themselves into a small set of Websites.

### Research questions

In spite of the scope of recent conceptual and empirical studies on online information behaviour, there are vacuums in the literature awaiting a systematic investigation for a more comprehensive understanding of how the Web is used. Although there have been efforts to classify users (e.g., [Jaillet 2002](#jai02); [Sheehan 2002](#she02)), studies focusing on observable browsing strategies and tactics as we have discussed above tend to neglect differences in users' overall online information behaviour. People may differ in the number of Websites navigated, number of page downloads within a site, and/or speed when they use the Web for whatever information is needed. Overall, people are likely to have very different behavioural profiles on the Web. Exploring such differences and the underlying factors leading to such differences may provide valuable insight into Web users' online information behaviour. Owing to the difficulty in accessing client-side data collected across sites, however, heterogeneity in Web users' Web usage patterns has seen little discussion in the literature.

The fact that heterogeneity in Web users' usage patterns has not yet been systematically analysed results in ambiguities that obstruct our in-depth understanding of online information behaviour. For example, it is reported that people seldom go back to Websites they visited before ([Nicholas _et al._ 2003: 24](#nic04)). It has also been established that Web users develop a _hub and spoke_ ([Catledge and Pitkow 1995](#cat95)) structure of navigation paths within a site in a repeated way ([Tauscher and Greenberg 1997](#tau97)). However, little is known about the more holistic picture. Does the typical cross-site behaviour of a user's everyday Web usage also include a core set of sites on which people concentrate most of their online activities and do they use this core set of sites to anchor their online navigation and exploration? If so, how do people differ in the size and the relative importance of the core?

Studies supporting _lock-in_ online information behaviour (e.g., [Bucklin and Sismeiro 2003](#buk03); [Zauberman 2003](#zau03); [Johnson _et al._ 2003](#joh03)) claim that because the switching cost increases with online experience, experienced Web users are likely to be locked into a very small set of Websites with which they are familiar, for the fulfilment of a specific information need. If this is certain, then, when facing increasing switching costs, these locked-in Web users are unlikely to make substantial efforts to explore the Web and will stick to familiar sites. On the other hand, in everyday life a typical Web user has multifarious information needs. Aggregating online activities for the fulfilment of these needs, what kind of pattern emerges as to Web users' overall online information behaviour?

To clarify these ambiguities, we propose to look at online information behaviour from the perspective of _behavioural concentration_. By _behavioural concentration_, we mean the degree to which a Web user concentrates his/her page-view downloads and visit duration among a set of sites visited (we illustrate behavioural concentration and operationalise the concept in the following Method section). The concept is derived from the work of researchers in author productivity studies who have determined that, 'a large portion of the productivity of a whole domain is "concentrated" on a relatively small number of authors', such that observing the degree of _concentration_ in a domain serves to characterize the heterogeneity of author productivity in that domain ([Yoshikane _et al._ 2003: 521](#yos03)). Following this idea, the differences in Web users' online behavioural profiles can be parsimoniously characterized by heterogeneity in their degrees of behavioural concentration. As a Web user typically visits multiple Websites during a substantial period of time, s/he spends more time on and reads more pages from a few of the sites that s/he visits. This _concentration_ focus is potentially important in opening a door to new avenues, which complement current ones, to understanding online information behaviour.

Focusing on behavioural concentration, by the analytical approach proposed below we aim to address the following questions:

1.  How do Web users (people who conduct any activity on the Web which leads to page downloads from Websites) differ in their behavioural concentration?
2.  How does one account for heterogeneity in such online information behaviour concentration? In other words, what factors predict the degree of behavioural concentration at the individual level?
3.  What insight can be gained from empirically analysing heterogeneity in such behaviour concentration?
4.  Especially since behavioural concentration is a new perspective to analysing online information behaviour, what pattern emerges from such an analysis to complement our understanding of online information behaviour?

## Method

### Concentration and the Gini Coefficient

Given the research questions, we are interested in exploring the degrees of concentration of Web users' online information behaviour and finding explanations of heterogeneity in degrees of concentration.

Online information behaviour here is operationally measured by every additional click of a mouse or ever keyboard depression by a user in an online mode. A user's online information behaviour thus encompasses all activities that s/he undertakes on the Internet for the sake of purchasing, occupationally-related task completion, information bank building, self-expression, maintaining interpersonal relationships, pleasure pursuing or experiencing interests, and so forth ([Hoffman and Novak 1996](#hof96); [Kari and Savolainen 2003](#kar03); [Song _et al._ 2004](#son04)).  

The notion of concentration plays an important role in modern informetrics as it is implied in the historical law of Lokta, which is the basis for authour productivity studies ([Egghe 2005](#egg05)). Being a concept that can be objectively measured so as to render an overall picture of the distributional pattern of the subject matter of concern, concentration has long been studied by economists (e.g., [Stigler 1964](#sti64); [Allen 1981](#all81); [Koeller 1995](#koe95)) and informetricians (e.g., [Drott 1978](#dro78); [Egghe and Rousseau 1991](#egg91); [Yoshikane _et al._ 2003](#yos03)). However, researchers have not yet recognized the potential that concentration studies may enrich our understanding of information behaviour.

There are various methods to measure concentration. Among these, the Gini coefficient is a very popular one utilized in both economics (e.g., [Stigler 1964](#sti64); [Allen 1981](#all81); [Koeller 1995](#koe95)) and informetrics (e.g., [Drott 1978](#dro78); [Egghe and Rousseau 1991](#egg91); [Yoshikane _et al._ 2003](#yos03)). Technically, it has been suggsted that, owing to certain properties, the Gini coefficient is 'a good concentration measure' ([Egghe 2005: 939](#egg05)). Yoshikane _et al._ ([2003](#yos03)) further point out that unlike some other concentration measures that are sensitive to extreme cases, the Gini coefficientis more robust.

<figure>

![geometric illustration of the gini coefficient](../p324fig1.gif)

<figcaption>

**Figure 1: The Geometric Illustration of the Gini Coefficient**</figcaption>

</figure>

The Gini coefficient is therefore adopted in this paper for the measurement of the degree of online information behavioural concentration. Geometrically, as Figure 1 illustrates, the Gini coefficient is defined as two times the area between a Lorenz curve and the 45-degree straight line (i.e. 2*A in Figure 1; [Egghe 2005](#egg05)). For user _j_ who downloaded pages from multiple sites, we calculate his/her Gini coefficient of information consumption by([Yoshikane _et al._ 2003](#yos03)):

<figure>

![Yoshikane equation](../p324fig12.gif)</figure>

where _V<sub>j</sub>_ represents the number of sites Web user _j_ visited, _f<sub>l</sub>_ and _f<sub>m</sub>_ represent units (number of page-views or duration in seconds) of information consumed in sites _l_ and _m_, respectively, and _P<sub>j</sub>_ represents user _j_'s average number of page-views per site across the _V<sub>j</sub>_sites. The Gini coefficient thus has a range between 0 and 1.

We use the simplified example in Figure 2 to illustrate what we mean by the concentration of online information behaviour (in terms of page-views). Cases in the figure represent the distribution of all page downloads that a Web user has made from all the sites s/he visited during a (short) period of time. For case (A), the user visited ten Websites and made ten page-views from each site within the period of time. Across the ten sites, this user's behaviour is evenly distributed and is not concentrated at all. The Gini coefficient is thus zero. For case (B), the user also visited ten Websites and made a total of 100 page downloads, but this time s/he made twelve page-views each from six sites and seven page-views from each of the other four sites. Thus, this user's online information behaviour is slightly more concentrated (on the six sites from which more page-views were made) than in case (A), as the Gini coefficient is 0.12\. In case (C), the user visited just five sites and made a total of fifty page-views. Thirty-six page-views were made from three sites, and the other fourteen pages were downloaded from the remaining two sites. Again, the Gini coefficient is 0.12\. In case (D), the user visited ten Websites and conducted a total of 100 page downloads. Among the 100 page-views, however, ninety-one pages were downloaded from a single site, whereas the other nine pages were downloaded from the remaining nine sites -- a much more concentrated case than the former three with a Gini coefficient of 0.81\. Because in the calculation of the Gini coefficient we only count sites that Web users visit, a Web user will have a less-than-one maximum Gini coefficient if s/he visits multiple sites. In case (D) when the Web user visited ten sites and made a total of 100 page-views, the maximum Gini coefficient is 0.81\.

An interesting observation is that case (B) is a scaled-up version of case (C) with the number of sites visited and page-views downloaded simultaneously having doubled without changing the distribution of page-views per site. The two cases have the same Gini coefficient: their degrees of behavioural concentration are identical. The equation above can generalize that the Gini coefficient will remain the same if (1) the number of sites visited and page-views downloaded change by the same scale without changing the distribution of page-views per site or (2) page-views per site across all the sites visited change by the same scale without changing the number of sites visited. This characteristic of the Gini coefficient is important in our following empirical analysis.

<figure>

![illustrations of gini coefficient](../p324fig2.gif)

<figcaption>

**Figure 2: Illustrative cases of behavioural concentration**</figcaption>

</figure>

Cases (F) and (G) are real-world examples drawn from the empirical study reported below. The panelist in case (F) visited 332 sites during the period of observation. The degree of page-view concentration across these 332 sites measured by the Gini is 0.78\. The panelist in case (G) visited much fewer (126) sites in the same period and concentrated most of the page-views made across the 126 sites on just a handful of sites. To be more specific, this Web user made 4,086 page-views across 126 sites in a month. Almost half (1,978) among the 4,086 page-views were made on two major portal sites. The Gini coefficient for this highly concentrated case is 0.92.

### Data

A month-long, client-side, user-centred panel dataset was obtained from [InsightXplorer Limited](http://www.insightxplorer.com/), Taiwan for our empirical study. InsightXplorer has a syndicated service (like those provided by NetRatings and MediaMetrix) for the Taiwanese Internet market. It maintains an online panel for the local cybermarket and tracks panelists' every online clickstream across Websites that they visit by installing a proprietary client-side software on panelists' computers. The software also solves tracking problems usually encountered upon analysing visiting activities using site-centric data such as caching and proxy server downloads. Panel participants allowed InsightXplorer to track their every clickstream online and in return were compensated by regular sweepstakes programmes. They can uninstall the client-side software and thereby stop being tracked by InsightXplorer at their own will.

Data were collected from 2,022 Internet users who have made at least one visit to any Website during the month of July 2003 and made available for this study. For each panelist, the number and time of sites visited and pages downloaded in July 2003 are recorded by the client-side software and transfered online back to InsightXplorer in the form of raw logs. Comparing demographics of the sample with the estimates provided by TWNIC (a non-profit organisation that takes charge of domain name registration and IP allocation in Taiwan), the sample's average age (31 years old) is close to the average Taiwanese Web users' age (30 years old) estimated by TWNIC, whereas the sample's male proportion (72%) is slightly higher than the TWNIC estimate (61%).

Aggregating from the raw logs, each panelist's number of sites visited, number of page-views made per site, and time spent per page-view were summarized. We utilize these data to calculate the Gini coefficients in page-view distribution and duration distribution for each panelist.

The 2,022 panelists under study made 4,138,423 page-views which lasted for a total of 151,100,519 seconds from 29,042 different Websites. Figure 3 presents the distribution of the number of sites the panelists visited during the observation month. On average, a panelist visited ninety-four different Websites in July 2003.

<figure>

![Distribution of the number of sites visited](../p324fig3.gif)

<figcaption>

**Figure 3: Distribution of the number of sites visited among the 2,022 panelists**</figcaption>

</figure>

Figure 4 and Figure 5 present the distributions of number of the page-views made and average duration per page-view across the 2,022 panelists in the month-long observation period, respectively.

<figure>

![Distribution of the number of page-views](../p324fig4.gif)

<figcaption>

**Figure 4: Distribution of the number of page-views**</figcaption>

</figure>

<figure>

![Distribution of duration per page](../p324fig5.gif)

<figcaption>

**Figure 5: Distribution of duration per page**</figcaption>

</figure>

Figure 6 and Figure 7 present the distributions of behavioural concentration of the panelists in terms of page-views made across sites visited and time duration at the sites visited, respectively. The average Gini coefficient in terms of page-view is 0.75, whereas that in terms of time duration is 0.78.

<figure>

![Distribution of behavioural concentration in page-view](../p324fig6.gif)

<figcaption>

**Figure 6: Distribution of behavioural concentration in page-view**</figcaption>

</figure>

<figure>

![Distribution of behavioural concentration in visit](../p324fig7.gif)

<figcaption>

**Figure 7: Distribution of behavioural concentration in visit duration**</figcaption>

</figure>

Given these descriptive statistics and distributional information, it is apparent that these dimensions of online information behaviour on the Web are highly heterogeneous. Furthermore, the relatively high degrees of behavioural concentration found here (Figures 6 and 7) indicate that most Web users in the panel have a core set of sites in which they may dig deeply to extract information, from where they may orient their online information gathering, on which they may anchor their navigation, and to where they may return frequently. Outside of this core set, there is a much larger set of sites for which the Web user just makes occasional limited visits.

## Analysis

The descriptive summaries reported above show high heterogeneity of online information behaviour across the panelists. In the following, we will try to establish the quantitative relationships between behavioural concentration, the focus of this study, and the dimensions of online information behaviour. For this purpose there are two phases of analysis. In the first phase, to get the flavour of the relationship between behavioural concentration and total number of online actions, we explore how behavioural concentration in terms of page-views made across sites is associated with total number of page-views with the aim of specifying a functional form best suitable for describing such an association. In the second phase, we run multiple regressions to further establish the relationships between behavioural concentration (in both terms of page-views and visit duration) and online information activities (number of sites visited, page-views per site, and time spent per page-view).

### Behavioural concentration and total page-views

We first plot each panelist's number of page-views made in the month against his/her Gini coefficient of page-views distributed across sites s/he visited (Figure 8). Judged by Figure 8 in which each point represents the association of a panelist's total page-view number and Gini coefficient, there is a non-linear relationship between the two variables. It should be noted that the upper and lower boundaries to the data in Figure 8 is due to the data and not the issue of the maximum Gini coefficient for a given number of page-views. For example, with 2000 page-views, the maximum Gini coefficient is 0.999 (when a visitor visited two sites and made 1,999 and 1 page downloads from them, respectively), which is higher than the upper boundary in Figure 2 for around 2000 page-views. The minimum Gini coefficient is 0 (when a visitor visited just one site), which is lower than the lower boundary in Figure 2 for around 2000 page-views.

<figure>

![Plotting the number of total page-views against Gini ](../p324fig8.gif)

<figcaption>

**Figure 8: Plotting the number of total page-views against Gini**</figcaption>

</figure>

Judged by Figure 8 in which each point represents the association of a panelist's total page-view number and Gini, there is a non-linear relationship between the two variables. A series of transforming attempts indicate that taking the double logarithm of the number of page-views best explains the Gini heterogeneity in a linear way. Figure 9 shows the relationship after the transformation. A simple regression of users' Gini coefficients on their _log_(_log_(number of page-views)) renders a coefficient of determination (_R<sup>2</sup>_) at 0.82\. Obviously, we have a very strong (statistically speaking) case here, whereby the more page-views a Web user makes, the higher the proportion of page-views concentrated on a relatively small set of _anchoring_ or core Websites.

<figure>

![Plot of log(log(no of page-views)](../p324fig9.gif)

<figcaption>

**Figure 9: Plotting _log_(_log_(number of total page-views)) against Gini**</figcaption>

</figure>

### Behavioural concentration and dimensions of online information behaviour

We next investigate how the dimensions (i.e., number of sites visited, number of page-views per site, and time spent per page-view) of online information behaviour influence behavioural concentration by multiple regressions. As we have mentioned, we focus on both the concentration of page-views and that of duration across Websites visited. Therefore, the backbone models of the analysis can be described by the following two regressions.

Page Model:  
Gini<sub>_page_i</sub> =  a<sub>0</sub> + a<sub>1</sub> * #Site<sub>i</sub> + a<sub>2</sub> * #page-views per site<sub>i</sub> + a<sub>3</sub> * Time spent per page<sub>i</sub>   

Duration Model:  
Gini<sub>_duration_i</sub> =  b<sub>0</sub> + b<sub>1</sub> * #Site<sub>i</sub> + b<sub>2</sub> * #page-views per site<sub>i</sub> + b<sub>3</sub> * Time spent per page<sub>i </sub> 

Gini<sub>_page_i</sub> refers to the Gini coefficient of page-view distribution across sites that Web user _i_ visited, and Gini<sub>_duration_i</sub> refers to user _i_'s Gini coefficient of duration distribution. With the data, in our empirical setting _i_ denotes a series of integers ranging from 1 to 2,022.

We again look for the appropriate transformation for the independent variables in the regressions by trying various transformations. Figures in Table 1 are R squared values associated with various transformed independent variables. As Table 1 shows, the double logarithm transformation once more brings forth the best fit. The signs and significance (at the 0.05 level) of the model coefficients, however, do not change with these transformations. In the Results section below, we report results from the models with independent variables taking a double logarithm.

<table><caption>

**Table 1: Model fit with various specifications of the independent variables**</caption>

<tbody>

<tr>

<td> </td>

<th>Page Model</th>

<th>Duration Model</th>

</tr>

<tr>

<td>(1) _R<sup>2</sup>_: dependent variable without transformation</td>

<td>0.40</td>

<td>0.38</td>

</tr>

<tr>

<td>(2) _R<sup>2</sup>_: dependent variable taking a logarithm</td>

<td>0.80</td>

<td>0.73</td>

</tr>

<tr>

<td>(3) _R<sup>2</sup>_: dependent variable taking a double logarithm</td>

<td>0.86</td>

<td>0.76</td>

</tr>

<tr>

<td>(4) _R<sup>2</sup>_: dependent variable taking a triple logarithm</td>

<td>0.75</td>

<td>0.63</td>

</tr>

</tbody>

</table>

## Results

### From the model

Running multiple regressions for both the Page and the Duration models as specified in the Analysis section with the independent variables taking a double logarithm, Table 2 summarizes the results. It is found that dimensions of online information behaviour, including number of sites visited, number of page-views per site visited, and time spent per page-view all significantly (at the 0.05 level) contribute to the explanation of the degree of behavioural concentration (in terms of both page-view concentration and duration concentration). Among them, number of sites visited and number of page-views per site positively explain page-view concentration and duration concentration. Time spent per site also positively explains duration concentration, but is negatively associated with page-view concentration across sites.

<table><caption>

**Table 2.: Model results from the best-fit specification [ ** p<0.01.]**</caption>

<tbody>

<tr>

<td> </td>

<th colspan="2">Page Model</th>

<th colspan="2">Duration Model</th>

</tr>

<tr>

<th>Independent variables</th>

<th>Coefficient</th>

<th>t-value</th>

<th>Coefficient</th>

<th>t-value</th>

</tr>

<tr>

<td>Intercept</td>

<td>0.2943**</td>

<td>17.08</td>

<td>0.2706**</td>

<td>13.36</td>

</tr>

<tr>

<td>log(log(no. of sites))</td>

<td>0.2146**</td>

<td>64.75</td>

<td>0.2256**</td>

<td>57.88</td>

</tr>

<tr>

<td>log(log(no. of pages/site))</td>

<td>0.2667**</td>

<td>58.05</td>

<td>0.1539**</td>

<td>28.49</td>

</tr>

<tr>

<td>log(log(duration/page))</td>

<td>-0.0569**</td>

<td>-4.60</td>

<td>0.0506**</td>

<td>3.48</td>

</tr>

<tr>

<td>

_R<sup>2</sup>_</td>

<td colspan="2">0.8639</td>

<td colspan="2">0.7632</td>

</tr>

<tr>

<td>

Adjusted _R<sup>2</sup>_</td>

<td colspan="2">0.8637</td>

<td colspan="2">0.7629</td>

</tr>

</tbody>

</table>

### Further analysis

To further investigate the role that a Web user's various behavioural dimensions play in influencing his/her behavioural concentration, we use the same model specification reported above and run a series of stepwise regressions. The focus now is on the partial _R<sup>2</sup>_. We now want to see how the relative contributions of independent variables predict the dependent variable change as the data structure changes. At the same time, since there is a maximum value for a dependant variable in a regression for some values of the independent variables, we also would like to check the validity of the regression results reported above so as to further validate the analytical framework.

We first run stepwise regressions on the whole dataset (the result is identical to what is reported by Table 2). Second, we run stepwise regressions on the dataset excluding panelists who visited fewer than five sites during the observation month. Third, we run stepwise regressions on the dataset excluding panelists who visited fewer than ten sites during the observation month. The process goes on with a consecutive regression, dropping panelists who visited less than 15 20, 25..., 100 sites. Therefore twenty-one stepwise regressions in total are run for each model. For each stepwise regression, the total _R<sup>2</sup>_ is divided into partial _R<sup>2</sup>_ of the independent variables. The partial _R<sup>2</sup>_ that each independent variable carries implies the relative contribution of that variable in the explanation of the dependent variable.

For both Page Model and Duration Model, the analysis shows that none of the coefficient signs or statistical significance (at the 0.05 level) is changed by different sample sizes. It can also be generalized from the analysis that the speed dimension of online information behaviour has a relatively minor influence on the degree of behavioural concentration (partial _R<sup>2</sup>_ below 0.01). An interesting finding is that for the whole dataset, number of sites visited is more important than number of page-views per site in terms of partial _R<sup>2</sup>_. However, if we drop more panelists who just visited a small number of sites, the importance of the latter grows and that of former decreases. For both models, number of page-views per site explains more variances in the models than number of sites visited when we drop out of the dataset those _light_ Web users who visited fewer than ten sites during July 2003.

<figure>

![Relative importance of number of sites visited - Page Model](../p324fig10.gif)

<figcaption>

**Figure 10: Relative importance of number of sites visited and page-views made per site in the Page Model**</figcaption>

</figure>

<figure>

![Relative importance of number of sites visited - Duration Model](../p324fig11.gif)

<figcaption>

**Figure 11: Relative importance of number of sites visited and page-views made per site in the Duration Model**</figcaption>

</figure>

Focusing on the change of partial _R<sup>2</sup>_, Figure 10 and Figure 11 plot the results from the analysis.

### Findings

Summarizing the results discussed above, we arrive at a series of empirical findings that reveal patterns in the heterogeneity of Web users' behavioural concentration relating to their number of sites visited, number of page-views per site, and time spent per page-view.

1.  _The larger the number of sites that a Web user visits, the higher the degree will be of his/her behavioural concentration._ Relative to Web users who visited fewer Websites, those who visited more Websites are more likely to make most of their page-views from and spend most of their online time at just a small proportion of the sites they visited.
2.  _The larger the number of page-views that a Web user makes per site visited, the higher the degree will be of his/her behavioural concentration._ Relative to Web users who just downloaded a few pages from the sites they visited, those who conduct more page-views per site visited are more likely to make most of their page-views from and spend most of their online time at just a small proportion of the sites they visited.
3.  _The faster the speed of a Web user's online information behaviour, the higher the degree of his/her behavioural concentration will be in terms of page-views._ Relative to Web users who browse slowly online, those who browse at a faster pace (who have a shorter duration per page-view) are more likely to make most of their page-views at just a small proportion of the sites they visited.
4.  _The faster the speed of a Web user's online information behaviour is, the lower the degree of his/her behavioural concentration will be in terms of duration._ Relative to Web users who browse slowly online, those who browse at a faster pace (who have a shorter duration per page-view) are less likely to spend most of their online time at just a small proportion of the sites they visited.
5.  _Except for those who visit very few sites (i.e., those who visit fewer than ten Websites during a month), number of page-views per site is the most important variable in the explanation of Web users' degree of behavioural concentration among Websites visited._

## Conclusion

This paper analyses how Web users differ in their online information behavioural patterns from the angle of their behavioural concentration. From a Web user's behavioural concentration, we look at the degree of concentration of page-views made and visit duration among the Websites s/he visited, and we measure this by the Gini coefficient. We also investigate how number of sites visited, number of page-views per site, and time spent per page-view explain people's heterogeneity in their concentration of online information behaviour. Empirically, through online panel data that record panelists' every move on the Web for a whole month, it is found that these three online information behavioural dimensions explain a substantial part of behavioural concentration in terms of both page-views and duration.

From the empirical findings, an obvious behavioural pattern emerges that distinguishes _heavy_ users from _light_ users. Web users who visited more sites and made more page-views from these sites, i.e., heavy Web users, are more likely to concentrate most of their online activities on a small set of core, anchoring Websites. On the contrary, _light_ Web users who visited a limited number of sites and downloaded fewer web pages are found to distribute their online activities more equally among the small scope of sites visited. If we follow Yoshikane _et al._ ([2003](#yos03)) to distinguish 'absolute concentration' which in our context directly indicates the number of sites visited within the sea of sites and 'relative concentration' which is exactly what we look at here through the Gini coefficient, then empirically we have established a concrete conclusion that for Web users' online information behaviour, relative behavioural concentration is positively associated with absolute behavioural concentration.

Our findings in this study lead to a series of questions whose clarification will further enhance our understanding of online information behaviour. First, it is of great interest to investigate what the set of core, anchoring Websites is and then analyse the degree of overlap among Web users' core sets of sites. It is obvious that on the aggregate level a substantial portion of online activities is concentrated on a few sites (such as big portals, search engines, etc.). However, very little research has been done so far as to the individual-level heterogeneity of the sites that Web users concentrate their online activities. An individual-level analysis, however, may enrich our understanding of heterogeneous online information behaviour. For example, a few Web users' core sets of sites may consist of Web services such as e-mail. Since it is just for convenience that e-mail can be checked via the Web, Web usage in checking e-mail should be distinguished from other types of Web-specific online information behaviour.

Secondly, as it has been quantitatively established that relatively _heavy_ Web users concentrate their online information behaviour on a small set of sites, what role this supposedly important set of core, anchoring Websites plays in the users' everyday online information lives to fit in with their _way of life_ and _mastery of life_ ([Savolainen 1995](#sav95)) needs further study. Third, it can be hypothesized that those many sites out of a Web user's core, anchoring set of sites that s/he visits only once or infrequently are actually visited for complementary or exploratory purposes. The existence of these sites in Web users' online experiences indicates that the site-and-context-specific studies supporting the _lock-in_ argument, in which experienced Web users resort to a very small set of Websites for the fulfillment of a specific information need ([Bucklin and Sismeiro 2003](#buk03); [Zauberman 2003](#zau03); [Johnson _et al._ 2003](#joh03)), are at best partially substantiated and need further elaboration. Fourth, as it has been established ([Schmittlein, _et al._ 1993](#sch93)), under certain circumstances users' degree of behavioural concentration may change over time. If a longitudinal study is feasible, then it will be of much more value to track how the number of sites visited, the number of page-views per site, the duration per page as well as how the concentration of Web users' online information behaviour evolves over time.

Focusing on Web users' behavioural concentration across Websites they have visited, this paper investigates heterogeneity in Web users' online information behaviour. The analytical approach proposed herein renders us a picture of online information behaviour that is important in understanding Web users' overall Internet usages and their heterogeneity, which have been missing in the related literature so far.

## Acknowledgement

Research funding from the National Science Council, Taiwan (No. 94-2416-H-007-006) is acknowledged by the first author.

## References

*   <a id="agi06"></a>Agichtein, E., Brill, E., Dumais, S. & Ragno, R. (2006). Learning user interaction models for predicting web search result preferences. _Proceedings of the 29th Annual International ACM SIGIR Conference on Research and Development in Information Retrieval SIGIR '06_ , 3-10.
*   <a id="all81"></a>Allen, B.T. (1981). Structure and stability in gasoline markets. _Journal of Economic Issues_, **15**(1), 73-94
*   <a id="and05"></a>Andrews, J.E., Pearce K.A., Ireson, C. & Love, M.M. (2005). Information- seeking behaviours of practitioners in a primary care practice-based research network (PBRN), _Journal of Medical Library Association,_ **93**(2), 206-212
*   <a id="ani03"></a>Anick, P. (2003). Human interaction. Using terminological feedback for web search refinement: a log-based study, _Proceedings of the 26th Annual International ACM SIGIR Conference on Research and Development in Informaion Retrieval,_ 88-95.
*   <a id="bil02"></a>Bilal, D. (2002). Children's use of the Yahooligans! Web search engine: III. Cognitive and physical behaviours fully generated search tasks, _Journal of the American Society for Information Science_, **53**(13), 1170-1183
*   <a id="bru04"></a>Bruce, H., Jones, W. & Dumais, S. (2004). [Information behaviour that keeps found things found](http://informationr.net/ir/10-1/paper207.html). Information Research, **10**(1), paper 207\. Retrieved 17 August, 2007 from http://informationr.net/ir/10-1/paper207.html
*   <a id="buk03"></a>Bucklin, R.E. & Sismeiro, C. (2003). A model of web site browsing behaviour estimated on clickstream data. _Journal of Marketing Research_, **40**(3), 249-267
*   <a id="cat95"></a>Catledge, L.D. & Pitkow, J.E. (1995). Characterizing browsing strategies in the World Wide Web. _Computer Networks and ISDN Systems_, **26**(6), 1065-1073
*   <a id="cho99"></a>Choo, C.W., Detlor, B. & Turnbull, D. (1999). [Information seeking on the web—an integrated model of browsing and searching](http://choo.fis.utoronto.ca/fis/respub/asis99/) . _Proceedings of the 62nd ASIS Annual Meeting_, **36**, 3-16\. Retrieved 18 August, 2007 from http://choo.fis.utoronto.ca/fis/respub/asis99/
*   <a id="col05"></a>Cole, C., Leide, J.E., Large, A., Beheshti, J. & Brooks, M. (2005). Putting it together online: information need identification for the domain novice user. _Journal of the American Society for Information Science and Technology_, **56**(7), 684-694\.
*   <a id="dam04"></a>D'Ambra, J. & Wilson, C.S. (2004). Integrating the construct of uncertainty in information seeking and the task-technology fit (TTF) model. _Journal of the American Society for Information Science and Technology_, **55**(8), 731-742
*   <a id="dro78"></a>Drott, M.C. & Grif.th, B.C. (1978). An empirical examination of Bradford's law and the scattering of scientific literature. _Journal of the American Society for Information Science_, **29**(5), 238-246
*   <a id="egg05"></a>Egghe, L. (2005). Zipfian and Lotkaian continuous concentration theory. _Journal of the American Society for Information Science_, **56**(9), 935-945
*   <a id="egg91"></a>Egghe, L. & Rousseau, R. (1991). Transfer principles and a classification of concentration measures, _Journal of the American Society for Information Science_, **42**(7), 479-489
*   <a id="ell89"></a>Ellis, D. (1989). A behavioural approach to information retrieval system design. _Journal of Documentation_, **45**(3), 171-212
*   <a id="hek03"></a>Hektor, A. (2003). Information activities on the Internet in everyday life. _New Review of Information Behaviour Research_, **4**(1), 127-138
*   <a id="hof96"></a>Hoffman, D. & Novak, T.P. (1996). Marketing in hypermedia computer-mediated environments: conceptual foundations. _Journal of Marketing_, **60**(3), 50-68
*   <a id="hop04"></a>Hope, B. & Li, Z. (2004). [Online newspapers: the impact of culture, sex, and age on the perceived importance of specified quality factors](http://informationr.net/ir/9-4/paper197.html). Information Research, **9**(4), paper 197\. Retrieved 18 August, 2007 from http://informationr.net/ir/9-4/paper197.html
*   <a id="hsi98"></a>Hsieh-Yee, I. (1998). Search tactics of Web users in searching for texts, graphics, known items and subjects: a search simulation study. _Reference Librarian_, **28**(60), 61-85.
*   <a id="hub89"></a>Huberman, B.A., Pirolli, P.L.T., Piktow, J.E. & Lukose, R.M. (1998). Strong regularities in world wide web surfing, _Science_, **280**(3 April), 95-97
*   <a id="jai02"></a>Jaillet, H.F. (2002). Web metrics: measuring patterns in online shopping. _Journal of Consumer Behaviour_, **2**(4), 369-381
*   <a id="jan00"></a>Jansen, B.J., Spink, A. & Saracevic, T. (2000). Real life, real users, and real needs: A study and analysis of user queries on the web. _Information Processing & Management_, **36**(2), 207-227
*   <a id="jan05"></a>Jansen, B.J., Spink, A. & Pedersen, J. (2005). A temporal comparison of AltaVista Web searching. _Journal of the American Society for Information Science_, **56**(6), 559-570
*   <a id="jan06"></a>Jansen, B.J. & Spink, A. (2006). How are we searching the world wide web? A comparison of nine search engine transaction logs. _Information Processing & Management_, **42**(1), 248-263
*   <a id="joh03"></a>Johnson, E.J., Bellman, S. & Loshe, G.L. (2003). Cognitive lock-in and the power law of practice. _Journal of Marketing_, **67**(2), 62-75
*   <a id="kar03"></a>Kari, J. & Savolainen, R. (2003). Towards a contextual model of information seeking on the Web. _New Review of Information Behaviour Research_, **4**(1), 155-175
*   <a id="kar04"></a>Kari, J. (2004). [Web information seeking by pages: an observational study of moving and stopping](http://informationr.net/ir/9-4/paper183.html). Information Research, **9**(4), paper 183\. Retrieved 18 August, 2007 from http://informationr.net/ir/9-4/paper183.html
*   <a id="kim01"></a>Kim, K. (2001). Information seeking on the Web: effects of user and task variables. _Library & Information Science Research_, **23**(3), 233-255
*   <a id="koe95"></a>Koeller, C.T. (1995). Innovation, market structure and firm size: a simultaneous equations model. _Managerial and Decision Economics_, **16**(3), 259-269
*   <a id="kou02"></a>Koufaris, M. (2002). Applying the technology acceptance model and flow theory to online consumer behavior. _Information Systems Research_, **13**(2), 205-223
*   <a id="lin05"></a>Lin, S. (2005). Internet working of factors affecting successive searches over multiple episodes, _Journal of the American Society for Information Science_, **56**(4), 416-436
*   <a id="mar95"></a>Marchionini, G.M. (1995). _Information seeking in electronic environments_. Cambridge: Cambridge University Press.
*   <a id="mat05"></a>Mat-Hassan, M. & Levene, M. (2005). Associating search and navigation behaviour through log analysis. _Journal of the American Society for Information Science_, **56**(9), 913-934
*   <a id="mck03"></a>McKenzie, P.J. (2003). A model of information practices in accounts of everyday-life information seeking, _Journal of Documentation_, **59**(1), 19-40
*   <a id="nic04"></a>Nicholas, D., Huntington, P., Williams, P. & Dobrowolski, T. (2004). Re-appraising information seeking behaviour in a digital environment: bouncers, checkers, returnees and the like, _Journal of Documentation_, **60**(1) 24-43
*   <a id="par"></a>Park, S., Bae, H. & Lee, J. (2005). End user searching: A web log analysis of naver, a korean web search engine. _Library & Information Science Research_, **27**(2) 203-221
*   <a id="pha04"></a>Pharo, N. (2004). [A new model of information behaviour based on the search situation transition schema](http://informationr.net/ir/10-1/paper203.html), Information Research, **10**(1). Retrieved 18 August, 2007 from http://informationr.net/ir/10-1/paper203.html
*   <a id="rie00"></a>Rieh, S.Y. & Belkin, N.J. (2000). Interaction on the Web: scholars' judgment of information quality and cognitive authority. _Proceedings of the 63rd ASIS Annual Meeting_, **37**, 25-38\.
*   <a id="rie04"></a>Rieh, S.Y. (2004). On the Web at home: information seeking and Web searching in the home environment. _Journal of the American Society for Information Science and Technology_, **55**(8), 743-753\.
*   <a id="sav95"></a>Savolainen, R. (1995). Introduction to the special issue. Everyday life information seeking: approaching information seeking in the context of way of life. _Library and Information Science Research_, **17**(3), 259-294\.
*   <a id="sav04"></a>Savolainen, R. (2004). [Enthusiastic, realistic and critical. Discourses of Internet use in the context of everyday life information seeking.](http://informationr.net/ir/10-1/paper198.html) Information Research, **10**(1), paper 198\. Retrieved 18 August, 2007 from http://informationr.net/ir/10-1/paper198.html
*   <a id="sav_kor_04"></a>Savolainen, R. & Kari, J. (2004). Placing the Internet in information source horizons: a study of information seeking by Internet users in the context of self-development, _Library & Information Science Research_, **26**(4), 415-433
*   <a id="sch93"></a>Schmittlein, D.C., Cooper, L.G. & Morrison, D.G. (1993). Truth in concentration in the land of (80/20) laws, _Marketing Science_, **12**(2), 167-183
*   <a id="she02"></a>Sheehan, K.B. (2002). Of surfing, searching, and newshounds: a typology of Internet users' online sessions, _Journal of Advertising Research_, **42**(5), 62-71
*   <a id="aasil1"></a>Silverstein, C., Henzinger, M., Marais, H. & Moricz, M. (1999). Analysis of a very large web search engine query log. _SIGIR Forum_, **33**(1), 6-12
*   <a id="slo03"></a>Slone, D.J. (2003). Internet search approaches: the influence of age, search goals, and experience, _Library & Information Science Research_, **25**(4), 403-418
*   <a id="son04"></a>Song, I., Larose, R., Eastin, M.S. & Lin, C.A. (2004). Internet gratifications and Internet addiction: on the uses and abuses of new media. _Cyberpsychology & behaviour_, **7**(4), 384-394
*   <a id="son99"></a>Sonnenwald, D. (1999). Evolving perspectives of human information behaviour: contexts, situations, social networks and information horizons, in T. D. Wilson & D. Allen (Eds.). _Exploring the contexts of information behaviour_. Proceedings of the 2nd International Conference on Research in Information Needs, Seeking and Use in Different Contexts 13-15 August 1998, Sheffield, UK. (pp. 176-190). London, England: Taylor Graham.
*   <a id="spi04"></a>Spink, A. & Cole, C. (2004), Introduction. _Journal of the American Society for Information Science and Technology_, **55**(9), 767-768.
*   <a id="spi04b"></a>Spink, A. & Jansen, B. (2004). _Web search: public searching of the Web_. Netherlands: Kluwer Academic Publishers.
*   <a id="spi04c"></a>Spink, A., Koricich, A., Jansen, B.J. & Cole, C. (2004). Sexual Information Seeking on Web Search Engines, _Cyberpsychology & behaviour_, **7**(1), 65-72
*   <a id="sti64"></a>Stigler, G. (1964). A theory of oligopoly, _Journal of Political Economy_, **72**(1), 44-61
*   <a id="tab05"></a>Tabatabai, D. & Shore, B. M. (2005). How experts and novices search the Web. _Library & Information Science Research_, **27**(2), 222-248
*   <a id="tau97"></a>Tauscher, L. & Greenberg, S. (1997). How people revisit Web pages: empirical findings and implications for the design of history systems. _International Journal of Human-Computer Studies_, **47**(1), 97-137
*   <a id="toms03"></a>Toms, E.G., Freund, L., Kopak, R. & Bartlett, J.C. (2003). The effect of task domain on search. In _Proceedings of the 2003 conference of the Centre for Advanced Studies on Collaborative Research, Toronto, Ontario, Canada._ (pp. 303-312). Lewisville, TX: MC Press publishing as IBM Press
*   <a id="tom05"></a>Tombros, A., Ruthven, I. & Jose, J.M. (2005). How users assess Web pages for information seeking, _Journal of the American Society for Information Science and Technology_, **56**(4), 327-344
*   <a id="wik01"></a>Wikgren, M. (2001). Health discussions on the Internet: A study of knowledge communication through citations, _Library and Information Science Research_, **23**(4), 305-317
*   <a id="wi197"></a>Wilson, T.D. (1997). Information behaviour: an inter-disciplinary perspective, in Vakkari, P., R. Savolainen, & B. Dervin (Eds). _Information Seeking in Context: Proceedings of An International Conference on Research in Information Needs_, Seeking and Use in Different Contexts 14-16 August, Tampere, Finland. (pp. 39-50), London: Taylor Graham
*   <a id="wil99"></a>Wilson, T.D., Ellis, D., Ford, N. & Foster, A. (1999). [Uncertainty in information seeking](http://informationr.net/tdw/publ/unis/). _Library and Information Commission Research Report_ 59, Sheffield: University of Sheffield. Retrieved 18 August, 2007 from http://informationr.net/tdw/publ/unis/
*   <a id="yos03"></a>Yoshikane, F., Kageura, K. & Tsuji, K. (2003). A model for comparative analysis of concentration of author productivity, giving consideration to the effect of sample size dependency of statistical measures. _Journal of the American Society for Information Science and Technology_, **54**(6), 521-528
*   <a id="zha04"></a>Zhang, D., Zambrowicz, C., Zhou, H., and Roderer, N.K. (2004). User information seeking behavior in a medical web portal environment: a preliminary study. _Journal of the American Society for Information Science and Technology_, **55**(8), 670-684
*   <a id="zau03"></a>Zauberman, G. (2003). The intertemporal dynamics of consumer lock-in. _Journal of Consumer Research_, **30**(3), 405-419.