<header>

#### vol. 23 no. 1, March, 2018

</header>

<article>

# Public library Websites as electronic branches: a multi-country quantitative evaluation

## [Diane L. Velasquez](#author) and [Nina Evans](#author).

> **Introduction**. This paper describes the findings of a study of 1517 public library Websites in Australia, Canada, and the United States over a period of four years. These Websites are referred to as ‘electronic branches' of the libraries, thereby extending the definition of physical library branches into the digital realm. The purpose of the research was to investigate the accessibility of public library Websites, the available online resources and whether library staff are available to respond to users' questions and concerns regarding the Website.  
> **Method**. A quantitative study was conducted, using a spreadsheet protocol to determine if 18 criteria were present on the Websites. General comments about the Websites were also recorded and included in the analysis.  
> **Analysis**. The quantitative data analysis for the 1517 Websites was done using Excel spreadsheets through formula manipulations. Descriptive statistics are used to report the findings.  
> **Results**. The data revealed that the Websites of Canadian and USA libraries include more of the criteria than the Australian libraries. Overall many similarities were found between the accessibility of the Websites of the different countries.  
> **Conclusion**. The study provides exemplars of an electronic branch of a public library and the services it can offer to its community members. These examples can be used to model ideal electronic branch libraries for library staff to improve their Websites.

<section>

## Introduction

Public library Websites are often the first, if not the main, point of contact that people have with their local library. In the future, public library patrons may not go into the bricks and mortar building but just access the Website for the services, materials, and programmes that are needed. Members access a public library Website for a variety of reasons, including locating resources, reserving resources, managing library accounts, bill payments, downloading e-books and audio books, and accessing online databases available free through their library. A library Website can be regarded as an _electronic branch_ of the library. An electronic branch is the Website of the library versus the bricks and mortar version of the library where patrons physically enter the library.

A physical library branch will not be used if it cannot be accessed, if patrons do not know where or how to ask for help, if the information and resources are out of date, and if the building and furniture are dilapidated and unappealing. The same can be said of the electronic branch. An inviting, easily accessible and vibrant Website is an essential part of a successful electronic branch. Library electronic branches also need to be developed, managed and maintained just like any other library branch. In short, the electronic branch needs to be easy to access and use.

Public library Websites provide information that includes catalogues, databases, events, marketing, staff information, programming, and services. In this study, 1,517 public library Websites were assessed and evaluated in a study over four years. The intent of the project was to find out what _basic_ and _desirable_ information was available on the Websites and to make recommendations about how Websites can be improved. The study extends previous research carried out in Pennsylvania ([Powers, 2011](#pow11)), from which a checklist of criteria for evaluating Websites was developed. Powers conducted the research in 2010 and used a random sample of 305 points of contact (URLs) of libraries in Pennsylvania. She used ‘_yes_' and ‘_no_' responses to code the answers. According to Powers, a _basic_ library Website contains ten of the criteria, while a _desirable_ Website would include eight additional criteria. For this study, these were the criteria used and it was based upon two papers: an article by Persichini, Samuelson, and Zeiter ([2008](#per08)) which set out the ten basic features and another by Mathews ([2009](#mat09)), which set out eight desirable criteria. The criteria are described in the Research methods section. This particular checklist was used as it was something that could easily be explained and used for student researchers. While the checklist had aspects that were unique to the USA, these could be changed and adapted for whichever location one chose to use.

The study presented here differs from the Powers study in three ways: first, a number of researchers evaluated and assessed the 1,517 Websites, instead of a single researcher; secondly, our study was conducted in three countries, whereas the Powers study was done in one USA state only; finally, our study was over a four-year time-frame, while Powers completed her study over a six-month period. We used most of Powers's criteria except that we added ‘_joint-use facilities_' and deleted ‘_Library board information_' and ‘_Description of particular library services_'.</section>

<section>

## Literature review

### Evaluation and assessment of Websites

Evaluation of library Websites began as early as 1994, when the Internet and World Wide Web were in their infancy. Empirical studies on Websites in other industries are informative regarding the questions to ask and the frameworks needed to move forward with the study of library Websites ([Cao, Zhang and Seydel, 2005](#cao05); [Chua and Goh, 2010](#chu10); [Teo, Oh, Liu and Wei, 2003](#teo03)). The majority of literature available on library Websites relates to the academic library sphere. Although these findings can inform the research on public libraries, they have a different focus. Academic libraries focus on curriculum and supporting academic staff and students with their scholarly research, while public libraries support lifelong learning and leisure. Literature describes the criteria for evaluating Websites as follows below.

</section>

<section>

### Usability

A Website's usability and the quality of its content are important aspects of its viability. According to Poll ([2007](#pol07), p. 1), ‘_contents, language, structure, design, navigation, and accessibility_' of Websites are key areas of focus for libraries. Determining what usability is can be different for each member of the community. Past research based on users' acceptance of a particular system has shown that usability is based upon the way the site functions or works ([Goodwin, 1986](#goo86); [Wang and Senecal, 2007](#wan07)).

Usability has been defined as ‘_how well and how easily a visitor, without formal training, can interact with an information system of a website_' ([Benbunan-Fich, 2001](#ben01), p. 151). According to Nielsen ([2012](#nie12), para. 3) usability is defined as a ‘_quality attribute that assesses how easy user interfaces are to use_'. Nielsen goes on to argue that usability refers to the methods for improving the ease of use during the design process. He further defines the five quality components of ‘_learnability, efficiency, memorability, errors, and satisfaction_' ([Nielsen, 2012](#nie12), para. 4). Implementing Nielsen's usability heuristics will improve any Website.

</section>

<section>

### User-centred design

In order to add value for community members, public libraries should stimulate user involvement and allow users to give feedback ([Sørum, Andresen and Vatrapu, 2012](#sor12)).

Nielsen proposed simple user-centred design guidelines that he called usability heuristics. Nielsen's suggestions were to ‘_make screens simple and natural, speak the users' language, be consistent, provide feedback, use plain language for error messages, prevent errors, and provide clearly marked exits_' ([Nielsen, 1996](#nie96), p. 34). None of Nielsen's suggestions are difficult to envisage when considered within a library framework. These suggestions can make a library Website move from a hard to navigate site, to an easy one that users will want to come back to visit. If a Website is difficult to navigate, users will leave and not return ([Nielsen, 2012](#nie12)). For a public library, this has to be avoided, as return visitors indicate that the electronic branch of the library is also a valued branch.

One aspect of good user-centred design is finding out what the community wants and expects on the Web page. It is important to remember that the Website exists for the users, not the library staff ([Goodman and Schofield, 2015](#goo15)). Library literature underscored the importance of usability testing and having an iterative process during the design phase ([Becker and Yannotta, 2013](#bec13); [Dominguez, Hammill and Brillat, 2015](#dom15)). If a library is redesigning their Website, many different methods can be used. Becker and Yannotta ([2015](#bec13)) in their project at Hunter College Libraries used an iterative usability testing process to improve their Website. Dominguez _et al._ ([2015](#dom15)) used a think-aloud protocol during usability testing, focus groups, and card sorting during their many phases of Website design from 2001 to 2012\. Becker and Yannotta, as well as Dominguez _et al._, used many of the processes that Nielsen ([1996](#nie96); [2012](#nie12)) suggested to carry out user-centred design, in order to improve the accessibility of the Website.

</section>

<section>

### Accessibility

Riley-Huff ([2012](#ril12)) suggests that every library should seriously consider accessibility for all. When putting the Website together, each piece has to be considered from the standpoint of each potential visitor who will be looking at it. Accessibility can be difficult if there are blind, deaf, or people with other disabilities visiting the library site. A simple colour choice can be something that can make a Website unavailable to a colour blind person. Conway ([2011](#con11)) did a Website audit of public libraries in Western Australia during 2010 to find out if they adhered to the W3C Web Content Accessibility Guidelines and discovered that none of the public libraries adhered to those guidelines. These Guidelines (an international standard) make recommendations for Websites that are ‘_perceivable, operable, understandable and robust_' ([World Wide Web Consortium, 2011](#wor11)).

</section>

<section>

### Other studies focused on public libraries

Metrics are needed to measure the success of Websites for public libraries. Public library Websites can be regarded as the electronic front door of the library. The impression they make may be viewed by some patrons as an indicator of the level of digital services of the library. Bérlanger _et al._ ([2006](#ber06)) suggest that owners' goals for the Websites will differ and therefore the success of the site may need to be specific to the information and audience. Public library Websites have multiple purposes. Community members can use the Website to look for the services and programming that is available at the library, they find information about the physical branch such as the address, telephone number, and operating hours; and gain access to available e-services ([Chow, Bridges and Commander, 2014](#cho14); [McMenemy, 2012](#mcm12)).

A literature review of studies that mainly focus on public libraries identified only a few worldwide. Beside the study by Powers ([2011](#pow11)), which is the basis for the methods used here, there is Conway's ([2011](#con11)) audit, mentioned above, and Maatta Smith ([2014](#maa14)), like Conway, looked at specific information for users to find on Websites of urban libraries in the USA. She concentrated on information related directly to disabilities and found that ‘_this was one of the greatest areas of disparities among libraries_' ([Maatta Smith, 2014](#maa14), p. 199). McMenemy's study researched the emergence of digital libraries in Scotland, which found that there was a ‘_consistent set of electronic resource… offering high quality information…_' ([McMenemy, 2012](#mcm12), p. 525). He also found difficulties with using resources, as there was confusion about terminology across different library services. Chow _et al._ ([2014](#cho14)) studied usability design comparing academic and public libraries, as each user group has different needs and functions. They focused primarily on how the pages were laid out, how the navigation worked, and if the Websites adhered to recommended design guidelines. Overall, Chow _et al._ determined that more than 70% of the libraries they studied had not conducted usability studies.

</section>

<section>

## Research method

This project partially replicates Powers's study in Pennsylvania, where a random sample of public library Websites were evaluated and assessed against the following eighteen criteria in a checklist ([Powers, 2011](#pow11), p. 8-9):

<table>

<tbody>

<tr>

<td>

1. Library name  
2. Library address (physical and mailing)  
3. Library phone number  
4. Online contact (e-mail address or online contact form)  
5. Hours of operation  
6. Library board members' names  
7. Link to online catalogue  
8. Link to state wide collaborative services  
9. Description of library services available to patrons  
10. Current site content indicated by a date when last updated or last reviewed</td>

<td>

11. Mixture of text and images  
12. Library events being promoted on the page  
13. Search box present  
14. Place to provide feedback about the site  
15. Free from spelling or grammatical errors  
16. Place to ask for help  
17. Image or icon or other graphic used to represent the library  
18. Site employs some Web 2.0 technologies like blogs or wikis or reference to a social networking profile.</td>

</tr>

</tbody>

</table>

The research was conducted by postgraduate students who are pursuing a library and information management degree at the University of South Australia, as part of an information management project course. This course was part of the student's capstone project under supervision of the course coordinator and lecturer. A total of forty-six students participated in the project over four years, in groups of four to six students per semester. This research project was ideal for students who needed to do virtual projects, because of either work or family commitments, instead of being placed at a host industry location. The students were located in different states throughout Australia and some were located internationally. The use of forty-six student researchers helped to eliminate bias in the project that would occur if only one person conducted the study (Raward, 2001). Students were required to write a 1,500 to 2,000 word literature review on any aspect of Website assessment, evaluation, or usability.

To assess and evaluate the Websites, nine groups of students worked over nine semesters (four years) from February 2013 to November 2016 to evaluate 1517 Australian, Canadian, and American public library Websites using a pre-determined protocol adapted from Powers' 2011 research. Each student was assigned a minimum of twenty-five public library Websites. The students were given a template (in Excel) on which they had to capture information for each public library. Powers used ten criteria from a study done in Idaho, which determined the elements that a basic public library Website should have ([Persichini, Samuelson, and Zeiter, 2008](#per08); [Powers, 2011](#pow11)). The remaining eight criteria that are considered desirable for library Websites, were based upon an article by Matthews ([2009](#mat09)). Powers ([2011](#pow11)) combined the two sets of criteria and used it in a spreadsheet protocol to assess which of the items appear on the sample Websites. Two of the criteria that Powers used were excluded for this study: first, the names of library board members were not included in the protocol, as Australian libraries do not have boards; secondly, the description of library services available to patrons was also removed. Instead, a section was introduced for student researchers to make free form comments regarding each public library Website, including what the student researcher thought of the Website, the navigation of the site, programming, or other general comments. The student researchers were encouraged to enter their opinions about the Websites in Item 20 in Table 1\. Another deviation from the Powers study was the inclusion of branch libraries in the data, which was time-consuming. Some of the branches share information with the main branch; others have their own Website presence.

The two sets of criteria (basic or desirable) that were used by Powers are shown in Table 1 below.

Many of the students added their own opinions about what constituted a good or bad Website. The disadvantage of having students do the research is that the lead researcher could not evaluate all 1,517 Websites; the advantage is that it helped to mitigate the bias that could occur when only one researcher does the whole investigation ([Raward, 2001](#raw01)). The lead researcher did spot checks of some of the Websites to make sure the findings were consistent with what the students recorded on the spreadsheets and to check the quality of the findings.

<table><caption>Table 1: Categories</caption>

<tbody>

<tr>

<th>Item no.</th>

<th>Criteria</th>

<th>Response</th>

</tr>

<tr>

<th colspan="3">Basic criteria</th>

</tr>

<tr>

<td>1</td>

<td>Council or library name</td>

<td>Provided</td>

</tr>

<tr>

<td> </td>

<td>URL</td>

<td>Provided</td>

</tr>

<tr>

<td>2</td>

<td>Is this a joint-use facility</td>

<td>Yes or No</td>

</tr>

<tr>

<td>2a</td>

<td>If yes, it is a joint-use facility, which entities are sharing the site? (i.e., public library/school, public library/council, etc.)</td>

<td>Actual answer</td>

</tr>

<tr>

<td>3</td>

<td>Library address (both physical & mailing)</td>

<td>Yes or No; complete address</td>

</tr>

<tr>

<td>4</td>

<td>Telephone</td>

<td>Yes or No; phone number</td>

</tr>

<tr>

<td>5</td>

<td>Online contact</td>

<td>Yes or No; online contact or email</td>

</tr>

<tr>

<td>6</td>

<td>Hours of operation</td>

<td>Yes or No; actual hours</td>

</tr>

<tr>

<td>7</td>

<td>Director/manager</td>

<td>Yes or No; name of library director or manager</td>

</tr>

<tr>

<td>8</td>

<td>E-mail</td>

<td>Yes or No; email address</td>

</tr>

<tr>

<td>9</td>

<td>Link to online catalogue</td>

<td>Yes or No</td>

</tr>

<tr>

<td>10</td>

<td>Link to state wide collaborative services (e.g., Trove or state provided databases)</td>

<td>Yes or No</td>

</tr>

<tr>

<td>11</td>

<td>Current site content indicated by a date when last updated or last reviewed</td>

<td>Yes or No; date site last updated</td>

</tr>

<tr>

<th colspan="3">Desirable criteria</th>

</tr>

<tr>

<td>12</td>

<td>Mixture of text and images</td>

<td>Yes or No</td>

</tr>

<tr>

<td>13</td>

<td>Library events being promoted on the page</td>

<td>Yes or No</td>

</tr>

<tr>

<td>14</td>

<td>Search box present</td>

<td>Yes or No</td>

</tr>

<tr>

<td>15</td>

<td>Place to provide feedback about the site</td>

<td>Yes or No</td>

</tr>

<tr>

<td>16</td>

<td>Free from spelling and grammar errors</td>

<td>Yes or No</td>

</tr>

<tr>

<td>17</td>

<td>Place to ask for help</td>

<td>Yes or No</td>

</tr>

<tr>

<td>18</td>

<td>Image, icon or other graphic used to represent the library (not the council)</td>

<td>Yes or No</td>

</tr>

<tr>

<td>19</td>

<td>Site employs some Web 2.0 technologies like blogs, wikis, or reference to a social networking profile (Facebook, Twitter, Instagram, Pinterest, Historypin, Yammer, etc.)</td>

<td>Yes or No</td>

</tr>

<tr>

<td>20</td>

<td>Comments</td>

<td>Free form comments</td>

</tr>

</tbody>

</table>

The binary (yes/no) responses to the questions were converted into numerical data (in Excel) in order to do descriptive statistics.

### Research question

The main research question guiding the study was: _What kind of online access do public libraries provide to their community members?_

</section>

<section>

## Findings

</section>

<section>

### Descriptive statistics

The researchers reviewed 1517 public library Websites between February 2013 and November 2016\. The data were gathered in Australia (February 2013 to February 2014), followed by Canada (July 2014 to November 2015) and then the USA (July 2015 to November 2016).The state and provincial data for each country will be shown first. Then all data will be collated, discussed and compared. The data were processed using Excel.

</section>

<section>

### Australian data

Table 2 shows all the states and territories in Australia where the public libraries are located. The Australian Capital Territory (ACT) has one library system with multiple branches throughout the territory. In Tasmania, the State Library of Tasmania is responsible for the public library system and all their Websites are part of the state library system. In South Australia, the public libraries adopted the One Card system during the time the research was conducted. The One Card system requires all public libraries in the state to be part of the one library management system with only one public library catalogue for the state. Users need only one public library card to access any public library in the state. Library users can also reserve an item from any library through the online catalogue, after which the item is delivered to their library of choice ([Strempel, 2013](#str13)). The system went live in South Australia in 2012\. The 130 South Australian libraries were incrementally converted to the One Card system over a period of three years from 2012 to 2014 ([Strempel, 2014](#str14)).

<table><caption>Table 2: State distributions – Australia</caption>

<tbody>

<tr>

<th>State</th>

<th>No. of public  
library websites</th>

<th>Percentage</th>

</tr>

<tr>

<td>Australian Capital Territory (ACT)</td>

<td>1</td>

<td>0.2</td>

</tr>

<tr>

<td>New South Wales (NSW)</td>

<td>88</td>

<td>18.0</td>

</tr>

<tr>

<td>Northern Territory (NT)</td>

<td>17</td>

<td>3.5</td>

</tr>

<tr>

<td>Queensland (QLD)</td>

<td>69</td>

<td>14.1</td>

</tr>

<tr>

<td>South Australia (SA)</td>

<td>65</td>

<td>13.3</td>

</tr>

<tr>

<td>Tasmania (TAS)</td>

<td>44</td>

<td>9.0</td>

</tr>

<tr>

<td>Victoria (VIC)</td>

<td>43</td>

<td>8.8</td>

</tr>

<tr>

<td>Western Australia (WA)</td>

<td>161</td>

<td>33.0</td>

</tr>

<tr>

<td>Totals</td>

<td>488</td>

<td>100.0</td>

</tr>

</tbody>

</table>

Table 3 shows all the provinces and territories represented in the public library Websites in Canada.

<table><caption>Table 3: Provincial distributions – Canada</caption>

<tbody>

<tr>

<th>Province</th>

<th>No. of public  
library websites</th>

<th>Percentage</th>

</tr>

<tr>

<td>Alberta</td>

<td>178</td>

<td>29.7</td>

</tr>

<tr>

<td>British Columbia</td>

<td>54</td>

<td>9.0</td>

</tr>

<tr>

<td>Manitoba</td>

<td>37</td>

<td>6.2</td>

</tr>

<tr>

<td>New Brunswick</td>

<td>2</td>

<td>0.3</td>

</tr>

<tr>

<td>Newfoundland & Labrador</td>

<td>106</td>

<td>17.7</td>

</tr>

<tr>

<td>Northwest Territories</td>

<td>3</td>

<td>0.5</td>

</tr>

<tr>

<td>Nova Scotia</td>

<td>5</td>

<td>0.8</td>

</tr>

<tr>

<td>Nunavut</td>

<td>12</td>

<td>2.0</td>

</tr>

<tr>

<td>Ontario</td>

<td>163</td>

<td>27.2</td>

</tr>

<tr>

<td>Prince Edward Island</td>

<td>3</td>

<td>0.5</td>

</tr>

<tr>

<td>Quebec</td>

<td>25</td>

<td>4.2</td>

</tr>

<tr>

<td>Saskatchewan</td>

<td>7</td>

<td>1.2</td>

</tr>

<tr>

<td>Yukon Territories</td>

<td>4</td>

<td>0.7</td>

</tr>

<tr>

<td>Totals</td>

<td>599</td>

<td>100.0</td>

</tr>

</tbody>

</table>

The provinces with the largest number of libraries were Alberta, Ontario, Newfoundland and Labrador. Libraries from Quebec were also included, as one of the student researchers had lived in Quebec and could speak French.

Table 4 shows all four the regions of the USA represented in the public library Websites. The [Appendix](#app) shows how the states were grouped into regions.

<table><caption>Table 4: Regional distributions – USA</caption>

<tbody>

<tr>

<th>Region</th>

<th>No. of public  
library websites</th>

<th>Percentage</th>

</tr>

<tr>

<td>East</td>

<td>122</td>

<td>28.4</td>

</tr>

<tr>

<td>Midwest</td>

<td>119</td>

<td>27.7</td>

</tr>

<tr>

<td>South</td>

<td>135</td>

<td>31.4</td>

</tr>

<tr>

<td>West</td>

<td>54</td>

<td>12.6</td>

</tr>

<tr>

<td>Totals</td>

<td>430</td>

<td>100.0</td>

</tr>

</tbody>

</table>

</section>

<section>

## Comparison of Australian, Canadian, and American public library Websites

The project was not initially designed as a comparative study. It started as an Australian project, but was expanded because of the success of that project. The project was first expanded to include Canada and then the USA. Such a comprehensive study allows comparison between countries. One cavaet must be mentioned, namely that the study did not include large urban or small rural libraries in the USA, but mostly suburban libraries situated just outside the large metropolitan areas that serve a population between 25,000 and 100,000\. In Canada and Australia, libraries of any size and location were included in the study.

</section>

<section>

### Joint-use facilities

Joint-use facilities were notable in the results, with 351 of the 1,517 sites surveyed (23.1%) falling into this category. A joint-use facility is a library building that is shared with another type of library (e.g., school, Technical and Further Education, or university libraries), another type of cultural centre (e.g., gallery or museum), or another type of council or governmental facility (e.g., a community centre or city government council centre). Each country had joint-use Websites, with Canada having the highest number. The top categories for joint-use facility by type were, first schools, then community centres, followed by other types in third place. ‘_Other_' refers to any type of facility that was not captured in the eight categories listed, such as tourist information centres, family centres, cultural centres (not an art gallery or a museum), post office, welcome centres, women's institute, local history rooms, and gardens. In Australia, joint-use facilities were found in 22% (108) of the public libraries. Within the 108 public libraries, four states noticeably have the majority of the joint-use facilities: New South Wales (26), South Australia (26), Tasmania (21), and Western Australia (26). The other four states or territories with joint-use facilities only had a maximum of four such facilities. The two most common types of joint-use facilities were public schools (30) and ‘_other_' (32) including family centres, tourist information centres, or a conglomeration of more than one function like a regional gallery, council office, and leisure centre.

As mentioned above, joint-use facilities are also prevalent in Canada. Joint-use facilities were found in 27% (159) of the public libraries that were included in the study. The three most common types of joint-use facilities were public schools (63), community centres (34) and meeting rooms (31) which together represented 80.5% of the joint-use facilities of the library Websites. Within the 159 public libraries, three provinces have the majority of the joint-use facilities, namely Newfoundland and Labrador (63), Ontario (35), and Alberta (33). The other provinces or territories only had minimal numbers of joint-use facilities.

In the USA, joint-use facilities were present in 20% (84) of the public libraries. The two most common types of joint-use facilities were community centres (29) and ‘other' (25). These two categories accounted for 64% of the joint facilities of the libraries in the USA.

Table 5 shows the joint use facilities by type for Australia, Canada and the USA.

<table><caption>Table 5: Number of joint-use facilities</caption>

<tbody>

<tr>

<th>Joint-use facility type</th>

<th>Australia</th>

<th>Canada</th>

<th>USA</th>

<th>Totals</th>

</tr>

<tr>

<td>Schools (K/R-12)</td>

<td>30</td>

<td>63</td>

<td>7</td>

<td>100</td>

</tr>

<tr>

<td>Technical and further education/community college</td>

<td>6</td>

<td>0</td>

<td>0</td>

<td>6</td>

</tr>

<tr>

<td>Universities</td>

<td>7</td>

<td>2</td>

<td>1</td>

<td>10</td>

</tr>

<tr>

<td>Galleries</td>

<td>3</td>

<td>12</td>

<td>3</td>

<td>18</td>

</tr>

<tr>

<td>Community centres</td>

<td>17</td>

<td>34</td>

<td>29</td>

<td>80</td>

</tr>

<tr>

<td>Meeting rooms</td>

<td>7</td>

<td>31</td>

<td>5</td>

<td>43</td>

</tr>

<tr>

<td>Councils</td>

<td>1</td>

<td>0</td>

<td>8</td>

<td>9</td>

</tr>

<tr>

<td>Museums</td>

<td>8</td>

<td>4</td>

<td>6</td>

<td>18</td>

</tr>

<tr>

<td>Other</td>

<td>29</td>

<td>13</td>

<td>25</td>

<td>67</td>

</tr>

<tr>

<td>Totals</td>

<td>108</td>

<td>159</td>

<td>84</td>

<td>351</td>

</tr>

</tbody>

</table>

</section>

<section>

### Top five and bottom five criteria

The top categories included in the Websites in Australia were the address, phone number, hours of operations, and link to the catalogue. Spelling and grammar used on the Websites were also of a high standard (refer to Table 6). The top categories in Canada were address, phone number, hours of operation, and having text and images on the Website. Similar to Australia, the spelling and grammar on the Websites were of a high standard. Australia and Canada also had the library's street address on the website. Unlike both Canada and Australia, the address was not one of the top five categories found in the USA Websites. Libraries in Australia and the USA had links to their catalogues on the Website. Sites in Canada and the USA included text and images.

<table><caption>Table 6: Top five criteria – cross-country comparison</caption>

<tbody>

<tr>

<th rowspan="2">Criterion</th>

<th colspan="2">Australia  
n=488</th>

<th colspan="2">Canada  
n=599</th>

<th colspan="2">USA  
n=430</th>

</tr>

<tr>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

</tr>

<tr>

<td>Address</td>

<td>430</td>

<td>88</td>

<td>580</td>

<td>97</td>

<td>—</td>

<td>—</td>

</tr>

<tr>

<td>Telephone</td>

<td>415</td>

<td>85</td>

<td>568</td>

<td>95</td>

<td>404</td>

<td>94</td>

</tr>

<tr>

<td>Hours of operations</td>

<td>404</td>

<td>83</td>

<td>551</td>

<td>92</td>

<td>404</td>

<td>94</td>

</tr>

<tr>

<td>Catalogue link</td>

<td>328</td>

<td>67</td>

<td>—</td>

<td>—</td>

<td>382</td>

<td>89</td>

</tr>

<tr>

<td>Spelling and grammar</td>

<td>400</td>

<td>82</td>

<td>537</td>

<td>90</td>

<td>374</td>

<td>87</td>

</tr>

<tr>

<td>Text and images</td>

<td>—</td>

<td>—</td>

<td>528</td>

<td>88</td>

<td>383</td>

<td>89</td>

</tr>

</tbody>

</table>

The five criteria that were included in the fewest public library Websites for each of the three countries are shown in Table 7:

<table><caption>Table 7: Bottom five criteria – cross-country comparison</caption>

<tbody>

<tr>

<th rowspan="2">Criterion</th>

<th colspan="2">Australia  
n=488</th>

<th colspan="2">Canada  
n=599</th>

<th colspan="2">USA  
n=430</th>

</tr>

<tr>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

</tr>

<tr>

<td>Ask for help</td>

<td>89</td>

<td>18</td>

<td>274</td>

<td>46</td>

<td>187</td>

<td>43</td>

</tr>

<tr>

<td>Director or manager name</td>

<td>99</td>

<td>20</td>

<td>—</td>

<td>—</td>

<td>—</td>

<td>—</td>

</tr>

<tr>

<td>Date Website updated</td>

<td>146</td>

<td>30</td>

<td>123</td>

<td>21</td>

<td>126</td>

<td>29</td>

</tr>

<tr>

<td>Place to give feedback</td>

<td>132</td>

<td>27</td>

<td>190</td>

<td>32</td>

<td>155</td>

<td>36</td>

</tr>

<tr>

<td>Unique icon for library</td>

<td>171</td>

<td>35</td>

<td>—</td>

<td>—</td>

<td>—</td>

<td>—</td>

</tr>

<tr>

<td>Place for online contact</td>

<td>—</td>

<td>—</td>

<td>315</td>

<td>53</td>

<td>160</td>

<td>37</td>

</tr>

<tr>

<td>Search box available</td>

<td>—</td>

<td>—</td>

<td>331</td>

<td>55</td>

<td>—</td>

<td>—</td>

</tr>

<tr>

<td>E-mail address 229 53%</td>

<td>—</td>

<td>—</td>

<td>—</td>

<td>—</td>

<td>229</td>

<td>53</td>

</tr>

</tbody>

</table>

Three of the bottom five categories were shared by all the countries, namely asking for help, being able to give feedback about the Website, and an indication when the Website was last updated. Australia also had a very few cases where the director's name and the library manager's name appeared on the Website. In Australia, many of the libraries are associated with councils and the councils tend to manage the libraries' marketing, so not many distinct icons or images were present on the libraries' Websites. In Canada and the USA online contact details were not included on many of the Websites. In Canada, the search box was not available in 45% in the libraries. In the USA, there was a lack of an e-mail address in 47% of the libraries.

</section>

<section>

### Basic and desirable data

The research with this particular group of libraries produced some interesting results on the distribution of basic and desriable data. Most of the libraries have basic information about their library on the Website:

<table><caption>Table 8: Basic information</caption>

<tbody>

<tr>

<th rowspan="2">Item</th>

<th colspan="2">Australia  
n=488</th>

<th colspan="2">Canada  
n=599</th>

<th colspan="2">USA  
n=430</th>

<th colspan="2">Totals  
n=1517</th>

</tr>

<tr>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

</tr>

<tr>

<td>Library name</td>

<td>488</td>

<td>100</td>

<td>599</td>

<td>100</td>

<td>430</td>

<td>100</td>

<td>1517</td>

<td>100</td>

</tr>

<tr>

<td>Address</td>

<td>430</td>

<td>88</td>

<td>580</td>

<td>97</td>

<td>361</td>

<td>84</td>

<td>1371</td>

<td>90</td>

</tr>

<tr>

<td>Telephone</td>

<td>415</td>

<td>85</td>

<td>568</td>

<td>95</td>

<td>404</td>

<td>94</td>

<td>1387</td>

<td>91</td>

</tr>

<tr>

<td>Online contact</td>

<td>182</td>

<td>37</td>

<td>315</td>

<td>53</td>

<td>160</td>

<td>37</td>

<td>657</td>

<td>43</td>

</tr>

<tr>

<td>Hours of operations</td>

<td>404</td>

<td>83</td>

<td>551</td>

<td>92</td>

<td>404</td>

<td>94</td>

<td>1359</td>

<td>90</td>

</tr>

<tr>

<td>Director or manager name</td>

<td>99</td>

<td>20</td>

<td>422</td>

<td>71</td>

<td>319</td>

<td>74</td>

<td>840</td>

<td>55</td>

</tr>

<tr>

<td>E-mail link</td>

<td>288</td>

<td>59</td>

<td>444</td>

<td>74</td>

<td>229</td>

<td>53</td>

<td>961</td>

<td>63</td>

</tr>

<tr>

<td>Catalogue link</td>

<td>328</td>

<td>67</td>

<td>437</td>

<td>73</td>

<td>382</td>

<td>89</td>

<td>1147</td>

<td>76</td>

</tr>

<tr>

<td>Databases</td>

<td>289</td>

<td>59</td>

<td>465</td>

<td>78</td>

<td>283</td>

<td>66</td>

<td>1037</td>

<td>68</td>

</tr>

<tr>

<td>Date Website updated</td>

<td>146</td>

<td>30</td>

<td>123</td>

<td>21</td>

<td>126</td>

<td>29</td>

<td>395</td>

<td>26</td>

</tr>

<tr>

<td>Joint-use facility</td>

<td>108</td>

<td>22</td>

<td>159</td>

<td>27</td>

<td>84</td>

<td>20</td>

<td>351</td>

<td>23</td>

</tr>

</tbody>

</table>

The desirable information on the Website depended on what library management deemed important for the patrons and varied across the country, state, county, and municipality. The council's information technology department could also suggest what is included and excluded on the Website. This list of the desirable elements is in accordance with that of Powers ([2011](#pow11)). Table 9 shows the summarised results of each country's libraries:

<table><caption>Table 9: Desirable information</caption>

<tbody>

<tr>

<th rowspan="2">Item</th>

<th colspan="2">Australia  
n=488</th>

<th colspan="2">Canada  
n=599</th>

<th colspan="2">USA  
n=430</th>

<th colspan="2">Totals  
n=1517</th>

</tr>

<tr>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

<th>Yes</th>

<th>%</th>

</tr>

<tr>

<td>Text and images</td>

<td>325</td>

<td>67</td>

<td>529</td>

<td>88</td>

<td>383</td>

<td>89</td>

<td>1237</td>

<td>82</td>

</tr>

<tr>

<td>Events promoted</td>

<td>324</td>

<td>66</td>

<td>469</td>

<td>78</td>

<td>360</td>

<td>84</td>

<td>1153</td>

<td>76</td>

</tr>

<tr>

<td>Search box</td>

<td>200</td>

<td>41</td>

<td>331</td>

<td>55</td>

<td>270</td>

<td>63</td>

<td>801</td>

<td>53</td>

</tr>

<tr>

<td>Feedback form</td>

<td>132</td>

<td>27</td>

<td>190</td>

<td>32</td>

<td>155</td>

<td>36</td>

<td>477</td>

<td>31</td>

</tr>

<tr>

<td>Correct spelling and grammar</td>

<td>400</td>

<td>82</td>

<td>537</td>

<td>90</td>

<td>374</td>

<td>87</td>

<td>1311</td>

<td>86</td>

</tr>

<tr>

<td>Ask for help</td>

<td>89</td>

<td>18</td>

<td>274</td>

<td>46</td>

<td>187</td>

<td>43</td>

<td>550</td>

<td>36</td>

</tr>

<tr>

<td>Icon or image different from council</td>

<td>171</td>

<td>35</td>

<td>358</td>

<td>60</td>

<td>325</td>

<td>76</td>

<td>854</td>

<td>56</td>

</tr>

<tr>

<td>Web 2.0</td>

<td>219</td>

<td>45</td>

<td>420</td>

<td>70</td>

<td>357</td>

<td>83</td>

<td>996</td>

<td>66</td>

</tr>

</tbody>

</table>

</section>

<section>

### Number of items included

The study identified the number of items that were included on the Websites of the libraries in the three countries. Figure 1 below shows the USA data (solid bar) as a normal distribution (bell curve) that peaks at 13 items. Of the Canadian Websites, 293 out of 599 (49%) contained 13 or 14 items. The USA and Australian numbers were not as high as that of Canada. Australia's data follows a wavy bell curve that peaks at 51 libraries containing 11 and 13 items, followed by 48 libraries with nine and 10 items. Between 9 and 15 items are included in 300 of the 488 libraries (62%). Canada's numbers were higher; 483 libraries (81%) had between 11 and 17 criteria on their Websites. The USA numbers varied from 11 to 17 items for 325 libraries (77%). This number is a little lower than Canada, but the items are spread more evenly. The number of items for Canadian Websites peaks at 14.

<figure>

![Figure 1: No. of items and libraries](../p786fig1.png)

<figcaption>Figure 1: No. of items and libraries – Australia, n=488; Canada, n=599; USA, n=430</figcaption>

</figure>

<figure>

![Figure 2: Items included by topic](../p786fig2.png)

<figcaption>Figure 2: Items included (by topic) – Australia, n=488; Canada, n=599; USA, n=430</figcaption>

</figure>

When looking at the data by category (Figure 2), it is obvious that, in some areas, one country's public libraries have more effective Websites. Given that there were more Canadian Websites than either Australian or American in most categories or topics, they have a larger impact on the data.

As public library Websites represent the electronic branch, certain data elements need to be included. In many cases, the majority of the libraries' Websites contained the data we searched for, but some included minimal or no data. Either these libraries did not have the ability to provide the services the community needed, or they did not have access to the required funding. Despite operating in the 21st century, some libraries still have not fully committed to including relevant information on their Website. The researchers discovered that static Websites still exist in all the countries. A static Website is one that does not have any links, search boxes or any way to contact the library through the Website. In a static Website there is no way to search for anything and the Website is simply a placeholder Website that has the name of the library, address, telephone number, and the hours the library is open.

</section>

<section>

## Discussion

In the 21st century, public libraries need well designed Websites to encourage community members to access and use their services. Public library Websites can also be referred to as electronic library branches as opposed to the bricks and mortar branch that can be physically entered. This paper reported the findings from research that focused on the accessibility and usability of public library Websites, regarding the inclusion of numerous criteria or items on the Websites. Although some well-designed Websites exist in all three countries included in this research, none of the libraries included all the criteria on the research template. These libraries can improve their Websites by following good Web design principles, especially user-centred design ([Goodman and Schofield, 2015](#goo15); [Nielsen, 1996](#nie96); [2012](#nie12)).

This research contributes to the understanding of public library managers about how the accessibility of their Websites can be improved, to make them effective points of entry to the programmes and services required by community members. The research questions were answered as follows:

</section>

<section>

### What kind of online access do public libraries provide to their community members?

The library Websites in the three countries provided varying degrees of access, first to the Website and secondly, to the resources. Access to a Website can be provided through the city council's site or a separate library Website and examples of both types were included in the research. Some of the Websites were static and included only basic information like the library name, address, telephone number, operating hours, and other branch locations. On some of the sites the user could link to maps and interactive information to find access to databases, catalogues, lists of books, as well as information tailored for a particular patron type (e.g., children, teens and seniors).

The resources that can be accessed also varied. Some Websites do not provide access to any resources, except to a catalogue link. Others made a plethora of information available, but users needed a library card number to obtain access to the databases. For example, in Australia the Websites provided links to the National Library of Australia's Trove database, as well as State Library resources. In South Australia specifically, the streaming services for e-books and movies is done through the [State Library of South Australia](http://www.webcitation.org/6wViQEgUM). In Canada and the USA, the library databases varied and some also included streaming services for movies and e-books.

</section>

<section>

### Were the users able to contact the public library through the website?

Four criteria directly determined whether the library can be contacted through the public library Website. Firstly, the Website can contain an online contact link where the customer can complete a form requesting library staff to contact them. Secondly, an email link with either a form or an email address for the library's general site can be provided. A feedback form can be included to ask questions about sources, programmes, or just about anything. Finally, there could be an ‘_ask for help_' link. Many libraries have this as a way to go into chat or instant messaging. Interestingly, three of these four items were not on many of the libraries' Websites.

<table><caption>Table 10: Features providing contact with libraries</caption>

<tbody>

<tr>

<th rowspan="2">Item</th>

<th colspan="2">Australia  
n=488</th>

<th colspan="2">Canada  
n=599</th>

<th colspan="2">USA  
n=430</th>

<th colspan="2">Totals  
n=1517</th>

</tr>

<tr>

<th>N</th>

<th>%</th>

<th>N</th>

<th>%</th>

<th>N</th>

<th>%</th>

<th>N</th>

<th>%</th>

</tr>

<tr>

<td>Online contact</td>

<td>182</td>

<td>37</td>

<td>315</td>

<td>53</td>

<td>160</td>

<td>37</td>

<td>657</td>

<td>43</td>

</tr>

<tr>

<td>E-mail link</td>

<td>288</td>

<td>59</td>

<td>444</td>

<td>74</td>

<td>229</td>

<td>53</td>

<td>961</td>

<td>63</td>

</tr>

<tr>

<td>Feedback form</td>

<td>132</td>

<td>27</td>

<td>190</td>

<td>32</td>

<td>155</td>

<td>36</td>

<td>477</td>

<td>31</td>

</tr>

<tr>

<td>Ask for help</td>

<td>89</td>

<td>18</td>

<td>274</td>

<td>46</td>

<td>187</td>

<td>43</td>

<td>550</td>

<td>36</td>

</tr>

</tbody>

</table>

Table 10 illustrates that online contact, feedback forms, and ask for help functions are all areas that are not done well by all of the libraries in our research study.

</section>

<section>

### How can users use the Websites to contact the library?

Contacting the library can be done in many ways, e.g., by telephone or e-mail, in person, through the Website, by providing feedback about the Website, and online contact forms. All of these methods, except in person contact, were captured as part of this research. The majority of the libraries show telephone numbers and addresses to facilitate personal visits to the bricks and mortar site, and e-mail addresses are included on the Websites, but not to the same degree as physical addresses and telephone numbers. E-mail addresses were not often included for all three countries (Australia 59%, Canada 74%, and the USA 53%). In fact, e-mail information was in the bottom five categories for the USA.

Contacting the library through providing feedback appeared in the bottom five categories for all three countries. Search boxes were available on the majority of the Websites for the three counties combined (801/1517, 53%), but Canada and the USA (55% and 63% respectively) were better at providing search capability than Australia (41%). Online contact forms were not popular for providing contact on any of the library Websites. Of the Canadian Websites, 53% provided online contact forms, but these figures were lower in the case of Australia (37%) and the USA (37%). Providing feedback on the Websites was available on only a small minority of Websites for all three countries and it seems that the libraries prefer not to receive feedback about the content and quality of their Websites (Australia 27%, Canada 32%, USA 36%).

</section>

<section>

## Study limitations

A limitation of this study was that inexperienced researchers were used in conducting research. However, the research design has been validated in previous studies and the questions were already developed, which simplified replicating the study with inexperienced researchers.

</section>

<section>

## Conclusions and recommendations

Four major observations emerged from the analysis of the data:

1. From the student comments, some of the Websites that were evaluated were high quality Websites that gave users a positive experience and welcomed them back repeatedly. These public library electronic branches used an intuitive approach and user-centred design to provide the patron with the experience they expect when visiting the site.

2. Based upon our knowledge of how public libraries work with councils, public libraries have to adhere to policies and procedures that are designed and implemented by information technology professionals in their city council offices. This often precludes the development of a custom Website, which is not the image that library management want to portray. When discussing this with public library managers during conferences, it was obvious that they were not always able to do exactly what they felt was necessary with their Websites.

3. From the research, it was obvious that Facebook and Twitter were still the most popular social media sites used by the public libraries. The inclusion of other Web 2.0 tools, such as Instagram and Snapchat, would improve the ability of public libraries to reach younger target audiences in order to improve engagement and perceptions about what libraries actually provide communities.

4. Public libraries do not communicate well to their communities and they do not provide sufficient mechanisms to allow their users to respond to the library staff. Websites lacking information such as e-mail addresses, staff names, feedback mechanisms, contact forms, etc. indicate that library staff are not interested in their users' opinions about their Website content and quality.

The research question investigated the access of libraries and resources through the Websites for the users. In all cases, the electronic branch of the public libraries could be found. However, true access includes satisfaction with the type of information the community members need. Some of the public libraries had databases and catalogues on their Websites and others did not. In the digital world that our communities live in today, there is an expectation that our public libraries need to inhabit this world and provide access to digital sources. The programming and other services that public libraries create for their stakeholders is the other aspect the staff provide.

The study has provided rich binary data on more than 1500 public libraries in three countries. This data shows that some of the public libraries provide a better quality Website experience than others. Overall, some of the staff in the exemplar libraries could guide other library staff towards improving their sites and providing better services to their community.

As part of future research, the researchers will revisit the Australian Websites to determine whether there have been any changes to the public library Websites and whether the original research findings are still valid. Websites evolve and change over a four year period. There have also been amalgamations in councils and changes in how the Australian councils are configured in different states. Changes in political structures will also have influences on how libraries are managed and what will and will not be placed on a library Website. It will investigate similar aspects of Websites, but will expand some of the questions to provide additional information. We also suggest that future research should investigate what types of tools and resources are available on the library Websites.

</section>

<section>

## Acknowledgements

The authors would like to thank the following students who assisted with the research while undertaking Information Management studies at the University of South Australia: Andrew Angus, Sarah Barkla, Andree Brett, William Chan, Michelle Chitts, Natalie Clifford, Seam Connelly, Robin Costelloe, Renee Davy, Lungile Dlamini, Daniel Easterbrook, Jenny Gallas, James Gaunt, Cory Greenwood, Fariha Hamid, Deanna Harrison, Jed Hood, Kerrie Impiombato, Catharine Kelly, Thea Kitchen, Isabelle Laskari, Matthew Leach, Annie Lewis, Zoe Manger, Daniel Mason, Cyprian Maynard, Tyler McCaffery, Claire Morris, Patrick O'Leary, Margaret Parker, Lauren Pillar, Jesse Pollard, Tamsin Reed, Brett Ross, Paula Saegenschnitter, Jessica Smith, Jasmine Taylor, Claire Telfer, Malcolm Thompson, Sarah Thompson, Amelia Trimboli, Amy Vanner, Jana Waldmann, Luke Watsford, Jaq Wheadon, and Fiona Wilkinson.

</section>

<section>

## <a id="author"></a>About the authors

**Diane L Velasquez** is the Program Director of the Information Management Program at the School of Information Technology and Mathematical Sciences, University of South Australia. She holds a BA in political science from San Jose State University, an MBA in Management from Golden Gate University, a MLS from the University of Arizona, and a Ph.D. from the University of Missouri. Her research interests are in risk management and disaster recovery, administrative and management of information agencies, and evaluation and assessment of public library websites. Dr Velasquez can be reached at [diane.velasquez@unisa.edu.au](mailto:diane.velasquez@unisa.edu.au).  
**Nina Evans** is Associate Head, School of Information Technology and Mathematical Science (ITMS) at the University of South Australia. She holds tertiary qualifications in Chemical Engineering, Education and Computer Science, a Masters' degree in Information Technology, an MBA and a PhD. Her research interests relate to business-IT fusion, information and knowledge management, enterprise social networks, innovation and ICT education. She can be contacted at [nina.evans@unisa.edu.au](mailto:nina.evans@unisa.edu.au)

</section>

<section>

## References

<ul> 
<li id="bec13">Becker, D. A. &amp; Yannotta, L. (2013). Modeling a library website redesign process: developing a user-centered website through usability testing.  <em>Information Technology and Libraries, 32</em>(1), 6-22.</li>
<li id="ben01">Benbunan-Fich, R. (2001). Using protocol analysis to evaluate the usability of a commercial web site. <em>Information &amp; Management, 39</em>(3), 151-163.</li>
<li id="ber06">Bérlanger, F., Fan, W., Schaupp, L. C., Krishen, A., Everhart, J., Poteet, D. &amp; Nakamoto, K. (2006). Web site success metrics: addressing the duality of goals. <em>Communications of the ACM, 49</em>(12), 114-116.</li>
<li id="cao05">Cao, M., Zhang, Q. &amp; Seydel, J. (2005). B2C e-commerce web site quality: an empirical examination. <em>Industrial Management &amp; Data Systems, 195</em>(5), 645-661.</li>
<li id="cho14">Chow, A. S., Bridges, M. &amp; Commander, P. (2014). The website design and usability of U.S. academic and public libraries: findings from a nationwide study. <em>Reference &amp; User Services Quarterly, 53</em>(3), 253-265.</li>
<li id="chu10">Chua, A. Y. K. &amp; Goh, D. H. (2010). A study of web 2.0 applications in library Websites. <em>Library &amp; Information Science Research, 32</em>(3), 203-211.</li>
<li id="con11">Conway, V. (2011). Website accessibility in Western Australia public libraries. <em>Australian Library Journal, 60</em>(2), 103-112.</li>  
<li id="dom15">Dominguez, G., Hammill, S.J. &amp; Brillat, A. I. (2015). Toward a usable academic library web site: a case study of tried and tested usability practices. <em>Journal of Web Librarianship, 9</em>(2-3), 99-120.</li> 
<li id="goo15">Goodman, A. L. &amp; Schofield, M. (2015). The user experience.  <em>Public Libraries, 54</em>(3), 20-21.</li>
<li id="goo86">Goodwin, N. (1986). Functionality and usability. <em>Communications of the ACM, 30</em>(3), 229-233.</li>
<li id="maa14">Maatta Smith, S. L. (2014). Web accessibility assessment of urban public library Websites. <em>Public Library Quarterly, 33</em>(3), 187-204.</li>
<li id="mcm12">McMenemy, D. (2012). Emergent digital services in public libraries: a domain study.  <em>New Library World, 113</em>(11/12), 507-527.</li>
<li id="mat09">Mathews, B. (2009). Web design matters: ten essentials for any library site. <em>Library Journal, 134</em>(3), 24-25.</li>
<li id="nie96">Nielsen, J. (1996). Usability heuristics. <em>Health Management Technology, 17</em>(11), 34.</li>
<li id="nie12">Nielsen, J. (2012). <a href="http://www.webcitation.org/6wVi3GDp5">Usability 101: introduction to usability</a>. Fremont, CA: Nielsen Norman Group. Retrieved from: http://www.nngroup.com/articles/usability-101-introduction-to-usability/ (Archived by WebCite® at http://www.webcitation.org/6wVi3GDp5.)</li>
<li id="per08">Persichini, G., Samuelson, M. &amp; Zeiter, L. B. (2008). E-branch in a box: how Idaho libraries created an easy and sustainable web presence. <em>PNLA Quarterly, 72</em>(6), 18-19.</li>
<li id="pol07">Poll, R. (2007). <em><a href="http://www.webcitation.org/6wViIINak">Evaluating the library website: statistics and quality measures</a></em>. Paper presented at the World Library and Information Conference: 73rd IFLA General Conference and Council, Durban, South Africa.  Retrieved from http://archive.ifla.org/IV/ifla73/papers/074-Poll-en.pdf (Archived by WebCite® at http://www.webcitation.org/6wViIINak.)</li>
<li id="pow11">Powers, B. S. (2011). Oh, what a tangled web we weave! An evaluation of Pennsylvania's public library Websites for a basic level of web presence and beyond. <em>Current Studies in Librarianship, 31</em>(1) 21-35.</li>
<li id="raw01">Raward, R. (2001). Academic library web site design principles: development of a checklist. <em>Australian Academic and Research Libraries, 32</em>(2), 123-136. </li> 
<li id="ril12">Riley-Huff, D. A. (2012). Web accessibility and universal design: a primer on standards and best  practices for libraries. <em>Library Technology Reports, 48</em>(7), 29-35.</li>  
<li id="sor12">Sørum, H., Andersen, K. N. &amp; Vatrapu, R. (2012). Public Websites and human-computer interaction: an empirical study of measurement of website quality and user satisfaction. <em>Behaviour &amp; Information Technology, 31</em>(7), 697-706.</li>
<li id="str13">Strempel, G. (2013). South Australian public libraries moving towards a state-wide consortium. <em>InCite, 34</em>(1/2), 30.</li>
<li id="str14">Strempel, G. (2014). <em><a href="http://www.webcitation.org/6wVibx62e">Standing on the shoulders of giants: how the New Zealand public libraries inspired the South Australian "One Card" Network</a></em>. Paper presented at the LIANZA Conference 2014 Pou Whakairo. Retrieved from http://www.lianza.org.nz/geoff-strempel-standing-shoulders-giants-how-new-zealand-public-libraries-inspired-south-australian (Archived by WebCite® at http://www.webcitation.org/6wVibx62e.)</li>
<li id="teo03">Teo, H-H., Oh, L-B., Liu, C. &amp; Wei, K-K. (2003). An empirical study of the effects of interactivity on web user attitude. <em>International Journal of Human-Computer Studies, 58</em>(3), 281-305.</li>
<li id="wor11">World Wide Web Consortium (W3C). (2011). <em><a href="http://www.webcitation.org/6wVipP0bt">WCAG 2 at a glance</a></em>. Cambridge, MA: W3C. Retrieved from https://www.w3.org/WAI/WCAG20/glance/ (Archived by WebCite® at http://www.webcitation.org/6wVipP0bt.)</li>
<li id="wan07">Wang, J. &amp; Senecal, S. (2007). Measuring perceived website usability. <em>Journal of Internet Commerce, 6</em>(4), 97-112.</li>
</ul>

</section>

<section>

## Appendix

<table><caption>USA states by region</caption>

<tbody>

<tr>

<th>Region</th>

<th>State</th>

<th>No. of public  
library websites</th>

<th>Percentage</th>

</tr>

<tr>

<td rowspan="13">East</td>

<td>No state designated</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td>Connecticut</td>

<td>20</td>

<td>4.7</td>

</tr>

<tr>

<td>Delaware</td>

<td>1</td>

<td>0.2</td>

</tr>

<tr>

<td>Maine</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Maryland</td>

<td>6</td>

<td>1.4</td>

</tr>

<tr>

<td>Massachusetts</td>

<td>19</td>

<td>4.4</td>

</tr>

<tr>

<td>New Hampshire</td>

<td>1</td>

<td>0.2</td>

</tr>

<tr>

<td>New Jersey</td>

<td>18</td>

<td>4.2</td>

</tr>

<tr>

<td>New York</td>

<td>31</td>

<td>7.2</td>

</tr>

<tr>

<td>Pennsylvania</td>

<td>21</td>

<td>4.9</td>

</tr>

<tr>

<td>Rhode Island</td>

<td>3</td>

<td>0.7</td>

</tr>

<tr>

<td>Washington DC</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<td>Vermont</td>

<td>0</td>

<td>0.0</td>

</tr>

<tr>

<th>Total East</th>

<th> </th>

<th>122</th>

<th>28.4</th>

</tr>

<tr>

<td rowspan="12">Midwest</td>

<td>Illinois</td>

<td>27</td>

<td>6.3</td>

</tr>

<tr>

<td>Indiana</td>

<td>14</td>

<td>3.3</td>

</tr>

<tr>

<td>Iowa</td>

<td>5</td>

<td>1.2</td>

</tr>

<tr>

<td>Kansas</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td>Michigan</td>

<td>17</td>

<td>4.0</td>

</tr>

<tr>

<td>Minnesota</td>

<td>3</td>

<td>0.7</td>

</tr>

<tr>

<td>Missouri</td>

<td>6</td>

<td>1.4</td>

</tr>

<tr>

<td>Nebraska</td>

<td>6</td>

<td>1.4</td>

</tr>

<tr>

<td>North Dakota</td>

<td>3</td>

<td>0.7</td>

</tr>

<tr>

<td>Ohio</td>

<td>23</td>

<td>5.3</td>

</tr>

<tr>

<td>South Dakota</td>

<td>1</td>

<td>0.2</td>

</tr>

<tr>

<td>Wisconsin</td>

<td>12</td>

<td>2.8</td>

</tr>

<tr>

<th>Total Midwest</th>

<th> </th>

<th>119</th>

<th>27.7</th>

</tr>

<tr>

<td rowspan="14">South</td>

<td>Alabama</td>

<td>5</td>

<td>1.2</td>

</tr>

<tr>

<td>Arkansas</td>

<td>6</td>

<td>1.4</td>

</tr>

<tr>

<td>Florida</td>

<td>10</td>

<td>2.3</td>

</tr>

<tr>

<td>Georgia</td>

<td>8</td>

<td>1.9</td>

</tr>

<tr>

<td>Kentucky</td>

<td>10</td>

<td>2.3</td>

</tr>

<tr>

<td>Louisiana</td>

<td>16</td>

<td>3.7</td>

</tr>

<tr>

<td>Mississippi</td>

<td>11</td>

<td>2.6</td>

</tr>

<tr>

<td>North Carolina</td>

<td>7</td>

<td>1.6</td>

</tr>

<tr>

<td>Oklahoma</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td>South Carolina</td>

<td>7</td>

<td>1.6</td>

</tr>

<tr>

<td>Tennessee</td>

<td>10</td>

<td>2.3</td>

</tr>

<tr>

<td>Texas</td>

<td>30</td>

<td>7.0</td>

</tr>

<tr>

<td>Virginia</td>

<td>8</td>

<td>1.9</td>

</tr>

<tr>

<td>West Virginia</td>

<td>6</td>

<td>1.4</td>

</tr>

<tr>

<th>Total South</th>

<th> </th>

<th>136</th>

<th>31.4</th>

</tr>

<tr>

<td rowspan="13">West</td>

<td>Alaska</td>

<td>3</td>

<td>0.7</td>

</tr>

<tr>

<td>Arizona</td>

<td>5</td>

<td>1.2</td>

</tr>

<tr>

<td>California</td>

<td>20</td>

<td>4.7</td>

</tr>

<tr>

<td>Colorado</td>

<td>4</td>

<td>0.9</td>

</tr>

<tr>

<td>Hawaii</td>

<td>1</td>

<td>0.2</td>

</tr>

<tr>

<td>Idaho</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td>Montana</td>

<td>1</td>

<td>0.2</td>

</tr>

<tr>

<td>Nevada</td>

<td>3</td>

<td>0.7</td>

</tr>

<tr>

<td>New Mexico</td>

<td>3</td>

<td>0.7</td>

</tr>

<tr>

<td>Oregon</td>

<td>6</td>

<td>1.4</td>

</tr>

<tr>

<td>Utah</td>

<td>1</td>

<td>0.2</td>

</tr>

<tr>

<td>Washington</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td>Wyoming</td>

<td>3</td>

<td>0.7</td>

</tr>

<tr>

<th>Total West</th>

<th> </th>

<th>54</th>

<th>12.6</th>

</tr>

</tbody>

</table>

</section>

</article>