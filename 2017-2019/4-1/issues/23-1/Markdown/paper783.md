<header>

#### vol. 23 no. 1, March, 2018

</header>

<article>

# How trust in Wikipedia evolves: a survey of students aged 11 to 25

## [Josiane Mothe](#author) and [Gilles Sahut](#author).

> **Introduction**. Whether Wikipedia is to be considered a trusted source is frequently questioned in France. This paper reports the results of a survey examining the levels of trust shown by young people aged eleven to twenty-five.  
> **Method**. We analyse the answers given by 841 young people, aged eleven to twenty-five, to a questionnaire. To our knowledge, this is the largest study ever published on the topic. It focuses on (1) the perception young people have of Wikipedia; (2) the influence teachers and peers have on the young person’s own opinions; and (3) the variation of trends according to the education level.  
> **Analysis**. All the analysis is based on ANOVA (analysis of variance) to compare the various groups of participants. We detail the results by comparing the various groups of responders and discuss these results in relation to previous studies.  
> **Results**. Trust in Wikipedia depends on the type of information seeking tasks and on the education level. There are contrasting social judgments of Wikipedia. Students build a representation of a teacher’s expectations on the nature of the sources that they can use and hence the documentary acceptability of Wikipedia. The average trust attributed to Wikipedia for academic tasks could be induced by the tension between the negative academic reputation of the encyclopedia and the mostly positive experience of its credibility.  
> **Conclusion**. Our survey demonstrates significant differences between the levels of education, both for Wikipedia use and its representation. This variable should be included in studies related to information behaviour by the young to avoid generalisations that deny the disparities between ages.

<section>

## Introduction

Following the development of World Wide Web usage, particularly the social web, many studies have focused on information credibility and trust in the sources. Credibility and trust are multifaceted concepts that give rise to a diversity of conceptualisations and analytical frameworks within library and information science ([Kelton, Fleischmann and Wallace, 2008](#kel08); [Rieh, 2010](#rie10); [Choi and Stvilia, 2015](#cho15)). These concepts are thus subject to multiple definitions. Their commonality is their reference to a particular dimension of information gathering: the belief in its truth value. We distinguish information credibility from trust in a source. Information credibility is considered as a synonym to believability ([Tseng and Fogg, 1999](#tse99)). Credibility results from a highly subjective assessment of a person who gives more or less credit to a piece of information ([Rieh, 2010](#rie10)). Trust in a source has an epistemic dimension, in the perception of its expertise; and a moral dimension, in the perception of its honesty. The evaluation of both dimensions ensures the credibility of the information provided by the source. We propose to define credibility as a characteristic granted to information depending on its truth-value; on the other hand, trust characterises a relationship in which a recipient (a reader) recognises that a source is able to provide credible information ([Sahut and Tricot, 2017](#sah17)).

The trust readers put in Wikipedia has been widely studied ([Okoli, Mehdi, Mesgari, Nielsen and Lanamäki, 2014](#oko14)). A specific focus has been given to the analysis of attitudes amongst young people. This focus is justified because of the importance assumed by the collaborative encyclopaedia in youth information-seeking practices ([Flanagin and Metzger, 2010](#fla10); [Judd and Kennedy, 2010;](#jud10) [Knight and Pryke, 2012](#kni12)) and because of the numerous questions posed on the reliability of Wikipedia. Young people are also usually considered to be less equipped than adults to deal with information credibility problems because of their lower cognitive development and limited life experience ([Eastin, 2008](#eas08); [Flanagin and Metzger, 2008](#fla08)).

Wikipedia is a source of information used by the vast majority of young people. However, the issue of changes in perceptions of Wikipedia according to age has received little attention, in the same way as the issue of credibility judgments. The importance of the age factor in the development of such judgments is based on a theoretical point of view ([Eastin, 2008](#eas08)), but lacks empirical support.

The aim of our study is to address these issues. We describe and compare trust in Wikipedia for different age groups. To this end, we submitted the same questionnaire (on attitudes towards this source) to young people in France: 1) in secondary education (eleven to eighteen years old) and 2) in higher education (nineteen to twenty-five years old).

</section>

<section>

## Literature review

### Use of Wikipedia

Wikipedia is well-known even amongst the youngest. According to a study that involved a large sample of pupils aged eight to ten, Wikipedia is the Web resource they know the best and they use the most for learning at this age ([Luckin, _et al._, 2008)](#luc08). In the US, 99% of teenagers (eleven to eighteen years old) have heard of the encyclopaedia and 84% have already used it ([Flanagin and Metzger, 2010](#fla10)). At this age, information seeking on the collaborative encyclopaedia is a very common digital activity, only surpassed by viewing online videos, consulting digital social networks, and updating profiles on these media ([Flanagin and Metzger, 2010](#fla10)). At university, the vast majority of students consistently or frequently use Wikipedia for everyday life information seeking and for course–related research ([Head and Eisenberg, 2010](#hea10); [Kim, Sin and Yoo-Lee, 2014](#kim14)). As a recent survey underlined, for bachelor degree students in Australia, ‘Wikipedia is now an embedded feature’ ([Selwyn and Gorard, 2016](#sel16)) in the students’ academic tasks: two thirds of them consider it useful or very useful for their studies.

Using Wikipedia is frequently associated with using Google. Querying Google and following a link to a Wikipedia article has become common practice. This practice has been observed when considering _collège_ pupils (eleven to fifteen years old) in France ([Cordier, 2011](#cor11)), _lycée_ students (fifteen to eighteen years old) in Sweden ([Sundin and Francke, 2009](#sun09)) and university students in the US ([Lim, 2009](#lim09); [Menchen-Trevino and Hargittai, 2011](#men11); [Colón-Aguirre and Fleming-May, 2012](#col12)).

Using the collaborative encyclopaedia is therefore a common and widespread practice for young people of different ages.

### How do young people judge Wikipedia?

Frequent use of Wikipedia and positive assessments attributed to it are linked. The various studies on this topic show that Wikipedia is very useful for information seeking. Young people appreciate Wikipedia for many reasons that link to the following features:

*   Coverage: Wikipedia is likely to provide information on an extremely wide range of topics ([Head and Eisenberg, 2010](#hea10); [Shen, Cheung and Lee 2013](#she13); [Garrison, 2015](#gar15));
*   Currency: Wikipedia changes quicker than printed resources ([Head and Eisenberg, 2010](#hea10));
*   Comprehensibility: both high school pupils ([Sundin and Francke, 2009](#sun09)) and college students ([Head and Eisenberg, 2010](#hea10)) consider that Wikipedia corresponds to their level;
*   Ease of access and use: Wikipedia users frequently describe browsing as easy and the presentation of articles as particularly clear ([Luyt, Zainal, Mayo and Yun, 2008](#luy08); [Head and Eisenberg, 2010](#hea10); [Shen _et al._, 2013](#she13); [Garrison, 2015](#gar15)). Convenience seems particularly appropriate to describe Wikipedia ([Biddix, Chung and Park, 2011](#bid11); [Watson, 2014](#wat14));
*   Good starting point: secondary school students ([Watson, 2014](#wat14)) and undergraduate students ([Lim, 2009](#lim09); [Head and Eisenberg, 2010](#hea10); [Biddix _et al._, 2011](#bid11); [Kim _et al._, 2014](#kim14); [Garrison, 2015](#gar15); [Selwyn and Gorard, 2016](#sel16)) perceive Wikipedia as a way of getting background and general information on a topic.

However, these results do not allow us to reach a consensus on the degree of trust that young people put in the collaborative encyclopaedia. The fact that Wikipedia often appears in the first page of search engines results ([Hochstötter and Lewandowski, 2009](#hoc09)) may be viewed by young people as a guarantee of credibility, even authority, because younger users show great confidence in search engine ranking ([Hargittai, Fullerton, Menchen-Trevino and Yates-Thomas, 2010](#har10)). Shen _et al_. ([2013](#she13)) demonstrate that trust in Wikipedia correlates with the perception of its usefulness for accomplishing academic tasks; moreover, positive experiences using this source explain why the students continue to use it.

Other works conclude that behaviour is much more differentiated regarding the encyclopaedia. A survey on eleven to eighteen year olds reported that 43% show limited trust in Wikipedia articles whilst almost a third rely on it ([Flanagin and Metzger, 2010](#fla10)). Several studies point out that trust in the encyclopaedia is neither optimum for high school students nor for university students ([Lim, 2009](#lim09); [Head and Eisenberg, 2010](#hea10); [Menchen-Trevino and Hargittai, 2011](#men11); [Watson, 2014](#wat14); [Georgas, 2014](#geo14); [Garrison, 2015](#gar15)). Among high school students, Julien and Barker ([2009](#jul09)) noticed that the use of Wikipedia generates ‘an uneasy tension’. Students often use Wikipedia, but, paradoxically, some of them do not recognise it as being a valid source of information.

Flanagin and Metzger ([2011](#fla11)) used a quasi-experimental method to compare trust in the Britannica, Wikipedia and Citizendium sources, showing that the identity of the encyclopaedic source is a central component of the credibility judgments made by adults and teenagers. The same information will be considered more credible if it appears in the Britannica than if it is found in Wikipedia. However, adults pay greater attention to the article content when assessing it, judging articles from Wikipedia to be as credible as those from the Britannica and higher under this criterion than those from Citizendium. Conversely, teenagers rely less on the informational content to make their credibility judgment and are more sceptical than adults with regard to Wikipedia. Hence, they show their commitment to the traditional editorial model characterised by the reassuring display of recognised expertise ([Flanagin and Metzger, 2011](#fla11)).

</section>

<section>

### Equivocal reputation of Wikipedia

The reputation of a source plays an important role in its evaluation and selection, specifically for young people ([Liu, 2004](#liu04); [Flanagin and Metzger, 2008](#fla08); [Watson, 2014](#wat14)). This is an important factor for Wikipedia because, unlike other sources, it is subject to widespread criticism in news media. According to Reagle ([2010](#rea10)), this led to an ‘_encyclopaedic anxiety’_ because the open and collaborative writing calls into question the very foundations of cognitive authority. In France, since 2007, some teachers, academics and journalists have issued many criticisms on the lack of reliability of Wikipedia as detailed by Sahut ([2015](#sah15)). These criticisms were echoed in academic circles. Teachers in secondary and higher education make extensive use of the encyclopaedia while trying to discourage or even prohibit its use to pupils or students. Various studies show this phenomenon in different countries: Sweden ([Francke and Sundin, 2012](#fra12)), the UK ([Knight and Pryke, 2012](#kni12)), the USA ([Purcell, Heaps, Buchanan and Friedrich, 2013](#pur13)), and France ([Ladage and Ravenstein, 2013](#lad13)).

Indeed, university teachers’ negative opinions about the collaborative encyclopaedia are primarily based on its perceived unreliability, the depreciation of the open and collaborative editing and the exclusive use of the encyclopaedia for information seeking when other more legitimate documents or sources could be used. Finally, its plagiarism in academic work and publication is also denounced. Therefore, it is not surprising that high school and university students are aware of Wikipedia’s bad reputation amongst teachers ([Luyt _et al._, 2008](#luy08); [Watson, 2014](#wat14); [Todorinova, 2015](#tod15)). It has been shown that the influence of teachers has an impact on student ratings and the use of Wikipedia at high school ([Sundin and Francke, 2009](#sun09)) and university ([Garrison, 2015](#gar15)). However, teachers’ opinions on Wikipedia do not converge. The majority of them are certainly negative, but a minority also hold a favourable opinion on using Wikipedia for tasks that are related to studies, which has been noted in, for example, Sweden ([Francke and Sundin, 2012](#fra12)) and the UK ([Knight and Pryke, 2012](#kni12)).

As has been demonstrated in studies from Singapore ([Luyt _et al._, 2008](#luy08)), the US ([Lim and Simon, 2011](#lim11)) and Canada ([Chung, 2012](#chu12)) young people are also sensitive to the opinions of their peers, mostly positive, with regard to the encyclopaedia and therefore, their relation to the encyclopaedia seems problematic. Students are uncertain about the variety of ways to use information sources like Wikipedia ([Garrison, 2015](#gar15)). Some young people conceal their use by not making references in homework and written texts that are going to be assessed ([Sundin and Francke, 2009](#sun09); [Head and Eisenberg, 2010](#hea10); [Lim and Simon, 2011](#lim11)). This suggests that the trust given to Wikipedia by young people is not only due to an appreciation of the intrinsic value of the encyclopaedia, but also to the judgment, expressed or supposed, that their teachers have come to about the source.

</section>

<section>

### Theoretical framework

According to some cognitive information seeking models ([Pirolli and Card, 1999](#pir99); [Fu and Gray, 2006](#fu06); [Pirolli, 2007](#pir07)), users evaluate a source of information based on a benefit-to-cost ratio. The benefit is the perceived information usefulness of the source. Costs represent physical, cognitive, and time-consuming efforts to search, query, and use the source. The minimisation of costs is a widespread trend among information seekers ([Jansen and Rieh, 2010](#jan10)), particularly among pupils and students ([Agosto, 2002](#ago02); [Connaway, Dickey and Radford, 2011](#con11)).

Whilst assessing sources can be considered as rational, this rationality is indeed limited ([Vera and Simon, 1993](#ver93)). Users make choices without considering all possible informational options. Their cognitive limitations and the material and temporal conditions under which information seeking takes place, mean that they do not choose the best sources, but rather those that they know, that are accessible and that they think are acceptable in their context.

The importance attached to epistemic trust is integrated in such a benefit to cost ratio. Referring to unreliable sources involves a risk and uncertainty about the benefit associated with obtaining the information. Individuals may therefore be aware of the need to reduce these risks by endeavoring to locate sources deemed reliable.

Trust in a source depends on multiple factors ([Kelton _et al._, 2008](#kel08)). We focus on two of them that appear central when studying Wikipedia:

*   Trust in a source is built through individual experience. Throughout their lives, individuals develop a catalogue of sources they trust, based on their past uses ([Wilson, 1983](#wil83); [Kelton _et al._, 2008](#kel08)). A source that regularly delivers credible information gains the trust of its users ([Tricot, Sahut and Lemarié, 2016](#tri16)). For young people, the experience of Wikipedia seems to be an important variable because, as seen above, the vast majority of young people make regular use of the collaborative encyclopedia.
*   However, epistemic trust does not depend solely on an individual relation to a source. Social factors play an important role, including the reputation of the source. We define the reputation of a source as the perception of opinion, positive or negative, on its shared information value at the level of a social group.

As social epistemology has shown ([Wilson, 1983](#wil83); [Origgi, 2012](#ori12)), trust in a source is the means to understand the information environment. It is thus a factor to reduce uncertainty on the value of the sources. Opinions on a source are constructed through exchanges in social networks (in the sociological sense of the term). Opinions can come from a variety of social actors and be conveyed by all types of communication channels (word of mouth, mass media, and so forth). Nevertheless, not all opinions play an equivalent role in building the reputation of a source. Opinions of those who are recognised as authorities in a sphere exert a strong influence on general opinion. In his seminal essay on cognitive authority, Wilson ([1983](#wil83)) evokes the phenomenon of transfer of authority. If a person I consider a cognitive authority recommends me a source, then I can trust that source. This variable has to be taken into account because, as the state of the question has shown, teachers who are supposed to be cognitive authorities for pupils seem to have a predominantly negative view of Wikipedia and are therefore likely to influence them.

</section>

<section>

## Research questions

In this paper, we analyse trust in Wikipedia by considering a large range of education levels, from _collège_ (youngest pupils aged eleven to twelve) to master’s degree level (majority aged twenty-two to twenty-five). Our study focuses on (1) the perception young people have of Wikipedia; (2) the influence peers and teachers have on a young person’s opinions of Wikipedia; and throughout the paper (3) the variation with the level of education.

### Young people’s perceptions of Wikipedia

A first set of research questions relates to the perceptions young people have of Wikipedia. The literature review shows that Wikipedia is seen as a particularly useful source, easily accessible and simple to use. However, there is no consensus on how young people trust Wikipedia. Based on these results from the literature, we investigate young people’s experience in using Wikipedia. Indeed, it emerges that the perception of past use of a source affects a person’s trust ([Rieh, 2002](#rie02); [Kelton _et al._, 2008](#kel08)) and positive experiences with an information source increase the credibility given to new information from the same source ([Lucassen and Schraagen, 2012](#luc12)). To analyse if the level of education correlates with users’ experience of a source, it is appropriate to consider the possible variations of trust in Wikipedia according to the level of education. This topic has been largely ignored in the literature. It is also important to consider the nature of the information seeking task, and how the importance of source quality for the young can increase for academic tasks ([Gross and Latham, 2009](#gro09)). The lack of variation on trust observed by Flanagin and Metzger ([2010](#fla10)) among eleven to eighteen year-olds requires in-depth analysis.

We thus consider the following research questions:

Question set 1\.  
How do young people judge their Wikipedia experience?  
How much do young people trust Wikipedia?  
Does the level of trust vary depending on the type of information seeking tasks?  
Is trust in Wikipedia linked to trust in the encyclopedia genre?  
Does trust in Wikipedia change with the level of education?

</section>

<section>

### Influence of the reputation of Wikipedia

The literature review shows that the use of Wikipedia, as well as its reputation in academic tasks, is often perceived as problematic. We therefore investigated whether young people are influenced by the opinions of others on the collaborative encyclopaedia, and if so, where these opinions came from. We paid particular attention to the way young people perceive their teachers’ attitudes with regard to the encyclopaedia. Indeed, when they recommend a source, teachers transfer some of their own cognitive authority to this source ([Sundin and Francke, 2009](#sun09)). As current research demonstrates, the judgments made by academics regarding Wikipedia are mostly negatively oriented, although positive opinions also exist. Again, we think the variable education level should be analysed. The advance in the curriculum may involve variations in Wikipedia’s reputation in relation to changes in teachers’ requirements regarding sources to be used for academic tasks.

We thus consider the following research questions:

Question set 2\.  
What is Wikipedia’s reputation among young people?  
Where do the positive and negative opinions on Wikipedia come from?  
Does Wikipedia’s reputation change with the level of education?

</section>

<section>

## Research method

### Design of the questionnaire survey

In our case study, a questionnaire was more appropriate for collecting data than a qualitative approach, as we want to describe the major and minor variations in young people’s perceptions of Wikipedia. Moreover, the very fact that we analysed responses to standardised questions from a sample of participants lends itself to the comparison of the responses of different population groups. In this paper, we compare the results according to the variable level of education.

We designed our questionnaire to collect several categories of empirical data. It consists of:

*   Questions related to the socio-demographic details of the participants (gender, age, school, level);
*   Questions related to use. We limited ourselves to questions related to the frequency of use of the encyclopaedia. A questionnaire cannot be a means to obtain a precise view on people’s use of Wikipedia because what individuals say they do and what they actually do can differ ([Kim and Sin, 2011](#kim11));
*   Questions about the representations or perceptions of Wikipedia. We want to understand the diversity of descriptions about Wikipedia and more specifically, the trust participants have in Wikipedia for information seeking tasks in different contexts. We query pupils and students concerning their behaviour with regard to Wikipedia in relation to in-class assignments and in relation to searches they conduct outside the academic framework, for example searches about video games, pieces of music, TV series. We asked about the students’ trust in printed encyclopaedias to compare it with the trust they give to Wikipedia. We also asked participants about the opinions they think other people have of the encyclopaedia (teachers, peers, etc.). We believe it is important to know about the criteria young people say they refer to in order to judge their trust in Wikipedia articles. To this end, the questionnaire also included open questions on users’ perceptions of Wikipedia and trust in this source.

A pre-test with the target population proved to be valuable to check the operational dimension of the questionnaire. One of the difficulties of this questionnaire was to be sure that questions could be understood by the youngest participants. To this end, the pre-test was first sent to the youngest targeted population (pupils aged eleven to fourteen). A first analysis of their comments, as well as comments from the librarians who supervised, helped us develop the final questionnaire. We tried to make all the questions easily understood by using a simpler vocabulary and appropriate formulations.

</section>

<section>

### Population

We diversified the population of participants by sending the questionnaire to schools with different sociological characteristics. The questionnaire was distributed in France. For readers who are more familiar with US and UK systems than the French system, Table 1 provides the equivalence between the three countries’ systems, even if we are aware of other differences in other countries. Moreover, to better reflect the survey we keep the French educational system throughout the analysis.

<table><caption>Table 1: Population according to the level of education. We split the population according to the French system. We provide the equivalence in the different educational systems although there is no complete overlap</caption>

<tbody>

<tr>

<th>Level: French, UK  
and US systems</th>

<th>No.</th>

<th>%</th>

<th>Level: French, UK  
and US systems</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>

FR: 6e  
UK: Year 7 class  
US: 6th grade  
</td>

<td>56</td>

<td>6.7</td>

<td rowspan="4">

France: Collège (11-15 year old)  
UK: Pupils in Year 7 to 10  
US: 6th to 9th grade students</td>

<td rowspan="4">256</td>

<td rowspan="4">30.4</td>

</tr>

<tr>

<td>

FR: 5e  
UK: Year 8 class  
US: 7th grade</td>

<td>82</td>

<td>9.8</td>

</tr>

<tr>

<td>

FR: 4e  
UK: Year 9 class  
US: 8th grade</td>

<td>59</td>

<td>7.0</td>

</tr>

<tr>

<td>

FR: 3e  
UK: Year 10 class  
US: 9th grade</td>

<td>59</td>

<td>7.0</td>

</tr>

<tr>

<td>

FR: Seconde  
UK: Year 11 class  
US : 10th grade</td>

<td>71</td>

<td>8.4</td>

<td rowspan="3">

France: _Lycée_ (16-18 year old)  
UK: Students in Year 11 to 13  
US:10th to 12th grade students</td>

<td rowspan="3">265</td>

<td rowspan="3">31.5</td>

</tr>

<tr>

<td>

FR: Première  
UK: Year 12 class  
US: 11th grade</td>

<td>43</td>

<td>5.1</td>

</tr>

<tr>

<td>

FR: Terminale  
UK: Year 13 class  
US : 12th grade</td>

<td>151</td>

<td>18.0</td>

</tr>

<tr>

<td>

FR: Bac +1  
UK/US : 1st year Bachelor degree</td>

<td>60</td>

<td>7.1</td>

<td rowspan="3">France: DUT, BTS, Licence  
UK/US: Bachelor’s degree</td>

<td rowspan="3">148</td>

<td rowspan="3">17.6</td>

</tr>

<tr>

<td>

FR: Bac +2  
UK/US : 2nd year Bachelor degree</td>

<td>36</td>

<td>4.3</td>

</tr>

<tr>

<td>

FR: Bac+3  
UK/US : 3rd year Bachelor degree</td>

<td>52</td>

<td>6.2</td>

</tr>

<tr>

<td>Bac+4  
Master 1</td>

<td>94</td>

<td>11.2</td>

<td rowspan="2">Master’s degree</td>

<td rowspan="2">172</td>

<td rowspan="2">20.5</td>

</tr>

<tr>

<td>Bac+5  
Master 2</td>

<td>78</td>

<td>9.3</td>

</tr>

<tr>

<td>Total</td>

<td>841</td>

<td>100.0</td>

<td>Total</td>

<td>841</td>

<td>100.0</td>

</tr>

</tbody>

</table>

We selected:

*   Six _collèges_ (_collège_ studies in France last four years and pupils are usually aged from 11-12 to 14-15);
*   Four _lycées_ (_lycée_ studies in France last three years and pupils are usually aged from 15-16 to 17-18);
*   With regard to higher education, respondents were from different public universities.

Note that the objective of this study is not to compare views on Wikipedia amongst students attending courses in different fields, but we wanted a large panel of pupils and students.

In the end, we obtained responses from 841 young people including 54.1% from female participants at different education levels. As reported in Table 1, the participants are almost equally distributed according to the following levels of education: _Collège_ (4 years long), _Lycée_ (3 years long) and University (3+2 years long).

### Conditions for delivering the questionnaire

The questionnaire was available online in 2012\. In _collèges_ and _lycées_ (secondary level), teachers and librarians supervised the completion of the questionnaire in the computer room or library. The questionnaire has been completed by the full classes. In higher education, the students, in most cases, were invited by email to participate in the survey. In the latter case, the response rate is estimated to be 15%.

</section>

<section>

### Reducing bias

One of the challenges of the survey is to obtain honest opinions about Wikipedia whilst being aware of its controversial reputation. As researchers we had to face a reporting bias induced by the phenomenon of social desirability. Indeed, on opinions or socially unacceptable acts, a respondent is likely to favour an answer that is consistent with the expectations of his or her social group ([Grimm, 2010](#gri10)).

A self-administered online questionnaire has the advantage of greatly limiting the social desirability bias because of the lack of a direct relationship between the interviewer and interviewees ([McBurney, 1994](#mcb94)). It offers the opportunity to ask questions about sensitive topics; respondents are less likely to give a favourable or consistent image of them-selves.

### Statistical analysis of the collected data

We used descriptive statistics to summarise the collected data (e.g. frequency of the different modalities over participants or participant groups, average on responses). We also used ANOVA (analysis of variance). Variance is a measure of dispersion, in other words, it enables us to estimate the heterogeneity or, conversely, the homogeneity of a series of values ([Howell, 2002](#how02)). ANOVA is used to examine whether groups of individuals, here pupils and students at various education levels, have characteristics which are significantly different from others.

In accordance with accepted conventions in the scientific community, in this paper, we retain a p-value lower than 0.05 (corresponding to an error rate of less than 5%) as a significant threshold.

</section>

<section>

## Results and findings

### Wikipedia experience

#### Frequency of use

While the majority of respondents state that they use Wikipedia either monthly, weekly or almost daily (see Figure 1), two respondent categories give different answers (i) participants who report they systematically use Wikipedia for seeking for information, but do not indicate a precise frequency (9.1%) and (ii) those who declare not to use the encyclopaedia (5.7%). In the latter case, the collected data shows that there are two distinct profiles of non-Wikipedia users:

1.  young people, mostly _collège_ pupils, who do not have computer access at home (2% of respondents but 4.7% of _collège_ pupils) or an internet connection from home (3.6% of respondents but 7.8% of collège pupils). We do not know if these young people’s parents are among the strong-minded digitally disconnected, or if the lack of equipment is because of economic reasons. The fact that our sample includes a collège with a predominantly disadvantaged population makes us lean toward the second hypothesis, and
2.  the young people who are resistant to Wikipedia, and who have consistently made very negative comments about this in response to other questions in the questionnaire.

<figure>

![Figure 1: Frequency of use of Wikipedia](../p783fig1.jpg)

<figcaption>Figure 1: Frequency of use of Wikipedia</figcaption>

</figure>

To reflect in a more accurate way the differences according to education levels, we have used ANOVA (See Table 2). It shows that _lycée_ pupils reported a higher frequency of use than _collège_ pupils (p <0.02 *). The use of Wikipedia is significantly higher at university level (p <0.001 ***) but without significant differences between the bachelor’s and master’s students (p >0.05).

<table><caption>Table 2: Frequency of use of Wikipedia - ANOVA and p-values</caption>

<tbody>

<tr>

<th>Level</th>

<th>Difference  
(mean)</th>

<th>p-value</th>

</tr>

<tr>

<td>Collège Lycée  
Bachelor  
Master</td>

<td>

-0.302*  
-0.864*  
-0.745*</td>

<td>

0.0198  
1.051E-11  
5.550E-10</td>

</tr>

<tr>

<td>Lycée Collège  
Bachelor  
Master</td>

<td>

0.302*  
-0.562*  
-0.443*</td>

<td>

0.0198  
0.00002  
0.0005</td>

</tr>

<tr>

<td>Bachelor Collège  
Lycée  
Master</td>

<td>

0.864*  
0.562*  
0.119</td>

<td>

1.051E-11  
0.00002  
0.7868</td>

</tr>

<tr>

<td>Master Collège  
Lycée  
Bachelor</td>

<td>

0.745*  
0.443*  
-0.119</td>

<td>

5.550E-10  
0.0005  
0.7868</td>

</tr>

</tbody>

</table>

If we exclude the minority of resistant participants, which is about 5.7%, Wikipedia is seen as a useful (48.1%) or very useful (46.3%) source by all respondents. _Collège_ and _lycée_ pupils have similar perceptions on this point (ANOVA,

0.05), but we note significant differences between them and bachelor’s students (ANOVA collège/bachelor’s p <0.001 ***, ANOVA _lycée_/Bachelor’s p <0.001 ***). The latter consider Wikipedia as more useful. Master’s students do not differ from bachelor’s students (ANOVA bachelor’s/master’s p >0.05), nor from _lycée_ (ANOVA p >0.05). However, there is a significant difference between master’s students and _collège_ pupils (ANOVA p <0.001 ***).

</section>

<section>

#### Qualifying the Wikipedia documentary experience

When young people consider their past experience on Wikipedia use, they mostly provide positive judgments about the quality of information available on Wikipedia, whatever their education level. The major point of view is that most often the collaborative encyclopaedia enables them to access information they qualify as useful (93%), understandable (92%), and accurate (92.7%). Indeed, in open questions about the perception of Wikipedia, only one of the 841 respondents reported that he found mistakes in Wikipedia.

We do not include all the ANOVA results in this paper, but rather focus on the most interesting features we found using it. _Collège_ and _lycée_ pupils, bachelor’s and master’s students report a similar image of their experience on the encyclopaedia according to the information usefulness and accuracy criteria (ANOVA, p >0.05). The only notable difference is the comprehensibility qualifier: _lycée_ pupils are significantly different from bachelor’s students (ANOVA p <0.001 ***) and Master’s students (ANOVA p <0.01 **) which, after all, is not surprising given the relative complexity of some Wikipedia articles.

</section>

<section>

### Trust in Wikipedia

Trust in Wikipedia varies depending on task and education level (see Figure 2a). It is interesting to note that collège pupils report higher trust in Wikipedia for academic tasks than for tasks related to leisure. This is no longer the case for higher levels of education and in particular for master’s students, where the gap between the two levels of trust is the highest.

<table><caption>Figure 2: Levels of trust in Wikipedia (Scale 1 (weak) to 4 (high))</caption>

<tbody>

<tr>

<td>

<figure>

![Trust according to information-seeking task](../p783fig2a.jpg)

<figcaption>a) according to information-seeking task</figcaption>

</figure>

</td>

<td>

<figure>

![Trust compared to printed encyclopaedias](../p783fig2b.jpg)

<figcaption>b) Compared to printed encyclopaedias for an information search requested by a teacher</figcaption>

</figure>

</td>

</tr>

</tbody>

</table>

In our survey, the _collège_ pupils differ significantly from the _lycée_ pupils regarding trust for academic information seeking (ANOVA, p <0.001 ***), but not for information seeking related to leisure (ANOVA, p >0.05). The _lycée_ pupils are no different from the bachelor’s students when considering trust for academic tasks (ANOVA,

0.05). Master’s students show a much more pronounced mistrust than _lycée_ pupils (ANOVA, p <0.01 **) and bachelor’s students (ANOVA, p <0.001 ***). For information seeking related to leisure, _lycée_ pupils are more cautious than undergraduates (ANOVA, p <0, 05), but we do not see significant differences between _lycée_ and master’s level (ANOVA, p >0.05). According to the reported data, we see a proven effect on the information seeking type of task for trust in Wikipedia. We will discuss this result later in the paper.

We also wondered whether the decrease in academic trust in Wikipedia could be because of a form of denigration of the encyclopaedic genre itself. The students, including at master’s level, might be likely to find such a document too simplistic compared to other specialised and scientific sources. This is actually not the case (See Figure 2b). We can see in Figure 2b that the _collège_ pupils have a significant specific behaviour regarding their trust in printed encyclopaedias for academic tasks compared to the other levels (ANOVA, p <0.001 ***). The difference in trust between printed encyclopaedias and Wikipedia is particularly high for master’s students. _Lycée_ pupils and university students have a specific distrust in the collaborative encyclopaedia but not in the encyclopaedic genre itself.

</section>

<section>

### Wikipedia’s reputation

#### Sources of opinions on Wikipedia

Our survey shows that Wikipedia is subject to social discussions in media, family, friends, or educational circles. A majority of the young people (67.9%) report having heard or read some negative reviews about Wikipedia (See Figure 3). Unsurprisingly, they focus on its unreliability; 41.2% of respondents state that they had heard such a review. In contrast, four out of ten state that they had heard positive reviews about Wikipedia. Again, convenience of use (24.4%) and completeness (11.4%) are the most frequently mentioned positive qualifications. The participants also report the origin of these opinions (see Figure 3).

<figure>

![Figure 3: Origin of the negative and positive opinions about Wikipedia](../p783fig3.png)

<figcaption>Figure 3: Origin of the negative and positive opinions about Wikipedia</figcaption>

</figure>

The negative comments about the encyclopaedia are mostly attributed to teachers (see Figure 3). On average, more than one out of two participants (53.8%) say they have heard one of their teachers criticising Wikipedia. Such opinions are less frequently encountered in the friend and family environment. The origin of positive opinions about the encyclopaedia has a greater distribution (see Figure 3). More generally, young people seem to face contrasting opinions in family, friend, and even media circles. Indeed, according to respondents, the negative judgments come from the teaching environment, but almost a fifth of respondents report positive evaluations from their teachers. Moreover, when asked about the position of their teachers with respect to Wikipedia, young people are divided ([Sahut, 2014](#sah14)). According to a majority of participants, their teachers generally have a poor opinion of Wikipedia (16% think their teachers have a very negative opinion of Wikipedia and 42% a negative one), but the proportion of the opposite opinion is not negligible (6% think their teachers have a very positive opinion and 36% a positive one).

According to descriptive statistics, the higher the education level, the more participants attribute to their teachers a negative image of the collaborative encyclopaedia (see Figure 4). While only 31.7% of _collège_ pupils feel that their teachers’ opinion about Wikipedia is low (or very bad), 67.2% of _lycée_ pupils, 68.2% of bachelor’s students and 76.7% of master’s students answer this way. ANOVA confirmed the gap between collège pupils (p <0.001 ***) and other young people.

<figure>

![Figure 4: Proportion of teachers who have a negative opinion of Wikipedia according to the survey participants](../p783fig4.png)

<figcaption>Figure 4: Proportion of teachers who have a negative opinion of Wikipedia according to the survey participants</figcaption>

</figure>

</section>

<section>

#### Wikipedia citation

Doubts about Wikipedia’s reputation are evident if one examines participants’ responses with regard to citations in their school or university work (see Figure 5). In Figure 5, we excluded collège pupil responses since writing a bibliography does not seem to be a widespread practice at this level of education. The comments associated with this question confirmed that some collège pupils did not understand what it meant. For the other participants, only a minority (18%) declare that they systematically cite Wikipedia, 50.3% say they refuse to cite it, and 31.7% say it depends on the teacher. Master’s students differ significantly from lycée pupils and bachelor’s students (ANOVA, p <0.001 *** in both cases) because they are more reluctant to cite the collaborative encyclopaedia.

<figure>

![Figure 5: Wikipedia citations in academic work](../p783fig5.png)

<figcaption>Figure 5: Wikipedia citations in academic work</figcaption>

</figure>

</section>

<section>

### Summary of the results

In this section, we summarise the results we found and try to answer our research questions.

Regarding [question set 1](#qs1), the frequency of Wikipedia use is more important at university (students aged at least eighteen) than at _collège_ or _lycée_ (pupils aged from 11/12 to 17/18). The experience of using Wikipedia as a source is judged predominantly as positive in terms of usefulness, accuracy, and information comprehensibility. The perception of usefulness and accuracy do not vary with the education level. For comprehensibility, undergraduate students have more favourable judgments than _collège_ or _lycée_ pupils.

We found that trust in Wikipedia depends on the type of information seeking tasks and on the education level. The _collège_ pupils put more trust in Wikipedia when information seeking is prescribed by teachers, rather than for leisure reasons. The _lycée_ pupils have a higher distrust in the encyclopaedia for prescribed tasks. As for Master’s level students, trust in Wikipedia is significantly higher for information searches related to leisure than for prescribed searches. For academic information seeking, this variation does not seem to be because of a loss of interest in the encyclopaedic genre. Indeed, Master’s students, as well as _lycée_ pupils and undergraduate students, say they place higher trust in printed encyclopaedias than in Wikipedia.

Concerning [question set 2](#qs2), our study confirms that Wikipedia is subject to contrasting social judgments. Some young people say they have heard positive statements about this source that originate from their peers. However, the majority of them also report negative opinions that mostly emanate from their teachers. At _lycée_, young people think that their teachers have rather poor opinions of Wikipedia. The reluctance to cite the collaborative encyclopaedia in academic work is particularly high at master’s level.

</section>

<section>

## Discussion and conclusions

### Variation of trust and the reputation of Wikipedia

We identified significant variations of trust in Wikipedia according to the level of education. For young people aged eleven to eighteen, our results differ from those of Flanagin and Metzger ([2010](#fla10)). They found no variation of trust in Wikipedia according to a participant’s age, while we found a significant difference between _collège_ (eleven to fifteen year old pupils) and _lycée_ (sixteen to eighteen year old pupils). These results could be explained by the information seeking task considered. In our own research, we made a distinction between trust for academic tasks and trust for leisure tasks. It is only on the academic type of information seeking that the _lycée_ pupils say they place less trust in Wikipedia than _collège_ pupils, not when they search for non-academic tasks. This reinforces the idea that trust in an information source is contextual and depends on the type of intended use, as suggested by other theoretical related work ([Wilson, 1983](#wil83); [Kelton _et al._, 2008](#kel08); [Choi and Stvilia, 2015](#cho15)). When the search is prescribed, information seeking is integrated within a communication situation where teachers are both recipients and evaluators of the work. Pupils or students must build a representation of a teacher’s expectations on the nature of the sources that they can use and hence the documentary acceptability of Wikipedia.

The perception of printed encyclopaedias and Wikipedia for information seeking, as prescribed by teachers, has been analysed in detail. It is from lycée that trust in printed encyclopaedias is clearly higher. Moreover, we also analysed the trust on other printed sources, such as magazines and journals in school libraries. Our results show that from lycée level onwards, there is a suspicion about Wikipedia credibility and pupils and students tend to consider that printed sources are more reliable than digital sources (details can be found in ([Sahut, 2014](#sah14))). This is consistent with some other studies that involve high school students ([Sundin and Francke, 2009](#sun09); [Watson, 2014](#wat14)). In our work, we note that this conclusion is also reflected strongly at university level.

We also observe that like the students interviewed by Lim ([2009](#lim09)), the vast majority of respondents positively assess their Wikipedia experience in terms of usefulness or accuracy. We have established that this perception is shared by collège pupils, lycée pupils, undergraduate students, and postgraduate students. Closed questions and open questions consistently demonstrate positive experiences when using Wikipedia. Considering solely past experience using this source, users perceive a high benefit to costs ratio. Unreliability of Wikipedia for academic tasks, discernible from _lycée_, thus does not come from past experience, since it is judged favourably, but rather for the large part from social discourses.

However, these social discourses do not converge. Those from responder peers (other young people) are more positively oriented. This result is consistent with what Lim and Simon ([2011](#lim11)) and Chung ([2012](#chu12)) observed. These authors noted that supportive discourses about Wikipedia exist among undergraduate students. However, at _lycée_, young people mostly think teachers distrust or are hostile to Wikipedia, even if these opinions are not unanimous. Using and citing Wikipedia for school tasks seems to generate uncertainty and not only because of possible inaccuracies. There is also the risk of being penalised because of possible teachers’ negative opinions about this source. From an overview of our results, we can say that throughout their schooling, young people seem to become gradually aware of the predominantly negative academic reputation of Wikipedia. Only a minority of _collège_ pupils seem to perceive it.

At the university levels, it is interesting to note the contrast between a higher frequency of use of Wikipedia and a lower rate of citation of this source. This observation is particularly true at master’s level. The majority of students at this level appear to have integrated the academic standards required for teachers to accept a source. In this case, the benefit to costs ratio is perceived as an issue. From _lycée_ level there is a trade-off between the ease of use of Wikipedia and the doubt on the benefits of using it in academic tasks. The positive experiences young people accumulate while using Wikipedia have not raised it to social recognition in the educational sphere. From this perspective, the collaborative encyclopaedia has not, yet, reached the level of knowledge institution. The average trust attributed to Wikipedia for academic tasks could be induced by the tension between the negative academic reputation of the encyclopaedia, especially noticeable from _lycée_ pupils and the mostly positive experience of its credibility by students.

</section>

<section>

### Further research

In this study we have observed how young people’s perceptions of Wikipedia are different as they progress through the curriculum. Not all young people attend higher education establishments. To generalise the conclusions we draw from this survey for all young people, it would be necessary to conduct an additional study where the effect of age and the education level would be considered separately and then together. This would include a means for estimating the weight of the influence of teachers’ views and university attendance on trust in Wikipedia.

The data we collected using a questionnaire has helped to capture the differences in perceptions and opinions of Wikipedia. However, the data has not enabled us to precisely identify the behaviour of young people. There may indeed be a gap between the actual behaviour of young people and the behaviour they declared, notably when considering information assessment ([Kim and Sin, 2011](#kim11)). Our study therefore could be complemented by observing the actual use of Wikipedia by _collège_ pupils, _lycée_ pupils, undergraduates, and master’s students.

More generally, our survey demonstrates significant differences between the levels of education, both for Wikipedia use and its representation. This variable should be included in studies related to information behaviour by the young to avoid generalisations that deny the disparities between the ages. However, we would like to highlight that in our own study it may have been appropriate to consider two other factors: sex and social background; we leave this for future work.

Sex differences are not studied in this paper. However, we analysed this in another publication ([Sahut, 2016](#sah16)). We found that girls declared that they trust Wikipedia less than boys, both for information searches related to school work and recreation in high school and bachelor's degree. On the other hand, at the college and master level, no significant differences were found for these variables by sex.

We collected data during 2012, and unlike longitudinal studies, our research provides an image that is reflected by a given population at a given time. However, trust in a source is built over time. Can Wikipedia last in the documentary landscape? Will, mostly, positive users’ experiences in terms of information credibility turn into a more widely shared trust capital? Will Wikipedia’s academic reputation positively evolve to the point of it becoming an authority recognized by knowledge institutions? A future study could be to propose the questionnaire we used in this survey to a similar sample of young people in five or ten years’ time. We could make chronological comparisons which take into account the different variables, to understand the possible changes in the perceptions and trust in Wikipedia.

This study focuses on Wikipedia, however the methodology we applied could be enlarged to consider other user-generate content in social media which are more and more used and for which the trustworthiness of news found is an open issue ([Fuhr _et al._, 2017](#fuh17)).

</section>

<section>

## Acknowledgements

We would like to thank the ESPE de l’Académie de Toulouse, France for their financial support of this research through the _recherche collaborative_ project. We would like to thanks Benoît Jeunier and André Tricot for their valuable comments on this research.

## <a id="author"></a>About the authors

**Josiane Mothe** is full Professor in computer science at the ESPE, Université de Toulouse and researcher at UMR5505 CNRS IRIT. She is a specialist in information retrieval, data mining and big data. From 2004 to 2014, she was the editor in chief for Europe and Africa of the international Information Retrieval Journal, (Springer). The general topic of her research is information retrieval from textual information, either semi-structured or unstructured. She can be contacted at [josiane.mothe@irit.fr](mailto:josiane.mothe@irit.fr)  
**Gilles Sahut** holds a PhD in Information and Communication Sciences. He is a researcher at Laboratoire d’Études et de Recherches Appliquées en Sciences Sociales. He is also a teacher trainer at ESPE, Université de Toulouse. His research focuses on the evaluation of various sources of information by young people and more specifically Wikipedia as well as media and information literacy. He is also interested in the document theories. He can be contacted at [gsahut@univ-tlse2.fr](mailto:gsahut@univ-tlse2.fr)

</section>

<section>

## References

<ul>

<li id="ago02">Agosto, D.E. (2002). Bounded rationality and satisficing in young people’s Web-based decision making. <em>Journal of the American Society for Information Science &amp; Technology, 53</em>(1), 16-27.</li>

<li id="bid11">Biddix, J. P., Chung J. C. &amp; Park, H. W. (2011). Convenience or credibility? A study of college student online research behaviors. <em>The Internet and Higher Education, 14</em>(3), 175-182.</li>

<li id="cho15">Choi, W. &amp; Stvilia, B. (2015). Web credibility assessment: conceptualization, operationalization, variability, and models. <em>Journal of the Association for Information Science and Technology, 66</em>(12), 2399–2414.</li>

<li id="chu12">Chung, S. (2012). Cognitive and social factors affecting the use of Wikipedia and information seeking. <em>Canadian Journal of Learning and Technology, 38</em>(3), 1-20.</li>

<li id="col12">Colón-Aguirre, M.,&amp; Fleming-May, R. A. (2012). "You just type in what you are looking for": undergraduates’ use of library resources vs. Wikipedia. <em>The Journal of Academic Librarianship, 38</em>(6), 391-399.</li>

<li id="con11">Connaway, L.S., Dickey, T.J., &amp; Radford, M.L. (2011). If it is too inconvenient, I'm not going after it: convenience as a critical factor in information-seeking behaviors. <em>Library &amp; Information Science Research, 33</em>(3), 179-190.

</li><li id="cor11">Cordier, A. (2011). <em><a href="https://tel.archives-ouvertes.fr/tel-00737637/document">Imaginaires, représentations, pratiques formelles et non formelles de la recherche d’information sur internet: le cas d’élèves de 6ème et de professeurs documentalistes.</a></em> [Imaginations, representations, formal and non-formal practices of the search for information on the internet: the case of 6th grade students and documentation teachers]. Unpublished doctoral dissertation, Université Charles de Gaulle Lille III, Villeneuve d'Ascq, France. Retrieved from  https://tel.archives-ouvertes.fr/tel-00737637/document  (Archived by WebCite&reg; at http://www.webcitation.org/6x6IpU77I)</li>

<li id="eas08">Eastin, M. S. (2008). Toward a cognitive development approach to youth perceptions of credibility. In M.J. Metzger &amp; A.J. Flanagin (Eds.), <em>Digital media, youth, and credibility</em> (pp. 29-48). Cambridge, MA: The MIT Press.</li>

<li id="fla08">Flanagin, A. J. &amp; Metzger, M. J. (2008). Digital media and youth: unparalleled opportunity and unprecedented responsibility. In M.J. Metzger &amp; A.J. Flanagin (Eds.), <em>Digital media, youth, and credibility</em> (pp. 5-27). Cambridge, MA: The MIT Press.

</li><li id="fla10">Flanagin, A. J. &amp; Metzger, M. J. (2010). <em>Kids and credibility: an empirical examination of youth, digital media use, and information credibility.</em> Cambridge, MA: The MIT Press.

</li><li id="fla11">Flanagin, A. J. &amp; Metzger, M. J. (2011). From Encyclopaedia Britannica to Wikipedia: generational differences in the perceived credibility of online encyclopedia information. <em>Information, Communication &amp; Society, 14</em>(3), 355-374.

</li><li id="fra12">Francke, H. &amp; Sundin, O. (2012). Negotiating the role of sources: educators’ conceptions of credibility in participatory media. <em>Library &amp; Information Science Research, 34</em>(3), 169-175.</li>

<li id="fu06">Fu, W. T. &amp; Gray, W. D. (2006). Suboptimal tradeoffs in information seeking. <em>Cognitive Psychology, 52</em>(3), 195-242.

</li><li id="fuh17">Fuhr, N., Giachanou, A., Grefenstette, G., Gurevych, I., Hanselowski, A., Jarvelin, K., … Stein, B. (2017). An information nutritional label for online documents. <em>ACM SIGIR Forum, 51</em>(3), 46-66</li> 

<li id="gar15">Garrison, J. C. (2015). <a href="http://www.webcitation.org/6x6JZVjdc">Getting a "quick fix": first-year college students’ use of Wikipedia.</a> <em>First Monday, 20</em>(10).  Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/5401  (Archived by WebCite&reg; at http://www.webcitation.org/6x6JZVjdc)</li>

<li id="geo14">Georgas, H. (2014). Google vs. the Library (Part II): student search patterns and behaviors when using Google and a federated search tool. <em>Portal: Libraries and the Academy, 14</em>(4), 503-532.</li>

<li id="gri10">Grimm P. (2010). Social desirability bias. In <em>Wiley international encyclopedia of marketing.</em> London: John Wiley and Sons. </li>

<li id="gro09">Gross, M. &amp; Latham, D. (2009). Undergraduate perceptions of information literacy: defining, attaining and self-assessing skills. <em>College and Research Libraries, 70</em>(4), 336-350.</li>

<li id="har10">Hargittai, E., Fullerton, L., Menchen-Trevino, E. &amp; Yates-Thomas, K. (2010). <em>Trust online: young adults’ evaluation of Web content</em>. <em>International Journal of Communication, 4,</em> 468-494. Retrieved from http://ijoc.org/index.php/ijoc/article/view/636/423  (Archived by WebCite&reg; at http://www.webcitation.org/6x6Jve8k2)</li>

<li id="hea10">Head, A. J. &amp; Eisenberg, M. B. (2010). How today’s college students use Wikipedia for course-related research. First Monday, 15(3). Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/2830/2476  (Archived by WebCite&reg; at http://www.webcitation.org/6x6K7uScJ)</li>

<li id="hoc09">Hochstötter, N. &amp; Lewandowski, D. (2009). What users see - structures in search engine results pages. <em>Information Sciences, 179</em>(12), 1796-1812.</li>

<li id="how02">Howell, D. C. (2002). <em>Statistical methods for psychology</em> (5th. ed.). Pacific Grove, CA: Duxbury/Thomson Learning.</li>

<li id="jan10">Jansen, B.J. &amp; Rieh, S.Y. (2010). The seventeen theoretical constructs of information searching and information retrieval. <em>Journal of the American Society for Information Science &amp; Technology, 61</em>(8), 1517-1534.</li>

<li id="jud10">Judd, T. &amp; Kennedy, G. (2010). A five year study of on-campus Internet use by undergraduate biomedical students. <em>Computers &amp; Education, 55</em>(4), 1564-1571.</li>

<li id="jul09">Julien, H. &amp; Barker, S. (2009). How high-school students find and evaluate scientific information: a basis for information literacy skills development. <em>Library &amp; Information Science Research, 31</em>(1), 12-17.</li>

<li id="kel08">Kelton, K., Fleischmann, K. R. &amp; Wallace, W. A. (2008). Trust in digital information. <em>Journal of the American Society for Information Science and Technology, 59</em>(3), 363-374.</li>

<li id="kim11">Kim, K. S. &amp; Sin, S. C. J. (2011). Selecting quality sources: bridging the gap between the perception and use of information sources. <em>Journal of Information Science, 37</em>(2), 178-188.</li>

<li id="kim14">Kim, K. S., Sin, S. C. J. &amp; Yoo-Lee, E. (2014). Undergraduates’ use of social media as information sources. <em>College and Research Libraries, 75</em>(4), 442-457.</li>

<li id="kni12">Knight, C. &amp; Pryke, S. (2012). Wikipedia and the University, a case study. <em>Teaching in Higher Education, 17</em>(6), 649-659.</li>

<li id="lad13">Ladage, C. &amp; Ravestein, J. (2013). Internet et enseignants: entre contrastes et clivages: enquête auprès d’enseignants du secondaire. [Internet and teachers: between contrasts and cleavages: survey of secondary school teachers] <em>Sciences et Technologies de l'Information et de la Communication
pour l'Éducation et la Formation, 20</em>.   Retrieved from http://sticef.univ-lemans.fr/num/vol2013/01-ladage/sticef_2013_ladage_01p.pdf  (Archived by WebCite&reg; at http://www.webcitation.org/6x6KeFHCF)</li>

<li id="lim09">Lim, S. (2009). How and why do college students use Wikipedia? <em>Journal of the American Society for Information Science and Technology, 60</em>(11), 2189-2202.</li>

<li id="lim11">Lim, S. &amp; Simon, C. (2011). Credibility judgment and verification behavior of college students concerning Wikipedia. <em>First Monday 16</em>(4).  Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/3263/2860  (Archived by WebCite&reg; at http://www.webcitation.org/6x6KqdIjh)</li>

<li id="liu04">Liu, Z. (2004). Perceptions of credibility of scholarly information on the Web. <em>Information Processing &amp; Management, 40</em>(6), 1027-1038.</li>

<li id="luc12">Lucassen, T. &amp; Schraagen, J. M. (2012). Propensity to trust and the influence of source and medium cues in credibility evaluation. <em>Journal of Information Science, 38</em>(6), 566-577.</li>

<li id="luc08">Luckin, R., Logan, K., Clark, W., Graber, R., Oliver, M. &amp; Mee, A. (2008). <em>Learners’ use of Web 2.0 technologies in and out of school in Key Stages 3 and 4</em>. Coventry, UK: Becta.</li>

<li id="luy08">Luyt, B., Zainal, C., Mayo, O. &amp; Yun, T. (2008). Young people’s perceptions and usage of Wikipedia. <em>Information Research, 13</em>(4), paper 377.  Retrieved from http://www.informationr.net/ir/13-4/paper377.html  (Archived by WebCite&reg; at http://www.webcitation.org/6x6L8u9bh)</li>

<li id="mcb94">McBurney D.H. (1994). <em>Research methods.</em> Pacific Grove, CA: Brooks/Cole.</li>

<li id="men11">Menchen-Trevino, E. &amp; Hargittai, E. (2011). Young adults’ credibility assessment of Wikipedia. <em>Information, Communication &amp; Society, 14</em>(1), 24-51.</li>

<li id="oko14">Okoli, C., Mehdi, M., Mesgari, M., Nielsen, F. Å. &amp; Lanamäki, A. (2014). Wikipedia in the eyes of its beholders: a systematic review of scholarly research on Wikipedia readers and readership. <em>Journal of the American Society for Information Science and Technology, 65</em>(12), 2381-2403.</li>

<li id="ori12">Origgi, G. (2012). A social epistemology of reputation. <em>Social Epistemology, 26</em>(3-4), 399-418.</li>

<li id="pir07">Pirolli, P. (2007). <em>Information foraging theory: adaptive interaction with information</em>. Oxford: Oxford University Press.</li>

<li id="pir99">Pirolli, P. &amp; Card, S. (1999). Information foraging. <em>Psychological Review, 106</em>(4), 643–675.

</li><li id="pur13">Purcell, K., Heaps, A., Buchanan, J. &amp; Friedrich, L. (2013). <em>How teachers are using technology at home and in their classrooms</em>. Washington, DC: Pew Research Center’s Internet &amp; American Life Project. </li>

<li id="rea10">Reagle, J. M. (2010). <em>Good faith collaboration: the culture of Wikipedia.</em> Cambridge, MA: The MIT Press.</li>

<li id="rie02">Rieh, S. Y. (2002). Judgment of information quality and cognitive authority in the Web. <em>Journal of the American Society for Information Science and Technology, 53</em>(2), 145-161.</li>

<li id="rie10">Rieh, S. Y. (2010). Credibility and cognitive authority of information. In <em>Encyclopedia of Library and Information Sciences</em> (3rd. ed.). (pp. 1337-1344). London: Taylor &amp; Francis.</li>

<li id="sah14">Sahut, G. (2014). Les jeunes, leurs enseignants et Wikipédia: représentations en tension autour d’un objet documentaire singulier. [Young people, their teachers and Wikipedia: representations in tension around a singular documentary object] <em>Documentaliste-Sciences de l’information, 52</em>(2), 70-79.</li>

<li id="sah15">Sahut, G. (2015). <em>Wikipédia, une encyclopédie collaborative en quête de crédibilité: le référencement en questions</em>. [Wikipedia, a collaborative encyclopedia in search of credibility: referencing in questions] Unpublished doctoral dissertation, Université de Toulouse III, Toulouse, France. Retrieved from https://tel.archives-ouvertes.fr/tel-01257207/document   (Archived by WebCite&reg; at http://www.webcitation.org/6x6Lfkyce)</li>

<li id="sah16">Sahut, G. (2016). Les jeunes et Wikipédia: un rapport genré? [Young people and Wikipedia: a gendered relationship?] <em>Revue de recherches en littératie médiatique multimodale, 4</em>, 1-37. </li>

<li id="sah17">Sahut, G. &amp; Tricot, A. (2017). <a href="http://www.webcitation.org/6vsRvBG7j">Wikipedia: an opportunity to rethink the links between sources’ credibility, trust, and authority</a>. <em>First Monday, 22</em>(11). Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/7108/6555 (Archived by WebCite® at http://www.webcitation.org/6vsRvBG7j) 

</li><li id="sel16">Selwyn, N. &amp; Gorard, S. (2016). Students’ use of Wikipedia as an academic resource - patterns of use and perceptions of usefulness. <em>The Internet and Higher Education, 28</em>, 28-34.</li>

<li id="she13">Shen, X.-L., Cheung, C. M. K. &amp; Lee, M. K. O. (2013). What leads students to adopt information from Wikipedia? An empirical investigation into the role of trust and information usefulness. <em>British Journal of Educational Technology, 44</em>(3), 502-517.</li>

<li id="sun09">Sundin, O. &amp; Francke, H. (2009). In search of credibility: pupils’ information practices in learning environments. <em>Information Research, 14</em>(4), paper 418.  Retrieved from http://www.informationr.net/ir/14-4/paper418.html  (Archived by WebCite&reg; at http://www.webcitation.org/6x6MGHgMR)</li>

<li id="tod15">Todorinova, L. (2015). Wikipedia and undergraduate research trajectories. <em>New Library World, 116</em>(3/4), 201-212.</li>

<li id="tri16">Tricot, A., Sahut, G. &amp; Lemarié, J. (2016).<em>Le document: communication et mémoire</em>.  [The document: communication and memory]. Louvain-la-Neuve, Belgium: De Boeck Supérieur.</li>

<li id="tse99">Tseng H. &amp; Fogg B. J. (1999). Credibility and computing technology. <em>Communications of the ACM, 42</em>(5), 39-44.</li>

<li id="ver93">Vera, A.H. &amp; Simon, H.A. (1993). Situated action: a symbolic interpretation. <em>Cognitive Science, 17</em>(1), 7-48.</li>

<li id="wat14">Watson, C. (2014). An exploratory study of secondary students’ judgments of the relevance and reliability of information. <em>Journal of the Association for Information Science and Technology, 65</em>(7), 1385-1408.</li>

<li id="wil83">Wilson, P. (1983). <em>Second-hand knowledge: an inquiry into cognitive authority</em>. Westport, CT: Greenwood Press.</li>
</ul> 

</section>

</article>