#### vol. 15 no. 4, December, 2010

## Proceedings of the Seventh International Conference on Conceptions of Library and Information Science - "Unity in diversity" - Part 2

# The Cool and Belkin faceted classification of information interactions revisited

#### [Isto Huvila](#author)  
Department of Archive, Library and Museum Studies, Uppsala University, Box 625, 75126 Uppsala, Sweden

#### Abstract

> **Introduction.** The complexity of human information activity is a challenge for both practice and research in information sciences and information management. Literature presents a wealth of approaches to analytically structure and make sense of human information activity including a faceted classification model of information interactions published by Cool and Belkin in 2002, which is discussed in this paper.  
> **Method.** The Cool and Belkin classification scheme and the version adopted by Huvila in 2006 for a study of archaeologists are compared and new qualitative empirical interview material is analysed by using the latter version of the scheme.  
> **Analysis.** Literature studies and qualitative analysis of interview material was carried out.  
> **Results.** The discussed revisions proposed in the present study and earlier by Huvila broaden the scope of the classification system from information seeking to the broad scope of information activity and from description of interaction instances to their contexts.  
> **Conclusions.** It is important to consider the consequences of classifying information interactions, to balance between complexity and simplicity and to consider when it is significant to strive for complexity of classifications and when a generic level of description is enough.

## Introduction

The complexity of human information activity is a challenge for both practice and research in information sciences and information management. Literature presents a wealth of approaches to analytically structure and make sense of human information activity. The variety of approaches used to address the complexity include, for instance, the different models of information behaviour, (e.g., [Wilson, T.D. 1981](#wilson1981); [Ellis 1993](#ellis1993); [Foster 2004](#foster2004)), Sense-Making ([Dervin 1998](#dervin1998)), information search process (ISP) of Kuhlthau ([1993](#kuhlthau1993)) and various task based approaches ([Byström and Hansen 2005](#bystrom2005)). The classificatory approaches represent one further line of research. Belkin _et al._ ([1993](#belkin1993)) proposed an approach to classify information activity based on four dimensions. Later, Cool and Belkin published together a new, more elaborate faceted classification model ([Cool and Belkin 2002](#coolbelkin2002)). The value and utility of the particular model and similar approaches to structure information activity have been acknowledged in various information science research contexts (e.g. [Huvila 2006b](#huvila2006); [Yuan and Belkin 2007](#Yuan2007); [Wilson, M.L. and Schraefel 2007](#Wilson2007); [Heckner _et al._ 2009](#Heckner2009)). Classification schemes provide means to explicate human information activity in a form that is adequately structured for developing information retrieval, organisation and management. However, as Bowker and Star ([2000](#bowkerstar2000)) have demonstrated, no classification is without consequences. Therefore, it is essential to pursue for more advanced classification systems and same time to be aware of their consequences and the general consequences of classifying and modelling information interactions and the biases of different approaches.

In a study of information work and knowledge organisation in archaeology ([2006a](#huvila2006)), a modified version of the Cool and Belkin ([2002](#coolbelkin2002)) faceted classification of information interactions was assumed as a part of a new methodology (called information work analysis, [2008b](#Huvila2008d)) for analysing human information activity. The present paper discusses the principal differences between the original classification scheme and the version adapted in ([2006a](#huvila2006)) and, based on the analysis of new material, proposes an additional sub-facet, _Affordances_, to the scheme. Finally, this paper discusses the theoretical and practical consequences of the different schemes of faceted classification to the notion of information interaction, and the rationale and non-rationale of developing the faceted classifications schemes further.

## Faceted classification of information interactions

Information interaction is a multi-faceted, yet relatively new concept proposed as a notion that would grasp better the complexity of information related activity than its predecessors. Marchionini ([2004](#marchionini2004)) proposes information interaction as a replacement for information retrieval, because the first notion does '_better reflect the active roles of people and the dynamic nature of information objects in the electronic environment_'. Nahum Gershon coined the term in 1995 and defined it as '_how human beings interact with, relate to, and process information regardless of the medium connecting the two_' ([Morville 2005](#Morville2005a)). Toms ([2002](#toms2002)) sees information interaction as '_the process that people use in interacting with the content of an information system_'. The definitions are similar in that they emphasise reciprocity and embeddedness of different types of information activities, and the active and dynamic role of both people and information.

Since the seminal work of Ranganathan ([1963](#ranganathan1963)) on the _Colon Classification_, faceted approaches of knowledge organisation have been applied to books and other information organisation tasks including the classification of software components ([Prieto-Díaz 1991](#prieto-diaz1991)) and social practices of information use ([Cool & Belkin](#coolbelkin2002) 2002; [Widén-Wulff 2007](#widenwulff2007); [Abraham _et al._ 2007](#Abraham2007)). The strength of the faceted approaches has been suggested to be in that they represent a way to assess the structural integrity and architecture of a particular theory or context. A faceted approach can be used to underline major issues related to a process, phenomenon, or, for instance, behaviour ([Bowker & Star 2000](#bowkerstar2000); [Broughton 2006](#broughton2006)).

Cool and Belkin ([2002](#coolbelkin2002)) propose a faceted classification scheme for the categorisation of information interactions. The suggested approach is based on the scheme of four different dimensions: mode (recognition - specification), method (scanning - searching), goal (learning - selecting) and resource (information - meta-information). The scheme was developed on the basis of an earlier, simpler, model based on four dimensions proposed by Belkin _et al._ ([1993](#belkin1993)).

The context of the Cool and Belkin classification system is information seeking. Its explicit purpose is to identify, describe and classify a range of information seeking strategies in a group of knowledge-intensive workers. The aim of the authors was to inform the work on the subject of information retrieval by explicating the phases of the information interaction tasks ([Cool & Belkin 2002](#coolbelkin2002)). The principal utility of studying information behaviour ([Wilson, M. L. & Schraefel 2007](#Wilson2007)) and the need to support different types of behaviour have been acknowledged widely in information retrieval and information management (e.g., [Heckner _et al._ 2009](#Heckner2009)) research. In practice, however, information behaviour research has been criticised for being too descriptive and, especially, some of the earlier models of information behaviour, for being too simplistic ([Pharo 1999](#Pharo1999); [Belkin 2008](#Belkin2008)).

The Cool and Belkin scheme comprise five major facets and several sub-facets listed in Table 1\. The assumption is that any type of human information interaction should be representable by the different combinations of the facets and sub-facets of the proposed classification system.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Faceted classification scheme ([Cool & Belkin 2002](#coolbelkin2002)).**</caption>

<tbody>

<tr>

<th>Facets</th>

<th>Sub-facets</th>

</tr>

<tr>

<td rowspan="3">Communication behaviour</td>

<td>Medium: speech, text, video</td>

</tr>

<tr>

<td>Mode: face-to-face, mediated</td>

</tr>

<tr>

<td>Mapping: one-to one, one-to many, many-to-many</td>

</tr>

<tr>

<td>Information behaviour</td>

<td>Create, make decisions, disseminate, organise and preserve, access, evaluate, modifyand use</td>

</tr>

<tr>

<td rowspan="3">Objects interacted with</td>

<td>Level: information, meta-information</td>

</tr>

<tr>

<td>Medium: image, written text, speech</td>

</tr>

<tr>

<td>Quantity: one object, set of objects, database of objects</td>

</tr>

<tr>

<td rowspan="3">Common dimensions of interaction</td>

<td>Information object (part - whole)</td>

</tr>

<tr>

<td>Systematicity (random - systematic)</td>

</tr>

<tr>

<td>Degree (selective - exhaustive)</td>

</tr>

<tr>

<td>Interaction criteria</td>

<td>Accuracy, alphabet, authority, date, importance, person, time, topic</td>

</tr>

</tbody>

</table>

Since the original article by Cool and Belkin, the scheme has been used in several studies. The model has clearly showed its applicability in various contexts. Li ([2004](#li2004)) has developed the classificatory approach further to concern tasks on a more generic level. Balatsoukas _et al._ ([2009](#Balatsoukas2009)) have referred to the model in developing an evaluation framework for user interaction with metadata surrogates, Widén-Wulff ([2007](#widenwulff2007)) has used the model as a starting point for data gathering in a qualitative study of knowledge sharing, and Huvila ([2006a](#huvila2006)) has used the same model as an instrument of data analysis. The model has been criticised for complexity because of the high number of unique combinations of facets ([M. L. Wilson _et al._ 2009](#Wilson2009)), but also for its cursory presentation in the original article and for difficulties of keeping the different facets apart ([Huvila 2006b](#huvila2006)).

## The faceted classification in _The Ecology of Information Work_

The 2006 study of information work in archaeology conducted by the author ([Huvila 2006a](#huvila2006)) that is a starting point of this discussion together with the original Cool and Belkin ([2002](#coolbelkin2002)) study, analyses and classifies information activity of archaeology professionals from Finland and Sweden from an information management perspective. Theoretically, the study is based on soft systems theory ([1990](#checkland1990)) and Gibson's ([1979](#gibson1979)) ecological approach. It uses a modified version of the Cool and Belkin classification scheme in the analysis of how archaeologists interact with information in their work. The approach to the classification scheme differs from the original model in two respects. First, the scheme itself is modified slightly in order to specify the analysis of the criteria and obstacles of information interactions. The second difference is in the emphasis of the subject of the analysis, its viewpoint and results.

In contrast to Cool and Belkin ([2002](#coolbelkin2002)) who focus on information seeking, the 2006 study ([Huvila 2006a](#huvila2006)) places a special emphasis to accommodating the entire spectrum of information interactions in a systemic perspective. The approach assumes that besides reflecting the characteristics of explicit information needs and the consequent activity, the framework is applicable to the description of the dynamics of the interactions in general ([Huvila](#huvila2006), [2006a](#huvila2006)). Information interactions that are related classification criteria are discussed with a reference to the framework of the characteristics of information needs introduced by Line ([1969](#line1969)) and developed further by Nicholas and Martin ([1997](#nicholasmartin1997)).

The analysis of the study focuses on the classification of the interactions from an information point of view, a viewpoint that takes '_information as a cognisant starting point of investigating human activity_'and '_fundamental substrate, which is present in all human activity_' ([Huvila 2006a](#huvila2006): 19). Therefore. the related communication behaviour is deliberately omitted. The communication aspect is discussed from the perspective of the information object interactions. In contrast to omitting a facet, an additional sub-facet, _Obstacles_, is introduced to the _Interaction_ criteria facet in the study. It is stated that the classifications are used as descriptive devices of non-isolated particular actions and it is argued further that their contexts and motivations require further attention ([Huvila 2006a](#huvila2006): 125). The new sub-facet, _Obstacles_, is used to describe the principal barriers and hindrances related to interactions ([Huvila 2006a](#huvila2006): 125-126). The purpose of the sub-facet is to add analytical precision to the criteria of interactions in order to elaborate the dynamics, purposes, meanings and values of information interactions.

The adopted classification scheme (including the additional sub-facet _Obstacles_ and without communication criteria) is summarised in Table 2 using '_Give course_' interaction related to academic teaching work in archaeology as an example.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: An example of the faceted classification of the '_Give course_'information interaction related to the academic teaching work role of archaeologists ([Huvila 2006a](#huvila2006)).**</caption>

<tbody>

<tr>

<th>Facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

</tr>

<tr>

<td>Information behaviour: disseminate</td>

<td>Method: speak, (show, discuss)</td>

<td>Mode: Instruction</td>

<td> </td>

</tr>

<tr>

<td>Communication behaviour: (omitted in the present study)</td>

<td>Medium: —</td>

<td>Mode: —</td>

<td>Mapping: —</td>

</tr>

<tr>

<td>Objects interacted with</td>

<td>Level: information and meta-information</td>

<td>Medium: written text, speech, images</td>

<td>Quantity: set of objects</td>

</tr>

<tr>

<td>Common dimensions of information</td>

<td>Information object: part</td>

<td>Systematicity: systematic</td>

<td>Degree: selective</td>

</tr>

<tr>

<td>Interaction criteria: topic, authority</td>

<td>Obstacles: training, information overflow, time</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

In Huvila ([2006a](#huvila2006)), the analysis focuses, in accordance with the information management perspective of the study, on information interactions related to a motivation to accomplish the principal activity in which a person is engaged. Concurrent divergent and contrary motivations are treated as anomalies from the system perspective, albeit their overall significance to the human information work is acknowledged ([Huvila 2006a](#huvila2006): 126).

The goal orientation is apparent also in the use of the classificatory scheme. Even though the classifications are used to describe the interactions as the informants consulted in the empirical investigation had described them, the perspective of the classificatory analysis is focused by the theoretical underpinnings of the study ([Huvila 2006a](#huvila2006): 127). In practice, all classifications of information activities that have been identified as central to the information activity investigated in the study, are dependent on these analytical and theoretical assumptions. In practice, it would have been possible to classify same interactions from different perspectives, because an individual information interaction tends to involve interplay between multiple modes of behaviour. Organization of (available) information does usually precede the creation of new resources similarly to access preceding use and comprehension. Acknowledging this, the study is explicit in that it has not attempted to represent the entire spectrum of the diversity ([Huvila 2006a](#huvila2006): 127). On average one to two interactions have been selected as being central (i.e., more central than others) to the principal activities of the different types of archaeological work. The choice of focusing on a limited number of distinct interactions is based on an assessment of the primary ('primary work') purposes, values and meanings the work as explained in the study ([Huvila 2006a](#huvila2006)).

When the version of the classification scheme used in the 2006 study is examined critically, it is clear that the approach does not attempt to use or evaluate the entire classification scheme. It merely uses it in a particular context. Further, the results of the classification is not turned to into clear technical design recommendations and the actual new systems are left for future studies.

## Revision of the revised model

The version of the Cool and Belkin model used in the 2006 study was applied in a classification of information interactions of twelve professionals from Finnish and Swedish museum, library and archives sectors. The data were gathered by the author of this article in a series of focused interviews (average length 120 min) in four different sessions in 2009 using similar methodology to that of the earlier study ([Huvila 2006a](#huvila2006)) with a variation of interview techniques to elicit richer data and to increase the validity and trustworthiness of the information according to the criteria of Lincoln and Guba ([1985](#lincoln1985): 316-321). The informants were selected as an analytical sample for two other studies discussed in more detail in forthcoming publications. The initial data analysis was based on a combination of grounded theory ([Strauss 1987](#strauss1987); [Corbin & Strauss 1990](#corbin1990)) and schema based approaches ([Ryan & Bernard 2000](#ryan2000): 782-784) that was elaborated in the later stages using writing as an explicit form of enquiry ([Richardson 2000](#richardson2000)). Because of the limitations of space, the results of the classification are described here only when they have implications for the classification model. The Table 3 illustrates the classifications by showing a classification of an information organisation interaction of an archivist who is arranging an archive.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: An example of the faceted classification of the '_Arrange an archive_' information interaction of an archivist (Informant A1).**</caption>

<tbody>

<tr>

<th>Facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

</tr>

<tr>

<td>Information behaviour: organize</td>

<td>Method: restructuring</td>

<td>Mode: attaching</td>

<td> </td>

</tr>

<tr>

<td>Communication behaviour</td>

<td>Medium: text and (physical) order of things</td>

<td>Mode: mediated</td>

<td>Mapping: one-to-many</td>

</tr>

<tr>

<td>Objects interacted with</td>

<td>Level: information and meta-information</td>

<td>Medium: written text</td>

<td>Quantity: set of objects</td>

</tr>

<tr>

<td>Common dimensions of information</td>

<td>Information object: whole</td>

<td>Systematicity: systematic</td>

<td>Degree: selective</td>

</tr>

<tr>

<td>Interaction criteria: archival value, authority potential usefulness, capability to inform</td>

<td>Obstacles: training, information overflow, time</td>

<td>Affordances: familiarity of materials, existing order, experience</td>

<td> </td>

</tr>

</tbody>

</table>

Generally, the classification scheme worked in a satisfactory manner providing facets to describe the interactions in detail. The analysis of new data pointed out, however, two significant omissions in the model. Even though the Interaction criteria facet described reasons for interaction and the new _Obstacles_ sub-facet put more emphasis on the reasons and areas of failure, there was no way to describe the most likely reasons and areas of success. In this sense the approach was problem-centred rather than 'positive' in terms of Hartel _et al._ ([2009](#Hartel2009)). To address this issue, a new sub-facet _Affordances_ (following the notion of Gibson [1979](#gibson1979)) was introduced to the _Interaction_ criteria facet. Another detail related to the mapping sub-facet of the _Communication behaviour_ facet. Using social media, communication can be one-to-one, one-to-many, many-to-many, but also many-to-one in form of personalisation and aggregation of messages, e.g., posts, tags and comments, on Web-based social media services.

The revised version of the classification scheme with sub-facets and isolates based on the studies discussed in this contribution is presented in Table 4\. The spelling of isolates has been normalised and narrowly context specific wordings have been replaced with more general terms (e.g., material object vs. artefact). New sub-facets and isolates are printed in italics.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Revised model of faceted classification of information.**</caption>

<tbody>

<tr>

<th>Facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

</tr>

<tr>

<th>Information behaviour</th>

<th>Method</th>

<th>Mode</th>

<th> </th>

</tr>

<tr>

<td>Create</td>

<td>documenting, writing, drawing, filming, photographing, cognising, arguing</td>

<td>registering, compilation, combination, interpolation, induction, deduction, interpretation</td>

<td> </td>

</tr>

<tr>

<td>Disseminate</td>

<td>presenting, publishing, showing, discussing, corresponding</td>

<td>contextualisation, writing, speaking, drawing, instructing, informing, making available</td>

<td> </td>

</tr>

<tr>

<td>Organize</td>

<td>merging, comparing, rewriting, structuring restructuring</td>

<td>attaching, contextualisation, summarisation, simplification, compiling</td>

<td> </td>

</tr>

<tr>

<td>Preserve</td>

<td>managing</td>

<td>maintenance</td>

<td> </td>

</tr>

<tr>

<td>Access</td>

<td>scanning, searching, excavating</td>

<td>recognition, specification</td>

<td> </td>

</tr>

<tr>

<td>Evaluate</td>

<td>conforming, cross-checking, looking for references</td>

<td>recognition, comparison</td>

<td> </td>

</tr>

<tr>

<td>Comprehend</td>

<td>reading, listening, seeing, observing</td>

<td>contextualisation, recognition</td>

<td> </td>

</tr>

<tr>

<td>Modify</td>

<td>writing, drawing, filming, photographing, reproducing</td>

<td>contextualisation, clarification, update, replace</td>

<td> </td>

</tr>

<tr>

<td>Use</td>

<td>interpreting, applying</td>

<td>identity construction, contextualisation, adaptation</td>

<td> </td>

</tr>

<tr>

<td>Communication behaviour</td>

<td>Medium: speech, text, video</td>

<td>Mode: face-to-face, mediated</td>

<td>Mapping: one-to-one, one-to-many, many-to-many, many-to-one</td>

</tr>

<tr>

<td>Objects interacted with</td>

<td>Level: information, meta-information</td>

<td>Medium: image, written text, speech, archaeological stratum, physical sites, drawing, photograph, personal experience, material object, database record, map, sketch</td>

<td>Quantity: one object, set of objects, database of objects</td>

</tr>

<tr>

<td>Common dimensions of information</td>

<td>Information object: part-whole</td>

<td>Systematicity: random-systematic, sampling (random or systematic)</td>

<td>Degree: selective-exhaustive</td>

</tr>

<tr>

<td>Interaction criteria: accuracy, alphabet, authority, date, importance, person, time, topic, exhaustivity, potential, subject, person, research question, quality, call number, representativity, sensitivity of materials, contingency, interest, outlook, attractiveness, unusuality, knowledge potential, plausibility, functioning, applicability</td>

<td>Obstacles: information overflow, resources, time, training, appropriateness, quality, search tools, lack of meta-information, access, lack of information, resistance to change</td>

<td>Affordances: familiarity of objects, existing order, experience, context</td>

<td> </td>

</tr>

</tbody>

</table>

## Discussion

The revisions of the original Cool and Belkin classification of information interactions discussed in the present article have focused on two areas. The principal difference relates to the scope of the use of the faceted analysis. Cool and Belkin focus on information seeking, while the two empirical investigations discussed in this article place emphasis on the entire spectrum of information activity. In comparison to Marchionini ([2004](#marchionini2004)) and Toms ([2002](#toms2002)), however, the original Cool andBelkin classification and the two empirical studies discussed in this article all represent a rather narrow view of the notion of information interaction. It is also worthwhile noting that in all versions of the scheme, the facet titled as 'information behaviour' is more focused than the typical understanding of the concept _information behaviour_ (e.g., [Wilson, T.D. 2000](#wilson2000)). The principal purpose of the information behaviour facet in the classification of Cool and Belkin ([2002](#coolbelkin2002)) and the two later versions, is to explicate the various information (and in the 2006 study, communication) behavioural activities instead of the entirety of the information behaviour. The facet describes the essential explicit behaviour of an actor, but as a classificatory device, it aims rather to distil its constituent aspects than to cover its complexity. The same applies to the notion of information interaction. The dynamics and complexity are lost rather on purpose than by accident. From this perspective, an advantage of the original classification scheme proposed by Cool and Belkin, is that it may be re-purposed to cover a rather significant redefinitions of the concepts as the two studies discussed in the present article have shown.

The ecological soft systems oriented approach of the 2006 study and the present addition of _Affordance_ sub-facet draw special attention to the enablers and disenablers, i.e., the criteria and obstacles to information process and information interactions. The _Obstacles_ and, in the present study, the _Affordances_ sub-facets were incorporated in the scheme in order to make a distinction between the conditions and criteria (sub-facet _Criteria_), motivation and drivers (_Affordances_) and hindrances (_Obstacles_) of the discussed interactions. The addition of the _Obstacles_ and _Affordances_ sub-facets emphasise the contextual and quasi-evolutionary nature of information activity and interactions. The original Cool and Belkin ([2002](#coolbelkin2002)) model may be argued to have focused on an interaction as it is observed i.e. as an action that is consequential to an outcome of human activity. The original model also sees information interactions more as technical procedures than as contextual, dynamic and life world wide human activity.

Another dimension of the breadth or narrowness of classification schemes is their capability to explicate complexity. Toms ([2002](#toms2002)) criticised typical models of human-computer interaction for failing to express the complexity of information interactions. Even though the Cool and Belkin classification was proposed to encompass complexity ([Belkin 2008](#Belkin2008)), even it might be criticised for failing to do that. All classification systems simplify reality and reduce complex interactions to more simple constructs or complex series of simple transactions. A more viable question would be where to find the fine line between enough complexity and simplicity in each individual case. The revisions made to the model in the present do not reduce the complexity of the scheme, quite the opposite. The addition of many-to-one isolate to the _Mapping_ sub-facet of _Communication_ behaviour facet is an example of added, but simultaneously relevant form of complexity in the context of the present study. Moreover, the new sub-facets that reside under the _Interaction_ criteria facet that might already be criticised of being difficult to translate into detailed and exact instructions for the developers of information systems and knowledge organization schemes.

The issue of complexity is characterised by the fact that the Cool and Belkin model has been criticised for being too complex ([M. L. Wilson _et al._ 2009](#Wilson2009)) and too generic ([Huvila 2006b](#huvila2006)). The critique of the complexity of Cool and Belkin model is warranted _per se_, but in practice, most of the studies that have utilised the model, have tended to focus more closely on some facets instead of attempting to apply the entire model (e.g., [Huvila](#huvila2006), [2006a](#huvila2006); [Yuan & Belkin](#Yuan2007), [2007](#Yuan2007)). It is probable than in comprehensive studies of information interaction processes, the model may be indeed too extensive and a more radical reduction of complexity is needed. On the other hand, the model needs to be credited for the inclusion of major information activities mentioned only seldom in the different models of information behaviour such as use ([Spink & Cole 2006](#spink2006)), organisation and creation. Use has become a focus of wider interest only recently, organisation has been included in some models (e.g., [Barry 1997](#Barry1997b); [Hartel 2003](#Hartel2003)), but creation has been discussed in detail only in some individual studies (e.g., [Trace 2007](#trace2007); [Huvila 2008a](#Huvila2008c)).

One possible solution to the problem of complexity is to standardise the classification scheme on facet and sub-facet level and use descriptive context specific isolates especially with the Interaction criteria facet. As for now, the model is still rather vaguely defined as noted earlier by Huvila and Widén-Wulff ([2006b](#huvila2006)). It is, however, important to keep in mind that after standardisation and reduction of details, a classificatory scheme has still consequences. Both the study of 2006 and the present investigation of information interactions emphasise the fact that same interactions can be classified using many different isolates depending on the approach and theoretical underpinnings of the study. This is strength as well as a weakness of the particular scheme and classification in general. The emergence of closely related isolates together with omission of individual facets can be seen as a sign of contextual needs for both complexity and simplicity and, simultaneously, as a sign of that how small adjustments in viewpoint can make information interactions to appear as considerably different types of activities. For instance, the excavating isolate in Information Behaviour - Access (Table 4 and [Huvila 2006a](#huvila2006)) can be seen as an interaction that combines searching and scanning, but also as an access activity of its own right that is perhaps closest related to the notion of information encountering described by Erdelez ([1997](#erdelez1997)). It is similarly dependent on the viewpoint whether Evaluation by conforming (e.g., to guidelines), (internal) cross-checking and broader study of similar and quasi-similar reference cases are distinct activities or not (Table 4). The ambiguity of the classifications of individual information interactions may be seen as a problem and an indication of the frailty of the classification approach. Simultaneously, ambiguity may be seen, however, as a fundamental characteristic of information interactions and the faceted classification as a useful instrument for explicating the ambiguities.

Besides the internal ambiguity, another sign of the complexity and consequentiality of classifying information interactions is the wealth of different approaches to structuring individual interactions in other faceted classification models. Different models tend emphasise slightly different aspects of information interactions than the Cool and Belkin model. Abraham _et al._ ([2007](#Abraham2007)) emphasise search and selection as two distinct phases of access, and they split evaluation to deciding and digesting facets although the first one may be considered to incorporate features of the make decisions facet of the Cool and Belkin model. Even though a faceted classificatory approach can be argued to be a good middle ground between unstructured human information behaviour and structure needed in information systems development, the classification is always a trade-off between these two different viewpoints. To make the middle ground work, complexity has to be represented on a level that fits the purposes of explicating information interactions in each specific case. The need for introducing the _Obstacles_ and _Affordances_ sub-facets is not _per se_ an indication of deficiencies in the original classification system. Rather, it is an indication that the system is practicable, because, if needed, it is capable of accommodating new sub-facets and omission of existing elements.

It may be assumed that in different context there may be need for further adjustments, inclusion and re-mapping of existing isolates and possibly inclusion of additional sub-facets in order to focus and specify the analysis on the principal objects of interest. On the basis of the present observations, it may be argued though that many of the adjustments are likely to be reflecting specific rather than generic needs. In general, however, based on the two studies discussed in detail and earlier literature, the existing framework of facets and sub-facets seems to function as an applicable basis for structuring and conceptualising information interactions, and for creating more detailed descriptions on a lower level of classification.

## Classification of information interactions

The present study raises also an additional, more general question about the practicability and implications of classifying information interactions. Earlier literature (e.g., [Li 2004](#li2004); [Huvila 2006a](#huvila2006); [Widén-Wulff 2007](#widenwulff2007); [Balatsoukas _et al._ 2009](#Balatsoukas2009)) as well as the present study show that the faceted approach proposed by Cool and Belkin ([2002](#coolbelkin2002)) is usable per se and can be used as a framework for both data gathering and analysis of information activity. This may be taken as an indication of a practical usefulness of the approach and of a certain general relevance of the particular classification system.

The applicability of the approach in individual contexts raises, however, a question of the generalisability of the classifications and whether the approach can provide researchers and practitioners with material for comparisons between different domains and contexts. The fact that the present study and the earlier classification of archaeologists' information activity both gave reasons to introduce new isolates and sub-facets may be taken as an indication of the fundamental contextuality of the classificatory approach. This is hardly surprising in the light of the relativist theory of knowledge organisation and makes it reasonable to acknowledge that the development of the classification system by increasing specificity is unlikely to be a successful approach. Added complexity does not negate the constraints posed by contextuality of the classifications or interactions, but would certainly make the classification system more difficult to use.

In spite of the evident difficulties with contextuality, a pragmatist would argue that it would be useful to be able to classify and compare information interactions. The demonstrated usefulness of the Cool and Belkin classification system in multiple contexts indicates that the approach itself may be considered to have general relevance. A question remains whether this is enough for purely pragmatic comparisons and limited, domain-specific (following the approach of Hjørland and Albrechtsen [1995](#hjorland1995)), universality. Another question is that what level of abstraction is at the same time universal and specific enough to be practical comparative purposes.

On the basis of the present study, it seems that a level of practical specificity and universality of the faceted classification approach may be found on the level of facets and sub-facets. The number of new contextual isolates introduced in different sub-facets both in the study of archaeologists' information work and the empirical analysis of archivists' information work increases complexity more than they contribute to the general explanatory capability of the approach. The rationale to introduce new isolates stem from the contexts of the observed information interactions (what people are doing in practice). In contrast, the facets and sub-facets form a generic model of constituent aspects of information work. The principal context of the facets and sub-facets is a theory of information activity represented by the faceted classification system rather than a specific instance of information work.

Finally, the present study raises a question of the feasibility of developing and especially revisiting existing models of information activity. If the primary relevance of such models relate to the empirical context of their origin, the rationale of continued development of existing models may seem questionable. The observations made during the two studies discussed in this article suggest that revisiting a model is feasible if it serves an explicit theoretical or empirical purpose. Universalist aspirations of such projects can be highly problematic and counter-productive. The same reservations apply also to the approach of faceted classification. The strength of faceted approaches has been argued to be their capability to assess the structural integrity and architecture of a particular theory or context and to underline major issues related to a process or phenomenon ([Bowker & Star 2000](#bowkerstar2000); [Broughton 2006](#broughton2006)). The faceted approach advocates the importance of certain dimensions of information work instead of representing a true diversity of all pertinent aspects. Therefore, an increased complexity of classification makes the representation more complex, but only marginally more inclusive.

## Conclusions

The present study has underlined two noteworthy implications regarding the faceted classification of information interactions. First, the present investigation has supported earlier evidence ([Huvila 2006a](#huvila2006); [Li 2004](#li2004)) that the Cool and Belkin scheme is a viable instrument of analysis and provided evidence that the scheme is valuable even outside its original narrow scope of information seeking and retrieval. The scheme functions even in classification of other types of information interactions. The classification system makes, however, a clear distinction between information and communication behaviours implying that, from an information interaction point of view, communication becomes more emphasised than, for instance, organisation or use. Because of the flexibility of the faceted classification approach, it is possible to omit certain facets. The omission of facets, for instance, communication behaviours in ([Huvila 2006a](#huvila2006)) does not imply their meaningfulness. The possibility to adapt the classification system underlines the possibility to use a faceted classification scheme in a flexible manner. At the same time, the possibility to make adjustments emphasises the pertinence of being cognisant of assumed viewpoints during the classification process. Depending on the assumed point of view, the individual interactions may be classified in ameaningful manner as significantly different types of activities. An evident strength ofthe Cool and Belkin model is its capacity to accommodate to a variety of changes and adjustments.

The adjustments discussed in the present paper, broaden the classification scheme to embrace ecological aspects of information interactions. In contrast to the earlier, rather transient view of information interactions, the new obstacles and affordances sub-facets emphasise the role of motivations, and in the terms of Gibsonian ecological approach, affordances and constraints, which affect information interactions. The scope of the revised approach is broader and emphasises links to related human activity instead of merely focusing on the technical instance of information interaction. It is assumed that in different context there may be need for similar adjustments and inclusion of additional sub-facets in order to focus and specify the analysis on the principal objects of interest.

Besides the practical conclusions made on the classification scheme, the second implications of this study is the importance to consider the consequences of modelling and classifying information interactions. Because of the contextuality of models and classifications, a revisit or a redevelopment of a model should always have an explicit purpose. Classification is always a simplification and it represents a viewpoint. Consequently, models and classifications, whether faceted or not, impose a set of priorities on information interactions. In the end, probably the most interesting aspect of the faceted classification approach is not the classification system itself, but the insights into information interactions that may be gathered during the process of its development.

## About the author

Isto Huvila is a post-doctoral research fellow at the department of ALM (Archival, Library and Information, and Museum and Cultural Heritage Studies) at Uppsala University in Sweden and a member of the Library 2.0: A New Participatory Context research group at the Department of Information Studies at Åbo Akademi University in Turku, Finland. He received a MA degree in cultural history at the University of Turku in 2002 and a PhD degree in information studies at Åbo Akademi University in 2006\. He can be contacted at [isto.huvila@abm.uu.se](mailto:isto.huvila@abm.uu.se)

#### References

*   Abraham, A., Petre, M. & Sharp, H. (2007). _Information seeking and sensemaking for "personal fit"_. Paper presented at the ACM SIGCHI 2007 Workshop on Exploratory Search and HCI: Designing and Evaluating Interfaces to Support Exploratory Search Interaction, 29 April 2007, San Jose, CA, USA. /li>
*   Balatsoukas, P., Morris, A. & O'Brien, A. (2009). An evaluation framework of user interaction with metadata surrogates. _Journal of Information Science_, **35**(3), 321-339.
*   Barry, C. (1997). The research activity timeline: a qualitative tool for information research. _Library & Information Science Research_, **19**(2), 153-179.
*   Belkin, N.J. (2008). Some(what) grand challenges for information retrieval. _SIGIR Forum_, **42**(1), 47-54.
*   Belkin, N.J., Marchetti, P.G. & Cool, C. (1993). Braque: design of an interface to support user interaction in information retrieval. _Information Processing and Management_, **29**(3), 325-344.
*   Bowker, G. C. & Star, S. L. (2000). Sorting things out: classification and its consequences. Cambridge, MA, USA: MIT Press.
*   Broughton, V. (2006). The need for a faceted classification as the basis of all methods of information retrieval. _Aslib Proceedings_, **58**(1/2), 49-72.
*   Byström, K. & Hansen, P. (2005). Conceptual framework for tasks in information studies. _Journal of the American Society for Information Science and Technology_, **56**(10), 1050-1061.
*   Checkland, P. & Scholes, J. (1990). Soft systems methodology in action. New York, NY: John Wiley & Sons, Inc.
*   Cool, C. & Belkin, N. (2002). A classification of interactions with information. In H. Bruce (Ed.), _Emerging Frameworks and Methods: CoLIS4: proceedings of the Fourth International Conference on Conceptions of Library and Information Science, Seattle, WA, USA, July 21-25, 2002_, (pp. 1-15). Greenwood Village, CO: Libraries Unlimited.
*   Corbin, J. M. & Strauss, A. (1990). Grounded theory research: Procedures, canons, and evaluative criteria. _Qualitative Sociology_, **13**(1), 3-21.
*   Dervin, B. (1998). Sense-making theory and practice: an overview of user interests in knowledge seeking and use. _Journal of Knowledge Management_, **2**(2), 36-46.
*   Ellis, D. (1993). Modeling the information-seeking patterns of academic researchers. _Library Quarterly_, **63**(4), 469-486.
*   Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In P. Vakkari, R. Savolainen, & B. Dervin (Eds.), _ISIC '96: Proceedings of an international conference on Information seeking in context_, (pp. 125-146). London: Taylor Graham.
*   Foster, A. (2004). A nonlinear model of information-seeking behavior. _Journal of the American Society for Information Science and Technology_, **55**(3), 228-237.
*   Gibson, J. J. (1979). _The perception of the visual world_. New York, NY: Houghton Mifflin.
*   Hartel, J. (2003). The serious leisure frontier in library and information science: Hobby domains. _Knowledge organization_, **30**(3-4), 228-238.
*   Hartel, J., Kari, J. & Stebbins, R. (2009). Towards positive information science. _Proceedings of the American Society for Information Science and Technology_, 46(1), 1-5
*   Heckner, M., Heilemann, M. & Wolff, C. (2009). Personal information management vs. resource sharing: towards a model of information behavior in social tagging systems. In aper at the Third nternational Conference on Weblogs and Social Media, ICWSM-09, Nashville, Tennessee, US.
*   Hjørland, B. & Albrechtsen, H. (1995). Toward new horizon in information science: domain analysis. _Journal of American Society for Information Science_, **46**(6), 400-425.
*   Huvila, I. (2006a). The ecology of information work - a case study of bridging archaeological work and virtual reality based knowledge organisation. Åbo: Åbo Akademi University Press. (Dissertation of Åbo Akademi University)
*   Huvila, I. (2008a). Entä informaatiokirjoitustaito? (What about information writing literacy?) In _Reader- and user-oriented communication: national conference of communication studies 2007_, (pp. 22-27). Vaasa: University of Vaasa.
*   Huvila, I. (2008b). [Information work analysis: an approach to research on information interactions and information behaviour in context](http://www.webcitation.org/5uTwWAZZl). _Information Research_, 13(3). Retrieved 6 July, 2010 from http://informationr.net/ir/13-3/paper349.html (Archived by WebCite® at http://www.webcitation.org/5uTwWAZZl)
*   Kuhlthau, C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, **49**(4), 339-55.
*   Li, Y. (2004). Task type and a faceted classification of tasks. _Proceedings of the 67th ASIS&T annual meeting_, **41**
*   Lincoln, Y. S. & Guba, E. G. (1985). _Naturalistic inquiry_. Beverly Hills, CA: Sage Publications
*   Line, M.B. (1969). Information requirements in the social sciences: some preliminary considerations. _Journal of Librarianship_, **1**(1), 1-19.
*   Marchionini, G. (2004). From information retrieval to information interaction. In G. Goos, J. Hartmanis & J. van Leeuwen, (Eds.). _Advances in information retrieval. Proceedings of the 26th European Conference on IR Research, ECIR 2004, Sunderland, UK, April 5-7, 2004_. (pp. 1-11). Berlin: Springer. (Lecture notes in computer science, vol. 2997, pp. 1-11)
*   Morville, P. (2005). _Ambient findability_. Sebastopol, CA: O'Reilly.
*   Nicholas, D. & Martin, H. (1997). Assessing information needs: a case study of journalists. _Aslib Proceedings_, **49**(2), 43-52.
*   Pharo, N. (1999). Web information search strategies: a model for classifying web interaction. In T. Aparac, T. Saracevic, P. Ingwersen & P. Vakkari, (Eds.). _Proceedings of the 3rd International Conference on the Conceptions of the Library and Information Science_, (pp. 207-218). Zagreb: Zavod za informacijske studije Odsjeka za informacijske znanosti
*   Prieto-Díaz, R. (1991). Implementing faceted classification for software reuse. _Communications of the ACM_, **34**(5), 88-97.
*   Ranganathan, S. R. (1963). _Colon classification: basic classification_. (6th ed.). Bombay: Asia Publishing.
*   <richardson, l.="" (2000).="" handbook="" of="" qualitative="" research.="" in="" n.="" k.="" denzin="" &="" y.="" s.="" lincoln="" (eds.),="" (p.="" 923-948).="" thousand="" oaks,="" ca:="" sage="" publications.<="" li="">--></richardson,>
*   Ryan, G. W. & Bernard, H. R. (2000). Handbook of qualitative research. In N. K. Denzin & Y. S. Lincoln (Eds.), (pp. 769-802). Thousand Oaks, CA: Sage.
*   Spink, A. & Cole, C. (2006). Human information behavior: integrating diverse approaches and information use. _Journal of the American Society for Information Science and Technology_, **57**(1), 25-35.
*   Strauss, A. L. (1987). _Qualitative analysis for social scientists_. Cambridge: Cambridge University Press.
*   Toms, E. G. (2002). Information interaction: providing a framework for information architecture. _Journal of the American Society for Information Science and Technology_, _53_(10), 855-862.
*   Trace, C. B. (2007). Information creation and the notion of membership. _Journal of Documentation_, **63**(1), 142-164.
*   Widén-Wulff, G. (2007). _Challenges of knowledge sharing in practice: a social approach._ Oxford: Chandos.
*   Wilson, M. L., Schraefel, M. & White, R. W. (2009). Evaluating advanced search interfaces using established information-seeking models. _Journal of the American Society for Information Science and Technology_, **60**(7), 1407-1422.
*   Wilson, M. L. & Schraefel, M. C. (2007). [Bridging the gap: using IR models for evaluating exploratory search interfaces](http://eprints.ecs.soton.ac.uk/13135/). In SIGCHI 2007 Workshop on Exploratory Search and HCI. Retrieved 6 July, 2010 from http://eprints.ecs.soton.ac.uk/13135/
*   Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation_, **37**(1), 3-15.
*   Wilson, T.D. (2000). Human information behavior. _Informing Science_, **3**(2), 49-55.
*   Yuan, X. & Belkin, N. J. (2007). Supporting multiple information-seeking strategies in a single system framework. In _SIGIR '07: Proceedings of the 30th annual international ACM SIGIR Conference on Research and Development in Information Retrieval_, (pp. 247-254). New York, NY: ACM Press.