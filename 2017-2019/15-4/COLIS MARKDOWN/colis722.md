#### vol. 15 no. 4, December, 2010

## Proceedings of the Seventh International Conference on Conceptions of Library and Information Science - "Unity in diversity" - Part 2

# Studying information needs as question-negotiations in an educational context: a methodological comment

#### [Anna Lundh](#author)  
The Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS), The Swedish School of Library and Information Science, University of Borås and University of Gothenburg, SE-501 90 Borås

#### Abstract

> **Introduction.** The concept of information needs is significant within the field of Information Needs Seeking and Use. _How_ information needs can be studied empirically is however something that has been called into question. The main aim of this paper is to explore the methodological consequences of discursively oriented theories when studying information needs. The discussion takes its starting point in the model of information needs and question formations presented by Robert S. Taylor in the 1960s.  
> **Method.** The empirical material used for exemplifying the proposed methodology consists of video recordings from an ethnographical study conducted at a Swedish primary school.  
> **Analysis.** The analysis is guided by concepts based in sociocultural and dialogistic theories and is directed towards question-negotiations in conversations and interactions in naturalistic settings.  
> **Conclusions.** Earlier studies of information needs are often based on the idea of an individually constructed actual need as it is presented in the first level in Taylor's model. With an interest in the two latter levels of the model the analytical focus is shifted to question-negotiations which are seen as the results of situated discursive and collective efforts. This shift entails a methodological shift in the study of information needs.

## Introduction

The concept of information needs has been used in several models within library and information science, and especially in the field of information needs, seeking and use, to describe triggering factors for information seeking processes (e.g., [Wilson 1981](#tw81); [Krikelas 1983](#jk83); [Leckie _et al._ 1996](#gl96); [Byström & Hansen 2005](#kb05)). However, it has been acknowledged that information needs as such are difficult to study empirically and that most user studies focus on information seeking activities, rather than on actual information needs ([Wilson 1981](#tw81): 7; [Krikelas 1983](#jk83): 7f; [Case 2007](#dc07): 78).

The main purpose of this paper is to explore methodological consequences of discursively oriented theories when studying information needs. This will be done by employing concepts based in sociocultural and dialogistic theories in analyses of question-negotiations in a primary school setting. I will argue that the idea of information seeking processes as driven by actual, true, inner and individual information needs is not useful when studying information needs. As a basis for the discussion of how information needs have been and can be studied the model of question formations published in the 1960s by Taylor ([1962](#rt62); [1968](#rt68)) is presented. It will be shown how different theoretical lenses provide different foci on the four levels in Taylor's model which has methodological implications for the study of information needs and question-negotiations.

## Information needs and question-negotiations

Taylor's model of how individuals' information needs are manifested has been very influential since it was published in the 1960s ([Case 2007](#dc07): 72). It describes information needs or question formations as evolving in a four step process: from _the visceral need_ (Q<sub>1</sub>) which is also described as the _actual need_, through _the conscious need_ (Q<sub>2</sub>) where the user can formulate the need for him/herself, _the formalized need_ (Q<sub>3</sub>) which is the need verbally expressed and finally _the compromised need_ (Q<sub>4</sub>) which is the question formulated in a way suitable for an information system ([Taylor 1962](#rt62): 392).

Taylor's model takes its point of departure in the individual user and his/her actual need which might not be accordant with the need as it is presented to an intermediary or an information system. The model has been criticised for being too focussed on the role of the individual in the construction of information needs ([Hjørland 1997](#bh97)). Still, the model implies that a '_process of negotiating_' ([Taylor 1968](#rt68): 179) is unavoidable when a user is interacting with a librarian or a formal information system to satisfy his/her information needs. Hence, Taylor's model consists of two parts (see [Kloda and Barlett 2009](#lk09): 3f); on the one hand the individual user's original information needs which are described as originating from within the individual (Q<sub>1</sub> and Q<sub>2</sub>) and, on the other hand, the process where the individual has to (re)formulate his/her question in interaction with an intermediary or an information system, in other words, in a question-negotiating process (Q<sub>3</sub> and Q<sub>4</sub>).

In the following section this division between the individual's inner information needs and the social question-negotiating process will be used when discussing some of the literature on information needs published after Taylor's model was introduced. In several models and studies the two first levels in Taylor's model, that is, the individuals' recognition of his/her information need, are emphasized. When employing discursively oriented starting points, such as sociocultural and dialogistic perspectives, the latter two levels where the negotiations and formulations of questions take place, come into focus. The methodological implications of these different stances are described below and also summarised in table 1.

### Studying information needs and question-negotiations

Since the user-centred paradigm shift in the 1980s (e.g., [Talja and Hartel 2007](#stjh07)) a number of models have been published where information seeking is seen as originating from individuals' more or less conscious information needs. Three well-known examples are the Anomalous States of Knowledge (ASK) model ([Belkin _et al._ 1982a](#nb82a); [1982b](#nb82a)), the sense-making approach towards the study of information seeking and use ([Dervin 2003](#bd03)) and the Information Search Process (ISP) model (Kuhlthau [2004](#ck04); [2009](#ck09)). Even though the models diverge in terms of theoretical starting points and intended applications, they share common assumptions regarding how information needs can be described, understood and studied.

In the models, information needs are described as shortages experienced within the individual (cf. [Tuominen 1997](#kt97): 360f), as '_an inadequate state of knowledge_' ([Belkin i 1982a](#nb82a): 62), a gap or a stop on the journey of life ([Dervin 2003](#bd03): 277) or '_a gap in knowledge or a lack of understanding_' ([Kuhlthau 2009](#ck09)). Hence, they all build on the idea of an actual, inner need as described by Taylor in level Q<sub>1</sub>. With such a point of departure the individual user becomes the unit of analysis in studies of information needs. The aim of such studies is then to gain an understanding of individuals' experienced actual information needs. This understanding is supposed to be reached through analysing individual users' accounts of their thoughts, feelings and perceptions of their information needs as expressed in, for example, interviews and questionnaires.

The idea of the individual as a starting point in user studies, as well as the view of language as a tool for gaining access to users' inner lives have been called into question during the last decades from perspectives that can all be said to be discursively oriented (e.g., [McKenzie 2004](#pm04); [Talja _et al._ 2005](#st05); [Tuominen 1997](#kt97); [Sundin and Johannisson 2005](#os05); [Savolainen 2007](#rs07)). A central idea within discursively oriented traditions is that humans' use of language is a fundamental part of the construction of social reality ([Talja and McKenzie 2007](#stpm07): 98). This means that the unit of analysis in user studies shifts from the individual to the interaction between individuals. With such starting points the idea of information needs as something private and originating from within the individual is contested. Instead, information needs are seen as best understood and studied as emerging in interaction ([McKenzie 2004](#pm04)) or in other words as evolving in processes of negotiating expressed in levels Q<sub>3</sub> and Q<sub>4</sub> in Taylor's model.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1\. Methodological implications of different emphases on the four levels of Taylor's model of information needs and question formations.**  
</caption>

<tbody>

<tr>

<td> </td>

<th colspan="2">**Q<sub>1</sub>       →                      Q<sub>2</sub>                             →              Q<sub>3</sub>                   →                Q<sub>4</sub>**</th>

</tr>

<tr>

<td>**_Main concept_**</td>

<td align="center">Information needs</td>

<td align="center">Question-negotiations</td>

</tr>

<tr>

<td>**_Unit of analysis_**</td>

<td align="center">Information needs as emerging within the individual user</td>

<td align="center">Questions as emerging through social interaction and language use</td>

</tr>

<tr>

<td>_**Researcher's task**_</td>

<td align="center">The role of the researcher is to understand/diagnose the actual information needs</td>

<td align="center">The role of the researcher is to analyse how questions emerge in interaction</td>

</tr>

</tbody>

</table>

At this point there are still few studies that show on a more detailed level how question-negotiations may take place in naturalistic settings. Studies directed towards the latter two levels in Taylor's model require a methodological framework that encompasses possibilities to study question-negotiations in interaction. Such a framework, founded in sociocultural and dialogistic theories, will be presented in the following section.

### Studying question-negotiations from a sociocultural and dialogistic perspective

The sociocultural perspective employed in this paper is based on a school of thought that originates from the writings of the Russian psychologist Vygotsky (e.g., [1978](#lv78); [1997](#lv97)). It has contemporary representatives such as Wertsch (e.g., [1998](#jw98)) and Säljö (e.g., [2000](#rs00); [2005](#rs05)) within educational science; a closely related perspective is presented in Linell's dialogism within linguistics (e.g., [1998](#pl98); [2009](#pl09)). A basic assumption within this sociocultural perspective is that learning is a part of all human activities ([Säljö 2000](#rs00): 28; [Lave 1990](#jl90)) and, therefore, some of the core concepts and ideas are illuminating and useful when discussing and studying question-negotiations.

A central idea within these perspectives is that _cultural tools_ or _mediational means_ ([Vygotsky 1997](#lv97); [Wertsch 1998](#jw98): 17) need to be taken into account when human actions and practices are studied. Mediational means are created and used for overcoming both physical and cognitive limitations and can therefore be both material and/or intellectual. As humans we need to _appropriate_ or _master_ certain cultural tools to be able to function and participate in different social practices ([Wertsch 1998](#jw98): 53). One important (albeit not the only) shared tool for carrying out tasks in human practices is language. It is through the use of language that people can negotiate, act upon and thereby form questions; it is in interaction that questions materialise and become possible for researchers to study (cf. [Kloda and Barlett 2009](#lk09): 3f). Thus, when studying question-negotiations the focus becomes what Frohmann ([2004](#bf04): 394) describes as '_practices with words_'. Frohmann's expression refers to Wittgenstein's notion of _language games_ which is central in his later works (e.g., [1973](#lw73)). Questions are formed through the use of language, in interaction with others, and if we want to understand question-negotiating processes, we must study such interactions.

The notion of language games has been used in various ways; in this paper it is used to emphasise that language use takes place in specific practices where utterances are created and become comprehensible (cf. [Alexandersson and Limberg 2009](#ma09): 90). In sociocultural theory, the idea of _situatedness_ ([Säljö 2000](#rs00): 128-156; [Lave 1990](#jl90): 311f) implies that human action always is enacted in specific contexts and settings. This means that an understanding of the immediate context created in the interactions between people, as well as of the wider sociocultural and historical context forms the basis for analyses of human actions ([Linell 2009](#pl09)). An analysis of how questions are constructed in interaction therefore requires an understanding of when and where the interaction takes place, as well as of the roles of the people that are engaged in the interaction. The social practice described in the empirical examples in this paper is that of doing a certain type of assignment in a Swedish primary school where individuals are acting and negotiating in different roles (cf. [Linell 1998](#pl98): 74), e.g., pupils, teachers and librarians. Still, this does not necessarily mean that these roles are rigid and self-evident; rather, these roles and their positions are also negotiated and contested in the interactions.

Empirical studies based on the idea of question-negotiations as situated language games require tools for analysing social interaction and spoken language. A way of operationalising this idea is to employ concepts developed within dialogistic discourse analysis described by Linell ([1998](#pl98); [2009](#pl09)). Below, it will be shown how such an analysis may be carried out. The two empirical examples used will be described as _episodes_ where certain _topics_, in these cases certain questions, are in focus.

The two episodes can also be described as different _communicative projects_ carried out in the context of children seeking and using information in primary school. Linell ([Linell 1998](#pl98): 217ff; [2009](#pl09): 188ff) describes communicative projects as collective activities where the participants are trying to accomplish one or several goals through the use of language. Communicative projects can be of different kinds, one of the episodes presented below will be described as a specific _communicative genre_ used in _institutional discourse activities_. According to Linell ([2009](#pl09): 202; [1998](#pl98): 238ff) communicative genres are specific language games often employed within institutional settings for carrying out activities such as: "court trials, police interrogations, school lessons, academic seminars, doctor-patient interactions, social worker-client interactions, job interviews, speech therapy sessions" ([Linell 1998](#pl98): 240). In these kinds of institutional discourse activities the participants, or at least some of them, have an understanding of how such activities usually are accomplished. Still, all institutional discourse activities are not matters of routine in the same sense, as we will see in the first episode presented below.

## Introduction to the empirical examples

The situatedness of question-negotiations which is stressed in the framework presented above implies that question-negotiating processes may look different in different contexts. Therefore, some of the contextual characteristics of the setting in focus are described below. Also, the data collection methods used for the empirical examples are presented; methods which make an analysis of social interaction and spoken language use possible.

### Doing _research_ in primary school

The two episodes used in the analysis below occurred in an ethnographical study conducted 2008 at a Swedish public and coeducational primary school. Two third-year forms, where most of the children were nine years old, participated in the study. The children were observed when working with what, in this setting, was called _research_. _Research_ written in italics refers to a way of working where the children were supposed to explore an area of interest through their own information seeking and use. Other ways of describing _research_ could be problem-centred assignments or children-centred ways of working. In this paper, the activities studied are described as _research_ since this was the term used by the participants.

The five week period of the children's _research_ began with an introduction in each classroom where the teachers explained for the two groups of children how they should carry out their tasks. The children had been working with _research_ before (see [Lundh and Limberg 2008](#al08)), but new elements in the way of working were also introduced. Especially, the importance of choosing a topic based on one's own interests was stressed by the teachers. The children's _research_ processes usually began with the children choosing a topic and creating a mind-map of different aspects of, and questions about, this topic, then seeking information and finally presenting their findings in written booklets and in oral presentations. Throughout the whole process the children were supported by their teachers, a librarian, an information technology assistant (who taught information seeking on the Internet) and sometimes by a remedial teacher.

Some characteristics of similar school activities and assignments have been identified in previous research within information science and education. One recurrent theme within this area is how the imperative of finding and creating one's own knowledge clashes with traditional school practices (e.g., [Alexandersson and Limberg 2003](#ma03)). This means that the students have to adjust to contradictory expectations. Limberg ([2007](#ll07)) highlights the '_ever-present norm in the discursive practice of school, expressed in assessment and evaluation_' as a typical feature of school assignments. However, in assignments characterised by a certain degree of freedom, such as assignments where students are supposed to find a problem area and explore this area in an independent way, this ever-present norm and the assessment criteria may become somewhat concealed for the students.

This concealment might also be a problem for librarians; Gross ([2001](#mg01)) describes the difficulties when pupils and students ask so-called _imposed question_s at reference desks. Imposed questions derive, according to Gross, from external demands, rather than personal curiosity, and school assignments are seen as typical bases for such questions:

> It is easy to conceptualize the teacher as imposer and the student as the agent, asked to respond to a question he or she did not think up, but is responsible for ([Gross 2001](#mg01)).

The imposed query model can be said to be binary; a question is seen as either individually self-generated or imposed by someone else. This division can be questioned from the sociocultural and dialogistic framework presented above and below it will be shown how imposed questions are actually imposed through interaction between a teacher and two pupils. However, as Limberg ([2007](#ll07)) points out, Gross's model highlights the fact that school children have to adjust to certain expectations when they are doing school assignments which require information seeking.

### Data collection

The study in which the empirical examples below are taken from was a continuation of an earlier study ([Lundh and Limberg 2008](#al08)), which means that the process of getting access to the field started long before the fieldwork began. In other words, the study can be seen as a part of an ethnography with "a selective intermittent time mode" ([Jeffrey and Troman 2004](#bj04): 540ff) where the focus is quite specific. The study was informed by visual ethnography ([Pink 2007](#sp07)) which is based on visual and audiovisual collection methods in addition to field notes which usually are viewed as the main data collection method within traditional ethnography. When working with video with children some ethical considerations need to be taken into special account. The implementation of the study has followed the ethical principles set out in the Swedish Ethical principles for humanistic and social scientific research ([_Forskningsetiska principer..._ 2002](#vr02)).

Empirical materials that consist of spoken language and social interaction can be presented in more than one way. Traditionally, transcripts are used; this is the case for the first episode below (the transcript notation can be found in the [Appendix](#app)). However, in the second episode the understanding of the conversation is facilitated by the inclusion of non-verbal elements to get the picture, for example, of how the participants are moving in the room and how artefacts are included in their conversation. For this purpose sequential art ([McCloud 1994](#sm94)) or comics has been found useful. The pictures used in this paper are all based on frames from the video recordings. These frames have been digitally edited to ensure confidentiality, before they have been structured as comic strips through the use of the software Comic Life. Within information science this way of representing empirical material is unusual, but it has been used within educational science in a few cases (e.g., [Plowman and Stephen 2008](#lp08); [Ivarsson 2010](#jiip)).

## Question-negotiations in the classroom and in the school library

The two following episodes have been chosen in order to show how question-negotiations can be analysed as situated language games. The analysis will show how questions emerge as topics for negotiation in the conversations in a primary school setting. As mentioned above, a significant feature of this setting is that the children are supposed to formulate questions in their assignments; this makes the episodes suitable for showing how question-negotiating processes may proceed.

In the first episode, we will see two girls and a teacher discussing what questions the girls' ongoing work should be based on. In this episode the negotiation between the children and the teacher is characterised by conflicting agendas; the teacher is trying to convince the girls that certain new questions are of importance. In the second episode the participants, two other girls and a school librarian, are discussing useful sources for information seeking; a discussion that implicitly concerns what questions the girls should be working with.

### Imposed questions that take form in interaction

The first episode takes place in the middle of the children's _research_ period and it can be described as an institutional discourse activity where one of the participants, the teacher, drives the conversation. In the episode two girls, Maja and Fanny, are working with their _research_ project about Guinness World Records at a desk in a classroom as their teacher, Pernilla, approaches them. Additional questions are introduced by the teacher and the episode is the first of several where she is trying to persuade the girls to include questions about the history of Guinness World Records and the rules and regulations for setting records. In this episode we can see how new questions are brought up as topics in the conversation, how these questions are put forward and negotiated, and how they are finally accepted by the girls. When compared to the second example below we will see that there is less agreement on what the outcome of the encounter should be; in this sense, the teacher's questions can be seen as imposed in the interaction. The first of the new questions is introduced in transcript 1 below:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2\. Transcript 1**  
</caption>

<tbody>

<tr>

<td></td>

<td align="center">**English translation**</td>

<td align="center">**Swedish transcript**</td>

</tr>

<tr>

<td>**1**</td>

<td>**Pernilla:** what (.) how have you done so far? is this what you have done?</td>

<td>**Pernilla:** vilk (.) hur är det ni har gjort nu? är det som ni har gjort?</td>

</tr>

<tr>

<td>**2**</td>

<td>**Fanny:** yes (.) and (.) some other</td>

<td>**Fanny:** ja (.) och (.) lite andra</td>

</tr>

<tr>

<td>**3**</td>

<td>**Maja:** I've done these</td>

<td>**Maja:** dom har jag gjort</td>

</tr>

<tr>

<td>**4**</td>

<td>**Pernilla:** so you write about some crazy records</td>

<td>**Pernilla:** så ni tar och skriver om några jättetokiga rekord</td>

</tr>

<tr>

<td>**5**</td>

<td>**Fanny:** yes</td>

<td>**Fanny:** ja</td>

</tr>

<tr>

<td>**6**</td>

<td>**Pernilla:** that you find interesting</td>

<td>**Pernilla:** som ni fastnar för</td>

</tr>

<tr>

<td>**7**</td>

<td>**Fanny:** yes</td>

<td>**Fanny:** ja</td>

</tr>

<tr>

<td>**8**</td>

<td>**Pernilla:** is there any history (.) why (.) who has come up with the idea of Guinness records (.) isn't there anything like that you can write about too?</td>

<td>**Pernilla:** finns det nån historia (.) varför (.) vem har hittat på Guinness rekordbok (.) finns det nåt sånt där man skriva också om?</td>

</tr>

</tbody>

</table>

To start with, the teacher asks the girls what they have been doing so far (line 1-7 in transcript 1). In this initial part of the conversation the focus is on what the children have done and written; it is not obvious from their reply that their information seeking and use have been guided by any specific question. However, in line 8 Pernilla introduces a new question and thereby lays the foundation for the continuing question-negotiation.

After the teacher has introduced the question of the history of Guinness World Records the girls explain that they already have tried to find information about that, but without any success. The teacher continues by asking them about which sources they have used and also suggests a new source, but this does not lead to any immediate information seeking activity. Instead, the conversation continues and in line 9 in transcript 2 below, the teacher herself points the girls to a paragraph in one of the record books they have at the desk and she underlines that she finds that information to be "very important" for the girls to use. By doing this, the teacher introduces yet another question:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3\. Transcript 2**  
</caption>

<tbody>

<tr>

<td></td>

<td align="center">**English translation**</td>

<td align="center">**Swedish transcript**</td>

</tr>

<tr>

<td>**9**</td>

<td>**Pernilla:** 'cause <u>look here</u> (.) what does this mean? (.) look there (.) I think it's very important to include that too (.) what does it say?</td>

<td>**Pernilla:** för <u>titta här</u> (.) vad menas med det? (.) titta där (.) det tycker jag är jätteviktigt man har med också (.) vad står det där?</td>

</tr>

<tr>

<td>**10**</td>

<td>**Maja:** ((tries to read)) () (.) I I can't</td>

<td>**Maja:** ((försöker läsa)) () (.) jag jag kan inte</td>

</tr>

<tr>

<td>**11**</td>

<td>**Fanny:** [records]</td>

<td>**Fanny:** [rekordbok]</td>

</tr>

<tr>

<td>**12**</td>

<td>**Pernilla:**[()] it's it (.) Guinness records- (.) ((reads: does rigorous examinations before a record is approved)) (.) what does that mean?</td>

<td>**Pernilla:**[()] det är den (.) Guinness rekordboks- (.) ((läser: genomför mycket noggranna kontroller innan något rekord godkänns)) (.) vad menas med det?</td>

</tr>

</tbody>

</table>

Hence, the questions the teacher introduces are of a somewhat different character; in the beginning of the discussion she introduces the idea of the history of Guinness World Records, but during the course of the conversation, by using one of the books on the desk, the question concerning the rules and regulations of setting records comes into focus.

Before the Pernilla leaves the desk she makes sure that the girls understand what the text says by using an example. Then, in line 13 in transcript 3 below she persists that it is '_important_' to include the information in the text in the girls' _research_. The girls' response is however fairly half-hearted. In line 16, the teacher creates some room for resistance by saying '_do I cause you trouble now?_' which Maja answers by saying '_no_'. Again, the teacher underlines that she finds the question or the questions she has introduced to be '_important_' in line 18\. She also makes sure that Fanny, who did not reply to Pernilla's question in line 16, knows where to find the paragraph they have discussed. In lines 25 and 27 the teacher demonstrates that she understands the girls' resistance, but she also makes it clear that she expects the girls to include the last question she has introduced, or possibly both of the questions, in their work.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4\. Transcript 3**  
</caption>

<tbody>

<tr>

<td></td>

<td align="center">**English translation**</td>

<td align="center">**Swedish transcript**</td>

</tr>

<tr>

<td>**13**</td>

<td>**Pernilla:** read a little about that because (.) it is really good that you write like this but I think it is important that you also write a little about why you are doing it and that not anyone (.) don't you?</td>

<td>**Pernilla:** läs om det lite för (.) det är jättebra att ni skriver lite så här men jag tycker att det är viktigt att man skriver lite också varfö man gö det och att inte vem som helst (.) tycker inte ni det?</td>

</tr>

<tr>

<td>**14**</td>

<td>**Maja:** mm</td>

<td>**Maja:** mm</td>

</tr>

<tr>

<td>**15**</td>

<td>**Fanny:** [mm]</td>

<td>**Fanny:** [mm]</td>

</tr>

<tr>

<td>**16**</td>

<td>**Pernilla:** [do I cause you trouble now?]</td>

<td>**Pernilla:** [ställer jag till det fö er nu?]</td>

</tr>

<tr>

<td>**17**</td>

<td>**Maja:** no</td>

<td>**Maja:** nej</td>

</tr>

<tr>

<td>**18**</td>

<td>**Pernilla:** no (.) look at that a little too and then you continue do some more (.) but I find that important too</td>

<td>**Pernilla:** nej (.) kolla lite det också och så fortsäter ni och gö lite mer (.) men det tycker jag också är viktigt</td>

</tr>

<tr>

<td>**19**</td>

<td>**Maja:** okay</td>

<td>**Maja:** okej</td>

</tr>

<tr>

<td>**20**</td>

<td>**Fanny:** yes</td>

<td>**Fanny:** ja</td>

</tr>

<tr>

<td>**21**</td>

<td>**Pernilla:** I think you'll find it in the beginning of your book too</td>

<td>**Pernilla:** jag tror att det står längst fram i din bok med</td>

</tr>

<tr>

<td>**22**</td>

<td>**Fanny:** yes I just have to (.) find a page</td>

<td>**Fanny:** ja jag måste bara (.) hitta en sida det är bara det</td>

</tr>

<tr>

<td>**23**</td>

<td>**Pernilla:** ((reads: about this book))</td>

<td>**Pernilla:** ((läser: om den här boken))</td>

</tr>

<tr>

<td>**24**</td>

<td>**Fanny:** mm</td>

<td>**Fanny:** mm</td>

</tr>

<tr>

<td>**25**</td>

<td>**Pernilla:** and a bit of this (.) it doesn't look much fun but I know you can read it</td>

<td>**Pernilla:** och så lite sånt här (.) det ser inte så roligt ut men jag vet att ni fixar att läsa det</td>

</tr>

<tr>

<td>**26**</td>

<td>**Fanny:** okay</td>

<td>**Fanny:** okej</td>

</tr>

<tr>

<td>**27**</td>

<td>**Pernilla:** mm (.) you can do it later if you like (.) but I want it to be included in your re- research</td>

<td>**Pernilla:** mm (.) ni kan ta det sen annars (.) men jag vill att det ska komma med lite på eran forsk- forskning</td>

</tr>

</tbody>

</table>

In this episode there is what Linell ([1998](#pl98): 254ff) calls _asymmetries of participation_, where Pernilla, as a teacher, can interrupt the children's current activities and start to guide them into another direction. Her attempt to introduce new questions is not immediately embraced, however. In the discussion there seems to be _competing goals_ ([Linell 1998](#pl98): 224f; 258) as the girls are interested in the records as such, whereas the teacher wants them to work with other questions as well.

The encounter could be said to be an example of how questions become _imposed_ (cf. [Gross 2001](#mg01)). Still, it has to be noted that the imposition occurs in an interaction where the girls have some room for resistance; for instance, Fanny uses this room by not responding to the teacher's question in line 16\. When looking at this single episode we can also see that the responsibility for the questions is actually not the girls' until they accept it in the conversation. Furthermore, when the teacher ends the conversation by leaving the desk it is still uncertain whether the girls will do what they are asked to do or not. Still, because of the asymmetries of participation which has its base in the different roles of teachers and pupils, we can see this episode as an example of how imposed questions are created. However, the distinction between imposed and self-generated questions is ambiguous. As noted above, the rationale behind the _research_ way of working is that children are to do work that is grounded in their own genuine interests. The children are told to find questions they are interested in and that seems to be what Fanny and Maja are doing when writing about different records. At the same time we can see that the girls have to adjust to certain institutional expectations expressed by their teacher. This makes the division between self-generated and imposed questions less self-explanatory in this setting.

A related way of looking at this episode is that the teacher manifests the assessment criteria for the girls' assignment in the conversation. By underlining that the inclusion of the new questions is important she demonstrates her expectations as a teacher with regard to their _research_. As we will see in the next example, this is not an option in the same sense for all of the professionals interacting with the children.

### Questions that are gradually determined in interaction

The second episode is taken from the very beginning of the period followed. Here two girls, Maria and Jessika, are going to the school library with a mind-map of their _research_ on beverages and drinks. At the library they find the librarian Kristina and the dialogue that takes place can be regarded as a communicative genre or a specific language game well-known by librarians, namely a reference interview. However, it has to be noted that even though reference interviews can be seen as a specific communicative genre, every encounter of this kind must also be seen as a part of the making of the genre of reference interviews; the participants have been involved in similar encounters before, but as in any conversation they can never be certain of what the exact outcome might be.

When the girls find the librarian in the library they introduce '_drinks_' as a topic in the conversation. In this kind of communicative genre '_drinks_' is understood as an embryo for a question that should lead to information seeking. The three participants seem to share the understanding that the encounter should result in finding at least one book for the girls to bring back to their classroom. To begin with the topic of '_drinks_' is fairly vague, but as the discussion unfolds there are several attempts to turn '_drinks_' into a defined question. This is done through negotiation between the librarian and the girls who are discussing which tools the girls can use for information seeking. The first instances where the topic of drinks are negotiated as a question are seen in frames 1-5 below when the librarian has moved to one of the library shelves where cookery books are ranged.

<div align="center">![Frames 1 to 5](colis722fig1.jpg)</div>

<div align="center">  
**Figure 1: Frames 1 to 5**</div>

In frames 2-3 and 5 the librarian is using books from the shelves for introducing ideas of how '_drinks_' can be understood. A first attempt to define drinks as a question in relation to the available books is when Kristina introduces the idea of '_Christmas drinks_' in frame 3\. This suggestion is however declined immediately by the two girls, who are exchanging dissatisfied looks and who then turn their attention to another girl who approaches them. Later (in frame 9), they also explicitly say that they do not want to borrow the first book. During the course of the conversation, in frame 5, Kristina makes another suggestion, with a basis in another book. This time it seems as if a question becomes temporarily defined, when the girls agree with this suggestion.

One way of looking at this encounter is to see it as a _gradual determination of an indeterminate topic_ (see [Linell 1998](#pl98): 200f); in this case an indeterminate question. In the conversation, the topic or the implicit question is described as 'it' or 'this' (frame 1 and 8) when tools for information seeking are discussed. This way of describing the question implies vagueness (cf. [Garfinkel _et al._ 1981](#hg81): 157); the participants do not articulate what the 'it' or 'this' is, rather the whole conversation can be seen as directed towards defining 'it' and 'this' . During the encounter the question of drinks evolve and the participants put efforts into trying to find a shared understanding of this question in relation to the available sources. Even though a determination of the question is not explicitly articulated by any of the participants the action of accepting a book, a tool that will be used in the girls' information seeking and use, shows that a question has been temporarily defined.

However, the negotiation of present and non-present information seeking tools, as well as the question, continues between Kristina and the girls. In frame 6 below, Jessika concludes this discussion by saying that they also can use '_the net_' for seeking information. Here the discussion becomes more future-oriented ([Linell 1998](#pl98): 230ff); it concerns what the girls could do somewhere else rather than in the library with tools for information seeking other than books. This also implies that the question of '_drinks_' will continue to be negotiated elsewhere.

<div align="center">![Frames 6 and 7](colis722fig2.jpg)</div>

<div align="center">  
**Figure 2: Frames 6 and 7**</div>

Even though the girls and the librarian seem to have defined a question, they still do not express the exact same description of the question of drinks. In frame 7 Kristina's and the girls' interpretations of the question diverge; while Kristina suggests a focus on how drinks are made, the girls ask for the ingredients of drinks. The different interpretations are however not acknowledged by the participants. Instead, the discussion is brought to a close with the joint establishment that the girls also will use "the computer" and borrow the book about fruit drinks, in frames 8 and 9 below. This means that the _communicative project_ shared by the two girls and the librarian is finalised; the girls leave and borrow the book and Kristina continues to help the other children who have come to the library during the conversation with Jessika and Maria.

<div align="center">![Frames 8 and 9](colis722fig3.jpg)</div>

<div align="center">  
**Figure 3: Frames 8 and 9**</div>

This episode can be seen as an example of question-negotiation in the communicative genre of reference interviews. In this setting, visits to the library are usually supposed to result in finding at least one book and this seems to be a common goal for the girls and the librarian. The girls have an assignment where they have to come up with one or a few questions to explore and this is accomplished, at least to some extent, in their encounter with the librarian. But their question is only temporarily defined in relation to the available sources and through the decision to borrow the book about fruit drinks; as we can see in the episode the question is not understood in one single way. Compared to the first episode above, where questions are imposed in the interaction between the girls and the teacher, this example is characterised by a higher degree of vagueness. During the five week period of the children's _research_ the question became even more ambiguous. However, for the purpose of this specific communicative project, a solution is found.

## Discussion

The two episodes described above show how questions, which have the potential to lead to information seeking activities, can be analysed as constructed and negotiated in social interaction, rather than constructed within individuals outside of social encounters. As McKenzie has ([2004](#pm04)) suggested, the focus is moved from the users' accounts of their inner thoughts and feelings about their assumed information needs to people discussing and interacting in naturalistic settings. This shift opens up for a detailed analysis of questions as emerging between individuals in the local context created through the interaction, but also for an analysis of the wider context in which the interaction takes place (cf. [Linell 2009](#pl09): 62f).

When comparing the two episodes we can see how different roles create different possibilities and entail different expectations regarding participation in the interactions. In the first encounter, where questions become imposed the negotiation is characterised by competing goals ([Linell 1998](#pl98): 224f; 258) where the position as a teacher gives some control over the outcome of the communicative project. As a teacher, Pernilla has other possibilities as well as other responsibilities in the interaction with the pupils than Kristina has, as a librarian, in the second encounter where the negotiation and determination of one or several questions is implicit rather than explicit and clearly articulated. Nonetheless, both the librarian and the teacher contribute to, and are thereby partly accountable for, the formulation of questions in the interactions.

According to the framework presented in this paper questions are seen as proposed, discussed, accepted or resisted in specific social settings and this creates certain possibilities and constraints for how questions can and cannot be constructed. The two examples above show how question-negotiating processes can proceed as communicative projects when children are doing _research_ in a primary school setting. The projects are carried out in different ways, but both of them are situated in an institutional context where the formulation of questions is an important part of the activities. In other words, in the role as a pupil in this setting you are expected to learn how to formulate questions that can lay a foundation for information seeking; question-formulating can be seen as a cultural tool that the children have to appropriate. This expectation underlies both of the episodes described above.

Even though the analyses build on two episodes, they raise issues that could be explored further, such as how assessment criteria are made visible and used to direct action in the interactions; how the genre of _research_ is constructed and enacted; and other questions that relate to information practices in primary school. Thus, what you chose to focus on as an analyst can vary; even though spoken language is the primary unit of analysis above, other aspects of the interactions might be relevant for analysis. One way of representing empirical material for such analyses is to use sequential art, which has possibilities far beyond how it has been used here. In this paper, this form of representation has mainly been used to enhance the understanding of the conversation itself, but it can also be used for analysing, for example, material and corporeal aspects of information practices.

## Conclusions

From the theoretical framework presented in this paper the idea that people are the carriers of actual information needs which they have difficulties in expressing is not seen as useful; from this perspective it is not feasible to think of information needs as something existing outside the realm of language and social interaction. Language is the cultural tool which humans use for thinking ([Säljö 2005](#rs05): 44; [Linell 2009](#pl09): 110) and instead of seeing language as something that might distort actual information needs it is seen as the facilitator of the formulation of questions which might lead to information seeking. These questions could, of course, also be negotiated within individuals through the use of language, but for an analyst, question-negotiating processes are accessible and possible to study in social interactions.

It is for this reason the analyses in this paper are built on empirical material produced in naturalistic settings where actions and accounts are visible to all participants, including the analyst. This does not mean that the analysis is diminished to a mere recording of people's behaviour (cf. [Hjørland 2007](#bh07)); the actions and utterances are understood as meaningful for the participants, for the practices they are a part of and as having practical implications (cf. [Linell 2009](#pl09): 414f).

The examples above are meant to demonstrate an alternative way of creating an understanding of users' questions which differs from the methods traditionally employed within the field of information needs, seeking and use (see, [Talja and Hartel 2007](#stjh07)). With an interest in the latter levels in Taylor's model (Q<sub>3</sub> and Q<sub>4</sub>) rather than the two first (Q<sub>1</sub> and Q<sub>2</sub>) questions are seen as the results of collective efforts and joint construction and not only of individual thoughts and feelings. This does not mean that information needs are seen as less real; they are seen as important parts of concrete, social, material and situated information practices and therefore as relevant to study within LIS.

## Acknowledgements

The writing of this paper has been conducted at The Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS) and it was made possible by funding from Stiftelsen Paul och Marie Berghaus donationsfond and Signhild Engkvists stiftelse.

The author would like to thank Louise Limberg and Mikael Alexandersson, as well as colleagues at seminars at the Centre, the Swedish School of Library and Information Science, Queensland University of Technology and Charles Sturt University for valuable comments on this paper. Also, a warm thanks to Frances Hultgren for her eminent assistance on issues concerning the English language and Rebecca Landmér for her invaluable help with editing the pictorial material.

## About the author

Anna Lundh is a Ph.D. Candidate at The Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society and The Swedish School of Library and Information Science, at the University of Borås and the University of Gothenburg. She can be contacted at: [anna.lundh@hb.se](mailto:anna.lundh@hb.se)

#### References

*   Alexandersson, M. & Limberg, L. (2009). Elevers 'forskning' via datorn: mantra metod eller meningsfullt lårande? [Pupils' 'research' through the computer: mantra, method or meaningful learning?] In J. Hedman & A. Lundh (Eds.), _Informationskompetenser: om lärande i informationspraktiker och informationssökning i lärandepraktiker_ [_Information literacies: learning in information practices and information seeking in learning practices_]. (pp. 85-107). Stockholm: Carlsson.
*   Alexandersson, M. & Limberg, L. (2003). Constructing meaning through information artefacts. _New Review of Information Behaviour Research_, **4**, 17-30\.
*   Antaki, C. (2002). _[CA tutorial: notation. An introductory tutorial in Conversation Analysis](http://www.webcitation.org/5uR5vzBam)_. Retrieved 14 April, 2010 from http://www-staff.lboro.ac.uk/~ssca1/notation.htm (Archived by WebCite® at http://www.webcitation.org/5uR5vzBam)
*   Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982a). ASK for information retrieval: part I. Background and theory. _Journal of Documentation_, **38**(2), 61-71.
*   Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982b). ASK for information retrieval: part II. Results of a design study. _Journal of Documentation_, **38**(3), 145-164.
*   Byström, K. & Hansen, P. (2005). Conceptual framework for tasks in information studies. _Journal of the American Society for Information Science and Technology_, **56**(10), 1050-1061.
*   Case, D.O. (2007). _Looking for information: a survey of research on information seeking,needs, and behavior_. (2nd. ed.) Amsterdam: Elsevier/Academic Press.
*   Dervin, B. (2003). From the mind's eye of the user: the sense-making qualitative-quantitative methodology. In B. Dervin, L. Foreman-Wernet & E. Lauterbach (Eds.), _Sense-making methodology reader: selected writings of Brenda Dervin_ (pp. 269-292). Cresskill, NJ: Hampton Press. Originally published in J. D. Glazier & R. R. Powell (Eds.), _Qualitative research in information management_ (pp. 61-84). Englewood, CO: Libraries Unlimited.
*   _[Forskningsetiska principer inom humanistisk-samhälsvetenskaplig forskning](http://www.webcitation.org/5uRBTMpKz)_. [Ethical principles for research in the humanities and social sciences] (2002). Stockholm: Vetenskapsrådet. Retrieved 14 April, 2010 from http://www.cm.se/webbshop_vr/pdfer/etikreglerhs.pdf (Archived by WebCite® at http://www.webcitation.org/5uRBTMpKz)
*   Frohmann, B. (2004). Documentation redux: prolegomenon to (another) philosophy of information. _Library Trends_, **52**(3), 387-407.
*   Garfinkel, H., Lynch, M. & Livingston, E. (1981). The work of a discovering science construed with materials from the optically discovered pulsar. _Philosophy of the Social Sciences_, **11**(2), 131-158.
*   Gross, M. (2001). [Imposed information seeking in public libraries and school library media centres: a common behaviour?](http://www.webcitation.org/5uRBaqRrB) _Information Research_, **6**(2), paper 100 Retrieved 14 April, 2010 from http://InformationR.net/ir/6-2/paper100.html (Archived by WebCite® at http://www.webcitation.org/5uRBaqRrB)
*   Hjørland, B. (2007). [Information needs](http://www.webcitation.org/5uSAKaXy6). Retrieved 23 November, 2010 from http://web.archive.org/web/20080607212338/www.db.dk/bh/Core+Concepts+in+LIS/articles+a-z/information_needs.htm (Archived by WebCite® at http://www.webcitation.org/5uSAKaXy6)
*   Hjørland, B. (1997). _Information seeking and subject representation: an activity-theoretical approach to information science_. Westport, CT: Greenwood Press.
*   Ivarsson, J. (2010). Developing the construction sight: architectural education and technological change. _Visual Communication_, **9**(2), 171-191
*   Jeffrey, B. & Troman, G. (2004). Time for ethnography. _British Educational Research Journal_, **30**(4), 535-548.
*   Kloda, L. & Bartlett, J. (2009). [Rehabilitation therapists' clinical questions in the context of evidence-based patient care: an exploratory study](http://www.webcitation.org/5uRCWOmoq). In Paulette Rothbauer, Siobhan Stevenson and Nadine Wathen, (Eds.). _Proceedings of the Canadian Association for Information Science (CAIS) 37th Annual Conference_. Retrieved 13 April, 2010 from http://www.cais-acsi.ca/proceedings/2009/Kloda_2009.pdf . (Archived by WebCite® at http://www.webcitation.org/5uRCWOmoq)
*   Krikelas, J. (1983). Information-seeking behavior: patterns and concepts. _Drexel Library Quarterly_, **19**(2), 5-20\.
*   Kuhlthau, C.C. (2009). [_Information search process_](http://www.webcitation.org/5uRCgzNys). Retrieved 14 April, 2010 from http://www.scils.rutgers.edu/~kuhlthau/information_search_process.htm (Archived by WebCite® at http://www.webcitation.org/5uRCgzNys)
*   Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services_ (2nd. ed.). Westport, CT: Libraries Unlimited.
*   Lave, J. (1990). The culture of acquisition and the practice of understanding. In J.W. Stigler, R.A. Shweder & G.H. Herdt (Eds.), _Cultural psychology: essays on comparative human development_ (pp. 309-327). Cambridge: Cambridge University Press.
*   Leckie, G.J., Pettigrew, K.E., & Sylvain, C. (1996). Modeling the information-seeking of professionals: a general model derived from research on engineers, health care professionals and lawyers. _Library Quarterly_, **66**(2), 161-193.
*   Limberg, L. (2007). [Learning assignment as task in information seeking research](http://www.webcitation.org/5uRCvScom). _Information Research_, **12**(4), paper colis28\. Retrieved 15 July, 2010 http://informationr.net/ir/12-4/colis/colis28.html (Archived by WebCite® at http://www.webcitation.org/5uRCvScom)
*   Linell, P. (2009). _Rethinking language, mind, and world dialogically: interactional and contextual theories of human sense-making_. Charlotte, NC: Information Age Publishing.
*   Linell, P. (1998). _Approaching dialogue: talk, interaction and contexts in dialogical perspectives_. Philadelphia, PA: John Benjamins Publishing
*   Lundh, A. & Limberg, L. (2008). Information practices in elementary school. _Libri_, **58**(2), 92-101\.
*   McCloud, S. (1994). _Understanding comics: the invisible art_. New York, NY: HarperCollins Publishers.
*   McKenzie, P.J. (2004). Positioning theory and the negotiation of information needs in a clinical midwifery setting. _Journal of the American Society for Information Science and Technology_, **55**(8), 685-694.
*   Pink, S. (2007). _Doing visual ethnography: images, media and representation in research_. London: Sage Publications.
*   Plowman, L. & Stephen, C. (2008). The big picture?: video and the representation of interaction. _British Educational Research Journal_, **34**(4), 541-565\.
*   Savolainen, R. (2007). Information behavior and information practice: reviewing the 'umbrella concepts' of information seeking studies. _The Library Quarterly_, **77**(2), 109-132.
*   Sundin, O. & Johannisson, J. (2005). The instrumentality of information needs and relevance. In F. Crestani and I. Ruthven (Eds.), _Context: nature, impact, and role: 5th International Conference on Conceptions of Library and Information Sciences, CoLIS 2005, Glasgow, UK, June 4-8, 2005; proceedings_. (pp. 107-118). Berlin: Springer.
*   Säljö, R. (2005). _Lärande och kulturella redskap: om lärprocesser och det kollektiva minnet_. [_Learning and cultural tools: on learning processes and the collective memory_]. Stockholm: Norstedts akademiska förlag.
*   Säljö, R. (2000). _Lärande i praktiken: ett sociokulturellt perspektiv_. [_Learning in practice: a sociocultural perspective_]. Stockholm: Prisma.
*   Talja, S. & Hartel, J. (2007). [Revisiting the user-centred turn in information science research: An intellectual history perspective](http://www.webcitation.org/5uRDCvTkm). _Information Research_, **12**(4), paper colis04\. Retrieved 14 April, 2010 from http://InformationR.net/ir/12-4/colis/colis04.html (Archived by WebCite® at http://www.webcitation.org/5uRDCvTkm)
*   Talja, S. & McKenzie, P.J. (2007). Editor's introduction: special issue on discursive approaches to information seeking in context. _The Library Quarterly_, **77**(2), 97-108\.
*   Talja, S., Tuominen, K. & Savolainen, R. (2005). 'Isms' in information science: constructivism, collectivism and constructionism. _Journal of Documentation_, **61**(1), 79-91\.
*   Taylor, R.S. (1968). Question-negotiation and information seeking in libraries. _College and Research Libraries_, (No. 29), 178-194.
*   Taylor, R.S. (1962). The process of asking questions. _American Documentation_, **13**(4), 391-396.
*   Tuominen, K. (1997). User-centered discourse: an analysis of the subject positions of the user and the librarian. _Library Quarterly_, **67**(4), 350-371.
*   Vygotsky, L.S. (1997). The instrumental method in psychology. In R. W. Rieber & J. Wollock (Eds.), _The collected works of L.S. Vygotsky. Vol. 3, problems of the theory and history of psychology: including the chapter on the crisis in psychology_ (pp. 85-90). New York, NY: Plenum Press.
*   Vygotsky, L.S. (1978). _Mind in society: the development of higher psychological processes_. Cambridge, MA: Harvard University Press.
*   Wertsch, J.V. (1998). _Mind as action_. New York, NY: Oxford University Press.
*   Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation_, **37**(1), 3-15.
*   Wittgenstein, L. (1973). _Philosophical investigations_. (3rd. ed.). Upper Saddle River, NJ: Prentice Hall

## Appendix: Transcription notation, after Antaki ([2002](#ca02)).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td>(.)</td>

<td>Pause</td>

</tr>

<tr>

<td>[ ]</td>

<td>Marks overlapping talk</td>

</tr>

<tr>

<td>( )</td>

<td>Unclear or inaudible, sometimes with a suggestion</td>

</tr>

<tr>

<td>=</td>

<td>Represents occurrences where there are no notable pause between two utterances</td>

</tr>

<tr>

<td><sup>o o</sup></td>

<td>Represents quiet speech</td>

</tr>

<tr>

<td><u>Underlining</u></td>

<td>Represents loud speech</td>

</tr>

<tr>

<td>-</td>

<td>Represents abrupt breaks</td>

</tr>

<tr>

<td>(( ))</td>

<td>Describes sounds and ways of talking that are hard to transcribe directly</td>

</tr>

</tbody>

</table>