#### vol. 15 no. 4, December, 2010

## Proceedings of the Seventh International Conference on Conceptions of Library and Information Science - "Unity in diversity" - Part 2

# Transcending silos, developing synergies: libraries and archives

#### [Gillian Oliver](#author)  
School of Information Management, Victoria University of Wellington, Wellington, New Zealand

#### Abstract

> **Introduction**Very little research crosses the institutional boundaries of libraries and archives, and theres appears to be little predisposition to look for areas of common interest. However, practitioners in these settings have much to learn from each other.  
> **Purpose** This paper argues that practitioners' understanding of information management is likely to be influenced by their occupation and that this narrow view does not foster an environment which is conducive to the sharing of knowledge and experiences between the institutional types. The potential value which could be realised in increasing awareness of the nature and purpose of each other's domains and familiarity with the literature in each area, is illustrated by using the example of joint use libraries and comparing this to the changing pattern of user services in archives. The need for greater understanding of different perspectives in the digital environment is also signalled.  
> **Conclusions** The information continuum theory is proposed as a framework to underpin research that encompasses all professional groups active in information management.

## Introduction

Librarians, records managers and archivists are all likely to refer to themselves as information managers. Practitioners in libraries and archives have much to learn from each other but even in this era of collaboration and convergence there appears to be very little research that crosses these traditional institutional boundaries. This paper argues that practitioners' understanding of information management is likely to be influenced by their occupation, rather than by theory, and that this does not foster an environment which is conducive to the sharing of knowledge and experiences. The potential value which could be realised in increasing awareness of the nature and purpose of each other's domains, and familiarity with the literature in each area, is explored with reference to both traditional and digital settings.

The paper is structured as follows. Firstly 'information management' as a concept is discussed, and the competitive nature of the working environment for librarians and recordkeepers (archivists and records managers) described. The information continuum theory is put forward as a framework which does not accord greater status to, or privilege, the work carried out by librarians or recordkeepers. This is followed by discussion of two specific settings. Firstly, comparison is made between the issues and challenges faced by joint use libraries with those by government archives in delivering services to users. Secondly, digital repository design is highlighted as an example of an approach which could be further utilised in the development of digital libraries and archives. The paper concludes with a call for greater awareness of, and respect for, each other's domain.

## Information management

The nature of information management as a discipline has been subject to much debate. It has been claimed that because of the involvement of multiple occupations in information management it is unrealistic to expect to find a single definition that covers its entire spectrum ([Lewis & Martin 1989](#lew89)), which seems to acknowledge that the concept is fuzzy and open to interpretation.

'Information management' seems to be frequently used by the various occupations as a branding device, to improve or update an existing image. For instance, the Association of Records Managers and Administrators, the professional association representing North American records managers changed the name of its journal in 1999 from _Records Management Quarterly_ to _Information Management Journal_. The editorial announcing the change explained that:

> the term 'information management' is an inclusive, or umbrella, designation that covers allied disciplines such as records management, archives management, electronic information systems, knowledge management, and corporate librarianship. ([Anonymous, 1999](#ano99))

Nevertheless, journal content continues to be firmly focused on records management.

Similarly, a preference for _information_ for at least certain segments of the library profession can be traced back to the early twentieth century. Ethel Johnson in 1915 is quoted as differentiating special libraries from public libraries by emphasising the key role of information:

> The main function of the general library is to make books available. The function of the special library is to make information available. ([Williams 1998](#wil98): 174)

More recent discussions of information management seem to reflect those earlier efforts to enhance professional status. For instance, Kirk ([1999](#kir99)) argues that the traditional view of information management has focussed on information as a resource or a commodity, and thus in organisational terms information management has been regarded as providing a service. By extending the definition beyond the resource and/or commodity focus, she suggests that information management will be seen as central to achieving organisational goals and ultimately shifting from service provision to strategy formation.

A different approach is taken by Rowley, who proposes a framework for information management comprising four different levels: information retrieval, information systems, information contexts and information environments. She describes information management as being:

> a practice-based discipline that has both technical, most broadly in the sense of systems based, and behavioural dimensions. ([Rowley 1998: 361](#row98))

She groups the different levels identified above into two sub-disciplines of information management:

> Microinformatics (concerned with individuals and their use of information, thus encompassing information retrieval and information systems), and  
> Macroinformatics (concerned with the relationship between information and society and its organisations, thus including information contexts and information environments)" ([Rowley 1998: 365](#row98)).

Although the approaches outlined above provide some insight into the complexity of information management, they do not acknowledge the management of information artefacts and information-as-process (as defined by [Buckland 1991](#buc91)) and in addition are not particularly helpful when trying to explore the roles played within it by the different occupational groups. The existence of distinct occupational groups within this domain, if acknowledged, tends to be in a cursory fashion; occupations may be identified, but reference is not made to the different purposes of information that they focus on (see, for instance [Gorman & Corbitt 2002](#gor02); [Rowley 1998](#row98)). An outcome of this situation is that for those working in libraries and archives it becomes difficult to move out of their professional silos.

## Our competitive environment

Furthermore, the environment that librarians and archivists are working in at present is one which is characterised by competition. In New Zealand, for instance, the status of both the national archives and the national library are currently uncertain as a result of a government proposal to merge the two institutions into the Department of Internal Affairs. This proposal has been greeted with much alarm from the archives community ([Council of the Archives... 2009](#cou09)). Underneath the veneer of the collaboration and convergence rhetoric, the application of Abbott's definition of professions, which is based on systems theory, suggests that there is a struggle for dominance taking place.

According to Abbott ([1988](#abb88)), professions arise as a result of system disturbance, and will then establish jurisdiction over a problem. As there are constant disturbances within society there are continually changes in demands on occupations and consequently competition for jurisdiction. Sweeney applied the theory specifically to the development of archives, records management and librarianship as individual disciplines, explaining that:

> The system disturbance that created the library profession was the creation of public libraries, and hence a demand for librarians. In the case of archivists, it was the creation of archives in response to an increased volume of material to preserve. For records managers, it was the surge in records creation during and after the Second World War, as well as the drive for efficiency and economy in both the public and private sectors. ([Sweeney 2001](#swe01): 28-29).

Reviewing more recent developments, it has been observed that library and information science is engaged in competition for jurisdiction with other information professions due to firstly changes in computing and telecommunications technologies, and secondly due to the increasing strategic importance of information ([Van House and Sutton 1996](#van96)).

The management of knowledge and information is of crucial importance for economies in the developed world today, and there is definitely competition for jurisdiction over this sphere of activity, not only between the three more traditional information management professions (records management, archives and librarianship) but also from information systems and human resources personnel. Analysis of the knowledge management literature has demonstrated that it reflects a range of disciplines ([Davenport & Cronin 2000](#dav00)).

However, lacking a clear and comprehensive theoretical underpinning which acknowledges the different roles and purposes of librarians and recordkeepers, there is no firm foundation for exploration of each other's domains. While there is competition for jurisdiction it is very hard to admit that we can learn anything from each other, thus librarians and archivists will tend to remain in their institutional silos.

Returning to the New Zealand situation, the rationale for the merger between the national library and archives is claimed to be because of the common goals of the two institutions in digitizing information:

> Both Archives and National Library have significant digitalisation plans and bringing these departments together with DIA will support this development with less risk and cost. It will produce opportunities for the use of common capability, economies of scale, and transfers to frontline services through shared backroom services and better access for the public. ([New Zealand... 2010](#new10))

An imposed amalgamation such as this could have negative consequences for both library and archives services, and it thus becomes even more imperative to try to make the best possible use of our collective experiences. Recognition of the distinct purposes for which librarians and archivists manage information will assist in providing common, rather than contested, ground.

## A new theory for information management

Academics at the School of Information Management and Systems at Monash University developed a model to use as a teaching tool to differentiate the work undertaken by the different occupations involved in the management of information. The _information continuum_ is one of a series of continua models, the best known of which, and the most familiar to the archives community, is the records continuum. The information continuum is outlined in an annexe to a paper describing the development of the records continuum ([Upward 2000](#upw00)). It has been applied in various settings, including to assess the sustainability of a community information network ([Schauder _et al._ 2005](#sch05)), to investigate information culture ([Oliver 2004](#oli04)) and in the development of a university-wide information strategy and digital repository ([Treloar, _et al._ 2007](#tre07)). This repository project is further discussed below.

The model acknowledges of three distinct purposes for which information needs to be managed: accountability, knowledge or awareness, and information for pleasure or entertainment. Where information is of the accountability type it provides evidence of organisational activity and functions as the corporate memory. This is the primary (not necessarily sole) purpose of the work undertaken by records managers and archivists. Management of information as knowledge or awareness and information for pleasure or entertainment is the focus for librarians.

This recognition of distinct purposes of information in the information continuum indicates its usefulness as a theory to inform research that crosses the disciplines working within information management. The benefits that we could derive in doing so is illustrated with reference to two quite different settings. Firstly, joint use libraries, and comparing the difficulties encountered with this concept with archives and their changing user profile. The second setting considered is the digital environment.

## Joint use libraries and archives users

Traditional library types have targeted collections and services to a type of user, whereas joint use libraries can be defined as libraries serving two or more client groups in the same building. McNicol states that this concept dates back to the 19th century but that its popularity has fluctuated over time. Although financially there are likely to be obvious benefits in such an approach, the complexity of the issues faced are enormous ([McNicol 2006](#mcn06)). Instances of joint use libraries discussed in a recent special issue of Library Trends include university and public library partnerships ([Dalton _et al._ 2006](#dal06); [Hansson 2006](#han06)) and one serving specialist and consumer groups in the healthcare sector ([Backus 2006](#bac06)). In other words, these joint use libraries have to engage with the challenges of providing services to two very different broad user groups.

Positions taken on the advantages and disadvantages of joint use libraries seem to be poles apart, as the following quote illustrates:

> Regrettably, the possibility of dual use libraries not only inflames passion but also seems to release all reason. One need only peruse the professional literature to realize that research is less commonly reported, where it even exists, than the experiences of both zealots and nonbelievers. Titles and subtitles include 'A success story!', 'Together at last', 'The long over due partnership', 'A call to action!' as well as 'A case against combination', 'Don't do it!' and 'A blueprint for disaster'.' The many, many examples profiled are based on assumed or presumed successes and the experiences of unmitigated catastrophe. ([Haycock 2006](#hay06): 489)

Reactions to joint use libraries seem to be based on emotional responses to departing from what is viewed as the norm, rather than research.

However, looking beyond the library silos to the archives world, it can be seen that there is a parallel community which has had to face exactly those problems inherent in serving two very disparate user groups.

The nature of archival records, and the diverse profiles of the communities they serve combine to ensure that repository types have not developed in the same way as libraries. The focus in this example is on government archives in particular, although many of the same points will apply to private archives. Government archives exist to provide trusted storage and access to government records deemed to be worthy of long term retention. Access to materials is available to all, but traditionally services have been targeted to scholars, rather than the general public. The information contained within archives has remained the same, but the interest in that information has over the years come from very different sectors of the community. The main reason for this is a huge surge of interest in family history research.

The function of government archives from the twentieth century onwards has largely changed from serving solely a very elite group of users to a much more egalitarian focus. An account of this changing focus in Britain is provided by Shepherd. In the mid twentieth century searchers were beginning to visit the national archives in increasing numbers and for different purposes, for instance those from the Indian sub-continent seeking to establish British citizenship ([Shepherd 2009](#she09): 82). She reports the findings from the first market research survey of user needs at the Public Records Office which took place in 1970\. Eight-three per cent were academics, students or professional researchers, 60% of enquiries were work or business related, and 25% of users were '_pursuing leisure interests including genealogy_'. ([Shepherd 2009](#she09): 86). A recent study of family historians use of archives reported that most studies have shown that in Europe and North America this group can now account for 50% to 90% of all users ([Tucker 2007](#tuc07)).

The growth in the number of genealogical users of the archives was further stimulated in 1972 when the 1871 British census records were released ([Shepherd 2009](#she09): 86). The combination of the release of these rich resources for family historians at ten-yearly intervals with digitisation and the ability for remote access through the Internet have resulted in unprecedented demand for archival records from all sectors of the community, world wide. A press release from The National Archives ([2009](#tna09)) refers to a '_family history frenzy_' with 3.4 million searches conducted on the first day of the release of the 1911 census. Yet at the same time our national archival institutions have to continue to serve the elite, the professional researchers and academics. In other words, the users of government archives are enormously diverse, and because of that diversity they bring very different sets of skills and expertise to their interactions with the archives, which of course impacts on staff.

The transition from focusing on the elite to a much more multi-faceted user profile certainly has not been easy or painless. As recently as 1993 Canadian research into attitudes towards genealogists reported the following as examples of the types of negative remarks made by archivists:

> Good. Its going to rain on Saturday. Maybe it will keep the genealogists away.  
>   
> Oh, she's a genealogist [as well as an archivist], but we won't hold that against her!  
>   
> I get sick to my stomach every time I see one of 'them' get off the elevator. ([Edwards 1993](#edw93): 3)

It is easy to imagine similar antipathy arising in the joint use library context, for example from staff having to tailor information services to patients if they are accustomed to only dealing with health care professionals.

Looking at the archives domain it can be seen that institutional models have had to evolve to serve user groups with fundamentally different aims and needs. This should not be taken to mean that the archives sector has all the answers which can be applied to solve the problems faced by joint use libraries. What it does mean however is that there is a rich vein of experience to be mined; a mass of data to be collected from staff and users by survey or interview which could inform decision making and strategy development when developing joint use libraries.

## Digital libraries and archives

Convergence and collaboration between libraries and archives in the digital environment is very much to be desired, as no one domain will have all the answers. However, the motivators for working together may be rather negative. The limited resources available to individual institutions in terms of funding and expertise will be significant influencing factors which will affect the nature and type of collaboration. In other words, this is a typical example of an environment where there is competition for jurisdiction, as discussed above.

The complexity and challenges of digital preservation in particular has prompted a call for

> individual disciplines to look at and transcend community-specific paradigms that have developed over time but that may not be appropriate in the digital context. ([Knight 2005](#kni05)).

The particular challenge for librarians and archivists in digital preservation is to understand why the different techniques associated with their domains have developed, what approaches need to be continued in the digital environment, what can be discontinued and where they can usefully exchange knowledge and expertise. Most importantly, they have to be able to communicate these understandings to each other; simply asserting 'we do it differently' is not sufficient.

A success story of developing a digital repository which includes the information traditionally managed by librarians as well as that which may be expected to be managed by recordkeepers is provided by Treloar and colleagues ([Treloar _et al._ 2007](#tre07); [Treloar & Harboe-Ree 2008](#tre08)). This was based on an adaption of the information continuum, resulting in a data curation continuum. This continuum provides for different values to be assigned to object properties, access conditions and management regimes. So, for example, the level and type of metadata needing to be applied to information can be adapted as required, in accordance with the purpose for which it is being managed (knowledge/awareness or accountability). This approach acknowledges the requirements specific to each information type and does not privilege the perspectives of one particular occupation or community specific paradigm.

## Conclusions

The controversy and debate surrounding the establishment of joint use libraries appears to reflect the acceptance of a clearly distinguishable library 'type' as a norm which should not be departed from. However, by dint of societal forces government archives in particular have been forced to adapt their services to meet both egalitarian as well as elite needs. This then provides a model for study which could illuminate problems and issues surrounding joint use libraries. In this case, the information continuum theory provides a setting in which comparisons can be made, without inferring solutions which would be inappropriate for the objectives of the different settings.

Application of continuum thinking to digital repository design provides a demonstration of how the differing management purposes of librarians and archivists can be accommodated within a single system. One view is not accorded any greater priority than the other; there is no attempt to force the information practices of one domain on another.

Libraries and archives have much to learn from each other, but in the absence of a generally accepted theory for information management which clearly acknowledges the contribution of the various occupations involved, we will remain working within our professional silos. Awareness of the issues and problems in each others' domains is likely to assist in providing new insight, and new perspectives on what may appear to be intractable problem areas. The information continuum model is recommended as the theoretical framework to underpin any research which spans the two distinct domains of libraries and archives. It provides a secure basis which should encourage exploration of colleagues' specialist domains, which have the potential to yield rich benefits for all concerned.

## About the author

Gillian Oliver is a Senior Lecturer at the School of Information Management, Victoria University of Wellington, Wellington, New Zealand. Department of Library and Information Science. She received her PhD from Monash University, Melbourne, Australia. She can be contacted at [gillian.oliver@vuw.ac.nz](mailto:gillian.oliver@vuw.ac.nz)

#### References

*   Abbott, A. (1988). The system of professions: an essay on the division of expert labor. Chicago, IL: University of Chicago Press
*   Anonymous (1999). Moving forward in 1999\. _Information Management Journal_, **33**(1) 1-2
*   Archives and Records Association of New Zealand. _Council,_ (2009). _[Position paper: review of Archives New Zealand](http://www.webcitation.org/5sxnReYOm)_. Wellington: ARANZ. Retrieved 23 September, 2010 from http://www.aranz.org.nz/Site/publications/position_papers/Archives_NZ_Review_.aspx (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5sxnReYOm)
*   Backus, E.B. (2006). Organizing electronic information to serve the needs of health practitioners and consumers. _Library Trends_, **54**(4), 607-619
*   Buckland, M. K. (1991). Information as thing. _Journal of the American Society for Information Science_, **42**(5), 351-360.
*   Dalton, P., Elkin, J. & Hannaford, A. (2006). Joint use libraries as successful strategic alliances. _Library Trends_, **54**(4), 535-548
*   Davenport, E. & Cronin, B. (2000). Knowledge management: semantic drift or conceptual shift. _Journal of Education in Library and Information Science_, , & (4), 294-306
*   Edwards, R.H. (1993). _Archivists' outlook on service to genealogists in selected Canadian provincial archives_. Vancouver, Canada: University of British Columbia.
*   Gorman, G.E. & Corbitt, B.J. (2002). Core competencies in inofrmation management education. _New Library World_, **103**, (11/12), 436-445\.
*   Hansson, J. (2006). Just collaboration or really something else? On joint use libraries and normative instituional change with two examples from Sweden. _Library Trends_, **54**(4), 549-568
*   Haycock, K. (2006). Dual use libraries: guidelines for success. _Library Trends_, **54**(4), 488-500
*   Kirk, J. (1999). [Information in organisations: directions for information management.](http://www.webcitation.org/5v924fSQr) _Information Research_, **4**(3). Available from http://informationr.net/ir/4-3/paper57.html (Archived by WebCite® at http://www.webcitation.org/5v924fSQr)
*   Knight, S. (2005). Preservation metadata: National Library of New Zealand experience. _Library Trends_, **54**(1), 91-110\.
*   Lewis, D.A. & Martin, W.J. (1989). Information management: state of the art in the United Kingdom. _ASLIB Proceedings_, **41**(7/8), 225-250
*   McNicol, S. (2006). Introduction. _Library Trends_, **54**(4), 485-487
*   The National Archives. (2009). _[3.4 million searches on www.1911census.co.uk in family history frenzy](http://www.webcitation.org/5sylSsxey)._ London: The National Archives. Retrieved 24 September, 2010 from http://www.nationalarchives.gov.uk/documents/14jan2009.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5sylSsxey)
*   New Zealand. _Minister of State Services_. (2010). _[Next steps in improving State Services performance](http://www.webcitation.org/5sxrSmyOW)._ Wellington: Minstry of State Services. Retrieved 28 April, 2010 from http://www.ssc.govt.nz/upload/downloadable_files/next-steps-in-improving-state-services-performance-march10.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5sxrSmyOW)
*   Oliver, G. (2004). Investigating information culture: comparative case study research design and methods. _Archival Science_, **4**(3-4), 287-314\.
*   Rowley, J. (1998). Towards a framework for information management. _International Journal of Information Management_, **18**(5), 359-369
*   Schauder, D., Stillman, L. &Johanson, G. (2005). [Sustaining and transforming a community network: the information continuum model and the case of VICNET](http://www.webcitation.org/5sxri3z1s). _Journal of Community Informatics_, 1(2). Retrieved 22 September, 2010 from http://ci-journal.net/index.php/ciej/rt/printerFriendly/239/203 (Archived by WebCite® at http://www.webcitation.org/5sxri3z1s)
*   Shepherd, E. (2009). _Archives and archivists in 20th century England_. Farnham, UK: Ashgate.
*   Sweeney, S. (2001). Merging or diverging? The creation of the information superprofessional. _Archifacts: Journal of the Archives and Records Association of New Zealand_, 24-34\.
*   Treloar, A., Groenewegen, D. &Harboe-Ree, C. (2007). [The data curation continuum: Managing data objects in institutional repositories.](http://www.webcitation.org/5swWN9gLx) _D-Lib Magazine_, **13**(9/10). Retrieved 22 September, 2010 from http://www.dlib.org/dlib/september07/treloar/09treloar.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5swWN9gLx)
*   Treloar, A. & Harboe-Ree, C. (2008). _[Data management and the curation continuum. How the Monash experience is informing repository relationships](http://www.webcitation.org/5swVLRfWZ)_. Paper presented at VALA 2008, Libraries/ changing spaces, virtual places. Retrieved 22 September, 2010 from http://www.valaconf.org.au/vala2008/papers2008/111_Treloar_Final.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5swVLRfWZ)
*   Tucker, S. (2007). Doors opening wider: library and archival services to family history. _Archivaria_, No. 62, 128-158
*   Upward, F. (2000). Modelling the continuum as paradigm shift in recordkeeping and archiving process, and beyond - a personal reflection. _Records Management Journal_, **10**(3), 115-139
*   Williams, R.V. (1998). The documentation and special libraries movements in the United States, 1910-1960\. In M.K. Buckland and T. B. Hahn (Eds.). _Historical studies in information science_ (pp. 173-179). Medford, NJ: Information Today
*   Van House, N. &Sutton, S. A. (1996). The panda syndrome: an ecology of LIS education. _Journal of Education for Library and Information Science_, **37**(2), 52-62