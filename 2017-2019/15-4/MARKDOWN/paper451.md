#### vol. 15 no. 4, December, 2010

# Cultural differences in the health information environments and practices between Finnish and Japanese university students

#### [Kreetta Askola](#author), [Toshimori Atsushi](#author) and [Maija-Leena Huotari](#author)  
Information studies, University of Oulu, Finland

#### Abstract

> **Introduction.** The aim of this study was to identify cultural differences in the information environment and information practices, namely active seeking and encountering, of web-based health information between Finnish and Japanese university students.  
> **Method.** The data were gathered with a Web-based survey among first-year university students at the University of Oulu, Finland, in 2007, and the University of Tsukuba, Japan, in 2008.  
> **Analysis.** The data were analysed quantitatively with SPSS 17.0 for Windows using mainly cross-tabulations and Chi-squared tests.  
> **Findings**. The statistically significant differences focused on the general information environment, and to the information practices used within several health information topics, including weight management related topics.  
> **Conclusion.** The cultural differences may influence the seeking of Web-based health information and should be examined more thoroughly in future studies, which should be apply both qualitative and quantitative methods.

## Introduction

The Internet has changed the way people of all ages seek information ([Rowlands _et al._ 2008](#row08)), and the ongoing paradigm shift in the realm of the Internet to Web 2.0, in other words, to more dynamic sources and interactive content, will change people’s information behaviour even more ([O’Reilly 2005](#ore05)). The Internet is a vast resource of health information and services. For example, in the context of weight management there is multitude of Internet-delivered interventions and tools (e.g., [Enwald and Huotari 2010](#enw10)). Issues related to information needs and reliability of Web-based health information have previously been extensively studied in various fields, including Information Studies, Communication Studies, and Nursing Science. There is, however, a great lack of empirical research on Web-based health information practices and related information environments. Moreover, empirical studies aiming to identify cultural differences in health information behaviour are rare.

This study aims to add to our knowledge of cultural differences in information behaviour. An earlier study on Web-based health information seeking among Finnish university students ([Askola and Huotari 2009](#ask09)) is expanded by including a Japanese sample in the analysis for purposes of comparison. The aim is to ascertain how the Internet is used as a health information environment in two different, but technologically advanced states, Finland and Japan.

We focus on three themes:

1.  Information environments: which sources form the a) general and b) Web-based health information environments of Finnish and Japanese university students?
2.  Information practices: is Web-based health information sought actively or encountered by chance? Which topical areas of health information are sought actively and which are encountered?
3.  Cultural differences: is it possible to identify cultural differences in the information behaviour of the Finnish and Japanese university students? If so, are these differences primarily related to the information environments or to the two types of information practices applied in this study?

This article begins with elaboration of Web-based health and information seeking, with special focus on culture and weight management and continues with the theoretical framework on health information environments and practices. This is followed by a description of the research setting. The findings focus on statistically significant findings and last the implications of the findings on cultural differences for the future research on Web-based, weight information are discussed.

## Web-based health information seeking and culture

The Internet is one of the most popular health information sources. Various features affect Web-based health information seeking. They include sex (e.g., [Lemire _et al._ 2008](#lem08)), level of education (e.g., [Tian and Robinson 2009](#tia09)), Internet self-efficacy ([Rains 2008](#rai08)) and eHealth literacy, i.e., the ability to use electronic health information resources when addressing a health problem ([Norman and Skinner 2006](#nor06)). Earlier research indicates that cultural issues also affect Web-based health information seeking (e.g., [Kakai _et al._ 2003](#kak03); [Peña-Purcell 2008](#pen08)).

In this study Finnish respondents are compared to Japanese respondents. It is important to note this distinction ([Ishida 1974](#ish74)). Japanese culture differs in various ways from Western cultures. These differences include, for example, emphasizing the significance of the social environment as a determinant of self, which emerges as self-effacing behaviour ([Heine _et al._ 2001](#hei01)). This collaterally emphasizes the welfare of the group and consensus among its members ([Caudill 1963](#cau63)), whether the group consists of the family members or workmates ([Minami 1971](#min71)). These features lead to a tendency to express feelings through action, not words ([Caudill 1963](#cau63)). Most of these features also appear to concern ethnic groups of Japanese origin, such as American-Japanese ([Kakai _et al._ 2003](#kak03)).

These differences are visible in practical matters of everyday life information behaviour, such as the use of the Internet. The user rates of the Internet are similar in Finland and Japan. More than 80% of Finns use the Internet regularly and the percentage of 15 to 29 year-old Internet users in Finland is almost 100 ([Statistics Finland 2007a](#sta07a)). In Japan about 75% of Japanese use the Internet and the user rate for younger population, aged 13 to 29, is over 95% ([Ministry of Internal Affairs… 2009](#min09)). However, there are some cultural differences in the use of the Internet. Japanese tend to use the Internet anonymously, for example to connect with the aid of the Internet to real-life strangers and to use the Internet primarily at home for private purposes, whereas Finns are more prone to make themselves known to other users and they also trust other users more. ([Kimura 2008](#kim08).)

It is claimed that Internet-based programmes and tools for the general public help people to lose weight if they are structured and offer personalized information on diet, exercise and behavioural strategies and are used frequently. Such programmes are rare, for most of them lack a structure or personalization elements. ([Saperstein _et al._ 2007](#sap07).) However, these programmes will be used and they are beneficial when the goal is minor weight loss. ([Williamson 2006](#wil06)). Also, in the context of the young, the goal set at mere weight maintenance and not weight loss may provide good results ([Jones _et al._ 2008](#jon08)). There is, however, a great lack of empirical research on Web-based health information practices and related information environments. Moreover, empirical studies aiming to identify cultural differences in health information behaviour are rare.

Despite the success of the Internet weight loss programmes, there are mixed findings on the efficacy of the Internet as a medium for weight maintenance ([Wadden 2007](#wad07)). Most of the findings indicate significant findings in weight loss, usually at six-month follow-up, but there is no evidence for weight maintenance. Most studies also report changes only in self-reported outcomes, such as opinions and eating habits. ([Bennett and Glasgow 2009](#ben09).) As in the case of weight loss, the outcomes of weight maintenance programmes are better if the intervention includes personal contacts with tailored information ([Wadden 2007](#wad07)).

## Health information environments and practices

In this paper we consider the Internet as an information channel for various types of information sources, from which the information user may choose suitable sources according his or her information need and preference. We do not seek to explore individual information source horizons (see [Sonnenwald 1999](#son99), [Savolainen and Kari 2004](#sav04)). Thus, we can use the term _the health information environment_, which we approach from two viewpoints. First, we examine it among other health information sources and second, we take a closer look at the health information sources on the Internet.

In this paper we examine cultural differences in information behaviour from the viewpoint of information seeking (see [Wilson 2000](#wil00)). We consider health information seeking as a part of everyday-life information seeking ([Savolainen 1995](#sav95); [Savolainen 2008](#sav08); [Spink and Cole 2001](#spi01)) and the concept of information practices as a sub-concept of information seeking. Our perspective on the concept of information practices derives from the framework of McKenzie ([2003](#mck03)) and is distinguished from the ideas of information practices presented by Savolainen ([2008](#sav08)). McKenzie ([2003](#mck03)) classifies information practices into four different modes: active seeking, active scanning, non-direct monitoring and by proxy. In this study we will apply two of these four information seeking practices for examination, namely, _active seeking_ and _encountering_, i.e., non-direct monitoring, of Web-based health information. Active seeking refers to situations in which a person looks for information with a particular question in mind, on the Internet. Encountering refers situations in which a person comes across health information on the Internet by chance.

We claim that this kind of approach to information practices is appropriate in this study. Models of Web-based information behaviour support this assumption by suggesting that the Internet is not used as often as an information source for formal searches as it is for more unconditioned seeking behaviour. For example, the model of Huang _et al._ ([2007](#hua07)) was created by analysing everyday life users’ use of the Internet. The model indicates that the more a user consumes the information within the Internet, the more likely he or she is to explore more categories, navigate more sites within a category, use fewer search engines, use more Web pages within a site and use them relatively quickly. Also, the behavioural model of information seeking on the Web, which was developed within the work context by Choo and colleagues ([1998](#cho98)), shows that even in a task-related environment the workers mostly used the modes of conditional viewing and informal search.

Even though information behaviour models usually fail to acknowledge information encountering, it is possible that some people do exploit it and may to some extent control it ([Foster and Ford 2003](#fos03)) or acquire information mostly in this way ([Erdelez 1997](#erd97), [1999](#erd99)). For the majority of Web users the Web provides a rich environment of information encountering opportunities ([Erdelez 1999](#erd99)). Moreover, information encountering has been shown to be recognized by varying groups of people: Icelandic health information seekers ([Pálsdóttir 2008](#pal08)), as well as Australian elderly people ([Williamson 1998](#wil98)), and scholars ([Foster and Ford 2003](#fos03)) use both purposive information seeking and incidental information acquisition as a means of information seeking.

## Research setting

The empirical study was conducted as a Web-based, structured survey for a convenience sample consisting of first-year university students in Finland and Japan. This target group was chosen to ensure that the respondents had easy access to the Internet and to increase the probability that they were used to dealing with the Internet. The Finnish sample consisted of all first-year students in the Faculty of Humanities and the Faculty of Science of the University of Oulu (N = 756). The Japanese sample consisted of all the first-year students (N = 101) in the College of Knowledge and Library Sciences, School of Informatics at the University of Tsukuba.

The Finnish sample was approached in November 2007 and the Japanese in November 2008 by an e-mail message that included a link to the Internet questionnaire. The Finnish respondents were offered an opportunity to take part in a lottery, the prizes being two memory sticks. The original survey was in Finnish and translated first into English and then into Japanese.

The surveys varied a little and the differences are explained next. The survey began with questions about the respondents’ background information regarding age, sex, faculty (only in Finland), assessments of health status and Internet skills and the average time spent on the Internet each week (only in Japan). Next, the health information sources used and preferences with regard to these indicated by the amount of use were elicited with closed questions. The Finnish respondents were then guided either to the end of the questionnaire or to continue answering depending on the use of the Internet as a health information source during the current year.

Web-based health information seeking was examined through the frequency of active seeking and/or accidental encountering. After this, the respondents were asked whether they sought or encountered information on various, stated health topics. There was an option to mention other topics as well. The Web-based health information source environment was examined by asking about the types of sites and health related services used. The Japanese survey also asked what sites the respondents had used after they had used a search engine. At the end of the questionnaire there was a set of twelve attitude statements with responses on a five-point Likert scale.

As there were differences the formulation of the questions the data from the two surveys were recoded in a uniform manner. Inadequate answers and '_no opinion_' answers were treated as missing responses. The data were analysed with SPSS 17.0 for Windows. The analysis consisted of a calculation of distributions, a dependency analysis using cross-tabulations and Chi-squaredd tests and, on one occasion, Fisher’s exact test between background variables and other variables separately. An eHealth literacy variable was constructed as a mean variable from attitude questions regarding eHealth literacy and this was analysed with Mann-Whitney dependency analysis. The findings were considered significant at the level _p_ = .005 or lower.

## Findings

### Background

The data were gathered from 371 students, of whom 19.4% (72) were from the University of Tsukuba, Japan and 81.6% (299) from the University of Oulu, Finland. The response rate was 39.5% in the Finnish sample and 71.3% in the Japanese sample. Even though the difference in the number of respondents between the countries differs a lot, it does not prevent statistical analysis.

Of the Finnish respondents 76.9% (230) were 18 to 22 years old, as was also the case with the Japanese respondents; only one Japanese respondent was over 22 years old. Of the Finnish respondents 71% were women and, of the Japanese sample, 68% . There was no statistically significant differences between the Finnish and Japanese respondents by sex (Χ² (1) = 1.055, _p_ = 0.304). Thus the samples are similar in sex and also sufficiently similar in the age of respondents. However, there were statistically significant differences in the respondents’ assessments of their Internet skills and also of their health status (Figure 1).

<div align="center">![Figure 1: Self-assessments of health status and Internet skills of the Finnish and Japanese respondents.](p451fig1.jpg)</div>

<div align="center">  
**Figure 1: Self-assessments of health status (%) and Internet skills (%) of the Finnish and Japanese respondents.**</div>

Even though the assessments of the two samples regarding their health status were quite similar, as most respondents assessed their health to be either average or good, the respondents of the Japanese sample assessed their health status statistically significantly lower (Χ² (3) = 13.0, _p_ = 0.005). This is mainly due to a greater share of those who assessed their health as poor. It must be noted that an earlier study comparing Japanese, British and Finnish employees also showed that the Japanese assessed their health status lower than the Finns or the British ([Martikainen _et al._ 2004](#mar04)).

The Japanese respondents estimated their Internet skills to be poorer than did the Finnish respondents (Χ² (3) = 101.65, _p_ = 0.000). Most of the Finnish respondents considered their Internet skills to be good, but most of the Japanese respondents assessed them to be satisfactory. In addition, over 25% of the Finnish respondents assessed their Internet skills to be excellent, while the same share of the Japanese respondents assessed them to be marginal. Such assessments of Internet skills of Japanese and Finnish students do not differ from those reported in earlier research ([Saito and Kimura 2008](#sai08)).

The difference in the assessments of the Internet skills is partially supported by the Mann-Whitney analysis of the eHealth literacy variable, which shows that the Finns assess their eHealth literacy related skills better (_p_ = .001). There was no statistically significant connection between the time spent on the Internet and the assessed Internet skills level in the Japanese sample according to Fisher’s exact test (_p_ = 0.128).

### Health information environments

The health information environments were examined on a general level and on the Internet. There are culturally interesting and statistically significant differences in the general information environments of the Finnish and Japanese university students. Figure 2 shows examples of health information sources and distributions of their use.

<div align="center">![](p451fig2.jpg)</div>

<div align="center">  
**Figure 2: Family, TV & radio, health care professionals and the Internet as health information sources (%) for Finnish and Japanese first year university students.**</div>

In the use of the health information sources most obvious differences were identified either in the categories '_a lot_' or '_not at all_'. For example, the Japanese respondents acquired more health information from their family (Χ² (3) = 23.63, _p_ = 0.000) and TV and radio (Χ² (3) = 8.45, _p_ = 0.038). The Finnish respondents, in turn, acquired more health information from health care professionals (Χ² (3) = 40.37, _p_ = 0.000) and the Internet (Χ² (3) = 10.88, _p_ = 0.012). The rates of Internet use for health information seeking (Japanese respondents 83.1%, Finnish respondents 84.5%) are somewhat higher than reported in earlier research (e.g., [Nicholas _et al._ 2003](#nic03); [Escoffery _et al._ 2005](#esc05); [Askola and Huotari 2009](#ask09); [Statistics Finland 2007b](#sta07b)). The statistics for all the sources asked about in the survey can be found in the [Appendix](#app).

In addition to the sources listed, only Finnish respondents mentioned other sources, for example personal sources (e.g., sports trainer) or institutional sources (e.g., military service) or the surrounding world (e.g., advertisements in the streets). Responses to the first statement regarding the trustworthiness of health information sources (see [Appendix](#app)) also reveal that the Japanese respondents trusted health information obtained from the family more than did the Finnish respondents (Χ² (3) = 39.79, _p_ = 0.000).

The findings indicate that general health information environments might differ depending on the nationality of the respondent. The Japanese respondents acquired health information through close interpersonal relationships and from electronic media and the Finnish respondents from professionals and printed media. This result is partially supported by a study on health information seeking among Hawaiian American-Japanese ([Kakai _et al._ 2003](#kak03)), which indicated that Japanese use more printed and electronic media while Caucasians use more Internet and professional sources.

Thus, characteristics of Finnish and Japanese culture may help to explain these differences. Japanese are more family-centric and may tend to use official health care just in serious matters. For Finns, by contrast, it may be more common also to use official health care services for minor health problems. Therefore, it is interesting to examine the Web-based health information sources of these two samples. The Web-based health information environments were examined by use of different types of information sources, namely the type of source (Figure 3) and health related interactive services.

<div align="center">![Figure 3: Health information source types (%) used by the Finnish and the Japanese university students.](p451fig3.jpg)</div>

<div align="center">  
**Figure 3: Health information source types (%) used by the Finnish and the Japanese university students.**</div>

All types of sites were used more by the Finnish respondents; however, the preferences regarding the use of these types are similar in both samples (Figure 3). A search engine was the most popular and chat the least popular type of information source in both samples. It must be noted that the sites of health care centres and other public health organisations were used statistically significantly more by the Finnish respondents (Χ² (1) = 6.84, _p_ = 0.009 and Χ² (1) = 17.69, _p_ = 0.000 respectively). The Finnish respondents also used discussion forums significantly more (Χ² (1) = 21.28, _p_ = 0.000).

Over 10% of the Finnish respondents also listed other types of sites in the open-ended questions. On these there is a clear tendency to acquire health information from commercial sources, such as the Websites of magazines or TV channels. Interestingly, Wikipedia was also mentioned by several respondents. The Japanese respondents did not provide alternative sources, but in the Japanese survey respondents were asked what types of sites they had found if they had used a search engine (n = 55): encyclopaedic sites like Wikipedia had been used by 45.5% and private or e-commerce sites by 32.7% of respondents.

The Web-based health information environment was also examined through the use of more interactive information sources, the Web-based health services, namely health related tests, calculators, competitions and Q&A services. The respondents were asked about their awareness and use of these services. At least 60% of the respondents in both samples were aware of all the services, apart from the Japanese respondents’ missing knowledge of health competitions. Overall the Finns were more aware of health related interactive services on the Internet and used them more.

The most notable difference was found in health related competitions, which only 20% of the Japanese respondents were aware of, compared to 75.5% of the Finns. Overall the different services are very well known. Interestingly, the Finns reported that the interactive features of Websites made the information acquired more memorable only slightly more than the Japanese respondents (Χ² (3) = 8.24, _p_ = .041) (see [Appendix](#app)). To sum up, these findings suggest that there is not as much difference on the Web-based health information environments of the two samples as there was in the general health information environments.

### Active seeking and encountering of Web-based health information

The Web-based health information practices of the respondents were identified by the frequency of either encountering or actively seeking of the information. The distributions of these frequencies are illustrated in Figure 4 [the response options were: not at all (1), a few times a year (2), once a month (3) a few times a month (4), weekly (5), several times a week (6).]

<div align="center">![](p451fig4.jpg)</div>

<div align="center">  
**Figure 4: Encountering and active seeking of health information (%) by the Finnish and Japanese first year university students.**</div>

There is a clear difference between the information practices examined in this study. Frequencies of Web-based health information encountering are quite evenly distributed between the classes and also between the samples (Χ² (5) = 4.05, _p_ = 0.542). On the other hand, the distribution of active seeking is much more skewed: in both samples most of the active seeking occurs only a few times a year. This means that Web-based health information is encountered more often than it is actively sought. In other words, the respondents tend to encounter Web-based health information and deliberately seek it only when they or significant others fall ill.

Although the frequencies of the active seeking are also quite similar, the difference in class “Not at all” is notable. The Chi-Square test could not be performed, but it is evident that the Finnish respondents acquire more Web-based health information by active seeking than do the Japanese respondents. The distributions of the statement regarding the primary source for health information also indicate that the Finnish respondents seek information more on the Internet (Χ² (3) = 14.69, _p_ = 0.002) (see [Appendix](#app)).

Web-based health information practices were also examined through several health information topics. There were statistically significant differences in the topics of sickness (Χ² (1) = 18.38, _p_ = .000), symptoms (Χ² (1) = 37.08, _p_ = 0.000) and medication (Χ² (1) = 8.11, _p_ = 0.004), which the Japanese respondents reportedly encountered more. The Finnish respondents sought actively more information on mental health (Χ² (1) = 5.70, _p_ = 0.017) and sexual health (Χ² (1) = 9.31, _p_ = 0.002). These differences in the last topics could be explained at least partially by the Japanese tendency to make sensitive issues such as sickness as invisible as possible ([Kakai _et al._ 2003](#kak03)).

Our interest is focused on the weight-management-related topics. Even though the survey did not directly address issues related to weight management, the topics of nutrition and exercise are examined in a more detailed manner. There were statistically significant differences in both topics (Table1).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 1\. Information practices and weight management related information topics.  
(* = _p_ < 0.05)**</caption>

<tbody>

<tr>

<th rowspan="3"> </th>

<th colspan="4">Encountering</th>

<th colspan="4">Active seeking</th>

</tr>

<tr>

<th colspan="2">Japanese</th>

<th colspan="2">Finnish</th>

<th colspan="2">Japanese</th>

<th colspan="2">Finnish</th>

</tr>

<tr>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<th>Nutrition</th>

<td align="center">39</td>

<td align="center">61</td>

<td align="center">83</td>

<td align="center">36</td>

<td align="center">29</td>

<td align="center">52</td>

<td align="center">124</td>

<td align="center">53</td>

</tr>

<tr>

<th>Exercise</th>

<td align="center">16</td>

<td align="center">25</td>

<td align="center">73</td>

<td align="center">32</td>

<td align="center">14</td>

<td align="center">25</td>

<td align="center">114*</td>

<td align="center">49*</td>

</tr>

</tbody>

</table>

The Table 1 shows that the Japanese respondents encountered statistically significantly more information on nutrition (Χ² (1) = 13.11, _p_ = 0.000) and the Finnish respondents sought actively more information on exercise (Χ² (1) = 10.65, _p_ = 0.001). The number of Finnish respondents engaged in active seeking of both nutrition and exercise related information both topics is similar to a British study ([Marshall _et al._ 2009](#mar09)).

## Discussion

This study aimed to identify cultural differences in the information behaviour of Finnish and Japanese first-year university students by conducting a Web-based survey at the University of Oulu, Finland and the University of Tsukuba, Japan. The analysis focused on differences in the overall and Web-based health information environments, in the information seeking practices of active seeking and of encountering and in their relation to topical areas of health information, with special interest on weight-management-related topics.

The statistically significant differences indicate that there are cultural differences in information seeking behaviour between Finnish and Japanese first year university students. The two samples are similar regarding sex and also sufficiently similar regarding age of respondents. However, there were statistically significant differences in the respondents’ self-assessments of their Internet skills and health status. The Finnish respondents assessed their health status and Internet skills statistically significantly higher than did the Japanese respondents. These findings are supported by earlier research, which has also indicated that the Japanese are self-critical when evaluating self-relevant information ([Heine _et al._ 2001](#hei01)). The same applies also to our findings.

Moreover, there are differences in the health information environments examined on a general level and on the Internet by analysing the use of different types of information sources, namely, the type of source and health related interactive services. At the general level statistically significant differences indicate that the Japanese respondents used more intrapersonal sources and electronic media and the Finnish respondents used more health care professionals and printed media. This difference was identified on the basis of the amount of information gained from a source. The characteristics of Finnish and Japanese cultures may help to explain these differences. Japanese are more family-centred and may tend to use official health care only for serious matters. For Finns, by contrast, it may be more common to use official health care services even for minor health problems. It must be noted, however, that both groups used all the sources mentioned in the survey.

The cultural differences regarding the Web-based health information environments focused on the variety of information sources and the awareness of information services on the Web. Sites of health care centres and other public health organisations were used statistically significantly more by the Finnish respondents, who also used significantly more discussion forums. The absence of social media sources in the respondents’ answers to open-ended questions in both Web-based health information environments is noteworthy. The use of discussion boards, in turn, may indicate that health information is also acquired by using social Web applications.

Overall, the Finns used more types of information sources and were more aware of Web-based health information services and the health competitions on the Web were used only by the Finnish students. This result differs from earlier research by Kimura ([2008](#kim08)), who reported that Finnish university students use the Internet more for communicating with other people, for retrieving news and voicing their opinions and the Japanese students for entertainment. This may indicate that there is a difference in the content of health services provided for Finns and Japanese on the Web.

The information practices examined in this study, active seeking and encountering, did not differ statistically significantly between samples. Despite this, the information practices in seeking Web-based health information varied. The Finnish respondents both actively sought and encountered Web-based health information more often, which is supported by the finding that Finnish respondents obtain information more often from the Internet. The most notable result related to information seeking practices was the large proportion of Japanese respondents who did not seek health information actively at all. Web-based health information was encountered by chance more often than actively sought in both samples and the relatively high rates in encountering information in both samples indicate that health information is well provided and easily accessed on the Web.

The information practices used varied by topical areas of health information acquired and the nationality of the respondent. These findings indicate that Japanese respondents acquire more health information from the Web by encountering it even when satisfying the most common information needs. The Finns, in turn, are more likely to actively seek information on sensitive topics. There were also statistically significant differences regarding weight management related topics. Difference in information practices related to the topic of nutrition could be explained as different Web content in each language. For example, it is assumed that there are more nutrition related commercials in Japanese media than in Finland. The difference in exercise may be related to differing perceptions of physical exercise. Due to a lack of more detailed questions in the survey, these remarks should be treated with caution.

This study has provided some indications for future studies in the area of Web-based weight management information use. Two crucial questions emerged from the findings: 1) Were there differences in the health information content in each language? 2) Did the Japanese and Finnish respondents answer or interpret questions differently? 3) How does the potentially different use of Web-based services and social media applications influence the use of Web-based weight management information? It is difficult to answer these questions with quantitative research and weight management is an even more sensitive issue than general health information. These issues emphasize the need to use a qualitative approach. However, it is also difficult to conduct a study using qualitative methods in different countries with different languages. The authors will focus on Web-based weight management information in just one country first qualitatively and after gaining more knowledge on the subject, conduct a new quantitative, cross-cultural study on the same issue.

This study has the following limitations: First, although the authors aimed at an objective approach, it is acknowledged that their own culture may have influenced the interpretation of the findings. Secondly, the data were collected with a Web-based survey by approaching the respondents by e-mail. Thus, the respondents form a convenience sample, which limits the generalization of the findings. For example, the data are biased towards Internet users and women. On the other hand, the respondents form a quite homogeneous sample in terms of their age, sex and life situation. The lower response rate in the Finnish sample, however, could indicate a bias towards more health concerned or orientated respondents.

## Conclusion

This study contributes to the field of Information Studies by examining cultural differences between Finnish and Japanese university students’ general and Web-based health information environments, related information seeking practices and the topical areas of information content searched for.

Statistically significant cultural differences were identified and they focused on the general health information environments and topical areas of health information. However, on the general level no statistically significant differences were identified in the use of the two types of information seeking practices examined in this comparative analysis. Therefore, more empirical studies are needed to identify in more detail the effect of cultural issues on Web-based health information behaviour and to improve the reliability of the findings by applying more elaborate research methods.

## References

*   Askola, K. & Huotari, M.-L. (2009). University students as health information seekers on the Internet. In P. Bath, G. Petterson & T. Steinschaden, (Eds.), _Evaluation and implementation of e-health and health information iniatiatives: international perspective. Proceedings of the 14th International Symposium on Health Information Management Research - ISHIMR 2009._ (pp. 171-180). Kalmar: University of Kalmar
*   Bennett, G.V. & Glasgow, R.E. (2009). The delivery of public health interventions via the Internet: actualizing their potential. _Annual Review of Public Health_, **30**(1), 273-292.
*   Caudill, W. (1963). Patterns of emotion in modern Japan. In R.J. Smith & R. Beardsley, (Eds.), _Japanese culture: its development and characteristics_. New York, NY: Wenner-Gren Foundation for Anthropological Research
*   Choo, C.W., Detlor, B. & Turnbull, D. (1998). [A behavioral model of information seeking on the Web: preliminary findings of a study of how managers and it specialists use the Web.](http://www.webcitation.org/5uvMhn25K) In Cecilia M. Preston, (Ed.), _Proceedings of the 61st Annual Meeting of the American Society for Information Science._ (pp. 290-302.) Silver Spring, MS: American American Society for Information Science and Technology. Retrieved August, 2009 from http://www.ischool.utexas.edu/~donturn/papers/asis98/asis98.html (Archived by WebCite® at http://www.webcitation.org/5uvMhn25K)
*   Enwald, H. & Huotari, M.-L. (2010). [Preventing the obesity epidemic by second generation tailored health communication: an interdisciplinary review.](http://www.webcitation.org/5uvOkeuSE) _Journal of Medical Internet research_ , **12**(2), e24\. Retrieved October, 2010 from http://www.jmir.org/2010/2/e24/ (Archived by WebCite® at http://www.webcitation.org/5uvOkeuSE)
*   Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information seeking in context. Proceedings of an international conference on research in information needs, seeking and use in different contexts. (14-16 August, 1996, Tampere, Finland)._ (pp. 412-241.) London: Taylor Graham
*   Erdelez, S. (1999). [Information encountering: it's more than just bumping into information.](http://www.webcitation.org/5uvOt8eSE) _Bulletin of the American Society for Information Science_, **25**(3). Retrieved August, 2009 from http://www.asis.org/Bulletin/Feb-99/erdelez.html (Archived by WebCite® at http://www.webcitation.org/5uvOt8eSE)
*   Escoffery, C., Miner, K.R., Adame, D.D., Butler, S., McCormick, L. & Mendell, E. (2005). Internet use for health information among college students. Journal of American College Health, **53**(4), 183-188.
*   Foster, A. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation_ **59**(3), 321-340.
*   Heine, S.J., Kitayama, S. & Lehman, D.R. (2001). Cultural differences in self-evaluation: Japanese readily accept negative self-relevant information. _Journal of Cross-Cultural Psychology_, **43**(4), 434-443
*   Huang, C.Y., Shen, Y.C., Chiang, I.P. & Lin, C.S. (2007). Characterizing Web users' online information behavior. _Journal of the American Society for Information Science and Technology_, **58**(13), 1988-1997.
*   Ishida, E. (1974). _Japanese culture : a study of origins and characteristics_. Tokyo: Tokyo University Press
*   Jones, M., Luce, K.H., Osborne, M.I., Taylor, K., Cunning, D., Doyle, A.C., Wilfrey, D.E. & Taylor, C.B. (2008). Randomized, controlled trial of an internet-facilitated intervention for reducing binge eating and overweight in adolescents. _Pediatrics_ **121**(3), 453-62.
*   Kakai, H., Maskarinec, G., Shumay, D.M., Tatsumura, Y. & Tasaki K. (2003). Ethnic differences in choices of health information by cancer patients using complementary and alternative medicine: an exploratory study with correspondence analysis. _Social Science & Medicine_ **56**, 851-862.
*   Kimura, T. (2008). [Cyberspace as socio-psychological space: cross-cultural comparison among the Japanese, Koreans and Finns](http://www.webcitation.org/5uvPBd4qD). _Journal of socio-informatics_ **1**(1), 57-70\. Retrieved 11 December, 2010 from http://www.soc.nii.ac.jp/jasi/eng/pdf/Journal_of_Socio-Informatics_Vol1_No1.pdf (Archived by WebCite® at http://www.webcitation.org/5uvPBd4qD)
*   Lemire, M., Paré, G., Sicotte, C. & Harvey, C. (2008) Determinants of Internet use as a preferred source of information on personal health. _International Journal of Medical Informatics_, **77**(11), 723-734
*   Marshall, A., Henwood, F., Carlin, L., Guy, E.S., Sinozic, T. & Smith, H. (2009). [Information to fight the flab: findings from the Net.Weight Study](http://www.webcitation.org/5uvPJqLbr). _Journal of Information Literacy_, **3**(2), 39-52\. Retrieved 11 December, 2010 from http://ojs.lboro.ac.uk/ojs/index.php/JIL/article/view/PRA-V3-I2-2009-3/1415 (Archived by WebCite® at http://www.webcitation.org/5uvPJqLbr)
*   Martikainen, P., Lahelma, E., Kagamimori, S., Sekine, M., Nishi, N., & Marmot, M. (2004). A comparison of socio-economic differences in physical functioning and perceived health among male and female employees in Britain Finland and Japan. _Social Science & Medicine_ **59**, 1287-1295.
*   McKenzie, P.J. (2003). A model of information practices in accounts of everyday-life information seeking. _Journal of Documentation_, **59**(1), 19-40.
*   Minami, H. (1971). _Psychology of Japanese people._ Tokyo: Tokyo University Press
*   Japan. _Ministry of Internal Affairs and Communications_. (2009). _[Communications usage trend survey in 2008 compiled](http://www.webcitation.org/5uvPeKhgt)_. Tokyo: Ministry of Internal Affairs and Communications. Retrieved 14 November 2009 from http://www.soumu.go.jp/johotsusintokei/tsusin_riyou/data/eng_tsusin_riyou2008.pdf (Archived by WebCite® at http://www.webcitation.org/5uvPeKhgt)
*   Nicholas, D., Huntington, P., Gunter, B., Russell, C. Y. & Withey, R. (2003). The British and their use of the Web for health information and advice: a survey. _Aslib Proceedings_ **55**(5/6), 261-276.
*   Norman, C. & Skinner, H. (2006). [eHEALS: the eHealth literacy scale.](http://www.webcitation.org/5uvPmPWKb) _Journal of Medical Internet Research_. **8**(4), e27\. Retrieved August, 2009 from http://www.jmir.org/2006/4/e27/ (Archived by WebCite® at http://www.webcitation.org/5uvPmPWKb)
*   O'Reilly, T. (2005). [What is Web 2.0?](http://www.Webcitation.org/5u9jRqwD8) Sebastopol, CA: O'Reilly. Retrieved August, 2009 from http://oreilly.com/Web2/archive/what-is-Web-20.html (Archived by WebCite© at http://www.Webcitation.org/5u9jRqwD8)
*   Pálsdóttir, Á. (2008). [Information behaviour, health self-efficacy beliefs and health behaviour in Icelanders' everyday life](http://www.Webcitation.org/5u9jc0Yrc). _Information Research_ **13**(1), paper 334\. Retrieved May, 2009 from http://informationr.net/ir/13-1/paper334.html (Archived by WebCite© at http://www.Webcitation.org/5u9jc0Yrc)
*   Peña-Purcell, N. (2008) Hispanics' use of Internet health information: an exploratory study. _Journal of Medical Library Association_ **96**(2),101-107.
*   Rains, S.A. (2008). Seeking health information in the information age: the role of Internet self-efficacy. _Western Journal of Communication_ **72**(1), 1-18.
*   Rowlands, I., Nicholas, D., Williams, P., Huntington, P., Fieldhouse, M., Gunter, B., Withey, R., Jamali, H.R., Dobrowoiski, T. Y. & Tenopir, C. (2008). The Google generation: the information behaviour of the researcher of the future. _Aslib Proceedings_ **60**(4), 290-310.
*   Saito, Y. Y. & Kimura, T. (2008). [Socio-cultural differences in the use of personal Web homepage and electronic communities among Japanese, Finnish and Korean youth.](http://www.webcitation.org/5uvPBd4qD) _Journal of Socio-informatics_, **1**(1), 137-146\. Retrieved March, 2010 from http://www.soc.nii.ac.jp/jasi/eng/pdf/Journal_of_Socio-Informatics_Vol1_No1.pdf (Archived by WebCite® at http://www.webcitation.org/5uvPBd4qD)
*   Saperstein, S., Atkinson, N. Y. & Gold R. (2007). The impact of Internet use for weight loss. _Obesity reviews : an official journal of the International Association for the Study of Obesity_, **8**(5), 459-465.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of way of life. _Library and Information Science Research_, **17**(3), 259-294.
*   Savolainen, R. (2008). _Everyday information practices: a social phenomenological perspective._ Lanham, MD: Scarecrow Press
*   Savolainen, R. Y. & Kari, J. (2004). Placing the Internet in information source horizons. A study of information seeking by Internet users in the context of self-development. _Library & Information Science Research_, **26**(4), 415-433.
*   Sonnenwald, D.H. (1999). Evolving perspectives of human information behaviour: contexts, situations, social networks and information horizons. In T.D. Wilson, & D.K. Allen, (Eds.), _Exploring the contexts of information behavior: proceedings of the second international conference in information needs, seeking and use in different contexts._ (pp. 176-190.) London: Taylor Graham
*   Spink, A.Y. & Cole, C. (2001). Everyday life information seeking research. _Library and Information Science Research_, **23**(4), 301-304.
*   Statistics Finland. (2007a) _[Internet users, spring 2001 to spring 2007, percentage of 15 to 74-year-olds by age group](http://www.webcitation.org/5uvQNTjAi)_. Helsinki: Statistics Finland. Retrieved October, 2009 from http://stat.fi/til/sutivi/2007/sutivi_2007_2007-09-28_kuv_004_en.html (Archived by WebCite® at http://www.webcitation.org/5uvQNTjAi)
*   Statistics Finland (2007b) _[Purposes of the use of the Internet in spring 2007, per cent of Internet users](http://www.webcitation.org/5uvQVdVdn)_. Helsinki: Statistics Finland. Retrieved October, 2009 from http://stat.fi/til/sutivi/2007/sutivi_2007_2007-09-28_tau_001_en.html Archived by WebCite at http://www.webcitation.org/5uvQVdVdn)
*   Tian, Y.Y. & Robinson, J.D. (2009). Incidental health information use on the Internet. _Health Communication_, **24**(1), 41-49.
*   Wadden, T. A. (2007). Lifestyle modification for the management of obesity. _Gastroenterology_ **132**(6), 2226-38.
*   Williamson, K. (1998). Discovered by chance: the role of incidental information acquisition in an ecological model of information use _Library & Information Science Research_, **20**(1), 23-40.
*   Williamson, D.A. (2006) Two-year internet-based randomized controlled trial for weight loss in African-American girls. _Obesity_ **14**(7), 1231-1243
*   Wilson, T.D. (2000). [Human information behavior.](http://www.webcitation.org/5uvQgFvQP) _Informing Science_ **3**(2), 49-55\. Retrieved 11 December, 2010 from http://informationr.net/tdw/publ/papers/2000HIB.pdf (Archived by WebCite® at http://www.webcitation.org/5uvQgFvQP)

## Appendix

### **Health information sources**

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
</caption>

<tbody>

<tr>

<th rowspan="2">Health information source</th>

<th rowspan="2">Category</th>

<th colspan="2">Japanese</th>

<th colspan="2">Finnish</th>

<th rowspan="2">_p_-level</th>

</tr>

<tr>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td rowspan="4">Family</td>

<td>Not at all</td>

<td align="center">3</td>

<td align="center">4.2</td>

<td align="center">33</td>

<td align="center">9.9</td>

<td align="center" valign="top" rowspan="4">0.000</td>

</tr>

<tr>

<td>A little</td>

<td align="center">17</td>

<td align="center">23.9</td>

<td align="center">79</td>

<td align="center">36.4</td>

</tr>

<tr>

<td>Some</td>

<td align="center">16</td>

<td align="center">2.5</td>

<td align="center">117</td>

<td align="center">36.5</td>

</tr>

<tr>

<td>A lot</td>

<td align="center">35</td>

<td align="center">49.3</td>

<td align="center">64</td>

<td align="center">27.2</td>

</tr>

<tr>

<td rowspan="4">Friends, etc.</td>

<td>Not at all</td>

<td align="center">8</td>

<td align="center">11.1</td>

<td align="center">27</td>

<td align="center">9.3</td>

<td align="center" rowspan="4" valign="top">0.499</td>

</tr>

<tr>

<td>A little</td>

<td align="center">22</td>

<td align="center">30.6</td>

<td align="center">93</td>

<td align="center">32.0</td>

</tr>

<tr>

<td>Some</td>

<td align="center">27</td>

<td align="center">37.5</td>

<td align="center">129</td>

<td align="center">44.3</td>

</tr>

<tr>

<td>A lot</td>

<td align="center">15</td>

<td align="center">20.8</td>

<td align="center">42</td>

<td align="center">14.4</td>

</tr>

<tr>

<td rowspan="4">Healthcare professionals</td>

<td>Not at all</td>

<td align="center">35</td>

<td align="center">50.0</td>

<td align="center">53</td>

<td align="center">24.1</td>

<td align="center" rowspan="4" valign="top">0.000</td>

</tr>

<tr>

<td>A little</td>

<td align="center">21</td>

<td align="center">30.0</td>

<td align="center">75</td>

<td align="center">26.3</td>

</tr>

<tr>

<td>Some</td>

<td align="center">11</td>

<td align="center">15.7</td>

<td align="center">107</td>

<td align="center">32.3</td>

</tr>

<tr>

<td>A lot</td>

<td align="center">3</td>

<td align="center">4.3</td>

<td align="center">60</td>

<td align="center">20.3</td>

</tr>

<tr>

<td rowspan="4">Books, journals</td>

<td>Not at all</td>

<td align="center">10</td>

<td align="center">14.3</td>

<td align="center">33</td>

<td align="center">11.9</td>

<td align="center" rowspan="4" valign="top">0.021</td>

</tr>

<tr>

<td>A little</td>

<td align="center">33</td>

<td align="center">47.1</td>

<td align="center">88</td>

<td align="center">33.4</td>

</tr>

<tr>

<td>Some</td>

<td align="center">14</td>

<td align="center">20.0</td>

<td align="center">99</td>

<td align="center">31.2</td>

</tr>

<tr>

<td>A lot</td>

<td align="center">13</td>

<td align="center">18.6</td>

<td align="center">72</td>

<td align="center">23.5</td>

</tr>

<tr>

<td rowspan="4">TV, radio</td>

<td>Not at all</td>

<td align="center">16</td>

<td align="center">22.2</td>

<td align="center">62</td>

<td align="center">21.7</td>

<td align="center" width="158" rowspan="4" valign="top">0.038</td>

</tr>

<tr>

<td>A little</td>

<td align="center">19</td>

<td align="center">26.4</td>

<td align="center">105</td>

<td align="center">34.5</td>

</tr>

<tr>

<td>Some</td>

<td align="center">20</td>

<td align="center">27.8</td>

<td align="center">88</td>

<td align="center">30.1</td>

</tr>

<tr>

<td>A lot</td>

<td align="center">17</td>

<td align="center">23.6</td>

<td align="center">32</td>

<td align="center">13.6</td>

</tr>

<tr>

<td rowspan="4">Internet</td>

<td>Not at all</td>

<td align="center">12</td>

<td align="center">16.9</td>

<td align="center">45</td>

<td align="center">15.8</td>

<td align="center" width="158" rowspan="4" valign="top">0.012</td>

</tr>

<tr>

<td>A little</td>

<td align="center">21</td>

<td align="center">29.6</td>

<td align="center">53</td>

<td align="center">20.5</td>

</tr>

<tr>

<td>Some</td>

<td align="center">29</td>

<td align="center">40.8</td>

<td align="center">103</td>

<td align="center">36.6</td>

</tr>

<tr>

<td>A lot</td>

<td align="center">9</td>

<td align="center">12.7</td>

<td align="center">89</td>

<td align="center">27.1</td>

</tr>

</tbody>

</table>

### 2) Selected statement questions

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
</caption>

<tbody>

<tr>

<th>Statement</th>

<th>Category</th>

<th colspan="2" valign="top">Japanese</th>

<th colspan="2" valign="top">Finnish</th>

<th>_p_-level</th>

</tr>

<tr>

<th> </th>

<th> </th>

<th align="center">n</th>

<th align="center">%</th>

<th align="center">n</th>

<th align="center">%</th>

<th> </th>

</tr>

<tr>

<td width="167" rowspan="4" valign="top">I trust information obtained from my family and friends more than health information obtained on the Internet.</td>

<td width="139" valign="top">Completely disagree</td>

<td align="center">1</td>

<td align="center">1.6</td>

<td align="center">7</td>

<td align="center">3.2</td>

<td align="center" colspan="2" rowspan="4" valign="top">0.000</td>

</tr>

<tr>

<td width="139" valign="top">Partly disagree</td>

<td align="center">9</td>

<td align="center">14.5</td>

<td align="center">111</td>

<td align="center">51.4</td>

</tr>

<tr>

<td width="139" valign="top">Partly agree</td>

<td align="center">34</td>

<td align="center">54.8</td>

<td align="center">84</td>

<td align="center">38.9</td>

</tr>

<tr>

<td width="139" valign="top">Completely agree</td>

<td align="center">18</td>

<td align="center">290.0</td>

<td align="center">14</td>

<td align="center">6.5</td>

</tr>

<tr>

<td width="167" rowspan="4" valign="top">Information acquired through interactive Internet services is easy to remember.</td>

<td width="139" valign="top">Completely disagree</td>

<td align="center">5</td>

<td align="center">8.5</td>

<td align="center">12</td>

<td align="center">6.2</td>

<td align="center" colspan="2" rowspan="4" valign="top">0.041</td>

</tr>

<tr>

<td width="139" valign="top">Partly disagree</td>

<td align="center">21</td>

<td align="center">35.6</td>

<td align="center">56</td>

<td align="center">28.9</td>

</tr>

<tr>

<td width="139" valign="top">Partly agree</td>

<td align="center">19</td>

<td align="center">32.2</td>

<td align="center">101</td>

<td align="center">52.1</td>

</tr>

<tr>

<td width="139" valign="top">Completely agree</td>

<td align="center">14</td>

<td align="center">23.7</td>

<td align="center">25</td>

<td align="center">12.9</td>

</tr>

<tr>

<td width="167" rowspan="4" valign="top">I obtain health information primarily on the Internet.</td>

<td width="139" valign="top">Completely disagree</td>

<td align="center">21</td>

<td align="center">30.9</td>

<td align="center">34</td>

<td align="center">150.0</td>

<td align="center" colspan="2" rowspan="4" valign="top">0.001</td>

</tr>

<tr>

<td width="139" valign="top">Partly disagree</td>

<td align="center">23</td>

<td align="center">33.8</td>

<td align="center">56</td>

<td align="center">24.7</td>

</tr>

<tr>

<td width="139" valign="top">Partly agree</td>

<td align="center">21</td>

<td align="center">30.9</td>

<td align="center">96</td>

<td align="center">42.3</td>

</tr>

<tr>

<td width="139" valign="top">Completely agree</td>

<td align="center">3</td>

<td align="center">4.4</td>

<td align="center">41</td>

<td align="center">18.1</td>

</tr>

</tbody>

</table>