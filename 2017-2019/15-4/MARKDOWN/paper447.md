#### vol. 15 no. 4, December, 2010

# Selecting and using information sources: source preferences and information pathways of Israeli library and information science students

#### [Jenny Bronstein](#author)  
Bar Ilan University, Ramat Gan, 52900, Israel

#### Abstract

> **Introduction.** The study investigated the source preference criteria of library and information science students for their academic and personal information needs.  
> **Method.** The empirical study was based on two methods of data collection. Eighteen participants wrote a personal diary for four months in which they recorded search episodes and answered an open-ended questionnaire.  
> **Analysis.** First, data were collected concerning the participants' preferences for information sources and the types of issues that brought them to look for information. Secondly, data from participants' answers to the open ended questionnaire were analysed using the content analysis method. The third phase of the process identified the sequences or pathways of information sources used by participants.  
> **Results.** The main preference criterion for networked sources was accessibility which was perceived as easiness of use, time saving, language and physical proximity. The main preference criterion for human sources was the quality and reliability of the information provided by the source.  
> **Conclusions.** Diaries proved to be a valuable tool for getting a glimpse into the participants' thought processes and decision making, while the researchers with an overall view of source preferences. The concept of information pathways further clarified source preferences.

## Introduction

Wilson defines information seeking behaviour as,

> the purposive seeking for information as a consequence of a need to satisfy some goal. In the course of seeking, the individual may interact with manual information systems (such as a newspaper or a library), or with computer-based systems (such as the World Wide Web). ([Wilson 2000](#WI00): 49).

The interactions users choose to make with information sources, or in other words, their preferences for some information sources over others available to them are of great significance to the understanding of users' information seeking behaviour; this understanding can result in the development and provision of information services that better serve users' information needs. The purpose of this study is to examine the different criteria by which users value and prioritize information sources and to identify how these criteria influence the sequences or pathways in which users choose to use information sources. The present study investigated the information seeking behaviour of library and information science students as recorded by them in a diary and in a subsequent open-ended questionnaire. This reflective method of investigation provides an innovative viewpoint to the users' own perception and evaluation of their use of the information sources both for personal and for academic purposes.

## Literature review

The reasons users select a specific information source have become of great importance because of developments in the field of information in the past decades. Information source preferences of users have been affected by the development of computers and telecommunications technologies, the information explosion and the availability of a whole range of modern information technologies for the efficient use of information resources One consistent finding in the literature has been that, in most cases, when choosing among options available to them, users will base their decision upon the single criterion of least average rate of probable work ([Allen 1977](#AL77), [Anderson _et al._ 2001](#AN01), [Culnan 1983](#CU83), [O'Reilly 1982](#OR82)). This theory was first proposed by [Gertsberger and Allen (1968)](#GE68) and is based on Zipf's law of Least Effort [(Zipf 1949)](#ZI49). Allen ([1977](#AL77)) found that in selecting information sources, engineers act in a manner that is intended not to maximize gain but rather to minimize loss. The loss to be minimized is the cost in terms of effort, either physical or psychological, which must be expended in order to gain access to an information source. Accessibility is intended as a measure of the perceived cost associated with the use of a source and is better related to the frequency of use than the quality of the information.

The notion of accessibility has been a central idea in the study of source preference in the literature and it has been defined in different ways over the years. Several studies have found that information sources that are easier to use are perceived as more accessible and will be used more frequently than less easy to use sources. This premise explains the users' preferences for easy to use and accessible sources such as informal communications and personal collections ([Anderson _et al._ 2001](#AN01); [Green 2000](#GR00); [Leckie _et al._ 1996](#LE96); [Von Seggern 1995)](#VO95). Besides ease of use, time constraints appear to be an influential parameter in the accessibility of the source ([Savolainen and Kari 2004](#SA04)).

In a study on accessibility Fidel and Green ([2004](#FI04)) uncovered twelve different meanings of the concept of accessibility such as physical proximity, interactivity and previous knowledge of the source among others.

Although many of these studies have found that the perceived quality of the information in the source is unrelated to its use, a number of studies challenge this finding (see [Bronstein and Baruchson 2008](#BR08)). Orr ([1970](#OR70)) argued that the quality of the information is the most important consideration in selecting an information source when the information sources available to users are equivalent in their information-yielding potential. Concurring with this, Swanson ([1987](#SW87)) asserted that when the ability of one source to substitute another is rather low, the quality of information becomes a decisive criterion of selection. Marton and Choo ([2002](#MA02)) found a strong relationship between the perceived quality of information sources and their use. This relationship was attributed to the need of women in information technology professions to overcome information overload by using those information sources that could provide them with the most relevant information. Other studies claimed that as physical access to information becomes more widespread through networks it seems reasonable to expect users to distinguish between sources on the basis of quality rather than accessibility ([Auster and Choo 1993](#AU93); [Klobas 1995)](#KL95).

Hertzum and colleagues established that '_the perceived quality of a source or piece of information is essentially a matter of establishing to what extent one is willing to place trust in it_' ([Hertzum _et al._ 2002](#HE02): 2). They defined trust as an assumption of risk and depending on the nature of this risk; trustworthiness may mean discretion, reliability, competence, integrity or empathy. Furthermore, to the user, trust involves an assessment of whether the other person possesses the required level of knowledge and skills to fulfill the user's information need. The issue of trust was revealed as a major criterion for participants in this study particularly with regards to human information sources.

Findings in this study will show that both the accessibility of the information source and the quality of its information are relevant criteria when selecting information sources.

The second aim of this study was to identify and understand the sequence or pathway in which participants used information sources. A pathway is a route someone follows in the pursuit of answers to questions ([Johnson _et al._ 1996](#JO06)). The notion of information pathways was developed by Johnson on the idea of the _information field_ which '_represents the typical arrangement of information stimuli to which an individual is daily exposed_' ([Johnson 2003](#JO03): 750). While the notion of an information field describes a static situation, an information pathway represents the active choices of this individual regarding the information sources s/he prefers within this specific information field ([Savolainen 2008)](#SA08).

For example: <span style="font-family: monospace;">consulting a librarian⇒searching Google⇒searching an academic database</span>.

Users can select different information paths for different information needs, but Savolainen claims that '_individuals may follow habitual pathways within a field_' ([Savolainen 2008:](#SA08) 278). This study investigated the information pathways described in the participants' diaries and attempted to uncover the criteria behind the construction of these pathways

## The study

### Research questions

This study addresses the following questions:

1.  What kind of information sources participants used both for academic and for personal purposes?
2.  According to which criteria do participants select the information sources they use when searching for information?
3.  What kind of information pathways, that is sequences of sources, do participants use when searching for information?
4.  Which criteria guide the creation of these sequences or pathways?

### The collection and analysis of the data

The data were collected from eighteen diaries written by students from the Department of Information and Librarianship Studies at Beit Berl Academic College from November to February 2008 and from a semi-structured questionnaire that served to clarify some issues from the data collected from the diaries.

Diaries were chosen as a method of data collection for a number of reasons. First, they can be put to use beyond the collection of _micro_ data and can equally be applied to the collection of data about a process ([Lewis and Massey 2004](#LE04)). As Toms and Duff noted, '_the diary can encapsulate a lengthy, mostly non-observable process_' ([Toms and Duff 2002](#TO02): 1236). That is, diaries can go beyond counting and collecting to enable the participant to describe and reflect on his or her behaviour and can represent a viable alternative to observation. Secondly, the use of diaries as data collection elements ensures the immediacy of data recording that prevents inaccuracy and enables the collection of a complete picture ([Dillman 2000](#DI00)). The keeping of real-time diaries has been referred to as event sampling. Event sampling is '_designed to provide detailed descriptions of specific moments or events in a person's life_' ([Reis and Gable 2000](#RE00): 190). Participants were asked to record data in their diaries whenever they searched for information in order to avoid distortions inherent in asking individuals to recall and summarize past events.

Writing a diary allowed participants to record their thoughts and reflections as well as their actions as they seek information. Different types of diaries have been adapted for used by different types of disciplines such as psychology, anthropology, and health care. Diaries have also been used in several studies in library and information science ([Goodall 1994](#GO94); [Kulthau 1993](#KU93); [Mellon 1990](#ME90); [Toms and Duff 2002](#TO02)). The present study used the type of diary that records a purposeful and structured account of activities related to a specific area in the lives of participants. In order to avoid many of the disadvantages outlined in the literature regarding the quality of data obtainable the focus of the diaries was constrained to specific areas of information seeking behaviour ([Johnson and Bytheway 2001](#JO01); [Toms and Duff 2002](#TO02)).

Participants were asked to record as fully as possible instances in which they describe two broad areas of their information seeking behaviour:

*   types of information sought, and
*   sources used to find information.

Diaries written in this study were used as a means of sequentially recording multiple types of content and were combined with the specific details of an open-ended questionnaire as a second data collection method, with the purpose of achieving the necessary triangulation. The purpose of using a triangulated approach was to mutually reinforce one research method with the other. This approach is consistent with the perspective enunciated by Denzin who commented on the desirability of 'combining multiple observers, theories, methods and data sources' with the aim of overcoming 'the intrinsic bias that comes from single-methods, single-observer, and single-theory studies' ([1970](#DE70): 313). The questionnaires, consisting of two open questions, which asked to participants to reflect on the following two issues, were distributed to participants after they completed their diaries:

*   the overall criteria that guided their preferences for information sources; and
*   the reasons why they chose a specific source to begin an information search. Eighteen students participated in the study, seventeen female and one male. The ages of the participants ranged from 29 to 55 averaging 34 years. Eight participants had university degrees and were studying for a Diploma in librarianship and ten were undergraduate students in the programme.

The data analysis process occurred in three phases: first, data were collected concerning the participants' preferences for information sources and the types of issues that brought participants to look for information; secondly, a qualitative content analysis of the data from participants' answers to the open ended questionnaire was performed, with the purpose of identifying the criteria guiding the source preference of participants; finally, the sequences or pathways of information sources used by participants were identified.

### Limitations of the study

There are two main limitations to the study. First, the population sample was small and specific and in no way representative. Second, a shortcoming of using diaries as a data collection tool resides in their personal and uncontrolled nature. Participants recorded the information they perceived as relevant to the subject and their writing could have been influenced by the awareness that the diary was written for a third party.

## Findings

The findings of this study will be presented in two main sections. The first section presents quantitative findings collected from the diaries relating to the source preferences for participants and qualitative findings of the content analysis of the data collected from the questionnaires regarding the criteria guiding those preferences. The second section will present the quantitative findings related to the sequence in which information sources were used and the qualitative findings resulted from content analysis of the participants' descriptions of these sequences.

### Source preferences

The analysis of the data collected from the 18 diaries contained 193 entries of which 116 (60.1%) were for personal needs and 77 (39.9%) were for academic needs. The first stage of the analysis identified twenty different information sources. There were 308 mentions of information sources, that is an average of 1.59 sources per search and an average of 17.11 mentions per diary. To compare the findings of the study, the sources mentioned in the diary entries were classified in the following four types of information sources:

*   Networked sources: this category included all types of information sources in networked form such as Internet sites of all types, search engines (specifically Google), academic databases and networked reference materials.
*   Human sources: this category included people in the immediate circle of the participants, friends, family members, and colleagues.
*   Printed sources: this category included all types of information sources in print form such as newspapers, books, magazines and reference materials.
*   Expert sources: this category included experts or professionals who have a clear area of expertise and a weak or previously non-existent ties to the user such as doctors or social workers. Table 1 presents an overall view of the source preference of participants, the analysis of the data collected from the diaries that shows the distribution of the data by type of source.

Table 1 presents an overall view of the source preference of participants, the analysis of the data collected from the diaries that shows the distribution of the data by type of source

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 1: Distribution by source types (N=308)**</caption>

<tbody>

<tr valign="top">

<th>Information source</th>

<th>Percentage</th>

</tr>

<tr>

<td>Networked sources</td>

<td align="center">63.96</td>

</tr>

<tr>

<td>Human resources</td>

<td align="center">18.50</td>

</tr>

<tr>

<td>Printed resources</td>

<td align="center">14.94</td>

</tr>

<tr>

<td>Expert resources</td>

<td align="center">2.60</td>

</tr>

</tbody>

</table>

As indicated in Table 1 the main source type used by participants were the networked sources followed by human sources. Printed and expert sources were the two categories least used.

The following stage in the analysis classified the searches described in the diaries into thematical groups; similar to other studies on information seeking behaviour (e.g., [Chen & Harnon 1982](#CH82); [Savolainen 2008](#SA08)). The searches analysed were thematically diverse and are presented in Table 2\.

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 2: Distribution of searches by theme**</caption>

<tbody>

<tr>

<th>Types of search</th>

<th>Percentage</th>

</tr>

<tr>

<td>Information on a specific subject related to an academic assignment</td>

<td align="center">38.86</td>

</tr>

<tr>

<td>Product or company searches</td>

<td align="center">12.43</td>

</tr>

<tr>

<td>Information on health issues</td>

<td align="center">6.21</td>

</tr>

<tr>

<td>Looking for a service</td>

<td align="center">6.73</td>

</tr>

<tr>

<td>Planning an event</td>

<td align="center">6.73</td>

</tr>

<tr>

<td>Information on a movie, play or concert</td>

<td align="center">5.69</td>

</tr>

<tr>

<td>Planning a trip</td>

<td align="center">5.18</td>

</tr>

<tr>

<td>Looking for a song</td>

<td align="center">3.10</td>

</tr>

<tr>

<td>Government information</td>

<td align="center">3.10</td>

</tr>

<tr>

<td>Genealogy search</td>

<td align="center">2.59</td>

</tr>

<tr>

<td>News information</td>

<td align="center">2.59</td>

</tr>

<tr>

<td>Other searches (such as information about cooking and political parties)</td>

<td align="center">6.20</td>

</tr>

</tbody>

</table>

    <p>Table 2 shows the wide 

variety of themes described in the diaries entries. The main theme was subject searches of all kinds followed by searches for information about products or companies. Participants also searched for information on subjects such as health, genealogy and tourism among others. From the data presented in Table 2 can be seen that the searches described in the diaries comprised a wide range of information needs and are representative of the participants' information interests.

### Criteria of source preferences

The purpose of the questionnaire was to give participants the opportunity to explain their selection of information sources recorded in their diaries. The analysis of the data collected from the eighteen questionnaires was analysed by means of qualitative content analysis by constantly comparing the explanations participants gave about their preferences, and coded into the two main categories:

*   Availability and accessibility of information sources: this category included criteria such as physical proximity of the information source, full-text availability, time saved by using the source and easiness of use of the information source.
*   Content availability: this category related to the nature of the information found in the source and it includes criteria such as quality and reliability of information

The following section presents the findings of the qualitative analysis of the data from the questionnaires. Findings will be presented and discussed according to the type of criteria.

### Availability and accessibility of information sources

The perceived accessibility of an information source is a concept that has been addressed in several studies in different ways over the years ([Gerstberger and Allen 1968](#GE68); [Allen 1977](#AL77); [Culnan 1985](#CU85); [Green 2000](#GR00); [Fidel and Green 2004](#FI04); [Bronstein and Baruchson 2008](#BR08);). In the present study the concept of accessibility appears as a central criterion and it received five different meanings: physical proximity, ease of use, full-text availability and language and sources that save time.

#### Physical proximity.

The convenience of accessing information from home or office was revealed as a major criterion in the source preferences of participants. That is, they perceived as accessible those sources that were available to them from their personal computers; this is probably one of the reasons why percentage of use of networked sources was so high in this study:

> Because I am usually really pressed for time, I will choose the easier path, that is, I will choose those sources that are easier and closer for me to use. Closer usually means sources available from my PC at home.

In some cases printed sources in personal collections physically close were perceived as more accessible:

> I needed some information on oak trees but I did not want to spend too much time searching so I just looked at the encyclopedia I had at home and found what I needed.

#### Ease of use

Friendly and easy to use interfaces are major criteria in deciding what source to use. This criterion is at the basis of the users' preferences for Internet sources specially Google:

> Many of my searches start with Google because it is really easy to use and it allows access to a wide range of information sources, sort of a starting point.

#### Saves time

Accessibility is intended as a measure of the perceived cost associated with the use of a source, therefore accessible sources are perceived to minimize the effort that the user has to invest in using the information source. In the present study the concept of effort was revealed as time spent accessing the information needed:

> The main reason for choosing an information source is time. The choice of a source will be based on how much time I have to invest in getting the information I need.

#### Language

This criterion appears to be relevant for Israeli students who perceived language as a barrier in using information sources so they perceived sources in Hebrew as more accessible:

> I will always look for information in Hebrew first because information sources in my own language are more accessible to me.

The nature of the information available at the source is the second category uncovered in the content analysis of the data from the questionnaires. This category includes two criteria: quality and reliability of information and full-text availability.

#### Full-text availability

The availability of information in full text format represented an important criterion for choosing an information source:

> The first articles I will consider using for my papers are those available in full text format because they are more convenient.

In some statements participants described the notion of accessibility with its three meanings:

> I will choose an information source that is accessible, easy to use and saves me time; these three parameters give me confidence in my search.

### Content availability

Quality and reliability of information. Several studies have found that the use of information is positively correlated with the perceived quality measured in terms of the relevance and reliability of information ([Auster and Choo 1993](#AU93); [Bronstein and Baruchson 2008](#BR08); [Klobas 1995](#KL95); [Orr 1970](#OR70)). This finding is also a significant criterion in source preference in this study as reflected in the next statement.

> I worry about the reliability and quality of the information on the Internet, this is why it is usually the last source I use.

The reliability of information appeared as a central criterion regarding the preference of human sources for personal needs. People close or known to the participants were perceived as reliable sources of information that provide them with quality information.

> Reliability and relevancy of information are really important to me when choosing an information source. This is why I usually turn to someone close to me (family member, friend or teacher), someone I trust and from whom I can get a quick and relevant answer. A participant made a distinction between different uses of information. When I need information in my everyday life I will try first to get it from someone I know, someone I trust.

The experience and prior knowledge of people as sources of information was important to participants when deciding which information source to select.

> I found that the experience people have on certain subjects is an invaluable resource; that is why, when looking for information I will first ask people I know have that experience on the subject and benefit from it.

The quality and reliability of information were important criteria in the source preferences of participants in this study, implying that the characteristics of the information sought is of significance when choosing an information source.

### Information pathways

_Sequences of information sources_ The last phase of the data collection process identified the information pathways described in the diaries, the data presented below provide both a quantitative and a qualitative overview of the findings resulted in this phase. Information pathways or sequences of sources used by participants were analysed focusing on the number of steps in each pathway, on the order of use of the preferred sources and on the distribution of the four sources types with regard to the steps. Each step is a consultation of a specific information source. Twenty one different information pathways were identified in the 193 searches in the sample and their distribution by number of steps is presented in Table 3

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 3: Length of information pathways indicated by the number of steps**</caption>

<tbody>

<tr>

<th>Number of steps in the pathway</th>

<th>Percentage</th>

</tr>

<tr>

<td align="center">1 step</td>

<td align="center">34.19</td>

</tr>

<tr>

<td align="center">2 steps</td>

<td align="center">44.04</td>

</tr>

<tr>

<td align="center">3 steps</td>

<td align="center">20.72</td>

</tr>

<tr>

<td align="center">4 steps</td>

<td align="center">1.03</td>

</tr>

</tbody>

</table>

Table 3 shows that a large percentage of the searches consisted of only one step. In one step searches the participants' need for information was satisfied after consulting one source, mainly networked sources (25.0%) and human sources (4.66%). Only in a minimal percentage of the one step information pathways participants consulted printed sources (2.07%) and expert sources (1.55%). The two step information pathway represents the largest percentage of the sample and it consisted of ten different combinations of the four types of information:

*   human ⇒ human
*   internet ⇒ academic databases
*   academic databases ⇒ internet
*   printed sources ⇒ internet
*   internet ⇒ printed sources
*   internet ⇒ internet
*   human ⇒ internet
*   database ⇒ human
*   internet ⇒ human
*   human ⇒ printed

A small percentage of the pathways consisted of three or four steps. The type of information source selected in each step and the reasons for these preferences will be reviewed in the next section.The sequence in which specific sources are chosen is also important because the step in which an information source is used was found to be significant. Table 4 presents the findings on this matter.

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 4: Distribution of steps by information source type**  
(numbers indicate the mentions of the source type in the specific step in the sequence)</caption>

<tbody>

<tr>

<th> </th>

<th colspan="5">Source type</th>

</tr>

<tr>

<th>Step</th>

<th>Human</th>

<th>Networked</th>

<th>Printed</th>

<th>Expert</th>

<th>Total</th>

</tr>

<tr>

<td align="center">1</td>

<td align="center">58</td>

<td align="center">118</td>

<td align="center">13</td>

<td align="center">4</td>

<td align="center">193</td>

</tr>

<tr>

<td align="center">2</td>

<td align="center">32</td>

<td align="center">90</td>

<td align="center">6</td>

<td align="center">0</td>

<td align="center">130</td>

</tr>

<tr>

<td align="center">3</td>

<td align="center">14</td>

<td align="center">15</td>

<td align="center">3</td>

<td align="center">0</td>

<td align="center">32</td>

</tr>

<tr>

<td align="center">4</td>

<td align="center">0</td>

<td align="center">2</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">2</td>

</tr>

</tbody>

</table>

Table 4 providea an overview of the distribution of information sources chosen at the different steps of the information pathways. As indicated in this table, the typical pathway will in most instances start with a human or a networked source, which are perceived as either more reliable or accessible and they also appeared in steps 2 and 3\. Only expert sources such as health centres or government offices figured solely at the beginning of the path. In the majority of the searches (90.48%) the first two information steps in the pathway fulfil the information needs of participants. This is especially true for expert sources which appeared only at the beginning of one-step pathways and apparently fulfilled the information need of the participants, so no other source was consulted. Information sources used in step 3 and step 4 (9.52%) provided supplementary information or feedback on the information obtained in previous steps. Findings in this section further demonstrate the importance of networked and human sources in the information source preferences of library and information studies students in Israel

### Qualitative findings

In order the better to explain the participants' preferences for information sources at each step of the pathways the following section presents relevant excerpts that can shed some light in the information pathways collected from the diaries during the qualitative content analysis phase. Findings in this section will explain the different criteria of source preference in previous sections in this article.

Reliability and quality of information found in the different information sources guided the order by which sources were chosen. This concern was expressed for networked, human and expert sources.

The following statement describes the need to verify the information content found on the Internet with a human source.

> I was not sure about vaccinating my son against the flu, so I checked some parenting forums and sites I have visited before and all of them recommended the vaccine. However, I was still afraid to make the wrong decision so I called my husband's aunt who is an experienced pharmacist and she also recommended the vaccine.

Sometimes the sequence is reversed and participants turn to the Internet to verify the information obtained from an expert source.

> When my doctor wanted to change the medicine I have been taking for years for a new drug in the market, I felt I needed more information before making a decision. When I searched Internet forums dealing with my disease I found many unfavorable comments about the drug, especially some side effects that the doctor have failed to mention, so I decided against taking the new drug.

The two statements above show how opinions provided by people affected the final decision of participants if they were based on experienced and/or professional knowledge. When searching for updated information, the Internet was perceived as more relevant with rapidly updated information content and marked its placed at the beginning of the pathway.

> Whenever I need information for a class assignment I will start my search in Google and Google scholar to get to most up to date information and then I will continue the search in academic databases from home.

In other cases printed media, especially local newspapers were favored over the Internet because they provide more locally relevant information on products and services. The following participant needed to buy a new mattress and followed the newspapers for sales advertisements.

> We needed to buy a new mattress for our bed so I searched the different company sites and got an idea of what we wanted; then every day I scanned my local newspaper for advertisements of sales and discounts in local stores until I found what I wanted on sale.

In this case the local newspaper provided the participant with the up to date information s/he was looking for.

Occasionally the selection of the first source in the pathway is a result of a habit without taking into account the quality of relevance of the information content.

> I needed information for one of my classes about a book from a very well known Israeli writer. My first instinct was to search Google for the name of the book and the writer but I got very few and irrelevant results. I realized my mistake and searched the Hebrew Index of Periodicals where I found enough information to do the assignment.

Google is perceived as an accessible and easy to use information source and many pathways begin with Google as a form of habit. A significant percentage of pathways were single step sequences in which the sole information source consulted was the Internet. The next statement presents an example of single step pathway in which the Internet provided adequate and relevant information.

> My daughter is getting married and I wanted some information on what the mother of the bride should wear. I searched Google for the phrase 'mother of the bride' and found all the information I needed, not only on what to wear but on how to deal with this very exciting time in every aspect.

Different combinations of human and networked sources represented a large portion of the pathways constructed from the data collected from the diaries. In the following statement the pathway starts with a trusted human source and continues with networked sources that expanded and reinforced the information obtain from the human source.

> When I was planning a birthday party for my husband I wanted to find a nice kosher restaurant that will provide private rooms for special occasions. I asked a coworker who I know respects kashrut laws and likes to eat out about restaurants in the area and he recommended a few. Then, I checked the restaurants sites for more information on the prices and the menus and checked some social sites for customers' reviews.

When dealing with significant issues in their daily life the issue of the reliability of the information is the basis for constructing the information pathway.

> When looking for a babysitter I approached the lady in charge of my son's day care but she could not recommend anyone. I ask a coworker who is also a mother of small children and she recommended a site that announces babysitting services. Although I searched the site I had a hard time trusting my child to some one I found on the net so I finally asked a neighbour and she gave the details of a very responsible student she knows.

The perceived reliability of the information source guided the construction of this information pathway because she could not trust the veracity and quality of the information found on the Internet.

The above statements illustrate the main criteria that guided the construction of information pathways, and the role each source has at different steps.

Finally, the data collection and analysis process identified the differences between the information paths used in academic and in personal searches. The main difference revealed in the analysis resides in the first source chosen by participants for each type of search. Table 6 presents the findings regarding the beginning of the two types of information pathways. In order to further understand the differences between the two types of information pathways, findings are presented by information source and not by information type.

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 5: Distribution of first source used by type of search (N = 193)**</caption>

<tbody>

<tr>

<th colspan="2">Academic searches (n = 77)</th>

<th colspan="2">Personal searches (n = 116)</th>

</tr>

<tr>

<th>Information source</th>

<th>Percentage</th>

<th>Information source</th>

<th>Percentage</th>

</tr>

<tr>

<td>Google</td>

<td align="center">25.97</td>

<td>Google</td>

<td align="center">32.75</td>

</tr>

<tr>

<td>Academic databases</td>

<td align="center">22.07</td>

<td>Human sources</td>

<td align="center">31.89</td>

</tr>

<tr>

<td>Printed sources</td>

<td align="center">18.18</td>

<td>Internet sites</td>

<td align="center">26.72</td>

</tr>

<tr>

<td>Library catalogue</td>

<td align="center">11.68</td>

<td>Expert sources</td>

<td align="center">3.44</td>

</tr>

<tr>

<td>Human sources</td>

<td align="center">10.38</td>

<td>Networked reference sources</td>

<td align="center">1.72</td>

</tr>

<tr>

<td>Internet sites</td>

<td align="center">5.19</td>

<td> </td>

<td align="center"> </td>

</tr>

<tr>

<td>Expert sources</td>

<td align="center">2.59</td>

<td> </td>

<td align="center"> </td>

</tr>

<tr>

<td>Networked reference sources</td>

<td align="center">2.59</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

Table 5 shows a distinct difference between the first source used in academic and in personal searches. Information pathways in personal searches tended to start with two main categories, human (31.89%) and networked sources (59.47%) (i.e., Google and Internet sites). In comparison, the distribution of the first source used in academic searches is more diverse. Although a percentage (25.97%) of academic searches started with Google (similar to personal searches) in academic searches participants rely less on human sources (10.38%) and on Internet sites (5.19%) and preferred more formal sources such as printed materials (18.18%) and academic databases (22.07%). This section reviewed the source preference criteria of participants in this study and the ways these criteria influenced the construction of information pathways. Findings show that the perceived accessibility, quality and reliability of the information source directly influenced the choice of the specific information source and its place in the information pathway.

## Discussion

The purpose of this study was twofold. First it examined the source preferences of the students that participated in the study and it identified the information pathways formed by these preferences. Two main outcomes can be drawn from the findings presented in the previous section on the subject of source preferences. First, this study demonstrated that the perception users have of the information sources available to them contains elements of value, and these elements directly affect not only source selection but also the order in which these sources are used. Second, findings indicate that when seeking information, users tend to draw primarily on networked and human sources.

Regarding source preference, findings show that both the accessibility of the source and the quality of its information are relevant criteria when selecting information sources. However, there was a distinct difference between the selection criteria participants applied in the selection of networked and in the selection of human sources. The primary criterion in the selection of networked sources was accessibility. Findings show that networked sources were favoured for their perceived accessibility, which echoes the principle of least effort. Savolainen and Kari ([2004](#SA04)) supported this assertion by claiming that accessibility was a major factor explaining source preferences for networked sources which were perceived as facilitating everyday life. Studies found the accessibility of networked sources so decisive that students use the Internet as their primary source of information as a matter of convenience without verifying the credibility and accuracy of the information obtained ([Graham and Metaxas 2003](#GR03); [Metzger 2003](#ME03)). However, the significance of the three elements of accessibility appears to be situational and it is not equally supported in all studies. There are studies that support the findings on the significance of the three elements of accessibility, easy of use ([Saiti and Prokopiadou 2008](#SAI08); [Savolainen and Kari 2004](#SA04)), time saving ([Fidel and Green 2004](#FI04); [Savolainen 1999](#SA99)) and physical proximity ([Abels _et al._ 1996](#AB96); [Fidel and Green, 2004](#FI04); [Savolainen 1999](#SA99)). But other studies challenge these findings. Fidel and Green ([2004](#FI04)) established that physical proximity was not the decisive factor in source selection; Abel _et al._ ([1996](#AB96)) found no significance to the easiness of use of the network; and Savoalinen ([2008](#SA08)) found that the easiness of use of the source is a marginal factor in source selection.

The relationship between the accessibility and the quality of the information source found in this study is consistent with findings from other studies. In Dewald and Silvius's ([2005](#DE05)) study of business faculty satisfaction with Web versus library databases, participants reported that the Web's ease of use and timeliness contributed to its heavy utilization. However, overall satisfaction with library databases was higher because of their accuracy and format. Fidel and Green's ([2004](#FI04)) study concluded that the distinction between accessibility of the information channels and the quality of the information they contain is blurred at times. They connected these two criteria through the notion of familiarity with an information source which was the most frequently cited reason for its use in their study 'one may turn to a familiar source to save effort, but also because one knows the source is likely to have the information of the desired quality' ([Fidel and Green 2004](#FI04): .574).

Findings on the centrality of human sources as sources of information are supported in several studies ([Anderson 2001](#AN01); [Ankem 2006](#AN06); [Green 2000](#GR00); [Stefl-Mabry 2003](#ST03); [Von Seggern 1995](#VO95)) but these studies start from the premise that informal sources are easier to use, therefore more accessible. In contrast, findings in this study identified the perceived reliability and quality as the primary preference criteria when selecting human sources. Besides quality and reliability of information, a number of content factors are found in this study that are supported in the literature, such as up to date information ([Fidel and Green 2004](#FI04)) and prior knowledge or experience of the source ([Stefl-Mabry 2003](#ST03)). Expertise was also a factor when selecting expert sources ([Savolainen 2008](#SA08)). Overall, findings emphasize the importance of content over accessibility and affirm findings in other studies that challenge the preeminence of accessibility as the sole criterion for source preference ([Bronstein & Baruchson 2008](#BR08); [Orr 1970](#OR70)). Savolainen explained the rising significance of content factors by the '_ubiquity of network services at home and work_' ([Savolainen 2008: 291](#SA08)) which renders their accessibility self-evident thus transferring the users' attention to content related criteria such as quality and full-text availability as revealed in this study.

The second purpose of the study was to identify the sequences or pathways by which sources are used. Findings on the subject of information pathways show that the largest percentage of pathways consisted of one or two steps and users tend to start their search by consulting networked or human sources. This fact is directly related to the perceived accessibility and reliability of these two types of sources ([Savolainen 2008](#SA08)). This situation changes to some extent in the case of academic searches where academic sources of information (i.e., library catalogues and academic databases) are used at the beginning of the pathway because they provide users with unique content not available in other sources. Nonetheless, networked sources still represent the largest percentage of sources used in academic searches. In a small number of cases Hebrew printed sources physically available to the participant were selected at the beginning of the pathway because they were perceived as more accessible and provide more relevant information in that specific search. In sum, the first two steps in the pathway are the main phases in which users fulfill their information needs while the third and fourth steps provide feedback or verification of the information found in the first two steps.

## Conclusions

Diaries as a method of data collection proved to be a valuable tool for getting a glimpse into the participants' thoughts processes and decision making and at the same time it provided the researchers with an overall view of the participants' source preferences. The open and reflective format of the diary encouraged participants to include different types of information such as details of their own personal lives, expressions of feelings and sometimes their own analysis of their information seeking behaviour. The personal and uncontrolled nature of the diary allowed participants to express their thoughts about what they thought was important and not just to respond to what the researcher believed was important.

Source preference criteria represent an important aspect of the information behaviour of users. Although the reasons why users select a specific source have been investigated extensibly there is enough evidence to suggest that source preferences are situational. Contrary to what has been accepted in the literature on the subject, this study found that content related criteria such as quality and reliability of information are decisive factors in source selection and in the placement of that source at a specific step in the information path. This fact is especially true when selecting human sources; although these sources are described in the literature as accessible, the main reason given by participants for their selection is their reliability. Notwithstanding, factors related to the accessibility of the source are still decisive when selecting networked sources since these are perceived as easier and faster to use and therefore believed to minimize the effort needed from the user in using the source. The concept of information pathways served to further clarify source preferences since the step in which a source is used is directly related to the criteria applied in its selection, thus revealing the perceived criteria by which users select each source.

### About the author

Jenny Bronstein received her Ph.D. in 2006 from the Department of Information studies at Bar Ilan University (Israel). Her research interests are in the application of social technologies in libraries and information seeking behaviour. Dr. Bronstein has published in refereed journals and teaches courses in information retrieval, information behaviour, academic libraries and business information at the Information Science department at Bar-Ilan University. She can be contacted at [jbrosztein@yahoo.com](mailto:jbrosztein@yahoo.com).

## References

*   Abels, E.G., Liebscher, P. & Denman, D.W. (1996). Factors that influence the use of networked networks by science and engineering faculty at small instution: Part 1\. Queries. _Journal of the American Society for Information Science_, **47**(2), 146-158.
*   Allen, T.J. (1977). _Managing the flow of technology_. Cambridge, MA: MIT Press
*   Anderson, C. J., Glassman, M., McAfee, R.B. & Pinelli, T. (2001). An investigation of factors affecting how engineers and scientist seek information. _Journal of Engineering and Technology Management,_ **18**(2), 131-155.
*   Ankem, K. (2006). Factors influencing information needs among cancer patients: a meta-analysis. _Library & information science research_, **28**(1), 7-23.
*   Auster, E. & Choo, W.C. (1993). Environmental scanning by CEOs in two Canadian industries. _Journal of the American Society for Information Science_, **44**(4), 194-203.
*   Bronstein, J. & Baruchson-Arbib, S. (2008). The application of cost & benefit and least effort theories in studies of information seeking behavior of humanists. _Journal of Information Science_, **34**(2), 131-144.
*   Chen, C. & Hernon, P. (1982). _Information seeking: assessing and anticipating information needs_. New York, NY: Neal-Schuman
*   Choo, C.W., Detlor, B. & Turnbull, D. (1998). A behavioral model of information seeking on the web. _Proceedings of the ASIS Annual Meeting,_ **35**, 290-301.
*   Culnan, M. J.(1983). Environmental scanning: the effects of task complexity and source accessibility on information gathering behavior. _Decision Sciences_, **14**(2), 194-206.
*   Culnan, M.J. (1985). The dimensions of perceived accessibility to information: implications for the delivery of information systems and services. _Journal of the American Society for Information Science_, **36**(5), 302-308.
*   Dewald, N. & Silvius, M.A.(2005). Business faculty research: satisfaction with the web versus library databases. _Portal: Libraries and the Academy_, **5**(3), 313-328.
*   Denzin, N.K. (1970). _The research act in sociology_. London: Butterworths
*   Dillman, D.A. (2000). _Mail and Internet surveys: the tailored design method_ (2nd ed.) New York, NY: John Wiley & Sons.
*   Fidel, R. & Green, M. (2004). The many faces of accessibility: engineers' perceptions of information sources. _Information Processing and Management_, **40**(4), 563-581.
*   Gerstberger, P.G. &. Allen, T.J. (1968). Criteria used by research and development engineers in the selection of an information source. _Journal of Applied Psychology,_ **52**(4), 272-279.
*   Goodall, D. (1994). Use of diaries in library and information science research. _Library and information research news_, **18**(59), 17-21\.
*   Graham, L. & Metaxas, P. (2003). "Of course it's true; I saw it on the Internet!" Critical thinking in the Internet area. _Communications of the ACM_, **46**(5), 70-75.
*   Green, R. (2000). Locating sources in humanities scholarship: the efficacy of following bibliographic references_. Library Quarterly_, **70**(2), 201-229\.
*   Hertzum, M. & Pejtersen, A.M. (2000). The information seeking practices of engineers: searching for documents as well as for people. _Information processing and management_, **36**(5), 761-778.
*   Hertzum, M., Andersen, H.H.K., Andersen, V. & Hansen, C.B. (2002). Trust in information sources: seeking information from people, documents, and virtual agents. _Interacting with computers_, **14**(5), 575-599.
*   Johnson, J.D.E. (2003). On contexts of information seeking. _Information processing and management_, 39(5), 735-760.
*   Johnson, J.D.E, Case D.O., Andrews, J., Allard, S.L. & Johnson, N.E. (2006). Fields and pathways: contrasting or complementary views of information seeking. _Information processing and management_, **42**(2), 569-582.
*   Johnson, J. & Bytheway, B. (2001). An evaluation of the use of diaries in a study of medication in later life. _International Journal of Social Research Methodology_, **4**(3), 183-204.
*   Klobas, J.E. (1995). Beyond information quality: fitness for purpose and networked information resource use. _Journal of Information Science_, **21**(2), 95-114.
*   Kuhlthau, C.C, (1993). _Seeking meaning: a process approach to library and information services._ Norwood, NJ: Ablex Publishing Corp.
*   Leckie, G.J., Pettigrew, K.E. & Sylvain, C. (1996). Modeling the information seeking of professionals: a general model derived from research on engineers, health care professionals, and lawyers. _Library Quarterly_, **66**(2), 161-193.
*   Lewis, K. & Massey, C. (2004). [_Help or hindrance? Participant diaries as a qualitative data collection tool._](http://www.webcitation.org/5tkjUHQjh) Paper presented at the 17th Annual Conference of the Small Enterprise Association of Australia and New Zealand. Brisbane, Queensland, 26-29 September, 2004\. Retrieved 25 October, 2010 from http://sme-centre.massey.ac.nz/files/Lewis-Help_or_hindrance.pdf (Archived by WebCite® at http://www.webcitation.org/5tkjUHQjh)
*   Marton, C. & Choo, C.W. (2002) A question of quality: the effect of source quality on information seeking by women in IT professions. _Proceedings of the ASIST Annual Meeting,_ **65**, 140-151\.
*   Mellon, C. (1990). _Naturalistic inquiry for library science._ New York, NY: Greenwood Press.
*   Metzger, J.M., Flanagin, A.J. & Zwarun, L. (2003). College student web use, perceptions of information credibility, and verification behavior. _Computers and education_, **41**(3), 271-290.
*   O'Reilly, A. (1982). Variations in decisions makers' use of information aources: the impact of quality and accessibility of information. _Academy of Management Journal_, **25**(4), 756-771.
*   Orr, R.H. (1970). The scientist as an information processor: a conceptual model illustrated with data on variables related to library utilization. In C.E. Nelson & D.K. Pollack, (Eds.), (pp. ) _Communication among scientists and engineers_. Lexington, MA: Heath Lexington.
*   Pinelli, T.E. (1991). The information-seeking habits and practices of engineers. _Science and technology libraries_, **11**(3), 5-25.
*   Reis, H.T. & Gable, S.L. (2000). Event-sampling and other methods for studying everyday experience. In H.T. Reis & C.M. Judd, (Eds.) _Handbook of research methods in social and personality psychology_. (pp. 190-222). New York, NY: Cambridge University Press.
*   Savolainen, R. (1999). The role of the internet in information seeking. Putting the networked services in context. _Information processing and management_, **35**(6), 765-782
*   Savolainen, R. (2008). Source preference in the context of seeking problem-specific information. _Information processing and management_, **35**(5), 765-782.
*   Savolainen, R. & Kari, J. (2004). Placing the Internet in information source horizons. A study of information seeking by Internet users in the context of self development. _Information processing and management,_ **26**(4), 415-433.
*   Saiti, A. & Prokopiadou, G. (2008). Post-graduate students and learning environments: Users' perceptions regarding the choice of information sources. _The International Information & Library Review,_ **40**(2), 94-103.
*   Stefl-Mabry, J. (2003). A social judgment analysis of information source preference profiles. An exploratory study to empirically represent media selection patterns. _Journal of the American Society for Information Science_, **54**(9), 879-904.
*   Swanson, B. E. (1987). Information channel disposition and use. _Decision Sciences,_ **18**(1), 131-147.
*   Toms, E.G. & Duff, W. (2002). I spent 1 1/2 hours sifting through one large box. . .: diaries as information behavior of the archives user: lessons learned. _Journal of the American Society for Information Science and Technology_, **53**(14), 1232-1238.
*   Von Seggern, M. (1995). Scientists information seeking and reference services. _The Reference Librarian,_ Nos. 49/50, 95-104.
*   Wilson, T.D. (2000). [Human information behavior.](http://www.webcitation.org/5tlwh3lDS) _Informing Science,_ **3**(2), 49-55\. Retrieved 26 October, 2010 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf (Archived by WebCite® at http://www.webcitation.org/5tlwh3lDS)
*   Yitzhaki, M. & Hammershlag, G. (2004). Accessibility and use of information sources among computer scientists and software engineers in Israel: academy versus industry. _Journal of the American society for information science and technology,_ **55**(9), 832-842.
*   Zipf, G.K. (1949). _Human behavior and the principle of least effort: an introduction to human ecology_. Cambridge, MA: Addison- Wesley.