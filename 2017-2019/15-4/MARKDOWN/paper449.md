#### vol. 15 no. 4, December, 2010

# Talk as a metacognitive strategy during the information search process of adolescents

#### [Leanne Bowler](#author)  
School of Information Sciences, University of Pittsburgh, Pittsburgh, Pennsylvania, 15260, United States.

#### Abstract

> **Introduction.** This paper describes a metacognitive strategy related to the social dimension of the information search process of adolescents.  
> **Method.** A case study that used naturalistic methods to explore the metacognitive thinking and associated emotions of ten adolescents. The study was framed by Kuhlthau’s Information Search Process model and the construct of metacognition.  
> **Analysis.**Data analysis used the words and actions of the participants to tell the story of their journey through the information search process.  
> **Results.**The study found that talk was used as a metacognitive strategy to help clarify thinking and that the talk strategy was more likely to be implemented face-to-face with trusted family and friends, than with the class teacher and college librarian or even social media.  
> **Conclusions.** There is a strong social component attached to metacognitive knowledge during the information search process. Face-to-face social processes during information seeking are an important consideration in the design of library and information systems and services for young people. Utilizing the social component of metacognitive knowledge to its maximum benefit is an important lesson that needs to be taught to young people.

## Introduction

This paper reports on a case study that, in the course of exploring the metacognitive thinking and associated emotions of ten adolescents during the information search process, uncovered a social aspect to metacognitive knowledge. The study was framed by Kuhlthau's information search process model ([1991, 2004](#kuh)) and the construct of metacognition ([Flavell 1979](#fla)). Using naturalistic methods the study found, first, that talk was used as a metacognitive strategy, as a means to help information seekers understand their thinking about their own thinking, and secondly, the _talk_ strategy was more likely to be implemented face-to-face with trusted family and friends, rather than with the class teacher and school librarian or even through social media. This metacognitive strategy has been labelled _Communicating_ and is one part of a larger taxonomy of adolescent metacognitive knowledge to emerge from the study (Readers wishing to learn more about the other thirteen attributes can refer to Bowler ([2010](#bow)).

## Background

### Metacognition and metacognitive knowledge

Metacognition is deliberate, planful, intentional, goal-directed, future-oriented mental behavior that can be used to accomplish cognitive tasks ([Flavell 1979](#fla)). Metacognition is '_knowledge of one's knowledge, processes, and cognitive and affective states; and the ability to consciously and deliberately monitor and regulate one's knowledge, processes, and cognitive and affective states_' ([Hacker 1998: 11](#hac)). Much of the foundational research on metacognition has been in the area of children and education, specifically in reading, writing, studying, mathematics and science (see, for example, [Baker and Brown 1984](#bbr); [Forrest-Pressley and Waller 1984](#for); [Garner 1987](#gar); [Paris _et al_ 1991](#par); [Van Hanaghan and Baker 1989](#van); [Scardamalia and Bereiter 1985](#sca); [Baker 1991](#bak), as cited in [Baker and Cerro 1996: 99](#cer)).

There are two aspects to metacognition: _control processes_ and _metacognitive knowledge_ ([Flavell 1979](#fla); [Garner 1987](#gar); [Moore 1995](#moo); [Baker and Cerro 1996](#cer); [Anderson and Krathwohl 2001](#and)). The first half, _control processes_, relates to executive control, self-monitoring and self-regulation, and reflects the application of strategies to control and coordinate aspects of metacognitive knowledge, the actual _doing_ ([Brown 1987](#bro); [Moore 1995: 4](#moo); [Hacker _et al_ 1998](#hac)). The latter half of the equation, _metacognitive knowledge_, is concerned with the contents of knowledge, the _knowing that_ certain strategies work better than others or _knowing that_ certain tasks might be easier to perform. This study focused on the _knowledge_ half of metacognition, _metacognitive knowledge_, aiming to explore the nature of adolescents' awareness of their own thoughts and feelings, as well as their awareness of cognitive strategies and the nature of the tasks to be tackled within the specific phenomenon of the search process.

Metacognitive knowledge consists of three interrelated components: _self-knowledge_ (awareness of one's own cognition, including knowledge of one's strengths and weaknesses and the awareness of one's motivational beliefs); _task knowledge_ (knowledge about the cognitive demands of the task); and _strategic knowledge_ (procedural knowledge of cognitive strategies to employ when unsuccessful) ([Flavell 1979](#fla), [Garner and Alexander 1989](#gaa), [Pintrich _et al_ 1996](#pin), [Anderson and Krathwohl 2001](#and)). An example of strategic metacognitive knowledge was seen in this study, when the participants actively and consciously used talk to regulate their own thinking processes. They knew, first of all, that the cognitive task required some dialogue, and secondly, they knew _when_ and _how_ to put that dialogue in service to their own cognition.

### Social aspects of information seeking

The social aspects of information seeking have not gone unnoticed in the field of information science. The role of information gatekeepers, people who control the flow of information through communication networks, has been actively studied for several decades (for a recent review of the literature on gatekeeping see [Lu 2007](#lu)). In the area of youth information seeking behaviour, [Dresang (1999)](#dre) proposed the adoption of a broader research paradigm that stretches beyond the walls of the physical library to include the internet and other every day settings in order to be able to investigate the social and collaborative ways that young people find, interpret, and use information.

Research in the realm of school libraries has shown that young people do turn to the people in their lives for help with school-based information problems, even when there is access to the internet. In an early study of high school students' use of the world wide web for school assignments, most students identified the librarian as a person they talked to (a claim the author questioned, saying that the students knew the researcher was a librarian) as well as friends and family members ([Lorenzen 2001](#lor)). The study by Williamson _et al._ ([2007](#wil)) into the seeking and use of information by secondary students in Australia confirmed that students turn to a range of people for help with information problems. Despite high levels of confidence in their own information seeking skills, many students in this study admitted to having consulted with another person for help. These people ranged from teachers, the librarian, other students, and in some cases, family members.

A renewed interest in informal information seeking, more recently referred to as _everyday life information seeking_, that is, information-seeking for non-work or non-school-related purposes, has resulted in study into how information seekers use people as information mediators and information sources in their everyday lives (see for example, [McKenzie 2003](#mck), [Savolainen 1995](#sav), [Spink and Cole 2001](#spi)). A few studies have looked at the social aspects of adolescent information seeking. One such study looked at how twenty-eight adolescent girls found information solutions to their everyday life concerns related to relationships, education and work, turning not to libraries but to the people around them; family, friends and teachers ([Poston-Anderson and Edwards 1993](#pos)). Looking further at how adolescent girls seek information about jobs and education, Edwards and Poston-Anderson ([1996](#edw)) found that girls tended to approach their mothers and fathers before teachers, career counsellors, and librarians. Shenton and Dixon ([2003](#she)) also looked at young people, aged 4 to 18, and their use of other people as an information-seeking method. They found this strategy to be a frequently employed and highly successful method by which youngsters obtain information. Julien ([2004](#jul)) explored adolescents' decision-making for careers and found that they relied heavily on people they trusted rather than on documentary sources.

## Methods

The case study used naturalistic research methods to investigate the metacognitive knowledge of ten adolescents, aged 16 to 18, as they searched for, selected, and used information for a school-based inquiry project. The primary question guiding this research was: _What is the role of metacognitive knowledge during the information search process of adolescents?_ Methods used in the study are described below.

### The setting

The study took place in Montreal, Canada, at an English-language, junior college, commonly called a CEGEP. The acronym CEGEP stands for _College d'enseignement general et professionnel_ or, 'College of General and Professional Education'. New students are, in a sense, a _tabula rasa_ and, at least in terms of library experience, they may have little else to guide them but their metacognitive knowledge. The position of teacher-librarian does not exist in public high schools and most private high schools in Montreal, and therefore information skills instruction at the high school level is limited to what students learn from the class room teacher.

### The participants

A specific class, History of Western Civilization, was selected as the field site. The class had a full term research project as its major component, which would allow for an in-depth exploration of the search process over the course of five months. While the specific course was selected purposefully, few constraints were set on the individual characteristics of participants drawn from these classes. In other words, the study was open to any student in the class wishing to participate. All volunteers were accepted, irrespective of gender, language or ethnicity, or of the number of volunteers. Ultimately, ten adolescents, aged 16 to 18, participated in the study. They were all in their first term, having graduated from high school the previous year and, therefore, were all new to the school. All of the participants were academic achievers, having been accepted to a specialized _Arts and Sciences_ programme at the College.

### The information-seeking task

The participants were asked to write a seven- to eight-page argumentative essay exploring continuity and change in western civilization, on a topic of their choice. To do so, they searched for, selected, evaluated, and used information from a variety of sources over the course of the fall 2006 term. The search process was a critical element to the success of the assignment because the students were to use information sources to defend their position. The class had an assigned text book but it was not to be included in the list of sources for the research paper. The students were first asked to identify a topic and to locate it within a specific geographic location and time frame. No specific guidelines or boundaries as regards the topic were provided, as long as it was related to the history of western civilization. As a result, the participants investigated a wide array of topics, from Greek architecture to classical music.

### Data collection protocols

The study used a combination of _think aloud_ and _think after_ verbal protocols in order to provide as many venues as possible for the expression of thoughts, feelings and actions experienced by the participants during the search process. In this way, the data could be triangulated. Five types of data collection protocols were used in this study: 1) a series of three telephone interviews; 2) written and/or audio journals kept by the participants over the course of the semester; 3) an in-person interview immediately following the final submission of the essay; 4) a visualizing exercise (a timeline) and; 5) a follow-up interview conducted several months later. Metacognitive knowledge was specifically targeted by questions related to _why_ and probes designed to reveal the _self-prompting questions_ the participants may have asked themselves. Questions related to actions helped to situate the participants' progress throughout the search process and were the critical factor in uncovering the different genres of search.

### Data analysis

Analysis was inductive and grounded in the data, using the words and actions of the participants to tell the story of their journey through the information search process. The data were transcribed and imported to a qualitative data analysis software application (Atlas.ti 5.2) and one _hermeneutic unit_ was created (the _container_ for all the data and coding). During coding, the data were segmented into meaning units at the sentence and paragraph level. Using a grounded theory approach, three levels of codes were developed: _descriptive, interpretive_ and _pattern_ ([Miles and Huberman 1994](#mil): 57), each representing a progressive drilling down through the data toward its essential themes.

## Limitations

Because of the small sampling size, generalizations beyond the context of the study are difficult to infer. The ten participants in this study were high academic-achievers in a Montreal-area private school, and their behaviour may not reflect that of the general population. Also, some of their behaviour may have been shaped by the type of information task assigned by the teacher. The results of this study relate to a specific content area, the history of western civilization, and are not generalizable to other content areas or information seeking tasks. In addition, the extent of the students' prior domain, information system and metacognitive knowledge, in relation to other students of their age, were not known as the qualitative methods to be used in this study precluded the use of a control group or wide sampling procedures. Only two of the ten participants were male and, therefore, the study presents no findings on sex differences.

## Results

The study identified thirteen attributes of adolescent metacognitive knowledge. This paper focuses on one of them; _communicating_. This attribute is linked to the social dimension of information seeking and involves the use of people as information mediators and information sources during the search process. Quite simply, it is knowing that talking to people is a useful cognitive strategy. Talking to people serves many cognitive purposes during the search process. _Talk_ can help to clarify points of confusion about conflicting information or it can help to unite information into a cohesive unit. _Talk_ can also be a quick source of information, helping to build a knowledge base, or can provide a road map for the next steps in the process. While the _Communicating_ attribute played out throughout the information search process, it was predominant during the topic selection stage of Kuhlthau's information search process model and later, when collecting information and preparing it for presentation, suggesting that the participants used _talk_ in highly specific and strategic ways.

Uncertainty may be common among young people working on their first research paper. Generally, the participants in this study understood that they were confused and knew that talking through the problem with a knowledgeable other at some point in the process was a dependable cognitive strategy for clarifying thoughts. The participants in this study used _talk_ as a way to reach out to people who could act as their information mediators. Interestingly, the role of information mediator was filled by people in the participants' everyday lives; their parents, siblings, grandparents, neighbours and family friends, and not so much by the librarian, the one person whose very job it is to mediate between information and users.

In some ways, parents, family and friends were more than just a bridge between the participants and the information; they were the information source. Given that these conversations were conducted in the context of the participants' everyday lives, anywhere, anyplace, anytime, the researcher was not able to record the actual words spoken between the participants and the people they talked to. It is difficult, therefore, to know where the mediation role ended and the information source role began. Nevertheless, the participants acknowledge that they turned to people in their network of relationships because they knew it was a good strategy for helping them to make sense of the problem.

Knowing that the strategy exists, when to implement it and why, represents a certain type of metacognitive knowledge. Table 1 shows who the participants talked to and when. While the table provides a sense of the _who_ and the _when_, it does not provide a picture of the _why_ and _how_. The participants used _talk_ to serve two very different purposes. One purpose was to use people in their circle of family and friends as _information mediators_, people who could help navigate the tangled web of information and meaning-making that the participants needed negotiate, and as _information sources_, people whose knowledge of a topic would provide a shortcut to actual information. To complete the picture, a more thorough description follows. This picture is framed by the six stages in the information search process.

<table style="border: medium solid '#444444'; font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background: color #ffffff;" align="center" border="1" cellpadding="3" cellspacing="0" width="65%"><caption align="bottom">  
**Table 1: Talk during the information a search process**</caption>

<tbody>

<tr>

<th>Tasks in the search process</th>

<th>Number of participants who used human information mediators (_talk_) during the search process.</th>

<th>Whom did they talk to?</th>

</tr>

<tr>

<td>_Task initiation_</td>

<td align="center">—</td>

<td align="center">—</td>

</tr>

<tr>

<td valign="top">_Topic selection_</td>

<td valign="top">Eight participants</td>

<td>Peers (1)  
Mother (2)  
Both parents (1)  
Grandmother (1)  
Sister (2), cousin (1)  
Neighbour who is a knowledgeable adult (1)  
Teacher (1)</td>

</tr>

<tr>

<td valign="top">_Prefocus exploration_</td>

<td valign="top">Three participants</td>

<td>University professors (3)*, Mother (1)  
Father (1)  
Teacher (1)  

*All interviewed by the same participant.</td>

</tr>

<tr>

<td valign="top">_Focus formulation_</td>

<td valign="top">Three participants</td>

<td>University professor (1)  
Teacher (1)  
Mother (1)</td>

</tr>

<tr>

<td valign="top">_Information collection_</td>

<td valign="top">Seven participants</td>

<td>Mother (3)*  
Teacher (3)  
College librarian (2)**  

*One mother was completing her Master of Library and Information Science (MLIS) degree and works in a university library.  
**Represents a one-to-one conversation between the participant and the librarian. The librarian also came to the class to teach locating skills (using the library portal, searching the online catalogue and data bases)</td>

</tr>

<tr>

<td>_Presentation_</td>

<td>Two participants</td>

<td>Teacher (2)</td>

</tr>

</tbody>

</table>

During the first stage of the information search process model, _task initiation_, there seemed to be no talk about the research project, perhaps because this is a passive stage where the student simply receives the assignment from the teacher. During _topic selection_, however, the participants actively sought and enjoyed the advice of those around them. Three factors seemed to drive this decision: the accessibility of the information mediator, his or her expertise in the topic, and an interest in the participant. For example, one participant thought she might want to look at the history of western civilization through the lens of philosophy, but the topic was large and needed to be narrowed somewhat. She began her search by going to her mother because _'she was a person who had read philosophers'_ and _'she was there and I knew she would help me'_. Using her mother as an information mediator offered the participant a quick and easy method to help clarify her thoughts on the topic; a cognitive shortcut.

The topic of philosophy was also of interest to another participant who, although he knew that he wanted to frame the research paper around the broad topic of philosophy, still needed to work out some of the details. Early in the search process, less than two weeks after the research project had been assigned and before a specific topic had been chosen, this participant identified _talk_ as a useful strategy. When asked to complete the sentence, _When looking for information sometimes it helps to..._ , he replied, _'Ask for help'_. Who would he ask? _'The reference librarian. My sister who studies philosophy at university.'_ At the end of the term, the participant represented his search process in a timeline and confirmed that the search was launched by talking to his sister to see _'if the project makes sense'_. Interestingly, although talking to the librarian was forecast as a useful strategy, in the event it does not seem to have actually happened. In any case, the sister filled the role of subject expert and talking to her helped to provide some clarification of the topic.

Other participants also shared their thoughts with family and friends as well. One participant outlined his timeline early in the game, saying his first step was to _discuss possible topic choices with my parents, sister and cousin for input and ideas'_. Why? Because _'I had no ideas for topics, but knew that their suggestions would be original. Used their experience in research'_. This decision was driven by the self-prompting question, identified by the participant in her timeline as _Who has experience in writing research papers & has a large store of knowledge to discuss with me?_ So, in this case, expertise in doing library-based research was the key, although a background in the topic area also played into the decision to turn to others, demonstrating that this participant recognized that she was weak in two areas of knowledge, the subject domain and the information-seeking domain, and that interpersonal communication with those who do have expertise is a useful method for filling in the gaps in knowledge.

It is surprising who filled the role of information mediator. In the case of one participant, a neighbour stepped in to help. The participant, who at this point had not made any decisions regarding the topic and indeed felt _desperate_ about it, turned to a neighbour who _'knows a lot about history and was always up for a discussion'_. In this case the neighbour's expertise in history and an openness to discussing it were two factors in the decision to talk about it. (Qualifying his understanding of the neighbour's expertise, the participant later explained that the neighbour reads a lot and has a large home library). Figuring he (the participant) _'would be able to handle most topics'_, he chose a topic suggested by the neighbour that _'seemed interesting'_.

As the students moved forward in the information search process, family members continued to play the role of information mediator. One participant was quite clear on her topic on the history of libraries and so, when she turned to her mother, whom she identified as a librarian at a local university, during the _Exploration_ phase of her search, it was to ask for her mother's help in identifying specific resources on the topic so that she could narrow down the focus. Her mother _'brought the books from the university'_ and _'emphasized what I should look for'_.

The mixed roles of mother and information professional raised ethical concerns (and some anxiety) for this student; Was it right for her mother to gather the information resources on her behalf? Was her mother doing her homework? This conundrum raises an interesting question regarding the role of information mediators, family or not. How far should a mediator go in providing information resources for a student? For information seekers, knowing that a parent with expertise may help to move one forward in the process by helping to clarify the information or modeling good practices in information gathering may be a valuable piece of metacognitive knowledge. On the other hand, it may be counter-productive, cognitively-speaking, with the parent carrying too much of the cognitive load and doing the heavy-lifting when it comes to information-gathering, thus preventing the student from learning.

Family members did not provide the only counsel for these students. Moving from _Exploration_ into _Focus Formulation_, one participant turned to three professors at the local university, who were also family friends and acquaintances, after having explored the literature somewhat because she '_was trying to make generalities_' and _'there's a lot of things that aren't stated in books'_. Furthermore, _'articles from journals are really quite useless at my level. They expect you have a fountain of knowledge'_. Knowing, then, that she lacked the requisite knowledge needed to explore the topic further and arrive at some focus on the topic, she turned to people who _did_ have that knowledge. In one quick stroke, the experts were able clarify some puzzling points the participant had about the information.

Reaching beyond the comfortable network of family and friends was a useful strategy, but those who did it, or who at least considered it, also knew that they had to know more about the topic before they contacted the expert so that they could ask the right questions. In other words, they understood that, cognitively-speaking, the best way to interact with an information mediator was to be able to have something that could launch a conversation. This meant waiting until the tail end of _Exploration_. For example, one participant considered talking to an art history teacher at the college but did not because, at that point in time, he did not _'feel informed enough to talk to them yet'._ Another participant talked with three family friends who happened to be professors but only after doing some pre-research because, as she said, she _'couldn't do the interviews without looking through books'_.

Moving from _Focus Formulation_ to _Information Collection_, participants again turned to information mediators for help. Some students at this point had already begun organizing their essays into sections. They knew that the information they had gathered was incomplete or at least, did not make sense yet, and so they discussed the gaps with others. Three of these interactions occurred with a parent and resulted in the collection of more information. The mother of one participant found a useful bibliography at the end of a fiction novel she had just finished reading, a novel that happened to be on the same research topic her daughter was studying (one wonders if there had been an earlier conversation about this particular novel that had inspired the participant to follow this topic). The participant then used the bibliography as a tool for collecting in-depth information on the topic. A third participant found information resources in the book collection housed at her mother's design firm: a lucky coincidence given that the student's topic was on architecture.

## Discussion

This study has shown that there is a social aspect to information seeking that filters through even to the metacognitive level of thinking. Kuhlthau's ISP model of the information search process identifies three parallel dimensions – cognitive, affective and behavioral. The use of communication as a metacognitive strategy during the information search process suggests that there is a fourth dimension that is based in the social and cultural worlds of information seekers. Dealing with this social dimension required a specific set of metacognitive strategies.

Given the seemingly natural inclination of young people to turn to knowledgeable-others in their lives for information, it is no surprise that communicating is an attribute of the metacognitive knowledge of the young people in this study. What is surprising is that this social aspect of metacognitive knowledge was implemented not in the context of everyday life but rather, in service to a school-based, information-seeking task, the purpose of which was to develop skills in finding and using formal documentary sources. This seems incongruous. After all, why should one turn to an informal information source when looking for a formal source?

Perhaps the reason for this inconsistency lies in the way that _information_ is defined. The kind of information that is used for the purposes of a school project tends to be what Bates ([2006: 1039](#bat)) has called _exosomatic information_; that is, _information that is stored externally to the body of animals_ (the human being a type of animal). This is the kind of information we associate with the documents in a library or on the Web and it is what we train students to find. But the students in this study also used a second type of information, _embodied information_; information that is _'the corporeal expression or manifestation of information previously in encoded form'_ ([Bates 2006: 1035](#bat)). This is information which is derived from a document and then expressed orally through language. Perhaps there is a need to broaden the set of information-seeking skills taught at school, providing the necessary tools needed to handle and, indeed, take advantage of, _embodied information_. Perhaps it is the case that metacognitively-aware information seekers deliberately go beyond the traditional boundaries of information, the _exosomatic_, and seek help in other realms of information.

A re-configuration of our conception of _information seeking_ may be needed to take account of this behaviour in the training that young people receive vis-à-vis the information search process. When librarians think of the traditional research essay, they often interpret _research_ to mean _finding a document_: a Web page, a journal article or a chapter in a book, which is hidden somewhere inside an information system; a database, a library catalogue, a Web portal, a search engine or a book. In direct contrast to this view, the participants in this study cast a much wider net and used the people in their world as information sources. Although one participant wondered about the reliability of her mother as an information source, most trusted their family and friends unquestioningly. It was a natural step for them and in metacognitive terms, perhaps the easiest step. As a metacognitive strategy, it was a move that paid off well because it lightened the cognitive load needed to find sources and helped them move forward in their search faster.

While the participants in this study used people as information sources, they equally used them as information mediators, that is, someone who provides a link between the information and the consumer of that information. The links were mediated by dialogue but the dialogue rarely occurred with the librarian. Perhaps this was due simply to the accessibility of family and friends. Or it could be due to a misalignment in the type of social interaction that the participants required and the social interaction that the librarian was prepared to give. The ways that librarians can intervene are many and one model, created by Kuhlthau, has mapped five levels of intervention to the six stages in the information search process model. They are, ranging from the lowest to highest level of intervention: organizer, locator, identifier, advisor and counsellor ([2004](#kuh): 129-134). It is at the highest levels where conversation between librarian and user helps to clarify meaning and organize thoughts.

As an information mediator, the librarian in this study functioned principally at the mid-level in Kuhlthau's model of the zones of intervention, as a _locator_ and _identifier_, but many of the participants sought out information mediators who could intervene at a higher level, as _advisors_ and _counsellors_ ([2004:129-134](#kuh)).

Perhaps the librarian was willing and able to offer a higher level of mediation but in the event, the participants did not seem to be interested in or even aware of the possibility of this sort of discourse with the librarian. Given that the participants had the wherewithal to use talk as a metacognitive strategy, the strategy was not employed with the one person who ought to really understand the information environment, the librarian. Although it seems natural that young people (or any information seeker, for that matter) would approach people from within their immediate circle first, people who are easily accessible and whom they trust, it does seem unfortunate that dialogue with the librarian was rarely part of the participants' metacognitive tool kit.

Perhaps there is a broader obligation here. The young people in this study were surrounded by people who had some level of expertise in the subjects being researched, people who read, who had home libraries or access to materials at work, or who were simply interested in what the participants were doing. The participants knew that dialogue with knowledgeable others was a useful metacognitive strategy but they also had the social resources needed to implement it. One wonders what might happen in those cases where a student has little to no social support. How useful is _talk_ as a metacognitive strategy for people who have no one to talk to?

## Conclusion

There was a strong social component attached to metacognitive knowledge during the information search process. The young people in this study actively sought humans to act as both information mediators and information sources. The natural way in which they turned to the people around them for help suggests that social processes during information seeking, even information seeking for the purposes of completing school assignments, should play an important role in the design of library and information systems and services for young people. More specifically, utilizing the social component of metacognitive knowledge to its maximum benefit is perhaps an important lesson that needs to be taught to young people.

## Acknowledgements

The author gratefully acknowledges the vital contribution of the young people who participated in this study. The research was funded in part by research grants from McGill University (the Herschel and Christine Victor Fellowship in Education) and the Fond Québécois pour la Recherche sur la Société et la Culture (FQRSC).


## References

*   Anderson, L.W. & Krathwohl, D. R. (Eds.), (2001). _A Taxonomy for learning, teaching, and assessing: A revision of Bloom's taxonomy of educational objectives_. Abridged edition. New York, NY: Addison Wesley Longman.
*   Baker, L. (1994). Fostering metacognitive development. _Advances in Child Development and Behavior_, **25**, 201-239.
*   Baker, L. & Brown, A.L. (1984). Metacognitive skills and reading. In P. D. Pearson, R. Barr, M. Kamil & P. Mosenthal, (Eds.), _Handbook of reading research_ (pp. 353-394). New York, NY: Longman.
*   Baker, L. & Cerro, L. (1996). Assessing metacognition in children and adults. In G. Schraw & J.C. Impara, (Eds.). _Issues in the measurement of metacognition_ (pp. 99-145). Lincoln, NE: Buros Institute of Mental Measurements.
*   Bates, M. (2006). Fundamental forms of information. _Journal of the American Society for Information Science and Technology_, **57**(8), 1033-1045.
*   Bowler, L. (2010). A taxonomy of adolescent metacognitive knowledge during the information search process. _Library and Information Science Research,_ **32**(1), 27-42
*   Brown, A.L. (1987). Metacognition, executive control, self-regulation, and other more mysterious mechanisms. In F. E. Weinert & R.H. Kluwe (Eds.), _Metacognition, motivation, and understanding_. (pp. 65-116). Hillsdale, NJ: Lawrence Erlbaum Associates.
*   Dresang, E. (1999). More research needed: informal information-seeking behavior of youth on the Internet. _Journal of the American Society for Information Science_, **50**(12), 1123-1124.
*   Edwards, S. & Poston-Anderson, B. (1996). Information, future time perspectives, and young adolescent girls: Concerns about education and jobs. _Library & Information Science Research_, **18**(3), 207-223.
*   Flavell, J. (1979). Metacognition and cognitive monitoring: a new area of cognitive-developmental inquiry. _American Psychologist_, **34**(10), 906-911\.
*   Forrest-Pressley, D. & Waller, T. G. (1984). _Cognition, metacognition, and reading_. New York, NY: Springer Verlag.
*   Garner, R. (1987). _Metacognition and reading comprehension_. Norwood, NJ: Ablex.
*   Garner, R. & Alexander, P. A. (1989). Metacognition: answered and unanswered questions. _Educational Psychologist_, **24**(2), 143-158.
*   Hacker, D. J., Dunlosky, J. & Graesser, A.C. (1998). _Metacognition in educational theory and practice._ Mahway, NJ: Erlbaum.
*   Julien, H. E. (2004). Adolescent decision-making for careers: an exploration of information behavior. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 321-568). Toronto: Scarecrow Press.
*   Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371.
*   Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services._ Westport, CT: Libraries Unlimited.
*   Lorenzen, M. (2001). The land of confusion? High School students and their use of the World Wide Web for research. _Research Strategies_. **18**(2), 151-163.
*   Lu, Y. (2007). The human in human information acquisition: understanding gatekeeping and proposing new directions in scholarship. _Library and Information Science Research_, **29**(1), 103-123.
*   McKenzie, P.J. (2003). A model of information practices in accounts of everyday-life information-seeking. _Journal of Documentation_, **59**(1), 19-40.
*   Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis: an expanded sourcebook_. Thousand Oaks, CA: Sage Publications.
*   Moore, P.A. (1995). Information problem solving: a wider view of library skills. _Contemporary Educational Psychology_, **20**(1), 1-31.
*   Paris, S.G., Wasik, B.A. & Turner, J.C. (1991). The development of strategic readers. In R. Barr., M. Kamil, P. Mosenthal & P.D. Pearson (Eds.), _Handbook of reading research,_ (2nd ed.), (pp. .609-640). New York, NY: Longman.
*   Pintrich, P.R., Wolters, C.A. & Baxter, G.P. (1996). Assessing metacognition and self-regulated learning. In G. Schraw & J.C. Impara (Eds). _Issues in the measurement of metacognition_, (pp. 43-97).Lincoln, NE: Buros Institute of Mental Measurements.
*   Poston-Anderson, B & Edwards, S. (1993). The role of information in helping adolescent girls with their life concerns. _School Library Media Quarterly_, **22**(1), 25-30.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of “way of life”. _Library and Information Science Research_, **17**, 259-294.
*   Scardamalia, M. & Bereiter, C. (1985). Fostering the development of self-regulation in children's knowledge processing. In S. F. Chipman, J. W. Segal & R. Glaser (Eds.), _Thinking and learning skills. Research and open questions_. (pp. 563-577). Hillsdale, NJ: Lawrence Erlbaum Associates.
*   Shenton, A.K. & Dixon, P. (2003). Youngsters' use of other people as an information-seeking method. _Journal of Librarianship and Information Science_, **35**(4), 219-233.
*   Spink, A. & Cole, C. (2001). Information poverty: information-seeking channels used by African-American low-income households. _Library & Information Science Research_, **23**(1), 45-65.
*   Van Haneghan, J.P. & Baker, L. (1989). Cognitive monitoring in mathematics. In C.B. McCormick, G.E. Miller & M. Pressley (Eds.), _Cognitive strategy research: from basic research to educational applications_ (pp. 215-238). Berlin: Springer-Verlag.
*   Williamson, K, McGregor, J., Archibald, A. & Sullivan, J. (2007). [Information seeking and use: the link between good practice and the avoidance of plagiarism.](http://www.webcitation.org/5utZ0wjr4) _School Library Media Research._ 10, n.p. Retrieved November 5, 2010 from http://bit.ly/efVkk0 (Archived by WebCite at http://www.webcitation.org/5utZ0wjr4)
