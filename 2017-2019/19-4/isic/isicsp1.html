﻿<!doctype html>  
 <html>  
 <head>  
 <title>Eliciting mental models of music resources: a research agenda</title>  
<meta charset="utf-8">
 <link href="../../IRstyle3.css" rel="stylesheet" media="screen" title="serif">  
<link rel="alternate stylesheet" type="text/css" media="screen" title="sans" href="../../IRstylesans.css">   
<!--Enter appropriate data in the content fields--> 
  
<meta name="dcterms.title" content="Eliciting mental models of music resources: a research agenda"> 
  
<meta name="author" content="Manca Noc, Maja Zumer">   
<meta name="dcterms.subject" content="Short paper">   
<meta name="description" content="The paper presents an approach and possible methods for eliciting mental models of the bibliographic universe for music resources and comparing them to Functional Requirements for Bibliographic Records (FRBR), a conceptual model of the bibliographic universe. A small pilot study using these methods is also presented. There are several viable methods for eliciting mental models such as card sorting, concept mapping, and graph selection. Card sorting and concept mapping were used to conduct the pilot study. The pilot study shows promising results, allowing us to continue with the full-fledged research, which hopes to contribute to a better understanding of the perceptions and needs of users, specifically for music resources."> 
  
<meta name="keywords" content="FRBR, mental models, classical music, user studies"> 
   <!--leave the following to be completed by the Editor-->   
<meta name="robots" content="all">   
<meta name="dcterms.publisher" content="Professor T.D. Wilson">  
<meta name="dcterms.type" content="text">   
<meta name="dcterms.identifier" content="ISSN-1368-1613">   
<meta name="dcterms.identifier" content="http://InformationR.net/ir/19-4/isic/isicsp1.html"> 
  
<meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/19-4/infres194.html"> 
  
<meta name="dcterms.format" content="text/html">  
<meta name="dc.language" content="en">   
<meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/"> 
  
<meta name="dcterms.issued" content="2014-12-15">   
<meta name="geo.placename" content="global">    

 
</head>    
<body> 
<header>
 <img height="45" alt="header" src="../../mini_logo2.gif" width="336"><br>
<span style="font-size: medium; font-variant: small-caps; font-weight: bold;">vol. 19  no. 4, December, 2014</span>
<br><br> 
<div class="button">
<ul>
  <li><a href="../infres194.html">Contents</a>  |  </li>
  <li><a href="../../iraindex.html">Author index</a> |  </li>
  <li><a href="../../irsindex.html">Subject index</a> |  </li>
  <li><a href="../../search.html">Search</a> |  </li>
  <li><a href="../../index.html">Home</a> </li></ul></div>
<hr>
</header>
<article>
<h1>Eliciting mental models of music resources: a research agenda </h1><br>

<div style="margin-right: 10%; margin-left: 5%;">
<h4><a href="#author">Manca Noc</a> and <a href="#author">Maja Zumer</a><br>
Faculty of Arts, University of Ljubljana, Ljubljana, Slovenia</h4>
</div>
<br>

 <form>  <fieldset>  <legend>Abstract </legend>
<blockquote>
 <strong>Introduction.</strong> The paper presents an 
  approach and possible methods for eliciting mental models of the bibliographic 
  universe for music resources and comparing them to Functional Requirements for 
  Bibliographic Records (FRBR), a conceptual model of the bibliographic 
  universe. A small pilot study using these methods is also 
  presented.<br>
 <strong>Method.</strong> There are several viable methods for 
  eliciting mental models such as card sorting, concept mapping, and graph 
  selection. Card sorting and concept mapping were used to conduct the pilot 
  study. <br>
 <strong>Conclusions.</strong>The pilot study shows promising 
  results, allowing us to continue with the full-fledged research, which hopes 
  to contribute to a better understanding of the perceptions and needs of users, 
  specifically for music resources.
 </blockquote>  </fieldset>  </form> 
  <section>
<br>
<h2>Introduction</h2>
<p>Current bibliographic information systems have been frequently criticised, 
namely because they are inefficient and not really in tune with users' needs. 
Users find it especially difficult to search for classical music resources (<a href="#cun03">Cunningham, 
Reeves and Britland, 2003</a>, <a href="#tho11">Thomas, 
2011</a>, <a href="#ril05">Riley, 
2005</a>). Liew and Ng (<a href="#lie06">2006</a>) 
state that the biggest issue in searching for musical documents is the current 
cataloguing system, which is not suitable for music, particularly because 
bibliographic information systems do not collocate records related to each 
other. Browsing is also not well supported, despite the fact that people 
typically discover music by tracing music similar to what they like and already 
know (<a href="#cun03">Cunningham, 
Reeves and Britland, 2003</a>). </p>
<p>A new paradigm is needed and Functional Requirements for Bibliographic 
Records (FRBR) (<a href="#fun98">Functional 
Requirements for Bibliographic Records, 1998</a>), developed by International 
Federation of Library Associations and Institutions (IFLA) is a conceptual 
entity-relationship model of the bibliographic universe, which has the potential 
to overcome current flaws and enhance the user experience (<a href="#car06">Carlyle, 
2006</a>). However, in order to assess its potential for music resources, user 
studies need to be conducted. The paper offers an approach to testing mental 
models of bibliographic universe for music resources, while comparing them to 
the FRBR conceptual model. Card sorting and concept mapping are presented as 
possible techniques for mental models elicitation, along with the results of a 
pilot study, conducted in March 2014.</p>
<h2>The model</h2>
<p>The FRBR model defines three groups of entities, the relationships between 
these entities and their attributes. The core of FRBR are the Group 1 entities, 
which are: work (a distinct intellectual or artistic creation - for example 
Mozart's Eine kleine Nachtmusik), expression (an intellectual or artistic 
realization of a work - for example a performance of Mozart's Eine kleine 
Nachtmusik), manifestation (a physical embodiment of an expression - for example 
an edition of  CDs of Mozart's Eine kleine Nachtmusik), and item (a single 
exemplar of a manifestation - for example a particular CD from a local library 
that contains the recording of Mozart's Eine kleine Nachtmusik).</p>
<p>However, despite its user-oriented approach, the FRBR model has not been 
tested prior to its release, therefore we cannot speculate on whether or not 
these principles even correspond with the users' needs (<a href="#pis10a">Pisanski 
and Zumer, 2010a</a>).</p>
<h2>Mental models</h2>
<p>In order to implement FRBR in bibliographic information systems, we have to 
find out how users search for documents, how they perceive the entities, and 
which relationships they recognize between entities, in short what their mental 
models of the bibliographic universe are. Norman (<a href="#nor83">1983</a>) 
defines a mental model as the mental representation constructed through 
interaction with the target system and constantly modified throughout this 
interaction. </p>
<p>Many researchers agree that the biggest issue in researching mental models is 
the research itself (<a href="#row95">Rowe 
and Cooke, 1995</a>, <a href="#lei92">Leiser, 
1992</a>). It is difficult to find a technique to successfully elicit, describe, 
and analyse people's mental models because they are often incomplete, full of 
contradictions, and inconsistent (<a href="#whi99">Whitman 
<em>et al.</em>, 1999</a>). There are several viable methods of eliciting mental models, 
such as card sorting, concept mapping, pair comparison, ranking tasks, and so 
on. Card sorting, concept mapping, comparison tasks, and their variations have 
proven to be successful in eliciting mental models of the bibliographic universe 
for fiction (<a href="#pis10a">Pisanski 
and Zumer, 2010a</a>, <a href="#pis10b">2010b</a>, 
<a href="#pis12">2012</a>), 
which is why these methods will be tested and implemented in our research of 
mental models for music. </p>
<h2>Design of the study</h2>
<p>Lately, there have been considerable efforts to fill the void of user studies 
of FRBR, dealing mainly with fiction (<a href="#pis10a">Pisanski 
and Zumer, 2010a</a>, <a href="#pis10b">2010b</a>, 
<a href="#pis12">2012</a>) 
and user evaluation of FRBR-based catalogues (<a href="#zum12">Zumer, 
Zhang and Salaba, 2012</a>). The results of these user studies provide us with a 
solid basis for our research agenda, which continues these previous efforts, 
asking whether or not the FRBR model is intuitive for lay users and whether or 
not the FRBR model actually corresponds with the users' mental models of 
searching and finding classical music resources. The latter are specific because 
of their different characteristics and diversity, compared to books, as they can 
be found in a variety of forms and media (printed and recorded music, CDs, DVDs, 
LPs, and other older media).</p>
<p>The research agenda consists of several studies. For each study, we intend to 
include 10-15 participants who are not music professionals. The first study will 
combine two separate tasks, card sorting and concept mapping. These were chosen 
in order to provide an initial insight in participants' mind-set and a sound 
basis for planning future research. Using the think-aloud method and in-depth 
interviews, participants will be encouraged to explain their actions while 
performing the tasks. </p>
<p>In the card sorting task participants will be asked to sort 14 cards, 
containing descriptions of different instances of entities, into at least three 
groups according to different levels of abstraction. This will be followed by 
the concept mapping task, where they will be asked to arrange the same cards to 
represent the relationships and connections they perceive between the entities, 
thus forming a concept map. </p>
<p>The resulting card sorts from the first task will be analysed with cluster 
analysis and the most frequent co-occurrences will be determined. The graphs 
from the second task will provide us with links between the entities, which will 
enable us to determine the most frequent links. These will be analysed by 
clustering and the average graph will be calculated and compared to the FRBR 
model. </p>
<p>Cards include simple lay descriptions of FRBR group 1 entities based on an 
example of Mozart's Eine kleine Nachtmusik. This example was chosen because it 
is generally well known and has been published in a variety of different forms 
and formats. </p>
<h2>Pilot study</h2>
<p>To test the design of our research, a small pilot study was conducted in 
March 2014. 4 participants were asked to complete both tasks, 2 for each 
example. The results of the pilot study show that participants understood the 
objectives, the instructions given, and that they had no major problems 
completing the tasks. </p>
<p>While none of the participants in the card sorting task sorted the cards 
completely according to the FRBR work, expression, manifestation, and item 
structure, the results show that their mental models are similar to the FRBR 
model. They can mostly identify the work as a separate entity (only one of the 
participants did not put it in a separate group), and can mostly differentiate 
between expressions, manifestations and items. They do, however, tend to 
separate expressions that are scores and expressions that are performances. None 
of the participants put all the expressions in the same pile. This is also the 
case with manifestations that derive from the expressions, for example an issue 
of the score or a recording of a performance. The interviews that were conducted 
alongside the card sort as well as the results of the concept mapping task show 
that this stems from the belief that scores and performances are not on the same 
level because scores are needed to perform the music, so they have priority. The 
results of the concept mapping task corroborate the result of the cart sort, 
namely that participants mostly recognize the work as a separate entity and to 
some extent differentiate between expressions, manifestations, and items. 
However, one participant did fail to produce a hierarchical concept map and 
organized the entities around a central entity - work and another did not put 
the work at the top of the hierarchy. </p>

<h2 id="author">About the authors</h2>
<p><br><strong>Manca Noc</strong> is a Young Researcher at the Department of Library and Information Science and Book Studies, Faculty of Arts, University of 
the Ljubljana, Slovenia. She can be contacted at <a href="mailto:manca.noc@ff.uni-lj.si">manca.noc@ff.uni-lj.si</a>
<br><strong>Maja Zumer</strong> is an Associate Professor at the Department of Library and Information Science and Book Studies at the Faculty of Arts, 
University of Ljubljana, Slovenia. Her research interest includes modelling of bibliographic information systems. She can be contacted at <a href="mailto:maja.zumer@ff.uni-lj.si"> 
maja.zumer@ff.uni-lj.si</a> </p>

</section>

<section class="refs">
<form>
<fieldset><legend style="padding: 0.1ex 0.5ex; color: white; font-size: medium; font-weight: bold; border-right-color: navy; border-bottom-color: navy; border-right-width: 1px; border-bottom-width: 1px; border-right-style: solid; border-bottom-style: solid; background-color: rgb(94, 150, 253);">References</legend> 
    
<ul class="refs">
  <li id="car06">Carlyle, A. (2006). Understanding FRBR as a conceptual model.   <em>Library Resources & Technical Services, 50</em>(4), 264-273.</li>
  <li id="cun03">Cunningham, S. J., Reeves, N., & Britland, M. (2003, May).   An ethnographic study of music information seeking: implications for the design of a music digital library. In <em>Proceedings of the 3rd ACM/IEEE-CS joint conference on Digital libraries</em> (pp. 5-16). Washington: IEEE Computer Society.</li>
  <li id="fun98"><em>Functional Requirements for Bibliographic Records: Final report</em> (1998). Munich, Germany: KG Saur.</li>
  <li id="lei92">Leiser, B. (1992). The presence phenomenon and other problems   of applying mental models to user interface design and evaluation. In Y. Roggers, A. Rutherford & P. A. Bibby, (Eds.), <em>Models in the mind: theory, perspective and application</em> (pp. 267-281). London: Academic Press.</li>
  <li id="lie06">Liew, C. L., & Ng, S. N. (2006). Beyond the notes: a qualitative study of the information-seeking behavior of ethnomusicologists. <em>The Journal of academic librarianship, 32</em>(1), 60-68.</li>
  <li id="nor83">Norman, D. A. (1983). Some observations on mental models. In D. Gentner & A. L. Stevens, (Eds.), <em>Mental models</em> (pp. 7-14). New 
  Jersey: Lawrence Erlbaum Associates, Inc. </li>
    <li id="pis07">Pisanski, J., & Zumer, M. (2007). Mental models and the bibliographic universe. In <em>Users and use of DL & economics of digital 
  libraries: LIDA 2007- libraries in a Digital Age</em> (pp. 69-76).</li>
  <li id="pis10a">Pisanski, J., & Zumer, M. (2010a). Mental models of the bibliographic universe. Part 1: mental models of descriptions. <em>Journal of 
  Documentation, 66</em>(5), 643-667.</li>
  <li id="pis10b">Pisanski, J., & Zumer, M. (2010b). Mental models of the bibliographic universe. Part 2: comparison task and conclusions. <em>Journal 
  of Documentation, 66</em>(5), 668-680.</li>
  <li id="pis12">Pisanski, J., & Zumer, M. (2012). User verification of the FRBR conceptual model. <em>Journal of Documentation, 68</em>(4), 582-592.</li>
  <li id="ril05">Riley, J. (2005). Exploiting musical connections: a proposal for support of work relationships in a digital music library. In <em>Proceedings of the International Conference on Music Information Retrieval</em> (pp. 123-129). London: ISMIR.</li>
  <li id="row95">Rowe, A. L., & Cooke, N. J. (1995). Measuring mental models: choosing the right tools for the job. <em>Human Resource Development Quarterly, 6</em>(3), 243-255.</li>
  <li id="tho11">Thomas, B. (2011). Creating a specialized music search interface in a traditional OPAC environment. <em>OCLC Systems & Services, 27</em>(3), 248-256.</li>
  <li id="zha09">Zhang, Y., & Salaba, A. (2009). What is next for functional requirements for bibliographic records? A Delphi study. <em>The Library, 79</em>(2), 233-255.</li>
  <li id="zha12">Zhang, Y., & Salaba, A. (2012). What do users tell us about FRBR-based catalogs? <em>Cataloging & Classification Quarterly, 50</em>(5-7), 705-723.</li></ul>
  </fieldset>
  </form>
  

</section>
</article>
<br>
<section>
   
 
 
 <br>
 <div style="text-align:center;"> 
     



  
</div>
<hr>
 
 
  
 </section>
 <footer>

<hr> 
 
 <hr>
</footer>
     
 </body>  
 </html> 