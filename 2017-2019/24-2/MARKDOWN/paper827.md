### vol. 24 no. 2, June, 2019

# A Floridian dilemma. Semantic information and truth

## [Ágústa Pálsdóttir](#author)

> **Introduction**. The study explored the opinion of Icelandic university students of the pros and cons of printed and electronic learning material. A better understanding of this is important for curriculum development and building a collection for university libraries. Answers were sought to the following research questions: what do students themselves say about the issue? What can we learn from their candid comments that is not captured by survey studies?  
> **Method**. This is part of an international survey conducted among students at two Iceland-ic universities in 2016\. The questionnaire contained an open question to which a total of 199 students replied.  
> **Analysis**. The comments were analysed qualitatively. The constant comparative method was employed.  
> **Results**. Five main themes were detected: 1) printed or electronic study material; 2) learning approach; 3) convenience and expenses; 4) technology: limitations and possibilities; 5) environmental issues.  
> **Conclusions**. The main results of the study relate to the students’ study habits and their view of how they can work with electronic reading material. They felt that electronic material offers certain possibilities. However, the opinion that came up more strongly was that the technology is neither sufficiently developed, nor properly used when electronic reading material is produced. The material, therefore, does not support their learning engagement sufficiently.

<section>

## Introduction

University students rely heavily on reading for their studies. In previous years, the possibilities for using digital class reading material have been changing and academic educational material is now increasingly available in electronic format ([Bergström, _et al._, 2017](#ber17); [Despot and Jakopec, 2013](#des13); [Wilders, 2017](#wil17); [Zhang and Niu, 2016](#zha16)). Knowledge of students’ attitudes toward reading formats, printed or electronic, is therefore im-portant for the organisation of courses and for curriculum development, as well as for universi-ty libraries for building a collection that suits their needs. The current paper will present results from an open question where Icelandic students presented their viewpoints of the advantages and disadvantages of printed and electronic material.

Previous studies have repeatedly found that university students prefer printed learning material rather than material in electronic format ([Kaznowska, Rogers and Usher, 2011](#kaz11); [Li, Poe, Potter, Quigley and Wilson, 2011](#li11); [Mizrachi, 2014](#miz14); [Mizrachi, _et al._, 2016](#miz16); [Pálsdóttir and Einarsdóttir, 2016](#pal16)). According to Singer and Alexander ([2017](#sin17)), the length of the text is among the various factors that influence the choice of format for reading. Students have been reported to read a short text on the screen but to prefer a longer academic text in print ([Baron, Calixte and Havewala, 2017](#bar17); [Mizrachi, 2014](#miz14); [Pálsdóttir and Einarsdóttir, 2016](#pal16)). There are also indications that electronic text-books are used more for scanning the text and searching for information, rather than to engage in responsive reading ([Freeman and Saunders, 2015](#fre15)). In addition, although it has been noted that students value the option of having access to digital learning material, studies have shown that they are likely to print some of it out for reading ([Ji, Michaels and Waterman, 2014](#ji14)). These findings can imply that university students consider the printed text format to have advantages over electronic text. This, furthermore, opposes the development that has taken place, with academic reading material increasingly being published in electronic form ([Bergström, _et al._, 2017](#ber17); [Wilders, 2017](#wil17); [Zhang and Niu, 2016](#zha16)).

There are indications that students consider certain advantages to be connected to the printed format. Baron, Calixte and Havewala ([2017](#bar17)) reported that the ease of making annotations in printed material was important to university students. Mizrachi ([2015](#miz15)) found that over 80% of the students in her study highlighted and annotated printed material, while only about one third of them did so with their electronic material. The results obtained by Pálsdóttir and Einarsdóttir ([2016](#pal16)) are similar, with 74% of the students highlighting and annotating printed material, compared to 35% doing so with electronic material. These findings were partly supported in a study by Rockinson-Szapkiw, Courduff, Carter and Bennett ([2013](#roc13)), who reported that being able to highlight the text, mark the pages by dog-earing them and to make handwritten notes, was seen as an advantage of the printed format. However, the same study also found that users of electronic textbooks were more likely to make notes directly into the text, than those who used printed books. Thus, even though students are of the opinion that the printed format can offer some positive options beyond the electronic format, this questions the extent they take advantage of it.

Studies have also shown that students value some aspects of electronic material. This includes being able to search inside and across electronic material, which was considered a benefit ([Baron, _et al._, 2017](#bar17); [Ji, _et al._, 2014](#ji14); [Zhang and Niu, 2016](#zha16)). They were also conscious of the possibility of getting electronic material free of charge ([Baron, _et al._, 2017](#bar17); [Ji, _et al._, 2014](#ji14)). The convenience of being able to bring a collection of study material with them in digital devices was, furthermore, appreciated ([Baron, _et al._, 2017](#bar17)). In addition, Farinosi, Lim and Roll ([2016](#far16)) reported that, although students saw the advantage of storing text digitally, they also pointed out that the use of it depended on the availability of electricity. In a study by Li _et al._ ([2011](#li11)), portability and mobility were mentioned as favourable attributes of electronic material, but interestingly, stu-dents felt that the same factors were among the advantages of printed material.

Students have reported negative physical side effects of using electronic educational material. This includes, for example, visual issues such as eyestrain and headaches when reading from the screen, and that students tire more easily ([Baron, _et al._, 2017](#bar17); [Bikowski and Casal, 2018](#bik18); [Farinosi, _et al._, 2016](#far16)).

Furthermore, how students can use educational material for learning is of great importance. Indeed, researchers have been asking whether students learn better by using printed or electronic material by investigating different aspects of learning engagement. Studies have reported that multi-tasking is higher when students read electronic material ([Baron, _et al._, 2017](#bar17); Daniel and Woody, 2013), they get more easily distracted and have more difficulties con-centrating ([Baron, _et al._, 2017](#bar17); [Farinosi, _et al._, 2016](#far16)). It has also been found that the majority of students believed that they can focus better on the course material, they remember it better if they read it in print, and they considered themselves to be more likely to review printed material ([Mizrachi, 2015](#miz15); [Pálsdóttir and Einarsdóttir, 2016](#pal16); [Põldaas, 2016](#pol16)).

Others have explored the issue of learning by conducting experiments that investigated reading speed, comprehension and recall, with quite divergent results and conclusions. Some studies have shown that the performance was better when reading from the printed format ([Kerr and Symons, 2006](#ker06); [Mangen, Walgermo and Brønnick, 2013](#man13); [Wästlund, Reinikka, Norlander and Archer, 2005](#was05)). Other studies have not found a significant difference in performance between the electronic and printed format ([Ball and Hourcade, 2011](#bal11); [Daniel and Woody, 2013](#dan13); [Lauterman and Ackerman, 2014](#lau14); [Porion, Aparicio, Megalakaki, Robert and Baccino, 2016](#por16)). However, Daniel and Woody ([2013](#dan13)) reported that reading time was longer when the students read electronic text.

Singer and Alexander ([2017](#sin17)) have pointed out that a definition of reading that includes digital reading is lacking. There are indications that reading electronic text requires students to possess skills beyond what is needed when text in printed format is being read. According to Goldhammer, Naumann and Keßel ([2013](#gol13)), basic computer skills, or the ability and speed of access-ing, collecting and providing information in a digital environment, relates positively with the ability to read electronic text. In addition, Hahne, Goldhammer, Naumanna and Kröhnea ([2016](#hah16)) found that students who had both good skills at reading printed text and good basic computer skills, performed better at reading electronic text. Thus, the students’ general reading skills as well as their abilities to work with digital text may have an impact.

Ball and Hourcade ([2011](#bal11)) have concluded that factors such as the familiarity and experience with the format is what matters, not the format itself. Lauterman and Ackerman ([2014](#lau14)) have, furthermore, suggested that students can be taught methods to improve digital reading. The points raised by Bikowski and Casal ([2018](#bik18)) are similar. They have noted that students may employ different learning strategies depending on what technology is being used. As students gain more experience with the use of the technologies, they also become more capable of utilis-ing specific learning functionalities and processes that can support their studying.

Previous studies have mainly used quantitative research methods, and this was also the case with the current study. However, the study contained one open question where the students were asked what they would like to say about their preferences for academic reading formats. From the comments that were made some new themes and issues were discovered, which the survey did not explore. In addition, the comments helped to explain and shed more light on the quantitative results about the students’ opinions and attitude towards the format of learning material.

This paper will focus on the results from the open question. The aim is to explore the opinion of Icelandic university students of the pros and cons of printed and electronic learning material. Earlier studies provide quantitative reports of university students' preferences for reading formats and activities. This paper goes deeper in order to address the questions: what do students themselves say about the issue? What can we learn from their candid comments that is not cap-tured by survey studies?

The purpose is to add to the knowledge about university students’ preferences for educational material and, specifically, why they chose to read it in printed or electronic format. A better understanding of their attitude is important for making policy decisions about the development of course syllabi and to what extent electronic reading material should be integrated into it. It is also of significance for university libraries for building a collection attuned to the students’ needs. Thus, the study may provide important knowledge about university students’ preferences for educational material.

</section>

<section>

## Research methods

This is the Icelandic part of an international online survey, conducted among all students at two universities, the University of Iceland and the University of Akureyri. The same questionnaire, adapted from the Academic Reading Questionnaire created by Mizrachi ([2014](#miz14)), was used by all participating countries. The questionnaire was translated from English to Icelandic. It consisted of background questions (sex, age, current status as student, field of study), 17 questions which emphasised preferences for reading format, attitude and reading behaviour, and choice of devices for reading electronic material. In addition, the questionnaire included one open question where the participants were asked: what else would you like us to know about your academic reading format preferences? This paper will focus only on results from the open question.

The survey was sent in 2016 by email through the universities’ registration offices, to 12,994 students at undergraduate, masters and doctoral levels. It was a census study, with the survey being sent to all members of the population and not confined to a sample of the students. The response rate was 7% which means that the findings from the quantitative part of the study cannot be generalised to all students at the universities. Nevertheless, given the large number of students who replied, a total of 910 students, the results may provide important insights into their preferences for educational material. Since data from the open question will not be analysed statistically, the issue of generalisability does not apply to it in the same way as the rest of the survey.

A total of 22% of participants, or 199 students, answered the open question. Of those, seventy-seven students were in the 20-24 years age bracket, thirty-one in the 25-29 years age bracket, thirty-nine in the 30-39 years age bracket and fifty-two students were 40 years or older. Of those who gave an answer about their sex, women were in majority, with one hundred female and twenty-three male participants, while seventy-six participants did not provide an answer about their sex. A total of ninety-seven students studied within the field of social sciences, sixty-eight within the field of sciences and twenty-five within the field of arts and humanities, nine students did not provide an answer about their field of study. The data were analysed according to qualitative methods and connections to participants’ age, sex or field of education will not be examined. The comments that were made varied in length, from a sentence of a few words and up to over ninety words in several sentences.

With the purpose of gaining a deep understanding of various aspects related to the students’ attitude and preferences in relation to electronic or printed study material, from their own point of view, it was decided to analyse the data qualitatively. The constant comparative method as described by Corbin and Strauss ([2008](#cor08)) was employed. Open coding was used to examine the data at the early stage of the analysis. The data were read, and words and phrases examined to note key remarks and concepts, descriptions were compared and grouped, and some initial themes developed. The comments were reanalysed at a later stage with the themes in mind. Axial coding was used, and questions asked about the conditions, actions and interactions, and consequences of the themes. Finally, selective coding was used to reorganise the data by making connections between the main themes and subthemes.

</section>

<section>

## Results

Five main themes were detected: 1) printed or electronic study material; 2) learning approach; 3) convenience and expenses; 4) technology: limitations and possibilities; 5) environmental issues.

### Printed or electronic study material

Some of the students explicitly expressed favouring either printed or electronic study material. ‘_Since all material is made digitally, nothing should stop this possibility’_, is an example of the comments made by those who only wanted electronic material. Another student said, ‘_the more teachers use electronic course readings, the happier I am_’.

Others said that they preferred printed reading material. Even though they believed that electronic material had certain advantages, they still wanted to read and study from print. ‘_I understand well that digital material is more convenient in many ways, but I prefer to have it in a printed form_’, was an expression of this opinion.

Two subthemes emerged here: a) flexibility; and b) printing electronic material

#### Flexibility

In some of the comments the students said that they wanted to be able to choose between printed or electronic format, depending on what they themselves considered to be best each time. ‘_I think it's best to be able to use reading material that is available both in digital form and print_’, said one of them. Their view was that sometimes it was appropriate to use electronic material, while in other cases it was better to use printed material. One of them expressed his wish for flexibility in the following way: '_I like to have a choice. Digital reading suits me better than printed in some courses, in others it's the opposite_'.

Thus, having the option to select the preferred format each time was favoured by them. One of the students described this so, ‘_It depends on the condition, the material and my mood_’.

#### Printing electronic material

The other subtheme involved the students’ wish to have the material ‘_digital with a possibility to print out’_. Several expressed the opinion that even though they had the material in electronic form they also needed to print it out, at least in some cases. ‘_I prefer electronic, but want to print it out_’, said one of them, while another student said, ‘_I print out some of the material that I think is particularly important, but not always_’.

### Learning approach

This theme describes the students’ viewpoints about how the format of the reading material influenced their learning engagement. Although the overall opinion was in favour of the printed format, as the following describes, ‘_I feel I learn better if the material is in a printed form_’, the electronic format was also considered to have certain advantages. Here three subthemes were discovered: a) the ability to concentrate on the reading; b) the ability to remember what was being read; and c) length of the text matters.

#### Ability to concentrate on the reading

It was of great importance for the students’ learning engagement that they would be able to focus on what they were reading. The overall picture that emerged was that they found it more difficult to concentrate, and therefore did not learn as much, when they used electronic material compared to when it was in print.

One aspect was that, because printed material existed as a physical object which they could hold and feel, it supported the learning process better than electronic material ‘_it's tangible and I connect better with what I'm learning_’, was an expression of this. Another student mentioned that using printed material helped him to focus on the reading and said, ‘_When using a computer, it's easier to get distracted than when you have to hold a book_’.

The study environment was also important. Some of the students complained that, when they worked in a digital environment, they were in a situation where they were easily distracted. The following is an expression of this: '_I find it difficult to keep focus on text in the computer, and it's often so tempting to do something else'_.

Yet another point was that the digital environment created various types of disturbance, or as one of them said ‘_and there's considerable interference. Messages and Snapchat keep popping up_’. To keep these distractions under control and resist the urge of shifting their attention from the reading could be demanding.

Thus, when the students needed to concentrate, print was considered to have certain advantages over the electronic format, because it created fewer opportunities for them to be diverted from their reading.

However, some felt that electronic material had positive impact on their learning engagement: '_when you read digitally it is not so clear how much of the text you have read already, which helps to keep concentration_'. Another student added ‘_the brightness of the background prevents you from becoming sleepy_’.

Moreover, they were not all completely in favour of either printed or electronic material, or as one of them said, ‘_It has little impact on academic performance whether it's in electronic format or print_'.

Furthermore, although the students liked to have the choice to read digitally, their estimation of how much they needed to use the material, could have some influence. ‘I print out all material that I need to use often or over a long time’, said one of them. A similar note was made by another student:

> If it's for large projects and essays, then it's better to have it on paper. But it's more convenient to have material that I don't need to use later electronically and be able to scroll through it.

This indicates that the choice depended on how much the students expected to use the reading material. Others, however, emphasised that the purpose of the reading was important. One of them explained this so: '_I prefer to read digitally, except for exams. Then I think it's best to write down and learn from paper_'.

The language of the text could also have some influence as the following remark indicated, ‘_I think it's better to read printed material rather than digitally. Particularly in other languages than Icelandic..._’

Hence, the purpose of the reading and how the students intended to work with the text could matter. That is, if they planned to read the text more superficially, or even just scan it, before they went to class, then electronic material could suit them. If they needed to concentrate more on the reading and working with the text, then printed material was considered more useful.

#### Ability to remember what was read

Most of the students who commented about this said that it was easier to memorise the text if the material was in print rather than electronic. ‘_I remember when I have the material in front of me in print_’, described their view on this. Although electronic material was believed to have its advantages, such as for browsing through it, it could not compete with print, ‘_I have to print it out to remember_’, said one of them.

Many of the comments emphasised the importance of connecting reading with actions, such as scribbling or underlining the text, and writing down notes. To do this manually was considered important. The following comments were an expression of this, ‘_I write all notes by hand…_’ and ‘_It’s more helpful to remember, rather than if I just print out the notes or keep them in the computer_’. One of the students said:

> I think it’s better to have it printed out so I can scribble on the paper and underline the most important things. I also feel that it’s more useful to write down by hand what I read, for example in the computer, rather than to make notes in the computer. That way I remember it better.

It was also mentioned that making notes manually was better for keeping focus and to get to the core of the text. The following remark was made about writing notes in the computer, ‘_I end up writing notes that are too long and I lose the main points_’. Furthermore, one of the students expressed his opinion on the difference between using printed or electronic material: '_Digital means that you become monotonous in your reading, and to make notes digitally becomes like a muscle memory rather than something that you are able to remember later on_'.

The general opinion was that, because of the way that the students preferred to work with the text while they read it, such as underlining and making notes, they remembered it better if it was in print and they could do this manually.

#### Length of the text matters

The third subtheme explains the students’ ideas about how the length of the text could influence their choice. In general, the opinion was that it was better to read longer texts in print. ‘_I prefer big textbooks (with a lot of pictures, graphs and tables) in print..._’ was an example of this. When the text was shorter, on the other hand, such as ‘_articles, handouts and all assignments and explanations_’, it was considered well-suited for reading digitally.

### Convenience and expenses

This theme demonstrates what the students saw as advantages of the electronic material over the printed material. The relevant factors that were mentioned were the possibilities to organise electronic material and the approachability of it, the volume of printed material, as well as the expenses of buying study material. This was divided up in two subthemes: a) organising, approachability and volume; and b) expenses.

#### Organising, approachability and volume

To have a good overview of the study material and to be able to approach it quickly and easily when needed was considered important. Some of the students found it easier to arrange it if it was electronic rather than printed. Among the comments that were made were the following, ‘_more convenient to keep track of digital material_’, and: '_better to have as much of it in a digital form as possible, because then I feel that it’s better organised_'.

Because electronic material was _‘always available in the phones and on the laptops_’, they did not have to worry about forgetting it at home. In addition, they considered it to be an advantage that it is less extensive than printed material. ‘_It’s less of an effort to take the computer with you than five books and a stack of sheets_’, and ‘_saves on both weight and size_’, were examples of the comments that were made.

Thus, the possibilities to organise and approach electronic material, together with how compact it is compared to printed material, appealed to the students.

#### Expenses

The option to save money by getting low-cost reading material was, furthermore, seen as an advantage. One of the students made the following remark:

> The price of books is important, and I choose electronic when it’s possible, if I don’t have to use the book a lot. And I try to print out as little as I can and read articles on the computer rather than to print them out.

Not having to pay for study material, as well as being able to control the expenses by reducing the cost of printing, seemed to have an impact on why the students chose to use digital material. This had a bearing, even when they preferred to read in print, as one of them explained, ‘_I choose whatever is cheapest, which is usually electronic. But I think it's better to read printed books_’. The price of textbooks was, furthermore, found to be high and one of them said, ‘_I don’t care about copyright_’. It was more important for him to be able to cut down on his expenses by getting the study material free of charge, or for a low price, than to obtain it by legal means.

### Technology: limitations and possibilities

This theme describes the students’ views of the attributes and limitations of the study material formats. This includes their ideas about technical possibilities, how the technology is being used when electronic study material is being produced and the need for improvements. The following subthemes were identified: a) searching and browsing; b) making notes, scribbling and highlighting; c) technological advancement; d) physical reasons.

#### Searching and browsing

It was considered good to be able to browse through electronic material, ‘_to see if it’s useful or not_’. The students, furthermore, valued the possibility of being able to search and quickly find the relevant items in the reading material. Even though their opinions varied somewhat, most of them discussed the benefits of electronic material. The following comments described this:

> In digital class readings it’s possible to search specific items, very fast and widely, which is an improvement over printed class reading.  
>   
> Makes things much easier to have everything electronic and to use ctrl+f to find specific items, rather than to go to a table of contents.

However, they did not all agree on this and there were also some comments that claimed the opposite. One of them said, ‘_More difficult to look material up, even though it’s possible to seek particular words_’. Another student added this about printed material, ‘_It's much easier to move between pages to check the material that I have read_’.

Nevertheless, despite some differences of opinions, the comments described in general what was considered an advantage of the electronic format. The ability to search through the text and look up specific items was appreciated, as it was found to ‘_improve the access to the reading material_’.

#### Making notes, scribbling and highlighting

This subtheme further clarified the significance of connecting reading with manual actions, which was described in the theme Learning approach together with the subtheme Ability to remember what was read. Many of the remarks explained how important it was for the students to be able to work with the reading material while they studied. This was done by, for example, making notes in the text, scribbling and underlining, as well as highlighting it, as the following remarks described:

> I prefer to work so that I underline material in a printed form, write in comments and mark the text with a coloured pen.  
>   
> When I read for exams, I use almost only material that is printed out, so that I can scribble and use pens to highlight the text.

Overall, the comments claimed that printed material was better suited for these activities as it was more difficult to work like this with electronic material:

> I think it’s much more convenient to read material that is printed out, because I think it’s easier to make notes.  
>   
> I have problems with it (electronic) and feel that I can’t make notes as easily.

Although the students recognised that it is possible to highlight text in electronic material, they felt that it was more problematic than if they worked with printed material. The comments, ‘_but it takes more effort..._', and, ‘_it takes much longer_’, were examples of their opinion on this.

#### Technological advancement

Several comments presented the students’ ideas of how significant it is to take good advantage of the possibilities that information technology offers, when electronic teaching material is provided. One of them said: '_Important that it’s definitely possible to use a software to search and that it’s easy to write comments and colour it_'.

The necessity for software that made it easy to add notes to the text was also emphasised: '_Often it’s difficult to make notes in PowerPoint, on a tablet. Think the problem is with Microsoft. They need to offer a solution so that you can write in the document…_'

It was also mentioned that it is essential to have software that allows one to ‘_zoom in and out as needed_’, as well as to ‘_turn the pages if they were scanned in from the side_'.

Some of the students expressed their view on the lack of study material that has especially been designed and produced in electronic format:

> In fact, the study material is usually designed as a traditional book or article and only transferred to an electronic form. And when this is done it’s possible to read the PDF. Instead, we should be able to design electronic content and possibly have a printout.

The use of PDF files was particularly discussed, and it was pointed out that they have limitations that make them inconvenient to use. An example of this point was the following:

> PDF is not suitable as it cannot be changed, such as reducing the margins and changing the font, in order to print out fewer pages.  
>   
> I don’t mind reading learning material digitally. BUT very often it’s photocopies that appear skewed on the screen and often there are some sort of black dots in the document.

Hence, if electronic material is to be a viable option, which allows the students to use it effectively for their study, it is important that it is produced digitally. Otherwise, it will continue to be difficult for them to read it and use as a learning material.

In addition, some students shared their thoughts about what was needed, for them to see electronic material as a suitable option:

> If there were specific tablets for reading textbooks, lightweight and convenient, and also textbooks formatted for tablets (easy to enlarge and reduce, underline and add comments), then I would try it.  
>   
> iPad or electronic books are remarkably well suited for reading, and to keep track of reading and learning materials. Especially when the amount of material is huge and many books etc. But usually the learning material is not available in appropriate format and is badly suited for reading from a computer screen.

Taken together, these comments indicated that the students were not negative towards using electronic reading material, provided that appropriate technological solutions were available, and that the material had been designed in a way that satisfied their needs.

#### Physical reasons

The last subtheme describes how reading electronic material could influence the students physically. The comments that were made expressed negative points about digital reading. The following remarks were examples of this, ‘_My eyes get very quickly tired when I read digital material_’, as well as, ‘_It’s much better to read from paper, background light on a computer screen disturbs_’.

In addition to complaints about bad consequences for eyesight, there were remarks about other negative influences. Some of the students, for example, tended to get a headache or had difficulty sleeping:

> I was diagnosed with migraines several years ago, which has an impact on my choice now.  
>   
> To read a text electronically badly affects my sleep. Usually when people study, they need to work late into the evening in front of the computer, so it’s not helpful to use it also to read text.

Hence, at least some of the students struggled with physical problems in relation to reading digitally, which made it difficult for them to use electronic material.

### Environmental issues

Some students were concerned about the environment and discussed how important it was to take it into account when reading material is being produced. ‘_I feel guilty printing out a lot of pages_’, was an example of this point of view, as well as:

> I feel that there is so much waste of paper in the school system and therefore I choose to read digitally as much as possible.  
>   
> I think it’s important to get used to reading the material in a digital form because you save trees by not making printed books.

Yet another student made the following remark, ‘_Adjustment is the key. The question is, for how long the environment allows us to adjust_'.

Hence, considerations about the environment, with an emphasis on the need to cut down on the waste of paper, was an issue that some of the students had in mind when choosing the electronic form rather than printed material.

</section>

<section>

## Discussion

Academic learning material is increasingly provided in electronic format but knowledge about the implications for students’ learning is still quite limited. It is therefore necessary to understand better how they feel that educational material, printed or electronic, supports their learning, and in what areas they think reforms are needed.

This paper presents qualitative findings from an open question where Icelandic university students expressed their opinions and attitude towards the format of learning material. Five main themes emerged from the comments: 1) printed or electronic study material; 2) learning approach; 3) convenience and expenses; 4) technology: limitations and possibilities; 5) environmental issues.

The findings show that, while some students were explicitly in favour of either printed or electronic study material, others wished to be able to select the format that suits them each time. The quantitative part of the study did not contain a question about this flexibility of choice. The results from it show that the students were more likely to favour printed material ([Pálsdóttir and Einarsdóttir, 2016](#pal16)), a finding which has repeatedly been reported by others ([Kaznowska, Rogers and Usher, 2011](#kaz11); [Li, _et al._, 2011](#li11); [Mizrachi, 2014](#miz14); [Mizrachi, _et al._, 2016](#miz16)). The purpose of using qualitative methods, to analyse the data from the open question, was to gain a deeper understanding of various aspects related to the students’ choice of publication format, from their own point of view ([Corbin and Strauss, 2008](#cor08)). The findings from it indicate that some students considered both formats to have its pros and cons. Being able to choose when they wanted to read the material in print or electronically, and not to be bound by the format, was therefore an option that was favoured by them.

When the students are expected to read electronic material, it is essential that it supports their learning engagement. The main findings of the study relate to their learning approach and how the enablers and constraints of the reading formats influenced their use of the source for learning.

According to the quantitative part of the study, most of the students claimed that they could focus better on printed material, remembered it better, and were more likely to highlight, notate and review the text ([Pálsdóttir and Einarsdóttir, 2016](#pal16)). By explaining how and why the students preferred to work with the text while reading it, the theme Learning approach provided further insight into this finding. This was done by, for example, underlining and making comments in the text. Being able to scribble, underline and write down notes was not only important because it helped the students to locate important issues when they returned to the text, as was suggested by Baron, _et al._ ([2017](#bar17)). It was considered essential for their learning strategies, as it helped them to focus on what they read, as well as to draw meaning out of it, and to remember it.

The same point was explained from a different angle under the theme _Technology: limitations and possibilities_. Although the students realised that they could highlight the text and make notes in the electronic format, they felt that it was not as easy as if the text was in print. Many of the comments described why it was thought to be more effort to work like this with electronic text. In addition, some of the comments pointed out that when learning material is produced in an electronic format it is necessary to use software that makes this easy for them.

Problems with concentrating while reading digital material were not only related to how the students preferred to work with the text, or to the technical restraints of the electronic format. There were also complaints that they were more easily distracted when reading from the screen. This could be because of disturbances when unrelated items, such as messages and Snapchat, popped up on the screen. But they also mentioned that they had more difficulties focusing on the reading because of the temptation to do something else on the computer, as was also described by Daniel and Woody ([2013](#dan13)). Thus, because the students needed to put an effort into controlling these distractions and keeping their focus on the reading material, the digital study environment could be quite demanding for them.

Furthermore, it is inevitable, that, if the students experience negative physical symptoms when they read from a screen, it will constrain their ability to focus on the learning material, and thereby disturb their learning. Some of them made remarks about eye strain, headaches and sleep disturbance, which made it difficult for them to read digital text. Similar symptoms have been reported about writing on a computer rather than by hand ([Baron, _et al._, 2017](#bar17); [Farinosi, _et al._, 2016](#far16)).

Some of the findings were, at least partly, in line with the results from the quantitative part of the survey ([Pálsdóttir and Einarsdóttir, 2016](#pal16)), but helped to shed more light on it and explain the reason behind it. This relates, for example, to the theme Learning approach and results about how the length of the text impacts the students’ choice. The survey found that 53% preferred to read material which is seven pages or more in print, which is in line with results from other research ([Baron, _et al._, 2017](#bar17); [Mizrachi, 2014](#miz14); [Singer and Alexander, 2017](#sin17)). The findings from the open question, however, described how the purpose for reading, and how much attention the students felt was needed, influenced their preferences. The viewpoint that emerged was that electronic material can be used for more superficial reading. This could, for example, be before going to class, when they did not need to focus specifically on what they read. Whereas, the printed format was considered better when they needed to read more in depth, such as for exams. The findings, therefore, indicate that not only the length of the text, but also the purpose for the reading and the level of concentration that is required, can influence the student’s choice of reading format.

Although the findings highlighted what was considered negative aspects of electronic reading material, there were also comments that pointed out positive sides of it. The theme _Convenience and expense_s described what the students saw as advantages of the electronic format over the printed format. The compactness of the material, together with organising possibilities, the ease of carrying it around and easy access to it when and wherever it was needed, appealed to the students. In addition, they valued the potentials of searching in the electronic text. The quantitative part of the study did not ask about these features, but these qualitative findings support results from other research ([Li _et al._, 2011](#li11); [Baron, _et al._, 2017](#bar17); [Zhang and Niu, 2016](#zha16)).

Furthermore, using electronic material was considered an opportunity to cut down on study expenses, as was also reported by Baron, _et al._ ([2017](#bar17)) and Ji, _et al._ ([2014](#ji14)). It has been suggested that free digital educational material may become a trend soon ([Despot and Jakopec, 2013](#des13)). As the main language of the learning material used by Icelandic university students is English, they are not confined to use material published in Icelandic. Since 2002, there has been good access to a wide range of electronic academic journals through the Iceland Consortium ([n.da](#icenda)), in addition to subscriptions bought by the universities. Thus, the students have excellent opportunities to obtain free electronic educational material in the form of journal papers. Although the number of electronic books has been growing in the past years, their possibilities to access free electronic textbooks through the Iceland Consortium ([n.db](#icendb)) is, however, much more limited. The same has also been found to be the case for Swedish university students ([Bergström _et al._, 2017](#ber17)).

Finally, environmental issues were not examined by the quantitative part of the study ([Pálsdóttir and Einarsdóttir, 2016](#pal16)) but it emerged as quite a strong viewpoint in the comments. It is clear that at least some of the students were concerned about the environment. They saw the use of electronic material as a positive option, a way of cutting down on the use of paper. Comparable ideas have been reported and suggestions made that social and cultural factors may have an impact on this ([Baron, _et al._, 2017](#bar17)).

It can, however, be difficult to distinguish between knowledge about environmental issues and attitudes towards them, and it is possible that the viewpoint of the students in the current study was based on misconception. Chowdhury ([2014](#cho14)) has argued that, although the carbon footprint can be reduced by replacing printed reading material with digital, questions are still unanswered about the overall environmental cost, for example, the increase in use of electricity for digital material. The environmental cost of producing electricity varies, however, greatly between countries. In Iceland, virtually all electricity supply is derived from environmentally friendly, renewable energy sources ([Statistics Iceland, 2018](#sta18)), which supports the students’ position on the positive impact of using electronic material. On the other hand, if they choose to print out electronic material, as some of them claimed to do, the environmental benefits must be reduced.

The findings are from one open question, with the data being analysed qualitatively, and cannot be generalised to the wider population of university students. Nevertheless, because the findings go beyond the quantitative results of the study, they add new insights, at a deeper level, into the students’ perspective. They may, therefore, help to shed more light on their preferences and attitudes towards the printed and electronic reading formats.

As academic educational material is increasingly being provided in an electronic format, it is important to understand better how the students can use it for their learning. The findings indicate that they feel that electronic material does not suit their learning styles well enough and that it is more difficult to concentrate on it. Thus, it cannot sufficiently support their learning engagement. This may, at least to a certain extent, explain why so many of them preferred using printed material. However, we live in a world where technology changes rapidly and it can be expected that the possibilities of using it to produce study material will develop. In addition, devices for reading and working with the text, such as marking it and making notes, are constantly evolving, which can have an impact on the students’ attitudes. Consequently, there is a need for more research on the various issues related to how and why university students favour printed or electronic study materials.

</section>

<section>

## About the author

**Ágústa Pálsdóttir** is a Professor in the Department of Information Science, University of Iceland, Iceland. She received her Bachelor's degree in Library and Information Science and Master of Library and Information Science from the University of Iceland and PhD in Information Studies from the Department of Information Studies, Abo Akademi University, Finland. She can be contacted at [agustap@hi.is](mailto:agustap@hi.is)

</section>

<section class="refs">

## References

*   Ball, R. & Hourcade, J.P. (2011). Rethinking reading for age from paper and computers. _International Journal of Human–Computer Interaction, 27_(11), 1066-1082\.
*   Baron, N.S., Calixte, R.M. & Havewala, M. (2017). The persistence of print among university students: an exploratory study. _Telematics and Informatics, 34_(5), 590-604\.
*   Bergström, A., Höglund, L., Maceviciute, E., Nilsson, S.K., Wallin, B. & Wilson, T.D. (2017). E-books in Swedish academic libraries. In _Books on screens: players in the Swedish e-book market_, (pp. 159-180). Gothenburg, Sweden: Nordicom.
*   Bikowski, D. & Casal, J.E. (2018). Interactive digital textbooks and engagement: a learning strategies framework. _Language Learning & Technology, 22_(1), 119–136\.
*   Chowdhury, G. (2014). Sustainability of digital libraries: a conceptual model and a research framework. _International Journal on Digital Libraries, 14_(3-4), 181-195\.
*   Corbin, J. & Strauss, A. (2008). _Basics of qualitative research: techniques and procedures for developing grounded theory_. 3rd. ed. Thousand Oaks, CA: Sage Publications.
*   Daniel, D.B. & Woody, W.D. (2013). E-textbooks at what cost? Performance and use of electronic v. print texts. _Computers & Education, 62_, 18-23\.
*   Despot, I. & Jakopec, T. (2013). The strategy for the development of electronic publishing in small markets. _Libellarium, 6_(1-2), 81-90\.
*   Farinosi, M., Lim, C & Roll, J. (2016). Book or screen, pen or keyboard? A cross-cultural sociological analysis of writing and reading habits basing on Germany, Italy and the UK. _Telematics and Informatics, 33_(2), 410-421\.
*   Freeman, R.S. & Saunders, E.S. (2015). E-book reading practices in different subject areas: an exploratory log analysis. In Suzanne M. Ward, Robert S. Freeman & Judith M. Nixon (Eds.), _Academic e-books: publishers, librarians and users_, (pp. 223-248). Indiana: Purdue University Press.
*   Goldhammer, F., Naumann, J. & Keßel, Y. (2013). Assessing individual differences in basic computer skills: psychometric characteristics of an interactive performance measure. _European Journal of Psychological Assessment, 29_(4), 263-275.
*   Hahne, C., Goldhammer, F., Naumanna, J. & Kröhnea, U. (2016). Effects of linear reading, basic computer skills, evaluating online information, and navigation on reading digital text. _Computers in Human Behavior, 55_, 486-500\.
*   Iceland Consortium (n.da). [About the Iceland Consortium](http://www.webcitation.org/78DcPyi7k). Reykjavik: Iceland Consortium. Retrieved from https://hvar.is/index.php?page=information-page (Archived by WebCite® at http://www.webcitation.org/78DcPyi7k)
*   Iceland Consortium (n.db). _[E-books in national access](http://www.webcitation.org/78Dd1xd0I)_. Reykjavik: Iceland Consortium. Retrieved from https://hvar.is/index.php?page=e-books (Archived by WebCite® at http://www.webcitation.org/78Dd1xd0I)
*   Ji, S.W., Michaels, S. & Waterman, D. (2014). Print vs. electronic readings in college courses: cost-efficiency and perceived learning. _Internet and Higher Education, 21_, 17–24.
*   Kaznowska, E., Rogers, J. & Usher, A. (2011). _The state of e-learning in Canadian universities, 2011: if students are digital natives, why don’t they like e-learning?_ Toronto, Canada: Higher Education Strategy Associates.
*   Kerr, M.A. & Symons, S.E. (2006). Computerized presentation of text: effects on children’s reading of informational material. _Reading and Writing, 19_(1), 1–19.
*   Lauterman, T. & Ackerman, R. (2014). Overcoming screen inferiority in learning and calibration. _Computers in Human Behavior, 35_, 455-463.
*   Li, C., Poe, F., Potter, M., Quigley, B. & Wilson, J. (2011). _UC libraries academic e-book usage survey_. California: University of California Libraries.
*   Mangen, A, Walgermo, B.R. & Brønnick, K. (2013). Reading linear texts on paper versus computer screen: effects on reading comprehension. _International Journal of Educational Research, 58_, 61-68.
*   Mizrachi, D. (2014). Online or print: which do students prefer? In Serap Kurbanoglu, Sonja Spiranec, Esther Grassian, Diane Mizrachi & Ralph Catts (Eds.), _Information Literacy, Lifelong Learning and Digital Citizenship in the 21st Century. Second European Conference, ECIL 2014, Dubrovnik, Kroatia, October 20-23, 2014, Proceedings_, (pp. 733-742). Heidelberg, Germany: Springer. (Communications in Computer and Information Science, CCIS, 492).
*   Mizrachi, D. (2015). Undergraduates’ academic reading format preferences and behaviors. _Journal of Academic Libraries, 41_(3), 301–311.
*   Mizrachi, D., Boustany, J., Kurbanoğlu, S., Doğan, G., Todorova, T. & Vilar, P. (2016). The academic reading format international study (ARFIS): investigating students around the world. In Serap Kurbanoğlu, Joumana Boustany, Sonja Špiranec, Esther Grassian, Diane Mizrachi, Loriene Roy & Tolga Çakmak (Eds.), Information literacy: key to an inclusive Society. 4th European conference, ECIL 2016, Prague, Czech Republic, October 10-13, 2016, Revised Selected Papers (pp. 215-227). Heidelberg: Springer. (Communications in Computer and Information Science, CCIS, 766).
*   Pálsdóttir, Á. & Einarsdóttir, S.B. (2016). Print vs. digital preferences: study material and reading behavior of students at the University of Iceland. In Serap Kurbanoğlu, Joumana Boustany, Sonja Špiranec, Esther Grassian, Diane Mizrachi, Loriene Roy & Tolga Çakmak (Eds.), _Information literacy: key to an inclusive society. 4th European conference, ECIL 2016, Prague, Czech Republic, October 10-13, 2016, Revised Selected Papers_ (pp. 228-237). Heidelberg, Germany: Springer. (Communications in Computer and Information Science, CCIS, 766).
*   Põldaas, M. (2016). Print or electronic? Estonian students’ preferences in their academic readings. In Serap Kurbanoğlu, Joumana Boustany, Sonja Špiranec, Esther Grassian, Diane Mizrachi, Loriene Roy & Tolga Çakmak (Eds.), _Information literacy: key to an inclusive society. 4th European conference, ECIL 2016, Prague, Czech Republic, October 10-13, 2016, Revised Selected Papers_ (pp. 215-227). Heidelberg: Springer. (Communications in Computer and Information Science, CCIS, 766).
*   Porion, A., Aparicio, X., Megalakaki, O., Robert, A. & Baccino, T. (2016). The impact of paper-based versus computerized presentation on text comprehension and memorization. _Computers in Human Behavior, 54_, 569-576.
*   Rockinson-Szapkiw, A.J., Courduff, J., Carter, K. & Bennett, D. (2013). Electronic versus traditional print textbooks: a comparison study on the influence of university students’ learning. _Computers & Education, 63_, 259–266.
*   Singer, L.M. & Alexander, P.A. (2017). Reading on paper and digitally: what the past decades of empirical research reveal. _Review of Educational Research, 87_(6), 1007 –1041.
*   Statistics Iceland. (2018). [_Iceland in figures 2018, Volume 23._ Reykjavik: Statistics Iceland.](http://www.webcitation.org/78DfW3LwP) Retrieved from https://hagstofa.is/media/51192/iceland_in_figures_2018.pdf (Archived by WebCite® at http://www.webcitation.org/78DfW3LwP)
*   Wästlund, E., Reinikka, H., Norlander, T. & Archer, T. (2005). Effects of VDT and paper presentation on consumption and production of information: psychological and physiological factors. _Computers in Human Behavior, 21_(2), 377-394.
*   Wilders, C. (2017). Predicting the role of library bookshelves in 2025\. _Journal of Academic Librarianship, 43_(5), 384-391.
*   Zhang, T. & Niu, X. (2016). The user experience of e-books in academic libraries: perception, discovery and use. In Suzanne M. Ward, Robert S. Freeman & Judith M. Nixon (Eds.), _Academic e-books: publishers, librarians and users_, (pp. 207-222). West Lafayette, IN: Purdue University Press.

</section>