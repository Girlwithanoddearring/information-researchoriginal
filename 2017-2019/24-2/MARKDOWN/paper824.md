### vol. 24 no. 2, June, 2019

# Everyday life information experiences in Twitter: a grounded theory

## [Faye Miller, Kate Davis](#author) and [Helen Partridge](#author).

> **Introduction**. This paper presents the findings from a project that investigated people's everyday life information experiences on Twitter.  
> **Method**. The project employed constructivist grounded theory methodology, which emphasizes personal, subjective meaning-making or construction of reality. Eleven people from Boston, Massachusetts participated in the study. Each person participated in two in-depth interviews.  
> **Analysis**. The study used the methods of constant comparison to create codes and categories towards constructing a new substantive model of information experiences on Twitter in the context of everyday life.  
> **Results**. The substantive model constructed consists of twelve categories: being aware of audiences; making sense of uncertainty; being part of a community; conversing freely; observing the world; having instant sources; being humorous; documenting life moments; being dependent; self-regulating; broadening horizons; and valuing diverse voices.  
> **Conclusion**. A conceptual model of people's everyday life experiences on Twitter was developed from an innovative information experience lens. The model can be used to inform research and design, and to lead to better digital, social and personal outcomes related to social media.

<section>

## Introduction

Social media are significantly altering the nature of human interaction and how individuals and communities connect, communicate and use information. These changes are reflected in recent inquiries from the public, the media and scholars into the various ways people experience social media and digital intimacies as an essential part of many different lifestyles, combining digital and offline experiences ([Murthy, 2018](#mur18); [Shields-Dobson, Robards and Carah, 2018](#shi18)). Whilst a growing body of research has begun to explore people's social media use in everyday life (i.e., not within formal study or work) from the perspective of journalism and the public sphere ([Bruns, 2018](#bru18)), political activism ([Murthy, 2018](#mur18)), social movements ([Barker-Plummer and Barker-Plummer, 2017](#bar17)), data activism as civic engagement ([Khan, Onye and Du, 2018](#kha18)) and human information behaviour on social live-streaming ([Scheibe, Fietkiewicz and Stock, 2016](#sch16)), very little research has investigated the phenomenon of social media from the field of information experience. This project addresses this gap. It investigated people's information experiences on one of the world's most popular social media platforms, Twitter. The paper begins with a brief overview of the existing relevant literature. This is followed by a discussion of the research method, including how information experience is delineated in the context of the current study. It then presents a detailed description of the research findings and concludes with a discussion of how this study increases our understanding of Twitter through an information experiences lens.

</section>

<section>

## Literature review

There is a growing body of research that examines information behaviour, including information sharing and information use, within and encompassing various facets of the Twittersphere, both virtual and non-virtual. (The Twittersphere is defined as Twitter users and their experiences as a collective, including the effects of having used Twitter, as experienced while people are offline; that is, mental and emotional spaces.) These studies, both qualitative and quantitative, explore information behaviour within a range of professional or workplace contexts, such as health and medical ([Neiger, Thackeray, Burton, Thackeray and Reese, 2013](#nei13)), librarianship ([Gunton and Davis, 2012](#gun12); [Shulman, Yep, and Tome, 2015](#shu15); [Mulatiningsih, Partridge and Davis, 2013](#mul13)), education ([Linek, Teka Hadgu, Hoffmann, Jäschke, and Puschmann, 2017](#lin17)), technology ([Talip, 2016](#tal16)), politics and government ([Stieglitz and Dang-Xuan, 2013](#sti13); [Halberstam and Knight, 2016](#hal16)), crisis and emergency management ([Pang and Ng, 2016](#pan16)), cybersecurity ([Jeske, McNeill, Coventry and Briggs, 2017](#jes17)) and financial markets ([Tafti, Zotti and Jank, 2016](#taf16)). While each of these studies increase our understanding of how various professional groups interact with and experience Twitter as a source or mediator of information between workplace and clients or public users, there is a gap in the literature that focuses on peoples' lived information experiences relating to their everyday use and non-use of Twitter. This study aimed to fill that gap.

There is a small amount of recent research that investigates and develops the concept of _information experience_ as a domain and object of study within social media, including Twitter as well as other forms of social media and microblogging. These are mainly qualitative studies using grounded theory or influenced by phenomenography, and they explore information experience intertwined with human life experiences, including new mothers' information experiences in social media ([Davis, 2015](#dav15)) and information experiences of citizens in social media in times of natural disasters ([Bunce, Partridge and Davis, 2012](#bun12); [Yates and Partridge, 2015](#yat15)). For example, one of the information experiences identified in Davis' study is '_experiencing moments of light_', with Twitter (as opposed to Facebook) specifically mentioned by some participants as an anonymous and non-judgemental space for experiencing empathy and solidarity. Yates and Partridge's study highlighted the '_convenience experience_' of the localised nature of information sharing and being updated on Twitter during a natural disaster.

While not specifically using the information experience lens, there have been studies into the nature of information-related lived experiences and situations on Twitter from a citizen-based perspective. These have been explored using qualitative, quantitative and mixed methodology research approaches, and include studies into information sharing through tweeting about mental health ([Berry, _et al._, 2017](#ber17)), information flows of citizen’s retweets for tsunami early warning systems ([Chatfield and Brajawidagda, 2012](#cha12)), sharing information on crisis events ([Tonkin, Pfeiffer and Tourte, 2012](#ton12)) and experiencing micro-serendipity in everyday life ([Bogers and Bjorneborn, 2013](#bog13)).

Similarly, there have been a number of educational research studies into Twitter since it is increasingly recognized as a major transformative force for higher education research and teaching practices ([Veletsianos and Kimmons, 2016](#vel16)) and lifestyles encompassing personal issues outside of work, such as online harassment ([Velesianos, Houlden, Hodson and Gosse, 2018](#vel18)). While these studies have focused on samples, both large and small scale, drawn from specific groups or occupations or particular issues of public interest, this study concentrates on understanding the nature of information experiences across a sample of people representing diverse backgrounds from one geographical location.

</section>

<section>

## Conceptualising information experience

The intent of this study was to investigate people’s everyday information experiences on Twitter. Before proceeding, it is necessary to provide some explanation of how the idea of information experience as a research object is delineated for this study. Information experience may be understood as a ‘_complex, multi-dimensional engagement with information_’ in real-world contexts ([Bruce, Davis, Hughes, Partridge and Stoodley, 2014](#bru14), p. 4). Information experience ‘_integrates all information related - actions, thoughts, feelings and has social and cultural dimensions_’ (_Hughes, 2014_, p. 34).

Davis ([2015](#dav15)) suggests that through information experience research, we can attend to: how people experience information, the ways they engage with information, what they experience as information, the nature of their experiences with information, and, their thoughts and feelings related to their information experience. Davis goes on to describe information experience as having three inseparable dimensions: (i) people: individuals and their worldviews, emotions, backgrounds, thoughts and feelings; (ii) information, in its myriad forms and as indicated in the data; (iii) context: the space (physical and/or virtual) in which the experience occurs.

Current views of information experience as a research object draw upon and are influenced by phenomenology, which seeks to investigate the unique meanings that comprise people’s lived experience of a particular phenomenon. Central to phenomenology is its interest in what is described as the life-world, which refers to ‘the world of immediate experience’, or ‘the existent world as we find ourselves in it’ ([Adams and van Manen, 2008](#ada08), p. 617). In attending to the idea of the life-world, interest is placed on understanding the inter-subjective world of human experience, which comprises people’s thoughts and actions, along with the social manifestation of these ([Schwandt, 2007](#sch07)). Thus, this study’s interest and intent was to examine people’s lived experience of Twitter as an informational life-world in the context of everyday life.

</section>

<section>

## Research approach

The purpose of this study was to explore and understand the ways people experience a particular phenomenon (i.e., everyday life information experience on Twitter). Consequently, a qualitative and interpretive research approach was employed. This study employed constructivist grounded theory methodology ([Charmaz, 2006](#cha06)) to explore people’s information experiences on Twitter in everyday life contexts. The rationale behind grounded theory is that theory should be grounded in empirical evidence, that is, evolve from data rather than be developed a priori and then tested. Constructivist grounded theory emphasizes personal, subjective meaning-making or construction of reality. The following paragraphs outline the process of developing the theoretical model using constructivist grounded theory.

The participants were eleven people living in Boston, Massachusetts: nine females and two males, aged between 25 and 54, with an average age of 29.5 years. Eight of the participants were employed with occupations including research fellow, librarian, teaching assistant, mail clerk, marketing associate and journalist; three participants indicated they were unemployed. Five of the participants had completed a postgraduate degree, five had completed an undergraduate degree, and one participant had completed high school. Participants were recruited by purposive and snowball sampling. Recruitment messages were added to the Facebook pages of local public library services and messages were also posted via Twitter using a range of hashtags such as #publiclibrary #boston. Facebook was used to broaden the recruitment process, as it was seen to be an alternative social media platform that Twitter uses might also use. Participants were also invited to recommend suitable others to be invited to take part in the study.

Each participant took part in two 30- to 60-minute semi-structured interviews. The interviews were all conducted by one member of the research team who was located in Boston, Massachusetts, at the time of data collection. Multiple sequential interviews helped ensure the appropriate quantity and quality of data was obtained to ‘_increase conceptual precision_’ of the emerging ideas ( [Charmaz, 2006](#cha06), p. 201). The interviews were conversational in nature allowing co-construction of knowledge between the participant and the interviewing researcher. The first interview with each participant focused on allowing participants to explain and describe their information experience on Twitter as part of their everyday life in a broad and holistic way. Interviews began with the question: When did you start using Twitter? This question was designed to allow the participants to respond without constraint and for a dialogue to be established between them and the interviewer. Probing questions were used to explore the participant’s responses and experiences, including: Could you explain that further? Could you tell me more about that? Could you please give me an example? Following the advice of Charmaz on the careful use of terminology and the emergent nature of grounded theory interviewing, the phrase _information experience_ was not used. It was recognised that participants were unlikely to be familiar with this phrase. Instead, the interview guide used language that would encourage participants to reveal their experiences of using Twitter as an information world, which would elicit data that related to their information experience.

The second or follow-up interview was conducted after a one week’s observation of participants’ activities on Twitter. With each participant’s permission, the project team member who conducted the data collection followed the participant on Twitter. The observations allowed this team member to get a sense of how the participants used Twitter on a daily basis. By observing the participants, the team member was able to see some of the practices the participants spoke about in the first interviews. The notes made from the observations informed what was explored in the follow-up interviews. For example, the interviewer could highlight an instance of something occurring in the participant’s Twitter feed and ask them to explain what was happening, as well as their thoughts and feelings about it. The notes were not treated as data _per se_, and as such were not included as part of the data analysis process. Their purpose was solely to inform the second interview. The notes were not shared with the other members of the research team.

One member of the project team took the lead in undertaking the coding of the interviews, with the other project team members providing critical commentary and insights to the process. All interviews were transcribed by a professional transcription service. Initial line-by-line coding of interview transcripts and memos was carried out, and from these, early categories were developed. Data analysis in the focused-coding phase targeted key processes or action verbs ([Charmaz, 2006](#cha06)). The majority of open (focused) coding and model development was carried out manually using tables in a word processor to enable the researchers to engage with the constant comparison technique towards developing the theoretical model of people’s everyday life information experiences in Twitter.

</section>

<section>

## Findings

Twelve categories of experience were constructed through a constructivist grounded theory analysis process. These categories are:

1.  Being aware of audiences.
2.  Making sense of uncertainty.
3.  Conversing freely.
4.  Being part of a community.
5.  Observing the world.
6.  Having instant sources.
7.  Being humorous.
8.  Documenting life moments.
9.  Being dependent.
10.  Self-regulating.
11.  Broadening horizons.
12.  Valuing diverse voices.

Together these twelve categories represent a substantive grounded theory or model that explains the nature of people’s everyday life information experiences in Twitter., The overarching theory describes how people experience information through six thematic couplets each consisting of two related categories (Table 1):

<table class="center" style="width:65%;"><caption>  
Table 1: Relationship between information experience themes and categories</caption>

<tbody>

<tr>

<th style="width:50%;">Themes (1-6)</th>

<th>Categories (1-12)</th>

</tr>

<tr>

<td>1\. Information to build one’s audience and interpretation of Twitter’s purposes</td>

<td style="font-style:italic;">1\. Being aware of audiences  
2\. Making sense of uncertainty</td>

</tr>

<tr>

<td>2\. Individual and communal interaction with information</td>

<td style="font-style:italic;">3\. Conversing freely  
4\. Being part of a community</td>

</tr>

<tr>

<td>3\. Twitter as a digital and mental space for everyday life information</td>

<td style="font-style:italic;">5\. Observing the world  
6\. Having instant sources</td>

</tr>

<tr>

<td>4\. Information as life moments</td>

<td style="font-style:italic;">7\. Being humorous  
8\. Documenting life moments</td>

</tr>

<tr>

<td>5\. Depending on and self-regulating constant streams of information</td>

<td style="font-style:italic;">9\. Being dependent  
10\. Self-regulating</td>

</tr>

<tr>

<td>6\. Empowering self and others through being informed</td>

<td style="font-style:italic;">11\. Broadening horizons  
12\. Valuing diverse voices</td>

</tr>

</tbody>

</table>

</section>

<section>

### Theme 1: Information to build one’s audience and interpretation of Twitter’s purposes

#### Category 1: Being aware of audiences

Participants described their Twitter experiences as being informed by knowing their audiences and becoming known, to help build and maintain audiences. For participants, audience awareness means being conscious of who is viewing their interactions and content, including tweets they post. It can also mean engaging with information to perform identity for an audience.

One of the main themes related to audience awareness is building and maintaining audience relationships. Building an audience involves finding and listening to other people (Participant 1). Participants seek to know their audiences and their potential interest in particular posts and seek to post content that resonates with those audiences (Participant 11). Over time, they become known to their audiences by the content they post and the style of their tweets.

Some participants experience being conscious of their own and others’ audience personas, identities or online image portrayals in a performative sense. This awareness and understanding of layers of audience personas (i.e. life in physical world versus online world) can inform their tweeting experiences, particularly how they choose to use Twitter and their styles of communicating with their audiences, as Participant 1 described:

> I think I am consciously aware of like when I’m being performative for an audience. Whether it be in social media or in real life which is a funny thing to think about! (Participant 1)

In contrast, Participant 4 displayed a strong awareness of a particular image she may have presented through her tweets, but at the same time did not concern herself with ‘_trying too hard to craft a specific image_’. Although she had an interaction where someone commented positively on the consistency between her online persona and her real life persona, she felt that it was not necessary to work on doing this deliberately.

Some participants experienced having mixed or blurred audiences as an unusual phenomenon that presented interesting dynamics and opportunities for communicating across audiences from different contexts:

> it’s interesting because I have so many different groups that are here that I’m engaged with but it’s all one screen... it’s funny to think sometimes about me posting something for one crowd and the other people seeing it and being confused like I honestly think that’s kind of funny in a way and it’s awkward! (Participant 5).

In _Being aware of audiences_, therefore, participants describe their information experiences in Twitter as enabling new ways and dynamics of communicating and relating with various audiences.

#### Category 2: Making sense of uncertainty

Participants described experiencing information as _Making sense of uncertainty_ about how to use Twitter. Their sense-making is informed by multiple ambiguous interpretations of Twitter’s purposes or intents and what is acceptable in terms of social etiquette, ethical use of social media and solving dilemmas. Participants experience _Making sense of uncertainty_ as being aware of their own and others’ uncertainty and navigating around that uncertainty in an attempt to make sense of it.

Some participants were feeling uncertainty and confusion around how Twitter was intended to be used or whether they were using it correctly. This indicated that compared to Facebook use, the purpose and use of Twitter was not clear, as expressed by Participant 5:

> I think the thing that was weirdest was it was kind of uncertain whether it was meant to be used to connect with personal connections, or… to follow brands and it was sort of meant for all of those things, but I think approaching it... I’m not really sure like who I’m supposed to follow. (Participant 5)

Several participants expressed uncertainty around what was acceptable within rules or common social etiquette practiced in using Twitter. Participant 4 mentioned feeling confusion about social etiquette around tweeting about the unexpected passing of a friend who had prided themselves on not using social media. Uncertainty also extended to content ownership and sharing of tweets on other platforms. Participant 1 perceived ambiguous and problematic situations with Twitter use regarding private ownership of content versus publicly shared tweets. She described an instance where someone posted tweets on BuzzFeed, without necessarily getting permission from the person who made the tweet. In particular, she was concerned about amplification, and tweets getting more visibility than they might on Twitter:

> the author of the piece was kind of like 'Well it’s on Twitter so, like, it’s public'... is it a common space or is it like this actual huge public medium and like where’s the divide between those things... as opposed to my Twitter feed, maybe like 200 people are going to see that but... if this is on a buzz feed post that’s is easily a million heads so... where’s the line between that?... how do we ethically source that information? (Participant 1).

Interestingly, although this participant raised issues of ethics related to sharing or reuse of content, she was ultimately ambivalent about others reusing her own content. However, others were not. For example, Participant 3 demonstrated an awareness of social etiquette through mentioning and acknowledging the original author of a tweet being shared. Conversely, this participant was aware and somewhat tolerant of others’ lack of social etiquette through similar acts of acknowledgements (or the absence thereof).

Another participant expressed ‘complicated’ feelings of ambivalence around how certain tweets portrayed them or how they might be perceived by their audience, admitting to deleting tweets due to feeling uncomfortable in hindsight about how they might be perceived:

> I dropped out the first time because of, like, clinical depression and undiagnosed ADHD and talking about some of those things, like, I’m very aware of the stigmas and so particularly, like, when I’m applying for jobs and know that they might go look at... like I get concerned, you know on the one hand I’m like, 'Well if they wouldn’t hire me because of that I don’t want to work for them anyway'. But at the same time, it’s like, if that’s the reason they wouldn’t hire me... I think some of it is that, and so then I delete them and then I’m like, 'I shouldn’t have deleted that'. Like I’m portraying a false image, like, you know, I want to be awesome! So yeah, it’s hard to balance those things. (Participant 9)

This participant found it challenging to balance being genuine and protecting their reputation in the face of perceived social stigmas and potentially harmful career impacts.

While Participant 9 retrospectively deleted tweets (and felt some remorse about doing so), Participant 4 also talked about dilemmas related to what they should and should not tweet, and concerns about the future impact of what they were tweeting, evidencing some concern with navigating these dilemmas. Some participants were conflicted about blurring of their personal and private lives in their Twitter context. They experienced dilemmas related to appropriateness of content for the various audiences they had, and whether those audiences would be interested in specific types of content, particularly personal content: ‘_[it’s] a source of some anxiety... it is this blurring of parts of your life together and there’s good things but there’s also confusing things around it_’ (Participant 9).

Participants’ experience information as _Making sense of uncertainty_ about social etiquette, ethics and dilemmas, which arise within and beyond the Twittersphere. This ambiguous experience leaves Twitter’s purposes open to people’s varied interpretations. Their interpretations were informed by their own understanding of etiquette, ethics and dilemmas to canvass how they use Twitter. These interpretations may be contrasted with how Twitter intends the platform to be used.

It is important to note that the experience of engaging with information to build and evolve one’s personal interpretation of Twitter’s purposes is pivotal to informing each of the other experiences, as described in category _10: Self-regulating_. How participants reflect on and practice their awareness of audiences and how they make sense of their uncertainty about etiquette, ethics and dilemmas shapes their active experience of information within and beyond the Twittersphere.

### Theme 2: Individual and communal interaction with information

#### Category 3: Being part of a community

Participants described _being part of a community_ as being able to receive and reciprocate information such as ideas and emotions, and to consult a trusted community of followers for advice. Being part of a community on Twitter manifests in a number of ways. It involves sharing quotations about topics of mutual interest: ‘_I’ve a bunch of friends... I guess there’s sort of a community of us who are interested in similar kinds of things we share, quotes..._’ (Participant 3). It also involves reciprocity or ‘_give and take_’ (Participant 1).

Importantly, being part of a community means active engagement with others:

> people who do similar work to me... I might be able to just favourite their tweet or... ask them a question about an article they just posted and you often get feedback from them... it makes a sense of community feel more real in a lot of different aspects of my life. (Participant 6)

Participant 3 experienced Twitter as a ‘_party with ongoing conversations_’:

> Twitter is this kind of rolling cocktail party of people who share a whole bunch of interests... it feels like a conversation that is sort of, ongoing, that I can just pop in and ask these kinds of questions and there’s people who would be delighted to help, and similarly like people ask about things that I might know about and I’m happy to help them back. (Participant 3)

Similarly, some participants mentioned that being able to frequently ask questions of a community of people with mutual interests and being able to reciprocate was a valuable aspect of using Twitter. They also mentioned the benefit of having a trusted community to turn to for specific expertise to aid with decision-making and recommendations, particularly around personal issues, such as finding lingerie shops or getting travel recommendations.

Being part of a community on Twitter can allow participants to overcome a sense of isolation. Participant 4 identified as someone who experienced a perceived intellectual isolation because of having moved to a new location and could only connect emotionally and genuinely with intellectual friendships she had made through Twitter. She experienced Twitter as a ‘_lifeline_’ which meant that it had become an integral part of not just her professional life as an academic, but also her personal life, thus becoming vital to maintaining her overall emotional wellbeing:

> And it wasn’t about establishing a professional identity... it was very hard to establish real friendship in California so for me, that was my emotional support. The intellectual connection is very connected to my emotional wellbeing... (Participant 4)

Participants experience sharing information through reciprocity and active engagement as part of a community.

#### Category 4: Conversing freely

In this category, participants’ information experience manifests as being able to converse or communicate freely, that is, without emotional, mental, geographical or technological barriers or restrictions. As Participant 6 described with reference to all of these types of barriers, experiencing Twitter has ‘_removed barriers to getting in touch with people that you want to talk to that you might not necessarily feel comfortable contacting... like cold calling or e-mailing_’ .

Some participants discussed using Twitter as a way of communicating or venting emotions such as frustration or anger, or sharing a sense of humour that is not able to be expressed elsewhere, as the following conveys:

> it’s very pressure-cooker-y... [I] like, write my anger and frustration away when I cannot speak or when someone is preventing me from speaking... It makes me feel...my frustration is a little bit less... it’s a lot of things that I should not say or cannot say and really very much want to say, even in the cases where I’m like, 'OK I would never actually say that...'. (Participant 4)

Just as Participant 4 was able to say things on Twitter that they may not say elsewhere, Participant 1 noted they had conversations they were unlikely to have in their physical community and allowed them to participate in ‘larger conversations’. They valued being able to participate in this type of conversation without being restricted by geography:

> it’s not something that feels like it happens in real life a lot... it’s nice to find and feel like that conversation is happening and that I can be a part of it, no matter really where I am, I guess if it’s not happening in my daily life, I can make it so. (Participant 1)

In addition to being exposed to different conversations, the informal aspect of Twitter enables one to strengthen connections to people and conversations in their fields of interest. As mentioned in the previous category _Making sense of uncertainty_, the informal aspect of Twitter may result in a sense of blurring the traditional professional and personal boundaries, where participants experienced mixed feelings or confusion around perceived effects.

In addition to breaking down barriers, the experience of conversing freely represents how Twitter has changed what people think is acceptable to speak up about via freedom of information.

### Theme 3: Twitter as a digital and mental space for engagement or non-engagement with everyday life information

#### Category 5: Observing the world

Participants in this study described experiencing information as observing or watching conversations, trends, incidents, content specialists and lifestyles. Twitter provides opportunities to be informed through active observation such as deliberately following and reading information shared through conversations on Twitter. Participants experience inactive observation through serendipitous liking but not necessarily interacting with tweets, lurking and being immersed or spectating without participating.

Participants learn through following conversations between people knowledgeable on a topic:

> I’ve learned a lot about video game criticism through Twitter... mostly [by] following their conversation with other people on Twitter. (Participant 9)

Participants intentionally cultivate or curate streams of information. Within those streams, they are exposed to ‘_random interesting things_’ as a form of inactive observation.

> Well it is sort of random in a way because you never know what you’re going to find... [but] I guess it’s not really all that random because you know, I intentionally follow certain people. (Participant 2)

Inactive observation can involve lurking: ‘_some of the friends who are there... call themselves “lurkers” as opposed to active participants_’ (Participant 8), or being subconsciously informed about issues of interest through following:

> I can just see their tweets and have a vague idea about how they feel about a particular issue... [it’s] not learning as such, but it’s in the background sort of thing. (Participant 10)

Both active and inactive observations can also be immersive experiences, which was apparent in examples where participants felt that a tragic incident was ‘_more real_’ when watching it playing out live on Twitter, as opposed to a delayed news broadcast:

> ...during the marathon bombing I was in California... I follow a lot of Cambridge people on Twitter and everyone was talking about that and so I was... trying to figure out what was going on and if everyone was ok... it felt like very real. It was very strange to be somewhere totally different and be very involved in this moment in Boston. (Participant 10)

It is also important for some participants to be able to vicariously experience other people’s lifestyles which are different from their own, reflected in their feeds:

> I love following a lot of people that have a more academic lifestyle because I’m a little envious so much of their time is spent kind of, thinking and reading and just like picking things apart, and I almost feel that I can absorb a little of that by keeping up with them on Twitter. (Participant 5)

The immersive information experience also involves a range of emotions around certain conversations or trends happening across the world. This might involve feeling enjoyment as they observe tweets about an event they were unable to attend due to illness (Participant 1), feeling pleased by seeing wider social interest in a topic that is often not discussed in ‘_real life_’ (Participant 1), or valuing discussion on ‘_stuff that matters to me_’ (Participant 1).

Participants also expressed mixed feelings of worry and amusement over trending topics:

> I get really annoyed by the hive mind... say some political thing happens and one particular group... I follow will be all up in arms about it... and then it passes and everything is back to normal again. It’s just kind of funny to watch. (Participant 2).

On the other hand, another participant described being fascinated by hive mind trends:

> what’s popular and watching trends ebb and flow. I like that from a much more of an intellectual perspective... I like just kind of seeing that stuff happen (Participant 6).

Having a bird’s eye view of everything, ‘_just a desire to be able to see everything if I wanted to_’ (Participant 5) is also important. This includes being able to watch conversations around live streams (Participant 1), following conferences via hashtags (Participant 4), or observing conversations on a topic of interest between two people they follow (Participant 10). Some participants observed that a large group of people sharing mutual interests can signal the topic’s importance:

> when several people all do it, like the same thing repeatedly, like that’s a really clear signal like, '_Ok, this is a thing I should be paying attention to_'. (Participant 4).

In _Observing the world_, participants describe their information experience in Twitter as enabling new ways of experiencing streams of information, news and entertainment through active or inactive observation.

#### Category 6: Having instant sources

Participants describe the experience of _Having instant sources_ of information as being exposed to news stories of the moment and live updates on events. Being informed about breaking or trending news in the Twittersphere, often before being broadcast on mainstream media, was a commonly discussed aspect :

> we were watching the news but everything that I was finding, that we’d learned from the news,... I’d seen 20 minutes earlier on Twitter. So that was... so fast-paced. (Participant 1)

Participant 5 used Twitter to get real time updates and warnings about a local terrorist attack:

> the Friday morning when Boston kind of, shut down ... I looked at Twitter to find out what was going on but I think because it was, in that case, my entire feed was full of the information so it wasn’t like an active search... it was immediately available. (Participant 5)

In _Having instant sources_, participants describe their information experience in Twitter as providing instant or pre-released access to inside information around a story.

### Theme 4: Information as life moments

#### Category 7: Being humorous

Participants described their information experience as _Being humorous_ by crafting, posting and sharing information as humorous, silly or playful tweets, partially in the form of memes or life moments. Several participants in this study experienced Twitter primarily as a space for creating, sharing and exploring many different types of humour or infotainment: ‘_I’m mostly just curious about people’s senses of humour... So it’s a way to be entertained_’ (Participant 7).

Some participants described the idea of infotainment as a combination of useful or substantial content alongside silliness or absurdity. This relates to both content and the way it is described with hashtags. Speaking about tweeting from an event, Participant 4 described the mix of useful and silly content:

> Like these are some very interesting ideas and then like a bunch of silly things happened and so, like I posted weird little snippets from the discussion because they were amusing. (Participant 4).

Similarly, Participant 1 described using both useful and silly hashtags:

> I do a lot of, like, making hashtags I know are ridiculous that no-one uses, like... #overcaffeinated... So, like, I try to combine ones that are funny to me but are also... like hashtags that are real and useable and like... So the #overcaffeinated... That was just me being silly. (Participant 1)

Some participants also experienced Twitter as a space to share absurd or weird moments in the form of humorous memes:

> So, I was walking home from the... park and here is a lone basketball... like three-quarters of a basketball, bright pink, covered in Internet 'lol speak' and it was so weird! And it made me laugh. So, I took a photo of it... because half my Twitter feed is sharing a love for the absurd and... well I remember being a child and, like, playing around and, like, balls flying around everywhere and there’s something very funny to me about the lingo of the Internet literally, physically harming you! (Participant 3)

Participant 11 suggested that being ‘_good at Twitter_’ meant posting frequent comedic one-liners mixed in with newsworthy posts:

> a lot of it has to do with comedy and brevity, snarky-ness, they post frequently so the more I use Twitter as, like, an everyday occurrence, there are often people who are in news organisations as well so there’s like something about newsworthiness and meta commentary that’s going on with these people who are particularly good at Twitter, but then there’s also, like, in jokes happening about like Twitter-specific things. (Participant 11)

In _being humorous_, participants discuss Twitter as a space for creating and sharing infotainment, memes and personal comedy as a way of collectively exploring and experiencing random moments in their lives.

#### Category 8: Documenting life moments

Participants described experiencing information as documenting life moments of personal and emotional significance by processing, celebrating and reminiscing over particular moments in their lives. They are actively documenting, curating, capturing and sharing moments in context.

Some participants share milestones with their followers as a way of acknowledging and processing experiences as they were being lived through. Participant 9 described how it is important to them to tweet not just about a milestone like graduating, but also about the experience that led up to the milestone, the hard work done over an extended period:

> One saying, 'Today I graduated, yay!' Next one...'But milestone in name only. The hard work of being me is work I’ve been doing in the last year. I’m proud of that'. (Participant 9)

For this participant, tweeting about the experience was a way of processing it, as well as celebrating the milestone. This participant also talked about expectations around engaging with others’ celebration tweets, saying ‘_somebody says like_ 'I graduated!' _Like you at least favourite that!_’

The experience of memory keeping or documenting random memories, moods and emotions is enacted through capturing events and moments in the present time, for future posterity, as illustrated by the following quotation:

> the G&T in bed tweet. Yeah, I was having a day! (laughs) I was eating a buffalo chicken pizza and eating... drinking a gin and tonic in bed because my roommate had just made fun of me for doing so! So, I was like, 'I wanna eat a pizza'. And she was like, 'You’re not dating, feel free to eat a pizza in bed'. (laughs) Yes! That’s exactly what I needed. (Participant 1)

Some participants experience Twitter as an open diary, not expecting anyone to read it but as more of an individual, personal experience of documenting, recording others’ tweets, tracking moods and life moments and combating fears of forgetting important life experiences. Participant 5 retweets other people’s content as a way of capturing it to revisit later: ‘_I mean essentially I see it as something for me like, especially when I’m tweeting other people’s content, it’s almost more a backlog that I can revisit_’.

Participants reported using Twitter to diarise their lives. Participant 5 described a practice of ‘_tweeting moments from my day to day life… [which is] almost a way of revisiting… it’s like, a weird little diary almost_’.

Twitter has become that kind of diary space for Participant 4, who described how they sometimes scrolled back through their feed to review their tweets. They described the role of Twitter in memory-keeping:

And it is really interesting cause sometimes I do end up scrolling back and just sort of reading stuff and just sort of seeing, '_Oh, that’s how I felt in the moment there_'. (Participant 4)

Participant 4 also described experiencing Twitter as a mood tracker. Although they do not consciously track their moods, they find their Twitter feed nevertheless reflects their mood over time:

> there was that weird moment where I realise I totally mood track, I just don’t think about it as mood tracking. … if you were to grab a number of tweets per day, you’d start to see some patterns, right?

Since Participant 5 uses Twitter to diarise their life, they do not try to cater to other people who may be following them, which leads them to question why people do follow them: ‘_sometimes I’ll step back and be like, “' wonder like, why people follow me on this thing?' Like sometimes it doesn’t make sense to me_’ (Participant 5).

In documenting life moments, participants describe curating information born digital in Twitter to preserve memories.

### Theme 5: Depending on and self-regulating constant streams of information

#### Category 9: Being dependent

This category describes participants’ experiences of feeling addicted to, emotionally or psychologically dependent on, or unable to live or work without, Twitter. Twitter provides a constant stream of information that participants draw from and contribute to as part of their everyday life. This stream is so important that they may feel cut off when they do not have access to draw from or contribute to it.

Participant 1 described their ‘_addiction_’ as deriving from being drawn into interesting conversations with people that they would not or could not have anywhere else. In this instance, information experience is addictive and ‘_spiralled_’ from the act of conversing,

> cause I was able to… find interesting information that… I wouldn’t have… come across in like any other social feeds and then I was also able to, like, have interesting conversations with people… And like it was kind of like a better back and forth and from then on it just kind of spiralled and now I’m absolutely addicted to it. (Participant 1)

Similarly, Participant 10, in response to a question about least beneficial aspects of using Twitter, implied that despite the perceived benefits of Twitter in being exposed to information that expanded their world views, the considerable amounts of time and energy spent, and the resulting ‘addiction’ to it as a ‘default’ way of interacting within their world, may be a growing problem:

> it’s addictive! ... like, I spend a lot of time on it … It’s sort of, like, my default, like, “Oh I need something to do right now.” (Participant 10)

Similarly, Participant 1 expressed feeling dependent on Twitter as a constant source of information, news and stimulation. They suggested that being unable to use it anymore would likely result in feelings of boredom and sadness. Participant 4 told a story about the time when they were Twitter banned (prevented by Twitter from using their account), and the awful feeling of ‘ceasing to exist’ that resulted from it, which seemed to cause serious emotional distress and panic. This participant had been banned due to exceeding the maximum tweet rate while live tweeting at a conference. This description suggests that tweeting is part of their voice and that through frequent use it had become an essential part of them and their whole identity and self:

> Like bad enough the feeling of being completely silenced, of being like I have just ceased to exist… like really it felt like an entire part of me that I’ve been cut off from … It was like… being stricken with sudden laryngitis, you can’t even speak to say that you can’t speak. It was the worst thing in the world to the point … I’m like, panicking… . (Participant 4)

While these participants expressed quite extreme emotions around their Twitter voice and identity being tied to their core selves, others held somewhat more moderate views by understanding Twitter as a separate dimension. The loss of Twitter would still result in a feeling of something being missing or may have a detrimental impact on their professional lives. However, in these cases the Twitter dimension was viewed as separate from their whole selves. Participant 2 said that losing Twitter,

> wouldn’t kill me, but I think I would feel like there was one channel that was sort of missing from me. And I say ‘channel’ because it is kind of like… it’s very varied information that I get and I don’t know, I feel like almost a dimension would be taken away from me.

Similarly, Participant 9 felt that while it would not be ‘_incredibly difficult_’ to give Twitter up, they would be concerned about the impact on their career of losing the connections they have on Twitter.

In _being dependent_, participants describe the addictive and habitual experience of Twitter, which appears to be both caused and fuelled by the nature of Twitter and its continuous and in some cases, essential streams of information.

#### Category 10: Self-regulating

Participants self-regulate their use of Twitter and their information experiences in that space to foster positive interactions and experiences. Some participants in this study are actively self-regulative and experienced this as limiting or avoiding different aspects of their (and others’) Twitter use or behaviour. These usage patterns relate mainly to timing and information quality and quantity, which if not effectively managed may lead to negative consequences in their professional and personal lives.

Some participants limit their use of Twitter to specific times of the day. For example, Participant 1 self-regulated the types of interactions she had during work hours:

> I can tweet at work, no-one’s going to be like 'Stop that!' But I try not to get into, like, deep conversations because they get me in trouble! (Participant 1)

Another participant described learning how to self-regulate tweeting behaviour over time, leading to a personal decision to block the Twitter site in order to keep focused on work:

> In order to stay focused on doing work today I actually... blocked my ability to visit the website... This past month, yes. Prior to this past month, no. (laughs) And then I decided I needed to. (Participant 3)

Participants talked about strategies for managing the amount of time they spent on Twitter. These strategies included not reading everything, as described by Participant 8:

> I felt that I had to read everything that everybody put and I was spending up to an hour at a time and I was like, “Wait a minute. This isn’t how it’s supposed to work. You just sort of tune in maybe once or twice a day or however often and you’re there for a little while and you read something. (Participant 8)

Following a manageable number of people, or organizing followed accounts into lists, are strategies participants use to avoid an unproductive tweet overload. Participant 3 described the experience of selecting and culling lists of people they followed based on current interests:

> And then if I decide they’re not interesting or they’re tweeting too much... and then I sort of cull that list back down again... I can only take so many people who tweet a lot otherwise they drown out all of the other people that I might be interested in.... (Participant 3)

Participants used a number of strategies to avoid having negative experiences on Twitter or to actively create positive experiences. Some participants indicated they had learned to censor comments to avoid conflicts online:

> I don’t to get into Twitter fights as much anymore but I used to be more like, ;No you’re wrong!' and [now] I’m like 'Whatever’. (Participant 10)

Others talked about fact checking to avoid miscommunication or sharing misinformation:

> I just wanted to check and make sure it was true, so I Googled around... Yeah, I didn’t want to mis-tweet... it’s, like, really embarrassing! (Participant 4)

Participants’ self-regulation was informed by reflecting on and being mindful of various aspects of Twitter. Being aware of ways to self-regulate involves developing a deeper understanding of the purpose of Twitter over time. The way they use Twitter evolved over time, as Participant 6 noted. Likewise, over time, participants became conscious of the processes involved, in contrast to other forms of social media, resulting in keen awareness (or meta-awareness) of various behaviour and interactions unique to Twitter. This awareness subtly informed the processes of discovering their own styles of use.

In _self-regulating_, participants describe how they balance or control their exposure to and interaction with the constant stream of information from Twitter.

### Theme 6: Empowering self and others through being informed

#### Category 11: Broadening horizons

Participants experience information in this category as broadening opportunities and choices since Twitter presents as space to learn, grow and develop themselves and help others. These experiences include feeling pleasantly surprised by unexpected invitations and opportunities coming through Twitter, noticing changes in behaviour towards personal and professional development, and having opportunities for both individual and shared learning.

Participants in this study described instances where an opportunity came their way through a Twitter interaction or connection, where the opportunities were unlikely to have surfaced without Twitter. For example, Participant 1 experienced being unexpectedly invited to be interviewed for a podcast after one of her followers saw her contribution to a Twitter conversation. Related to this, Participant 5 described stumbling across opportunities (specifically, information about participating this study, as well as volunteering opportunities) through Twitter.

Similarly, Twitter provided participants with opportunities to connect with people they admire, who they never imagined they could ever connect with. Twitter broadens the possibility of who they might connect with.

> [E]very now and then I will actually tweet to someone who I consider you know, more of a celebrity... you know, someone who I hold in high regard in whatever field they’re in. And I don’t really ever expect an answer... but when I do get an answer, it’s, like, “Wow!”… it’s like having this connection with people with whom that you just never imagine having a connection with.... Twitter broadens the possibilities. (Participant 2)

Participant 9 also reported similar experiences, specifically around feeling empowered to contact and ask things of people in their professional field who had followed them on Twitter.

Participant 1 noticed a change in their personality through being informed using Twitter, becoming more outspoken about issues of interest. People in their personal life had commented that they had become more confident and outspoken, and Participant 1 perceived that this was in large part attributable to being more informed about topics of interest through Twitter. Similarly, Participant 9 perceived a change in their behaviour with regard to ‘having worthwhile things to say in the conversation’ due to the nature of Twitter interactions:

> I think [one of its] benefits... is starting to help me feel like I do have valid things to say... And so, like, when people do respond to things, it sort of adds to the, like, “Oh I have something to say” and when they don’t it doesn’t subtract as much. (Participant 9)

Learning new things from trusted and relevant sources of knowledge through tailored Twitter feeds is an experience valued by many participants in this study. Participant 6 also deliberately followed people who added value to their learning, viewing learning as reciprocal or shared between themselves and followers:

> that’s why I think I follow a lot of people that I do, is because they add certain value to help me with my learning. And so, I see that in a reciprocal way of what I’m providing to other people… . (Participant 6)

While some participants deliberately seek learning experiences by tailoring their feeds, others such as Participant 10 foreground the informative side of Twitter, while still being aware that learning is happening.

> I can, like, click through if it’s something I find interesting or I can just see their tweets and have a vague idea about how they feel about a particular issue... Yeah, so not learning as such, but it’s in the background, sort of thing. (Participant 10)

In _broadening horizons_, participants experience Twitter as space for using information for personal and collective empowerment.

#### Category 12: Valuing diverse voices

Participants’ information experience on Twitter involves valuing the diverse voices that exist there. This allows them to be critically informed, challenged by information, and to amplify information. They are exposed to multiple voices, perspectives, debates and conversations on various issues. The experience involves being conscious of and actively countering the ‘echo chamber’ effect to some extent. Participants described the echo chamber effect as resulting from people deliberately funnelling their information because of the nature of Twitter, which fosters tailoring of feeds. Participant 1 found it interesting that ‘_the kind of news that I get via Twitter is really tailored exactly to what I want to hear_’.

Some participants in this study actively made an effort to listen to and consider perspectives through Twitter that were different from their own, to counter the echo chamber effect:

> Yeah, I think it’s important to avoid the, sort of, echo chamber effect. Not to say I don’t end up following a lot of people who are similar to [me] I’m sure. That’s a majority, but once in a while I can look at someone and be like, “Oh, you seem to have a very different take on this thing. I want to see what you’re saying”. (Participant 5)

Countering or avoiding the echo chamber effect is enacted through being curious about different views, being able to add people to a feed who are outside of an individual’s main interests and being exposed to everything including those with different interests and opinions.

For some participants, diversifying information sources is important for democracy. Participant 2 expressed feeling bothered by prevailing unbalanced or one-sided views on Twitter:

> one thing that I try to do too is to not listen to just one point of view so I try to follow people who have differing points of view as well... in keeping perspectives open, you know, seeing different sides of things which I think is very important. (Participant 2)

Some participants collect diverse perspectives and worldviews via Twitter to feed into their own developing perspectives. Participant 6 described a stronger experience than just collecting diverse perspectives, that of,

> challenging your own beliefs and either coming through that believing your convictions even more, and being able to explain them better, or challenging them maybe having a different perspective afterwards.

While some participants actively build feeds that contain alternative perspectives, others limit their feeds to exclude content that is in opposition to their personal beliefs. These participants are often aware of the importance of having wider and more balanced perspectives on issues but have made the decision to limit their feed regardless. In the following example, Participant 9 talked about limiting their feed to exclude perspectives they do not want to see:

> I mean it’s not a full echo chamber but certainly it’s like, 'Alright these are sexist views that I’m not that interested in reading'. Why would I choose to expose myself to that when, you know, in day to day life I often don’t have a choice about whether I’m exposed to those. (Participant 9)

When being exposed to different views on Twitter, criticality is incredibly important to participants. Being _critically_ informed and having the capacity to evaluate information was noted by participants as an important part of this experience. This involved perceiving oneself as an editor of credible news and information or being able to detect fake news stories:

> Twitter has all stories substantiated and unsubstantiated! (laughs) So I trust myself as an editor in that sense. I think I have a pretty good eye towards when something is... like, let’s say it’s maybe more of a scale than a dichotomy of if it’s true or false. I think that for me,... I’m pretty aware of when something looks fake off the bat. (Participant 6)

Another part of being critically informed is being able to gain more critical perspectives on complex social issues:

> I think it allows me to see a lot more perspectives which is really useful to me. Like that’s a lot of what I find really valuable about Twitter. Like, it’s more critical sort of perspectives. (Participant 10)

As they are exposed to diverse content, participants amplify interesting people’s content and engage with others by retweeting to amplify their content:

> it’s also a way of engaging with the person I think by being, like, 'Hey, I like your content enough to, like, kind of amplify it'. So, I do think about that sometimes if it’s someone I care about kind of, keeping a rapport with, and thought was, like, a cool person. (Participant 5)

Amplifying also includes learning from and empowering or advocating marginalised voices:

> I have found that it’s really a great space to give people who, like, marginalised people who don’t necessarily have a platform to speak to issues that matter to them and like that is really where I’ve done a lot of my learning from that which is cool. (Participant 1)

In valuing diverse voices, participants describe enacting democratic principles while countering echo chambers and misinformation, themes of particular importance in the current _post-truth_ world.

## Implications towards understanding Twitter and social media through an information experience lens

The findings of this study increase our understanding of Twitter, as a social media context and phenomenon, and also show how the integration of Twitter into everyday life is altering the ways in which people interact with and experience information. As exploratory research using constructivist grounded theory emphasizing individual and personal meaning-making, this study reveals the complex and nuanced intricacies of everyday life use of Twitter as a unique digital and mental space for one’s everyday life information world. The study illustrates some of the emerging and conflicting values, emotions, dynamics and cultures that reflect both communal and individual experiences in which people interact with the broad variety of information within and beyond Twitter content, elements and functions. As the sample was limited to one location, findings cannot be generalized and need to be compared with other contexts. However, an in-depth understanding of the phenomenon was gained.

This study points to two main ways Twitter is altering the nature of how people connect, communicate and use information. The first is through informing social change. Information experiences such as conversing freely, observing the world, being dependent and self-regulating, broadening horizons and valuing diverse voices, reflect experiences which are evidently driving and informing new and unprecedented social change across local and global contexts. These information experiences are unique to Twitter, in that they can only be experienced via the digital realm of Twitter. These information experiences, closely linked to the themes identified in this study such as individual and communal interaction with information, Twitter as digital and mental space for everyday life information, depending on and self-regulating constant streams of information and empowering self and others through being informed, are important areas for further research into how Twitter (and other forms of social media) can inform social change.

Recent sociological studies into some of these areas are emerging, for example, the role of emotion in tweeting and social media conversations ([Brownlie and Shaw, 2019](#bro19)), public media audiences’ networks of influence ([Willis, Fisher and Lvov, 2015](#wil15)), higher levels of social media addiction and depression among Twitter users ([Jeri-Yabar, _et al._, 2019](#jer19)), combating and challenging the existence of echo chambers through media diversity ([Dubois and Blank, 2018](#dub18)) and misinformation from Twitter discussions during political campaigns ([Kusen and Strembeck, 2018](#kus18)). There is as yet no research that investigates how people’s holistic experiences on digital social media platforms including Twitter can inform, and be informed by, individual and communal social change.

The second way Twitter alters the nature of communication and information use is through experiencing traditional sources of information in new ways. Information experiences such as building audiences and interpretation of purposes, being part of a community, instant sources, being humorous and documenting life moments suggest new ways of experiencing information that people have always interacted with in more traditional ways (such as print, journals, television, online discussion boards or face-to-face). It should be noted that two themes in this study have broader overlapping significance in both informing social change and experiencing traditional information sources in new ways. These themes are individual and communal interaction with information and Twitter as a digital and mental space for everyday life information.

Findings from several studies into changing practices and emerging literacies on Twitter include new literacy practices of teenage Twitter users ([Gleason, 2016](#gle16)). Gleason’s study indicates that this group had developed a hybrid literacy of traditional practices, such as attention to audience, conventional spelling and grammar, and emerging literacy practices, such as multimodal composition and situation-specific communication which violates conventions to resonate with audiences. Chen ([2015](#che15)) explored young consumers’ interpretation of marketing information on Twitter as ‘_trendy_’ and ‘_entertaining_’. Ethical considerations regarding Twitter use have also been explored, for example, insights gathered from deleted tweets and the ethics of analysing deleted tweets ([Meeks, 2018](#mee18)). However, many of these studies into experiencing information in new and altered ways do not yet investigate in-depth citizens’ experiences (i.e. ethical, critical and interpretative practices) with information in everyday life scenarios. This grounded theory study has gone beyond superficial interpretations of Twitter found in other studies, towards more meaningful, mental health-conscious interactions and moments experienced through Twitter.

## Conclusion

As one of only a small number of studies focused on information experience as an object of research, this study contributes to our emerging understanding of how people experience information as part of their everyday lives. It builds on and extends the work of Davis ([2015](#dav15)), Bunce, Partridge and Davis ([2012](#bun12)), Haidn, Partridge and Yates ([2014](#hai14)), and Yates and Partridge ([2015](#yat15)) to extend our conceptual understanding of information experience as research object. It also contributes to the establishment of information experience as a research domain in which researchers take an experiential lens to explore human information seeking and use. It is suggested that the conceptual model presented in this paper serves as a basis towards informing social media experience research and design for better digital, personal and educational outcomes, specifically in Twitter, as well as other popular social media platforms sometimes informing in tandem, such as Facebook, Instagram and Reddit.

To develop a strong conceptual understanding of information experience in social media, further research is needed which focuses on information experience in the context of social media, both within and across platforms, involving different cohorts and using a range of methodologies. This study has opened several avenues for future information experience and related social research, particularly in understanding how Twitter alters people’s experiences of traditional sources of communication and information and how it informs aspects of social change that are occurring within digital and mental spaces.

## About the authors

**Faye Miller** is a Research and Teaching Fellow in the Faculty of Arts and Design at the University of Canberra, Australia and Founder of Human Constellation, a digital research consulting company. Her doctoral research completed in 2014, explored how early career researchers experience information for learning as a knowledge ecosystem. She can be contacted at [Faye.Miller@Canberra.edu.au](mailto:Faye.Miller@Canberra.edu.au)  
**Helen Partridge** is Professor and Pro Vice-Chancellor (Education) at the University of Southern Queensland Australia. Her research interests explore the interplay between information, learning and technology. She has been a visiting research fellow at the Oxford Internet Institute, University of Oxford, and the Berkmein Klein Centre for Internet and Society, Harvard University. She can be contacted at [helen.partridge@usq.edu.au](mailto:helen.partridge@usq.edu.au).  
**Kate Davis** is a Senior Research Fellow at the University of Southern Queensland, Australia. She is a social scientist who researches information experience, particularly in the context of social media, using qualitative approaches designed to get to the heart of people’s experience. Her doctoral study exploring the information experience of new mothers in social media produced the first theoretical rendering of information experience as an object of study. She can be contacted at [Kate.Davis@usq.edu.au](mailto:Kate.Davis@usq.edu.au)

</section>

<section class="refs">

## References

*   Adams, C. & van Manen, M. (2008). Phenomenology. In L. Given (Ed.), _The Sage encyclopaedia of qualitative research methods_ (pp. 615–620). Los Angeles, CA: Sage Publications.
*   Barker-Plummer, B. & Barker-Plummer, D. (2017). Twitter as a feminist resource: #YesAllWomen, digital platforms, and discursive social change. In J. Earl and D.A. Rohlinger (Eds.), _Social movements and media_. (pp. 91-118). Bingley, UK: Emerald Publishing Limited. (Studies in media and communications, Volume 14).
*   Berry, N., Lobban, F., Belousov, M., Emsley, R., Nanadic, G. & Bucci, S. (2017). [#WhyWeTweetMH: understanding why people use Twitter to discuss mental health problems](http://www.webcitation.org/77oV1iCi3). _Journal of Medical Internet Research, 19_(4). Retrieved from https://doi.org/10.2196/jmir.6173 (Archived by WebCite® at http://www.webcitation.org/77oV1iCi3)
*   Bogers, T. & Bjorneborn, L. (2013). [Micro-serendipity: meaningful coincidences in everyday life shared on Twitter](http://www.webcitation.org/77oWHnBHd). In _Proceedings of the iConference 2013_ (pp. 196-208). Urbana-Champagne, IL: University of Illinois. Retrieved from http://hdl.handle.net/2142/36052 (Archived by WebCite® at http://www.webcitation.org/77oWHnBHd)
*   Brownlie, J. & Shaw, F. (2019). Empathy rituals: small conversations about emotional distress on Twitter. _Sociology, 53_(1), 104–122\.
*   Bruce, C., Davis, K., Hughes, H., Partridge, H. & Stoodley, I. (Eds.). (2014). _Information experience: approaches to theory and practice_. Bingley, UK: Emerald Group Publishing Limited. (Library and Information Science, Volume 9)
*   Bruns, A. (2018). _Gatewatching and news curation: journalism, social media, and the public sphere_. New York, NY: Peter Lang.
*   Bunce, S., Partridge, H. & Davis, K. (2012). Exploring information experience using social media during the 2011 Queensland Floods: a pilot study. _Australian Library Journal, 6_(1), 34–45.
*   Charmaz, K. (2006). _Constructing grounded theory: a practical guide through qualitative analysis_. London: Sage Publications.
*   Chatfield, A. T. & Brajawidagda, U. (2012). Twitter tsunami early warning network: a social network analysis of Twitter information flows. In J. W. Lamp (Ed.), _ACIS-2012: Location, location, location : proceedings of the 23rd Australasian Conference on Information Systems 2012_ (pp. 1–10). Geelong, Australia: Deakin University.
*   Chen, H. (2015). College-aged young consumers’ interpretation of Twitter and marketing information on Twitter. _Young Consumers, 16_(2), 208–221.
*   Davis, K. E. (2015). _The information experience of new mothers in social media: a grounded theory study._ (Unpublished doctoral dissertation). Queensland University of Technology, Queensland, Australia.
*   Dubois, E. & Blank, G. (2018). The echo chamber is overstated: the moderating effect of political interest and diverse media. _Information, Communication & Society, 21_(5), 729–745.
*   Gleason, B. (2016). New literacies practices of teenage Twitter users. _Learning, Media and Technology, 41_(1), 31-54.
*   Gunton, L. & Davis, K. (2012). Beyond broadcasting: customer service, community and information experience in the Twittersphere. _Reference Services Review, 40_(2), 224-227.
*   Haidn, I., Partridge, H. & Yates, C. (2014) Informed democracy: information experiences during the 2012 Queensland election. In J. T. Du, Q. Zhu and A. Koronios (Eds.), _Library and information science research in Asia-Oceania: theory and practice_ (pp. 8-23) Harrisburg, PA: IGI Publishing.
*   Halberstam, Y. & Knight, B. (2016). Homophily, group size, and the diffusion of political information in social networks: evidence from Twitter. _Journal of Public Economics, 143_(1), 73-88.
*   Hughes, H. (2014). Researching information experience: methodological snapshots. In C. Bruce, K. Davis, H. Hughes, H. Partridge, and I. Stoodley (Eds.), _Information experience: approaches to theory and practice_ (pp33-50). Bingley, UK: Emerald Group Publishing Limited.(Library and Information Science, Volume 9).
*   Jeri-Yabar, A., Sanchez-Carbonel, A., Tito, K., Ramirez-delCastillo, J., Torres-Alcantara, A., Denegri, D. & Carreazo, Y. (2019). Association between social media use (Twitter, Instagram, Facebook) and depressive symptoms: are Twitter users at higher risk? _International Journal of Social Psychiatry, 65_(1), 14–19.
*   Jeske, D., McNeill, A. R., Coventry, L. & Briggs, P. (2017). [Security information sharing via Twitter: 'heartbleed' as a case study.](http://www.webcitation.org/77oWSLPqx) _International Journal of Web Based Communities, 13_(2). Retrieved from http://nrl.northumbria.ac.uk/30856/ (Archived by WebCite® at http://www.webcitation.org/77oWSLPqx)
*   Khan, H. R., Onye, U. U. and Du, Y. (2018). Data‐activism as civic engagement: forming a narrative to foster social change on gun control in the United States. In L. Freund (Ed.), _Proceedings of the Association for Information Science and Technology_ (pp. 841–842.) Hoboken, NJ: Wiley.
*   Kusen, E. & Strembeck, M. (2018). Politics, sentiments, and misinformation: an analysis of the Twitter discussion on the 2016 Austrian Presidential Elections. _Online Social Networks and Media, 5_(2), 37-50.
*   Linek, S., Teka Hadgu, A., Hoffmann, C. P., Jäschke, R. & Puschmann, C. (2017). It’s all about information? The following behaviour of professors and PhD students in computer science on Twitter. _Journal of Web Science, 3_(1), 1-15.
*   Meeks, L. (2018). Tweeted, deleted: theoretical, methodological, and ethical considerations for examining politicians’ deleted tweets. _Information, Communication & Society, 21_(1), 1-13.
*   Mulatiningsih, B, Partridge, H. & Davis, K. (2013). Exploring the role of Twitter in the professional practice of LIS professionals: a pilot study. _Australian Library Journal, 62_(3), 204-217.
*   Murthy, D. (2018). _Twitter_. 2nd ed. Boston, MA: Polity Press. (Digital Media and Society Series)
*   Neiger, B.L., Thackeray, R., Burton, S.H., Thackeray, C.R. & Reese, J.H. (2013). [Use of Twitter among local health departments: an analysis of information sharing, engagement, and action](http://www.webcitation.org/77oVPMVix). _Journal of Medical Internet Research, 15_(8). http://doi.org/10.2196/jmir.2775 (Archived by WebCite® at http://www.webcitation.org/77oVPMVix)
*   Pang, N. & Ng, J. (2016). Twittering the Little India Riot: audience responses, information behavior and the use of emotive cues. _Computers in Human Behavior, 54_, 607-619\.
*   Scheibe, K., Fietkiewicz, K.J. & Stock, W.G. (2016). Information behavior on social live streaming services. _Journal of Information Science Theory and Practice, 4_(2), 6–20.
*   Schwandt, T. (2007). Lifeworld. In T. Schwandt (Ed.), _The Sage dictionary of qualitative inquiry_ 3rd ed. (pp. 179–180). Thousand Oaks, CA: Sage Publications.
*   Shields-Dobson, A., Robards, B. & Carah, N. (2018). _Digital intimate publics and social media_. Cham, Switzerland: Palgrave Macmillan.
*   Shulman, J., Yep, J. & Tome, D. (2015). Leveraging the power of a Twitter network for library promotion. _The Journal of Academic Librarianship, 41_(2), 178-185.
*   Stieglitz, S. & Dang-Xuan, L. (2013). Emotions and information diffusion in social media: sentiment of microblogs and sharing behaviour. _Journal of Management Information Systems, 29_(4), 217-248\.
*   Tafti A., Zotti R. & Jank, W. (2016). [Real-time diffusion of information on Twitter and the financial markets.](http://www.webcitation.org/77oW4uDFA) _PLoS ONE, 11_(8). https://doi.org/10.1371/journal.pone.0159226 (Archived by WebCite® at http://www.webcitation.org/77oW4uDFA)
*   Talip, B. (2016). Digital ethnography as a way to explore information grounds on Twitter. _Qualitative and Quantitative Methods in Libraries, 5_(1), 89-105.
*   Tonkin, E., Pfeiffer, H. D. & Tourte, G. (2012). Twitter, information sharing and the London riots? _Bulletin of the Association for Information Science and Technology, 38_(2), 49-57.
*   Veletsianos, G. & Kimmons, R. (2016). Scholars in an increasingly digital and open world: how do education professors and students use twitter? The Internet and Higher Education, 30, 1-10.
*   Veletsianos, G., Houlden, S., Hodson, J. & Gosse, C. (2018). Women scholars’ experiences with online harassment and abuse: self-protection, resistance, acceptance, and self-blame. _New Media & Society, 20_(12), 4689-4708.
*   Willis, A., Fisher, A. & Lvov, I. (2015). Mapping networks of influence: tracking Twitter conversations through time and space. _Participations: Journal of Audience & Reception Studies, 12_(1), 494–530.
*   Yates, C. & Partridge, H. (2015). [Citizens and social media in times of natural disaster: exploring information experience](http://www.webcitation.org/6WySni6cw). _Information Research, 20_(1), paper 659\. Retrieved from http://InformationR.net/ir/20-1/paper659.html (Archived by WebCite® at http://www.webcitation.org/6WySni6cw)

</section>