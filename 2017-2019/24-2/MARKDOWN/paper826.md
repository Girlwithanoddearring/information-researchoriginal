### vol. 24 no. 2, June, 2019

# The consequences of online information overload confusion in tourism

## [Wee-Kheng Tan](#author) and [Ping-Chen Kuo](#author)

> **Introduction**. This study considers the often neglected impact of confusion arising from information overload in the tourism context.  
> **Method**. A questionnaire was used to gather data from visitors who had visited Hualien, a well-known Taiwanese tourist destination, and had used online websites to look for information regarding Hualien. The survey respondents were classified based on their number of visits to Hualien: first-timers (have made one visit) and repeaters (have visited more than once).  
> **Analysis**. Hypotheses were tested using partial least squares (PLS). The PLS Multi-group Analysis was then applied to compare each of the path coefficients of the two groups of visitors (first-timers and repeaters) to identify the paths that significantly differ across these two groups of visitors.  
> **Results**. Overload confusion adds to the undesirable outcome of regret and undermines the desirable outcome of the escape experience.  
> **Conclusion**. This study provides the empirical link between overload confusion and two tourism-related concepts that are not adequately explored: destination image and escape experience. The paper also adds to the literature of information science by providing evidence of the applicability of the concept of information overload to the tourism context and its negative effect, also prevalent in some commonly discussed tourism-related concepts.



## Introduction

The dramatic proliferation of destination-related information on the Internet has had a deep impact on tourists’ pre-trip behaviour. The large volume and easy accessibility of information allows tourists to plan an itinerary before the trip. However, the vast information also causes difficulties by making tourists feel overwhelmed and confused.

Confusion arising from information overload ([Shang, Chen and Chen, 2013](#sha13)) during information search is relevant in the tourism context. Since tourism products are often experience goods with many crucial decisions being made before the trip ([Kang, Cheon and Shin, 2011](#kan11)), trip planning is often filled with uncertainty ([Jeng and Fesenmaier, 2002](#jen02)). Hence, tourists often conduct an intensive information search in the overly information-rich online environment before a trip. However, previous research has shown that the '_confusion issue might even be more salient in the online tourism context. This issue, however, has not received much research attention in the tourism literature_' ([Lu, Gursoy and Lu, 2016](#lu16), p. 77).

Although this area has started to receive some attention from researchers ([Lu, Gursoy and Lu, 2016](#lu16)), a deeper understanding of overload confusion in the tourism context is still needed. A cognitive psychological phenomenon ([Sproles and Kendall, 1986](#spr86)), overload confusion is '_a lack of understanding caused by the consumer being confronted with an overly information rich environment that cannot be processed in the time available to fully understand, and be confident in, the purchase environment_' ([Mitchell, Walsh and Yamin, 2005](#mit05), p. 143). From the information science perspective, it can be viewed as one of the negative effects of information overload. The presence of overload confusion could start a chain of negative events. Overload confusion is known to affect aspects of tourists’ behaviour, such as regret and satisfaction ([Desmeules, 2002](#des02); [Matzler, Stieger and Füller, 2011](#mat11); [Schwartz, _et al._, 2002](#sch02); [Walsh and Mitchell, 2010](#wal10)).

Regret, a negative, cognitively-based emotion ([Zeelenberg, 1999](#zee99)), can negatively influence post-decision attitudes ([Tseng, 2017](#tse17)), such as by undermining satisfaction ([Bui, Krishen and Bates, 2011](#bui11)). Regret also has a potential effect on tourists’ experience and is therefore important, since tourists’ decisions to purchase tourism products often hinges '_on a mere promise, a notion, and a socially constructed image of what constitutes an interesting or appealing experience_' ([Curtin 2005](#cur05), p. 2). Even though holidays are frequently considered '_occasions of escape and liminal leisure_' ([Voase, 2018](#voa18), p. 384), the way overload confusion felt by tourists affects their escape experience has often been ignored. Furthermore, destination image is widely examined in tourism research ([King, Chen and Funk, 2015](#kin15)). However, the way overload confusion affects tourists’ perception of the destination remains uncertain.

A tourist destination often depends on domestic visitors for survival. Studies have often shown that domestic tourists are more likely than foreign tourists to make repeat visits, and tourists who repeatedly visit a destination can be engaged at a lower cost than new visitors, which is useful to destination marketing organisations ([Zhang, Fu, Cai and Lu, 2014](#zha14)). However, these return tourists behave differently from first-time tourists in various aspects, such as the perceived level of confusion ([Mitchell, _et al._, 2005](#mit05)), use of information sources ([Baloglu, 2001](#bal01)), experience felt ([Yuksel, 2001](#yuk01)), destination image ([Fakeye and Crompton, 1991](#fak91)), and satisfaction ([Lim, Kim and Lee, 2016](#lim16)).

Given that the nature of the links between tourists’ confusion arising from information overload; their regret over choosing some attractions over others whilst touring the destination; escape experience arising from the trip; destination image and trip satisfaction; and how the tourists’ number of visits moderates these relationships is yet to be fully understood, this study suggests the following research question and research model (Figure 1).

_**Research question**_: In the tourism context, how does overload confusion arising from information overload affect destination visitors’ escape experience, regret over choosing the destination, destination image and, ultimately, overall trip satisfaction? In addition, how does the impact of overload confusion differ among first-time and repeat visitors?

<figure class="centre">![Figure 1: Research model](../figs/p826fig1.png)

<figcaption>Figure 1: Research model</figcaption>

</figure>

Thus, this paper attempts to contribute to the understudied aspect of examining the outcomes of overload confusion arising from information overload in the tourism context. This study examines the relationship between information overload and some commonly discussed tourism related concepts. The study also adds to the literature of information science by providing evidence of the applicability of the concept of information overload beyond areas such as health information ([Swar, Hameed and Reychav, 2017](#swa17)) and organisation related activities ([Eppler and Mengis, 2004](#epp04)) to tourism. The study also has practical implications for destination marketing organisations, as well as entities and individuals involved in authoring, providing, and/or using information about destinations. For example, since information overload arises from excessive information provision, and providing and monitoring destination information is an important function of the destination marketing organisations, these organisations must be concerned with the implications arising from overload confusion. Many bloggers are also in the business of writing about tourist destinations. They will also be interested to learn how information overload and the accompanying overload confusion affects their readers and, hence, the receptiveness of the readers to the blog articles.

The research question and model are examined through the residents of Taiwan (i.e., domestic visitors) visiting [Hualien](http://www.webcitation.org/query?url=https%3A%2F%2Fwww.islandlifetaiwan.com%2Fthe-best-of-hualien%2F&date=2019-05-20), a favourite nature and rural tourist destination in picturesque eastern Taiwan. Hualien offers a wide range of attractions, such as the Taroko Gorge National Park, East Rift Valley, river rafting, hot springs, whale sighting, and Li-Chuan Aquafarm. Hualien’s distance from major urban centres (which are where most of its visitors come from) and its many attractions provide tourists with the opportunity to escape the hectic and usual urban lifestyle whilst touring Hualien. Given the popularity of Hualien, there is plenty of online destination information. Hence, information overload and its associated confusion are likely.

## Literature review and development of hypotheses

### Information overload

Whilst information overload is not a new concept ([Bawden, Holtham and Courtney, 1999](#baw99)), it has become prominent and is widely researched because of the popularity of the Internet ([Jackson and Farzaneh, 2012](#jac12)). The most well known of information pathologies ([Bawden and Robinson, 2009](#baw09)), information overload commonly conveys the idea of receiving too much information ([Eppler and Mengis, 2004](#epp04)). However, there is no single agreed definition for information overload ([Bawden and Robinson, 2009](#baw09); [Jackson and Farzaneh, 2012](#jac12)). Bawden and Robinson ([2009](#baw09)) have mentioned that information overload occurs '_when information received becomes a hindrance rather than a help when the information is potentially useful_' (p. 249). Eppler ([2015](#epp15)) suggests that information overload '_describes situations in which we receive too much information to sensibly deal with it all in our available time frame_' (p. 217).

Eppler and Mengis ([2004](#epp04)) have suggested five categories that cause information overload, i.e., personal factors, information characteristics, task and process parameters, organisational design, and information technology. The symptoms and negative effects of information overload have also been considered. Swar, _et al._ ([2017](#swa17)) found that perceived information overload negatively influences the psychological well-being of online health information seekers. Limited information search and retrieval strategies, arbitrary information analysis and organisation, suboptimal decisions, and strenuous personal states such as confusion and demotivation are considered symptoms of information overload by Eppler and Mengis. Feelings of stress, ineffective information processing and confusion are also considered outcomes of information overload by Eppler ([2015](#epp15)) and Thorson, Reeves and Schleuder ([1985](#tho85)). Thus, from the information science perspective, confusion is often considered a symptom or result of information overload.

### Overload confusion and its consequences

Consumer confusion is ‘consumer failure to develop a correct interpretation of various facets of a product/service, during the information processing procedure’ ([Turnbull, Leek and Ying, 2000](#tur00), p. 145) and consumers noticing that information processing could be confusing ([Walsh, Hennig-Thurau and Mitchell, 2007](#wal07)). Researchers have identified three forms of confusion: similarity, ambiguity, and overload confusion ([Lu, Gursoy and Lu, 2016](#lu16)).

Tracing back to brand confusion ([Wang and Shukla, 2013](#wan13)), similarity confusion arises from consumers perceiving a similarity of products or services in aspects such as name, colour, size, packaging, and imitators ([Matzler, _et al._, 2011](#mat11); [Wang and Shukla, 2013](#wan13)). Originating from decision sciences ([Wang and Shukla, 2013](#wan13)), information that is complex, unclear, and confusing can contribute to ambiguity confusion ([Lu, _et al._, 2016](#lu16); [Matzler, _et al._, 2011](#mat11)).

Arising from an overly information rich environment where consumers do not have time to process the information ([Mitchell, _et al._, 2005](#mit05); [Moon, Costello and Koo, 2017](#moo17)), overload confusion is thus a negative outcome of information overload when viewed from the information science perspective. Information overload ([Feng and Agosto, 2017](#fen17)) and its associated reaction of confusion have been examined in many contexts, such as food labelling ([Kangun and Polonsky, 1995](#kan95)), telecommunications ([Turnbull, _et al._, 2000](#tur00)), and online tourism ([Lu, _et al._, 2016](#lu16)). Overload confusion also exists on the Internet ([Lu, _et al._, 2016](#lu16)) and is a consequence of the breakneck growth of websites and consumers being overwhelmed by the vast quantity of information. Although consumer confusion ‘_has not received much research attention in the tourism literature_’ ([Lu, _et al._, 2016](#lu16), p. 77), it is prevalent in the online tourism context. For example, Lu, _et al._ investigated the antecedents and outcomes of consumer confusion in the online tourism context and found that price consciousness and the need for cognition negatively affected overload confusion. This overload confusion resulted in consumers obtaining more information and using familiar information sources.

Nevertheless, more studies are needed in the tourism context, since confusion can have a detrimental effect on many aspects of tourists’ behaviour, such as satisfaction, regret, word-of-mouth, and decision postponement ([Desmeules, 2002](#des02); [Matzler, _et al._, 2011](#mat11); [Schwartz, _et al._, 2002](#sch02); [Walsh, _et al._, 2007](#wal07)). This study chooses to focus on overload confusion because overload confusion has been more frequently mentioned in the literature on consumer confusion than the other two types of confusion. In addition, interviewed individuals who used online information to plan their earlier trips to Hualien consistently mentioned their concern about the massive amount of available information, but mentioned less frequently their similarity and ambiguity confusion.

Escape is often a motivational factor which pushes an individual to travel ([Bi and Lehto, 2018](#bi18); [Dickinson, Hibbert and Filimonau, 2016](#dic16)). Escape experience is also one of the four experience dimensions ([Pine and Gilmore, 1999](#pin99)). Escape experience involves immersion in the destination experience and a sense of escaping from the individual’s normal routine ([Lee, Lee and Ham, 2014](#lee14)). Escape experience also includes tourists distancing _‘themselves from the daily routines, no matter what the daily routines are, where they head, and what they do_’ ([Oh, Fiore and Jeoung, 2007](#oh07), p. 122). The other three types of experience are aesthetic experience, whereby tourists appreciate being in the destination environment; educational experience, which involves tourists increasing their knowledge; and entertainment experience ([Oh, _et al._, 2007](#oh07)).

Each experience dimension is unique ([Oh, _et al._, 2007](#oh07)), and not every dimension has to be present and accounted for simultaneously. For example, Lee, _et al._ ([2014](#lee14)) do not consider aesthetic experience. Given that the nature and rural based Hualien provides an excellent environment for visitors to be temporarily removed from the urban and hectic environment they come from, this study considers specifically the escape experience. Few studies have considered the effect of overload confusion on tourist experience. This study suggests that the overload confusion during information processing could undermine a tourist’s escape experience during a trip. Thus:

> H1a: Overload confusion influences escape experience.

This study further postulates that overload confusion can affect destination image, which is ‘_the sum of beliefs, ideas, and impressions that a person has of a destination_’ ([Crompton, 1979](#cro79), p. 18). The image is often considered to be a combination of offerings and attributes ([Kim, 2014](#kim14)). Some attributes are cognitive in nature and involve an individual’s perceptions of and attitudes about a place ([Martin and Bosque, 2008](#mar08)). Other attributes are affective and involve emotions evoked in the tourists, such as relaxing, pleasant, exciting, and rousing ([Martin and Bosque, 2008](#mar08)). The cognitive and affective components can be considered independently ([Baloglu and Brinberg, 1997](#bal97)), but the presence of both elements ‘_may explain in a better way the image a tourist has of a place_’ ([Martin and Bosque, 2008](#mar08), p. 264).

Past studies have shown that individuals create a destination image through different information sources ([Greaves and Skinner, 2010](#gre10)). Baloglu and McCleary ([1999](#bal99)) suggested that such sources affect how tourists evaluate the cognitive component of a destination. However, external information has a weaker influence on the affective component ([Li, Pan, Zhang and Smith, 2009](#li09)). Nonetheless, few studies have considered the effect of overload confusion on destination image; therefore, in response to the previous studies’ findings, this study suggests the following:

> H1b-1: Overload confusion influences the cognitive destination image.  
>   
> H1b-2: Overload confusion influences the affective destination image.

Regret is what ‘_people experience when they realise or imagine that their present situation or condition would have been better if they had acted differently_’ ([Kaur, Dhir, Chen and Rajala, 2016](#kau16)). It occurs when one feels responsible for negative events ([Zeelenberg, van Dijk, Manstead and van der Pligt, 2000](#zee00)) and _‘is not experienced if the consumer feels that he/she can alter the current outcome, for example, if the consumer purchases a warranty on a product_’ ([Krishen, Bui and Peter, 2010](#kri10), p. 175). Hence, regret has a strong tendency of self-blame ([Zeelenberg and Pieters, 2007](#zee07)) and can be frustrating ([Kahneman and Tversky, 1982](#kah82)).

There are multiple consequences attached to regret. In addition to making individuals feel worse off ([Schwartz, _et al._, 2002](#sch02)), regret also lowers an individual’s confidence in the choice they have made ([Desmeules, 2002](#des02); [Matzler, _et al._, 2011](#mat11); [Walsh, _et al._, 2007](#wal07)).

Since tourists often actively search for information during trip planning and reflect on the events and success of the trip, overload confusion may make tourists feel a sense of regret. This is especially so in the tourism context, since many outcomes cannot be altered and are intensely felt by tourists, especially after much effort, time, money, and anticipation have been expended for a memorable holiday experience. Hence regret, if present, should be felt strongly by tourists. Furthermore, while most literature argues for the usefulness of an information search before a purchase, there is an alternative view whereby, if viewed from the angle of regret literature, ‘external search processes may also have the unintended consequence of contributing to regret’ ([Keaveney, Huber and Herrmann, 2007](#kea07), p. 1028). Thus, this study postulates that tourists’ overload confusion arising from information search could contribute to their regret over choosing some attractions over others whilst touring the destination:

> H1c: Overload confusion influences regret.

Past studies have shown that a lack of understanding arising from overload confusion can lead individuals to feel anxious, frustrated, and dissatisfied ([Matzler, _et al._, 2011](#mat11); [Walsh and Mitchell, 2010](#wal10)). However, Keaveney, _et al._ ([2007](#kea07)) have shown that information search does not affect satisfaction. Thus:

> H1d: Overload confusion influences trip satisfaction.

### Escape experience and its consequences

Since escape experience is often longed for by tourists ([Bi and Lehto, 2018](#bi18)), it is reasonable to assume that it could affect the level of regret felt by tourists. Past studies often suggest that tourist experience positively influences satisfaction ([Lee, _et al._, 2014](#lee14); [Oh, _et al._, 2007](#oh07)). The following hypotheses are thus postulated:

> H2a: Escape experience influences regret.  
>   
> H2b: Escape experience influences trip satisfaction.

### Destination image and its consequences

Whilst some studies indicate that the destination image cognitive dimension influences the affective dimension ([Chen and Phou, 2013](#che13); [Papadimitriou, Kaplanidou and Apostolopoulou, 2018](#pap18)), it is also possible that ‘_destination image is simultaneously formed with cognitive and affective components, instead of being a mere result of causality_’ ([Kim and Chen, 2016](#kim16), p. 2). Given the conflicting results, further investigation is needed regarding how cognitive and affective destination images are related. Destination image also influences behaviour such as satisfaction ([Kim, Hallab and Kim, 2012](#kim12); [Laing, Wheeler, Reeves and Frost, 2014](#lai14)). Thus:

> H3a: Cognitive destination image influences affective destination image.  
>   
> H3b: Cognitive destination image influences trip satisfaction.  
>   
> H3c: Affective destination image influences trip satisfaction.

### Regret and its consequences

Past studies have illustrated that regret negatively influences post-decision attitudes ([Tseng, 2017](#tse17)) and repurchase intent ([Chang, Gao and Zhu, 2015](#cha15)) and leads to dissatisfaction ([Tsiros and Mittal, 2000](#tsi00)). Bui, _et al._ ([2011](#bui11)) also found that regret lowers the level of satisfaction and increases negative emotion. Thus, the following hypothesis is suggested:

> H4: Regret influences trip satisfaction.

### Number of visits as a moderator

Repeat visits are an indicator of a visitor’s experience with the destination ([Kerstetter and Cho, 2004](#ker04)). The number of visits ‘_represents a key marketing variable in segmenting and targeting certain groups and developing a marketing action plan, including product, distribution, pricing and promotion decisions_’ ([Baloglu 2001](#bal01), p. 127). Thus, it is a crucial aspect of tourism research.

First-time and return visitors behave differently. According to Mitchell, _et al._ ([2005](#mit05)), ‘_experience can work for or against the consumer with respect to confusion_’ (p. 147). Consumers without sufficient product knowledge and facing many choices encounter difficulty in decision making ([Heitmann, Lehmann and Herrmann, 2007](#hei07)). This difficulty in information processing likely leads to confusion ([Johnson and Russo, 1984](#joh84)). Matzler, _et al._ ([2011](#mat11)) found that product knowledge negatively affects consumer confusion. With lower levels of information searched, consumers involved in low-involvement purchase are less likely to be confused because of information overload ([Mitchell, _et al._, 2005](#mit05)); however, consumers may, through putting in more effort, avoid confusion during a high-involvement purchase ([Foxman, Berger and Cole, 1992](#fox92)).

Lim, _et al._ ([2016](#lim16)) found that, for recreational and wellness tourist attractions in South Korea, the motivation and satisfaction levels of first-time and return visitors differed. Their behaviour at the destination also differed. Repeat visitors to New Zealand visit fewer attractions than those who visit for the first time ([Oppermann, 1997](#opp97)). First-time visitors explore a broad range of activities that are geographically dispersed, while repeat visitors travel to shop, dine, and socialise with friends and family while in Hong Kong ([Lau and McKercher, 2004](#lau04)). First-timers look for new experiences, while return visitors look for something that they are comfortable and familiar with ([Kim, _et al._, 2012](#mat11)).

Furthermore, the satisfaction perception of visitors who have toured only once and those who made repeated visits relies on different interpretations of their experiences at the destination ([Yuksel, 2001](#yuk01)). Previous experience also affects visitors’ perception of the destination image ([Fakeye and Crompton, 1991](#fak91)). Chew and Jahari ([2014](#che14)) point out that _‘repeat tourists have different cognitive processes in image formation and travel behaviour than those of first-time visitors_’ (p. 383).

Since the number of visits is an important parameter for destination marketing organisations, knowing more about the differences between first-timers (those visitors who have made a single visit to the destination) and repeaters (those visitors who have made more than one visit) can help destination marketing organisations understand visitors better, design destination positioning, and improve visitor satisfaction ([Lim, _et al._, 2016](#lim16)). Thus, this study postulates the following:

> H5: Number of visits moderates the above relationships.

</section>

<section>

## Method

Past studies’ construct items were altered to match the requirements of this study (Table 1). The items for overload confusion were modified from Lu, _et al._([2016](#lu16)). The items of escape experience, regret, and trip satisfaction were obtained from Oh, _et al._ ([2007](#oh07)); Kang, Hong and Lee ([2009](#kan09)); and Lee, _et al._ ([2014](#lee14)), respectively. Survey respondents indicated the degree to which they concurred with the items on a five-point Likert scale ranging from not at all (1) to a lot (5).

The cognitive destination image’s attributes differ across destinations. To obtain a list of attributes of Hualien that is extensive, yet sufficiently brief enough for this study, the attributes summarised by Echtner and Ritchie ([2003](#ech03)) were used as the initial list. Through discussions with visitors who have toured Hualien, those attributes that were irrelevant or unimportant, such as political stability and city atmosphere, were deleted. This mechanism eventually produced a list of 10 cognitive attributes (Table 1). The four often-adopted affective attributes were utilised in this study ([Beerli and Martin, 2004](#bee04)). The survey participants rated the extent to which they agreed with the attributes on a five-point Likert scale.

<table class="center"><caption>  
Table 1: Constructs</caption>

<tbody>

<tr>

<th style="width:25%;">Construct</th>

<th>Item</th>

</tr>

<tr>

<td style="vertical-align:top;">Overload confusion (OC)</td>

<td>I felt confused because there are so many Hualien information websites to choose from.  
The more Hualien information websites I browsed, the more confusion I experienced.  
I felt overwhelmed by too much information provided by those Hualien information websites (deleted).</td>

</tr>

<tr>

<td style="vertical-align:top;">Escape experience (EE)</td>

<td>I felt like I was living in a different time or place.  
I completely escaped from reality.  
I felt I played a different character here (deleted).</td>

</tr>

<tr>

<td style="vertical-align:top;">Regret (RE)</td>

<td>I feel sorry about choosing the attractions in Hualien.  
I regret choosing the attractions in Hualien.  
I should have chosen other alternative attractions in Hualien</td>

</tr>

<tr>

<td style="vertical-align:top;">Trip satisfaction (TS)</td>

<td>I am happy with my decision to visit Hualien.  
My Hualien experience here exceeded my expectations.  
Overall I am satisfied with my Hualien visit</td>

</tr>

<tr>

<th colspan="2">Destination image</th>

</tr>

<tr>

<td style="vertical-align:top;">Nature cognitive (DN)</td>

<td>Farm  
Nature  
Good scenery  
Sea view  
Shoreline</td>

</tr>

<tr>

<td style="vertical-align:top;">Non-nature cognitive (DNN)</td>

<td>Good accommodation quality  
Clean  
Food  
Friendly locals  
Good service quality</td>

</tr>

<tr>

<td style="vertical-align:top;">Affective (DA)</td>

<td>Unpleasant-pleasant  
Gloomy-exciting  
Sleepy-arousing  
Distressing-relaxing</td>

</tr>

</tbody>

</table>

Research assistants were employed to reach out to potential respondents through convenience sampling. They had to ensure, by questioning, that the potential respondents were Taiwan residents who had toured Hualien and had used online websites to look for information regarding Hualien. If the criteria were met, the potential respondents were then provided with information regarding the purpose of the survey and invited to complete the paper-based survey form.

Those survey respondents who had only visited Hualien once were classified as first-timers, and those who had visited Hualien more than once were labelled as repeaters. In accordance with the process in Beerli and Martin ([2004](#bee04)) and Lim and Weaver ([2014](#lim14)), the obtained data of destination image attributes were subjected to factor analysis. This study also used SmartPLS to perform partial least squares analysis for the two visitor groups. This method was used because it is applicable for appreciating complex models ([Urbach and Ahlemann, 2010](#urb10)), such as the model considered in this study. The method also does not have stringent requirements for residual distributions and sample size ([Chin, 1998](#chi98)). The partial least squares, multi-group analysis ([Henseler, Ringle and Sinkovics, 2009](#hen09); [Sarstedt, Henseler and Ringle, 2011](#sar11)) was then applied to compare each of the path coefficients in Figure 1 of the two groups of visitors (first-timers and repeaters), to identify the paths that significantly differ across these two groups of visitors.

</section>

<section>

## Data analysis

Table 2 shows the respondents’ demographic profile, which is generally well spread, with a similar proportion of male and female respondents, in line with Taiwanese being generally well educated, and Hualien is popular across all age groups. Among the 226 returned survey forms, 119 respondents had toured Hualien once (the first-timers), and 107 had toured Hualien twice or more (the repeaters).

<table class="center"><caption>  
Table 2: Respondents’ demographic profile.</caption>

<tbody>

<tr>

<th rowspan="2">Variable</th>

<th colspan="2">First timers  
(n=119)</th>

<th colspan="2">Repeaters  
(n=107)</th>

<th colspan="2">Total  
(n=226)</th>

</tr>

<tr>

<th>No.</th>

<th>Percent</th>

<th>No.</th>

<th>Percent</th>

<th>No.</th>

<th>Percent</th>

</tr>

<tr>

<td>_**Sex**_:  Male</td>

<td style="text-align:center;">60</td>

<td style="text-align:center;">50.4</td>

<td style="text-align:center;">48</td>

<td style="text-align:center;">44.9</td>

<td style="text-align:center;">108</td>

<td style="text-align:center;">47.8</td>

</tr>

<tr>

<td style="padding-left: 37px;">Female</td>

<td style="text-align:center;">59</td>

<td style="text-align:center;">49.6</td>

<td style="text-align:center;">59</td>

<td style="text-align:center;">55.1</td>

<td style="text-align:center;">118</td>

<td style="text-align:center;">52.2</td>

</tr>

<tr>

<td>_**Age**_: 18-20</td>

<td style="text-align:center;">14</td>

<td style="text-align:center;">11.8</td>

<td style="text-align:center;">18</td>

<td style="text-align:center;">16.8</td>

<td style="text-align:center;">32</td>

<td style="text-align:center;">14.2</td>

</tr>

<tr>

<td style="padding-left: 37px;">21-30</td>

<td style="text-align:center;">54</td>

<td style="text-align:center;">45.4</td>

<td style="text-align:center;">28</td>

<td style="text-align:center;">26.2</td>

<td style="text-align:center;">82</td>

<td style="text-align:center;">36.3</td>

</tr>

<tr>

<td style="padding-left: 37px;">31-40</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">9.2</td>

<td style="text-align:center;">29</td>

<td style="text-align:center;">27.1</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">17.7</td>

</tr>

<tr>

<td style="padding-left: 37px;">41-50</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">21.0</td>

<td style="text-align:center;">20</td>

<td style="text-align:center;">18.7</td>

<td style="text-align:center;">45</td>

<td style="text-align:center;">19.9</td>

</tr>

<tr>

<td style="padding-left: 37px;">>50</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">12.6</td>

<td style="text-align:center;">12</td>

<td style="text-align:center;">11.2</td>

<td style="text-align:center;">27</td>

<td style="text-align:center;">11.9</td>

</tr>

<tr>

<td>_**Education**_: Senior-high & below</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">33.6</td>

<td style="text-align:center;">35</td>

<td style="text-align:center;">32.7</td>

<td style="text-align:center;">75</td>

<td style="text-align:center;">33.2</td>

</tr>

<tr>

<td style="padding-left: 71px;">Undergraduate</td>

<td style="text-align:center;">73</td>

<td style="text-align:center;">61.3</td>

<td style="text-align:center;">64</td>

<td style="text-align:center;">59.8</td>

<td style="text-align:center;">137</td>

<td style="text-align:center;">60.6</td>

</tr>

<tr>

<td style="padding-left: 71px;">Graduate</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">5.0</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">7.5</td>

<td style="text-align:center;">14</td>

<td style="text-align:center;">6.2</td>

</tr>

<tr>

<td>_**Employment**_: Student</td>

<td style="text-align:center;">42</td>

<td style="text-align:center;">35.2</td>

<td style="text-align:center;">32</td>

<td style="text-align:center;">29.9</td>

<td style="text-align:center;">74</td>

<td style="text-align:center;">32.8</td>

</tr>

<tr>

<td style="padding-left: 71px;">Manufacturing</td>

<td style="text-align:center;">50</td>

<td style="text-align:center;">42.0</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">31.8</td>

<td style="text-align:center;">84</td>

<td style="text-align:center;">37.2</td>

</tr>

<tr>

<td style="padding-left: 71px;">Services</td>

<td style="text-align:center;">14</td>

<td style="text-align:center;">11.8</td>

<td style="text-align:center;">20</td>

<td style="text-align:center;">18.7</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">15.0</td>

</tr>

<tr>

<td style="padding-left: 71px;">Other</td>

<td style="text-align:center;">13</td>

<td style="text-align:center;">11.0</td>

<td style="text-align:center;">21</td>

<td style="text-align:center;">19.6</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">15.0</td>

</tr>

<tr>

<td>_**No. of visits**_: 1</td>

<td style="text-align:center;">119</td>

<td style="text-align:center;">100</td>

<td> </td>

<td> </td>

<td style="text-align:center;">119</td>

<td style="text-align:center;">52.7</td>

</tr>

<tr>

<td style="padding-left: 78px;">2</td>

<td> </td>

<td> </td>

<td style="text-align:center;">61</td>

<td style="text-align:center;">57.0</td>

<td style="text-align:center;">61</td>

<td style="text-align:center;">27.0</td>

</tr>

<tr>

<td style="padding-left: 78px;">3</td>

<td> </td>

<td> </td>

<td style="text-align:center;">19</td>

<td style="text-align:center;">17.8</td>

<td style="text-align:center;">19</td>

<td style="text-align:center;">8.4</td>

</tr>

<tr>

<td style="padding-left: 78px;">3</td>

<td> </td>

<td> </td>

<td style="text-align:center;">27</td>

<td style="text-align:center;">25.2</td>

<td style="text-align:center;">27</td>

<td style="text-align:center;">11.9</td>

</tr>

</tbody>

</table>

The factor analysis of the destination image attributes showed a Kaiser-Meyer-Olkin (KMO) measure of 0.91 and a Bartlett test of sphericity of 2854.87, which is significant; three factors (cumulative variance explained = 79.10%) were extracted and labelled as nature, non-nature, and affective (Table 1).

<table class="center"><caption>  
Table 3\. Descriptive statistics, average variance extracted (AVE), composite reliability (CR) and Cronbach’s alpha values (Alpha)</caption>

<tbody>

<tr>

<th rowspan="2">Variable  
(see Table 1)</th>

<th colspan="5">First-timers</th>

<th colspan="5">Repeaters</th>

</tr>

<tr>

<th>Mean</th>

<th>SD</th>

<th>AVE</th>

<th>CR</th>

<th>Alpha</th>

<th>Mean</th>

<th>SD</th>

<th>AVE</th>

<th>CR</th>

<th>Alpha</th>

</tr>

<tr>

<td>OC</td>

<td style="text-align:center;">2.83</td>

<td style="text-align:center;">0.72</td>

<td style="text-align:center;">0.80</td>

<td style="text-align:center;">0.89</td>

<td style="text-align:center;">0.76</td>

<td style="text-align:center;">2.55</td>

<td style="text-align:center;">0.87</td>

<td style="text-align:center;">0.86</td>

<td style="text-align:center;">0.92</td>

<td style="text-align:center;">0.83</td>

</tr>

<tr>

<td>EE</td>

<td style="text-align:center;">3.77</td>

<td style="text-align:center;">0.68</td>

<td style="text-align:center;">0.79</td>

<td style="text-align:center;">0.88</td>

<td style="text-align:center;">0.74</td>

<td style="text-align:center;">3.97</td>

<td style="text-align:center;">0.75</td>

<td style="text-align:center;">0.75</td>

<td style="text-align:center;">0.86</td>

<td style="text-align:center;">0.67</td>

</tr>

<tr>

<td>RE</td>

<td style="text-align:center;">2.47</td>

<td style="text-align:center;">0.70</td>

<td style="text-align:center;">0.75</td>

<td style="text-align:center;">0.90</td>

<td style="text-align:center;">0.83</td>

<td style="text-align:center;">2.29</td>

<td style="text-align:center;">0.84</td>

<td style="text-align:center;">0.82</td>

<td style="text-align:center;">0.93</td>

<td style="text-align:center;">0.89</td>

</tr>

<tr>

<td>TS</td>

<td style="text-align:center;">3.68</td>

<td style="text-align:center;">0.62</td>

<td style="text-align:center;">0.78</td>

<td style="text-align:center;">0.92</td>

<td style="text-align:center;">0.86</td>

<td style="text-align:center;">3.96</td>

<td style="text-align:center;">0.68</td>

<td style="text-align:center;">0.83</td>

<td style="text-align:center;">0.94</td>

<td style="text-align:center;">0.90</td>

</tr>

<tr>

<td>DN</td>

<td style="text-align:center;">4.13</td>

<td style="text-align:center;">0.66</td>

<td style="text-align:center;">0.83</td>

<td style="text-align:center;">0.96</td>

<td style="text-align:center;">0.95</td>

<td style="text-align:center;">4.31</td>

<td style="text-align:center;">0.64</td>

<td style="text-align:center;">0.83</td>

<td style="text-align:center;">0.96</td>

<td style="text-align:center;">0.95</td>

</tr>

<tr>

<td>DNN</td>

<td style="text-align:center;">3.67</td>

<td style="text-align:center;">0.56</td>

<td style="text-align:center;">0.66</td>

<td style="text-align:center;">0.91</td>

<td style="text-align:center;">0.87</td>

<td style="text-align:center;">3.90</td>

<td style="text-align:center;">0.66</td>

<td style="text-align:center;">0.72</td>

<td style="text-align:center;">0.93</td>

<td style="text-align:center;">0.90</td>

</tr>

<tr>

<td>DA</td>

<td style="text-align:center;">3.83</td>

<td style="text-align:center;">0.83</td>

<td style="text-align:center;">0.81</td>

<td style="text-align:center;">0.94</td>

<td style="text-align:center;">0.92</td>

<td style="text-align:center;">4.17</td>

<td style="text-align:center;">0.85</td>

<td style="text-align:center;">0.86</td>

<td style="text-align:center;">0.96</td>

<td style="text-align:center;">0.94</td>

</tr>

<tr>

<td colspan="11">Note: There is a significant difference across the two visitor groups for all the constructs (ρ<0.05) except RE</td>

</tr>

</tbody>

</table>

The constructs possess internal consistency reliability since the composite reliability values are larger than 0.7 ([Fornell and Larcker, 1981](#for81)). Average variance extracted (AVE) values exceeding 0.5 support convergent validity. Discriminant validity ([Chin, 1998](#chi98)) exists because the square root of the AVE of each construct is larger than the correlation coefficients involving the construct (Table 4), the Heterotrait-Monotrait Ratio (HTMT) requirement is met, and the HTMT confidence interval does not contain the value 1 ([Henseler, Ringle and Sarstedt, 2015](#hen15)). Collinearity does not exist since the VIF values are lower than the value of 5 ([Hair, Ringle and Sarstedt, 2011](#hai11)).

<table class="center"><caption>  
Table 4: Discriminant validity assessment.</caption>

<tbody>

<tr>

<th> </th>

<th>DA</th>

<th>DNN</th>

<th>DN</th>

<th>OC</th>

<th>EE</th>

<th>RE</th>

<th>TS</th>

</tr>

<tr>

<th colspan="8">First-timers</th>

</tr>

<tr>

<td>DA</td>

<td style="text-align:center;">**0.90**</td>

<td colspan="6"> </td>

</tr>

<tr>

<td>DNN</td>

<td style="text-align:center;">0.44</td>

<td style="text-align:center;">**0.81**</td>

<td colspan="5"> </td>

</tr>

<tr>

<td>DN</td>

<td style="text-align:center;">0.43</td>

<td style="text-align:center;">0.54</td>

<td style="text-align:center;">**0.91**</td>

<td colspan="4"> </td>

</tr>

<tr>

<td>OC</td>

<td style="text-align:center;">0.00</td>

<td style="text-align:center;">0.00</td>

<td style="text-align:center;">-0.06</td>

<td style="text-align:center;">**0.90**</td>

<td colspan="3"> </td>

</tr>

<tr>

<td>EE</td>

<td style="text-align:center;">0.41</td>

<td style="text-align:center;">0.49</td>

<td style="text-align:center;">0.51</td>

<td style="text-align:center;">-0.17</td>

<td style="text-align:center;">**0.89**</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>RE</td>

<td style="text-align:center;">-0.09</td>

<td style="text-align:center;">-0.06</td>

<td style="text-align:center;">-0.30</td>

<td style="text-align:center;">0.53</td>

<td style="text-align:center;">-0.45</td>

<td style="text-align:center;">**0.86**</td>

<td> </td>

</tr>

<tr>

<td>TS</td>

<td style="text-align:center;">0.34</td>

<td style="text-align:center;">0.43</td>

<td style="text-align:center;">0.50</td>

<td style="text-align:center;">-0.13</td>

<td style="text-align:center;">0.72</td>

<td style="text-align:center;">-0.50</td>

<td style="text-align:center;">**0.89**</td>

</tr>

<tr>

<th colspan="8">Repeaters</th>

</tr>

<tr>

<td>DA</td>

<td style="text-align:center;">**0.93**</td>

<td colspan="6"> </td>

</tr>

<tr>

<td>DNN</td>

<td style="text-align:center;">0.64</td>

<td style="text-align:center;">**0.85**</td>

<td colspan="5"> </td>

</tr>

<tr>

<td>DN</td>

<td style="text-align:center;">0.56</td>

<td style="text-align:center;">0.59</td>

<td style="text-align:center;">**0.91**</td>

<td colspan="4"> </td>

</tr>

<tr>

<td>OC</td>

<td style="text-align:center;">-0.30</td>

<td style="text-align:center;">-0.41</td>

<td style="text-align:center;">-0.39</td>

<td style="text-align:center;">**0.92**</td>

<td colspan="3"> </td>

</tr>

<tr>

<td>EE</td>

<td style="text-align:center;">0.53</td>

<td style="text-align:center;">0.44</td>

<td style="text-align:center;">0.52</td>

<td style="text-align:center;">-0.41</td>

<td style="text-align:center;">**0.86**</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>RE</td>

<td style="text-align:center;">-0.55</td>

<td style="text-align:center;">-0.54</td>

<td style="text-align:center;">-0.57</td>

<td style="text-align:center;">0.70</td>

<td style="text-align:center;">-0.43</td>

<td style="text-align:center;">**0.91**</td>

<td> </td>

</tr>

<tr>

<td>TS</td>

<td style="text-align:center;">0.55</td>

<td style="text-align:center;">0.58</td>

<td style="text-align:center;">0.59</td>

<td style="text-align:center;">0.48</td>

<td style="text-align:center;">0.53</td>

<td style="text-align:center;">-0.64</td>

<td style="text-align:center;">**0.91**</td>

</tr>

<tr>

<td colspan="8">Note: Diagonal elements (in bold) show the square root of AVE. Off-diagonal elements are correlations between constructs</td>

</tr>

</tbody>

</table>

The outcomes of the hypothesis testing and the partial least squares multi-group analysis are listed in Table 5.

</section>

<section>

## Results and discussion

### Overload confusion

Overload confusion is the only construct considered in this study for which the mean is higher for first-timers (mean = 2.83) than for repeaters (mean = 2.55). The means of the other constructs are significantly lower when the visitors are first-timers than repeaters (escape experience: first-timers = 3.77; repeaters = 3.97; nature cognitive destination image: first-timers = 4.13; repeaters = 4.31; non-nature cognitive destination image: first-timers = 3.67; repeaters = 3.90; affective destination image: first-timers = 3.83; repeaters = 4.17; trip satisfaction: first-timers = 3.68; repeaters = 3.96), or similar (regret: first-timers = 2.47; repeaters = 2.29). This observation shows that first-timers are more susceptible to overload confusion than are the more experienced repeaters. This finding supported those of past studies, such as Heitmann, _et al._ ([2007](#hei07)) and Matzler, _et al._ ([2011](#mat11)).

<table class="center"><caption>  
Table 5\. Path analysis</caption>

<tbody>

<tr>

<th rowspan="2">Hypothesis</th>

<th colspan="2">First-timers</th>

<th colspan="2">Repeaters</th>

<th rowspan="2">Multi-group  
analysis (H5)</th>

</tr>

<tr>

<th>Path-value</th>

<th>t-value</th>

<th>Path-value</th>

<th>t-value</th>

</tr>

<tr>

<td colspan="6">H1a:  Overload confusion→Escape experience</td>

</tr>

<tr>

<td style="padding-left: 30px;">OC→EE</td>

<td style="text-align:center;">-0.17*</td>

<td style="text-align:center;">2.21</td>

<td style="text-align:center;">-0.41**</td>

<td style="text-align:center;">6.06</td>

<td style="text-align:center;">Yes</td>

</tr>

<tr>

<td colspan="6">H1b-1:   Overload confusion→Cognitive destination image</td>

</tr>

<tr>

<td style="padding-left: 30px;">OC→DN</td>

<td style="text-align:center;">-0.06</td>

<td style="text-align:center;">0.63</td>

<td style="text-align:center;">-0.39**</td>

<td style="text-align:center;">5.41</td>

<td style="text-align:center;">Yes</td>

</tr>

<tr>

<td style="padding-left: 30px;">OC→DNN</td>

<td style="text-align:center;">0.00</td>

<td style="text-align:center;">0.04</td>

<td style="text-align:center;">-0.41**</td>

<td style="text-align:center;">5.34</td>

<td style="text-align:center;">Yes</td>

</tr>

<tr>

<td colspan="6">H1b-2:  Overload confusion→Affective destination image</td>

</tr>

<tr>

<td style="padding-left: 30px;">OC→DA</td>

<td style="text-align:center;">0.02</td>

<td style="text-align:center;">0.21</td>

<td style="text-align:center;">0.00</td>

<td style="text-align:center;">0.02</td>

<td> </td>

</tr>

<tr>

<td colspan="6">H1c:  Overload confusion→Regret</td>

</tr>

<tr>

<td style="padding-left: 30px;">OC→RE</td>

<td style="text-align:center;">0.47**</td>

<td style="text-align:center;">5.24</td>

<td style="text-align:center;">0.63**</td>

<td style="text-align:center;">8.54</td>

<td> </td>

</tr>

<tr>

<td colspan="6">H1d:  Overload confusion→Trip satisfaction</td>

</tr>

<tr>

<td style="padding-left: 30px;">OC→TS</td>

<td style="text-align:center;">0.12</td>

<td style="text-align:center;">1.08</td>

<td style="text-align:center;">-0.02</td>

<td style="text-align:center;">0.14</td>

<td> </td>

</tr>

<tr>

<td colspan="6">H2a:  Escape experience→Regret</td>

</tr>

<tr>

<td style="padding-left: 30px;">EE→RE</td>

<td style="text-align:center;">-0.38**</td>

<td style="text-align:center;">4.61</td>

<td style="text-align:center;">-0.17</td>

<td style="text-align:center;">1.73</td>

<td> </td>

</tr>

<tr>

<td colspan="6">H2b:  Escape experience→Trip satisfaction</td>

</tr>

<tr>

<td style="padding-left: 30px;">EE→TS</td>

<td style="text-align:center;">0.49**</td>

<td style="text-align:center;">4.48</td>

<td style="text-align:center;">0.19</td>

<td style="text-align:center;">1.70</td>

<td style="text-align:center;">Yes</td>

</tr>

<tr>

<td colspan="6">H3a:  Cognitive destination image→Affective destination image</td>

</tr>

<tr>

<td style="padding-left: 30px;">DN→DA</td>

<td style="text-align:center;">0.28*</td>

<td style="text-align:center;">2.60</td>

<td style="text-align:center;">0.29**</td>

<td style="text-align:center;">3.70</td>

<td> </td>

</tr>

<tr>

<td style="padding-left: 30px;">DNN→DA</td>

<td style="text-align:center;">0.28**</td>

<td style="text-align:center;">2.63</td>

<td style="text-align:center;">0.47**</td>

<td style="text-align:center;">5.63</td>

<td> </td>

</tr>

<tr>

<td colspan="6">H3b:  Cognitive destination image→Trip satisfaction</td>

</tr>

<tr>

<td style="padding-left: 30px;">DN→TS</td>

<td style="text-align:center;">0.11</td>

<td style="text-align:center;">1.30</td>

<td style="text-align:center;">0.15</td>

<td style="text-align:center;">1.47</td>

<td> </td>

</tr>

<tr>

<td style="padding-left: 30px;">DNN→TS</td>

<td style="text-align:center;">0.10</td>

<td style="text-align:center;">1.08</td>

<td style="text-align:center;">0.17</td>

<td style="text-align:center;">1.41</td>

<td> </td>

</tr>

<tr>

<td colspan="6">H3c:  Affective destination image→Trip satisfaction</td>

</tr>

<tr>

<td style="padding-left: 30px;">DA→TS</td>

<td style="text-align:center;">0.02</td>

<td style="text-align:center;">0.22</td>

<td style="text-align:center;">0.07</td>

<td style="text-align:center;">0.62</td>

<td> </td>

</tr>

<tr>

<td colspan="6">H4:  Regret→Trip satisfaction</td>

</tr>

<tr>

<td style="padding-left: 30px;">RE→TS</td>

<td style="text-align:center;">-0.30**</td>

<td style="text-align:center;">2.80</td>

<td style="text-align:center;">-0.33**</td>

<td style="text-align:center;">2.60</td>

<td> </td>

</tr>

<tr>

<td colspan="6">Notes:  
1. * Significant paths at ρ<0.05 level; ** Significant paths at ρ<0.01 level  
2. Multi-group analysis: Yes – significant difference across groups at the ρ<0.05 level.  
3. First-timer: R2 of DA=0.24; DN=0.00; DNN=0.00; EE=0.03; RE=0.42; SAT=0.60.  
4. Repeater: R2 of DA=0.46; DN=0.15; DNN=0.17; EE=0.17; RE=0.52; SAT=0.55.</td>

</tr>

</tbody>

</table>

The study further provides additional insights on the negative effect of information on satisfaction and the less-considered constructs of escape experience and destination image as information becomes excessive. Firstly, previous studies have proposed that information can enhance trip satisfaction. However, this study found that information associated with overload confusion, instead of enhancing trip satisfaction, could have no effect on trip satisfaction (H1d: path coefficient of first-timers = 0.12; repeaters = −0.02). It also makes visitors regret visiting the destination (H1c: path coefficient of first-timers = 0.47; repeaters = 0.63). This study thus supported the finding of Keaveney, _et al._ ([2007](#kea07)) that overload confusion significantly contributes to a sense of regret.

Secondly, this paper provides the empirical relationship between overload confusion and escape experience. The analysis showed that overload confusion negatively impacts escape experience (H1a: path coefficient of first-timers = −0.17; repeaters = −0.41), and, according to the partial least squares multi-group analysis, there is significant difference between the path coefficient across the two visitor groups. Thus, the negative effect of overload confusion on escape experience is felt more strongly by repeaters than first-timers. This finding could be explained by the difference between tourists’ behaviour at the destination arising from the number of visits. Everything about the destination is new during the first trip. Hence, the effect of the confusion arising from too much information during the information search is less able to exert its negative influence on escape experience. However, repeaters should have already visited many of the destination attractions and would therefore need more information to discover additional attractions, or to have more in-depth touring ideas to achieve the escape feeling, which results in repeaters being more susceptible to overload confusion.

Thirdly, even though past studies have often illustrated that information contributes to destination image ([Greaves and Skinner, 2010](#gre10)), this study suggests that the contribution of information to destination image could be minimal or possibly decrease. Thus, according to past literature, having more information can enhance the destination image, but, once a certain information threshold that leads to confusion has been reached, the contribution of overload confusion to the cognitive destination image becomes insignificant for first-timers (H1b-1: path coefficient of nature cognitive destination image = −0.06; non-nature cognitive destination image = 0.00) and undermines destination image for repeaters (H1b-1: path coefficient of nature cognitive destination image = −0.39; non-nature cognitive destination image = −0.41), and their path coefficients differ significantly across the two visitor groups. Similarly, given the more broad and exploratory nature of visits by first-timers, overload confusion is not able to exert its negative influence on these visitors. However, the more in-depth type of visit by repeaters renders the negative impact of overload confusion on cognitive destination image more strongly felt.

### The impact of overload confusion

#### Escape experience

It is encouraging to observe that repeaters (mean = 3.97) have a significantly higher level of escape experience than do first-timers (mean = 3.77). The possible reasons are that repeaters are less likely to rush from one attraction to another, have more in-depth travel, are more able to immerse themselves in the environment, and have deeper appreciation of Hualien.

The escape experience can reduce the feeling of regret for first-timers but not repeaters (H2a: path coefficient of first-timers = −0.38; repeaters = −0.17), thus indicating that repeaters are more demanding and less easy to please than are first-timers. Whilst partially supporting studies, such as Lee, _et al._ ([2014](#lee14)) and Oh, _et al._, ([2007](#oh07)), this suggests that experience positively influences tourists’ satisfaction, and even though repeaters have more escape experience than first-timers, the contribution of escape experience to trip satisfaction is significantly higher for first-timers (H2b: path coefficient = 0.49) and significantly different from repeaters (H2b: path coefficient = 0.19), which is not significant, hence illustrating again the more demanding nature of repeaters.

#### Regret

The negative effect of regret over visiting the wrong destination attractions on satisfaction is prevalent for both first-timers and repeaters (H4: path coefficient of first-timers = −0.30; repeaters = −0.33), thus confirming the finding of past studies that regret can lower satisfaction ([Bui, _et al._, 2011](#bui11); [Tsiros and Mittal, 2000](#tsi00)).

#### Destination image

The study supported the findings of past research, such as Chen and Phou ([2013](#che13)) and Papadimitriou, _et al._ ([2018](#pap18)), that the cognitive dimension contributes to the affective dimension of destination image (H3a: nature cognitive destination image - path coefficient of first-timers = 0.28; repeaters = 0.29, and non-nature cognitive destination image - path coefficient of first timers = 0.28; repeaters = 0.47). While Kim, _et al._ (2012) and Laing, _et al._, ([2014](#lai14)) have shown that the destination image influences satisfaction, this was not the case in this study. Hence, as a whole, satisfaction is more affected by regret followed by escape experience than by the other examined factors.

#### Number of visits as a moderator

As discussed above, this paper augments the existing literature on the difference between first-timers and repeaters. The difference is particularly evident in the consequence of overload confusion and escape experience. As a whole, overload confusion is more strongly felt by repeaters than first-timers. The escape experience is negatively affected by overload confusion and is more strongly felt by repeaters than first-timers. The negative impact of overload confusion on nature and non-nature cognitive destination image is also more strongly felt by repeaters than by first-timers. On the other hand, escape experience influences trip satisfaction in a positive manner and is more strongly felt by first-timers than repeaters (which is insignificant). The result again suggests that repeaters are more demanding than first-timers, as the latter move from the touch-and-go and exploratory type of visit to in-depth visits.

### Practical implications

This paper shows the ability of overload confusion to undermine escape experience and increase the regret associated with a trip. It can also lower the destination image perceived by repeaters. The lowering of escape experience could increase regret, and regret would lower trip satisfaction. The negative impact of overload confusion is particularly felt among repeaters. While it is not possible for destination marketing organisations to prevent others from providing information about the destination, they should act on the issue of overload confusion. Destination marketing organisations should heed the advice of Bawden, _et al._ ([1999](#baw99)), who emphasised the need for individuals and organisations to exercise control to cope with information overload. Thus, while destination marketing organisations should continue to provide quality information on their own public tourism websites ([Kua and Chen, 2015](#kua15)) and a good and concise summary of good information available beyond the destination marketing organisation websites, it will be useful for destination marketing organisations to work with and promote a few selected and major bloggers and websites to provide authoritative information so that these sites would be the first stop for potential visitors, thereby lowering overload confusion. They should also promote the expertise of these bloggers to the public.

Destination marketing organisations should conduct further studies on the types and depth of information needs of their visitors, particularly on the repeaters, and organise or co-ordinate the information to reduce overload confusion. Such a move is also in line with the various strategies to cope with information overload and suggested in information science research and, thus, with how tourists will respond when faced with information overload. For example, Savolainen ([2007](#sav07)) has suggested individuals may adopt the filtering and withdrawal strategies to cope with information overload. The former coping strategy involves ignoring information considered useless, and the latter deals with keeping the number of information sources used to a minimum.

Besides the filtering and withdrawal strategies, Saxena and Lamest ([2018](#sax18)) also mentioned the summarising strategy. By providing useful and relevant information that caters to the needs of first-timers and repeaters, as well as useful summaries, the information sources manned by destination marketing organisations and bloggers will not likely be the casualties as a result of the filtering and withdrawal strategies adopted by the visitors. Promoting the expertise of the websites also fits well with another visitor’s strategy to cope with information overload, i.e., preference for shortcuts provided by experts ([Stanton and Paolo, 2012](#sta12)).

The implications for destination marketing organisations are also relevant to bloggers who specialised in tourist destinations. Since the study has shown that repeaters are generally more demanding than first-timers, bloggers could carefully consider the customer segment they wish to better serve and provide articles with the required breadth and depth accordingly. They should also actively promote themselves beyond the Internet world to avoid being ignored by readers when they adopt the various coping strategies arising from information overload.

Research limitations and future research directions

This study has limitations, even though it provides several interesting insights. This study used a convenience sampling method. Although the respondents have a broad profile, caution is needed in extending the results to other contexts. This study is limited to Hualien, a rural and nature-based tourist destination. The cognitive destination image attributes of one destination are likely to differ from those of another destination. Future research could examine different types of destinations. The study shows that as the amount of information reaches a certain threshold, its positive effect on escape experience, destination image and trip satisfaction declines or even becomes negative. Hence, this study suggests that future studies could consider the rate and other influencing factors of the diminishing positive effect of information on these constructs.

This study has only considered overload confusion. Further studies could include the other two types of confusion, namely, similarity confusion and ambiguity confusion, and investigate how these three types of confusion are related in the tourism context. The study has only considered the destination image formed by visitors after a trip. However, visitors, especially repeaters, already have some image of the destination before the trip formed through the content of the information sources they have read and/or their previous visit. It will be useful to include the destination image before the trip as a construct and consider how it works together with overload confusion to influence destination image after the trip. This study has also suggested an interesting link between overload confusion and destination image. It will be of interest to both information science researchers and marketers if future studies could extend the concept of information overload and consider the impact of overload confusion on brand image and country image and examine whether some generalisation could be made about the role of overload confusion in shaping the image of products and services.

</section>

<section>

## Conclusions

This study provides a direct link between overload confusion and two tourism-related concepts that have not been adequately explored: destination image and escape experience. It further illustrates that there is a limit to the influence of information on the frequent concerns of destination marketing organisations, i.e., trip satisfaction and, to some extent, destination image, even though past studies have often illustrated that information positively affects trip satisfaction and destination image. Once information leads to overload confusion, information could have no further positive impact on these two constructs. Furthermore, the negative effect of information appears when the volume of information becomes too much for tourists to cope with and causes overload confusion. Overload confusion adds to the undesirable outcome of regret and undermines the desirable outcome of escape experience. This study also further contributes to the existing literature on the difference between first-timers and repeaters. Thus, this paper also adds to the literature of information science by providing evidence of the applicability of the concept of information overload to the tourism context, and its negative effect which is also prevalent in some commonly discussed tourism-related concepts.

</section>

<section>

## About the authors.

**Wee Kheng Tan** received his Ph.D. from National University of Singapore. He is an Associate Professor at National Sun Yat-sen University, Taiwan. His research interests include electronic commerce, consumer behaviour, tourism and leisure activities. He can be contacted at [tanwk@mail.nsysu.edu.tw](mailto:tanwk@mail.nsysu.edu.tw)  
**Ping-Chen Kuo** is a master’s degree student at Yuan Ze University. His research interests are e-commerce and e-tourism.

</section>

<section class="refs">

## References

*   Baloglu, S. (2001). Image variations of Turkey by familiarity index. _Tourism Management, 22_(2), 127-133.
*   Baloglu, S. & Brinberg, D. (1997). Affective images of tourism destination. _Journal of Travel Research, 35_(4), 11-15.
*   Baloglu, S. & McCleary, K.W. (1999). A model of destination image formation. _Annals of Tourism Research, 26_(4), 868-897.
*   Bawden, D. & Robinson, L. (2009). The dark side of information: overload, anxiety and other paradoxes and pathologies. _Journal of Information Science, 35_(2), 180-191\.
*   Bawden, D., Holtham, C. & Courtney, N. (1999). Perspectives on information overload. _Aslib Proceedings, 51_(8), 249-255.
*   Beerli, A. & Martin, J.D. (2004). Factors influencing destination image. _Annals of Tourism Research, 31_(3), 657-681.
*   Bi, J. & Lehto, X.Y. (2018). Impact of cultural distance on international destination choices: the case of Chinese outbound travellers. _International Journal of Tourism Research, 20_(1), 50-59.
*   Bui, M., Krishen, A.S. & Bates, K. (2011). Modelling regret effects on consumer post-purchase decisions. _European Journal of Marketing, 45_(7/8), 1068-1090.
*   Chang, Y.P., Gao, Y. & Zhu, D.H. (2015). The impact of product regret on repurchase intention. _Social Behavior and Personality, 43_(8), 1347-1360.
*   Chen, C. & Phou, S. (2013). A closer look at destination: image, personality, relationship and loyalty. _Tourism Management, 36_, 269-278.
*   Chew, Y. & Jahari, S. (2014). Destination image as a mediator between perceived risks and revisit intention: a case of post-disaster Japan. _Tourism Management, 40_, 382-393.
*   Chin, W. (1998). The partial least squares approach to structural equation modelling. In G. Marcoulides (Ed.), _Modern methods for business research_ (pp. 295-358). Mahwah, NJ: Lawrence Erlbaum Associates.
*   Crompton, J.L. (1979). An assessment of the image of Mexico as a vacation destination and the influence of geographical location upon the image. _Journal of Travel Research, 18_(4), 18-23.
*   Curtin, S. (2005). Nature, wild animals and tourism: an experiential view. _Journal of Ecotourism, 4_(1), 1-15.
*   Desmeules, R. (2002). The impact of variety on consumer happiness: marketing and the tyranny of freedom. _Academy of Marketing Science Review, 12_(12), 1-33.
*   Dickinson, J.E., Hibbert, J.F. & Filimonau, V. (2016). Mobile technology and the tourist experience: (dis)connection at the campsite. _Tourism Management, 57_, 193-201.
*   Echtner, C.M. & Ritchie, J. (2003). The meaning and measurement of destination image. _Journal of Tourism Studies, 14_(1), 37-48.
*   Eppler, M.J. (2015). Information quality and information overload: the promises and perils of the information age. In L. Cantoni & J.A. Danowski (Eds.), _Communication and technology_ (pp. 215-232). Berlin: de Gruyter Mouton.
*   Eppler, M.J. & Mengis, J. (2004). The concept of information overload: a review of literature from organization science, accounting, marketing, MIS, and related disciplines. _The Information Society, 20_(5), 325-344\.
*   Fakeye, P.C. & Crompton, J.L. (1991). Image differences between prospective, first-time, and repeat visitors to the Lower Rio Grande Valley. _Journal of Travel Research, 30_(2), 10-15.
*   Feng, Y. & Agosto, D.E. (2017). [The experience of mobile information overload: struggling between needs and constraints](http://www.webcitation.org/6r2R7ZVch). _Information Research, 22_(2), paper 754\. Retrieved from http://informationr.net/ir/22-2/paper754.html (Archived by WebCite® at http://www.webcitation.org/6r2R7ZVch)
*   Fornell, C. & Larcker, D. (1981). Evaluating structural equation models with unobservable and measurement error. _Journal of Marketing Research, 18_(1), 39-50.
*   Foxman, E.R., Berger, P.W. & Cole, J.A. (1992). Consumer brand confusion: a conceptual framework. _Psychology and Marketing, 9_(2), 123-141.
*   Greaves, N. & Skinner, H. (2010). The importance of destination image analysis to UK rural tourism. _Marketing Intelligence & Planning, 28_(4), 486-507.
*   Hair, J.F., Ringle, C.M. & Sarstedt, M. (2011). PLS-SEM: indeed a silver bullet. _Journal of Marketing Theory and Practice, 19_(2), 139-152.
*   Heitmann, M., Lehmann, D.R. & Herrmann, A. (2007). Choice goal attainment and decision and consumption satisfaction. _Journal of Marketing Research, 44_(2), 234-250.
*   Henseler, J., Ringle, C.M. & Sarstedt, M. (2015). A new criterion for assessing discriminant validity in variance-based structural equation modelling. _Journal of the Academy of Marketing Science, 43_(1), 115-135.
*   Henseler, J., Ringle, C.M. & Sinkovics, R.R. (2009), The use of partial least squares path modeling in international marketing. In R.R. Sinkovics, & P.N. Ghauri (Eds.), _New challenges to international marketing_ (pp. 277-320). Bingley, UK: Emerald. (Advances in international marketing, volume 20)
*   Jackson, T.W. & Farzaneh, P. (2012). Theory-based model of factors affecting information overload. _International Journal of Information Management, 32_(6), 523-532.
*   Jeng, J. & Fesenmaier, D.R. (2002). Conceptualizing the travel decision-making hierarchy: a review of recent developments. _Tourism Analysis, 7_(1), 15-32.
*   Johnson, E.J. & Russo, E.J. (1984). Product familiarity and learning new information. _Journal of Consumer Research, 11_(1), 542-550.
*   Kahneman, D. & Tversky, A. (1982). The psychology of preferences. _Scientific American, 246_(1), 160-173.
*   Kang, I., Cheon, D. & Shin, M.M. (2011). Advertising strategy for outbound travel services. _Service Business, 5_(4), 361-380.
*   Kang, Y.S., Hong, S. & Lee, H. (2009). Exploring continued online service usage behavior: the roles of self-image congruity and regret. _Computers in Human Behavior, 25_(1), 111-122.
*   Kangun, N. & Polonsky, M.J. (1995). Regulation of environmental marketing claims: a comparative perspective. _International Journal of Advertising, 13_(4), 1-24.
*   Kaur, P., Dhir, A., Chen, S. & Rajala, R. (2016). Understanding online regret experience using the theoretical lens of flow experience. _Computers in Human Behavior, 57_, 230-239.
*   Keaveney, S.M., Huber, F. & Herrmann, A. (2007). A model of buyer regret: selected prepurchase and postpurchase antecedents with consequences for the brand and the channel. _Journal of Business Research, 60_(12), 1207-1215.
*   Kerstetter, D. & Cho, M.H. (2004). Prior knowledge, credibility and information search. _Annals of Tourism Research, 31_(4), 961-985.
*   Kim, H. & Chen, J.S. (2016). Destination image formation process: a holistic mode. _Journal of Vacation Marketing, 22_(2), 154-166.
*   Kim, J.H. (2014). The antecedents of memorable tourism experiences: the development of a scale to measure the destination attributes associated with memorable experiences. _Tourism Management, 44_, 34-45.
*   Kim, K., Hallab, Z. & Kim, J.N. (2012). The moderating effect of travel experience in a destination on the relationship between the destination image and the intention to revisit. _Journal of Hospitality Marketing & Management, 21_(5), 486-505.
*   King, C., Chen, N. & Funk, D.C. (2015). Exploring destination image decay: a study of sport tourists’ destination image change after event participation. _Journal of Hospitality & Tourism Research, 39_(1), 3-31.
*   Krishen, A.S., Bui, M. & Peter, P.C. (2010). Retail kiosks: how regret and variety influence consumption. _International Journal of Retail & Distribution Management, 38_(3), 173-189.
*   Kua, E.C.S. & Chen, C.D. (2015). Cultivating travellers’ revisit intention to e-tourism service: the moderating effect of website interactivity. _Behaviour & Information Technology, 34_(5), 465-478.
*   Laing, J., Wheeler, F., Reeves, K. & Frost, W. (2014). Assessing the experiential value of heritage assets: a case study of a Chinese heritage precinct, Bendigo, Australia. _Tourism Management, 40_, 180-192.
*   Lau, A. & McKercher, B. (2004). Exploration versus acquisition: a comparison of first-time and repeat visitors. _Journal of Travel Research, 42_(3), 279-285.
*   Lee, K., Lee, H.R. & Ham, S. (2014). The effects of presence induced by smartphone applications on tourism: application to cultural heritage attractions. In Z. Xiang, & I. Tussyadiah (Eds.), _Information and communication technologies in tourism 2014: proceedings of the international conference in Dublin, Ireland, January 21-24, 2014_ (pp. 52-72). Cham, Switzerland: Springer.
*   Li, X., Pan, B., Zhang, L. & Smith, W. (2009). The effect of online information search on image development: insights from a mixed-methods study. _Journal of Travel Research, 48_(1), 45-57.
*   Lim, Y. & Weaver, P.A. (2014). Customer-based brand equity for a destination: the effect of destination image on preference for products associated with a destination brand. _International Journal of Tourism Research, 16_(3), 223-231.
*   Lim, Y.J., Kim, H.K. & Lee, T.J. (2016). Visitor motivational factors and level of satisfaction in wellness tourism: comparison between first-time visitors and repeat visitors. _Asia Pacific Journal of Tourism Research, 21_(2), 137-156.
*   Lu, A.C.C., Gursoy, D. & Lu, C.Y.R. (2016). Antecedents and outcomes of consumers’ confusion in the online tourism domain. _Annals of Tourism Research, 57_, 76-93.
*   Martin, H.S. & Bosque, I.A. (2008). Exploring the cognitive-affective nature of destination image and the role of psychological factors in its formation. _Tourism Management, 29_(2), 263-277.
*   Matzler, K., Stieger, D. & Füller, J. (2011). Consumer confusion in Internet-based mass customization: testing a network of antecedents and consequences. _Journal of Consumer Policy, 34_(2), 231-247.
*   Mitchell, V.W., Walsh, G. & Yamin, M. (2005). Towards a conceptual model of consumer confusion. _Advances in Consumer Research, 32_(1), 143-150.
*   Moon, S-J., Costello, J.P. & Koo, D-M. (2017). The impact of consumer confusion from eco-labels on negative WOM, distrust, and dissatisfaction. _International Journal of Advertising, 36_(2), 246-271.
*   Oh, H., Fiore, A.M. & Jeoung, M. (2007). Measuring experience economy concepts: tourism applications. _Journal of Travel Research, 46_(2), 119-132.
*   Oppermann, M. (1997). First-time and repeat visitors to New Zealand. _Tourism Management, 18_(3), 177-181.
*   Papadimitriou, D., Kaplanidou, K.K. & Apostolopoulou, A. (2018). Destination image components and word-of-mouth intentions in urban tourism: a multigroup approach. _Journal of Hospitality & Tourism Research, 42_(4), 503-527.
*   Pine, B.J., & Gilmore, H.J. (1999). _The experience economy: work is theatre and every business a stage._ Cambridge, MA: Harvard Business School Press.
*   Sarstedt, M., Henseler, J. & Ringle, C.M. (2011). Multi-group analysis in partial least squares (PLS) path modelling: alternative methods and empirical results. In M. Sarstedt, M. Schwaiger, C.R. Taylor (Eds.), _Measurement and research methods in international marketing_ (pp. 195-218). Bingley, UK: Emerald. (Advances in international marketing, volume 22)
*   Savolainen, R. (2007). Filtering and withdrawing: strategies for coping with information overload in everyday contexts. _Journal of Information Science, 33_(5), 611-621.
*   Saxena, D. & Lamest, M. (2018). Information overload and coping strategies in the big data context: evidence from the hospitality sector. _Journal of Information Science, 44_(3), 287-297.
*   Schwartz, B., Ward, A., Monterosso, J., Lyubomirsky, S., White, K. & Lehman, D.R. (2002). Maximizing versus satisficing: happiness is a matter of choice. _Journal of Personality and Social Psychology, 83_(5), 1178-1197.
*   Shang, R.A., Chen, Y.C. & Chen, S.Y. (2013). The effects of the number of alternative products and the way they are presented on the consumers’ subjective statuses in online travel sites. _Behaviour & Information Technology, 32_(7), 630-643.
*   Sproles, G.B. & Kendall, E. (1986). A methodology for profiling consumers’ decision-making styles. _Journal of Consumer Affairs, 20_(2), 267-279.
*   Stanton, J.V. & Paolo, D.M. (2012). Information overload in the context of apparel: effects on confidence, shopper orientation and leadership. _Journal of Fashion Marketing and Management, 16_(4), 454-476.
*   Swar, B., Hameed, T. & Reychav, I. (2017). Information overload, psychological ill-being, and behavioral intention to continue online healthcare information search. _Computers in Human Behavior, 70_, 416-425.
*   Thorson, E., Reeves, B. & Schleuder, J. (1985). Message complexity and attention to television. _Communication Research, 12_(4), 427-454.
*   Tseng, A. (2017). Why do online tourists need sellers' ratings? Exploration of the factors affecting regretful tourist e-satisfaction. _Tourism Management, 59_, 413-424.
*   Tsiros, M. & Mittal, V. (2000). Regret: a model of its antecedents and consequences in consumer decision making. _Journal of Consumer Research, 26_(4), 401-17.
*   Turnbull, P.W., Leek, S. & Ying, G. (2000). Customer confusion: the mobile phone market. _Journal of Marketing Management, 16_(1-3), 143-163.
*   Urbach, N. & Ahlemann, F. (2010). Structural equation modelling in information systems research using partial least squares. _Journal of Information Technology Theory and Application, 11_(2), 5-40.
*   Voase, R. (2018). Holidays under the hegemony of hyper-connectivity: getting away, but unable to escape? _Leisure Studies, 37_(4), 384-395.
*   Walsh, G. & Mitchell, V-W. (2010). The effect of consumer confusion proneness on word of mouth, trust, and customer satisfaction. _European Journal of Marketing, 44_(6), 838-859.
*   Walsh, G., Hennig-Thurau, T. & Mitchell, V-W. (2007). Consumer confusion proneness: scale development, validation, and application. _Journal of Marketing Management, 23_(7-8), 697-721.
*   Wang, Q. & Shukla, P. (2013). Linking sources of consumer confusion to decision satisfaction: the role of choice goals. _Psychology and Marketing, 30_(4), 295-304.
*   Yuksel, A. (2001). Managing customer satisfaction and retention: a case of tourist destinations, Turkey. _Journal of Vacation Marketing, 7_(2), 153-168.
*   Zeelenberg, M. (1999). Anticipated regret, expected feedback and behavioral decision making. _Journal of Behavioral Decision Making, 12_(2), 93-106.
*   Zeelenberg, M. & Pieters, R. (2007). A theory of regret regulation 1.0\. _Journal of Consumer Psychology, 17_(1), 3-18.
*   Zeelenberg, M., van Dijk, W.W., Manstead, A. & van der Pligt, J. (2000). On bad decisions and disconfirmed expectancies: the psychology of regret and disappointment. _Cognition and Emotion, 14_(4), 521-541.
*   Zhang, H., Fu, X., Cai, L. & Lu, L. (2014). Destination image and tourist loyalty: a meta-analysis. _Tourism Management, 40_, 213-223.

