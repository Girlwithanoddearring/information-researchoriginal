#### vol. 21 no. 3, September, 2016

# Subject positions of children in information behaviour research

## [Anna Hampson Lundh](#author)

#### Abstract

**Introduction**. This paper problematises how children are categorised as a specific user group within information behaviour research and discusses the implications of this categorisation.  
**Methods**. Two edited collections of papers on childrens information behaviour are analysed.  
**Analysis**. The analysis is influenced by previous discourse analytic studies of users within information science and by the sociology of childhood and the discourse analytic concept of subject positions guides the analysis.  
**Results**. In the children-focussed discourse of information behaviour research, children are described as being characterised by distinctive child-typical features, which means that similarities between children and other groups, as well as differences within the group, are downplayed. Children are also characterised by deficiencies: by not being adults, by not being mature and by not being competent information seekers. The discourse creates a position of power for adults, and for children a position as those in need of expert help. Children are also ascribed a subject position as users of technologies that affect the group in various ways.  
**Conclusions**. It is suggested that information behaviour research would benefit from shifting the focus from trying to explain how children innately are and therefore behave with information, to creating understandings of various information practices which involve people of a young age.

## Introduction

Within the research field of information behaviour (also known as, for example, information-seeking studies; information seeking behaviour; or information needs, seeking and use), the notion of categorising people in different user groups is fundamental. In the early history of information behaviour research, the focus was largely on different professional groups and researchers, and their interactions with information and information systems when carrying out work and research tasks. In more recent years, however, groups outside workplace and institutional settings have attracted greater interest. Two such groups are the elderly (e.g., [Asla, Williamson and Mills, 2006](#awm06)) and children and young people (e.g., [Large, 2005](#lrg05)). Whereas other groups of users studied in the field of information behaviour are regarded as distinct groups because of, for example, shared work duties (e.g., [Huvila, 2010](#hu10); [Pilerot, 2014](#pi14)) or common spare time interests (e.g., [Case, 2010](#ca10); [Hartel, 2010](#ha10)), the basis for the groupings of the elderly and children is that of similar age ([Case, 2012](#ca12), p. 349-355). Thus, within information behaviour research, old or young age is, explicitly or implicitly, seen as an important factor affecting information behaviour. In this paper, this assumption is problematised.

However, as Dervin highlighted in [1989](#der89), the division of people into user groups within information behaviour research might lead to the reproduction of static categories, leaving little room for questioning the very basis for the categorisations. In the present study, therefore, how children as a group are described, and thereby constructed, in information behaviour research will be examined and discussed. The motivation for the study is that the construction of children as a user group within the research field of information behaviour has implications for the knowledge production of this research field, as well as for the actual design and use of information systems and services for children and young people.

The effects of the ways in which children are described in information behaviour research can be said to be of three kinds: _'_discursive effects_', '_subjectification effects_'_ and _'_lived effects_'_ ([Bacchi, 2009](#ba09), p. 40). The discursive effects concern the knowledge claims regarding the user group of children that are produced through information behaviour research. Within any research field, there are conventional ways in which the field's objects of study are described. However, such conventions constitute _'_the limits imposed on what can be said or thought_'_ ([Bacchi, 2009](#ba09), p. 40) within the field. In information behaviour research, as in many other fields where children are studied, children are often seen as a given, natural and self-evident group. One implication of seeing young age as a determining factor for information behaviour is that other ways of categorising people who are young seem less evident. The very idea of problematising how and why children are categorised as a specific user group, as is done in this study, may therefore appear irrelevant and even absurd. This is, however, why such a problematisation is important.

Subjectification effects, which are the analytic focus of this study, are about the relationships created within the research field between children as a group and other groups of users. Children appear as a specific user group because they are contrasted with and positioned in relation to other groups (cf. [Bacchi, 2009](#ba09), p. 16). For example, children are often defined as people who are not yet adults. An implication of this type of positioning of children is that similarities between children and other groups, as well as differences within the group, are obscured. Another consequence is a focus on describing how children _are_ in general, rather than discussing, for example, the conditions for children's lives in different situations, historical contexts and societies.

Discursive and subjectification effects might sound quite abstract. However, they are always connected to lived effects, which are material and experienced effects in people's lives ([Bacchi, 2009](#ba09), p. 17-18). Thus, how the user group of children is described and regarded within information behaviour research, and within information science generally, has certain consequences: for the design of studies; for how research problems are formulated; for how actual research subjects are treated; and for how research outcomes are put into practice, for example through the design of information systems and services for children and young people.

In light of the above-mentioned understanding that the ways in which user groups are described have discursive, subjectification and lived effects, the aim of the present study is to create a better understanding of how the user group of children is constructed within information behaviour research. The study is guided by the following two research questions:

1.  How is the user group of children described in information behaviour research
2.  In relation to other groups, which subject positions are children ascribed in information behaviour research

The research questions will be addressed through an analysis of two edited books on children's information behaviour, published in 2004 and 2013\. The main focus of the analysis will be on the ways in which children are distinguished as a group of users and the subject positions of this group.

## Literature review

The study connects to a tradition of critical analysis of central concepts and ideas within information science based in discourse analytic theory. The analysis is influenced by previous studies of how users are discursively constructed (e.g., [Hedemark, Hedman and Sundin, 2005](#hhs05); [Rothbauer and Gooden, 2006](#rg06); [Tuominen, 1997](#tu97)), as well as a research tradition called the sociology of childhood (e.g., [James, Jenks and Prout, 1998](#jjp98); [Prout and James, 1997](#pj97)). These traditions, which both can be described as social constructionist (cf. [Talja, Tuominen and Savolainen, 2005](#tts05)), are introduced below.

### Discourse analyses of and in information science

Since at least the 1990s, basic assumptions within information science have been scrutinised through several Foucauldian inspired discourse analyses ([Olsson, 2010](#ol10)). The focus of such studies has been to describe how discourses within information science are produced from the positions of '_institutionally privileged speakers_' ([Frohmann, 1994](#fr94), p. 120), creating stances, viewpoints and concepts that become taken-for-granted within the discipline. Examples of such studies are Radford's ([2003](#rad03)) discussion on the discursive formations of librarianship, Frohmann's ([1992](#fr92)) critique of the cognitive viewpoint and Talja's ([1997](#ta97)) and Savolainen's ([2007](#sav07)) analyses of the information-seeking research field.

Other examples concentrate on the discursive construction of _users_. Frohmann, in ([1994](#fr94), suggested that such studies could challenge:

> the assumption that the identities studied - children, young adults, women, scientists, engineers, chief executive officers of corporations, academics in various disciplines, graduate students, undergraduates, social scientists, and many more - are "natural", or "found" identities- (Frohmann, 1994, p. 134).

One such study is that by Hedemark _et al._ ([2005](#hhs05)), which analyses statements on users in professional journals in the Swedish public library sector. Working with a type of discourse analysis developed by Laclau and Mouffe ([1985](#lm85)), Hedemark _et al._ ([2005](#hhs05)) identify four discourses, _'a general education discourse; a pedagogical discourse; an information technology discourse';_ and _'an information management discourse'_, and discuss what implications these different ways of _'speaking of users'_ might have for actual public library users. A similar analysis is conducted by Rothbauer and Gooden ([2006](#rg06)), in their paper on representations of children in studies published in _Journal of the American Society for Information Science and Technology_ between the years 1985 and 2005\. Their main conclusion is that children in information science research are described as developing and in need of _'training, education and more responsive information systems design'_ ([Rothbauer and Gooden, 2006](#rg06), p. 8).

A somewhat different discourse analytic approach from that of Hedemark _et al_. ([2005](#hhs05)) and Rothbauer and Gooden ([2006](#rg06)) is taken by Tuominen ([1997](#tu97)), in an analysis of the subject positions of librarians and users constructed in information behaviour studies, using Kuhlthau's well-known book _Seeking Meaning_ ([1993](#ku93)) as an example of _'user-centered discourse'_. Tuominen argues that, in this discourse, _'the user is in the position of a layperson, and the librarian functions as an expert who can diagnose the user's mental states and propose treatments on the basis of the diagnosis'_ ([Tuominen, 1997](#tu97), p. 362), creating relations similar to those between doctor and patient or, which is noteworthy in the context of the present study, that between adult and child.

Hence, these discourse analytic studies highlight power relations between users and librarians, as well as between research subjects and researchers. In the case of Rothbauer and Gooden, the possible power relations between young research participants and adult information science researchers is discussed with a basis in what they call _'social research with children'_ ([Rothbauer and Gooden, 2006](#rg06), p. 1). This research tradition is also known as the sociology of childhood, which will be discussed next.

### The sociology of childhood

The sociology of childhood is often paired with the epithet of _new_. However, as the research tradition now goes back at least a couple of decades and has been influential for the study of children in areas such as sociology, educational science and media and communication studies, the approach cannot any longer be said to be novel ([Prout, 2011](#pr11); [Tisdall and Punch, 2012](#tp12)). Information behaviour research and information science in general, however, seems so far to be quite unaffected by this research tradition ([Lundh, 2011](#lu11), pp. 14-15; [Rothbauer and Gooden, 2006](#rg06)).

A central notion in the sociology of childhood is that of childhood as a social construct. In an introduction to the field from its formative years, Prout and James clarify this notion by stating that _'[t]he immaturity of children is a biological fact of life but the ways in which this immaturity is understood and made meaningful is a fact of culture'_ ([Prout and James, 1997](#pj97), p. 7). This means that ideas of what childhood is and should be are not seen as natural and given, but as socially, historically and geographically situated ([James, Jenks and Prout, 1998](#jjp98)). It also means that the idea of children as a homogenous group is questioned and that factors other than age, such as gender, ethnicity and class, should be taken into account when analysing childhood ([Prout and James, 1997](#pj97)).

For researchers within the tradition of the sociology of childhood, rather than trying to explain how children innately _are_, an important task is to identify different understandings of childhood in different social contexts and historical eras and the consequences of these understandings. Just as in the discourse analytic studies of the constructions of users presented above, this tradition highlights the implications of socially constructed categorisations of people, in society in general and for the relationships between research subjects and researchers in particular. This approach, of bringing taken-for-granted categories into the light of scrutiny, is the foundation for the analysis in the present study.

## Theoretical and methodological framework

The rationale for this study is the assumption that the ways in which the research field of information behaviour describes and defines the user group of children have actual, practical consequences for this user group, as well as for researchers, professionals and institutions working with children and their information behaviour. In order to create a better understanding of how the user group of children is constructed within this field of research, the discourse analytic concept of subject positions guides the analysis of two edited books on children's information behaviour.

### Subject positions

In his paper from [1997](#tu97) mentioned above, Tuominen argues that there is a _'user-centered discourse'_ within information science and that this discourse creates different subject positions for users and librarians. In a similar vein, a basic assumption in the present study is that a children-focused discourse has been formed within information behaviour research during the past decades, producing certain subject positions (cf. [Hall, 2001](#hall01), p. 80; [LindskLindsköldld, 2013](#li13), pp. 59/276) for children in relation to other groups, such as adults in general and different professional groups, including information science researchers.

Hence, the analysis conducted is not an analysis of the perspectives of individual authors, but rather of what, how and from which subject positions something is expressed within a discourse (cf. [Foucault, 1972](#fou72), p. 222; [Hedemark, 2009](#he09), pp. 36/169; [Lindsköld, 2013](#li13), pp. 61/276). The texts analysed are thus seen as examples of a discourse on children in information behaviour research.

Hedemark ([2009](#he09), pp. 36/169, see also [Lindsköld, 2013](#li13), pp. 62) highlights that in analysed texts, there are different types of subject positions, depending on whether they are the positions of _those who speak or those who are being spoken about_. In Tuominen's ([1997](#tu97)) analysis, both users and librarians are spoken about in the text analysed. In the analysis of the present study, children and some groups of adults are spoken about, whereas the subject position of the information scientist is often that of those who speak.

### Material analysed and analytical approach

Two edited books have been chosen for analysis, as illustrative examples of children-focused discourse in information behaviour research. The first, published in 2004, is _Youth information-seeking behavior: theories, models, and issues_ ([Chelton and Cool, 2004](#cc04)); the second, published nine years later, is _The information behavior of a new generation: children and teens in the 21st century_ ([Beheshti and Large, 2013](#bl13)). A list of the twenty-seven chapters analysed is found in [Appendix 1](#app).

The reason for choosing these two edited books for analysis is that they constitute significant attempts to establish a new research area through compiling chapters that all are about various aspects of children's information behaviour, and through introductions and final chapters that summarise the area and suggests ways forward. As such, both books are cohesive and comprehensive resources to turn to for those who are new to the field. This attempt at establishing a field is especially explicit in the 2004 book, in which several chapters previously published as single journal papers are included. This also means that the book chapters span a considerable period of time (1996-2013, with references to studies conducted earlier). It is important to highlight that the two books, implicitly in the first and explicitly in the second, mainly cover North American contexts. Hence, the discourse analysed, as is the case with any discourse, is tied to a specific time and place. It should be noted that one edited book ([Beheshti and Bilal, 2014](#bb14)), which could have been the object of analysis, was excluded on the basis that a chapter proposal, presenting similar ideas as the present study, was submitted to and accepted for this book, but subsequently withdrawn by the author because of time constraints.

In the analysis, each of the twenty-seven chapters was carefully analysed with a focus on statements about children and subject positions created. A model and a set of questions guiding the close reading of the chapters were developed, based on the two research questions (see Table 1). The results of the analysis are presented in the next section.

<table class="center"><caption>  
Table 1: Model of analysis</caption>

<tbody>

<tr>

<th>1\. Statements about children</th>

</tr>

<tr>

<td>Questions asked:</td>

</tr>

<tr>

<td>How are children described Which nouns are used</td>

</tr>

<tr>

<td>What distinctive features are children ascribed What are children described as doing</td>

</tr>

<tr>

<th>2\. Subject positions</th>

</tr>

<tr>

<td>Questions asked:</td>

</tr>

<tr>

<td>How are children described in relation to other groups</td>

</tr>

</tbody>

</table>

## Analysis

The analysis procedure was guided by the two research questions and the following presentation is structured accordingly. Statements about children in the analysed texts are presented, concentrating on the terms used to describe the user group and descriptions of the distinctive features ascribed to this group (see Table 1). These terms and distinctive features are then linked to the positions of the user group in relation to other groups in the analysed texts. The analysed chapters are referenced using the numbers assigned in [Appendix 1](#app).

### Statements

#### Terms used

The titles of the books indicate that they are about the information (seeking) behaviour of _'youth', 'a new generation', 'children'_ and _'teens'_. However, in both books a large number of terms are used to describe the group of people in focus. It is also acknowledged in the introduction of each book that the terminology is not self-evident (1, p. vii; 16, p. vi). The analysis of the books below does, however, indicate a number of recurring ways of describing the group. A categorisation of terms into 16 groups is presented in the list below, with some examples of each.

1.  **General terms**; e.g., _children_; _kids_
2.  **Age- and development-based terms**; e.g., _12- to 17-year olds_; _adolescents_; _young people_
3.  **Education- and learning-related terms**; e.g., _learners_, _students_, _preschool children_
4.  **Terms for study participants**; e.g., _Bastian, a typical 14-year-old_; _participants_; _respondents_
5.  **Terms for types of users**; e.g., _novice users_; _users_; _young users_
6.  **Information-seeking related terms**; e.g., _novice searcher_; _school-aged information users_; _young information seekers_
7.  **Technology-related terms**; e.g., _digital age youth_; _online teens_; _participants (in online communities)_
8.  **Generational terms**; e.g., _digital natives_; _the new generation_; _post-Web school generation_
9.  **Design team-related terms**; e.g., _design partner_; _student members of the design team_; _young colleagues_
10.  **Ability-related terms**; e.g., _learning impaired and normal students_; _young people with intellectual disabilities_; _non-verbal children_
11.  **LGBT-related terms**; e.g., _"queer youth_; _young "queer individuals_; _gay and lesbian youth_
12.  **Demographic terms (other than age)**; e.g., _adolescent girls_; _boys_; _urban teens_
13.  **Non-children-specific terms**; e.g., _individuals_; _novices_; _person_
14.  **Group-related terms**; e.g., _friends_; _the norm group_; _peers_
15.  **Names of individuals (other than as study participants)**; e.g., _Eric_; _a young adult named David Abitbol_
16.  **Other terms**; e.g., _expert children_; _drug users_; _victim_

The first category of terms includes inflections of _child_ and _kid_, the former widely used in both books. These are general terms, highlighting that this group is seen as both having something in common and being different from other groups. The second category, which includes a variety of terms, points toward age as something that distinguishes this group from others, but also to the idea that the group goes through stages of development, such as adolescence. The prevalence of terms relating to educational institutions and learning in general (category 3) highlights the role of this group of people as students at different levels in an educational system.

Categories 4-9 all relate to specific interests within the research field. Given that the analysed books are about research on a user group and its information behaviour, the use of terms such as _participants_ or _respondents_ (category 4) and various formulations including _users_ (category 5) and _information seekers_ (category 6) is perhaps not very surprising. Category 7 terms all relate to the use of modern and digital technologies, where young people are described as, for example, _online teens_ and _digital age youth_. This category is also connected to category 8 which describes children and young people as belonging to a _new generation_ which in many cases is described in relation to digital media and the Web. These terms are more common in the later book. It should, however, be noted that different authors use terms such as _digital natives_ from various perspectives, some being reluctant towards and critical of the use of the term (e.g., [22](#22), pp. 137-138), whereas others use it unproblematically to describe typical features of young people in the beginning of the 21th century (e.g., [23](#23), pp. 143-144, 148, see also [16](#16), p. vi). One category, number 9, is only found in the book from 2013 and relates to a specific research approach, namely that of participatory design processes ([21](#21), pp. 96-97; [26](#26), pp. 221-222, 230).

Three categories, 10-12, all relate to differences within the group based on sexuality or gender identity, (dis)ability and demographics other than age. It is interesting to note that categories 10 and 11, which both include a small number of terms, are about that which is described as unusual and thereby highlight what can be perceived as the norm (e.g., heterosexual, cissexual, able-bodied, able-minded). The demographic differences included in category 12 are gender, socio-economic status and, in a few cases, nationality and ethnicity.

The terms in category 13 are all understood as denoting children only because of the contexts in which they are used, as for example, _individuals_, _novices_ and _person_, which are not child-specific terms. This is also the case for many of the group-related terms of category 14, which include, for example, _friends_ and _peers,_. The two last categories (categories 15-16) are related to particular chapters in which the terminology used is quite specific. For example, one chapter ([25](#25)), refers to legal cases where individual names and terms such as _victim_ are used.

Many of the terms used in the books implicate actions and activities which the user group is described as being involved in, such as: going through developmental stages; attending school; seeking information; using technologies; being part of court cases; and participating in research studies. These and many other actions and activities are described in the books. What is interesting is that many of these activities are described as typical for the user group; hence, the focus lies on how children typically _are_ and how they do things in child-characteristic ways. Discerning a group of people in this way suggests that members of this group have something in common that distinguishes them from other groups. In the analysed texts, this discursive segregation of the group of children is both implied and explicit.

#### Distinctive features

One example of the idea of children as a particular group of people that differs from other groups is found in a chapter presenting research on children's cognitive development and its usefulness for the field of information behaviour ([18](#18)). Characteristic for research on cognitive development is a focus on _'age differences'_, _'age comparisons'_ and _'age trends'_ ([18](#18), p. 23) which is claimed to be little researched within information behaviour research and therefore encouraged ([18](#18), pp. 40-41). Three groups discerned throughout the chapter are _'children, adolescents, and adults'_ ([18](#18), pp. 28, 39, 40, see also 36, 38). It is acknowledged that classical theories within the field, which state that levels of understanding are _'qualitatively distinct'_ in different age groups, have been nuanced ([18](#18), p. 30). Nevertheless, an overriding interest in predicting differences depending on age and levels of expertise remains.

Classical theories of cognitive development are referred to in some of the other analysed chapters. One example is number 8, which opens with the following statement:

> Literature tells us that children think differently than adults. Professionals who work with children concur with this information. It should follow, then, that the information interests and needs of children are different than those of adults. This, too, has been addressed in research. ([8](#8), p. 181)

This chapter presents classical developmental psychology as its theoretical foundation and concludes that _'children at different developmental levels'_ in the empirical research reported _'did have unique information interests'_ ([8](#22), p. 205). That the group _children_ have distinctive features that affect their information behaviour is thus very clear in this example.

All statements are not as articulated in terms of theoretical foundations, but do still highlight distinctive features of the user group. One example of a description of what is typical of children is the following:

> digital natives do not distinguish greatly between their physical and virtual realities. They are growing up in a world submerged in technologies  a world where they are comfortable and at ease. ([25](#25), p. 203)

An interesting aspect of this quote is the relationship between children and new digital technology. Both books discuss the impact changing technologies have on children's information behaviour. In the quote above, children at the beginning of the 21st century are described as _'at ease'_ with modern technologies. Another view, which also builds on statements on the distinctive features of children, can be found, for example, in a description of a study on children's information searching using manual card catalogues and online catalogues (OPACs), where younger children are described as not being mature enough for one type of digital technology:

> Fourth graders are believed to be "developmentally unready" for the type of searching involved in manipulation of the OPAC. ([2](#2), p. 12)

However, both approaches to children and technologies - where children are seen as either competent, or immature - build on descriptions of children as being a certain kind of user group. The relationship between this group and new technologies will be further discussed in terms of subject positions in the next section.

There are ample examples of statements regarding the distinctive features of the user group of children in both books. It would, however, be an oversimplification to claim that the user group is described as completely homogeneous in the analysed texts. As observed in the list above, some of the categories of terms used for children imply that there are differences within the group, for example in terms of gender and socio-economic status. There are also examples of studies reporting on different search-styles within the group of children (e.g., [10](#10)). On the penultimate page of the 2004 book, in a discussion of future directions for the research field, it is acknowledged that:

> The category of "child" or "youth" is a social construction. What are the boundaries of "youth" We have discussed developmental stages, but there are legal and regulatory issues involved here as well. ([15](#15), p. 389)

This quotation could indicate a movement towards a quite different understanding than that of children as a group mainly characterised by particular ages and stages. At least, it is an acknowledgement that the perspective adopted in the book is only one of many. It should be noted, though, that this example stands out as being unusual in both of the analysed books. The sociology of childhood is mentioned once in the 2013 book ([21](#21), p. 98), when a previous study on children's use of digital media is outlined. This perspective does not, however, permeate the discourse on children's information behaviour in the analysed literature.

### Subject positions

The analysis thus far suggests that children are portrayed as a distinct group characterised by particular features, in both books. This is further emphasised by statements in which children are described as different from other groups. There are many examples of children being described as dissimilar to adults in the texts, such as the following:

> Rowlands and his partners (2008) conclude that adults and youth behave similarly when seeking information; that "we are all the Google generation" (308)... However, as many researchers have indicated, there are differences between adults and children, even though both groups may use similar tools in their information behavior. ([27](#27), p. 237)

In this quote, some similarities between the groups are actually acknowledged; but the argument still results in a perpetuation of the notion of adults and children as different. Another particularly clear example of a statement where the differences are emphasised is the following:

> Children are information seekers with needs and development characteristics that vary from those of adults. Cognitive developmental ability, memory and recall levels, emotional, social, and physical developments are factors that influence children's interaction with various information-retrieval systems, including the Web. Children are not 'small adults' but an entirely different user population with their own culture, norms, complexities, curiosities, interests, abilities, and information needs. Therefore, researchers need to develop a good understanding of and sensitivity toward the needs of these young users when they involve them in research projects. ([11](#11), p. 285)

In this statement, children are described as an _'entirely'_ separate group of users from that of adults. The statement resembles that of the approach of _'the tribal child'_ ([James, Jenks and Prout, 1998](#jjp98), pp. 28-30), in which children are seen as belonging to social worlds of their own. However, what this approach conceals are similarities between children and adults, as well as differences within the group of children. A consequence of this approach is how the need for a particular _'understanding'_ and _'sensitivity'_ from researchers towards the group of children also means that the perceived differences between the groups are in fact maintained. This is also the consequence of statements such as _'adults and youth are partners in the digital world'_ ([21](#21), p. 94).

Thus, it appears as though children and adults are ascribed different subject positions in the analysed statements. The relationships between the groups are, however, described in different terms, depending on the roles of the adults, for example as parents, teachers, librarians or researchers. When parents are mentioned, the relationship is rarely problematised: parents have the position of those parenting (e.g., [26](#26), p. 215) and children are described as belonging to or being members of families (e.g., [23](#23), p. 146).

In relation to educators and librarians, children are often described as those being taught, for example in terms of information literacy:

> The research highlights the need to teach basic information literacy skills at an early age so that bad habits never form; furthermore, the students can build upon these basic skills over the course of their K-12 education so that they become second nature. ([19](#19), p. 54)

In the above statement, emphasis is put on children and youth in the roles of being students and the need for them to develop in a desired direction. Elsewhere, the group is also described as being in need of help from educators and librarians to become better information seekers:

> The novice searcher's need for some form of mediated assistance in analyzing research questions and in constructing effective search statements is evident. As information specialists and curriculum consultants in a school setting, library media specialists must be able to analyze the problems that students encounter in their information searches and devise methods to increase students' proficiency in information retrieval. ([6](#6), p. 125)

The role of analysing or diagnosing (cf. [Tuominen, 1997](#tu97)) children's problems and helping them on their learning trajectories highlights educators' and librarians' positions as experts and children as students and learners. There are few exceptions to these described subject positions of teachers/librarians and children in the two books. Another group which holds the position of the expert helper is the group of system designers:

> System designers, librarians, and teachers can all provide assistance to children as they embark on research in unfamiliar areas and can contribute to the design of information-retrieval systems that will support children's information needs and educational pursuits in an information society. ([10](#10), p. 267)

The role of the system designer is to design systems that suit children's specific needs (cf. [Rothbauer and Gooden, 2006](#rg06), p. 5). Sometimes, the system designer also has the role of the information science researcher for whom children are research subjects. In some chapters in the later book with a specific focus on information systems design, the positions of children in design teams are described as that of a partner and even an expert. An example is the following description of a type of design methodology:

> The new methodology was called Bonded Design, because of a natural bond between adults as design experts and children as experts on being children. (26, pp. 220-221)

What this statement suggests, as in the quote above on adults and youth as partners ([21](#21), p. 94), is that adults and children can be members of the same team and children can even be experts, at least _'on being children'_. However, they are still described as two distinct entities requiring a _'bond'_. So adults and children may be partners, but the latter are still in need of specialised information system solutions and help from the former.

Children's position as the recipients of and partners in information system design also touches upon or coincides with the position of the research subject. As the analysed books present and synthesise original studies and previous research on various aspects of children's information behaviour, this subject position could be expected. It does, however, reinforce the division into an _us_ as adults and a _them_ as children. This is not least the case when the position of the reader is included in the same _we_ as the author position, as for example in the title _'Youth and online social networking: what do we know so far'_ ([22](#22), p. 117).

In the previous section, one relationship that was touched upon was that between children and (new) technologies, which is a concern in both books. Technologies are ascribed important roles in the lives of children. In some cases children are described as a group with stable characteristics, while technologies are changing:

 Whether or not we see young people as Digital Natives, they are still young people who are grappling with the developmental issues encountered by all youth as they grow to be adults in our modern technologically dependent society. They encounter the same issues that young people have always dealt with, but now they have more sources and channels of information in which to find help with their everyday life information needs. ([20](#20), p. 86)

This is sometimes described as a problem for educational institutions, as children might be experts on the technologies, but lacking other types of expertise which are said to be important for successful information behaviour (e.g., [7](#7), p. 176). In other cases, children are described as a group whose characteristics change because of technologies, which may also create problems for educational institutions:

> The fact is that students have changed drastically as a result of early attachment to technology, thus effectively creating a serious problem for the education system. ([23](#23), p. 147)

In either case, with few exceptions, technologies are described as a force which is having effects on children and their lives, rather than the other way around. Thus, the subject positions of children in relation to technology, are that of a user (and in some cases a non-user), rather than a master.

### Discussion and conclusion

In summary, the subject positions of users identified by Tuominen in [1997](#tu97), as well as the discourses on children in information science described by Rothbauer and Gooden in [2006](#rg06), still seem to be in play in the later literature analysed in this study. In the children-focused discourse of information behaviour research, children are described as a group with distinctive child-typical features. Even though differences within the group are acknowledged, the differences between this group and that of adults are emphasised. The user group of children is characterised by deficiencies: by _not_ being adults, by _not_ being mature and by _not_ being competent information seekers. What possible discursive, subjectification and lived effects might these descriptions have?

The discourse of children in information behaviour research, which includes the description of children as being in the process of developing and maturing and as being affected by the force of technologies, tends to gloss over that the ways in which children as a group are described and understood are _social_ rather than naturally given descriptions and understandings. Thus, the discourse of children's information behaviour does echo a more general discourse of information behaviour, which previously has been analysed by Savolainen ([2007](#sav07)). The description of information needs, seeking and use as individual cognitive processes, rather than material and collective practices, means that the _social_ is treated as a factor, rather than as an inherent aspect of human life. The aim of describing how children essentially _are_ also has the effect that the different childhoods that actual children live (cf. [Prout and James, 1997](#pj97), p. 8), as within a geographical context such as North America, are overlooked.

Depending on the roles of adults, children are ascribed different subject positions: as members of families where the adults are parents; as students and learners who are developing and are in need of education and help from teachers and librarians; and as in need of specific solutions designed by system designers, albeit sometimes with the involvement of the children themselves. By describing adults such as teachers, librarians and system designers as a group that helps children creates a position of power for adults and a position as those in need of expert help for children. Furthermore, children are ascribed a subject position as users of technologies that - as some kind of external force - affect the group in various ways.

Based on the analysis conducted in this study, the lived effects of the children-focused discourse in information behaviour research can only be speculated about. However, there seems to be room in the field for creating alternate ways of doing research about and with children. One conclusion is that in order to allow for understandings of children as social beings and childhoods as socially constructed in the field of information behaviour, the field needs to open up to influences from the new sociology of childhood, as well as from other, socially oriented theories of information practices. A way forward for the field is to shift the focus from trying to explain how children in general _are_ and therefore behave with information, to creating understandings of various information practices which involve people of young age. Such a theoretical shift would allow information science to create more nuanced and less static descriptions of and knowledge about children and the conditions within which they perform their information activities.

## Acknowledgements

This study has been funded by the Curtin Research Fellowship, Curtin University, Perth, Australia. The author is a member of the Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS) at the University of Gothenburg and the University of ås, Sweden, funded by the Swedish Research Council, ref 349-2006-146.

The author would like to thank Mats Dolatkhah, Linnéa Lindsköld, Amira Sofie Sandin and the research seminar at the Swedish School of Library and Information Science, as well as Brad Gobby at Curtin University for their valuable input and support during the writing of this paper; and Bradley Smith at Semiosmith Editing and Consulting Services for his help with the English editing of the manuscript.

## About the author

**Anna Hampson Lundh**, PhD, is Senior Research Fellow at the Department of Information Studies, Curtin University, Bentley, Perth, Western Australia 6102, and Senior Lecturer at the Swedish School of Library and Information Science, University of Borås, Allågatan 1, 501 90, Borås, Sweden. Her research interests concern children's reading and documentary practices, and accessible media. She can be contacted at [anna.hampsonlundh@curtin.edu.au](mailto:anna.hampsonlundh@curtin.edu.au) or [anna.lundh@hb.se](mailto:anna.lundh@hb.se)

#### References

*   (Please note that the references to the books in the dataset are listed in [Appendix 1](#app))  

*   Asla, T., Williamson, C. M. C. & Mills, J. (2006). The role of information in successful aging: the case for a research focus on the oldest old. _Library & Information Science Research, 28_(1), 49-63.
*   Bacchi, C. L. (2009). _Analysing policy: what's the problem represented to be_ Frenchs Forest, NSW: Pearson.
*   Beheshti, J. & Bilal, D. (Eds.). (2014). _New directions in children's and adolescents' information behavior research_. Bingley, UK: Emerald.
*   Beheshti, J. & Large, A. (Eds.). (2013). _The information behavior of a new generation: children and teens in the 21st century_. Lanham, MD: Scarecrow Press.
*   Case, D. O. (2010). [A model of the information seeking and decision making of online coin buyers](http://www.webcitation.org/6bg26WS2g). _Information Research, 15_(4), paper 448\. Retrieved from http://InformationR.net/ir/15-4/paper448.html (Archived by WebCite® at http://www.webcitation.org/6bg26WS2g).
*   Case, D. O. (2012). _Looking for information: a survey of research on information seeking, needs, and behaviour_ (3rd expanded ed.). Bingley, UK: Emerald.
*   Chelton, M. K. & Cool, C. (Eds.). (2004). _Youth information-seeking behavior: theories, models, and issues_. Lanham, MD: Scarecrow Press.
*   Dervin, B. (1989). Users as research inventions: how research categories perpetuate inequities. _Journal of Communication, 39_(3), 216-232.
*   Foucault, M. (1972). _The archaeology of knowledge and the discourse on language_. New York, NY: Pantheon Books.
*   Frohmann, B. (1992). The power of images: a discourse analysis of the cognitive viewpoint. _Journal of Documentation, 48_(4), 365-86.
*   Frohmann, B. (1994). Discourse analysis as a research method in library and information science. _Library and Information Science Research, 16_(2), 119-138.
*   Hall, S. (2001). Foucault: power, knowledge and discourse. In M. Wetherell, S. Taylor & S.J. Yates (Eds.), _Discourse theory and practice: a reader_ (pp. 72-81). London: Sage.
*   Hartel, J. (2010). Managing documents at home for serious leisure: a case study of the hobby of gourmet cooking. _Journal of Documentation, 66_(6), 847-874.
*   Hedemark, Å. (2009). _Det föreställda folkbiblioteket: en diskursanalytisk studie av biblioteksdebatter i svenska medier 1970-2006_. [The imagined public library: a study of library debates in the Swedish press between 1970 and 2006 using discourse analysis.] Uppsala, Sweden: Uppsala University, Department of ABM. (Doctoral theses, Volume 3).
*   Hedemark, Å., Hedman, J. & Sundin, O. (2005). [Speaking of users: on user discourses in the field of public libraries](http://www.webcitation.org/6bg2FZvAT). _Information Research, 10_(2), paper 218\. Retrieved from http://InformationR.net/ir/10-2/paper218.html (Archived by WebCite® at http://www.webcitation.org/6bg2FZvAT).
*   Huvila, I. (2010). Information sources and perceived success in corporate finance. _Journal of the American Society for Information Science and Technology, 61_(11), 2219-2229.
*   James, A., Jenks, C. & Prout, A. (1998). _Theorizing childhood_. Cambridge: Polity Press.
*   Kuhlthau, C. C. (1993) _Seeking meaning: a process approach to library and information services_. Norwood, NJ: Ablex.
*   Laclau, E. & Mouffe, C. (1985). _Hegemony & socialist strategy: towards a radical democratic politics_. London: Verso.
*   Large, A. (2005). Children, teenagers, and the Web. _Annual Review of Information Science and Technology, 39_, 347-392.
*   Lindsköld, L. (2013). _Betydelsen av kvalitet: en studie av diskursen om statens stöd till ny, svensk skönlitteratur 1975-2009_. [The meaning of quality: a study on the discourse of the state support to new, Swedish fiction 1975-2009.] s, Sweden: Valfrid. (Doctoral thesis, University of Gothenburg, Gothenburg, Sweden).
*   Lundh, A. (2011). _Doing research in primary school: information activities in project-based learning_. ås, Sweden: Valfrid. (Doctoral thesis, University of Gothenburg, Gothenburg, Sweden).
*   Olsson, M. (2010). Michel Foucault: discourse, power/knowledge, and the battle for truth. In G.J. Leckie, L.M. Given & J. Buschman (Eds.). _Critical theory for library and information science: exploring the social from across the disciplines_ (pp. 63-74). Santa Barbara, CA: Libraries Unlimited.
*   Pilerot, O. (2014). Making design researchers' information sharing visible through material objects. _Journal of the Association for Information Science and Technology, 65_(10), 2006-2016.
*   Prout, A. (2011). Taking a step away from modernity: reconsidering the new sociology of childhood. _Global Studies of Childhood, 1_(1), 4-14.
*   Prout, A. & James, A. (1997). A new paradigm for the sociology of childhood: Provenance, promise and problems. In A. James & A. Prout (Eds.). _Constructing and reconstructing childhood: contemporary issues in the sociological study of childhood_ (2nd rev. and updated ed.) (pp. 7-33). London: Falmer.
*   Radford, G.P. (2003). Trapped in our own discursive formations: toward an archaeology of library and information science. _Library Quarterly, 73_(1), 1-18.
*   Rothbauer, P. M. & Gooden. R. (2006). Representations of young people in information science: the case of the Journal of the American Society for Information Science (and Technology), 1985-2005\. In H. Moukdad (Ed.), _Information science revisited: approaches to innovation: proceedings of CAIS_ (pp. 1-11). Toronto, ON: York University.
*   Savolainen, R. (2007). Information behavior and information practice: reviewing the 'umbrella concepts' of information seeking studies. _The Library Quarterly, 77_(2), 109-132.
*   Talja, S. (1997). Constituting 'information' and 'user' as research objects: a theory of knowledge formations as an alternative to the information man theory. In P. Vakkari, R. Savolainen, A. Malmsjö, L. Limberg & B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts 14-16 August, 1996, Tampere, Finland_ (pp. 67-80). London: Taylor Graham.
*   Talja, S., Tuominen, K. & Savolainen, R. (2005). 'Isms' in information science: constructivism, collectivism and constructionism. _Journal of Documentation, 61_(1), 79-91.
*   Tisdall, E.K.M. & Punch, S. (2012). Not so 'new': Looking critically at childhood studies. _Children's Geographies, 10_(3), 249-264.
*   Tuominen, K. (1997). User-centered discourse: an analysis of the subject positions of the user and the librarian. _Library Quarterly, 67_(4), 350-371.

## Appendices

### Appendix 1\. List of analysed book chapters

1.  Chelton, M. K. & Cool, C. (2004). Introduction. In M.K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. vii-xiii). Lanham, MD: Scarecrow Press.
2.  Cool, C. (2004). Information-seeking behaviors of children using electronic information services during the early years. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 1-35). Lanham, MD: Scarecrow Press.
3.  Kuhlthau, C. C. (2004). Student learning in the library: what Library Power librarians say. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 37-63). Lanham, MD: Scarecrow Press.
4.  Neuman, D. (2004). Learning and the digital library. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 65-94). Lanham, MD: Scarecrow Press.
5.  McGregor, J. H. & Streitenberger, D. (2004). Do scribes learn: copying and information use. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 95-118). Lanham, MD: Scarecrow Press.
6.  Nahl, D. & Harada, V. H. (2004). Composing Boolean search statements: self-confidence, concept analysis, search logic, and errors. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 119-144). Lanham, MD: Scarecrow Press.
7.  Watson, J. S. (2004). "If you don't have it, you can't find it": a close look at students' perceptions of using technology. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 145-180). Lanham, MD: Scarecrow Press.
8.  Cooper, L.Z. (2004). Children's information choices for inclusion in a hypothetical child-constructed library. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 181-210). Lanham, MD: Scarecrow Press.
9.  Gross, M. (2004). Children's information seeking at school: findings from a qualitative study. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 211-240). Lanham, MD: Scarecrow Press.
10.  Hirsh, S. G. (2004). Domain knowledge and children's search behavior. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 241-270). Lanham, MD: Scarecrow Press.
11.  Bilal, D. (2004). Research on children's information seeking on the web. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 271-291). Lanham, MD: Scarecrow Press.
12.  Large, A. (2004). Information seeking on the web by elementary school students. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 293-319). Lanham, MD: Scarecrow Press.
13.  Julien, H. (2004). Adolescent decision making for careers: an exploration of information behavior. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 321-352). Lanham, MD: Scarecrow Press.
14.  Todd, R. J. & Edwards, S. (2004). Adolescents' information seeking and utilization in relation to drugs. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 353-386). Lanham, MD: Scarecrow Press.
15.  Chelton, M. K. (2004). Future direction and bibliography. In M. K. Chelton & C. Cool (Eds.), _Youth information-seeking behavior: theories, models, and issues_ (pp. 387-397). Lanham, MD: Scarecrow Press.
16.  Large, A. & Beheshti, J. (2013). Introduction. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. v-x). Lanham, MD: Scarecrow Press.
17.  Cole, C. (2013). Concepts, propositions, models, and theories in information behavior research. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 1-22). Lanham, MD: Scarecrow Press.
18.  Byrnes, J. P. & Bernacki, M. L. (2013). Cognitive development and information behavior. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 23-43). Lanham, MD: Scarecrow Press.
19.  Bowler, L. & Nesset, V. (2013). Information literacy. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 45-63). Lanham, MD: Scarecrow Press.
20.  Abbas, J. & Agosto, D. E. (2013). Everyday life information behavior of young people. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 65-91). Lanham, MD: Scarecrow Press.
21.  Dresang, E. T. (2013). Digital age libraries and youth: learning labs, literacy leaders, radical resources. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 93-116). Lanham, MD: Scarecrow Press.
22.  Agosto, D. E. & Abbas, J. (2013). Youth and online social networking: what do we know so far In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 117-141). Lanham, MD: Scarecrow Press.
23.  Vincenti, G. (2013). Gaming and virtual environments. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 143-165). Lanham, MD: Scarecrow Press.
24.  Hanson-Baldauf, D. (2013). Everyday life information in support of enhanced quality of life for young adults with intellectual disabilities. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 167-194). Lanham, MD: Scarecrow Press.
25.  Shariff, S. (2013). Defining the line on cyber-bullying: how youth encounter and distribute demeaning information. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 195-211). Lanham, MD: Scarecrow Press.
26.  Beheshti, J. & Large, A. (2013). Systems. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 213-236). Lanham, MD: Scarecrow Press.
27.  Beheshti, J. & Large, A. (2013). The future. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 237-242). Lanham, MD: Scarecrow Press.