#### Vol. 11 No. 1, October, 2005



# Variables associated with environmental scanning among clinicians at substance abuse treatment clinics

#### [Alison L. Koch](mailto:akoch@med.wayne.edu), Cynthia L. Arfken, and Elizabeth Agius  
Department of Psychiatry and Behavioral Neurosciences  
Wayne State University, Detroit MI, USA  

Marcus W. Dickson and Jacqueline K. Mitchelson  
Department of Psychology, Wayne State University, Detroit MI, USA  



#### Abstract

> **Introduction**: Environmental scanning, as a component of absorptive capacity, has been shown to be associated with increased use of innovative treatment techniques at substance abuse treatment programmes. As the transfer of innovative, evidence-based treatment techniques from research to practice is gaining attention, we aimed to identify variables associated with higher levels of environmental scanning among substance abuse treatment clinicians.  
> **Method**: A cross-sectional survey was administered to 162 clinicians at fifteen substance abuse treatment clinics in Michigan, which were selected based upon treatment modality, population served, geographic location, and affiliation with the Clinical Trials Network.  
> **Measures**: Environmental scanning was measured by frequency of use of the Internet, journals, seminars or conferences, and people at other treatment clinics for new substance abuse treatment information. Clinicians were asked for their perceptions of their clinic's openness to new treatment techniques and support for acquiring new information, access to and satisfaction with information sources at work, as well as if they feel it is their job to keep up to date with current treatment research. Additional measures included whether they intended to quit their jobs and whether they were emotionally drained from work.  
> **Findings**: We found positive associations between environmental scanning and perceived clinic support for acquiring new information, perceived clinic openness to new treatment techniques, access to e-mail and Internet at work, and satisfaction with resources. Turnover intention and being emotionally drained were negatively associated with environmental scanning.  
> **Conclusions**: Individual and organizational level variables were found to be associated with higher levels of environmental scanning activity. Although the causal directions of these associations are not known, the findings suggest ways to increase environmental scanning among clinicians.



## Introduction

Recent research in substance abuse treatment has developed several new pharmacological and behavioural treatments, but the slow implementation of these evidence-based treatments into clinical practice has been cause for concern ([Lamb _et al._ 1998](#Lamb)). Multiple studies have tried to address this issue by examining the transfer of specific innovative treatment techniques ([Sorensen _et al._ 1988](#Sorenson); [Martin _et al._ 1998](#Martin); [Thomas _et al._ 2003](#Thomas)) or by exploring clinicians' attitudes and beliefs about treatment techniques ([Arfken _et al._ 2005](#Arfken2); [Ball _et al._ 2002](#Ball); [Forman _et al._ 2001](#Forman)). Less emphasized have been the organizational characteristics that facilitate implementation of evidence-based treatments (e.g., [Roman & Johnson 2002](#Roman)). Knudsen & Roman ([2004](#Knudsen04)) were among the first in substance abuse treatment research to investigate absorptive capacity, an organizational characteristic defined as the ability to access and properly utilize novel information ([Cohen & Levinthal 1990](#Cohen); [Zahra & George 2002](#Zahra)). Knudsen and Roman found higher absorptive capacity to be associated with higher organizational use of innovative treatment techniques. One component of absorptive capacity, environmental scanning, had an independent positive effect on innovation adoption. Building from this prior literature, the objective of this study was to determine variables that lead to higher levels of environmental scanning among clinicians in substance abuse treatment programmes. This issue merits attention as efforts to increase environmental scanning may lead to faster implementation of new evidence-based treatment techniques.

### Environmental scanning

Environmental scanning, a concept first proposed by Aguilar ([1967](#Aguilar)), refers to the process of acquiring information from the outside world. Although contextualized in different ways, the purpose of environmental scanning is to gather new information from the external environment with the intent to incorporate the information into the organizational knowledge base ([Damanpour 1991](#Damanpour); [Cohen & Levinthal 1990](#Cohen); [Choo 1994](#Choo)). It differs from simple information-seeking in that it refers not only to the search for information but also to the exposure to information without a specific purpose ([Aguilar 1967](#Aguilar)). Engaging in environmental scanning allows people to become aware of and interpret changes taking place in the external environment ([Choo 1994](#Choo)), as well as acquire information that may lead to future decisions about the direction of their organization ([Aguilar 1967](#Aguilar); [Zahra & George 2002](#Zahra)).

### Factors influencing environmental scanning

Unfortunately, few studies have looked at the factors that can influence environmental scanning. In one notable exception, Correia and Wilson ([2001](#Correia)) qualitatively found four factors influencing the activity of environmental scanning. The first factor, information consciousness, refers to the attitudes towards information gathering activities, including feelings of personal responsibility. The second factor, exposure to information, refers to the amount of opportunities to make contact with information-rich resources such as knowledgeable people or publications. The third and fourth factors, information climate and organizational outwardness, refer to organizational concepts. They encompass the openness of an organization, its permeability to external information sources, and the conditions of the organization that influence access to and use of informational sources. Other studies have also demonstrated how the accessibility of and satisfaction with a resource can increase its frequency of use during the information gathering process ([Gertsberger & Allen 1968](#Gertsberge); [Herring 2001](#Herring)).

These four factors are also believed to be interrelated with each other. For instance, Correia and Wilson ([2001](#Correia)) proposed that the more open the organization is to its environment, the more likely it is that individuals in the organization will be exposed to information and develop an information consciousness. If this occurs, an information climate will develop that supports the individual's efforts in environmental scanning ([Correia & Wilson 2001](#Correia)). Within substance abuse treatment, if clinicians believe that their efforts to engage in environmental scanning are not only supported but rewarded, they should be more likely to do so ([Schneider _et al._ 1996](#Schneider)). This assumes, however, they are motivated to engage in environmental scanning. The emotional stress of a job may have a negative effect on motivation (e.g., [Spector & Fox 2005](#Spector); [Pugliesi 1999](#Pugliesi)) as would intention to quit ([Knudsen _et al._ 2003](#Knudsen03)).

### Proposed model

Building on the literature reviewed, we hypothesized that positive associations would exist between environmental scanning and: a) perception of a supportive work environment for acquiring new information; b) perception of clinic openness to new treatment techniques; c) access to computers, e-mail, and Internet at work; d) satisfaction with resources at work; and e) the view that is part of the clinician's job to keep up with current substance abuse treatment information. We also hypothesized that environmental scanning would be negatively associated with clinicians' intentions to quit and being emotionally drained.

We also explored the possibility of an association between the level of education of the clinician and environmental scanning. The literature suggests that clinicians with advanced degrees may be more inclined to scan the environment on the assumption that they have a broader knowledge base ([Cohen & Levinthal 1990](#Cohen); [Damanpour 1991](#Damanpour)) or may possibly be more inclined to use sources such as peer-reviewed journals. However, a professional workforce is considered a contributing factor to absorptive capacity ([Knudsen & Roman 2004](#Knudsen04)), as is environmental scanning, so there may be no association between the two. Furthermore, in a previous analysis we showed that clinicians in substance abuse treatment clinics access multiple resources regardless of their educational background ([Arfken _et al._ 2005](#Arfken)). Thus, in this analysis we explored the influence of level of education on environmental scanning activity to determine what effect, if any, it had.

## Methods

### Participants

This study was conducted as part of an ongoing longitudinal investigation of organizational characteristics that influence the diffusion and adoption of innovative treatment techniques among substance abuse treatment clinics. The data used in the current study were collected from 162 clinicians in the third year of the longitudinal study. Participating organizations include fifteen treatment clinics, which were selected on the basis of treatment modality, populations served and geographic location. Five of the participating clinics were included specifically because of their affiliation with the Great Lakes Node of the [Clinical Trials Network](http://www.nida.nih.gov/CTN/Index.htm) (CTN; see [Hanson _et al._ 2002](#Hanson)) as this network was the central focus of the longitudinal study. The non-affiliated clinics are matched 2:1 with the CTN clinics on the basis of treatment modality, populations served, and geographic location. The Institutional Review Board at Wayne State University approved this study.

Overall, 59% of the respondents were female and 51% were white. The mean age of the clinicians was forty-seven years, the mean number of years at the organization was 5.41 years (range of .04—29 years), and the mean number of years in the substance abuse treatment field was 9.91 years (range of .04—35 years). Nearly half of the responding clinicians had a Master's degree or higher and 43% reported personally being in recovery from a drug or alcohol addiction.

### Data collection

For each year of the longitudinal study, a research assistant visited each clinic and distributed surveys to the clinical staff and directors. Each participant has the option of completing the survey (which took around thirty minutes) at that time or finishing it later and mailing it in. Response rate was nearly 100% for the data collected in the third year that were used in this study.

### Measures

Clinicians were asked how often they use the Internet, journals, seminars or conferences, and people in other treatment clinics to gather new information about treatment techniques. Possible responses for each information source were based on frequency of access, ranging from 1 (never) to 5 (weekly). These responses were summed to create the dependent variable _environmental scanning_. Measuring this variable as a sum of the responses denotes the number of sources used as well as their frequency of use.

_Clinic support for acquiring new information_ about treatment techniques was a scale using seven items. Possible responses ranged from 1 (strongly disagree) to 4 (strongly agree). Results of the factor analyses for this scale and all other scales in this analysis support the scales' unidimensionality, with only one factor emerging with an eigenvalue greater than 1.00\. The scale for clinic support for acquiring new information had an internal consistency reliability (Cronbach's alpha) of 0.62.

_Clinic openness to new treatment techniques_ was a scale using four items. Responses ranged from 1 (strongly disagree) to 5 (strongly agree). Two items were reverse scored to ensure that all items were consistent in direction. The internal constancy reliability of the scale was 0.69.

Access to resources included four separate measures. Three of the four measures were whether or not clinicians had access at work to 1) computers, 2) e-mail, and 3) Internet. Possible responses were 0 (no access), 1 (yes I have access but I do not use it regularly), and 2 (yes I have access and I use it regularly). The fourth measure was for clinicians answering _Yes_ to having access to Internet, in which case they were asked whether or not they used it to obtain information on substance abuse treatment. Possible responses were 0 (no) or 1 (yes).

_Satisfaction with resources_ was a scale consisting of four different items. Each item measured clinician satisfaction with the following resources at their clinic: 1) computers, 2) Internet, 3) journals, and 4) books. The response range for these items was 1 (very unsatisfied) to 5 (very satisfied). The internal consistency reliability of this scale was 0.77\.

Clinicians were asked the extent to which they agreed with the belief that it is their responsibility to keep up with current information. Possible responses ranged from 1 (strongly disagree) and 4 (strongly agree). Due to the skewedness of responses, it was recoded into 1 (strongly agree) and 0 (do not strongly agree) with a resulting distribution of 55% and 45%, respectively.

Turnover intention was the mean of three items ([Walsh _et al._ 1985](#Walsh)) measuring the clinician's intent to leave their current position. Possible responses for each item ranged from 1 (strongly disagree) to 5 (strongly agree). Internal consistency reliability of the scale was 0.86.

Being emotionally drained was measured using two items ([Totterdell & Holman 2003](#Totterdell)). Possible responses ranged from 1 (strongly disagree) to 5 (strongly agree). The internal consistency reliability of this scale was 0.71.

Having an advanced degree was a dichotomous variable. An advanced degree was defined as a Master's degree or higher.

### Statistical analysis

Data were analysed using [Stata/SE](http://www.stata.com/) 8.2 statistical software. Because sampling was by clinic, we first examined the intra-class correlations of environmental scanning and the potential organization level variables of perceived clinic openness and of perceived clinic support. The intra-class correlations were 0.10, 0.08 and 0.18 respectively, suggesting some clustering effect. To remove the clustering effect, we conducted a random effects linear regression with clinic as the clustering variable. Univariate models were examined to assess the association between environmental scanning and each independent variable. Because of the concern that the independent variables may be interrelated, we also used a multivariate model to assess independent associations with the dependent variable. Those variables significant at p <0.05 were then entered into the multivariate model. Repeating this analysis without clinics as a cluster did not change the results. The results from the analysis with clinic as a cluster are presented below. Confidence intervals are presented in Table 2.

## Results

### Frequency of external information source use

Environmental scanning scores ranged from four to twenty, with a mean of 12.56 (_Standard Deviation_ = 3.40) (Table 1). The distribution of the components used to measure environmental scanning is shown in Figure 1\. Seminars/conferences had the highest frequency of use with 84% of clinicians using them at least yearly as a source for new treatment information. The Internet was the information source with the lowest number of clinicians using it at least yearly (68%).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Descriptive statistics of measures**</caption>

<tbody>

<tr>

<th rowspan="2">Variable</th>

</tr>

<tr>

<th style="font-style: italic;">N</th>

<th>Mean</th>

<th>Range</th>

<th>_SD_</th>

</tr>

<tr>

<td>Environmental scanning</td>

<td align="center">160</td>

<td align="center">12.56</td>

<td style="text-align: center;">4-20</td>

<td style="text-align: center;">3.40</td>

</tr>

<tr>

<td>Perceived clinic support</td>

<td align="center">162</td>

<td style="text-align: center;">2.90</td>

<td style="text-align: center;">1.71-4</td>

<td style="text-align: center;">0.44</td>

</tr>

<tr>

<td>Perceived clinic openness</td>

<td align="center">162</td>

<td style="text-align: center;">3.57</td>

<td style="text-align: center;">1-5</td>

<td style="text-align: center;">0.69</td>

</tr>

<tr>

<td>Personal responsibility</td>

<td align="center">162</td>

<td style="text-align: center;">0.59</td>

<td style="text-align: center;">0-1</td>

<td style="text-align: center;">0.50</td>

</tr>

<tr>

<td>Access to computers</td>

<td align="center">161</td>

<td style="text-align: center;">1.65</td>

<td style="text-align: center;">0-2</td>

<td style="text-align: center;">0.64</td>

</tr>

<tr>

<td>Access to e-mail</td>

<td align="center">161</td>

<td style="text-align: center;">1.28</td>

<td style="text-align: center;">0-2</td>

<td style="text-align: center;">0.87</td>

</tr>

<tr>

<td>Access to the Internet</td>

<td align="center">161</td>

<td style="text-align: center;">1.31</td>

<td style="text-align: center;">0-2</td>

<td style="text-align: center;">0.82</td>

</tr>

<tr>

<td>Use of Internet for new information</td>

<td align="center">123</td>

<td style="text-align: center;">0.80</td>

<td style="text-align: center;">0-1</td>

<td style="text-align: center;">0.40</td>

</tr>

<tr>

<td style="vertical-align: top;">Satisfaction with resources</td>

<td style="vertical-align: top;">162</td>

<td style="vertical-align: top; text-align: center;">3.28</td>

<td style="vertical-align: top; text-align: center;">1-5</td>

<td style="vertical-align: top; text-align: center;">0.90</td>

</tr>

<tr>

<td style="vertical-align: top;">Turnover intention</td>

<td style="vertical-align: top;">162</td>

<td style="vertical-align: top; text-align: center;">1.84</td>

<td style="vertical-align: top; text-align: center;">1-5</td>

<td style="vertical-align: top; text-align: center;">1.02</td>

</tr>

<tr>

<td style="vertical-align: top;">Being emotionally drained</td>

<td style="vertical-align: top;">161</td>

<td style="vertical-align: top; text-align: center;">2.50</td>

<td style="vertical-align: top; text-align: center;">1-5</td>

<td style="vertical-align: top; text-align: center;">1.16</td>

</tr>

<tr>

<td style="vertical-align: top;">Having an advanced degree</td>

<td style="vertical-align: top;">162</td>

<td style="vertical-align: top; text-align: center;">0.48</td>

<td style="vertical-align: top; text-align: center;">0-1</td>

<td style="vertical-align: top; text-align: center;">0.50</td>

</tr>

</tbody>

</table>

<div align="center">![figure1](p244fig1.jpg)</div>

<div align="center">  
**Figure 1: Percentage of clinicians reporting use of information sources by frequency of use.**</div>

### Variables associated with environmental scanning

In the univariate analysis (Table 2), as expected, environmental scanning was positively associated with perceived clinic openness to new treatment techniques and perceived clinic support for acquiring new information. The association between the belief that is it part of the clinician's job to keep up with current information and environmental scanning was marginally significant. The hypothesized positive association between access to resources and environmental scanning was supported only for access to e-mail and access to the Internet at work. Using the Internet to search for new information on substance abuse treatment was not associated with environmental scanning. As expected, satisfaction with resources at the clinic was positively associated with environmental scanning. Turnover intention was negatively associated with environmental scanning as predicted, as was being emotionally drained. We did not find an association between having an advanced degree and environmental scanning.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption style="caption-side: bottom;">  
**Table 2: Associations with environmental scanning**</caption>

<tbody>

<tr>

<td style="text-align: center; vertical-align: middle;" rowspan="2">**Variable**</td>

<th colspan="2" style="text-align: center; vertical-align: middle;">Univariate</th>

<th colspan="2" style="text-align: center; vertical-align: middle;">Multivariate</th>

</tr>

<tr>

<th style="text-align: center; vertical-align: middle;">B (95% CI*)</th>

<th style="font-style: italic; text-align: center; vertical-align: middle;">p</th>

<th style="text-align: center; vertical-align: middle;">B (95% CI)</th>

<th style="font-style: italic; text-align: center; vertical-align: middle;">p</th>

</tr>

<tr>

<td>Perceived clinic support</td>

<td style="text-align: center;"><small><small><small><small><small><small></small></small></small></small></small></small>2.79 (1.35, 4.23)</td>

<td style="text-align: center;">.001</td>

<td style="text-align: center;">2.01 (.27, 3.74)</td>

<td style="text-align: center;">026</td>

</tr>

<tr>

<td>Perceived clinic openness</td>

<td style="text-align: center;">1.06 (.31, 1.81)</td>

<td style="text-align: center;">.009</td>

<td style="text-align: center;">.37 (-.65, 1.38)</td>

<td style="text-align: center;">.451</td>

</tr>

<tr>

<td>Personal responsibility</td>

<td style="text-align: center;">1.00 (-.24, 2.23)</td>

<td style="text-align: center;">.104</td>

<td rowspan="1" colspan="2" style="text-align: center;"></td>

</tr>

<tr>

<td>Access to computers</td>

<td style="text-align: center;">.42 (-.59, 1.42)</td>

<td style="text-align: center;">.390</td>

<td rowspan="1" colspan="2" style="text-align: center;"></td>

</tr>

<tr>

<td>Access to e-mail</td>

<td style="text-align: center;">.72 (.19, 1.25)</td>

<td style="text-align: center;">.011</td>

<td style="text-align: center;">.53 (-.90, 1.96)</td>

<td style="text-align: center;">.439</td>

</tr>

<tr>

<td>Access to the Internet</td>

<td style="text-align: center;">.70 (.15, 1.26)</td>

<td style="text-align: center;">.016</td>

<td style="text-align: center;">.03 (-1.48, 1.54)</td>

<td style="text-align: center;">.966</td>

</tr>

<tr>

<td style="vertical-align: middle; text-align: left;">Use the Internet for new information</td>

<td style="text-align: center; vertical-align: middle;">.88 (-.68, 2.45)</td>

<td style="text-align: center; vertical-align: middle;">.245</td>

<td style="text-align: center; vertical-align: middle;" rowspan="1" colspan="2"></td>

</tr>

<tr>

<td>Satisfaction with resources</td>

<td style="text-align: center;">1.19 (.44, 1.93)</td>

<td style="text-align: center;">.004</td>

<td style="text-align: center;">.15 (-.78, 1.07)</td>

<td style="text-align: center;">.734</td>

</tr>

<tr>

<td>Turnover intention</td>

<td style="text-align: center;">-.80 (-1.39, -.21)</td>

<td style="text-align: center;">.012</td>

<td style="text-align: center;">-.40 (-1.15, .37)</td>

<td style="text-align: center;">.288</td>

</tr>

<tr>

<td style="vertical-align: middle;">Being emotionally drained</td>

<td style="text-align: center; vertical-align: middle;">-.55 (-.88, -.22)</td>

<td style="text-align: center; vertical-align: middle;">.003</td>

<td style="text-align: center; vertical-align: middle;">-.20 (-.83, .44)</td>

<td style="text-align: center; vertical-align: middle;">.516</td>

</tr>

<tr>

<td style="vertical-align: top;">Having an advanced degree</td>

<td style="vertical-align: top; text-align: center;">.90 (-.49, 2.28)</td>

<td style="vertical-align: top; text-align: center;">.185</td>

<td style="vertical-align: top; text-align: center;" rowspan="1" colspan="2"></td>

</tr>

<tr>

<td colspan="5">Note: * CI=Confidence Interval</td>

</tr>

</tbody>

</table>

In multivariate analysis with seven independent variables (those significant at p <0.05 in univariate analysis), perceived clinic support for acquiring information was independently associated with environmental scanning (Table 2). Including all eleven original independent variables in the model did not change this result. As the coefficients in the multivariate analysis for the non-significant independent variables changed by more than 10% from the univariate analysis, we explored why the models differed by performing additional analyses. With perceived clinic support as the outcome variable and the remaining variables each entered separately as an independent variable, there were positive associations with perceived clinic openness (β = 0.25, 95% CI: 0.14-0.36, p <0.001) and satisfaction with resources (β = 0.29, 95% CI: 0.20-0.37, p <0.001), and a negative association with turnover intention (β = -0.10, 95% CI: -0.19-0.02, p <0.05).

## Discussion and conclusions

We found environmental scanning for new information to be independently associated with perceived clinic support for acquiring information. Perceived support encompasses the clinic's encouragement of clinicians to attend outside educational functions, provision of and training on informational resources, and discussion of new treatment techniques at staff meetings. Thus clinics' steps in nurturing an environment conducive to information scanning appear to be associated with higher levels of environmental scanning.

The univariate results were consistent with our hypotheses. Environmental scanning was positively associated with perceived clinic openness to new treatment techniques, satisfaction with resources at the clinic, and access to e-mail and the Internet at the clinic. Lower levels of environmental scanning were associated with clinicians' intentions to quit and being emotionally drained.

Together, the multivariate and univariate results support three of the four factors (exposure to information, information climate, and organizational outwardeness) proposed by Correia and Wilson ([2001](#Correia)), and imply several ways to enhance environmental scanning among clinicians. One such step clinic directors could take is to cultivate open, supportive work environments to increase environmental scanning activities. By encouraging clinical staff to attend conferences and trainings, to read current journal articles, or to network with people from other treatment clinics, the clinic _boundary_ could become more open to new techniques. Although in our sample the perceptions of clinic openness to new techniques and support for acquiring new information were not necessarily shared by all members of a clinic, information entering the clinic could still be diffused internally. In theory, having at least one clinician with a high level of environmental scanning activity would accelerate the acquisition of new knowledge within a clinic ([Arfken _et al._ 2005](#Arfken2); [Rogers 2003](#Rogers)).

Complementing the fostering of open and supportive work environments, clinic directors could potentially increase environmental scanning activities by providing access to information rich resources. Our results show that having access to the specific resources of e-mail and the Internet was associated with high levels of environmental scanning; access to computers did not share this association. This lack of association may be due to a ceiling effect, given the high amount of clinicians with access to computers, or it may also be due to computers at some clinics being available solely for clinical documentation or accessing client records. In contrast, when the available computers are connected to the Internet and the clinicians are allowed to access it, there is a greater potential for the flow of information from publications or substance abuse related Websites into the clinic. In addition, the availability of e-mail allows for the exchange of information between co-workers and people at other treatment clinics. Furthermore, our results suggest that ensuring these information resources are satisfactory to the clinicians is associated with higher levels of environmental scanning. From this finding, it appears that investing in a knowledgeable workforce requires investing in the technological capacity to support it.

Importantly, our results also suggest that decreasing negative influences may result in higher levels of environmental scanning. Clinicians' intentions to quit and the emotional stress they experience at work were negatively related to environmental scanning. These negative influences, already linked to high turnover rates ([Carise _et al._ 1999](#Carise); [Knudsen _et al._ 2003](#Knudsen03)), also appear to adversely impact the clinic's absorptive capacity.

Enhancing environmental scanning activities at substance abuse treatment clinics is important, as high amounts of environmental scanning have been shown to be associated with higher use of innovative treatment techniques in substance abuse treatment programmes ([Knudsen & Roman 2004](#Knudsen04)). Scanning the environment can serve as a means for the diffusion of innovations to occur, a process which involves the communication of an innovation over time among the member of a social system ([Rogers 2003](#Rogers)). The information sources that clinicians use serve as the communication channels by which they can become familiar with or form an opinion about an innovation in treatment techniques. The first stage in the innovation-decision process is the knowledge stage, in which an individual is exposed to the innovation's existence and gains an understanding of how it functions. While exposure to information about an innovation is not sufficient for its adoption and implementation, it does represent the first step ([Rogers 2003](#Rogers); [Simpson 2002](#Simpson)). However, longitudinal research is required to clarify the relationship between the factors found in this analysis and organizational use of innovative treatment techniques.

The limitations of this study include limited geographic area and small sample size. Additionally, we only included four information sources in our environmental scanning variable, and it may be useful for future research to consider type of information source. Howell and Shea ([2001](#Howell)) found differences in examining managers' environmental scanning of people and environmental scanning of documents. However, for this analysis we based our choices on sources similar to those presented in Knudsen and Roman's study ([2004](#Knudsen04)).

Our results complement those of Correia and Wilson ([2001](#Correia)), and provide quantitative evidence that environmental scanning is associated with individual factors (i.e., perceptions about their clinic) as well as conditions in the organization such as the provision of access to satisfactory information sources. These findings indicate several specific areas that substance abuse treatment programme directors can target to promote environmental scanning among their clinical staff. Although the causal directions of these relationships are not known, our results suggest that when these conditions are present, clinicians may be more likely to engage in environmental scanning.

## Acknowledgements

The authors would like to thank the National Institute on Drug Abuse (grant RO1 DA14483) and the State of Michigan (Joe Young, Sr.) for providing funding for this research.

## References

*   <a name="Aguilar" id="Aguilar"></a>Aguilar, F.J. (1967). _Scanning the business environment_. New York, NY: McMillan.
*   <a name="Arfken" id="Arfken"></a>Arfken, C.L., Agius, E., & Dickson, M.W. (2005). Clinicians' sources of information for new substance abuse treatment. _Addictive behaviours_, **30**(8), 1592-1596.
*   <a name="Arfken2" id="Arfken2"></a>Arfken, C.L., Agius, E., Dickson, M.W, Anderson, H.L, & Hegedus, A.M. (2005). Clinicians' beliefs and awareness of substance abuse treatment in research and non-research affiliated programs. _Journal of Drug Issues_, **35**(3), 547-558.
*   <a name="Ball" id="Ball"></a>Ball, S., Bachrach, K., DeCarlo, J., Farentinos, C., Keen, M., McSherry, T., Polcin, D., Snead, N., Sockriter, R., & Wrigley, P. (2002). Characteristics, beliefs, and practices of community clinicians trained to provide manual-guided therapy for substance abusers. _Journal of Substance Abuse Treatment_, **23**(4), 309-318.
*   <a name="Choo" id="Choo"></a>Choo, C.W. (1994). Perception and use of information sources by chief executives in environmental scanning. _Library & Information Science Research_, **16**(1), 23-40\.
*   <a name="Carise" id="Carise"></a>Carise, D., McLellan, A.T., Gifford, L.S., & Kleber, H.D. (1999). Developing a national addiction treatment information system. An introduction to the Drug Evaluation Network System. _Journal of Substance Abuse Treatment_, **17**(1-2), 67-77.
*   <a name="Cohen" id="Cohen"></a>Cohen, W.M. & Levinthal, D.A. (1990). Absorptive capacity: a new perspective on learning and innovation. _Administrative Science Quarterly_, **35**(1), 128-152.
*   <a name="Correia" id="Correia"></a>Correia, Z. & Wilson, T.D. (2001). [Factors influencing environmental scanning in the organizational context.](http://informationr.net/ir/7-1/paper121.html) _Information Research_, **7**(1), paper 121\. Retrieved 8 February, 2005 from http://informationr.net/ir/7-1/paper121.html
*   <a name="Damanpour" id="Damanpour"></a>Damanpour, F. (1991). Organizational innovation: a meta-analysis of effects of determinants and moderators. _Academy of Management Journal_, **34**(3), 555-590.
*   <a name="Forman" id="Forman" ""=""></a>Forman, R. F., Bovasso, G., & Woody, G. (2001). Staff beliefs about addiction treatment. _Journal of Substance Abuse Treatment_, **21**(1), 1-9.
*   <a name="Gertsberge" id="Gertsberge"></a>Gertsberger, P.G. & Allen, T.J. (1968). Criteria used by research and development engineers in the selection of an information source. _Journal of Applied Psychology_, **52**(4), 272-279.
*   <a name="Hanson" id="Hanson"></a>Hanson, G.R., Leshner, A.I., & Tai, B. (2002). Putting drug abuse research to use in real-life settings. _Journal of Substance Abuse Treatment_, **23**(2), 69-70.
*   <a name="Herring" id="Herring"></a>Herring, S.D. (2001). Using the World Wide Web for research: Are faculty satisfied? _The Journal of Academic Librarianship_, **27**(3), 213-219.
*   <a name="Howell" id="Howell"></a>Howell, J.M. & Shea, C.M. (2001). Individual differences, environmental scanning, innovation framing, and champion behaviour: key predictors of project performance. _The Journal of Product Innovation Management_, **18**(1), 15-27.
*   <a name="Knudsen03" id="Knudsen03"></a>Knudsen, H.K., Johnson, J.A., & Roman, P.M. (2003). Retaining counseling staff at substance abuse treatment centers: effects of management practices. _Journal of Substance Abuse Treatment_, **24**(2), 129-35.
*   <a name="Knudsen04" id="Knudsen04"></a>Knudsen, H.K. & Roman, P.M. (2004). Modeling the use of innovations in private treatment organizations: the role of absorptive capacity. _Journal of Substance Abuse Treatment_, **26**(1), 51-59.
*   <a name="Lamb" id="Lamb"></a>Lamb, S., Greenlick, M.R., & McCarty, D. (Eds.). (1998). _Bridging the gap between practice and research: forging partnerships with community-based drug and alcohol treatment_. Washington, D.C.: National Academy Press.
*   <a name="Martin" id="Martin"></a>Martin, G.W., Herie, M.A., Turner, B.J., & Cunningham, J.A. (1998). A social marketing model for disseminating research-based treatments to addictions treatment providers. _Addiction_, **93**(11), 1703-1715.
*   <a name="Pugliesi" id="Pugliesi"></a>Pugliesi, K. (1999). The consequences of emotional labor: effects on work stress, job satisfaction, and well-being. _Motivation & Emotion_, **23**(2), 125-154.
*   <a name="Rogers" id="Rogers"></a>Rogers, E.M. (2003). _Diffusion of innovations_ (5th ed.). New York, NY: The Free Press.
*   <a name="Roman" id="Roman"></a>Roman, P.M. & Johnson, A.A. (2002). Adoption and implementation of new technologies in substance abuse treatment. _Journal of Substance Abuse Treatment_, **22**(4), 211-218.
*   <a name="Schneider" id="Schneider"></a>Schneider, B., Brief, A.P., & Guzzo, R.A. (1996). Creating a climate and culture for sustainable organizational change. _Organizational Dynamics_, **24**(4), 6-29.
*   <a name="Simpson" id="Simpson"></a>Simpson, D.D. (2002). A conceptual framework for transferring research to practice. _Journal of Substance Abuse Treatment_, **22**(4), 171-182.
*   <a name="Sorenson" id="Sorenson"></a>Sorensen, J.L., Hall, S.M., Loeb, P., Allen, T., Glaser, E.M., & Greenberg, P.D. (1988). Dissemination of a job seekers' workshop to drug treatment programs. _behaviour Therapy_, **19**, 143-155.
*   <a name="Spector" id="Spector"></a>Spector, P.E. & Fox, S. (2005). The stressor-emotion model of counterproductive work behaviour. In S. Fox & P. E. Spector (Eds.). _Counterproductive work behaviour: Investigations of actors and targets_, pp. 151-174\. Washington, DC: USical Association.
*   <a name="Thomas" id="Thomas"></a>Thomas, C.P., Wallack, S.S., Lee, S. McCarty, D., & Swift, R. (2003). Research to practice: adoption of naltrexone in alcoholism treatment. _Journal of Substance Abuse Treatment_, **24**(1), 1-11.
*   <a name="Totterdell" id="Totterdell"></a>Totterdell, P. & Holman, D. (2003). Emotion regulation in customer service roles: testing a model of emotional labor. _Journal of Occupational Health Psychology_, **8**, 55-73.
*   <a name="Walsh" id="Walsh"></a>Walsh, J.P., Ashford, S.J. & Hill, T.E. (1985). Feedback obstruction: the influence of the information environment on employee turnover intentions. _Human Relations_, **38**(1), 23-46.
*   <a name="Zahra" id="Zahra"></a>Zahra, S.K. & George, G. (2002). Absorptive capacity: a review, reconceptualization, and extension. _Academy of Management Review_, **27**(2), 185-203.



## Appendix: Survey questions used for this analysis (in order of appearance on the survey)

**What is the highest education level you have achieved? (Please check your answer)**

1\. High school

2\. Some college

3\. College graduate

4\. Some graduate school

5\. Master's degree

6\. Ph.D./Psy.D./Ed.D.

7\. M.D./D.O.

### The following questions measure access to resources (Response options: No; Yes, but I do NOT use it regularly; Yes, and I use it regularly):

1\. Do you have access to a computer at work?

2\. Do you have access to e-mail at work?

3\. Do you have access to the Internet at work?

**If yes, do you use the internet at work to obtain information on substance abuse treatment? (Yes or No)**

### The following questions were used to form the _organizational support for acquiring new information_ scale: (R) indicates that the item was reverse scored

**Please tell us the extent to which you agree or disagree (Strongly disagree, Disagree, Agree, Strongly agree) with the following statements:**

1\. Conferences and outside training are a good source of information on new treatment techniques.

2\. My facility does not have access to current print journals relating to treatment. (R)

3\. Staff meetings are a valuable source for new information on evidence-based practices.

4\. I am not encouraged to attend outside trainings and conference to develop my skills. (R)

5\. I am easily able to access journals online to gather information.

6\. The computers we use are upgraded (regularly, when necessary).

7\. Staff orientation included computer training related to clinical documentation.

### The following question was used to measure _personal responsibility_:

**Please tell us the extent to which you agree or disagree (Strongly disagree, Disagree, Agree, Strongly agree) with the following statements:**

1\. It is part of my job to keep up with current research in the field.

### The following questions were used to form the _satisfaction with resources_ scale:

**How satisfied (Very unsatisfied, Unsatisfied, Somewhat satisfied, Satisfied, Very satisfied) are you with the following resources at your clinic?**

1\. Computer access

2\. Internet access

3\. Journals

4\. Books

### The following questions were used to form the _environmental scanning_ variable:

**How often (Never, Less than yearly, Yearly, Monthly, Weekly) do you get information on new substance abuse treatment from the following resources?**

1\. Internet

2\. Seminars/conferences

3\. People in other treatment facilities

4\. Journals

### The following questions were used to form the _organizational openness to change_ scale: (R) indicates that the item was reverse scored

**Using the scale below (Strongly disagree, Disagree, Neutral, Agree, Strongly agree), please check the degree to which you agree or disagree with each of the following statements:**

1\. The people in my organization are confident in their treatment approaches, and are highly unlikely to change. (R)

2\. My organization as a whole is very open to change.

3\. People in my organization want to try new approaches to treatment.

4\. People in my organization have little access to information about new approaches to treatment. (R)

### The following questions were used to form the _being emotionally drained_ scale:

**Please rate the extent to which you agree or disagree (Strongly disagree, Disagree, Neutral, Agree, Strongly Agree) with the following statements. Remember, your answers are completely confidential and your identity will not be revealed This survey is completely voluntary and you can choose not to answer any question.**

1\. I often feel emotionally drained at work.

2\. I often feel emotionally numb at work.

### The following questions were used to form the _turnover intention_ scale:

**Please rate the extent to which you agree (Strongly disagree, Disagree, Neutral, Agree, Strongly agree)  or disagree with the following statements. Remember, your answers are completely confidential and your identity will not be revealed This survey is completely voluntary and you can choose not to answer any question.**

1\. As soon as I can find a better job, I will leave this program.

2\. I am actively looking for a job at another program.

3\. I am seriously thinking about quitting my job.



