<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="style.css">
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
 <meta name="dcterms.title" content="Factors affecting browsing duration on a health discussion forum: analysis of eye-tracking data" />
<meta name="citation_author" content="Pian, Wenjing" />
<meta name="citation_author" content="Khoo, Christopher S.G." />
<meta name="citation_author" content="Li, Gang" />
<meta name="citation_author" content="Chi, Jianxing" />
 <meta name="dcterms.subject" content="User-contributed information on health-related social media sites contains useful user-experience information that complements information on authoritative health information websites. " />
 <meta name="description" content="User-contributed information on health-related social media sites contains useful user-experience information that complements information on authoritative health information websites. Though more people are searching and browsing social media sites for health information, there are few studies of user browsing behaviour on these sites, and the factors that affect the behaviour. This study investigated the factors affecting the users’ skimming and examining durations when browsing a health discussion forum. An eye-tracker system was used to record users’ eye fixations and eye movements when browsing a health discussion forum, HealthBoards.com. Stepwise multiple linear regression was used to develop models to predict users’ skimming and examining durations.The type of health information need context (i.e. browsing for own health issue, browsing for someone else’s health issue, and browsing with no particular health issue in mind), the perceived urgency of the health information need, and the length of the health information text were found to be significant predictors of the examining and skimming durations. Demographic factors of gender and age were not significant. Future studies of online health information seeking should take these three factors into consideration, and distinguish between browsing two types of screens—the summary screen of document surrogates and the detailed content screen." />
 <meta name="keywords" content="health information, information searching behaviour, eye tracking analysis, information needs," />
 <meta name="robots" content="all" />
 <meta name="dcterms.publisher" content="University of Borås" />
 <meta name="dcterms.type" content="text" />
 <meta name="dcterms.identifier" content="ISSN-1368-1613" />
 <meta name="dcterms.identifier" content="http://InformationR.net/ir/24-1/isic2018/isic1837.html" />
 <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/24-1/infres241.html" />
 <meta name="dcterms.format" content="text/html" />  <meta name="dc.language" content="en" />
 <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
 <meta  name="dcterms.issued" content="2019-03-15" />
  <meta name="geo.placename" content="global" />
</head>

 <body>
    <h4 id="vol-24-no-1-march-2019">vol 24 no.1, March 2019</h4>
 <h5>
  Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.
 </h5>

 <h1>Factors affecting browsing duration on a health discussion forum: analysis of eye-tracking data
</h1>  

 <h2 class="author"><a href="#author">Wenjing Pian</a>, <a href="#author">Christopher S.G. Khoo</a>, <a href="#author">Gang Li</a> and <a href="#author">Jianxing Chi</a>
</h2>

 <blockquote>
 <strong>Introduction</strong>.User-contributed information on health-related social media sites contains useful user-experience information that complements information on authoritative health information websites. Though more people are searching and browsing social media sites for health information, there are few studies of user browsing behaviour on these sites, and the factors that affect the behaviour. This study investigated the factors affecting the users’ skimming and examining durations when browsing a health discussion forum.<br />
<strong>Methods</strong>. An eye-tracker system was used to record users’ eye fixations and eye movements when browsing a health discussion forum, HealthBoards.com.<br />
<strong>Analysis</strong>. Stepwise multiple linear regression was used to develop models to predict users’ skimming and examining durations.<br />
<strong>Results</strong>. The type of health information need context (i.e. browsing for own health issue, browsing for someone else’s health issue, and browsing with no particular health issue in mind), the perceived urgency of the health information need, and the length of the health information text were found to be significant predictors of the examining and skimming durations. Demographic factors of gender and age were not significant.<br />
<strong>Conclusion</strong> Future studies of online health information seeking should take these three factors into consideration, and distinguish between browsing two types of screens—the summary screen of document surrogates and the detailed content screen. <br />
 </blockquote>
 <section>

 <h2>Introduction</h2>

 <p>People are increasingly searching for and browsing health information on websites and social media platforms (<a href="#mor15">Moreland, French and Cumming, 2015</a>; <a href="#fox13">Fox and Duggan, 2013</a>; <a href="#son16">Song <em>et al.</em>, 2015</a>). User-contributed health information on social media sites offer user-experience information that complements information on authoritative health information sites (<a href="#che16">Chew and Khoo, 2016</a>).</p>

<p>Different social media platforms have different functional characteristics, which may result in different information browsing behaviours. There have been several studies of health information behaviour on Facebook public groups (e.g., <a href="#sco16">Scott and Hand, 2016</a>). However, there have been few studies of information seeking and browsing behaviour on health discussion forums. It is important to understand how people browse health topics in these forums as they have large numbers of registered users. The health discussion forum used in this study, HealthBoards.com (<a href="http://www.healthboards.com/boards/index.php">http://www.healthboards.com/boards/index.php</a>), has 1.1 million registered users.</p>

<p>A typical health discussion forum has a two-stage interface structure. <em>Stage 1</em> displays a summary page of post surrogates (mainly post titles) on a particular topic or retrieved by a search query. The user browses the page of post surrogates and selects (clicks on) the post surrogate that seems relevant or useful. The system then displays a <em>Stage 2</em> page of the detailed post content, together with comments from other users. Within each stage, two kinds of browsing behaviour can be distinguished: skimming quickly over the text, and examining the text closely. These are reflected in two kinds of eye movements: saccade or quick eye movement, and eye fixation. The two-stage structure of user interaction with the discussion forum combined with the two types of browsing behaviour (skimming and examining) is used to structure the analyses in this study.</p>

<p>This study investigated the factors affecting users&rsquo; skimming and examining durations when browsing the summary pages of post surrogates and detailed post-content pages on a popular health discussion forum. In particular the following factors were investigated:</p>

<ol>
<li>The user&rsquo;s health information need context: whether the user is seeking information for his or her own health issue, seeking information for someone else&rsquo;s (i.e. family member of friend&rsquo;s) health issue, or browsing without any particular health issue in mind;</li>
<li>Perceived urgency of the health information need;</li>
<li>Length of the information content (number of words)</li>
<li>Demographic factors of gender and age.</li>
</ol>

<p>Previous studies have identified several factors that affect user online seeking behaviour (<a href="#teo01">Teo, 2001</a>; <a href="#bid15">Bidmon and Terlutter, 2015</a>; <a href="#sco16">Scott and Hand, 2016</a>; <a href="#pia16">Pian, Khoo and Chang, 2016</a>), including gender, age, types of health information need context, and urgency of health information need. However, the relative importance of these factors for different types of browsing behaviour, types of health information site, and types of display screens is not known.</p>

</section>
<section>

<h2>Method</h2>

<p>The study was carried on the campus of the Nanyang Technological University, Singapore. Eighty people were recruited in the study. Seven participants failed the eye-tracker calibration test due to their thick glasses. Of the 73 participants, 60 were university students, faculty members, and administrative staff; the remaining 13 were Singapore residents. None of the participants was a health professional, and none had a critical or terminal health issue (e.g., cancer or HIV). They were competent in English and instructions, survey questions and interview were all carried out in English. The study was approved by the university&rsquo;s Institutional Review Board.</p>

<p>The participants were asked to browse a health discussion forum (HealthBoards.com) to find relevant or useful information related to a health issue of their choice. If a participant could not think of a health issue, they were assigned to the category of browsing with no particular issue in mind. Their browsing behaviours were recorded using a Tobii T-series eye tracker system, and the eye fixation and eye movement data were exported to a Microsoft Excel file for processing and analysis. The participants were interviewed to collect demographic information, a health issue to seek information for in the session, and the perceived urgency of the health information need.</p>

<p>Stepwise multiple linear regression was used to develop predictive models for the durations of examining and skimming, based on the independent variables of type of health information need context, level of urgency of information need, gender, age, and length of the health information text. The unit of analysis in this study was the individual Web pages, which may be a summary screen displaying a list of post surrogates or a detailed post content screen (including user comments). As each research participant browsed several summary and detailed post pages, there were totally 814 data units in the analyses.</p>

<p>Two dummy variables were created to represent the three types of health information need context (browsing for self: 1, 0; browsing for others: 0, 1; and browsing with no particular issue: 0, 0). A 7-point Likert scale was used to represent the perceived urgency of information need from extremely urgent (7) to not urgent at all (1). The length of the information text on the Web page was measured as the number of words and divided by 100, to obtain values in single digits.</p>

<p>As each participant browsed several summary pages and detailed post pages, the duration of examining for each page was calculated by accumulating individual fixation durations on the page (<a href="#sco16">Scott and Hand, 2016</a>; <a href="#wan16">Wang, Tsai and Tsai, 2016</a>; <a href="#des13">Desjarlais, 2013</a>), and the duration of skimming was calculated by accumulating the time gaps between fixations (<a href="#pia16">Pian, Khoo and Chang, 2016</a>; <a href="#zha12">Zhang, 2012</a>).</p>

</section>

<section>

<h2>Results</h2>

<p>The regression models developed for predicting the examining and skimming durations were found to be good in accounting for the variances in the dependent variables (see Table 1). The model fitness adjusted R squares range between 0.39 and 0.52.</p>

<p>For the stage of browsing the detailed post screen, three factors&mdash;type of information need context, urgency of information need and length of information text&mdash;were found to be significant predictors of the examining duration (<em>&alpha;</em>=0.05). In general, the examining duration was highest for users seeking for their own health information need, lower when seeking for other&rsquo;s health information need, and lowest when browsing with no particular health issue in mind. Post hoc comparisons using Tukey&rsquo;s HSD found significant differences among these three types of health information need context.</p>

<p>In addition, the interaction variables, <em>Seeking_for_self*Length_of_information_text </em>and <em>Seeking_for_others*Length_of_information_text </em>were significant. This indicates that longer post content pages increase the examining durations of users searching for self or others, more than for users browsing with no particular issue in mind. Table 2 shows the average examining durations for the different combinations of information need context and length of information text.</p>

<p>As for the skimming duration, again all the factors were significant at the 0.05 level, but the coefficients for <em>Seeking_for_self and Seeking_for_others </em>were negative, indicating that users with no particular health issue in mind had a longer skimming duration.</p>

<p>The length of information text was found to affect the examining duration significantly. The demographic factors of gender and age were not found to be significant in predicting browsing duration.</p>

<p>For the stage of browsing the summary screen of post surrogates, type of health information need context and urgency of information need were significant predictors of examining and skimming durations, but not the information length. Interestingly, for the examining duration, <em>Seeking_for_self&nbsp; </em>had a negative coefficient, indicating that users browsing for own health issue spent less time examining post surrogates than users browsing on others&rsquo; behalf or with no particular issue in mind. Users browsing for other&rsquo;s health issue had the highest examining duration. Gender and age were again not significant. As for the skimming duration, users searching with a conscious health issue (either for self or for others) had a shorter skimming duration than users browsing with no issue in mind.</p>

<table class="center" style="width:70%;">
<caption><br />Table 1: Summary of regression models for predicting examining and skimming durations for summary screen of post surrogates and detailed post content screen</caption>
<tbody>
<tr><th></th><th colspan="2">Examining duration for detailed post content</th><th colspan="2">Skimming duration for detailed post content</th><th colspan="2">Examining duration for post surrogates</th>
</tr><tr><td>Variable</td><td style='text-align:left;' colspan="2">Model adj R2: 0.388 <br />Model Sig.&lt;.001 </td><td style='text-align:left;' colspan="2">Model adj R2: 0.434<br />Model Sig.=.001</td><td style='text-align:left;' colspan="2">Model adj R2:0.524<br />Model Sig.&lt;.001</td>
</tr><tr><td></td><td style='text-align:left;'>Coefficient</td><td style='text-align:left;'>Sig.</td><td style='text-align:left;'>Coefficient</td><td style='text-align:left;'>Sig.</td><td style='text-align:left;'>Coefficient</td><td style='text-align:left;'>Sig.</td>
</tr><tr><td>Constant</td><td style='text-align:left;'>16.665</td><td style='text-align:left;'>.160</td><td style='text-align:left;'>32.356</td><td style='text-align:left;'>.000</td><td style='text-align:left;'>10.936</td><td style='text-align:left;'>.000</td>
</tr><tr><td>Seeking for self</td><td style='text-align:left;'>24.001</td><td style='text-align:left;'>.027</td><td style='text-align:left;'>-5.817</td><td style='text-align:left;'>.002</td><td style='text-align:left;'>-1.007</td><td style='text-align:left;'>.000</td>
</tr><tr><td>Seeking for others</td><td style='text-align:left;'>12.432</td><td style='text-align:left;'>.000</td><td style='text-align:left;'>-7.061</td><td style='text-align:left;'>.041</td><td style='text-align:left;'>2.294</td><td style='text-align:left;'>.012</td>
</tr><tr><td>Urgency of health information need</td><td style='text-align:left;'>1.005</td><td style='text-align:left;'>.021</td><td style='text-align:left;'>.502</td><td style='text-align:left;'>.047</td><td style='text-align:left;'>.584</td><td style='text-align:left;'>.014</td>
</tr><tr><td>Length of information text</td><td style='text-align:left;'>.391</td><td style='text-align:left;'>.031</td><td style='text-align:left;'>.053</td><td style='text-align:left;'>.120</td><td style='text-align:left;'>.141</td><td style='text-align:left;'>.562</td>
</tr><tr><td>Seeking for self<br /> * Length of information text</td><td style='text-align:left;'>2.334</td><td style='text-align:left;'>.025</td><td></td><td></td><td></td><td></td>
</tr><tr><td>Seeking for others * Length of information text</td><td style='text-align:left;'>1.546</td><td style='text-align:left;'>.019</td><td></td><td></td><td></td><td></td>
</tr></tbody>
</table>


<table class="center" style="width:55%;">
<caption><br />Table 2: Interaction between types of information need context and length of health information text, when browsing detailed post content </caption>
<tbody>
<tr><th colspan="2" rowspan="2">Average examining duration (in seconds)</th><th colspan="3">Type of information need context</th>
</tr><tr><td style='text-align:left;'>Seeking for own health issue</td><td style='text-align:left;'>Seeking for other’s health issue</td><td style='text-align:left;'>No particular health issue in mind</td>
</tr><tr><td rowspan="3">Length of health information text (words)</td><td style='text-align:left;'>Long (>500)</td><td style='text-align:left;'>65.2</td><td style='text-align:left;'>54.3</td><td style='text-align:left;'>27.0</td>
</tr><tr><td style='text-align:left;'>Medium (200-499)</td><td style='text-align:left;'>61.1</td><td style='text-align:left;'>44.1</td><td style='text-align:left;'>25.2</td>
</tr><tr><td style='text-align:left;'>Short (&lt;200)</td><td style='text-align:left;'>57.6</td><td style='text-align:left;'>36.2</td><td style='text-align:left;'>20.1</td>
</tr></tbody>
</table>



</section>

<section>

<h2>Conclusion</h2>

<p>This study found that three factors&mdash;type of health information need context (i.e. seeking for self, seeking for others, and seeking with no particular health issue in mind), the perceived urgency of the health information need, and the length of the health information text&mdash;are significant predictors of the durations of examining and skimming behaviour on a health discussion forum. The type of health information need context has the strongest influence on examining and skimming durations.</p>

<p>Users browsing for own health issue tend to spend less time browsing the summary screen of post surrogates (relative to the other two groups of users). They are more <em>decisive</em> in making predictive evaluations about the likely relevance of a post, based on the post surrogate. They spend much more time reading the post content closely, and less time skimming.</p>

<p>Users browsing for other&rsquo;s health issue tend to spend the most time examining the summary screen of post surrogates, trying to decide which post is likely to be useful. They spend more time examining the post content than users with no particular issue in mind, but less time than users browsing for own health issue. Users browsing with no particular health issue in mind engage in a lot of skimming. They tend to spend the most time skimming the post surrogates and post content, relative to the other two groups of users.</p>

<p>Longer post content increase the examining duration significantly, but not the skimming duration. The length of the post content interacts with the type of information need context: longer post content has a bigger impact on the examining duration for users seeking for self and others than for users browsing with no issue in mind. However, the length of the summary page of post surrogates does not have a significant impact on examining and skimming duration, probably because the number of post surrogates displayed was fixed for each webpage.</p>

<p>As expected, perceived urgency of health information need increases examining and skimming duration for both the summary screen of post surrogates and detailed post screen. Gender and age were not significant. Previous studies have found demographic factors to have a relationship with people&rsquo;s online health information behaviour (e.g., <a href="#bid15">Bidmon and Terlutter, 2015</a>; <a href="#cul02">Cullen, 2002</a>; <a href="#teo01">Teo, 2001</a>). In the current study, browsing durations were recorded exactly using an eye-tracker machine, whereas previous studies used self-reported data from questionnaires or interviews (e.g., <a href="#teo01">Teo, 2001</a>; <a href="#fox13">Fox and Duggan, 2013</a>). Also, previous studies made use of cumulative time spent over a period of time, such as one week or one month. This study focused on the average browsing durations for individual webpages.</p>

<p>The results of this study indicate that future studies of health information seeking should take into consideration users&rsquo; health information need context (whether seeking health information for self, for others, or with no particular issue in mind), and distinguish between browsing detailed health information content or summary screens of search results.</p>

</section>

<section>

<h2>Acknowledgements</h2>

<p>This research was supported in part by National Natural Science Foundation of China (No. 71790612) and Social Science Foundation of Fujian Province of China (No. FJ2017C011). The authors would like to thank the anonymous reviewers for their comments and suggestions and the editors for their efforts to publish this paper.</p>
</section>

<section>

<h2 id="author">About the authors</h2>

<p><strong>Wenjing Pian </strong>(first author and corresponding author) is an Assistant Professor at School of Economics and Management at Fuzhou University (address: No.2 Xueyuan Road, University Town, Minhou County, Fuzhou City, Fujian Province, China, 350116) and Distinguished Scholar at Mengchao Hepatobiliary Hospital of Fujian Medical University (address: No.312 Xihong Road, Gulou District, Fuzhou City, Fujian Province, China, 350025). He received his PhD degree at Nanyang Technological University, Singapore. His research focuses on consumer health information seeking, health/medical informatics, and machine learning in clinical decisions. He can be reached at <a href="mailto:swatpwg@foxmail.com">swatpwg@foxmail.com </a>.<br />
<strong>Christopher S.G. Khoo </strong>is an Associate Professor at Wee Kim Wee School of Communication and Information at Nanyang Technological University (address: WKW SCI building, Nanyang Link, Singapore, 637718). He obtained his PhD at Syracuse University in 1997, his MSc in library and information science at the University of Illinois, Urbana-Champaign in 1987 and a BA from Harvard University. His main research interests are in knowledge organization, ontologies, automatic sentiment categorization, human categorization behaviour, natural language processing, information extraction, multidocument summarization, and clinical decision support systems. He can be reached at <a href="mailto:ASSGKHOO@ntu.edu.sg">ASSGKHOO@ntu.edu.sg</a>.<br />
<strong>Gang Li </strong>is a Professor and the Director in the Centre for Studies of Information Resources at Wuhan University (address: 299 Bayi Street, Wuchang District, Wuhan City, Hubei Province, China, 430072), awarded Changjiang Professor in 2016. He is now researching on big data integration and analysis focusing on multiple aspects of urban information, such as geographical, cultural and scientific data. He has completed and is working on multiple related projects. He can be reached at <a href="mailto:imiswhu@aliyun.com">imiswhu@aliyun.com</a>.<br />
<strong>Jianxing Chi </strong>(corresponding author) is a Lecturer at College of Communication at Fujian Normal University (address: Fujian Normal University Qishan Campus, University Town, Fuzhou City, 350117 ). She received her Master&rsquo;s degree from London School of Economics. She does research in quantitative and qualitative social research and health communication and social media. Her research interest relates to health communication, new media and communication technology. She can be reached at <a href="mailto:chijx@fjnu.edu.cn">chijx@fjnu.edu.cn</a>.</p>

</section>
<section class="refs">

<h2>References</h2>
<ul class="refs">
<li id="bid15">Bidmon, S. &amp; Terlutter, R. (2015). <a href="http://www.jmir.org/2015/6/e156/">Gender differences in searching for health information on the internet and the virtual patient-physician relationship in Germany: exploratory results on how men and women differ and why</a>. <em>Journal of Medical Internet Research, 17</em>(6). Retrieved from http://www.jmir.org/2015/6/e156/ (Archived by WebCite&reg; at http://www.webcitation.org/70AlFw65X)</li>

<li id="che16">Chew, S.H. &amp; Khoo, C.S.G. (2016). Comparison of drug information on consumer drug review sites versus authoritative health information websites. <em>Journal of the American Society for Information Science and Technology, 67</em>(2), 333-349.</li>

<li id="cul02">Cullen, R.J. (2002). In search of evidence: family practitioners' use of the Internet for clinical information. <em>Journal-Medical Library Association, 90</em>(4), 370-379.</li>

<li id="des13">Desjarlais, M. (2013). Internet exploration behaviours and recovery from unsuccessful actions differ between learners with high and low levels of attention. <em>Computers in Human Behaviour</em>, <em>29</em>(3), 694-705.</li>

<li id="fox13">Fox, S. &amp; Duggan, M. (2013). <a href="http://www.pewinternet.org/2013/01/15/health-online-2013/"><em>Health online 2013</em></a>. Pew Research Center. Retrieved from http://www.pewinternet.org/2013/01/15/health-online-2013/ (Archived by WebCite&reg; at&nbsp; http://www.webcitation.org/70AliuZux)</li>

<li id="mor15">Moreland, J., French, T.L. &amp; Cumming, G.P. (2015). <a href="http://www.researchprotocols.org/2015/3/e85/"> The prevalence of online health information seeking among patients in Scotland: a cross-sectional exploratory study</a>. <em>JMIR Research Protocols, 4</em>(3). Retrieved from http://www.researchprotocols.org/2015/3/e85/ (Archived by WebCite&reg; at http://www.webcitation.org/70AmKDgST)</li>

<li id="pia16">Pian, W., Khoo, C.S. &amp; Chang, Y-K. (2016). <a href="http://www.jmir.org/2016/6/e136/">The criteria people use in relevance decisions on health information: an analysis of user eye movements when browsing a health discussion forum</a>. <em>Journal of Medical Internet Research, 18</em>(6), 1-18. Retrieved from http://www.jmir.org/2016/6/e136/ (Archived by WebCite&reg;at http://www.webcitation.org/70AlZW6Td)</li>

<li id="sco16">Scott, G.G. &amp; Hand, C.J. (2016). Motivation determines Facebook viewing strategy: an eye movement analysis. <em>Computers in Human Behavior, 56</em>, 267-280.</li>

<li id="son16">Song, H., Omori, K., Kim, J., Tenzek, K.E., Hawkins, J.M., Lin, W.Y., . . . Jung, J.Y. (2016). <a href="http://www.jmir.org/2016/3/e25/">Trusting social media as a source of health information: online surveys comparing the United States, Korea, and Hong Kong</a>. <em>Journal of Medical Internet Research, 18</em>(3). Retrieved from http://www.jmir.org/2016/3/e25/ (Archived by WebCite&reg; at http://www.webcitation.org/70Am4qqF5 </li>

<li id="teo01">Teo, T.S.H. (2001). Demographic and motivation variables associated with Internet usage activities. <em>Internet Reserch: Electronic Networking Applications and Policy, 11</em>(2), 125-137.</li>

<li id="wan16">Wang, C.Y., Tsai, M.J. &amp; Tsai, C.C. (2016). Multimedia recipe reading: predicting learning outcomes and diagnosing cooking interest using eye-tracking measures. <em>Computers in Human Behaviour</em>, <em>62</em>, 9-18.</li>

<li id="zha12">Zhang, Y. (2012). Consumer health information searching process in real life settings. <em>Proceedings of the Annual Meeting of the American Society for Information Science and Technology</em>, <em>49</em>(1), 1-10.</li>


</ul>
</section>

</body>
 </html>
