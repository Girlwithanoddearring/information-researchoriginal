#### vol 24 no.1, March 2019

##### Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.


# Semiotics based discursive communities in online book reviews

## [Kyunghye Yoon](#author) and [Adam Bezdicek](#author)

> **Introduction**.This paper discusses semiotics as a methodological framework for analysing user generated online book reviews (UGOBR) to identify shared collateral experiences conditional for communities of discourse to arise.  
> **Methods**. Purposely selected UGOBR text from amazon.com were coded and analysed with a triadic semiotic model as a guiding framework.  
> **Analysis**. Coding categories were analysed for common interpretive frames comprised of similar expressed experiences.  
> **Results**. Groupings of collateral experience among reviewers emerged about a book, which seemed to indicate a common interpretive frame.  
> **Conclusion**. The result suggested that reviewers’ meaning created from reading the same book can be identified to form a basis for collateral experience.

<section>

## Introduction

Inquiry on information meaning is one of the central foci of LIS research. Any information activities assume an understanding of the meaning of information for the producer, the user and a processor. Information organization such as indexing or cataloguing involves a process of denoting and interpreting what it means to represent and create a surrogate for retrieval. Information users make meaning of information according to their need and use context. Meaning is embedded in information phenomena during its production, organization, retrieval, use and any intersection of these activities.

The rise of user generated content (UGC) and big data have resulted in an increased focus within LIS on machine processing, natural language recognition, and _semantic textuality_ ([Xiao and Conroy, 2017](#xia17)). Establishing a methodological framework for identifying communities of meaning within UGC would be beneficial to the identification of proto-communities of users in UGC contexts where formal communities and community building features may be lacking. In this paper, we will discuss semiotics as a useful methodological framework for interpretative analysis of user meaning in UGC, using UGOBR as a case study.

</section>

<section>

## Semiosis and community

According to Peirce, ‘_A sign is something, A, which brings something, B, its interpretant sign determined or created by it, into the same sort of correspondence with something, C, its object, as that in which itself stands to C_’ ([Huang and Chuang, 2009](#hua09), p. 342). The process by which people negotiate between these elements to create meaning is dynamic, ongoing, and generative of further signs and semiosis. The end result of the semiosis, or the interpretant, takes the form of a new _sign_ generated from a meaningful interpretation of the relationship between the form a sign takes (representamen) and the concepts/things it represents (object) ([Mai, 2001](#mai01); [Thellefsen, Brier and Thellefsen, 2003](#the03); [Thellefsen, Thellefsen and Sorensen, 2018](#the18)). As such, any new interpretant always bares the possibility that it could in turn become a representamen for a new process of semiosis, potentially stretching into infinity. Peirce grounded these distinctions with a hierarchical phenomenology outlining the varying ways in which a sign’s triadic elements are manifested through human perception, sensation, and cognition. Based on this, he developed 10 sign classes ([Mai, 2001](#mai01)).

He acknowledged that a single sign can mean different things to different people based upon their past experiences with the sign. Via his conception of legisign, or signs as _generalizable laws_, Peirce acknowledged that the only way that language can function and discourse can arise is via shared experience with signs ([Peirce, 1955](#pei55), p. 11). This invites the possibility of the formation of _semiotic communities_, or communities of individuals defined by a shared means of interpreting signs. Several within the field of ILS have begun to think of ways that semiotics might be applied to the study of communities ([Brier, 1996](#bri96); [Friedman and Smiraglia, 2013](#fri13); [Sorenson, Thellefsen and Thellefsen, 2016](#sor16)). Schreiber ([2012](#sch13)) adapts Irvine Goffman’s notion of frame to study of individual users of collaborative learning software employ different framing devices for the interpretation of meaning.

Thellefsen and Thellefsen ([2013](#the13)) supplement this conception of semiotic framing devices by distinguishing between the roles of _collateral experience_ and _discursive communities_ in semiotic processes. They use the concept of _collateral experience_ or the _‘actual knowledge shared by the utterer and interpreter about concepts or phenomena in a communication process’_ ([Thellefsen and Thellefsen, 2013](#the13), p. 1739). The more experience that an individual has with the interpretation of a given representamen, the more limited and defined their interpretations will be. Individuals with shared collateral experiences are better able to arrive at a common understandings of the meaning of a sign, opening up a space where communities of discourse can arise. While discourse communities refer to norms for discussing a given topic, be they ad hoc or highly structured and specialized, _collateral experience_ refers to shared experiences with a given sign that allow communities of discourse to arise.

This distinction between _discourse community_ and communities of _collateral experience_ is an important one to the study of ad hoc discursive communities that may arise along with UGC. While all processes of semiosis will be contextualized within some sort of discursive community, examining book reviews in terms of communities of _collateral experience_ will help to identify common features within individuals that may lead to certain types of interpretations, even in the absence of any formalized community structures.

</section>

<section>

## Semiosis and reading

Peirce’s conception of semiosis provides a methodological framework for unpacking and differentiating between the complex relationship between individual perception and emotion, informational content, and socio-cultural context involved in the process of reading books, writing about books, and understanding the multiplicity of meanings that those books can have for individuals.  Peirce allows us to begin to separate the different semiotic processes that are involved in the review writing process: (1) the semiosis of the book’s author, (2) the semiosis of the reader who writes the book review, and (3) the semiosis of researcher analysing the review.

Applied to Peirce’s triadic model of the sign, the text of the book can be viewed as a collection of signs each of which has a representamen, object, and interpretant.  However, the exact nature of each of these triadic elements changes depending upon whether it is placed within the context of the book’s author, the book’s reader, or the researcher’s analysis of UGOBR.  To the author, the text of the book, as the end result of a semiotic process, can be viewed as the interpretant. The author’s own semiosis, in writing the book, involves interpreting a host of signs and synthesizing this interpretation into collections of rhemes, dicent statements, and arguments. To the reader, the book’s text constitutes a collection of representamen, a host of signs and also an object, what is signified by the book content, the idea that is conveyed in the book by the author. For the reader, the semiotic processes through which they interpret and make sense of the book, as a representamen, involve a complex interplay between their previous collateral experiences and their emotional and cognitive experiences of reading the book. As researchers, we analyse the text of the OBR as the reader’s interpretant (or representamen, from our perspective), employing semiosis as a framework for interpretation and analysis.

</section>

<section>

## UGOBR text analysis

The primary objective of this research was to explore using semiotics as not only a theoretical but also a methodological framework for empirically identifying the underlying experiential basis for shared semiotic frames that result in similar interpretation of signs among sets of individuals that do not necessarily self-identify with a common community. We employed it in the analysis of OBR text for examining a shared collateral experience of book reading. In this paper, we discuss how we used the triadic sign model as a guiding ground for coding and analysis of review text in order to see if and to what degree the collateral experience can be identified in user generated book reviews.

A corpus of 200 reviews was purposively gathered from Amazon (amazon.com) by selecting the top 20 _most helpful_ user reviews for 10 popular science books. The body of the reviews along with relevant metadata were archived coded and analysed.

Coding and analysis were done inductively and iteratively with an interpretative approach, allowing categories to evolve through the iterative process. The analysis started with an individual review to generate a researcher’s re-creation (i.e., researcher’s interpretant) of the meaning combining with the multiple layers of coding categories, then moved across all reviews for a given book, grouping them together by their similarities in meaning and sentiment.

The initial phase focused on categorizing claims by topic to capture the researcher’s meaning of the content as a relationship between the sign vehicle (i.e., review text) and the sign object (i.e., reviewer’s meaning) of Peirce’s triadic sign. A claim was defined as any chunk of a text representing a meaningful assertion and constituting a core unit of linguistic meaning ([Yoon, 2012](#yoo12); [Yoon and Bezdicek, 2015](#yoo15)). It has yielded the following four categories; (1) the _book_ itself and its author pertaining the book value; (2) the _book content_ such as subject topics, theses, themes and summaries; (3) the _reviewer_’s own personal experience, knowledge, preferences, expectations, and other aspects of their personal lives ([Bezdicek and Yoon, 2016](#bez16)); and (4) the _recommendation_.

The second phase of coding and analysis involved shifting focus from our own semiotic processes as researchers to that of the reviewers. At this stage analysis focused on the reviews as interpretants, or the end result of the reviewer’s semiotic processes. Within each of the 4 categories identified in stage 1, subcategories were developed using Peirce’s typology of symbolic legisigns (rheme, dicent, and argument). Where appropriate, sentiment was coded using a combination of star ratings and, at the level of argument, whether the specific claim expressed a positive or negative disposition toward the book

The third phase analysed the reviewer’s meanings across the reviews within a book, where the grouping of reviewers emerged based on the different meanings they created. Reviewer’s personal statements were analysed across reviews to identify shared experiences constitutive of groupings of shared collateral experiences. Then, content statements and critiques were compared against personal statements to identify common interpretative frames among the reviewers. Pairings of similar content and personal statements were analysed and coded one last time to identify common interpretive _frames_ comprised of similar expressed experiences that tended to appear with similar statements about a book’s meaning. Frames were categorized at the level of content/personal pairings rather than at the level of the reviewer to detect instances where multiple frames might be employed within a single review.

Analysis of coding at multiple levels yielded a rich understanding of user meaning within the individual _frame_. It produced grouping of reviewers among which the _collateral experience_ seems to play a critical role. Table 1 shows a breakdown of different sources of framing that were identified within the reviewer/personal category, along with descriptions and examples of the types of arguments deployed within each frame.

<table class="center" style="width:60%;"><caption>  
Table 1: Groupings of collateral experience among reviewers with Sample review text</caption>

<tbody>

<tr>

<th>Reviewer/Personal sources of framing</th>

<th>Description and examples</th>

</tr>

<tr>

<td rowspan="2">Argument positive: e.g., agreement or compliment with the author, book and content that seems to be in relation to or confirm with own knowledge and experience  
</td>

<td style="text-align:left;">Accept the author’s point based on logical thinking  
E.g., ‘because there has been prior training, study, rehearsal that such wise snap judgement is possible’.(Z6 Review 9)  
E.g., ‘All of us make our judgements based on our experience--experience that becomes so ingrained that we can become ultimately unaware of why we know what we know’. (Z6 Review 10)</td>

</tr>

<tr>

<td style="text-align:left;">Argue against based on personal experience  
E.g., ‘My credentials match the author's and I'm at least as well researched as she is, so I feel confident when I say this book muddies the waters more than parts them on the subject of introversion/extroversion’. (Z14 Review 11)  
E.g., ‘And if, like me, you're an introvert who has been criticized for not liking small talk or socializing activities or been told that you think too much, this book will come as a great relief’. (Z14 Review 12)</td>

</tr>

<tr>

<td rowspan="3">Argument negative: e.g., disagreement with or objection to the author, book and content based on reviewer’s own knowledge, experience and world view  
</td>

<td style="text-align:left;">Criticizing logical weakness  
E.g., ‘the book is a little too anecdotal... Too often conclusions are made on basic handwaving’, ‘the whole thing derails into examples that just don’t seem appropriate for the topic’. (Z6 Review 1)  
E.g., ‘doesn’t arrive at a coherent and usable conclusion’. (Z6 Review 14)</td>

</tr>

<tr>

<td style="text-align:left;">Didn’t apply to one’s life or personal experience  
E.g., ‘As an introvert myself I think it missed part of the whole picture’. (Z14 Review 4)  
E.g., ‘My credentials match the author's and I'm at least as well researched as she is, so I feel confident when I say this book muddies the waters more than parts them on the subject of introversion/extroversion’. (Z14 Review 11)</td>

</tr>

<tr>

<td style="text-align:left;">Didn’t meet one’s expectation  
E.g., ‘As I read through ‘Part one: the extrovert ideal’, I thought I had a winner. I thought I had a book that I was going to be able to say to all of you out there that are introverts and writers (because most actual writers are introverts (and I say it that way to exclude people (celebrities) who write books but don't really do the actual writing)), ‘You need to read this book’! But, as it turns out, I was wrong’. (Z14 Review 13)  
E.g., ‘This is surprisingly patronizing, given its title. I hoped for a manifesto. What I got was pablum’. (Z14 Review 14)</td>

</tr>

<tr>

<td>Argument ambivalent or neutral  
</td>

<td style="text-align:left;">Split views on different aspects  
E.g., ‘An excellent theory harmed by very poor examples’. (Z6 Review 13)  

Reviewer followed and took author’s points, still disappointed because it didn’t meet expectation  
E.g., ‘Sound decision making is borne of a balance between instinctive, reactive realization and deliberate, studied conclusions. I think he pretty well proves the point, but he doesn’t really give us a road map for finding that balanced place’. (Z6 Review 11)  
E.g., ‘with experience comes intuition’. ‘hoping ...a discussion on how to hone yourself to make better ‘blink’ decisions outside your field of expertise. The author does not address this at all...’. (Z6 Review 15)</td>

</tr>

</tbody>

</table>

</section>

<section>

## Discussion and conclusion

This paper explored using semiotics as a methodology for identifying shared interpretative framing devices deployed by writers of UGOBRs who expressed similar collateral experiences. Semiotics provided a methodological basis for bridging the conceptual relations of signs with empirical data analysis and presented semiotics as a useful methodological framework for untangling the distinct semiotic processes of the authors of the books being reviewed, the reviewers writing reviews of the book, and the researchers’ own processes of interpreting the OGBRs. The analysis suggested that reviewer’s meaning created from reading, more specifically based on his or her own knowledge and experience, can be identified as a _frame_ to interpret the sign object. These frames are grounded in similar collateral experiences and may be deployed to identify proto-communities of users in UGC contexts where formal communities and community building features may be lacking.

</section>

<section>

## About the authors

**Kyunghye Yoon** Associate Professor, in the MLIS Program at St. Catherine University, 2004 Randolph Ave. St. Paul, MN 55105\. She received her PhD from Syracuse University. Her research interests include information seeking behavior, human information interaction, accessibility and user-experience design. She can be contacted at [kyoon@stkate.edu](mailto:kyoon@stkate.edu).  
**Adam Bezdicek** Librarian at Pine Technical and Community College, 400 4<sup>th</sup> St. SE, Pine City, MN 55063\. He received his MA in English Literature from the University of St. Thomas and MLIS from St. Catherine University.  His research interests include data librarianship, the digital humanities, and information literacy instruction. He can be contacted at [adam.bezdicek@pine.edu](mailto:adam.bezdicek@pine.edu).

</section>

<section class="refs">

## References

*   Bezdicek, A. & Yoon, K. (2016). [Investigating reading appeal in user generated online book reviews: reading as a meaning making process.](http://hdl.handle.net/2142/89334) In _Proceedings of the iConference 2016\._ Retrieved from http://hdl.handle.net/2142/89334
*   Brier, S. (1996). Cybersemiotics: a new interdisciplinary development applied to the problems of knowledge organization and document retrieval in information science. _Journal of Documentation_, _52_(3), 296-344.
*   Friedman, A. & Smiraglia, R.P. (2013). Nodes and arcs: concept map, semiotics, and knowledge organization. _Journal of Documentation, 69_(1), 27-48.
*   Huang, A. W.-C. & Chuang, T.-R. (2009). Social tagging, online communication, and Peircean semiotics: a conceptual framework. _Journal of Information Science, 35_(3) 340–357.
*   Mai, J. (2001). Semiotics and indexing: an analysis of the subject indexing process. _Journal of Documentation_, _57_(5), 591-622.
*   Peirce, C.S. (1955). _Philosophical writings of Peirce_. New York, NY: Dover Publications.
*   Schreiber, C. (2013). Semiotic processes in chat-based problem-solving situations. _Educational Studies in Mathematics_, _82_(1), 51-73.
*   Sorenson, B., Thellefsen, T. & Thellefsen, M. (2016). The meaning creation process, information, emotion, knowledge, two objects, and significance-effects: some peircean remarks. _Semiotica,_ 208, 21-33.
*   Thellefsen, T., Brier, S. & Thellefsen, M. (2003). Problems concerning the process of subject analysis and the practice of indexing. _Semiotica,_ 144, 177-218.
*   Thellefsen, T. & Thellefsen, M. (2013). Emotion, information, and cognition, and some possible consequences for library and information science. _Journal of the American Society for Information Science and Technology_, _64_(8), 1735-1750.
*   Thellefsen, M., Thellefsen, T. & Sorensen, B. (2018). Information as signs: a semiotic analysis of the information concept, determining its ontological and epistemological foundations. _Journal of Documentation, 74_(2), 372-382.
*   Xiao, L. & Conroy, N. (2017). Discourse relations in rationale-containing text-segments. _Journal of the Association for Information Science and Technology_, _68_(12), 2783-2794.
*   Yoon, K. (2012). Conceptual syntagmatic associations in user tagging. _Journal of American Society for Information Science and Technology_, _63_(5), 923–935.
*   Yoon, K. & Bezdicek, A. (2015). Using syntagmatic relations to examine semantic communities in user generated book reviews. _Proceedings of the 78th Association of Information Science and Technology Annual Meeting, 52_(1), 1-3.

</section>

</article>