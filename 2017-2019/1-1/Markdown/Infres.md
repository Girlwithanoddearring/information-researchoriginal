<table>

<tbody>

<tr>

<td>

## Volume 1 No 1 April 1995

#### _Information Research: an electronic journal,_ is published three times a year by Professor T.D. Wilson of the Department of Information Studies, University of Sheffield.

* * *

## Contents

#### [Introduction](#intro)

#### [A note on the electronic publication of _Information Research_](#note)

#### [Non-hierarchic document clustering using a genetic algorithm](paper1.html)  
G.Jones, A.M. Robertson, C. Santimetvirul, and P. Willett.

#### [An action research approach to curriculum development](paper2.html)  
P. Riding, S.P. Fowell, and P.C.M. Levy

#### [Information systems strategy formation in higher education institutions](paper3.html)  
David K. Allen

* * *

### <a name="intro"></a>**Introduction**

This first issue of _Information Research_ contains three papers originally published in the print predecessor, _Information Research News ._ There is a considerable amount of work going on in the Computational Information Systems Research Group within the Department on the use of genetic algorithms in database-processing applications: the paper by Jones et al. describes a recent MSc Information Management dissertation project that considered their use for clustering document databases. There is increasing interest within the Department in electronic support for collaborative teaching and learning, and the paper by Riding et al. discusses current work on the development of an introductory undergraduate module on the use of computer-mediated communications. Finally, Allen reports some of the initial work in a doctoral study of the information strategies that are currently being developed in higher education institutions in the United Kingdom.

### <a name="note"></a>**A note on the electronic publication of Information Research**

This is a trial issue of an electronic version of _Information Research News_ which publishes short papers by staff and students of the Department. We are now exploring the possibility of regular publication of _Information Research_ to take over that function, and would welcome comments from present subscribers to the paper version, as well as from those who have not previously subscribed.

Copyright of papers accepted for _Information Research_ rests with the author(s) and publication in this form is not intended to limit revision and subsequent publication elsewhere.

* * *

Back to the [Contents](#cont)

* * *

_Information Research_ is designed, maintained and published by by [Professor Tom Wilson.](mailto:t.d.wilson@sheffield.ac.uk) Design and editorial content © T.D. Wilson, 1995

* * *

</td>

</tr>

</tbody>

</table>