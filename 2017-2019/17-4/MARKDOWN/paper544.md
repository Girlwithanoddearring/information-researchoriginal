#### vol. 17 no. 4, December, 2012

# Modelling historians' information-seeking behaviour with an interdisciplinary and comparative approach

#### [Hea Lim Rhee](#author)  
Department of R&D System Development, NTIS Center, Korea Institute of Science and Technology Information, Daejeon, Republic of Korea.

#### Abstract

> **Introduction.** The information-seeking behaviour of historians may be different from that of other user groups in libraries and archives and historical research may be unique.  
> **Method.** This research combined interdisciplinary and comparative approaches. It compared findings about historians' information-seeking behaviour, obtained from a literature review in the fields of library and information science, archival studies, and history, to a model of social scientists' information-seeking behaviour developed by Meho and Tibbo.  
> **Analysis.** This research employed content analysis and Glaser and Strauss's grounded theory, with the assistance of qualitative data analysis software NVivo8.  
> **Results.** The results show that the significance, degree, and rationale of the features and stages of historians' information-seeking behaviour differ from those of social scientists. The features of the two groups' information-seeking behaviour overlap, but historians have three additional features: orienting, constructing contextual knowledge, and assessing. These differences between the two groups' information-seeking behaviour seem to derive from 1) the fact-driven nature of the historical discipline and 2) historians' more frequent use of archives and archival materials. The results of this research led to the development of a model of historians' information-seeking behaviour throughout the entire historical research process.  
> **Conclusions.** The findings of this research led to recommendations for practitioners in the archival and library fields on how to improve information systems and services for historical research. This research's methodology, literature review, and model form a foundation for future research in library and information science and archival studies on historians' information-seeking behaviour.

## Introduction

Historians form one of the largest user groups in both libraries and archives. However, despite the persistent efforts of researchers in the fields of archives and library and information science, historians still claim that librarians and archivists do not accurately or fully understand the unique nature of historical research and their information-seeking behaviour (e.g., [Rosenberg and Swierenga 1993](#rosenberg1993): 53). Gordon suggests that historians have 'their own systems which may not be recognizable as such to librarians and archivists' ([2001](#gordon2001): 11). A number of researchers in the library and information science and archival fields also acknowledge a gap between archivists and historians, and a lack of consideration of historians' information-seeking behaviour in archival and library systems and services (e.g., [Cole 2000a](#cole2000a); [Cook 2009](#cook2009)). In addition, the conventional thinking that 'history falls somewhere between the humanities and social sciences groups' ([Cole 2000a](#cole2000a): 88) seems to imply that the discipline of history-and hence historians' information-seeking behaviour-is unique.

The objectives of this study are to identify how and why historians' information-seeking behaviour and historical research are unique and to develop a broad, flexible, hypothetical process model of it. To do so, this study reviewed relevant literature on historians' information-seeking behaviour in library and information science, archival studies, and history and compared historians' information-seeking behaviour with that of social scientists. This study also aims to combine interdisciplinary and comparative approaches to investigate the information-seeking behaviour of users in the library and information science and archival fields and to distinguish historians' information-seeking behaviour from that of social scientists, which is more commonly studied. The interdisciplinary nature of the literature review enabled the author to identify, for the first time, some similarities and differences between historians' information-seeking behaviour (in libraries and archives) as reported in (a) user studies in library and information science, (b) archival user studies, and (c) historians' own writings.

This study maps features of historians' information-seeking behaviour, identified in the literature review, from the three disciplines to the generic features of such behaviour by social scientists, identified by Meho and Tibbo. It also indicates three new features of historians' information-seeking behaviour. By comparing this behaviour of historians with that of social scientists, this study empirically shows how historical research is unique, in particular how historians' information-seeking behaviour is different from that of social scientists. The Conclusions section of this paper extends the study's findings to recommend practical improvement of information systems and services for historians.

#### Conceptual framework

The author selected Meho and Tibbo's model as a theoretical foundation for developing her own model of historians' information-seeking behaviour for two reasons. First, Meho and Tibbo's model is based on David Ellis's behavioural model of social scientists(<a>1989a</a>, [1989b](#ellis1989b)), which subsequent studies have used. Secondly, Meho and Tibbo's model includes new generic features of social scientists' information-seeking behaviour that reflect the development of information technology in the digital age, particularly the Internet, since Ellis's studies (<a>1989a</a>, [1989b](#ellis1989b)). Using Meho and Tibbo's model was expected to help identify unique information-seeking activities of historians by comparing their behaviour to that of social scientists. The author also referred to features of social scientists' information-seeking behaviour in Meho and Tibbo's model to create categories and nodes in NVivo8 for deductive content analysis. The following section will fully explain Meho and Tibbo's model.

#### Meho and Tibbo's model of social scientists' information-seeking behaviour

Meho and Tibbo ([2003](#meho2003)) presented a behavioural model of social scientists' information-seeking behaviour by investigating social science faculty studying stateless nations. Their study ([2003](#meho2003)) found four new generic features of social scientists' information-seeking behaviour accessing, networking, verifying, and information-managing) in addition to Ellis's ([1989a](#ellis1989a), [1989b](#ellis1989b)) six generic features, resulting in the following ten characteristics:

*   _Starting_ consists of the initial search for information sources and developing an overview of relevant literature in a research area. Common starting activities include identifying references, making personal contacts, and consulting literature reviews, online public access catalogues (OPACs), bibliographies, abstracts, and indexes.
*   _Chaining_ involves activities following chains of citation or reference connections between sources or materials found in starting activities.
*   _Browsing_ is characterised by activities associated with casually looking for information in areas of potential interest. It includes scanning published issues of journals, tables of contents of books, OPACs, abstracts, indexes, and Web sources.
*   _Differentiating_ applies known differences (e.g., reputation of authors and journal rankings) between sources to filter the quantity, quality, and nature of information found.
*   _Extracting_ is characterised by activities associated with working systematically through particular sources (e.g., runs of periodicals, series of monographs, computer databases, and collections of indexes, abstracts, or bibliographies) and selectively identifying relevant material from those sources.
*   _Verifying_ involves activities that check the accuracy of information found.
*   _Monitoring_ involves activities associated with keeping up with research developments in a research area of interest by continually following particular sources (e.g., journals, listservs, publishers' catalogues, and Web sources) and informally contacting others (e.g., colleagues and students).
*   _Accessing_ is characterised by activities associated with getting at the material or information sources that are identified and located.
*   _Networking_ involves activities associated with communicating with and maintaining close relationships with other people (e.g., colleagues and government officials).
*   _Information-managing_ involves activities associated with filing, organising, and archiving information collected or used for research.

Furthermore, Meho and Tibbo broke down the information-seeking activities of social scientists into four interrelated stages (searching, accessing, processing, and ending), as Figure 1 shows ([2003](#meho2003): 584-585).

*   _Searching stage:_ where identifying relevant and potentially relevant materials is begun.
*   _Accessing stage:_ the bridging stage between the searching stage and the processing stage.
*   _Processing stage:_ where the collected information is synthesised and analysed and where a paper is written as a final product.
*   _Ending stage:_ where the cycle of a research project terminates.

<figure>![Stages in the information-seeking behaviour of academic social scientists](p544fig1.png)

<figcaption>  
Figure 1: Stages in the information-seeking behaviour of academic social scientists <details><summary>Source</summary> Meho and Tibbo (2003: 584).</details></figcaption>

</figure>

## Methodology

To gain an holistic and wide-ranging base of information about how historians conduct their research, the author examined, analysed, and synthesised library and information science literature, archival literature, and history literature on historians' information-seeking behaviour. The breadth of the literature review was intended to capture the diverse, unexpected, unintended, and often instinctual characteristics of historians' information-seeking behaviour across the entire historical research process.

The author collected literature on historians' information-seeking behaviour by searching databases (e.g., ERIC, JSTOR, Library and Information Science Abstracts, and Library, Information Science & Technology Abstracts), citations, and bibliographies of relevant literature and by reading materials on the syllabi of relevant university courses (e.g., on information-seeking behaviour and historical research methods). The author searched on various terms, both as subject terms and keywords, in basic and Boolean searches: 'historical research method', 'historian AND information seeking', 'historian AND research method', 'historian AND information retrieval', 'historian AND information needs', 'historian AND information organization', 'historian AND library', 'historian AND archive', 'historian AND information source', 'historian AND archival source', 'historian AND primary source', and others. The author also received literature recommendations from reference librarians and professors with an academic background in one or more of the three fields.

The author included in the literature review only those information-seeking behaviour studies, in the fields of library and information science and archival science, that focused on historians alone and as a distinct group, not as a subset of social scientists or humanists. Of the body of historians' own writings on their own information-seeking behaviour, the author selected a subset of the huge volume of handbooks, guidebooks, and methodology books about historical research by consulting (a) the syllabi of relevant university courses (e.g., 'Philosophy of History' from the University of Virginia, 'Introduction to Historical Method' from Tennessee Technological University, 'Historical Methods' from Virginia Tech, 'Historiography' from the University of New Hampshire, and 'Historiography and Historical Methods' from the Bloomsburg University of Pennsylvania), (b) librarians, and (c) professors with an academic background in history. To be included in this study, a book had to be cited three or more times in one or more of the sources listed above.

All forty-six publications analysed in this study are marked by an asterisk in the References section, and are broken down by topic and type of publication (chapter, article, book, etc.) in Table 1.

<table class="center" style="width:100%;"><caption>  
Table 1: Number of reviewed publications by type and topic (with percentage of total)</caption>

<tbody>

<tr>

<th rowspan="2">Publication Type</th>

<th colspan="2">Topic</th>

<th style="width:15%;"> </th>

</tr>

<tr>

<th>Library and information science and archival user studies on historians' information-seeking behaviour</th>

<th>Historians' writing on their information-seeking behaviour and historical research</th>

<th>Total</th>

</tr>

<tr>

<td>Research papers (journal)</td>

<td style="text-align:center;">20</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">21 (45.7%)</td>

</tr>

<tr>

<td>Essays (journal)</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">9 (19.6%)</td>

</tr>

<tr>

<td>Handbooks, guidebooks and methodology books about historical research</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">7</td>

<td style="text-align:center;">7 (15.2%)</td>

</tr>

<tr>

<td>Books or book chapters</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">6 (13.0%)</td>

</tr>

<tr>

<td>Conference papers</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">2 (4.3%)</td>

</tr>

<tr>

<td>Doctoral dissertations</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">1 (2.2%)</td>

</tr>

<tr>

<td style="text-align:center">Total</td>

<td style="text-align:center;">23 (50.0%)</td>

<td style="text-align:center;">23 (50.0%)</td>

<td style="text-align:center;">46 (100%)</td>

</tr>

</tbody>

</table>

The literature on historians' information-seeking behaviour reviewed in this study was published between 1965 and 2010 (see Table 2). Although more than half were published after 2000, only a few publications examined how historians interact with recent information technology for their research. The dearth of information on this topic opened some questions on this behaviour in contemporary historians, which will be addressed in the _Features of historians' information-seeking behaviour_ section.

<table class="center" style="width:70%;"><caption>  
Table 2: Publication years of reviewed literature</caption>

<tbody>

<tr>

<th rowspan="1">Publication Year</th>

<th colspan="1">Number of Publications</th>

</tr>

<tr>

<td style="text-align:center;">1960—1969</td>

<td style="text-align:center;">1 (2.2%)</td>

</tr>

<tr>

<td style="text-align:center;">1970—1979</td>

<td style="text-align:center;">2 (4.3%)</td>

</tr>

<tr>

<td style="text-align:center;">1980—1989</td>

<td style="text-align:center;">5 (10.9%)</td>

</tr>

<tr>

<td style="text-align:center;">1990—1999</td>

<td style="text-align:center;">12 (26.1%)</td>

</tr>

<tr>

<td style="text-align:center;">2000—2009</td>

<td style="text-align:center;">25 (54.3%)</td>

</tr>

<tr>

<td style="text-align:center;">2010</td>

<td style="text-align:center;">1 (2.2%)</td>

</tr>

<tr>

<td style="text-align:center">Total</td>

<td style="text-align:center;">46 (100.0%)</td>

</tr>

</tbody>

</table>

The author employed content analysis and grounded theory, with the assistance of NVivo8 software, to analyse the data. She used content analysis to compare 'similar phenomena inferred from different bodies of texts' and identify 'differences among the inferences drawn from texts'([Krippendorff 2004](#krippendorff2004): 93). Following Ellis's groundbreaking use of grounded theory to develop a behavioural model for social scientists ([1989a](#ellis1989a), [1989b](#ellis1989b)) and researchers ([1993](#ellis1993)), the author also used grounded theory employing the constant comparative method ([1967](#glaser1967)) to derive a hypothetical model of historians' information-seeking behaviour. NVivo8 software was used to facilitate the approach ([Lonkila 1995](#lonkila1995)) and to help 'develop a model from a tentative conceptual framework' ([Smyth 2006](#smyth2006): 6).

For data analysis, the author determined that the smallest unit of analysis would be a single sentence of text. She created preliminary categories based on Meho and Tibbo's generic features of social scientists' information-seeking behaviour ([2003](#meho2003)). Those categories were represented as tree nodes in a coding structure of NVivo8\. Each tree node had three child nodes (library and information science literature, archival literature, and history literature). With the preliminary tree node structure in place, the author read all selected publications.

In the data analysis process, the author also used open coding by applying the constant comparative method. Any available PDF versions of publications were imported into NVivo8\. Then the author selected and coded text excerpts to the appropriate tree nodes. For publications not available in electronic form, the author created a Microsoft Word file for the publication, selected and entered text excerpts relevant to historians' information-seeking behaviour, and imported the file into NVivo8\. Then she coded the excerpts to the appropriate tree nodes.

Furthermore, some free nodes were created for any new concepts that were not features of Meho and Tibbo's model but were assumed to be features of historians' information-seeking behaviour. As the data analysis progressed, free nodes representing similar concepts were merged, using the search capabilities of NVivo8 to facilitate categorisation. Finally, the author identified three free nodes that represent historians' information-seeking behaviour features which are not shared by social scientists: orienting, constructing contextual knowledge, and assessing. The author defined orienting as becoming acquainted with information repositories (e.g., archives and libraries), including their holdings and information systems. The orienting node merged similar concepts such as _being accustomed_, _familiarising_ and _educating themselves_. _Constructing contextual knowledge_ was defined as activities conducted to understand, interpret and frame the context of records relevant to a research subject as well as the context of the research subject itself. This node merged concepts such as _understanding and framing context_, _re-creating context_, and _understanding circumstances of a research subject_. _Assessing_ was defined as the evaluation of the information found and the determination of the significance or value of collected information sources and materials to the overall project. The assessing node merged similar concepts such as _appraising_, _evaluating_ and _valuing_. The three free nodes were moved to the tree node structure and the textual analysis was repeated to confirm and finalise the coding and the coding structure.

## Terms

The three fields under examination in this study (library and information science, archival studies, and history), and even individual authors within each field, use somewhat different terminology for the same things. For example, _direct sources_ in library and information science (though even that term is not consistently used in that field) are usually called _primary sources_ in archival studies and history. Additionally, some terms mean different things to different fields or authors. For instance, researchers in library and information science use _information sources_ to ' ' while historians often use the term to mean 'primary and secondary material that is the raw material of their research'([Dalton and Charnigo 2004](#dalton2004): 401). This paper uses terms as they are written in the reviewed literature.

However, two main concepts. _historians_ and _information-seeking behaviour_, need to be clarified. Most historians in the reviewed literature were history faculty members and history doctoral students, and few were professional historians. Information-seeking behaviour is defined as:

> the purposive seeking for information as a consequence of a need to satisfy some goal. In the course of seeking, the individual may interact with manual information systems (such as a newspaper or a library), or with computer-based systems (such as the World Wide Web)' ([Wilson 2000](#wilson2000): 49)

.

Following this definition, this study considered information-seeking behaviour in its broadest sense.

## Findings

Historians' writings reviewed in this study fell into two types. The essays, books, and book chapters presented subjective narratives of the authors' individual research strategies and processes, as well as the reasons for their research activities. They supported findings in archival studies and library and information science and also provided additional information on historians' information-seeking behaviour. However, historians' own subjective, individual narratives of historical research cannot be generalised. On the other hand, the second type of historians' writings, history handbooks, guidebooks, and research methodology books, gave generic, objective instructions on how to conduct historical research. This study included such prescriptive writings because they were expected to help identify the unique characteristics of historical research, some reasons behind historians' information-seeking activities, reasons why historians' information-seeking behaviour is different from that of social scientists, and factors uniquely affecting the historical research process. Though these instructional texts probably reflect accepted research methods, they do not necessarily reflect actual practice. However, the other publications reviewed in this study strongly indicated that historians indeed tend to follow the instructional texts reviewed in this study, as shown in the _Features of historians' information-seeking behaviour_ section. All three disciplines' literature supported each other's findings and compensated for each other's drawbacks.

### Features of historians' information-seeking behaviour

In this and the following sections, _historians_ refers to the historical researchers investigated in the library and information science and archival literature reviewed in this study and the authors of the historical research literature reviewed in this study. _Social scientists_ refers to researchers investigated in Meho and Tibbo's study ([2003](#meho2003)).

This study found that the information-seeking behaviours of historians and social scientists share the same features and stages identified by Meho and Tibbo. It also identified three additional features for historians: orienting, constructing contextual knowledge, and assessing. This section compares the features of historians' and social scientists' information-seeking behaviour. It should be noted that these features are neither entirely nor always sequential. Historians do not always conduct activities subsumed under each feature exclusively; they move back and forth between activities of different features and can even conduct them concurrently. To analyse this study's data, the author counted the number of instances of each feature of historians' information-seeking behaviour and the number of publications containing them (see Table 3). Because many publications included more than one instance of one feature, the number of instances is greater than the number of publications.

<table class="center"><caption>  
Table 3: Numbers of instances of historians' information-seeking behaviour features and of the reviewed publications describing them</caption>

<tbody>

<tr>

<th>Features</th>

<th>Number of instances</th>

<th>Number of publications (% of total)</th>

</tr>

<tr>

<td>Starting</td>

<td style="text-align:center;">38</td>

<td style="text-align:center;">14 (30.4%)</td>

</tr>

<tr>

<td>Chaining</td>

<td style="text-align:center;">33</td>

<td style="text-align:center;">17 (37.0%)</td>

</tr>

<tr>

<td>Browsing</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">12 (26.1%)</td>

</tr>

<tr>

<td>Differentiating</td>

<td style="text-align:center;">55</td>

<td style="text-align:center;">20 (43.5%)</td>

</tr>

<tr>

<td>Extracting</td>

<td style="text-align:center;">18</td>

<td style="text-align:center;">5 (10.9%)</td>

</tr>

<tr>

<td>Verifying</td>

<td style="text-align:center;">27</td>

<td style="text-align:center;">16 (34.8%)</td>

</tr>

<tr>

<td>Monitoring</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">6 (13.0%)</td>

</tr>

<tr>

<td>Accessing</td>

<td style="text-align:center;">56</td>

<td style="text-align:center;">17 (37.0%)</td>

</tr>

<tr>

<td>Networking</td>

<td style="text-align:center;">49</td>

<td style="text-align:center;">21 (45.7%)</td>

</tr>

<tr>

<td>Information-managing</td>

<td style="text-align:center;">21</td>

<td style="text-align:center;">15 (32.6%)</td>

</tr>

<tr>

<td>Orienting</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">12 (26.1%)</td>

</tr>

<tr>

<td>Constructing contextual knowledge</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">18 (39.1%)</td>

</tr>

<tr>

<td>Assessing</td>

<td style="text-align:center;">38</td>

<td style="text-align:center;">28 (60.9%)</td>

</tr>

</tbody>

</table>

#### Starting

Historians, like social scientists, begin the initial search for information related to new research topics and relevant sources by using both traditional tools (e.g., indexes and abstracts) and electronic tools (e.g., online finding aids and OPACs). Nevertheless, the archival and library and information science literature indicates that historians still tend to prefer traditional print tools over electronic tools (e.g., [Anderson 2004](#anderson2004); [Tibbo 2003](#tibbo2003)). For example, three successive library and information science studies conducted at intervals of approximately ten years report that historians still usually start with footnotes, bibliographies of recent books, and journal articles to discover relevant information ([Dalton and Charnigo 2004](#dalton2004); [Stieg 1981](#stieg1981); [Tibbo 1993](#tibbo1993)). On the other hand, several archival studies published in the late 1970s and 1980s claimed that typical formal reference sources such as the National Union Catalog of Manuscript Collections are relatively ineffective and are not heavily used (e.g., [Conway 1986](#conway1986); [Lytle 1980](#lytle1980); [Orbach 1991](http://orbach1991)). However, more current studies demonstrated that many historians still use printed finding aids (e.g., [Duff and Johnson 2002](#duff2002); [Tibbo 2003](#tibbo2003)).

It is not clear whether historians do not frequently use electronic tools because historians do not prefer such tools or because such tools are not available to them. However, the continual development of technology will almost certainly lead to more efficient electronic tools for historical research, so historians may use them more.

Regardless of the form of finding aids used by historians, previous studies indicate that historians use several kinds of access points to identify relevant materials, predominantly names (e.g., names of people and organisations, place names, and event names) rather than subjects (e.g., [Duff and Johnson 2002](#duff2002); <a>Cole 2000b</a>).

In addition to literature searches, historians also use personal contacts for starting. Historians more often use their colleagues and other historians as informal resources than they use librarians (see, e.g., [Dalton and Charnigo 2004](#dalton2004); [Delgadillo and Lynch 1999](#delgadillo1999)). However, historians frequently interact with archivists, believing that archivists are important in locating relevant sources and materials (see, e.g., [Duff _et al._ 2004a](#duff2004a); [Orbach 1991](#orbach1991)).

It is noteworthy that historians in three countries show slightly different behaviour when first seeking information for their research. Dalton and Charnigo's study ([2004](#dalton2004)) in America and the study by Duff _et al._ ([2004b](#duff2004b)) in Canada show that Canadian historians locating archival sources rely much more on informal communications with archivists and colleagues than American historians locating information sources, including archival sources. However, Anderson's study ([2004](#anderson2004): 98) reported that historians in the United Kingdom use 'informal leads' (e.g., asking colleagues, browsing stacks, and serendipity) as much as print-based retrieval methods, such as following leads in articles and books, when locating primary sources. These differences may be due to differences in the questions participants were asked, the cultural differences affecting the information-seeking behaviour of historians of different nationalities, and the kinds of investigated sources (archival sources versus information sources versus primary sources).

#### Orienting

This study's data analysis indicates that orienting needs to be considered as an additional feature of historians' information-seeking behaviour even though the feature is not shared by social scientists (see Table 3). Orienting is characterised by activities associated with becoming informed about and acquainted with information repositories (e.g., archives and libraries), including their holdings and access tools. Indeed, historians' own writings report that historians orient themselves to archives, libraries, OPACs, finding aids, and collections (e.g., [Bolton 2002](#bolton2002); [Gordon 2001](#gordon2001); [Griffen-Foley 2002](#griffen-foley2002)). Historians may orient more than other researchers because historians tend to use multiple archives and libraries and various types of materials. Hence, orienting activities seem frequently to co-occur with starting activities, particularly when employing new access tools in unfamiliar information repositories.

Because historians are usually less familiar with the archival environment than the library environment, it seems likely that they need to do more orienting activities in archives. In particular, historians have difficulties using finding aids in archives, and they make efforts to learn how to use them (see, e.g., [Duff and Johnson 2002](#duff2002); [Howell and Prevenier 2001](#howell2001)). Historians' frequent use of archives, their efforts to understand archival systems (e.g., finding aids, organisation and structure of archival collections), and the significance of using archival materials appear in many publications reviewed in this study, particularly in historians' writings (e.g., [Appleby _et al._ 1994](#appleby1994); [Rundell 1970](#rundell1970)).

#### Chaining

Historians' propensity for following citations probably derives from historical literature's emphasis on the importance of citations as tools for locating relevant sources for research projects, as references for checking sources, and as markers of intellectual history that chart changing research in a subject area and changing researchers' perspectives and views (see, e.g., [D'Aniello 1993](#daniello1993); [Evans 1999](#evans1999); [Grafton 1997](#grafton1997)). Gordon, a historian, claims that footnotes are 'the core of the discipline' ([2001](#gordon2001): 12).

The results of the data analysis suggest that backward chaining is important to historians, just as Meho and Tibbo claim it is important for social scientists. As soon as historians become immersed in research projects, they mainly follow the footnotes and bibliographies of secondary works related to their topics (see, e.g., [Duff _et al._ 2004b](#duff2004b); [Gordon 2001](#gordon2001); [Orbach 1991](#orbach1991)).

Even though citations are important to historians, the literature reviewed in this study rarely showed that historians use citation indexes such as the Social Science Citation Index for citation searching, which matches infrequent usage by social scientists. The reasons are not clear. Most historians might not know about the Index, or perhaps it is not convenient for them. Though the advancement of information technology has allowed for or improved citation databases (e.g., Web of Science), their use by historians was rarely mentioned in the literature reviewed for this study.

In historical research, it seems natural that chaining activities could lead to new starting activities. Many historians frequently start a new research project by following citations and references of secondary literature and seeking out materials that are cited (see, e.g., [Duff _et al._ 2004a](#duff2004a), <a>2004b</a>; [Tibbo 1993](#tibbo1993)). Following the citations of one research project could spark a new research idea and starting activities for a new research project.

#### Browsing

This study's data analysis indicates that browsing is important to historians, just as Meho and Tibbo claim it is important for social scientists. It indicates that historians tend to browse eagerly, expecting to serendipitously find relevant sources. This strategy does sometimes pay off (see, e.g., [Dalton and Charnigo 2004](#dalton2004); [Delgadillo and Lynch 1999](#delgadillo1999)). Many historians intentionally browse print sources, bookshelves, results of computerised bibliographic retrieval systems, and Web search results in the hope of encountering sources they otherwise might miss (see, e.g., [Delgadillo and Lynch 1999](#delgadillo1999); [Elam 2007](#elam2007); [Rose 2002](#rose2002)). This seems to support Orbach's finding (1991) that most historians prefer high recall to high precision. This leads to the assumption that historians want to browse lots of search results in order to accidentally find unexpected but relevant sources that might be missed otherwise.

The difficulty in locating a specific archival item, due to hard-to-use or inadequate finding aids and archival description, may also increase the significance of browsing and historians' reliance on serendipity while browsing. Archives describe their holdings at the collection level, so they do not individually describe each archival item (e.g., a letter from a king to his wife on a certain date). Because archival description of a collection does not provide all information on relevant or potentially relevant items it contains, historians may want or need to scan all items in the collection's boxes and folders. This study suggests that browsing bookshelves, archival boxes, and archival folders needs to be considered a main type of browsing in addition to two other main types of browsing conducted by social scientists identified in Meho and Tibbo's study: (1) the scanning of recently published issues of journals and tables of contents of relevant books; and (2) browsing the online catalog, indexes, and abstracts, web resources, and references of materials found and/or read (Meho and Tibbo [2003](#meho2003): 580).

#### Differentiating

Historians' differentiating activities appear in many publications reviewed in this study (see Table 3). The analysis of the literature review indicates similarities and differences in the differentiating feature of historians' and social scientists' information-seeking behaviour. Historians and social scientists both apply several criteria for differentiating decisions: nature, quality, relative importance, usefulness, and identity or origin of information sources. However, historians also seem to use additional criteria: evidential value and format of information sources and materials (see, e.g., [Duff _et al._ 2004a](#duff2004a); [Graham 2001](#graham2001)).

Historians often make differentiating decisions based on the evidential value of information sources and materials (see, e.g., [Cohen 2002](#cohen2002); [Howell and Prevenier 2001](#howell2001)). That is, historians differentiate and select information sources and materials that they can use as evidence in historical argument. This may be because the nature of their discipline is to pursue facts. The discipline of history, including its literature, research methods, and education, requires historians to evaluate identified sources for use as evidence in historical arguments (see, e.g., [Evans 1999](#evans1999); [Howell and Prevenier 2001](#howell2001)).

To select sources that they can use as evidence, historians check the authenticity and reliability of sources (see, e.g., [Evans 1999](#evans1999); [Howell and Prevenier 2001](#howell2001)), for which they have '_routines and procedures_' ([Jenkins 2003](#jenkins2003): 26). Unlike historians, social scientists often make differentiating decisions based on '_the identity or origin of the information sources used_' because of concerns about _bias_ ([Meho and Tibbo 2003](#meho2003): 581). The need for authenticity and reliability probably affects differentiating criteria for selecting among found information sources, for example, historians' preference for sources in their original format instead of copies. Historians generally want to use materials in their original formats instead of surrogates such as photocopies and microcopies of original sources (see, e.g., [Duff _et al._ 2004a](#duff2004a), [2004b](#duff2004b); [Howell and Prevenier 2001](#howell2001)). The reasons for this preference include errors during reproduction and bad quality of reproduction results ([Duff _et al._ 2004b](#duff2004b): 20). Moreover, historians still tend to prefer print information sources over electronic sources, even though the advancement of information technology has enabled more and better electronic sources of information (see, e.g., [Duff _et al._ 2004a](#duff2004a); [Rose 2002](#rose2002)). There are no more recent studies on this subject than those from 2004, so it is unclear if historians' preference for print information sources still exists.

#### Verifying

This study showa that verifying activities are important to both historians and social scientists. However, verifying is important to each group for different reasons according to the individual nature of each discipline and its research topics. Meho and Tibbo found that social scientists emphasise verifying activities because of:

> the "biased" nature of a major proportion of information they locate and use... [and] because of the use of web resources, which, according to most participants, lack accuracy, reliability, and authenticity. (Meho and Tibbo [2003](#meho2003): 584).

These concerns seem to stem from the subject undertaken by the participants in Meho and Tibbo's study: the political nature of studying stateless nations.

On the other hand, verifying activities may be significant in historical research because of the nature of the history discipline and historical research. Historians try to distinguish facts that matter or between 'facts-reliable information-and nonfacts-unverifiable information' ([Howell and Prevenier 2001](#howell2001): 87) in their historical research. In the pursuit of historical facts, historians conduct their research by using historical sources with evidential value. Many publications reviewed in this study indicate that historians are cognisant of the significance of collecting, criticising, and verifying available sources to discover and reconstruct historical facts (e.g., [Howell and Prevenier 2001](#howell2001); <a>Duff _et al._ 2004a</a>).

Historians seem to learn verifying activities according to the conventions of history education. Literature on the history discipline and historical research methodology teaches that sources must be evaluated for accuracy, authenticity, and reliability to be used as evidence (see, e.g., [Evans 1999](#evans1999); [Howell and Prevenier 2001](#howell2001); [Presnell 2007](#presnell2007)). It makes sense that the significance of authenticity and reliability of sources in historical research and historians' intention to use sources as evidence would intensify historians' verifying activities. Historians indicated the significance of archivists as partners in their verifying processes and their reliance on 'archivists to ensure the authenticity of sources archives acquired and described' ([Duff _et al._ 2004a](#duff2004a): 69).

#### Extracting

Few publications reviewed in this study mention extracting activities (see Table 3). However, those that do suggest that historians and social scientists conduct similar extracting activities and divide them into two types: '_those applied on direct sources (e.g., books and journal articles) and those applied on indirect sources (e.g., bibliographies, indexes and abstracts, and online catalogs)_' ([Meho and Tibbo 2003](#meho2003): 582). Historians' own writings show that historians try to extract only relevant information and avoid the temptation to focus exclusively on research topics that, while fascinating, have little relevance to their work (e.g., [Clodfelter 1991](#clodfelter1991): 52; [Griffen-Foley 2002](#griffen-foley2002)).

#### Accessing

This study finds that accessing is the most frequently appearing feature in the literature reviewed in this study (see Table 3). This finding suggests that historians, like social scientists, conduct accessing activities for their research but, it seems, to a greater degree. Because historical research requires more original sources with historical and evidential value than other kinds of research, historians seem often to face more accessing issues. In particular, because archival materials are often unique, dispersed, or classified, it is often more difficult or complicated for historians to access archival materials than library materials, which significantly affects historians' critical access to original sources. The nature of archival materials and the archival environment also present more barriers to accessing archival materials than do library materials and the library environment. The barriers include: remote geographic location; lack of arrangement and description; the unwillingness of repositories to photocopy their manuscripts for use elsewhere; complete lack of finding aids; finding aids that provide too little or too much detail; bad physical condition of materials; difficult-to-use format of materials; institutional restrictions; and legal restrictions concerning privacy, copyright, and security issues ([Duff _et al._ 2004a](#duff2004a): 63-65; [2004b](#duff2004b): 14-16; [Rundell 1970](#rundell1970)).

Accessing issues may affect historians' entire research process. In the study by Duff _et al._ ([2004b](#duff2004b)), some historians had to revise research objectives, reframe their research plan and process, or conduct their research less comprehensively or less completely. In the end, barriers to accessing relevant materials lower the quality of historical research and sometimes cause historians to quit their research projects ([Duff _et al._ 2004a](http://duff2004a): 64-65; [2004b](#duff2004b): 15).

#### Monitoring

We find that, as with social scientists, historians try to maintain awareness of developments in their research areas using both formal and informal information channels. For up-to-date information, historians frequently use bibliographies and book reviews, which are readily available in primary journals in their specialisations, and they also read the articles within those journals (see [Rosenberg and Swierenga 1993](#rosenberg1993); [Tibbo 1993](#tibbo1993)). In addition, historians keep current by consulting colleagues who work on the same or related subjects, attending professional meetings, and checking publishers' flyers and publication lists of scholarly and professional societies ([Delgadillo and Lynch 1999](#delgadillo1999); [Rosenberg and Swierenga 1993](#rosenberg1993); [Tibbo 1993](#tibbo1993)).

Collection-level archival description and finding aids seem to give historians problems in monitoring new arrivals in archives, particularly those inserted into existing collections. Some historians rely on archivists to monitor archival materials that are newly arrived and accessioned. They keep in contact with archivists remotely, and some archivists monitor the type of materials of interest to particular researchers and inform them of any relevant acquisitions (<a>Duff and Johnson 2002</a>: 484; [Johnson and Duff 2005](#johnson2005): 123).

#### Networking

Historians, like social scientists, conduct networking activities with colleagues, archivists, and librarians. However, historians tend not to consult general reference librarians, although they consider as essential the assistance of special collection librarians, librarians with expertise in a specific field (e.g., art history), and especially archivists (see, e.g., [Delgadillo and Lynch 1999](#delgadillo1999); [Douglas 1987](#douglas1987); [Goldring 1987](#goldring1987); [Hay 1987](#hay1987); <a>Rosenberg and Swierenga 1993</a>).

The communication between historians and archivists seems to be particularly strong. This study's data analysis suggests that many historians communicate with and maintain relationships with archivists to get help with 1) searching for relevant or potentially relevant sources and materials (related to starting activities), 2) accessing sources and materials (related to accessing activities), and 3) receiving information on relevant new arrivals (related to monitoring activities). These activities seem to imply that historians' networking may take place in both the searching stage and the accessing stage, while social scientists' networking occurs at only the searching stage.

#### Information-managing

Information-managing may be more essential to historians than to social scientists, though it is important to both. Basic research methodology textbooks for historians tend to detail note-taking and information-managing and highlight their importance (e.g., [Danto 2008](#danto2008); [Presnell 2007](#presnell2007)); however, social science research methodology textbooks tend to focus on information-collecting rather than information-managing. In addition, historians' own writings indicate that historians actively conduct information-managing activities in more stages than social scientists. Whereas information-managing of social scientists is included in the processing stage ([Meho and Tibbo 2003](#meho2003)), historians' information-managing activities seem to be undertaken before the processing stage (see, e.g., [Allen and Sieczkiewicz 2010](#allen2010); [Rose 2002](#rose2002)).

Historians seem to begin to file and organise, even temporarily or partly, collected information in the searching stage and the accessing stage, even though they cannot finish organising systematically all collected information before the processing stage. For example, historians in Rosenberg and Swierenga's study ([1993](#rosenberg1993): 57) often created a prioritised list of sources for later consultation from footnotes and references. They supplemented their agendas by browsing library bookshelves, reading journal articles, reading book reviews, and consulting their colleagues.

During the research process, historians take notes by hand or on personal computers and make photocopies. Historians often store these materials in their offices, often in some kind of card file to index collected materials, and organise them, primarily by topic ([Case 1991a](#case1991a), [1991b](#case1991b)). Though the advancement of information technology has provided historians with more electronic options for managing their collected materials, little is known about historians' information-managing activities in the digital age-for instance, how they manage downloaded electronic documents and how they use bibliographic software.

#### Constructing contextual knowledge

Most historians' writings reviewed in this study indicate why historians need to construct contextual knowledge in historical research, why and how they establish the interpretative context, how they frame the context, and how contextual knowledge influences historical research (e.g., [Conard 2003](#conard2003); [Nix 2009](#nix2009); [Toplin 2003](http://toplin2003); [Westhoff 2009](#westhoff2009)).

Historians construct contextual knowledge to understand, interpret, and re-create stories about the past (see, e.g., [Duff and Johnson 2002](#duff2002); [Evans 1999](#evans1999)). That is, historians construct contextual knowledge to explain history. Historians want to know the context of records relevant to their research subject as well as the context of their research subject itself: who created the records and why, how the records are organised and structured, the record-creating process, and other circumstances of their research subject ([Duff and Johnson 2002](#duff2002); [Howell and Prevenier 2001](#howell2001): 34-42).

To collect contextual information, historians appear to use multiple information-seeking strategies (see, e.g., [Cohen 2002](#cohen2002); [Duff and Johnson 2002](#duff2002)). In libraries, historians construct contextual knowledge by consulting with librarians, browsing bookshelves, and searching and scanning OPAC results. In archives, they do so by 'consulting with an archivist, studying the finding aids, and examining the material itself' ([Duff and Johnson 2002](#duff2002): 486). Many historians also visit historic places and investigate artifacts that are related to their research projects. As expressed by Gottschalk, 'A historical context can be given to [artifacts] only if they can be placed in a human setting' ([1965](#gottschalk1965): 44). Site visits seem to give researchers clues and contextual information as well as more authenticity and credibility (see [Cohen 2002](#cohen2002)).

Constructing contextual knowledge seems to be related to other features, particularly differentiating, browsing, information-managing, and assessing. As historians have more contextual information, they can differentiate more efficiently between relevant and irrelevant specific materials ([Duff and Johnson 2002](#duff2002)). The purpose of active browsing activities seems to be to collect contextual information as well as to find information and sources relevant to the research topic (see, e.g., [Allen and Sieczkiewicz 2010](#allen2010)). Contextual knowledge might help historians organise collected materials by topic.

#### Assessing

Assessing is shown to be a significant activity in historical research and needs to be considered a feature of historians' information-seeking behaviour and comprises evaluating the information found, such as for quantity and internal quality, and determining the significance or value of collected information sources and materials to the overall project (see, e.g., [Duff and Johnson 2002](#duff2002); <a>Howell and Prevenier 2001</a>; <a>Rundell 1970</a>).

Though Meho and Tibbo ([2003](#meho2003)) do not include assessing as a feature of social scientists' information-seeking behaviour, they seem to point implicitly to assessment as underlying a particularly important mode of decision-making. According to their study, social scientists become involved in 'decision-making activities with regard to proceeding to the processing stage or returning to the searching stage', and their decisions are based on 'the success or failure of obtaining needed materials and/or gaining access to various sources and types of information (e.g., subjects, archival materials, and government documents)' ([Meho and Tibbo, 2003](#meho2003): 585). However, Meho and Tibbo do not define decision-making nor include it as a feature of social scientists' information-seeking behaviour.

Assessing seems to have an impact on decisions on movement between stages of historical research. It may be guided primarily by how sufficiently researchers have satisfied their needs with found information sources and materials at each stage. For example, if historians and social scientists need new information sources and materials in the accessing stage and processing stage, they may go back to the searching stage. Otherwise they may proceed to the next stage, either the processing stage or, for historians particularly, the ending stage. That is, it can be said that assessing is the basis for deciding which stage to go to next. The next section describes each stage of historical research and will demonstrate how historians move between stages depending on their assessment of achievement.

### A model of historians' information-seeking behaviour

The results of this study led to the development of a hypothetical model for the process of historians' information-seeking behaviour encompassing historians' information-seeking features (see Figure 2). As previously mentioned, historians often serendipitously gain a new research topic and start a new research project in the process of following citations, identifying relevant materials, and constructing contextual knowledge for an ongoing research project. That is, during any stage in a research project, historians can be inspired to jump to another stage for a new research project. Figure 2 depicts the stages and features of historians' information-seeking behaviour during a research project whose research topic has already been defined.

<figure>![Stages and features in historians' information-seeking behaviour](p544fig2.png)

<figcaption>  
Figure 2: Stages and features in historians' information-seeking behaviour</figcaption>

</figure>

The model shown in Figure 2 was adapted from Meho and Tibbo ([2003](#meho2003): 584, fig. 1). The historian's model and the social scientists' model comprise the same stages (searching, accessing, processing, and ending), but the historians' model differs in several important ways: 1) the connections between interrelated stages; 2) features in each stage; 3) three new features that were included in the author's model; and 4) analysing, synthesising, and writing activities considered to be information-using activities rather than information-seeking activities as in Meho and Tibbo's model. Here, a preliminary explanation is given for each stage in the model of historian's information-seeking behaviour shown in Figure 2.

#### Searching stage

During this stage, social scientists '_might use starting, chaining, browsing, differentiating, monitoring, extracting, and networking activities_' ([Meho and Tibbo 2003](#meho2003): 585). In addition to those seven activities, historians may also conduct orienting, verifying, information-managing, constructing contextual knowledge, and assessing activities. When historians think they have enough information on relevant materials, they move to the accessing stage to acquire those materials.

#### Accessing stage

To historians, accessing is significant as a stage as well as a feature. The nature of historical research and historians' preference for direct sources, particularly original sources, suggests that the accessing stage may be more critical and difficult for historians than for other researchers. Social scientists sometimes depend on 'alternative sources or methods' to gain information, often using 'secondary sources' ([Meho and Tibbo 2003](#meho2003): 581). However, a number of historical publications reviewed for this study suggest that there could be no 'alternative sources or methods' to circumvent the problem of inaccessible sources (e.g., [Duff _et al._ 2004a](#duff2004a), [2004b](#duff2004b); [Rundell 1970](#rundell1970)).

From the searching stage in Meho and Tibbo's model (see Figure 1), social scientists '_may continue to the accessing stage or to the processing stage, or to both, depending on the initial types of information used: indirect or non-full text sources and direct or full-text sources (e.g., books and journal articles)_' ([2003](#tibbo2003): 585). The act of accessing actually happens with both direct sources and, with extra work, with materials originally cited in indirect sources. To emphasise the historian's unique dependence on access to both kinds of sources, the model of historians' information-seeking behaviour includes accessing both direct or full-text sources and indirect or non-full-text sources in the accessing stage.

As previously discussed, the evidence-driven nature of historical research often seems to cause historians many problems in accessing necessary materials and information sources, which often requires complex accessing activities, and much time and effort. Poor access to materials, which is particularly damaging to historical research, could cause historians to proceed directly from the accessing stage to the ending stage. Historians who give up their research projects seem to return to the searching stage with new research topics whose materials are more accessible (see, e.g., [Duff _et al._ 2004b](#duff2004b); [Rundell 1970](#rundell1970)). This may be true for social scientists as well, but it is not reflected in Meho and Tibbo's model because it is very rare. Even if the cases are equally rare between the two groups, the consequences of this case are probably more severe for historians, so it is prudent to represent this case in the model of historians' information-seeking behaviour as a broken line in Figure 2.

At the accessing stage, historians may conduct accessing, assessing, networking, orienting, constructing contextual knowledge, and information-managing activities whereas, according to Meho and Tibbo's model, social scientists conduct only decision-making activities. However, as described earlier, both historians and social scientists may conduct accessing and assessing activities in this stage. In addition, historians may do networking activities to get permission to gain access to certain private libraries and archives and their holdings. They also seem to continue constructing contextual knowledge, information-managing, and orienting activities as they access new sources and materials.

#### Processing stage

At this stage, in general, historians' features may differ only slightly from social scientists'. Both groups might engage in chaining, extracting, differentiating, verifying, and information-managing activities. In addition, it seems that historians conduct constructing contextual knowledge activities and assessing activities.

The research cycles of historians and social scientists alike return to the searching stage from the processing stage if they come upon new questions or new information needs during the processing stage. Moreover, if historians discover that their sources do not support their conclusions as evidence, the literature suggests that they usually go back to the searching stage to seek other evidence (see, e.g., [Benjamin 2001](#benjamin2001)). Otherwise, they go to the ending stage.

#### Ending stage

Social scientists' analysing, synthesising, and writing activities are not defined in Meho and Tibbo's study ([2003](#meho2003)). This study considers these three activities to be information-using activities (indicated by parentheses in Figure 2) rather than information-seeking activities because those activities use information that was already found and collected. Analysing is characterised by detailed examination of the content of materials and synthesising involves combining found information to construct contextual knowledge. Finally, historians write up results of their research projects using information they collected in their information-seeking process. Hence, Figure 2 includes synthesising, analysing, and writing in the ending stage, whereas Meho and Tibbo's model includes them in the processing stage.

Social scientists generally '_move back and forth between the searching, the accessing, and/or the processing stages until the project is completed (ending stage)_' ([Meho and Tibbo 2003](#meho2003): 585). Unlike social scientists, historians seem to go through the same process until a project is either completed or abandoned.

The model shown in Figure 2 is hypothetical and, like other models of information-seeking behaviour, a generalisation. Hence, stages in real historical research may not always occur in the order shown in Figure 2\. Moreover, each historian's research strategies are slightly different, and historians may perform different activities in each stage than the ones in Figure 2\. This is possible even for the same historian during different research projects.

## Conclusions

Although this study found that the features of historians' information-seeking behaviour in the reviewed literature are similar to those of the social scientists reported in Meho and Tibbo's study ([2003](#meho2003)), it also found two factors that distinguish the historical research process: 1) the fact-driven nature of the history discipline, historical education, and historical research and 2) historians' frequent use of archives and archival materials. These distinguishing factors lead to three additional features to Meho and Tibbo's model of social scientists that are particular to historians' information-seeking behaviour: orienting, constructing contextual knowledge, and assessing.

This study's establishment of two significant factors differentiating the information-seeking behaviour of historians and social scientists, as well as the three additions to Meho and Tibbo's model that account for the differences, indicate seven directions for library and information service providers:

First, when archivists appraise archival materials, they should consider the significance of the materials' evidential value in historical research. Historians can help archivists evaluate the evidential value of archival materials. Indeed, there are many cases of archivists consulting historians for archival appraisal practice. For the benefit of both groups, this productive collaboration between archivists and historians should continue.

Second, when developing information systems, archivists and librarians should be cognisant of historians' preference for browsing many search results. Browsing search results of relevant sources seems to help historians serendipitously find sources that are not directly relevant to their research topic but that do give contextual information. To facilitate this activity, archivists and librarians can enhance the efficiency of the browsing process in information systems.

Third, archivists and librarians should provide reference services that acknowledge the significance of serendipity in historical research. When helping historians, archivists and librarians should not restrict their source recommendations to the most obviously relevant information sources and materials.

Fourth, because historians have difficulty monitoring the acquisition of new archival materials by themselves, archivists need to inform their users about new arrivals to their archives. Archivists could institute a customised service that sends users information on the arrival of materials relevant to their research area. However, this kind of service requires many resources, particularly archivists' time and effort. Alternatively, archives could simply announce new arrivals to their repositories in their newsletters and Websites. Archivists would need to clearly describe the processing disposition of the new arrivals (e.g., not yet processed, in processing, or processed) so that historians will know when materials they want to use are actually available.

Fifth, historians' frequent orienting activities in archives indicate that archives need to more actively educate historians in group orientation sessions and private consultations. In particular, historians are often unfamiliar with finding aids and have difficulty using them. Because finding aids among different archives are not standardised as much as OPAC systems in libraries, historians frequently need to learn new finding aids when they visit unfamiliar archives. Archives should allocate resources to finding aid instruction, particularly with online finding aids as they become more readily available.

Sixth, archivists should consider historians' information-seeking and information-managing activities when developing archival description and finding aids. Historians usually identify relevant sources by name but organise collected materials primarily by topic. This implies that historians use names as access points to develop the contextual knowledge of their research topics and then use that knowledge to organise their collected materials. Because historians construct contextual knowledge by searching for names in archival collections, archivists should try to make access points out of all salient names contained in each collection and provide contextual information for the collection in archival description and finding aids. Also, archivists should indicate the relationship of the collection with other collections, both within and beyond their archives, in archival descriptions and finding aids. This will help historians collect contextual information distributed among multiple archival collections and identify other relevant materials. When designing online finding aids, archivists should include all access points for a collection and make its contextual information searchable.

Finally, to improve their services and systems, librarians and archivists must keep up with changes in historical research trends and historians' information-seeking behaviour. Although studies on such behaviour by historians have been conducted since the late 1970s, few have been conducted by archivists in their own archives, the ideal scenario. However, if archivists cannot conduct such studies, they should consult studies on the topic conducted by similar archives.

The above recommendations may not be easy or practical for many archives and libraries because of several factors, such as institutional resources. However, archivists and librarians must continue to try to improve their systems and services by considering their users' needs. The recommendations above could be used as a starting point for long-term improvement or as general guidelines.

## About the author

**Hea Lim Rhee** is a senior researcher at the Korea Institute of Science and Technology Information. She got her Doctor of Philosophy degree from the School of Information Sciences at the University of Pittsburgh in 2011\. She received her MSc in information science from the University of Michigan, where she specialized in archives and records management. Before coming to the United States, her undergraduate major was library and information science. She continued to pursue her studies in this area in her master's programme at Ewha Womans University in Korea, specializing in East Asian archival studies. Her research interests include information-seeking behaviour, user studies, government records, international archives, digital archives, and records management. Her professional experience in libraries and archives includes work as a librarian at a main library at Ewha and as an intern for a library at Columbia University. She can be contacted at [rhee.healim@gmail.com](mailto:rhee.healim@gmail.com)

#### References

_Sources marked with an asterisk_ (*) _were publications on historians’  information-seeking behaviour analysed in this study._

*   Allen, R. B. & Sieczkiewicz, R. (2010). How historians use historical newspapers. _Proceedings of the American Society for Information Science and Technology_, **47**(1), 1-4 *
*   Anderson, I. G. (2004). [Are you being served? Historians and the search for primary sources.](http://www.webcitation.org/6C9RyKOtF) _Archivaria_, No.58, 81-129\. Retrieved 12 November 2010 from http://journals.sfu.ca/archivar/index.php/archivaria/article/view/12479/13592 (Archived by WebCite® at http://www.webcitation.org/6C9RyKOtF) *
*   Appleby, J., Hunt, L. & Jacob, M. (1994). _Telling the truth about history_. New York, NY: W. W. Norton Company *
*   Benjamin, J. R. (2001). _A student’s guide to history_. (8th ed.). Boston, MA & New York, NY: Bedford/St. Martin’s *
*   Bolton, G. (2002). Confessions of a library user. _Library Automated Systems Information Exchange_, **33**(1), 7-16 *
*   Case, D. O. (1991a). Conceptual organization and retrieval of text by historians: the role of memory and metaphor. _Journal of the American Society for Information Science_, **42**(9), 657-668 *
*   Case, D. O. (1991b). The collection and use of information by some American historians: a study of motives and methods. _Library Quarterly_, **61**(1), 61-82 *
*   Clodfelter, M. M. (1991). Problems and pitfalls in researching the air war against North Vietnam. _Air Power History_, **38**(3), 49-53 *
*   Cohen, P. C. (2002). [Tales from the vault: on the road again.](http://www.webcitation.org/6CDHZqtOW) _Common-Place_, **2**(2). Retrieved 9 May, 2010 from http://www.common-place.org/vol-02/no-02/tales/(Archived by WebCite® at http://www.webcitation.org/6CDHZqtOW) *
*   Cole, C. (2000a). Inducing expertise in history doctoral students via information retrieval design. _Library Quarterly_, **70**(1), 86-109 *
*   Cole, C. (2000b). Name collection by Ph.D. history students: inducing expertise. _Journal of the American Society for Information Science_, **51**(5), 444-455 *
*   Conard, R. (2003). Facepaint history in the season of introspection. _The Public Historian_, **25**(4), 9-24 *
*   Conway, P. (1986). Research in presidential libraries: a user survey. _Midwestern Archivist_, **11**(1), 35-56
*   Cook T. (2009). The archive(s) is a foreign country: historians, archivists, and the changing archival landscape. _The Canadian Historical Review_, **90**(3), 497-534
*   Dalton, M. S. & Charnigo, L. (2004). Historians and their information sources. _College and Research Libraries_, **65**(5), 400-425 *
*   D'Aniello, C. A. (Ed.). (1993). _Teaching bibliographic skills in history: a sourcebook for historians and librarians_. Westport, CT & London: Greenwood Press *
*   Danto, E. A. (2008). _Historical research_. New York, NY: Oxford University Press *
*   Delgadillo, R. & Lynch, B. P. (1999). Future historians: their quest for information. _College and Research Libraries_, **60**(3), 245-259 *
*   Duff, W. M. & Johnson, C. A. (2002). Accidentally found on purpose: information-seeking behavior of historians in archives. _Library Quarterly_, **72**(4), 472-496 *
*   Duff, W., Craig, B. & Cherry, J. (2004a). [Finding and using archival resources: a cross-Canada survey of historians studying Canadian history.](http://www.webcitation.org/6CHc79VaI) _Archivaria_, No. 58, 51-80\. Retrieved 10 May, 2010 from http://journals.sfu.ca/archivar/index.php/archivaria/article/view/12478/13590 (Archived by WebCite® at [http://www.webcitation.org/<wbr>6CHc79VaI](http://www.webcitation.org/6CHc79VaI)) *
*   Duff, W., Craig, B. & Cherry, J. (2004b). Historians’ use of archival sources: promises and pitfalls of the digital age. _The Public Historian_, **26**(2), 7-22 *
*   Elam, B. (2007). Readiness or avoidance: e-resources and the art historian. _Collection Building_, **26**(1), 4-6 *
*   Ellis, D. (1989a). A behavioural approach to information retrieval system design. _Journal of Documentation_, **45**(3), 171-212
*   Ellis, D. (1989b). A behavioural model for information retrieval system design. _Journal of Information Science_, **15**(4-5), 237-247
*   Ellis, D. (1993). Modeling the information-seeking patterns of academic researchers: a grounded theory approach. _Library Quarterly_, **63**(4), 469-486
*   Evans, R. J. (1999). _In defense of history_. New York, NY: W.W. Norton Company. *
*   Glaser, B. G. & Strauss, A. L. (1967). _The discovery of grounded theory: strategies for qualitative research_. New York, NY: Aldine
*   Goldring, P. (1987). Some modest proposals: a public historian looks at archives. _Archivaria_, No. 24, 121-128 *
*   Gordon, A. D. (2001). A portrait of research in legal history. _Legal Reference Services Quarterly_, **20**(1/2), 5-15 *
*   Gottschalk, L. (1965). _Understanding history: a primer of historical method_. New York, NY: Alfred A. Knopf *
*   Grafton, A. (1997). _The footnote: a curious history_. Cambridge, MA: Harvard University Press *
*   Graham, S. (2001). [Historians and electronic resources: a second citation analysis](http://www.webcitation.org/6CKXB2vDU). _Journal of the Association for History and Computing_, **4**(2). Retrieved 11 November, 2012 from http://quod.lib.umich.edu/cgi/p/pod/dod-idx/historians-and-electronic-resources-a-second-citation.pdf?c=jahc;idno=3310410.0004.203 (Archived by WebCite® at http://www.webcitation.org/6CKXB2vDU)
*   Griffen-Foley, B. (2002). Confessions of a library user part 2\. _Library Automated Systems Information Exchange_, **33**(1), 17-24 *
*   Hay, D. (1987). [Archival research in the history of the law: a user’s perspective.](http://www.webcitation.org/6CHcSSTfp) _Archivaria_, No.24, 36-46\. Retrieved 10 May, 2010 from http://ssrn.com/abstract=<wbr>1945971 (Archived by WebCite® at http://www.webcitation.org/6CHcSSTfp) *
*   Howell, M. & Prevenier, W. (2001). _From reliable sources: an introduction to historical methods_. Ithaca, NY & London: Cornell University Press *
*   Jenkins, K. (2003). _Re-thinking history_. London & New York, NY: Routledge Classics *
*   Johnson, C. A. & Duff, W. M. (2005). Chatting up the archivist: social capital and the archival researcher. _American Archivist_, **68**(1), 113-129 *
*   Krippendorff, K. (2004). _Content analysis: an introduction to its methodology_.(2nd. ed.). Thousand Oaks, CA: Sage Publications
*   Lonkila, M. (1995). Grounded theory as an emerging paradigm for computer-assisted qualitative data analysis. In U. Kelle, (Ed.), _Computer-aided qualitative data analysis_ (pp. 41-51). London; Thousand Oaks, CA & New Delhi: SAGE Publications
*   Lytle, R.H. (1980). Intellectual access to archives: I. Provenance and content indexing methods of subject retrieval. _American Archivist_, **43**(1), 64-75 *
*   Meho, L. I. & Tibbo, H. R. (2003). Modeling the information-seeking behavior of social scientists: Ellis’s study revisited. _Journal of the American Society for Information Science and Technology_, **54**(6), 570-587
*   Nix, E. M. (2009). Constructing public history in the classroom: the 1968 riots as a case study. _The Public Historian_, **31**(4), 28-36 *
*   Orbach, B. C. (1991). The view from the researcher’s desk: historians’ perceptions of research and repositories. _American Archivist_, **54**(1), 28-43 *
*   Presnell, J. L. (2007). _The information-literate historian: a guide to research for history students_. New York, NY: Oxford University Press *
*   Rose, T. (2002). [Technology’s impact on the information-seeking behavior of art historians.](http://www.webcitation.org/6CHciBrtz) _Art Documentation_, **21**(2), 35-42\. Retrieved 10 November, 2010 from http://web.simmons.edu/~mahard/Rose%202002.pdf (Archived by WebCite® at http://www.webcitation.org/6CHbHPJh2) *
*   Rosenberg, J. A. & Swierenga, R. P. (1993). Finding and using historical materials. In C. A. D'Aniello (Ed.), _Teaching bibliographic skills in history: a sourcebook for historians and librarians_ (pp. 51-61). Westport, CT & London: Greenwood Press *
*   Rundell, W. (1970). _In pursuit of American history: research and training in the United States_. Norman, OK: University of Oklahoma Press *
*   Smyth R. (2006). Exploring congruence between Habermasian philosophy, mixed-method research, and managing data using NVivo. _International Journal of Qualitative Methods_, **5**(2), 1-11
*   Stieg, M. F. (1981). The information of needs of historians. _College and Research Libraries_, **42**, 549-560 *
*   Tibbo, H. R. (1993). _Abstracting, information retrieval and the humanities: providing access to historical literature_. Chicago, IL: American Library Association *
*   Tibbo, H. R. (2003). Primarily history in America: how U.S. historians search for primary materials at the dawn of age. _American Archivist_, **66**(1), 9-50 *
*   Toplin, R. B. (2003). Cinematic history: where do we go from here? _The Public Historian_, **25**(3), 79-91 *
*   Westhoff, L. M. (2009). Lost in translation: the use of primary sources in teaching history. In R. G. Ragland & K. A. Woestman, (Eds.), _The teaching American history project: lessons for history educators and historians_ (pp. 62-77). New York, NY: Routledge *
*   Wilson, T. D. (2000). [Human information behavior.](http://www.webcitation.org/6CI40DHJD) _Informing Science_, **3**(2), 49-55\. Retrieved 1 September, 2010 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf (Archived by WebCite® at http://www.webcitation.org/6CI40DHJD)

_Items analysed, but not cited in the text_

*   Beattie, D. L. (1989). [An archival user study: researchers in the field of women’s history.](http://www.webcitation.org/6CDGYlaKr) _Archivaria_, No. 29, 33-50\. Retrieved 2 July 2011 from http://journals.sfu.ca/archivar/index.php/archivaria/article/view/11607/12554 (Archived by WebCite® at http://www.webcitation.org/6CDGYlaKr)
*   Stam, D. C. (1984). _The information-seeking practices of art historians in museums and colleges in the United States, 1982-83_. Unpublishedcd doctoral dissertation. Columbia University, New York, NY, USA
*   Stevens, M. E. (1977). The historians and archival finding aids. _Georgia Archive_, No. 5, 64-74
*   Tibbo, H. R. (2002). Primary history: historians and the search for primary source materials. _Proceedings presented at the 2002 ACM IEEE joint conference on digital libraries_.
*   Incomplete: who were the editors of the proceedings, who was the publisher, where was it published, and on which pages does this paper appear. Note that "In" is required before a conference proceedings title. -->
*   Tibbo, H. R. (2002). Primarily history: historians and the search for primary source materials. In _Proceedings of the 2nd ACM/IEEE-CS Joint Conference on Digital Libraries_. (pp.1-10). New York, NY: ACM