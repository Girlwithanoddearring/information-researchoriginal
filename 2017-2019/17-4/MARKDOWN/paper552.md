#### vol. 17 no. 4, December, 2012

# Organizational strategy and competitive intelligence practices in Malaysian public listed companies

#### [Ching Seng Yap, Md Zabid Abdul Rashid](#authors) and [Dewi Amat Sapuan](#authors)  
Universiti Tun Abdul Razak, Capital Square, Block C & D, No. 8, Jalan Munshi Abdullah, 50100 Kuala Lumpur, Malaysia

#### Abstract

> **Introduction.** Drawing from the strategic typology of Miles and Snow, this study examined the difference in competitive intelligence practices between two strategic typologies in Malaysian public listed companies.  
> **Method.** Data were collected by mail questionnaire from ninety-three public listed companies in various industry categories.  
> **Analysis.** Descriptive statistics were used to describe the sample and the distribution of responses to each question. Differences in competitive intelligence practices between defenders and analysers were analysed using the independent-t test.  
> **Results.** The study found that the practice of competitive intelligence varies according to type of strategic typologies. Specifically, when compared with defenders, analysers acquired a higher amount of intelligence about technological and economic sectors and used a greater extent of competitive intelligence in eight out of eleven strategic decisions tested in the study, including decisions concerning strategic alliance, market entry and exit, new product or service development, and technology adoption.  
> **Conclusions.** Companies practised competitive intelligence according to their perceived importance of the environmental sector, acquired intelligence from that sector and used it in strategic decision making.

## Introduction

Being a relatively new management tool in the business world, competitive intelligence plays an important role to support managers today for better decision making and strategic planning. Competitive intelligence is defined as a systematic and ethical programme for gathering, analysing, and managing information about the present and future behaviour of competitors, suppliers, customers, technologies, government, acquisitions, market and general business environment ([Vedder and Guynes 2000](#ved00) ). Businesses today have some sort of competitive intelligence practices, whether conducted formally or not. Cases occur where competitive intelligence brings positive impacts on firm performance in different areas, for instance, Merck & Co., NutraSweet, Texas Instrument, Shell, East Kodak Company, Motorola, AT&T, Metropolitan Life Insurance Company, Marion Merrell Dow, FMC & Corning ([Gilad 1989](#gil89) ; [Rouach and Santi 2001](#rou01)). Despite the increasing importance of competitive intelligence in businesses, there are few systematic attempts to investigate the relationship between competitive intelligence practices and its antecedents and consequences. Previous studies reveal that current evidence of the value and impact of competitive intelligence is anecdotal or consists of indirect assessment ([McGonagle and Vella 2002](#mcg02)). The existing empirical studies are mostly from the Western countries with limited studies from the emerging economies. Therefore, this paper aims to investigate the level of competitive intelligence practices undertaken by Malaysian companies and examine whether a difference exists in competitive intelligence practices between companies that adopt different organizational strategy.

This paper proceeds as follows. The next section reviews the foundation concepts of organizational strategy and past empirical studies with respect to competitive intelligence practices and its link to organizational strategy. The third section describes sampling procedures, data collection method, and variables and measurement, and the fourth section presents results and discussions. The paper concludes with an examionation of implications, limitations, and recommendations for future studies.

## Review of the literature

### Organizational strategy

The word strategy was derived from the Greek word _strategos_ which means the art of the [military] general ([Cohen 2004:10](#coh04)). The concept of strategy was introduced into organizational literature and advanced most notably during the 1950s by the Harvard Business School ([Miles and Snow 1978](#mil78); [Snow and Hambrick 1980](#sno80)). There is no single, universally accepted definition of strategy in the literature. Hambrick ([1980: 567](#ham80)) defined strategy as a pattern in a stream of important decisions that guides the organization in its relationships with its environment, affects the internal structure and processes of the organization and centrally affects the organization's performance. Strategy can also be defined normatively, that is the content of a strategy should include the company's basic image, purposes, fields of present and future activities, and expected future position in these fields ([Aguilar 1967: 4](#agu67)).

In conceptualizing organizational strategy, Miles and Snow ([1978: 153](#mil78)) portrayed business as continuously cycling through set of decisions on three broad problem areas which constantly confront management: the entrepreneurial problem - what products or services and market should the organization select; the engineering problem - what is the appropriate process or technology for assembling and delivering the product or services; the administrative problem - what should be the structure and managerial processes for controlling and coordinating the activities of the organization. Based on research in four industries, they distinguished groups of companies that deal with the three problems in a relatively similar manner. The respective groups may be designated as strategic typologies - _prospector, defender, and analyser_.

The three effective strategic types are neither inferior nor superior among themselves. Each type has its own unique strategy for relating to its chosen market, a particular configuration of technology, structure, managerial process, risks and power distribution that is consistent with its market strategy. It means any three strategic types will perform well so long as their strategy implementation is effective. _Defenders_ have narrow product domain, compete primarily based on price, quality and services. They focus on tight control and cost efficiency and pay less emphasis on technological changes. As such, the organizational structure tends to be more formalized and centralized to facilitate intensive planning. _Prospectors_ focus on new market opportunities and compete primarily through new product innovation. They attempt to be the first mover in the market and thus ability to capitalize technological changes and research and development are of utmost importance in determining their success. In order to adapt to the rapid changing environment, the organizational structure and administrative system of prospectors is more flexible. _Analysers_ are the hybrid types of organization, combining the features of defenders and prospectors. They emphasise production and cost efficiency while innovations are selectively adopted in promising new market. As such, they tend to be second mover in the market after prospectors. The organizational structure tends to be more complex, integrating centralization and decentralization to suit the different markets. The fourth type of organization is called the _reactor_, which is a form of strategic failure in that inconsistencies exist in its strategy, technology, structure, and process ([Miles and Snow 1978](#mil78)).

### The role of competitive intelligence in strategic activities

Generally, the role of competitive intelligence in business organizations can be classified into the following categories: (i) supporting strategic decision making ([Prescott and Smith 1989](#pre89)); (ii) identifying early warning of threats and blind spots in business ([Ghoshal and Westney 1991](#gho91)); (iii) supporting strategic planning and implementation for marketing, information technology and research and development activities ([Vedder and Guynes 2002](#ved02)); (iv) supporting competitor assessment and tracking ([Caudron 1994](#cau94)); and (v) performing industry benchmarking with competitors ([Gelb _et al._ 1991](#gel91)).

### Organizational strategy and competitive intelligence

The empirical studies conducted specifically on competitive intelligence are mostly descriptive in nature. Most studies profile the practices of competitive intelligence in organization across different countries. Almost all of the studies do not provide empirical evidence on the relationships between competitive intelligence and its antecedents and consequences. There is even lesser empirical research examining the relationship between organizational strategy and competitive intelligence practices. On the other hand, literature on environmental scanning, the sister field of competitive intelligence, is considered more established. Environmental scanning is defined as the acquisition of information about events, relationships, and trends in a company's external environment, the knowledge of which would assist top management in planning the company's future course of action ([Aguilar 1967](#agu67)). According to Gilad ([1989](#gil89)), the scope of environmental scanning is broader as competitive intelligence focuses on forces with which an organization transacts. Calof and Wright ([2008](#cal08)), on the other hand, regard environmental scanning as the predecessor of competitive intelligence. Rouibah ([2003](#roi03)) considered environmental scanning as another term of competitive intelligence along with early warning system, strategic watch and business intelligence. As such, the literature on environmental scanning is considered relevant to competitive intelligence research.

Reviews of the literature contend that competitive strategy affect the manners in which environment scanning activities were conducted. For instance, past studies adopted either Porter's ([1980](#por80)) competitive strategy model such as low cost leadership strategy and differentiation strategy ([Jennings and Lumpkin 1992](#jen92); [Miller 1989](#mil89); [Raymond _et al._ 2001](#ray01)), or Miles's and Snow's ([1978](#mil78)) strategic typology such as prospector, defender, analyser, and reactor ([Beal 2000](#bea00); [Cartwright _et al._1995](#car95); [Hambrick 1982](#ham82); [Miller 1988](#mil88)), or combination of the two ([Parnell 1997](#par97), [2000](#par00)).

Miller ([1989](#mil89)) examined the relationship between Porter's competitive strategies and strategy making process, including systematic scanning of the environments, in ninety-eight Canadian firms. He found that innovative differentiation strategy is significantly associated with all strategy making variables, including scanning activities which track technological developments and anticipate competitor and customer reactions to new products. Particularly, this relationship is more obvious among the successful companies, as measured by perceptual long run profitability and its growth. On the contrary, cost leadership strategy performed very little scanning and market analysis. Cost leadership allowed only information processing that contributes to internal efficiency such as cost analysis and scanning for more efficient method of production. Cost leadership's simplicity and economy discouraged costly information processing in scanning and analysis of customer preferences, competitors' innovations, and new market opportunities. A similar relationship was found in focus strategy where the relative simplicity of administrative task did not require much information processing in the form of environmental scanning. Spontaneous reactions and rules of thumb may suffice in the focus strategy.

Jennings and Lumpkin ([1992](#jen92)) studied the relationships between environmental scanning activities and the strategy adopted by companies of forty-four chief executives from savings and loan industry through telephone interviews. The study adopted Porter's generic strategies of leadership and differentiation, and opportunities and threats as the dimensions of scanning activities. They found a significant difference between strategies adopted by companies and environmental scanning activities, specifically, organizations with a differentiation strategy engaged in environmental scanning focusing on opportunities and customers' attitudes evaluation while cost leadership strategy focuses on evaluation of threats from competitors and regulators. The finding was consistent with Miller's ([1989](#mil89)) and was later supported by the studies of Hagen and Amin ([1995](#hag95)) and Hagen _et al._ ([2003](#hag03)) using samples from Egypt and Jordan and by that of Kumar _et al._ ([2001](#kum01)) using a hospital sample.

Subramanian _et al._ ([1993](#sub93)) examined the link between strategy and environmental scanning in sixty-eight Fortune 500 manufacturing firms. By using Miles's and Snow's ([1978](#mil78)) strategic typology and Jain's ([1984](#jai84)) environmental scanning phases in terms of its scope, depth, and focus, they found that the scanning systems used by an organization vary depending on its strategic orientation, in which _prospectors_ implemented the most advanced scanning systems, followed by _analysers_ and _defenders_.

Cartwright _et al._ ([1995](#car95)) surveyed seventy-four companies, which are members of the SCIP (Strategic and Competitive Intelligence Professionals). The study examined whether a relationship exists between perceived competitive intelligence usefulness in strategic decision making and strategic orientation of the firms and specific characteristics of competitive intelligence. The study found that technical adequacy (quality) and interaction with the competitive intelligence unit are the major characteristics influencing the perceived usefulness of competitive intelligence. Project-based competitive intelligence tends to be perceived useful regardless of strategic orientation of the firm. However, continuous competitive intelligence, both comprehensive and focus, was perceived differently by firms based on their strategic orientation. _Prospectors_ and _analysers_ perceived these comprehensive types of competitive intelligence as more useful than did _defenders_ and _reactors_.

Walters and Priem ([1999](#wal99)) investigated the relationship between business-level strategy and scanning activities among chief executive officers of 116 single-business manufacturing firms. In addition to information collected through survey, an experiment in a field setting was also conducted wherein chief executive officers responded in their offices to interactive, computer-delivered information search task. The study found that business-level strategy was related to the focus of scanning activities; specifically the differentiators emphasised external opportunities and thus collected information on market and technological sectors. On the other hand, cost leaders focused more on internal capabilities and thus placed more emphasis on political, legal and economic sectors. The high performing firms matched the external and internal scanning practices to their business-level strategy.

Kumar _et al._ ([2001](#kum01)) examined the relationship between strategy and scanning as well as the moderating role of scanning in the strategy- performance relationship in the health care industry. Data were collected from 159 chief executives of the hospitals listed on the American Hospital Association Guide. The sample was heterogeneous in terms of profit orientation, size, location, age, and ownership. Based on variables developed by Jennings and Lumpkin ([1992](#jen92)),the study found that differentiators focused on environmental scanning activities regarding opportunities. On the other hand, cost leaders tended to focus on collecting information regarding threats. organizational performance as measured by return in new services, success in retaining customers, profit margin and success in controlling cost was found to be higher in hospitals which are able to align their environmental scanning focus and competitive strategy.

Walters _et al._ ([2005](#wal05)) examined the link between the business-level strategy and scanning practices and internal firm characteristics of eighty-six small manufacturing firms. The study found that the scanning practices of firms are dependant on the strategy adopted. In terms of internal scanning efforts, managers pursuing a cost leadership strategy emphasised on cost control but managers pursuing a differentiator strategy emphasised on product research and development, market research and basic engineering. In terms of scanning effort in external environment sectors, differentiators placed more emphasis on market sector. However, firms pursuing either strategy are required to have a challenging balancing act to perform their scanning activities in order to achieve success.

The above review illustrates the link between organizational strategy and competitive intelligence practices. The following hypotheses were proposed for this study:

> Hypothesis 1: The amount of competitive intelligence acquisition varies across strategy types.  
> Hypothesis 2: The extent of competitive intelligence use in strategic decision making varies across strategy types.

<figure class="centre">![Figure 1: The conceptual framework](p552fig1.jpg)

<figcaption>Figure 1: The conceptual framework</figcaption>

</figure>

## Methods

### Sample and procedures

The study sample was taken from companies listed on Bursa Malaysia in light of the belief that larger companies would have a higher possibility of having a formal competitive intelligence unit as compared to smaller firms and larger companies tend to be listed companies. One thousand companies were listed on Bursa Malaysia (formally Kuala Lumpur Stock Exchange) as at end of 2007, of which 640 on the Main Board, 234 on the Second Board, and 126 on the MESDAQ market. The final sample excludes those divisions or units of conglomerates or subsidiary companies where competitive intelligence practices may be inherited from the main companies. Similarly, all the holding companies are excluded as they do not conduct any business activities. A search from the website, which includes Bursa Malaysia and individual company's websites was conducted to obtain the corresponding address of each sample. Finally, a total of 900 companies was selected as the targeted samples in this study.

Of the 900 questionnaire forms sent out, ninety-three were returned with complete and usable data, a response rate of 10.3%. The respondents consists of companies from various industry categories. Trading and service, finance, and industrial product categories represent half of the total respondents (50%). In terms of organizational size measured by total number of employee, the largest company has more than 30,000 employees, while the smallest company has less than fifty employees. The mean is 2,538 with a standard deviation of 5,613\. A median of 500 was computed for additional information. The questionnaire was completed by departmental manager (40%), followed by vice president, general manager, divisional director or chief operating officer (33%), and chief executive officer or managing director (27%). _Analyser_ is the strategic typology mostly pursued by the sample (45%), followed by _defender_ (26%). Meanwhile, the remaining sample is about equally distributed between _prospector_ (15%) and _reactor_ (14%).  

In order to check the representativeness of the respondents, the distributions of organization by industry category in the respondents and sampled companies are compared. For the industry category, the deviations between the sampled companies and respondents are less than �6%, with an exception for Finance category, where the deviation is -8%. Since almost all deviations are within �6% range ([Kerlinger & Lee, 2000](#ker00)), the study suggested that the respondents are representative of the sampled companies.

As the response rate for the study was considered low, it might have the possibility that respondents and non-respondents may differ in some significant manner. Due to the difficulty associated with the identification of non-respondents� characteristics in an anonymous research, a wave analysis of non-response bias was conducted ([Rogelberg & Stanton, 2007](#rog07)). The procedure involves comparing early respondents whose survey forms were received within one month and late respondents whose survey forms were received after a month. In this study, there are 55 responses that can be classified as early respondents and 42 as late respondents. Using independent sample _t_ test, the results show no significant difference between early and late respondents on competitive intelligence acquisition and use. Since late responses proxy non-responses, it can therefore be suggested that non-response bias will not seriously affect the generalisability of the study findings.

<table class="center" style="width:70%;"><caption>  
Table 1: Sample characteristic (n=93)  
</caption>

<tbody>

<tr>

<th>_Industry_</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>Consumer products</td>

<td style="text-align: center;">14</td>

<td style="text-align: center;">5.1</td>

</tr>

<tr>

<td>Industrial products</td>

<td style="text-align: center;">15</td>

<td style="text-align: center;">16.1</td>

</tr>

<tr>

<td>Construction</td>

<td style="text-align: center;">7</td>

<td style="text-align: center;">7.5</td>

</tr>

<tr>

<td>Trading and services</td>

<td style="text-align: center;">17</td>

<td style="text-align: center;">18.3</td>

</tr>

<tr>

<td>Finance</td>

<td style="text-align: center;">15</td>

<td style="text-align: center;">16.1</td>

</tr>

<tr>

<td>Technology</td>

<td style="text-align: center;">8</td>

<td style="text-align: center;">8.6</td>

</tr>

<tr>

<td>Properties</td>

<td style="text-align: center;">7</td>

<td style="text-align: center;">7.5</td>

</tr>

<tr>

<td>Plantation</td>

<td style="text-align: center;">6</td>

<td style="text-align: center;">6.5</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align: center;">4</td>

<td style="text-align: center;">4.3</td>

</tr>

<tr>

<th colspan="3">_Size (No. of employees)_</th>

</tr>

<tr>

<td>Below 200</td>

<td style="text-align: center;">20</td>

<td style="text-align: center;">21.5</td>

</tr>

<tr>

<td>200-499</td>

<td style="text-align: center;">24</td>

<td style="text-align: center;">25.8</td>

</tr>

<tr>

<td>500-999</td>

<td style="text-align: center;">14</td>

<td style="text-align: center;">15.1</td>

</tr>

<tr>

<td>1,000-2,999</td>

<td style="text-align: center;">19</td>

<td style="text-align: center;">20.4</td>

</tr>

<tr>

<td>3,000 and above</td>

<td style="text-align: center;">16</td>

<td style="text-align: center;">17.2</td>

</tr>

<tr>

<td colspan="3">_Position_</td>

</tr>

<tr>

<td>Chief executive or managing director</td>

<td style="text-align: center;">25</td>

<td style="text-align: center;">26.9</td>

</tr>

<tr>

<td>Vice president, general manager, chief operating officer, or director</td>

<td style="text-align: center;">31</td>

<td style="text-align: center;">33.3</td>

</tr>

<tr>

<td>Departmental manager</td>

<td style="text-align: center;">37</td>

<td style="text-align: center;">39.8</td>

</tr>

<tr>

<th colspan="3">_Organizational strategy_</th>

</tr>

<tr>

<td>Defender</td>

<td style="text-align: center;">24</td>

<td style="text-align: center;">25.8</td>

</tr>

<tr>

<td>Prospector</td>

<td style="text-align: center;">14</td>

<td style="text-align: center;">15.1</td>

</tr>

<tr>

<td>Analyser</td>

<td style="text-align: center;">42</td>

<td style="text-align: center;">45.1</td>

</tr>

<tr>

<td>Reactor</td>

<td style="text-align: center;">13</td>

<td style="text-align: center;">14.0</td>

</tr>

</tbody>

</table>

<table class="center" style="width:60%;"><caption>  
Table 2: Comparison of respondents and sampled companies by industry</caption>

<tbody>

<tr>

<th>Industry</th>

<th>Respondents</th>

<th>Sampled companies</th>

<th>Deviation</th>

</tr>

<tr>

<td>Consumer products</td>

<td style="text-align:center;">15%</td>

<td style="text-align:center;">13%</td>

<td style="text-align:center;">-2%</td>

</tr>

<tr>

<td>Industrial products</td>

<td style="text-align:center;">16%</td>

<td style="text-align:center;">22%</td>

<td style="text-align:center;">6%</td>

</tr>

<tr>

<td>Construction</td>

<td style="text-align:center;">8%</td>

<td style="text-align:center;">7%</td>

<td style="text-align:center;">-1%</td>

</tr>

<tr>

<td>Trading & services</td>

<td style="text-align:center;">18%</td>

<td style="text-align:center;">23%</td>

<td style="text-align:center;">5%</td>

</tr>

<tr>

<td>Finance</td>

<td style="text-align:center;">16%</td>

<td style="text-align:center;">8%</td>

<td style="text-align:center;">-8%</td>

</tr>

<tr>

<td>Technology</td>

<td style="text-align:center;">9%</td>

<td style="text-align:center;">3%</td>

<td style="text-align:center;">-6%</td>

</tr>

<tr>

<td>Properties</td>

<td style="text-align:center;">8%</td>

<td style="text-align:center;">14%</td>

<td style="text-align:center;">6%</td>

</tr>

<tr>

<td>Plantation</td>

<td style="text-align:center;">7%</td>

<td style="text-align:center;">7%</td>

<td style="text-align:center;">�</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align:center;">4%</td>

<td style="text-align:center;">5%</td>

<td style="text-align:center;">1%</td>

</tr>

</tbody>

</table>

### Variables and measurement

#### Organizational strategy

The study adopts the strategic typology of Miles and Snow ([1978](#mil78)) because: (1) it is based on empirical evidence in a cross-section of industries; (2) it is parsimonious for identification of strategic typologies within a variety of industries; (3) it has made explicit relationships that might exist between strategy typologies and scanning activities, which is related to competitive intelligence practices; (4) it has been extensively applied in the strategy literature and has been found consistently and strongly supported in different industries, including hospitals, colleges, banking, industrial products, and life insurance ([Hambrick 1983](#ham83); [McKee _et al._ 1989](#mck89); [Shortell and Zajac 1990](#sho90)).

Conant _et al._ ([1990](#con90)) developed an instrument that considers each of the elevan adaptive cycles originally proposed by Miles and Snow. This instrument is considered to be more appropriate compared to other instruments as it is multi-item scale covering each of the dimensions with scale consistency and useful for large samples. Each of the eleven items consists of four responses representing each of the four strategy types. In determining which type of strategy typology is adopted by the organizations, a majority-rule decision structure is applied. Thus, organizations are classified as _defenders, prospectors, analysers_ or _reactors_ based on the archetypal response option that is selected most often. In the case of ties between _defender, prospector_ and/or _analyser_ response options will result in the organization being classified as an _analyser_, while any tie involving _reactor_ response options will result in the organization being classified as a _reactor_ ([Conant _et al._ 1990](#con90)). This instrument has been tested as reliable and valid in many strategy studies (e.g., [DeSarbo _et al._ 2005](#des05); [Dyer and Song 1997](#dye97); [Parnell and Wright 1993](#par93) ).

#### Competitive intelligence practices

Competitive intelligence practice is conceptualised as having two dimensions; acquisition and strategic use. For competitive intelligence acquisition, respondents answer two questions based on the perceived importance of acquiring competitive intelligence and the frequency of competitive intelligence acquisition. The type of competitive intelligence is classified based on both internal and external business environmental sectors which include customer, competitor, supplier, technology, regulatory, socio-cultural, economic, human resource, global, and organization ([Daft _et al._ 1988](#daf88) ). The perceived importance of each competitive intelligence type is measured along a five-point scale from 1 - not important to 5 - very important. The frequency of competitive intelligence acquisition is measured using a five-point scale from 1 - very low to 5 - very high. The measurement scale and the phrasing of the questions were adopted from Choo ([1993](#cho93)) who measured the amount of environmental scanning by chief executive officers. An index for the amount of competitive intelligence acquisition is computed by multiplying the acquisition frequency of competitive intelligence and the perceived importance ascribed to each type of competitive intelligence.

For competitive intelligence strategic use, respondents answer two questions based on the perceived importance of each strategic decision in their organization, and the frequency of competitive intelligence use in strategic decision making. Dess and Robinson ([1984](#dss84)) define strategic decisions as having a significant impact on the future state of the firm and/or resulting in the commitment of large amounts of organizational resources. It is exemplified by development of new products and purchase of major equipment. It is vital to note that types of decisions that are strategic in one industry may be less so in another ([Hickson _et al._ 1986](#hic86)) and the categories do overlap. Again, there is no universally accepted way of classifying strategic decisions; the study selects eleven strategic decisions which are adopted from the studies of Mintzberg _et al._ ([1976](#min76)), Dean and Sharfman ([1996](#dea96)), and Culver ([2006](#cul06)). The strategic decisions selected are merger and acquisition; strategic alliance and joint venture; market entry or exit; vertical integration; capacity expansion; new product and service development; diversification; divestment or divestiture; technology adoption; global; and organization. The perceived importance of each strategic decision is measured along a five-point scale from 1 - not important to 5 - very important. The frequency of competitive intelligence use in strategic decision making is measured using a five-point scale from 1 - very low to 5 - very high. The measurement scale and the phrasing of the questions were adopted from Choo ([1993](#cho93)) who measured the amount of information use in decision making by chief executive officers. Similar to competitive intelligence acquisition, an index for the extent of competitive intelligence use in strategic decision making is computed by multiplying the use frequency of competitive intelligence and the perceived importance ascribed to each strategic decision.

## Results and discussion

### Descriptive analysis

The amount of competitive intelligence acquisition is computed by multiplying the perceived importance of competitive intelligence acquisition and frequency of competitive intelligence acquisition. The ranking of the amount of competitive intelligence acquisition is similar to that of the frequency, in which customer is ranked first, followed by competitor, economic and global and technological. Meanwhile, the amount of competitive intelligence acquisition is lowest for socio-cultural and human resources. In most cases, the frequency of acquiring competitive intelligence from a sector is consistent with the perceived importance of that sector, in which the higher the perceived importance of the sector, the higher the frequency of acquiring competitive intelligence from the sector.

The extent of competitive intelligence strategic use is computed by multiplying the perceived importance of strategic decision and frequency of competitive intelligence use in strategic decision making. The sampled companies use competitive intelligence in the greatest extent when making decision concerning new products and services development, followed by capacity expansion, strategic alliance, and market entry or exit. Meanwhile, competitive intelligence is least used in making decision about divestment and vertical integration. Similar to competitive intelligence acquisition, the frequency of using competitive intelligence in making strategic decision is consistent with the perceived importance of the type of decision, in which the higher the perceived importance of a strategic decision, the higher the frequency of using competitive intelligence in making that strategic decision.

<table class="center"><caption>  
Table 3: Amount of competitive intelligence (CI) acquisition by type of competitive intelligence</caption>

<tbody>

<tr>

<th> </th>

<th colspan="3">Perceived importance</th>

<th colspan="3">Acquisition frequency</th>

<th colspan="3">Amount of CI acquisition</th>

</tr>

<tr>

<th>Type of CI</th>

<th>M</th>

<th>SD</th>

<th>Rank</th>

<th>M</th>

<th>SD</th>

<th>Rank</th>

<th>M</th>

<th>SD</th>

<th>Rank</th>

</tr>

<tr>

<td>Customer</td>

<td style="text-align: center;">4.43</td>

<td style="text-align: center;">0.69</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">2.96</td>

<td style="text-align: center;">1.06</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">13.23</td>

<td style="text-align: center;">5.43</td>

<td style="text-align: center;">1</td>

</tr>

<tr>

<td>Competitor</td>

<td style="text-align: center;">4.26</td>

<td style="text-align: center;">0.72</td>

<td style="text-align: center;">2</td>

<td style="text-align: center;">2.85</td>

<td style="text-align: center;">1.07</td>

<td style="text-align: center;">3</td>

<td style="text-align: center;">12.37</td>

<td style="text-align: center;">5.60</td>

<td style="text-align: center;">2</td>

</tr>

<tr>

<td>Economic</td>

<td style="text-align: center;">3.78</td>

<td style="text-align: center;">0.83</td>

<td style="text-align: center;">4</td>

<td style="text-align: center;">2.89</td>

<td style="text-align: center;">1.14</td>

<td style="text-align: center;">2</td>

<td style="text-align: center;">11.14</td>

<td style="text-align: center;">5.58</td>

<td style="text-align: center;">3</td>

</tr>

<tr>

<td>Global</td>

<td style="text-align: center;">3.67</td>

<td style="text-align: center;">0.86</td>

<td style="text-align: center;">6</td>

<td style="text-align: center;">2.73</td>

<td style="text-align: center;">1.15</td>

<td style="text-align: center;">4</td>

<td style="text-align: center;">10.33</td>

<td style="text-align: center;">5.48</td>

<td style="text-align: center;">4</td>

</tr>

<tr>

<td>Technological</td>

<td style="text-align: center;">3.86</td>

<td style="text-align: center;">0.88</td>

<td style="text-align: center;">3</td>

<td style="text-align: center;">2.45</td>

<td style="text-align: center;">0.98</td>

<td style="text-align: center;">7</td>

<td style="text-align: center;">9.70</td>

<td style="text-align: center;">4.97</td>

<td style="text-align: center;">5</td>

</tr>

<tr>

<td>Supplier</td>

<td style="text-align: center;">3.58</td>

<td style="text-align: center;">0.88</td>

<td style="text-align: center;">7</td>

<td style="text-align: center;">2.60</td>

<td style="text-align: center;">0.99</td>

<td style="text-align: center;">5</td>

<td style="text-align: center;">9.52</td>

<td style="text-align: center;">4.95</td>

<td style="text-align: center;">6</td>

</tr>

<tr>

<td>Regulatory</td>

<td style="text-align: center;">3.70</td>

<td style="text-align: center;">0.94</td>

<td style="text-align: center;">5</td>

<td style="text-align: center;">2.54</td>

<td style="text-align: center;">1.06</td>

<td style="text-align: center;">6</td>

<td style="text-align: center;">9.45</td>

<td style="text-align: center;">4.77</td>

<td style="text-align: center;">7</td>

</tr>

<tr>

<td>organizational</td>

<td style="text-align: center;">3.47</td>

<td style="text-align: center;">0.89</td>

<td style="text-align: center;">8</td>

<td style="text-align: center;">2.44</td>

<td style="text-align: center;">1.01</td>

<td style="text-align: center;">8</td>

<td style="text-align: center;">8.72</td>

<td style="text-align: center;">4.75</td>

<td style="text-align: center;">8</td>

</tr>

<tr>

<td>Human Resources</td>

<td style="text-align: center;">3.39</td>

<td style="text-align: center;">0.77</td>

<td style="text-align: center;">9</td>

<td style="text-align: center;">2.40</td>

<td style="text-align: center;">1.07</td>

<td style="text-align: center;">9</td>

<td style="text-align: center;">8.28</td>

<td style="text-align: center;">4.45</td>

<td style="text-align: center;">9</td>

</tr>

<tr>

<td>Socio-cultural</td>

<td style="text-align: center;">3.39</td>

<td style="text-align: center;">0.87</td>

<td style="text-align: center;">10</td>

<td style="text-align: center;">2.23</td>

<td style="text-align: center;">1.03</td>

<td style="text-align: center;">10</td>

<td style="text-align: center;">7.42</td>

<td style="text-align: center;">4.42</td>

<td style="text-align: center;">10</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 4: Extent of competitive intelligence use by type of strategic decision  
</caption>

<tbody>

<tr>

<th>Type of strategic decision</th>

<th colspan="3">Perceived importance</th>

<th colspan="3">Use frequency</th>

<th colspan="3">Extent of CI use</th>

</tr>

<tr>

<th> </th>

<th>M</th>

<th>SD</th>

<th>Rank</th>

<th>M</th>

<th>SD</th>

<th>Rank</th>

<th>M</th>

<th>SD</th>

<th>Rank</th>

</tr>

<tr>

<td>New product development</td>

<td style="text-align: center;">4.04</td>

<td style="text-align: center;">0.81</td>

<td style="text-align: center;">3</td>

<td style="text-align: center;">3.95</td>

<td style="text-align: center;">0.91</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">16.29</td>

<td style="text-align: center;">5.82</td>

<td style="text-align: center;">1</td>

</tr>

<tr>

<td>Capacity expansion</td>

<td style="text-align: center;">4.05</td>

<td style="text-align: center;">0.86</td>

<td style="text-align: center;">3</td>

<td style="text-align: center;">3.90</td>

<td style="text-align: center;">0.97</td>

<td style="text-align: center;">2</td>

<td style="text-align: center;">16.25</td>

<td style="text-align: center;">6.36</td>

<td style="text-align: center;">2</td>

</tr>

<tr>

<td>Strategic alliance</td>

<td style="text-align: center;">4.08</td>

<td style="text-align: center;">0.88</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">3.78</td>

<td style="text-align: center;">1.06</td>

<td style="text-align: center;">3</td>

<td style="text-align: center;">16.04</td>

<td style="text-align: center;">6.71</td>

<td style="text-align: center;">3</td>

</tr>

<tr>

<td>Market entry/exit</td>

<td style="text-align: center;">3.82</td>

<td style="text-align: center;">0.96</td>

<td style="text-align: center;">4</td>

<td style="text-align: center;">3.66</td>

<td style="text-align: center;">1.04</td>

<td style="text-align: center;">4</td>

<td style="text-align: center;">14.59</td>

<td style="text-align: center;">6.51</td>

<td style="text-align: center;">4</td>

</tr>

<tr>

<td>Technology adoption</td>

<td style="text-align: center;">3.80</td>

<td style="text-align: center;">0.83</td>

<td style="text-align: center;">5</td>

<td style="text-align: center;">3.65</td>

<td style="text-align: center;">1.01</td>

<td style="text-align: center;">5</td>

<td style="text-align: center;">14.41</td>

<td style="text-align: center;">6.18</td>

<td style="text-align: center;">5</td>

</tr>

<tr>

<td>Merger & acquisition</td>

<td style="text-align: center;">3.54</td>

<td style="text-align: center;">1.22</td>

<td style="text-align: center;">10</td>

<td style="text-align: center;">3.43</td>

<td style="text-align: center;">1.26</td>

<td style="text-align: center;">7</td>

<td style="text-align: center;">13.31</td>

<td style="text-align: center;">7.80</td>

<td style="text-align: center;">6</td>

</tr>

<tr>

<td>organizational</td>

<td style="text-align: center;">3.61</td>

<td style="text-align: center;">0.87</td>

<td style="text-align: center;">7</td>

<td style="text-align: center;">3.47</td>

<td style="text-align: center;">0.90</td>

<td style="text-align: center;">6</td>

<td style="text-align: center;">13.07</td>

<td style="text-align: center;">5.61</td>

<td style="text-align: center;">7</td>

</tr>

<tr>

<td>Global</td>

<td style="text-align: center;">3.61</td>

<td style="text-align: center;">0.98</td>

<td style="text-align: center;">7</td>

<td style="text-align: center;">3.41</td>

<td style="text-align: center;">1.00</td>

<td style="text-align: center;">8</td>

<td style="text-align: center;">12.92</td>

<td style="text-align: center;">6.30</td>

<td style="text-align: center;">8</td>

</tr>

<tr>

<td>Diversification</td>

<td style="text-align: center;">3.48</td>

<td style="text-align: center;">0.99</td>

<td style="text-align: center;">6</td>

<td style="text-align: center;">3.30</td>

<td style="text-align: center;">1.01</td>

<td style="text-align: center;">11</td>

<td style="text-align: center;">12.87</td>

<td style="text-align: center;">6.17</td>

<td style="text-align: center;">9</td>

</tr>

<tr>

<td>Vertical integration</td>

<td style="text-align: center;">3.58</td>

<td style="text-align: center;">0.95</td>

<td style="text-align: center;">9</td>

<td style="text-align: center;">3.38</td>

<td style="text-align: center;">1.04</td>

<td style="text-align: center;">9</td>

<td style="text-align: center;">12.74</td>

<td style="text-align: center;">6.23</td>

<td style="text-align: center;">10</td>

</tr>

<tr>

<td>Divestment</td>

<td style="text-align: center;">3.22</td>

<td style="text-align: center;">1.09</td>

<td style="text-align: center;">11</td>

<td style="text-align: center;">3.34</td>

<td style="text-align: center;">1.11</td>

<td style="text-align: center;">10</td>

<td style="text-align: center;">11.45</td>

<td style="text-align: center;">6.23</td>

<td style="text-align: center;">11</td>

</tr>

</tbody>

</table>

### Hypotheses testing

In testing the two hypotheses, _prospectors_ and _reactors_ are excluded as the sample size for both groups is below twenty. In addition, _reactors_ have been excluded from the analysis in previous studies as they do not possess a consistent pattern of strategy ([Matsuno and Mentzer 2000](#mat00); [Song _et al._ 2007](#son07)). Independent sample t-test was used to test the difference in competitive intelligence practices between _analysers_ and _defenders_. As shown in Table 5, the difference in the amount of competitive intelligence acquisition is higher among _analysers_ and _defenders_ in all ten sectors but only significant in two out of the ten sectors, namely the technology and economic sectors. Thus, Hypothesis 1 was partially supported by the data. It may be argued that, being the second mover in the market, _analysers_ acquired significantly higher amount of the competitive intelligence from technology and economic sectors in order to exploit the technological opportunities and capitalise on changing economic conditions. One the other hand, the other environmental sectors are seen as important by all strategic typologies and, therefore, the amount of competitive intelligence acquisition is not significantly different between the two types.

Meanwhile, the significantly higher extent of competitive intelligence use is prevalent in eight out of 11 strategic decisions among the _analysers_ as compared to the _defenders_. As shown in Table 6, the largest difference is found in technology adoption decision, followed by market entry/exit, strategic alliance, and new product/service development. On the other hand, no significant difference is found in decisions concerning diversification, divestment, and organisation. Similarly, Hypotheis 2 was also partially supported by the data. It may be argued in the way that strategic decision concerning organisation is viewed as important by both typologies, the extent of competitive intelligence use is thus insignificant between the two typologies. On the other hand, _analysers_ are more aggressive in pursuing new product and service development as well as other expansion strategies, the extent of competitive intelligence use is significantly higher than the defenders.

<table class="center" style="width: 95%;"><caption>  
Table 5: Difference in the amount of competitive intelligence acquisition</caption>

<tbody>

<tr>

<th rowspan="2">Type of competitive intelligence</th>

<th colspan="6"></th>

</tr>

<tr>

<th colspan="2">Defender (n=24)</th>

<th colspan="2">Analyser (n=42)</th>

<th colspan="2">Mean difference</th>

</tr>

<tr>

<th> </th>

<th>M</th>

<th>SD</th>

<th>M</th>

<th>SD</th>

<th>t-value</th>

<th>p-value</th>

</tr>

<tr>

<td>Technology</td>

<td style="text-align: center;">7.13</td>

<td style="text-align: center;">3.39</td>

<td style="text-align: center;">11.12</td>

<td style="text-align: center;">5.30</td>

<td style="text-align: center;">3.727</td>

<td style="text-align: center;">0.000</td>

</tr>

<tr>

<td>Economic</td>

<td style="text-align: center;">9.13</td>

<td style="text-align: center;">5.19</td>

<td style="text-align: center;">12.69</td>

<td style="text-align: center;">5.71</td>

<td style="text-align: center;">2.521</td>

<td style="text-align: center;">0.014</td>

</tr>

<tr>

<td>Global</td>

<td style="text-align: center;">8.75</td>

<td style="text-align: center;">5.80</td>

<td style="text-align: center;">11.48</td>

<td style="text-align: center;">5.05</td>

<td style="text-align: center;">1.998</td>

<td style="text-align: center;">0.050</td>

</tr>

<tr>

<td>Socio-cultural</td>

<td style="text-align: center;">5.83</td>

<td style="text-align: center;">4.48</td>

<td style="text-align: center;">8.02</td>

<td style="text-align: center;">4.43</td>

<td style="text-align: center;">1.926</td>

<td style="text-align: center;">0.059</td>

</tr>

<tr>

<td>Human resources</td>

<td style="text-align: center;">7.08</td>

<td style="text-align: center;">3.80</td>

<td style="text-align: center;">8.88</td>

<td style="text-align: center;">4.58</td>

<td style="text-align: center;">1.628</td>

<td style="text-align: center;">0.109</td>

</tr>

<tr>

<td>Supplier</td>

<td style="text-align: center;">8.63</td>

<td style="text-align: center;">3.50</td>

<td style="text-align: center;">10.21</td>

<td style="text-align: center;">5.47</td>

<td style="text-align: center;">1.438</td>

<td style="text-align: center;">0.155</td>

</tr>

<tr>

<td>organization</td>

<td style="text-align: center;">7.42</td>

<td style="text-align: center;">4.32</td>

<td style="text-align: center;">9.10</td>

<td style="text-align: center;">4.70</td>

<td style="text-align: center;">1.436</td>

<td style="text-align: center;">0.156</td>

</tr>

<tr>

<td>Regulatory</td>

<td style="text-align: center;">8.25</td>

<td style="text-align: center;">4.00</td>

<td style="text-align: center;">9.93</td>

<td style="text-align: center;">5.01</td>

<td style="text-align: center;">1.492</td>

<td style="text-align: center;">0.165</td>

</tr>

<tr>

<td>Competitor</td>

<td style="text-align: center;">11.91</td>

<td style="text-align: center;">6.19</td>

<td style="text-align: center;">13.00</td>

<td style="text-align: center;">5.47</td>

<td style="text-align: center;">0.731</td>

<td style="text-align: center;">0.467</td>

</tr>

<tr>

<td>Customer</td>

<td style="text-align: center;">12.92</td>

<td style="text-align: center;">4.34</td>

<td style="text-align: center;">13.64</td>

<td style="text-align: center;">5.57</td>

<td style="text-align: center;">0.549</td>

<td style="text-align: center;">0.585</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 6: Difference in the extent of competitive intelligence use</caption>

<tbody>

<tr>

<th>Type of strategic decision</th>

<th colspan="2">Defender (n=24)</th>

<th colspan="2">Analyser (n=42)</th>

<th colspan="2">Mean difference</th>

</tr>

<tr>

<th> </th>

<th>M</th>

<th>SD</th>

<th>M</th>

<th>SD</th>

<th>t-value</th>

<th>p-value</th>

</tr>

<tr>

<td>Techonolgy adoption</td>

<td style="text-align: center;">10.88</td>

<td style="text-align: center;">5.74</td>

<td style="text-align: center;">15.45</td>

<td style="text-align: center;">5.13</td>

<td style="text-align: center;">3.341</td>

<td style="text-align: center;">0.001</td>

</tr>

<tr>

<td>Market entry or exit</td>

<td style="text-align: center;">11.63</td>

<td style="text-align: center;">6.50</td>

<td style="text-align: center;">16.21</td>

<td style="text-align: center;">5.86</td>

<td style="text-align: center;">2.941</td>

<td style="text-align: center;">0.005</td>

</tr>

<tr>

<td>Strategic alliance</td>

<td style="text-align: center;">13.21</td>

<td style="text-align: center;">7.34</td>

<td style="text-align: center;">17.64</td>

<td style="text-align: center;">5.58</td>

<td style="text-align: center;">2.764</td>

<td style="text-align: center;">0.007</td>

</tr>

<tr>

<td>New product development</td>

<td style="text-align: center;">13.83</td>

<td style="text-align: center;">5.65</td>

<td style="text-align: center;">17.76</td>

<td style="text-align: center;">5.47</td>

<td style="text-align: center;">2.774</td>

<td style="text-align: center;">0.007</td>

</tr>

<tr>

<td>Merger and acquisition</td>

<td style="text-align: center;">10.58</td>

<td style="text-align: center;">7.40</td>

<td style="text-align: center;">15.55</td>

<td style="text-align: center;">7.14</td>

<td style="text-align: center;">2.682</td>

<td style="text-align: center;">0.009</td>

</tr>

<tr>

<td>Global</td>

<td style="text-align: center;">10.17</td>

<td style="text-align: center;">5.66</td>

<td style="text-align: center;">13.91</td>

<td style="text-align: center;">5.72</td>

<td style="text-align: center;">2.564</td>

<td style="text-align: center;">0.013</td>

</tr>

<tr>

<td>Vertical integration</td>

<td style="text-align: center;">10.38</td>

<td style="text-align: center;">5.72</td>

<td style="text-align: center;">14.36</td>

<td style="text-align: center;">6.41</td>

<td style="text-align: center;">2.521</td>

<td style="text-align: center;">0.014</td>

</tr>

<tr>

<td>Capacity expansion</td>

<td style="text-align: center;">14.04</td>

<td style="text-align: center;">6.99</td>

<td style="text-align: center;">18.14</td>

<td style="text-align: center;">5.14</td>

<td style="text-align: center;">2.512</td>

<td style="text-align: center;">0.016</td>

</tr>

<tr>

<td>Organizational</td>

<td style="text-align: center;">11.42</td>

<td style="text-align: center;">5.91</td>

<td style="text-align: center;">13.79</td>

<td style="text-align: center;">4.61</td>

<td style="text-align: center;">1.809</td>

<td style="text-align: center;">0.075</td>

</tr>

<tr>

<td>Divestment</td>

<td style="text-align: center;">10.88</td>

<td style="text-align: center;">7.04</td>

<td style="text-align: center;">12.76</td>

<td style="text-align: center;">5.56</td>

<td style="text-align: center;">1.202</td>

<td style="text-align: center;">0.234</td>

</tr>

<tr>

<td>Diversification</td>

<td style="text-align: center;">12.33</td>

<td style="text-align: center;">6.77</td>

<td style="text-align: center;">14.00</td>

<td style="text-align: center;">5.07</td>

<td style="text-align: center;">1.136</td>

<td style="text-align: center;">0.260</td>

</tr>

</tbody>

</table>

## Conclusion

The findings have implications on two perspectives: academic and managerial. For the academic perspective, this study serves as one of the earliest empirical studies to examine the relationship between organizational strategy and competitive intelligence practices. The findings of this study also confirm the link between organizational strategy and competitive intelligence practices in which a higher level of competitive intelligence acquisition in technological and economic sectors and a greater extent of competitive intelligence use in most of the strategic decision making is found among _analysers_. Business organizations that adopt the _analyser_ strategy may wish to pay greater attention to issues and development trends of new technological changes and acquire a higher level of intelligence about this sector. As _analysers_ emphasise capitalising on the innovativeness of new technology advancement, the higher level of technology intelligence acquisition enables managers to use technology intelligence in a greater extent when making strategic decisions pertaining to technology adoption.

The study also has implications for managers responsible for strategic decision making in organizations pursuing _analyser_ typology. As competition in the global market intensifies and the pace of technological change accelerates, managers should initiate and organize competitive intelligence activities in their organization. Systematic acquisition of intelligence about the technology and economic sectors enables managers to better understand the business situation and enhance the quality of strategic decision making concerning technology adoption and market expansion. Business organizations may wish to design and implement a competitive intelligence system which is capable of systematically capturing intelligence from internal and external environments. The competitive intelligence system should be integrated with other enterprise-wide information systems in order to achieve long term benefits from the competitive intelligence efforts, specifically in enhancing the quality of strategic decision making. As competitive intelligence emphasises on actionable information as compared to other information management tools, whether the intelligence acquired is subsequently used in strategic decision making is of utmost importance. Therefore, business organizations must ensure that competitive intelligence systems will be able to provide intelligence to the right people at the right time so that it will be utilised in the relevant strategic decision making.

This study suffers from several limitations. The sample size of this study is below one hundred and represents only slightly over 10% of the population. As such, the findings of this study cannot be generalised across the Malaysian public listed companies, which practise formal competitive intelligence. Future studies are recommended to increase the sample size, specifically from the _prospectors_ and _reactors_, so that comparison in terms of competitive intelligence practices among all strategy types will be feasible. This study also does not deal with all the activities of competitive intelligence cycle besides competitive intelligence acquisition and strategic use. It would be too ambitious to cover all the phases of competitive intelligence cycle for this study, and there is no doubt that the other phases of competitive intelligence cycle are also important. In order to capture the complete domain of competitive intelligence practices, future researchers are recommended to include intelligence planning, analysis, and dissemination apart from acquisition and strategic use as presented in this study. Future researchers are also recommended to include moderating variables such as industry category, business functions, and firm size as these variables may have some impact on the hypothesized relationship among the research constructs. When time permits, longitudinal research is recommended, especially in examining the causal or probable reciprocal relationship between organizational strategy and competitive intelligence practices. Functional mangers constitute the largest proportion in the sample (40%) of this study. Each functional manager may differ in the competitive intelligence practices, specifically the amount of competitive intelligence acquisition and the extent of competitive intelligence use in different decision making. As the functional managers may focus only on some particular sectors, some of the survey items may fall outside their task domain. Moreover, the study could not examine this difference due to limited responses from each business function.

## Acknowledgements

This research was funded by the Universiti Tun Abdul Razak's Research Grant.

## About the authors

**Ching Seng Yap** is an Assistant Professor in the Graduate School of Business, Universiti Tun Abdul Razak, Malaysia. He received his Bachelor's degree in Agribusiness and Master of Information Technology from Universiti Putra Malaysia and his PhD in Management from Universiti Tun Abdul Razak, Malaysia. He can be contacted at:[chingseng@unirazak.edu.my](mailto:chingseng@unirazak.edu.my)  
**Md Zabid Abdul Rashid** is Professor and President/Vice Chancellor of Universiti Tun Abdul Razak. He received his Bachelor's degree in Agribusiness from Universiti Putra Malaysia and his Master of Science from University of London, UK and his DSc from Aix-Marseilles/ESSEC, France. He can be contacted at: [zabid@unirazak.edu.my](mailto:zabid@unirazak.edu.my)  
**Dewi Amat Sapuan** is an Associate Professor in the Graduate School of Business, Universiti Tun Abdul Razak, Malaysia. She received her Bachelor's degree in Business Administration from Universiti Utara Malaysia and her Master of Business Administration and PhD in Management from Universiti Tun Abdul Razak, Malaysia. She can be contacted at: [dewi@unirazak.edu.my](mailto:dewi@unirazak.edu.my)

#### References

*   Aguilar, F.J. (1967). _Scanning the business environment_. New York, NY: MacMillan.
*   Beal, R.M. (2000). Competing effectively: environmental scanning, competitive strategy, and organizational performance in small manufacturing firms. _Journal of Small Business Management_, **38**(1), 27-47.
*   Calof, J.L. & Wright, S. (2008). Competitive intelligence: a practitioner, academic and inter-disciplinary perspective. _European Journal of Marketing_(7/8), **42**(), 717-730.
*   Cartwright, D. L., Boughton, P. D. & Miller, S. W. (1995). Competitive intelligence systems: relationships to strategic orientation and perceived usefulness. _Journal of Managerial Issues_, **7**(4), 420-434.
*   Caudron, S. (1994). I spy, you spy. _Industry Week_, **143**(18), 35-40.
*   Choo, C. W. (1993). _Environmental scanning acquisition and use of information by chief executive officers in the Canadian telecommunications industry_. Unpublished doctoral dissertation, University of Toronto, Canada.  

*   Cohen, W. A. (2004). _The art of the strategist_. New York: AMACOM.  

*   Conant, J. S., Mokwa, M. P. & Varadarajan, P. R. (1990). Strategic types, distinctive marketing competencies and organizational performance: a multiple measures-based study. _Strategic Management Journal_, **11**(5), 365-383.
*   Culver, M. (2006). Using tactical intelligence to help inform strategy. _Strategy & Leadership_, **34**(6), 17-23.
*   Daft, R. L., Sormunen, J. & Parks, D. (1988). Chief executive scanning, environmental characteristics and company performance: an empirical study. _Strategic Management Journal_, **9**(2), 123-139.
*   Dean, J. W. & Sharfman, M. P. (1996). Does decision process matter? A study of strategic-decision making effectiveness. _Academy of Management Journal_, **39**(2), 368-396.
*   Desarbo, W. S., Di Benedetto, C. A., Song, M. & Sinha, I. (2005). Revisiting the Miles and Snow strategic framework: uncovering interrelationships between strategic types, capabilities, environmental uncertainty, and firm performance. _Strategic Management Journal_, **26**(1), 47-74.
*   Dess, G. G. & Davis, P. S. (1984). Porter's (1980) generic strategies as determinants of strategic group membership and organizational performance. _Academy of Management Journal_, **27**(3), 467-488.
*   Dyer, B. & Song, X. M. (1997). The impact of strategy on conflict: a cross-national comparative study of US and Japanese firms. _Journal of International Business Studies_, **28**(3), 467-493.
*   Gelb, B. D., Saxton, M. J., Zinkhan, G. M. & Albers, N. D. (1991). Competitive intelligence: insights from executives. _Business Horizons_, **34**(1), 43-47.
*   Gilad, B. (1989). The role of organized competitive intelligence in corporate strategy. _Columbia Journal of World Business_, **24**(4), 29-35.
*   Ghoshal, S. & Westney, D. E. (1991). Organizing competitor analysis systems. _Strategic Management Journal_, **12**(1), 17-31.
*   Hambrick, D. (1980). Operationalizing the concept of business-level strategy in research. _Academy of Management Review_, **5**(4), 567-575.
*   Hambrick, D. (1982). Environmental scanning and organizational strategy. _Strategic Management Journal_, **3**(2), 159-174.
*   Hambrick, D. (1983). Some tests of the effectiveness and functional attributes of Miles and Snow's strategic types. _Academy of Management Journal_, **26**(1), 5-26.
*   Hagen, A. F. & Amin, S. G. (1995). Corporate executives and environmental scanning activities: an empirical investigation. _SAM Advanced Management Journal_, **60**(2), 41-47.
*   Hagen, A., Haile, S. & Maghrabi, A. (2003). The impact of the type of strategy on environmental scanning activities in the banking industry: an international perspective. _International Journal of Commerce & Management_, **13**(2), 122-143.
*   Hickson, D. J., Butler, R. J., Cray, D., Mallory, G. R. & Wilson, D. C. (1986). _Top decisions: Strategic decision-making in organizations_. San Francisco, CA: Jossey-Bass.
*   Jain, S. C. (1984). Environmental scanning in U.S. corporations. _Long Range Planning_, **17**(2), 117-128.
*   Jennings, D. F. & Lumpkin, J. R. (1992). Insight between environmental scanning activities and Porter's generic strategies: an empirical analysis. _Journal of Management_, **18**(4), 791-803.
*   Kerlinger, F. N. & Lee, H. B. (2000). _Foundations of behavioral research_ (4th ed.). New York: Harcourt College Publishers.
*   Kumar, K., Subramanian, R. & Strandholm, K. (2001). Competitive strategy, environmental scanning and performance. A context specific analysis of their relationship. _International Journal of Commerce and Management_, **11**(2), 1-33.
*   Matsuno, K. & Mentzer, J. T. (2000). The effects of strategy type on the market orientation-performance relationship. _Journal of Marketing_, **64**(4), 1-16.
*   McGee, J. E. & Sawyerr, O. O. (2003). Uncertainty and information search activities: a study of owner-managers of small high-technology manufacturing firms. _Journal of Small Business Management_, **41**(4), 385-401.
*   McGonagle, J. J. & Vella, C. M. (2002). A case for competitive intelligence. _Information Management Journal_, **36**(4), 35-40.
*   McKee, D.O., Varadarajan, P.R. and Pride, W.M. (1989), Strategic adaptability and firm performance: a market contingent perspective. _Journal of Marketing_, **53**(3), 21-35.
*   Miles, R. E. & Snow, C. C. (1978). _Organizational strategy, structure, and process_. New York, NY: McGraw-Hill.
*   Miller, D. (1988). Relating Porter's business strategies to environment and structure: analysis and performance implications. _Academy of Management Journal_, **31**(2), 280-308.
*   Miller, D. (1989). Matching strategies and strategy making: Process, content and performance. _Human Relations_, **42**(3), 241-260.
*   Mintzberg, H., Raisinghani, D. & Theoret, A. (1976). The structure of unstructured decision processes. _Administrative Science Quarterly_, **21**(2), 246-275.
*   Parnell, J. A. & Wright, P. (1993). Generic strategy and performance: an empirical test of the Miles and Snow Typology. _British Journal of Management_, **4**(1), 29-36.
*   Parnell, J. A. (1997). New evidence in the generic strategy and business performance debate: a research note. _British Journal of Management_, **8**(2), 175-181.
*   Parnell. J. A. (2000). Reframing the combination strategy debate: defining forms of combination. _Journal of Applied Management Studies_, **9**(1), 33-54.
*   Porter, M. E. (1980). _Competitive strategy_. New York, NY: Free Press.
*   Prescott, J. E. & Smith, D. C. (1989). The largest survey of "leading-edge" competitor intelligence managers. _Planning Review_, **17**(3), 6-13.
*   Raymond, L., Julien, P. A. & Ramangalaby, C. (2001). Technlogical scanning by small Canadian manufacturers. _Journal of Small Business Management_, **39**(2), 123-138.
*   Rogelberg, S. G. & Stanton, J. M. (2007). Understanding and dealing with organizational survey non-response. _Organizational Research Methods_, **10**(2), 195-209.
*   Rouach, D. & Santi, P. (2001).Competitive intelligence adds value: Five intelligence attitudes. _European Management Journal_, **19**(5), 552-559.
*   Rouibah, K. (2003). Environmental scanning, anticipatory information and associated problems: insight from Kuwait. _Communications of the International Information Management Association_, **3**(1), 47-63.
*   Snow, C. C. & Hambrick, D. C. (1980). Measuring organizational strategies: some theoretical and methodological problems. _Academy of Management Review_, **5**(4), 527-538.
*   Song, M., Di Benedetto, C. A. & Nason, R. W. (2007). Capabilities and financial performance: the moderating effect of strategic type. _Journal of the Academy of Marketing Science_, **35**(1), 18-34.
*   Shortell, S. M. & Zajac, E. (1990). Perceptual and archival measures of Miles and Snows strategic types: a comprehensive assessment of reliability and validity. _Academy of Management Journal_, **33**(1), 817-832.
*   Subramanian, R., Fernandes, N. & Harper, E. (1993). An empirical examination of the relationship between strategy and scanning. _The Mid-Atlantic Journal of Business_, **29**(3), 315-330.
*   Vedder, R. G. & Guynes, C. S. (2000). A study of competitive intelligence practices in organizations. _The Journal of Computer Information Systems_, **41**(2), 36-39.
*   Vedder, R. G. & Guynes, C. S. (2002). CIOs' perspective on competitive intelligence. _Information Systems Management_, **19**(4), 49-55.
*   Walters, B. A. & Priem, R. L. (1999). Business strategy and CEO intelligence acquisition. _Competitive Intelligence Review_, **10**(2), 15-22.
*   Walters, B. A., Priem, R. L, & Shook, C. L. (2005). Small business manager scanning emphases and the dominant logic of the business-level strategy. _Journal of Small Business Strategy_, **15**(2), 19-32.