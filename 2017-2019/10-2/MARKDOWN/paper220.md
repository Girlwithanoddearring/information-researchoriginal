#### Vol. 10 No. 2, January 2005


# How human information behaviour researchers use each other's work: a basic citation analysis study

#### [Lynne (E.F.) McKechnie](email:mckechnie@uwo.ca), George R. Goodall, and Darian Lajoie-Paquette  
Faculty of Information and Media Studies  
University of Western Ontario, London, Ontario, Canada  
and  
Heidi Julien  
School of Library and Information Studies  
University of Alberta, Edmonton, Alberta, Canada  



#### Abstract

> **Introduction.** The purpose of this study was to determine if and how human information behaviour (HIB) research is used by others.  
> **Method.** Using ISI Web of Knowledge, a citation analysis was conducted on 155 English-language HIB articles published from 1993 to 2000 in six prominent LIS journals. The bibliometric core of 12 papers was identified. Content analysis was performed on papers citing the core (n=377) to determine how the papers were cited. A domain visualization was constructed of the citing relationships within the entire corpus.  
> **Analysis.** Citation analysis, content analysis and social network analysis were used to analyse the data.  
> **Results.** HIB literature is being cited, primarily (81.5%) by LIS authors. Fields outside of LIS citing HIB articles include engineering, psychology, education and medicine. Papers were cited generally (36.0%), for findings (28.5%) and for theory (25.3%) with few citations for method (6.0%). The domain visualization depicted a clear core of HIB scholarship surrounded by a periphery of largely uncited literature.  
> **Conclusion.** HIB literature is yet to have a significant impact on other disciplines. It appears to be a second stage discipline, marked by theoretical consistency and exponential growth in publications and new researchers. More attention should be paid to writing and citation practices to allow HIB literature to become a rich guide to the act of doing HIB research.



## Introduction and background

Librarianship and information science researchers, including human information behaviour researchers, have a strong tradition of critical reflection on their research practices. This is evident in the literature which explores the growth and use of theory within the discipline as well as accounts of methodological approaches.

The development and use of theory in librarianship and information science and human information behaviour research has been explored by a variety of scholars. For example, Järvelin and Vakkari ([1993](#Järvelin1993)) reported theory use in six to eight per cent of the librarianship and information science literature published in 1965, 1975 and 1985\. Pettigrew and McKechnie ([2001](#pettigrew2001)) found that 34.1% of 1,160 librarianship and information science articles published from 1993 to 1998, incorporated theory, mostly (70.1%) drawn from other disciplines. A citation analysis of the authority references for the eleven most frequently occurring librarianship and information science theories found by Pettigrew and McKechnie, revealed little citation (20.1%) outside of librarianship and information science journals, with much of this occurring in papers written by librarianship and information science authors publishing outside the field. McKechnie _et al._ ([2001](#mckechnie2001)) found that 58.9% of the human information behaviour articles published from 1993 to 1998 employed theory. While most of these theories were drawn from other disciplines, including the social sciences (64.4%), the sciences (5.9%) or broader librarianship and information science literature (28.7%), fifteen of the authors of the human information behaviour papers proposed new theories. Overall, there is a growing body of evidence to suggest that both the amount and depth of theoretical work in human information behaviour research is increasing.

Information about the use of methods within human information behaviour research may be found in a variety of types of studies. Analyses of methodology for librarianship and information science overall, such as a comprehensive study by Powell ([1999](#powell1999)), while not specific to human information behaviour, are relevant. Notable among Powell's findings are the increasing use of methods from other disciplines, the increasing use of multi-method designs and the emergence of qualitative approaches. Wang ([1999](#wang1999)) provides a descriptive overview of methodologies and methods for human information behaviour by identifying the methods used (surveys, interviews, experiments and observation) and giving examples of studies using these methods. In a paper published in 1999, [Sonnewald and Iivonen](#sonnewald1999) argued the case for a multi-method, integrated research approach to human information behaviour. Julien ([1996](#julien1996)) and Julien and Duggan ([2000](#julien2000)) conducted content analyses of the information needs and uses literature from 1984 to 1998 and found that the most frequently used research methods were questionnaires, transaction log analysis and interviews, with some researchers also using content analysis, ethnography and experiments. More recently McKechnie, _et al._ ([2003](#mckechnie2003)), in a content analysis of 247 human information behaviour articles published from 1993 to 2000, found that human information behaviour research remains largely focused on interview and survey methods, with qualitative approaches and multiple methods increasing. Further, human information behaviour authors did not describe or document their methods well and neither _Library Literature_ nor _Library and Information Science Abstracts_ consistently or appropriately indexed human information behaviour articles for method.

Case, in his comprehensive literature review summarizes the state of human information behaviour research as follows:

> Information behavior research finds itself at a crossroads. It still retains themes, theories, and methodologies from half a century past, and some of these older approaches remain useful. At the same time it has embraced new perspectives, theories, and methods that would have been considered heretical even a quarter century ago. The new vigor it has shown over the last decade—with many productive researchers still in the early stages of their careers—bodes well for the future. ([Case 2002](#case2002): 291)

The present study grew out of an interest in the vigorous activity, development and growth evident in human information behaviour research. How is this work being done? Are human information behaviour researchers using the work of other human information behaviour researchers to inform their inquiries? And, if so, how are they using this work? Is the work of human information behaviour researchers being used by scholars from outside the area and, if so by whom and how?

## Method

To answer these research questions we conducted a two part study: a basic citation analysis of 155 English-language human information behaviour articles published from 1993 to 2000; and, a content analysis of the citing articles for the twelve most frequently cited papers in the sample. In addition, social network diagrams were constructed of the citing relationships evident within the corpus.

### Sample

The sample of 155 human information behaviour papers was developed from a sample used in an earlier related study of research methods trends ([McKechnie _et al._ 2003](#mckechnie2003)). Included are human information behaviour articles from six library and information science journals: _Information Processing and Management (IP&M), Journal of the American Society for Information Science and Technology (JASIST), Journal of Documentation (JDoc), Journal of Education for Library and Information Science (JELIS), Library and Information Science Research (LISR)_ and _The Library Quarterly (LQ)_. These journals were selected because they are prominent, international in scope, contain peer-reviewed articles and are outlets frequently used by human information behaviour scholars to disseminate the results of their research. Unfortunately, as they are not included in the Institute for Scientific Information's _Web of Knowledge_, we were unable to include proceedings of the Information Seeking in Context 1996 and 1998 conferences in our analysis. We did not extend the sample of human information behaviour articles beyond the 2000 publication date as time was needed to allow other scholars to use and cite these works.

### Citation analysis

A basic citation analysis ([Egghe & Rousseau 1990](#egghe1990)) of all 155 human information behaviour articles was conducted in January 2004\. ISI's _Web of Knowledge_ was used to determine which articles had cited the human information behaviour sample. The analysis, therefore, is constrained by the limitations of ISI's _Web of Knowledge_ such as language biases and the under-representation of national and local journals ([van Leeuwen _et al._ 2001](#vanleeuwen2001); [Zitt _et al._ 2003](#zitt2003)). Bibliographic records for each citing article were imported from _Web of Knowledge_ to _EndNote_. From _EndNote_, the records were parsed into a custom-designed relational database implemented in Microsoft _Access_ for analysis. To construct the social network diagrams, a co-citation matrix was developed using the database.

### Content analysis

While descriptive bibliometrics provides a certain degree of information about the corpus of literature, an additional layer of analyses is required. As Cronin notes:

> If we accept that writing is a social act like a conversation, with rules for conversing... then the nuances with which these conversational cues and clues are imbued will be lost once they are wrested from their conversational scaffolding, like words repeated out of context and out of sequence by an uninformed third party ([2000](#cronin2000): 446).

To understand some of the citation context to which Cronin alludes, we conducted a content analysis ([Krippendorf 1980](#krippendorf1980); [Allen & Reser 1990](#allen1990)) on the core of the 155 human information behaviour articles.

As noted by White and McCain ([1989](#white1989)), determining the bibliometric core of a particular corpus of documents is necessarily a subjective process. For the purpose of this study, we decided to divide the corpus into three distinct zones as defined by a Bradford multiplier of 2.5\. The analysis yielded a core consisting of the twelve most frequently cited journal articles. A geometric analysis of the cumulative Bradford form of the rank-frequency distribution provides a similarly sized core (see [Rousseau 1987](#rousseau1987)). Taken together, these twelve core articles accounted for 30% of the total number of citing articles (n=1293) for the entire corpus of 155 human information behaviour articles. They were cited an average of 31.4 times a paper (range = 25 - 37) as compared to 8.3 times a paper for the full sample. Further, these twelve papers yielded a relatively large number of citing articles (n=379) which could be efficiently located and analysed using the resources and time available. The twelve most frequently cited papers are listed in [Appendix 1](#app).

A list of citing articles was prepared for each of the twelve papers. To increase our understanding of the citation practices observed, each of the twelve papers was carefully read. We located the citing articles for each paper and scanned them for text-embedded references to the cited paper. Each embedded reference was coded according to its use by the author(s) of the article. Use codes included the following:

*   General: the paper is cited in a general or overall way rather than specifically for theory, method or findings.
*   Theory: the paper is cited for its theoretical content.
*   Method: the paper is cited for its methodological content.
*   Findings: the paper is cited for its results/findings.
*   Other: the paper is cited for a reason other than its general, theoretical, methodological or findings content.
*   Hanging reference: although included in the list of references, a reference to the paper did not appear anywhere within the text of the article.

Sometimes an article cited a particular paper more than once. In those cases where the citations were for different purposes, we noted each type of purpose. For example, if an article cited a paper once for theory and once for method, we counted both citations. In those cases where the multiple citations were for the same purpose, we only noted that purpose once. For example, if a paper was cited four times for findings within the same article, we only counted findings once.

Of the 379 citing articles we were actually able to locate and code 377 articles. The two articles we were unable to code were published in a non-English language journal. Non-inclusion of these two articles, which comprise only 0.5% of the total number of citing articles, has minimal impact on the results of this study.

### Social network analysis

To explore the structure of the corpus of human information behaviour articles, an attempt was made at _domain visualization_ ([Borner _et al._ 2003](#borner2003)). Co-citation counts were extracted from the custom _Access_ database to construct a square matrix. The software programs UCINET 6 and NetDraw 1.0 ([Borgatti _et al._ 2002](#borgatti2002)) were then used to process the data and produce a representation of the strength of relationships between the authors of the 155 articles. A spring-embedding algorithm was used to establish the relative locations of the authors in the representation. It should be noted that the analysis conflates the disciplinary intellectual structure inherent in co-citation patterns with the social structure inherent in co-authorship analysis ([Borner _et al._ 2003](#borner2003)). The purpose of this analysis is not to create a rigorous examination of the relationships between various human information behaviour authors, such as that provided by White and McCain's ([1998](#white1998)) study of the entire librarianship and information science discipline, but rather to provide a descriptive picture of the field of human information behaviour as an emerging discipline.

## Findings

### Citation analysis

Citation analysis yielded descriptive findings related to both the initial sample of 155 human information behaviour articles and the citing body of 1293 articles.

Of the citing articles, 81.5% are from core librarianship and information science journals as indicated by ISI's _Journal Citation Reports_ (JCR). It should be noted, however, that JCR omits several pertinent journals from the 'INFORMATION SCIENCE & LIBRARY SCIENCE' category. These journals include: _Information Research: An International Electronic Journal_; _Internet Research: Electronic Networking Application and Policy_; and, _Science and Technology Libraries_. If these journals are included, 85.0% of the citing articles are from core librarianship and information science journals.

The top-citing core librarianship and information science journal is the _Journal of the American Society for Information Science and Technology_ [.[1](#note1)] (19.8% of total citations). The details for the top six citing librarianship and information science journals are summarized in Table 1.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Number of citations by the six most frequently citing librarianship and information science journals**</caption>

<tbody>

<tr>

<th>Journal</th>

<th>Number of Citations</th>

<th>Percentage of Total</th>

</tr>

<tr>

<td>_JASIST_</td>

<td align="center">256</td>

<td align="center">19.8%</td>

</tr>

<tr>

<td>_LISR_</td>

<td align="center">122</td>

<td align="center">9.4%</td>

</tr>

<tr>

<td>_IP&M_</td>

<td align="center">114</td>

<td align="center">8.8%</td>

</tr>

<tr>

<td>_ARIST [[2](#note2)]_</td>

<td align="center">110</td>

<td align="center">8.5%</td>

</tr>

<tr>

<td>_JDOC_</td>

<td align="center">89</td>

<td align="center">6.9%</td>

</tr>

<tr>

<td>_LQ_</td>

<td align="center">49</td>

<td align="center">3.8%</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">740</td>

<td align="center">100.0%</td>

</tr>

</tbody>

</table>

Of the citing journals that are not part of the librarianship and information science core set of journals, only one accounts for greater than one per cent of the total citing articles: _Information Research: An International Electronic Journal_. Other journals that contributed more than five citations to the total include: _Educational Technology Research and Development_; _Journal of Educational Research_; and, _American Behavioral Scientist_.

A variety of fields is represented in the scatter of the non-core librarianship and information science journals including medicine (20 articles), nursing (7), engineering (35), urban planning (5), education (26), management (12), social work (12), and psychology (25). Of the articles in these journals, seventeen were authored by researchers in librarianship and information science faculties or by authors with an librarianship and information science education. Brian Detlor, for example, is on the faculty of the DeGroote School of Business at McMaster University but received his Ph.D. from the Faculty of Information Studies at the University of Toronto.

As noted by White and McCain ([1989](#white1989)), there may be a relationship between article age and the number of citations received. Based on our sample of 155 human information behaviour articles, the correlation of article age (M = 6.93, SD = 2.16) and the number of citations (M = 10.69, SD = 11.16) was significant, r(120) = .36, p <.01.

### Content analysis

Overall, we counted 457 instances of citation types. The results are summarized in Table 2.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Citation counts for studied papers**</caption>

<tbody>

<tr>

<th rowspan="2">Cited Paper</th>

<th colspan="6">Type of Citation (No.)</th>

</tr>

<tr>

<td align="center">**General**</td>

<td align="center">**Theory**</td>

<td align="center">**Findings**</td>

<td align="center">**Method**</td>

<td align="center">**Other**</td>

<td align="center">**Hanging**</td>

</tr>

<tr>

<td>_Bates ([1993](#bates1993))_</td>

<td align="center">9</td>

<td align="center">0</td>

<td align="center">23</td>

<td align="center">3</td>

<td align="center">0</td>

<td align="center">1</td>

</tr>

<tr>

<td>_Byström ([1995](#Byström1995))_</td>

<td align="center">6</td>

<td align="center">15</td>

<td align="center">16</td>

<td align="center">5</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>_Chatman ([1996](#chatman1996))_</td>

<td align="center">11</td>

<td align="center">12</td>

<td align="center">6</td>

<td align="center">1</td>

<td align="center">3</td>

<td align="center">2</td>

</tr>

<tr>

<td>_Ellis ([1993](#ellis1993))_</td>

<td align="center">12</td>

<td align="center">8</td>

<td align="center">16</td>

<td align="center">7</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>_Gorman ([1995](#gorman1995))_</td>

<td align="center">24</td>

<td align="center">1</td>

<td align="center">9</td>

<td align="center">3</td>

<td align="center">2</td>

<td align="center">0</td>

</tr>

<tr>

<td>_Kuhlthau ([1993](#kuhlthau1993))_</td>

<td align="center">9</td>

<td align="center">23</td>

<td align="center">10</td>

<td align="center">1</td>

<td align="center">2</td>

<td align="center">1</td>

</tr>

<tr>

<td>_Leckie ([1996](#leckie1996))_</td>

<td align="center">10</td>

<td align="center">7</td>

<td align="center">14</td>

<td align="center">0</td>

<td align="center">2</td>

<td align="center">1</td>

</tr>

<tr>

<td>_Savolainen ([1993](#savolainen1993))_</td>

<td align="center">17</td>

<td align="center">7</td>

<td align="center">0</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">1</td>

</tr>

<tr>

<td>_Savolainen ([1995](#savolainen1995))_</td>

<td align="center">20</td>

<td align="center">10</td>

<td align="center">7</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">0</td>

</tr>

<tr>

<td>_Schacter ([1998](#schacter1998))_</td>

<td align="center">16</td>

<td align="center">1</td>

<td align="center">24</td>

<td align="center">3</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>_Wilson ([1997](#wilson1997))_</td>

<td align="center">15</td>

<td align="center">11</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>_Wilson ([1999](#wilson1999))_</td>

<td align="center">19</td>

<td align="center">24</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

<td align="center">1</td>

</tr>

<tr>

<td>**Total No.**</td>

<td align="center">168</td>

<td align="center">119</td>

<td align="center">127</td>

<td align="center">26</td>

<td align="center">11</td>

<td align="center">7</td>

</tr>

<tr>

<td>**Percentage**</td>

<td align="center">36.8%</td>

<td align="center">26.0%</td>

<td align="center">27.6%</td>

<td align="center">5.7%</td>

<td align="center">2.4%</td>

<td align="center">1.5%</td>

</tr>

</tbody>

</table>

Of the 457 total citation type incidents, the largest category (168 incidents or 37.0%) was general citation, wherein the author(s) cited the paper overall rather than for a specific purpose such as its theoretical, methodological or findings content. A typical example of a citation of this nature is found in Chen's citation of the Bates, _et al._ ([1993](#bates1993)) paper:

> Most previous studies on art historians' information-seeking behavior and needs focussed on the text-related information (Bates, _et al._ 1993; Wiberley 1998).' ([Chen 2001](#chen2001): 701-702)

The second largest group of citations was that for findings, comprising 126 or 28% of all incidents. Choi and Rasmussen's citation of the Bates, _et al._ ([1993](#bates1993)) paper is a good example:

> This finding is consistent with the findings in Bates, Wilde, and Siegfried (1993) and Collins (1998) who studied user queries in historical disciplines. ([Choi and Rasmussen 2002](#choi2002): 701)

Citations related to theory were also a fairly large group with 119 (26%) citation incidents. Sometimes authors indicated they had used another scholar's theory in their own study. For example, Hersberger cites Chatman's ([1996](#chatman1996)) paper as follows:

> Interview data was analysed using a framework developed from Chatman's Theory of Information Poverty (1996, 197-98). The theory is comprised of six propositions that can serve as barriers to needed information... ([Hersberger 2003](#hersberger2003): 52)

On other occasions, the citation refers more to an author's theoretical work. For example, Kuhlthau's ([1993](#kuhlthau1993)) paper on the principle of uncertainty was cited in an article by Sugar ([1995](#sugar1995): 87) as follows:

> The holistic approach goes a step further than the cognitive approach by including affective and physical aspects of a search. This addition is based on the theoretical importance of the 'uncertainty principle' (Kuhlthau, 1993a). ([Sugar 1995](#sugar1995): 87)

With only 26 (6.0%) citation incidents, very few papers were cited for their methodological content. McKenzie's reference of the Chatman ([1996](#chatman1996)) paper is illustrative:

> The analysis therefore differs from that of the ethnographic researchers such as Chatman (1996), whose mode of data collection and analyses allowed them to analyze what actually occurred in an information-seeking interaction. ([McKenzie 2002](#mckenzie2002): 34)

Although infrequently (11 citation incidents, 2.4% of all), authors did cite papers for a variety of other reasons. For example, Attfield, _et al._ ([2003](#attfield2003)) cite Kuhlthau's ([1993](#kuhlthau1993)) paper as a secondary source about the work of John Dewey; Stefl-Mabry ([2003](#steflmabry2003): 885) cites the Leckie, _et al._ ([1996](#leckie1996)) paper for its definition of _professional_; and, Pettigrew and McKechnie ([2001](#pettigrew2001): 62) cite Chatman for her general comments about theory in library and information science: 'Working with conceptual frameworks and empirical research has never been easy.' ([Chatman 1996](#chatman1996))

Seven citations (1.5%) were hanging references, listed in an article's reference list but not referred to in the text of the article.

Finally, it is evident that there are differences in the way that each of the twelve papers was cited. Some were cited most often for their findings ([Bates _et al._ 1993](#bates1993); [Byström & Järvelin 1995](#Byström1995); [Ellis _et al._ 1993](#ellis1993); [Leckie _et al._ 1996](#leckie1996); and [Schacter _et al._ 1998](#schacter1998)). Others were cited primarily for their theoretical content ([Chatman 1996](#chatman1996); [Kuhlthau 1993](#kuhlthau1993); [Wilson 1999](#wilson1999)). Both papers by Savolainen ([1993](#savolainen1993), [1995](#savolainen1995)) as well as Gorman ([1995](#gorman1995)) and Wilson ([1997](#wilson1997)) were cited generally more often than for other reasons. This can, in part at least, be explained by the nature of the papers. It is not surprising that Kuhlthau's ([1993](#kuhlthau1993)) paper which describes the principle of uncertainty is cited most frequently for its theoretical content while the paper by Bates, _et al._ ([1993](#bates1993)), a report of empirical research with little discussion of its theoretical framework, is cited most frequently for its findings.

### Social network analysis

The domain visualization resulting from the co-citation analysis is depicted as Figure 1.

<div align="center">![fig1](p220fig1.png)</div>

<div align="center">  
**Figure 1: Network representation of co-citation patterns among 155 human information behaviour papers**</div>

The diagram depicts a clear core of scholarship surrounded by a periphery of largely uncited literature. It should be noted that the existence of this periphery might be a function of the relative youth of the articles within the periphery rather than the merit of their contribution to the human information behaviour literature. As expected, the authors of the twelve most highly cited articles are generally located within the core.

One interesting aspect of the diagram not evident in the other analyses is the indication of an emerging sub-discipline within human information behaviour research. This cluster appears to feature authors who write primarily about information behaviour in relation to the Internet and electronic communication. Further examination of this apparent sub-discipline, however, is beyond the scope of this analysis.

The network diagram is significant not only for what is present but for who is absent, notably Paul Gorman. Although one of Gorman's articles was among the top twelve most highly cited in our sample, he is not cited in conjunction with any other author from our initial sample of 155 articles. One explanation for his absence is that Gorman is not an academic librarianship and information science researcher but rather a medical doctor and Assistant Professor in the [Department of Medical Informatics and Clinical Epidemiology](http://www.ohsu.edu/dmice/faculty/gorman.shtml) in the School of Medicine at Oregon Health Science University in Portland, Oregon.

## Discussion

With almost 1300 citations in the last ten years for the sample of 155 papers, it is clear that human information behaviour research is being used. The high rate of citation within librarianship and information science journals (85.0%) indicates that it is used primarily by other human information behaviour and librarianship and information science authors. There is some evidence of impact in fields beyond librarianship and information science in the 15.0% citation rate for non-librarianship and information science journals. This result is slightly smaller than that (20.1%) found by Pettigrew & McKechnie ([2001](#pettigrew2001)) for a broader librarianship and information science sample, suggesting that human information behaviour literature may be used outside of librarianship and information science at a somewhat lower level than other librarianship and information science literature. As in the Pettigrew & McKechnie ([2001](#pettigrew2001)) study, some of the citation beyond librarianship and information science was accounted for by librarianship and information science scholars publishing in journals beyond the field. When citing authors were not librarianship and information science scholars they tended to cite the human information behaviour papers for general purposes and occasionally for findings. This finding suggests that our human information behaviour literature is yet to have theoretical and methodological impact on other disciplines.

The ways in which the work of human information behaviour scholars is used are reflected in the results of the citation content analysis. General citations may simply be included by authors to indicate awareness of relevant work and to acknowledge those who did the work. This usage is in keeping with the functions of citations as identified by, among others, Egghe and Rousseau ([1990](#egghe1990)). That human information behaviour studies are cited for their findings is not surprising given the characteristics of the scholarly research process which requires authors to contextually situate their work and compare its findings with that of earlier studies. One encouraging aspect of the high rate of citation of human information behaviour research for findings is that it points to a growing body of findings and by implication knowledge. The strong level of citation for theory suggests that the theory development noted in earlier studies continues and that human information behaviour authors are using the emerging human information behaviour theoretical perspectives to inform their own work. The very low levels of citation for method are disappointing but not unexpected. As human information behaviour authors do not often describe or document their methods in their publications ([McKechnie _et al._ 2003](#mckechnie2003)), they do not make this information available for others to use in their own work.

The cohesive nature of citing practices within the human information behaviour community is evident in the social network diagram. The diagram also indicates a great deal of homogeneity in the ways that the human information behaviour literature is being used. This observation is consistent with _second stage_ disciplines ([Crane 1972](#crane1972); [Price 1963](#price1963)). According to Crane ([1972](#crane1972)), second stage disciplines are marked by theoretical consistency and exponential growth in the numbers of publications and new researchers. As disciplines mature into the third and fourth stages, research agendas are exhausted and specialized sub-disciplines begin to appear. This process may be evident in the emerging sub-discipline of human information behaviour literature exploring the Internet and electronic communication.

While good writing and citation practices are essential to disseminate knowledge and to associate work with particular scholars, we contend they are also important to foster the development and growth of human information behaviour research. Writing clearly about how we do our work, especially theoretical and methodological practices, and documenting the sources used to inform our work through careful citation practices, has the potential to allow the corpus of human information behaviour literature to become an informal but rich and living guide to the act of doing human information behaviour research.

## Notes

<a name="note1" id="note1"></a>1: Includes both the _Journal of the American Society for Information Science_ and its renamed successor the _Journal of the American Society for Information Science and Technology._

<a name="note2" id="note2"></a>2: _Annual Review of Information Science and Technology_

## References

*   <a name="allen1990" id="allen1990"></a>Allen, B., & Reser, D. (1990). Content analysis in library and information science research. _Library and Information Science Research_, **12**(3), 251-262.
*   <a name="attfield2003" id="attfield2003"></a>Attfield, S., Blandford, A., & Dowell, J. (2003). Information seeking in the context of writing: a design psychology interpretation of the 'problematic situation.' _Journal of Documentation_, **59**(4), 430-453.
*   <a name="borgatti2002" id="borgatti2002"></a>Borgatti, S.P., Everett, M.G. and Freeman, L.C. (2002). _Ucinet for Windows: software for social network analysis_. Harvard, MA: Analytic Technologies.
*   <a name="borner2003" id="borner2003"></a>Borner, K., Chen, C.M., & Boyack, K.W. (2003). Visualizing knowledge domains. _Annual Review of Information Science and Technology_, **37**, 179-255.
*   <a name="case2002" id="case2002"></a>Case, D.O. (2002). _Looking for information: a survey of research on information seeking, needs, and behavior_. New York, NY: Academic Press.
*   <a name="chen2001" id="chen2001"></a>Chen, H.L. (2001). An analysis of image retrieval tasks in the field of art history. _Information Processing & Management_, **37**(6), 701-720.
*   <a name="choi2002" id="choi2002"></a>Choi, Y., & Rasmussen, E.M. (2002). Users' relevance criteria in image retrieval in American history. _Information Processing & Management_, **38**(5), 695-726.
*   <a name="crane1972" id="crane1972"></a>Crane, D. (1972). _Invisible colleges: diffusion of knowledge in scientific communities_. Chicago, IL: University of Chicago Press.
*   <a name="cronin2000" id="cronin2000"></a>Cronin, B. (2000). Semiotics and evaluative bibliometrics. _Journal of Documentation_, **56**(4), 440-445.
*   <a name="egghe1990" id="egghe1990"></a>Egghe, L. & Rousseau, R. (1990). _Introduction to informetrics: quantitative methods in library, documentation and information science_. Amsterdam: Elsevier Science Publishers.
*   <a name="hersberger2003" id="hersberger2003"></a>Hersberger, J. (2003). Are the economically poor information poor? Does the digital divide affect the homeless and access to information? _Canadian Journal of Information and Library Science_, **27**(3), 45-63.
*   <a name="Järvelin1993" id="Järvelin1993"></a>Järvelin, K. & Vakkari, P. (1993). The evolution of library and information sciences 1965-1985: a content analysis of journal articles. _Information Processing and Management_, **29**(1), 129-144.
*   <a name="julien1996" id="julien1996"></a>Julien, H. (1996). A content analysis of recent information needs and uses literature. _Library and Information Science Research_, **18**(1), 53-65.
*   <a name="julien2000" id="julien2000"></a>Julien, H., & Duggan, L.A. (2000). A longitudinal analysis of the information needs and uses literature. _Library and Information Science Research_, **22**(3), 291-309.
*   <a name="krippendorf1980" id="krippendorf1980"></a>Krippendorf, K. (1980). _Content analysis: an introduction to its methodology_. Newbury Park, CA: Sage.
*   <a name="mckechnie2003" id="mckechnie2003"></a>McKechnie, L., Baker, L., Greenwood, M., & Julien. H. (2003). Methodological trends in human information behaviour research. _New Review of Information Behaviour Research_, **3**, 113-125.
*   <a name="mckechnie2001" id="mckechnie2001"></a>McKechnie, L., Pettigrew, K.E., & Joyce, S.L. (2001). The origins and contextual use of theory in human information behaviour research. _New Review of Information Behaviour Research_, **2**, 47-63.
*   <a name="mckenzie2002" id="mckenzie2002"></a>McKenzie, P.J. (2002). Communication barriers and information-seeking counterstrategies in accounts of practitioner-patient encounters. _Library and Information Science Research_, **24**(1), 31-47.
*   <a name="pettigrew2001" id="pettigrew2001"></a>Pettigrew, K.E., & McKechnie, L. (2001). The use of theory in information science research. _Journal of the American Society for Information Science and Technology_, **52**(1), 62-73.
*   <a name="powell1999" id="powell1999"></a>Powell, R. (1999). Recent trends in research: a methodological essay. _Library and Information Science Research_, **21**(1), 91-119.
*   <a name="price1963" id="price1963"></a>Price, D.J. de S. (1963). _Little science, big science_. New York, NY: Columbia University Press.
*   <a name="rousseau1987" id="rousseau1987"></a>Rousseau, R. (1987). The nuclear zone of a Leimkuhler curve. _Journal of Documentation_, **43**(4), 322-333.
*   <a name="sonnewald1999" id="sonnewald1999"></a>Sonnewald, D., & Iivonen, M. (1999). An integrated human information behavior research framework for information studies. _Library and Information Science Research_, **21**(4), 429-457.
*   <a name="steflmabry2003" id="steflmabry2003"></a>Stefl-Mabry, J. (2003). A social judgement analysis of information source preference profiles: an exploratory study to empirically represent media selection patterns. _Journal of the American Society for Information Science and Technology_, **54**, (9) 879-904.
*   <a name="sugar1995" id="sugar1995"></a>Sugar, W. (1995). User-centered perspective of information retrieval research and analysis methods. _Annual Review of Information Science and Technology_, **30**, 77-109.
*   <a name="vanleeuwen2001" id="vanleeuwen2001"></a>van Leeuwen, T. N., Moed, H. F., Tijssen, R. J. W., Visser, M. S., & van Raan, A. F. J. (2001). Language biases in the coverage of the Science Citation Index and its consequences for international comparisons of national research performance. _Scientometrics_, **51**(1), 335-346.
*   <a name="wang1999" id="wang1999"></a>Wang, P. (1999). Methodologies and methods for user behavioral research. _Annual Review of Information Science and Technology_, **34**, 53-99.
*   <a name="white1989" id="white1989"></a>White, H.D., & McCain, K.W. (1989). Bibliometrics. _Annual Review of Information Science and Technology_, **24**, 119-186.
*   <a name="white1998" id="white1998"></a>White, H.D., & McCain, K.W. (1998). Visualizing a discipline: an author co-citation analysis of information science, 1972-1995\. _Journal of the American Society for Information Science_, **49**(4), 327-355.
*   <a name="zitt2003" id="zitt2003"></a>Zitt, M., Ramanana-Rahary, S., & Bassecoulard, E. (2003). Correcting glasses help fair comparisons in international science landscape: country indicators as a function of ISI database delineation. _Scientometrics_, **56**(2), 259-282.


## <a name="app" id="app"></a>Appendix 1: Most frequently cited information behaviour articles

(Organized alphabetically by first author's surname)

*   <a name="bates1993" id="bates1993"></a>Bates, M. J., Wilde, D.N., & Siegfried, S.L. (1993). An analysis of search terminology used by humanists: the Getty Online Search Project report number 1\. _Library Quarterly_, **63**(1), 1-39.
*   <a name="Byström1995" id="Byström1995"></a>Byström, K, & Järvelin, K.. (1995). Task complexity affects information seeking and use. _Information Processing & Management_, **31**, 191-213.
*   <a name="chatman1996" id="chatman1996"></a>Chatman, E.A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science and Technology_, **47**(3), 193-206.
*   <a name="ellis1993" id="ellis1993"></a>Ellis, D., Cox, D., & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation_, **49**(4), 356-369.
*   <a name="gorman1995" id="gorman1995"></a>Gorman, P.N. (1995). Information needs of physicians. _Journal of the American Society for Information Science_, **46**(10), 729-736.
*   <a name="kuhlthau1993" id="kuhlthau1993"></a>Kuhlthau, C. C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, **49**(4), 339-355.
*   <a name="leckie1996" id="leckie1996"></a>Leckie, G. J., Pettigrew, K.E., & Sylvain, C. (1996). Modeling the information seeking of professionals: a general model derived from research on engineers, health care professionals, and lawyers. _Library Quarterly_, **66**(2), 161-193.
*   <a name="savolainen1993" id="savolainen1993"></a>Savolainen, R. (1993). The sense-making theory: reviewing the interests of a user-centered approach to information seeking and use. _Information Processing & Management_, **29**(1), 13-28.
*   <a name="savolainen1995" id="savolainen1995"></a>Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of 'Way of Life.' _Library and Information Science Research_, **17**(3), 259-294.
*   <a name="schacter1998" id="schacter1998"></a>Schacter, J., Chung, G. & Dorr, A. (1998). Children's Internet searching on complex problems: performance and process analyses. _Journal of the American Society for Information Science and Technology_, **49**(9), 840-849.
*   <a name="wilson1997" id="wilson1997"></a>Wilson, T.D. (1997). Information behavior: an interdisciplinary perspective. _Information Processing & Management_, **33**(4), 551-572.
*   <a name="wilson1999" id="wilson1999"></a>Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270.
