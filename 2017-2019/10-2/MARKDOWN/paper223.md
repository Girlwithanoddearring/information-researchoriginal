#### Vol. 10 No. 2, January 2005

# Something old, something new: preliminary findings from an exploratory study about people's information habits and information grounds

#### [Karen Fisher](mailto:fisher@u.washington.edu) and [Charles Naumer](mailto:naumer@u.washington.edu)  
Information School, The University of Washington  
Seattle, WA USA  
[Joan Durrance](mailto:durrance@umich.edu)  
School of Information, University of Michigan  
Ann Arbor, MI USA  
[Lynn Stromski](mailto:lstromski@uwkc.org) and [Torben Christiansen](tchristiansen@uwkc.org)  
United Way of King County  
Seattle, WA USA

#### Abstract

> **Introduction.** We explored Harris and Dewdney's principle that people follow habitual patterns when seeking everyday information along with Pettigrew's notion of information grounds.  
> **Method.** A telephone survey of 612 urban-rural residents was conducted regarding their needs for health and human services in partnership with the United Way of America.  
> **Analysis.** Both quantitative and qualitative methods (content analysis of open question responses) were used to analyse the data.  
> **Results.** Findings revealed: strong ties (40%) and the Internet (39%) are people's primary information sources; people both value and critique most of their sources' ability to provide reliable and trusted information; and, that the most common information grounds are places of worship and the workplace.  
> **Conclusion.** People have information grounds from which they acquire information and the Internet is emerging as a popular information ground. Our findings suggest that further research may provide insight into types of information grounds, the ways in which information is socially constructed at these places and how information grounds evolve over time.


## Introduction

In _Barriers to information: how formal help systems fail battered women_ ([1994](#Harris)), Harris and Dewdney proposed six principles of everyday information seeking that they derived from an extensive review of several literatures, including library and information science, communications, sociology and social psychology. According to their sixth principle (p. 27), 'people follow habitual patterns in seeking information', meaning that people tend to adhere to deeply engrained patterns or habits when seeking information, much the same as they do when carrying out other routine tasks, such as driving to work. As Harris and Dewdney further explain, people 'tend to seek information that is easily accessible, preferably from interpersonal sources such as friends, relatives or co-workers rather than from institutions or organizations, unless (an important qualification) there is a particular reason for avoiding interpersonal sources'. (Harris & Dewdney 1994: 21-26)

Case ([2002](#Case): 289) remarked similarly that, 'many people use formal sources rarely, relying instead on informal sources such as friends and family, or knowledgeable colleagues at work', which he included in his summary of eight lessons of information behaviour research. On these bases, one can conclude or at least hypothesize that barring special circumstances, people turn to other people when seeking everyday information.

While the notion that people habitually engage in interpersonal information-seeking can be viewed as one of the information behaviour field's tenets, a conceptual newcomer is the idea of information grounds, which was derived from Pettigrew's ([1998](#Pettigrew1998), [1999](#Pettigrew1999)) field work at community clinics. A student of Dewdney and Harris, Fisher, then writing as Pettigrew, used Touminen and Savolainen's ([1997](#Tuominen)) social constructionist approach to define information grounds as synergistic 'environment[s] temporarily created when people come together for a singular purpose but from whose behaviour emerges a social atmosphere that fosters the spontaneous and serendipitous sharing of information' ([Pettigrew 1999](#Pettigrew1999): 811). In Fisher _et al._, the propositions of the information grounds framework were given as follows:

> 1.  Information grounds can occur anywhere, in any type of temporal setting and are predicated on the presence of individuals.
> 2.  People gather at information grounds for a primary, instrumental purpose other than information sharing.
> 3.  Information grounds are attended by different social types, most, if not all, of whom play expected and important, albeit different, roles in information flow.
> 4.  Social interaction is a primary activity at information grounds such that information flow is a by-product.
> 5.  People engage in formal and informal information sharing, and information flow occurs in many directions.
> 6.  People use information obtained at information grounds in alternative ways, and benefit along physical, social, affective and cognitive dimensions.
> 7.  Many sub-contexts exist within information grounds and are based on people's perspectives and physical factors; together these sub-contexts form a grand context. ([Fisher _et al._ 2004](#Fisher): )

As shown in Figure 1, these seven propositions can be summarized as eight concepts. Furthermore, the figure shows that information behaviour can be viewed as occurring within information grounds, given Wilson's ([2000](#Wilson): 49) definition of information behaviour as 'the totality of human behaviour in relation to sources and channels of information, including both active and passive information-seeking, and information use', and the definition by Pettigrew _et al._, that information behaviour is 'how people need, seek, give and use information in different contexts', ([Pettigrew _et al._ 2001](#Pettigrew2001): 44)

<div align="center">![fig1](p223fig1.jpg)</div>

<div align="center">  
**Figure 1: Information habits and information grounds.**</div>

Beyond health clinics, Pettigrew ([1998](#Pettigrew1998)) proposed that information grounds might occur in such settings as barber shops and hair salons, quilting bees, playgrounds, tattoo parlours, buses, food banks, etc. Recently, she further hypothesized that information grounds 'hold likely regional and global impact in that they occur across all levels of all societies, especially as people create and utilize information grounds as they perform tasks in the course of daily life' ([Fisher _et al._ 2004](#Fisher): 757), which has parallels with Oldenburg's ([1999](#Oldenburg)) notion of the _third place_ (that is, people's third locale of habitude beyond the home and workplace). The premise behind a deep understanding of information grounds, from a systems design perspective, is that it can suggest ways of effectively delivering or facilitating different types of information, especially through utilizing specific social types such as opinion leaders and gate-keepers. [Examples documented to date](http://ibec.ischool.washington.edu/ibecCat.aspx?subcat=Projects&cat=Tools%20and%20Resources&tri=infoGroundConcept) include the dissemination of health and human services information (e.g., breast cancer and HIV/AIDS) at community clinics and hair salons in Canada, the southern and the northwestern U.S., as well as in master huts in Indonesia, children's story-time hours at Ontarian public libraries, bicycle shops for teenagers in South Seattle, community technology centres in rural Washington, and literacy skill centres in Queens, New York.

Piqued by the underpinnings and implications of Harris and Dewdney's sixth principle that people follow habitual patterns when seeking everyday information and Pettigrew's information grounds, in Fall 2003 we explored both through a telephone survey conducted in partnership with the United Way of America<sup>[[1](#note1)]</sup>. The purpose of our paper is thus three-fold: (1) to share our preliminary findings, (2) to elaborate upon our experience with conducting a large-scale survey, and (3) to discuss ideas for further analysis and investigation.

## Methodology

Our exploratory study of people's information habits and information grounds was guided by the following questions:

1.  What are people's preferred or default ways of seeking everyday information?
2.  Why do they prefer these sources?
3.  What do they obtain from these sources?
4.  What drawbacks do people associate with these sources?
5.  What are people's information grounds?
6.  What characteristics make an information ground opportune for acquiring information?
7.  What types of information do people obtain at information grounds?

While surveys and questionnaires are credited as the second most frequently used research method for investigating information behaviour ([McKechnie _et al._ 2002](#McKechnie)), Case points out that 'very few surveys of information seeking behaviours (whether by mail, telephone, or in-person) have involved samples of over 500 respondents' ([Case 2002](#Case): 198). Citing, in particular the Baltimore study by Warner _et al._ ([1973](#Warner)), Chen and Hernon's ([1982](#Chen)) New England Study, and the California study by Palmour _et al._ ([1979](#Palmour)), to which may be added the Seattle study by Dervin _et al._ ([1976](#Dervin1976)) and the Ontario Ministry of Culture and Communications Review ([1991](#Ontario)), Case further adds that 'the classic, large-scale investigations of the information needs and uses of citizens are quite old now' ([Case 2002](#Case): 260). Answers as to why conducting such large surveys fell by the wayside are linked to expense, operational difficulty and, consequently, effort. While the trend in recent information behaviour research is to go deep, gathering rich data through qualitative or naturalistic means, large-scale surveys signal going broad and, therefore, sacrificing the ability or opportunity to focus in-depth and explore emergent phenomena.

Because we were interested in exploring two simple or straightforward phenomena; information habits and information grounds, a large survey was very appealing, especially in light of rich findings reported by Harris and Dewdney ([1994](#Harris)), whose research team went door-to-door in several communities in studying information needs and battered women, and Marcella & Baxter ([2000](#Marcella2000), [2001](#Marcella2001)) who engaged in a 'random walk around Britain' to understand citizens' information needs. Costs associated with a telephone or door-to-door approach, however, were prohibitive. At the same time, we learned that colleagues at the United Way of King County (UWKC) were planning a telephone survey that dovetailed with our research interests in people's everyday information behaviour.

Thus, a telephone survey was designed and directed by UWKC research staff in consultation with faculty from the local University in an effort to understand residents' health and human services needs for the purpose of informing public policy. The survey instrument included approximately fifty questions addressing the need for and use of (and stress and satisfaction levels associated with) twenty-six health and human service areas (including everyday information) within the past twelve months, as well as respondent demographics (See [Appendix 1](p223App.doc)). The survey was conducted from the 2nd to the 20th of October, 2003 through telephone interviews with 612 residents located throughout various geographic areas in East King County and selected by cluster sampling based on postal code (the quota of completed interviews for each of the twenty-two postal code areas was set to reflect each post code's number of occupied households as a percentage of occupied households in East King County). Because all survey questions were factual and household based, the UWKC did not select quotas according to individual characteristics but, instead, on geography (postal codes) of Eastside cities.

The interviews averaged 31.9 minutes and were completed in either English or Spanish. Survey calls were placed by trained staff from a call room in Halifax, Nova Scotia, Canada. At the beginning and periodically throughout the survey, members of the research team listened in on the calls to ensure that respondents were understanding and answering the survey questions and that callers were administering the instrument correctly. Based on 612 completed interviews the survey has a 95% confidence level with a confidence interval (margin of error) of 3.95% of the population value.

The survey was designed with a user-centred approach in mind. Past health and human services needs surveys are very similar to those information needs and use instruments critiqued by Zweizig and Dervin ([1977](#Zweizig)) and Dervin and Nilan ([1986](#Dervin1986)) as being system-focused and looking at the user in the life of the system. Therefore, the UWKC employed a _person-in-situation_ approach, which despite its extensive use in basic information behaviour research, was nonetheless novel from the UWKC's applied research, social service perspective. Thus, at the outset of the survey respondents were asked whether they had a need for help or information in any of twenty-six areas. The computerized data entry system recorded those areas that received positive response, and, consequently, callers were able to ask in-depth questions on those areas.

The seven open-ended, survey questions regarding information habits and information grounds are listed in Figure 2 (in the interest of space, the full survey is not appended). During the phone interviews, the callers coded the responses according to the instructions provided on the survey instrument. For several questions, the instrument included response categories to aid the interviewers and save data recording time. The categories were derived from pre-testing earlier versions of the questions. In addition to the coded answers, interviewers also recorded additional information in the _Other_ category.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Figure 2: Information habit and information ground survey questions**</caption>

<tbody>

<tr>

<td>1. Generally speaking, where or to whom do you mostly turn when you need to find something out?  
[If respondent says 'it depends' or that they 'have several', ask: what is your favorite source if you could have only one?]

[If respondent says a person: family, friend, neighbor, co-worker, physician, clergy, therapist, etc., then ask 'Do you have a strong relationship with this person?' and code response as #1 or #2]  

> 1\. Someone with whom you have a strong relationship or are close with (specify)  
> 2\. Someone with whom you do not have a strong relationship or do not know well (specify)  
> 3\. Phonebook/yellow pages, flyer, radio, television, newspaper  
> 4\. Organization: public library, school, non-profit, crisis clinic or hotline, etc.  
> 5\. Computer or the Internet  
> 6\. Department at your workplace  
> 7\. Other (specify)

2. Why do you usually use this source? [pause and give person time to provide multiple responses]. [Do not read responses. Record answers that fit best below or record as "other" and specify.]  

> 1\. Gives reliable information or is an expert/authority in the topic  
> 2\. Knows me and understand my needs  
> 3\. Provides emotional support  
> 4\. Quick to contact/access  
> 5\. Easy to use or communicate with  
> 6\. Convenient, inexpensive, close  
> 7\. Can communicate face-to-face or by phone  
> 8\. Anonymity  
> 9\. Neutral, not biased  
> 10\. Other [specify]

3. What are two or three examples of what you usually learn from this source? [Briefly summarize person's answer using his/her main words. Likely responses include: how to solve a specific problem, how to find or where to buy something, or may indicate something very particular such as help with health problems.]  

4. What are the drawbacks of using this source? [Pause and give person time to provide multiple responses; Do not read responses. Record answers that fit best below or record as "other" and specify.]  

> 1\. Information not always reliable or source is not an expert/authoritative  
> 2\. Doesn't know me well or understand my needs  
> 3\. Does not provide emotional support  
> 4\. Is not quick to contact/access  
> 5\. Difficult to use or communicate with  
> 6\. Inconvenient, expensive  
> 7\. Cannot communicate face-to-face or by phone  
> 8\. No anonymity  
> 9\. Biased, not neutral  
> 10\. I get distracted or lost while on the Internet  
> 11\. Other [specify]

5. Sometimes people go to a place for a particular reason such as to eat, get a haircut, to worship, for child care, get something repaired, make crafts, see a health provider or get exercise, but end up sharing information just because other people are there and you start talking. Does such a place come to mind for you? What is it?  

[Briefly summarize respondent's answer using respondents' main words. Give person a few moments to think; if person does not answer, then repeat question or list examples again]  

6. What makes this a good place for obtaining information, either accidentally or on purpose?  

[Briefly summarize person's answer using his/her main words. Likely responses include: people are there who like to talk, a lot of chit-chatting goes on, you're there for a while so talking is a way to fill time, there are interesting things there that prompt conversation, etc]  

7. What are some examples of information that you might pick up there? [Summarize person's answer using his/her main words. Likely responses include: how to solve a specific problem or get something fixed, how to find or where to buy something, what's playing or going on, or may indicate something very particular such as help with health problems.]  

8. For you personally, with regard to what's going on in your life, what kind of information would help you most right now?</td>

</tr>

</tbody>

</table>

Content analysis (c.f., [Krippendorff 1980](#Krippendorff)) was used to code the open-ended question data. Data logged as 'other' were examined and recoded into existing categories if appropriate. We were thus able to reduce significantly the number of answers coded as _Other_. Recoding the answers included clarifying our coding scheme to account for responses such as _God_ as a source of information under Question 1\. In the case of respondents listing 'God' as an answer, we recoded this as (1), that is, a close personal relationship.

Following the development of coding schemes, tests of intra-coder and inter-coder reliability were conducted. The inter-coder reliability testing began by meeting with the independent coders and reviewing the purpose of the study, the data and the codebook. Independent coding was then conducted for all questions containing open responses. Completed coding was then compared amongst all coders and coding decisions were reviewed and discussed. The final inter-coder agreement rate was 100%.

A limitation of the study, however, is that the responses recorded by the phone interviewers cannot be checked for intra- or inter-coder reliability. There were diverse answers for the questions regarding sources of information and reasons for believing an information source was particularly useful or had drawbacks. For future studies, we recommend recording respondents' entire answers. Also, because of the lack of detail recorded for individual responses we were limited in our ability to develop our codebook beyond the original categories. Analysis of the data indicates a range of responses that may warrant further examination and more detailed coding.

Starting with an overview of the residents of King County, in the remainder of this paper we share our preliminary findings regarding people's information habits and information grounds, along with our plans for further analysis and investigation.

## The residents of East King County

King County, Washington, population 1.7 million, is located in the northwest corner of the United States along the coast of the Pacific Ocean. One hundred miles south of the Canadian border, its major cities include Seattle, Bellevue and Redmond, the home of Microsoft Corporation and several other major companies. With living areas ranging from urban to suburban to rural, its largely Caucasian population (78%) followed by Asian (11%) and African (5%) is young (median age is thirty-six with 25% under the age of twenty and 14% over sixty), and the median household income is $53,157 with 8.4% living below the poverty line ([American FactFinder 2004](#FactFinder)).

The survey focused on residents of the Eastside of King County. Of the respondents (Table 1), 63% were female, ages ranged from under thirty-five to over sixty-five (average 49.7), they were primarily white (84%), followed by Hispanic (5%), Asian (5%) and African American (1%), and 58% reported an annual household income of over $50,000\. In terms of education, over 36% answered, _postgraduate training_, 41% had graduated from college, 12% had some college education, and 11% completed all or some high school education. The occurrence of more female than male respondents is probably because the quotas were selected using households within post codes as opposed to individual characteristics, such as sex.

<table width="40%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Respondents' age range**</caption>

<tbody>

<tr>

<th>Age</th>

<th>N</th>

<th>%</th>

</tr>

<tr>

<td>Under 35</td>

<td align="center">116</td>

<td align="center">19.5</td>

</tr>

<tr>

<td>35-44</td>

<td align="center">144</td>

<td align="center">24.2</td>

</tr>

<tr>

<td>45-64</td>

<td align="center">220</td>

<td align="center">36.9</td>

</tr>

<tr>

<td>65 and above</td>

<td align="center">116</td>

<td align="center">19.5</td>

</tr>

<tr>

<td>Refused</td>

<td align="center">16</td>

<td align="center">2.7</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">**612**</td>

<td align="center">**100.0**</td>

</tr>

</tbody>

</table>

## Information habits

During the survey, interviewers asked participants where or to whom they turn, in general, when they need to find out something and to indicate their reasons. As shown in Table 2, all respondents indicated that they had a place they turned to when they needed to find something out. Most commonly, respondents indicated _someone with whom they have a strong relationship_ (that is, someone with whom they feel close, such as family and friends) or the _Internet/Computer_, by which it was understood that respondents were referring to Websites, databases, and other static sources as opposed to interaction with interpersonal sources by e-mail, chat rooms, newsgroups, etc.). Younger people tended to consider the Internet as their primary source more often than people over the age of sixty-five. Men were also more likely to choose the Internet than women, who were more likely to identify a close personal relationship. From a statistical perspective, the distribution of information sources between males and females is significant—Pearson χ<sup style="font-size: x-small;">2</sup>= 23.3847, df=6, p<=0.001\. Further analysis revealed that the use of the Internet increased by income. Over 57% of those whose income exceeded $100,000 chose the Internet compared to the average of 40%.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Habitual information source by frequency and sex**</caption>

<tbody>

<tr>

<th>Habitual information source</th>

<th>N</th>

<th>%</th>

<th>Male %</th>

<th>Female %</th>

</tr>

<tr>

<td>Someone with whom you have a strong relationship</td>

<td align="center">244</td>

<td align="center">39.9</td>

<td align="center">31</td>

<td align="center">45</td>

</tr>

<tr>

<td>The Internet</td>

<td align="center">241</td>

<td align="center">39.4</td>

<td align="center">46</td>

<td align="center">35</td>

</tr>

<tr>

<td>Phonebook, flyer, radio, TV, newspaper</td>

<td align="center">41</td>

<td align="center">6.7</td>

<td align="center">5</td>

<td align="center">8</td>

</tr>

<tr>

<td>Organization: public library, school, non-profit, crisis clinic or hotline, etc.</td>

<td align="center">38</td>

<td align="center">6.2</td>

<td align="center">6</td>

<td align="center">6</td>

</tr>

<tr>

<td>Other</td>

<td align="center">32</td>

<td align="center">5.2</td>

<td align="center">8</td>

<td align="center">4</td>

</tr>

<tr>

<td>Someone with whom you do not have a strong relationship</td>

<td align="center">10</td>

<td align="center">1.6</td>

<td align="center">2</td>

<td align="center">2</td>

</tr>

<tr>

<td>Department at your workplace</td>

<td align="center">6</td>

<td align="center">1.0</td>

<td align="center">2</td>

<td align="center">0</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">**612**</td>

<td align="center">**100**</td>

<td align="center">**100**</td>

<td align="center">**100**</td>

</tr>

</tbody>

</table>

Table 3 distills the rationales given through open-ended, multiple responses for selecting a preferred source of information. The most common explanation to this open-ended, multiple response question, focused on reliability or trustworthiness (32.4%). This characteristic was likely to be used by those who described their information source as someone with whom they have a close relationship, and it was named nearly twice as often as the next most common characteristic, convenience or accessibility (16.5%). The latter was especially important for those who chose the Internet as their primary information resource. A related rationale was characterizing a preferred resource as inexpensive (14.7%).

Compared to 35% of women, only 28% of men indicated that the reliability and trustworthiness of the information was a reason that they chose a source. The difference between the way that women and men answered this question may be related to the fact that women chose someone with whom they have a close relationship. When examined statistically, a difference exists between men and women's responses regarding source attributes at a .10 level but not at a .05 level (χ<sup style="font-size: x-small;">2</sup>=23.8061, df=10, p<=0.008). When data were analyzed according to income categories, there were relatively small differences in the way that respondents answered this question, and hence no relationship between income categories in the reasons that respondents chose an information source.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Reasons for the source or habit**</caption>

<tbody>

<tr>

<th>Source attribute</th>

<th>N</th>

<th>%</th>

<th>Male %</th>

<th>Female %</th>

</tr>

<tr>

<td>Gives reliable information/trustworthy</td>

<td align="center">280</td>

<td align="center">32.4</td>

<td align="center">28.0</td>

<td align="center">35.1</td>

</tr>

<tr>

<td>Quick to contact/access/convenient</td>

<td align="center">142</td>

<td align="center">16.5</td>

<td align="center">17.4</td>

<td align="center">15.9</td>

</tr>

<tr>

<td>Inexpensive</td>

<td align="center">127</td>

<td align="center">14.7</td>

<td align="center">16.1</td>

<td align="center">13.9</td>

</tr>

<tr>

<td>Easy to use or communicate with</td>

<td align="center">91</td>

<td align="center">10.5</td>

<td align="center">13.4</td>

<td align="center">8.9</td>

</tr>

<tr>

<td>Knows me and understands my needs</td>

<td align="center">77</td>

<td align="center">8.9</td>

<td align="center">5.6</td>

<td align="center">10.9</td>

</tr>

<tr>

<td>Anonymity</td>

<td align="center">54</td>

<td align="center">6.3</td>

<td align="center">6.5</td>

<td align="center">6.1</td>

</tr>

<tr>

<td>Broad based/diverse/not biased information</td>

<td align="center">33</td>

<td align="center">3.8</td>

<td align="center">4.3</td>

<td align="center">3.5</td>

</tr>

<tr>

<td>No response or missing</td>

<td align="center">32</td>

<td align="center">3.7</td>

<td align="center">5.6</td>

<td align="center">2.6</td>

</tr>

<tr>

<td>Provides emotional support</td>

<td align="center">20</td>

<td align="center">2.3</td>

<td align="center">2.2</td>

<td align="center">2.4</td>

</tr>

<tr>

<td>Can communicate face-to-face or by phone</td>

<td align="center">5</td>

<td align="center">0.6</td>

<td align="center">0.3</td>

<td align="center">0.7</td>

</tr>

<tr>

<td>Other</td>

<td align="center">2</td>

<td align="center">0.2</td>

<td align="center">0.6</td>

<td align="center">0.0</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">**863**</td>

<td align="center">**100.0**</td>

<td align="center">**100.0**</td>

<td align="center">**100.0**</td>

</tr>

<tr>

<td colspan="5">Pearson chi-square = 23.8061, df=10, p<=0.008</td>

</tr>

</tbody>

</table>

In terms of the types of information that people obtain from their primary sources, multiple-responses to this open-ended question indicated that people most commonly reported looking for general, everyday advice; other commonly needed information included recreational (hobbies and travel) and health and health care information (Table 4). Examples of responses included: 'information about medical conditions, directions for locating businesses, information about services we need', 'historical facts, medical condition diagnoses, and a map of Iraq', and 'movie times, new books and CDs'.

Further analysis showed that those seeking everyday advice, including child care preferred a close personal tie, while people who sought information related to hobbies and/or travel turned to the Internet. Likewise for those needing health care information, the Internet was slightly preferred over close ties. Some individuals interviewed described the encouragement they received from the social exchange of information. For example, 'How to keep on keeping on' and 'to participate in more activities and more encouragement' were some of the answers to this question. Others described skills gained from the information ground such as 'I learn patience. I also learn how to listen to people and not talk' or 'I have learned compassion for others'. These examples describe the social nature of the information exchange and emphasize a different result from obtaining data beyond the fulfillment of a particular information need.

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Type of information provided**</caption>

<tbody>

<tr>

<th>Type of information provided</th>

<th>N</th>

<th>%</th>

</tr>

<tr>

<td>Everyday advice</td>

<td align="center">166</td>

<td align="center">34.0</td>

</tr>

<tr>

<td>Hobbies, Travel</td>

<td align="center">107</td>

<td align="center">21.9</td>

</tr>

<tr>

<td>Health - Health care</td>

<td align="center">89</td>

<td align="center">18.2</td>

</tr>

<tr>

<td>Goods and Services</td>

<td align="center">45</td>

<td align="center">9.2</td>

</tr>

<tr>

<td>News, Current Events</td>

<td align="center">27</td>

<td align="center">5.5</td>

</tr>

<tr>

<td>Work related</td>

<td align="center">18</td>

<td align="center">3.7</td>

</tr>

<tr>

<td>Government services, financial & taxes</td>

<td align="center">18</td>

<td align="center">3.7</td>

</tr>

<tr>

<td>Entertainment</td>

<td align="center">8</td>

<td align="center">1.6</td>

</tr>

<tr>

<td>Studying or classes</td>

<td align="center">10</td>

<td align="center">2.0</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">**488**</td>

<td align="center">**100.0**</td>

</tr>

</tbody>

</table>

The respondents to this study recognized that their preferred information sources may have limiting characteristics. Indeed, in answering Question 4, over 40% indicated that one of the drawbacks to their source was that the information was not always reliable or trustworthy. Responses to this question affirm that respondents were most concerned with the quality of the information, having indicated that this was both the main benefit and drawback of their information source. While approximately 44% of respondents who chose the Internet as their primary source also indicated that it was not always reliable or trustworthy, only slightly fewer (39%) indicated that the same regarding strong or close interpersonal sources. When examined by income category, data revealed that respondents with the higher income categories were slightly more concerned with the reliability and trustworthiness of the data whereas the lower income categories were slightly more concerned with issues of access and expense.

## Information grounds

This study has provided the first opportunity to collect data to support or refute the information grounds theoretical framework from a large sample. Open-ended, multiple responses to Question 5, which asked residents to identify their information grounds, reflected the influence of a social setting for the exchange of information. The most commonly identified information grounds were places of worship, the workplace and _activity areas_, that is, clubs, teams, play groups and places associated with hobbies which accounted for over 50% of the responses (Table 5). Places such as these facilitated, in the words of one respondent, desirous _mingling_. Although, most respondents listed places where they have face-to-face interaction with others, a few did suggest the Internet as a place for this type of social exchange.

No statistically significant differences occurred between men and women's responses (Pearson χ<sup style="font-size: x-small;">2</sup> = 16.6821, df=12, p = .162). However, respondents from the lower income categories were more likely to answer a place of worship as a place where they shared information; respondents from the upper income categories (especially the category $100,000 and more) were more likely to respond that their workplace was a place they shared information (Table 6; χ<sup style="font-size: x-small;">2</sup>93.6505, df=96, p<=0.549).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Information grounds by frequency and sex**</caption>

<tbody>

<tr>

<th>Information ground</th>

<th>N</th>

<th>%</th>

<th>Male %</th>

<th>Female %</th>

</tr>

<tr>

<td>Place of worship</td>

<td align="center">92</td>

<td align="center">24.4</td>

<td align="center">24.6</td>

<td align="center">24.3</td>

</tr>

<tr>

<td>Workplace</td>

<td align="center">84</td>

<td align="center">22.3</td>

<td align="center">28.7</td>

<td align="center">19.2</td>

</tr>

<tr>

<td>Clubs, teams, play group or hobby</td>

<td align="center">40</td>

<td align="center">10.6</td>

<td align="center">9.0</td>

<td align="center">11.4</td>

</tr>

<tr>

<td>Gym</td>

<td align="center">31</td>

<td align="center">8.2</td>

<td align="center">9.0</td>

<td align="center">7.8</td>

</tr>

<tr>

<td>School or classes</td>

<td align="center">29</td>

<td align="center">7.7</td>

<td align="center">4.9</td>

<td align="center">9.0</td>

</tr>

<tr>

<td>Store or shopping mall</td>

<td align="center">22</td>

<td align="center">5.8</td>

<td align="center">5.7</td>

<td align="center">5.9</td>

</tr>

<tr>

<td>Food or drink place, coffee shop or bar</td>

<td align="center">21</td>

<td align="center">5.6</td>

<td align="center">6.6</td>

<td align="center">5.1</td>

</tr>

<tr>

<td>Other</td>

<td align="center">19</td>

<td align="center">5.0</td>

<td align="center">4.1</td>

<td align="center">5.5</td>

</tr>

<tr>

<td>Hair salon, barber shop or beauty salon</td>

<td align="center">16</td>

<td align="center">4.2</td>

<td align="center">0.8</td>

<td align="center">5.9</td>

</tr>

<tr>

<td>Waiting room (Doctors', car repair)</td>

<td align="center">9</td>

<td align="center">2.4</td>

<td align="center">2.5</td>

<td align="center">2.4</td>

</tr>

<tr>

<td>Travel</td>

<td align="center">5</td>

<td align="center">1.3</td>

<td align="center">1.6</td>

<td align="center">1.2</td>

</tr>

<tr>

<td>Library</td>

<td align="center">7</td>

<td align="center">1.9</td>

<td align="center">0.8</td>

<td align="center">2.4</td>

</tr>

<tr>

<td>No place</td>

<td align="center">2</td>

<td align="center">0.5</td>

<td align="center">1.6</td>

<td align="center">0.0</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">**377**</td>

<td align="center">**100.0**</td>

<td align="center">**100.0**</td>

<td align="center">**100.0**</td>

</tr>

</tbody>

</table>

<table width="100%" border="1" cellspacing="0" cellpadding="5" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: Information grounds by income level**</caption>

<tbody>

<tr>

<th>Source Attribute</th>

<th>N</th>

<th>%</th>

<th><$10k  
%</th>

<th>$10k-20k  
%</th>

<th>$20k-30k  
%</th>

<th>$30k-40k  
%</th>

<th>$40k-50k  
%</th>

<th>$50k-75k  
%</th>

<th>$75k-100k  
%</th>

<th>> $100k  
%</th>

</tr>

<tr>

<td>Place of worship</td>

<td align="center">79</td>

<td align="center">24.5</td>

<td align="center">44.4</td>

<td align="center">36.8</td>

<td align="center">20.8</td>

<td align="center">36.0</td>

<td align="center">31.6</td>

<td align="center">21.4</td>

<td align="center">23.2</td>

<td align="center">16.7</td>

</tr>

<tr>

<td>Workplace</td>

<td align="center">71</td>

<td align="center">22.0</td>

<td align="center">11.1</td>

<td align="center">21.1</td>

<td align="center">12.5</td>

<td align="center">12.0</td>

<td align="center">10.5</td>

<td align="center">22.9</td>

<td align="center">16.1</td>

<td align="center">32.2</td>

</tr>

<tr>

<td>Clubs/teams/playgroup/hobby</td>

<td align="center">35</td>

<td align="center">10.8</td>

<td align="center">11.1</td>

<td align="center">10.5</td>

<td align="center">12.5</td>

<td align="center">4.0</td>

<td align="center">15.8</td>

<td align="center">11.4</td>

<td align="center">16.1</td>

<td align="center">8.9</td>

</tr>

<tr>

<td>Gym</td>

<td align="center">28</td>

<td align="center">8.7</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">8.0</td>

<td align="center">5.3</td>

<td align="center">8.6</td>

<td align="center">14.3</td>

<td align="center">12.2</td>

</tr>

<tr>

<td>School or classes</td>

<td align="center">24</td>

<td align="center">7.4</td>

<td align="center">0.0</td>

<td align="center">5.3</td>

<td align="center">8.3</td>

<td align="center">8.0</td>

<td align="center">10.5</td>

<td align="center">4.3</td>

<td align="center">8.9</td>

<td align="center">7.8</td>

</tr>

<tr>

<td>Store/mall</td>

<td align="center">21</td>

<td align="center">6.5</td>

<td align="center">0.0</td>

<td align="center">10.5</td>

<td align="center">8.3</td>

<td align="center">4.0</td>

<td align="center">5.3</td>

<td align="center">5.7</td>

<td align="center">7.1</td>

<td align="center">7.8</td>

</tr>

<tr>

<td>Food/drink place/coffee shop or bar</td>

<td align="center">20</td>

<td align="center">6.2</td>

<td align="center">11.1</td>

<td align="center">0.0</td>

<td align="center">8.3</td>

<td align="center">8.0</td>

<td align="center">21.1</td>

<td align="center">8.6</td>

<td align="center">5.4</td>

<td align="center">2.2</td>

</tr>

<tr>

<td>Other</td>

<td align="center">15</td>

<td align="center">4.6</td>

<td align="center">0.0</td>

<td align="center">10.5</td>

<td align="center">12.5</td>

<td align="center">8.0</td>

<td align="center">0.0</td>

<td align="center">5.7</td>

<td align="center">3.6</td>

<td align="center">2.2</td>

</tr>

<tr>

<td>Hair salon/barber shop/beauty salon</td>

<td align="center">12</td>

<td align="center">3.7</td>

<td align="center">11.1</td>

<td align="center">5.3</td>

<td align="center">4.2</td>

<td align="center">12.0</td>

<td align="center">0.0</td>

<td align="center">5.7</td>

<td align="center">1.8</td>

<td align="center">1.1</td>

</tr>

<tr>

<td>Waiting room (Dr's, car repair)</td>

<td align="center">6</td>

<td align="center">1.9</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">8.3</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">1.4</td>

<td align="center">0.0</td>

<td align="center">3.3</td>

</tr>

<tr>

<td>Library</td>

<td align="center">6</td>

<td align="center">1.9</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">2.9</td>

<td align="center">3.6</td>

<td align="center">2.2</td>

</tr>

<tr>

<td>Travel</td>

<td align="center">5</td>

<td align="center">1.5</td>

<td align="center">11.1</td>

<td align="center">0.0</td>

<td align="center">4.2</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">1.4</td>

<td align="center">0.0</td>

<td align="center">2.2</td>

</tr>

<tr>

<td>No place</td>

<td align="center">1</td>

<td align="center">0.3</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">0.0</td>

<td align="center">1.1</td>

</tr>

<tr>

<th>Totals</th>

<th>323</th>

<th>100.0</th>

<th align="center">100.0</th>

<th align="center">100.0</th>

<th align="center">100.0</th>

<th align="center">100.0</th>

<th align="center">100.0</th>

<th align="center">100.0</th>

<th align="center">100.0</th>

<th align="center">100.0</th>

</tr>

</tbody>

</table>

We sought to determine the characteristics that foster information grounds as opportune places for receiving information, asking in Question 6, 'What makes this a good place for obtaining information, either accidentally or on purpose?' Responses focused on the people who were brought together by the information grounds. Many noted the opportunity to share common interests or needs, feeling as if the people understood their needs, and being able to trust them. One person described a health care facility as a place where 'I'm talking to people with similar life experiences that I've had'. Many people described their churches as places where they could find like-minded people, as several respondents explained: 'the people there have the same values that I have and the same type of concerns', 'we have similar values and the people are interested in your family's welfare', 'they're a good bunch of people', and it is 'a place you can trust'.

Some respondents valued the diversity in the social situation. One person described her neighbourhood restaurant as a place where there are 'people... from all walks of life'. Another respondent described his workplace as having a 'diversified population, we have so many points of view'. Some respondents described the qualities of the people that they associated with their information grounds. For example, they described them as 'genuine people' or said, 'I work with smart people'. Many respondents who named their church as a place of sharing information described the people as being 'friendly, honest people' or 'good people, trustworthy'.

Others seemed to describe the quality of the experience they had while sharing information. One person who said that her child's pre-school centre was a place of sharing information, described her reasons as 'getting to know people better, friendship bond, it's a local school'. Another person explained that her hair dresser and Bible study group were places where she found companionship. Many people described their information grounds as places where they connected with friends, such as the woman who said her restaurant is a 'good place to bump into friends'.

Responses also concentrated on the quality and quantity of the information exchanged. For example, one person said he valued the exchange of information at work because of the 'availability of lots information from many people'. Another person cited his Health Maintenance Organization as his information ground and said 'the people there are knowledgeable about health things'.

When asked for 'examples of information that you might pick up there', most respondents described local community information and services such as, 'what is the best school in the area and where to get your hair cut', 'what's going on in the area', 'find out about registering my cat', 'where to find help from outside contractors' or the 'job market' (Table 7). Others listed broader topics such as 'financial and spiritual information', or 'political information, good books, health experiences, and information about someone who I haven't seen for a while'.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7: Information grounds by information topic**</caption>

<tbody>

<tr>

<th>Topic</th>

<th>N</th>

<th>%</th>

<th>Male %</th>

<th>Female %</th>

</tr>

<tr>

<td>News, current events</td>

<td align="center">51</td>

<td align="center">20.3</td>

<td align="center">13</td>

<td align="center">9</td>

</tr>

<tr>

<td>Hobbies, travel</td>

<td align="center">49</td>

<td align="center">19.5</td>

<td align="center">13</td>

<td align="center">9</td>

</tr>

<tr>

<td>Everyday advice (childcare)</td>

<td align="center">46</td>

<td align="center">18.3</td>

<td align="center">19</td>

<td align="center">22</td>

</tr>

<tr>

<td>Entertainment</td>

<td align="center">32</td>

<td align="center">12.7</td>

<td align="center">13</td>

<td align="center">14</td>

</tr>

<tr>

<td>Goods and services</td>

<td align="center">28</td>

<td align="center">11.2</td>

<td align="center">9</td>

<td align="center">12</td>

</tr>

<tr>

<td>Health and health care</td>

<td align="center">18</td>

<td align="center">7.2</td>

<td align="center">18</td>

<td align="center">17</td>

</tr>

<tr>

<td>Work related</td>

<td align="center">16</td>

<td align="center">6.4</td>

<td align="center">7</td>

<td align="center">4</td>

</tr>

<tr>

<td>Studying or classes</td>

<td align="center">6</td>

<td align="center">2.4</td>

<td align="center">4</td>

<td align="center">8</td>

</tr>

<tr>

<td>Government services, financial or taxes</td>

<td align="center">5</td>

<td align="center">2.0</td>

<td align="center">4</td>

<td align="center">5</td>

</tr>

<tr>

<td>**Total**</td>

<td align="center">**251**</td>

<td align="center">**100.0**</td>

<td align="center">**100**</td>

<td align="center">**100**</td>

</tr>

</tbody>

</table>

## Future analysis and research

This study enabled us to gain data from a large, randomly selected, urban and rural population about their information habits and information grounds. We plan to further analyse our data along several lines. Regarding the information habit data, we will examine statistical relationships among different variables, and compare the results with findings from related studies regarding everyday information behaviour. We intend to use approaches such as Savolainen's ([1995](#Savolainen)) everyday life information seeking _habitus_ framework. Further analysis and research are needed to understand the strong popularity of the Internet as people's habitual information source, an emergent phenomenon that is displacing the long-standing tradition of the strong-tie, interpersonal source.

The notion of information grounds may be only newly proposed in the literature, but the phenomenon itself is not new, only its identification. Linked strongly with people's natural inclination of constructing and sharing information interpersonally and thus socially, information grounds have existed since time immemorial and yet, little research, at least from an information behaviour perspective, has explored their nature. We plan, therefore, to spend significant energy further analysing information grounds: specifically we wish to derive an information grounds typology that captures the nuances among such information grounds as coffee shops, waiting rooms in auto repair shops and medical offices, grocery shop queues, communal laundry rooms, ferries, and the luggage carousel at airports. In addition to other facets, we expect to classify information grounds by:

*   focal activity;
*   actor and social type roles;
*   the effect of information type (information for trivial versus significant decisions; insider versus outsider)
*   motivation (voluntary versus forced, e.g., choir groups versus _hostage_ settings such as waiting rooms)
*   membership size and type (open versus closed)

We also plan to analyse and study information grounds in terms of how information needs are expressed and recognized, how information is socially constructed among different actors, how people's perceptions and participation in information grounds change over time, the life-cycles of information grounds (how they are created and sustained, and what causes them to disappear or transform), and how they can be used to facilitate information flow such as in the examples provided earlier. In this effort we will add data that addresses information habits and information grounds from three other related studies (with the general public in the Pacific Northwest; Hispanic farm workers, and people who are homeless) in addition to focusing upon other populations through ethnographic study. Given the high incidence of places of worship and the workplace, we plan to focus initial attention there. The effects of the workplace information grounds on people who lose their jobs, for example, will be of particular interest as we hypothesize that employers can alleviate the stressors of unemployment by helping employees establish or identify replacement information grounds that will facilitate the availability of information needed during such a transitional time.

## Notes

<a id="note1" name="note1"></a>1\. _United Way of America_: a national body for the United Way movement, consisting of community-based United Way organizations that work, through fund-raising, to improve life in the community.


## References

*   <a name="FactFinder" id="FactFinder"></a>_[American FactFinder](http://factfinder.census.gov/servlet/BasicFactsTable?_lang=en&_vt_name=DEC_2000_SF1_U_DP1&_geo_id=05000US53033)_. (2004). Washington, DC: US Department of Commerce, Census Bureau. Retrieved 12 September, 2004 from http://factfinder.census.gov/servlet/BasicFactsTable?_lang=en&_vt_name=DEC_2000_SF1_U_DP1&_geo_id=05000US53033
*   <a name="Case" id="Case"></a>Case, D.O. (2002). _Looking for information: a survey of research on information seeking, needs, and behavior_. Amsterdam: Academic Press.
*   <a name="Chen" id="Chen"></a>Chen, C. & Hernon, P. (1982). _Information-seeking: assessing and anticipating user needs_. New York, NY: Neal-Schuman.
*   <a name="Dervin1976" id="Dervin1976"></a>Dervin, B., Zweizig, D., Banister, M., Gabriel, M., Hall, E. P., & Kwan, C., (with Bowes, J, & Stamm, K.). (1976). _The development of strategies for dealing with the information needs of urban residents: Phase I: The citizen study. Final report of Project L0035J to the U.S. Office of Education._. Seattle, WA: School of Communications. (ERIC: ED 125640)
*   <a name="Dervin1986" id="Dervin1986"></a>Dervin, B., & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33.
*   <a name="Fisher" id="Fisher"></a>Fisher, K.E., Durrance, J.C., & Hinton, M.B. (2004). Information grounds and the use of need-based services by immigrants in Queens, NY: a context-based, outcome evaluation approach. _Journal of the American Society for Information Science & Technology_, **55**(8), 754-766.
*   <a name="Harris" id="Harris"></a>Harris, R.M., & Dewdney, P. (1994). _Barriers to information: how formal help systems fail battered women_. Westport, CT: Greenwood.
*   <a name="Krippendorff" id="Krippendorff"></a>Krippendorff, K. (1980). _Content analysis: an introduction to its methodology_. Newbury Park, CA: Sage.
*   <a name="Marcella2000" id="Marcella2000"></a>Marcella, R., & Baxter, G. (2000). Information need, information seeking behaviour and participation, with special reference to needs related to citizenship: results of a national survey. _Journal of Documentation,_ **56**(2), 136-160.
*   <a name="Marcella2001" id="Marcella2001"></a>Marcella, R., & Baxter, G. (2001). A random walk around Britain: a critical assessment of the random walk sample as a method of collecting data on the public's citizenship information needs. _New Review of Information Behaviour Research_, **2**, 87-103\.
*   <a name="McKechnie" id="McKechnie"></a>McKechnie, L.E.F., Baker, L., Greenwood, M. & Julien, H. (2002). Research methods in human information literature. _The New Review of Information Behaviour Research_, **3**, 113-126.
*   <a name="Oldenburg" id="Oldenburg"></a>Oldenburg, R. (1999). _The great good place: cafes, coffee shops, bookstores, bars, hair salons and other hangouts at the heart of a community_. New York: Marlowe.
*   <a name="Ontario" id="Ontario"></a>Ontario Ministry of Culture and Communications. (1991). _Review of access to human services information_. Toronto: Queen's Printer for Ontario.
*   <a name="Palmour" id="Palmour"></a>Palmour, V., Rathbun, P., Brown, W., Dervin, B. & Dowd, P. (1979). _Information needs of Californians. Commissioned by California State Library for the California Governor's Conference on Libraries and Information Services_. Rockville, MD: King Research Inc.
*   <a name="Pettigrew1998" id="Pettigrew1998"></a>Pettigrew, K.E. (1998). _The role of community health nurses in providing information and referral to the elderly: a study based on social network theory_. London, ON: The University of Western Ontario.
*   <a name="Pettigrew1999" id="Pettigrew1999"></a>Pettigrew, K. E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behavior among attendees at community clinics. _Information Processing & Management_, **35**(6), 801-817\.
*   <a name="Pettigrew2001" id="Pettigrew2001"></a>Pettigrew, K.E., Fidel, R., & Bruce, H. (2001). Conceptual frameworks in information behavior. _Annual Review of Information Science & Technology_, **35**, 43-78.
*   <a name="Savolainen" id="Savolainen"></a>Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "way of life." _Library and Information Science Research, **17(**3)_, 259-294.
*   <a name="Tuominen" id="Tuominen"></a>Tuominen, K., & Savolainen, R. (1997). A social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen, & B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts, 14-16 August, 1996, Tampere, Finland_ (pp. 81-96). London: Graham Taylor.
*   <a name="Warner" id="Warner"></a>Warner, E. S., Murray, A.D., & Palmour, V.E. (1973). _Information Needs of Urban Residents. Final Report of the Baltimore Regional Planning Council and Westat Inc._ Washington, DC: U.S. Department of Health, Education and Welfare, Division of Library Programs. (ERIC: ED 088464).
*   <a name="Wilson" id="Wilson"></a>Wilson, T.D. (2000). [Human information behavior](http://inform.nu/Articles/Vol3/v3n2p49-56.pdf). _Informing Science_, **3**(2), 49-55\. Retrieved 23 January, 2004 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf
*   <a name="Zweizig" id="Zweizig"></a>Zweizig, D., & Dervin, B. (1977). Public library use, users, uses: advances in knowledge of the characteristics and needs of the adult clientele of American public libraries. _Advances in Librarianship_, **7**, 231-255.



