#### Vol. 10 No. 2, January 2005

# Managers look to the social network to seek information

#### [Maureen L. Mackenzie](mailto:mackenzm@dowling.edu)  
School of Business, Dowling College  
Oakdale, New York 11769, USA


#### Abstract

> **Introduction.** The purpose of this study was to explore how managers selected individuals to serve as information sources. The social context of a for-profit business environment offered opportunity to study information seeking among interacting line-managers.  
> **Method.** The qualitative methods of social network mapping and interview were used to capture the data. The study was conducted within a stand-alone business unit of a major US-based corporation. A total of twenty-two line-managers participated in the study.  
> **Analysis.** Content analysis was selected as the data analysis technique. The elements of interest were the themes within the data. Open coding was used to interrogate the data to ensure a systematic approach so that future researchers can replicate the process.  
> **Results.** Relationship, more than knowledge, can be the reason a line-manager is sought as an information source. In addition to relationship, an individual manager's knowledge, communication behaviour, cognitive style, and cognitive ability play an influencing role in being selected as an information source.  
> **Conclusion.** The non-hierarchical flow of information among managers and the reasons managers seek others as information sources further differentiates line-managers as a unique information user group.


## Introduction and background

A series of three research studies was planned to investigate the information seeking behaviour of line-managers. At the 2002 ISIC conference the results of the first of three was presented ([Mackenzie 2003](#mac03)). This quantitative study embraced the work of both information science and management theorists ([McKenney & Keen 1974](#mac74); [Mintzberg 1973](#min73); [Kotter 1982](#kot82); [Lindblom 1959](#lin59); [Brewer & Tomlinson 1964](#bre64); [Simon 1955](#sim55), [1957](#sim57), [1976](#sim76); [Luthans 1988](#lut88); [Katzer & Fletcher 1992](#kat92) and [Grosser 1991](#gro91)). The statistical results suggested that line-managers tend 'to engage a broad range of information regardless of their area of responsibility... Managers are cooperative and friendly and their relationships with colleagues, subordinates and superiors may add to their success on the job as well as provide access to information' ([Mackenzie 2003](#mac03): 75). This information behaviour leads to an accumulation of knowledge stored in the individual's _cognitive savings account_. When decision opportunities emerge, the manager is able to draw from these stored _funds_. This behaviour assists the line-manager in reducing uncertainty, making decisions on the spot and projecting a public image of being _in the know_ ([Mackenzie 2003](#mac03)).

The second study, reported here, builds upon these quantitative results to provide further insight into the information seeking behaviour of line-managers. In this study I investigated the social information relationships among a group of interacting line-managers and identified the factors that influenced these managers' selections of certain individuals as information sources.

## Literature review

Classic management research has revealed that managers prefer to receive their communication orally, do little with the more routine paper reports that provide expected or even stale information, and prefer to use the most current information flow. As a result, managers spend a substantial percentage of their time in personal contact with people both inside and outside the organization ([Mintzberg 1973](#min73); [Luthans 1988](#lut88); [Kotter 1982](#kot82)).

Luthans _et al._ ([1985](#lut85)) and Luthans ([1988](#lut88)) found that the two activities most related to managerial success were interaction with people outside the organization, such as suppliers and customers, and socializing and politicking within the organization. Luthans stated that 'networking makes the biggest relative contribution to manager success' ([Luthans 1988](#lut88): 130).

The theories that underlie cooperation, social networks, and cognitive authority offer plausible explanation as to how and why the individual managers within a business environment interact with one another to gain access to the information flow. Cooperation leads to the formation of the organization's emergent social network ([Tichy & Fombrun 1979](#tic79)) where information can be gathered and traded in the information marketplace ([Davenport & Prusak 1998](#dav98)). The concept of cognitive authority ([Wilson 1983](#wil83)) provides insight on how a manager determines what information is credible by considering the source of the information.

### Cooperation and friendliness.

Experienced managers know that reciprocity is the norm within the business environment. They recognize that 'the individual situation of each of those cooperating will be improved' ([Barnard 1968](#bar68): 52). As a result, managers develop 'networks of cooperative relationships using a wide variety of face-to-face methods. They try to make others feel legitimately obliged to them by doing favours or by stressing their formal relationships... They carefully nurture their professional reputations in the eyes of others' ([Kotter 1982](#kot82): 162). Friendliness, though perceived as a social choice, is a factor that assists managers in getting what they need ([Simon 1976](#sim76)). Simon suggested that it is a major task of managers 'to maintain attitudes of friendliness and cooperation... so that the informal communication system will contribute to the efficient operation of the organization rather than hinder it' ([Simon 1976](#sim76): 161).

### Social networks.

The knowledge of the organization flows through the informal social networks; 'people ask each other who knows what' ([Davenport & Prusak 1998](#dav98): 37). Business managers may accumulate bits and pieces of useful information that are personally irrelevant but can be traded within the information marketplace for information that is relevant to their own needs and business roles ([Davenport & Prusak](#dav98) 1998). The metaphor of a cognitive savings account, where managers can accumulate information for which there is no current or pressing need, fits well in a framework that considers the business environment as an information marketplace where accumulated information has value as a commodity and can be drawn upon when needed.

### Cognitive authority.

Managers depend on each other for ideas and information to form a shared viewpoint. To explore how managers accumulate information that leads to shared values and viewpoints, an understanding of the social network and how individuals select information sources is required. Looking for this information within the prescribed organization may be misleading. The formal organizational hierarchy may not reflect the flow of company knowledge among its employees ([Davenport & Prusak 1998](#dav98)). Therefore, the primary research question for this study is: What influences a manager to seek and select an information source?

Wilson had considered the question as to what criteria exist for selecting an information source. He wrote that 'authority can be justified simply on the grounds that one finds the views of an individual intrinsically plausible, convincing, or persuasive' ([Wilson 1983](#wil83): 24). Without a demand for critical thinking, non-experts can become influential through the power of persuasion by presenting plausible and convincing viewpoints that may not always be in the best interest of the organization. As a result, understanding what factors influence a manager to select an information source is the focus of this research study.

## Methodology

The purpose of this study was to explore how managers selected individuals to serve as information sources. The research was designed to capture an intimate view of information interaction and flow among a group of interacting line-managers. The qualitative methods of social network mapping and interview were used to capture the data. Qualitative rather than quantitative methods were selected because I am interested in exploring meaning and how a group of managers make sense of their professional experiences and negotiate their prescribed organizational structure ([Creswell 1994](#cre94); [Tichy & Fombrun 1979](#tic79)).

### Research questions

The research questions for the investigation were:

*   What influences a line-manager to seek and select an information source?
*   What can be learned when the information relationships among a group of interacting line-managers are graphically represented?
*   What are the primary reasons a line-manager is selected as an information source?
*   Who are the gatekeepers or cognitive authorities within this business environment?

### Social network mapping

By using social network data to express the relationships among the interacting managers I was permitted a unique view of the intellectual structure of this business environment ([White 2000](#whi00)). The intellectual structure may not always be explicit because managers spend much of their time communicating orally and many decisions are made without documentation. Uncovering the information-gathering relationships may allow for observation of both the social and the intellectual structure of this business environment.

### The business environment

The study was conducted within an autonomous business unit of a major US-based corporation (also referred to in this study as the business environment). The corporation is among the largest publicly-traded companies in the insurance industry. The business unit controls the sales, technology, education, personnel, public relations, budgeting, and support functions for the business conducted within the US state in which it is located. The culture is moderately strong and portrays a moderate _work hard and play hard_ culture ([Deal & Kennedy 2000](#dea00)). The business unit is under the direction of an officer-level executive (Vice President) who maintains his office in the business unit. The leadership team is composed of the department heads, senior sales managers, and the Vice-president. The front-line managers and divisional managers are the link between the leadership team and the non-manager employee group. Figure 1 illustrates the reporting relationships.

<div align="center">![fig1](p216fig1.gif)</div>

<div align="center">  
**Figure 1: Organizational reporting hierarchy**</div>

### Subjects

The term line-manager is defined as a management level employee responsible for more than two other employees within the organization. A total of twenty-two line-managers participated in the study. Of these, eighteen were male and four were female. The group had an average service with the company of fifteen years (sd=4.94) with a minimum of five years and a maximum of twenty-five years of service.

### Data collection and procedure

The following steps were established to collect and analyze the social network data:

1.  The senior sales managers were selected as the starting point for inquiry. They were chosen because they lead the business unit's top-line results and most likely interact more dynamically with other managers than any other group.
2.  The snowball method was used to collect the data rather than the full network method or the ego-centric network method ([Hanneman 2000](#han00): 7-9):

    > Snowball methods begin with a focal actor or set of actors. Each of these actors is asked to name some or all of their ties to other actors. Then, all the actors named... are tracked down and asked for some or all of their ties. The process continues until no new actors are identified' ([Hanneman 2000](#han00): 8).

3.  Each manager was asked a series of three questions.
4.  A social network map ([Hanneman 2000](#han00): 21) was constructed revealing the direction from each inquiring manager toward each manager who was sought for information.
5.  The answers from the interview questions as to why the manager sought out the specified individual as an information source were coded and condensed into a set of descriptive terms.
6.  The descriptive terms used by the managers were condensed and categorized into the major themes.

Content analysis was selected as the data analysis technique. The elements of interest for this study were the themes within the data. Open coding was used to interrogate the data to ensure a systematic approach so that future researchers can replicate this process.

## Results

A social network map was created to illustrate the information seeking relationships among the interacting line-managers. I asked each manager who he or she depended on as a source for information and why. The aims were (1) to gather data on the information relationships on the job, and (2) to identify the line-managers within the business environment that may serve as gatekeepers ([Allen 1970](#all70)), knowledge brokers ([Davenport and Prusak 1998](#dav98)), and cognitive authorities ([Wilson 1983](#wil83)). The results are presented in three subsections: sociometric map, reasons for selecting a manager as an information source, and reasons for being selected by another manager as an information source.

### Sociometric map

A graphical expression of the network data is presented in Figure 2\. Each node represents one manager. A manager was only selected to be included in the social network map if he or she had been identified as an information source by another manager previously interviewed

<div align="center">![fig2](p216fig2.gif)</div>

<table align="center" width="674" style="font-size: smaller; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th>Legend:</th>

</tr>

<tr>

<td>**H(#)** **Department head.** A department head is a member of the leadership team, but is not a senior sales manager (includes risk management, technology, finance and personnel). The number next to each H is used as a label to aid discussion. It has no qualitative value.</td>

</tr>

<tr>

<td>**SS(#)** **Senior sales manager.** This manager is also a member of the leadership team. The senior sales managers were the focal group. The interview process that resulted in the social network map started with this group of eight managers. They are represented in rectangular boxes. The number next to each SS is used as a label to aid discussion and has no qualitative value.</td>

</tr>

<tr>

<td>**s(#)** **Front-line** sales manager. This manager reports to a senior sales manager. Sales representatives report to the frontline sales managers.  
The number next to each 's' is used as a label to aid discussion. It has no qualitative meaning.</td>

</tr>

<tr>

<td>**D(#)** **Divisional manager.** This manager reports to one of the four department heads. Both non-managers and managers may report to a divisional manager. The number next to each D is used as a label to aid discussion. It has no qualitative value.</td>

</tr>

<tr>

<td>**/L** **Local manager.** The /L indicates that this manager is responsible for an operation located in or within driving distance from business unit main office.</td>

</tr>

<tr>

<td>**/R** **Remote manager.** The /R indicates that this manager is responsible for an operation located away from the main office of the business unit.</td>

</tr>

<tr>

<td>**Vice-president** **Vice President.** The leadership team reports to the Vice-president who is an officer of the company.</td>

</tr>

<tr>

<td>![box1](p216fig2a.gif) **Members of the leadership team**, which includes the senior sales managers (SS) and department heads (H). The leadership team reports to the Vice-president.</td>

</tr>

<tr>

<td>![box2](p216fig2b.gif) **Divisional managers.** All divisional (D) managers report to one of the four department heads (H).</td>

</tr>

<tr>

<td>![box3](p216fig2c.gif) **Front-line sales managers.** All front-line sales managers (s) report to one of the eight senior sales managers (SS).</td>

</tr>

</tbody>

</table>

<div align="center">  
**Figure 2: Sociometric map illustrating an information network**</div>

The eight senior sales managers (focal group) are represented by the rectangular nodes. The inclusion of all other managers stems from these original interviews (circular nodes). Each node is coded to describe the manager's role within this business environment. Twenty-two nodes are illustrated, but only twenty-one are included in the data analysis. The node described as Vice President (Vice-president) was shown as an information source for one manager (H2) but the node was not followed because the Vice-president is an officer of the company and, therefore, plays a unique role with a reporting relationship that leads outside of this business unit. Except for the Vice-president node, the social network map reflects a closed network. Each manager's sources were followed until no new managers were identified.

The nodes in Figure 2 are connected with the arrowhead pointing to the manager's information source. Eight of the arrows reflect reciprocal relationships. The remaining relationships are independent (one-way relationships).

Table 1 ranks the top seven managers by the number of times he or she was named as an important information source. The remaining fourteen managers were either named as a source by only one other manager or not named at all.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Managers ranked by number of times named as a source**</caption>

<tbody>

<tr>

<th> </th>

<th>Manager's role in organization</th>

<th>How many selected  
this manager  
as a source</th>

</tr>

<tr>

<td>H1</td>

<td>Personnel manager</td>

<td align="center">6</td>

</tr>

<tr>

<td>H2</td>

<td>Finance manager</td>

<td align="center">6</td>

</tr>

<tr>

<td>H3</td>

<td>Risk manager</td>

<td align="center">4</td>

</tr>

<tr>

<td>H4</td>

<td>Technology manager</td>

<td align="center">3</td>

</tr>

<tr>

<td>SS1/R</td>

<td>Senior Sales Manager - Remote</td>

<td align="center">3</td>

</tr>

<tr>

<td>SS2/L</td>

<td>Senior Sales Manager - Local</td>

<td align="center">3</td>

</tr>

<tr>

<td>s3/R</td>

<td>Frontline sales manager-remote</td>

<td align="center">2</td>

</tr>

</tbody>

</table>

### Reasons for selecting a manager as an information source

When aggregated, the descriptive terms provided by each manager tell a story as to why certain individuals were selected as information sources. Five themes label the reasons:

*   **_Relationship:_** an individual is selected as an information source because of the relationship he or she has with the manager as well as with others in the organization. Confidentiality is a valued attribute of a good relationship.
*   **_Knowledge_**: an individual is selected as a source because of the knowledge they possess. The information received from this individual can be trusted to be accurate.
*   _**Communication behaviour**_: an individual is selected because his or her behaviour reflects a willingness to share information. The individual may be helpful, respond promptly as well as appear to be approachable, easy to talk to and willing to listen.
*   _**Communication style**_: an individual is selected because his or her communication style is candid and practical. He or she may be able to make complex issues easy to understand and then able concisely to articulate good advice. This individual may respond as the voice of reason in difficult situations.
*   _**Cognitive ability**_: an individual is selected because of his or her cognitive ability. The individual has a well-developed intuition and displays strong insight. Also, the individual may be viewed as intelligent with a cognitive approach to problems that is global in nature.

The results reveal that the relationship with the individual (Figure 3) was the primary reason for selecting that individual as an information source (for the detailed results, see, [Appendix 1](#app1)).

<div align="center">![fig3](p216fig3.gif)</div>

<div align="center">  
**Figure 3: Reasons for selecting an information source**</div>

#### Do managers within different roles have different reasons for selecting an individual as an information source?

I have identified and defined six roles that were used to compare the results.

*   Leadership team: included the four department heads and the eight senior sales managers.
*   Front-line managers: included all the frontline sales managers who reported to the eight senior sales managers and the three divisional managers who reported to the department heads.
*   Local managers: includes the fourteen managers (four department heads, six senior sales managers, two front-line sales managers, two divisional managers) who were located within or relatively close to the physical business unit.
*   Remote managers: included the seven managers (two senior sales managers, one divisional manager and four front-line sales managers) who were housed beyond a reasonable driving distance from the physical business unit.
*   Male managers: included seventeen male managers.
*   Female managers: included four female managers.

The results revealed that relationship was the primary reason for selecting an individual as a source for all six categories or roles. Members of the leadership team are clearly influencedby the relationship (41%) with the individual (Figure 4). Relationship (26%), knowledge (22%), and communication behaviour (22%) are all influencing factors for front-line managers. Worth mentioning is that the sub-group of front-line sales managers rely more heavily on knowledge and communication behaviour than on relationship when selecting a source ([Appendix 1](#app1)).

<div align="center">![Fig4](p216fig4.gif)</div>

<div align="center">Figure 4: Reasons for selecting an individual as an information source. Leadership team vs. front-line managers.</div>

In comparing remote managers to local managers (Figure 5), it appears that both groups will most often select an individual because of _relationship_, but remote managers are heavily influenced by _communication behaviour_ as well as relationship when selecting an individual as an information source.

<div align="center">![fig5](p216fig5.gif)</div>

<div align="center">Figure 5: Reasons for selecting an individual as an information source. Local vs. remote managers.</div>

When comparing female to male managers, _relationship_ once again emerged as the primary reason, but the secondary influence for men is _knowledge_, while the secondary influence for women is _communication behaviour_ (Figure 6).

<div align="center">![fig6](p216fig6.gif)</div>

<div align="center">Figure 6: Reasons for selecting an individual as an information source. Male vs. female managers.</div>

#### Extended sample of front-line managers

The design of the social network data collection led to the inclusion of all leadership team members. In comparison, only a segment of front-line managers was included (not all). To investigate whether or not the themes reported in Figure 4 (comparison of leadership team to front-line managers) were valid, I extended my data collection to include an additional thirteen front-line managers (ten front-line sales managers and three divisional managers). The extended sample of front-line managers is not unusually different from the original sample drawn into the sociometric map. I interviewed these thirteen managers using the same questions as had been used with the original focal group. My intent in extending the sample was to reduce any sampling bias. The results reported in Figure 7 compare the leadership team to a total of twenty-two front-line managers rather than the original nine front-line managers.

<div align="center">![fig7](p216fig7.gif)</div>

<div align="center">Figure 7: Extended sample: reasons for selecting an individual as an information source. Leadership team vs. front-line managers.</div>

In comparing reasons why a manager will select another manager as an information source, the results reported in Figure 7 reflect a clear difference between leadership team members and front-line managers. As previously reported (Figure 4), leadership team members are influenced by _relationship_ to a greater extent than are front-line managers. Front-line managers (Figure 7) are influenced most often by _knowledge_ (46%) when selecting an information source.

#### Reasons for being selected by another manager as an information source

So far I have reported the reasons why a manager will select a certain individual as an information source; but each of these managers is a member of a prescribed management group and membership of that group may influence his or her selection as an information source for others. The data reveals (Figure 8) that _relationship_ (40%) is a primary reason for selecting a type of manager as an information source ([Appendix 2](#app2)).

<div align="center">![fig8](p216fig8.gif)</div>

<div align="center">Figure 8: Reasons for being selected as an information source by another manager</div>

When deciding to select a leadership team member, _relationship_ is the primary influence followed by the individual's _knowledge_ and _cognitive ability_. But when deciding to select a front-line manager as a source, although the _relationship_ is primary, the individual's _communication behaviour_ emerged as a strong secondary influence (Figure 9).

<div align="center">![fig9](p216fig9.gif)</div>

<div align="center">Figure 9: Leadership team vs. front-line managers. Reasons for being selected as an information source by another manager</div>

In comparing the remote managers to the local managers (Figure 10) _relationship_ emerged as a primary reason for both groups, but with greater strength for the local managers.

<div align="center">![fig10](p216fig10.gif)</div>

<div align="center">Figure 10: Local vs. remote managers. Reasons for being selected as an information source by another manager</div>

And finally, when comparing men to women, _relationship_ once again emerged as the primary reason for both groups (Figure 11).

<div align="center">![fig11](p216fig11.gif)</div>

<div align="center">Figure 11: Male vs. female managers. Reasons for being selected an information source by another manager</div>

## Discussion

### An information network of managers - the emergent structure

In viewing the social map (Figure 2) a fairly integrated network emerges. Although many of the managers rely on the same people, there are clearly outliers. The senior sales managers (rectangles) are not _go to_ people for the larger family of managers. By virtue of their role, it appears that the senior sales managers are seekers of support and rely on the inside network (department heads), except for the two remote senior sales managers who link to the larger network through the front-line divisional managers. The two least-tenured senior sales managers (SS3 and SS4) also link back to the larger network through lower level front-line managers. It is supposed that as the managers' tenure and experience increase they are drawn closer to the centre of the information network. One senior sales manager stands out (SS2/L) as a source for non-sales managers. SS2 is the longest-tenured senior sales manager. It appears that non-sales managers (especially financial managers) may use SS2's viewpoint as a surrogate view of all senior sales managers when an opinion is sought.

#### Department heads

The department heads lie at the centre of the network. The four department heads (H) each control a different function in the business unit. It is reasonable that as a group they would be sought after by the senior sales managers. The Personnel and Finance managers emerged as the top _go to_ managers in the business unit. If I were to predict the top _go to_ department heads solely based on position I would suggest the technology manager (H4) and the risk manager (H3) because they are closer to the core business functions. The personnel function is not traditionally at the core of a sales-focused operation. The comments that offer explanation as to the flow within the network suggest that it is the people in these positions that are selected as _go to_ managers, rather than the position. Although the technology manager and the risk manager are highly regarded and sought after, the personnel manager and finance manager each plays a larger role than their positions would suggest and each acts as a gatekeeper (personnel) and a cognitive authority (finance manager).

#### Front-line managers

Although grouped together, there is a distinction between the front-line sales manager and the divisional managers. Divisional managers (D) can be described as department heads (H) in training. Their influence is growing. The divisional managers are closer to the centre of the network. Four of the senior sales managers use the divisional managers as links to the larger network. The divisional managers are subordinate in position and salary grade to the senior sales managers, but may have greater access to the information held by the department heads. Senior sales managers who are remote (SS1/R and SS8/R) or less tenured (SS3/L and SS4/L) may have a less developed relationship and may not want to engage one of the department heads if the information sought is of an explanatory nature or if they are uncertain about what they need. These less-connected managers may not want to risk criticism or a perception that they do not know what they should know by seeking advice from the department heads.

Front-line sales managers (s) are senior sales managers in training. They are not necessarily _go to_ people for other managers, but rather require support. The remote front-line managers depend primarily on their bosses for information and guidance. The local front-line sales managers identified as s6/L and s5/L have a more broadly developed network due to employment tenure and proximity to other managers, which is more difficult for the remote front-line sales managers because of the limited opportunity to see and talk to the leadership team members on any regular basis.

#### Remote managers

The group of managers clustered around SS1R (top left quadrant of Figure 2) illustrates the dependency that develops when the proximity of the larger network is remote. Relationships cannot easily develop. The remote staff considers themselves outsiders and clearly not within the _in_ group that manages and influences the business unit.

#### Vice-president

The Vice-president is charismatic, actively involved, intelligent and clearly the leader of the business unit; but, from among all his managers, only the finance manager named the Vice-president as an information source. This observation has an unspoken implication. I suggest that although the other department heads and senior sales managers have a well-developed reporting relationship to the Vice-president, they are reluctant to reach out to him for information, though it has been stated that he has more access to information due to his position as an officer of the company. But why would managers not use this readily available source? I suggest it is to maintain their autonomy in managing their own operations without inviting his opinion, which may limit their decision alternatives.

### Reasons managers select or are selected as information sources

A relationship with an individual was the primary influence in selecting a source for information. Managers prefer to seek out individuals they know, like, or trust more often than individuals who are the foremost subject experts. This insight emerged clearly when condensing the descriptive terms managers used to describe their sources. These results align with what is known about managers ([Kotter 1982](#kot82), [Simon 1976](#sim76)) and the need to protect and project a good image at work. If information is power and information is gained through cooperation, then relationship should be a strong influence within a functional information network of managers. Knowledge, communication style, communication behaviours and cognitive ability are also highly valued, but are secondary influences. It would be unwise for managers to seek out expert advice, if vulnerability to criticism or damage to self-image or public-image could result ([Ashford and Cummings 1983](#ash83)).

The insight that relationships on-the-job lead to information access appears to be less developed with junior (front-line) managers and more evident with senior managers. Junior (front-line) managers are most likely still learning how to navigate the complex waters of their business environment. Front-line managers (versus leadership team members) seek answers that are right, explaining why knowledge emerged as an important attribute when selecting an information source (Figure 6). The differences between junior and senior managers suggest that junior managers learn by observing, or under the tutelage of their bosses, allowing them to effectively slip into senior manager roles when opportunities arise.

The value of this research is in the ability to view the line-managers within their live business environment. The context of the environment within which managers interact begins to take shape and provide clues as to why managers may behave as they do. The non-hierarchical flow of information among managers and the reasons managers seek others as sources will differentiate and document managers as a unique user-group.

## Conclusions

The results and insights offer an insider view of manager information seeking behaviour and reveal the complex nature of individuals who choose to be line-managers. The economic or rational assumption that a manager will seek out a colleague as an information source because he or she values the individual's level of knowledge is not always the correct assumption. The results support that relationship, more than knowledge, is the reason an individual is sought as an information source. A plausible explanation for such an insight is that seeking information under pressure is an uncomfortable behaviour for managers; they prefer to be the source, solution, and providers of information. Also, because of perceptions defining their role, managers are expected to have answers on demand. Therefore, when a manager must reach out, a trusting relationship is preferred despite the apparent opportunity cost. Managers prefer to seek out individuals they know, like or trust more often than individuals who are the foremost subject matter experts.

The results of this study offer a different perspective of how a specific user-group may deal with the bombardment of potentially relevant information. Insights can help corporate leadership and information architects to develop training, orientation and enculturation processes that will maximize manager effectiveness rather than developing systems that may not align with manager tendencies. The results of this study offer organizational leaders a perspective of how closely integrated the flow of information is with the relationships among interacting managers.

Although the results of this study offer plausible explanation as to why managers behave as they do within this specific information environment, generalization of results to the population is not permitted by the research design and sampling procedure selected. The resulting suppositions must be further tested before results can be generalized. Theory building is a labour-intensive process and requires many layers of empirical evidence.

The third study in the series of research studies has been completed and builds upon the quantitative and qualitative results of the first two studies. The focus is on the information seeking behaviour of high-performing, successful, non-management, sales producers. The social network will be expressed and investigated. The results will be compared to the information seeking behaviour of managers working within the same business culture. Though exploratory in nature, this study can also be used as a jump off point for further user-studies that focus on managers working within different industries and under different information-need scenarios.

## Acknowledgements

## References

*   <a id="all70" name="all70"></a>Allen, T.J. (1970). Roles in technical communication networks. In E.N. Carnot and Donald K. Pollack (Eds.), _Communication among scientists and engineers_ (pp 191-208). Lexington MA: D.C. Heath and Company.
*   <a id="ash83" name="ash83"></a>Ashford, S.J. & L.L. Cummings. (1983). Feedback as an individual resource: personal strategies of creating information. _Organizational Behaviour and Human Performance_, **32**(3), 370-398.
*   <a id="bar68" name="bar68"></a>Barnard, C.I. (1968). _The functions of the executive._ Cambridge, MA: Harvard University Press.
*   <a id="bre64" name="bre64"></a>Brewer, E. & Tomlinson, J.W.C. (1964). The manager's working day. _Journal of Industrial Economics_, **12**(3), 191-197.
*   <a id="cre94" name="cre94"></a>Creswell, J.W. (1994). _Research design: qualitative and quantitative approaches._ London: Sage Publications.
*   <a id="dav98" name="dav98"></a>Davenport, T.H. & Prusak, L. (1998). _Working knowledge: how organizations manage what they know._ Boston, MA: Harvard Business School Press.
*   <a id="dea00" name="dea00"></a>Deal, T.E. & Kennedy, A.A. (2000). _Corporate cultures, the rites and rituals of corporate life._ Cambridge: Perseus Publishing.
*   <a id="gro91" name="gro91"></a>Grosser, K. (1991). Human networks in organizational information processing. _Annual Review of Information Science and Technology_, **26**, 379-402.
*   <a id="han00" name="han00"></a>Hanneman, R. (2000). [Introductory textbook on social network analysis.](http://faculty.ucr.edu/~hanneman/networks/nettext.pdf) Retrieved 14 August, 2002 from http://faculty.ucr.edu/~hanneman/networks/nettext.pdf.
*   <a id="kat92" name="kat92"></a>Katzer, J. & Fletcher, P.T. (1992). The information environment of managers. _Annual Review of Information Science and Technology_, **27**, 227-263.
*   <a id="kot82" name="kot82"></a>Kotter, J. (1982). What effective general managers really do. _Harvard Business Review_, **60**(2), 157-169.
*   <a id="lin59" name="lin59"></a>Lindblom, C.E. (1959). The science of 'muddling through'. _Public Administration Review_, **19**(2), 79-88.
*   <a id="lut88" name="lut88"></a>Luthans, F. (1988). Successful vs. effective real managers. _Academy of Management Executive_, **2**(2), 127-132.
*   <a id="lut85" name="lut85"></a>Luthans, F., Rosenkrantz, S.A. & Hennessey, H.W. (1985). What do successful managers really do? An observation study of managerial activities. _Journal of Applied Behaviour Science_, **21**(3), 255-270.
*   <a id="mck74" name="mck74"></a>McKenney, J. L. & Keen, P. G. W. (1974). How managers' minds work. _Harvard Business Review_, **52**(3), 79-90.
*   <a id="mac03" name="mac03"></a>Mackenzie, M. L. (2003). An exploratory study investigating the information behaviours of line-managers within a business environment. _New Review of Information Behaviour Research_, **3**, 63-78.
*   <a id="min73" name="min73"></a>Mintzberg, H. (1973). _The nature of managerial work_. New York, NY: Harper & Row.
*   <a id="sim55" name="sim55"></a>Simon, H. A. (1955). A behavioural model of rational choice. _Quarterly Journal of Economics_, **69**(1), 88-118\. [Reprinted in Herbert A. Simon, _Models of man_ (pp. 241-260). New York, NY: John Wiley and Sons, Inc.]
*   <a id="sim57" name="sim57"></a>Simon, H. A. (1957). _Models of man._ New York, NY: John Wiley & Sons, Inc.
*   <a id="sim76" name="sim76"></a>Simon, H. A. (1976). _Administrative behavior_. (3rd ed.) New York, NY: The Free Press.
*   <a id="tic79" name="tic79"></a>Tichy, N. & Fombrun, C. (1979). Network analysis in organizational settings. _Human Relations_, **32**(11), 923-965.
*   <a id="whi00" name="whi00"></a>White, H. (2000). Toward ego-centered citation analysis. In Blaise Cronin and Helen Borsky Atkins (Eds.), _The web of knowledge_, (pp 471-496). Medford, NJ: Information Today, Inc.
*   <a id="wil83" name="wil83"></a>Wilson, P. (1983). Second hand knowledge: an inquiry into cognitive authority. Westport, CT: Greenwood Press.



# Appendices

## <a id="app1" name="app1"></a>Appendix 1 Data summary: selecting an information source

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th rowspan="2">Category of manager</th>

<th colspan="5">Why do managers select a certain individual as an information source?</th>

</tr>

<tr>

<th>Relationship  
35%</th>

<th>Knowledge  
19%</th>

<th>Communication  
behaviour 18%</th>

<th>Communication  
style 15%</th>

<th>Cognitive  
ability 13%</th>

</tr>

<tr>

<td>Department heads (4)</td>

<td align="center">6</td>

<td align="center">4</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">0</td>

</tr>

<tr>

<td>Local senior sales managers (6)</td>

<td align="center">7</td>

<td align="center">2</td>

<td align="center">1</td>

<td align="center">2</td>

<td align="center">4</td>

</tr>

<tr>

<td>Remote senior sales managers (2)</td>

<td align="center">4</td>

<td align="center">1</td>

<td align="center">3</td>

<td align="center">2</td>

<td align="center">1</td>

</tr>

<tr>

<td>Local front-line sales managers (2)</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">2</td>

<td align="center">0</td>

</tr>

<tr>

<td>Remote front-line sales managers (4)</td>

<td align="center">1</td>

<td align="center">3</td>

<td align="center">3</td>

<td align="center">1</td>

<td align="center">3</td>

</tr>

<tr>

<td>Divisional managers (3)</td>

<td align="center">5</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">1</td>

<td align="center">1</td>

</tr>

<tr>

<td>Total for all groups of managers</td>

<td align="center">24</td>

<td align="center">13</td>

<td align="center">12</td>

<td align="center">10</td>

<td align="center">9</td>

</tr>

<tr>

<td colspan="6">Note: The percentage represents the strength of the reason selected as compared to the other reasons selected.</td>

</tr>

<tr>

<td colspan="6">Note: The number under each reason represents how many times the reason (e.g, relationship) was cited to describe why an individual manager was selected as an information source.</td>

</tr>

<tr>

<td colspan="6">Note: The number in parentheses next to the category of manager represents the number of managers in the group.</td>

</tr>

</tbody>

</table>



## Appendix 2 Data summary: being selected as an information source, by manager type

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th rowspan="2">Category of manager</th>

<th colspan="5">Why are certain groups of managers selected as information sources?</th>

</tr>

<tr>

<th>Relationship  
40%</th>

<th>Knowledge  
18%</th>

<th>Communication  
behaviour 18%</th>

<th>Communication  
style 11%</th>

<th>Cognitive  
ability 13%</th>

</tr>

<tr>

<td>Department heads (4)</td>

<td align="center">20</td>

<td align="center">10</td>

<td align="center">6</td>

<td align="center">4</td>

<td align="center">6</td>

</tr>

<tr>

<td>Local senior sales managers (2 selected)</td>

<td align="center">4</td>

<td align="center">1</td>

<td align="center">2</td>

<td align="center">1</td>

<td align="center">1</td>

</tr>

<tr>

<td>Remote senior sales managers (SS1/R)</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">3</td>

</tr>

<tr>

<td>Local front-line sales managers (2)</td>

<td align="center">2</td>

<td align="center">1</td>

<td align="center">1</td>

<td align="center">0</td>

<td align="center">0</td>

</tr>

<tr>

<td>Remote front-line sales managers (4)</td>

<td align="center">6</td>

<td align="center">3</td>

<td align="center">3</td>

<td align="center">2</td>

<td align="center">1</td>

</tr>

<tr>

<td>Divisional managers (3)</td>

<td align="center">2</td>

<td align="center">0</td>

<td align="center">3</td>

<td align="center">1</td>

<td align="center">0</td>

</tr>

<tr>

<td>Total for all groups of managers</td>

<td align="center">35</td>

<td align="center">16</td>

<td align="center">16</td>

<td align="center">9</td>

<td align="center">11</td>

</tr>

<tr>

<td colspan="6">Note: The number under each reason represents how many times the reason (i.e., relationship) was cited to describe the selection of managers within the group (e.g., Divisional).</td>

</tr>

<tr>

<td colspan="6">Note: The percentage represents the strength of the reason selected as compared to the other reasons selected.</td>

</tr>

<tr>

<td colspan="6">Note: The number in parentheses next to the category of manager represents the number of managers in the group from which comments were received.</td>

</tr>

</tbody>

</table>


