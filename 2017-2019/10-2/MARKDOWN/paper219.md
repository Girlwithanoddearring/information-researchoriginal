#### Vol. 10 No. 2, January 2005



# Meaning and authority: the social construction of an 'author' among information behaviour researchers

#### [Michael Olsson](mailto:michael.olsson@uts.edu.au)  
Department of Media Arts, Communication and Information  
University of Technology, Sydney, Australia



#### Abstract

> **Introduction.** The study explores the social processes that influence the construction by academic (information behaviour) researchers of the meaning(s) and significance of an author and her work prominent in the literature of their field (Brenda Dervin).  
> **Methods.** Semi-structured qualitative interviews, based in part on the 'Life-Line' and 'Time-line' techniques developed by Dervin and her collaborators. Participants were purposefully sampled to reflect a range of experience levels and conceptual approaches.  
> **Analysis.** The study adopted an inductive approach to data analysis, based on the 'constant comparison' approach of Glaser and Strauss. Feedback from participants was sought throughout the analysis process by e-mail.  
> **Results.** 'Interactions and Relationships' describes the social contacts involved in their construction of the author; 'The Role of Existing Constructions' deals with participants' existing knowledge and understandings; and 'Accepted and Contested Constructions' demonstrates how they drew on their existing constructions in order to accept or contest the constructions of the author conveyed to them.  
> **Conclusions.** Participants' constructive processes involved drawing on their previous experience ('existing constructions') in order to accept or contest the constructions of the author conveyed to them in each new encounter. Participants' constructive processes had two interdependent aspects: the construction of meaning and the construction of authority.



## Introduction

This paper reports on a study of the social processes that influence the construction by academic (information behaviour) researchers of the meaning(s) and significance(s) of an author (Brenda Dervin) and her work prominent in the literature of their field.

In shifting theoretical attention from individual cognition to social processes, the study seeks to address criticisms of prevailing approaches to information behaviour research voiced by critics such as Frohmann ([1992](#fro)), Talja ([1997](#tal97)) and Julien ([1999](#jul)). In highlighting the social nature of participants' constructive processes and the inter-relationship of their constructions of meaning and constructions of authority, the paper both builds on and challenges prevailing conceptions of information behaviour.

### Conceptual influences

Although the study's findings and conclusions were developed through inductive analysis, they have been influenced by a range of different social constructivist theories and approaches; the Sense-Making theories of Dervin and discourse analytic writings of Foucault in particular.

The study echoes Patrick Wilson's view that: '...like the clothes one wears. The food one eats, the accent and vocabularies of one's speech, so also the things one is informed about and the questions on which one has views are all influenced by social location' ([Wilson 1983](#wilp): 149).

The analysis of participants' constructive processes was very much in keeping with the Sense-Making meta-theoretical perspective, that 'the human, a body-mind-heart-spirit living in a time-space, moving from a past, in a present, to a future, [is] anchored in material conditions... in time-space' ([Dervin 1999](#der99)).

Another key conceptual influence was Foucault's construction of the inseparable link between knowledge and power:

> We should admit... that power produces knowledge... that power and knowledge directly imply one another; that there is no power relation without the correlative constitution of a field of knowledge, nor any knowledge that does not presuppose and constitute at the same time power relations. ([Foucault 1977](#fou77): 27)

The study would find many parallels between Foucault's construction of knowledge and power and participants' constructive processes.

## Methodology

The researcher conducted semi-structured qualitative interviews, based in part on the 'Life-line' and 'Time-line' techniques developed by Dervin and her collaborators ([Dervin 1999](#der99); [Dervin & Frenette 2001](#der01)), with fifteen information behaviour researchers from eight universities in five countries in Europe and North America. Participants were purposefully sampled based on analysis of their published work to reflect a range of experience levels and conceptual approaches. In addition, three participants were drawn from White & McCain's ([1998](#whi)) list of the most cited authors in library and information science, while five participants were identified by the author as having long-term associations with her. Participants described the events and relationships they regarded as significant in their relationship with the author and her work.

The study adopted an inductive approach to data analysis, based on the _constant comparison_ approach of Glaser and Strauss ([1967](#gla67)). Feedback from participants was sought throughout the analysis process by e-mail.

### An _insider_ study

Framing the study as both _in_ and _about_ information behaviour research had advantages and potential disadvantages. Participants were able to see the researcher as an _insider_ ([Chatman 1999](#cha)), a member of their own community. Participants were able to draw on a shared specialist language, and shared experience of writers and theories, in talking to the researcher. The researcher was able to follow the nuances and fine distinctions they made in a way that would not have been possible working with researchers from a field with which he was less familiar.

However, it is also probable that this shared engagement with the field of information behaviour research had other, potentially negative, effects. Although the study adopted strategies (such as avoiding using certain _loaded_ phrases such as _gap_ and _information need_) to encourage participants to describe their experiences in a naturalistic way, it would seem unlikely that participants' accounts of their constructions and behaviour were not heavily influenced by their own engagement with theories and approaches designed to explain such behaviour! A less knowledgeable group of participants might have brought fewer preconceptions to describing their behaviour.

## Findings

The findings are organised into three sections. _Interactions and relationships_ describes the social contacts involved in their construction of the author; _The role of existing constructions_ deals with participants' existing knowledge and understandings; and _Accepted and contested constructions_ demonstrates how they drew on their existing constructions of the field, their informant and the academy in order to accept or contest the constructions of the author conveyed to them.

### Interactions and relationships

Participants' constructions of the author and her work were based on a wide-ranging engagement with both people and texts. These encounters, however, were far more likely to arise from conversations with their colleagues or academic mentors, their attendance at a conference or workshop, or other social activities associated with their role as an information behaviour researcher, rather than as the result of purposeful searching.

#### Initial interactions

Thirteen participants' initial contact with the author's work involved interaction with another person: in twelve of them, that person was also associated with the same department. Six participants, who were all students at the time, were introduced to the author's work by a lecturer: 'we had a lecture... about information needs and seeking and he used Dervin and Nilan's paper'.

Similarly, seven already established researchers reported that their introduction came through another member of their department, a colleague (five participants) or a research student (two). These participants emphasised the informal and interactive nature of their discussions, talking about how they occurred 'over quite a long time... many months' and contextualising them in terms of their established working relationship with their colleagues: 'And we worked together, she worked with me and that's where we did some stuff together'.

By contrast, only two participants described their initial contact with the author's work as arising from purposeful literature searching.

Participants often explicitly linked their relationships with people and texts to one another. For example, six of the eight participants who read an author text as part of their initial contact, describe their interactions with another person as an important influence on their reading of the text: 'I was pleased to have at the time a colleague say to me, "Look, focus upon pages eleven to sixteen, that's where the nuts and bolts is..."'.

#### Subsequent interactions and relationships

Participants' accounts of their subsequent significant events and relationships also largely focus on non-purposive, social interactions. However, while participants may not have instigated such encounters as part of an information search, equally, they did not regard them as unexpected or surprising. Rather, they saw them as a normal part of the working life of an active researcher in the field: that part of their role was to be involved in such information-sharing events, both formal and informal.

Again, their accounts drew attention to the importance of personal communication. For example, participants' discussion of the value of conferences emphasised their importance as venues for informal discussions with colleagues from other universities, including the author herself: '...she and I met at a... conference... and talked for a while about our work'.

One unexpected finding was that every participant described some form of personal contact with the author, either in the form of informal contact or through attending a conference or workshop given by the author. This may relate to a phenomenon articulated by three participants: that information behaviour research is a field characterised by researchers knowing one another personally:

> ... we're a small field, relatively speaking compared to communication, for example. We all know each other, we all talk to each other, we all go to the same conferences. And perhaps this is why there is not much negative citation; we don't want to give too much criticism to each other...

Eight participants described themselves as having an ongoing relationship with the author. All but one of the eight said this relationship was a significant influence on their interpretation of her work. All eight emphasised that some of their most important interactions with the author took place during informal, social meetings:

> ...we were staying at the same hotel and went out... and we talked about what was going on, but it was quite informal... So we talked a lot about Sense-Making and her work and my work...

The importance of interpersonal communication for participants' constructions of the author is not to imply that formal information sources were unimportant. 'Author texts' featured in fourteen participants' accounts, while non-author texts played a role in seven participants' accounts.

> ...she told me about this new Dervin article and said I should read it.

> See, this was one thing that I've been carrying around. I got that from Brenda and I've used it at various times.

Again, however, participants' interactions with 'author texts' were commonly mediated by their interpersonal communication with their colleagues, collaborators and mentors. For example, seven of the eight participants who had a relationship with the author described it as a significant influence on their interpretation of her written work.

Participants frequently described the significant influences on their constructions in terms of long-term relationships with other people and with the written work of authors. Rather than a series of isolated encounters with information sources, participants spoke of the continuing nature of their relationships. Each individual encounter (whether with a person or a text) built on the participant's previous experience, enriching their constructions of both the author and their informants.

A number of participants emphasised the importance of the level of trust and mutual understanding, developed over a long working relationship:

> Well naturally because I knew her so well, we were colleagues, had worked together for a long time. So not only did I respect her opinion a great deal... there was a kind of shorthand between us. We didn't have to go into every detail... If she said something was important or I should read that, then obviously I would listen.

### The role of existing constructions

Participants' constructions of the author and her work did not occur in isolation. Rather, they were grounded in their existing knowledge, beliefs and understandings; that is, their _existing constructions_. These constructions were the lens through which participants _saw_ the author and her work.

For example, eleven participants described their engagement with a particular conceptual framework or school of thought, such as 'Social Constructivism' (four participants) or 'Cognitivsm' (two), as a significant influence; for example, '...I had discovered social constructivism and discourse analysis... And I was from the beginning finding her to be a social constructivist...'.

Similarly, eleven participants reported ideas, approaches and works by authors outside information studies as important influences on their constructions of both the author and the field; for example, '... You need to understand that my orientation to her was as a linguist... I am first and foremost linguist...'

Five participants from three European countries suggested that their cultural backgrounds led them to a different view of the author's work:

> ...we have very strong tradition of welfare state, social structures, social they are always here. We are respecting them. Maybe we are not so fanatic individualists as Americans are. We are always looking at individuals in social contexts. We are not separating them.

Their different social context led to them bringing different pre-existing constructions to the constructive process.

### Accepted and contested constructions

During these interactions, participants drew on their existing constructions (of their informant, of the field and of the academy) to assess the validity of the constructions of the author conveyed to them.

The analysis revealed three types of outcome: _accepted constructions_ (seventy-three occurrences), where the participant accepted the constructions of the author and her work conveyed to them by their informing source, incorporating them into their own view; _contested constructions_ (twenty-seven occurrences), where they challenged the validity of the constructions conveyed to them; and _common ground constructions_ (fifty-three occurrences) which included elements of both acceptance and contestation.

#### Accepted constructions

Five of the six _student_ participants accepted the constructions of the author conveyed to them in their first encounter. Their accounts emphasise their _lack_ of existing constructions as an important factor: 'when you're starting out everything's new and unfamiliar... you know the lecturer knows more than you'. As students, they were in a position where they routinely had interpretations, not only of the author's work but of the literature of the field in general, conveyed to them by their informant or lecturer. Further, this relationship occurred in an institutional context - one whose established conventions of the lecturer/student relationship would act to reinforce their constructions of themselves as _inexperienced_ and of their informants as more knowledgeable than themselves:

> ...in that situation, you're not very likely to say to the professor “No, you're wrong!” I wasn't anyway... You accept that what they're telling you is right—it's their job!

However, the majority of accepted constructions identified by the analysis demonstrate that it was not only neophyte researchers who accepted the interpretations of the author and her work conveyed to them:

> I would say that probably any thought that I've had about Dervin has passed through Dan to me. The gold is, the discovery of the New World by the Portuguese, the gold travelled straight from Brazil to London via Lisbon. So I think that any gold of Dervin came directly through Dan.

Rather, most accepted constructions were the result of a critical evaluation, which drew on their existing constructions, leading them to see the meanings conveyed to them as _valid_:

> But maybe also one of the things that fascinated me [about the author's work], it was possible to use the ideas from other fields of social science, social psychology, sociology... Possible to expand the horizon, not only the library view, that's very narrow... And actually, I have studied sociology... it's my second discipline... so I could relate it to that.

An existing construction of their informant as knowledgeable and/or authoritative played a role in many more experienced researchers' accepted constructions. Long-term relationships with department colleagues (nine participants) and research collaborators (five participants) were considered important influences and were generally marked by accepted constructions. Some participants constructed their colleague as a more knowledgeable mentor, while others described more _equal_, dialogic relationships as significant:

> I suppose, we talked about that a lot when I was at Seth with Harold... He had that same problem, and I think that was a fairly major question... we both had about her work...

In these cases, both the participant and their colleague actively presented their own views of the meaning and significance of the author's work leading ultimately to a shared understanding.

Many participants saw the author herself as possessing a unique authority. For example, their accounts include nineteen examples of accepted constructions in relation to author texts:

> I discovered Dervin's, the information democracy article, it contradicted everything, all the pieces had been needed in order to develop further information seeking research. It had the ideas that should be taken up and then develop information seeking, develop new research questions, new research programmes.

A number of participants indicated in their accounts that they regarded texts written by the author as the most _authoritative_ source in relation to the author's work, especially Sense-Making: '...it's anything that Dervin herself writes, of course because it's her creation. I couldn't imagine who could speak with more authority'.

Similarly, acceptance was also commonly found (eighteen occurrences) in participants' accounts of their informal contacts with the author:

> That was marvellous because not only did I have this study, but I was able to talk to her and sort out a number of things that I didn't understand in the study or how she went about doing this ...

Of eight participants who described having an ongoing personal relationship with the author, five showed a strong acceptance of the meaning and significance conveyed to them by the author:

> I sent her an e-mail and she sent me a very useful reply... very detailed... and she showed how to apply Sense-making to my situation... Since then, if I am unclear on some point, I know I can ask her for advice... she is very generous... she wants to make sure you use it in the right way...

In a related phenomenon, five participants talked about accepting interpretations conveyed to them by an author associate:

> ...well Allison at that time was in regular contact with Dervin... worked with her... she always seemed to know about new developments... we all saw her as the resident Dervin guru...

These informants' personal association with the author was seen as giving them a stronger insight into the meaning and significance of her work

#### Contested constructions

Participants contested constructions were also based on their existing understandings and beliefs:

> ... at the beginning I was very suspicious of those kinds of views that were represented. I was educated in the traditional library systems-centred way of approaching things and my colleagues' views were challenging my ideas... At the beginning I was fighting against them because I didn't believe that the user-centred way is the real one.

The three participants whose initial contacts involved contested constructions all talk about the crucial role played by their previous educational experience:

> Undoubtedly my own training and philosophical background... So it's my training that brings me to this point.

This experience led them to a construction of the field that they saw as antithetical to that conveyed to them - 'my colleagues' views were challenging my ideas' - leading them to contest and then reject them. The authority of their existing constructions thus outweighed that of their informants and the author.

Participants' subsequent behaviour was marked by more contested constructions than their initial constructions. A number of participants, whose initial constructions had been strongly accepting, explicitly characterised their subsequent constructions in relation to the author as _more critical_.

In some cases, the change can be accounted for by the participant's growing experience; a growing familiarity with the literature of information behaviour and related fields giving the participant a more complex set of existing constructions with which to contrast those conveyed to him/her. In addition, greater experience led participants to construct themselves as _more knowledgeable_ and, in consequence, they were more willing to contest meanings conveyed to them:

> I was more critical... I had discovered Foucault... different theories in the sociology of health... discourse analytic work from the British social psychologists... they influenced my thinking, made me look at Dervin in a more critical way...

Another reason for a greater degree of contestation related to what some participants constructed as changes in either the author's and/or their own research interests:

> ...but anyway in some way I felt that she is, at least from my point of view, that she was going to the wrong direction... she is more moving in a general direction... writing about general philosophical development... and actually that's opposite to my own interest today. I was more generally oriented earlier but now... I began to focus more on studies on information servicing.

This was particularly notable among information retrieval researchers, who constructed the author's later work as 'too philosophical'.

#### Common ground constructions

Participants' subsequent interactions and relationships also featured a greater proportion of encounters and interactions that include elements of both acceptance and contestation of the constructions conveyed to them:

> I think fundamentally we still see the world very similarly it's just that the kind of problems that I'm working on which are really design problems are much more pragmatic and I think that she is interested in paradigm level, conceptual, philosophical issues, which I'm interested in, but that's just not where I am working professionally.

In these interactions, the participant identified certain aspects of the constructions conveyed to them as _valid_; they accepted them, largely as a result of constructing them as compatible with their existing constructions, while contesting other aspects as "not interesting", "silly" or simply "not relevant to my research interests today". In doing so, the participant found _common ground_ between their own constructions of the field and those conveyed by the source, without either fully accepting or rejecting its knowledge claims.

In this we can see evidence that the _battle for truth_, the acceptance and contesting of constructions conveyed to participants, occurred not only at the level of individual texts, lectures and conversations, but also _within_ participants' interpretation of these sources.

## Conclusions

These findings have a range of implications for our understanding of information behaviour and for future research in the field. They suggest that the social nature of meaning(sense)-making and the relationship between meaning and authority (knowledge and power) are concepts that need to be integrated into our understanding of information behaviour.

### The social nature of participants' constructive processes

The findings demonstrated that participants' constructions of the meaning(s) and significance(s) of the author and her work were highly contextualised. In examining the constructions conveyed to them, participants did not simply ask 'What does this mean?' or even 'What does this mean for me?' Rather, they asked 'What does this mean for me in terms of my understanding of and engagement with my field? My specialisation(s) and particular research interests? My philosophical and conceptual frameworks? My current projects, current teaching?'

This contrasts with the approach taken by a number of cognitivist researchers, such as Belkin ( [1990](#bel)), who, whilst acknowledging the importance of social factors, conceptualised them as simply one of a range of factors impacting on the individual information user's cognitive processes. Such approaches, as pointed out by Frohmann ([1992](#fro)), imply a Cartesian conception of human consciousness as an entity separate from the physical and social worlds. The study's findings, by contrast, suggest that participants' cognitive processes are inextricably linked to their engagement with their social context. In doing so, they support the social constructivist conceptions of information behaviour of researchers such as Chatman ( [1999](#cha)), Talja ([2001](#tal01)) and Dervin, who argues, "Sense-Making mandates simultaneous attention to both the inner and outer worlds of human beings and the ultimate impossibility of separating them" ([Dervin 1999](#der99): 730).

### Meaning and authority—knowledge and power

Participants' analysis of the meanings conveyed to them involved more than determining their aboutness; an integral part of their constructive processes was assessing the credibility of the informants' messages. This determination of the message's _authority_ formed the basis of participants' decision to either accept or contest the meanings they conveyed. In other words, participants' constructive processes had two interdependent aspects, two sides of the same coin: the construction of meaning and the construction of authority.

Participants' accounts showed that they were very adept at making such meaning and/or authority judgments, to give detailed explanations of both their assessments of the knowledge claims of their informants and of the meaning and significance of the author and her work. Their abilities were very much consistent with Talja's conceptualisation of 'users as knowing subjects, as cultural experts' ([Talja 1997](#tal97) : 77).

An important aspect of participants' constructions of authority related to their construction of the authority of the _informant_, as opposed to the individual _message_ or _text_. This related, in particular, to the importance of long-term relationships for participants' constructions of the author and her work. In dealing with a familiar source, a participant's existing constructions of that informant played a key role in whether they accepted or rejected it. If an informant was already viewed as authoritative in a particular context, they were pre-disposed to accept their message, almost before hearing its content.

The study showed that participants' constructions of authority were also essentially 'transportable' between the written and verbal forms. That is, if a participant regarded a researcher's published work as authoritative; they would also regard their informal communications as authoritative.

This was particularly notable in relation to participants' relationship with the author herself. Seven participants suggested that they regarded the author as having a unique authority when it came to constructing her own work, while three participants viewed the author as the 'embodiment of Sense-Making': that her authority to interpret her own work, because of her status as its originator, was stronger than anyone else's could be.

This would seem to be somewhat at odds with the post-modern concept of _Death of the Author_, as articulated by Barthes ([1988](#bar88)) and Foucault ([1980](#fou80)), which emphasises the distinction between author-constructs (the disembodied authors of texts) and the author-as-person. While the findings are strongly supportive of the central precept of Barthes' and Foucault's theory: that meaning or significance is not determined by authors but constructed by readers. One product of these constructive processes, at least for some participants, was a construction of the author as the most authoritative interpreter of her own work.

That certain members of a community are acknowledged as more knowledgeable, and their opinions particularly influential among other members of the community, has long been established. In an academic context, Price ([1963](#pri)) developed the notion of the _invisible college_, while Patrick Wilson ([1983](#wilp)) defined such power in terms of _cognitive authority_. Chatman's theory of _life in the round_ talked about "'insiders'...people who use their greater understanding of the social norms to enhance their own social roles. By doing so, they establish standards for everyone else" ([Chatman 1999](#cha): 212). The present study provides further evidence for the on-going importance of this phenomenon, arguing that it is central to the construction of shared 'archives' of meaning and authority constructs.

The study found that participants' constructions of the author and her work drew on a complex array of existing _knowledge and power_ structures, derived not only from information science, but a range of other disciplines. Whether accepting or rejecting an interpretation conveyed to them, it was important for participants to relate their constructions to the views of established authorities. This allowed them to _justify_ their own constructions, both to themselves and other members of the academic community.

This benefit which a shared set of _knowledge and power_ constructions offers to the members of a community is central to understanding participants' willing engagement with the established structures of meaning and authority operating within information behaviour, information science, the social sciences and the academy in general. The study suggests that these _knowledge and power_ structures are not primarily _imposed_: rather, they are _accepted_. The inductive processes in which existing power structures, and the established social practices that create them, may impose limitations on the individual, but also bring tangible benefits. While established social practices might incline a participant towards a particular decision, ultimately it was up to their own judgment (based on their previous knowledge and experience) to determine whether an informant was an 'authoritative speaker' ([Rabinow 1984](#rab) ) in that particular context.

This is consistent with Foucault's view of the inductive nature of the relationship between knowledge and power:

> Power is everywhere; not because it embraces everything but because it comes from everywhere ... Power comes from below; that is there is no binary and all-encompassing opposition between ruler and ruled at the root of power relations... ([Foucault 1979](#fou79): 93-94)

Such a conception of participants' power relations helps us to understand why the behaviour of such a relatively socially-advantaged group (cosmopolitan, intelligent, well-educated, and financially comfortable) should nonetheless be so tied to established _knowledge and power_ structures.

It was clear, however, that neither participants' positions in relation to existing _knowledge and power_ structures, nor the structures themselves, were fixed. Participants, for example, described how some of their _contested_ interactions led them to revise their existing view of the author and, in some cases, of the nature and aboutness of research in the field. Similarly, participants were able to talk knowledgeably about how opinions of the author and her work, and consequently her authority, among information behaviour researchers had changed and developed over time.

This illustrates the dynamic nature of _knowledge and power_ constructions among participants and the information behaviour community as a whole. Authority and meaning were not fixed but negotiated. The validity of a given knowledge claim was not assessed by reference to a Popper-esque objective _reality_, but rather was determined by participants, individually and collectively, using established social practices; methods of critical analysis and comparison acquired through their previous experience, both as information behaviour researchers and in other academic disciplines.

The findings showed that participants did, on occasion, _contest_ an informant's authority to interpret the meaning and significance of the author and her work. In this, they parallel Foucault's perspective:

> There is a battle 'for truth' or at least 'around truth' - it being understood once again that by truth I do not mean 'the ensemble of truths which are to be discovered and accepted' but rather 'the ensemble of rules according to which the true and the false are separated and specific effects of power attached to the true. (Foucault, in [Rabinow 1984](#rab): 418)

Participants' ability to engage in and resolve such situations suggests that among the knowledge and skills their experience equipped them with were methods for managing disagreement: accepted social practices by which members of a community could articulate divergent subject positions and negotiate a new shared understanding.

### Implications for future research

The study's findings and conclusions have both built on existing understandings within information behaviour research and challenged aspects of existing conceptual frameworks. In consequence, it has a range of implications for future research.

The findings in relation to the inter-relationship of the construction of meaning and the construction of authority are closely related to Foucault's discursive construction of _knowledge and power_. Foucault's notion that power in this context is essentially inductive, in particular, offers a close parallel with the study's analysis of participants' experiences. Dervin ([1989](#der89)) has suggested that information behaviour researchers have tended to ignore the question of the role of power relations on information behaviour. The present study would suggest that Foucault's theories offer some useful insights in exploring this issue.

The study found that only a relatively small number of the events that participants described as significant were the result of purposive information seeking. Yet as Ross ([1999](#ros99)) and Wilson ([2000](#wilt)) have pointed out, the majority of information behaviour research continues to focus on this type of behaviour. The findings support the view that non-purposeful information behaviour is an area that warrants significant future attention from information behaviour researchers.

Talja ([1997](#tal97)) and Julien ([1999](#jul)) have argued for a new way of looking at users; one focused on their expertise rather than the gaps in their knowledge. The present study suggests that such an approach can lead to new insights into the nature of information behaviour.

The findings also suggest that the distinction drawn between formal and informal information behaviour in many existing information behaviour studies needs to be re-evaluated. The degree to which the two are inter-related in the study's findings suggest that this is a somewhat arbitrary, systems-oriented distinction: an example of the kind of reification of research categories critiqued by Dervin ([1989](#der89)).

One of the study's key findings was the importance of ongoing relationships, with informants, with the author and her work, with conceptual frameworks, for participants' constructive processes. That participants' constructive processes were so intimately connected to their previous knowledge and experiences (their existing constructions) suggests that Dervin & Nilan's ([1986](#der86)) call for a less atomistic approach to studying information behaviour needs to be taken further: rather than conceiving of information behaviour as being driven by the desire to satisfy discrete information needs, any information interaction or encounter should be seen as one chapter in an individual's ongoing engagement with, and construction of, their life-world.

## Acknowledgements

The author wishes to acknowledge the contribution of his doctoral supervisor, Ms Susan Edwards, without whose suggestions and constructive critique this paper would not have been possible; the research participants for their willingness to share their experiences and insights; and the suggestions of the anonymous referees.

## References

*   <a name="bar88" id="bar88"></a>Barthes, R. (1988). _A Barthes reader._ New York, NY: Noonday Press.
*   <a name="bel" id="bel"></a>Belkin, N. (1990). The cognitive viewpoint in information science. _Journal of Information Science_, **16**(1), 11-15\.
*   <a name="cha" id="cha"></a>Chatman, E. (1999). A theory of life in the round. _Journal of the American Society for Information Science_, **50**(3), 207-217\.
*   <a name="der89" id="der89"></a>Dervin, B. (1989). Users as research inventions: how research categories perpetuate inequities. _Journal of Communication_ **39**(3), 216-232\.
*   <a name="der99" id="der99"></a>Dervin, B. (1999). On studying information seeking and use methodologically: the implications of connecting metatheory to method. _Information Processing & Management_, **35**(6), 727-750\.
*   <a name="der01" id="der01"></a>Dervin, B., & Frenette, M. (2001). Sense-making methodology: communicating communicatively with campaign audiences. In R.E. Rice and C.K. Atkin (Eds.), _Public communication campaigns._ (pp. 69-87). Thousand Oaks, CA: Sage.
*   <a name="der86" id="der86"></a>Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33
*   <a name="fou77" id="fou77"></a>Foucault, M. (1977). _Discipline and punish: the birth of the prison._ London: Allen Lane.
*   <a name="fou79" id="fou79"></a>Foucault, M. (1979). _The history of sexuality. Volume one: an introduction._ London: Allen Lane.
*   <a name="fou80" id="fou80"></a>Foucault, M. (1980). _Power/knowledge: selected interviews and other writings 1972-1977._ London: Harvester Press.
*   <a name="fro" id="fro"></a>Frohmann, B. (1992). The power of images: a discourse analysis of the cognitive viewpoint. _Journal of Documentation,_ **48**(4), 365-386\.
*   <a name="gla67" id="gla67"></a>Glaser, B.G. & Strauss, A.L. (1967). _The discovery of grounded theory: strategies for qualitative research._ New York, NY: Aldine Publishing Corporation.
*   <a name="jul" id="jul"></a>Julien, H. (1999). Where to from here? Results of an empirical study and user-centred implications for information design. In T.D. Wilson and D.K. Allen (Eds.), _Exploring the contexts of information behaviour._ (pp. 586-596). London: Taylor Graham.
*   <a name="mil" id="mil"></a>Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis: an expanded sourcebook._ (2nd ed.) Thousand Oaks, CA: Sage.
*   <a name="pri" id="pri"></a>Price, D.J. de S. (1963). _Little science, big science._ New York, NY: Columbia University Press.
*   <a name="rab" id="rab"></a>Rabinow, P. (1984). _The Foucault reader._ Harmondsworth, Middlesex: Peregrine Books.
*   <a name="ros99" id="ros99"></a>Ross, C.S. (1999). Finding without seeking: what readers say about the role of pleasure-reading as a source of information. In T.D. Wilson and D.K. Allen (Eds.). _Exploring the contexts of information behaviour_, (pp. 343-355). London, Taylor Graham: 343-355\.
*   <a name="tal97" id="tal97"></a>Talja, S. (1997). Constituting "information" and "user" as research objects: a theory of knowledge formations as an alternative to the information-man theory. In P. Vakkari, R. Savolainen and B. Dervin. _Information seeking in context: proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts, 14-16 August, 1996, Tampere, Finland._ (pp. 67-80) London: Los Angeles, CA: Taylor Graham.
*   <a name="tal01" id="tal01"></a>Talja, S. (2001). _Music, culture and the library: an analysis of discourses._ Lanham, MD: Scarecrow Press.
*   <a name="whi" id="whi"></a>White, H.D. & McCain, K.W. (1998). Visualizing a discipline: an author co-citation analysis of information science. _Journal of the American Society for Information Science._ **49**(4), 327-355\.
*   <a name="wilp" id="wilp"></a>Wilson, P. (1983). _Second-hand knowledge: an inquiry into cognitive authority._ Westport, CT: Greenwood Press.
*   <a name="wilt" id="wilt"></a>Wilson, T.D. (2000). Human information behaviour. _Informing Science ._ **3**(1), 49-55.

