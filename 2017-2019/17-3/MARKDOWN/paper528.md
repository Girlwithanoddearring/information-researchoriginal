#### vol. 17 no. 3, September 2012

# Access to printed and electronic information sources as means of continuing professional development of Iranian agricultural staff

#### [Esmail Karamidehkordi](#author)  
University of Zanjan,
 Zanjan, 
 Iran, 45371-38111

#### Abstract

> **Introduction.** The professional staff of agricultural organizations need to obtain information from multiple sources for their continuing professional development to communicate with their stakeholders effectively and facilitate agricultural development.  
> **Aim.** This study examines the accessibility of professional printed sources and information and communication technologies for the development of Iranian agricultural professionals.  
> **Method.** The research utilises a focus group and a self-completion questionnaire using a random sample of the agricultural professionals working in the public sector of one of the south-west provinces of Iran.  
> **Results.** Overall, the professionals, particularly the practitioners, had limited access to electronic and printed sources as self-directed learning methods for their continuing professional development. Access among higher educated professionals, such as researchers and subject specialists, mostly located at provincial level, was greater than their other colleagues, particularly those at district and county levels. International sources were poorly accessible to most staff.  
> **Conclusions.** Management should be aware of the unequal access to these sources among different staff because of factors such as geography, language, education and type of profession.

## Introduction

### Importance of information access means for professional development

It is crucial to strengthen the human resource capacity of farmers and the staff of agricultural organizations, especially those within agricultural extension and advisory systems. This capacity building can be institutionalised through continuing professional development programmes using different means to facilitate information access to them. Continuing professional development can be delivered through both conventional and alternative channels. For example, according to Swanson and Rajalahti ([2010](#swa10)) training, as one of the interpersonal channels, can help the staff to use more participatory methods, and to enhance their technical, management and marketing skills. The alternative sources or channels are mostly based on information and communication technology, including access to the Internet, mobile phones, short message services, databases and electronic media. In the context of Iran, some studies have stressed the low accessibility of conventional methods, such as training courses, workshops and meetings for continuig professional development. However, there is little evidence on the availability of alternative information means, such as printed and information and communication technology based tools and sources.

This study examines the extent that professional staff of agricultural and natural resources organizations have access to alternative information sources for their continuig professional development. It aims also to understand who has access to which sources and what factors influence the level of access. In other words, it is necessary to know the inequality or differences of accessibility to information channels in terms of varied geographical, organizational and personal characteristics of agricultural professionals.

### Continuing professional development concept

The concept of continuing professional development was originally derived from the principles of adult education ([Gibbs _et al_ 2005](#gib05)). According to Knowles ([1984](#kno84)), adults learn based on their own special learning styles and as they mature they get more self-directed. Their previous experiences and their profession's problems act as important stimuli for their learning. Continuig professional development involves developing both scientific and complementary skills and knowledge (including attitudes and behaviours), which are required to participate and progress in today's dynamic world of work ([Faerber and Johnson 2006](#fae06)). Professional practice is more than just holding a job with defined skills and knowledge; it also involves evolving expertise through ongoing development. Many employees may not feel the need to develop their professional skills ([Westcott 2005](#wes05)). Continuig professional development opportunities can help staff obtain efficiently the skills and knowledge required for enhancing their job.

The continuig professional development concept has been utilised in many fields and bodies, for example sport, education, healthcare and medical professions ([Cushion _et al_ 2003](#cus03); [Day and Sachs 2005](#day05); [Evetts 1998](#eve98); [Galloway 1998](#gal98); [Gorman 2003](#gor03); [Grant 2004](#gra04); [Kirkwood and Christie 2006](#kir06); [Knight 1998](#kni98); [Lewis and Day 2004](#lew04); [Littlejohn 2001](#lit01); [Wermke 2010](#wer10); [Wynne-Jones 1999](#wyn99)). Some organizations, particularly the medical profession, even provide a professional portfolio to be completed using these techniques ([Capio 2006](#cap06); [Dornan, _et al_. 2002](#dor02)). They are also effective for experiential learning and critical incidents, which are not pre-planned. Kolb's ([1984](#kol84)) theory of experiential learning fits the way most adults learn and it fits continuous professional development as well, in which individuals in their work experiences (trying things out-planned or accidental), evaluate (reflection on that experience), conceptualise (constructive thinking from observation and reflection which tries to make sense of what is happening and searching for ways to make it happen more successfully) and experience (trying the revised behaviour in practice).

### Continuing professional development in agriculture

Continuing personal and professional development is needed not only for farmers or farm managers, but also agricultural professionals (researchers, extensionists and practitioners). Professional development or improving competences of agricultural professionals has been urged as a key element of agricultural and rural development ([Lee 2006](#lee06); [Lindner _et al._ 2003](#lin03); [Ramlall 2006](#ram06); [Zorzi _et al_. 2002](#zor02)). Continuing professional development helps the professionals to undertake some form of continual evaluation and development of their knowledge, skills and competence. Competence is defined as the ability to implement a task using knowledge, education, skills, and experience ([Moore and Rudd 2004](#moo04)). According to Grant ([2004](#gra04)), no best approach to learning for continuing professional development exists. The effectiveness however requires the process of continuing professional development to be managed effectively. Therefore, agricultural and natural resources professionals including practitioners, specialists and scientists, may use different learning methods to maintain and develop their competence and knowledge. This can help the governance and quality of their activities and services in agricultural development and natural resource management. In addition to traditional educational methods such as training courses, workshops and seminars, self-directed learning methods using printed, audio-visual and electronic materials and facilities are efficient techniques for facilitating professional development.

The application of information and communication technology in agricultural extension and rural development has significantly increased in several countries where it has provided access to agricultural information ([Richardson 2003](#ric03)). information and communication technology has been utilised as an extension tool, which has enhanced the information flow between agricultural extension services and their clients ([Meera _et al_. 2004](#mee04)).

Recent studies show although most farmers and agricultural professionals in developed countries take advantage of new informal educational opportunities available through electronic facilities such as the Internet and email, those in developing countries have little access to these opportunities ([Lesaona-Tshabalala 2001](#les01); [Raab and Abdon 2005](#raa05)). The e-learning related initiatives have been utilised through online information as resources for informal learning. Some suggest that agricultural professionals and knowledge intermediaries are the most relevant targets of e-learning for supporting agriculture in developing countries. In some areas, learning networks have been initiated. For example the Sustainable Development Learning Network (SDLearn) in the Asia and Pacific region was established as a continuing education, e-learning programme for agricultural educators and other professionals in the public, private, educational and non-governmental sectors relevant to developing farming communities and promoting sustainable agriculture ([Singh 2006](#sin06)). The Asia-Pacific Regional Technology Centre provided continuing educational opportunities for agricultural professionals mostly through e-learning and multi-sectoral partnerships ([Raab _et al_. 2002](#raa02)). The initial experience revealed that the approach was workable and cost effective. This shows that all partners and the clients could benefit from the collaboration.

The emergence of new learning opportunities such as printed facilities and e-learning methods for continuing development of agricultural professionals may enhance their information access, but mayreduce the dominance of traditional approaches and providers such as higher and continuing education by non-profit colleges and universities ([Raab _et al_. 2002](#raa02)). However, before any discussion regarding competition between traditional and new learning opportunities, the question of access to these new opportunities must be addressed.

### Continuing professional development for Iranian agricultural professionals

Government institutions of the Ministry of Jahad Agriculture (MOJA) in Iran have played a crucial role in policy making in agriculture and natural resource management at national and provincial levels, as well as implementing projects at district and county levels. Many studies and scholars have noted the necessity of improving the competences of these professionals in Iran ([Alibeigi and Zarafshani 2006](#ali06); [Karbasioun 2006](#kar06); [Karbasioun and Mulder 2004](#kar04); [Malak-Mohammady 1998](#mal98); [Pezeshki Raad et al. 1994](#pez94)). Thus an emphasis has been on the analysis of their development needs, their required competences and how these competences can be developed.

Some studies have also shown that traditional methods for continuing professional development such as training courses, workshops and meetings have not been effective. For example, Pishbin ([1996](#pis96)) identified that 24% of the agricultural staff of the Fars Province had not attended any pre-service or in-service training courses and others only a few courses. Shahani's ([1999](#sha99)) study showed 67% of the agricultural extension workers in the Mazndaran Province had received no training courses and others had participated in only one to four courses. These results were also confirmed by the study of Mohammadzadeh ([2003](#moh03)) and a national survey conducted by the Human Resource Development Administration of the Extension and Farming Systems Deputy of the Ministry of Jahad-e-Agriculture ([2003](#hum03)). This low attendance of staff in professional training activities may be related to limited opportunities or motivation provided by their institutions to these staff.

However, it is not clear to what extent alternative facilities such as printed or electronic tools and sources are accessible. A few studies are indirectly relevant to this issue, but they have not emphasised the accessibility of the printed and electronic facilities and media to the agricultural professionals. For example, the study of Haji Hashemi ([2008](#haj08)) showed that the agricultural extension workers in the Isfahan Province had a positive attitude toward using information and communication technology. The information and communication technology application and attitude were related to factors such as the applicants' major area of study, knowledge of extension, educational degree, and agricultural extension work experience. Movahed and Iravani ([2002](#mov02)) and Yaghoubi and Shamsayi's ([2004](#yag04)) studies revealed that the agricultural academic members had positive attitudes toward Internet use. Their Internet use was correlated to the characteristics of age, English language skills, computer skills, research activities, number of scientific publications, job status, field of study, access to the Internet, and their perceptions of Internet. Moreover, Pouratashi and Rezvanfar ([2009](#pou09)) identified that skills, support, and facilities influenced the application of information and communication technology by agricultural students.

Therefore, not much information is available to show if continuing professional development has been emphasised by the authorities to facilitate a self-directed learning environment in addition to providing the conventional methods for the professionals and practitioners. This study will examine to what extent this environment has been provided for different staff, i.e. to what extent they have access to self-directed learning facilities for their professional development.

## Material and methods

### Procedures

The study utilised a focus group and a survey through a self-completion questionnaire by the relevant professional staff and managers of the governmental agriculture and natural resource management institutions at provincial, district (or township) and county levels in the Chaharmahal and Bakhtiari (Ch&B) Province, which are part of the provincial body of the Ministry of Jahad-e-Agriculture in Iran.

### Focus groups

Three focus groups were established through the participation of the managers and staff of the agricultural extension, production oriented directorates and the natural resources administrations. The purpose was to understand the local context of expertise and activities of different agricultural and natural resources professionals. Each focus group comprised six to eight staff and managers from both provincial and township levels. The results of these focus groups also helped the researcher to design the survey questionnaire with a higher content validity.

### Survey

A questionnaire was designed by the researcher based on the literature and an initial focus group with five staff of the Extension and Farming System Directorate (EFSD) and Natural Resource Administration (NRA) and Watershed Management Directorate (WMD). The literature gave the researcher insight to identify the most important information means and how to assess them. The focus groups also helped the author to understand which information sources or channels are more relevant to the context of agricultural professionals at different geographical levels. The questionnaire contained a cover letter, close-ended questions consisting of a dichotomous scale (yes/no), five-point Likert scale (very low, low, moderate, high, and very high) with an extra point of 'no' and some open-ended questions (comments, categories or number). The content of the questionnaire was validated by the researcher's university colleagues and the staff of the Extension and Farming System Deputy at national level in 2007\. The questionnaire was also pilot tested by thirty staff of the relevant institutions in other districts of the province. The Coronbach's Coefficient Alfa employed for testing the reliability of the questions regarding the access to educational printed and electronic media/ facilities and information sources scored 0.90\. This coefficient for the main research data was scored 0.94 out of 1\. The face validity of the questionnaire was assessed by cursory review of the items (questions) by both the university colleagues and the participants in the pilot test, based on their judgments on whether the items were relevant or clearly understandable.

### Population and sampling

Based on the Iranian political-geography, each province includes several districts or townships (Shahrestan in Persian) and each district includes one or more than one county (Bakhsh in Persian), which in turn contains one or more than one rural unit called Dehestan comprising a different number of villages. As will be discussed later, agricultural professional practitioners and researchers belonged to different geographical and expertise groups. It was not possible to investigate the entire province; therefore, in addition to randomly selecting the provincial staff, a multistage sampling method was applied to select randomly two townships out of five. Then, three out of four counties were randomly selected in these two districts, followed by randomly selecting their staff. A 115 sample out of 150 staff of the province and the two townships were selected at different geographical levels as follows (see Figure 1).

*   The Extension and Farming System Directorate (EFSD), Natural Resource Administration (NRA), Watershed Management Directorate (WMD), Nomadic Affairs Administration (NAA), Horticulture Directorate (HD), Agronomy Directorate (AD) and Agriculture and Natural Resource Research Centre (ANRRC) at the provincial level;
*   Extension and Participation Offices (EPOs), Horticulture Offices (HOs), Watershed Management Offices (WMOs), District Jahad-e Agriculture Directorate (DJAD) managers, Nomadic Affair Offices (NAOs) and Natural Resource Offices (NROs) in Kohrang and Farsan Districts;
*   Agricultural Service Centres (ASCs) and Forestry Offices (FOs) in three counties/ Dehestan.

The above abbreviations are used in Figure 1 below. In addition, the following abbreviations are used:  
LD - Livestock Directorate; WSDs - Water and Soil Directorates; AOs - Agronomy Offices; LOs - Livestock Offices; and WSOs - Water and Soil Offices

<figure>![Figure 1: Organizational chart of the respondents](p528fig1a.jpg)

<figcaption>**Figure 1: Organizational chart of the respondents**</figcaption>

</figure>

The self-completion questionnaires were personally handed out or posted to the respondents. In addition to telephone contacts, two follow-up questionnaires were sent to non-respondents after two and four weeks. Finally, 88 out of 96 questionnaires (most parts of the 8 eliminated questionnaires had not been completed and the respondents were not accessible afterward) received were used for data analysis through descriptive and inferential statistics using the SPSS and Excel software.

Moreover, an index of access to the printed and electronic media was created by the relevant variables. This helped the researcher summarise the variables and interpret them more appropriately. Because these variables were categorically ordinal, the initial analysis of applied categorical principle component analysis was used to identify underlying, common dimensions among the variables which measured access to the printed and electronic facilities. This technique is similar to standard principal component analysis, but it is applied to not only numerical data, but nominal or ordinal data ([Meulman and Heiser 2005](#meu05); [Sridharan _et al_. 2005](#sri05)). Without making assumptions about the distributions of variables, categorical principle component analysis permits the use of skewed variables ([Bauder 2006](#bau06)). In the analysis, the mean of each index or component across the multiple variables is 0 and standard deviation is one ([Sridharan _et al_. 2005](#sri05)). Each group of interrelated variables, making an index, captures one concept.

## Results

### Local context of institutions and the competences required for their staff

The Ministry of Jahad Agriculture supported its provincial units administratively and in terms of its policies. The most relevant provincial institutions of the ministry were the Provincial Jihad-e-Agriculture Organization (PJAO) and the administrations of Natural Resources, Fisheries, Nomadic Affairs and Veterinary. The institutions in districts with direct or supportive interventions relevant to natural resources and agricultural activities were the District Jahad-e Agriculture Directorates, Natural Resource Offices and Nomadic Affairs Offices. The main concern of 'the nomadic specialists' at both national and local levels was arranging support for nomads.

The Provincial Jihad-e-Agriculture Organization consisted of several directorates, which had offices at district/township level. The Extension and Farming Systems Directorates (or Extension Management), its district level offices (in Extension and Participation Offices) and its county level centres (agricultural service centres) aimed to help farmers increase their general and technical knowledge and encourage participation of rural and nomadic people. They needed continuing professional development to improve their competences to define extension and participation projects in this regard. The Agricultural Service Centres' members usually acted as local extension workers who were also in charge of agricultural input delivery, such as walnut seedlings, new wheat varieties, livestock nutrition, pesticide and herbicide. Moreover, they also introduced farmers to the district offices to get subsidised inputs. Extension workers' activities and competences were related to face-to-face communication, establishing fruit tree nurseries, facilitating extension training courses, establishing model farms, distributing extension publications among communities, recording production and cultivation data, and reporting the damage of annual floods to farms and villages. The institutions related to the Horticulture, Agronomy, Livestock and Soil and Water Directorates and their district level offices were mostly production-oriented, focusing on irrigation systems projects; financial and technical support for current farmers or potential producers to establish or develop irrigated fruit tree orchards, greenhouses, outdoor flower and vegetable farms and mushroom farms; and crop animal production growth. The ASCs coordinated their activities at local level, though there was only one in each Service Centre.

The main interests of the Natural Resource Administration and its district (Natural Resource Offices) and county level (Forestry Offices) offices were the conservation, rehabilitation, development and appropriate use of renewable natural resources (rangelands, forests, woodlands and soil). Conservationists at local level were responsible for visiting nationalised rangelands forest areas regularly to prevent forest cutting and crop cultivation in the forest and rangelands. They monitored communities' activities, guided them and reported any probable offence regarding natural resources to district and provincial managers for further prosecution. They also facilitated provincial and district managed projects and training courses. Moreover, they had to control the nomads' seasonal migration time (or _koch_) to prevent early coming of nomads to their summer lands. Moreover, the staff of the Watershed Management Directorate and its district offices mostly focused on the soil and water conservation. Therefore, these natural resources conservationists needed professional development to improve the competences relating to these activities.

The Agricultural Research and Education Organization at the national level is responsible for formulating agricultural research policies and projects as well as formal agricultural training courses. The provincial research centre covered a wide range of subjects related to agriculture and natural resource management. Continuing professional development for its staff was important for improving their research competences based on the farming problems of the province. Agricultural training centres also provided short and long-term pre-service and in-service training courses for the ministry's staff, vocational agricultural training courses for farmers, agricultural students at secondary education and the two-year colleges.

### Accessibility and relations with personal and organizational characteristics

As illustrated in Tables 1, 2 and 3, 73% of respondents were from provincial institutions 20.2% from districts and 6.8% from counties. Responses based on institutional interests were 14.6% agricultural and natural resources researchers 18% agricultural extensionists 15.7% watershed conservationists, 36% natural resources conservationists, and 10.1% production oriented specialists. 69.5% of respondents had a permanent full-time job, while the others worked through contracts. Their average age was 37.6 years with an average of 14 years work experience. The respondents' education median was at a BSc level with a frequency distribution of 29.2% under BSc, 51.7% at BSc 18% at Master of Science and 1.1% at PhD level. 95.5% of respondents were male.

The majority of respondents had no managerial responsibility (61.8%), but others worked as office manager, administration managers/ general directors or a deputy of them. 63.5% had no membership in scientific associations/ societies. They also reported their scientific or professional positions as follows: 12.5% as assistant or technicians, 76.1% as specialists 10.2% as MSc research fellows and 1.1% as an assistant professor (PhD).

The staff of the relevant institutions were asked to identify to what extent they had access to electronic and printed materials. According to tables 1 and 4, there is a significant positive relation between the geographical level of work and the access to the printed and electronic facilities and materials. In other words, the Kruskal Wallis Test showed very significant (at the 0.05, 0.01 and 0.001 significant levels) differences among these three levels, in which these materials and facilities were more available and accessible in the provincial institutions than in district and county level institutions.

<table class="center" style="width:90%;"><caption>  
Table 1: Accessibility of printed and electronic media in terms of geographical levels</caption>

<tbody>

<tr>

<th rowspan="3">Medium or facilities</th>

<th colspan="12">Geographical levels</th>

</tr>

<tr>

<th colspan="3">County (n=6)</th>

<th colspan="3">District (n=18)</th>

<th colspan="3">Provincial (n=63)</th>

<th colspan="3">Kruskal Wallis Test</th>

</tr>

<tr>

<th>mean*</th>

<th>SD</th>

<th>Median</th>

<th>mean*</th>

<th>SD</th>

<th>Median</th>

<th>mean*</th>

<th>SD</th>

<th>Median</th>

<th>Chi-Squared</th>

<th>Sig</th>

</tr>

<tr>

<td>Computer Hardware</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3.4</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">4</td>

<td style="text-align:center">33.31</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>computer Software</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2.8</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">34.20</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Databases</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0.4</td>

<td style="text-align:center">0.6</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">2</td>

<td style="text-align:center">26.80</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Internet</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0.1</td>

<td style="text-align:center">0.2</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1.9</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">2</td>

<td style="text-align:center">25.05</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Persian Journals and Magazines</td>

<td style="text-align:center">2.0</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">2</td>

<td style="text-align:center">0.9</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2.2</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">14.48</td>

<td style="text-align:center">0.001</td>

</tr>

<tr>

<td>International Journals and Magazines</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0.1</td>

<td style="text-align:center">0.3</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">12.20</td>

<td style="text-align:center">0.002</td>

</tr>

<tr>

<td>Persian Books</td>

<td style="text-align:center">2.0</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2.6</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">3</td>

<td style="text-align:center">15.86</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>International Books</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0.1</td>

<td style="text-align:center">0.2</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">14.94</td>

<td style="text-align:center">0.001</td>

</tr>

<tr>

<td>Audio-visual devices</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2.5</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">3</td>

<td style="text-align:center">19.83</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Experimental/ fieldwork facilities</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0.0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0.4</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">27.56</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Audio-visual materials</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0.6</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2.3</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">3</td>

<td style="text-align:center">18.31</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Printed materials for training</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2.7</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">24.29</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td colspan="12" style="background-color:#000; color:#fff; height:25px;">* The six item scale: 0= no access 1= very low 2= low, 3= somewhat, 4= high and 5= very high</td>

</tr>

</tbody>

</table>

<figure>![](p528fig2.jpg)

<figcaption>Figure 2: Accessibility of printed and electronic media in terms of geographical levels</figcaption>

</figure>

The Kruskal Wallis Test was used to understand the relationship between the institutional interests or tasks and the access to the printed and electronic facilities and materials. The test showed statistically significant differences (at the 0.05, 0.01 and 0.001 significant levels) among these institutions. Materials and facilities were more available and accessible in the provincial institutions than in district and county level institutions. A high accessibility of computer hardware was reported in the province, but this was very low or not accessible in district or county levels respectively. Moreover, computer software, Persian journals, magazines and books, audiovisual devices and materials and printed materials for training were moderately accessible in the provinces, while low or not accessible in the districts and counties. Database and Internet access was low at the provincial level, but very low or not accessible at all at the other levels.

Furthermore, international materials and experimental facilities were also very low or not accessible at all levels, though the provincial staff had a higher access. According to Tables 2 and 3, the researchers and production-oriented specialists had high access, while the staff of the other institutions had moderate or low access to their required computer hardware. This might be because of the low accessibility of these facilities in their district or county level institutions. Internet and required software were accessible mostly by the researchers and the provincial production-oriented specialists, but others had poor access. The high standard deviation within the groups showed that some provincial staff, such as the watershed conservationists, had high access to the Internet and the software. Databases were also to some extent accessible by the researchers, but largely inaccessible to the others.

The researchers, extensionists and production-oriented specialists showed moderate access to the Persian printed materials and audio-visual devices, which were less accessible to others. The researchers and extensionists also had moderate access to the audio-visual materials. All staff had low or poor access to international journals and books, and experimental or fieldwork facilities, though the researchers and some specialists had better access. The district and county level extension workers were the only district or county level staff who had better access (moderate access) to the Persian printed materials and audio-visual devices. This may be because of the nature of their tasks.

<table class="center"><caption>  
Table 2: Accessibility of required printed and electronic media and materials in terms of institutional interest or task (1)  
</caption>

<tbody>

<tr>

<th>Type of Professional</th>

<th> </th>

<th>Computer hardware</th>

<th>Computer Software</th>

<th>Databases</th>

<th>Internet</th>

<th>Audio-visual devices</th>

<th>Audio-visual materials</th>

</tr>

<tr>

<td rowspan="3">Researchers (n=13)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">4.0</td>

<td style="text-align:center">3.5</td>

<td style="text-align:center">2.8</td>

<td style="text-align:center">3.1</td>

<td style="text-align:center">2.9</td>

<td style="text-align:center">3.2</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">0.4</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">0.1</td>

<td style="text-align:center">0.1</td>

<td style="text-align:center">0.9</td>

<td style="text-align:center">0.7</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">4</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td rowspan="3">Extensionists (n=14)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">2.3</td>

<td style="text-align:center">2.1</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">2.9</td>

<td style="text-align:center">2.6</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.8</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.4</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0</td>

<td style="text-align:center">3</td>

<td style="text-align:center">2.5</td>

</tr>

<tr>

<td rowspan="3">Watershed management conservationists(n=14)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">2.3</td>

<td style="text-align:center">1.8</td>

<td style="text-align:center">1.9</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.3</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">2.1</td>

<td style="text-align:center">1.8</td>

<td style="text-align:center">1.9</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">1.5</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">0.5</td>

</tr>

<tr>

<td rowspan="3">Natural resources conservationists(n=32)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">2.4</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">0.4</td>

<td style="text-align:center">1.6</td>

<td style="text-align:center">1.4</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.4</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td rowspan="3">Nomadic Affairs staff (n=5)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">3.0</td>

<td style="text-align:center">2.0</td>

<td style="text-align:center">2.0</td>

<td style="text-align:center">2.4</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1.6</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">2.0</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">2.3</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1.3</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">3</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td rowspan="3">Production oriented specialists (n=9)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">4.0</td>

<td style="text-align:center">3.3</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">3.7</td>

<td style="text-align:center">3.7</td>

<td style="text-align:center">1.7</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">0.9</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.3</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">4</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">4</td>

<td style="text-align:center">4</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td rowspan="3">Total (n=87)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">2.8</td>

<td style="text-align:center">2.2</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">2.1</td>

<td style="text-align:center">1.9</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.5</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td rowspan="2">Kruskal Wallis Test</td>

<td style="text-align:center">Chi-squared</td>

<td style="text-align:center">15.79</td>

<td style="text-align:center">20.52</td>

<td style="text-align:center">22.35</td>

<td style="text-align:center">44.19</td>

<td style="text-align:center">28.19</td>

<td style="text-align:center">20.40</td>

</tr>

<tr>

<td style="text-align:center">Sig</td>

<td style="text-align:center">0.007</td>

<td style="text-align:center">0.001</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">0.001</td>

</tr>

<tr>

<td colspan="8" style="background-color:#000; color:#fff; height:25px;">* The six item scale: 0= no access 1= very low 2= low, 3= somewhat, 4= high and 5= very high</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 3: Accessibility of required printed and electronic media and materials in terms of institutional interest or task (2)  
</caption>

<tbody>

<tr>

<th rowspan="1">Type of professional</th>

<th> </th>

<th>Persian journals and magazines</th>

<th>International journals and magazines</th>

<th>Persian books</th>

<th>International books</th>

<th>Experimental or fieldwork facilities</th>

<th>Printed materials for training</th>

</tr>

<tr>

<td rowspan="3">Researchers (n=13)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">2.8</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">3.0</td>

<td style="text-align:center">1.8</td>

<td style="text-align:center">2.4</td>

<td style="text-align:center">3.2</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">0.7</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">0.9</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">0.7</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">3</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">2</td>

<td style="text-align:center">3</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td rowspan="3">Extensionists (n=14)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">2.0</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">2.4</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">3.1</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">0.9</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">1.4</td>

<td style="text-align:center">1.3</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2.5</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td rowspan="3">Watershed management conservationists(n=14)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">0.7</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1.6</td>

<td style="text-align:center">1.4</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">1.7</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1.6</td>

<td style="text-align:center">1.6</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td rowspan="3">Natural resources conservationists(n=32)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">1.6</td>

<td style="text-align:center">0.3</td>

<td style="text-align:center">1.8</td>

<td style="text-align:center">0.3</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">1.8</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">0.6</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.4</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">1</td>

<td style="text-align:center">0</td>

<td style="text-align:center">2</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td rowspan="3">Nomadic Affairs Staff (n=5)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">1.6</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">2.4</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.6</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">1.5</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.2</td>

<td style="text-align:center">1.5</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">2</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">1</td>

<td style="text-align:center">2</td>

</tr>

<tr>

<td rowspan="3">Production oriented specialists (n=9)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">3.3</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">3.3</td>

<td style="text-align:center">0.3</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">2.7</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">0.5</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">0.5</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">3</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td rowspan="3">Total (n=87)</td>

<td style="text-align:center">Mean*</td>

<td style="text-align:center">1.9</td>

<td style="text-align:center">0.8</td>

<td style="text-align:center">2.3</td>

<td style="text-align:center">0.9</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">2.2</td>

</tr>

<tr>

<td style="text-align:center">SD</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">1.4</td>

</tr>

<tr>

<td style="text-align:center">Median</td>

<td style="text-align:center">2</td>

<td style="text-align:center">0</td>

<td style="text-align:center">3</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td rowspan="2">Kruskal Wallis Test</td>

<td style="text-align:center">Chi-squared</td>

<td style="text-align:center">26.45</td>

<td style="text-align:center">22.07</td>

<td style="text-align:center">16.22</td>

<td style="text-align:center">22.39</td>

<td style="text-align:center">13.73</td>

<td style="text-align:center">18.09</td>

</tr>

<tr>

<td style="text-align:center">Sig</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">0.001</td>

<td style="text-align:center">0.006</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">0.017</td>

<td style="text-align:center">0.003</td>

</tr>

<tr>

<td colspan="8" style="background-color:#000; color:#fff; height:25px;">* The six item scale: 0= no access 1= very low 2= low, 3= somewhat, 4= high and 5= very high</td>

</tr>

</tbody>

</table>

<section>

### Categorical principal component analysis

The aim was to construct an index related to access to the different printed and electronic facilities. This index was named 'ACCESS index' and it comprised comprised variables. One dimensional solution was obtained from the categorical principal component analysis. The total Cronbach's alpha of one dimension model was calculated to be 0.948 out of 1.00, which indicated a very high reliability and the Eigenvalue was measured as 7.612 explaining 63.44% of total variations (see Table 5). The Eigenvalue can be used as an indication of how many dimensions are needed. The Eigenvalue for a dimension should be greater than 1 ([Meulman and Heiser 2005: 174](#meu05)).

The quantification of the variables clarified the meaning of loading value of the variable in the component loading. It showed a higher accessibility for the staff with medium to high access to computer hardware and software, Persian journals, magazines, books and audio-visual devices. Moreover, the low accessibility of these facilities was correlated to no or very low access to databases and audio-visual and printed materials for communicating with their clients, which were related to lack of access to the Internet, international journals, magazines and books, and experimental or fieldwork facilities (Table 4).

</section>

<table class="center"><caption>  
Table 4: Quantification of the variables of access to printed and electronic media in Categorical principal component analysis</caption>

<tbody>

<tr>

<th>Medium or facility</th>

<th>Category</th>

<th>No</th>

<th>Very Low</th>

<th>Low</th>

<th>Medium</th>

<th>High</th>

<th>Very High</th>

</tr>

<tr>

<td rowspan="2">Computer hardware*</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">12</td>

<td style="text-align:center">14</td>

<td style="text-align:center">5</td>

<td style="text-align:center">19</td>

<td style="text-align:center">23</td>

<td style="text-align:center">14</td>

</tr>

<tr>

<td style="text-align:center">Quantification**</td>

<td style="text-align:center">-1.38</td>

<td style="text-align:center">-1.38</td>

<td style="text-align:center">-0.99</td>

<td style="text-align:center">0.42</td>

<td style="text-align:center">0.89</td>

<td style="text-align:center">0.89</td>

</tr>

<tr>

<td rowspan="2">Computer software</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">16</td>

<td style="text-align:center">15</td>

<td style="text-align:center">8</td>

<td style="text-align:center">30</td>

<td style="text-align:center">17</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-1.36</td>

<td style="text-align:center">-1.22</td>

<td style="text-align:center">-0.06</td>

<td style="text-align:center">0.76</td>

<td style="text-align:center">0.98</td>

<td style="text-align:center">1.12</td>

</tr>

<tr>

<td rowspan="2">Databases</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">30</td>

<td style="text-align:center">19</td>

<td style="text-align:center">12</td>

<td style="text-align:center">18</td>

<td style="text-align:center">7</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-1.25</td>

<td style="text-align:center">-0.01</td>

<td style="text-align:center">0.69</td>

<td style="text-align:center">1.05</td>

<td style="text-align:center">1.34</td>

<td style="text-align:center">1.34</td>

</tr>

<tr>

<td rowspan="2">Internet</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">44</td>

<td style="text-align:center">8</td>

<td style="text-align:center">12</td>

<td style="text-align:center">9</td>

<td style="text-align:center">9</td>

<td style="text-align:center">5</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-0.99</td>

<td style="text-align:center">0.85</td>

<td style="text-align:center">0.99</td>

<td style="text-align:center">0.99</td>

<td style="text-align:center">1.12</td>

<td style="text-align:center">1.12</td>

</tr>

<tr>

<td rowspan="2">Persian Journals</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">18</td>

<td style="text-align:center">14</td>

<td style="text-align:center">19</td>

<td style="text-align:center">27</td>

<td style="text-align:center">9</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-1.76</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">0.93</td>

<td style="text-align:center">0.93</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td rowspan="2">International journals</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">46</td>

<td style="text-align:center">18</td>

<td style="text-align:center">15</td>

<td style="text-align:center">1</td>

<td style="text-align:center">7</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-0.94</td>

<td style="text-align:center">0.98</td>

<td style="text-align:center">0.98</td>

<td style="text-align:center">1.25</td>

<td style="text-align:center">1.92</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td rowspan="2">Persian books</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">12</td>

<td style="text-align:center">14</td>

<td style="text-align:center">17</td>

<td style="text-align:center">28</td>

<td style="text-align:center">16</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-2.16</td>

<td style="text-align:center">-0.29</td>

<td style="text-align:center">-.029</td>

<td style="text-align:center">0.69</td>

<td style="text-align:center">0.97</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td rowspan="2">International books</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">46</td>

<td style="text-align:center">17</td>

<td style="text-align:center">14</td>

<td style="text-align:center">9</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-0.92</td>

<td style="text-align:center">0.78</td>

<td style="text-align:center">0.89</td>

<td style="text-align:center">1.64</td>

<td style="text-align:center"></td>

<td style="text-align:center">1.67</td>

</tr>

<tr>

<td rowspan="2">Audio-visual devices</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">19</td>

<td style="text-align:center">15</td>

<td style="text-align:center">12</td>

<td style="text-align:center">21</td>

<td style="text-align:center">19</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-1.76</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">0.81</td>

<td style="text-align:center">0.81</td>

<td style="text-align:center">1.30</td>

</tr>

<tr>

<td rowspan="2">Experimental or fieldwork facilities</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">30</td>

<td style="text-align:center">24</td>

<td style="text-align:center">11</td>

<td style="text-align:center">17</td>

<td style="text-align:center">5</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-1.30</td>

<td style="text-align:center">0.31</td>

<td style="text-align:center">0.45</td>

<td style="text-align:center">1.20</td>

<td style="text-align:center">1.20</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td rowspan="2">Audio-visual materials</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">24</td>

<td style="text-align:center">12</td>

<td style="text-align:center">16</td>

<td style="text-align:center">22</td>

<td style="text-align:center">12</td>

<td style="text-align:center">1</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-1.50</td>

<td style="text-align:center">-0.23</td>

<td style="text-align:center">0.46</td>

<td style="text-align:center">0.80</td>

<td style="text-align:center">1.04</td>

<td style="text-align:center">1.39</td>

</tr>

<tr>

<td rowspan="2">Printed materials for training</td>

<td style="text-align:center">Frequency</td>

<td style="text-align:center">15</td>

<td style="text-align:center">14</td>

<td style="text-align:center">14</td>

<td style="text-align:center">29</td>

<td style="text-align:center">12</td>

<td style="text-align:center">3</td>

</tr>

<tr>

<td style="text-align:center">Quantification</td>

<td style="text-align:center">-1.98</td>

<td style="text-align:center">-0.46</td>

<td style="text-align:center">0.26</td>

<td style="text-align:center">0.64</td>

<td style="text-align:center">0.92</td>

<td style="text-align:center">0.92</td>

</tr>

<tr>

<td colspan="8" style="background-color:#000; color:#fff; height:25px;">*Optimal scaling level: ordinal; **Variable principal normalisation</td>

</tr>

</tbody>

</table>

<section>

Table 5 shows the loading values of each variable in the ACCESS component, which indicates the strength of the relationship between the component and each variable. Besides these loading values, the quantifications of each variable's categories give an interpretation of these relationships. All variables had statistically significant and positive loadings with the component: very strong (over 0.7) or strong association (0.67).

<table class="center"><caption>  
Table 5: Access to printed and electronic media component loadings of categorical principal component scale  
</caption>

<tbody>

<tr>

<th rowspan="1">Medium or facility</th>

<th>Component loadings</th>

</tr>

<tr>

<td>Computer hardware</td>

<td style="text-align:center">0.84</td>

</tr>

<tr>

<td>Computer software</td>

<td style="text-align:center">0.86</td>

</tr>

<tr>

<td>Databases</td>

<td style="text-align:center">0.78</td>

</tr>

<tr>

<td>Internet</td>

<td style="text-align:center">0.78</td>

</tr>

<tr>

<td>Persian journals and magazines</td>

<td style="text-align:center">0.79</td>

</tr>

<tr>

<td>International journals and magazines</td>

<td style="text-align:center">0.72</td>

</tr>

<tr>

<td>Persian books</td>

<td style="text-align:center">0.83</td>

</tr>

<tr>

<td>International books</td>

<td style="text-align:center">0.67</td>

</tr>

<tr>

<td>Audio-visual devices</td>

<td style="text-align:center">0.86</td>

</tr>

<tr>

<td>Experimental or fieldwork facilities</td>

<td style="text-align:center">0.79</td>

</tr>

<tr>

<td>Audio-visual materials</td>

<td style="text-align:center">0.80</td>

</tr>

<tr>

<td>Printed materials for training</td>

<td style="text-align:center">0.83</td>

</tr>

<tr>

<td colspan="8" style="background-color:#000; color:#fff; height:25px;">Cronbach's Alpha (Eigenvalue)= 0.948; Variance accounted for (Eigenvalue=7.612, % of variance = 63.437)</td>

</tr>

</tbody>

</table>

Correlational analyses (Spearman's Rho) between the ACCESS index and the personal and organizational characteristics of the respondents showed a strongly significant correlation at the 0.001 level (2-tailed) with the education level (?=0.65***), membership in scientific associations (?=0.63***), geographical level (?=0.60***) and scientific/ professional position (?=0.50***).

A poor, but significant correlation was also identified between the management responsibility and the ACCESS index (?=0.23*). This was mostly related to access to computer hardware and software; the Persian books and printed training materials; and the access to audio-visual devices. It seems these facilities and information sources have been provided more to the managers than other staff. Age, job experience and attendance on training courses for professional career development showed no significant association with the index. This also shows conventional professional career development methods such as training courses or workshop are not associated to alternative professional development techniques.

<table class="center" style="width:90%;"><caption>  
Table 6: Correlation between access to printed and electronic media and personal and organizational characteristics</caption>

<tbody>

<tr>

<th rowspan="2">Variable</th>

<th colspan="2">Object scores (dimension)</th>

</tr>

<tr>

<th>?</th>

<th>Sig.</th>

</tr>

<tr>

<td>Attending technical training courses related to natural resources, watershed conservation, horticulture in 5 years</td>

<td style="text-align:center">-0.07</td>

<td style="text-align:center">0.541</td>

</tr>

<tr>

<td>Attending professional training courses related to development, extension, management, education in 5 years</td>

<td style="text-align:center">0.00</td>

<td style="text-align:center">0.999</td>

</tr>

<tr>

<td>Geographical level</td>

<td style="text-align:center">0.60***</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Type of job</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">0.539</td>

</tr>

<tr>

<td>Membership in scientific associations</td>

<td style="text-align:center">0.63***</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Scientific or professional position</td>

<td style="text-align:center">0.50***</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Management responsibility</td>

<td style="text-align:center">0.23*</td>

<td style="text-align:center">0.034</td>

</tr>

<tr>

<td>Job experience (b)</td>

<td style="text-align:center">-0.03</td>

<td style="text-align:center">0.773</td>

</tr>

<tr>

<td>Educational Level (b)</td>

<td style="text-align:center">0.65***</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Age (b)</td>

<td style="text-align:center">-0.14</td>

<td style="text-align:center">0.205</td>

</tr>

<tr>

<td colspan="3" style="background-color:#000; color:#fff; height:25px;">a) Spearman's rho correlation, b) Pearson correlation - correlation is significant: * at the 0.05 level, **at the 0.01 level and ***at the 0.001 level (2-tailed)</td>

</tr>

</tbody>

</table>

ANOVA tests showed the ACCESS index scores were significantly different among the staff of different geographical levels (F=34.43*** and Eta=0.671***) and among different institutions based on their interests (F=6.937*** and Eta= 0.548***). This reveals that the provincial staff had higher object scores or higher access to the printed and electronic facilities than the district or county level staff. Furthermore, because of the positively higher object scores of the researchers and the production oriented specialists, the facilities in those institutions were much more accessible than the others. Moreover, the extensionists showed higher object scores than the NR and watershed conservationists and the nomadic affairs specialists (Table 7).

<table class="center"><caption>  
Table 7: ANOVA for comparing access index, object scores, among three geographical levels of work and among different institutions  
</caption>

<tbody>

<tr>

<th rowspan="1">Geographical level</th>

<th>N</th>

<th>Mean(a)</th>

<th>Sd</th>

<th>F</th>

<th>Sig.</th>

<th>Eta</th>

</tr>

<tr>

<td>County Level</td>

<td style="text-align:center">6</td>

<td style="text-align:center">-0.7</td>

<td style="text-align:center">0.3</td>

<td style="text-align:center">34.43</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">0.673***</td>

</tr>

<tr>

<td>District Level</td>

<td style="text-align:center">18</td>

<td style="text-align:center">-1.2</td>

<td style="text-align:center">0.9</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Provincial level</td>

<td style="text-align:center">63</td>

<td style="text-align:center">0.4</td>

<td style="text-align:center">0.7</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">87</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<th rowspan="1">Institutions (based on interest)</th>

<th>N</th>

<th>Mean(a)</th>

<th>Sd</th>

<th>F</th>

<th>Sig.</th>

<th>Eta</th>

</tr>

<tr>

<td>Researchers</td>

<td style="text-align:center">13</td>

<td style="text-align:center">1.0</td>

<td style="text-align:center">0.3</td>

<td style="text-align:center">6.937</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">0.548***</td>

</tr>

<tr>

<td>Extensionists</td>

<td style="text-align:center">14</td>

<td style="text-align:center">0.1</td>

<td style="text-align:center">0.6</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>Watershed conservationists</td>

<td style="text-align:center">14</td>

<td style="text-align:center">-0.4</td>

<td style="text-align:center">1.3</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>Natural resources conservationists</td>

<td style="text-align:center">32</td>

<td style="text-align:center">-0.5</td>

<td style="text-align:center">0.9</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td>Nomadic affairs staff</td>

<td style="text-align:center">5</td>

<td style="text-align:center">-0.1</td>

<td style="text-align:center">1.1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

</tr>

</tbody>

<tbody>

<tr>

<td>Production- oriented specialists</td>

<td style="text-align:center">9</td>

<td style="text-align:center">0.7</td>

<td style="text-align:center">0.4</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td>Total</td>

<td style="text-align:center">87</td>

<td style="text-align:center">0</td>

<td style="text-align:center">1</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

<td style="text-align:center">-</td>

</tr>

<tr>

<td colspan="7" style="background-color:#000; color:#fff; height:25px;">(a)Object scores, ***Eta correlation is significant at the 0.001 level (2-tailed)</td>

</tr>

</tbody>

</table>

## Discussion and conclusion

Continuing professional development of agricultural and natural resources organizations staff is essential for improving competences and performance. Self-directed learning techniques are considered to be effective and efficient methods to facilitate experiential learning and professional development. Overall, this study shows these techniques are not accessible to Iranian agricultural professionals, especially practitioners.

The study reveals that in addition to the type of profession and organization, there is a positive correlation between the variables of the education level and the access to academic printed material, software, databases and the Internet. This supports the studies by Movahed and Iravani ([2002](#mov02)), Yaghoubi and Shamsayi ([2004](#yag04)) and Haji Hashemi ([2008](#haj08)). Both personal and organizational characteristics of the respondents are recognized to be important characteristics correlated to the accessibility of printed and electronic information sources. The education level, membership in scientific associations, geographical level of the institutions, institutional interests of the staff, and scientific or professional position are the most important variables. However, access to the electronic and printed facilities is not significantly different in terms of age and job experience. This result does not support the studies by Movahed and Iravani ([2002](#mov02)) and Yaghoubi and Shamsayi ([2004](#yag04)). It seems these facilities, as mostly modern information sources, have been provided to some staff in the recent years; therefore age or job experience have not been important factors for accessing these sources and facilities.

The provincial staff have better access than the district and county level ones to the software and hardware electronic sources, the Persian books and journals, audio-visual devices and materials, printed materials for training, and required experimental or fieldwork facilities. This means the geographical gap between the institutions can increase the inequality of access to the self-directed leaning methods for continuing professional developoment in Iran and perhaps other countries with similar characteristics. This is specifically important for the electronic sources or information and communication technologies. The concept of _digital divide_ or _digital inequality_ introduced by some scholars ([Alvarez 2003](#alv03); [Anderson and Tracey 2001](#and01); [DeHaan 2003](#deh03); [Greenhill 1998](#gre98); [Hargittai and Hinnant 2008](#har08); [Hsieh _et al._ 2008](#hsi08)) can explain and articulate the concerns regarding the inequality of access to such technologies. This inequality contradicts the promise of enabling unlimited access to information irrespective of a person's social situation. Policies and actions are needed to reduce the existing barriers and to remove the information gaps created by this inequality.

The access to electronic and printed facilities and information sources among the higher skilled or more professional staff, such as agricultural researchers and some subject specialists (agronomists, horticulturists and watershed management conservationists), are higher than the others. Extension staff and other natural resource conservationists at the provincial level have moderate access to these sources and facilities, while the district, county and grassroots level staff have little access to them. Therefore, professional level and scientific position are important factors in relation to access to these sources for continuing professional development. This may be because these professionals demand access (directly because of their education and skills in using electronic facilities and international material) or because they have the influence and power to access them. This discussion is supported when we look at the association of the geographical level with their access, because scientists and higher ranked professionals and managers are mostly located at the provincial level rather than district or county levels.

Iranian agricultural professionals, especially practitioners, have poor access to international journals and books for continuing professional development. This can be both related to supply and demand. The provincial institutions lack international currency to provide these materials to their staff at different geographical levels. The availability of these materials in the province is also very limited because of international sanctions and the lack of international book stores at provincial and district levels. According to the author's experience and informal discussions during the focus groups, the poor English skills of the majority of Iranian agricultural professionals constrain their demand for these materials either in their institutions or at their homes. This issue may still restrain using these information sources, even if institutions provide on-line international databases and journals in the future. organizing English training courses in parallel with establishing a system of electronic journals and books may help organizations cope with this issue. Linking these institutions with university libraries and on-line sources can also help alleviate this low accessibility in the provinces, where the academics can translate the international books and journal papers. However, the problem still remains for the lower geographical level staff. Moreover, this requires a well-defined relationship between practitioners and academic intuitions, which is weak based on the author's studies.

The lessons learned from this discussion are that planning professional development strategies and programmes does not totally depend on the organization's internal learning environment. The environment or the supra-system surrounding that organization can directly or indirectly affect such development. Moreover, human resources development programmers assess the demand or the base capability of their staff prior to developing any learning approach or strategy to them. This means that providing the required programmes is related to the socio-economic capacity and the power of an organization: external constraining or stimulating forces at different geographical levels, and the pre-requisite competences needed by the professionals to use or demand accessibility to these programmes, particularly self-directed leaning methods.

Provision of these facilities are required for the district and county level institutions as well as materials appropriate for all staff irrespective of educational level or managerial and scientific position. Improving electronic skills and international communication, especially English skills, may help them use international documents and materials much more effectively. In addition to providing the required infrastructure, an information network involving the various specialists, staff and researchers with different interests and at different geographical levels is required. This will help them improve their competences for participating in a multi-interest and multi-level decision making. Traditional methods of continuing professional development can also be used along with the alternative techniques in order to increase its effectiveness.

## Acknowledgements

The author acknowledges the participation of agricultural and natural resources professionals in the Chaharmahal and Bakhtiari Province of Iran in this research. Special thanks also go to the referees for their substantial comments, which led to significant changes in the manuscript as well as copy-editors for their helpful revisions that enabled the author to satisfy the style requirements of the journal.

## About the author

**Esmail Karamidehkordi** is an Assistant Professor in the Department of Agricultural Extension and Education, University of Zanjan, Zanjan, Iran. He received his Bachelor and Master's degrees in Agricultural Extension and Education from the University of Ahwaz and the University of Tarbiat Modarres, Iran and his PhD in the Knowledge and Information Systems in Agriculture and Natural Resource Management from the University of Reading, the UK. He can be contacted at: [e.karamidehkordi@gmail.com](mailto:h.gibson@lim.ac.sa) or [e.karami@znu.ac.ir](mailto:e.karami@znu.ac.ir)]

#### References

*   Alibeigi, A.H. & Zarafshani, K. (2006). Are agricultural graduates meeting employers' expectations? A perspective from Iran. _Perspectives in Education,_ **24**(3), 53-61.
*   Alvarez, A. S. (2003). Behavioral and environmental correlates of digital inequality. _IT &Society_ **1**(5), 97-140.
*   Anderson, B. & Tracey, K. (2001). Digital living. _American Behavioral Scientist,_ **45**(3), 456-475.
*   Bauder, H. (2006). Origin, employment status and attitudes towards work: immigrants in Vancouver, Canada. _Work, Employment and Society,_ **20**(4), 709-729.
*   Capio. (2006). _Continuing professional development maximising effectiveness_. Reading,UK: Capio Healthcare UK Ltd.
*   Cushion, C. J., Armour, K. M. & Jones, R. L. (2003). Coach education and continuing professional development: experience and learning to coach _QUEST_, **55**(3) 1-10.
*   Day, C. & Sachs, J. (2005). _International handbook on the continuing professional development of teachers_. Maidenhead, UK: Open University Press.
*   DeHaan, J. (2003). IT and social inequality in the Netherlands. _IT &Society,_ **1**(4), 27-45.
*   Dornan, T., Carroll, C. & Parboosingh, J. (2002). An electronic learning portfolio for reflective continuing professional development. _Medical Education,_ **36**(8), 767-769.
*   Evetts, J. (1998). Continuing professional development for engineers: UK and European Dynamics. _European Journal of Engineering Education_, **23**(4), 443-452.
*   Faerber, C. & Johnson, A. (2006). Continuing professional development. In P. Pritchard (Ed.), _Success strategies for women in science, a portable mentor_, (pp. 19-35). London: Academic Press.
*   Galloway, S. (1998). The professional body and continuing professional development: new directions in engineering. _Innovations in Education & Training International,_ **35**(3), 231-240.
*   Gibbs, T., Brigden, D. & Hellenberg, D. (2005). Continuing professional development. _South African Family Practice_, **47**(3), 5-6.
*   Gorman, H. (2003). Which skills do care managers need? A research project on skills, competency and continuing professional development. _Social Work Education_, **22**(3), 245-259.
*   Grant, J. (2004). Continuing professional development. In T. Van Zwanenberg & J. Harrison (Eds.), _Clinical governance in primary care_, (pp. 183-198). Abingdon, UK: Radcliffe Medical Press.
*   Greenhill, A. (1998). _[Equity and access to information technology shifting the source of the problem in the 21st Century](http://www.webcitation.org/6AEbiHAI9)_. Paper presented at the "TASA 98" conference, Queensland University of Technology, December 1998\. Retrieved from http://www.spaceless.com/papers/9.htm (Archived by WebCite® at http://www.webcitation.org/6AEbiHAI9)
*   Haji Hashemi, Z. (2008). _Investigating factors influencing the use of information and communication technologies (ICTs) in agricultural extension, a study of extension workers' view in the Isfehan province._ Tehran: Tarbiat Modares University, .
*   Hargittai, E. & Hinnant, A. (2008). Digital inequality. _Communication Research,_ **35**(5), 602-621.
*   Hsieh, J.J.P-A., Rai, A. & Keil, M. (2008). Understanding digital inequality: comparing continued use behavioral models of the socio-economically advantaged and disadvantaged. _Management Information Systems Quarterly,_ **34**(1), 97-126
*   Iran. _Ministry of Jahad-e- Agriculture, Human Resource Development Administration_. (2003). _Agricultural extension personnel statistics and their training courses_. Tehran: Human Resource Development Administration of the Extension and Farming Systems Deputy of the Ministry of Jahad-e-Agriculture (in Persian).
*   Karbasioun, M. (2006). _Needs assessment and competency profiles for agricultural instructors in the context of agricultural development in Iran_ . Unpublished doctoral dissertation, Wageningen University, Wageningen, The Netherlands.
*   Karbasioun, M. & Mulder, M. (2004). [HRM and HRD in agricultural extension organizations in Iran, a literature review](http://www.webcitation.org/6AEkWEwYh). In James J. Connors, (Ed.). _Proceedings of the 20th Annual Conference of the Association for International Agricultural and Extension Education, Dublin, Ireland,_ (pp. 13-24). Retrieved 27 August, 2012 from http://aiaee.tamu.edu/2004/Accepted/021.pdf (Archived by WebCite® at http://www.webcitation.org/6AEkWEwYh) [Proceedings available online at http://aiaee.tamu.edu/2004/]
*   Kirkwood, M. & Christie, D. (2006). The role of teacher research in continuing professional development. _British Journal of Educational Studies,_ **54**(4), 429-448.
*   Knight, P. (1998). Professional obsolescence and continuing professional development in higher education. _Innovations in Education & Training International,_ **35**(3) 248-256.
*   Knowles, M. (1984). _Andragogy in action_. Houston, TX: Gulf Publishing Co.
*   Kolb, D. A. (1984). _Experiential learning_. Englewood Cliffs, NJ: Prentice Hall.
*   Lee, Y. (2006). _[An investigation and critique of competencies needed by human resource development (HRD) Master's degree graduates in Korea.](http://www.webcitation.org/6AEl5krC3)_ Unpublished doctoral dissertation, Florida State University, Tallahassee, Florida, USA. Retrieved 27 August, 2012 from http://diginole.lib.fsu.edu/cgi/viewcontent.cgi?article=2905&context=etd&sei-redir=1 (Archived by WebCite® at http://www.webcitation.org/6AEl5krC3)
*   Lesaona-Tshabalala, B. V. (2001). _Agricultural information needs and resources available to agriculturists and farmers in a developing country with special reference to Lesotho._ Unpublished Master's dissertation, Rand Afrikaans University, Johannesburg, South Africa.
*   Lewis, J. & Day, G. (2004). Continuing professional development for teachers. _Journal of Biological Education,_ **38**(3) 144-144.
*   Lindner, J. R., Dooley, K. E. & Wingenbach, G. J. (2003). A cross-national study of agricultural and extension education competencies. _Journal of International Agricultural and Extension Education,_ **10**(1), 51-59.
*   Littlejohn, A. (2001). New lessons from past experiences: recommendations for improving continuing professional development in the use of ICT. _Journal of Computer Assisted Learning,_ **18**(2), 166-174.
*   Malak-Mohammady, E. (1998). _Extension education_. Tehran: Centre for University Publication.
*   Meera, N., Jhamtani, A. & Rao, D. (2004). _[Information and communication technology in agricultural development: a comparative analysis of three projects from India.](http://www.webcitation.org/6AElge3T0)_ London: Agricultural Research & Extension Network. (Network paper, no .135) Retrieved 27 August, 2012 from http://www.odi.org.uk/work/projects/agren/papers/agrenpaper_135.pdf (Archived by WebCite® at http://www.webcitation.org/6AElge3T0) .
*   Meulman, J. J. & Heiser, W. J. (2005). _SPSS categories 14.0_. Chicago, IL: SPSS Inc.
*   Mohammadzadeh, J. (2003). _Agricultural extension workers' attitude towards participation with farmers and agricultural researchers in the Western Azarbayjan Province._ Tehran: Tarbiat Modares University (in Persian).
*   Moore, L. L. & Rudd, R. D. (2004). Leadership skills and competencies for extension directors and administrators. _Journal of Agricultural Education,_ **45**(3) 22-33.
*   Movahed, H. & Iravani, H. (2002). A model for Internet use by graduate students of selected agricultural faculties in Iran. _Iranian Journal of Agricultural Sciences,_ **33**(4), 717-726.
*   Pezeshki Raad, G., Yoder, E. P. & Diamond, J. E. (1994). Professional competencies needed by extension specialists and agents in Iran. _Journal of International Agricultural and Extension Education,_ **1**(1), 45-53.
*   Pishbin, A. (1996). _Professional Competencies needed by Agricultural Extension Agents of the Fars Province._ Tehran: Tarbiat Modares University (in Persian).
*   Pouratashi, M. & Rezvanfar, A. (2009). Analysis of factors influencing application of ICT by agricultural graduate students. _Journal of the American Society for Information Science and Technology,_ **61**(1), 81-87.
*   Raab, R. T. & Abdon, B. R. (2005). [Distance education and eLearning for sustainable agriculture: lessons learned.](http://www.webcitation.org/6AEmY43vV) In K. Toriyama, K. L. Heong & B. Hardy (Eds.), _Rice is life: scientific perspectives for the 21st century, Proceedings of the World Rice Research Conference held in Tokyo and Tsukuba, Japan, 4-7 November 2004,_ (pp. 574-576). Los Baños, Philippines: International Rice Research Institute. Retrieved 27 August, 2012 from http://books.irri.org/9712202046_content.pdf (Archived by WebCite® at http://www.webcitation.org/6AEmY43vV)
*   Raab, R. T., Ellis, W. W. & Abdon, B. R. (2002). Multisectoral partnerships in e-learning, a potential force for improved human capital development in the Asia Pacific. _Internet and Higher Education,_ **4**(3-4), 217-229.
*   Ramlall, S. J. (2006). Identifying and understanding HR competencies and their relationship to organizational practices. _Applied Human Resources Management  Research,_ **11**(1) 27-38.
*   Richardson, D. (2003). _Agricultural extension transforming ICTs? Championing universal access ICT Observatory 2003: ICTs – transforming agricultural extension?_ Paper presented at the WICC/CTA's ICT Observatory, September 2003.
*   Shahani, A. (1999). _Evaluation of in-service training courses by agricultural extension workers of Mazndaran province._ Tehran: Tarbiat Modares University (in Persian).
*   Singh, S. (2006). _[Selected success stories on agricultural information systems](http://www.webcitation.org/6AEnN7BXM)_. Thailand: Asia-Pacific Association of Agricultural Research Institutions Retrieved 27 August, 2012 from http://www.apaari.org/wp-content/uploads/2009/05/ss_2006_01.pdf (Archived by WebCite® at http://www.webcitation.org/6AEnN7BXM)
*   Sridharan, S., Kawata, J. H., Campbell, B. & Tseng, C.-W. M. (2005). Contemporaneous relationship between substance abuse treatment and poly-substance use: evidence from the persistent effect of treatment studies. _Journal of Substance Abuse Treatment,_ **28**(2), S83-S90.
*   Swanson, B. E. & Rajalahti, R. (2010). _Strengthening agricultural. Extension and advisory systems: procedures for assessing, transforming, and evaluating extension systems_ Washington, DC: The World Bank.
*   Wermke, W. (2010). Continuing professional development in context: teachers' continuing professional development culture in Germany and Sweden. _Professional Development in Education_, **37**(5), 665-683
*   Westcott, L. (2005). Continuing professional development. In T. Clouston & L. Westcott (Eds.), _Working in health and social care: an introduction for allied health professionals_ (pp. 87-102). London: Elsevier Health Sciences.
*   Wynne-Jones, J. (1999). Continuing professional development. In J. Cox & I. Mungall (Eds.), _Rural healthcare_ (pp. 121-134). Abingdon, Oxon: Radcliffe Medical Press Ltd.
*   Yaghoubi, J. & Shamsayi, E. (2004). _Assessing effective factors in using Internet by faculty members of Agricultural College of Zanjan University, Iran._ Paper presented at the Proceedings of the 20nd Annual Conference of the Association for International Agricultural and Extension Education (AIAEE), Dublin, Ireland.
*   Zorzi, R., Perrin, B., McGuire, M., Long, B. & Lee, L. (2002). Defining the benefits,outputs, and knowledge elements of program evaluation. _Canadian Journal of Program Evaluation,_ **17**(3), 143-150.