#### vol. 20 no. 3, September, 2015

# Applying the user meta model to the analysis of scientific knowledge production and transfer. insights from exploring scientific, small-scale, fishery management in Chile

#### [Carlos Hidalgo](#author), [Francisco Ther](#author) and [Asunción Diaz](#author)  
Universidad de los Lagos, Pobl lomas de Puyehue, Osorno, Los Ríos Region, Chile

#### Abstract

> **Introduction**. Our goal is to provide insights that improve the relationship among scientific knowledge production and fisheries' sustainability by examining the scientific endeavour of a Chilean small-scale fisheries administration regime: the ‘Management and Exploitation Areas for Benthic Resources’.  
> **Method**. Given the importance of this reflexive exercise the user meta model is employed as a methodological framework, and a relational analysis that combines classic social network analysis of co-authorship and citation with a qualitative approach based on categorical classifications of the expert choice technique is employed.  
> **Analysis**. Analysis combines the calculation of centrality measurements in both co-authorship and citation networks, and the use of nodes’ attributes in a temporal axis to identify trends in scientific production and transfer.  
> **Results**. The results reveal: a) the existence of two clusters or scientific schools that include biological and social scientists and b) the isolation of economic science. The results also reveal the existence of scientific threshold articles that have significantly modified processes of scientific production.  
> **Conclusion**. Scientific threshold articles do not exclusively belong to a single scientific school, suggesting the absence of a single hegemonic scientific cluster at the level of innovative practices. Conclusions highlight the implications of this dynamics to the sustainability of fisheries management.

## Introduction

On occasion, scientific fields develop innovative reflexive tools through which science is re-defined ([Kuhn, 1962](#kuh62)). This natural tendency is often shaped by historical trends. Numerous studies have examined this phenomenon as researchers have grown more aware of reflexive exercises. This trend can be attributed to several factors: epistemological changes, a gradual awareness of historical authenticity, and the effects of various constructions of reality ([Escobar, 1999](#esc99)). Some of these approaches are crucial because they highlight the need to devalue previously unquestioned truths when conducting scientific investigations. Following Latour ([1993](#lat93)), these reflexive exercises disassemble the false truth that modernity (and its knowledge strategy, _normal_ science and technology) has created the distinction between nature and society and, consequently, between science and research problems. An objective of this article is to expose areas in which this distinction is not recognized.

This paper examines problems that result from the production and transfer of scientific knowledge on a specific topic, assuming that multiple disciplines are affected by these topics. Knowledge of a co-management mechanism used in small-scale Chilean fisheries is examined as a case study. Recognizing the complexity of scientific practices and knowledge, scientists are now interested in characterizing transformations of scientific production and transfer to address the ‘Management and Exploitation Areas for Benthic Resources’ (MEABRs). Although a description of MEABRs is given below, they can be briefly defined as an administrative figure for the co-management of small-scale Chilean fishery resources that has legally existed since enacting the Fisheries and Aquaculture Law ([1991](#chi91)). It is essential to examine scientific knowledge production in this research area because scientific production can be used to refine co-management arrangements that have already been initiated in the international arena ([Evans, Cherrett and Pemsl, 2011](#eva11); [Salas, Chuenpagdee, Seijo and Charles, 2007](#eva11)).

Our final goal is provide insights that improve the relationship among scientific knowledge production and fisheries' sustainability by using classical research tools to the social studies of science and technology. Using the ‘user meta model’ as a methodological frame, we conduct a social network analysis of co-authorship and citation networks with qualitative features. We argue that this approach can be used in studies of citation and co-authorship when applying social network analysis to small samples as in our case. We modify previously selected and weighted attributes when applying the _expert choice_ technique. These attributes are utilized to strengthen our analysis of social network analysis centrality measures and to link citation and co-authorship networks through ‘threshold paper’ processes and through collaboration between authors. Thus we unveil expert production and knowledge transfer trends that can be useful to understand limitations of scientific assessments regarding fisheries' public policies and their economic institutions.

## Reflexivity and the social study of science and technology

Despite the existence of numerous approaches that may be useful for our purposes, we focus on theoretical aspects that allow us to compare features of the bibliometric social studies of science and technology and social science reflexivity. This approach may contribute to the interpretation of certain scientific tasks, revealing the current state of scientific fields dedicated to MEARB research.

Although scientific studies of reflexivity and second-order observation are not new, these studies have been refined over the last four decades, particularly in response to scepticism of the relationship between science, technology and social welfare. Reflexivity and self-observation exercises have grown more prevalent because of scepticism surrounding the linear predictive model of conventional science and because of Thomas Kuhn’s ([1962](#kuh62)) work, which highlights the relevance of socio-historical contexts to scientific production and transfer. Only since the post war period have Western modernization and development models been severely challenged. Following this trend, the development of social studies of science and technology recaptured the interest of the scientific community, primarily owing to its critique of essentialist approaches to scientific knowledge and practices.

Social studies of science and technology objectives are concerned with social contexts of scientific and technological production and with the consequences of scientific advancement in the social world ([Barnes, Bloor and Henry, 1996](#bar96); [Knorr-Cetina and Mulkay, 1983](#kno83); [Thirunarayanan, 1992](#thi92)). The geographical expansion of social studies of science and technology began during the mid-twentieth century, expanding concerns surrounding the epistemological and methodological functioning of sciences worldwide and taking various forms, such as Frankfurt's critical school, the Mertonian approach, Edinburgh’s _strong programme_, and the ethnography of science, among others ([Bugliarello, 1988](#bug88); [Lewis, 1990](#lew90); [Woolgar, 1981](#woo81)). Of these approaches, we are particularly interested in the European tradition of social studies of science and technology, which focuses on the conditions of scientific production and transfer rather than on the social consequences and axiological assessments of scientific knowledge, that are addressed in the American tradition ([García, _et al_., 2001](#gar01); [Martínez, 2004](#mar04)). Through this approach, a focus on social contexts allows for the emergence of knowledge. Sociological theories have stressed that modern society has generated various reflexivity mechanisms that vary based on ontological approaches. From economics and politics to education and the sciences, methodologies and theories have been developed to observe, explain or interpret phenomena, establishing self-observation mechanisms of a second order.

The theory of knowledge can also be understood as a self-observation tool that allows for auto-regulation of the scientific system ([Luhmann, 1996](#luh96)), enabling the questioning of object and subject distinctions and the evaluation of scientific model performance. According to the biology of knowledge ([Varela, 2005](#var05)), such reflexivity practices can be understood as autonomous processes that appear in nature in varying forms as regulation mechanisms of system boundary settings. The biological self-referential model can be used to observe scientific systems if we assume that a community of scientists applies structural limits to distinctions and scientific observations, thus applying theoretical development to various research fields. Maturana ([1995](#mat95)) proposes that all human activity is generated through differentiated cognitive domains, such as science, which are defined through actions framed by criteria of scientific explanation acceptability or validation. This point demonstrates the importance of investigating transformations that occur in research fields and our motivation for depicting the relational conditions that allow for MEABR-based scientific knowledge production. Although we examine a small sample, this study is not a single-case inquiry. Hence, Bruno Latour's ‘actants’ ([Latour, 1987](#lat87), [2005](#lat87); [Latour and Woolgar, 1979](#lat79)) are not identified or tracked. Instead, this investigation examines how relationships between research fields and researchers have transformed scientific knowledge production and transfer.

However there is still a long distance to link in a proper way the more abstract ideas cited about biological self-referential model with the methodological frame employed here; whilst the first is built over a philosophical debate and qualitative arguments, the latter deploys mathematics and structural properties. Here we only venture to propose a possible pathway to relate them, particularly by complementing participatory and qualitative designed properties for researchers and papers, and threshold papers will enable interpreting some self-organizing properties of co-authorship networks, citation networks and their interface.

## A brief history of the _Management and Exploitation Areas for Benthic Resources_

Chile is a country with a unique geographical configuration. Located in the Southern Cone of America, its length (between parallels 17° 30' and 56° 30' S latitude, almost 4,300 km long) and narrowness (average 177 km) form an elongated shape that is confined within the Andes mountains to the east and by the Pacific Ocean to the west. The country is also characterized by a relationship between its national history and coastal culture: coastal populations have used marine ecosystems for subsistence since pre-Columbian times ([Hucke-Gaete _et al._, 2010](#huc10); [Standen, Santoro and Arriaza, 2004](#sta04)). However, during the second half of the last century, political-administrative transformations, technological changes, government credits, high demand and an increased commercial value of certain species have led to the over-exploitation of marine resources, particularly during the 1980s ([Castilla, 1994](#cas94); [Castilla and Fernández, 2005](#cas05); [Meltzoff, Lichtensztajn and Stotz, 2002](#mel02)). Over-exploitation has especially affected the Loco shellfish (_Concholepas concholepas_), known as Chilean abalone, whose catch rate quadrupled between the 1970s and 1980s, forcing the closure of Chilean abalone fisheries in the late 1980s ([Stotz, 1997](#sto97)).

With a dramatic increase in marine landing fees and in response to a lack of central regulation at the time, several experimental ecology studies were performed during the late 1980s ([Castilla, 1986](#cas86), [2000](#cas00)), leading to the subsequent enactment of the Fisheries and Aquaculture Law (,a href="#chi91">1991). This law, along with Area Management Regulations No. 355 ([1995](#chi95) and No. 314 ([2004](#chi04b) and decrees of modifications supreme decree No. 572 ([2000](#chi00), No. 320 ([2001](#chi01) and No. 253 ([2004](#chi04a)), introduced the Management and Exploitation Areas for Benthic Resources (MEABRs) implemented through fishery organizations. MEABRs are administrative structures that promote the rational use of benthic resources by small-scale fishermen and are defined (by the state) as co-management initiatives based on territorial users' rights for fishery ([Gelcich, Godoy and Castilla, 2009](#gel09)).

Although over fifty benthic species are managed, Loco is considered the most critical species because of its importance to the small-scale fishery economy ([González _et al._, 2006](#gon06)). MEABRs serve as administrative bodies that manage marine space ceded to small-scale fishing organizations (unions, federations, trade associations) through the SUBPESCA Undersecretary of Fisheries together with other interested parties, including the scientific community. Although the scientific literature originally referred to MEABRs as triumvirate initiatives managed by a State-Community-Scientist axis ([Schumann, 2007](#sch07)), new research has indicated that they represent dynamic instances of collaborative and conflictive interaction between stakeholder groups (e.g., [Marín and Berkes, 2010](#mar10)) identified seven stakeholder groups involved in Chilean central coast research). Formal requests for MEARBs and scientific assessments established by the Fisheries and Aquaculture Law began in 1997\. Although scientific publications produced by MEABRs have not grown numerous, recent trends suggest that they are increasing. This specialized literature identifies three dimensions: environment, economy and social organization ([Castilla and Defeo, 2001](#cas01); Gelcich, [2005a](#gel05a), [2005b](#gel05b), [2008](#gel08), [2009](#gel09); [Zúñiga, Ramírez and Valdebenito, 2008](#zun08), [2010](#zun10)), and our inquiry addresses the current absence of reflexive articles examining the scientific deployment of MEARBs.

## Materials and methods

Given the nature of our theoretical-conceptual approach, the materials and methods used have been slightly altered (by the authors) from traditional bibliometric and statistical tools of the social studies of science and technology. This modification was made mainly due to trends of limited MEABR scientific production. The small sample that we collected allowed us to explore a more qualitative and profound version of the social studies of science and technology.

A bibliographic review of data recollection and analysis in the social studies of science and technology suggests that the user meta model proposed by Börner, Chen, and Boyack ([2003](#bor03)) and applied by Janssen, Schoon, Ke, and Börner ([2006](#jan06)) and Janssen ([2007](#jan07)) is suitable for our purposes. This methodological framework consists of a sequence of techniques that have been refined over time and that rely on researcher criteria for sample construction and meta-information inclusion (authors, depicting terms, and keywords, among others). The user meta model applies the following steps:

1.  Information extraction
2.  Analysis unit definition
3.  Measurement selection
4.  Calculation of similarity or unit proximity
5.  Ordering
6.  Visualization for analysis and interpretation

This methodological framework was selected because it accommodates relational analyses (according to certain social network analysis procedures) that apply a more qualitative approach through the use of various measures, such as consensual article attributes and article authors. These steps were performed through our investigation as follows.

First, research was conducted through the Thomson Reuters’ Web of Science databases and SCIELO (Scientific Electronic Library Online). This approach is justified through our definition of step 2.

Regarding analysis units, we applied a social network analysis of co-authorship and citation based on our objective to study forms of scientific production and knowledge transfer. Our analysis units include researchers (authors), research articles (scientific papers published in journals indexed in Web of Sciene or SCIELO; we considered articles and papers as synonyms), and their relationships. Once these analysis units were established, samples of articles and authors were collected through an online search using keywords (e.g., MEABR, Chilean fisheries co-management, and small-scale Chilean fisheries, among others) and through a crosscheck of authors and articles in all bibliographic references; results yielded N=30 published scientific papers with N=33 scientific authors. The time period covers from the earliest scientific publication of experimental biology which set the ground for a Fisheries and Aquaculture Law (i.e., 1986), to the last publication identified at the beginning of this research (i.e. 2012).

For step 3, a deeper review was conducted to establish the foundations of our methodological modifications to classic analyses of co-authorship and citation. Following user meta model principles, a categorization strategy was designed for both authors and articles, and the _expert choice_ technique was applied to assess multiple variables. During this phase, categories and subcategories were employed to qualitatively characterize authors and articles based on the establishment of an interdisciplinary panel of seven experts (geographers, anthropologists and biologists). As shown in Table 1, a statistical distribution of authors and papers was created based on these categories, and sample attributes were classified and defined based on these categories and subcategories. We also conducted a bibliometric for co-authorship and citation networks’ analysis. According to Derek de Solla Price's foundational article ([1970](#pri70)), an article is cited when it is listed the bibliographic references of another article, and citation analysis therefore involves the study of citation practices in scientific literature. Citation studies require the application of complex methods. In 1998, Leydesdorff ([1998](#ley98)) questioned whether citations should be treated as _explanans_ (as a means of explaining something) or as _explanandums_ (as a subject to be explained). If we focus on the explanandum dimension, Leydesdorff argued that a citation involves the existence of a binomium quoted article:quoting article and, therefore, encompasses a dynamic and relational operation that occupies a position within the multidimensional space formed by other citations. Citations are highly complex as a form of author communication, allowing the possibility to address the social and cognitive contexts of their production (although it is not sufficient to simply provide an account of scientific knowledge production, as the currently unresolved debate demonstrates).

White, Wellman, and Nazer ([2004](#whi04)) identified three citation analysis hypotheses: i) the social network hypothesis, which explains the citation of a group or of an invisible college through interpersonal relationships between publication authors; ii) the intellectual network hypothesis,’ which proposes that citations are better defined as belonging to disciplines and related research topics rather than to interpersonal ties; iii) and the socio-cognitive network hypothesis (employed in this paper), which relies on the interdependency of both dimensions (social relationships and disciplinary and thematic affinities) and thus assumes the necessity of techniques that address them together.

As for the definition and treatment of data, debates have focused on differences that, according to authors such as Cronin (1984), have formed a dichotomy between a ‘microsociological’ approach that advocates not concentrating exclusively on citations but also on their contexts of production and a ‘scientometric’ approach, which develops instruments for measuring citations. In our case, a citation analysis was conducted based on changes in the distribution of attributes over time using categories and subcategories. We thus attempted to uphold part of Moravcsik and Murugesan’s ([1975](#mor75)) proposal, although in our case, attributes are intended to reflect the contexts in which the citations are made. Finally, we employed Finney's classificatory strategy ([1979](#fin79)) for our social network analysis. The data collection procedure was carried out through a review of literature references for each paper identified, classifying references through a paper-by-paper asymmetric square matrix of binary values in which 1 represented a citation and 0 represented no citation (an X matrix where xij > 0 if article _i_ cited article _j_ and xij = 0 if _i_ did not cite _j_). In turn, by reading the matrix vertically, it is possible to identify the articles cited in each item. Co-authorship analyses offer insight into the structures of research communities by (partially) revealing trends of collaboration between various actors.

According to De Stefano and collaborators,

> Scientific collaboration is a mix of informal mechanisms (e.g., advice, face-to-face contacts, exchange of personal knowledge), and formal activities (e.g., writing papers, participating in research projects) among scientists involved in producing knowledge… ([De Stefano, Fuccella, Vitale and Zaccarin, 2013](#des13): p. 371).

We thus did not propose conducting a traditional co-authorship analysis, i.e., exploring the ‘preferential attachment hypothesis' and the presence of free-scale networks through scientific collaboration ([Barabási _et al._, 2002](#bar02)), examining _small-world_ network configurations ([Watts and Strogatz, 1998](#wat98)) or applying the G-index ([Abbasi, Altmann and Hossain, 2011](#abb11); [Egghe, 2006](#egg06)). Instead, we supplement the collaboration network with attributive analyses via classification strategy and with the citation network through the ‘threshold paper’ concept, which is defined below. For data collection purposes, we examined articles directly. Co-authoring information was extracted directly from the articles and then displayed using a symmetrical author-by-author matrix of numeral values in which 0 denotes no collaboration and _i_=1, 2, 3…m for m collaborations (an X matrix where xab > 0 if author _a_ published im times with author _b_ and where xab = 0 if the authors did not publish together). This matrix design is based on author intentions to precisely visualize collaborations for each pair of investigators. This information was combined with attributes selected by the authors (rank and nationality of the researcher), illustrating the formation of sub-groups within the scientific collaboration network via relational and attributive analyses. Given the small sample examined, co-authorship issues associated with name homonyms were negligible ([Kang _et al._, 2009](#kan09)).

<table class="center"><caption>  
Table 1: Categories and subcategories of paper’s classification.</caption>

<tbody>

<tr>

<th>Categories</th>

<th>Sub-categories</th>

<th>No. of papers</th>

</tr>

<tr>

<th colspan="3">By papers</th>

</tr>

<tr>

<td rowspan="3">Knowledge production</td>

<td>Monodisciplinary</td>

<td>16</td>

</tr>

<tr>

<td>Multidisciplinary</td>

<td>11</td>

</tr>

<tr>

<td>Interdisciplinary</td>

<td>3</td>

</tr>

<tr>

<td rowspan="3">Methodological approach</td>

<td>Qualitative</td>

<td>6</td>

</tr>

<tr>

<td>Quantitative</td>

<td>9</td>

</tr>

<tr>

<td>Mixed</td>

<td>15</td>

</tr>

<tr>

<td rowspan="2">Target group</td>

<td>Academic</td>

<td>11</td>

</tr>

<tr>

<td>Public Policy</td>

<td>19</td>

</tr>

<tr>

<td rowspan="2">Local authors</td>

<td>No foreign presence</td>

<td>18</td>

</tr>

<tr>

<td>With foreign presence</td>

<td>12</td>

</tr>

<tr>

<th colspan="3">By authors</th>

</tr>

<tr>

<td rowspan="3">Degree of specialization</td>

<td>Social science</td>

<td>4</td>

</tr>

<tr>

<td>Natural science</td>

<td>22</td>

</tr>

<tr>

<td>Economic science</td>

<td>7</td>

</tr>

<tr>

<td rowspan="2">Nationality of the researcher</td>

<td>Chilean</td>

<td>23</td>

</tr>

<tr>

<td>Foreign</td>

<td>10</td>

</tr>

</tbody>

</table>

In step 4, centrality measures of social network analysis were calculated. These included degree (in-degree and out-degree for the asymmetrical citation network), betweenness and density ([Freeman, 1977](#fre77), [1978](#fre78); [Marsden, 2002](#mar02); [Scott, 2000](#sco00)). In social network analysis, degree centrality corresponds to the number of ties of a node and ‘_reflects the node’s position and role in terms of popularity and activity of the node..._’ ([Abbasi _et al,_ 2012](#abb12), p. 406). Thus, in our co-authorship network a node’s degree represents the number of collaborations of an author (scholar), and in a citation network represents the number of times an article has been cited by others. Adapting Abbasi, Hossain and Leydesdorff’s equations for this measure ([Abbasi _et al.,_ 2012](#abb12), p. 406) degree (CD) of a node k is defined by:

<figure class="centre">![equation 1](p679eqA1.png)</figure>

Where n is the number of nodes of the network and a(p<sub>i</sub>, p<sub>k</sub>)=1 only if I and k are linked. Otherwise a(p<sub>i</sub>, p<sub>k</sub>) =0.

Another centrality measure is _betweenness_, which can be understood as,

> the portion of the number of shortest paths (between all pairs of nodes) that pass through the given node divided by the number of shortest path between any pair of nodes (regardless of passing through the given node... ([Abbasi _et al._, 2012](#abb12), p. 407)

and represents a node’s function as broker or gatekeeper to connect other nodes and sub-groups. In our co-authorship network betweenness represents an author’s capacity of connecting other authors and sub-groups of authors, and in the citation network represent the capability of a paper to connect via citation two different papers. Betweenness CB of node k can be synthesised as:

<figure class="centre">![equation 2](p679eqA2.png)</figure>

Where g<sub>ij</sub> are the shortest paths connecting p<sub>i</sub> and p<sub>j</sub>, and g<sub>ij</sub>(p<sub>k</sub>) is the shortest path connecting p<sub>i</sub> and p<sub>j</sub> that contains p<sub>k</sub> ([Abbasi _et al._, 2012](#abb12), p. 407).

In step 5, data are presented as sociograms expressed using UCINET and NETDRAW software. For data interpretation purposes, we used the following guiding questions:

**For co-authorship**: Considering the author attributes and relational data, which collaboration structures can be identified in MEABR scientific production? Do collaboration structures respond to specialty grades and/or researcher nationalities?

**For citation**: Based on the defined categories, how have relationships between scientific articles changed? Which articles have transformed MEABR scientific production and knowledge transfer and how? These questions raise the ‘threshold article’ concept, which defines a publication as including three main characteristics: i) significance by introducing modifications to scientific production (e.g., a transformation in knowledge production or methodology); ii) significance through relational features that exhibit a high degree of citation; iii) the inclusion of a node with a significant betweenness measure. By merging these three characteristics, it may be possible to identify the transformation of scientific works through MEABRs, revealing substantial temporal trends and transformations.

**For co-authorship and citation**: How are threshold articles and collaboration structures linked? Does a subgroup of collaboration dominate scientific production or do transformations originate from various subsets of collaboration?

## Results: forms of scientific production and transfer using MEARB

We now present the results of the co-authorship and citation analysis. Both analyses were conducted based on attributes selected for each case.

### Co-authorship network

<figure class="centre">![](p679fig1.png)

<figcaption>Figure 1: Co-authorship network. Attributes: specialty degree (circle: biological science; square: social science; triangle: economic science) and nationality of the researcher (white: Chilean; black: foreign).</figcaption>

</figure>

<table class="center" style="width:50%;"><caption>  
Table 2: Author clusters.</caption>

<tbody>

<tr>

<th>Frequency</th>

<th>Members</th>

</tr>

<tr>

<td style="text-align:center;">13</td>

<td>Castilla; Geaghan; Payne; Fernandez; Defeo; Gelcich; Edward-Jones; Kaiser; Watson; Godoy; Prado; Marin; Berkes</td>

</tr>

<tr>

<td style="text-align:center;">10</td>

<td>Stotz; Perez; Meeltzoft; Gibran; Gonzalez; Garrido; Orensanz; Parma; Tapia; Zulueta</td>

</tr>

<tr>

<td style="text-align:center;">2</td>

<td>Arias; Iglesias</td>

</tr>

<tr>

<td style="text-align:center;">2</td>

<td>Sobenes; Chavez</td>

</tr>

<tr>

<td style="text-align:center;">3</td>

<td>Zuniga; Ramirez; Valdebenito</td>

</tr>

<tr>

<td style="text-align:center;">1</td>

<td>Schumman</td>

</tr>

<tr>

<td style="text-align:center;">1</td>

<td>Gonzalez, E.</td>

</tr>

<tr>

<td style="text-align:center;">1</td>

<td>Galvez</td>

</tr>

</tbody>

</table>

Figure 1 reveals certain dynamics of co-authorship and collaboration. First, a clustering tendency into two central _scientific schools_ is shown below. These scientific clusters are mainly composed of biological science researchers, with social scientists represented to a lesser degree. Collaboration trends also reveal a focus on dominant figures or authors who exhibit high degrees of centrality (Castilla: 18; Stotz: 9\. See Table 3) and betweenness (Castilla: Betweenness 45 and nBetweenness 8.871; Stotz: Betweenness 23 and nBetweenness 4.536\. See Table 3). A lack of collaboration between these schools and economic scientists is also found, revealing the isolation of this discipline within MEABR scientific production and constraints to knowledge transfer. A disconnect is also identified within the field of economic scientific production, dividing this field into varies subfields. Hence, while the social sciences remain peripheral, they are at least integrated within scientific collaborations. The economic sciences are instead excluded from interdisciplinary collaborative efforts.

### Citation networks

To examine the citation network and its co-authorship patterns, we employed the _threshold article_ concept. As noted above, threshold articles are special publications that i) exhibit high centrality; ii) present significant betweenness centrality; and iii) have introduced changes in attribute tendencies. Starting with social network analysis measures, articles that combine high degree and betweenness measures are deemed _d, h, j, l_ and _v_. The following sections describe the attribute tendencies of the sampled papers with a focus on articles that exhibit changes or transformations. The following sociograms are arranged based on X-axis time criteria (starting from the left with articles _a_ and _b_ published in 1986 and ending with _ad_ articles published in 2012).

<figure class="centre">![citation network](p679fig2.png)

<figcaption>Figure 2: Citation network. Attributes: knowledge production (circle: mono-disciplinary; square: multidisciplinary; triangle: interdisciplinary) and author nationality (white: includes foreign authors; black: only Chilean authors).</figcaption>

</figure>

Figure 2 reveals a tendency towards disciplinary composition transformation, beginning with a period dominated by monodisciplinary practice that slowly changes with the publication of article _l_. Although _e_ was the first multidisciplinary article, it does not feature high degree or betweenness measures. Thus, _e_ does not appear to be important in this case. Within the last decade, article _k_ constitutes the first interdisciplinary research effort. However, _k_ shows a low degree of centrality (inDegree: 0\. See Table 3) and is therefore not a threshold article. Other articles with threshold potentiality do not show signs of significance or knowledge production attribute changes. An examination of _knowledge production_ and _hometown authors_ suggests the presence of other relevant information, as the mono-disciplinary articles evaluated have not involved foreign researchers (with the exception of _s_ and _aa_). Rather, we can assume that until the publication of article _l_, Chilean scientific production was not able to generate multidisciplinary research, a trend that shifted with the valuable contributions of foreign scientists.

<figure class="centre">![citation networks](p679fig3.png)

<figcaption>Figure 3: Citation network. Attributes: knowledge production (circle: monodisciplinary; square: multidisciplinary; triangle: interdisciplinary)  
and methodological approach (white: qualitative; blue: quantitative; black: mixed).</figcaption>

</figure>

Article _d_ was the first to employ a mixed methodology and features a high degree of centrality. Article _j_ also exhibits a high degree of centrality and strengthens qualitative methodologies. Again, this article is distinct from the other threshold papers.

<figure class="centre">![Citation network](p679fig4.png)

<figcaption>Figure 4: Citation network. Attributes: knowledge production (circle: mono-disciplinary; square: multidisciplinary; triangle: interdisciplinary) and target group (white: academy; black: public policy).</figcaption>

</figure>

Figure 4 shows that articles _d_ and _h_ both feature a high degree of centrality and mono-disciplinary knowledge production (with _academy_ and _public policy_ as the target populations, respectively). Moreover, since the publication of article _l_, multi- and inter-disciplinary research has grown more prominent. Multi- and inter-disciplinary articles published since the publication of article _l_ have mostly focused on public policy issues. Hence, multidisciplinary knowledge production may be directly related to public policy.

<figure class="centre">![citation network](p679fig5.png)

<figcaption>Figure 5: Citation network. Attributes: methodological approach (circle: qualitative; square: quantitative; triangle: mixed)  
and target group (white: academy; black: public policy).</figcaption>

</figure>

Figure 5 lists articles that employ qualitative methodologies with the exception of article _j_, which is academically oriented. Therefore, _j_, is of relevance because it represents the only qualitative article oriented towards public policy.

## Discussion

The figures shown highlight several MEABR scientific production and knowledge transfer trends. Referring back to our initial research questions, what would happen if we were to combine threshold articles with scientific schools? According to our definition, a threshold article features the three conditions listed above, and only three articles fulfil these conditions: _l_, , which belongs to what we can call the _Stotz’s school_, and _d_ and _j_, which belong to the _Castilla school_. However, the transformation of MEABR science may have involved disciplinary and methodological shifts: a movement away from a mono-disciplinary and quantitative model to a multidisciplinary science that is strengthened by mixed methodologies and that allows for the emergence of a qualitative methodological model. This trend highlights a main characteristic of scientific production: the division of MEABR science into two differentiated periods. The first period (from article _a_, to article _j_, or from 1986 to 2000) was primarily mono-disciplinary, was dominated by biological science and involved both public policy and academic target groups. This period ended with the publication of article _j_, after which qualitative methodologies started to appear, inaugurating the second period. By 2001, with the publication of articles _k_ and _l_ this second period was in full operation, introducing multi- and interdisciplinary approaches. During this second period, scientific production was oriented mainly toward public policy and was marked by foreign contributions. These trends suggest that with the exception of _j_ every article employed qualitative methodologies and was intended for academic rather than public policy audiences, providing data to hypothesize a weak relationship between public policy and qualitative methods, albeit this topic require intense inquiry efforts that exceed the limitations of the current work.

Our examination of threshold articles and scientific schools demonstrates that both scientific clusters caused transformations in scientific production. This finding, however, does not imply that each school is an exclusive owner of the attributes that were introduced, as both have exhibited the ability to adopt other contributions. Clear examples include the Stotz transformations introduced in _l_ that were later adopted by Castilla’s group of collaborators (Figures 2, 3, 4 and 5). A key question arises here: how then are threshold papers able to refer to structural properties and also provide insights for a self-organizing model to interpret scientific knowledge production and transfer? The answer lies precisely in its capability to enable identification of trends in both co-authorship networks and citation networks by detecting holes or breaks in trends of both networks. In other words, in its capability of intertwine two ways of self-organizing networks that determine scientific information production and transfer, and doing it by using structural properties of nodes but also revealing data of how different domains of scientific information are formed and interact. Here domain can be interpreted (or read in the sociograms) as the multidimensional spaces of similar papers over the years, i.e., the existence of researches with common properties and combination of these properties, mainly _knowledge production_ and _methodological approach_.

## Final considerations

In closing, we wish to highlight the two main ideas presented here. The first corresponds to the research problem that we address: MEABR scientific production. We identified a mono-disciplinary bias in scientific deployment during initial stages. This bias helped to establish further questioning on scientific deployment given that interdisciplinary practices remain novel and experimental and have not yet been institutionalized to address complex problems. Employing notions of Maturana regarding the structural limits of scientific knowledge, we demonstrated that knowledge fields are restricted to specific topics. In this case, MEABR scientific production involves a modus operandi that is not yet consistent with development in other areas of knowledge production. The hypothesis that an interdisciplinary approach currently addresses social and environmental issues faced by Chilean MEABRs can be questioned by taking into account the continuity of mono-disciplinary practices and quantitative methodologies throughout the time period, despite an apparent shift begun at 2001\. To state it simpler, an interdisciplinary approach is still a path to walk through rather than an institutionalized scientific practice, although some incipient trajectory cannot be denied.

The second idea is related to methodological strategies. We believe that the value of the presented methodological model lies in the possibility of combining social network analyses of small samples with qualitative techniques applied to science and technology studies. This model highlights the benefits of combined qualitative and quantitative techniques for addressing complex scientific problems. We hope that this case study illustrates possible pathways for their articulation while also highlighting inconsistencies that emerge from their application.

## Acknowledgements

This work was supported through the MEL-CONICYT Project N° 81100002: ‘Environmental-Economic Value of Small-Scale Fishing Activities. Strategies for Productive Diversification in Southern Chilean Coastal Societies’. We thank the Chilean National Commission of Scientific Research and Technology (CONICYT) for its support.

## About the authors

**Carlos Hidalgo** is an Associate Researcher of the ATLAS research programme, Department of Social Sciences, Universidad de los Lagos, Chile. He received his Bachelor’s degree in anthropology from Universidad Católica de Temuco and his Master’s degree in social sciences by Universidad de los Lagos. He can be contacted at [chidalgarrido@gmail.com](mailto:chidalgarrido@gmail.com)  
**Francisco Ther** is a Titular Professor of Universidad de Los Lagos, Osorno, Chile. He received his Bachelor’s degree of anthropology from Universidad Austral de Chile, his Master’s degree in social anthropology and PhD. in anthropology from the Universidad Nacional Autónoma de México. He can be contacted at [fther@ulagos.cl](mailto:fther@ulagos.cl)  
**Asunción Díaz** is an Associate Researcher of the ATLAS research programme, Department of Social Sciences, Universidad de los Lagos, Chile. She received her Bachelor’s degree of anthropology and Master’s Degree in systemic analysis applied to society from Universidad de Chile, and she is currently finishing her PhD. in human sciences in Universidad Austral de Chile. She can be contacted at [asudiaz@gmail.com](mailto:asudiaz@gmail.com)

#### References

*   Abbasi, A., Altmann, J. & Hossain, L. (2011). Identifying the effects of co-authorship networks on the performance of scholars: a correlation and regression analysis of performance measures and social network analysis measures. _Journal of Informetrics, 5_, 594–607.
*   Abbasi, A., Hossain, L., Leydesdorff, L. (2012). Betweenness centrality as a driver of preferential attachment in the evolution of research collaboration network. _Journal of Informetrics, 6_(3), 403-412.
*   Barabási, A. L., Jeong, H., Néda, Z., Ravasz, E., Schubert, A. & Vicsek, T. (2002). Evolution of the social network of scientific collaborations. _Physica A: Statistical Mechanics and Its Applications, 311_, 590-614.
*   Barnes, B., Bloor, D. & Henry, J. (1996). _Scientific knowledge: a sociological analysis._ London: A & C Black..
*   Börner, K., Chen, C. & Boyack, K. (2003). Visualizing knowledge domains. _Annual Review of Information Science & Technology, 37_, 179–255.
*   Bugliarello, G. (1988). STS perspectives: The science-technology-society matrix. _Bulletin of Science, Technology & Society, 8_(2), 125-126.
*   Castilla, J. C. (1986). ¿Sigue existiendo la necesidad de establecer parques y reservas marinas en Chile? [Is it still necessary to establish parks and marine reserves in Chile?] _Ambiente y Desarrollo, 2_, 53-63.
*   Castilla, J. C. (1994). The Chilean small scale benthic shellfisheries and the institutionalization of new management practices. _Ecological International Bulletin, 21_, 47–63.
*   Castilla, J. C. (2000). Roles of experimental marine ecology in coastal management and conservation. _Journal of Experimental Marine Biology and Ecology, 250_(1), 3-21.
*   Castilla, J. C. & Defeo, O. (2001). Latin American benthic shellfisheries: Emphasis on co-management and experimental practices. _Reviews in Fish Biology and Fisheries, 11_, 1-30.
*   Castilla, J. C. & Fernández, M. (2005). Marine conservation in Chile: historical perspective, lessons, and challenges. _Conservation Biology, 19_(6), 1681–2065.
*   Chile. _Supreme decrees_. (2004). _[Modifica decreto supremo No. 247 de 2000](http://www.leychile.cl/Navegar?idNorma=223524)._ [Exempt Decree N°253, amending Decree N°247/200]. (Decreto supremo 2004 No. 253) Retrieved from http://www.leychile.cl/Navegar?idNorma=223524
*   Chile. _Supreme decrees_. (2004). [Reglamento de actividades de acuicultura en áreas de manejo](http://www.subpesca.cl/normativa/605/w3-article-7779.html) [Regulation of aquaculture operations in management areas.] (Decreto supremo 2004 No. 314). Retrieved from http://www.subpesca.cl/normativa/605/w3-article-7779.html
*   Chile. _Supreme decrees_. (2001). _[Reglamento ambiental para la acuicultura](http://www.subpesca.cl/normativa/605/w3-article-4632.html)_. [Environmental Regulations for Aquaculture]. (Decreto supremo 2001 No. 329) Retrieved from http://www.subpesca.cl/normativa/605/w3-article-4632.html
*   Chile. _Supreme decrees_ (1995). _[Reglamento sobre áreas de manejo y explotación de recursos bentonicos](http://www.leychile.cl/Navegar?idNorma=12627&buscar=Decreto+Supremo+355)_. [Regulation on the areas of management and exploitation of benthic resources.] Retrieved from http://www.leychile.cl/Navegar?idNorma=12627&buscar=Decreto+Supremo+355
*   Chile. _Statutes_ (1991). [_Ley general de pesca y acuicultura_](http://www.leychile.cl/Navegar?idNorma=30265&idVersion=1991-09-06). [General law of fisheries and aquaculture]. Retrieved from http://www.leychile.cl/Navegar?idNorma=30265&idVersion=1991-09-06
*   Cronin, B. (1984). _The citation process. The role and significance of citations in scientific communication._ London: Taylor Graham.
*   De Stefano, D., Fuccella, V., Vitale, M. P. & Zaccarin, S. (2013). The use of different data sources in the analysis of co-authorship networks and scientific performance. _Social Networks, 35_, 370-381.
*   Egghe, L. (2006). Theory and practise of the g-index. _Scientometrics, 69_(1), 131–152.
*   Escobar, A. (1999). _El final del salvaje: naturaleza, cultura y política en la antropología contemporánea_. [The end of the savage: nature, culture and politics in contemporary anthropology]. Bogotá: Instituto Colombiano de Antropología.
*   Evans, L., Cherrett, N. & Pemsl, D. (2011). Assessing the impact of fisheries co-management interventions in developing countries: a meta-analysis. _Journal of Environmental Management, 92_(8), 1938-1949.
*   Finney, B. (1979). _The reference characteristics of scientific texts_. Unpublished doctoral dissertation, City University, London, UK
*   Freeman, L. C. (1977). A set of measures of centrality based on betweenness. _Sociometry, 40_(1), 35-41.
*   Freeman, L. C. (1978). Centrality in social networks: conceptual clarification. _Social Networks, 1_(3), 215-239.
*   García, E., González, J., López, J., Luján, J., Gordillo, M., Osorio, C. & Valdés, C. (2001). _Ciencia, tecnología y sociedad: una aproximación conceptual_. [Science, technology and society: a conceptual approximation]. Madrid: Organización de Estados Iberoamericanos para la Educación, la Ciencia y la Cultura.
*   Gelcich, S., Edward-Jones, E., Kaiser, M. & Watson, E. (2005a). Using discourses for policy evaluation: the case of marine common property rights in Chile, society & natural resources. _Anais: An International Journal, 18_(4), 377-391.
*   Gelcich, S., Edwards-Jones, G. & Kaiser, M. J. (2005b). Importance of attitudinal differences among artisanal fishers toward co-management and conservation of marine resources. _Conservation Biology, 19_(3), 865-875.
*   Gelcich, S., Godoy, N. & Castilla, J. C. (2009). Artisanal fishers’ perceptions regarding coastal co-management policies in Chile and their potentials to scale-up marine biodiversity conservation. _Ocean & Coastal Management, 52_(8), 424–432.
*   Gelcich, S., Kaiser, M. J., Castilla, J. C. & Edwards-Jones, G. (2008). Engagement in co-management of marine benthic resources influences environmental perceptions of artisanal fishers. _Environmental Conservation, 35_(1), 36–45.
*   González, J., Stotz, W., Garrido, J., Orensanz, J., Parma, A., Tapia, C. & Zulueta, A. (2006). The Chilean TURF system: How is it performing in the case of the loco fishery? _Bulletin of Marine Science, 78_(3), 499-527.
*   Hanneman, R. & Riddle, M. (2005). _Introduction to social network methods_. Riverside, CA: University of California, Riverside.
*   Hucke-Gaete, R. Álvarez, R., Navarro, M., Ruiz, J., Lo Moro, J. & Farías, A. (2010). _Investigación para desarrollo de área marina costera protegida Chiloé-Palena-Guaitecas_. [Research for the development of the marine protected area of Chiloé-Palena-Guaitecas]. Valdivia, Chile: Universidad Austral de Chile.
*   Janssen, M. A. (2007). [An update on the scholarly networks on resilience, vulnerability, and adaptation within the human dimensions of global environmental change.](http://www.webcitation.org/6adLLHB1F) _Ecology and Society, 12_(2), 9 Retrieved from http://www.ecologyandsociety.org/vol12/iss2/art9/. (Archived by WebCite® at http://www.webcitation.org/6adLLHB1F)
*   Janssen, M. A., Schoon, M. L., Ke, W. & Börner, K. (2006). Scholarly networks on resilience, vulnerability and adaptation within the human dimensions of global environmental change. _Global Environmental Change, 16_(3), 240-252.
*   Kang, I. S., Na, S. H., Lee, S., Jung, H., Kim, P., Sung, W. K. & Lee, J. H. (2009). On co-authorship for author disambiguation. _Information Processing & Management, 45_(1), 84–97.
*   Knorr-cetina, K. & Mulkay, M. (1983). Introduction. In K. Knorr-Cetina & M. Mulkay (Eds.), _Science observed: perspectives on the social study of science_ (pp. 1-18). London: Sage Publications.
*   Kuhn, T. (1962). _The structure of scientific revolutions_. Chicago, IL: University of Chicago Press.
*   Latour, B. (1987). _Science in action: how to follow scientists and engineers through society_. Cambridge, MA: Harvard University Press.
*   Latour, B. (1993). _We have never been modern_. Cambridge, MA: Harvard University Press.
*   Latour, B. (2005). _Reassembling the social: an introduction to actor-network-theory_. Oxford, UK: Oxford University Press.
*   Latour, B. & Woolgar, S. (1979). _Laboratory life: the social construction of scientific facts_. Los Angeles, CA: Sage Publications.
*   Lewis, A. O. (1990). STS historical perspectives. _Bulletin of Science, Technology, and Society, 10_(5-6), 254-258.
*   Leydesdorff, L. (1998). Theories of citation? _Scientometrics, 43_(1), 5-25.
*   Luhmann, N. (1996). _La Ciencia de la sociedad_. [The science of society.] México: Universidad Iberoamericana, Anthropos ITESO.
*   Marín, A. & Berkes, F. (2010). Network approach for understanding small-scale fisheries governance: the case of the Chilean coastal co-management system. _Marine Policy, 34_(5), 851–858.
*   Marsden, P. V. (2002). Egocentric and sociocentric measures of network centrality. _Social Networks, 24_(4), 407-422.
*   Martínez, F. (2004). [El movimiento de estudios ciencia- tecnología- sociedad: su origen y tradiciones fundamentales.](http://scielo.sld.cu/scielo.php?pid=S1727-81202004000100002&script=sci_arttext) [The society-technology-science studies movement: its origin and fundamental traditions.] _Revista de Humanidades Médicas, 4_(1). Retrieved from http://scielo.sld.cu/scielo.php?pid=S1727-81202004000100002&script=sci_arttext.
*   Maturana, H. (1995). _La realidad objetiva o construida._ [Objective or constructed reality.] México: Universidad Iberoamericana.
*   Meltzoff, S. K., Lichtensztajn, Y. G. & Stotz, W. (2002). Competing visions for marine tenure and co-management: genesis of a marine management area system in Chile. _Coastal Management, 30_(1), 85-99.
*   Moravcsik, M.J. & Murugesan, P. (1975). Some results on the function and quality of citations. _Social Studies of Science, 5_(1), 86-92
*   Otte, E. & Rousseau, R. (2002). Social network analysis: A powerful strategy, also for the information sciences. _Journal of Information Science, 18_(6), 441-453.
*   Price, D. de S. (1970). Citation measures of hard science, soft science, technology, and nonscience. In C. E. Nelson & D. K. Pollack (Eds.), _Communication among scientists and engineers_ (pp. 3-22). Lexington, MA: Heath.
*   Salas, S., Chuenpagdee, R., Seijo, J. C. & Charles, A. (2007). Challenges in the assessment and management of small-scale fisheries in Latin America and the Caribbean. _Fisheries Research, 87_(1), 5–16.
*   Schumann, S. (2007). Co-management and "consciousness": fishers’ assimilation of management principles in Chile. _Marine Policy, 31_(2), 101–111.
*   Scott, J. (2000). _Social network analysis: a handbook_ (2nd ed.). London, UK: Sage Publications.
*   Standen, V., Santoro, C. & Arriaza, B. (2004). Síntesis y propuestas para el periodo arcaico en la Costa del extremo norte de Chile. [Synthesis and proposals for the archaic period in the Chilean northern coast.] _Chungará_, No. 36, 201-212.
*   Stotz, W. (1997). Las áreas de manejo en la ley de pesca y acuicultura: primeras experiencias de evaluación de la utilidad de esta herramienta para el recurso loco. [Management areas in the law on fisheries and aquaculture: first experiences of evaluation about its usefulness for loco (Chilean abalone) resources.] _Estudios Oceanológicos_, No. 16, 67-86.
*   Thirunarayanan, M. O. (Ed.). (1992). _Handbook of science, technology and society. Volume 1: A theoretical and conceptual overview of science, technology and society education._ Tempe, AZ: Arizona State University.
*   Varela, F. (2005). El círculo creativo. Esbozo histórico natural de la reflexividad. [Natural historical sketch of reflexivity] In Paul Watzlawick, P. (Eds.), La realidad inventada (pp. 251-263). Barcelona, Spain: Editorial Gedisa.
*   Watts, D. J. & Strogatz, S. H. (1998). Collective dynamics of ‘small-world’ networks. _Nature, 393_(6684), 440-442.
*   White, H. D., Wellman, B. & Nazer, N. (2004). Does citation reflect social structure? Longitudinal evidence from the “Globenet” Interdisciplinary Research Group. _Journal of the American Society of Information Science and Technology, 55_(2), 111-126.
*   Woolgar, S. (1981). Interests and explanation in the social study of science. _Social Studies of Science, 11_(3), 365-394.
*   Zúñiga, S., Ramírez, P. & Valdebenito, M. (2008). Situación socioeconómica de las áreas de manejo en la región de Coquimbo, Chile. [Socioeconomic situation of management areas in the region of Coquimbo, Chile.] _Latin American Journal of Aquatic Research, 36_(1), 63-81.
*   Zúñiga, S., Ramírez, P. & Valdebenito, M. (2010). Medición de los impactos socio económicos de las Áreas de Manejo en las comunidades de Pescadores del norte de Chile. [Measurement ofthe socio-economic impact of management areas in the fishing communities of northern Chile.] _Latin American Journal of Aquatic Research, 38_(1), 15-26.