#### vol. 20 no. 3, September, 2015

# Factors, frameworks and theory: a review of the information systems literature on success factors in project management

#### [Robert Irvine](#author) and [Hazel Hall](#author)  
Centre for Social Informatics, Edinburgh Napier University, 10 Colinton Road, Edinburgh, City of Edinburgh, EH10 5DT

#### Abstract

> **Introduction.** We provide a critical evaluation of the literature on success factors in information systems projects, with a particular focus on organizational information systems development. This responds to recent editorial comment on the need for literature reviews that can furnish a foundation for theory building and research landscaping.  
> **Method.** Relevant material was found in the core fields of information management, information systems and project management. Additional material from domains such as business management and software development were also identified.  
> **Analysis.** Four broad research themes emerged from the analysis of the literature: (1) the identification and exploration of project success factors and success factor lists; (2) contributions of individual/group project success factors to project success (or failure); (3) causal interactions between individual/groups of project success factors and simulations of these; and (4) project success factor frameworks.  
> **Results.** A high number of unique project success factors exist. Some have attracted more attention than others, there is a lack of agreement on their relative importance, and few frameworks have been proposed to model their influence. To date it has been common practice to list project success factors, whereas less attention has been paid to the question of how knowledge of the existence of these factors can be used to eliminate problems in practice. Despite the amount of research in this area, the contribution of particular success factors to project success remains unexplored, as are causal interactions between individual/groups of project success factors, and simulations of these.  
> **Conclusions.** Through the identification and analysis of the extant literature we identify opportunities for advancing knowledge of the practical and theoretical aspects of information systems project organization, with particular reference to success factors and project success. Contributions from those who offer expertise in the socio-technical analysis of systems implementations would be especially welcomed.

## Introduction

This review article provides a critical evaluation of the literature on project success factors in information systems, with a particular focus on organizational information systems. An organizational information system can be defined as '_any of a wide combination of computer hardware, communication technology and software designed to handle information related to one or more [organizational] processes_' (Flowers, 1996 cited by [Yeo, 2002](#yeo02), pp. 241-242). This definition excludes _personal_ information systems such as those deployed by pilots in the cockpits of modern fighter jets (see, for example, [Kopp, 1981/2005](#kop81)). Systems that fall into this latter category are not subject to the issues that arise when technology is implemented in an organizational context and are thus beyond the scope of this evaluation of the literature.

Organizational information systems can vary significantly in terms of their scale, complexity and functionality, as can their host organizations and end-user populations. These are both internal, for example functional groups charged with performing organizational processes such as human resource management or production, and external to the host organization, for example customers and suppliers. Examples of organizational information systems include enterprise resource planning systems, Web-based e-commerce systems and customer relationship management systems.

The specific focus of this review paper falls at the intersection of two concepts (1) organizational information system development projects and (2) success factors. Defined generically, a project is '_a temporary endeavour undertaken to create a unique product or service_' ([Project Management Institute, 2008, p. 5](#pro08)). An information systems project represents a specialized form of a project. It may be conceived as a _temporary endeavour_ performed to provide an information system for a host organization, or to significantly update or upgrade an existing implementation.

Information systems projects can be further classified as (1) implementation-only or (2) development projects. Implementation-only information systems projects do not include any significant software development (although they may still involve lesser degrees of software development for system installation, data transfer, minor customizations, etc.). They comprise the implementation of commercial packaged software products for a host organization. In contrast, development projects involve a significant amount of software development to create the information system prior to implementation. In this paper, development projects are referred to as organizational information systems development projects to accentuate (a) the organizational aspect of the project's deliverable (the information system hosted on the hardware), and (b) the software development process used to create it.

It is acknowledged that the term project success is difficult to define and interpret (for example, [Baccarini, 1999, p. 25](#bac99); [Cooke-Davies, 2004](#coo04), p. 99<; [Davis, 2014](#dav14), p. 189>; [Ika, 2009](#ika09), p. 8; [Standing, Guilfoyle, Lin and Love, 2006](#sta06), p. 1149; [Thomas and Fernandez, 2008](#tho08), p. 733). In addition, those who discuss project success factors often neglect to provide a detailed explanation of the concept of the broader term in their work. Ika ([2009](#ika09)) suggests that this issue is ignored because it is presumed that '_everyone knows what is meant by project success_' (p. 7), even though success in the context of project management is multi-dimensional, time-dependent and is very much determined by the perspective of the stakeholder who is making the judgment of success (or otherwise). For example, for a business owner, project success may be considered as a positive impact of the work completed on the business in question. It is also important to differentiate project success from project management success. The former refers to the satisfaction and benefit accrued by the host organization. The latter is concerned with management performance, typically whether the project adhered to the requirements of the schedule and budget.

Taking the above into account, for the purposes of this paper _success_ is considered as an expected outcome of all projects (as identified by, for example, [Hall, Beecham, Verner and Wilson, 2008](#hal08), pp. 31-32; [Nicholas and Hidding, 2010](#nic10), p. 152; [Project Management Institute, 2008](#pro08), p. 5; [Subramanyam, Weisstein and Krishnan, 2010](#sub10), p. 137). We argue that whether or not a project is successful may be judged by assessing the extent to which the project in question meets its intended aim. At the same time we recognize that others (for example, [de Wit, 1998](#dew98)) have argued that to objectively measure the success of a project is impossible.

Project success factors are conceived here as the conditions, circumstances and events that contribute to the success of a project. This definition is in alignment with terminology used in Ika's study ([2009](#ika09), p. 8), which is regularly cited alongside Jugdev and Müller ([2005) as one of two literature reviews that provide a generic analysis of project success factors. It is important that project success factors are distinguished from project success criteria (](#jug05)[Cooke-Davies, 2004](coo04), p. 99). The latter are the principles and standards by which project success can be judged, i.e. the measures that indicate that a project has been successful ([Lim and Mohamed, 1999](#lim99), p. 243). In the research literature these two components of project success are sometimes deployed loosely, occasionally giving the impression of misinterpretation and/or confusion (for example: [Bernroider and Ivanov, 2011](#ber11), p. 326; [Davis, 2014](#dav14), p. 189; [Gingnell, Franke, Lagerström, Ericsson and Lilliesköld, 2014](#gin14); [Lim and Mohamed, 1999](#lim99), p. 244; [Turner, Ledwith and Kelly, 2009](#tur09), p. 292).

In the domain of information systems, the burgeoning interest in project success factors has created a major research stream with findings generated mainly from quantitative studies ([Larsen and Myers, 1999](#lar99), p. 397). Project success factors have attracted the attention of many researchers over the years, as noted by a number of authors (for example, [Bryde, 2008](#bry08), p. 800; [Cooke-Davies, 2002](#coo02), p. 185; [Christensen and Walker, 2004](#chr04), p. 39; [Söderlund, 2004a](#so04a), p. 186; [Söderlund, 2011](#sod11), p. 159; [Thi and Swierczek, 2010](#thi10), p. 570) and their study holds a prominent position in the field ([Lu, Huang and Heng, 2006](#luh06), p. 295). Articles on project success factors feature in project management journals such as the _Project Management Journal_ and the _International Journal of Project Management_ ([Ika, 2009](#ika09), p. 11; [Söderlund, 2004a](#so04a), p. 189). Practitioners have also felt the influence of project success factor research. This is evident in the way that its output has been codified into standards, for example in project management bodies of knowledge ([Papke-Shields, Beise and Quan, 2010](#pap10), p. 660).

Although other researchers have previously addressed the question of project success factors in their work, their contributions have typically been at a generic level and, to date, a literature review with a specific focus on organizational information systems development has not been published in any established peer-reviewed journal. In addition, although they are valuable to information systems research from a broad perspective, the two most recent generic analyses of project success factors ([Jugdev and Müller, 2005](#jug05); [Ika, 2009](#ika09)) are now somewhat dated. Much has changed in the working environment in the past few years, especially in the domain of organizational information systems development with, for example, the uptake of mobile technologies, the proliferation of new breeds of software and the move towards agile information systems development methods. Our contribution is thus a timely analysis that responds to the question of how project success factors are treated in the organizational information systems development literature and where future research endeavours should be directed to enhance theoretical development in the domain.

As well as presenting an up-to-date and novel articulation of the treatment of success factors in an area of information systems project management practice that has previously been unexplored in detail, this paper also has practical value to information management, information systems, and project management researchers and practitioners. Since there is a paucity of literature reviews in the domain (despite the proliferation of articles that discuss project success factors per se), it is perhaps unsurprising that members of the first of these audiences - those who research information systems project management - have been accused of lacking a grasp of the literature (for example, [Morris, 2010](#mor10), p. 140). Our work addresses this issue by presenting a much-needed evaluative summary that can be accessed by the information systems project management research community at large. In addition, given that organizational information systems development projects are also renowned for high failure rates (as noted, for example, by [Gingnell _et al._, 2014](#gin14), p. 21; [Glass, 2006](#gla06), p. 15; [Walsh and Schneider, 2002](#wal02)), the content of a paper such as this can help improve understanding amongst a wider practitioner audience.

The main contribution of this work is found in the identification of the four broad research themes that have emerged from the detailed analysis of the body of literature on the topic of project success factors in information systems development. These themes, and the main findings as related to them, are summarised in Table 1.

<table class="center"><caption>  
Table 1: Overview of themes and findings identified from the analysis  
</caption>

<tbody>

<tr>

<th>Theme</th>

<th>Overview of findings</th>

</tr>

<tr>

<td>1\. Identification and exploration of project success factors and success factor lists</td>

<td>A large number of factors are identified and listed in the information systems literature - for example, 488 individual factors evident in fifty-six organizational information systems development papers in the period 1979-2012 sampled for this study - yet there is little agreement from one published list to the next.</td>

</tr>

<tr>

<td>2\. Contributions of individual or group project success factors to project success (or failure)</td>

<td>There has been much research on the contribution of project success factors to project success, but a lack of agreement in the findings of such studies and very little coverage of this question with specific reference to organizational information systems development.</td>

</tr>

<tr>

<td>3\. Causal interactions between individuals or groups of project success factors and simulations of these</td>

<td>Although the theme of causal interactions is considered in the literature, the treatment is incomplete and untested causal models are presented. Attempts to trace the dynamics of the interactions between success factors are under-explored, with no evidence of this having been researched to date with reference to organizational information systems development projects.</td>

</tr>

<tr>

<td>4\. Project success factor frameworks</td>

<td>While there is some evidence of published frameworks of relevance to information systems projects in general, they are lacking for organizational information systems development projects in particular.</td>

</tr>

</tbody>

</table>

Our articulation highlights where further opportunity lies to build on extant knowledge and to enhance understanding in this area of information systems research, as is presented in the account below.

The paper responds to recent calls for more literature reviews in the information systems literature. For example, in 2014 the editor of the European Journal of Information Systems appealed for 'literature reviews that offer the most solid foundations for theory building and research landscaping' ([Rowe, 2014](#row14), p. 242). By summarising the prior research and critically examining its various contributions, we identify some thematic gaps and possible future research directions.

## Methods and presentation of findings

Our main findings as related specifically to organizational information systems development derive from an analysis of fifty-six papers of core relevance published between 1979 and 2012\. These are listed in the [Appendix](#app). They include papers from the leading, peer-reviewed conferences and journals in the fields of information systems, information management and project management. This core set of papers represents just over a third of the material cited in this paper (56 out of 163 publications in total).

The papers analysed in our review were identified through extensive literature searches using commercial online databases such as _ABI/Information Complete (ProQuest)_, _ACM Digital Library (ACM)_, _Emerald Journals (Emerald)_, _Expanded Academic ASAP (Gale)_ and _IngentaConnect (Ingenta)_. _Google Scholar_ was also used to identify a number of articles. The initial searches were conducted using combinations of the terms shown in Table 2.

<table class="center"><caption>  
Table 2: Literature search term combinations</caption>

<tbody>

<tr>

<th>Term 1</th>

<td style="text-align:center;" rowspan="5">AND</td>

<th>Term 2</th>

</tr>

<tr>

<td>Information systems</td>

<td>Success factors</td>

</tr>

<tr>

<td>Information technology</td>

<td>Failure factors</td>

</tr>

<tr>

<td>Software projects</td>

<td>Risk factors</td>

</tr>

<tr>

<td>Software development</td>

<td>Success criteria</td>

</tr>

</tbody>

</table>

Many of the papers are published in information systems, project management and information management titles including the _European Journal of Information Systems_, _Information Research_, _Information and Management_, the _International Journal of Information Management_, the _Journal of Strategic Information Systems, International Journal of Project Management_ and the _Project Management Journal_. Since many phenomena in information systems research are interdisciplinary, titles from other related fields, such as business management and software development, were accessed to provide relevant material, as is established practice in the domain ([Rowe, 2014](#row14), p. 247).

In recognition that software development has been described as a practitioner-led discipline ([Glass, 2006](#gla06)), and that important advances in project management research have also been made in publications outside the realm of academic journal articles ([Jugdev and Müller, 2005](#jug05), p. 25), various other sources such as books and commercial articles, written by academics and practitioners alike, were deemed appropriate for the analysis presented in this paper and are thus also cited in the analysis below.

The literature search also involved citation pearling to identify further material from reference lists found in the papers identified. This proved to be particularly important, as a number of papers that are key to the theme under discussion, such as Moynihan ([1996](#moy96)), would not have been identified simply by using the search terms shown in Table 2.

The content of the material accessed was then classified according to:

*   the project type under discussion: for example, decision support system, executive information system, software development project.
*   the description of factors listed: for example, success, failure, risk.
*   the classification of the information systems project(s) examined: development, both development and packaged system implementation, or unspecified.
*   the perspective from which the account of the project(s) is presented: client, in-house, supplier (or unspecified).
*   the context of the work described, to include details of geographic location and industry sector where specified.
*   the data source on which the analysis presented is based: for example, survey data, anecdotal evidence.

An overview of the classifications as applied to the papers is provided in the Appendix table. The classification process contributed to a broader analysis that led to the identification of the four main research themes in the extant literature, as explored in further detail below.

The structure of our paper allows for the broad context of success factor research to be presented first. It is against this that the specifics of project success factor research as related to information systems in general, and to organizational information systems development as a particular sub-category of information systems, are then set. We then present the detail to support the articulation of the four themes summarized, exposing where there are overlaps between the general and specific literatures, and gaps particular to organizational information systems development. We conclude by arguing that there are many opportunities to develop theoretical insight in an area that is ripe for research and that such endeavours will be of value to both information systems research and practice in general, as well as organizational information systems development project management in particular.

## The development of project success factor research in the domain of project management

With its focus on organizational information systems development as a sub-category of information systems, this paper contributes to a larger body of literature on project success factor research. Interest in this area in general (i.e. independent of fields such as information systems and project management) grew in the middle of the twentieth century. The popularity of an article in _Harvard Business Review_ ([Daniel, 1961](#dan61)) on the development of executive information systems spawned initial interest. This work argued that companies are typically subject to between three and six industry-specific success factors ([Daniel, 1961](#dan61), p. 116). Almost two decades later, in 1979, a further article was published in _Harvard Business Review_ on the same theme ([Rockart, 1979](#roc79)). This later contribution became regarded as seminal in the study of success factors ([Fortune and White, 2006](#for06), p. 53). It describes an interview method to assist chief executive officers in establishing the information needs of organizations. The method was later extended to include (amongst other developments) success factor classification constructs ([Bullen and Rockart, 1981](#bul81)). These ideas were subsequently popularized in the project management literature, thus seeding the stream of research on project success factors.

In the period up until the mid-1980s empirical research on success factors in the field of project management was largely based on limited data sets and/or anecdotal evidence from single case studies, and derived from small samples ([Cooke-Davies, 2004](#coo04), p. 101; [Jugdev and Müller, 2005](#jug05), p. 24; [Söderlund, 2011](#sod11), p. 160). Although thought-provoking, the output of these studies cannot be considered as generalizable. Over time more rigorous studies were undertaken, drawing on data from larger samples ([Söderlund, 2011](#sod11), p. 160). The aim here was to identify generic project success factors applicable to all types of project, as opposed to a specific context or domain of application ([Fortune and White, 2006](#for06), p. 53; [Söderlund, 2004b](#so04b), p. 659). While the majority of the findings from these later studies were generated from quantitative surveys ([Ika, 2009](#ika09), p. 12; [Larsen and Myers, 1999](#lar99), p. 397), a few in-depth case studies were also published ([Söderlund, 2011](#sod11), p. 160).

The work on project success factors described above is noted as belonging to a tradition labelled the _factor school_ ([Söderlund, 2011](#sod11), p. 158). Typically such studies identify ten or more items in the format of project success factor lists (i.e., a larger number than the three to six of Daniel's 1961 work on general success factors noted above). Added to this empirical research are other studies relevant to success factor research which have been labelled as _theoretical_. These either derive their findings from secondary data, or make suggestions about project success factors that have yet to be tested empirically ([Fortune and White, 2006](#for06), p. 56).

A reading of the more recent project management literature as related to project success factors points to the long-held belief (or at least the assumption) that the identification and subsequent use of project success factors will lead to improved project performance ([Papke-Shields _et al._, 2010](#pap10), p. 660; [Pinto and Prescott, 1988](#pin88) cited by [Söderlund, 2004a](#so04a), p. 189). It is therefore unsurprising that efforts continue to be directed towards explaining which conditions, circumstances and events contribute to the success of a project. Recent studies reported in the information systems literature cover, for example:

*   the impact of political behaviours and game-playing in information systems project development ([Chang, 2013](#cha13))
*   the role of management support in multiple project environments ([Elbanna, 2013](#elb13))
*   means of quantifying success factors through deployment of an expert-based Baysian model ([Gingnell _et al._, 2014](#gin14))
*   the influence of cultural and external pressures on IT project performance ([Gu, Hoffman, Cao and Schniederjans, 2014](#guh14))
*   the relationship between management styles and control in a financial services information systems implementation ([Gregory and Keil, 2014](#gre14))
*   the part that organizational change plays in project success ([Hornstein, 2015](#hor15))
*   critical skills for managing information technology projects ([Keil, Lee and Deng, 2013](#kei13))
*   risk in information technology project performance ([Liu and Wang, 2014](#liu14))
*   uses of management control systems in information systems projects ([Sakka, Barki and Côté, 2013](#sak13)).

Further incentive for such research is the common practice of attempting to achieve organizational goals through project work ([Dvir, Lipovetsky, Shenhar and Tishler, 1998](#dvi98), p. 915; [Söderlund, 2004a](#so04a), p. 186). Another driver of these efforts is to understand why organizations that adopt this way of working continue to suffer poor project performance ([Mir and Pinnington, 2014](#mir14), p. 202; [Söderlund, 2011](#sod11), p. 159).

The general view is that with enhanced knowledge of project success factors, managers can focus their energies on those that matter most, then, it is hoped, watch as project performance improves ([Andersen, Birchall, Jessen and Money, 2006](#and06), p. 129). The desire to pin down the most important project success factors perhaps explains the steady stream of research publications on this theme (as noted by, for example, [Bryde, 2008](#bry08), p. 800; [Cooke-Davies, 2002](#coo02), p. 185; [Christensen and Walker, 2004](#chr04), p. 39>; [Söderlund, 2004a](#so04a), p. 186; [Söderlund, 2011](#sod11), p. 159; and [Thi and Swierczek, 2010](#thi10), p. 570). This also accounts for the calls for more research into generic project success factors (for example, [Bryde, 2008](#bry08), p. 800; [Davis, 2014](#dav14)).

## Project success factors in the information systems and organizational information systems development literature

The focus of the evaluation presented in this paper is previously published research on success factors in a particular area of information systems project management, i.e. projects related to organizational information systems development. Having set the general context of current priorities in success factor research in project management, the remainder of this review article considers the coverage of project success factors in this area by characterising its main themes and by highlighting gaps in knowledge to be addressed in future research.

The fifty-six papers of core relevance to software development, competences, activities or artefacts published in the past four decades (Table 2) (henceforth referred to as the _core papers_) are authored by academics, practitioners, commercial organizations and, in one case, a professional body. The majority derive from the USA. The body of work under review was published across a wide range of journals. The scattering of the literature, which is also evident in closely related areas such as enterprise resource planning projects ([Ngai, Law and Wat, 2008, p. 549](#nga08)), may be considered an indicator of the disparate nature of research in this domain. Such disparity impedes knowledge development ([Savolainen, Ahonen and Richardson, 2012](#sav12), p. 10) and perhaps explains in part why there are still many differing opinions on project success factors ([Andersen _et al._, 2006](#and06), p. 130), as will be elaborated below.

Despite the difficulties associated with researching an extant literature that is located across many titles, some authors have clearly made efforts to access and use it as a starting point for the identification of candidate success factors to explore in their own studies. In contrast, a number give the impression that their research has been carried out in near isolation. As a consequence, the core papers, when taken as a collection, indicate that there has been little opportunity for learning from previously published studies. This issue has been identified as a shortcoming of project management publications in general and often highlighted at project management conferences and in project management journals ([Reich _et al._, 2013](#rei13), p. 938). A further reason why the more recent authors appear not to have benefited from prior research is the lack of detail on research methods made available in the earlier work. For example, many accounts fail to discuss the limitations of the work presented (for example, there is regular use of localised convenience samples); the validity and reliability of findings; and the lessons learned from the studies reported. Nor do these authors offer recommendations for further research. In some cases, the research methodology is not made clear at all. This is a criticism that has been levelled at project management research as a whole (Smyth and Morris, 2007 cited by [Morris, 2010](#mor10), p. 143).

The content of the core papers covers both in-house and supplier software development projects. Even so, in over half the cases (33), this distinction is not always clear from the written accounts of the studies. The most common perspective presented is that of the host organization. Again, however, this detail is not always made explicit in the text of 30 papers. Three papers ([Cerpa and Verner, 2009](#cer09); [Hartman and Ashrafi, 2002](#har02); [Sharma, Sengupta and Gupta, 2011](#sha11)) consider the client and supplier perspectives together, and one focuses exclusively on the supplier perspective ([Moynihan, 1996](#moy96)). The project settings comprise both public sector and commercial organizations. The findings from the studies derive predominantly from the analysis of quantitative data collected by survey. Some qualitative research is also presented, based mainly on structured interviews.

The detailed evaluation of previously published research on success factors related to organizational information systems development projects presented below considers the characteristics of the literature according to the four themes presented above in Table 1.

### The identification and exploration of project success factors and the generation of project success factor lists: quantity of factors and lack of agreement

#### Consideration of individual project success factors in information systems research

It has been established that general research efforts related to project success factors stretch back several decades. Even so, the identification of individual factors is still highlighted as a priority area. For instance, there is a need to explore in greater detail the meaning of brief, high-level abstract ideals of factors that frequently feature in factor lists. As illustration, executive support is often named as a project success factor, but simply learning that this is important does not tell a project manager how to support a project more effectively ([Zwikael, 2008a](#zw08a), p. 387). Another area that requires attention is the inappropriate grouping of factors. In some cases, project success factors are abstracted to such a level that they become composites (or groups) of two or more individual factors. Although it has been argued that there can be merit in grouping success factors together ([Clarke, 1999](#cla99), p. 140; [Gingnell _et al._, 2014](#gin14), p. 25), this can also present problems. For example, the monitoring and control process has often been noted as significant (for example, [Taylor, 2000](#tay00), p. 25; [Walsh and Kanter, 1988](#wal88), p. 19), yet these are actually two distinguishable elements and should be considered as such ([Gardiner and Stewart, 2000](#gar00), p. 252). Separating them out helps identify the effects of each individual factor and the possible interactions between them. A third limitation of previous research is that it has not identified which factors can (and cannot) be transferred from one type of project to another. Part of the problem is that the extant studies have contributed to a project management literature that, in general, is predominantly normative ([Ahlemann, El Arbi, Kaiser and Hexk, 2013](#ahl13), p. 43): it describes how projects _should_ be managed, rather than how they are actually managed ([Nicholas and Hidding, 2010](#nic10)).

Over the past decade a number of studies have considered in detail individual success factors for information systems projects. Unlike the remainder of organizational information systems project success factor research, such studies often employ qualitative methods and concentrate on factors related to particular actors, namely:

1.  Project manager competences (for example, [Bloom, 1996](#blo96); [Müller and Turner, 2007](#mul07); [Skulmoski and Hartman, 2010](#sku10); [Seiler, Lent, Pinkowska and Pinazza, 2012](#sei12); [Thite, 2000](#thi00))
2.  Executive management support (for example, [Beckley and Gaines, 1991a](#be91a); [Beckley and Gaines, 1991b](#be91b); [Wight, 1983](#wig83); [Young and Jordan, 2008](#you08); [Zwikael, 2008a](#zw08a); [Zwikael, 2008b](#zw08b))
3.  End user involvement (for example, [Hsu, Lin, Zheng and Hung, 2012](#hsu12); [Petter, 2008](#pet08); [Subramanyam _et al._, 2010](#sub10)).

Ample opportunity remains to enhance understanding here (see, for example, [Petter, 2008](#pet08), p. 708; [Hsu _et al._, 2012](#hsu12), p. 9). For example, with the exception of the three areas listed above, few other actor-based success factor studies exist. Research into other project actors, such as the project team, is merited.

A few published studies of project success factors in the extant literature consider processes, such as, for example, project success factors for the software development life-cycle (for example, [Jones, 1996](#jon96); [Smuts, van der Merwe, Kotzé and Loock, 2010](#smu10)). As well as those who highlight the dearth of the coverage of processes (for example, [Hofmann and Lehner, 2001](#hof01), p. 58), there are others who point out the difficulties of examining them when it is often the case that the processes prescribed are routinely not followed in practice ([de Bakker, 2009](#deb09), p. 3; [Fortune and White, 2009](#for06), p. 37). Thus there is merit in investigating how alternative behaviours and activities of individuals impact organizational information systems development project processes ([Kutsch and Hall, 2005](#kut05), p. 595).

#### The deployment of project success factor lists in information systems research

The broader literature shows that since the 1960s project success factors have typically been presented in lists ([Fortune and White, 2006](#for06), p. 53). Many lists have been produced. Indeed, the literature of project management is replete with them ([Alojairi and Safayeni, 2012](#alo12), p. 17). Furthermore, existing lists readily spawn others. For example, Gingnell _et al._ ([2014](#gin14)) have recently summarized and consolidated nine lists from the information systems literature into a new one that features twenty-one factors (pp. 23-25). Most of these lists are named as project success factor lists. Other labels have also been applied: for example, check lists ([Alojairi and Safayeni, 2012](#alo12), p. 17), top ten lists ([Klakegg, 2009](#kla09), p. 500) and hit lists ([Gingnell _et al._, 2014](#gin14), p. 23). A continued lack of agreement across these published project success factor lists demonstrates that it has not yet been possible to generate a comprehensive list of the factors that are applicable to all projects ([Altuwaijri and Khorsheed, 2012](#alt12), p. 38; [Fortune and White, 2006](#for06), p. 54; [Söderlund, 2004a](#so04a), p. 186). It should also be noted that project success factor lists often do not distinguish between the most commonly cited project success factors and the most influential project success factors. Those that attract more attention may not be the most important ([Klakegg, 2009](#kla09), p. 500). A further issue is that the lists that resemble the top ten format negate the possibility of the influence of a far wider range of factors on any particular piece of work. Belassi and Tukel ([1996](#bel96), p. 142) and Jugdev and Müller ([2005](#jug05), p. 24) note another concern related to the sheer quantity of candidate project success factors in the information systems literature. The factors listed are not grouped or classified in any coherent manner in the published studies; an exception to this in the organizational information systems development literature is the work of Chow and Cao ([2008](#cho08), p. 964).

#### Individual project success factors and lists in the organizational information systems literature

Our analysis of the core papers reveals that, on average, each identifies twelve project success factors, thus reflecting the pattern in the wider literature as noted above. In contrast, two previous studies that form part of the set present a high number of factors: Moynihan ([1996](#moy96)) derived 113 risk constructs for project success (or failure) and Bannerman ([2008](#ban08)) refers to (although does not elaborate upon) over 300 artefacts that '_appeared to be relevant or important in enabling or inhibiting the performance and/or outcome_' of organizational information systems development projects (p. 2123).

Our audit revealed 488 unique project success factors related to organizational information systems development. Although further consolidation might be possible (for example, by additional effort to resolve near-synonymous terms), this number does not reflect the critical nature of a small number of success factors as originally defined by Daniel ([1961](#dan61), p. 116), nor the spirit of a top ten list or similar as noted above. This indicates that the success of organizational information systems development projects is the function of a very large range of factors.

This also points to a possible reason for the failure of project success factor research to provide a deep understanding of real-world projects, as highlighted above: the majority of success factor studies do not acknowledge, let alone address, the large number of factors relevant to organizational information systems development projects. This does not, however, imply that individual factors are not commonly shared, nor that individual projects are so dissimilar that nothing from one case could be applied to another. Indeed, our means of describing project success factors as entity-characteristic pairs as shown in Table 3 demonstrates that there is sufficient commonality to support the transfer of certain factors between projects of different types.

<table class="center"><caption>  
Table 3: Project entities and characteristics</caption>

<tbody>

<tr>

<th>Entity types</th>

<th>Entity examples</th>

<th>Characteristic examples</th>

<th>Example pairing</th>

</tr>

<tr>

<td>Actors</td>

<td>End users  
Project board members  
Project manager  
Project team</td>

<td rowspan="3">Clarity  
Effectiveness  
Fitness for purpose  
Involvement  
Maturity  
Stability  
Supportiveness</td>

<td>End users - involvement</td>

</tr>

<tr>

<td>Processes</td>

<td>Change control  
Communication  
Monitoring  
Risk management</td>

<td>Communication - clarity</td>

</tr>

<tr>

<td>Artefacts</td>

<td>Project deliverable  
Project estimates  
Project plan  
Project requirements</td>

<td>Project plan - stability</td>

</tr>

</tbody>

</table>

In some instances the relative rankings of the importance of project success factors identified is presented in the lists published in the information systems literature (Kwon and Zmud, 1987, cited by [Larsen and Myers, 1999](#lar99), p. 397). In keeping with the presentation style employed by the majority of such studies, Table 5 shows the top twenty-four project success factors in the core papers. It is worth highlighting that none of these factors is exclusively restricted to organizational information systems development. Indeed, most are applicable to generic projects and their management. The exceptions are end users' involvement and the maturity of the project deliverable's technology.

<table class="center"><caption>  
Table 4: Twenty-four most-cited organizational information systems development success factors</caption>

<tbody>

<tr>

<th>Rank</th>

<th>Entity type</th>

<th>Entity</th>

<th>Characteristic</th>

<th>Citations</th>

</tr>

<tr>

<td>1</td>

<td>Actor</td>

<td>End users</td>

<td>Involvement</td>

<td>18</td>

</tr>

<tr>

<td rowspan="3">2</td>

<td>Actor</td>

<td>Project board</td>

<td>Supportiveness</td>

<td>14</td>

</tr>

<tr>

<td>Actor</td>

<td>Project team</td>

<td>Competence</td>

<td>14</td>

</tr>

<tr>

<td>Process</td>

<td>Project planning</td>

<td>Effectiveness</td>

<td>14</td>

</tr>

<tr>

<td rowspan="2">5</td>

<td>Artefact</td>

<td>Requirements</td>

<td>Stability</td>

<td>11</td>

</tr>

<tr>

<td>Process</td>

<td>Project management</td>

<td>Effectiveness</td>

<td>11</td>

</tr>

<tr>

<td>7</td>

<td>Artefact</td>

<td>Requirements</td>

<td>Fitness for purpose</td>

<td>10</td>

</tr>

<tr>

<td rowspan="4">8</td>

<td>Artefact</td>

<td>Estimates</td>

<td>Fitness for purpose</td>

<td>7</td>

</tr>

<tr>

<td>Artefact</td>

<td>Project deliverable\technology</td>

<td>Maturity</td>

<td>7</td>

</tr>

<tr>

<td>Artefact</td>

<td>Requirements</td>

<td>Clarity</td>

<td>7</td>

</tr>

<tr>

<td>Process</td>

<td>Communication</td>

<td>Effectiveness</td>

<td>7</td>

</tr>

<tr>

<td rowspan="7">12</td>

<td>Actor</td>

<td>Client/host organization</td>

<td>Staff turnover</td>

<td>6</td>

</tr>

<tr>

<td>Actor</td>

<td>Project team</td>

<td>Competence/fit with project</td>

<td>6</td>

</tr>

<tr>

<td>Process</td>

<td>Change control</td>

<td>Effectiveness</td>

<td>6</td>

</tr>

<tr>

<td>Process</td>

<td>Project control</td>

<td>Effectiveness</td>

<td>6</td>

</tr>

<tr>

<td>Process</td>

<td>Project monitoring</td>

<td>Effectiveness</td>

<td>6</td>

</tr>

<tr>

<td>Process</td>

<td>Project</td>

<td>Size</td>

<td>6</td>

</tr>

<tr>

<td>Process</td>

<td>Risk management</td>

<td>Effectiveness</td>

<td>6</td>

</tr>

<tr>

<td rowspan="6">19</td>

<td>Actor</td>

<td>Project manager</td>

<td>Experience</td>

<td>5</td>

</tr>

<tr>

<td>Actor</td>

<td>Project team</td>

<td>Competence/ technical</td>

<td>5</td>

</tr>

<tr>

<td>Actor</td>

<td>Project team</td>

<td>Experience</td>

<td>5</td>

</tr>

<tr>

<td>Artefact</td>

<td>Requirements</td>

<td>Completeness</td>

<td>5</td>

</tr>

<tr>

<td>Process</td>

<td>Estimating</td>

<td>Effectiveness</td>

<td>5</td>

</tr>

<tr>

<td>Process</td>

<td>Project</td>

<td>Complexity</td>

<td>5</td>

</tr>

</tbody>

</table>

Two findings from this analysis are of particular interest. First, Table 4 highlights differences in the level of detail that can be found in success factor lists. Project requirements, for example, has four detailed entries in the list, each with different characteristics: (1) stability; (2) clarity; (3) completeness; and (4) fitness for purpose. In contrast, end users' involvement can be considered a much vaguer term. It does not distinguish, for example (1) the project activities in which the end users are to be involved, nor (2) the form that this involvement is to take (for example, full or part-time). This is likely to be indicative of a literature that has tended to focus on processes rather than socio-technical issues (as also noted by [Hornstein, 2015](#hor15), p. 291), and the relative maturity of the consideration of these two perspectives. Here we highlight an opportunity for contributions from information systems researchers working in areas such as socio-technical studies and social informatics to make a contribution to widening the scope of studies of information systems project success factors.

A second observation is the low level of agreement on the ranking of project success factors across the papers, reflecting the case in the broader literature noted above:

*   the top two factors appear together in only eight of the papers
*   the top three factors appear together in only three papers
*   the top four factors only appear together in one paper
*   the top five factors do not appear together in any single paper.

We argue that this may be accounted for by the different contexts of the studies reported in the core papers (although it should be noted that the provision of details of context is often poor in the core papers, as can be seen in Table 2). Others have previously called for research into context-specific project success factors, for example by specific project type, geography or culture ([Söderlund, 2011](#sod11), p. 159), where significant differences have previously been identified ([Pinto and Covin, 1989](#pin89), p. 49). Equally it has been argued that the influence of organizational dynamics, particularly associated with information systems and information technology projects, is often ignored (Gauld, 2007, cited by [Altuwaijri and Khorsheed, 2012](#alt12), p. 38) yet merits particular attention as an important contextual factor. Since it is rare that a particular set of success factors holds the same importance across multiple studies (Kwon and Zmud, 1987, cited by [Larsen and Myers, 1999](#lar99), p. 398), investigations into the relative value of project success factors in similar environments is likely to furnish more meaningful comparisons and assessments of the generalizability of conclusions from research reported. For example, Smuts _et al._ ([2010](#smu10)) identified almost fifty individual success factors related to the outsourcing of software development work. However, their findings derive from a single, South African case study and this is highly unlikely to be representative of project success factors in the development life-cycle of other contexts. This work would also address, to an extent, the question of applicability of top project success factors in particular contexts, yet also recognize that a single unified list is unlikely ever to be established ([Ika, 2009](#ika09), p. 9) due to the unique nature of each project.

Time should also be regarded as part of the context. This is because the influence of any project success factor is temporal: it varies at different points across the life-cycle of a project ([Larsen and Myers, 1999](#lar99), p. 398; [Pinto and Covin, 1989](#pin89), p. 49 cited by [Jugdev and Müller, 2005](#jug05), p. 26; [Pinto and Covin, 1989](#pin89), p. 59; [Pinto and Prescott, 1988](#pin88), p. 5; Pinto and Slevin, 1988 cited by [Söderlund, 2004a](#so04a), p. 189). In addition, the project life-cycle itself is not static, but a dynamic phenomenon ([Altuwaijri and Khorsheed, 2012](#alt12), p. 38; Ginzberg, 1981 cited by [Larsen and Myers, 1999](#lar99), p. 398; Paré and Elam, 1997 cited by [El Sawah, Tharwat and Rasmy, 2008](#els08), p. 260; [Söderlund, 2004a](#so04a), p. 189; [Söderlund, 2011](#sod11), p. 159-160). A further element of time, extraneous to the studies under review but important to the methods deployed in our study, is the period over which the core papers were published: much has changed in information systems in the thirty-three years that the collection spans. Therefore, it is perhaps inevitable that the attention paid to individual factors over the years is varied.

### Contributions of individual/group project success factors to project success (or failure): much research, little agreement, weak coverage in the organizational information systems development literature

Some have claimed that there has been limited research into the influence of project success factors on project success (or failure) _per se_ (for example, [Andersen _et al_., 2006](#and06), p. 128; [Thi and Swierczek, 2010](#thi10), p. 572). However, this review of the literature demonstrates that several papers on this theme have been published in the information systems literature since 2000, as noted in the examples listed in Table 5.

<table class="center"><caption>  
Table 5: Examples of research papers that discuss the influence of project success factors on project success (or failure) _per se_  
</caption>

<tbody>

<tr>

<th>Influence</th>

<th>Factor</th>

<th>Paper</th>

</tr>

<tr>

<td rowspan="20">Positive</td>

<td rowspan="2">Common knowledge (end users & project team)</td>

<td>*Tesch, Sobol, Klein and Jiang ([2009](#tes09))</td>

</tr>

<tr>

<td>Hsu _et al._ ([2012](#hsu12))</td>

</tr>

<tr>

<td>End user involvement</td>

<td>*Jiang, Chen and Klein ([2002](#jia02))</td>

</tr>

<tr>

<td>Executive management support</td>

<td>Young and Jordan ([2008](#you08))</td>

</tr>

<tr>

<td>Organizational support</td>

<td>Gelbard and Carmeli ([2009](#gel09))</td>

</tr>

<tr>

<td>Project commitment</td>

<td>Andersen _et al._ ([2006](#and06))</td>

</tr>

<tr>

<td>Project communications</td>

<td>Andersen _et al._ ([2006](#and06))</td>

</tr>

<tr>

<td rowspan="2">Project manager's leadership style</td>

<td>Müller and Turner ([2007](#mul07))</td>

</tr>

<tr>

<td>Sumner, Bock and Giamartino ([2006](#sum06))</td>

</tr>

<tr>

<td>Project manager's use of vision</td>

<td>Christensen and Walker ([2004](#chr04))</td>

</tr>

<tr>

<td>Project planning practices</td>

<td>Kearns ([2007](#kea07))</td>

</tr>

<tr>

<td>Project sponsorship</td>

<td>Bryde ([2008](#bry08)</td>

</tr>

<tr>

<td>Project team dynamics</td>

<td>Gelbard and Carmeli ([2009](#gel09))</td>

</tr>

<tr>

<td>Project team motivation</td>

<td>Verner, Beecham and Cerpa ([2010](#ver10))</td>

</tr>

<tr>

<td>Project vision</td>

<td>Christensen and Walker ([2004](#chr04))</td>

</tr>

<tr>

<td>Quality of planning</td>

<td>Dvir and Lechler ([2004](#dvi04))</td>

</tr>

<tr>

<td rowspan="2">Requirements engineering</td>

<td>Hofman and Lehner ([2001](#hof01))</td>

</tr>

<tr>

<td>Hsu _et al._ ([2012](#hsu12))</td>

</tr>

<tr>

<td>Risk management</td>

<td>De Bakker, Boonstra and Wortmann ([2012](#deb12))</td>

</tr>

<tr>

<td>Supportive organizational environment</td>

<td>Gray ([2001](#gra01))</td>

</tr>

<tr>

<td rowspan="5">Negative</td>

<td>Goal changes</td>

<td>Dvir and Lechler [(2004](#dvi04))</td>

</tr>

<tr>

<td>Incremental organizational change</td>

<td>Winklhofer ([2001](#win01))</td>

</tr>

<tr>

<td>Project size</td>

<td>Sauer, Gemino and Reich ([2007](#sau07))</td>

</tr>

<tr>

<td>Project volatility</td>

<td>Sauer _et al._ ([2007](#sau07))</td>

</tr>

<tr>

<td>Staff turnover</td>

<td>Hall _et al._ ([2008](#hal08))</td>

</tr>

<tr>

<td>Neutral</td>

<td>Developer input to project estimates</td>

<td>Verner, Evanco and Cerpa ([2007](#ver07))</td>

</tr>

<tr>

<td colspan="3">**Key:** *core papers</td>

</tr>

</tbody>

</table>

With the exception of a few qualitative studies (for example, [Christensen and Walker, 2008](#chr08); [de Bakker _et al._, 2012](#deb12)), research into the influence of project success factors on project success _per se_ is predominantly quantitative, as is the case for success factor lists as discussed above. Typically the results derive from surveys coupled with descriptive statistical analysis. The studies tend to focus on just one factor, or occasionally two, where the factor under investigation is considered as an independent variable that directly influences the dependent variable of project success, as illustrated in Figure 1\. Notably, project success is not normally considered in terms of specific success criteria_,_ i.e. measures that indicate that a project has been successful. Some of the studies also give consideration to moderating variables, i.e. those factors that might have a bearing on the influence of the factor under investigation.

<figure class="centre">![Figure 1: Research model for the influence of an individual factor on project success (or failure)](p676fig1.jpg)

<figcaption>Figure 1: Research model for the influence of an individual factor on project success (or failure)</figcaption>

</figure>

The vast majority of studies listed in Table 5 conclude that the factor under investigation is indeed a factor of project success, i.e. it has an influence. This suggests that future studies of the impact of other factors will demonstrate that project success is a function of a far wider range of factors than previously believed. This finding calls into question once more the validity of the top ten checklists highlighted above.

Although a detailed discussion of each of the papers listed in Table 5 is beyond the scope of this paper, the study published by Sauer _et al._ ([2007](#sau07)), which conforms to the research model presented in Figure 1, merits further discussion. The authors raise an issue that is largely ignored in the published research on project success factors. They argue that if a project success factor is to be treated as an independent variable to be measured or assessed in some way, then there should be some agreement of the units of measurement to achieve this. Taking project size as an example, Sauer _et al._ ([2007](#sau07)) recommend that project budget is an inappropriate unit of measurement. Instead, project size is better measured in terms of effort. This can be viewed as the product of project team size and project duration, and expressed, for instance, as person-months ([Sauer _et al._, 2007](#sau07), p. 82).

A further issue identified by Sauer _et al_. ([2007](#sau07)), but not taken into account by the majority of reported studies, is the degree or magnitude of any factor that influences project success. Without this information, the logical assumption is that the magnitude of a specific factor is directly proportional to its influence on project success. Thus it might be assumed that the more success factors in the mix, the better. However, Sauer _et al._'s ([2007](#sau07)) study shows this is not necessarily the case. This demonstrates, for example, that the influence of project size on project success is not linear. Instead, it rises slowly from twenty-five person-months to 1000 person-months, at which point it starts to rise far more steeply ([Sauer _et al._, 2007](#sau07), p. 81).

A third issue related by Sauer _et al._ ([2007](#sau07), p. 87), and noted by others (for example, [Andersen _et al._, 2006](#and06); [de Bakker _et al._, 2012](#deb12); [Hsu _et al._, 2012](#hsu12); [Tesch _et al._, 2009](#tes09)), is that the relationship between a specific factor and project success '_is [typically] not as simple or direct as many think_' ([Sauer _et al.,_ 2007](#sau07), p. 87). In many cases, specific factors are found to comprise a number of other sub-factors (as noted, for example, by [de Bakker _et al._, 2012](#deb12); [Gelbard and Carmeli, 2009](#gel09); [Verner _et al._, 2007](#ver07)).

It is also relatively common for studies of project success factors and their contribution to project success to acknowledge that the interaction between factors is relevant and that these interactions merit further investigation (for example, [Bryde, 2008](#bry08); [Hall _et al._, 2008](#hal08); [Tesch _et al._, 2009](#tes09)). A number of studies (for example, [Gelbard and Carmeli, 2009](#gel09); [Hsu _et al._, 2012](#hsu12); [Tesch _et al._, 2009](#tes09)) also identify the moderating effects of intermediate factors. In addition, these studies reveal small, localized causal chains associated with the specific project success factor under investigation (for example, [Hofmann and Lehner, 2001](#hof01); [Hsu _et al._, 2012](#hsu12); [Jiang _et al._, 2002](#jia02); [Wallace _et al._, 2004](#wa04b)). Any claim for a specific project factor to have a direct and distinct influence on project success is therefore difficult to justify. As Hall _et al._ ([2008](#hal08)) put it: '_the impact of individual factors is complex_' (p. 33).

The analysis of the literature thus reveals that despite the number of papers published on the influence of success factors on project success and given the high number of project success factors that can be identified from sampling the literature (488 from the core papers), there is clear opportunity for further investigations into the influence of success factors on project success. This is especially the case in terms of endeavours to identify the full range of factors at work and how groups of factors have an impact.

### Causal interactions between individual/groups of project success factors and simulations of these: incomplete and untested causal models

The relationships between factors is a priority area for project management research. Indeed, it has long been argued that project success factors should not be considered on an independent basis ([Clarke, 1999](#cla99), p. 141). This is because '_the inter-relationships between factors are at least as important as the individual factors_' ([Fortune and White, 2006](#for06), p. 54). Knowledge of these relationships is of particular benefit to practitioners engaged in information systems development (Nandhakumar, 1996 cited by [Fortune and White, 2006](#for06), p. 54). However, the success factor list approach tends to treat each factor as an independent variable and overlooks the interactions between the factors identified (Nandhakumar, 1996 cited by [Larsen and Myers, 1999](#lar99), p. 398). As a result relationships are left unexplained ([El Sawah _et al._, 2008](#els08), p. 260; Ginzberg, 1981 cited by [Larsen and Myers, 1999](#lar99), p. 398).

The published research discussed in the previous section supports the contention that numerous factors have an impact on project success. A closer analysis of this literature on project success in information systems also reveals a low understanding of how these factors interact. The need to enhance the extant knowledge was identified almost two decades ago by Belassi and Tukel ([1996](#bel96), p. 150), and has been expressed by others since (for example, [Kim and Pan, 2006](#kim06), p. 73). The earlier work noted that a combination of a large number of factors along the project life-cycle leads to project success ([Belassi and Tukel, 1996](#bel96), p. 142). Equally, the means by which these factors promulgate their influence in practice is significant: the factors do not typically affect a project's outcome in a direct manner ([Belassi and Tukel, 1996](#bel96), p. 142). It is therefore necessary to recognize that the influence of any given factor, or combination of factors, differs at points along the project life-cycle (because time influences context, as noted above) and this artefact of factor behaviour needs to be considered when assessing the overall impact of any factor on the final project outcome.

In some respects it is not surprising that there is low understanding of how success factors interact in information systems projects. Such projects are multifarious ([British Computer Society, 2004](#bri04), p. 15; [Williams, Klakegg, Walker, Andersen and Magnussen, 2012](#wil12), p. 44; [Xia and Lee, 2004](#xia04), p. 69) and their management is '_a very complex undertaking in which a complex network of interrelationships and interactions exists_' ([Abdel and Madnick, 1983](#abd83), p. 346). Indeed, complexity itself has been cited as a factor that influences project outcome, and normally one of failure, as noted in three of the core papers ([Charette, 2005](#cha05); [Tiwana and Keil, 2004](#tiw04); [Wohlin and Andrews, 2002](#woh02)).

As far back as the 1980s it was noted that an integrative model is necessary to make sense of the complex network as identified above, and this model should be flexible enough to represent the large number of factors in the complex network of interrelationships. In addition the model should be able to determine the dynamic behaviour of interactions between factors ([Abdel-Hamid and Madnick, 1983](#abd83), p. 346). Such a model would thus enhance understanding of information systems project success factors in two stages: first, by identifying the relationships between factors and, second, by making visible the dynamics of these relationships over the life-cycle of the project. The majority of research papers that have attempted to model project success factors address only the first issue. It should also be noted that most of these studies deal with a range of information systems projects, and not organizational information systems development projects specifically. Nonetheless, the factors considered in these studies are the same as those that feature in the success factor lists for organizational information systems development projects, for example executive management support and end user involvement. It is therefore likely that their findings are relevant to organizational information systems development projects.

Table 6 summarizes the characteristics of studies that have modelled the interactions of project success factors.

<table class="center"><caption>  
Table 6: Studies that model project success factors</caption>

<tbody>

<tr>

<th>Date</th>

<th>Authors</th>

<th>Characteristics of the studies and the models proposed</th>

</tr>

<tr>

<td>2002</td>

<td>Akkermans and Van Helden ([2002](#akk02))</td>

<td>This exploratory study of an enterprise resource mplementation attempts to understand the interrelationships between factors.  
It builds a relationship model by incorporating the relationships between ten success factors identified in an earlier study ([Somers and Nelson, 2001](#som01)).  
The model identifies a reinforcing loop of causal interactions that can act as both a vicious or virtuous feedback loop (p. 42).  
The model demonstrates how a change in a success factor can lead to a self-perpetuating cycle of good or poor performance that eventually leads to project success (or failure).</td>

</tr>

<tr>

<td>1999</td>

<td>[Butler and Fitzgerald (1999)](#but99)</td>

<td>This case study of an information systems development process presents a network analysis of twenty project success factors.  
It demonstrates that information systems development success factors, such as a committed project sponsor and adequate documentation, are closely related.  
It concludes that these success factors influence each other and that the strength of these influences can vary.</td>

</tr>

<tr>

<td>2006</td>

<td>Fortune and White ([2006](#for06))</td>

<td>This theoretical study framed twenty-seven success factors in a formal system model that considers the relationships between success factors.  
The model was then used to _distinguish_ between two information systems projects, one of which was successful, the other not (p. 63).</td>

</tr>

<tr>

<td>2004  
2006</td>

<td>Kim ([2004](#kim04)); Kim and Pan ([2006](#kim06))</td>

<td>The authors develop relationship models for information systems projects comprising _essential_ factors (twelve in the 2004 publication, and ten in 2006).  
The models derive from data collected in case studies of customer relationship management (CRM) system implementations.  
The models help explain how and why success factors affect one another and how their interaction leads to project success (or failure) ([Kim, 2004](#kim04), p. 28; [Kim and Pan, 2006](#kim06), p. 72).</td>

</tr>

<tr>

<td>2006  
2008</td>

<td>King and Burgess ([2006](#kin06)), King and Burgess ([2008](#kin08))</td>

<td>Influenced by work of Akkermans and Van Helden, the authors propose conceptual models that comprise a limited number of success factors linked in causal chains ([King and Burgess, 2006](#kin06), p. 66; [King and Burgess, 2008](#kin08), p. 426).</td>

</tr>

<tr>

<td>2005</td>

<td>[Procaccino, Verner, Darter and Amadio (2005)](#pro05)</td>

<td>The study proposes a model for organizational information systems development project success.  
Data for the model were collected from software practitioners.  
The model identifies a number of relationships between factors.  
The authors argue that there is a chronological critical path of success factors. Here the existence of a project champion influences the amount of time that end users make for requirements gathering.  
In turn, this leads to a high level of end user involvement in the development process, and results in better agreement on requirements between end users and the project team (p. 196).</td>

</tr>

<tr>

<td>2006</td>

<td>Sabherwal, Jeyaraj and Chowa ([2006](#sab06))</td>

<td>This study presents a theoretical information systems model that comprises six factors (and four criteria).  
It classifies the factors as either context-related (top management support and facilitating conditions) or user-related (user experience, training, attitude and participation).  
A number of relationships between factors are revealed.  
Some relationships were unanticipated by the researchers, for example, the influence of user attitudes on information systems quality (p. 1858).</td>

</tr>

<tr>

<td>2000</td>

<td>Yetton, Martin, Sharma and Johnston ([2000](#yet00))</td>

<td>This study presents a causal model of project performance with references to twelve project success factors.  
The model highlights the significance of executive management support, risk management, project team dynamics for strategic projects and end user involvement to successful project performance (p. 263)</td>

</tr>

</tbody>

</table>

While these studies have enhanced understanding of the interaction of project success factors, they have limitations, and these may go some way to explain the low understanding in this area noted above. Most striking perhaps is that even though our detailed study of the core papers identifies 488 unique project success factors, the studies summarised in Table 6 above consider just a few key success factors each. A further issue is that only three sets of researchers ([Akkermans and Van Helden, 2002](#akk02); [King and Burgess, 2006](#kin06) and [2008](#kin08); and [Yetton _et al._, 2000](#yet00)) investigate and report upon (simple) causal loops in their work. A related issue is that the treatment of success criteria across the studies as a whole is superficial. As such, the papers present rather simple scenarios. A stronger portrayal of the complexity of the causal interactions between success factors in information systems projects in general, and organizational information systems development projects in particular, would result from the inclusion of a wider range of factors, relationships and discussion of project success criteria in future work.

A number of researchers have noted that in cases where project success factors have been modelled, the models presented are somewhat static (for example, [Altuwaijri and Khorsheed, 2012](#alt12), p. 38; [King and Burgess, 2006](#kin06), p. 67). It is thus interesting to consider the extent to which attempts have been made to simulate the dynamics of success factor relationships.

It has been argued that '_simulation is a useful tool for tactical management in software engineering. It provides a means to study complex phenomena in project development that cannot be carried out easily with actual cases_' ([Lee and Miller, 2004](#lee04), p. 80). Added to this, the use of modelling techniques for projects in general has proved extremely useful in uncovering how complex projects behave, enhancing understanding of the actuality of projects ([Cicmil, Williams, Thomas and Hodgson, 2006](#ci206), p. 682-684).

Despite this, simulation remains an under-explored technique for understanding success factors in information systems projects, including those related to organizational information systems development ([King and Burgess, 2008, p. 430](#kin08)). Some earlier publications point to computerized simulation modelling techniques as a means to extend knowledge of success factor dynamics (for example, [Abdel-Hamid and Madnick, 1983](#abd83), p. 346; [King and Burgess, 2006](#kin06), p. 62; [King and Burgess, 2008, p. 430](#kin08); [Lee and Miller, 2004, p. 80](#lee04)). However, much of this work is tentative (for example, [King and Burgess, 2008](#kin08)). The use of simulation models to enhance understanding of the dynamic relationships between success factors in organizational information systems development projects is thus another area ripe for research. There is thus significant scope for researchers to generate more comprehensive causal models to better explain factor influence in real-world organizational information systems development projects. In addition, it would be useful if the conceptual models already developed were validated, complemented, modified and/or extended through the work of others in the future.

### Project success factor frameworks: dearth of frameworks for organizational information systems development projects

A variety of generic project success factor frameworks, in a range of shapes and forms, have been proposed to address the issues such as how project success factor may be grouped. These are considered below.

Discussion of frameworks is commonplace in the mainstream general project success factor literature ([Ika, 2009](#ika09), p. 11; [Jugdev and Müller, 2005](#jug05), p. 25) and some of the frameworks described have been tested empirically ([Jugdev and Müller, 2005](#jug05), p. 29). However, it has been noted that there remains a need for more inclusive project success factor frameworks ([Ika, 2009](#ika09), p. 11).

Early examples of general project success factor frameworks include Kerzner's model for project excellence ([Kerzner, 1987](#ker87), p. 33) and Slevin and Pinto's project implementation framework ([Slevin and Pinto, 1987](#sle87), p. 35). By the mid-1990s, Belassi and Tukel ([1996](#bel96), p. 144) had developed a more holistic framework. This encouraged the consideration of project success factors in terms of their classification and relationships. It also highlighted the implications of not taking particular project success factors into account ([Jugdev and Müller, 2005](#jug05), p. 25). The value of Belassi and Tukel's framework was that it was assessed by a range of study participants, some of whom were information systems practitioners ([Belassi and Tukel, 1996](#bel96), p. 149). Another general framework that was tested for its usefulness amongst information systems practitioners was that of Westerveld ([2003](#wes03), p. 415). This framework linked project success factors to project success criteria (i.e. the measures that indicate that a project has been successful). Evidence of the value of Westerveld's framework is shown in the assessment of an enterprise resource planning implementation ([Westerveld, 2003](#wes03), p. 417).

More recently, a number of success factor frameworks directly related to information systems (as opposed to general) projects have been published. For example, Vithanage and Wijayanayake ([2007](#vit07), p. 37) developed a framework for large-scale information systems implementations. Similarly, in 2010 Hawari and Heeks proposed a design-reality gap model for enterprise resource planning projects (p. 151). Although both of these studies are concerned with implementation-based projects, the factors that they encompass suggest that the frameworks in question may be relevant to the implementation aspects of organizational information systems development projects. Indeed Hawari and Heeks ([2010](#haw10), p. 155) state explicitly that there is no reason why their framework cannot be exported to other information systems applications. Fortune and White ([2006](#for06)) have also shown how a generic framing device can be applied to information systems projects (including one organizational information systems development project). The evidence provided in these studies thus suggests that generic project success factor frameworks can be applied to information systems projects in general, and possibly also to organizational information systems development projects.

The literature search conducted for the analysis presented in this article, however, uncovered just one framework that addresses factors for organizational information systems development projects in particular ([Keil _et al._, 1998](#kei98), p. 80). This framework focuses on risk, with particular attention paid to two of its dimensions: (1) perceived relative importance; and (2) perceived level of control. The general content of Keil _et al._'s 1998 paper, including the framework described, is clearly useful in itself. However, its greater value in the context of a review of the literature such as this lies in its identification of the need for further development of project success factor frameworks for organizational information systems development projects. Despite the length of time since the publication of Keil _et al._'s study in 1998, the analysis of the literature presented in this article supports the currency of this view.

## Conclusion

Our goal when preparing this review was to generate a critical evaluation of the literature pertinent to success factors in organizational information systems development projects within the broader context of the information systems literature. It was anticipated that the output would highlight where future research endeavour could extend the existing knowledge base on project success factors in organizational information systems development projects. This goal has been met by considering four broad research themes that emerged from an analysis of extant literature as summarised in . Although we cannot claim comprehensive coverage of all literature belonging to the domain - this is often impossible in this kind of work ([Rowe, 2014](#row14), p. 246) - the collection of work reviewed is comprehensive enough for identifying (1) where previous research effort has been invested and (2) possible future research directions.

Perhaps the most prominent finding here is just how much opportunity there is to enhance the understanding of project success factors in organizational information systems development so that better decisions can be made in practice, and those decisions can be translated into actions that are properly resourced. Indeed, it is striking how a research stream that has been in existence for several decades and has produced plenty of output in this time (despite claims to the contrary, for example, [Belout and Gauvreau, 2004](#bel04)) has failed to generate any convincing theoretical models. What is found here is an abundance of factors and a paucity of frameworks. This apparent immaturity of the domain is also reflected in the superficial treatment of some topics in studies that lack rigour. This is exhibited, for example, in the reluctance in some studies to acknowledge that project success factors cannot be considered as independent variables. This superficiality is also demonstrated in cases where there appears to be general ignorance of certain topics, such as the question of causal loops.

While it is useful for practitioners to have an awareness of a set of project success factors (many of which are presented in the literature), it does not follow that such knowledge can directly improve project performance. Practitioners need to understand, for example, which of the factors are most important to project success ([Altuwaijei and Khorsheed, 2012](#alt12), p. 38; [Fortune and White, 2006](#for06), p. 54; [Söderlund, 2004a](#so04a), p. 186); how these factors can be used to alleviate problems faced in practice ([Clarke, 1999](#cla99), p. 139; [Nakatsu and Iacovou, 2009](#nak09), p. 64); and the potential consequences of actions taken ([King and Burgess, 2006](#kin06), p. 59; [King and Burgess, 2008](#kin08), p. 421). In short, literature that gives evidence of the existence of project success factors in the form of lists, but does not give advice on how they can be used to help eliminate problems in practice, is inadequate. There is a need for an additional narrative that describes what these real-world factors represent and also for advice and guidance on the practical application of these factors so that practitioners can predict the consequences of their actions. Stronger theoretical models would be beneficial to those who wish to understand the primary causes of success and failure ([King and Burgess, 2008](#kin08), p. 421). However, with very few exceptions (for example, [Rodriguez-Repiso, Setchib and Salmeron, 2007](#rod07)), the literature is almost completely silent on the subject of project success factor management.

There are several possible explanations for the criticisms levelled above. First, the lack of strong research tradition in project management can be cited. The main impact of this deficiency is that this domain lacks research direction, both in terms of the identification of priority areas for research and in promoting appropriate approaches for the execution of empirical work. Equally a literature that is scattered makes it difficult for researchers to access, critically evaluate and draw upon established work in the course of their own research. It is also the case that the question of context - for example in terms of the timing, culture, governance, geographic location, strategic intent - as related to the organizational information systems development projects under scrutiny has a strong influence on project success factor behaviour. The complexity of organizational information systems development projects is also acknowledged as an inhibiting factor here. Thus it may be unreasonable to expect the findings from any particular study to necessarily match with others.

That said, this review makes clear that there are several areas related to project success factors in organizational information systems development projects that are ripe for research and extending knowledge in the context of the project management of organizational information systems development projects would also be welcomed by the broader information systems community. Some of the opportunity lies in addressing straightforward gaps in knowledge. For example, it is not possible to say with certainty which are the key project success factors in organizational information systems development projects. Others have made comments such as this in the context of information systems projects in general (for example, [Fortune and White, 2006](#for06), p. 54). However, none have gone as far as to say that the notion of the existence of _critical_ success factors may be illusory and the search for them is not worth continuing. Our own view is that that the word _complex_ may be a better epithet than _critical_ to apply to the phrase _success factors_. Further gaps in knowledge may be filled by exploring artefacts and actors as success factors for organizational information systems development projects and by examining the use of simulation models to enhance understanding of dynamic relationships between success factors in organizational information systems development projects.

Further research could address the contradictory nature of earlier studies in the domain. For example, there is little agreement as to the relative importance of particular project success factors. Even if agreement cannot be reached, the reasons why it is lacking would be a starting point of interest to the wider research community. Future research contributions that explore these issues and address others highlighted in this review will help build an evidence base that would engage both the academic and practitioner communities. This, it is hoped, will lead to the generation of theoretical insight to enhance the understanding of real-world organizational information systems development projects, as well as improve the general theoretical underpinning of project management in the domain of information systems as a valuable, and valued, domain of research.

Some of these questions may be addressed by engaging a broader community of researchers in studies that have traditionally been dominated by project management and/or general information systems research. Such individuals may be found working in particular areas of information systems research, such as socio-technical studies, social informatics and other related disciplines. Much more may be achieved by drawing on such research domains that (1) have a tradition of in-depth qualitative analyses of systems implementations and (2) use a range of approaches that go beyond a focus on processes to include consideration of actors and artefacts in the context of the wider macrostructure of systems implementation. Thus there is the potential for the discussion in this domain to move on from lists of factors to the presentation of frameworks, detailed simulation models and the possible development of theory.

## Acknowledgements

The authors gratefully acknowledge the assistance of Dr Bruce Ryan in preparing the final version of this paper for publication.

## About the authors

**Robert Irvine** is an Information Systems and Software professional. He has over 20 years' experience of managing and leading projects in Europe, the US and China for a range of companies that includes SAP, Motorola and Rolls Royce. He holds a Bachelors degree in Energy Engineering, a Masters degree in Business Administration, a Masters degree in Software Engineering, and a PhD in Computing.  
**Hazel Hall** is Professor of Social Informatics in the Institute for Informatics and Digital Innovation within the School of Computing at Edinburgh Napier University, UK. She holds a Bachelors degree in French, a Masters degree in Library and Information Studies, and a PhD in Computing. For further information please see the profile at [http://hazelhall.org/about](http://hazelhall.org/about). Hazel Hall can be contacted at [h.hall@napier.ac.uk](mailto:h.hall@napier.ac.uk)

#### References - Note: * identifies the 'core papers' referred to in the text

*   Abdel-Hamid, T. K. & Madnick, S. E. (1983). The dynamics of software project scheduling. _Communications of the ACM, 26_(5), 340-346.
*   Ahlemann, F., El Arbi, F., Kaiser, M.G. & Hexk, A. (2013). A process framework for theoretically grounded prescriptive research in the project management field. _International Journal of Project Management, 31_(1), 43-56.
*   Akkermans, H. & van Helden, K. (2002). Vicious and virtuous cycles in ERP implementation. _European Journal of Information Systems, 11_(1), 35-46.
*   Alojairi, A. & Safayeni, F. (2012). The dynamics of inter-node coordination in social networks: a theoretical perspective and empirical evidence. _International Journal of Project Management, 30_(1), 15-26.
*   Altuwaijri, M. M. & Khorsheed, M. S. (2012). InnoDiff: a project-based model for successful IT innovation diffusion. _International Journal of Project Management, 30_(1), 37-47.
*   *Averweg, U. R. & Erwin, G. J. (1999). Critical success factors for implementation of decision support systems in South Africa. In R. Sprague, (Ed.), _Proceedings of the 32nd Hawaii International Conference on System Sciences_ (pp. 1-10). New York, NY: IEEE.
*   Andersen, E. S., Birchall, D., Jessen, S. A. & Money, A. H. (2006). Exploring project success. _Baltic Journal of Management, 1_(2), 127-147.
*   Baccarini, D. (1999). The logical framework method for defining project success. _Project Management Journal, 30_(4), 25-32.
*   *Bannerman, P. L. (2008). Risk and risk management in software projects: a reassessment. _The Journal of Systems and Software, 81_(12), 2118-2133.
*   Beckley, G. B. & Gaines, M. (1991a). Implementing business systems: what the CEO can do to ensure success. _Industrial Management, 33_(2), 25-26.
*   Beckley, G. B. & Gaines, M. (1991b). Implementation of business systems is as important as the hardware. _CPA Journal, 61_(2), 61-63.
*   Belassi, W. & Tukel, O. I. (1996). A new framework for determining critical success/failure factors in projects. _International Journal of Project Management, 14_(3), 141-151.
*   Belout, A. & Gauvreau, C. (2004). Factors influencing project success: the impact of human resource management. _International Journal of Project Management, 22_(1), 1-11.
*   Bernroider, E. W. & Ivanov, M. (2011). IT project management control and the control objectives for IT and related technology (CobiT) framework. _International Journal of Project Management, 29_(3), 325-336.
*   *Berntsson-Svensson, R. & Aurum, A. (2006). Successful software project and products: an empirical investigation. In G.H. Travassos, J.C. Maldonado & C. Wohlin, (Eds.), _Proceedings of the 2006 ACM/IEEE International Symposium on Empirical Software Engineering_, (pp. 144-153). New York, NY: ACM.
*   Bloom, N. L. (1996). Selecting the right IS project manager for success. _Personnel Management, 75_(1), 6-9.
*   *British Computer Society. (2004). [_The challenges of complex IT projects_](http://www.webcitation.org/6VWXmCsqG). London: British Computer Society. Retrieved from http://www.bcs.org/upload/pdf/complexity/pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6VWXmCsqG)
*   *Brocke, H., Uebernickel, F. & Brenner, W. (2009). Success factors in IT-projects to provide customer value propositions. _Proceedings of the 20th Australasian Conference on Information Systems_, (pp. 1-10). Melbourne, Australia: Association for Information Systems.
*   Bryde, D. (2008). Perceptions of the impact of project sponsorship practices on project success. _International Journal of Project Management, 26_(8), 800-809.
*   Bullen, C. V. & Rockart, J. F. (1981). [_A primer on critical success factors_](http://www.webcitation.org/6VWYAzKaF). Cambridge, MA: Sloan School of Management. (CISR No. 69 Sloan WP No. 1220-81). Retrieved from http://dspace.mit.edu/bitstream/handle/1721.1/1988/SWP-1220-08368993-CISR-069.pdf?sequence=1 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6VWYAzKaF)
*   *Bussen, W. & Myers, M. D. (1997). Executive information system failure: a New Zealand case study. _Journal of Information Technology, 12_(2), 145-153.
*   *Butler, T. & Fitzgerald, B. (1999). Unpacking the systems development process: an empirical application of the CSF concept in a research context. _Journal of Strategic Information Systems, 8_(4), 351-371.
*   *Cash, C. H. & Fox, R. (1999). Elements of successful project management. _Journal of Systems Management, 43_(9), 10-12.
*   *Cerpa, N. & Verner, J. M. (2009). Why did your project fail? _Communications of the ACM, 52_(12), 130-134.
*   Chang, C.L. (2013). The relationship among power types, political games, game players, and information system project outcomes - a multiple case study. _International Journal of Project Management, 31_(1), 57-67.
*   *Charette, R. N. (2005). Why software fails. _IEEE Spectrum, 42_(9), 42-49.
*   *Chow, T. & Cao, D. (2008). A survey study of critical success factors in agile software projects. _Journal of Systems and Software, 81_(6), 961-971.
*   Christensen, D. & Walker, D. H. (2004). Understanding the role of vision in project success. _Project Management Journal, 35_(3), 39-52.
*   Christensen, D. & Walker, D. H. (2008). Using vision as a critical success element in project management. _International Journal of Managing Projects in Business, 1_(4), 611-622.
*   Cicmil, S., Williams, T., Thomas, J. & Hodgson, D. (2006). Rethinking project management: researching the actuality of projects. _International Journal of Project Management, 24_(8), 675-686.
*   Clarke, A. (1999). A practical use of key success factors to improve the effectiveness of project management. _International Journal of Project Management, 17_(3), 139-145.
*   Cooke-Davies, T. (2002). The real success factors on projects. _International Journal of Project Management, 20_(3), 185-190.
*   Cooke-Davies, T. (2004). Project success. In P. W. Morris, & J. K. Pinto (Eds.), _The Wiley guide to managing projects_ (pp. 99-122). Hoboken, NJ: John Wiley.
*   Daniel, D. R. (1961). Management information crisis. _Harvard Business Review, 39_(5), 111-121.
*   Davis, K. (2014). Different stakeholder groups and their perceptions of project success. _International Journal of Project Management, 32_(2), 189-201.
*   de Bakker, K. (2009). Risk management does (not) contribute to project success. _The Risk Register, 11_(4), 1-5.
*   de Bakker, K., Boonstra, A. & Wortmann, H. (2012). Risk management's communicative effects influencing IT project success. _International Journal of Project Management, 30_(4), 444-457.
*   de Wit, A. (1998). Measurement of project success. _International Journal of Project Management, 6_(3), 164-170.
*   Dvir, D. & Lechler, L. (2004). Plans are nothing, changing plans is everything: the impact of changes on project success. _Research Policy, 33_(1), 1-15.
*   Dvir, D., Lipovetsky, S., Shenhar, A. & Tishler, A. (1998). In search of project classification: a non-universal approach to project success factors. _Research Policy, 27_(9), 915-935.
*   Elbanna, A. (2013). Top management support in multiple-project environments: an in-practice view. _European Journal of Information Systems, 22_(3), 278-294.
*   El Sawah, S., Tharwat, A. A. & Rasmy, M. H. (2008). A quantitative model to predict the Egyptian ERP implementation success index. _Business Process Management Journal, 14_(3), 288-306.
*   *Evans, M. W., Abela, A. M. & Beltz, T. (2002). Seven characteristics of dysfunctional software projects. _Crosstalk: Journal of Defense Software Engineering, 15_(4), 16-20.
*   *Ewusi-Mensah, K. (1997). Critical issues in abandoned information systems development projects. _Communications of the ACM, 40_(9), 74-80.
*   Fortune, J. & White, D. (2006). Framing of project critical success factors by a systems model. _International Journal of Project Management, 24_(1), 53-65.
*   *Gaitros, D. A. (2004). Common errors in large software development projects. _Crosstalk: Journal of Defense Software Engineering, 17_(3), 21-25.
*   Gardiner, P. D. & Stewart, K. (2000). Revisiting the golden triangle of cost, time and quality: the role of NPV in project control, success and failure. _International Journal of Project Management, 18_(4), 251-256.
*   Gelbard, R. & Carmeli, A. (2009). The interactive effect of team dynamics and organizational support on ICT project success. _International Journal of Project Management, 27_(5), 464-470.
*   Gingnell, L., Franke, U., Lagerström, R., Ericsson, E. & Lilliesköld, J. (2014). Quantifying success factors for IT projects - an expert-based Baysian model. _Information Systems Management, 31_(1), 21-36.
*   Glass, R. L. (2006). The Standish report: does it really describe a software crisis? _Communications of the ACM, 49_(8), 15-16.
*   Gray, R. J. (2001). Organisational climate and project success. _International Journal of Project Management, 19_(2), 103-109.
*   Gregory, R.W. & Keil, M. (2014). Blending bureaucratic and collaborative management styles to achieve control ambidexterity in IS projects. _European Journal of Information Systems, 23_(3), 343-356.
*   Gu, V.C., Hoffman, J.J., Cao, Q. & Schniederjans, M.J. (2014). The effects of organizational culture and environmental pressures on IT project performance: a moderation perspective. _International Journal of Project Management, 32_(7), 1170-1181.
*   Hall, T., Beecham, S., Verner, J. & Wilson, D. (2008). The impact of staff turnover on software projects: the importance of understanding what makes software practitioners tick. In D. Lending, C. Vician, C. Riemenschneider & D.J. Armstrong, (Eds.), _Proceedings of the SIGMIS Computer Personnel Doctoral Consortium and Research Conference_ (pp. 30-39). New York, NY: ACM.
*   *Han, W.-M. & Huang, S.-J. (2007). An empirical analysis of risk components and performance on software projects. _Journal of Systems and Software, 80_(1), 42-50.
*   *Hartman, F. & Ashrafi, R. A. (2002). Project management in the information systems and information technologies industries. _Project Management Journal, 33_(3), 5-15.
*   Hawari, A. & Heeks, R. (2010). Explaining ERP failure in a developing country: a Jordanian case study. _Journal of Enterprise Information Management, 23_(2), 135-160.
*   Hofmann, H. F. & Lehner, F. (2001). Requirements engineering as a success factor in software projects. _IEEE Software, 18_(4), 58-66.
*   Hornstein, H.A. (2015). The integration of project management and organizational change management is now a necessity. _International Journal of Project Management, 33_(2), 291-298.
*   Hsu, J. S.-C., Lin, T.-C., Zheng, G.-T. & Hung, Y.-W. (2012). Users as knowledge co-producers in the information system development project. _International Journal of Project Management, 30_(1), 27-36.
*   Ika, L. A. (2009). Project success as a topic in project management journals. _Project Management Journal, 40_(4), 6-19.
*   *Jiang, J. J., Chen, E. & Klein, G. (2002). The importance of building a foundation for user involvement in information system projects. _Project Management Journal, 33_(1), 20-26.
*   *Jiang, J. J. & Klein, G. (1999). Risks to different aspects of system success. _Information and Management, 36_(5), 263-272.
*   *Jiang, J. J. & Klein, G. (2001). Software project risks and development focus. _Project Management Journal, 32_(1), 4-9.
*   Jiang, J. J., Klein, G., & Ellis, T. S. (2002). A measure of software development risk. _Project Management Journal, 33_(3), 30-41.
*   Jones, C. (1996). Our worst current development practices. _IEEE Software, 13_(2), 102-104.
*   *Jones, C. (2004). Software project management practices: failure versus success. _CrossTalk: Journal of Defense Software Engineering, 17_(10), 5-9.
*   Jugdev, K. & Müller, R. (2005). A retrospective look at our understanding of project success. _Project Management Journal, 36_(4), 19-31.
*   *Kanter, J. & Walsh, J. J. (2004). Toward more successful project management. _Information Systems Management, 21_(2), 16-21.
*   Kearns, G. S. (2007). How the internal environment impacts informations systems project success: an investigation of exploitative and explorative firms. _Journal of Computer Information Systems, 48_(1), 63-75.
*   Keil, M., Cule, P. E., Lyytinen, K. & Schmidt, C. (1998). A framework for identifying software project risks. _Communications of the ACM, 41_(11), 76-83.
*   Keil, M., Lee, H.K. & Deng T. (2013). Understanding the most critical skills for managing IT projects: a Delphi study of IT project managers. _Information and Management, 50_(7), 398-414.
*   Kerzner, H. (1987). In search of excellence in project management. _Journal of Systems Management, 38_(2), 30-39.
*   Kim, H.-W. (2004). A process model for successful CRM system development. _IEEE Software, 21_(4), 22-28.
*   Kim, H.-W. & Pan, S. L. (2006). Towards a process model of information systems implementation. _The Data Base for Advances in Information Systems, 37_(1), 59-76.
*   *Kim, C. S. & Peterson, D. K. (2001). Developers' perceptions of information systems success factors. _Journal of Computer Information Systems, 41_(2), 29-35.
*   King, S. F. & Burgess, T. F. (2006). Beyond critical success factors: a dynamic model of enterprise system innovation. _International Journal of Information Management, 26_(1), 59-69.
*   King, S. F. & Burgess, T. F. (2008). Understanding success and failure in customer relationship management. _Industrial Marketing Management, 37_(4), 421-431.
*   *Klein, G., Jiang, J. J. & Tesch, D. B. (2002). Wanted: project teams with a blend of IS professional orientations. _Communications of the ACM, 45_(6), 81-87.
*   Klakegg, O. J. (2009). Pursuing relevance and sustainability: improvement strategies for major public projects. _International Journal of Managing Projects in Business, 2_(4), 499-518.
*   Kopp, C. (1981, March). [The modern fighter cockpit](http://www.webcitation.org/6X1EwfpLo). _Australian Aviation and Defence Review_, No. 10, 92-95\. Retrieved from http://www.ausairpower.net/TE-Fighter-Cockpits.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6X1EwfpLo).
*   Kutsch, E. & Hall, M. (2005). Intervening conditions on the management of project risk: dealing with uncertainty in information technology projects. _International Journal of Project Management, 23_(8), 591-599.
*   Larsen, M. A. & Myers, M. D. (1999). When success turns into failure: a package-driven business process re-engineering project in the financial services industry. _Journal of Strategic Information Systems, 8_(4), 395-417.
*   Lee, B. & Miller, J. (2004). Multi-project management in software engineering using simulation modelling. _Software Quality Journal, 12_(1), 59-82.
*   *Leishman, T. R. & Cook, D. A. (2004). Risk factor: confronting the risks that impact software project success. _Crosstalk: Journal of Defense Software Engineering, 17_(5), 31-34.
*   Lim, C. S. & Mohamed, M. Z. (1999). Criteria of project success: an exploratory re-examination. _International Journal of Project Management, 17_(4), 243-248.
*   Liu, S. & Wang, L. (2014). Understanding the risks on performance in internal and outsourced information technology projects. _International Journal of Project Management, 32_(8), 1494-1510.
*   Lu, X.-H., Huang, L.-H. & Heng, M. S. (2006). Critical success factors of inter-organizational information systems: a case study of Cisco and Xiao Tong in China. _Information and Management, 43_(3), 395-408.
*   *Magal, S. R., Carr, H. H. & Watson, H. J. (1988). Critical success factors for information center managers. _MIS Quarterly, 12_(3), 413-425.
*   *Mahaney, R. C. & Lederer, A. L. (2003). Information systems project management: an agency theory interpretation. _The Journal of Systems and Software, 68_(1), 1-9.
*   *May, L. (1998). Major causes of software project failures. _CrossTalk: Journal of Defense Software Engineering, 11_(7), 9-12.
*   *Merla, E. (2005). Beginning at the ending. _Proceedings of the PMI Global Conference_ (pp. 1-7). Edinburgh, UK: Project Management Institute.
*   *Milis, K. & Mercken, R. (2002). Success factors regarding the implementation of ICT investment projects. _International Journal of Production Economics, 80_(1), 105-117.
*   Mir, F.A. & Pinnington, A.H. (2014). Exploring the value of project management: linking project management performance and project success. _International Journal of Project Management, 32_(2), 202-217.
*   *Moore, J. H. (1979). A framework for MIS development projects. _MIS Quarterly, 3_(1), 29-38.
*   Morris, P. W. (2010). Research and the future of project management. _International Journal of Managing Projects in Business, 3_(1), 139-146.
*   *Moynihan, T. (1996). An inventory of personal constructs for information systems project risk researchers. _Journal of Information Technology, 11_(4), 359-371.
*   Müller, J. & Turner, R. (2007). Matching the project manager's leadership style to project type. _International Journal of Project Management, 25_(1), 21-32.
*   *Nakatsu, R. T. & Iacovou, C. L. (2009). A comparative study of important risk factors involved in offshore and domestic outsourcing of software development projects: a two-panel Delphi study. _Information and Management, 46_(1), 57-68.
*   Ngai, E. W., Law, C. C. & Wat, F. K. (2008). Examining the critical success factors in the adoption of enterprise resource planning. _Computers in Industry, 59_(6), 548-56.
*   Nicholas, J. & Hidding, G. (2010). Management principles associated with IT project success. _International Journal of Management and Information Systems, 4_(5), 147-156.
*   Papke-Shields, K. E., Beise, C. & Quan, J. (2010). Do project managers practice what they preach, and does it matter to project success? _International Journal of Project Management, 28_(7), 650-662.
*   Petter, S. (2008). Managing user expectations on software projects: lessons from the trenches. _International Journal of Project Management, 26_(7), 700-712.
*   Pinto, J. K. & Covin, J. G. (1989). Critical factors in project implementation: a comparison of construction and R&D projects. _Technovation, 9_(1), 49-62.
*   Pinto, J. K. & Prescott, J. E. (1988). Variations in critical success factors over the stages of the project life cycle. _Journal of Management, 14_(1), 5-18.
*   *Procaccino, J. D., Verner, J. M., Darter, M. E. & Amadio, W. J. (2005). Toward predicting software development success from the perspective of practitioners: an exploratory Bayesian model. _Journal of Information Technology, 20_(3), 187-200.
*   Procaccino, J.D., Verner, J.M. & Lorenzet, S.J. (2006). Defining and contributing to software development success. _Communications of the ACM, 49_(8), 79-83.
*   Project Management Institute. (2008). _A guide to the project management body of knowledge_ (4th ed.). Newton Square, PA: Project Management Institute.
*   *Reel, J. S. (1999). Critical success factors in software projects. _IEEE Software, 16_(3), 18-23.
*   Reich, B.H., Liu, L., Sauer, C., Bannerman, P., Cicmil, S., Cooke-Davies, T., & Thomas, J. (2013). Developing better theory about project organizations. _International Journal of Project Management, 31_(7), 938-942.
*   *Richardson, G. & Ives, B. (2004). Systems development processes. _Computer, 37_(5), 84-86.
*   Rockart, J. F. (1979). Chief executives define their own data needs. _Harvard Business Review, 57_(2), 81-92.
*   Rodriguez-Repiso, L., Setchib, R. & Salmeron, J. L. (2007). Modelling IT projects success: emerging methodologies reviewed. _Technovation, 27_(10), 582-594.
*   *Ropponen, J. & Lyytinen, K. (2000). Components of software development risk: how to address them? A project manager survey. _IEEE Transactions on Software Engineering, 26_(2), 98-112.
*   Rowe, F. (2014). What literature review is not: diversity, boundaries and recommendations. _European Journal of Information Systems, 23_(3), 241-255.
*   Sabherwal, R., Jeyaraj, A. & Chowa, C. (2006). Information system success: individual and organizational determinants. _Management Science, 52_(12), 1849-1864.
*   Sakka, O., Barki, H. & Côté, L. (2013). Interactive and diagnostic uses of management control systems in IS projects: antecedents and their impact on performance. _Information and Management, 50_(6), 265-274.
*   *Salmeron, J. L. & Herrero, I. (2005). An AHP-based methodology to rank critical success factors of executive information systems. _Computer Standards & Interfaces, 28_(1), 1-12.
*   *Sauer, C. & Cuthbertson, C. (2003). _[State of IT project management: UK 2002-2003](http://www.webcitation.org/6VWYQr85J)_. Retrieved from http://www.bestpracticehelp.com/The_State_of_IT_Project_Management_in_the_UK_2003_2004.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6VWYQr85J)
*   Sauer, C., Gemino, A. & Reich, B. H. (2007). The impact of size and volatility on IT project performance: studying the factors influencing project risk. _Communications of the ACM, 50_(11), 79-84.
*   Somers, T. M., & Nelsen, K. (2001). The impact of critical success factors across the stages of enterprise resource planning implementations. In _Proceedings of the 34th Hawaii International Conference on System Sciences_, (pp. 1-10). New York, NY: IEEE.
*   Savolainen, P., Ahonen, J. J. & Richardson, I. (2012). Software development project success and failure from the supplier's perspective: a systematic literature review. _International Journal of Project Management, 30_(4), 1-12.
*   Seiler, S., Lent, B., Pinkowska, M. & Pinazza, M. (2012). An integrated model of factors influencing project managers' motivation: findings from a Swiss survey. _International Journal of Project Management, 30_(1), 60-72.
*   *Sharma, A., Sengupta, S. & Gupta, A. (2011). Exploring risk dimensions in the Indian software industry. _Project Management Journal, 42_(5), 78-91.
*   Skulmoski, G. J. & Hartman, F. T. (2010). Information systems project manager soft competencies: a project-phase investigation. _Project Management Journal, 41_(1), 61-80.
*   Slevin, D. P., & Pinto, J. K. (1987). Balancing strategy and tactics in project implementation. _Sloan Management Review, 29_(1), 33-41.
*   Smuts, H., van der Merwe, A., Kotzé, P. & Loock, M. (2010). Critical success factors for information systems outsourcing management: a software development lifecycle view. In P. Kotzé, A. van der Merwe & A. Gerber, (Eds.), _Proceedings of the 2010 Annual Research Conference of the South African Institute of Computer Scientists and Information Technologists_ (pp. 304-313). New York, NY: ACM.
*   Söderlund, J. (2004a). Building theories of project management: past research, questions for the future. _International Journal of Project Management, 22_(3), 183-191.
*   Söderlund, J. (2004b). On the broadening scope of the research on projects: a review and a model for analysis. _International Journal of Project Management, 22_(8), 655-667.
*   Söderlund, J. (2011). Pluralism in project management: navigating the crossroads of specialization and fragmentation. _International Journal of Management Reviews, 13_(2), 153-176.
*   Standing, G., Guilfoyle, A., Lin, C. & Love, P. E. (2006). The attribution of success and failure in IT projects. _Industrial Management & Data, 106_(8), 1148-1165.
*   *Standish Group International Inc. (2001). [_Extreme CHAOS_](http://www.webcitation.org/6VWZQ9Vqw). Boston, MA: Standish Group International Inc. Retrieved from https://courses.cs.ut.ee/MTAT.03.243/2014_spring/uploads/Main/standish.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6VWZQ9Vqw)
*   *Standish Group International Inc. (1995). [_CHAOS_](http://www.webcitation.org/6VWZaymJ5). Boston, MA: Standish Group International Inc. Retrieved from http://www.projectsmart.co.uk/docs/chaos-report.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6VWZaymJ5)
*   Subramanyam, R., Weisstein, F. L. & Krishnan, M. S. (2010). User participation in software development projects. _Communications of the ACM, 53_(3), 137-141.
*   Sumner, M., Bock, D. & Giamartino, G. (2006). Exploring the linkage between the characteristics of IT project leaders and project success. _Information Systems Management, 23_(4), 43-49.
*   *Taylor, A. (2000). IT projects: sink or swim. _The Computer Bulletin, 42_(1), 24-26.
*   *Tesch, D., Kloppenborg, T. J. & Frolick, M. N. (2007). IT project risk factors: the project management professional's perspective. _Journal of Computer Information Systems, 47_(4), 61-69.
*   Tesch, D., Sobol, M. G., Klein, G. & Jiang, J. J. (2009). User and developer common knowledge: effect on the success of information system development projects. _International Journal of Project Management, 27_(7), 657-664.
*   Thi, C. H. & Swierczek, F. W. (2010). Critical success factors in project management: implication from Vietnam. _Asia Pacific Business Review, 16_(4), 567-589.
*   Thite, M. (2000). Leadership styles in information technology projects. _International Journal of Project Management, 18_(4), 235-241.
*   Thomas, G. & Fernandez, W. (2008). Success in IT projects: a matter of definition. _International Journal of Project Management, 26_(7), 733-742.
*   *Tiwana, A. & Keil, M. (2004). The one minute risk assessment tool. _Communications of the ACM, 47_(11), 73-77.
*   Turner, J. R., Ledwith, A. & Kelly, J. (2009). Project management in small to medium-sized enterprises. _International Journal of Managing Projects in Business, 2_(2), 282-296.
*   *Ugwu, O. O. & Kumaraswamy, M. M. (2007). Critical success factors for construction ICT projects: some empirical evidence and lessons for emerging economies. _ITCon, 12_ (Special issue), 231-249.
*   Verner, J., Beecham, S. & Cerpa, N. (2010). Stakeholder dissonance: disagreements on project outcome and its impact on team motivation across three countries. In M. Gallivan, D. Downey & D. Joseph, (Eds.), _Proceedings of 2010 Special Interest Group on Management Information Systems 48th Annual Conference on Computer Personnel Research_ (pp. 25-33). New York, NY: ACM.
*   *Verner, J. M. & Evanco, W. M. (2005). In-house software development: what project management practices lead to success? _IEEE Software, 22_(1), 86-93.
*   Verner, J. M., Evanco, W. M. & Cerpa, N. (2007). State of the practice: an exploratory analysis of schedule estimation and software project success prediction. _Information and Software Technology, 49_(2), 181-193.
*   *Verner, J. M., Overmyer, S. P. & McCain, K. W. (1999). In the 25 years since the mythical man-month what have we learned about project management? _Information and Software Technology, 41_(14), 1021-1026.
*   Vithanage, D. K. & Wijayanayake, W. M. (2007). Insight to the large scale information systems implementation in Sri Lanka. In J.B. Ekanayake, (Ed.), _Proceedings of the Second International Conference on Industrial and Information Systems_ (pp. 33-39). New York, NY: IEEE.
*   *Wallace, L. & Keil, M. (2004). Software project risks and their effect on outcomes. _Communications of the ACM, 47_(4), 68-73.
*   *Wallace, L., Keil, M. & Rai, A. (2004). How software project risk affects project performance: an investigation of the dimensions of risk and an exploratory model. _Decision Sciences, 35_(2), 289-321.
*   *Walsh, J. J. & Kanter, J. (1988). Toward more successful project management. _Journal of Systems Management, 39_(1), 16-21.
*   Walsh, K.R. & Schneider, H. (2002). [The role of motivation and risk behaviour in software development success](http://www.webcitation.org/6VWc4wfrF). _Information Research, 7_(3), paper 129\. Retrieved from http://InformationR.net/ir/7-3/paper129.html. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6VWc4wfrF)
*   *Warkentin, M., Moore, R. S., Bekkering, E. & Johnston, A. C. (2009). Analysis of systems development project risks: an integrative framework. _The DATA BASE for Advances in Information Systems, 40_(2), 8-27.
*   *Wateridge, J. (1995). IT projects: a basis for success. _International Journal of Project Management, 13_(3), 169-172.
*   Westerveld, E. (2003). The project excellence model: linking success criteria and critical success factors. _International Journal of Project Management, 21_(6), 411-418.
*   Wight, O. W. (1983). _The executive's guide to successful MRPII_ (2nd ed.). Englewood Cliffs, NJ: Prentice-Hall.
*   Williams, T., Klakegg, O. J., Walker, D. H., Andersen, B. & Magnussen, O. M. (2012). Identifying and acting on early warning signs in complex projects. _Project Management Journal, 42_(3), 37-53.
*   Winklhofer, H. (2001). Organizational change as a contributing factor to IS failure. In R. Sprague, (Ed.), _Proceedings of the 34th Hawaii International Conference on System Sciences_ (pp. 1-9). New York, NY: IEEE.
*   *Wohlin, C. & Andrews, A. A. (2002). Analysing primary and lower order project success drivers. In G. Tortora & S. Chang, (Eds.), _Proceedings of the 14th International Conference on Software Engineering and Knowledge Engineering_ (pp. 393-400). New York, NY: IEEE.
*   Xia, W. & Lee, G. (2004). Grasping the complexity of IS development projects. _Communications of the ACM, 47_(5), 69-74.
*   *Yeo, K. T. (2002). Critical failure factors in information system projects. _International Journal of Project Management, 20_(3), 241-246.
*   Yetton, P., Martin, A., Sharma, R. & Johnston, K. (2000). A model of information systems development project performance. _Information Systems Journal, 10_(4), 263-289.
*   Young, R. & Jordan, E. (2008). Top management support: mantra or necessity? _International Journal of Project Management, 26_(7), 713-725.
*   Zwikael, O. (2008a). Top management involvement in project management: exclusive support practices for different project scenarios. _International Journal of Managing Projects in Business, 1_(3), 387-403.
*   Zwikael, O. (2008b). Top management involvement in project management: a cross country study of the software industry. _International Journal of Managing Projects in Business, 1_(4), 498-511.

## Appendix: Organizational information systems development project success factor studies 1979-2012

<table class="center">

<tbody>

<tr>

<th>ID</th>

<th>Paper</th>

<th>Project system/ process type</th>

<th>Factor type</th>

<th>C</th>

<th>D</th>

<th>P</th>

<th>Context</th>

<th>DS</th>

<th>Methods (or data sources)</th>

</tr>

<tr>

<td>1</td>

<td>Averweg and Erwin ([1999](#ave99))</td>

<td>Decision support systems</td>

<td>Critical success factors</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>Decision support systems in South Africa.</td>

<td>I</td>

<td>Analysis based on unspecified number of structured interviews with business managers, end users, IT personnel and academics in eighteen non-government organizations.</td>

</tr>

<tr>

<td>2</td>

<td>Bannerman ([2008](#ban08))</td>

<td>Software projects</td>

<td>Major risk factors</td>

<td>B</td>

<td>B</td>

<td>I/C</td>

<td>Software projects in government agencies in an Australian state.</td>

<td>I</td>

<td>Quantitative analysis of twenty-three structured interviews with project, IT and business managers.</td>

</tr>

<tr>

<td>3</td>

<td>[Berntsson-Svensson and Aurum (2006)](#ber06)</td>

<td>Software projects</td>

<td>Success factors</td>

<td>B</td>

<td>B</td>

<td>I/C</td>

<td>Financial services, consulting and telecommunications industries in Swedish and Australian companies.</td>

<td>S</td>

<td>Analysis of survey questions completed by unspecified number of software practitioners and managers.</td>

</tr>

<tr>

<td>4</td>

<td>British Computer Society ([2004](#bri04))</td>

<td>Complex software and IT projects</td>

<td>Key players, key success factors</td>

<td>B</td>

<td>S</td>

<td>C</td>

<td>UK public and private sector, software and information technology projects.</td>

<td>U</td>

<td>Analysis of written and oral _evidence_ of seventy directors, managers, project managers and software engineers from the private and public sectors, as well as academic experts. Detail of methods not specified.</td>

</tr>

<tr>

<td>5</td>

<td>Brocke, Uebernickel, and Brenner ([2009](#bro09))</td>

<td>Information technology projects</td>

<td>Success factors</td>

<td>B</td>

<td>B</td>

<td>I/C</td>

<td>European telecommunications firm.</td>

<td>C</td>

<td>Five case studies (two development projects). Details of methods not specified.</td>

</tr>

<tr>

<td>6</td>

<td>Bussen and Myers ([1997](#bus97))</td>

<td>Executive information systems</td>

<td>Risk factors</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>Large organization in New Zealand.</td>

<td>C</td>

<td>Case study.</td>

</tr>

<tr>

<td>7</td>

<td>Butler and Fitzgerald ([1999](#but99))</td>

<td>Information systems development process</td>

<td>Critical success factors</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>Large Irish telecommunications company.</td>

<td>I</td>

<td>Qualitative, case-based research based on an analysis of four development projects. Thirty-eight interviews with participants in development process.</td>

</tr>

<tr>

<td>8</td>

<td>Cash and Fox ([1999](#cas99))</td>

<td>Computer systems</td>

<td>Elements (of success)</td>

<td>U</td>

<td>U</td>

<td>I/C</td>

<td>Generic.</td>

<td>A</td>

<td>Not specified, although findings appear to be based on anecdotal evidence.</td>

</tr>

<tr>

<td>9</td>

<td>Cerpa and Verner ([2009](#cer09))</td>

<td>Software development</td>

<td>Failure factors</td>

<td>D</td>

<td>B</td>

<td>B</td>

<td>Software developers from the USA, Australia and Chile.</td>

<td>S</td>

<td>Analysis of survey of software practitioners. Covers seventy failed projects.</td>

</tr>

<tr>

<td>10</td>

<td>Charette ([2005](#cha05)<)/td></td>

<td>Software projects</td>

<td>Failure factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Unspecified.</td>

<td>U</td>

<td>Unspecified.</td>

</tr>

<tr>

<td>11</td>

<td>Chow and Cao ([2008](#cho08))</td>

<td>Agile software projects</td>

<td>Critical success factors</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Agile software projects.</td>

<td>S</td>

<td>Quantitative analysis of a survey of agile professionals working on 109 projects in twenty-five countries.</td>

</tr>

<tr>

<td>12</td>

<td>Evans, Abela and Beltz ([2002](#eva02))</td>

<td>Software projects</td>

<td>Risk characteristics</td>

<td>B</td>

<td>U</td>

<td>U</td>

<td>Focus appears to be US-based projects, including development projects providing data processing applications.</td>

<td>O</td>

<td>Analysis based on risk assessment of company database of twelve years' worth of project assessments.</td>

</tr>

<tr>

<td>13</td>

<td>Ewusi-Mensah ([1997](#ewu97))</td>

<td>Information systems development projects</td>

<td>Critical issues</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Project abandonment</td>

<td>U</td>

<td>Unspecified, other than it is noted that the report is based on previous research by author.</td>

</tr>

<tr>

<td>14</td>

<td>Gaitros ([2004](#gai04))</td>

<td>Large software development projects</td>

<td>Common errors</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Generic software development.</td>

<td>A</td>

<td>Anecdotal account based on author experience.</td>

</tr>

<tr>

<td>15</td>

<td>Han and Huang ([2007](#han07))</td>

<td>Software projects</td>

<td>Software risks</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Generic software projects.</td>

<td>S</td>

<td>Analysis of data generated from Web-based survey of 115 project managers.</td>

</tr>

<tr>

<td>16</td>

<td>Hartman and Ashrafi ([2002](#har02))</td>

<td>Information systems & information technology projects</td>

<td>Critical success factors</td>

<td>B</td>

<td>B</td>

<td>B</td>

<td>Canadian software projects.</td>

<td>I</td>

<td>Findings derive from thirty-six structured interviews. Cover twelve projects.</td>

</tr>

<tr>

<td>17</td>

<td>Jiang and Klein ([1999](#jia99))</td>

<td>Information systems development</td>

<td>Risks</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Information systems development in the USA.</td>

<td>S</td>

<td>Findings derive from eighty-six survey returns from information systems project managers. Includes statistical analysis.</td>

</tr>

<tr>

<td>18</td>

<td>Jiang and Klein ([2001](#jia01))</td>

<td>Software projects</td>

<td>Risks</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Information systems software development in the USA.</td>

<td>S</td>

<td>Findings derive from 152 survey returns from information systems project managers, project leaders and professionals. Includes statistical analysis.</td>

</tr>

<tr>

<td>19</td>

<td>Jiang, Klein and Ellis ([2002](#jke02))</td>

<td>Software development</td>

<td>Risks</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Information systems development in the USA.</td>

<td>S</td>

<td>Findings derive from 152 survey returns from information systems project managers. Includes statistical analysis.</td>

</tr>

<tr>

<td>20</td>

<td>Jones ([2004](#jon04))</td>

<td>Software project management practices</td>

<td>(Opposing) major factors</td>

<td>D</td>

<td>B</td>

<td>U</td>

<td>Large software projects including information systems, corporations and government agencies. Appears to be US-based study.</td>

<td>U</td>

<td>Analysis of 250 projects, the full details of which are not included in the paper.</td>

</tr>

<tr>

<td>21</td>

<td>Kanter and Walsh ([2004](#kan04))</td>

<td>Software development project</td>

<td>Major problem areas</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>Information technology organization in a large, unidentified, decentralised company in USA.</td>

<td>C</td>

<td>Case study.</td>

</tr>

<tr>

<td>22</td>

<td>Keil, Cule, Lyytinen and Schmidt ([1998](#kei98))</td>

<td>Software projects</td>

<td>Risk factors</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Software development projects in Finland, Hong Kong and the USA.</td>

<td>P</td>

<td>Delphi method with input of three panels of experienced software project managers from Finland, Hong Kong and the USA.</td>

</tr>

<tr>

<td>23</td>

<td>Kim and Peterson ([2001](#kim01))</td>

<td>Information systems</td>

<td>Success factors</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>Internal information systems development in the USA.</td>

<td>S</td>

<td>Statistical analysis of seventy-nine questionnaires completed by software developers working for large conglomerate companies in the USA.</td>

</tr>

<tr>

<td>24</td>

<td>Klein, Jiang and Tesch ([2002](#kle02))</td>

<td>System development projects</td>

<td>Leading indicators</td>

<td>D</td>

<td>U</td>

<td>I/C</td>

<td>Information systems development in companies in the USA.</td>

<td>S</td>

<td>Findings derive from survey of 239 experienced information systems professionals, including information systems department managers, information systems project leaders, information systems analysts and others, from six large private organizations in the USA.</td>

</tr>

<tr>

<td>25</td>

<td>Leishman and Cook ([2004](#lei04))</td>

<td>Software projects</td>

<td>Risk factors</td>

<td>B</td>

<td>U</td>

<td>U</td>

<td>Software projects.</td>

<td>A</td>

<td>Findings based on anecdotal evidence.</td>

</tr>

<tr>

<td>26</td>

<td>Magal, Carr and Watson ([1988](#mag88))</td>

<td>Information centres</td>

<td>Critical success factors</td>

<td>U</td>

<td>U</td>

<td>I/C</td>

<td>Information centres in the USA.</td>

<td>S</td>

<td>Findings derive from survey of 311 information centre managers, directors and analysts. Includes statistical analysis.</td>

</tr>

<tr>

<td>27</td>

<td>[Mahaney and Lederer (2003)](#mah03)</td>

<td>Information systems development projects</td>

<td>Reasons for failure and risk factors</td>

<td>D</td>

<td>B</td>

<td>I/C</td>

<td>Information systems development in the USA.</td>

<td>I</td>

<td>Analysis derives from structured interviews with 12 information systems project managers in a variety of industries in the USA.</td>

</tr>

<tr>

<td>28</td>

<td>May ([1998](#may98))</td>

<td>Software projects</td>

<td>Failure causes</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Software development projects (probably in the USA).</td>

<td>I</td>

<td>Analysis of data from interviews conducted with practitioners and consultants.</td>

</tr>

<tr>

<td>29</td>

<td>Merla ([2005](#mer05))</td>

<td>Information technology projects</td>

<td>Key success factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>'_Addresses the typical problems encountered in Information Technology projects_' (p.1).</td>

<td>O</td>

<td>Findings drawn from post-implementation project reviews in an unspecified organization.</td>

</tr>

<tr>

<td>30</td>

<td>Milis and Mercken ([2002](#mil02))</td>

<td>Information and communication technology investment projects</td>

<td>Success factors</td>

<td>U</td>

<td>U</td>

<td>I/C</td>

<td>ICT investment projects.</td>

<td>I</td>

<td>Qualitative analysis of four projects in Belgian banks based on sixteen in-depth interviews and document analysis. Grounded approach.</td>

</tr>

<tr>

<td>31</td>

<td>Moore ([1979](#mor79))</td>

<td>Management information systems software development projects</td>

<td>Characteristics</td>

<td>D</td>

<td>B</td>

<td>U</td>

<td>Software development projects.</td>

<td>S</td>

<td>Statistical analysis derived from survey of twenty-four department (project) managers in various organizations.</td>

</tr>

<tr>

<td>32</td>

<td>Moynihan ([1996](#moy96))</td>

<td>Information systems projects</td>

<td>Factors/themes</td>

<td>D</td>

<td>S</td>

<td>S</td>

<td>Information systems projects in Ireland.</td>

<td>I</td>

<td>Analysis based on fourteen personal construct elicitation sessions with experienced application systems developers involved in the management of bespoke software-intensive application development projects for external clients.</td>

</tr>

<tr>

<td>33</td>

<td>Nakatsu and Iacovou ([2009](#nak09))</td>

<td>Software development projects</td>

<td>Key risk factors</td>

<td>D</td>

<td>S</td>

<td>C</td>

<td>Offshore and domestic outsourcing.</td>

<td>P</td>

<td>Analysis derives Delphi study that comprised thirty-two experienced information technology project managers in two panels (one domestic and one offshore).  </td>

</tr>

<tr>

<td>34</td>

<td>Procaccino, Verner and Lorenzet ([2006](#pro06))</td>

<td>Software development</td>

<td>Process success drivers</td>

<td>D</td>

<td>B</td>

<td>U</td>

<td>Software development in USA.</td>

<td>S</td>

<td>Analysis derives from findings of online survey of thirty developers at twenty USA-based software development organizations/departments.</td>

</tr>

<tr>

<td>35</td>

<td>Reel ([1999](#ree99))</td>

<td>Software projects</td>

<td>Critical success factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Software projects.</td>

<td>A</td>

<td>Unspecified, but evidence appears to be anecdotal.</td>

</tr>

<tr>

<td>36</td>

<td>Richardson and Ives ([2004](#ric04))</td>

<td>Software development processes</td>

<td>Reasons for project failure</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Software development projects.</td>

<td>O</td>

<td>Analysis is based on findings of previous, unspecified studies.</td>

</tr>

<tr>

<td>37</td>

<td>Ropponen and Lyytinen ([2000](#rop00))</td>

<td>Software development</td>

<td>Risk components</td>

<td>D</td>

<td>B</td>

<td>U</td>

<td>Software development in Finland.</td>

<td>S</td>

<td>Statistical analysis of survey data collected from Finnish project managers.</td>

</tr>

<tr>

<td>38</td>

<td>Salmeron and Herrero ([2005](#sal05))</td>

<td>Executive information systems</td>

<td>Critical success factors</td>

<td>D</td>

<td>U</td>

<td>I/C</td>

<td>Executive information systems.</td>

<td>O</td>

<td>Analytic hierarchy process based research based on eighteen users.</td>

</tr>

<tr>

<td>39</td>

<td>Sauer and Cuthbertson ([2003](#sau03))</td>

<td>Information technology project management in the UK</td>

<td>Risk factors</td>

<td>B</td>

<td>B</td>

<td>U</td>

<td>Information technology project management in the UK.</td>

<td>S</td>

<td>Analysis based on online survey of 1,456 information technology project managers.</td>

</tr>

<tr>

<td>40</td>

<td>Sharma, Sengupta and Gupta ([2011](#sha11))</td>

<td>Software projects</td>

<td>Risk factors (dimensions)</td>

<td>D</td>

<td>S</td>

<td>B</td>

<td>Software projects in India.</td>

<td>S</td>

<td>Analysis based on findings derived from 300 questionnaires completed by information technology professionals.</td>

</tr>

<tr>

<td>41</td>

<td>Standish Group ([1995](#sta95))</td>

<td>Information technology application development</td>

<td>Success factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>US companies with a management information system that operate in a range of industries and vary in size.</td>

<td>S</td>

<td>Analysis based on surveys completed by 365 respondents, focus groups and personal interviews with information technology executive managers.</td>

</tr>

<tr>

<td>42</td>

<td>Standish Group ([2001](#sta01))</td>

<td>Information technology application development</td>

<td>Success factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Unspecified, although domain details likely to be similar to [Standish Group (1995)](#sta95).</td>

<td>S</td>

<td>Unspecified, although methodology details likely to be similar to [Standish Group (1995)](#sta95).</td>

</tr>

<tr>

<td>43</td>

<td>Standish Group ([2009](#sta09))</td>

<td>Information technology application development</td>

<td>Success factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Unspecified, although domain details likely to be similar to Standish Group ([1995](#sta95).)</td>

<td>S</td>

<td>Unspecified, although methodology details likely to be similar to Standish Group ([1995](#sta95)). (This report, featured in electronic newsletter _CHAOS activity news,_ was received in a personal communication.)</td>

</tr>

<tr>

<td>44</td>

<td>Taylor ([2000](#tay00))</td>

<td>Information technology projects</td>

<td>Critical success factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Information technology projects in the UK.</td>

<td>I</td>

<td>Analysis derives from detailed questioning of thirty-eight members of the British Computer Society, the Association for Project Management and the Institute of Management.</td>

</tr>

<tr>

<td>45</td>

<td>Tesch, Kloppenborg and Frolick ([2007](#tes07))</td>

<td>Software development projects</td>

<td>Risks</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Software development projects in the USA.</td>

<td>S</td>

<td>Analysis based on findings derived from survey of twenty-three project management professionals and group (panel) work.</td>

</tr>

<tr>

<td>46</td>

<td>Tiwana and Keil ([2004](#tiw04))</td>

<td>Software development</td>

<td>Key risk drivers</td>

<td>D</td>

<td>U</td>

<td>C</td>

<td>Software development projects.</td>

<td>S</td>

<td>Analysis based on findings of survey of sixty-one information systems/IT directors in a variety of companies. Includes statistical analysis.</td>

</tr>

<tr>

<td>47</td>

<td>Ugwu and Kumaraswamy ([2007](#ugw07))</td>

<td>Information and communication technology projects</td>

<td>Critical success factors</td>

<td>B</td>

<td>U</td>

<td>C</td>

<td>Projects in the construction industry.</td>

<td>S</td>

<td>Analysis based on findings of a survey of forty client-based stakeholder groups.</td>

</tr>

<tr>

<td>48</td>

<td>Verner and Evanco ([2005](#ver05))</td>

<td>In-house software development</td>

<td>Project management practices</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>In-house software development, apparently in Australia and the USA.</td>

<td>S</td>

<td>Analysis based on findings derived from survey of 101 in-house development practitioners. Includes statistical analysis.</td>

</tr>

<tr>

<td>49</td>

<td>Verner, Overmyer and McCain ([1999](#ver99)</td>

<td>Software development projects</td>

<td>Success and failure factors</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Software development projects in the USA.</td>

<td>I</td>

<td>Analysis based on findings derived from interviews with twenty software developers.</td>

</tr>

<tr>

<td>50</td>

<td>Wallace and Keil ([2004](#wa04a))</td>

<td>Software development projects</td>

<td>Risk factors</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Software development projects.</td>

<td>S</td>

<td>Analysis based on findings from Web-based survey of 507 software project managers. Includes statistical analysis.</td>

</tr>

<tr>

<td>51</td>

<td>Wallace, Keil and Rai ([2004](#wa04b))</td>

<td>Software projects</td>

<td>Risks</td>

<td>D</td>

<td>U</td>

<td>U</td>

<td>Software projects.</td>

<td>S</td>

<td>Analysis based largely on findings from a survey of 507 software project managers. Includes statistical analysis.</td>

</tr>

<tr>

<td>52</td>

<td>Walsh and Kanter ([1988](#wal88))</td>

<td>Application development</td>

<td>Major problem areas</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>Application development.</td>

<td>C</td>

<td>Case study.</td>

</tr>

<tr>

<td>53</td>

<td>Warkentin, Moore, Bekkering and Johnston ([2009](#war09))</td>

<td>Information systems development projects</td>

<td>Risks</td>

<td>D</td>

<td>U</td>

<td>I/C</td>

<td>Information systems development projects.</td>

<td>S</td>

<td>Analysis based on findings derived from two different open-ended questionnaires administered in two stages to eight IT professionals.</td>

</tr>

<tr>

<td>54</td>

<td>[Wateridge (1995)](#wat95)</td>

<td>Information technology projects</td>

<td>Success factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Information technology projects.</td>

<td>U</td>

<td>Analysis of prior research.</td>

</tr>

<tr>

<td>55</td>

<td>Wohlin and Andrews ([2002](#woh02))</td>

<td>Software projects</td>

<td>Success drivers</td>

<td>D</td>

<td>I</td>

<td>I</td>

<td>Software projects.</td>

<td>C</td>

<td>Case study of twelve software projects from various divisions of a single company.</td>

</tr>

<tr>

<td>56</td>

<td>Yeo ([2002](#yeo02))</td>

<td>Information system projects</td>

<td>Critical failure factors</td>

<td>U</td>

<td>U</td>

<td>U</td>

<td>Information system projects.</td>

<td>S</td>

<td>Analysis based on findings derived from a survey of 92 participants associated with a failed IT project. Includes statistical analysis.</td>

</tr>

<tr>

<td colspan="10">**Key**  
C: Classification (D: development only, B: both development and packaged system implementation, U: unspecified, but development verified in or inferred by article content).  
D: Development (B: both, I: in-house, S: supplier-based, U: unspecified).  
P: Perspective (B: both, C: client, I: in-house, S: supplier, U: unspecified).  
DS: Methods or data source (A: anecdotal; C: case study; I: interviews; O: other; P: panels; S: survey, U: unspecified).</td>

</tr>

</tbody>

</table>