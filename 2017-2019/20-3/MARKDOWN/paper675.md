#### vol. 20 no. 3, September, 2015

# Towards a definition of serendipity in information behaviour

#### [Naresh Kumar Agarwal](#author)  
School of Library and Information Science, Simmons College, 300 The Fenway, Boston, Massachusetts, 02115, USA

#### Abstract

> **Introduction**. Serendipitous or accidental discovery of information has often been neglected in information behaviour models, which tend to focus on information seeking, a more goal-directed behaviour.  
> **Method**. This theoretical paper seeks to map the conceptual space of serendipity in information behaviour and to arrive at a definition. This is done through carrying out a literature review on information behaviour and serendipity and defining relevant terms in the area. Using Wilson’s framework as a starting point, a series of frameworks is arrived at to include serendipity in information behaviour models.  
> **Analysis**. The terms used in this research space were investigated, as well as the times when serendipitous finding can occur, the dimensions of serendipitous findings, and a series of assumptions to draw out the key elements of serendipity.  
> **Results**. The results of the analysis is a framework of continuums that identifies the core of this research area of serendipity in information behaviour and arrives at its definition.  
> **Conclusions**. By including serendipity in information behaviour models, the frameworks arrived at should help further research in this area. A working definition of serendipity in information behaviour is a starting point for other researchers to investigate related questions in the area.

## Introduction

Serendipitous discovery of information is different from purposive information seeking, as it is more about encountering or stumbling upon information when not directly looking for it ([Erdelez 1995](#erd95), [1997](#erd97), [2005](#erd05); [Toms 2000a](#tom00a), [2000b](#tom00b); [Cunha, 2005](#cun05); [Lawley and Tompkins 2008](#law08); [McCay-Peet and Toms, 2010](#mcc10); [Makri and Blandford 2012a](#mak12a), [2012b](#mak12b)), often drawing a reaction of happiness, surprise or simply an ahah! moment (and, sometimes, disappointment as well). We think of serendipity as chance finding of pertinent information, either when not looking for anything in particular or when looking for information on something else ([Cunha, 2005](#cun05); [McCay-Peet and Toms, 2010](#mcc10)). Both are of interest in this paper.

Serendipity has long been of concern in the field of information science, going back, for example, to the 19th century cataloguing and classification schemes of Cutter, as means to ensure '_the quick finding of any particular book_' ([1876](#cut76), p. 526). Other early examples of encouraging creativity and exploration can be seen in Toms ([2000a](#tom00a)) where she cites, among others, Grose and Line ([1968](#gro68)), who suggested that books be randomly shelved to facilitate novel browsing.

However, serendipity has often been inferred but not explicitly stated in past works on information behaviour. For example, Bates’ ([1989](#bat89)) ‘berrypicking’ model and Wilson’s ([1999](#wil99)) nested model infer but do not explicitly state serendipitous behaviour. While theories, models and frameworks of information seeking behaviour (which deal with goal-oriented or task-based processes; see Wilson ([1999](#wil99)) and Case ([2012](#cas12)) for a review) need not include serendipity, the discovery of information by chance or accident does need to find an explicit place in models and frameworks of information behaviour. This is because information behaviour implies a wider term that includes activities other than purposive information seeking.

Thus, serendipity needs to find its place in models of information behaviour. Also, there has not been enough clarity as to what constitutes the core of the research area of serendipity in information behaviour. What are the distinguishing aspects of this form of information behaviour? What is the appropriate terminology? What is its conceptual space within the broader field? In this theoretical paper, I attempt to map the conceptual space of serendipity in information behaviour and arrive at a definition through a series of diagrammatic frameworks. The shared terminology and the mapping are intended to help further research in this area.

## Literature review

A review of the literature was carried out using the comprehensive texts Case ([2012](#cas12)), Cole ([2012](#col12)), Fisher, Erdelez and McKechnie ([2005](#fis05)), information science journals such as the _Journal of the Association for Information Science & Technology_, _Information Research_, _Journal of Documentation_, as well as an online search using Google scholar. The search strategy focused on terms such as serendipity in information behaviour, information encountering, chance finding, etc. From the large number of references retrieved, eighty-two were found to be pertinent and used. A content analysis of most retrieved articles was performed which focused on the objective of the study, method used, findings, and the key themes that would be applicable to the paper.

The literature review is organized into two broad sections: information behaviour and serendipity within information behaviour. Under each section, I have included the definitions of the salient terms used throughout the paper. As Line ([1974](#lin74)) wrote, and which continues to hold today, the literature on ‘user needs’ has been confused by imprecise use of terms and studies whose investigators have purported to be concerned with needs, when they have in fact examined uses or demands.

### Information behaviour

Through an analysis of various frameworks, Wilson ([1999](#wil99)) created a nested model (see Figure 1) that shows the relationship between information searching, information seeking and information behaviour. The model nests searching on computer-based systems within seeking, and seeking within behaviour. I use Wilson’s nested model as the theoretical lens for this paper. Wilson’s model provides a simple representation of existing models in the field of information behaviour. The model does not explicitly deal with serendipity, although it does infer serendipitous behaviour in the term information behaviour, of which seeking is part.

<figure class="centre">![Figure1: Wilson’s (1999) nested model](p675fig1.jpg)

<figcaption>Figure 1: Wilson’s ([1999](#wil99)) nested model</figcaption>

</figure>

In Figure 1, information behaviour may be seen as a more general field of investigation, subsuming seeking and searching, as well as the totality of other unintentional or passive activities that do not involve seeking, such as avoiding information ([Wilson, 1999](#wil99); [Case, 2012](#cas12)). Courtright ([2007](#cou07)) wrote that the term information behaviour might be considered shorthand for the cumbersome information needs, seeking and use.

Information seeking has been defined as a conscious effort to acquire information in response to a need, want or gap in our knowledge ([Case, 2012](#cas12)). Allen ([1996](#all96)) defined information seeking as _‘the behaviour that is the directly observable evidence of information needs [or wants] and the only basis upon which to judge both the nature of the need [or want] and its satisfaction’_ (p.56). There have been a large number of theories, models and frameworks of information seeking such as those of Wilson ([1981](#wil81)), Wilson and Walsh ([1996](#wil96)), Dervin ([1983](#der83), [1992](#der92)), Krikelas ([1983](#kri83)), Kuhlthau ([1991](#kuh91)), Ellis ([1989](#ell89)), Bates ([1989](#bat89)), Ellis, Cox and Hall ([1993](#ell93)) and Saracevic ([1996](#sar96)), among others (see Wilson ([1999](#wil99)) and Case ([2012](#cas12)) for a review). Dervin’s ([1983](#der83), [1992](#der92)) sense-making theory, which postulates constant information seeking behaviour by humans in order to make sense of their environment, has subsequently been of significance in information behaviour research.

Information searching, on the other hand, is _‘a subset of information seeking, particularly concerned with the interactions between information user [actor]… and computer-based information systems’_ ([Wilson, 1999](#wil99), p. 263).

Since the user is at the heart of information seeking behaviour, the subject of what goes on in this person’s head leading to a search for information is incorporated in most models of information behaviour. This is often referred to as the person’s information need. See Case ([2012](#cas12), pp. 77-89) for a review of well-known perspectives on information need such as Belkin’s ASK or anomalous state of knowledge, Dervin’s gap in understanding (questions, confusions, muddles, riddles, angst), Atkin’s uncertainty and Taylor’s typology of information needs.

Taylor’s ([1968](#tay68)) typology has had significant influence on the literature on information behaviour. He outlined human need for information along a continuum with a series of question formations moving from Q1 to Q4\. Q1 is the actual, but unexpressed, need for information (the visceral need). He termed it a vague sort of dissatisfaction that is probably inexpressible in linguistic terms. Q2 is the conscious, within-head description of the need (the conscious need) that Taylor described as an ambiguous and rambling statement. At this stage, actors may talk to someone else to sharpen their focus. Q3 is the formal statement of the need (the formalized need), where actors are describing their area of doubt in concrete terms. Q4 is when the question is presented to the information source or system (the compromised need) wherein actors present their formalized need in the way the source or system would understand.

A theory and deeper exploration of information need are addressed in Cole ([2012](#col12)), who begins with Taylor’s typology, examines it from an information science and a computer science perspective, and proposes a theory connecting information search to knowledge formation. He views information need as an adaptive human mechanism driving humans to seek out, recognise and then adapt to changes in their social and physical environments.

Taylor’s typology was written in the context of actors seeking information from a reference librarian in a special library. Examining this from the point of serendipitous encounter of information (when one is not really seeking), the most applicable is Q1, or the visceral need. Taylor’s Q1 infers serendipity or may in fact explain how it occurs because the Q1 level of information need is not known to the searcher. The actor does not really put in the effort to take Q1 and transform it to Q2, Q3 or Q4 or initiate a formal search for information. It is only when information is encountered fortuitously that the actor associates the information with an unsatisfied Q1 and can now use it in some way. However, it is possible that the visceral need (Q1) has progressed to the Q2 or Q3 stage when the actor encounters information accidentally.

The information need or want is often created by a task at work or a problem situation that a person is encountering, as exemplified by many investigators of studies that bind the search context to a task or problem situation at hand ([Courtright, 2007](#cou07); [Agarwal, Xu and Poo, 2009](#aga09); [Agarwal, Xu and Poo, 2011](#agax11)). Several authors have looked at task-based information seeking (e.g., [Agarwal, Xu and Poo, 2011](#agax11); [Zach, 2005](#zac05); [Fidel and Pejtersen, 2004](#fid04); [Jarvelin and Ingwersen, 2004](#jar04); [Kuhlthau, 1996](#kuh96), [1997](#kuh97); [Leckie and Pettigrew, 1997](#lec97)).

A problem situation can also occur in everyday life settings (see the works of Savolainen ([1995](#sav95)), McKenzie ([2003](#mck03)), Agosto and Hughes-Hassell ([2006a](#ago06a), [2006b](#ago06b)) and Abbas and Agosto ([2013](#abb13))) that creates an information need or want and requires a person to look for information (information demand). Studies of this type include those by Julien and Michels ([2004](#jul04)), Rieh ([2004](#rie04)), Ikoja-Odango and Ocholla ([2003](#iko03)), Dervin ([1997](#der97)), Harris and Dewdney ([1994](#har94)) and several others. See Courtright ([2007](#cou07)) for a discussion. In the works of Agosto and Abbas, the focus is on the everyday life information behaviour of young people.

Table 1 includes definitions of some of the terms used throughout the paper. Whenever possible, Line’s definitions have been incorporated below.

<table class="center"><caption>  
Table 1: Definitions of terms</caption>

<tbody>

<tr>

<td>Actor</td>

<td>Seeker, user or person who is looking for information or who finds information on something unexpectedly. According to Dervin’s sense-making methodology, this actor is a _‘body-mind-heart-spirit moving through time and space, with a past history, present reality and future dreams or ambitions’_ (Foreman-Wernet, 2003, p.7; Agarwal, 2012).</td>

</tr>

<tr>

<td>Information need</td>

<td>Line (1974) distinguished need from want, demand, use and requirement. He defined need as what the actor _ought_ to (or should) have, for one’s work, research, edification, recreation, etc., and compares need to necessity.</td>

</tr>

<tr>

<td>Information want</td>

<td>Line (1974) defined _want_ as what the actor _would like_ to have. Line wrote that individuals may need an item they do not want, or want an item they do not need. In today’s context, it could be persons wanting to buy a tablet computer because they see someone else with one, but do not really need it.</td>

</tr>

<tr>

<td>Information seeking/demand</td>

<td>Line defined _demand_ as what the actor actually asks for (either to a computer-based system, a colleague or a library). Information seeking is the same as demand and is defined as a conscious effort to acquire information in response to a gap in knowledge (Case, 2012) that may be a need or a want. In terms of Taylor’s (1968) typology, this would map to _formalized_ or _compromised_ need.</td>

</tr>

<tr>

<td>Information requirement</td>

<td>Line proposed requirement as a useful bridging term to mean what is needed, what is wanted or what is demanded or sought, one that could be employed to cover all three categories.</td>

</tr>

<tr>

<td>Get</td>

<td>The information that the actor actually gets (that might satisfy a need, a want or a demand – a requirement).</td>

</tr>

<tr>

<td>Context</td>

<td>All those factors surrounding and influencing the actor’s information seeking behaviour may be loosely understood as _context_. This would include need and want, other personal attributes of the actor such as prior knowledge, and the actor’s work role, team dynamics, organizational issues, etc. Dervin described context as power structures and dynamics, domain knowledge systems, cultures and communities (Agarwal, 2012).</td>

</tr>

<tr>

<td>Information use</td>

<td>Line (1974) defined use as what the actor actually uses. A use may be a satisfied demand (the result of seeking) or it may be the result of a serendipitous finding or encounter.</td>

</tr>

</tbody>

</table>

Definitions of other terms pertaining to information science that are used but not listed above may be found in Case ([2012](#cas12)). Those listed are the most salient for this paper.

### Serendipity in information behaviour

A number of authors of past studies have looked at serendipity in information behaviour. Sanda Erdelez coined the term _information encountering_ to explain the phenomenon (see [Erdelez, 2005](#erd05); also see [Erdelez, 1995](#erd95), [1997](#erd97), [1999](#erd99), [2004](#erd04); [Erdelez and Rioux, 2001](#erd01)). Another important work based on a Ph.D. dissertation is by Williamson (see [1998](#wil98)) who used the term _incidental information acquisition_ to describe this chance discovery of information by her respondents.

Cunha ([2005](#cun05)) discussed four building blocks of serendipity in an organizational context: the precipitating conditions that facilitate serendipitous discovery, the search for a solution for a given problem, a process of bisociation leading to the combination of previously unrelated skills or information, and the discovery of an unexpected solution to a different problem. Building upon Cunha ([2005](#cun05)), McCay-Peet and Toms ([2010](#mcc10)) focused on understanding the precipitating conditions that must be present to facilitate serendipity. Their results suggest that serendipity occurs during social networking and active learning, and more specifically in the act of exploratory search (this would map to browsing and scanning as discussed by Case ([2012](#cas12))), or at the visceral or conscious need stage of Taylor ([1968](#tay68)). McCay-Peet and Toms ([2010](#mcc10)) also found that serendipity is not always instant and that a period of incubation is sometimes necessary before serendipity is recognized.

Lawley and Tompkins ([2008](#law08)) proposed a perceptual model of serendipity wherein a person with a prepared mind encounters an unexpected event, recognizes a potential, seizes the moment, amplifies the effects and then evaluates them. They cautioned that while the serendipitous event ‘gets all the press’, everything that happens before and after is as important, if not more so. Raising one’s awareness of serendipity and how it works will mean a more prepared mind. Makri and Blandford ([2012a](#mak12a)) conducted semi-structured critical incident interviews with twenty-eight researchers across disciplines to unearth examples of coming across information serendipitously in their research or everyday life. Building on Lawley and Tompkins’ ([2008](#law08)) work, Makri and Blandford proposed a process-model of serendipity (see Table 2 for their definition of serendipity).

Serendipity has been studied in many different fields. Israel Kirzner’s ([1997](#kir97)) much cited work on entrepreneurial discovery and the competitive market process has important implications for charting the conceptual space of serendipity as it applies to information behaviour. Shapiro ([1986](#sha86)), Rosenman ([1988](#ros88)) and Roberts ([1989](#rob89)) have written about the role and value of serendipity in scientific investigation. Sawaizumi, Katai, Kawakami and Shiose ([2007](#saw07)) applied the concept of serendipity in the field of education. Beheshti, Large and Clement ([2008](#beh08)) designed a virtual reality library to assist children and young adults in browsing online information for educational projects, where serendipitous information can be tested. Of course, serendipity can have various other applications, from libraries and archives, to hospitals and organizations, and in fields ranging from law to medicine to psychology to sociology to knowledge management.

Methodologies for studying the role of serendipity in information behaviour have largely used qualitative approaches grounded in in-depth interviews, as well as surveys ([Erdelez, 1997](#erd97); [Erdelez and Rioux, 2001](#erd01); [Williamson, 1998](#wil98); [Foster and Ford, 2003](#fos03); [Duff and Johnson, 2002](#duf02)). Fine and Deegan ([1996](#fin96)) discuss the role of serendipity in qualitative research. Erdelez ([2005](#erd05)) also wrote about research in a controlled environment (for example, [Toms, 2000b](#tom00b), [Campos and de Figueiredo, 2001](#cam01), [Erdelez, 2004](#erd04)). Rubin, Burkell and Quan-Haase ([2010](#rub10)) examined secondary data (blogs) created by bloggers to explore serendipity as described in social media. Ross ([1999](#ros99)), in her study based on interviews with 194 committed readers, looked at information encountering in the context of reading for pleasure. Foster and Ford ([2003](#fos03)) examined the role of serendipity in the behaviour of forty-five university students and faculty. They reported serendipity arising from both conditions and strategies, and as both purposive and non-purposive in the process of information seeking or acquisition. See the special issue of Information Research on serendipity ([Information Research, 2011](#inf11)) for more examples of methodologies used to study serendipity in information behaviour. Other methods of data collection, ranging from surveys to experimental research, could be applied to serendipity as well.

In Table 2 below, the remainder of the terms used in this paper are defined as I model the conceptual space and move toward a definition of serendipity in the research field of information behaviour.

<table class="center"><caption>  
Table 2: Definitions of terms pertaining to serendipity in information behaviour  
</caption>

<tbody>

<tr>

<td>Actor in _non-purposive_ or passive information seeking mode</td>

<td>When the actor is _not_ actively looking for (i.e. seeking or demanding) information.</td>

</tr>

<tr>

<td>Actor in _purposive_ or active information seeking mode</td>

<td>When the actor is looking for information to satisfy a particular goal/purpose (need or want), typically arising due to a task that one must work on, but might be recreational. The term _information seeking_ or demand may be understood as _purposive information seeking_.</td>

</tr>

<tr>

<td>Current information need or want (n<sub>current</sub>)</td>

<td>The current information need/want of the seeker based on the task at hand or the seeker’s curiosity. This would map to the _conscious need_ in Taylor (1968).</td>

</tr>

<tr>

<td>Different information need/want than the current context (n<sub>different</sub>)</td>

<td>An information need or want of the seeker relating to a different context than the current one, which might have arisen sometime in the near or distant past (or simply a need or want within oneself if there is no active search going on). This would map to Taylor’s (1968) _visceral_ need at most times (but may have progressed to the _conscious_ need stage) – an actual, but unexpressed need, a vague dissatisfaction felt at some point in the past.</td>

</tr>

<tr>

<td>Information being sought or demanded (i<sub>current</sub>)</td>

<td>Information that is currently being sought or demanded by the seeker to satisfy the current information need or want (n<sub>current</sub>).</td>

</tr>

<tr>

<td>Serendipity</td>

<td>Makri and Blandford (2012a) proposed a process-model of serendipity wherein they define serendipity as 1) making a new connection involving a mix of unexpected circumstances and insight; 2) projecting value of outcome; 3) exploiting the connection, leading to 4) a valuable, unanticipated outcome; and 5) reflecting on the value of the outcome; all the while 6) reflecting on unexpectedness of circumstances that led to connection and/or role of insight in making the connection.  

I define serendipity as an incident-based, unexpected discovery of information when the actor is either in a passive, non-purposive state or in an active, purposive state, followed by a period of incubation leading to insight and value.  

I extend this definition toward the end of this paper.</td>

</tr>

<tr>

<td>Serendipitous finding of information (i<sub>chance</sub>)</td>

<td>Information encountered serendipitously or accidentally which does not address a current need (n<sub>current</sub>) but rather addresses a different information need/want (or simply a _visceral need_ if the actor is not engaged in any active search for information). Line (1974) termed this _accident_ and called it information recognized as a need or a want when received, although not previously articulated into a demand.  

This is different from _get_, which may include information that is needed, wanted, demanded or sought.</td>

</tr>

</tbody>

</table>

## Conceptual space

In the framework below, I extend Wilson’s ([1999](#wil99)) framework (Figure 1) to show the place of serendipitous information discovery within information behaviour. This has been mapped as distinct from the concept of information seeking (or purposive information seeking), although there might be overlaps.

<figure class="centre">![Figure2: Seeking versus finding: placing serendipity within information behaviour](p675fig2.jpg)

<figcaption>Figure 2: Seeking versus finding: placing serendipity within information behaviour</figcaption>

</figure>

I will explore the circle of serendipitous information finding or encountering further in the sub-sections below.

### Terminology: what do we call this research space?

Serendipity is any situation where an actor or user is not necessarily seeking or looking for information. Instead, the person accidentally or serendipitously encounters the information at a moment in time. When that happens, the feeling, reaction or outcome is usually _happy_ or _beneficial_ (but may be disappointing as well, depending on the information encountered).

When applied to the field of information behaviour, researchers have difficulty agreeing on a common term for serendipity. Suggestions range from serendipity to accidental, incidental or chance encountering or discovery of information. Case ([2012](#cas12)) defined serendipity as ‘_the seemingly accidental discovery of relevant information_’ (p.37). Erdelez ([1997](#erd97)) called it _information encountering_ based on her research of _accidental acquisition of information_ among 132 information users in an academic environment ([Erdelez, 2005](#erd05)). A participant in the 2010 Workshop on Opportunistic Discovery of Information at the University of Missouri highlighted a possible negative connotation associated with the term _accidental_ as used in _accidental acquisition of information_ or _accidental information encountering_, especially for non-native speakers of English. Other terms that have been used are _opportunistic discovery of information_ or _unintentional_ discovery of information. Erdelez ([2005, p.179](#erd05)) also uses _opportunistic acquisition of information_. Williamson ([1998](#wil98)) called it _incidental information acquisition_. The call for papers for a special issue in Information Research had the terms ‘_information by chance: opportunistic discovery of information’_ ([Information Research, 2011](#inf11)). A 2011 workshop in Lisbon, Portugal during INTERACT 2011 had the phrase '_encouraging **serendipity** in interactive systems'_ in its title. A 2012 workshop in Montreal, Canada was termed _SCORE: Serendipity, Chance and Opportunity in Information Discovery_.

If we look closely at all the commonly used terms listed above, we find that they relate to two aspects: the first is the act of finding (which is also called encountering, acquisition, discovery or learning), the second is the serendipitous nature of this finding (which is also called accidental, opportunistic, active, incidental or unintentional). Only when we combine any of these latter adjectives to any of the terms pertaining to the act of finding can we describe the phenomenon as serendipitous.

### When can serendipitous finding occur?

Ross ([1999](#ros99)) examined non-goal-oriented transactions with text to investigate the information encountered in the context of daily living. Most information science researchers view individuals as experiencing a problem situation before formally initiating a search from a source such as an individual, online resource, library reference, etc., whereby the researcher asks the respondent to first think of a specific task or problem situation ([Ross, 1999](#ros99), p. 784, [Agarwal, Xu and Poo, 2011](#agax11)). Even in research on _everyday life_ information seeking, wrote Ross, it is assumed that the respondents have problem situations, though the focus is more on various types of barriers that make it hard for outsider groups ([Chatman, 1996](#cha96)) to seek information purposely from system-sources. Erdelez ([2005](#erd05)) defined information encountering as _‘an instance of accidental discovery of information during an active search for some other information’_ (p.180). According to her definition, information encountering would occur during active or purposive search or seeking ([Wilson and Walsh, 1996](#wil96); [Wilson, 1999](#wil99)) but may not occur in a situation when one is not actively seeking.

However, I would like to posit that encountering or stumbling upon (a term used in a popular Website, StumbleUpon) can happen at any point, whether or not one is actively seeking. For example, while waiting for a bus at a bus stop, one might encounter information about an apartment for rent pinned to a board. During this time, the person is not in a state of active search for some other information, yet the instance of finding, encountering or stumbling upon information happens. The stumbling upon does satisfy an information need, albeit, one relating to a different context than the present. Apted ([1971](#apt71)) termed serendipity a special case of _browsing_ for which purposive seeking is not required. Thus, the incident of serendipitous finding might happen during an active process of seeking, i.e. finding B when looking for A ([Erdelez, 2005](#erd05)), or at times when one is not actively looking for anything, i.e., finding something when not looking (_non-purposive seeking_ or other non-purposive processes such as browsing, scanning, foraging, grazing, navigating etc. ([Case, 2012](#cas12), p. 100-102).

Thus, serendipitous finding could occur in one of two hypothetical scenarios. Take for example, an actor who is:

*   Scenario 1) not actively searching for anything (non-purposive/passive information seeking) (imagine Isaac Newton walking in his gardens in the late seventeenth century) or
*   Scenario 2) actively searching for information (i<sub>current</sub>) based on a current information need (n<sub>current</sub>) (purposive/active information seeking) (imagine Alexander Fleming searching for a cure for influenza in the early twentieth century)

Figure 3 below attempts to place serendipity within information behaviour when the actor is in a purposive or non-purposive state of searching.

<figure class="centre">![Figure3: Placing serendipity within information behaviour when not actively seeking (scenario 1, left e.g., Newton) and during active seeking or search (scenario 2, right e.g., Fleming)](p675fig3.jpg)

<figcaption>Figure 3: Placing serendipity within information behaviour when not actively seeking (scenario 1, left e.g., Newton) and during active seeking or search (scenario 2, right e.g., Fleming)</figcaption>

</figure>

The nested circle on the left (in Figure 3) corresponds to scenario one, while that on the right corresponds to scenario two. Serendipitous information finding has been placed in the intersection of the two circles as it could occur in both the scenarios. For scenario two (on the right), seeking and searching are both included, as serendipity might happen either in an everyday life setting (seeking) or when interfacing with or through a computer-based information system (searching).

### Dimensions of serendipitous findings

Let us further explore our list of terms discussed earlier in this section. In the first aspect, while learning or acquisition relate to a process with an active intent, encountering, finding and discovery speak to a specific instance in time. In the second aspect, while the terms accidental, incidental, serendipitous, unintentional and chance speak to an incident or occurrence, the terms opportunistic and active bring within them a variable of the actor or person. Opportunistic or active denote a state of motion, progress or energy engaged in by the actor and are tied to the next step of _exploiting_ the opportunity or a possible action resulting from the discovery of the information. These contradictions in _not intended_ versus _variable of the actor_ point to different dimensions of serendipitous encounters:

> During a passive phase (including browsing, scanning or non-purposive seeking):  
> 1\. {accidental, incidental, serendipitous, unintentional or chance} {encountering, finding, stumbling upon, acquisition or discovery} of information.  
> During an active (purposive seeking or search) phase:  
> 2\. {accidental, incidental, serendipitous, unintentional or chance} {encountering, finding, stumbling upon, acquisition or discovery} of information.  
> 3\. {opportunistic or active} {encountering, finding, stumbling upon, acquisition or discovery} of information.

Here, dimension one is the most passive, non-purposive and incident-based (finding something purely by luck or chance when not really looking). Dimension two is a chance encounter during an active process of seeking (closer to what Erdelez ([2005](#erd05)) implies in her definition of information encountering). Dimension three implies an active, purposive and process-based method of finding information during an active seeking process (where you expect to find unexpected information).

Thus, serendipitous (or accidental, incidental, unintentional, chance) finding (or encountering, stumbling upon, acquisition, discovery) of information can happen either during a passive phase when the actor is not looking, or an active phase when the actor is looking for information relating to a different context than the one encountered. Also, when the actor is in an active state of seeking, s/he might be engaged in an opportunistic or active discovery of information of value to the actor.

We will now discuss a series of assumptions to help us delineate the core of our research area and help us arrive at a definition of serendipity in information behaviour.

### Assumption 1: the visceral information need of the actor

As we talk of information finding as distinct from searching and seeking, there has to be a search within (in one’s subconscience). This would relate to a different context than the current one (n<sub>different</sub> as in Table 2 – _visceral or conscious_ need as proposed by Taylor ([1968](#tay68))). Thus, even though the information encountered might not be directly addressing an immediate information requirement (n<sub>current</sub> in Table 2) due to a gap, uncertainty or anomalous state of knowledge ([Belkin, Oddy and Brooks, 1982](#bel82)), it would address a _visceral_ (or _conscious_) information need or want residing within the mind and relating to a different context (n<sub>different</sub>).

In both of the scenarios relating to Newton and Fleming discussed above, it is possible that they (Newton or Fleming) may serendipitously discover information (i<sub>chance</sub>). For example, in scenario one, Newton noticed that the apple fell downwards from a tree and not in any other direction. In scenario two, Fleming noticed that bacterial cultures were being infected with a strange mould. In both the scenarios, the discovered information does not address the need (n<sub>current</sub>). Some passive information need might have been in Newton’s mind at that point. Fleming was engaged in an active search for the cure for influenza. However, the new information addresses another different need for information (n<sub>different</sub>) that the actor is currently feeling or has felt in the past. This might be a visceral need – an unexpressed, vague dissatisfaction ([Taylor, 1968](#tay68)) - or a gap or unease or uncertainty ([Case, 2012](#cas12), p.77-89) about not understanding something fully, as Newton or Fleming might have felt. The discovery of i<sub>chance</sub> might even cause a new need or interest to arise ([Case, 2012](#cas12), p. 100).

### Assumption 2: unexpectedness or degree of surprise

The information encountered often leads to a feeling of happiness, shock, surprise, awe or simply an aha! moment. This happens because the phases until that moment (see the initiation and exploration phases of Kuhlthau’s ([1991](#kuh91), p. 367) framework of the information search process) are often marked with feelings (affective) of _uncertainty_ or _confusion, frustration, doubt_ and _general or vague_ thoughts (cognitive). The serendipitous encounter and resulting insight (this could be mapped to the _recognise_ aspect of Kuhlthau’s framework) is likely to spring the actor out of these affective and cognitive conditions, resulting in the _aha!_ moment.

However, at times, it might lead to disappointment as well. For example, if a researcher accidentally comes across a published article which has similar findings to the project s/he has been working on for the past few months, the experience is likely to be one of disappointment (as opposed to a happy feeling), though just as serendipitous as other accidental encounters. Lawley and Tompkins ([2008](#law08)) related this encountering to a _prepared mind_, while Makri and Blandford ([2012a](#mak12a)) termed it _insight_.

At this point, the person may either file the information (i<sub>chance</sub>) in memory or bookmark it (if searching in a computer-based system as opposed to seeking in an everyday life setting). When the information (i<sub>chance</sub>) is encountered under scenario two when the need (n<sub>current</sub>) is still not met, it is possible that the person continues the search for i<sub>current</sub> until the need (n<sub>current</sub>) is met (either fully or partially) or until s/he gives up. Case ([2012](#cas12)) interestingly points out that the searcher always gives up eventually, because there is always more that could be known regarding a topic. It is also possible that the discovery (i<sub>chance</sub>) is so significant that the actor might give up the search for i<sub>current</sub> at that point (as might have happened with Fleming when he discovered penicillin serendipitously). Thus, in the first scenario, serendipity happens within a cycle of non-purposive or passive seeking. This could be when the actor is browsing, scanning ([Case, 2012, p.100-102](#cas12)) or engaged in another activity where the person is not looking for anything in particular (like Newton meandering in his garden). In the second scenario, serendipity happens within a cycle of purposive or active seeking or search when i<sub>chance</sub> is encountered.

### Assumption 3: serendipitous alertness

People exhibiting a high degree of serendipitous alertness and those who encountered information frequently (super-encounterers ([Erdelez, 1997](#erd97)); prepared mind ([Lawley and Tompkins, 2008](#law08)) are likely to witness less surprise or less of an _ahah!_ moment as compared to those who encountered information less frequently. These super-encounterers might engage more in opportunistic finding or active discovery.

### Insight and value

Makri and Blandford ([2012b](#mak12b)) proposed a framework for subjectively classifying whether or not a particular experience might be considered serendipitous and the degree of serendipity (from purer to dilute) involved. Their framework has different possible mixes of unexpectedness (see the discussion under assumption 2 above), insight and value for experiences that are considered to be serendipitous. According to their framework, the greater the degree of unexpectedness, the greater the degree of insight, and the greater the degree of value, the purer is the degree of serendipity. As one or more of these become weaker, Makri and Blandford termed it a dilution of the degree of serendipity.

Based on the particular information encountered, and the degree of insight or value the actor attaches to it, an actor might exude a bigger _AHA!_ when encountering something of higher perceived value unexpectedly, as compared to other encounters by the same actor (a smaller _aha!_).

### Incident (with incubation) versus process

The information-seeking mode of the actor can vary along the continuum between passive (when the actor is not looking for anything in particular) and active (when the actor is engaged in a process of purposive seeking or searching for information). The goal or intentionality of the actor can vary between non-purposive (during passive phase) and purposive (during active phase). The act of finding information by chance will always be an incident, though it might be followed by a period of incubation before the actor realizes that the information is of value ([McCay-Peet and Toms, 2010](#mcc10)). However, as the actor gets more opportunistic or active in looking for information (e.g., being on the lookout for useful Websites or books in a library or bookstore), s/he gets more used to the process of finding information. The degree of unexpectedness reduces in such a case.

Having discussed various aspects of serendipity in information behaviour, let us now explore the defining elements of this area.

### The core of the research area

For any research field or area, it is important to underline its defining characteristic. As Ken said about the field of information systems, _‘there is a need for reflection on the field, its roots, relations with other disciplines and historical context’_ ([Keen, 1980](#kee80)). For that field, Benbasat and Zmud ([2003](#ben03)) talked about the problem of identity crisis, while other researchers spoke of fragmented adhocracy ([Hirschheim and Klein, 2003](#hir03)) or the danger of the discipline being subsumed by other disciplines in the absence of an intellectual core of research questions, protocols and standards in the field ([Fitzgerald and Adam, 1996](#fit96)). To define the core of the field of information systems, Benbasat and Zmud suggested the centrality of the information technology-artefact: _‘how to best design IT artifacts and IS systems to increase their compatibility, usefulness, and ease of use or how to manage and support IT or IT-enabled business initiatives’_ ([2003](#ben03), p. 191-192).

Similar concerns would apply when trying to establish the place of serendipity within the wider umbrellas of information seeking, information behaviour and information science. What is the core of this research area? What would be the defining elements that would distinguish this research area from the rest?

Explicating his notion of entrepreneurial discovery as it applies to the field of economics, Kirzner wrote:

> what distinguishes discovery (relevant to hitherto unknown profit opportunities) from successful search (relevant to the deliberate production of information which one knew one had lacked) is that the former (unlike the latter) involves that surprise which accompanies the realization that one had overlooked something in fact readily available. (‘It was under my very nose!’) This feature of discovery characterizes the entrepreneurial process of the equilibrating market (Kirzner, ([1997](#kir97), p.72).

Participants in the _2010 Workshop on Opportunistic Discovery of Information_ at the University of Missouri deliberated whether this core is the element of surprise. Should it be shock? Is this part of the outcome? Does this have to be positive? Can it be negative (e.g., encountering information that someone has cancer)? While surprise came close, the participants concluded that the emotion was more of an _ahah!_ moment.

Makri and Blandford ([2012b](#mak12b)) added insight and value to this unexpectedness or degree of surprise to define serendipity. However, there are other attributes that also need to be taken into consideration before arriving at a definition for serendipity in information behaviour. In Figure 4, I attempt to map the discussion thus far in a framework of continuums.

<figure class="centre">![Figure4: Research area: framework of continuums](p675fig4.jpg)

<figcaption>  
Legend: 1: Information seeking behaviour; 2: Browsing, scanning, non-purposive seeking; 3: Purposive seeking, searching; 4: With period of incubation; 5: Degree of surprise (aha! moment or disappointment); 6: Accidental, incidental, serendipitous, unintentional, chance; 7: Encountering, finding, stumbling upon, acquisition, discovery; 8: Opportunistic, active.  

Figure 4: Research area: framework of continuums</figcaption>

</figure>

The framework in Figure 4 shows that the actor could either be in an active, purposive seeking mode or a passive non-purposive mode when s/he encounters information by chance (the actor mode continuum and the intentionality continuum in the framework). The act of finding is an incident (followed by the period of incubation) when one is engaged in natural alertness, as opposed to a process-based approach used by those who engage in opportunistic finding or active discovery (the three continuums of _incident vs. process based_, alertness and terminology used). As per our earlier discussion and Makri and Blandford’s ([2012b](#mak12b)) framework, the three continuums of insight, value and unexpectedness are important in mapping the conceptual space of serendipity in information behaviour.

As we move from serendipitous encountering to opportunistic or active discovery (from the left to the right side of the framework), the process-based aspect is similar to that found in models and frameworks of information seeking behaviour (and in the models of Lawley and Tompkins ([2008](#law08)) and Makri and Blandford ([2012a](#mak12a))).

Thus, the core of this area (to distinguish it from information seeking behaviour) can be characterized by the presence of the eight continuums of Figure 4\. These help determine the degree, type and frequency of serendipitous encounter taking place. These also map closely to Denrell, Fang and Winter’s ([2003](#den03)) definition of serendipity, who defined it as _‘effort and luck joined by alertness and flexibility’_ (p.978). As we see in the framework of Figure 4, moving towards opportunistic discovery of information (right-hand side) might stem a departure from the very core of the notion of serendipitous information encounter, but might be useful from an application perspective. Makri and Blandford ([2012a](#mak12a)) emphasized _reflection_ on the unexpectedness of circumstances leading to the chance encounter and the value of the information found, while both Agarwal ([2011](#aga11)) and Lawley and Tompkins ([2008](#law08)) tied it to information _use_.

Based on the information encountered, a happy or fortuitous outcome may happen at most times, but the outcome could sometimes be unhappy or unpleasant. Finally, the information encountered is applied to new contexts and novel uses, which often foster creativity.

## Towards a definition

Based on Figure 4, and including the assumptions arrived at thus far, we can reach a few conclusions about serendipitous information finding (i<sub>chance</sub>) that are at the core of this research area:

1.  It can occur at times when the actor is engaged in passive, non-purposive modes (not really looking for anything in particular – the Newton scenario) or active, purposive modes (the Fleming scenario).
2.  The serendipitous discovery itself is always an incident or trigger of a chance encounter, followed by a period of incubation which leads to attaching insight and value to the information encountered. This happens when a visceral or conscious information need (n<sub>different</sub>) is satisfied [assumption 1].
3.  Serendipitous information finding leads to unexpectedness, surprise or an _ahah!_ moment [assumption 2] but could lead to disappointment as well.
4.  The more unexpected, the more insightful and the more valuable the finding is, the greater the degree of serendipity is ([Makri and Blandford, 2012b](#mak12b)).
5.  The degree of surprise (_ahah!_ moment) will be higher when one is in a state of natural alertness, as opposed to being in a state of serendipitous alertness or a prepared mind [assumption 3].

Based on Figure 4 and the conclusions above, we can see serendipity as an incident-based, unexpected discovery of information when the actor is either in a passive, non-purposive state or in an active, purposive state. Merely finding information unexpectedly might not be of much value or use. Makri and Blandford ([2012b](#mak12b)) argued that the chance discovered information must be insightful and valuable to the actor. McCay-Peet and Toms ([2010](#mcc10)) argued that the serendipitous incident is always followed by a period of incubation which leads to the insight. Thus, we can define serendipity as an incident-based, unexpected discovery of information leading to an _aha!_ moment when a naturally alert actor is in a passive, non-purposive state or in an active, purposive state, followed by a period of incubation leading to insight and value.

## Conclusion

We have looked at the terminology surrounding serendipity in information behaviour (Tables 1 and 2). Using Wilson's model ([1999](#wil99)) as a theoretical lens (Figure 1), we have sought to place serendipity within other models of information behaviour (Figure 2). Depending on the immediacy of information need that the encountered information satisfies, a possible nested model for serendipity is proposed (Figure 3). We map the terminology among the three different dimensions of serendipitous findings shown earlier. The core of the research area of serendipity in information behaviour is proposed, which includes a framework of continuums (Figure 4). This framework helps us arrive at a definition for serendipity in information behaviour.

Being a relatively new entrant to the field, past theories, models and frameworks of information behaviour have largely left out serendipitous discovery of information from their efforts. Also, there has not been enough clarity as to what constitutes the core of this research area. This paper has attempted to change this. We have arrived at a framework of continuums modelling the theoretical and conceptual space of serendipity in information behaviour. It should make serendipitous information encountering easier to understand and help further research in this area.

As Erdelez ([2005](#erd05), p. 183) wrote, _‘A theory that builds upon the_ [information encountering] _view of information behavior will need to accommodate the interplay between purposeful and opportunistic acquisition of information and may help explain information behavior in a natural and holistic way’_. This paper is an effort in that direction.

Now that a definition of serendipity is proposed, future work will look at the relationship of serendipity with the context of information behaviour. The mapping of serendipity with other models of context will be explored. Future work will also validate the framework of continuums in specific settings and contexts: in the context of information behaviour of medical residents in a hospital setting, behaviour of library and information science students when working on assignments, and of knowledge workers as part of their work tasks. Other researchers are encouraged to also test the framework in their studies, looking at both task-based active search processes, as well as information encountered during non-purposive search. Further studies should also investigate other methods of gathering data on serendipity such as surveys and experiments. Diary studies could be conducted with samples of participants, investigating how they encounter serendipitous information in social media, mobile devices, etc. and the sorts of creative uses that those encounters lead to.

Further work could build on whether some people are more serendipity prone compared to others (see [Heinström, 2006a](#hei06a), [2006b](#hei06b)) and how serendipity can be simulated and applied in various environments. As Lawley and Tompkins ([2008](#law08)) also implied, while serendipitous encounters lead to an _ahah!_ moment, what happens before and after is also equally important. As a person reflects on the processes leading to the serendipitous encounter ([Makri and Blandford, 2012a](#mak12a)), the mind gets more prepared to recognize the information encountered ([Lawley and Tompkins, 2008](#law08)). Thus, the very process of carrying out studies on serendipitous encounters helps not only in the study of serendipity, but also in the process of reflection leading to people becoming more aware of serendipitous encounters. Information behaviours such as browsing and scanning ([Case, 2012, p.100](#cas12)), and other aspects of exploratory search ([McCay-Peet and Toms, 2010](#mcc10)), are conducive to opportunistic discovery of information. As people actively start looking out for serendipitous encounters, structures and interfaces can be designed to help them happen more naturally. From learning commons in libraries ([Fox and Doshi, 2013](#fox13)) to communities of practice ([Smith, Calderwood, Dohm and Lopez, 2013](#smi13)) to pocket neighbourhoods ([Chapin, 2011](#cha11)), spaces and systems can be designed to facilitate more frequent serendipity, leading to novel and creative uses. Thus, probing deeper into the phenomenon of serendipity in information behaviour is not only important for research and theoretical knowledge, but has important implications for practice.

## Acknowledgement

The author is grateful to his co-participants in an international workshop on the opportunistic discovery of information held in Columbia, Missouri in October 2010 and those at the 2012 SCORE workshop in Montreal, Canada who greatly helped inform discussions in this area.

The author is also grateful to the anonymous reviewers and to the Regional Editor for persevering with me through this paper, and to Joshua Jasper for many hours of discussions and editing.

## About the author

**Naresh Kumar Agarwal** is an Associate Professor at the School of Library and Information Science, Simmons College, Boston, Massachusetts, USA. He received his PhD from the National University of Singapore. He can be contacted at: agarwal@simmons.edu

#### References

*   Abbas, J. & Agosto, D. E. (2013). Everyday life information behavior of young people. In J. Beheshti & A. Large (Eds.), _The information behavior of a new generation: children and teens in the 21st century_ (pp. 65-91). Lanham, MD: Scarecrow Press.
*   Agarwal, N.K. (2011). Information source and its relationship with the context of information seeking behavior. In _Proceedings of iConference 2011_ (pp. 48-55). New York, NY: ACM.
*   Agarwal, N.K. (2012). Making sense of sense-making: tracing the history and development of Dervin's sense-making methodology. In T. Carbo & T.B. Hahn (Eds.). _International perspectives on the history of information science & technology: proceedings of the ASIS&T 2012 Pre-Conference on the History of ASIS&T and Information Science and Technology_. (pp. 61-73). Medford, NJ: Information Today.
*   Agarwal, N.K., Xu, Y. (C.) & Poo, D.C.C. (2011). A context-based investigation into source use by information seekers._Journal of the American Society for Information Science and Technology, 62_(6), 1087-1104.
*   Agarwal, N.K., Xu, Y.(C.) & Poo, D.C.C. (2009). [Delineating the boundary of ‘context’ in information behavior: towards a contextual identity framework.](http://www.webcitation.org/6Z09ALGAB) _Proceedings of the Annual Meeting of the American Society for Information Science and Technology, 46_(1), 1-29\. Retrieved from http://gslis.simmons.edu/blogs/nareshagarwal/files/2012/06/Agarwal-Xu-Poo-ASIST2009-publisher1.pdf (Archived by WebCite® at http://www.webcitation.org/6Z09ALGAB)
*   Agosto, D.E. & Hughes-Hassell, S. (2006a). Toward a model of the everyday life information needs of urban teenagers: part 1, theoretical model. _Journal of the American Society for Information Science & Technology, 57_(10), 1394-1403.
*   Agosto, D.E. & Hughes-Hassell, S. (2006b). Toward a model of the everyday life information needs of urban teenagers: part 2, empirical model. _Journal of the American Society for Information Science & Technology, 57_(11), 1418-1426.
*   Allen, B.L. (1996). _Information tasks: toward a user-centered approach to information systems_. San Diego, CA: Academic Press.
*   Apted, S. (1971). General purposive browsing. _Library Association Record, 73_(12), 66-78.
*   Bates, M.J. (1989). The design of browsing and berrypicking techniques for the online search interface. _Online Information Review, 13_(5), 407-424.
*   Beheshti, J., Large, A. & Clement, I. (2008). Exploring methodologies for designing a virtual reality library for children. In C. Guastavino & J. Turner (Eds). _Proceedings of the Annual Conference of the Canadian Association for Information Science_. (15 p.) Canadian Association for Information Science.
*   Belkin, N.J., Oddy, R. & Brooks, H. (1982). ASK for information retrieval. _Journal of Documentation, 38_(2), 61-71.
*   Benbasat, I. & Zmud, R.W. (2003). The identity crisis within the IS discipline: defining and communicating the discipline’s core properties. _MIS Quarterly, 27_(2), 183-194.
*   Campos, J. & de Figueiredo, A.D. (2001). Searching the unsearchable: inducing serendipitous insights. In R. Weber & C. Greese (Eds.), _Proceedings of the workshop program at the fourth International Conference on Case-Based Reasoning, ICCBR 2001_. (pp.159-164). Washington, D.C.: Naval Research Laboratory, Navy Center for Applied Research in Artificial Intelligence. (Technical Note AIC-01-003).
*   Case, D.O. (2012). _Looking for information: a survey of research on information seeking, needs and behavior._ (3rd. ed.). Bingley, UK: Emerald Group Publishing.
*   Chapin, R. (2011). _Pocket neighborhoods: creating small-scale community in a large-scale world_. Newtown, CT: Taunton Press.
*   Chatman, E. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science, 47_(3), 193-206.
*   Cole, C. (2012). _Information need: a theory connecting information search to knowledge formation_. Medford, NJ: Information Today, Inc.
*   Courtright, C. (2007). Context in information behavior research. _Annual Review of Information Science and Technology, 41_, 273-306.
*   Cunha, M.P.E. (2005). [_Serendipity: why some organizations are luckier than others._](http://www.webcitation.org/6Z09wKkln) Lisbon: Universidade Nova (FEUNL Working Paper No. 472). Retrieved from Nova University of Lisbon - Faculty of Economics: http://fesrvsd.fe.unl.pt/WPFEUNL/WP2005/wp472.pdf (Archived by WebCite® at http://www.webcitation.org/6Z09wKkln)
*   Cutter, C.A. (1876). _Public libraries in the United States of America: their history, condition, and management_. Washington, D.C.: US Government Printing Office.
*   Denrell, J., Fang, C. & Winter, S.G. (2003). The economics of strategic opportunity. _Strategic Management Journal, 24_(10), 977-990.
*   Dervin, B. (1997). Given a context by any other name: methodological tools for taming the unruly beast. In P. Vakkari, R. Savolainen and B. Dervin (Eds.) _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp.13-38). London: Taylor Graham.
*   Dervin, B. (1983). _An overview of sense-making research: concepts, methods and results to date._ Paper presented at _the International Communications Association Annual Meeting_, Dallas, Texas.
*   Dervin, B. (1992). From the mind’s eye of the user: the sense-making qualitative-quantitative methodology. In J.D. Glazier and R.R. Powell (Eds.), _Qualitative research in information management_ (pp. 61-84). Englewood, CO: Libraries Unlimited.
*   Duff, W.M. & Johnson, C.A. (2002). Accidentally found on purpose: information-seeking behavior of historians in archives. _Library Quarterly, 72_(4), 472-496.
*   Ellis, D. (1989). A behavioral approach to information retrieval design. _Journal of Documentation, 45_(3), 171-212.
*   Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation, 49_(4), 356-369.
*   Erdelez, S. (1995). _Information encountering: an exploration beyond information seeking_. Unpublished dissertation, Syracuse University, Syracuse, New York, USA.
*   Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In P. Vakkari, R. Savolainen & B. Dervin. (Eds.), _Information seeking in context. Proceedings of an international conference on research in information needs, seeking, and use in different contexts, Tampere, Finland, 1996_ (pp. 412-421). Los Angeles, CA: Taylor Graham.
*   Erdelez, S. (2004). Investigation of information encountering in the controlled research environment. _Information Processing & Management, 40_(6), 1013-1025.
*   Erdelez, S. (2005). Information encountering. In K.E. Fisher, S. Erdelez and L. McKechnie (Eds.), _Theories of information behaviour_ (pp. 179-184). Medford, NJ: Information Today, for ASIS&T.
*   Erdelez, S. (1999). [Information encountering: it's more than just bumping into information.](http://www.webcitation.org/6Z0A8vI5l) _Bulletin of the American Society for Information Science, 25_(3), 25-29\. Retrieved from http://www.asis.org/Bulletin/Feb-99/erdelez.html (Archived by WebCite® at http://www.webcitation.org/6Z0A8vI5l)
*   Erdelez, S. & Makri, S. (Eds.). (2011). Thematic issue on the opportunistic discovery of information, being a selection of papers from the International Workshop on Opportunistic Discovery of Information, University of Missouri, October 2010\. _[Information Research](http://www.webcitation.org/6Z0CFkMd0), 16_(3). Retrieved from http://informationr.net/ir/16-3/infres163.html (Archived by WebCite® at http://www.webcitation.org/6Z0CFkMd0)
*   Erdelez, S. & Rioux, K. (2001). Sharing information encountered for others on the Web. In L. Hoglund and T.D. Wilson (Eds.), _Information seeking in context: proceedings of the 3rd International Conference on Research in Information Needs, Seeking and Use in Different Contexts_ (pp. 219-233). London: Taylor Graham.
*   Fidel, R. & Pejtersen, A.M. (2004). [From information behavior research to the design of information systems: the cognitive work analysis framework.](http://www.webcitation.org/6Z0B8SEcq) _Information Research, 10_,(1), paper 210\. Retrieved from http://informationr.net/ir/10-1/paper210.html (Archived by WebCite® at http://www.webcitation.org/6Z0B8SEcq)
*   Fine, G.A. & Deegan, J. (1996). Three principles of serendipity: insight, chance and discovery in qualitative research. _International Journal of Qualitative Studies in Education, 9_(4), 434-447.
*   Fisher, K.E., Erdelez, S. & McKechnie, L. (Eds.) (2005). _Theories of information behavior_. Medford, NJ: Information Today.
*   Fitzgerald, B. & Adam, F. (1996). [The future of IS: expansion or extinction?](http://www.webcitation.org/6Z0C9vMhE) In _Proceedings of First Conference of the UK Academy for Information Systems_. (15 p.). Retrieved from http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.104.2487&rep=rep1&type=pdf (Archived by WebCite® at http://www.webcitation.org/6Z0C9vMhE)
*   Foreman-Wernet, L. (2003). Rethinking communication: introducing the sense-making methodology. In B. Dervin and L. Foreman-Wernet with E. Lauterbach (Eds.), _Sense-making methodology reader: selected writings of Brenda Dervin_ (pp. 3-16). Cresskill, NJ: Hampton Press.
*   Foster, A.E. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation, 59_(3), 321-340.
*   Fox, R. & Doshi, A. (2013). Longitudinal assessment of "user-driven" library commons spaces. _Evidence Based Library and Information Practice, 8_,(2), 85-95.
*   Grose, M.W. & Line M.B. (1968). On the construction of white elephants – some fundamental questions concerning the catalogue. _Library Association Record. 70_(1), 2-5.
*   Harris, R.M. & Dewdney, P. (1994). _Barriers to information: how formal help systems fail battered women_. Westport, CT: Greenwood Press.
*   Heinström, J. (2006a). Broad exploration or precise specificity: two basic information seeking patterns among students. _Journal of the American Society for Information Science and Technology, 57_(11), 1440-1450.
*   Heinström, J. (2006b). Psychological factors behind incidental information acquisition. _Library & Information Science Research, 28_(4), 579-594.
*   Hirschheim, R. & Klein, H.K. (2003). Crisis in the IS field? A critical reflection on the state of the discipline. _Journal of the Association for Information Systems_, 4(5), 237-293.
*   Ikoja-Odango, R. & Ocholla, D.N. (2003). Information needs and information-seeking behavior of artisan fisher folk of Uganda. _Library and Information Science Research, 25_(1), 89-105.
*   Jarvelin, K. & Ingwersen, P. (2004). [Information seeking research needs extension towards tasks and technology.](http://www.webcitation.org/6Z0CMzhgO) _Information Research_, 10(1), paper 212\. Retrieved from http://www.informationr.net/ir/10-1/paper212.html (Archived by WebCite® at http://www.webcitation.org/6Z0CMzhgO)
*   Julien, H. & Michels, D. (2004). Intra-individual information behavior in daily life. _Information Processing and Management, 40_(3), 547-562.
*   Keen, P.G.W. (1980). [MIS research: reference disciplines and a cumulative tradition.](http://www.webcitation.org/6Z0CTcdrr) In _Proceedings of the First Conference on Information Systems_ (pp. 9-18). Retrieved from http://aisel.aisnet.org/icis1980/9 (Archived by WebCite® at http://www.webcitation.org/6Z0CTcdrr)
*   Kirzner, I.M. (1997). Entrepreneurial discovery and the competitive market process: an Austrian approach. _Journal of Economic Literature, 35_(1), 60-85.
*   Krikelas, J. (1983). Information-seeking behavior: patterns and concepts. _Drexel Library Quarterly, 19_(2), 5-20.
*   Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user’s perspective. _Journal of the American Society for Information Science, 42_(5), 361-371.
*   Kuhlthau, C.C. (1996). The concept of zone of intervention for identifying the role of intermediaries in the information search process. _Proceedings of the 59th Annual Meeting of the American Society for Information Science, 33_(1), 91-94.
*   Kuhlthau, C.C. (1997). The influence of uncertainty on the information seeking behavior of a securities analyst. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Proceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp. 268-274). London: Taylor Graham Publishing.
*   Lawley, J. & Tompkins, P. (2008). [_Maximising serendipity: the art of recognising and fostering potential_.](http://www.webcitation.org/6Z0D8frM2)) Paper presented at _the Developing Group_. Updated version retrieved from http://www.academia.edu/1836363/Maximising_Serendipity_The_art_of_recognising_and_fostering_potential (Archived by WebCite® at http://www.webcitation.org/6Z0D8frM2)
*   Leckie, G.J. & Pettigrew, K.E. (1997). A general model of the information seeking of professionals: role theory through the back door? In P. Vakkari, R. Savolainen & B. Dervin, (Eds.). _Proceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp. 99-110). London: Taylor Graham Publishing.
*   Line, M.B. (1974). Draft definitions: information and library needs, wants, demands and uses. _Aslib Proceedings, 26_,(2), 87.
*   Makri, S. & Blandford, A. (2012a). Coming across information serendipitously – part 1: a process model. _Journal of Documentation, 68_,(5), 684-705.
*   Makri, S. & Blandford, A. (2012b). Coming across information serendipitously – part 2: a classification framework. _Journal of Documentation, 68_,(5), 706-724.
*   McCay-Peet, L. & Toms, E. (2010). The process of serendipity in knowledge work. In _Proceedings of the Third Symposium on Information Interaction in Context_ (pp.377-382). New York, NY: ACM.
*   McKenzie, P. J. (2003). A model of information practices in accounts of everyday-life information seeking. _Journal of documentation, 59_(1), 19-40.
*   Rieh, S.Y. (2004). On the Web at home: information seeking and Web searching in the home environment. _Journal of the American Society for Information Science and Technology, 55_, 743-753.
*   Roberts, R.M. (1989). _Serendipity: accidental discoveries in science_. New York, NY: John Wiley & Sons.
*   Rosenman, M. (1988). Serendipity and scientific discovery. _Journal of Creative Behavior, 22_(2), 132-138.
*   Ross, C.S. (1999). Finding without seeking: the information encounter in the context of reading for pleasure. _Information Processing and Management, 35(6)_, 783-799.
*   Rubin, V.L., Burkell, J. & Quan-Haase, A. (2010). Everyday serendipity as described in social media. _Proceedings of the American Society for Information Science and Technology, 47_(1), 1-2.
*   Saracevic, T. (1996). Modeling interaction in information retrieval (IR): a review and proposal. In S. Hardin (Ed.), _59th Annual Meeting of the American Society for Information Science_ (pp. 3-9). Silver Spring, MD: American Society for Information Science.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "way of life". _Library & Information Science Research, 17_(3), 259-294.
*   Sawaizumi, S., Katai, O., Kawakami, H. & Shiose, T. (2007). [Using the concept of serendipity in education.](http://www.webcitation.org/6Z0DWams4) In _Proceedings of the Second International Conference on Knowledge, Information and Creativity Support Systems_. (pp. 116-121). Nomi City, Taiwan: JAIST Press. Retrieved from https://dspace.jaist.ac.jp/dspace/bitstream/10119/4087/1/13.pdf (Archived by WebCite® at http://www.webcitation.org/6Z0DWams4)
*   Shapiro, G. (1986). _A skeleton in the darkroom: stories of serendipity in science_. San Francisco, CA: Harper & Row.
*   Smith, E.R., Calderwood, P.E., Dohm, F.A. & Lopez, P.G. (2013). Reconceptualizing faculty mentoring within a community of practice model. _Mentoring & Tutoring: Partnership in Learning, 21_(2), 175-194.
*   Taylor, R. S. (1968). Question-negotiation and information seeking in libraries. _College & Research Libraries, 29_(3), 178-194.
*   Toms, E.G. (2000a). [Serendipitous information retrieval.](http://www.webcitation.org/6Z0DcZYZK) In _Proceedings of the First DELOS Network of Excellence Workshop on Information Seeking, Searching and Querying in Digital Libraries, Zurich, Switzerland, December, 11-12, 2000_ (pp. 17-20). Sophia Antipolis, France: The European Research Consortium for Informatics and Mathematics. Retrieved from http://www.ercim.eu/publication/ws-proceedings/DelNoe01/3_Toms.pdf (Archived by WebCite® at http://www.webcitation.org/6Z0DcZYZK)
*   Toms, E.G. (2000b). Understanding and facilitating the browsing of electronic text. _International Journal of Human-Computer Studies, 52_(3), 423-452.
*   Williamson, K. (1998). Discovered by chance: the role of incidental information acquisition in an ecological model of information use. _Library & Information Science Research, 20_(1), 23-40.
*   Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation, 37_(1), 3-15.
*   Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation, 55_(3), 249-270.
*   Wilson, T.D. & Walsh, C. (1996). _[Information behaviour: an interdisciplinary perspective](http://www.webcitation.org/6Z0DjIPNH)_. Sheffield, UK: Department of Information Studies, University of Sheffield. Retrieved from http://www.informationr.net/tdw/publ/infbehav/cont.html (Archived by WebCite® at http://www.webcitation.org/6Z0DjIPNH)
*   Zach, L. (2005). When is "enough" enough? Modeling the information-seeking and stopping behavior of senior arts administrators. _Journal of the American Society for Information Science and Technology, 56_(), 23-35.