####Information Research, Vol. 11 No. 3, April 2006


# A model for the development of virtual communities for people with long-term, severe physical disabilities

#### [C.M. Tilley](mailto:ctilley@acenet.net.au), C.S. Bruce, G. Hallam and A.P. Hills  
School of Information Systems, Faculty of Information Technology  
Queensland University of Technology  
GPO Box 2434, Brisbane. Australia. 4001


#### Abstract

> **Introduction**. This paper reports results of an investigation into the needs of persons with disabilities wanting to participate in the use of virtual communities. The aim was to investigate 'how virtual communities for persons with long-term, severe physical disabilities can best be facilitated'?  
> **Method** . A Grounded Theory approach was adopted to inform the investigation. In- depth interviews were conducted with twelve persons with paraplegia, quadriplegia or other severe, long-term physical or mobility disabilities and six health care professionals, service providers, information personnel and policy advisers who were involved in their well-being.  
> **Analysis**. Rich explanations were derived about the information and communication technology (ICT) usage and the technologies' contributions towards restoration of sense of control over their lives.  
> **Results**. The primary outcome of the investigation is a theory regarding the character of virtual communities for the disabled. The theory is represented as a Virtual Community Model. The model identifies: the need for 'a sense of control' as the foundation element of virtual communities for the disabled; the key domains in which disabled people participate in virtual communities; and the barriers and enablers to their participation.  
> **Conclusion**.The model provides a framework which can be used by interest groups and other organizations to facilitate the development of virtual communities for persons with severe physical disabilities. The six key types of community need to be represented in such virtual communities if a full 'sense of control' is to be achieved by disabled persons.



## Introduction

Despite the long-standing interest in online or virtual communities in the western world, particularly in Australia, there is a paucity of successful virtual communities or models of 'best practice' for persons with long-term, severe physical disabilities. The purpose of this qualitative study was to develop a theoretical framework or model for a virtual community for this specific group of people

The key findings, model and theory for this study parallelled and extended general findings elaborated by Beamish ([1995](#Beamish)). They also upheld the five necessary multi-disciplinary characteristics that Whittaker _et al._ ([1997](#Whittaker): 137) found essential to a virtual community. Beamish ([1995](#Beamish)) described community networks as sharing the following three characteristics, also integral to the theoretical framework for a virtual community for Queenslanders with long-term physical disabilities in the present study:

*   a focus on local issues in addition to information services, community networks provide forums for discussion of local issues; for example, in this study, the participants focused on their shared 'interests,' that is, in disability rights;
*   access—the network is concerned to reflect and include all members of the community; for example, in this study, the participants accessed information on assistive technology;
*   social change and community development—there is a belief that the system with its communication and information can strengthen and vitalise the community; for example, the participants were empowered to pursue aspects of capacity building as part of strengthening and vitalising their community.

The first generation of community network developers originated from the industrial world, but subsequently were joined by numerous others from all sectors. Schuler ([1996](#Schuler)), a founding member of the Seattle Community Network, is one of the prime movers in questioning what these developers are trying to accomplish with these systems. There are certain kinds of answers that were advanced at the first 'Ties that Bind' community-networking conference in 1994, sponsored by [Apple Corporation](http://www.apple.com/) and the [Morino Institute](http://www.morino.org/). They include the following: rebuilding civil society, securing access to information to disadvantaged or disabled people, community economic development, improving access to health care and health care information, providing forums for minority and alternative voices, improving communication among civic groups and improving literacy (Morino [1994](#Morino)). This earlier work, affecting all stakeholders, needs to be reviewed in the context of experience. Rheingold ([1994](#Rehingold)) and Schuler ([1996](#Schuler)) were the front runners in visioning and implementing online communities through the 1990s. Preece also has written extensively on usability and human computer interaction (HCI), but not with a view to developing a specific online community. However, in Preece ([2000](#Preece2000)) she correlates the key themes of sociability and usability, creating the link between knowledge about human behaviour and appropriate social planning, policies and software design to carry virtual communities well into the new millenium. usability has been identified repeatedly as an important component of human-computer interaction design (Preece [1993](#Preece1993); Nielsen & Mack [1994](#Nielsen); Preece _et al._ [1994](#Preece1994); Hackos & Redish [1998](#Hackos); Shneiderman, [1998](#Shneiderman); Mayhew [1999](#Mayhew); Preece, _et al._ [2001](#Preece2001); Hirose [2001](#Hirose) and Carroll [2001](#Carroll)) and sociability with social interaction. usability is primarily concerned with developing computer systems to support rapid learning, high skill retention and minimal error rates. Such systems support high productivity, are consistent, controllable and predictable, making them pleasant and effective to use (Shneiderman [1998](#Shneiderman)). The implications for online communities is that users are able to communicate with each other, find information and navigate the community software with ease (Preece [2000](#Preece2000)).

While the literature abounds with definitions for virtual communities, they all include the idea that these virtual communities or electronic communities are social aggregations of a critical mass of people on the Internet. They engage in public discussions, interactions, and information exchanges with sufficient human feeling on matters of common interest to form webs of personal relationships. It is worth noting that the types of interactions that community members have, the type of content they provide to the community, the kinds of benefits they derive from the communities, and the types of exchanges communities have with other interest groups vary greatly from community to community (Chang, _et al._ [1999](#Chang): 1).

The ability of virtual communities to provide emotional support for their participants is well documented. In fact, communities have been formed expressly to provide such support for precisely defined groups of users, such as parents of children with special needs (Mickelson 1997, cited in Burnett, [2000](#Burnett)) and people with particular health problems (Preece 1999, cited in Burnett, [2000](#Burnett)). Even in virtual communities that are not explicitly designed as such support groups, the development of interpersonal relationships is common (Parks & Floyd 1995, cited in Burnett [2000](#Burnett)). Such relationships, even though they may be, in some cases, closely tied to the specific subject domains of the communities in which they occur, can be both intimate and supportive (Rosson 1999, cited in Burnett [2000](#Burnett)); Wellman & Gulia 1999, cited in Burnett [2000](#Burnett)). According to Burnett, little specific research has been conducted into the types of interactions or whether they are linked with information exchange within virtual communities that may be used to provide such emotional support (Burnett, [2000](#Burnett)). However, Preece asserts that emotional support and information sharing are likely to be closely linked in such situations (Preece, [1993](#Preece1993)).

Preece has found the term online community, or virtual community, difficult to define and prefers a working definition of an online community that says it consists of four essential components: people, purpose, policies and computer systems (Preece [2000](#Preece2000): 10). She finds this working definition sufficiently general to apply to a range of different communities, avoiding defining 'community' only as an entity rather than a process. Communities develop and continuously evolve and it is only the software supporting them that is designed, not the community itself. Preece's general or working definition also includes physical communities that have become networked (Schuler [1996](#Schuler); Lazar & Preece [1998](#Lazar)), communities supported by a single bulletin board, list-server or chat software. However, it is the first three components of Preece's definition that relate to the complexities and unpredictability that comprise human behaviour and link back into Preece's notion of sociability. For Preece, ([2000](#Preece): 26), sociability is concerned with planning and developing social policies that are understandable and acceptable to members, to support the community's purpose. Understanding a community's needs is essential for developing communities with good sociability and usability and Preece has represented these close dynamic relationships in a way that is very useful to a virtual community developer (Preece, [2000](#Preece): 27).

Preece's model of 'designing usability and supporting sociability' with 'lifelong learning as their central focus' is exemplified by Britain's Learning Cities and offers a powerful model for Queensland, and Australian, virtual communities for people with long-term, severe physical disabilities to emulate.

### Online communities for the physically disabled: the state of the art

While internationally there are a significant number of web sites dedicated to disability issues, the Queensland Centre of National Research on Disability and Rehabilitation Medicine (CONROD) 'Disability Lifestyles' project validates this study. The CONROD project established an online environment in 2004 for the provision and sharing of information about everyday life for people who were completing, or had recently completed, rehabilitation after a traumatic spinal injury. This '[Disability Lifestyle](http://www.disabilitylifestyles.org.au)' Web site was an online stepping-stone between rehabilitation services and full adjustment back into the community. It also may be seen to meet the needs of the beginning of the disability 'life cycle', whereas the theoretical model that emerged from this study represents the needs some thirty years into the disability 'life cycle'.

## Towards establishing a theory about virtual communities for disabled persons

### Aims, goals of the research

The research has focused on the needs of one group of participants in virtual communities; persons with long-term, severe physical disabilities; and sought to develop understandings of one such community to inform how virtual communities for disabled persons might best be facilitated.

### Use of the grounded theory approach

Grounded theory was employed for this study. This approach allowed for exploration of virtual communities as required by persons with physical disabilities engaged in a 'community of practice'. The substantive theory and conceptual model were developed using this approach and were grounded in the real world experiences of persons with disabilities as they described engaging with information and communication technologies. Grounding this study in real life engagement within the disability community:

*   broadens our understanding of how virtual communities are experienced;
*   enables the development of a baseline study for further research into virtual communities; and
*   provides facilitators with a conceptual framework that will inform future design and practices.

Denzin and Lincoln ([1994](#Denzin)) have conveniently divided the history of qualitative research into five 'moments,' some of which occur simultaneously. The formulation of the grounded theory method was in the second of these 'moments' and was introduced and published by Glaser and Strauss in [1967](#Glaser). However, it was Strauss and Corbin in [1990](#Strauss), who presented a major reformulation of the method, which was further developed and refined by them in 1998\. Glaser strongly refuted Strauss and Corbin's efforts and maintained that they failed to present the reasons for their changes to the method as originally described by Glaser and Strauss (Glaser, [1978](#Glaser1978), [1992](#Glaser1992); Glaser & Strauss, [1967](#Glaser167); Strauss & Corbin, [1990](#Strauss1990), [1998](#Glaser1998)). There are similarities and differences between these two major approaches to grounded theory research; however, the controversy seems to stem from their different uses of axial coding (Kendall, [1999](#Kendall): 746-748). The grounded theory method has evolved into three distinct variations including Glaserian; Strauss and Corbin's objectivist version; and constructivist grounded theory (Charmaz, [2003](#Charmaz); Goulding, [2002](#Goulding)).

Several considerations were necessary before deciding to use a qualitative research methodology for this study. Strauss & Corbin, ([1990](#Strauss)) claimed that qualitative methods were used to understand better any phenomenon about which little was yet known. Additionally, such methods could be used to gain new perspectives on topics about which much was already known; or, to gain more in-depth information that would be difficult to convey quantitatively. Therefore, the Strauss and Corbin method was accepted as most appropriate to this study because while information about many aspects of virtual communities has been available during the past decade, there is no recorded understanding about the needs of particular communities such as persons with physical disabilities.

### The participants

The twelve persons with disabilities ranged in age from twenty-four years to sixty years with a mean age of forty-four years. There were four females and eight males interviewed: nine persons resided in Brisbane, the capital of the state of Queensland, and the other threepersons were from a large town west of Brisbane, Capricornia and Far North Queensland. Eight people suffered from traumatic spinal injuries resulting in quadriplegia1, two persons were paraplegic2, while one person had had poliomyelitis and another had a muscle wasting condition. Two people were extremely severely affected physically and disabled as a result of their injuries. Six quadriplegic people were regarded as very affected, while the remaining four persons were not as severely affected. In fact, the greater the severity of the injury and consequent disability, the greater the potential dependence on technology of these people for their 'independence' and the resumption of their 'sense of control' over their lives. The diversity of educational attainment of the group interviewed was consistent with the wide ranging nature of most of their other distinguishing criteria. Two persons had secondary education, three TAFE education and seventertiary education with six others who had completed higher degrees. The range of disability onset was from the age of 13 to 51 and the average onset time of disability was twenty-seven and a half years ago. All participants were categorised as persons with long-term disabilities. Consequently, the participant group had primarily dealt with all the rehabilitation, personal care and basic lifestyle survival issues that would inevitably preoccupy newly disabled people.

### The interviewer-researcher

In this study we used 'identification' as a methodological principle and the study may be considered 'subjective'. The principal researcher and interviewer in this investigation, reflects below on her experience of the interview:

> I am a Queenslander, a person with a severe, long-term physical disability. I had no need to disclose these facts about myself to the interviewees, as the majority of them know me. However, I argue that my identification is a strength rather than a weakness, as the interviewees identify with my efforts to determine how virtual communities are best facilitated for persons with disabilities and they recognise the relevance of my study. In some of my interviewing situations, I was aware of the enormous pain it brought to some of the interviewees to recall the trauma of their accident and then to recount the nature of their ongoing disability. I began the study knowing there could be difficult and sad aspects, but not realising the depth of it, and how it would sometimes affect me.

> 'Personal experience can increase sensitivity if used correctly' (Strauss & Corbin, [1998](#Strauss): 48). I would like to believe that my personal experience of deep identification with my interviewees contributed to my sensitivity in the interview situation and later during the data analysis stage in that it sometimes assisted me 'to make sense out of the previously unexplainable data' (ibid.). Reinharz says: 'The value in identifying lies in the enhanced understanding one can develop and assistance one can give. The drawback is the pain that comes from identifying with people who are suffering. We may gradually turn away to protect our equilibrium and continue our work'. (Reinharz, [1992](#Reinharz): 234).

The subjective researcher seeks to know the situation through the eyes of the interviewee. This use of the traditional 'object's' viewpoint is asserted by Chatman ([1986](#Chatman)), Mellon ([1990](#Mellon)), Fidel ([1993](#Fidel)) and by Sutton ([1993](#Sutton)).

### Data gathering and analysis: forming the grounded theory

As with other grounded theory research the process of data gathering and analysis in this study were closely interwoven. As the interviewer conversed with interviewees, the analysis process began and influenced the course of future interviews, and also the interviewees selected.

For this research, a single interview question was used, derived from working with interviewees during the pilot phase. 'What can you tell me about your experience with virtual communities and how you use ICT (information and communications technology) both personally and professionally?' Through the resultant conversations with interviewees, their experience was made known to the interviewer, Each interview took approximately one hour. More time was allocated if the interviewee was distressed. The number of persons interviewed continued until 'theoretical saturation' of content occurred and no new content was emerging from the interviews. Each interview explored in detail the elements and any barriers behind the usage of information and communication technologies and/or assistive technology.

Finally, the coding and the analysis were performed. The participants' perceptions were analysed using the grounded theory constant comparison methodology. The relationship of the analysis to the literature was also considered repeatedly, as part of the necessary iterative process. Personal responses and narratives of both the people with disabilities who use the technology and the allied health care professionals were analysed and interpreted for meaning. All transcripts were returned to participants for validation. These de-constructed meanings were then compared and contrasted with those in the current literature.

The central theme to emerge from these narratives was how the use of ICT towards an online community by persons with long-term disabilities allowed them to regain a sense of control and independence in their lives. Other major themes emerged; being online tended to break down isolation and also had the potential to change the work paradigm, both vexing issues for people with disabilities. These outcomes are increasingly seen as offering ways to enhance people with disabilities' inclusion, participation and empowerment in our society.

According to the grounded theory research methodology (Strauss & Corbin, [1998](#Strauss): 101) 'phenomena' are the central ideas in the data represented as concepts. Concepts are the building blocks of the theory. Categories are concepts that stand for phenomena. Properties are the characteristics of a category, the delineation of which defines and gives it meaning. For example, for the category 'transaction' described later, the properties were 'e-commerce' and 'employment' (see Figure 1 below). Dimensions are the ranges along which general properties of a category vary, giving specification to a category and variation to the theory. For example, some of the dimensions of the category 'transaction' were 'investments', 'shopping', 'ordering', and 'bill paying' (see Figure 1 below). And sub-categories are the concepts that pertain to a category, giving it further clarification and specification: in this case, they are 'tele-working' and 'tele-marketing'. The main objective of grounded theory was the discovery of theoretically comprehensive explanations about particular phenomenon. The techniques and analytical procedures such as described above, allow the development of a substantive theory that is significant, theory-observation compatible, able to be generalised from, reproducible and rigorous. Grounded theory methodology is both deductive and inductive. Inductively, theory emerged from the interviews and generated data. This theory can than be empirically tested to develop forecasts or predictions from the general principles.

Strauss and Corbin acknowledge that developing theory is a complex activity. They say that 'we use the term 'theorising' to denote this activity because developing theory is a process and often is a long one' (Strauss & Corbin, [1998](#Strauss): 21-22). According to them, theorising is work that entails not only conceiving or intuiting ideas [concepts] but also formulating them into a logical, systematic, and explanatory scheme. They argue that at the heart of theorising lies the interplay of making inductions [deriving concepts, their properties, and dimensions from data] and deductions. Deductions are the hypothesising about the relationships between concepts, the relationships also are derived from data, but data that have been abstracted by the analyst from the raw data.

For them, theory:

> ...denotes a set of well-developed categories [e.g. themes, concepts] that are systematically interrelated through statements of relationship to form a theoretical framework that explains some relevant social, psychological, educational, nursing, or other phenomenon. The statements of relationship explain who, what, when, where, why, how, and with what consequences an event occurs. Once concepts are related through statements of relationship into an explanatory theoretical framework, the research findings move beyond conceptual ordering to theory (Strauss & Corbin, [1998](#Strauss): 22).

Thus, the findings of the study and the model that emerged from the findings established the theory that Queenslanders with physical disabilities needed virtual communities with specific characteristics.

## Results: a model of virtual communities for persons with disabilities

<div align="center">![figure1](p253fig1.jpg)</div>

<div align="center">  
**Figure 1: The six types of E-Communities that constitute a framework or the cornerstones for a Virtual Community model for persons with long-term physical disabilities. (Note that Access and Barriers relate to all situations - not just those depicted left and right of the diagram.)**</div>

The model for a virtual community for persons with long-term, severe physical disabilities that emerged was a meld of six types of electronic communities (see Figure 1). The types of e-communities or sets of well-developed discreet categories (e.g., themes and concepts) that the data from this study revealed were the following, depending on the types of consumer needs they met:

*   education;
*   fantasy-oriented;
*   information-oriented;
*   interest-oriented;
*   relationship-oriented; and
*   transaction-oriented.

The e-communities or categories are systematically interrelated through statements of relationship to form the theoretical framework that explains the relevant social phenomenon, although they are presented here as separate units for the sake of graphical representation. The statements of relationship between these categories allows for objectification and explains who (relationship), what (information), when (interest), where (transaction), why (education and empowerment), how (fantasy), and with what consequences (access and barriers) an event occurs. For the model to be truly generalisable and transformational, all six types of virtual communities needed to be established as potential portals to be developed for persons with disabilities. It is also possible that the model for a virtual community for persons with long-term severe physical disabilities, in fact, meets several, if not all, of the above needs, purposes or orientations simultaneously. In the end, it must be emphasised that if any one of these types of e-communities were ignored, then the full virtual community experience will not be available to persons with disabilities.

### A sense-of-control: the foundation stone of the model

The need to gain a sense of control appears as a foundational element of disabled persons' interest in and commitment to virtual communities. The central theme or idea that emerged slowly, but strongly, from the open coding analytic process that all eighteen in-depth interviews were subjected to, was that persons with long-term physical disabilities desired to regain 'a sense of control' over their lives. Increasingly, they found that this 'sense of control' was achieved by their use of information and communication technologies and the professionals were as unanimous in their enthusiasm for the technology for their clientele as were the persons with disabilities. Access afforded 'a sense of control' and the barriers that are considered later militate against this control.

> There is a body of research that has created connections between the concept that information and power means 'control' over one's life. This control is 'denied' to people with disabilities in the initial stages of their coming to terms with their disability. The concept of power that grows from persons with a disability being able to access information, as required, leads to their subsequent rehabilitation and their regaining their sense of well-being. First, they must regain this sense of 'control', which may partially be achieved through the ability to access and share information (Bar-tal 1994; Folkman 1984 both cited in Tilley _et al._ [2002](#Tilley): 508).

### Contexts for virtual communities - potential portals

The other particularly significant themes or concepts that emerged from these interviews were the importance of empowerment, the seeking of 'fantasy' or virtual reality, the need and use of information, 'interest', relationship and transactions. The identification of these six phenomena or concepts were the building blocks of the theoretical model or framework for a virtual community that was useful to the users - Queenslanders with long-term physical disabilities - and the facilitators of such a virtual community. They represent potential portals.

The table immediately below presents the 'findings' material sorted to show the relationships between the six (6) categories, the properties, the dimensions and sub-categories according to the Grounded Theory methodology.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Grounded theory relationships from study findings**</caption>

<tbody>

<tr>

<th colspan="4">Phenomenon: _A sense of control_</th>

</tr>

<tr>

<th>Categories</th>

<th>Properties</th>

<th>Dimensions</th>

<th>Sub-categories</th>

</tr>

<tr>

<td valign="top">Education</td>

<td valign="top">e-learning, capacity building, information literacy, technologically literate</td>

<td valign="top">Digital divide, confidence, information exchange, well-being, motivation, skills</td>

<td valign="top">Empowerment, engagement, equality, life-long learning, shared vision</td>

</tr>

<tr>

<td valign="top">Fantasy</td>

<td valign="top">Virtual activities, virtual community, virtual family, virtual living, virtual reality</td>

<td valign="top">Access, anonymity, communicate, interact, participate</td>

<td valign="top">Double-edged sword of technology, barrier-free, recreation</td>

</tr>

<tr>

<td valign="top">Information</td>

<td valign="top">e-searching, equality, virtual communities</td>

<td valign="top">Discussion groups, education, entertainment, legislation, leisure</td>

<td valign="top">Access, choice</td>

</tr>

<tr>

<td valign="top">Interest</td>

<td valign="top">Disability rights, leadership, moral purpose, social justice</td>

<td valign="top">Access, equity, independence, mobility, participation, values-driven</td>

<td valign="top">Assistive technology, civil society, inclusion, self-determination, vision, voice</td>

</tr>

<tr>

<td valign="top">Relationship</td>

<td valign="top">Community role, peer support, systemic advocacy</td>

<td valign="top">Collective experience, shared knowledge, self-esteem, valued role</td>

<td valign="top">Acceptance, identity, privacy, social networks</td>

</tr>

<tr>

<td valign="top">Transaction</td>

<td valign="top">e-commerce, employment</td>

<td valign="top">Investments, shopping, ordering, bill paying</td>

<td valign="top">Tele-working, tele-marketing.</td>

</tr>

</tbody>

</table>

If any one or more of these types of e-communities were to be ignored, then only fragments of the potential virtual community experience would be available to persons with disabilities. Furthermore, the findings also indicated that information and technological literacy enhanced access to the notion of a virtual community, while costs of connecting and various physical and technological aspects caused barriers to a successful virtual community. They included, for example, connection problems for rural and remote people, the 'isolation' double-edged sword issue and, the inaccessibility of chat rooms to people with physical disabilities.

The first type of e-community was the community of education or empowerment that created an environment for capacity building (or social capital) and bridged information literacy and the digital divide and tackled inequities and so forth. For this type of virtual community to be information literate, it had to provide the prerequisites for 'participating effectively in the Information Society, and is part of the basic human right of life long learning' (The Prague Declaration, [2003](#IMILE)).

The second type of e-community was fantasy-oriented where users created new environments, personalities, stories and role-play and were drawn to virtual reality 'activities' because of access and mobility issues. This had not emerged from the data as strongly relevant to the interviewees of this study, but nonetheless, it was a concern under the guise of the double-edged sword; that is, access to particular experiences versus total isolation from such experience. I commented in a chat room that I was doing research into virtual communities for persons with physical disabilities and that I would like ideas about the following:

> 'What is your secret wish? Assume that the Telecommunications and Information Technology industry is going to grant one wish to the disability sector. What should it be? It could be hardware or software... Any wish, no matter how way-out. Just as long as it relates to telephones, computers, the Internet and so on'. I asked for some background and for the respondent to be explicit about how this would benefit themselves or others in a similar situation.

One response was:

> I'd like to try cyber sex with one of those full body suits. I'd also give cyber-sky-diving a go, deep-sea diving, mountain climbing, body surfing - the lot. Stuff I can't do in real life. Virtual reality would bring my wildest fantasies to life.

The third type of e-community that emerged from the data was the information-oriented community. The information-oriented community, for example, sought a personal understanding of the availability and applicability of assistive technology, specific health information for persons with disabilities, as well as information about accessible travel and accommodation, in addition to all the other general informational needs that any person might exhibit. In this study, sometimes this third type of e-community overlapped with the fourth type of e-community, the community of interest, but invariably it proved to be more 'personal'. Burnett contributed to the debate on virtual communities as information-oriented social spaces and developed a model of information exchange in virtual communities and a typology of the varieties of information behaviour to be found in virtual communities. He also drew attention to the fact that virtual communities can function as forums for both information and social-emotional activities. He lamented the fact that little research had been undertaken to determine the relationship between these activities. He concluded that it may be possible that social activities, however trivial, were used in online environments to exchange information of various sorts, and that information sharing itself was a fundamentally social act (Burnett, [2000](#Burnett)). Effectively, this study into a virtual community for people with physical disabilities substantiated Burnett's thinking about virtual communities functioning as forums for both information and social-emotional activities, and as a means of accessing relatively easily a wealth of information: the Internet may well excel.

It was the fourth type of community, that is, the community of interest that was particularly relevant to the model developed for persons with physical disabilities. This was where members had a significantly higher degree of interaction than in the community of information and usually on the topics of their interest that were especially relevant to the more general group interests of the interviewees in this study. These communities usually had chat rooms, message boards, and discussion groups for extensive member interaction. Thus, they were characterised by a significant amount of user-generated content on topics of moral purpose, disability rights, bioethics and such specific issues.

The fifth type of community 'relationship' was built around certain life experiences that were usually very intense and led to personal bonding between members. Again, when asked the same question about a secret wish, one person responded: 'It's not really a secret wish - free Internet Service Provider access for all people with disabilities'. She clarified:

> This is not really a secret wish but a very practical one and one that has the potential to impact on people with disabilities in all areas of Australia. Today, people can use voice-activated programs to communicate, where once this was not possible. So let's hear it for a push to energise and empower the community for e-mail and Internet provision for people with disabilities in Australia and provided free as a service to those who otherwise could not access the world.

The sixth and what may be described as the transaction-oriented communities primarily facilitated the buying and selling of products and services and delivered information that was related to fulfilling those transactions. These communities did not address the member's social needs in any manner. Indeed, there was little, or no interaction, between members, the communities existed solely either to transact business or to provide informational leads or consultations about other possible participants in financial transactions.

### Barriers and enablers for virtual communities for the disabled

The interviewees identified four major types of barriers to people with disabilities forming successful virtual communities, costs, physical, technological and social barriers. The diversity and range of barriers identified by interviewees limited their successful use of information and communication technologies. Most of the interviewees were aware of barriers such as connection problems for rural and remote people, the 'isolation' double-edged sword issue and the inaccessibility of chat rooms to people with physical disabilities. Other barriers included costs because most people with disabilities have limited economic resources. Some of the cost barriers identified included the set-up costs for the hardware and software (for example, voice recognition), assistive technology, the Internet Service Provider (ISP) and the on-going telecommunication cost, which may include broadband. On the other hand, they suggested some solutions to the cost barriers such as cheap, recycled 'green' computers.

Information literacy skills are fundamental to the success of a virtual community for persons with long-term, severe, physical disabilities. Information literacy can contribute to participative citizenship, social inclusion, acquisition of skills, innovation and enterprise, the creation of new knowledge, personal, vocational, corporate and organisational empowerment and learning for life ([ALIA 2003](#ALIA)). While this definition of information literacy affords an insight into the notions encompassed by the term, there are several 'faces' of information literacy worth embracing ([Bruce 1997](#Bruce). Library and information services professionals have chosen to undertake a responsibility to develop the information literacy of their clients. They support governments at all levels, and the corporate, community, professional, educational and trade union sectors, in promoting and facilitating the development of information literacy for all Australians as a high priority. The Council of Australian University Librarians (CAUL) adopted the Australian School Library Association (ASLA) Statement on Information Literacy in 1994\. The Australian Library and Information Association (ALIA) adopted their policy statement on 'information literacy' for all Australians in 2001 and amended it in 2003 (ALIA, [2003](#ALIA)). Information literacy promotes the free flow of information and ideas in the interest of all Australians and a thriving culture, economy and democracy. ALIA believes that a thriving national and global culture, economy and democracy, will be advanced best by the people who recognise their need for information. They also need to be able to identify, locate, access, evaluate and apply that information. The interviewees in this study were intuitively aware of their need for information and to be information and technologically literate as deemed desirable according to the 'Thematic Debate on Information Literacy' (UNESCO, [2005](#UNESCO)).

Some of the more serious telecommunications issues and barriers for consumers with disabilities highlighted by the interviewees were the fact that the telecommunications product market was inaccessible, complex and confusing. Frequently, there was a lack of knowledge on the part of staff about the telecommunications products they sold. Products designed to suit the needs of older people and people with disabilities were also limited. Furthermore, invariably there was a lack of Disability Equipment Program ([Bytheway 2001](#byt01) ) knowledge by staff at [Telstra Shopfronts](http://www.telstra.com/index.jsp?SMIDENTITY=NO), in conjunction with a lack of explanatory brochures or the Program catalogue. There was a lack of access to a disability equipment programme when a consumer's primary carrier was not Telstra, preventing a person with disabilities taking advantage of competitive deals. As consumers, many interviewees suggested that there was a need for a comprehensive, independent, universally available disability equipment program. There was a continuing need to address the manifold difficulties that prevent people with disabilities, especially those on low income, from bridging the digital divide. Being information literate, or technologically literate, usually meant that attitudinal or psychological barriers also were minimised.

## Discussion and conclusions

This study is unique in that a conceptual model was developed that would provide the platform for building a virtual community that met the specific needs of persons with long-term, severe physical disabilities. The model is innovative, sustainable and, if used to help guide the development and implementation of environments that support virtual communities it would be likely to lead to reduced social isolation for people with physical disabilities and would be transferable across communities to some other disability types and timeframes. It was expected that this theoretical model of 'best practice' also contributed to the reduction of social isolation of other population groups, for example, older people.

The Association for Computing Machinery - Computer-Human Interaction Conference (ACM-CHI) in 1997 identified the five necessary multi-disciplinary characteristics of a virtual community. These characteristics are compared, below, with the findings from this study:

*   **ACM-HCI:** members have a shared goal, interest, need, or activity that provides the primary reason for belonging to the community.  
    **This study:** it became apparent that the shared goal, interest, need, or activity for Queenslanders with long-term, severe, physical disabilities derived from the grounded theory 'phenomena' that featured in all their interviews to regain 'a sense of control' over their lives.
*   **ACM-HCI:** members engage in repeated, active participation; often, intense interactions, strong emotional ties, and shared activities occur among participants.  
    **This study:** it is likely that members of that virtual community would engage in repeated, active participation accessing their shared online communities that support sociability through shared areas of need and interest (Burnett [2000](#Burnett)).
*   **ACM-HCI:** members have access to shared resources, and policies determine the access to those resources.  
    **This study:** the members would have access to shared resources through the portals, and they would monitor the policies that governed access to those resources. According to grounded theory, the policies would be described or determined by the 'properties'.
*   **ACM-HCI:** reciprocity of information, support, and services among members is important.  
    **This study:** indicated that members they would reciprocate information, support and services through discussion groups. The 'sub-categories' of grounded theory would define the nature of the information exchanged.
*   **ACM-HCI:** there is a shared context of social conventions, language, and protocols.  
    **This study:** confirmed that there would be a shared context of social conventions, language, and protocols based on shared experiences pertaining to technology, disability, and so forth (Whittaker, _et al._ [1997](#Whittaker)). It would be the 'dimensions' of grounded theory that would outline their social conventions, language, and protocols.

The model outlined above upholds the five necessary multi-disciplinary characteristics of a virtual community as defined at the ACM-CHI Conference. It highlighted that access, equity, participation and self-empowerment were absolutely essential to any virtual community for persons with disabilities.

Schuler presented core values and their attributes for community networks as democracy, education, health, equality, information exchange and communication. He wrote:

> They must rest on the solid foundations of principles and values and be flexible and adaptable, intelligent, and creative. They must be inclusive. Everyone must be allowed to participate. They will have to engage both governments and business because they both exist to provide services for people. These institutions must be accountable to the people, and not the reverse (Schuler, [1996](#Schuler): xi).

In this study into the needs or characteristics of a virtual community or communities for Queenslanders with a physical disability, the six essential characteristics or concepts were identified as education or empowerment, fantasy or virtual activity, information, interest, relationship and transaction. Thus, the needs or characteristics of a virtual community for Queenslanders with a physical disability essentially lined up with the core values that Schuler identified for building community networks.

The definitions of virtual communities provided by Burnett ([2000](#burnett)) and Chang, _et al._ ([1999](#chang)) are not, in fact, challenged or modified by the conceptual framework or model for a virtual community presented here. In the process of developing the model, it was demonstrated how access to information and communications technology, information literacy development, telecommunications and bridging the digital divide, were all crucial components of a virtual community for people with physical disabilities. Indeed, for many such persons all these components were equally vital to their social and economic life. Additional to the general information and communications technology applications that enabled the formation of such a successful virtual community were a variety of applications of particular interest to those with mobility restrictions, for example, the elderly. These included alternate modes of communication and information access and presentation and the opportunity to undertake activities and transactions from the home and remote access to care and other support services. Information services, tele-shopping, transactions like tele-banking and remote access to entertainment and other leisure pursuits, such as, video-on-demand offered new possibilities.

The outcomes are general and applicable to other groups. The adaptation of many of these approaches and findings on information and communications technology in this specific area also indicated wider application. Sectors within a global community needed to be served particularly and many of the concerns identified in this study have served as guideposts for a future serving all types of groups. However, some of the investigations undertaken in this study highlighted a number of serious telecommunications issues or barriers for consumers with disabilities that warranted addressing. We believe that it is essential that the Commonwealth Government, individual carriers and carriage service providers, and the Australian Communications Industry Forum continue to work closely with Telecommunications Disability Advisory Bodies to address and act to resolve these issues.

In conclusion, on the basis of this theory, the study proposed strategies for implementing a virtual community model based on user information needs for people with long-term physical or mobility disabilities. The conclusion of this study was that the technology itself could provide strategies for independence and thus facilitate self-empowerment. However, the process that gives 'a sense of control' and is empowering is also capable of being dis-empowering. Empowerment and dis-empowerment are intersecting processes because of digital divide and information literacy issues and the fact that virtual reality for people with physical disabilities may be a 'double-edged sword'. Based on the new knowledge and the theory as the outcomes of this study, a range of recommendations that have application in the community were available.

## Acknowledgements

The research reported here was conducted as part of a PhD program, in the Faculty of Information Technology at the Queensland University of Technology, Brisbane, Queensland, Australia.

## References

*   <a name="alia" id="alia"></a>Australian Library and Information Association. (2003). [Statement on information literacy for all Australians.](http://www.alia.org.au/policies/information.literacy.html) Deakin, Australia: Australian Library and Information Association. Retrieved 17 February, 2005 from http://www.alia.org.au/policies/information.literacy.html
*   <a name="beamish" id="beamish"></a>Beamish, A. (1995). _[Communities online: community-based computer networks.](http://hdl.handle.net/1721.1/11860)_ Unpublished master's thesis, Massachusetts Institute of Technology, USA. Retrieved 14 December, 2005 from http://hdl.handle.net/1721.1/11860
*   <a name="bruce" id="bruce"></a>Bruce, C S. (1997). _The seven faces of information literacy._ Adelaide: Auslib Press.
*   <a name="burnett" id="burnett"></a>Burnett, G. (2000). [Information exchange in virtual communities: a typology](http://informationr.net/ir/5-4/paper82.html). _Information Research,_ **5**(4), paper 82\. Retrieved 25 November, 2005 from http://informationr.net/ir/5-4/paper82.html
*   <a id="byt01" name="byt01"></a>Bytheway, L. (2001). _[Disability equipment program: draft discussion paper.](http://www.aceinfo.net.au/Resources/Downloads/CORP/DTEPDiscussFinal.pdf)_ Brisbane: Australian Communication Exchange. Retrieved 5 March, 2006 from http://www.aceinfo.net.au/Resources/Downloads/CORP/DTEPDiscussFinal.pdf
*   <a name="carroll" id="carroll"></a>Carroll, J. M. (2001). _Human-computer interaction in the new millennium_. New York, NY: ACM Press.
*   <a name="chang" id="chang"></a>Chang, A., Kannan, P. K. & Whinston, A. B. (1999). [Electronic communities as intermediaries: the issues and economics.](http://csdl2.computer.org/persagen/DLAbsToc.jsp?resourcePath=/dl/proceedings/&toc=comp/proceedings/hicss/1999/0001/05/0001toc.xml&DOI=10.1109/HICSS.1999.772942) Paper presented at the 32nd Hawaii International Conference on System Sciences, Maui, 1999\. IEEE, **3**, 1-10\. Retrieved 6 March, 2006 from http://csdl2.computer.org/persagen/DLAbsToc.jsp?resourcePath=/dl/proceedings/&toc=comp/proceedings/hicss/1999/0001/05/0001toc.xml&DOI=10.1109/HICSS.1999.772942
*   <a name="charmaz" id="charmaz"></a>Charmaz, K. (2003). Grounded theory: objectivist and constructivist methods. In N.K. Denzin & Y.S. Lincoln (Eds.) _Strategies of qualitative inquiry_ (2nd ed.)(pp. 249-291). Thousand Oaks, CA: Sage Publications.
*   <a name="chatman" id="chatman"></a>Chatman, E.A. (1986). Diffusion theory: a review and test of a conceptual model in information diffusion. _Journal of the American Society for Information Science_, **37**(6): 377-386\.
*   <a name="caul" id="caul"></a>Council of Australian University Librarians. (2001). [Information literacy standards](http://www.caul.org.au/policy/p_infol.htm). Canberra. Retrieved 17 February 2005 from http://www.caul.org.au/policy/p_infol.htm.
*   <a name="denzin" id="denzin"></a>Denzin, N K. & Lincoln, Y S. (1994). _Handbook of qualitative research_. London: Sage.
*   <a name="fidel" id="fidel"></a>Fidel, R. (1993). Qualitative methods in information retrieval research. _Library and Information Science Research_, **15**(3), 219-247\.
*   <a name="Glaser1978" id="Glaser1978"></a>Glaser, B G. (1978). _Theoretical sensitivity: advances in the methodology of grounded theory_. Mill Valley, CA: Sociology Press.
*   <a name="glaser1992" id="glaser1992"></a>Glaser, B G. (1992). _Emergence vs. forcing: basics of grounded theory, strategies for qualitative research_. Chicago, IL: Aldine.
*   <a name="glaser1967" id="glaser1967"></a>Glaser, B G. & Strauss, A L. (1967). _Discovery of grounded theory: strategies for qualitative research_. New York, NY: Aldine.
*   <a name="goulding" id="goulding"></a>Goulding, C. (2002). _Grounded theory: a practical guide for management, business and market researchers_. London: Sage.
*   <a name="hackos" id="hackos"></a>Hackos, J. & Redish, J. C. (1998). _User analysis and task analysis for interface design_. New York, NY: Wiley.
*   <a name="hirose" id="hirose"></a>Hirose, M. (Ed.) (2001). _Human-computer interaction. INTERACT'01._ Amsterdam: IOS Press.
*   <a name="imile" id="imile"></a>International Meeting of Information Literacy Experts (2003). _[The Prague Declaration. Towards an information literate society](http://www.infolit.org/International_Conference/PragueDeclaration.doc)_. Cambridge, MA: National Forum on Information Literacy. Retrieved 14 March, 2005 from http://www.infolit.org/International_Conference/PragueDeclaration.doc
*   <a name="lazar" id="lazar"></a>Lazar, J. & Preece, J. (1998). [Classification schema for online communities.](http://www.ifsm.umbc.edu/~preece/Papers/1998_AMCIS_Paper.pdf) In Ellen D. Hoadley and Izak Benbasat, (Eds.) _Proceedings of the 1998 Association for Information Systems, Americas Conference._ (pp. 84-86). Atlanta, GA: Association for Information Systems. Retrieved 6 March, 2006 from http://www.ifsm.umbc.edu/~preece/Papers/1998_AMCIS_Paper.pdf
*   <a name="mayhew" id="mayhew"></a>Mayhew, D. J. (1999). _The usability engineering lifecycle_. San Francisco, CA: Morgan Kaufmann.
*   <a name="mellon" id="mellon"></a>Mellon, C. (1990). _Naturalistic inquiry for library science: methods and applications for research, evaluation and teaching_. New York, NY: Greenwood.
*   <a name="morino" id="morino"></a>Morino, M. (1994). _Assessment and evolution of community networking_. Washington, DC: The Morino Institute.
*   <a name="nielsen" id="nielsen"></a>Nielsen, J. & Mack, R. L. (1994). _Usability inspection methods_. New York, NY: Wiley.
*   <a name="preece1993" id="preece1993"></a>Preece, J. [Ed.] (1993). _A guide to usability, human factors in computing_. Wokingham, UK: Addison Wesley.
*   <a name="preece2000" id="preece2000"></a>Preece, J. (2000). _Online communities: designing usability, supporting sociability_. Chichester, UK: John Wiley & Sons.
*   <a name="preece2001" id="preece2001"></a>Preece, J., Rogers, Y. & Sharp, H. (2001). _Interaction design_. New York, NY: Wiley.
*   <a name="preece1994" id="preece1994"></a>Preece, J., Rogers, Y., Sharp, H., Benyon, D., Holland, S. & Carey, T. (1994). _Human-computer interaction_. Wokingham, UK: Addison Wesley.
*   <a name="reinharz" id="reinharz"></a>Reinharz, S. (1992). _Feminist methods in social research_. Oxford: Oxford University Press.
*   <a name="rheingold" id="rheingold"></a>Rheingold, H. (1994). _The virtual community: homesteading on the electronic frontier._ New York, NY: Harper Perennial.
*   <a name="schuler" id="schuler"></a>Schuler, D. (1996) _New community networks - wired for change_. Reading, MA: Addison-Wesley.
*   <a name="shneiderman" id="shneiderman"></a>Shneiderman, B. (1998) _Designing the user interface: strategies for effective human-computer interaction_ (3rd ed.). Reading, MA: Addison Wesley.
*   <a name="strauss1990" id="strauss1990"></a>Strauss, A. L. & Corbin, J. (1990) _Basics of qualitative research: grounded theory procedures and techniques_. London: Sage.
*   <a name="strauss1998" id="strauss1998"></a>Strauss, A. L. & Corbin, J. (1998) Basics of Qualitative research: techniques and procedures for developing grounded theory. Newbury Park, CA: Sage Publications.
*   <a name="sutton" id="sutton"></a>Sutton, B. (1993) The rationale for qualitative research: a review of principles and theoretical foundations. _Library Quarterly_, **63**(4) 411-430\.
*   <a name="tilley" id="tilley"></a>Tilley, C. M., Hills, A.P., Bruce, C.S. & Meyers, N. (2002), Communication, information and well-being for Australians with physical disabilities. _Disability & Rehabilitation_, **24**(9), 503-510\.
*   <a name="unesco" id="unesco"></a>UNESCO (2005) _[Information for All programme: thematic debate on information literacy](http://portal.unesco.org/ci/en/ev.php-URL_ID=19621&URL_DO=DO_TOPIC&URL_SECTION=201.html)_. Retrieved 3 June, 2005 from http://portal.unesco.org/ci/en/ev.php-URL_ID=19621&URL_DO=DO_TOPIC&URL_SECTION=201.html
*   <a name="whittaker" id="whittaker"></a>Whittaker, S., Issacs, E. & O'day, V. (1997) Widening the net: workshop report on the theory and practice of physical and network communities. _SIGCHI Bulletin_, **29**(3): 127-37\.
