#### vol. 15 no. 2, June, 2010



# "I just want more information about who I am": the search experience of sperm-donor offspring, searching for information about their donors and genetic heritage



#### [Amber L. Cushing](#author)  
School of Information and Library Science, University of North Carolina at Chapel Hill, Chapel Hill, NC, USA

#### Abstract

> **Introduction.** This paper discusses the findings of a qualitative study of sperm-donor offspring conceived in the United States who have searched for information about their donors and genetic heritage. It explores how these individuals search for information and the characteristics of such searches.  
> **Method.** Sixteen telephone interviews were conducted with sperm-donor offspring who had engaged in varying levels of search for varying amounts of time.  
> **Analysis.** Interview transcripts were coded with codes initially developed from the interview guide.  
> **Results.** Results indicate that sperm-donor offspring often begin their search by talking to their mother and then trying to contact their mother's doctor, very soon after being told that they were donor-conceived. Next, individuals use University yearbooks to find "look-alikes." Eventually, some donor offspring attempt to contact prospective donors.  
> **Conclusions.** Overall, this research demonstrates the sometimes intense, emotional and personally driven nature of search. Many participants engaged in search to gain a greater sense of their identity and self.


## Introduction

In the past few years, stories about donor offspring searching for information about their origins have become more frequent in the popular media. However, many of these instances are out-of-the-ordinary success stories. For example, a 2008 television news story reported that a fourteen year old California girl took the vague details that she knew about her donor, entered the information into the social networking Website _MySpace_, retrieved seven results, e-mailed the men, was able to confirm the donor ID number with one man and then met him (Mulvihill [2008](#mul)). The popular media tend to focus on these success stories, but many donor offspring struggle to find scraps of information (Mahlstedt _et al._ [2010](#mah)).

Psychologists write about the difficulty donor offspring experience as they come to terms with how they were conceived, paralleling the emotions experienced by adoptees when they discover they were adopted (Spencer [2007](#spe); Turner and Coyle [2000](#tur)). While much has been written about the experience of adoptees and their search for their origins, little has been written about the search experience of donor offspring as they search for their donor and/or their genetic origins. Why do they search? What are they seeking? How do they define success in search? This research demonstrates that sperm-donor offspring can feel incomplete and that they engage in search to discover information about themselves.

As a donor offspring, the researcher is aware of the difficulty experienced when denied access to information about one's donor, especially when that information is related to the offspring's own health. This knowledge was used in developing the interview questions. In the researcher's experience, recent stories in the popular media do not accurately portray the search for information about one's anonymous donor.

While many individuals conduct searches for information related to work or hobbies, these searches do not tend to be emotionally laded and deeply meaningful. In-depth research about the search experiences of a variety of donor offspring (especially not just those that are successful in finding their donor) is needed to gain a more comprehensive understanding of the search experience which may often be highly emotional and deeply meaningful.

## Literature review

Artificial insemination is defined as '_the introduction of semen into the vagina or cervix by artificial means_' (Jensen ([1982](#jen)). When the semen is from a donor, the process is referred to as artificial insemination by donor (Jensen ([1982](#jen)). The first recorded case of artificial insemination occurred in 1799 in London, UK, but the first known case of artificial insemination by anonymous donor did not occur until the early late 19th century (Jensen ([1982](#jen); Stevens [2001](#st1); Stevens [2009](#st2)). The practice of sperm-donor insemination became popular during the 1940s in the United States and the United Kingdom, as a solution to male infertility (Lorbach [2003](#lor)). Until the last few decades, patients were warned never to tell their children the true nature of their conception. It is difficult to know how many donor offspring were told the truth about their origins, but it is evident that many have been told and seek support in dealing with the news and information about their donor. It is unknown how many such offspring decide to search for their donor and how many do not.

While it is difficult to trace common information practices surrounding donor conception since the 1940s, Stevens's documentaries _Offspring_ ([2001](#st1)) and _Biodad_ ([2009](#st2)) help us to piece together some information. According to Stevens ([2001](#st1)), the few physicians who practiced donor insemination in the mid-20th century controlled information about the identity of the donor. Healthy female patients married to infertile men were provided with the option of donor insemination to conceive a child. The physician chose a donor and did not inform the prospective mother of the donor's identity (Stevens [2001](#st1)).

Over time, the practice of donor insemination changed. Today single women, lesbian couples or heterosexual couples may seek sperm-donor insemination (Blyth & Frith [2009](#bly)). The prospective parents often choose a donor from non-identifying information such as family history, religious background, interests, profession, and academic credentials. (Mahlstedt _et al._ [2010](#mah)). Some sperm banks provide prospective parents with the option of choosing a donor who agrees to have his identity revealed once the child reaches the age of eighteen. Parents of donor-conceived individuals and, in some cases, those individuals themselves were provided with varying degrees of information about their donor. The information provided is mostly dependent on the physician who performed the insemination and/or the sperm bank that provided the sperm. The sperm-donor business is largely unregulated in the United States, which results in inconsistent practices of information release (Freeman _et al._ [2009](#fre)).

Currently, the United States does not regulate record keeping in cases of individuals conceived through donor insemination. In 1989, the National Conference of Commissioners on Uniform State Laws promulgated the Uniform Status of Children of Assisted Conception Act (Shelf [2000](#she)). According to Shelf ([2000](#she)) the act attempted to address the issues of access to information for people born from assisted conception, but did not attempt to regulate records retention in cases of anonymous donation. According to Blyth and Frith ([2009](#bly)), since 1985, only ten jurisdictions nationwide (Austria, Finland, the Netherlands, Norway, Sweden, Switzerland, the United Kingdom, New Zealand and two Australian states) have formally prohibited anonymous gamete (reproductive cell) donation. This prohibition does not provide donor offspring with a right to identifying information about their donor; rather it abolishes a donor's right to anonymity (Blyth & Frith [2009](#bly)). Laws in these jurisdictions clearly state that the donor has no legal or financial obligation to the individual conceived with the donor's sperm. All jurisdictions require donor insemination providers to forward information to a body charged with maintaining a donor register. Laws requiring the maintenance of records about donor insemination practices for a certain period of time differ from jurisdiction to jurisdiction (Blyth & Frith [2009](#bly)).

Turner and Coyle ([2000](#tur)) interviewed sixteen donor-conceived individuals about their experiences to inform theory and counselling practice. They found that participants reported feeling a loss if identity when they were told of the true nature of their conception. All of the participants interviewed had conducted searches for information about their donors. Participants also desired support from friends and family in their search for information and recognition from others that their search was important (Turner & Coyle [2000](#tur)). Mahlstedt _et al._ ([2010](#mah)) investigated sperm-donor offsprings' attitudes about their conception through a questionnaire about their families, relationships with family members, method through which they were told of the true nature of their origins, and their feelings about their donors and the practice of sperm-donor insemination in general. The eighty-five respondents' attitudes toward their conception were evenly distributed from 'very bad' to 'very good' (Mahlstedt _et al._ [2010](#mah)). Mahlstedt _et al._ ([2010](#mah)) found that most donor offspring wanted to know more information about their donor and had searched for information. Curiosity about one's donor and the propensity to search was not related to the desire to escape negative family issues, as most respondents who said that wanted to meet their donor reported positive relationships with their parents. (Mahlstedt _et al._ [2010](#mah)).

According to Spencer ([2007](#spe)), research on donor insemination has been conducted from the point of view of donor offspring, parents, social workers, physicians and psychologists. Spencer identified searching for information about one's donor as one of the themes associated with confronting the reality of being donor offspring.

Freeman _et al._ ([2009](#fre)) studied parents' experiences of searching for their donor-conceived child's donor siblings and donor. Their research surveyed 791 respondents through a questionnaire distributed to the Donor Sibling Registry electronic mailing list. Results indicated the most commonly reported motivation to searching for donor siblings were 'curiosity' and 'for my child to have a better understanding of who he/she is'. The most frequently reported motivations to search for the donor were 'for my child to have a better understanding of who he/she is'; 'to give my child a more secure sense of identity' and 'curiosity about characteristics of my child's donor'. Of the parents of donor offspring who reported searching for information about the child's donor, only 29% had informed their child of their search (Freeman _et al._ ([2009](#fre)). While this study provides needed information about searching for sperm-donors and half-siblings, all data is provided from the perspective of the parents of the donor-conceived individuals. Little information exists about the specific search experiences of donor offspring.

Psychologists draw many parallels between the experiences of donor offspring and adoptees: in particular, both groups experience problems concerning identity development, sometimes referred to as genealogical bewilderment (Turner & Coyle [2000](#tur)). Attributed to Wellisch ([1952](#wel)) and Sants ([1964](#san)), genealogical bewilderment refers to the lack of knowledge about one or both biological parents and overall ancestry. Wellisch ([1952](#wel)) and Sants ([1964](#san)) determined that those suffering from genealogical bewilderment lack a stable concept of self, which can cause other psychological problems. As a result, psychologists sometimes direct patients suffering from genealogical bewilderment to undergo a search for information about their origins, to aid identity development (Turner & Coyle [2000](#tur)). Psychological studies of adoptees have also investigated how adoptees who have found information while searching, incorporate the information into their existing identity (Pugh [1999](#mul)).

While the adoptee rights movement has been successful in opening adoption records for access in many US states and for advocating a more open adoption process, donor offspring have not experienced as much success in their attempt to gain access to information about their origins (Carp [1998](#car)). However, many countries have created national donor registries and donor offspring are allowed to access the registry, usually when they turn eighteen. Currently, the United States has no laws requiring such a registry. Furthermore, it does not have laws about the number of times a specific donor's sperm may be used. In comparison, Sweden limits the number of children born from one donor to six, Switzerland to eight children, New Zealand to ten children, the Netherlands to twenty-five children. Austria, Finland, the states of New South Wales and Western Australia in Australia, and the UK limit the number of families that may conceive children from one donor. Norway uses a combination of children born from the sperm of one donor and the number of families that may purchase sperm from one donor (Blyth & Frith [2009](#bly)).

An early study of adoptees' search for information concluded that half of the study participants viewed their adoption as a failure, prompting psychologists to believe that adoptees who searched had not properly adjusted to their adoptive families (Pugh [1999](#pug)). More recent research about adoptees and search has concluded that search is not a sign of a failed adoption. Research has found that more female adoptees than male adoptees search and a search is most often prompted by a pregnancy, a birthday, a death in the family or the discovery of a genetic medical condition (Sachdev [1992](#sac); Pugh [1999](#pug); Wrobel, Grotevant & McRoy [2004](#wro); Tieman, van der Ende & Verhulst [2008](#tie)). Tieman _et al._ ([2008](#tie)) conducted analysis of covariance (ANCOVA) tests to determine the correlation between various factors such as problem behaviour, psychological well-being, relationship to family, and adjustment within the adoptive family and search behaviour, but the correlations were not significant. Chelton _et al._ ([2001](#che)) found that when interacting with librarians, adoptees often disguise their search task when looking for adoption information resources, including conducting research about a birth parent or seeking information about adoption support services.

In the documentaries _Offspring_ ([2001](#st1)) and _Biodad_ ([2009](#st2)), film maker Barry Stevens details his discovery that he was conceived by donor sperm and his search for information about the donor. In an (anecdotal) study of how archivists can work together with social workers and psychologists, Etherton ([2006](#eth)) noted the obsessive nature of a male donor offspring, willing to search through several thousands of names in an archival repository. The lack of data makes it difficult to ascertain the common characteristics of the donor offspring search experience: a more rigorous investigation of their experiences is needed.

## Methodology

The initial research questions were:

*   How do sperm-donor offspring search for information about their donors and their genetic origins?
*   What commonalities and differences exist between the characteristics of donor offspring searches for information about their donors and their genetic origins?

Interview questions were designed to elicit information about how participants discover that they were donor-conceived, how they search for information and their feelings and reflections about search and donor conception.

Interview subjects were recruited from an announcement sent to the _People Conceived via Artificial Insemination_ (PCVAI) electronic mailing list and an announcement forwarded by the director of a non-profit organization specializing in infertility and donor conception. (As some participants are very concerned about maintaining anonymity, every effort has been made to ensure that that they cannot be identified in any way.) To take part in the study, potential subjects had to be eighteen years of age, have been conceived in the United States and be currently residing in the United States. The researcher restricted this study to the United States because laws concerning a donor-conceived individual's access to information about their donor differs from country to country and comparisons would have been less valid. Finally, participants must have engaged in a search for information about their donor and/or their genetic heritage. Search activity was not limited by form or length of time: any search was considered satisfactory to take part in the study. Participants were offered a $10 Amazon.com gift card as an incentive for taking part in the research study. Hour-long, semi-structured telephone interviews were conducted from July 2009-October 2009\. Some participants wished to answer these questions by providing a chronological account of their experiences, rather than according to the set of interview questions.

All interviews were audiotaped and transcribed. A pseudonym was assigned to each participant. Categories for analysis were initially developed from the interview guide. All interviews were asked about how being donor-conceived related to their identity so the code “identity” was developed. Other codes emerged from the interviews. The majority of subjects spoke of the desire to learn medical information about their donor, so the code “medical information” was developed.

## Findings

Sixteen participants were interviewed; the average age was 37, with a range of 22 to 59\. Fourteen were women and two were men. On average, participants had been actively engaged in search for four years with ranges from two weeks to 12.5 years and still searching.

### Information gathering lifecycle

The majority of participants began searching for information as soon as they were told they were donor-conceived. Participants reported feeling shocked, a sense of loss and anger but also that it 'made sense.' Those reportin that it 'made sense' remarked that they had always felt different from their social fathers and siblings and that this new piece of information helped to justify those feelings. Some of the participants' experiences can be described as traumatic, especially if they were told of their donor conception during their middle adult life. For example, Marie was not told that she was donor-conceived until she was 46 and, by then, had children of her own and had mourned the passing of her social father years earlier. From the table below, it can be seen that age at which offspring where told of their donor conception and the immediacy of the need to search may be related.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1\. Age at which participants discovered donor conception and when they began searching**</caption>

<tbody>

<tr>

<th>Participant and current age</th>

<th>Age when discovered donor-conceived</th>

<th>How long after discovery did you begin searching?</th>

<th></th>

<th>Participant and current age</th>

<th>Age when discovered donor-conceived</th>

<th>How long after discovery did you begin searching?</th>

</tr>

<tr align="center">

<td>Marie, 59</td>

<td>46</td>

<td>immediately</td>

<td></td>

<td>Sam, 54</td>

<td>28</td>

<td>a few days</td>

</tr>

<tr align="center">

<td>Paul, 46</td>

<td>44</td>

<td>immediately</td>

<td></td>

<td>Tina, 26</td>

<td>25</td>

<td>2 years</td>

</tr>

<tr align="center">

<td>Joy, 44</td>

<td>44</td>

<td>right away</td>

<td></td>

<td>Anne, 27</td>

<td>19</td>

<td>6 months</td>

</tr>

<tr align="center">

<td>Erin, 51</td>

<td>39</td>

<td>immediately</td>

<td></td>

<td>Kelly, 25</td>

<td>8</td>

<td>15 years</td>

</tr>

<tr align="center">

<td>Emma, 50</td>

<td>35</td>

<td>a few years</td>

<td></td>

<td>Rose, 27</td>

<td>8</td>

<td>8 years</td>

</tr>

<tr align="center">

<td>Kate, 39</td>

<td>33</td>

<td>right away</td>

<td></td>

<td>Kim, 22</td>

<td>4</td>

<td>15 years</td>

</tr>

<tr align="center">

<td>Tiffany, 32</td>

<td>32</td>

<td>less than a day</td>

<td></td>

<td>Cindy, 31</td>

<td>4</td>

<td>6 years</td>

</tr>

<tr align="center">

<td>Amy, 35</td>

<td>29</td>

<td>right away</td>

<td></td>

<td>Lucy, 24</td>

<td>As early as I could remember</td>

<td>age 18</td>

</tr>

</tbody>

</table>

Most participants were told of their donor conception by their mothers. Amy and Joy were told by their fathers after their mothers had died and Erin was told by her mother's friend after her mother had died and her father was terminally ill. If participants were told of their donor conception when they were young children, they usually did not begin searching for information until they were in their teens. Cindy reported beginning her search at age 10, which was the youngest age reported for beginning to search. Lucy waited the longest to begin her search, but she had known of her donor conception her entire life. Unlike all the other participants, Lucy was not raised with a social father and did not experience the discovery that the man she thought was her biological father was actually not related to her at all.

After being told of their donor conception, participants followed similar search strategies. Taking the information provided by their mother, a handful of participants looked for contextual information because they were not familiar with donor conception and what it actually was.

> Tina: I think 2 years passed before I even tried to do any sort of Internet searching. I didn't even know the words. I didn't even know “donor-conceived.” So I had to start from the basics, “what is this?” And then as I get more information I kind of am building on this, I guess.

Many of the participants turned to their mother's doctor who had completed the insemination procedure, for answers. Some doctors provided non-identifying information, but the majority of doctors were not willing to provide information to the participants. If the doctor had passed away, participants looked for information about the doctor and his medical practice. According to Paul:

> I hunted down the OBGYN [obstetrician/gynecologist] who did the procedure, he's actually still alive, for how much longer I don't know. He was completely unwilling to give any information citing the fact that this was done in total secrecy as was quite common back then and that everyone was sworn to secrecy and the donor had wanted it that way as well.

Lucy contemplated contacting her mother's doctor, but decided to contact the sperm bank. Eventually, she was able to obtain some non-identifying information about her donor. The use of sperm banks in infertility treatment became more common beginning in the late 1970s.

Many of the participants immediately tried to obtain their mother's medical records to look for clues about the donor. Some of the participants already had these records, while others were told that the medical records had been destroyed or that the records contained no information about the donor, so they did not take their attempt to obtain the records any further.

Almost all of the participants were told by their mother and/or their mother's doctor that their donor was a medical student. As a result, a majority of the participants turned to medical school yearbooks for information or spoke of planning to search yearbooks for potential look-alikes in the near future. Sam, Kim and Kelly did not search yearbooks because they identified their donor through other methods. Paul preferred to search using information from a genetic genealogy test, an option only available to men because of the ability to trace the Y chromosome through different generations. Anne did not know the name of the medical school her donor attended so did not consider using yearbooks as an option.

While some participants learned about searching yearbooks from donor offspring and adoption electronic mailing lists and support groups, several of the participants reported coming up with the idea of searching yearbooks on their own. Cindy reported that she was on a yearbook committee in junior high school which made her think of looking in medical school yearbooks, while Rose's mother suggested looking in medical school yearbooks because a neighbour's friend had attended medical school and had just received his yearbook, confirming that they did indeed exist. If participants successfully identified look-alikes the next step was to contact those persons:

> Cindy: I went to the medical school who happened to have photo yearbooks and went through the yearbooks and basically tried to narrow down people who met the height requirement, the hair colour, the eye colour and I literally called the offices of every single guy.

### Search and the Internet

While most participants turned to similar information resources in the same order, the growth of the Internet changed searching for information about one's donor. Sam, Cindy, Emma and Marie all began their search before 1997 and did not mention using the Internet. Marie made extensive use of library resources such as microfilmed newspaper articles, inter-library loan and journal from a medical library. She describes starting her search on the Internet, finding little available and then turning to traditional library resources:

> Marie: I'm a librarian. The next day was a Monday, we had our first Internet terminal at the library and I'd been trained to use it. I went on to the Internet. But of course, the Internet was in its infancy, there was absolutely nothing on it in 1996\. There was nothing. I had to do traditional library work and you know, I found one book in the stacks where I was, I can't remember what the title was. I needed something to read immediately because I was a wreck.

If participants started their search after 1997, the majority of the search was conducted online. Figure 1 is a timeline of when participants reported actively searching. The participants with light blue searches reported the Internet as one of their most important search tools. About half of the participants reported that they were currently actively engaged in searching for their donor. Of the participants not currently engaged in search, Sam, Kim and Kelly have found their donor, Cindy thinks she has found her donor, and Amy, Lucy, Marie and Rose felt that at present there is nothing more they could do that would add to their search or bring in any new information.

<div align="center">![figure1](p428fig1.png)</div>

<div align="center">  
**Figure 1: Timeline of Search Activity for All Participants**</div>

Cindy, Amy, Lucy, Anne, Emma and Rose all described cycles of intense search activity followed by a period of less activity. Anne and Emma completely stopped their search and restarted it years later. According to Cindy:

> I went back and forth like that for a little bit. Also just researching these two people. And then after that, I just kind sat on it for a while and went through life for a few years and just kinda thought I think I know who my donor is, that's good enough. And then, over time, it became a little bit more important again. Its kinda weird, it goes in cycles, you know? I feel like I want to know more and at other points I just don't care so much.

These cycles could be related to the emotional nature of the search. Lucy reported that searching can be an emotional drain:

> its [searching] just very emotionally and physically and mentally time-consuming and so I think I put it on the back burner for my own sanity, a lot of the time.

Other participants reported spending more time at the beginning of the search and then waning after time, when they began to discover little new information.

### Searching for support

Many participants mentioned being helped by other PCVAI electronic mailing list members or the mailing list founder, B. The Yahoo group is open to donor offspring only and upon joining, each member is required to provide a summary about themselves, to serve as an introduction to other members. As of January 2010, the group had 231 members worldwide. Marie, Joy, Kate, Tina, Sam, Amy, Anne, Erin and Lucy all mentioned coming across PCVAI or an article about B and his search for his donor when beginning their own searches. Many reported that hearing about B's experiences and reading the posts of other donor offspring as being very helpful, especially when participants first discovered that they were donor-conceived:

> Anne: So I really wanted to find other people I could talk to that had some experience with it and that's how I found the Yahoo group. They were, they were really, for most of the time have been my primary source of information about the practice of donor conception.

However, the Yahoo group was not as useful for Amy:

> There's a lot of anger in that group. And there's a lot of pain and I would say that I don't share a lot of that. I have a little bit, a kind of easier time relating to it [being donor-conceived] than some of the folks that are on the site.

Cindy mentioned searching for support groups and eventually connected with a neighbour who was also donor-conceived. Tiffany mentioned seeking the support of church friends and support from her faith. Emma mentioned 'connecting with people', but did not say specify with whom or of what kind (support group, mailing list, or whatever). Kim, Kelly, Rose and Paul did not mention seeking support of any kind but Paul mentioned seeing a psychologist at the time.

From these statements, it appears that during the early stages of search, participants often searched for support while they were simultaneously searching for information about donor conception and information about their donor.

### Making contact

Unlike some stories in the popular media, sperm-donor offspring are not always wholeheartedly accepted into their donor's life after making contact. After searching for look-alikes in medical school yearbooks, Emma, Cindy, Rose and Amy all attempted to make contact with men whom they thought were their donors. All of these women first sent letters to the men from the yearbooks with whom they thought they shared a resemblance. They were able to find contact information for the men using the Internet or by calling the alumni office. Emma and Amy contacted a few men who either did not respond or responded that they had never donated sperm. For both women, there was one man in particular whom they thought might be their donor: they both reported having 'a feeling' about each of the men because of a strong physical resemblance. Amy sent the man most likely to be her donor a total of three letters before he responded that he never made a sperm donation. Emma wrote several letters and called the man she thought most likely to be her donor, but he said he had not donated sperm. However, Emma holds out hope that she might have had the right man, because, as she reports, when she called this man, 'he had to go into another room, like have privacy before he could talk. So that makes me think that he probably has not told his wife or his family members if it is him.'

Cindy reports that she did not feel comfortable contacting the donor directly so she developed an alternative strategy to gain more information. After gathering contact information from the alumni office, she contacted the medical practices of her potential donors and asked a secretary in the office to physically describe the doctor (and potential donor):

> I kind of made up a little bit of a story. I said hey I'm looking for a doctor I think it may be Dr. Smith or whoever, can you kind of just give me his physical description, how tall is he, what eye colour does he have…I was a teenager, I was like 14, somewhere around there and I'm calling all these people. It was funny, these women were very helpful. I think they were probably like, "this is a weird question"

Rose conducted the most extensive search of any of those interviewed. After visiting the medical library to view the yearbooks and getting contact information from the alumni office, Rose sent out a total of 600 letters, one to every man pictured in the yearbook. To organize her search, Rose created alphabetical binders to keep track of all the men:

> So I ended up creating these two binders, organized alphabetically and I devoted one page per man where I put his yearbook pictures, the pictures I found online and contact information. I did this for 600 men and I started out in rounds of twenty, writing the ones that looked most like me and eventually I wrote all 600\.

Rose achieved an impressive response rate: 250 men responded to her letters and forty formerly anonymous sperm-donors came forward. Rose completed DNA tests with 16 of the men, but all results were negative. She says that she is glad she did this because she has remained friends with many of the men and their families.

Rose and Amy said that they were 'very careful' about how they phrased the letters and how they decided which information to include:

> Rose: I just explained about my conception, that I was looking for my sperm-donor, I explained what I am looking for. I'm also diabetic and like I said no medical history. I explained that part of it. I explained what I'm looking for. I'm not looking for anything financial. I'm not trying to force a relationship, I just want more information about who I am. I tried to address any concerns that they might have and I only used a non-legally binding DNA test that could not be used in court because I thought that that might ease their mind a little bit. I also sent a timeline of my photographs from infancy through currently.  
>   
> Amy: The first letter I sent pictures and I sent a picture of me as a child, I also tried to make it clear that I was happy and happily married and happily living a life and reasonably successful in... basic terms. I tried to make it clear that I wasn't a kook trying to meet some unfulfilled emotional needs you just have the sense that people, they just don't want to... they're wary of biting off more than they can chew in certain circumstances.

After visiting his mother's doctor who was near death, the doctor told Sam the name of his donor. After sending a letter to the donor, Sam received a letter from the donor's attorney, stating that he did not want to have any contact with Sam. Years later, to gather information, Sam visited the archives of a university where the donor was a faculty member. As he describes it, this was his chance to meet his donor-he was in the town and had the donor's address:

> I'm driving out of town and I see a phone booth on the side of the road and I think "Sam, you are such a pussy! Why don't you stop in and see this guy?" And I'm talking to myself "well, I don't want to. I'm scared." Well, you're never going to have another chance, so I pulled the ten cents out of my pocket, put it in the phone, dialled, his wife answered the phone and she says yeah he's in the back can I help you and I said I'm just an old friend, I want to stop by and say hi, she says come on over. So I came on over. I knocked on the door, she came to the door and she went in the back and got him and I about died. This guy was huge! He's like 6'4", built like a football player, grey hair, glasses and he looks over his glasses and says, "Yes?" I said, Professor So and So, "Hi I'm Sam." And he says, "Where do I know you from?" And our hands are locked. I'm melting, like ice. I said "[Midwestern town]." And his eyes got a little wider. "Dr. so and so" and his eyes got really wide and turned three shades paler and I knew he made the connect. He knew who I was because I had tried contacting him years earlier through his attorney and I got a form letter back from the attorney saying he wanted nothing to do with me. He respected my wishes, but he doesn't want anything to do with me. And that hurt, that hurt a lot. And so all I wanted to do was put an end to it. So anyway, our hands are locked and I say all I want to do, and I know he thought I wanted money or something. So I said all I want to do is thank you for giving me the gift of life, I'm never going to bother you again and I turned around and I walked away and that was it.

Marie, Joy, Tina and Erin mentioned looking at yearbooks and/or class photos, but could not find a look-alike so did not follow through with contacting donors using this method. As Rose put it, '_I thought would happen is that a male version of me would just pop off the page in a couple hours and that isn't what happened at all_.' Lucy and Kate mentioned wanting to look at yearbooks, but were not living near the medical school and did not think they could make inquires as to whether they could have copies of the yearbook pages sent from the archives. Tiffany had only been conducting her search for a few months, but wanted to look at yearbooks if she could confirm the name of the medical school. Anne was also uncertain of the name of the medical school the sperm-donor may have attended.

With the exception of Tina, all the participants interviewed said that they wanted to meet their donor (if they had not already), if given the chance. Tina said that she was mainly interested in learning personal information about herself, mainly medical history. With the exception of Tina, discovering the identity of the donor was always the goal of the search. In addition to discovering the identity of the donor, eleven of the sixteen participants said they wanted medical information about the donor and count the desire to know medical information as one of the main reasons for beginning to search. According to Marie:

> The medical issue even as I get older, I'm a very healthy person, but yet the medical issues... my sister recently was diagnosed with a cancer and I was like "oh my gosh" and then you go, I only have a 50% chance of getting that cancer... there is no cancer in my mother's family. Perhaps it was in her donor's family... who knows why, it's not necessarily a genetic cancer but it could be.

### The Donor Sibling Registry

Several participants mentioned posting to the [Donor Sibling Registry](http://www.donorsiblingregistry.com/), which was founded in 2000 by Wendy Kramer, a mother who conceived her son by donor insemination. As of 2009, the Registry included 25,973 members. For a $40 annual fee, donors, donor offspring and parents and social parents of donor offspring may post to the Registry. The Registry asks members to provide information such as the mother's doctor and fertility clinic, the date of donation or insemination and any physical features the donor or offspring can provide about themselves. Many of those who post do not have complete information, so the forms are open-ended. Many of the donor offspring are able to find half-siblings and donors because of their unique donor number. While it is not clear when women conceiving by donor insemination were first provided unique donor numbers, it is rare for those born before 1980 to have a donor number. Of those interviewed only Lucy (born in 1985), Kim (born in 1987) and Kelly (born in 1984) had donor numbers. Many of the participants interviewed were born before 1980 and some expressed frustration at the fact the younger donor offspring are provided with a 'magic' number that gives them information, while these older offspring are left to grasp whatever scraps of information they can find.

The Donor Sibling Registry proved especially useful to Kim and Kelly. Kim (22) and Kelly (25) are full sisters conceived through the same donor (after speaking with both, the author determined that they were sisters because of their similar stories and the fact that they mentioned each other by name). Two weeks after Kim posted to the Donor Sibling Registry, she was contacted by her donor. After the initial contact, Kelly took on the role of verifying her sister's find and stepped in to converse with the donor:

> I didn't believe it was him because he said all this stuff you'd want to hear if you were looking for your Dad. That he was an attorney and that he was interested in finding us. So I just thought that it was a scam or something and I was really mad at my sister because she had just given him more information. So he has some kind of little Website and I went to this site because it had some pictures of him and like I said I never believed it was him before and then I looked at one of the pictures and it was just, it was my sister with a beard. So I just knew it was him.

After conversing over e-mails, Kim and Kelly were able to meet their donor and now have a relationship with him. They count themselves as very fortunate because few formerly anonymous donors are willing to come forward and post on the Registry, let alone welcome into their lives the children conceived by a sperm donation. Kim and Kelly both reported having a strained relationship with their social father before they began searching and both mentioned feeling rejected. The fact that they are now able to have a relationship with their donor father was in a way, healing for them. According to Kim, finding her biological father:

> ...made me feel better about myself that in this situation that's bad, it's OK because he's [social father] not really my Dad. It kind of made me excited because I was this normal person but then I had this crazy background father of mystery and I don't know who he is. He could be anyone! I thought it was kind of exciting and cool.

Kim has also come to view the fact that she was donor-conceived as positive, unlike all of the other participants interviewed, with the exception of her sister Kelly. Kelly acknowledges that she is lucky to have found her donor father and that there is a huge potential for disappointment when offspring search for their donors.

Other participants mentioned using the Donor Sibling Registry. Lucy reported finding potential donors and potential half-siblings listed on the registry, but after completing DNA tests with the potentials, she described immense disappointment and despair. She links these feelings to the fact that over time, she would develop a bond with these potential donors or siblings, which were shattered by a negative result:

> A lot of times you get so emotionally attached to these people, you have to grieve like you do when someone dies and then eventually that turns into... you have to go through a mourning period and its over and then you go back to living.

Anne mentioned posting to the Registry when it was free, but did not want to pay the annual fee to correct and add to the information provided in her posting. Joy, Kate, Tina, Amy, Rose Paul and Erin all mentioned that they had posted to the DSR, but have yet to find a donor or sibling match. Joy was able to find an individual whose mother was a patient of the same obstetrician/gynaecologist as her mother. Paul admitted that he thinks that his posting on the yDNA genealogy registry will prove more fruitful than a posting on the Registry. Marie (incorrectly) believed she was too old to post on the Registry because she did not have a donor number.

For offspring with donor numbers, the Donor Sibling Registry can be a great resource for locating half-siblings who share the same donor, or sometimes even donors themselves. A recent Slate.com headline asks "Are sperm-donors really anonymous anymore?" and emphasizes how easy it was for several mothers to use the Registry to find their child's half-siblings. (Lehmann-Haupt ([2010](#leh)). However, many donor offspring, especially older individuals, are provided with little concrete information about their donors, making a search of the Registry less fruitful and searching in general, more difficult.

### The search evolves

For some participants, the search for information about their donor has evolved into new kinds of search. After finding the man she thought to be her donor, she began doing his genealogy. Cindy is Mormon and explained that the importance of genealogy in her religion, which prompted this search behaviour. While engaged in genealogy research, Cindy began to research the donor's immediate family, including his children and used some unconventional tactics to get at the information:

> Yeah and I happen to, the picture in the yearbook book was just of him, some men have pictures with their families, that kind of thing, so it didn't show his wife or anything, but I had somehow, on that alumni list, it showed his home address and I just happen to work at [a test preparation company], you know... those people? Anyway, I was working there and... a lot of things I've just kind of found out by chance. I was doing something in the computer someday at [the test preparation company] and it pulled up a girl with the same last name at a university like forty miles away... same last name and home address. So I'm going, oh, well that's gotta be his daughter so I kinda started poking around and figured out if I type in their last name and this university then it pulls up all of his kids, all three of them. I found out all of their names that way.

Cindy now regularly searches the Internet for information about her potential half-siblings and their children. Cindy expressed fears that since her children are the same age as her potential half-nieces and -nephews, that her children might meet and marry these potential half-cousins:

> Actually one of his kids has a blog that I actually read. My two kids know pictures of her daughter, that kind of thing. Because if I am right, this is the person, I don't want my kids growing up to like, marry their cousin. No matter what the relationship is, that worries me. The situation is that he lives two states away and his kids are now all over the country but they have all gone to school here at a church university that my husband graduated from and we're the same religion and so and I think, if all of our kids end up going to that school.

While Amy, Emma, Anne and Lucy still say they are searching for information about their donors, the focus of their searches has shifted to these other, related topics. Amy is a filmmaker and has begun making a documentary about donor conception and her experiences as a donor offspring. Emma conducted research on the experiences of other donor offspring to earn her masters degree in psychology. Anne has turned her search toward searching for information about her sister's donor and her sister's half-siblings, after her sister passed away unexpectedly. Lucy says that she has expanded her search to focus on searching for donor-conceived half-siblings and has since found a half-sister.

### Search and advocacy

Sam, Rose and Lucy all mentioned being interviewed by national medial about their search experiences. Media requests are regularly circulated on donor-conceived related mailing lists. Sam and Rose reported being interviewed after they had completed their active search:

> Rose: This lady in Canada, she made a media request through the DSR [Donor Sibling Registry]. She did phone interviews and she liked my story. That was my first media and it kind of exploded from there. I've been in national and international media, sixteen times I think.  
>   
> Sam: I didn't know it [the search] was going to be so hard or take so long. Very few people at the time, when I came public with my story, it was in the late 80s and people were very interested in it. I was in magazines, newspapers, Joan Rivers, Geraldo, the NBC Nightly News...

Sam's objective is to disseminate information about anonymous donor insemination. He believes the practices are unethical:

> I am actually totally, 100% against donor insemination using anonymous donors. Period. The people that participate in that, the parents, the doctors and the donors really don't know the internal ramifications that come out of this situation that they're creating.

Rose and Lucy currently maintain blogs where they post recent media stories about donor conception and offer tips to other donor offspring beginning a search. According to Lucy,

> I feel I've done as much as I feel I could have done, at this time to find those answers and to find siblings and I think that's why I devote a lot of my blog to giving advice and giving ideas to other people because I feel like, you know, I want to pay it forward. I know so many people don't know where to start and being in library school you find how ridiculous some people's search abilities are and I feel like putting information out there about ways to do things and resources, I feel like then the next person who wants to go and search has an easier time than I did. I have six years of experience and advice and information on my blog. I don't want the next person to spend six years finding what I already found.

The desire to inform others about what it is like to be a donor offspring and/or provide information to others searching, demonstrates advocacy at the end of the search cycle. Sam, Amy and Rose have ended the search for their donor. Lucy is still searching for her donor, but admits she is currently investing more time into searching for siblings. While participants were not directly asked about advocacy activities, it is clear that this stage may be an important part of the search cycle for some. More research is needed to determine why some offspring engage in advocacy activity and some do not, whether those who engage in these activities consider themselves advocates and whether they receive any therapeutic or coping value from acting as an advocate.

### Identity issues

> Kate: Its like I'm not just searching for answers in my career or my love life anymore, but now I'm searching for answers about my own identity.

When asked how being donor-conceived effected their identity, most of the participants expressed frustration at the lack of knowledge they have about themselves. About half of the participants felt that they only could be certain about half of their identity, the part the received from their mothers. Some of the participants specifically tied this to genetics, heritage and genealogy. Throughout the interviews many participants mentioned feeling frustrated that when in conversation, someone (asking about heritage) would ask, 'What are you?' because they felt they could not answer the question truthfully or provide an answer at all.

> Kate: Just this summer when I was at training, it was primarily natives who were in this training programme and interestingly, most Alaska natives now are not pure blood Alaska natives. Most of them are, you know, one parent was native, the other parent was who knows what. The subject kind of came up-people would ask me if I had native blood and I honestly said "I don't know."

Many participants discussed looking and feeling different from their nuclear family and associated this with the 'nature versus nurture' debate. Some participants specifically mentioned feeling different from siblings who were not donor-conceived, while others felt misunderstood. According to Emma:

> I think that growing up, my parents wanted me to be a certain way but yet, my genes were different. I was just different than what they wanted me to be so I had a lot of identity confusion around choosing my career, I never knew what I wanted to do.

A few of the participants said that their mother had died when they were a child or in their early 20s and therefore, they felt alone because they no longer had a living parent in their life to whom they were genetically related. According to Amy, whose mother died when she was ten,

> In terms of thinking about who I am and what my family story is I definitely feel sort of like a lone satellite.

Throughout the interviews, most participants mentioned a feature or characteristic about themselves, explained that the trait did not come from their mother and would then attribute the trait or characteristic to the donor. They stated this as fact even though none of the participants revealed that they had an advanced understanding of genetics. They made if/then statements concerning physical features and personality traits: if not my mother, then the donor. Participants explained the characteristics and traits that they thought came from the donor with different levels of confidence: while some participants were certain that specific characteristics and traits came from the donor, other participants were unsure. Rose, Kelly and Anne express different levels of confidence about attributing their own characteristics and traits to their donor:

> Rose: My favourite programme is Discovery Health and all I know about my sperm-donor is that he is a medical doctor so there are things that I know came from him.  
>   
> Kelly: ...like my little sister and I are both kinda curvy and we would joke that our Dad was black because we had big butts...  
>   
> Anne: I look a fair amount like my mom, and so I've got that but you know, I don't know what's from her and what's from this mysterious donor person I've never met.

While Rose appears very confident knowing the fact that her interest in health must come from her donor, Kelly uses racial stereotypes to make a joke out of the issue and Anne wonders about the issue aloud. Many participants mentioned looking in the mirror and wondering where what they saw came from.

### Identity and search

When asked if searching related to their identity, feelings were mixed. Tina and Emma felt that searching was an empowering experience, while Kate resented the fact that she had to search for information about herself that she should have had anyway. Cindy felt that search reminded her of her link to her mother, who had passed away, while Anne felt search reminded her of her relationship to her sister, who had passed away. Kim, Kelly, and Erin did not believe search related to their identity at all.

Sam, Erin and Rose all said that the search experience boosted their confidence and their view of themselves. For Sam, the fact that he was able to locate and meet his donor proved to himself that he could do what he set his mind to, while Erin said it confirmed that she was good at searching. Both Sam and Erin referred to themselves as 'detective' or 'sleuth.' Rose, who had copied four years worth of a medical school yearbook and undertaken the monumental task of writing to 600 men to ask if they had donated sperm the year she was conceived, expressed that searching had 'boosted her confidence' because her searching abilities demonstrated that she could 'think outside the box.' Marie felt that searching suited her personality because she was a librarian. While many participants responded that searching had been a positive experience, some said it had not and would not be regarded as positive, until they reached their goal of finding their donor.

Lucy and Marie expressed that searching strained their relationships with their mothers because they felt that their mothers felt hurt and unloved if they went searching for another parent.

Lucy felt that searching was intrinsic to being donor-conceived:

> I feel like searching is so intrinsic to donor conception. Its almost like... to me its so intrinsically connected. Because I'm donor-conceived I have to do this searching to find who I am and it makes me, it gives me an identity that other people don't have.

## Conclusion

While popular media may give the impression that searching for one's anonymous donor is easy and always ends happily ever after with offspring and donor developing a relationship, this is not so in all cases. Most of the participants interviewed have spent years searching, without finding their donor. Those few who have found their donor (or think that they have found their donor) are often rejected or in some cases, not even acknowledged.

For many participants, searching for their donor was a life-altering experience and a vital necessity in order to learn needed information about themselves. Further research is needed to determine why some donor offspring search and some do not and why some engage in advocacy activities while others do not. Additionally, participants' relationships with the information discovered while searching may provide clues as to an individual's affective relationship with information in an emotionally heightened context. Specific characteristics of this type of search such as the simultaneous seeking for the donor as well as emotional support and why some individuals become information advocates could also direct future research. Overall, this research demonstrates the intense, emotional, personally driven nature of sperm-donor offspring's search for information about their donors and genetic heritage.

## Acknowledgements

This research was partially funded by the American Society of Information Science and Technology SigUse Elfreda A. Chatman Research Proposal Award. The author wishes to thank Dr. Barbara Wildemuth, for her guidance during the study and Kjersti Kyle for her help in formatting the paper.

## About the author

[Amber L. Cushing](http://www.unc.edu/~cushinga/) is a doctoral student at the School of Information and Library Science at the University of North Carolina at Chapel Hill where she also serves as a research assistant for the Archival Metrics and User Evaluation for Government Archives project, funded by the National Historical Publications and Records Commission. Her current research interests include individual's affective relationships with records, personal digital archiving and maintaining behavior. Her dissertation work examines the influence of self-extension on individual's conscious decisions to save digital objects on personal computers. She can be contacted at [cushinga@email.unc.edu](mailto:cushinga@email.unc.edu)

<h2>References</h2>

*   <a id="bly" name="bly"></a>Blyth, E. & Frith, L. (2009). Donor-conceived people's access to genetic and biological history: an analysis of provisions in different jurisdictions permitting disclosure of donor identity. _International Journal of Law, Policy and the Family_, **23**(2), 174-191.
*   <a id="car" name="car"></a>Carp, E. (1998). _Family matters: secrecy and disclosure in the history of adoption._ Cambridge, MA: Harvard University Press.
*   <a id="che" name="che"></a>Chelton, M., Brody, R. & Crosslin, D. (2001). Adoption searchers' use of libraries. _Reference Quarterly_, **40**(3), 264-273.
*   <a id="eth" name="eth"></a>Etherton, J. (2006). The role of archives in the perception of self. _Journal of the Society of Archivists_ **27**(2), 227-246.
*   <a id="fre" name="fre"></a>Freeman, T., Jadva, V., Kramer, W. & Golombok, S. (2009). Gamete donation: parents' experiences of searching for their child's donor siblings and donor. _Human Reproduction_ **1**(1), 1-12.
*   <a id="jen" name="jen"></a>Jensen, B.J. (1982). Artificial insemination and the law. _Brigham Young University Law Review_ **1982**(4), 935-990.
*   <a id="leh" name="leh"></a>Lehmann-Haupt, R. (2010, March 1). [Are sperm-donors really anonymous anymore?](http://www.webcitation.org/5q4JdmhLS) _Slate_ Retrieved from http://www.slate.com/id/2243743\. (Archived by WebCite® at http://www.webcitation.org/5q4JdmhLS)
*   <a id="lor" name="lor"></a>Lorbach, C. (2003). _Experiences of donor conception: parents, offspring and donors through the years_. London: Jessica Kingsley Publishers.
*   <a id="mah" name="mah"></a>Mahlstedt, P.P., LaBounty, K. & Kennedy, W.T. (2010). The views of adult offspring of sperm donation. Essential feedback for the development of ethical guidelines within the practice of assisted reproductive technology in the United States. _Fertility and Sterility_ **93**(7), 2236-2246\.
*   Mulvihill, K. (2008, December 1). [Teen uses the Web to reunite with biological father](http://www.webcitation.org/5q4JskWs1). _KSL TV News_. Retrieved from http://www.ksl.com/?nid=148&sid=4951040\. (Archived by WebCite® at http://www.webcitation.org/5q4JskWs1)
*   <a id="pug" name="pug"></a>Pugh, G. (1999). _Unlocking the past: the impact of access to Barnardo's childcare records._ Brookfield, VT: Ashgate.
*   <a id="sac" name="sac"></a>Sachdev, P. (1992). Adoption reunion and after: a study of the search process and experience of adoptees. _Child Welfare_ **71**(1), 53-68.
*   <a id="san" name="san"></a>Sants, H. (1964). Genealogical bewilderment in children with substitute parents. _British Journal of Medical Psychology_ **37**(2), 133-141.
*   <a id="she" name="she"></a>Shelf, A. (2000). Need to know basis: record keeping, information access, and the uniform status of children of assisted conception act. _Hastings Law Journal_ **51**(5), 1047-1072.
*   <a id="spe" name="spe"></a>Spencer, L. (2007). _Sperm donor offspring: identity and other experiences._ Charleston, SC: Booksurge.
*   <a id="st1" name="st1"></a>Stevens, B. (Director). (2001). _Offspring_. [Documentary]. Toronto: Barna-Alper Productions Inc.
*   <a id="st2" name="st2"></a>Stevens, B. (Director). (2009). _Bio-Dad_. [Documentary]. Toronto: Barna-Alper Productions Inc.
*   <a id="tie" name="tie"></a>Tieman, W., van der Ende, J, & Verhulst, F. (2008). Young adult international adoptees search for birth parents. _Journal of Family Psychology_ **22**(5), 678-687.
*   <a id="tur" name="tur"></a>Turner, A. & Coyle, A. (2000). What does it mean to be donor offspring? The identity experiences of adults conceived by donor insemination and the implications for counseling and therapy. _Human Reproduction_ **15**(9), 2041-2051.
*   <a id="wel" name="wel"></a>Wellisch, E. (1952). Children without genealogy: a problem of adoption. _Mental Health_ **13**(1), 41-42.
*   <a id="wro" name="wro"></a>Wrobel, G., Grotevant, H. & McRoy, R. (2004). Adolescent search for birthparents: who moves forward? _Journal of Adolescent Research_ **19**(1), 132-151.

