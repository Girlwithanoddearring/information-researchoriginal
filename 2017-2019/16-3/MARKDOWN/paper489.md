#### vol. 16 no. 3, September, 2011

# Potential for inclusion of information encountering within information literacy models

#### 
[Sanda Erdelez](#authors) and [Josipa Basic](#authors)  
School of Information Science and Learning Technologies, University of Missouri,  
303 Townsend Hall, Columbia, Missouri, 65211, USA  
[Deborah D. Levitov](#authors)  
School Library Monthly, 3520 South 35th St., Lincoln, Nebraska, 68506, USA

#### Abstract

> **Introduction.** Information encountering (finding information while searching for some other information), is a type of opportunistic discovery of information that complements purposeful approaches to finding information. The motivation for this paper was to determine if the current models of information literacy instruction refer to information encountering.  
> **Method.** Through a literature search we identified five information literacy models popular in the U.S. elementary and secondary school environment and evaluated their descriptions to determine if they include information encountering. We relied on the literature sources that provide initial descriptions of the information literacy models and the secondary literature that discusses the application of the models.  
> **Analysis.** The analysis for the presence of information encountering first included independent readings by the research team members, followed by a collective discussion of observations to formulate the findings.  
> **Results.** None of the information literacy models included explicit reference to information encountering or other types of opportunistic discovery of information; however, they all have components that can accommodate this type of information behaviour.  
> **Conclusions.** Within each of the five analysed models there are stages where natural occurrences of information encountering are possible and could be articulated for students. Additional empirical research is needed about the impact of information encountering-enhanced models of information literacy on the students' learning outcomes and instructional processes.

## Introduction

Information literacy is an important life skill in modern society. For close to three decades, information literacy models have provided pedagogical tools and learning strategies for teaching students about the information research process. These models are used by teachers in many U.S. schools, from kindergarten to high school, to guide the students through their research projects and help them learn how to use library resources.

Information literacy models typically include a description of specific steps that the students are expected to complete while conducting research. These steps provide useful scaffolding for novice information seekers as they gain more search experience and learn how to address more complex and time consuming research activities. However, the prescriptive simplicity of these steps is criticized by some authors because it does not capture the opportunistic and exploratory dimensions of research activities. For example, George describes the traditional methods of teaching research as 'mechanical and dubiously precise' with no elements of surprise that are the common characteristic of most satisfying and worthwhile discoveries ([George 2005](#geo05): 381). Nutefall and Ryder ([2010](#nut10)) have similar views regarding the impact of the imposed structure on students' research writing, commenting that students who are too organized can become engaged in identifying a list of arguments supporting predefined concepts instead of writing an exploratory research paper.

Specific information activities depicted within information literacy models include concepts such as identification of information needs, selection of information sources, information seeking and information use, which have been traditionally the domain of human information behaviour research. The predominant research theme in this field for the last fifty years has been users' information seeking behaviour in various information environments. Based on this empirical research, various models of information problem solving and information seeking processes have been proposed in the literature, many of them having a noticeable similarity with information literacy models. However, during the last fifteen years some information behaviour researchers have become also interested in experiences of opportunistic information discovery that occur naturally in the everyday information activities of many information users. The possibilities for unexpected but important discoveries and insights are especially abundant in the online information environment, which is often a primary resource for students' research projects as well as the information context where they will live and work as information users after finishing their formal education.

Both information literacy (as an applied discipline) and human information behaviour (as a research field) have information seeking behaviour as a shared domain of interest. We believe that there is also a conceptual connection between information literacy models and opportunistic discovery of information, especially the experiences of information encountering ([Erdelez 2005](#erd05)). In the context of student research projects, information encountering refers to situations when students search for information on one topic and come across information related to some other topic of interest. As presented above, one of the goals of information literacy is to prepare new generations of information users to use information effectively in an increasingly electronic information environment. Through exposure to information encountering in information literacy models, students could learn strategies for coordinating their search experience by attending to opportunities provided in the information rich environment.

Based on the above insights, we decided to examine a selection of the most prominent information literacy models for references to information encountering in a research process. The paper first provides background information related to information literacy and opportunistic discovery of information in human information behaviour research, with an emphasis on information encountering. The next section explains the procedures for selection and evaluation of information literacy models, followed by short descriptions of individual models and their evaluation for presence of information encountering. The final section discusses the importance of including information encountering in information literacy models and student instruction and proposes future research on this topic.

## Background

The term _information literacy_ was coined by Paul Zurkowski in 1974 during his presidency at the Information Industry Association ([Eisenberg 2004](#eis04)). Since its then, the topic has garnered widespread attention from many constituencies, such as information professionals, educators, government committees, and the business sector. Since the late 1980s the focus of information literacy has been on the knowledge and skills needed by students in the information environments of the 21st century. This emphasis has emerged in response to the report, _A Nation at Risk_, published by the U.S. Department of Education's National Commission on Excellence in Education in 1983 that presented the declining state of education in the United States (National). By the 1990s _information literacy_ had become a term widely used throughout education on all grade levels.

In 1996 the American Association of School Administrators published a report, Preparing Students for the 21st Century, listing many skills that students need, including those relating to information literacy ([Uchida 1996](#uch96)). Among these skills were: critical thinking, reasoning, and problem-solving skills; skills for using computers and other technologies; ability to conduct research and interpret and apply data; and comprehensive reading and understanding skills. The importance of information literacy was also recognized by the National Education Association, which in its 1996 and 1997 resolutions included the statement that '.equity must be assured as public education works to meet the critical need to prepare all students to become information literate adults and responsible citizens' ([National... 1996](#nat96): 3).

The emergence of information processing models in the early 1980's preceded the educational popularity of information literacy. The models, referred to often as 'research process models,' were developed to help students in elementary and high school settings to acquire research strategies, identify needed information, and structure their time while doing research projects ([Kuhlthau 1994](#kuh94). As the concept of information literacy evolved, these models emerged as tools for assisting students in becoming information literate and in guiding them in an efficient and effective manner through the problem-solving/research process ([Callison 2002](#call02)).

The Final Report of the American Library Association's Presidential Committee on Information Literacy stated that in order to be information literate ". a person must be able to recognize when information is needed and have the ability to locate, evaluate and use effectively the needed information" ([American Library Association 1989](#ame89): 1). This definition is also the basis for the Information Literacy Standards for Student Learning published in 1998 by the American Association of School Librarians and Association for Educational Communications and Technology. This document provides nine standards and twenty-nine indicators that support and expand upon the three broad categories: information literacy, independent learning, and social responsibility. In addition to these standards, various information literacy models became an important part of the strategies established for teachers and library media specialists as they attempted to integrate and address the concept of information literacy within the learning objectives for their students.

In the academic research context, information literacy relates to the study of information seeking and human information behaviour within the field of library and information science. Case ([2002](#cas02)) provides the following definitions of these concepts:

> *   _Information seeking_ is a conscious effort to acquire information in response to a need or gap in your knowledge.
> *   _Information behavior_ encompasses information seeking as well as the totality of other unintentional or passive behaviors (such as glimpsing or encountering information), as well as purposive behaviors that do not involve seeking, such as actively avoiding information ([Case 2002](#cas02): 5).

Wilson ([1999](#wil99)) provided comparative summaries of several models of information behaviour and information seeking behaviour that share many common characteristics with information literacy models. However, while information seeking models are more theoretical and provide a basis for hypotheses for further research, information literacy models (research or process models) are oriented towards practical application in the educational setting.

One model that overlaps both theoretical and practical dimensions is Kuhlthau's Information Search Process Model. This model has been used extensively in both information behaviour research and in information literacy instruction. Wilson ([1999](#wil99)) recognized similarities and differences between Kuhlthau's model and several other models developed by Dervin ([1983](#der83)), Ellis ([1987](#ell87)) and Wilson himself. As Wilson ([1999](#wil99)) points out, the difference between Kuhlthau's model and Ellis's model, is that the former addresses _stages_ of information behaviour while Ellis's model addresses _characteristics_ of information behaviour. The elements of Kuhlthau and Ellis'a models are complementary and create a natural conceptual connection between information process models in information literacy and human information behaviour models.

Another similarity between information process models and information behaviour models has been their focus on purposeful search for information. Over the last two decades, however, many information behaviour researchers have recognized the importance of serendipity and various forms of opportunistic discovery of information in human information activities (see, [Erdelez 1997](#erd97); [Williamson 1998](#wil98); [Toms 2000](#tom00); [Foster and Ford 2002](#fos02); [Foster 2003](#fos03); [Heinström 2006](#hei06); [2008](#bjo08); [McBirnie 2008](#mcb08)). With the proliferation of the Internet and the Web that facilitate easy navigation and movement across various information resources, serendipitous acquisition of information has also attracted research attention in the context of human-computer interaction and the development of information systems, especially digital libraries (see, [Beale 2007](#bea07); [Andre _et al._ 2009](#and09); [Toms and McKay-Peet 2009](#pee09)).

Information encountering is one type of opportunistic discovery of information that is especially relevant to the information research process in an educational context. The concept of information encountering, introduced by Erdelez ([1997](#erd97)) refers to situations where, during the search for information on one topic, users accidentally find information related to some other topic of interest. For example, while searching the online library catalogue for her science paper on global warming, a student notices interesting images of igloos that she could use for her social science poster.

Figure 1 depicts the functional components of an information encountering episode as identified by Erdelez ([2004](#erd04)). This episode is situated within the context of a _foreground_ information search task (e.g., student's search for information on global warming) and is interrupted by noticing information relevant so some other, _background_ task or interest, which is currently not actively pursued (e.g., task to find the images of various types of human dwellings for the social studies poster). The model assumes that information users in the course of their everyday lives have many tasks that will require active information search at some future time.

Figure 1 illustrates a simplified, complete information encountering episode in which the user notices the background information, stops the current search, examines the information for relevancy, captures or saves it for future use and then returns back to the original search task. However, in a natural information search process many different scenarios may play out. For example, a student notices background information but does not interrupt the search to examine it, or the student examines the information but skips the capturing step. The model also illustrates the situation in which the user, upon capturing the encountered background information, returns to the foreground task. In real-life information searching, especially in online information environments, users often do not return to the search task they initially started and switch to the encountered information as their new foreground search topic.

<div align="center">![Figure 1: A model of information encountering](p489fig1.jpg)</div>

<div align="center">  
**Figure 1\. A model of information encountering (adapted from Fisher _et al._ [2005](#fis05): 181)**</div>

## Procedures

To evaluate information literacy models for the presence of information encountering, we selected five established information literacy models that have been used in the U.S. elementary schools and high schools. The following models were selected based on their longevity and popularity in the literature:

*   Kuhlthau's information search process (1985, 1989)
*   Eisenberg and Berkowitz' the Big6<sup>®</sup> (1988, 1990)
*   Stripling and Pitts' research process model (1988)
*   Pappas and Tepe's pathways to knowledge model (1997)
*   Jamie McKenzie's research cycle (2000)

These models were selected for evaluation not only because of their prominence but also because of their mutual differences and unique characteristics. For example, Kuhlthau's model serves as the basis for all other models because of the extensive research that the author has conducted in elementary and high-school settings. Unlike others' models, Kuhlthau's model includes researcher's feelings during the research process. The Big6<sup>TM</sup> is probably the most widely used model in schools in the USA and has an emphasis on student's real-time information problem solving. Stripling and Pitts' model focuses on '_making sense out of information_' through reflection and engagement in critical, high-order thinking throughout the process ([Veltze 2003](#vel03): 19). The Pappas and Tepe model focuses on articulating the complexities and nonlinear nature of the search process. Finally, McKenzie's model was included because it emphasizes the use of technology in the research process.

We examined the above models by consulting both the original sources of their publication and the secondary literature. The first level of evaluation focused on descriptive characteristics of the models, such as: 1) the components of the model and 2) the context of its application. The second level of evaluation addressed the analysis of the models' content for the presence of information encountering, especially in the form of tactics that involve the functional elements of information encountering model (Figure 1) as described by Erdelez ([2005](#erd05)): noticing, stopping, examining, storing and use of encountered information, followed by returning to the original search task. If the model did not provide either an explicit or implicit reference to information encountering, we tried to identify where this behaviour could be accommodated within the model.

The following section summarizes the key structural elements of each model and presents our findings about inclusion of information encountering. For the reader's additional information, references are provided for each model's source.

### Descriptions of information literacy models and inclusion of information encountering

The analysis revealed that none of the five evaluated models include explicit or implicit reference about handling information that students may accidentally encounter in the research process. However, we believe that each of the models can accommodate information encountering and help students become more cognizant of handling such unpredictable opportunities within the research process.

#### Kuhlthau's information search process

The information search process model is based in the philosophy of constructivist learning that '_involves the total person, the feelings as well as the thoughts and actions_' ([Kuhlthau 1994](#kuh94):7) and prepares students to deal with an abundance of available information in a way that has meaning and substance. The intent of the model is to help students understand the process of research, to serve as a guide through a research assignment, and to assist in teaching information skills. Kuhlthau emphasizes that the process was developed in response to a need in an educational setting linked to an assignment or a project. She feels that the '_.process approach gives a new perspective to dealing with students' questions_'. ([Kuhlthau 1994](#kuh94): 7). The model involves seven stages and recognizes the feelings that students experience and mood/attitudes that are important for them to succeed at each stage. These feelings change from anxiety to confidence as students move towards the final stages of the process. The key stages identified in the model are: task initiation, topic selection, prefocus and exploration, focus formulation, information collection, search closure, and start writing ([Kuhlthau 1994](#kuh94)).

An evaluation of Kuhlthau's model revealed that this model provides strategies (e.g., journaling and note-taking) that students may use when gathering information. These strategies are applied during Task initiation, Topic Selection or Information Collection steps of the model, and can be used to make note of information that is accidentally located and that might relate to other problems or areas of students' interest. By expanding journaling and note taking to include information encountering, the information users can capture encountered information and save it for later use. This activity is similar to so called serendipity-cards which, according to Sawaizumi _et al_. ([2007](#saw07)) externalize the serendipitous events from our brains, are 'small enough to be kept in a pocket,' and can be easily used to record a serendipitous event in any situation (Sawaizumi _et al._ [2007](#saw07): 4).

#### The Big6<sup>®</sup> skills

The Big6<sup>®</sup> model was introduced by Eisenberg and Berkowitz ([1998](#eis98)) as a problem-solving process built on Bloom's taxonomy of cognitive objectives. It was promoted as a library and information skills curriculum with an emphasis on critical thinking and information problem solving rather than the more limited, traditional emphasis of location and access. Eisenberg and Berkowitz referred to The Big6<sup>®</sup> as a process '_to teach students the life-long skills needed to be information literate_' ([Eisenberg and Berkowitz 1998](#eis98): 99). The goal of The Big6p<sup>®</sup> was to integrate information skills with curriculum content, focusing on real information needs of students. The model was presented in six steps and specific skills were associated with each step of the process: task definition, information seeking strategies, location and access, use of information, synthesis, and evaluation. Even though the model is presented in a linear six steps, the authors emphasize that process is not necessarily sequential and that the researcher/information seeker often loops back through the previous steps.

The Big6<sup>®</sup> model has potential to include information encountering in the steps of Task definition, where students search for background information, and Location and access, where students search for information related to their task. During both of these steps, the students are exposed to information sources with a variety of content, which is conducive to opportunities for information encountering. In each step students could be advised to be prepared for information encountering experiences and taught about techniques for capturing and managing encountered information. The emphasis of The Big6<sup>®</sup> on the use of technology in a research process makes it even more adaptable to information encountering, since many such accidental findings of information involve the use of the Web. For example, common types of information encountering on the Web are situations when useful information is encountered while scanning the list of items retrieved by a search engine, whereas certain query features have more potential for serendipity ([Beale 2007](#bea07); [_et al._ 2009](#and09); ).

#### The Stripling and Pitts research process model

This model focuses on thinking and reflection during the research process and the need to use and teach thinking skills: '_The thinking frame for research (which serves as a guide for _how_ to think rather than for _what_ to think) is the research process_' ( [Stripling and Pitts 1988](#str98): 19). The model was developed as a guide for research assignment projects planned for students from kindergarten to twelfth grade (elementary and high-school). The emphasis on thinking and reflection helps to avoid shallow and mundane research results that do not reflect what students think about the research. It is a ten-step process and included within each step are teaching and learning strategies with focused study and thinking skills, providing a guide for students to follow when doing research. The steps are: choose a broad topic; get an overview of the topic; narrow the topic; develop a thesis or statement of purpose; formulate questions to guide research; plan for research and production; find, analyse and evaluate sources; evaluate evidence, take notes and compile a bibliography; establish conclusions and organize information into an outline; and create and present a final product ([Stripling and Pitts 1988](#str98)).

The Stripling and Pitts model may be adapted to include information encountering at the second through the sixth level of its research taxonomy, when students are given responsibility for most of the research process. These steps include the following activities: narrow the topic, find and analyse sources, and evaluate evidence and take notes. Similar to Kuhlthau's model and the Big6<sup>®</sup> model, these activities expose students to diverse information sources and also involve information recording techniques that could be easily applied to information encountering.

#### Pathways to knowledge model

Developed by Marjorie L. Pappas and Ann Tepe, in collaboration with the Follett Software Company, this model is an holistic approach to information seeking and the research process with an emphasis on constructivism and inquiry-based learning ([Zimmerman _et al._ 2002](#zim02)). Although similar to other models, this model focuses on the nonlinear, recursive nature of the process and the supporting literature emphasizes the importance of teaching students that the process is nonlinear ([Pappas and Tepe 2002](#pap02)). Zimmerman and her colleagues ([2002](#zim02)) also pointed out that the real emphasis of the model is to help students think about the process. The basic pathways of the model are articulated by the following stages: appreciation, pre-search, search, interpretation, communication, and evaluation ([Pappas and Tepe 2002](#pap02)). During appreciation, the phase unique for this model, students appreciate, listen, sense, view, and enjoy information in the world around them which stimulates their imagination, curiosity and motivation to learn more about a certain topic. The stages of appreciation and evaluation occur throughout the process. The information need and the learning style of the information seeker will impact upon the strategies and pathways used to find and use information, making each undertaking unique ([Zimmerman _et al._ 2002](#zim02)).

Among the models we evaluated, the pathways to knowledge model comes closest to capturing the environment that facilitates information encountering. Within this model information encountering best fits in the appreciation stage, which includes students' recognition of the world through information received in varied and different formats, such as natural settings, the Web, books, video, music, paintings, and which can occur throughout the information seeking process ([Pappas and Majorie 2002](#pam02)). This phase is meant to '_foster curiosity and imagination_' and encourage the information seeker to '_explore any relationships between topic and other, related ideas or concepts_' ([McKenzie 2000](#mck00): 4). A high level of curiosity and imagination during the appreciation stage may be the prologue to a discovery phase in information seeking activity; therefore, students could become aware of accidental findings of information and should be guided to use strategies learned in relationship to this stage to make note of such occurrences for later use.

#### Research Cycle

Mckenzie's research cycle is a model that blends information literacy with the use of technology to assist students in meaningful information seeking and use, taking them beyond fact-finding ([Milam 2002](#mil02)). At the heart of the research cycle are questioning skills that guide students to a more demanding level of constructing their own learning. The model includes six steps: list subsidiary questions; develop research plan; gather information; sort and sift; synthesize, and evaluate. Students repeat these six steps many times as they research a topic, until they are prepared to complete the seventh step, the reporting phase ([McKenzie 2000](#mck00)). The focus of the cycle is to engage students in thought provoking research that moves beyond topical to meaningful problem-solving and decision-making research ([McKenzie 2000](#mck00)). The questions serve to guide the research, while the action of cycling back through the process forces students to take and in-depth look at their subject and make discoveries and form opinions, creating their own learning (McKenzie [2000](#mck00)). McKenzie emphasizes that technology makes '_word-moving_' even easier, students can just cut and paste, allowing them to gather facts without real thought ([McKenzie 2000](#mck00): 5). The question-based research thus becomes essential to force students to investigate and think about their subject of interest, rather than just report readily available, predetermined facts.

McKenzie's research cycle could include information encountering within the model, especially since students recycle through the phases. The phases that would especially support information encountering are planning, gathering, and sorting and Sifting. Information encountering could be addressed in these phases to help students understand such occurrences and be more cognizant of handling such unpredictable opportunities within the research cycle. McKenzie's model provides strategies that students use when gathering information in terms of storage and retrieval that could easily be used to make note of information related to other problems or areas of their interest that they accidentally find, and thus enabling them to return to this information later.

Table 1 provides a summary overview of all the models that were analysed. In each model, specific stages or phases are identified with anasteriskx where teachers and library media specialists could articulate for students the natural occurrences of information encountering and suggest to them efficient strategies for managing encountered information.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1\. Summary of information literacy models**</caption>

<tbody>

<tr>

<th width="33%">Information literacy model</th>

<th width="33%">Model components</th>

<th width="33%">Context</th>

</tr>

<tr>

<td>Kuhlthau's information search process (1984, 1989)</td>

<td>Task initiation*  
Topic selection*  
Prefocus and exploration  
Focus formulation  
Information collection*  
Search closure  
Starting writing</td>

<td>A research process model with emphasis on the creative learning process (elementary and high-school)</td>

</tr>

<tr>

<td>Eisenberg and Berkowitz's The Big6<sup>®</sup> (1988,1990)</td>

<td>Task definition*  
Information seeking strategies  
Location and access*  
Use of information  
Synthesis  
Evaluation</td>

<td>A problem solving and information seeking model (elementary and high-school).</td>

</tr>

<tr>

<td>Stripling's and Pitt's research process model (1988)</td>

<td>Choose a broad topic  
Get an overview of the topic  
Narrow the topic*  
Develop a thesis or statement of purpose  
Formulate questions to guide research  
Plan for research and production  
Find, analyse and evaluate sources*  
Evaluate evidence, take notes and compile a bibliography*  
Establish conclusions and organize information into an outline  
Create and present final product</td>

<td>A research model for K-12 with emphasis on thinking and reflection throughout the process. Dependent on aligning research with various levels of thought from simple to complex, e.g., fact-finding, asking/searching, examining/deliberating, integrating/concluding and conceptualizing.</td>

</tr>

<tr>

<td>Pappas's and Teppe's pathways to knowledge model (1995)</td>

<td>Appreciation  
Pre-search*  
Search*  
Interpretation  
Communication  
Evaluation  
</td>

<td>A model that articulates complexities & nonlinear nature of the information seeking process K-12\. (Appreciation through communication may occur throughout the process)</td>

</tr>

<tr>

<td>Jamie McKenzie's research cycle (2000)</td>

<td>List subsidiary questions  
Develop research plan*  
Gather information*  
Sort and sift*  
Synthesize  
Evaluate  
**(Cycle through above phases, possibly several times)** - and  
Report</td>

<td>A research model that emphasizes technology use and information literacy skills in elementary and high-school education.</td>

</tr>

<tr>

<td colspan="3">* marks phases in the model where information encountering may be incorporated</td>

</tr>

</tbody>

</table>

## Discussion

The major purpose of information literacy models is to help students successfully complete the research process for specific assignments within a designated time-line. The models imply that students will devote their research time to purposeful use of information sources, such as print and online resources. Most of students' school-related information seeking will, therefore, occur in the context of assignments and time-lines that do not promote pursuit of serendipitous information. These task-driven characteristics of the learning environment may inhibit the students' freedom to experience information encountering.

The current trends in education that promote standardization and elicit extensive control over curricula, teacher behaviour and student learning ([McNeil 2000](#mcn00)) further stress the importance of students' staying on task. In order to meet the requirements of the standardized curriculum, the teachers will likely impose strict classroom management and supervise their students' behaviour in relation to achieving the primary instructional objectives. Under these conditions, students' experiences of information encountering may be seen as counterproductive and disruptive of planned classroom activities.

In the elementary and high school setting, the research process is traditionally presented in a goal-oriented, precise and often very step-wise fashion ([George 2005](#geo05)) the better to help students recognize the individual activities that can be linked to specific learned strategies. Over time, however, changes in the level of students' sophistication emerge as they become more comfortable with the research process and capable of dealing with the overall information gathering activities it involves. The research process then becomes more intuitive and its phases or stages more simultaneous. This change is reflected in the research taxonomy levels of Stripling and Pitts and is also addressed by Foster ([2003](#fos03)) who observed that experienced academic and postgraduate researchers are capable of interdisciplinary information-seeking. During this more natural process of research students are likely to experience situations of opportunistic discovery of information and information encountering. By incorporating information encountering into the current information literacy models, teachers could promote a more holistic view of information literacy that would broaden students' awareness of naturalistic research processes.

Through the investigation of five information literacy models, we identified stages or phases and strategies that could lend themselves to the inclusion and recognition of information encountering. As presented in Table 1, we believe that information encountering could be incorporated into information literacy models without extensive alteration of the current processes. The elements of the models particularly well suited for 'information encountering enhancement' are the stages of the research process that involve information location and access (e.g., task definition, topic selection, information collection and seeking, narrowing of the topic, searching, gathering, sifting, and sorting). This enhancement of information literacy models would raise the information encountering awareness by teachers, school librarians and computer technology instructors who typically provide information literacy instruction and rely on the models of information literacy as a major instructional tool.

Through information-encountering-enhanced information literacy models students will become more aware of their own experiences with information encountering and will consequently need to learn how to address these unexpected situations with effective information management strategies. For example, students could be encouraged to make a written note of the information they have encountered and its location ([Sawaizumi _et al._ 2007](#mck07)); they could print out textual information or store it in their personal folders on the school network; they could bookmark or e-mail to themselves the URL for a relevant website; they could make a photocopy of a page with relevant information, etc. These strategies would be especially beneficial when students are learning about searching on the Internet and when using online databases and other library resources that result in instances of information encountering. The state of technology infrastructure in schools is, therefore, an important consideration when assessing the attention given to information encountering in educational settings. Lack of infrastructure supporting technology-ease in terms of access to servers for storing information, email accounts for students, high speed Internet access and provision for online database access creates barriers for rewarding students' information encountering experiences in an electronic environment. However, as the state of school technology becomes more established and reliable, it will support the integration of the concept of information encountering into the information literacy agenda. Ultimately, the integration of information encountering strategies within information literacy in electronic and other types of information environments can extend beyond school-related activities to life-long learning, which is an ongoing process useful for personal, professional, or educational information needs.

## Conclusion

Our investigation of information literacy models revealed that they do not address the concept of information encountering. One contributing factor is that the models were developed before information encountering and opportunistic discovery of information emerged as recognized concepts in the study of human information behaviour. Also, these models were developed to support purposeful information seeking, usually related to an assignment that is content-based within the educational curriculum.

Based on the analysis of five information literacy models, we believe that within each model are various stages or phases where natural occurrences of information encountering are possible and should be articulated for students. Acknowledging the need for teaching information encountering strategies in educational settings lends support to the philosophy behind the information literacy models that encourages students' experiences with multiple research strategies and promotes alignment of these strategies with, what McBirnie ([2008](#mcb08)) calls learners' self-taught search skills.

Based on the research-based understanding of opportunistic discovery of information as a concept in human information behaviour we believe that information encountering provides another dimension of information literacy that could be incorporated into the information literacy models. However, educators (e.g., classroom teachers, technology specialists and library media specialists) would need to have clear understanding of expected benefits and potential drawbacks of making adjustments to present instructional strategies for information encountering to become part of information literacy instruction. Therefore, more empirical research is needed to address questions such as:

*   How does information literacy instruction without students' introduction to information encountering compare to instruction with information encountering in terms of measurable student learning outcomes?
*   What is the impact of inclusion of information encountering in information literacy instruction on the various aspects of teachers' classroom management?
*   Does maturity of the student make a difference in their ability to address information encountering experiences without thwarting the demands of the immediate assignment or task?
*   What strategies should be introduced to teach students how to efficiently manage information encountering in various stages of research process models?

Additional research will strengthen further connections between information behaviour research and information literacy instruction and will also provide a more complete understanding of the potential role information encountering in this instruction. Based on the research findings, specific instructional strategies can be developed to enhance current information literacy models, contributing to emergence of more holistic information literacy.

## About the authors

**Sanda Erdelez** is an Associate Professor at the School of Information Science and Learning Technologies, at University of Missouri, USA and an Associate Director at Missouri Informatics Institute. She is the founding director of the Information Experience Laboratory at University of Missouri and co-editor of _Theories of Information Behavior_ (2005) published by Information Today. Her research interest is human information behaviour, usability of information systems and opportunistic discovery of information. Dr. Erdelez can be reached at: [erdelezs@missouri.edu](mailto:erdelezs@missouri.edu)  
**Josipa Basic** is a doctoral student at the School of Information Science and Learning Technologies at University of Missouri, USA. She works as a Research Assistant at the Information Experience Laboratory at the same University. She received both Bachelor's and Master's degree in Library and Information Science from the University of Zadar, Croatia. She can be contacted at: [jbyv7@mail.missouri.edu](mailto:jbyv7@mail.missouri.edu)  
**Deborah Levitov** is a Managing Editor for _School Library Monthly_, a magazine that supports the school librarians in elementary and secondary schools in their efforts to strengthen their students' information literacy skills, inquiry and the research process. She received her Ph.D. from University of Missouri and can be reached at [dlevitov@abc-clio.com](mailto:dlevitov@abc-clio.com).

#### References

*   André, P., Teevan, J. & Dumais, S.T. (2009). From x-rays to silly putty via Uranus: serendipity and its role in web search. _Proceedings of the 27th International Conference on Human factors in Computing Systems_, pp. 2033-2036.
*   American Library Association _and the_ Association for Educational Communications and Technology. (1998). Information power: building partnerships for learning. Chicago, IL: American Library Association.
*   American Library Association. _Presidential Committee on Information Literacy_, (1989). _Final report._ Chicago, IL: American Library Association.
*   Beale, R. (2007). Supporting serendipity: using ambient intelligence to augment user exploration for data mining and web browsing. _International Journal of Human-Computer Studies_, **65**(5), 421-433.
*   Björneborn, L. (2008). [Serendipity dimensions and users' information behaviour in the physical library interface.](http://www.webcitation.org/612MPhD2O) _Information Research_, **13**(4). Retrieved August 5, 2011, from http://informationr.net/ir/13-4/paper370.html. (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/612MPhD2O)
*   Callison, D. (2002). Models (Part I). _New Review of Information Behaviour Research_, **19**(1), 34-37.
*   Case, D.O. (2002). _Looking for information: a survey of research on information seeking, needs, and behavior_. San Diego, CA: Academic Press.
*   Chung, J.S. & Neuman, D. (2007). High school student's information seeking and use for class project. _Journal of American society for Information Science and Technology_, **58**(10), 1503-1517.
*   Dervin, B. (1983, May). _An overview of Sense-Making research: concepts, methods, and results to date._ Paper presented at the Annual Meeting of the International Communication Association, Dallas, TX.
*   Eisenberg, M.B. & Berkowitz, R.E. (1990). _Information problem-solving: the Big Six skills approach to library & information skills instruction._ Norwood, NJ: Ablex.
*   Eisenberg, M.B. & Berkowitz, R.E. (1988). _Curriculum initiative: an agenda and strategy for library media programs_. Norwood, NJ: Ablex.
*   Eisenberg, M.B., Lowe, C.A. & Spitzer, K.L. (2004). _Information literacy: essential skills for the information age_. Westport, CT: Libraries Unlimited.
*   Ellis, D. (1987). _The derivation of a behavioural model for information retrieval system design_ . Unpublished doctoral dissertation, University of Sheffield, Sheffield, UK.
*   Erdelez, S. (2004). Investigation of an opportunistic acquisition of information in the controlled research environment. _Information Processing & Management_, **40**(6), 1013-1025.
*   Erdelez, S. (2000). Towards understanding information encountering on the Web. _Proceedings of the Annual Meeting of the American Society for Information Science_, **47**, 363-371.
*   Erdelez, S. (1997). Information encountering: a conceptual framework for accidental information discovery. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), Information Seeking in Context. _Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts_, (pp. 412-421). London: Taylor Graham.
*   Foster, A. (2003). A nonlinear model of information-seeking behavior. _Journal of the American Society for Information Science and Technology_, **55**(3), 228-237.
*   Foster, A.E. & Ford, N.J. (2002). Serendipity and information seeking: an empirical study. _Journal of Documentation_, **59**(3), 321-340.
*   Heinström, J. (2006). Psychological factors behind incidental information acquisition. _Library & Information Science Research_, **28**(4), 579-594.
*   Kuhlthau, C.C. (1994). _Teaching the library research process_, 2nd Edition. London: Scarecrow Press.
*   Kuhlthau, C.C. (1989). Information search process: a summary of research and implications for school library media programs. _School Library Media Quarterly_, **18**(1), 19-25.
*   Kuhlthau, C.C. (1985). A process approach to library skills instruction. _School Library Media Quarterly_, **4**(1), 31-44
*   McBirnie, A. (2008). Seeking serendipity: the paradox of control. _ASLIB Proceedings_, **60**(6), 600-618.
*   McKenzie, J. (2000). _Beyond technology: questioning, research and the information literate school._ Bellingham, WA: FNO Press.
*   McNeil, L.M. (2000). Cover story: creating new inequalities. _Phi Delta Kappa_, **81**(10), 728-736.
*   Milam, P. (2002). Moving beyond technology with strategic teaching: Jamie McKenzie's research cycle. _School Library Media Activities Monthly_, **19**(4), 22-23.
*   National Education Association. (1996). _The 1996-97 Resolutions of the National Education Association._ Washington, DC: National Education Association.
*   Nutefall, J.E. & Ryder, P.M. (2010). The serendipitous research process. _Journal of Academic Librarianship_, **36**(3), 228-234.
*   Pappas, M.L. & Tepe, A.E. (2002). _Pathways to knowledge and inquiry learning_. Greenwood Village, CO: Libraries Unlimited.
*   Pappas, M.L. & Tepe, A.E. (1997). _Pathways to knowledge: Follett's information skills model_ (3rd ed.). McHenry, IL: Follett Software.
*   Sawaizumi, S., Katai, O., Kawakami, H. & Shoise, T. (2007). _Using the concept of serendipity in education_. Paper presented at the KICSS 2007: The Second International Conference on Knowledge, Information and Creativity Support Systems, Nomi, Ishikawa, Japan.
*   Stripling, B.K. & Pitts, J.M. (1988). _Brainstorms and blueprints: teaching library research as a thinking process._ Englewood, CO: Libraries Unlimited.
*   Toms, E.G. (2000). Understanding and facilitating the browsing of electronic text. _International Journal of Human Computer Studies_, **52**(3), 423-452.
*   Toms , E.G. & McCay-Peet, L. (2009). Chance encounters in the digital library. In M. Agosti, J. Borbinha, S. Kapidakish, C. Papatheodorou & G. Tsakonas (Eds.), _ECDL'09\. Proceedings of the 13th European Conference on Research and Advanced Technology for Digital Libraries, Corfu, Greece_, (pp. 192-202). Berlin: Springer Verlag. (Lecture notes in computer science 5714)
*   Uchida, D. _et al._ (1996). _Preparing students for the 21st century._ Arlington, VA: American Association of School Administrators.
*   Veltze, L. (2003). The Pitts/Stripling model of information literacy. _School Library Media Activities Monthly_, **19**(8), 17-21.
*   Williamson, K. (1998). Discovered by chance: the role of incidental information acquisition in an ecological model of information use. _Library & Information Science Research_, **20**(1), 23-40.
*   Wilson, T.D. (2000). [Human information behavior](http://www.webcitation.org/612NUeUdv). _Informing Science_, **3**(2), 49-55\. Retrieved 18 August, 2011 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/612NUeUdv)
*   Wilson, T.D. (1999). [Models in information behaviour research](http://www.webcitation.org/612NdQyEI). _Journal of Documentation_, **5**(3), 249-269\. Retrieved 18 August, 2011 from http://informationr.net/tdw/publ/papers/1999JDoc.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/612NdQyEI)
*   Zimmerman, N.P., Pappas, M.L. & Tepe, A.E. (2002). Pappas and Tepe's pathways to knowledge model. _School Library Media Activities Monthly_, **19**(3), 24-27.