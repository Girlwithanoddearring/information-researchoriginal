#### vol. 16 no. 3, September, 2011

# A user-centred mobile diary study approach to understanding serendipity in information research

[Xu Sun](#authors) and [Sarah Sharples](#authors)  
Human Factors Research Group Department of Mechanical, Materials and Manufacturing Engineering Faculty of Engineering University of Nottingham Nottingham, U.K.  
[Stephann Makri](#authors)  
Interaction Centre, Division of Psychology & Language Sciences and Department of Computer Science University College London Gower Street London U.K.

#### Abstract

> **Introduction.** While serendipity is gaining increasing attention in the context of information research these years, there is a lack of empirical evidence to demonstrate the nature of serendipity in literature.  
> **Method.** We conducted a diary study with eleven participations to understand serendipity in information research. A mobile diary application was developed which allows participants to rapidly capture how serendipity happens in their daily life and the context in which they experience serendipity for one week. Their diary entries were discussed during post-study interviews.  
> **Analysis.** An Emergent Themes Analysis was conducted to understand our data.  
> **Results.** We identified: 1) some key elements to support understanding of serendipity, 2) the influential role of context in serendipitous experiences, 3) a framework of understanding how serendipity happens and 4) the positive impacts of serendipity in people's information research.  
> **Conclusions.** Our research suggests that a framework for classifying serendipity should consider aspects associated with the activity, the value of the information, the source of the information and the interaction between the individual and the context.

## Introduction

> I was doing research on art work and I had this idea I am going to develop. I accidentally found another artist who has a very similar idea via the Internet. It is about collecting people's favourite songs. When I was reading about it, I realized that actually I had a book by this artist next to me. So that was the moment where all things came together, and I actually realized that I knew this artist, and I had a book by this artist next to me! (Participant, PhD student in Digital art, female)

Memorable experiences of unexpectedly encountering something fortunate have often been captured as examples of serendipity from researchers' daily lives and work. Literature suggests that not only is serendipity widely experienced among researchers, but that it can also contribute to the generation of new knowledge ([Foster and Ford 2003](#fos03)).

There is much of interest in this subject from different disciplines. Previous studies suggest that serendipity plays a positive role in discovery and creativity in science (e.g., [Roberts 1989](#rob89)), arts and humanities (e.g., [Delgadillo and Lynch 1999](#del99)), social sciences (e.g., [Fine and Deegan 1996](#fin96)) and information seeking (e.g., [Foster and Ford 2003](#fos03)). Although it has received an increasing amount of attention over recent years, serendipity in the context of information research has not been examined often ([Toms 1998](#tom98)).

There are no cohesive theories about serendipity, as it is associated with a wide range of meanings. For example, Case defines serendipity as '_the action of, or aptitude for, encountering relevant information by accident_' ([Case 2007](#cas07): 337). Cunha refers to it as '_the accidental discovery of something that, post hoc, turns out to be valuable_' ([Cunha 2005](#cun05): 320) and Cooksey suggests that serendipity is '_the happy convergence of the mind with conditions_' ([Cooksey 2004](#coo04): 25).

There is little consensus on theoretical frameworks that link serendipity to end users' understanding, a lack of empirical evidence to demonstrate the nature of serendipity ([McBirnie 2008](#mcb08)), and little direct support for information researchers and designers of serendipitous information systems for end users.

This paper presents an empirical study undertaken with a mobile diary application developed to capture serendipitous experiences in people's real lives and work. The overall aim of the research to which this study contributes is to apply a user-centred approach to understand and influence serendipity in an information research context. The specific objectives of the study presented here were to:

1.  investigate peoples' perceptions of serendipity in an information research context;
2.  collect examples of how serendipity happens in the context of information research;
3.  investigate the impact of serendipity in an information research context;
4.  develop a framework to represent the role of serendipity in people experiencing serendipity

This paper makes both practical and methodological contributions. These should help researchers and practitioners who seek to (1) understand the nature of serendipity in information research, (2) design information systems that aim to encourage potential opportunities for serendipity and (3) to decide on methodological approaches for capturing serendipitous information discoveries.

## Theoretical Background

### Definition of serendipity

The term 'serendipity' was coined and defined by Horace Walpole in 1754, after a fairy tale in which the main characters were always making discoveries by '_accident and sagacity_' ([Merton and Barber 2004](#mer04): 2). Walpole highlights that '_no discovery of a thing you are looking for comes under this description_' ([Merton and Barber 2004](#mer04): 2). Most existing definitions of the serendipity in information research reflect its accidental and/or sagacity characteristics. For example, Cunha refers to it as '_the accidental discovery of something that, post hoc, turns out to be valuable_' ([Cunha 2005](#cun05): 320) and Cooksey ([2004](#coo04)) as '_the happy convergence of the mind with conditions_'. The definitions of Cunha and Cooksey differ from Walpole's original, which does not suggest that serendipity is always positive (i.e., 'happy,' or 'fortunate'). Moreover, the definition of Cunha ([2005](#cun05)) features chance and positivity but not mental effort; and Cooksey's ([2004](#coo04)) includes positivity and mental efforts, but not chance.

Foster defines serendipity as '_a method for achieving breadth and identifying information or sources from unknown or partially unknown directions_' ([2006](#fos06): 157) and Case defines it as '_the action of, or aptitude for, encountering relevant information by accident_' ([2007](#cas07): 337). McBirnie suggests that the word _method_ in Foster's definition suggests a purposeful approach, whilst _aptitude_ in Case's definition suggests a skill or ability, thereby incorporating the notion of control into the definition. McBirnie argues, however, that since the relationship between control and serendipity is unclear, '_great care must be taken over the inclusion of the concept of control, either intentionally or unintentionally in the definition of serendipity_' ([McBirnie 2008](#mcb08): 604).

André _et al_. summarizes a number of definitions of serendipity that have been explored by researchers and argue that,

> rather than one understanding of serendipity, we have seen a kind of continuum of understandings from inadvertently finding something of personal interest, to the critical breakthrough of a domain expert making a key 'sagacious' insight between a perceived phenomena and an opportunity for a new invention ([Andre _et al_ 2009](#and09): 313).

Or as mentioned by Danzico, '_falling somewhere between accidental and sagacity, serendipity is synonymous with neither one nor the other_' (Danzico [2010](#dan10): 16). There is currently a lack of consensus regarding existing definition of serendipity, and a lack of empirical evidence to demonstrate people's perceptions of serendipity.

### Nature of serendipity

Most previous research has aimed to develop an understanding of serendipity from a theoretical perspective. Austin ([2003](#aus03)) discusses four types of opportunistic discovery of information, which are blind luck (chance that comes with no effort), happy accidents (chance which is due to exposure to seemingly unconnected facts and experiences), _prepared mind_ (chance is perceived due to exposure to many facts related to the problem at hand) and _individual_ (chance favours a particular individual as a result of the person's distinctive knowledge or interest). Similarly, Liestman ([1992](lie92)) proposes six approaches to serendipity in information research, including:

*   coincidence: chance encounters as a result of random luck;
*   _prevenient grace_: chance occurs because of efforts performed by those who are unseen and unknown;
*   synchronicity: serendipity is a result of the concurrence of two meaningful but not causally connected events;
*   perseverance: chance is encouraged by looking hard for information;
*   _altamirage_: serendipity happens as a result of the distinguished habit, expertise or character of an individual;
*   sagacity: serendipity is due to a '_random juxtaposition of ideas, in which loose pieces of information frequently undergo a period of incubation in the mind and are brought together by the demands of some external events_'.

The first three approaches give no credit to the individual, while the last three propose some effort on the part of the individual to encounter serendipity. The theoretical frameworks of Austin ([2003](#aus03)) and Liestman ([1992](#lie92)) are conceptual in nature, drawing on researchers' perceptions of serendipity.

Some empirical studies have also examined the nature of serendipity. Foster and Ford ([2003](#fos03)) studied serendipity with academic and postgraduate researchers in 45 interviews. As a result of their findings, they classified serendipitous encounters as either unexpectedly finding information where 1) information was of unexpected value or 2) the existence and/or location of the information was unexpected. The unexpected information then had the potential to either reinforce or strengthen the researchers' existing problem conception or to take them in a new direction in which the problem conception is reconfigured in some way.

Erdelez ([1997](#erd97) [1999](#erd99)) examined what she refers to as _information encountering_, which she defines as '_an instance of accidental discovery during an active search for some other information_' ([Erdelez 2005](#erd05): 180). She identified four different types of information encounterers: non-encounterers (who seldom experience encountering information), occasional encounterers (who sometimes encounter information but do not see this as more than luck), encounterers (who often come across information but do not see a connection between information encountering and other aspects of their information behaviour) and super-encounterers (who rely on bumping into information as a method for information acquisition and often encounter information related to them). Erdelez ([2004](#erd04); [2005](#erd05)) later suggests a conceptual model of information encountering based on her empirical data. The model supposes that a user is actively looking for information related to a particular foreground problem and encounters information related to a background problem. She explains that the _problem_ translates into information needs and then may lead to the acquisition of information.

McBirnie ([2008](#mcb08)) undertook some empirical studies with jazz musicians and academics in order to study serendipity. She found serendipity was widely experienced as both passive and active, that participants regularly linked serendipity with finding useful information to address a specific information need and that they regarded the encounter of information as highly positive.

These studies identify the role of user goals, attitudes and activities in likelihood of occurrence and perception of value of serendipity. The unexpected or opportunistic nature of serendipity is also highlighted. It has been pointed out that serendipity has not been the subject of extensive empirical research, although there is increasing interest in studying human information behaviour ([McBirnie 2008](#mcb08)). We need further empirical work in order better understand the nature of serendipity which, in turn, should allow us to design interactive systems that are more effective in creating opportunities for serendipity to occur. The overall aim of the study presented here is to take a user-centred approach to tackle the question of what serendipity is, and the nature and context in which serendipity takes place.

## Research methods and activities

### Methods

Andre _et al_ ([2009a](#and09a)) highlight that serendipity is inherently rare, so it is difficult for researchers to capture it for study. Both Erdelez ([2004](#erd04)) and Toms ([2000](#tom00)) have exploited controlled experiments to study serendipity, and they suggest that future studies of the serendipitous acquisition of information should consider the context in which a serendipitous encounter is activated. In this study, we have applied a situated diary study, to capture and study people's serendipitous experiences in a naturalistic context.

Diary study is a self-reporting instrument and has been used by many studies to examine ongoing experience, offering the opportunity to investigate the social, psychological and even the physiological processes within everyday situations ([Bolger _et al_. 2003](#bol03)). Diary studies differ from other social science methods by reducing the time between an event's occurrence and recording the information; hence, it is less subject to memory lapses, as may be the case with retrospective interviews. In addition, the diary method may help to bridge the gap between the naturalistic study methods (e.g., observations in the field) and the laboratory study methods (i.e., studying entities in a controlled laboratory) by providing objective data about people's real context in the field which may guide further study in the laboratory ([Rieman 1993](#rie93)). Participants take the initiative in a diary study to capture information, which allows real time data collection, thereby enhancing the reliability and completeness of the data.

There are generally two types of diary studies: elicitation studies and feedback studies ([Carter and Mankoff 2005](#car05)). Feedback studies require participants to answer the predefined questions of researchers, while in elicitation studies participants capture information as prompts for discussion in interviews with researchers. The difference between feedback studies and elicitation studies is that feedback studies place the emphasis on asynchronous communication between researchers and participants, while elicitation studies involve synchronous communication between researchers and participants. Another difference is that in feedback studies, participants should normally provide information of interest immediately after they perceive it, while in elicitation studies participants only capture some aspects of an experience when it occurs but then provide more information about it later during an interview. In some studies these two types are combined, with results from feedback serving as a memory cue during the elicitation study. This combined approach is adopted in the case presented here.

For the diary method to work successfully, the data entry needs to be made as simple as possible. To obtain reliable and valid data, diary studies must achieve a level of participant commitment and dedication ([Bolger _et al_. 2003](#bol03)). Paper diary studies have been used effectively for the collection of naturalistic data in information research; however, an ecologically valid study of today's information behaviour is challenging because the conditions of obtaining information are dynamic, varied, and difficult for people to observe and record.

Various constraints to the traditional paper-based diary method are summarised from existing literature, as follows:

1.  participants must stop their activity and manually record it; this is not as troublesome when users work at a desk and the activities of interest occur there, but it becomes problematic when participants are mobile;
2.  it can take a considerable amount of time for respondents to fill in paper-based diary sheets, and the burden on respondents is considerable if they are to record all of their activities on paper;
3.  it requires strong commitment from the participants during the test period. Participants must be motivated to make a considerable effort to record their activities over the time-span of the diary;
4.  it is easy for a user to neglect to complete the diary. If the users do not complete their diary immediately after experiencing something, they will not remember the situation correctly and many items of important information could be forgotten;
5.  it takes a considerable amount of time for researchers to input collected data.

In an attempt to address these issues, some researchers have applied existing technologies to develop new diary methods (e.g., [Ohmori _et al_. 2005](#ohm05); [Palen and Salzman 2002](#pal02); [Leong _et al_. 2010](#leo10)).

Mobile technology has created opportunities for capturing everyday activity in new ways. The mobile diary tool allows people to capture serendipitous experiences as they happen. This is particularly important as it is difficult, if not impossible to predict when serendipity will occur. Making a record of experiences as they happen is important as it is often difficult to recall/recount these experiences after some time has passed, unless they are especially memorable. For this study we chose mobile phone-based elicitation studies which allowed participants to rapidly capture information about the event that would serve as a memory cue during a later interview.

A mobile diary application was developed as a result of the need to quickly and conveniently understand how people experience serendipity across a variety of work and everyday life situations. The application was developed on the Android platform using the Java programming language. Applications developed on Android can run on any mobile phone that is Android compatible without (or with minor) modification. The application has three main functions: 1) a reminder to fill in the diary: users can customize the time so that they are reminded to use the diary during a regular period of downtime (such as when travelling home from work). They can also disable the reminder whenever they want to. 2) a write diary function: participants can enter notes, take photographs, make annotations on the photographs, and record voice memos or videos to capture serendipitous experiences on the move. 3) a view diary function: participants can access their own diaries and comment on them. This diary also has the benefit of serving as a reflective tool which we used during interviews with our participants to remind them of their serendipitous experiences and to elicit further details about their experiences.

<div align="center">![Figure 1: Snapshot of the application - user can enter text notes, record voice memos, take photograph or video.](p492fig1.jpg)</div>

<div align="center">**Figure 1: Snapshot of the application - user can enter text notes, record voice memos, take photograph or video.**</div>

### Participants

The study applied a combination of stratified and opportunistic sampling where PhD students were contacted by e-mail and effort was made to represent a range of backgrounds and interests amongst the sample. We chose to contact PhD students who were involved in at least a year of individual research. This is because previous studies had suggested that serendipity is widely experienced among the researchers ([Foster and Ford 2003](#fos03)). Eleven doctoral research students (five male and six female) were recruited from the Horizon Doctoral Training Centre at the University of Nottingham. These participants conducted research on a daily basis and use computers intensively at work. They were from a range of academic backgrounds, including human-computer interaction, human factors, political and international relations, geospatial sciences, english, telecommunications, computer science, art, management in information technology, and psychology. All participant names reported in the results are aliases.

All participants reported in an initial briefing session that they had previously thought about the concept of serendipity and that they were enthusiastic about taking part in the study. The frequency of their serendipitous encounters (which they reported informally) varied from '_a couple of times a week_' to '_once every couple of months_', consistent with Erdelez ([1999](#erd99)) and McBirnie ([2008](#mcb08)) who found that serendipitous information experiences were more common for some participants than for others.

### Procedure

A pilot study was used to check the timings, refine the data collection methods, and resolve any usability problems with the data collection tools. This was carried out by two researchers at the University of Nottingham.

Participants were first introduced to the purpose of the study and had a brief discussion with researchers about their views on the concept of serendipity. Each participant was then given an Android phone, with the diary application, and they were given guidance on how to use the mobile diary application. Participants were required to record their serendipitous experiences using the application for one week. For the purpose of this study, participants were encouraged to be as broad as possible in their interpretation of examples of serendipity when selecting experiences to record. One week after the briefing session, the participants were interviewed for approximately one hour each. The interview was qualitative in nature and semi-structure and revolved around the diary entries captured by the participants and their understanding of serendipity. The diaries were used as illustrations and as memory cues in the interviews. For each diary, participants were asked a number of different questions to establish: 1) how their serendipitous experiences came about; 2) the contextual factors that played a role in their serendipitous experiences and 3) the impact of serendipity on their research.

### Data collection

Two types of data were collected as part of the study; participants' diary entries and post-study interview data. Participants' diary entries were saved automatically on the Android phones, and the entries produced by participants included text, audio, video and image files (see Figure 2). We obtained 23 diary entries. The diary entries were transcribed on paper, including photographs or videos which were scanned and placed in context within the transcript of the diaries. The entries were used as prompts for fuller reflection during the post study interviews.

The interviews took place one week after the diary study. Interviews lasted for approximately one hour for each of the participants. The interviews were taped and transcribed and they were supplemented by notes made by the interviewer.

<div align="center">![Figure 2: Examples of mobile diary data collected (text and image files).](p492fig2.jpg)</div>

<div align="center">**Figure 2: Examples of mobile diary data collected (text and image files).**</div>

### Analysis

Both diary entries and interview response data are qualitative in nature. An Emergent Themes Analysis (Wong and Blandford [2002](#won02)) was conducted to understand the data. First, participants' transcribed data was viewed several times, and then open coding was conducted to examine the data for various points of interest. The analysis was guided by our research objectives (presented in the previous section), but also involved remaining open to new concepts that emerged from our data. During the analysis, the qualitative data analysis software, Nvivo 9, was used to manage the process of coding and linking concepts, and to enable the effective searching of quotations.

## Results

The themes that emerged during the analysis were first broadly grouped as:

*   participants' perception of serendipity;
*   the nature of a serendipitous experience;
*   the role of contextual factors in serendipitous experience

Each of these themes is examined in turn below and used to inform a model depicting the nature of serendipity in an information research context.

### Participants' perceptions of serendipity

Our participants had different perceptions of serendipity. Some participants perceived serendipity as a '_fortunate accident_', a '_happy accident_' or a '_lucky coincidence_', as follows:

> I think (serendipity is) sort of, in a broad sense, a kind of lucky coincidence. (P6)  
> My kind of understanding (of serendipity) is that it is a type of happy accident where you might come across something just by chance or good fortune - things like that. (P8)

Some considered serendipity as the unexpected finding of information that brings about a positive impact, such as:

> It (serendipity) is something that happens while you are doing something and you find something else which is useful for you. (P6)  
> It's more about you being engaged in one thing and something else occurs and you find something new unexpectedly. (P7)

Some participants defined serendipity as the unexpected connections between information; for example:

> My understanding of serendipity is a moment when a number of different things come together unexpectedly in a nice way within that particular context. (P10)  
> Serendipity comes from different types of things connecting together. I see it as something happening which makes me reinterpret things from a conceptual thinking perspective. In terms of the thought process, it is where things happen to allow reinterpretation or to add something crucial. (P3)

The characteristics of serendipity as identified by participants were that 1) serendipity is encountered unexpectedly, not intentionally, or accidentally, as illustrated by the following quotations:

> It (serendipity) is something that happens unexpectedly which surprises me. (P6)  
> It was not intentional that X or Y would happen, but X or Y happened unintentionally. (P10)

It is also about the unexpected finding of something relevant or making unexpected connections, such as:

> It (serendipity) is when you find something which is relevant for you, but unintentionally. (P1)  
> It (serendipity) happens when you make an unexpected connection. (P4)

Furthermore, it is impactful, productive and beneficial; for example:

> Almost entirely emotionally, it (serendipity) is something which has a good impact, far above what you expected to have. (P3)  
> Serendipity leads to a productive result. (P9)  
> I think serendipity has a beneficial component to it. I didn't go to the class to network with this guy, but I went to the class to listen to the topic. So that is perhaps more serendipitous if I benefit from it ultimately. (P9)

Finally, it involves temporal consideration (it often happens at the right time), social interaction and a contextual dimension (it may change over time and between situations):

> Serendipity seems happy at the beginning of a project. (P7)  
> I think it (serendipity) has to be between two people or more, really. For example, you just have just met somebody, and it turns out that you have some common interests. It could be useful for you. (P8)  
> It (serendipity) is like a spark, and certain things change when you think about it further. What I found to be serendipitous ten years ago would be different from what I am finding now. I think it is context specific. Therefore, it is a constantly mobile concept for each individual. (P10)

### The nature of a serendipitous experience

During our analysis of participants' diary entries, two different characteristics of levels of abstraction of serendipitous experiences emerged: 1) the unexpected finding of information, and 2) making unexpected connections between pieces of information. These two levels appear from the data to be distinct elements that provide routes to serendipity, either independently or in combination. Although these concepts have parallels with the categories proposed by Foster and Ford ([2003](#fos03)) the framework presented in the following section emerged directly and independently from the data obtained.

#### First level of abstraction: unexpected finding of information

There are three different components considered in the first level of abstraction, the unexpected finding of information. The first component considers whether the information is directly related to the activity currently being undertaken by the individual (either it is activity based or non-activity based). The second component considers the value of the information to the individual; whether or not the information encountered is unexpectedly valuable to the encounterers. The third component relates to the source of the information; whether or not it is an unexpected source, (information which the individual is not even aware exists also falls into this category) or, alternatively, a source that might be considered to be _likely_ (sources that the individual is aware of or has used frequently).

If it is considered that each of these three components have two alternative states (non-activity-based (A) vs. activity based (B); unexpectedly valuable (A) or not (B); unexpected (A) or likely (B) source) then it could be concluded that there are eight possible combinations of these components (i.e., AAA, AAB, ABA, ABB, BAA, BAB, BBA, BBB). We identified six of these combinations in our data:

1.  finding non-activity-based, unexpectedly valuable information from unexpected sources (AAA)
2.  finding non-activity-based, unexpectedly valuable information from _likely_ sources (AAB)
3.  finding non-activity-based information from unexpected sources (ABA)
4.  finding activity-based, unexpectedly valuable information from unexpected sources (BAA)
5.  finding activity-based, unexpectedly valuable information in _likely_ sources (BAB)
6.  finding activity-based information from unexpected sources (BBA)

Two combinations (non-activity-based information from likely sources (ABB) and activity-based information from likely sources (BBB) were absent from our data. This is likely to be because these experiences would not be classed as serendipity by our participants: the first (non-activity based information from likely sources) could be articulated as '_brilliant, I was looking for that earlier_' and the second (activity-based information from likely sources) merely describes what would hopefully be a successful directed information search activity.

The following section describes each of the six combinations that were identified as serendipitous by our participants, and explains how they were categorised using the framework described.

1.  Finding non-activity-based, unexpectedly valuable information from unexpected sources (i.e., location and/or existence). This denotes a situation in which: 1) people find information which is not directly relevant to their current activity, 2) the information is of value which was not anticipated, and 3) they find it in places where people would not normally think of looking for this type of information or they would not normally think that this type of information exists.  
    **_Example 1: Unexpected course module._**

    > I looked at the whiteboard in a seminar room when attending a workshop. I suddenly noticed that some interesting module codes had been written on one of the whiteboards. Actually, I was looking for module information from different sources (leaflets, the Internet) recently. As a result, I took courses in this mobile learning and new media module and I found it to be really useful. (P2)

    Here the participant found this information (information module code) as a consequence of engaging in something else (attending a seminar) in the seminar room. He did not normally think of the whiteboard as the place to find this type of information; instead he had tried to look for this information from different sources (leaflets, the Internet). In addition, he took the course and found the value of this module was unexpectedly relevant to his PhD research.

2.  Finding non-activity based, unexpectedly valuable information from 'likely' sources. This describes finding information which is not directly relevant to people's current activity and is of unexpected value. However, people would normally expect this type of information to exist or would normally think of looking for information in this location.  
    **_Example 2: Review of the iPhone._**

    > I have a dog and I took her for a walk in the morning, and I let her out to see whether or not she needed to wee. While she was outside, I read the news for five to ten minutes. I was reading news on phones. I found Wired (a website) has done a really good review of the iPhone 4\. But this is not a simple review by those who has played with it or looked at it; it is written by people who used it for one or two months and then they reviewed it. And so I was not looking to do something like that as I was just browsing the news - I found that was really cool. (P1)

    In this example, the participant described finding the review of the iPhone while letting his dog out. The source (i.e., location of the information) was something he expected because his intended activity was reading a news article about the iPhone and the website provided a relevant review of the iPhone. However, the review turned out to be of unexpected value.

3.  Finding non-activity based information from unexpected sources (i.e., location and/or existence). This denotes finding information which is not directly relevant to people's current activity and in places people would not normally think of looking, or people would not normally expect the existence of this type of information. Here there is no emphasis on the unexpected value of the information encountered.  
    **_Example 3: Sister of a Swiss colleague_**

    > On my journey back from the conference, I travelled back with my Swiss colleague. The serendipitous moment occurred on the train. We had a couple of hours talking, so the conversation went beyond work and towards more personal topics, one of which was my PhD [on privacy]. It turns out that her sister is a privacy scholar and is very interested in the impacts of technology. She suggested that, once I had something written, I should send a copy to her sister to see if there might be space for collaboration. (P10)

    The participant did not expect to have a good conversation with a Swiss colleague while travelling home from a conference. During the conversation, she accidently found out that the sister of her colleague is working on something relevant to her PhD, so she might be worth contacting later on. Despite the lack of immediate evidence of value of this encounter, it was still perceived as having been serendipitous, probably due to its perceived potential value in the future.

4.  Finding activity based, unexpectedly valuable information from unexpected sources. This refers to people finding information related to what they are doing or what they have been doing. However the information was unexpected in terms of: 1) the information being of unanticipated value and 2) finding it in places where people would not normally think of looking for this type of information, or where they would not normally think that this type of information exists.  
    **_Example 4: Akkadian website._**

    > This is an acquaintance of mine who does not know anything about what I do here. I introduced him a bit about poetry and languages. He then accidentally shared information with me which led me to a Website called 'Speech is Fire', which is run by Cambridge University [[now here](http://www.soas.ac.uk/baplar/recordings/)]. It is set of Akkadian poems read by various people because the Akkadian language is old, dating from about 1000BC from Iran area. It has been used here because it is connected to something I am looking at in my PhD, and again he sent it to me but he had no reason to know that it would be meaningful to me. (P4)

    The participant shared knowledge with an acquaintance about language and poetry. In association with this activity, the acquaintance supplied him with information which he did not know existed (unexpected source) and which was unexpectedly valuable to his PhD research.

5.  Finding activity-based, unexpectedly valuable information from likely sources (i.e., location and/or existence). This describes finding information which is relevant to people's current activity. Although people would normally expect this type of information to exist, or would normally think of looking for the information in a particular location, they still think of it as unexpected because of the value of the information encountered.  
    _**Example 5: A book.**_

    > I went to the library yesterday hoping to find some helpful books (and actively trying to open myself up to serendipitous opportunities) and as I looked around the return shelves I saw one on television discourse that I wouldn't have thought to look for ordinarily. It turned out to have a section on eavesdropping and non-participant interaction, which could be really relevant to my research! (P2).

    He went to a library to look for a book. He used to consider looking around the return shelves as a strategy to find useful books during his bachelor's degree study. This time, he tried the same strategy and he found a relevant book. This was all expected until he found that the content of the book was unexpectedly relevant to his PhD research.

    **_Example 6: A lecturer_**

    > I went to a lecture; the lecturer had mentioned that part of his work was identity related. I have done some digital identity work. So I approached him afterwards and as I talked to him he asked '_who is your supervisor?_' I told him it was Peter and he asked, '_who do you think was his supervisor? It was me_'. He then suggested that Andrew would probably introduce me to some of his work. It was a kind of serendipity in the sense that I did not know that he was Andrew's supervisor. He happened to be my supervisor's supervisor. What a funny coincidence. I got a new thread, a new body of research to investigate. (P3).

    The participant intentionally went to talk to a lecturer because the lecturer was doing something relevant to his PhD (activity based, likely sources). However from the talk, he accidently found that the lecturer was the supervisor of his own supervisor, from which he obtained information of unexpected value.

6.  Finding activity-based information from unexpected sources. This describes finding information which is relevant to people's current activity and in places people would not normally think of looking, or where people would not normally expect the existence of this type of information.  
    **_Example 7: Meeting with John_**

    > I had a meeting a few days ago with a chap called John from the business department, who had loads of good ideas and he pointed me to loads of cool people to study. In regard to my PhD, plenty of good stuff came out of this meeting. Thinking about it, the meeting was perhaps serendipitous because ordinarily I would never ask John these things at all. The only reason why I did goes back quite a long way; it was a chain of events. Last year I ended up living with a girl called Lily. When we lived together, we developed similar research ideas. She ended up doing a project on radio place. She asked me for a reference and I asked her to see John because he is an expert on that sort of thing. John ended up supervising her. While John was supervising her, she mentioned my research to him and he said he would be keen to meet me. So we ended up having this meeting and having these cool references. This was a little chain of events, where each one links to the next one, leading further and further away from the probability of things happening if the first event hadn't happened. (P2).

    The participant found useful information during a meeting with John regarding his PhD research (activity based), but the meeting came about as a result of a chain of social events, which comprised an unlikely source for a useful research meeting.

#### Overview of abstraction level one

The classifications in the examples above show some parallels with the work of Foster and Ford ([2003](#fos03)) in terms of considering serendipity as unexpected finding of information (i.e., unexpected value of the information and/or the unexpected source of the information). However this study places additional emphasis on scenarios which are either activity-based or non-activity-based to better understand the context and model aspects of human behaviour in serendipity The examples from abstraction level two (making unexpected connections - discussed in the next section) do not necessarily preclude this element, but demonstrate particular emphasis on making of the connection in which a serendipitous experience was perceived, but where the reflection on the experience did not focus on the discovery of new information.

#### Second level of abstraction: make unexpected connections

The second level of abstraction for participants to consider serendipity is that people make unexpected connections. Here, the emphasis is on the connection made, rather than the information encountered. The connections made include connections between different pieces of information, people and ideas. The examples of different types of connections are illustrated below:

Example 1: making unexpected connections between information.

> I was doing research on art work and I had this idea I am going to develop. I accidentally found another artist who has a very similar idea via the Internet. It is about collecting people's favourite songs. When I was reading about it, I realized that actually I had a book by this artist next to me. So that was the moment where all things came together, and I actually realized that I knew this artist, and I had a book by this artist next to me! So that was the moment. I suppose serendipity is where all things came together and she actually realized that she knew this artist. And she had a book by this artist next to her. (P5)

What the participant has described here is an example of the simultaneous occurrence of three unexpectedly connected things - her research idea, the artist who had a similar idea and the book about the artist beside her.

Example 2: making unexpected connections between information and people's existing memory.

> One day, I went on to Spotify to listen to classical music, and I focused on working on some literature review work. Suddenly the song 'Mad World' from Donnie Darko was played. I had forgotten all about that song, although I really liked it. I had completely forgotten about it. So I was not expecting to hear anything I particularly recognize or care about. But it was just there. It was really cool. The songs fit into a scene of a moment when it was snowing when I was at States. It is a serendipitous experience of reminiscence (P1).

In this example, he emphasised the connections made between the song encountered and his memory of being in the United States.

Example 3: making unexpected connections between ideas.

> I was thinking of ways of how human sense CO<sub>2</sub>, whilst writing about framing photographs to capture light and colour. The thought of what you might capture in the photo when looking up at the sky made me think that people could see examples of CO<sub>2</sub> and could guess how much might be in the atmosphere based on what they see. So it is the moment when you are thinking about one thing and it creates another kind of idea in your mind or makes the connection at that moment (P5).

The participant made a connection between two ideas which are not normally connected, which led to a solution of her research problem.

Example 4: making unexpected connections between people's physical environment and people's existing memories and ideas.

> I was heading to the airport and I caught a cold. I was walking down South Kensington and passing by the Victoria and Albert museum. It has a kind of hideous Victorian architecture on the top of it, and it is quite a good museum. The idea of serendipity was kicking around in the back of my head. That's a place with meaning for me, as I spent years working next door.Then it came to me like a bolt, the thought, all of this - someone made that. The PhD, the MRL, the horizon. For me, it's just a support system to help me make things, and I need to remember that. I can't lose sight of that. It's the interplay of memory, sight, and my ideas coming together in a flash. (P11)

The examples of serendipity at abstraction level two tend to place emphasis on the role of the individual, similar to the perseverance, altamirage and sagacity described by Liestman ([1992](#lie92)).

<div align="center">![Figure 3:The two levels of abstraction of a serendipitous experience.](p492fig3.jpg)</div>

<div align="center">**Figure 3: The two levels of abstraction of a serendipitous experience.**</div>

### The role of context in seredipitous experiences

Context has been discussed widely within the relevant literature (e.g., Dix _et al_. [2000](#dix00); Dourish [2001](#dou01)). In general terms, context includes aspects of the user, temporal factors, and physical and social environment (including objects, people and resources) that an individual is operating within. We found that context (e.g., the state of the individual experiencing serendipity, temporal factors and their environment) played an influential role in people experiencing serendipity. These elements of context are described in further detail below.

#### The role of people in encountering serendipity

In the study, serendipity happened when people were socialising (e.g., talking to a colleague), reading (e.g., reading a paper or a book), listening to a work-related talk/presentation, looking at something in their environment (e.g., a whiteboard or a building), listening to music or quietly thinking. Thus, the examples, as demonstrated below, showed both individuals who were in a 'state' that enabled them to experience serendipity, or in a social context that supported or directly provided the serendipitous opportunity.

People played an active role by engaging themselves with information seeking activity (e.g., reading) or socialising with others:

> Talking to the professor helped me to think about these topics. and he directly affected me by getting me to think about the type of structure of the PhD. (P3)  
> If I had not read this book, then I would not have found that information for the project. So definitely it is because I was reading that book (P10)

People played a less active role by opening up opportunities for serendipity; for example:

1.  People experienced serendipity when they were not narrowly focused on a particular task:

    > When I was not overly focused on the task. So, if I am not completely focused on a problem, but I do have problems with something I was working with. That might be a time when serendipity happens (P11)  
    > I did not do anything actively to get to this point. I was in a very clear thinking space. It (serendipity) just happened (P6)

2.  People experienced serendipity when they felt relaxed, less pressure and able to devote both physical and mental attention to making serendipitous connections/discoveries:

    > I think you need to be mentally ready to make that connection, because the possibility is always there. When I am less busy on something I am more receptive to that (serendipitous) information (P5)  
    > It was a quiet evening and I had time to read something I have to read. I was thinking what I want to do. I know I will read this because I have not read it yet, besides that I don't have much to do. I was just having a cup of coffee. I was just relaxing. Serendipity just happened (P9)

#### The role of temporal factors in encountering serendipity

Serendipity occurred when information came at a certain time (both at a particular stage in a research project and at particular times of day):

> It is usually before I have started doing the work, at the beginning of a project before I have clear ideas. I am open to information. That's when serendipity might happen. (P7)  
> _'Sometimes early in the morning I wake up and go into a very clear thinking space. Sometimes ideas come into my mind. I don't know where of ideas come from._(P2)

#### The role of environment in encountering serendipity

The physical environment was also seen to have a role in promoting serendipity. Serendipity happened frequently in a structured environment where people were thinking about work, including offices, lecture/seminar rooms, and libraries. It also happened when people were working 'on the move.'

Our participants experienced serendipity in:

1.  The working environment (e.g., an office) is considered a resource-rich place for people to experience serendipity:

    > The Doctoral Training Centre space was intense because there are always ideas flying around and things going on. There are lots of ideas and information going on and not in a direct fashion. You can guarantee that there is someone you can talk to, and who has something to contribute. Something you need someone to contribute. (P2)  
    > The Doctoral Training Centre is great for serendipity because there are all these people around you who you would not necessarily normally talk to, and they come from several different disciplines, with many areas of different expertise and different resources. It is useful just being able to talk to them, to share ideas with them and recommend them to each other. If I was in this place on my own, there are many papers I would never have thought to read or look up without having spoken to various members of the centre's students or staff. (P7)

2.  Places (e.g., conferences, lectures rooms, libraries, seminar rooms) where the provision of information is the primary function can nurture serendipitous events.

    > Being in the library was important. Being in a place and in an environment where I had reference material around me, I was in a creative environment, I suppose. (P2)  
    > I love the conference environment because you get to know so many people, to talk about your research while you are there, and to exchange ideas. (P6)

3.  As explained by this participant, in constantly changing environments where people are on the move the unfamiliarity of a changing environment focuses people's ability to engage with new information:

    > Only in the case (lots of people talking in the office) that I might block them out so I could not hear other people. So, I might not listen to music if I was just sitting in the office. I probably would just sit on the beanbag with my feet up and reading, going through my literature review, rather than blocking people out. (P1)  
    > Many people were watching this phone, which caught my attention. As a result, I found the design of this phone serendipitous. (P5)

Schmidt ([2000](#sch00)) proposes two general categories for structuring the concept of context: human factors and physical environment. These have three subcategories each. Human factors divide into: information on the user; social environment; and tasks, while the physical environment distinguishes: location; infrastructure; and physical conditions. Situations and environments are generally characterised by a large degree of continuity over time, so that time becomes an important feature for approximation of a given situation or environment.

Figure 4 adopted Schmidt's context model to denote the relationship between the role of the individual and their context in serendipity. It demonstrates both the different elements that might impact on an individual's readiness to experience serendipity, such as their attentional resources available, the pressure they are under, or their focus on the activity underway (which could be both positive, if the serendipitous experience is activity-based, or negative, if there is an opportunity of non-activity-based serendipity). The individual is then located within both a social and physical environment, the characteristics of which will impact on opportunity for serendipity, and temporal factors may also be of influence.

<div align="center">![Figure 4:The role of context in serendipity. ](p492fig4.jpg)</div>

<div align="center">**Figure 4: The role of context in serendipity.**</div>

### The impact of serendipity

Ford and Foster ([2003](#fos03)) suggest that serendipity can impact significantly on people's information research. Therefore, we also investigated the impact of serendipity on people's research from this empirical study. Our results showed that the impact can be short-term (where the benefits of serendipity can be seen immediately) or longer-term (where the serendipitous experience is only potentially useful and the impact will become clearer in time). The types of impacts mentioned include: 1) the experience is emotionally positive; 2) it helps to solve research related problems; 3) it strengthens existing research; 4) it leads to new research directions; 5) it is relevant to people's general interests. We now provide examples of each of these types of impacts:

1.  Information encountered was emotionally positive:

    > It was useful for reminiscing (P1)  
    > For me, how it (serendipity) worked in my research is quite fun. It is like you have two random ideas and just of sudden things getting together and you have a completely new idea. It is quite exciting or it is a great idea, and you feel that it came out of nowhere (P11)

2.  The serendipitous information often provided solutions to participants' existing problems:

    > I, kind of, took a new way of looking at things and that helped to solve certain problems (P3)

3.  The information encountered strengthened the researchers' existing research:

    > It really was a breakthrough for me. It is sort of helping me to understand a major gap in the literature. It would take me down a specific road. If I am right, the literature is in fact lacking this one point, which is exactly what I am going to explore (P6)

4.  The serendipitous information led the researchers in a new direction:

    > You come across things that are really useful to your PhD or to your research that you have not come across before, and they may serve as the first link in a new chain of events. Once you have found one thing or had some sort of interaction with one person, it can lead to another. In that regard, that is really beneficial (P8)

5.  The serendipitous information was relevant to some participants' general interests and its usefulness was likely to become apparent over time:

    > It was useful to me because I wanted to know about it. It is just my general interest area. I have not followed-up and done anything with the information. I just thought about it a while ago (P1)

## Discussion

### Understanding serendipity

Andre _et al._ ([2009a](#and09a)) propose a continuum of understandings from _blind luck_ to _sagacious_. Similarly our study also suggests that there are multiple possible understandings of serendipity rather than one particular _correct_ understanding. To derive a general understanding of serendipity, the key elements are abstracted from participants' perceptions: serendipity happens unexpectedly, unintentionally, and accidentally and, as a result, people find unexpected information or make unexpected connections between pieces of information. It brings positive, beneficial impacts (either short-term or long-term), and it involves temporal (e.g., being at the right time), social (i.e., communications) and environmental dimensions.

Consistent with the empirical studies of Foster and Ford ([2003](#fos03)) describing the nature of serendipity, we have found that serendipity involves the unexpected finding of information. This refers to finding information of value, which people would not normally think of, and/or finding information in places where people would not normally look, or finding the type of information which people would not normally think exists. The unexpectedness emphasises both the degree of familiarity (e.g., the location where people are not normally looking for information) and awareness (e.g., being unaware of the value of the information).

Besides Foster and Ford's ([2003](#fos03)) emphasis on the unexpected finding of information, we also identified another important nature of serendipity; that of making unexpected connections. Leong ([2009](#leo09)) has pointed out how serendipity is encountered when connections are made that lead to enlightened discoveries. Indeed, we found serendipitous encounters as a result of people making unexpected connections. The examples collected include connections between people's ideas and ideas, information and people's existing memories and between people's ideas and their physical environment.

Finding unexpected information and making unexpected connections are two levels of abstraction at which participants tended to consider serendipity. One level of serendipity can lead to another. By way of an example, one participant was doing a literature review for his research while listening to music and he accidently found a song which he had forgotten about (i.e., the unexpected finding of information). As he listened to the song, he started to connect it to his memory of a scene in the US. The difference between these two levels of serendipity is that participants put emphasis on the information encountered in describing the 'unexpected finding of information', whereas they focused on the unexpected connection in the level two understanding of serendipity. We consider the making of connections to be an important part of serendipity. Indeed, as highlighted by Cooksey, serendipity is not only about the piece of information encountered but also about '_the mind that sees the significance and makes the connections_' ([Cooksey 2004](#coo04): 25).

Another difference between our study and Foster and Ford's ([2003](#fos03)) studies is that while both studies are based on understanding of people's information activity, our study emphasised that serendipity is closely align to people's information activity and we incorporated it in describing the nature of serendipity. As Cunha ([2005](#cun05)) suggests, serendipity may be 'accidental' but it is not 'fortuitous' because it involves a deliberate process of searching for information to achieve a particular goal. However, it can be argued that serendipity can also happen more effortlessly (without people actively searching for information). For example, a participant travelled home from a project meeting and socialized with a colleague on the train. She serendipitously found unexpected information from the conversation with the colleague, which may be relevant for her research. In this example, the participant was not actively looking for information for her research; it happened in a more passive form. Therefore, we suggest that studying the nature of serendipity should include people's activity, which can help to describe what happens when people experience serendipity. Incorporating people's activity can help people to better understand the context in which serendipity is encountered and model aspects of human behaviour in serendipity.

The importance of context in serendipity is highlighted by Leong ([2009](#leo09)), who described how context could have an influence upon people's engagement with the information and which, in turn, could influence people to encounter serendipity. Indeed, the phenomenon of information seeking and serendipitous encounters are embedded in space of that information context. Contextual factors were found as enablers, as well as inhibitors of serendipity, which charted a design space for technology to create opportunities to stimulate serendipity. Serendipity occurs because people are in the right place at the right time. Fine and Deegan ([1996](#fin96)) suggest that the temporal opportunity for serendipity can be facilitated by judging '_where the action is_', discovering novel data sources and noticing '_a pattern or implication that has gone unnoticed and having exposed it, finding it in other social settings_'. Environmental factors occupy the central position among the contextual factors of information seeking practices (Reneker _et al_. [2001](#ren01)).

Based on our context-related findings, we suggest that in order to stimulate serendipity, technology should consider creating: 1) a resource-rich environment where people are exposed to multiple influences, with several things around them (e.g., visual stimuli), 2) an information environment which contains resources from outside of people's habitual data, information or search domain where new ideas can be stimulated, 3) a relaxing environment where people are not actively focusing on one thing but where they are open to exploring the things around them, and 4) an environment where people's minds are open and they are used to making many connections between information. At the same time, contextual factors can inhibit people from experiencing serendipity, such as, pressure, stress and limited attention (individual factors) therefore any technology to support serendipity should be able to take account of these contextual factors. McBirnie ([2008](#mcb08)) noted that people engaged their own personal 'serendipity filters' when they were unwilling or unable to act on serendipitous information discoveries that they made. Technology might be able to mirror such a filter by allowing users to control when they are 'open' and 'closed' to serendipitous opportunities.

<div align="center">![Figure 5:The nature of serendipity in an information research context.](p492fig5.jpg)</div>

<div align="center">**Figure 5: The nature of serendipity in an information research context.**</div>

### Methods for studying serendipity

Serendipity is difficult for researchers to capture or study, because it happens unexpectedly and it is a relatively rare occurrence. There have been some experimental studies to investigate serendipity (e.g., [Toms and McCay-Peet 2009](#tom09); [Erdelez 2004](#erd04)). It has been suggested that experimental research may well be useful for understanding serendipity, but future empirical studies of information encountering could include a naturalistic environment ([Erdelez 2004](#erd04)). Similarly, McCay-Peet and Toms ([2010](#mcc10)) suggest that studies of serendipity should consider the context that contains the precipitating conditions that activate a serendipitous experience.

Naturalistic, real-time observation is a widely used method to study phenomena or experiences in people's real life contexts. However its efficiency is limited in the study of serendipitous information discovery. The reasons are: 1) serendipity in information research involves unpredictable actions at unpredictable times and in unpredictable spaces, and 2) experiencing serendipity engages cognitive processes without overt observable actions. Therefore, a naturalistic, self-reporting, self-reflection method is needed to study this phenomenon. This study applied a mobile diary method as a reflective medium which allowed participants not only to decide when to diarize their serendipitous experiences but also to capture the moment in their real life contexts. Capturing participants' own serendipitous experiences helped them to realise the concept and to reflect upon it. Their diary entries were used later on in post-study interviews as resources and memory cues to explore more deeply their understanding and experience of serendipity with the researchers.

Most participants (9 out of 11) enjoyed using the mobile diary application. The mobile diary provides a unique tool which allows people to capture serendipitous experience _as they happen_, which is particularly important as it is difficult to predict when serendipity will occur. No participants reported that they missed capturing any serendipitous experience using the tool. Serendipitous experiences in today's information-intensive environment emphasises users' mobility. People can move efficiently among information sources that are relevant to their problem or interest under various environments and situations, which subsequently means that they can find unexpected information while on-the-go. Making a record of experiences as they happen is vital as it is often hard to recount these experiences after some time has passed, unless they are particularly memorable.

One concern of using the application was portability. A few participants were reluctant to carry an extra mobile device (which ran the diary application) during the study. We suggest in the future integration of the mobile application to people's existing use of technology (e.g., participants' own smart phones).

A by-product of the study was that the diary application itself was found be a means of stimulating serendipity, because it allowed participants to gather all kinds of research data and ideas, and helped them to reflect on their research, and to capture personal experiences in real time. By capturing the phenomena our participants thought more about serendipity, through which their ability and willingness to adapt their attitudes and approaches to encourage serendipity could be actively nurtured ([McBirnie 2008](#mcb08)).

## Conclusion

Whilst this study is not intended to provide a comprehensive understanding of serendipity in information research because of the relatively short period of the study and the small sample size of participants, the data gathered contained rich and detailed information.

The paper contributes to this field of research by applying a user-centred approach to study serendipity in terms of how people perceive serendipity, how serendipity happens and the role of context in the way people experience serendipity and the impacts of serendipity. The results suggest that serendipity is about the finding of unexpected information and/or constructing unexpected connections between pieces of information. It brings positive and beneficial impacts to those people who encounter it. Our research suggests that a framework for classifying serendipity should consider aspects associated with the activity, the value of the information, the source of the information and the interaction between the individual and the context.

This paper also makes an important methodological contribution by suggesting that mobile diary studies are an effective way of capturing serendipitous experiences, particularly when used in conjunction with semi-structured interviews. Serendipity may be difficult to study with traditional observational approaches. Our mobile diary was developed to capture people's serendipitous experiences 'on the move' and this enabled people to be more aware of opportunities for serendipity and therefore may have actually indirectly contributed to some of their serendipitous experiences without them even realising it. Whilst our framework can be regarded as a potentially useful way of understanding and classifying experiences of serendipitous information discovery, our mobile diary study method can be considered as a useful (and novel) way of capturing these experiences in the first place.

Future research will investigate how the mobile diary application might be used to gain an understanding of patterns in individuals' serendipitous experience through longitudinal use and investigate its possibility as a tool for evaluating impact of serendipity and user experiences that have arisen as a result of using interactive systems that aim to encourage serendipitous experiences. As this study found the diary application itself was found to be a means of stimulating serendipity, it will be also interesting to further investigate when and how serendipity was stimulated using the tool.

## Acknowledgements

This work is supported by Horizon Digital Economy Research (RCUK grant EP/G065802/1) and EPSRC (EP/H042741/1).

## About the authors

**Xu Sun** has worked as a Research Fellow at the University of Nottingham on the UK Research Council-funded project: SerenA: Chance counters in the Space of Ideas. She will shortly be comencing a lectureship in Product Design at the University of Nottingham, Ningbo campus in China. She can be contacted at [xu.sun@nottingham.ac.uk](mailto:xu.sun@nottingham.ac.uk.).  
**Sarah Sharples** is Associate Professor and Reader in Human Factors and Head of Human Factors Research Group at the University of Nottingham. She is co-investigator on the SerenA project and her main areas of interest and expertise are Human-Computer Interaction, cognitive ergonomics and development of quantitative and qualitative research methodologies for examination of interaction with innovative technologies in complex systems. She can be contacted at [sarah.sharples@nottingham.ac.uk](mailto:sarah.sharples@nottingham.ac.uk).  
**Stephann Makri** is a Research Associate at University College London Interaction Centre with a background in understanding how people acquire, interpret and use information in the context of their work and leisure activities and using this understanding to inform the design and evaluation of interactive systems. He is also working on the SerenA project and his current work involves understanding how researchers come across information serendipitously in their work and everyday lives and using this understanding to inform the design of ubiquitous computing environments. He can be contacted at [s.makri@ucl.ac.uk](mailto:s.makri@ucl.ac.uk).

#### References

*   Andre, P. & Schrafel, M.C. (2009). Computing and chance: designing for (un) serendipity. _The Biochemist E-Volution_, **31**(6), 19-22.
*   Austin, J.H. (2003). _Chase, chance and creativity: the lucky art of novelty_. (2nd Ed.) Cambridge, MA.: MIT Press
*   Bolger, N., Davis, A. & Rafaeli, E. (2003). Diary methods: Capturing life as it is lived. _Annual Review of Psychology_. **54**, 579-616
*   Case, D. (2007). _Looking for information_ (2nd ed.) London: Academic Press.
*   Cooksey, E.B. (2004). Too important to be left to chance - serendipity and the digital library. _Science and Technology Libraries_, **25**(1), 23-32.
*   Cunha, M. (2005). _Serendipity: why some organisations are luckier than others_. Lisbon: Universidade Nova de Lisboa. (FEUNL working paper series)
*   Danzico, L. (2010). The design of serendipity is not by chance. _Interactions_ **17**(5), 16-18.
*   Dix, A., Rodden, T., Davies, N., Trevor, J., Friday, A. & Palfreyman, K. (2000). Exploiting space and location as a design framework for interactive mobile systems. _ACM Transactions in Computer Human Interaction_. **7**(3), 285-321.
*   Fine, G.A. & Deegan, J.G. (1996). Three principles of serendipity: insight, chance, and discovery in qualitative research. _Qualitative Studies in Education_ **9**(4), 434-447.
*   Foster, A. and Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation_, **59**(3), 321-340.
*   Foster, A. (2006). A non-linear perspective on information seeking. In Amanda Spink & Charles Cole (Eds.)., _New directions in human information behaviour_, (pp. 155-170). Dordrecht: Springer.
*   Leong, T.W. (2009). Understanding serendipitous experiences when interacting with personal digital content. Unpublished PhD thesis. University of Melbourne, Australia.
*   Leong, T.W., Wright, P., Vetere, F. & Howard, S. (2010) Understanding experience using dialogical methods - The case of serendipity. In _Proceedings of the 22nd Conference of the Computer-Human Interaction Special Interest Group of Australia on Computer-Human Interaction, OzCHI, Brisbane, Australia, 2010_, (pp. 256-263). New York, NY: ACM Press,.
*   McCay-Peet, L. & Toms, E.G. (2010). The process of serendipity in knowledge work. In _IIiX '10 Proceedings of the Third Symposium on Information Interaction in Context_, (pp. 377-382). New York, NY: ACM Press.
*   McBirnie, A. (2008). Seeking serendipity: the paradox of control. _Aslib Proceedings_, **60**(6), 600-618.
*   Merton, R.K. & Barber, E. (2004). _The travels and adventures of serendipity._ Oxford: Princeton University Press.
*   Palen, L. & Salzman, M. (2002) Voice-mail diary studies for naturalistic data capture under mobile conditions. In _Proceedings of the 2002 ACM Conference on Computer Supported Cooperative Work 2002_, (pp. 87-95) New York, NY: ACM Press.
*   Roberts, R.M. (1989). _Serendipity: accidental discoveries in science_. New York, NY: Wiley.
*   Schmidt, A. (2000). Implicit human computer interaction through context. _Personal and Ubiquitous Computing_, **4**(2), 191-199.
*   Toms, E. (1998) _Information exploration of the third kind: the concept of chance encounters_. Paper presented at the Making the Impossible Possible, ACM Special Interest Group on Computer-Human Interaction Conference on Human Factors in Computing Systems, Los Angeles, CA.
*   Toms, E. (2000) Understanding and facilitating the browsing of electronic text. _International Journal of Human-Computer Studies_. **52**(3).
*   Toms , E.G. & McCay-Peet, L. (2009). Chance encounters in the digital library. In M. Agosti, J. Borbinha, S. Kapidakish, C. Papatheodorou & G. Tsakonas (Eds.), _ECDL'09\. Proceedings of the 13th European Conference on Research and Advanced Technology for Digital Libraries, Corfu, Greece_, (pp. 192-202). Berlin: Springer Verlag. (Lecture notes in computer science 5714)

## Appendix

Post-interview debriefing questions

1.  Participants' background questions:
    *   What is your academic background? (Which course are you studying? What topic is your research on?)
    *   Before we go through your diary, can you tell me what you understand by the term 'serendipity'? Can you give an example? Have you ever come across information serendipitously?
2.  Go through each serendipitous experience:
    *   You captured this? can you elaborate?
    *   How did you come across the information/idea or thought? (e.g., How exactly did it happen? Where? What were you doing and immediately after? How did you feel?)
    *   What made you think this was serendipity? At what point did you decide that you had experienced serendipity?
    *   How useful was the information/idea you came across? Why?
    *   What did you do with the information/idea after you had come across it?
    *   Did other people play any role in you coming across the information? If so, what role?
3.  Closing questions:
    *   What are the best/worst aspects of coming across information serendipitously?
    *   Have you had any other serendipitous experiences which you did not capture during this study?
    *   Did the presence of the diary or participating in the study change the way you view serendipity? If yes, how?