#### vol. 16 no. 3, September, 2011

# Exploration of the needs of South African oncology nurses for current awareness services available through the Internet

#### [Ina Fourie](#author) and [Retha Claasen-Veldsman](#author)  
Department of Information Science, University of Pretoria, Private bag X20, Hatfield, Pretoria 0028, South Africa.

#### Abstract

> **Introduction.** Reports on an exploratory study on the information needs of a small group of South African oncology nurses.  
> **Method.** Individual questionnaires, focus group interviews, and semi-structured interviews were used for data collection from a convenience sample of nursing staff, an oncologist and an oncology social worker.  
> **Analysis.** A qualitative analysis was done; recorded interviews were analysed to identify main themes.  
> **Results.** Tasks are perceived as not information intensive with limited opportunities for decision-making regarding patient care. Although emotion, stress and underlying anxiety feature strongly in daily tasks, these are not linked to information needs. Limited interest in current awareness services is influenced by working conditions, lack of access to information resources, lack of computer skills and experience in using the Internet, lack of motivation to use current awareness services, expectations and support from management, and the impact of specific tasks and responsibilities.  
> **Conclusions.** Task-based information seeking models, context (organizational and health care cultures), motivation for personal development, and the impact of social organization in health care work and emotion and anxiety require further exploration. Suggestions for the development of a model of information behaviour and further theoretical grounding are offered.

## Introduction

With an increase of access to the Internet and free access to numerous types of current awareness services available through the Internet, it seems timely to explore interest in current awareness services from different professional groups. This article focuses on the interest of oncology nurses, specialising in cancer care. Apart from work environments presenting new challenges such as cancer patient education ([Echlin and Rees 2002](#ech02); [Hartigan 2003](#har03); [Hughes _et al._ 2000](#hug00); [Jenkins 1997](#jen97); [Mahon and Williams 2000](#mah00); [Tarzian, Iwata and Cohen 1999](#tar99); [Thomson 2000](#tho00); [Van der Molen 1999](#van99)), there is an increase in the number of cancer patients, as well as interest in the use of technology in cancer prevention ([Jimbo _et al._ 2006](#jim06)). The increase in cancer patients was also noted during the study reported in this article: '_Cancer in general has increased a lot in the last year??? much more intensive_' (All quotations originally in Afrikaans have been translated). According to Dickerson _et al._ ([2006: E16](#dic06))'nurses should be open to discussion of the information that patients bring to the interaction. Nurses also should be knowledgeable about useful Web sites and online support groups to recommend to patients'.

The purpose of the survey was to explore whether oncology nurses are using current awareness services and if not, whether they would be interested in them, and how such interest can be stimulated. The purpose was not to generalise findings based on the input of a small, purposive, convenience sample, but to explore the refinement of research methods and data collection and new routes (e.g., theories and models of information behaviour) to expand the study and build a model for the use of current awareness services by oncology nurses.

## Factors leading to the study

Factors leading to the study are discussed by Fourie and Claasen-Veldsman ([2005](#fou05), [2007a](#fou07a), [2007b](#fou07b)) and can be summarised as follows:

1.  Increased interest in information needs and information seeking in context. Recently there has been an increase in publications on information seeking behaviour and information needs ([Case 2006](#cas06), [Fisher _et al._ 2005](#fis05a), [Courtright 2007](#cou07)). Although a number of models of information seeking behaviour , for example Ellis and Haughan ([1997](#ell97)), depict information monitoring, a term related to current awareness services, as one of the phases or stages in information seeking, information monitoring is normally not mentioned. Wilson ([1999a](#wil99a), [b](#wil99b)) depicts information monitoring as _ongoing search_. It seems as if we know very little about the actual use of current awareness services for information monitoring as well as the need for, and interest in, current awareness services. Alper _et al._'s ([2004](#alp04)) article on the effort required to keep up with literature in primary care is a valuable contribution on this.
2.  Increased access to current awareness services, especially those available for free through the Internet (e.g., tables of contents services or RSS feeds, electronic newsletters, book alerting services, electronic discussion groups, monitoring of Websites with noteworthy content, portals, Weblogs and news filtering services). Although this should increase interest in the use of current awareness services, the potential is not always noted. Apart from not being aware of these services, or how to use them, people may actually not be interested in such services. In earlier studies Bystr?m ([2002](#bys02), [2005](#bys05)) found that there are often tasks considered not to be information intensive. Wilson ([1993](#wil93)) also makes it clear that'keeping current' does not necessarily imply a change of knowledge. He notes: 'one may be interested in a field of activity where nothing is happening - one stays current by continuing to look for something to happen, but nothing does??? Current does not mean better' ([Wilson 1993](#wil93): 639). Sometimes you can also cope quite well without putting in the effort of staying abreast. The variety of current awareness services available through the Internet for oncology nurses and examples are discussed by Fourie and Claasen-Veldsman ([2005](#fou05), [2007b](#fou07b)).

## Purpose of the exploratory study

The survey attempted to determine the need and interest for current awareness amongst oncology nurses. It is hoped that the findings can promote the use of Internet current awareness services available for free and to refine research methodology to do more extensive local and internationally comparative studies. This might pave the way for studies with other health care professionals, bearing in mind that'_oncology is different from anything else???_' (All quotations originally in Afrikaans have been translated).

The study focused on a number of influencing factors and interests noted by a mixture of positions (i.e., an oncologist, unit manager, Sister, nurses, staff nurses, etc). This paper reports on:

1.  demographic data;
2.  impact of the environment and setting;
3.  impact of specific tasks and responsibilities;
4.  barriers to information seeking and the use of current awareness services; and
5.  perceptions of the value of information and interest in using current awareness services.

## Clarification of terminology

Oncology is the field of medicine devoted to cancer and the study of tumours ([Medicine Net 2003](#med03)), and oncology nursing concerns cancer patients. Fourie and Claasen-Veldsman ([2006](#fou06): 38) cite a very apt description from Nursing Spectrum which explains an oncology nurse as someone who 'cares for patients with the diagnosis of cancer in various settings; utilizes an emphatic and caring approach to patients whose diagnosis and treatment are often painful and life threatening; administers chemotherapy, conducts patient teaching, and manages illness- and treatment-related symptoms'.

Although there are numerous definitions of current awareness services and alerting services (often used interchangeably with current awareness services), the emphasis is that these are services to help people keep track of current developments and the latest information. Fourie defines current awareness services as:

> a selection of one or more systems that provide notification of the existence of new entities added to the system's database or of which the system took note (e.g., documents, web sites, events such as conferences, discussion groups, editions of newsletters). CAS automatically notify users or allow users to check periodically for updates. The entities can be specified according to users' subject interests or according to the type of entity (e.g., books or newsletters) ([Fourie 1999](#fou99): 282).

Many interpretations of _information behaviour_ have also been offered. Widely accepted is the encapsulating definition of Wilson ([1999a](#wil99a): 249), explaining information behaviour as'_the totality of human behavior in relation to sources and channels of information, including both active and passive information-seeking and information use_'. Fisher _et al._ ([2005](#fis05a): xix) define information behaviour as'_how people need, seek, give and use information in different contexts_'. In the context of this article, information behaviour is interpreted as how people (in this case oncology nurses) need information (including factual information, retrospective searches and information to stay abreast), their interest or lack of interest in information, the sources and channels that they prefer (i.e., how they actually seek the information) and the factors that influence their behaviour to search for information and use it, or not to do this. Although the study focuses on the use of current awareness services (i.e., information monitoring), it also addresses the _encapsulating_ information behaviour of oncology nurses.

## Research setting

An exploratory study based on purposive, convenience sampling was conducted at a medical oncology centre and two oncology hospital wards in Pretoria (South Africa). One of the wards mostly treats patients receiving intensive chemotherapy, stem cell transplants, etc., while the other mostly treats patients who are terminally ill (needing palliative care), as well as children (needing paediatric care). The sample is depicted in Table 1.

<table style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">  
**Table 1: Convenience sample**</caption>

<tbody>

<tr>

<th>Medical oncology centre</th>

<th>Hospital oncology wards</th>

</tr>

<tr>

<td>Head oncologist (management) (1)</td>

<td>Ward unit manager (Sister) (1)</td>

</tr>

<tr>

<td>Oncology nurses (Sisters) (5)</td>

<td>Oncology nurses (Sisters, staff nurses, assistant nurses) (16)</td>

</tr>

<tr>

<td>Oncology social worker (1)</td>

<td>Care workers (2)</td>

</tr>

</tbody>

</table>

The medical oncology unit and hospital wards serve the same patients. However, they function under different management, funding and administrative structures (as explained in more detail in sections 7.2.2 and 7.2.3). The study covers the views of management (the head oncologist and the hospital ward unit manager) as well as staff members on different levels (e.g., Sisters, nurses and care workers). Both the medical centre and the hospital wards rely on the support of a multi-disciplinary team including dieticians, physiotherapists and pharmacists. An oncology social worker (a staff member of the medical oncology centre) that consults with centre patients and staff from both the centre and hospital wards was also included.

Although a small survey, it was felt that both groups are sufficiently representative of leading oncology settings in South Africa for exploratory purposes. (Many studies with similar small groups of participants have been reported e.g., studies with twenty patients each by Dickerson _et al._ ([2006](#dic06)) and Andreassen _et al._ ([2005](#and05a)) However, when expanding the survey, participants from academic hospitals and nursing students specialising in oncology nursing should also be included. Permission for the study was obtained from the university's research ethics committee, head oncologist, hospital management and unit manager of the hospital wards.

Due to heavy work schedules and staff shortages, the hospital could not immediately participate. The study with the oncology medical centre was conducted in September 2005, while the study with the hospital wards was conducted in January 2006\. The head oncologist suggested participants, while the hospital unit manager called on all staff members available on the two days the researchers visited the wards.

## Methods

The study is based on a literature survey and empirical study. Apart from the methods used, suggestions for adaptations for further research are offered.

### Literature survey

A wide variety of databases in the field of information science, nursing and medicine were searched, namely _CINAHL_, _Library and Information Science Abstracts_, _Library Literature and Information Science_, _Social SciSearch_, _Medline_, _AIDS_ and _Cancer Research Abstracts_, _Conference Papers Index_, _Applied Social Sciences Index and Abstracts_, _ScienceDirect_, _Emerald_ and _Web of Science_. It showed that very little is known about the information seeking behaviour of oncology nurses. In one of very few studies on the information needs of oncology nurses, Fitch, Chart and Parry ([1998](#fit98)) report on their information needs with regard to breast diseases. Most of the nurses felt that they experienced no gap in their knowledge, and therefore did not need information. Cobb and Baird ([1999](#cob99)) report on how oncology nurses use the Internet for drug information, literature searches, academic information, patient education, and continuing education. Nothing could be traced on the provision of current awareness services for oncology nurses.

Slightly more is known about the information needs of nurses in general. In an early study, Wakeham ([1992](#wak92)) found that nursing students are not readers and that they prefer to consult with colleagues to satisfy information needs. A preference to consult colleagues, reference manuals, textbooks and protocol manuals is also confirmed by Cogdill ([2003](#cog03)). According to McCaughan _et al._ ([2005](#mcc05)) nurses prefer _humanised_ sources of information including help lines, e-mail discussion groups and networking with colleagues. Their knowledge of CD-ROM and other electronic databases in general was low. Secco _et al._ ([2006](#sec06)) also stress the preference for printed sources such as textbooks and reference works and inter-personal communication, while nurses with higher levels of computer skills used more computer-based sources. Anthony ([2000](#ant00)) reports on an international comparison of computer networks' use and potential use in a health care context. Not surprisingly there seem to be significant differences between poor and rich countries with respect to access to networking facilities. There was a significant difference between rich and poor countries with respect to the World Wide Web and USENET, with richer countries having greater access. Anthony ([2000](#ant00)) noted enthusiasm for using computer networks in poorer and geographically more remote countries. From a list of ten services available through the Internet, the network resource most valued by nursing organizations was online databases; the least valued was videoconferencing.

In 2003 Estabrooks _et al._ reported low Internet use at work for nurses, even though there is an increase in its use at home. According to Wozar and Worona ([2003:216](#woz03))'_Practicing clinical nurses will use online medical information resources if they are first introduced to them and taught how to access and use them_'. In their study, Dee and Stanley ([2005](#dee05)) found participants (clinical nurses and nursing students) were very keen on training in database searching if they had better computer skills. Nicholas _et al._ ([2005](#nic05): 39) and Bertulis and Cheeseborough ([2008](#ber08)) noted very poor access to computers and the Internet for nurses.

Nurses require information because of encounters with patients ([Cogdill 2003](#cog03)) as well as for decision-making in evidence-based practices ([French 1998](#fre98)). Bertulis and Cheeseborough ([2008](#ber08): 187) confirm a patient care focus in the needs of nurses.

Other reports on the information seeking of nurses that are worth noting include Sundin's ([2002](#sun02)) report on nurses' information seeking and use as participation in occupational communities, Cogdill's ([2003](#cog03)) work on primary care nursing practitioners, Urquhart and Crane's ([1994](#urq94)) report on nurses' information-seeking skills and perceptions of information sources, Barta's ([1995](#bar95)) report on the information seeking of paediatric nurse educators, Verhey's ([1999](#ver99)) discussion of information literacy as part of an undergraduate nursing curriculum and Gregg and Wozar's ([2003](#gre03)) report on a training course to help nurses to use Internet health-related sources. In a study by McCaughan _et al._ ([2005](#mcc05)) a nurse reported accepting a site with commercial interest as highly reliable on diabetes because of the Website address. (A similar experience was confirmed by one of our participants.) This study also found that nurses were more inclined to use the Internet at home where the presence of family members (expert users) was welcomed.

> Practice nurses and nurse practitioners have easier access to computers than other groups of nurses working in the community but require further training in information technology and critical appraisal skills, along with protected time, if they are to exploit the information resources that are becoming increasingly available to them online ([McCaughan _et al._ 2005](#mcc05): 496).

McKnight ([2007](#mck07): 70) reports on the information seeking of on-duty critical care nurses: 'No one can retrieve reliable literature and systematically review it while watching monitors, checking on patients, administering and verifying therapies, and answering telephone calls??? They respect research-informed practice and want the best of what academia and libraries can give them to support the care of their patients. However, their duties leave no room for such pursuits??? Given the economic realities of health care, hospital administrators are unlikely to pay nurses for off duty time for such pursuits. What they do, they have to do on their own time'. This is in line with the findings by MacIntosh-Murray and Choo ([2005](#mac05): 1332) that'_front-line staff are task driven, coping with heavy workloads that limit their attention to and recognition of potential information needs and gaps_'.

To put the information-seeking behaviour of oncology and other nurses in context, one should note some of the findings concerning clinicians and other health care workers: Andrews _et al._ ([2005](#and05b)), for example, found that typical barriers for primary care practitioners include lack of time, cost of using resources, the format of information resources and their information seeking skills, while Cullen ([2002](#cul02)) found that most family practitioners urgently need training in searching and evaluating information from the Internet. In a study conducted in New Zealand, Cullen ([2002](#cul02)) found that most family practitioners did not have access to high quality information sources.

French ([2006](#fre06)), exploring uncertainty and information needs in nursing, noted the difficulty in recognising and articulating information needs as well as different types of uncertainty, namely: known or accepted uncertainty, hidden (unaware) uncertainty, unrecognised (ignored) uncertainty and pragmatic uncertainty. Such uncertainty impacts on information seeking.

Although these research reports explain the research methods and data collection, it was interesting to note that they did not dwell on problems experienced. (Boisson and Docsi ([2005](#boi05)) is an exception, reporting difficulty in making appointments with female general practitioners, probably because of their busy schedules.) For the reported study it was found that this is one aspect that can have a major impact on further research with oncology nurses. Methods that were reported include a large survey based on questionnaires and interviews ([Estabrooks _et al._ 2003](#est03)); participant observation and in-context interviews ([McKnight 2006](#mck06)); questionnaires, interviews and observation with twenty-five nursing students and twenty-five clinical nurses ([Dee and Stanley 2005](#dee05)); questionnaires (300 nursing professionals) and interviews (twenty nursing professionals) ([Cogdill 2003](#cog03)); and interviews, observation and documentary analysis with a purposive sample of twenty-nine practice nurses and four nurse practitioners ([McCaughan _et al._ 2005](#mcc05)).

### Empirical study

The methods for data collection used in the before-mentioned studies (reporting on qualitative and quantitative methods) as well as studies on information-seeking behaviour in general (e.g., as reported by [Case 2006](#cas06); [Fourie 2002](#fou02)) were considered in addition to the demanding nature of the work of oncology nurses. The following methods were selected:

*   Individual, detailed, semi-structured interviews with management according to an interview schedule (i.e., with the head oncologist and unit manager of the hospital wards).
*   Self-administered questionnaires.
*   Focus group interviews according to an interview schedule. [The final version of the schedule [can be found here](p484focusgrp.pdf)]. We later moved to semi-structured individual interviews guided by the questionnaire.

Although aware of the hectic schedules of the participants as well as the emotionally demanding nature of their jobs, it was felt that the combination of methods would enable the collection of information-rich data, which was considered essential for purposes of the study. By means of the questionnaire we hoped to collect more detailed information on individual information behaviour .

The (rather detailed) questionnaire was tested with three participants (two from the centre and one from the hospital wards) and adapted; it went through two iterations. [The final version of the questionnaire [can be found here](p484nursqure.pdf)].

#### Survey in the medical oncology centre

The survey of staff of the medical oncology centre was completed in September 2005\. In addition to the questionnaire, two focus group interviews were conducted with the centre staff. These were followed by an interview with the head oncologist.

#### Survey in the hospital wards

Based on the survey with the medical oncology centre, the methods for data collection were adapted. Apart from the expected hectic schedules, they were working with serious staff shortages: it was obvious that they would not have time to complete the questionnaire and that focus group interviews would not be possible. Since they need to take care of patients, complete rounds with doctors and other urgent responsibilities, it is almost impossible to free more than one staff member at a time: '_If you get two of them together at the same time you are lucky'_. The questionnaire was used as a guideline for individual interviews, and the questions were adapted according to answers.

The survey with the hospital staff was conducted in January 2006\. It started with an individual interview with the unit manager, who holds the rank of Sister. The interview was according to the same schedule as for the head oncologist.

Over two days, interviews were conducted with the staff on duty. Being on site and available for a large part of the day was an effective way to gather data, as it provided a valuable opportunity to speak to the staff and also to glean more perceptions and information from them than would be possible from the questionnaire alone. It was also valuable as it provided an opportunity to explain what was meant by concepts such as current awareness service and the different techniques used. Not only Sisters, but also other staff levels, including the care workers, were included. Although this method of data collection saved much of the participants' time, it meant very intensive interviewing for the researchers.

Both researchers were present during all the interviews. The interviews were tape recorded with the consent of participants, and notes were also taken by one of the researchers. Due to the nature of the project (exploratory) it was not considered necessary to do a verbatim transcription of the interviews.

#### Questionnaire

Questions covered the following:

*   Personal and demographic detail (e.g., age, sex).
*   Qualifications and experience (in oncology as well as other), opportunities to attend training sessions, conference papers presented and papers published (the last two were included, because it could point to the need to use current awareness services).
*   Workplace, tasks and job description.
*   Availability of technology to access information as well as opportunities for access.
*   Use of technology to access information (e.g., e-mail, Internet), reasons for using the Internet.
*   Skills that will help in keeping up with new information (e.g., computer skills, Internet search skills).
*   Need for information and information skills.
*   Awareness of new information and developments.
*   Methods used to keep up with task-related developments.
*   Feelings about the importance of current awareness services.
*   Knowledge and ability to successfully use current awareness services.
*   Perceptions of management's support for the use of current awareness services.
*   Interest in learning more about current awareness services.

#### Interview schedules

An interview schedule was used for the interviews with management. It included the following broad categories: task environment, staff information, access provided to information and information communication technology (ICT), expectations for staff to use ICT and information, institution's expectations for staff to use current awareness services, institutional support offered to staff to become aware of new developments, importance of various types of opportunities to keep up with developments, sharing new information in the institution, problems that might be experienced, advantages expected from using current awareness services, and interest in searching for solutions (e.g., through training interventions, access to current awareness services, e.g., through a Website, software solutions, or collaborative information seeking/work).

The following broad categories were covered in the interview with the centre staff: c hanging and dynamic working environments, importance of keeping up with developments, getting information, keeping up with developments, effectiveness of resources to keep up with developments, sharing information, problems experienced, and enthusiasm for using current awareness services.

#### Suggestions on methods for empirical data collection

Although the combination of methods was very useful and ideal for collecting information rich data, it needs to be adapted to accommodate busy and fairly inflexible schedules. The following are suggested ([Fourie & Claasen-Veldsman 2007a:51](#fou07a)):

1.  Individual semi-structured interviews with management: this worked well, and provided very valuable information on the task environment and wider context. Since management was more aware of the importance and value of current awareness services, it also seemed as if they were more willing to set time aside.
2.  Very short, self-administered questionnaires for demographic detail such as workplace, job position, access to computers and the Internet, as well as computer and information literacy skills. Although participants were familiar with conferences, short courses and workshops as methods to keep track in their profession, most had no knowledge of current awareness services as we viewed them. They therefore had difficulty in answering questions on these. Self-administered questionnaires might work well after individual or focus group interviews in which the concept of current awareness services via the Internet (including free availability), and examples of current awareness services might be explained.
3.  Individual semi-structured interviews according to an interview schedule. Following an explanation of current awareness services, valuable information can be collected on individual perceptions of current awareness services, their potential value for them, their interest in current awareness services, etc. Although it takes up much more of the researchers' time, this method of data collection is more convenient for the participants, and many also feel more comfortable sharing information on an individual basis. It is more rewarding in terms of the data that can be collected.
4.  Focus group interviews according to an interview schedule. If participants' work schedules allow, this is an excellent method for collecting information on perceptions of current awareness services and reasons for using/not using current awareness services. Since focus groups take more of the participants' time, it might be combined with a purposive incentive, such as a free workshop on computer and Internet skills.

## Selected findings

The findings for this exploratory study are based on a qualitative analysis only. From the questionnaires and especially interviews, a few key issues were identified for further analysis. The identification of these is based on the guidelines by Gorman and Clayton ([1985](#gor85)) and Williamson ([2006](#wil06)) to identify main, recurring themes and supporting evidence supported by respondents' own words. For the moment a quantitative analysis will not be done, for example identifying correlations between the demographic data (e.g., age, gender, qualifications) and interest in current awareness services.

### Demographic data

The demographic information was collected to get a clearer picture of the participating group. At this stage there does not seem to be any obvious correlations. From the total of twenty-six research participants, all of them were female, except for one male (the head oncologist). The age distribution of the participants is as follows:

<table style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">  
**Table 2: Age distribution of participants in the study**</caption>

<tbody>

<tr>

<th rowspan="2"> </th>

<th colspan="7">Age distribution</th>

<th> </th>

</tr>

<tr>

<th>20-25</th>

<th>26-30</th>

<th>31-35</th>

<th>36-40</th>

<th>41-45</th>

<th>46-50</th>

<th>50+</th>

<th>Total</th>

</tr>

<tr>

<td>No.</td>

<td align="center">1</td>

<td align="center">4</td>

<td align="center">7</td>

<td align="center">4</td>

<td align="center">4</td>

<td align="center">1</td>

<td align="center">5</td>

<td align="center">25</td>

</tr>

</tbody>

</table>

Respondents closer to retirement indicated that they might be less interested in current awareness services: '_At my age??? too late to start with such things. My family is for me the most important'_.

The academic qualifications of the participants ranged from Grade 12 (i.e., highest secondary qualification) to postgraduate qualifications (e.g., a Masters' degree in Social Work). The Sisters had a tertiary qualification (e.g., a degree in nursing).

With the exception of two, none of the participants had specific qualifications in oncology nursing, only work experience. Most had between one and ten years experience in oncology, while a few had more than twenty years experience, with the highest being twenty-five years. A wide variety of other experiences such as paediatric care, heart diseases, working in an old age home, and being a matron were mentioned.

### Impact of environment and setting

We found that the daily tasks, as well as the institutional environment, have an impact on information-seeking behaviour and the need to use current awareness services. This includes the managing and financial environment, and the wider health care context. Although some of the patients treated by the two participatory groups overlap, there are considerable differences between the environment and settings in which the two groups (medical oncology centre and hospital wards) operate. Both are, however, affected by the South African health care situation, regulations and staff shortages, i.e., by the more comprehensive context. Kari and Savolainen ([2007](#kar07)) stress the importance of context in information seeking for personal development. These publications as well as the Ingwersen and J?rvelin ([2005](#ing05)) model referring to social, systemic, work task, conceptual and emotional, economic, techno-physical, social and historical contexts, as well as the work of Lamb, King and Kling ([2003](#lam03)) on organizational contexts, Menzies ([1993](#men93)) on the functioning of social systems as defence against anxiety and Strauss _et al._ ([1985](#str85)) on the social organization of medical work, should be used to guide data collection and interpretation in future research since these might help to explain the impact of anxiety, perceived roles and organizational dynamics on information needs and information usage that were noted. Suggestions are offered below (although these issues were noted, the paper will not, at this stage, pursue an in-depth analysis.).

#### South African health care setting

The impact of surrounding environments is widely accepted (e.g., [Courtright 2007](#cou07); [Wilson 1999a](#wil99a), [b](#wil99b); [Ingwersen and J?rvelin 2005](#ing05)). The importance of considering the impact of the working environment (e.g., the difference between the medical oncology centre setting and the oncology hospital wards), as well as the perceived difference between South African and some other international health care environments (e.g., the UK or USA) was also specifically noted by the head oncologist: in South Africa, oncology nurses''_responsibilities are much more_' and there is '_Very scant emphasis on??? academic development_'. (Randell _et al._ ([2009](#ran09)) note that nurses are expected to be more autonomous in the British National electronic Library for Health (NeLH).) It also seems from the exploratory study as if evidence-based nursing/medicine does not feature strongly in the South African context; this is an aspect that needs to be tested in further studies. The perception was also expressed that there is more emphasis overseas on patient research (at least in academic settings) than in South Africa. From findings mentioned in the following sections it also appears as if participants' perception of their role/function in the health care setting (e.g., receiving instructions and taking care of patients according to doctors' versus own input) can have an impact. In other settings in South Africa or in other countries it might, perhaps, be found that nurses have more responsibility in decision-making. From the survey it appeared that the nurses' own professional identity did not come through very strongly, but this does not necessarily imply that it does not exist. Unlike in the study reported by McCaughlin _et al._ ([2005](#mcc05)), they did not report the need for additional information in their decision-making, apart from what could be learned from the oncologists/doctors. According to them, nurses might have a perceived or actual lack of control over relevant factors.

The following briefly highlights some facets of the South African health care setting that may impact on the participants.

Pretoria , the setting for the empirical study, is one of the largest cities in South Africa with a number of oncology units and hospitals treating cancer patients. In Johannesburg (the largest South African city), about seventy kilometres from Pretoria, there are also a number of oncology units and treating hospitals.

Unfortunately the South African nursing situation is a concern for many: due to the long working hours and poor working conditions it is difficult to recruit staff and many young nurses are not staying in the profession for long: this is very similar to the situation reported by Menzies ([1993](#men93)). Nurses prefer to work overseas (e.g., in the UK, Ireland or USA) ([Pienaar 2006](#pie06)). According to a report by Louw ([2006](#lou06)), 42,000 nursing posts were vacant in contrast to the 41,563 posts filled.

At the time of the study, medical practices and hospitals experienced serious problems with regard to profit making from medication , due to new health legislation. This is still the case. Many had to cut staff and sometimes also had to settle for staff with lesser qualifications. Although inadequate funding is probably an international problem, it seems to have a serious impact in South Africa.

There are a number of cancer and oncology related societies in South Africa including The South African Oncology Nursing Society (no Website), The Oncology Social Work Association (no Website), [The Cancer Association of South Africa](http://www.cansa.org.za), and the [South African Oncology Consortium](http://www.saoc.org.za/index.php). Many participants were, however, not members.

More detail on the South African health infrastructure and regulations can be found at the [National Department of Health Website](http://www.doh.gov.za/).

The findings for the two settings (medical oncology centre and hospital wards) in terms of the environment are dealt with in the sections that follow.

#### Environment of the medical oncology centre and hospital wards

The findings for the medical oncology centre are depicted in [Table 3](#tab3) and the hospital wards in [Table 4](#tab4) in the Appendix. It would be useful for further studies to explore different types of hospital wards such as academic hospitals and other private and government hospitals. (Suggestions are offered in the [Recommendations](#rec), below.) The issues raised in Tables 3 and 4 also link well to findings by McKnight ([2007](#mck07)) for inadequate time and support for literature searching and models of information seeking stressing the importance of the environment (e.g., Wilson's 1981 and 1996 models ([Wilson 1999a](#wil99a), [b](#wil99b))). Although anxiety resulting from not coping with the job was noted, this cannot at this stage be interpreted in terms of the work of Menzies ([1991](#men91)) and Strauss _et al._ ([1985](#str85)). Isikhan, Comex and Danis ([2004](#isi04)) report on job stress and coping strategies in health care professionals working with cancer patients.

Menzies ([1993](#men93)) notes the following defensive techniques in a hospital environment confronted with anxiety:

*   Splitting up the nurse-patient relationship.
*   Depersonalization, categorization, and denial of the significance of the individual.
*   Detachment and denial of feelings.
*   Attempt to eliminate decisions by ritual task-performance.
*   Reducing the weight of responsibility in decision-making by checks and counter-checks.
*   Collusive social redistribution of responsibility and irresponsibility.
*   Purposeful obscurity in the formal distribution of responsibility.
*   Reduction of the impact of responsibility by delegation to superiors.
*   Idealization and underestimation of personal development possibilities.
*   Avoidance of change.

In a medical environment Strauss _et al._ ([1985:13](#str85)) distinguish divisions of labour such as safety work, machine work, comfort work, articulation work and especially sentimental work. It seems worthwhile to analyse future work according to such divisions.

#### Daily tasks and functions within the environment and setting

Information is mostly associated with effective decision-making, sense making, soothing anxieties experienced because of a lack of knowledge or a gap between what is known and what needs to be known, and problem-solving ([Dervin 1999](#der99); [Dervin and Nilan 1986](#der86); [Belkin _et al._ 1982a](#bel82a), [b](#bel82b); [Wilson _et al._ 2002](#wil02)). Bearing this in mind a number of research projects have focused on the importance of information in the context of work, for example Bystr?m ([2002](#bys02)) and Vakkari ([2003](#vak03)). Wessel _et al._ ([2006](#wes06)) report on the link between the tasks of clinical research coordinators and their information-seeking behaviour . According to Bystr?m ([2002](#bys02), [2005](#bys05)) there are tasks that are not information intensive and that are not related to problem solving. Different levels of information intensity can also be distinguished. The value and application of the task-based information seeking model is further explored by J?rvelin and Wilson ([2003](#jar03)). Li and Belkin ([2008](#liy08)) also report on a faceted approach to conceptualising tasks in information seeking, exploring categories distinguished by various authors. The tasks of the oncology nurses are also mentioned in Fourie and Claasen-Veldsman ([2007a](#fou07a)), but are here, following a style of qualitative analysis, supplemented with quotes from the respondents' discussion.

A list of the typical tasks which the participants identified is included in [Table 5](#tab5) in the Appendix. As part of the exploratory study on which we are reporting it does not seem to be worthwhile to distinguish between the two settings or the position of the participants in terms of seniority and the level of responsibility they take (partly because we worked with a small and diverse group and the intention of the exploratory survey was to identify the route for more in-depth studies as indicated in the recommendations). It is, however, noted elsewhere in the paper that there are (as expected) higher expectations for the Sister-in-charge (medical centre) and the hospital ward manager to be able to offer information and guidance on completing tasks. Although we found considerable overlap between the daily tasks of the two groups in terms of their routine nature, there seems to be a difference between the intensity of some of the tasks and especially the extent to which patients rely on the nursing staff for information and emotional support.

There was strong agreement on the _routine_ nature of their tasks and the fact that the nurses' main role concerns caring for patients, but without linking it to the need to use external sources to find information. No anxieties and stress to find information or information gaps were mentioned; if such occurred they rely on the oncologists and other doctors; only stress in taking good care of patients was mentioned:

*   '_try not to make mistakes_' (very busy)
*   '_at the end of the day you feel so guilty_' (because you could not pay enough attention to everything).
*   '_You are running around like a mad thing all the time_'
*   '_We are all so stressed with all the work we do_'.

Strauss _et al._ ([1985](#str85)) refer to sentimental work, and Menzies's ([1993](#men93)) defensive actions to deal with stress might be useful in further exploration. Respondents agreed that it is the responsibility of the oncologists to stay abreast of developments in patient treatment, and to ensure that they give nurses appropriate instructions. This is reflected in the following quotations from the interviews: '_Information can come from the doctor_' and'_I usually do what the doctors prescribe??? for the doctors it is important [to access information resources], but not for me_'. '_Not much is expected from them [the nurses] with regard to treatment_' (The latter reflects the oncologist's as well as hospital unit manager's point of view). In contrast to this, respondents indicated that the oncologists are very busy and that they do not actually have time to share information. In further studies it might be worthwhile to test the validity of the oncologists as ultimate information source for all types of oncology related information, against the possibility of oncology nurses learning something from their own literature to successfully fulfil their daily tasks and functions, i.e., taking some responsibility in supporting the doctors. The question also arises whether the literature the oncologists/doctors monitor concerns nursing practices, or as one would expect, patient treatment and cure of the disease. Bertulis and Cheeseborough ([2008](#ber08): 187) note that nursing information is seen as patient- care centred and holistic in contrast to the medical and scientific information required by doctors, while Thain and Wales ([2005](#tha05): 133) found that searches by health care professionals were more related to patient care.

The well-being of patients seems to be at the centre of their work: '_I am a nurse and will always stay a nurse_' (Sister) and'_For me it is about the person - the patient_' and'_We do not have enough time for the emotional care of patients_'. ' _I think one actually shares a lot with these patients??? because for them it becomes like a second home_ '. ' _The reason why you studied nursing science was at the end of the day to get to the patients_'. Being informed of developments and practices concerning patient education and emotional well-being might therefore help in at least patient education and counselling, and dealing with patients' questions based on Internet searches. Quotations from respondents also led us to the belief that a model for information behaviour in this particular health care sector should more strongly reflect affective (the need to emotionally care for patients) or affective-cognitive information needs (e.g., information on methods to deal with stress and anxiety) than purely cognitive information needs (e.g., the best drug to use for chemotherapy or dealing with nausea). The idea of emotional context is also mentioned by Ingwersen and J?rvelin ([2005](#ing05)) and needs to be further explored. A recent literature review showed that emotion features very scantly in studies on information behaviour reported in health care contexts ([Fourie 2009](#fou09)).

When asked about the importance of the latest information we were told by a Sister: '_It will be nice to be oriented in such a way, but it is not essential_'. Another explained: ' _For me it is not that important??? for the doctors it is very important. I usually do what the doctors prescribe_'. It therefore seems as if the quality of the nurses' task completion does not depend on information for problem-solving or decision-making and as if their tasks are not information intensive: they can adequately take care of patients without searching for information. Nicholas _et al._ ([2005](#nic05): 40) also note: '_It was apparent that information seeking did not form part of the culture of the job, except with regard to training_'.

The Sister-in-charge (medical centre) who takes more responsibility in supporting other staff members with information and who is also involved in conference presentations and research is the only one who occasionally uses the Internet, medical databases and tables of content as part of her job. The head oncologist also expects her to put in more effort to keep track of new developments. In a study with Bakker at the Central Cancer Library of the Netherlands Cancer Institute (still to be published), a respondent remarked that although some of them are very keen on keeping up with new developments, there are also some staff who do not even bother to look at the national professional journal.

Nursing staff from the medical oncology centre seem to take on more doctor-related responsibilities than the typical nursing activities that apply in the ward. They need to handle as many patient telephone queries as possible, before directing patients to the oncologists: '_she_ (a Sister) _is the first step in the system??? many of the things I do not even know about??? only hear afterwards that this and this have happened??? that the problem has been sorted out_'.

Patient care is, however, not just about following instructions on treatment; there are many issues involved such as guidance, preparation, education and, especially, emotional care and support, as has been pointed out in the list of daily tasks and functions in Table 4: ' _the questions people ask us, are often more emotional_ '.

Although it was not specifically investigated by the exploratory study, it seems as if information needs can also be influenced by staff members' positions (e.g., higher expectations for the Sister-in-charge and ward unit manager to be well-informed). This is an aspect that needs to be addressed in further studies.

Both groups mentioned that there is an increase in the number of patients finding information on the Internet: they ask more questions and have more sophisticated information needs. The nurses are concerned about this trend since they question the quality of the information the patients find on the Internet: '_The patients know more about the new things than me_ '. '_Patients very well informed about new things??? it makes things difficult_'. It was also mentioned that information on the Internet is not filtered: '_For some or other reason patients have more access to rubbish than access to things that are useful_'. This also leads to feelings of inadequacy: ' _One feels stupid if patients know more than you do because they went onto the Internet_ '. '_On quality it can become an issue, when someone asks you something and there stands the nurse and 'Er??? um'_ . It therefore seems as if the need for patient information may be a good rationale for the use of current awareness services in an environment which appears to be mostly not information intensive. It might, for example, be very useful if nursing staff can identify and refer patients to appropriate information available on the Internet.

From experiences with this exploratory project (as will also become clear in the following sections) it seems as if there is a need to consider the use of current awareness services not only for factual information, for example on patient care and new job opportunities, and problem solving, but also for the affective dimensions of information, for example on joy, comfort, job satisfaction and inspiration. (McKnight ([2007](#mck07)) also reported that the health care professionals cared deeply for their patients.) This seems important in professions such as oncology nursing, where not only physical care according to doctors' instructions, but especially emotional care features very strongly. Strauss _et al._ ([1985](#str85)) refer to sentimental work, while such care and emotions, according to Menzies ([1993](#men93)), lead to defensive actions, which might again be the cause of avoiding information. Considering the potential value of current awareness services for oncology nurses, the question arises whether research as reported in this article should merely note reported information behaviour, or whether expected changes in the future should also be considered. For example, the oncologist noted there is not a culture of information use: the nurses rely on their existing knowledge base and the oncologists. Somewhere in the future, this will probably prove inadequate. Bertulis and Cheeseborough ([2008:186](#ber08)) noted that workplace culture (affecting information seeking) is as important as information technology.

Considering the work by Bystr?m ([2002](#bys02), [2005](#bys05)) and Vakkari ([2003](#vak03)) on task-based information seeking and the importance of considering the complexity and information intensity of tasks and functions, it is suggested that future collection of data (e.g., with the view to recommend current awareness services and Websites) should work from a preliminary categorisation of functions/daily tasks. Such a categorisation should also acknowledge categorisations from oncology nursing literature. With hindsight it seems as if the work by Menzies ([1993](#men93)) and Strauss _et al._ ([1985](#str85)) on the social organization of medical work and the defences in dealing with anxiety should also be noted, and used as bases for future studies.

Considering the models of, for example, Wilson ([1999a](#wil99a), [1999b](#wil99b)), Ellis ([1989](#ell89)), Kuhlthau ([1991](#kuh91), [1993](#kuh93)), Dervin ([1999](#der99)), and Belkin _et al._ ([1982](#bel82a)) there is no evidence that the nurses are strongly aware of needs for information, gaps in their knowledge base, etc. that cannot be filled by the oncologists. There was also no mention of anxiety or uncertainty because of inadequate information. Considering the defensive actions by Menzies ([1993](#men93)), it was noted through personal observation that the nurses do not quite have the opportunity for splitting up the nurse-patient relationship (especially in the wards), but there seems to be a trend to reduce the impact of responsibility by delegation to superiors. To test whether the other defensive actions mentioned by Menzies ([1993](#men93)) apply and impact on information seeking, a follow-up study would be required. Considering the work division of Strauss _et al._ ([1985](#str85)) it seems as if the sentimental work category features strongly.

### Impact of daily tasks and functions within the environment and setting

Apart from the routine nature of most tasks, participants especially stressed that their work is very tiring and emotionally demanding. More patients are, for example, dying than in the general hospital wards, which would, according to Menzies ([1993](#men93)), impact on defensive actions. They spend a lot of time on their feet, and, the hospital staff especially, work very long shifts (i.e., more than twelve hours per shift). All complained about numerous interruptions and the lack of privacy, which is seen as an inhibiting factor for effective information seeking. Due to staff and financial shortages, the hospital staff especially finds it difficult to participate in opportunities for continuing education, and to balance these with their responsibilities in their personal lives: '_Difficult to attend courses??? difficult to get out of the wards??? do not want to come in on my day off_'. Apart from the fact that their daily tasks do not require them to search for information, they do not have the time and energy for information searches:

*   ' _The hours we are working??? sometimes we are so tired_ '
*   '_It is your training time and research time in comparison with clinical work time_'.
*   '_Try not to make mistakes???_ (very busy)'
*   '_At the end of the day you feel so guilty_' (because they could not pay enough attention to everything)
*   '_You are running around like a mad thing all the time_'
*   '_We are all so stressed with all the work we do_'.

Similar concerns and the lack of time for information searches were also noted by McKnight ([2007](#mck07)).

Current awareness services appropriately planned and tailor-made according to individual preferences can, however, be a great time saver, as we will explain in our recommendations. However, because most participants were not familiar with the concept of current awareness services, this is something that they do not realise, and that needs to be demonstrated. When promoting the use of current awareness services, it seems as if there should also be a strong consideration of the demands of people's jobs. Current awareness services pushing information to nurses is also supported by Randell _et al._ ([2009](#ran09)).

In spite of opinions that they do not need to seek additional information to complete their daily tasks, some remarks pointed to an interest in information, e.g.

*   Information is necessary for patients and even children: '_My boy, you need to take charge of your illness_'.
*   '_You do learn new things??? and you do learn from other people's experiences_' (from attending meetings).
*   '_Very enthusiastic (about information)??? feel as if you are totally isolated??? otherwise your job becomes??? boring_'.
*   '_If we attend something - see what is going on - very interesting_'.

### Influences and barriers to the use of information and Internet current awareness services

We questioned management as well as the nursing staff on their perceptions of influences and barriers to the use of information and Internet current awareness services. These should be read against the impact of the environment and daily tasks as already discussed.

#### Medical oncology centre

Staff from the medical oncology centre felt that they did not have enough time and private access to the Internet to use current awareness services. Due to the routine nature of their daily tasks, there appears to be no real need for them to look for additional information. Most of them lacked computer skills as well as skills in using the Internet (this seems to be a major barrier). Nicholas _et al._ ([2005](#nic05): 39) also noted poor access to computer facilities and not enough networked terminals, and perceived low levels of Internet literacy and awareness.

The Sister-in-charge, however, felt confident about her skills. She is also the only one who is currently using some current awareness services (e.g., tables of contents to which the oncologists subscribe and occasional database searches when necessary). She also regularly presented papers at conferences.

From management's point of view, the centre staff did not require additional information to complete their tasks. It was also mentioned that they might not always see the value of information for their tasks, and that many lack personal motivation or reasons for spending time on information seeking: '_depend on individual interest??? for many just a job??? not an academic approach_' ;'_milieu is not there to work in such a way_' and '_you must have the opportunity as well as the reason to use it_'. Bertulis and Cheeseborough ([2008](#ber08): 187) found a relationship between employer attitudes to evidence-based practice and the ability to access electronic resources at work. They also noted that the level of encouragement by organizations and perceptions of whether they have time for information seeking is important.

#### Hospital wards

The hospital staff claimed lack of time, the physical and emotionally demanding nature of their jobs, inadequate funding and access to the technology, as well as the lack of opportunities as barriers to the use of information and current awareness services. They especially lack computer skills and skills in using the Internet, and the opportunity to use the Internet. The unit manager thought they could perhaps '_create a little corner???where those who are really interested??? perhaps in their time off_ ' (referring to lunch breaks) could use the Internet. It was also mentioned that the nursing staff lack knowledge of oncology issues, and that training opportunities are limited: '_Not really us who get the opportunity to do it_'

Although they were all aware of the Internet, most do not know how to use it. One nurse said about the Internet: '_I just heard about it_'. Only one participant has received formal training in information searching as part of her university studies. These barriers were also raised from the hospital management's perspective.

One nurse indicated that she has access to a laptop at home, but that she does not really know how to use it and, therefore, does not use it for e-mail or access to the Internet. A link between computer skills and an inclination to use electronic information sources is also reported by Secco _et al._ ([2006](#sec06)).

One of the very few who feel confident about her search skills said. '_Oh no, I'll find it??? I'll find it??? no matter??? I'll find it??? OK??? I'll get it_'. Most felt less confident: ' _If I try to search something, I battle??? I am scared that I will waste time and money, because I do not actually know how to search_ '. Also: '_If somebody asks me something, I will ensure that he gets the right information, but I will not spend the whole day searching for it_'.

Tannery _et al._ ([2007](#tan07)) and McKnight ([2007](#mck07)) noted similar barriers of lack of time, lack of access to information resources and lack of skills. The possible impact of personal characteristics and attitudes, adoption of innovation, and lack of professional curiosity are also mentioned by them.

From the study it was also clear that nurses cannot afford to stay abreast of their subject field and enrol for events of continuing education if not supported financially as well as in terms of time allowed.

### Need for information and the need to use current awareness services

As stressed before, both the nurses and management feel that information is not essential to successfully complete their daily tasks. In cases where they need to know something they consult with the oncologists, Sister-in-charge, ward unit manager or other senior staff members. Staff from the medical oncology unit are expected to at least be aware of what is reported at South African conferences.

There was general agreement that the oncologists are very up-to-date, and that they make every effort in this regard. They are, however, considered to be less successful in sharing information with the nurses. Due to the fact that they try to keep up with developments, the oncologists are also well aware of the negative effects. One respondent, however, stated'_I think they do it (sharing information) quite well_'. With regard to book announcements it was remarked: '_people force information down from a company's perspective_'. They can only consider'_what you have time for??? 90% I never look at_'. One participant mentioned interest in medication: '_Maybe about the research they have done_' (she was planning to enrol for a qualification in oncology nursing). Tannery _et al._ ([2007](#tan07): 18) mention the following as a barrier: '_Did they perceive cues in the environment that discouraged the use of these resources, such as disparagement from supervisors or a perception that consulting the professional literature during work hours is wasting time?_' This issue was not explicitly stated in this study.

On the value of current awareness services and the information obtained, the following views were expressed:

*   '_Even if you do not use it, it is something you can store??? and you can remember it_'.
*   '_If you have the latest information, you can say to a doctor, I have read something, what do you think about it?_'.
*   '_If you understand how other cancer wards (at other hospitals) operate, it gives you insight in the problems of your own ward_'.
*   '_I like to watch the news_'.
*   '_I like new information. I am always curious_'.

In spite of the barriers that were raised, these statements leave the impression that at least some of the participants could see the benefit of keeping track with information. From a management perspective it was explained: '_Since there are so many changes in oncology one must really stay abreast of all the developments??? I think we have a large gap for this??? working under so much pressure'_'_it is as if we get stuck on the same type of stuff_'.

Participants were mostly unaware of current awareness services, and therefore also of how this can be used in their jobs. This might be in contrast to other settings. A study in 2009 in a different South African context showed more interest in the use of information and current awareness services, while the previously mentioned study in The Netherlands noted that not all staff are keen on using information or current awareness services.

In South Africa all nurses have to be registered with the [South African Nursing Council](http://www.sanc.co.za/). Some of the nurses are also members of [The Democratic Nursing Organisation of South Africa (Denosa)](http://www.denosa.org.za). Very few participants were, however, members of national professional societies (e.g., The South African Oncology Nursing Society and The Oncology Social Work Association). They had hardly any knowledge of the international societies or the information that can be gained from these societies' Websites (e.g., newsletters, conference announcements). One also said that she did not ' _know where you can buy new books_' while others indicated that they are only aware of books brought to their attention by Denosa. None of them used discussion lists as a means of keeping up. The oncologist felt that such information is often sufficiently covered by formal sources. Even some of those with their own access to the Internet and computer skills did not often use it for information seeking: '_I am scared I will waste time and money_'.

Although the medical centre subscribes to professional journals, these are not necessarily aimed at oncology nurses. The hospital staff had no access to the professional journal literature or tables of contents. Both groups indicated that they take note of conference announcements and opportunities for continuing education (mostly locally). (We did not attempt to determine whether they noted all important/key events.) None of the respondents was using discussion lists, RSS feeds, online tables of contents and tables of contents services, Weblogs, Web monitoring services, news alerting services or Web portals. Except for the Sister-in-charge (medical centre), most do not have the skills to use these. They are also unaware of the variety of current awareness services available for _free_ (e.g., as identified by [Fourie & Claasen-Veldsman 2005](#fou05), [2007b](#fou07b)), or how this can help them to actually save time and improve the quality of patient care. Although there seems to be a serious lack of computer skills, somebody mentioned that management expects them to read the protocols (for administration of medication) on the computer. They should not print it out.

A few participants mentioned that they sometimes receive useful information from patients, most often such information is, however, considered to be unreliable due to the poor reputation of Internet information sources: '_Tons of information available from the Internet - they don't know what to do with it??? Patients argue with doctors about information they found on the Internet??? they do not believe the doctors_'. In an earlier study, Fourie and Claasen-Veldsman ([2005](#fou05), [2007b](#fou07b)) identified some very useful resources that are aimed at patients. By using current awareness services nurses can, for example, notice such resources and bring them to the attention of patients.

Other preferred sources of information are colleagues, reference works, conferences and workshops. The last two are especially highly valued as means of continuing education and keeping up with developments. The oncologist also confirmed that excellent formal training opportunities are presented at conferences. Although there seems to be trust in relying on colleagues for correct answers, it was also confirmed that there is not a strong culture in sharing and disseminating new information: senior staff members who are more exposed to new information just do not have time for this. '_I am willing to share information as long as they do not pick my brain_'. '_I almost do not get the opportunity, because there is not time??? to give feedback to the staff_'. Earlier work by Sonnenwald ([1999](#son99)) on horizons would be useful to further explore their use of information resources with a strong reliance on doctors and colleagues (mostly senior).

### Interest in learning more about current awareness services

In spite of the fact that there is no obvious need for information to complete their daily tasks, and the barriers we have noted, many participants indicated that they would be interested in learning about current awareness services, and especially in learning computer and Internet search skills. Staff members who are ambitious to further their careers, seemed especially interested in using current awareness services, while staff members who are very close to retiring age felt that they would rather focus on'_being a nurse - caring for patients_' and using their personal time for their families. When asked about attending a workshop we heard:

*   '_workshop definitive_'
*   '_whatever is needed ?^' 8 hours_' (with regard to the time she would be prepared to spend at a workshop)
*   '_would love to learn about the Internet_'.
*   '_I would love to_'
*   '_I will really enjoy it'_

Others indicated less time, such as four or six hours, for attending a workshop, and some also pointed out the practical realities such as it would'_depend on day_', and their shift. '_Maybe in a month you can go twice_' (two hours at a time).

About using their personal time for current awareness services one respondent remarked: '_Must get away from the situation_', and about access: '_outside the job/work situation there are many Internet cafes_'.

Although staff are not expected to use current awareness services, management from both groups indicated that they are interested in exploring further the possibilities current awareness services could offer to their staff and that they would support staff in attending a workshop in using current awareness services. For most participants, a workshop in computer skills would, however, be a pre-requisite. (Their interest in learning information technology skills could be further explored, for example, by considering educational literature and literature on adult learning. It was mentioned that if they do not develop a culture of using current awareness services, staff will continue to rely on'_a past-base_' (i.e., based on knowledge gained in the past) which at some point in the future will no longer be adequate. We '_need to create a milieu for training, research??? in comparison with clinical work time_'.

With regard to support from the hospital management: '_I think they can improve considerably on the support_'. There are, however, opportunities for personal development in the hospital, for example for computer literacy training. '_They must create an opportunity, or give a reason, why not_'.

Overall the participants were interested in receiving information on medical and drug related issues, job and training opportunities as well as information for personal use. For example, they would like to use the Internet to help their children with their homework, and to impress their children. Putting in the extra time to learn search skills might thus have a double benefit. In spite of their interest, participants were not quite sure how they would use it, or how they would find the time: as mentioned before, they did not seem to realise that current awareness services that are based on push technology can actually save time in contrast to one-off information searching. Therefore, we will interpret their interest as _conditional interest_: the use of current awareness services and the training should be streamlined and tailor-made to meet their needs and circumstances.

In addition to what has been mentioned, the work by Vachon ([1998](#vac98)) on personal coping strategies in palliative oncology settings, which explores the need to find pleasure in one's professional role and experiencing a feeling of accomplishment, should also be addressed in further exploration, as well as the development of interventions. 'Association in a professional interest group may be particularly helpful to caregivers who do not receive the validation they need in their particular work setting' ([Vachon 1998:154](#vac98)). This should also be aligned with the work of Menzies ([1993](#men93)) on the functioning of social systems as a defence against anxiety, and research following on her work, e.g., Van der Walt and Swartz ([2002](#van02)) on task oriented nursing in a tuberculosis centred programme in South Africa.

### Contextualising the findings: a soft systems analysis

Stokes and Lewin ([2004](#sto04)) used soft systems analysis to depict the information seeking behaviour of nurse teachers that is neither precise nor predictable, as a rich picture. This followed the work of Checkland ([1999](#che99)). Figure 1 presents an attempt to apply this to this study with the intention to show the potential for in-depth application for future studies and for building a theoretical model in future, adding insights gained from other studies.

<div style="text-align:center">![A soft systems approach to the complexities influencing the need for current awareness services in a South African oncology nursing context](p484fig1.png)</div>

<div style="text-align:center">  
**Figure 1: A soft systems approach to the complexities influencing the need for current awareness services in a South African oncology nursing context**</div>

### Strengths and weaknesses of the research setting

[Table 6](#tab6) (in the Appendix) depicts the potential of force field analysis to identify the strengths and weaknesses of the research setting towards the use of current awareness services. Force field analysis is a technique for implementing changes in structure, technology and people, and can identify three sets of organization field conditions, namely facilitating, constraining and blocking. The technique was developed by Kurt Lewin in 1951 and since then has had many applications, e.g., Ajimal ([1985](#aji85)), Thomas ([1985](#tho85)), Baulcomb ([2003](#bau03)) and Cronshaw and McCulloch ([2008](#cro08)). In this paper it will only be presented to show the potential of the technique for future studies.

## Limitations of the study

The study reported here involved only a small number of oncology nurses in a specific setting. It might be that findings in some regards might differ when considering other settings. Considering the fact that neither of the South African professional societies / organizations for oncology nurses and social workers had a Website at the time of writing, it seems safe to assume that much needs to be done to promote the use of current awareness services available via the Internet by oncology nurses in South Africa. Apart from depicting the findings using a soft systems approach, the limited scope of the study does not quite lend itself to suggesting a model of information behaviour regarding current awareness services at this stage. Recommendations to move forward in this regard are, however, offered.

## Recommendations

Some practical recommendations are mentioned by Fourie and Claasen-Veldsman ([2007a](#fou07a)). The intention of this article, following a more in-depth analysis was to note the methods, theories and models to explore in future studies.

We acknowledge the impact of the environment and daily tasks on the oncology nurses' information needs, and the fact that many tasks are not information intensive. In the light of the changing demands of society and the increased use of the Internet by patients, we however feel that we need to further explore the potential of current awareness services and information dissemination and move beyond the barriers that have been mentioned. A soft systems analysis, force field analysis and Roger's theory of diffusion seem useful to deepen understanding of the context and promote a culture of using current awareness services. In addition, anxiety to give patients the best possible care, as well as a strong shift towards the oncologists and other doctors were noted in the responsibility for decision-making and staying abreast of developments. Future studies may thus benefit from working from the defensive actions against anxiety as identified by Menzies ([1993](#men93)) and the division of labour identified by Strauss _et al._ ([1985](#str85)).

Following the force field analysis depicted in [Table 6](#tab6) (Appendix), we recommend that the following should be in place in order to promote the use of Internet current awareness services as information resources for oncology nurses:

1.  Sufficient access to information technology, in an environment conducive to information seeking, allowing for private and uninterrupted time (even if only for short sessions). Although personal desktop access to the Internet is the ideal situation, a dedicated computer in a separate office/corner would also suffice.
2.  Skills training (including computer and Internet search skills) are of utmost importance. Without the necessary skills, it is impossible to use the available infrastructure. Such skills training should, however, be embedded in context (which needs to be further explored).
3.  Management support for the introduction of a culture of using current awareness services.
4.  A supportive current awareness service infrastructure (e.g., a portal) that can be personalized according to the needs of individual nurses. Such a portal could be developed by library and information professionals, based on the specific information needs of oncology nurses.
5.  Encouragement for collaboration and i nformation sharing amongst colleagues with consideration of findings from further research on the context of oncology nurses and especially their task environment.

For further research we recommend the following:

1.  An information audit of the information resources monitored by oncologists (in a specific setting) and the relevancy of the information for the nursing staff (in the same setting).
2.  A content analysis of information resources targeted at oncology nurses and how this can support them in their daily tasks and priorities.
3.  Exploration of technological time-saving mechanisms to improve the efficiency and effectiveness of current awareness services and information dissemination.
4.  Exploration of a variety of theories in information seeking behaviour that seems to hold potential for the oncology arena, namely:
    *   Miller's monitoring and blunting theory that concerns the use of information to deal with stressful situations (this can apply to nurses and other health care workers as well as patients) ([Baker 2005](#bak05)).
    *   Dealing with anxiety and the dynamics of health care organizational environments (e.g., the work of Menzies ([1993](#men93)) and Strauss _et al._ ([1985](#str85))).
    *   Network gatekeeping theory that can be explored for quality control with regard to the sources used by patients ( [Barzilai-Nahon 2005](#bar05)).
    *   Development of communities of practice in oncology nursing ([Davies 2005](#dav05)).
    *   Exploration of the concept of information grounds to explore a role for oncology nurses in supporting the information needs of cancer patients ([Fisher 2005](#fis05b)).
5.  Exploring misattributions that occur across staff levels in this type of setting (e.g., management and nurses both believed they did not need information, although the nurses indicated that they need information from the oncologists and have difficulty obtaining it because the doctors do not communicate it when needed), as well as what can be learned from other sectors experiencing similar misattributions such as reported in the organizational management literature.
6.  Exploring information horizons (as suggested by Sonnenwald ([1999](#son99))) and information source horizons (as explained by Savolainen & Kari ([2004](#sav04))) based on the respondents' reported tasks, tasks reported in the oncology nursing subject literature and results from further studies. This could include encouragement to recognise themselves as information resources.
7.  Exploring the opportunity to raise research awareness and creating a practice culture in which there are high motivation and incentives for change ([Gannon-Leary _et al._ 2006:252](#gan06)).
8.  Following the application of a soft systems approach above, it is suggested that a model is developed for studying the information needs of oncology nurses. It seems worthwhile to develop such a model from work focusing on the context such as the organization and wider environment, e.g., the work by Savolainen and Kari ([2004](#sav04)), Sonnenwald ([1999](#son99)), Ingwersen and J?rvelin ([2004](#ing04)), Menzies ([1993](#men93)), Strauss _et al._ ([1985](#str85)), Lamb, King and Kling ([2003](#lam03)) and Rogers ([2003](#rog03)), as well as empirical studies in the specific field.

## Conclusion

Although the sample group of oncology nurses for the exploratory study had hardly any knowledge of Internet current awareness services, and although there are many barriers to information seeking, it seems as if there is an interest in promoting a culture of using current awareness services. In order to develop an appropriate current awareness service infrastructure and training opportunities and other interventions tailor-made for the needs of oncology nurses, we need to deepen our understanding of the information behaviour of oncology nurses and their context. This implies that we take note of the information seeking behaviour of other health care professionals as well as cancer patients, and that we expand our survey to accommodate national and international comparative studies.

To gain real depth in our understanding we intend to develop a preliminary th eoretical model for the use of current awareness services by using the results from the exploratory study, re-interpretation of the results against a selection of existing models (e.g., Lamb, King and Kling's social informatics and Rogers' innovation and diffusion model), as well as an extended literature survey to identify other ways of looking at information behaviour and exploring context. Such a model can then be tested.

## Acknowledgements

Our thanks to the anonymous reviewers for bringing the publications of Menzies and Strauss to our attentionand to to Mrs Joukje Geertsema for her help with the conversion to HTML format.

## About the authors

**Ina Fourie** is a full professor in the Department of Information Science, University of Pretoria, South Africa. She received her Bachelor's degree in Library and Information Science, her Honours and Masters degree in Library and Information Science from the University of the Orange Free State University, Bloemfontein (South Africa) her D.Litt et Phil from the Rand Afrikaans University, Johannesburg (South Africa), and a Post-graduate Diploma in Tertiary Education from the University of South Africa (Pretoria). She can be contacted at [ina.fourie@up.ac.za](mailto:ina.fourie@up.ac.za)  
Retha Claasen-Veldsman was a junior lecturer in the Department of Information Science, University of Pretoria, South Africa at the time of writing. She obtained her qualifications, a BA Information Science, BA (Honours) Information Science and an MA in Development Communication from the University of Pretoria. She currently work for GEM-Science as public participation practitioner. She can be contacted at [info@gemscience.co.za](mailto:info@gemscience.co.za)

#### References

*   Ajimal, K.S. (1985). Force field analysis - a framework for strategic thinking. _Long Range Planning_, **18**(5), 55-60.
*   Alper, B.S., Hand, J.A., Elliot, S.G., Kinkade, S., Hauan, M.J., Onion, D.K. _et al._ (2004). How much effort is needed to keep up with the literature relevant for primary care? _Journal of the Medical Library Association_, **92**(4), 429-437.
*   Andreassen, S., Randers, I., N?slund, E., Stockfeld, D. & Mattiasson, A.C. (2005). Family members' experiences, information needs and information seeking in relation to living with oesophageal cancer.  _European Journal of Cancer Care,_ **14**(5), 426-434.
*   Andrews, J.E., Pearce, K.A., Ireson, C. & Love, M.M. (2005). Information-seeking behaviors of practitioners in a primary care practice-based research network (PBRN). _Journal of the Medical Library Association_, **93**(2), 206-212.
*   Anthony, D.M. (2000). An international comparison of computer networks' use and potential use. _Health Informatics Journal_, **6**(1), 15-22.
*   Baker, L.M. (2005). Monitoring and blunting. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 239-241). Medford, NJ: Information Today (ASIST monograph series).
*   Barta, K.M. (1995). Information-seeking, research utilization, and barriers to research utilization of pediatric nurse educators. _Journal of Professional Nursing_, **11**(1), 49-57.
*   Barzilai-Nahon, K. (2005). Network gatekeeping. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 247-253). Medford, NJ: Information Today (ASIST monograph series).
*   Baulcomb, J.S. (2003). Management of change through force field analysis. _Journal of Nursing Management_, **11**(4), 275-280.
*   Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982a). ASK for information retrieval: part I. _Journal of Documentation_, **38**(2), 61-71.
*   Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982b). ASK for information retrieval: part II. _Journal of Documentation_, **38**(3), 575-591.
*   Bertulis, R. & Cheeseborough, J. (2008). The Royal College of Nursing's information needs survey of nurses and health professionals. _Health Information and Libraries Journal_, **25**(3), 186-197.
*   Bystr?m, K. (2002). Information and information sources in tasks of varying complexity. _Journal of the American Society for Information Science and Technology_, **53**(7), 581-591.
*   Bystr?m, K. (2005). Information activities in work tasks. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 174-178). Medford, NJ: Information Today (ASIST monograph series).
*   Case, D.O. (2006). _Looking for information: a survey of research on information seeking, needs, and behavior_. Amsterdam: Academic Press.
*   Checkland, P. (1999). _Systems thinking, systems practice: includes a 30-year retrospective_. Chichester, UK: John Wiley.
*   Cobb, S.C. & Baird, S.B. (1999). Oncology nurses' use of the Internet for continuing education: a survey of Oncology Nursing Society congress attendees. _Journal of Continuing Education in Nursing_, **30**(5), 199-202.
*   Cogdill, K.W. (2003). Information needs and information seeking in primary care: a study of nurse practitioners. _Journal of the Medical Library Association_, **91**(2), 203-215.
*   Courtright, C. (2007). Context in behavior research. _Annual Review of Information Science and Technology_, **41**, 273-306.
*   Cronshaw, S.F. & McCulloch, A.N.A. (2008). Reinstating the Lewinian vision: from force field analysis to organization field assessment. _Organization Development Journal_, **26**(4), 89-103.
*   Cullen, R.J. (2002). In search of evidence: family practitioner's use of the Internet for clinical information. _Journal of the Medical Library Association_, **90**(4), 370-379.
*   Davies, E. (2005). Communities of practice. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 104-107). Medford, NJ: Information Today (ASIST monograph series).
*   Dee, C. & Stanley, E.E. (2005). Information-seeking behavior of nursing students and clinical nurses: implications for health sciences librarians. _Journal of the Medical Library Association_, **93**(2), 213-222.
*   Dervin, B. (1999). On studying information seeking methodologically: the implications of connecting metatheory to method. _Information Processing and Management,_ **34**(6), 727-750.
*   Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33.
*   Dickerson, S.S., Boehmke, M., Ogle, C. & Brown, J.K. (2006). Seeking and managing hope: patients' experiences using the Internet for cancer care. _Oncology Nursing Forum_, **33**(1), E8-E17.
*   Echlin, K.N. & Rees, C.E. (2002). Information needs and information-seeking behaviors of men with prostate cancer and their partners: a review of the literature. _Cancer Nursing_, **25**(1), 35-41.
*   Ellis, D. (1989). A behavioural approach to information retrieval design. _Journal of Documentation_, **46**(3), 318-338.
*   Ellis, D. & Haughan, M. (1997). Modeling the information seeking patterns of engineers and research scientists in the industrial environment. _Journal of Documentation_, **53**(4), 384-403.
*   Estabrooks, C.A., O'Leary, K.A., Ricker, K.L. & Humphrey, C.K. (2003). The Internet and access to evidence: how are nurses positioned? _Journal of Advanced Nursing_, **42**(1), 73-81.
*   Fisher, K., Erdelez, S. & McKechnie, L. (Eds.). (2005). _Theories of information behavior_. Medford, NJ: Information Today. (ASIST monograph series).
*   Fisher, K. (2005). Information grounds. In K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 185-190). Medford, NJ: Information Today. (ASIST monograph series).
*   Fitch, M.I., Chart, P. & Parry, N. (1998). Information needs of nurses regarding breast disease. _Canadian Oncology Nursing Journal_, **8**(3), 176-182.
*   Fourie, I. (1999). Empowering users - current awareness on the Internet. _The Electronic Library_, **17**(6), 379-388.
*   Fourie, I. (2002). A review of web information-seeking/searching studies (2000 - 2002): implications for research in the South African context. In T. Bothma & A. Kaniki, (Eds.), _Progress in Library and Information Science in Southern Africa: proceedings of the second biennial DISSAnet Conference_ (PROLISSA conference, 24-25 October, Pretoria) (pp. 49-75). . Pretoria: Infuse.
*   Fourie, I. (2009). Learning from research on the information behaviour of health care professionals: a review of the literature 2004-2008 with a focus on emotion. _Health Information and Libraries Journal._ **26**(3), 171-186.
*   Fourie, I. & Claasen-Veldsman, R. (2005). Disintermediation: using intermediary skills to offer oncology nurses opportunities for their own World-Wide Web current awareness services. _Mousaion_, **23**(2), 196-212. 
*   Fourie, I. & Claasen-Veldsman, R. A. (2007a).  South African perspective on oncology nurses' need for current awareness services (CAS) via the WWW. _Mousaion_, **25**(1), 44-66.
*   Fourie, I. & Claasen-Veldsman, R. (2007b). WWW current awareness services for oncology nurses: a theoretical framework. _The Electronic Library_, **25**(8), 36-53.
*   French, B. (1998). Developing skills required for evidence-based practice. _Nurse Education Today_, **18**(1), 46-51.
*   French, B. (2006). Uncertainty and information need in nursing. _Nurse Education Today_, **26**(3), 245-252.
*   Gannon-Leary, P. _et al._ (2006). Use of evidence by nursing students: an interdisciplinary study. _Library & Information Science Research_, **28**(2), 249-264.
*   Gorman, G.E. & Clayton, P. (2005). _Qualitative research for the information professional: a practical handbook_. London: Facet Publishing.
*   Gregg, A.L. & Wozar, J.A. (2003). Delivering Internet health resources to an underserved health care profession: school nurses. _Journal of the Medical Library Association_, **91**(4), 398-403.
*   Hartigan, K. (2003). Patient education:  the cornerstone of successful oral chemotherapy treatment. _Clinical Journal of Oncology Nursing_, **7**(6), 21-24.
*   Hughes, L.C., Hodgson, N.A., Muller, P., Robinson, L.A. & McCorkle, R. (2000). Information needs of elderly postsurgical cancer patients during the transition from hospital to home. _Journal of Nursing Scholarship_, **32**(1), 25-30. 
*   Ingwersen, P. & J?rvelin, K. (2005). _The turn: integration of information seeking and retrieval in context_. Dordrecht: Springer.
*   Isikhan, V., Comez, T. & Danis, M.Z. (2004). Job stress and coping strategies in health care professionals working with cancer patients. _European Journal of Oncology Nursing_, **8**(3), 193-285.
*   J?rvelin, K. & Wilson, T.D. (2003). [On conceptual models for information seeking and retrieval research](http://www.webcitation.org/60n8jdPEQ). _Information Research_, **9**(1), paper 163\. Retrieved 8 August, 201 from http://InformationR.net/ir/9-1/paper163.html (Archived by WebCite? at http://www.webcitation.org/60n8jdPEQ)
*   Jenkins, J. (1997). Educational issues related to cancer genetics. _Seminars in Oncology Nursing_, **13**(2), 141-144. 
*   Jimbo, M., Nease (Jr.), D.E., Ruffin, M.T. & Rana, G.K. (2006). [Information technology and cancer prevention.](http://www.webcitation.org/60n93uWVs) _CA: a Cancer Journal for Clinicians_, **56**(1), 26-35\. Retrieved 8 August, 2011 from http://onlinelibrary.wiley.com/doi/10.3322/canjclin.56.1.26/full (Archived by WebCite? at http://www.webcitation.org/60n93uWVs)
*   Kari, J. & Savolainen, R. (2007). Relationships between information seeking and context: a qualitative study of Internet searching and the goals of personal development. _Library & Information Science Research_, **29**(1), 47-69.
*   Kuhlthau, C.C. (1991). Inside the search process: information seeking from the users' perspective. _Journal of the American Society for Information Science,_ **42**(5), 361-371.
*   Kuhlthau, C.C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, **49**(4), 339-355.
*   Lamb, R., King, J.L. & Kling, R. (2003). Informational environments: organizational contexts of online information use. _Journal of the American Society for Information Science and Technology_, **54**(2), 97-114.
*   Li, Y. & Belkin, N.J. (2008). A faceted approach to conceptualizing task in information seeking. _Information Processing and Management_, **44**(6), 1822-1837.
*   Louw, M. (2006, May 12.). Meer as helfte van poste vir verpleging vakant - DA (_More than half of posts for nursing vacant_). _Beeld,_ 12 May, p. 11.
*   MacIntosh-Murray, A. & Choo, C.W. (2005). Information behavior in the context of improving patient safety. _Journal of the American Society for Information Science and Technology_, **56**(12), 1332-1345.
*   Mahon, S.M. & Williams, M. (2000). Information needs regarding menopause: results from a survey of women receiving cancer prevention and detection services. _Cancer Nursing_, **23**(3), 176-185.
*   McCaughan, D., Thompson, C., Cullum, N., Sheldon, T. & Raynor, P. (2005). Nurse practitioner and practice nurses' use of research information in clinical decision making: findings from an exploratory study. _Family Practice_, **22**(5), 490-497.
*   McKnight, M. (2006). The information seeking of on-duty critical care nurses: evidence from participant observation and in-context interviews. _Journal of the Medical Library Association_, **94**(2), 145-151.
*   McKnight, M. (2007). A grounded theory model of on-duty critical care nurses' information behavior: the patient-chart cycle of informative interactions. _Journal of Documentation_, **63**(1), 57-73.
*   MedicineNet.com. (2003). _[Definition of oncology](http://www.webcitation.org/61BGjE7J8)_. Retrieved 8 October, 2006 from http://www.medterms.com/script/main/art.asp?articlekey=18683\. (Archived by WebCite? at http://www.webcitation.org/61BGjE7J8)
*   Menzies, I.E.P. (1993). _The functioning of social systems as a defence against anxiety: a report on a study of the nursing service of a general hospital_. London: Travistock Institute of Human Relations.
*   Nicholas, D., Williams, P., Smith, A. & Longbottom, P. (2005). The information needs of perioperative staff: a preparatory study for a proposed specialist library for theatres (NeLH). _Health Information and Libraries Journal_, **22**(1), 35-43.
*   Pienaar, A. (2006, May 12.). Verpleegsters kyk gou wyer: moeilike ure, SA hospitale knak jonges (_Nurses soon look wider: difficult hours, SA young ones fold)_. _Beeld,_ 12 May, p. 11.
*   Randell, R., Mitchell, N., Thompson, C., McCaughan, D. & Dowling, D. (2009). From pull to push: understanding nurses' information needs._Health Informatics Journal_, **15**(2), 75-85.
*   Rogers, E.M. (2003). _Diffusion of innovations_. (5<sup>th</sup> ed.). New York, NY: Free Press.
*   Savolainen, R. & Kari, J. (2004). Placing the Internet in information source horizons: a study of information seeking by Internet users in the context of self-development. _Library & Information Science Research,_ **26**(4), 415-433.
*   Secco, M.L. _et al._ (2006). A survey of pediatric nurses' use of information sources. _Computers, Informatics, Nursing: CIN_, **24**(2), 105-112.
*   Sonnenwald, D.H. (1999). Evolving perspectives of human information behaviour: contexts, situations, social networks and information horizons. In Thomas D Wilson & David K. Allen (Eds.), _Exploring the contexts of information behaviour_ (pp. 176-190). London: Taylor Graham.
*   Stokes, P.J. & Lewin, D. (2004). Information-seeking behavior of nurse teachers in a school of health studies: a soft systems analysis. _Nurse Education Today_, **24**(1), 47-54.
*   Strauss, A., Fagerhaugh, S., Suczek, B. & Wiener, C. (1985). _Social organization of medical work_. Chicago, IL: University of Chicago Press.
*   Sundin, O. (2002). Nurses' information seeking and use as participation in occupational communities. _New Review of Information Behaviour Research_, **3**, 187-202.
*   Tannery, N.H. _et al._ (2007). Hospital nurses' use of knowledge-based information resources. _Nursing Outlook_, **55**(1), 15-19.
*   Tarzian, A.J., Iwata, P.A. & Cohen, M.Z. (1999). Autologous bone marrow transplantation:  the patient's perspective of information needs. _Cancer Nursing_, **22**(2), 103-110.
*   Thain, A. & Wales, A. (2005). Information needs of specialist health care professionals: a preliminary study based on the West of Scotland Colorectal Cancer Managed Clinical Network. _Health Information and Libraries Journal_, **22**(2), 133-142.
*   Thomas, J. (1985). Force field analysis: a new way to evaluate your strategy. _Long Range Strategy_, **18**(6), 54-59.
*   Thomson, H. (2000). Information needs in the early detection phase of colorectal cancer. _Canadian Oncology Nursing Journal_, **10**(1), 22-25.
*   Urquhart, C. & Crane, S. (1994). Nurses' information-seeking skills and perceptions of information sources: assessment using vignettes. _Journal of Information Science_, **20**(4), 237-246.
*   Vachon, M.L.S. (1998). Caring for the caregiver in oncology and palliative care. _Seminars in Oncology Nursing_, **14**(2), 152-157.
*   Vakkari, P. (2003). Task-based information searching. _Annual Review of Information Science and Technology_, **37**, 413-464.
*   Van der Molen, B. (1999). Relating information needs to the cancer experience: 1\. Information as a key coping strategy. _European Journal of Cancer Care_, **8**(4), 238-244. 
*   Van der Walt, H.M. & Swartz, L. (2002). Task orientated nursing in a tuberculosis control programme in South Africa: where does it come from and what keeps it going? _Social Science & Medicine_, **54**(7), 1001-1009.
*   Verhey, M.P. (1999). Information literacy in an undergraduate nursing curriculum: development, implementation and evaluation. _Journal of Nursing Education_, **38**(6), 252-259.
*   Wakeham, M. (1992). The information seeking behaviour of nurses in the UK. _Information Services and Use_, **12**(2), 131-140.
*   Wessel, C.B., Tannery, N.H. & Epstein, B.A. (2006). Information-seeking behavior and use of information resources by clinical research coordinators. _Journal of the Medical Library Association_, **94**(1), 48-54.
*   Williamson, K. (2006). Research in constructivist frameworks using ethnographic techniques. _Library Trends_, **55**(1), 83-101.
*   Wilson, P. (1993). The value of currency. _Library Trends_, **41**(4), 632-643.
*   Wilson, T.D. (1999a). Exploring models of information behavior: the'uncertainty' project. _Information Processing and Management,_ **35**(6), 839-849.
*   Wilson, T.D. (1999b). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270.
*   Wilson, T.D., Ford, N., Ellis, D., Foster, A. & Spink, A. (2002). Information seeking and mediated searching. Part 2: uncertainty and its correlates. _Journal of the American Society for Information Science and Technology,_ **53**(9), 704-715.
*   Wozar, J.A. & Worona, P.C. (2003). The use of online information sources by nurses. _Journal of the Medical Library Association_, **91**(2), 216-221.

## Appendices

### [Focus group interview guide.](p484focusgrp.pdf) [.pdf file]

### [Questionnaire for oncology nurses](p484nursqure.pdf) [.pdf file]

### Tables supporting text

<table id="tab3" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">  
**Table 3: Medical oncology centre**</caption>

<tbody>

<tr>

<td>**Autonomy**</td>

<td>Privately owned by a group of oncologists. They make their own decisions with regard to the infrastructure, staff appointments, staff duties, the budget including subscription to information services, conference and workshop attendance, etc. They can make their own decisions on introducing current awareness services to the nurses and other staff members.</td>

</tr>

<tr>

<td>**Financing**</td>

<td>Reasonable funding (including sponsorships from industry) is available for continuing education, current awareness services, conference attendance, etc. The oncologists frequently attend international conferences and make use of other opportunities for peer interaction. Staff members on other levels (e.g., the nurses, social worker, pharmacists) also get opportunities to attend conferences, courses, workshops, etc., nationally as well as internationally. Although it is recommended, they are not expected to do presentations or to publish.</td>

</tr>

<tr>

<td>**Infrastructure**</td>

<td>Although computer and Internet access are available to the staff (management felt the number of computers to be adequate), not all staff members have individual access. Some pointed out the lack of privacy and opportunity for uninterrupted searching. (They felt that a dedicated computer in a separate room might be a solution.) In cases where it was essential for their daily tasks (e.g., the Sister-in-charge and the social worker), they did, however, have their own computers.</td>

</tr>

<tr>

<td>**Staff situation**</td>

<td>The centre relies on the support of a multi-disciplinary team including the Sister-in-charge, oncology nurses, a social worker, pharmacists, financial and administrative staff. They receive their instructions from the oncologists and rely on them to keep up with new developments. For some aspects they rely on the Sister-in-charge. The staff is mostly expected to work conventional office hours. However, they feel that the work is very exhausting, especially since they spend a lot of time on their feet.</td>

</tr>

<tr>

<td>**Patients**</td>

<td>Although most patients are from Pretoria and surrounding areas, there are also patients from elsewhere in the country. Only some of the patients are hospitalised. Interaction with patients in the centre seems to be less intensive than in the hospital wards: they come to the centre for shorter periods (e.g., an hour or a few hours at a time), or even shorter periods for consultation only.</td>

</tr>

<tr>

<td>**External pressures**</td>

<td>The centre is experiencing pressures due to the South African National Health Act (Act 61 of 2003) and the Government's health policy. They need to ensure that they are nationally and internationally competitive, that they are involved in research, and that the oncologists keep abreast of developments (e.g., through conference attendance, monitoring the professional literature, as well as membership of professional organizations and other bodies). Due to the nature of their tasks, the oncologists have a strong sense of the importance of current awareness services; they are active users of a number of them. There is, however, no evidence that other staff members experience similar pressure.</td>

</tr>

<tr>

<td>**External interaction**</td>

<td>Interaction with other national as well as international institutions, medical practices, professional bodies etc. is considered very important, especially for the oncologists.</td>

</tr>

</tbody>

</table>

<table id="tab4" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">  
**Table 4: Hospital oncology wards**</caption>

<tbody>

<tr>

<td>**Autonomy**</td>

<td>A unit manager (rank of Sister) is in charge of the two hospital wards. She reports to the hospital management. The hospital is privately run and forms part of a large South African hospital group. Although the unit manager and nursing staff may make suggestions on continuing education, IT infrastructures, etc., these need to be approved by the hospital management. The hospital staff must follow the oncologists' orders for patient treatment. ('_They take orders and we [the oncologists] must come up with the orders'_). If they want to promote the use of current awareness services, they will need permission from the hospital management, especially if funding is involved.</td>

</tr>

<tr>

<td>**Financing**</td>

<td>Considerably less funding and opportunities than for the medical oncology centre are available for continuing education, conference attendance, etc. It was mentioned by participants that it is often only the senior staff that receive such opportunities. There are, however, some opportunities available for everybody as part of the staff development programme.</td>

</tr>

<tr>

<td>**Infrastructure**</td>

<td>There is one computer with e-mail access available in the wards. This is used for administrative purposes. Participants (including the ward unit manager) felt that this is inadequate. Internet access is only available for the hospital top management.</td>

</tr>

<tr>

<td>**Staff situation**</td>

<td>There is a clear hierarchical structure, including the ward unit manager, Sisters, nurses, staff nurses and assistant nurses as well as care workers. They are experiencing serious staff shortages, and work very long shifts (more than twelve hours per shift), with a lot of time on their feet. They are not expected to keep up with new developments concerning the treatment of cancer or the administration of medication, chemotherapy or drugs. This is the oncologists' responsibility. There is also very little emphasis on academic development: their main function is to care for patients to a very high standard. They have day and night shifts. (For reasons of convenience, we only included staff working the day shift in our survey.) It was, however, mentioned that the circumstances of the night shift may differ; patients, for example, ask more questions during the day. '_More questions during the day than at night??? see them for two hours and then they sleep??? less time to chat'_. '_Day shifts are completely different from night shifts'._ (Translated from Afrikaans.)</td>

</tr>

<tr>

<td>**Patients**</td>

<td>Staff only interact with the centre patients who are hospitalised, often for periods of several weeks. The interaction with patients therefore seems to be different from that of the centre staff. (This is explained in more detail in the section on tasks and functions.) It also seems as if there is more opportunity and need for (potentially information rich) interaction with patients and patient education, an issue that we will raise again in the recommendations.</td>

</tr>

<tr>

<td>**External pressures**</td>

<td>The hospital is experiencing pressures due to the South African National Health Act (Act 61 of 2003) and the Government's health policy. The quality of the care they offer should meet with the expectations of the oncologists, patients and the hospital management.</td>

</tr>

<tr>

<td>**External interaction**</td>

<td>There is less pressure for interaction with other national and international hospitals and organizational bodies. Some of the hospital staff does, however, have informal contact with staff from other hospitals. The ward unit manager admitted that she and other hospital staff do not experience the same pressures as the oncologists to be competitive.</td>

</tr>

</tbody>

</table>

<table id="tab5" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">  
**Table 5: Tasks and functions**</caption>

<tbody>

<tr>

<th>Tasks</th>

<th>Comments</th>

</tr>

<tr>

<td>Administration of treatments</td>

<td>This includes chemotherapy, assisting patients with hygiene, wound-care, and injections. Although treatment is offered by both groups, the treatment of hospitalised patients is often much more intense and more intensive, for example, chemotherapy for eight hours a day over a period of five to six days. All treatment is prescribed by the oncologists: the nursing staff must merely ensure that it is correctly and appropriately administered. The perception is that the responsibility to keep up to date therefore lies with the oncologists, and not the nursing staff. (In other settings and countries it may be that the nurses may have more responsibility for taking decisions.)</td>

</tr>

<tr>

<td>Monitoring and observation</td>

<td>This includes monitoring, for example, blood pressure, weight, blood results, and fever, with appropriate and regular feedback to the treating oncologists who take responsibility for appropriate treatment.</td>

</tr>

<tr>

<td>Administrative tasks</td>

<td>At the centre these include patient admission, patient appointments, getting results of tests, ward bookings, charging accounts, telephone enquiries, reception management, switch board management, the organization and preparation work for stem cell transplants and the management of patient records.</td>

</tr>

<tr>

<td>Staff management</td>

<td>This includes the delegation of tasks and staff management, and was specifically pointed out by the hospital staff.</td>

</tr>

<tr>

<td>Doctors' rounds</td>

<td>A senior hospital staff member needs to accompany the oncologists on their rounds with patients. This often happens twice a day at times that fit in with the different oncologists' schedules.</td>

</tr>

<tr>

<td>Information sessions</td>

<td>The hospital staff needs to lead and participate in information sessions at shift take-over. They, as well as staff from the centre, are also expected to attend meetings by the oncologists where information on patients is shared.</td>

</tr>

<tr>

<td>Patient education and counselling</td>

<td>Both the hospital and centre staff saw this as an important function. It includes sharing information with patients, for example on self-treatment such as self-injection and the implications of treatment processes, preparing patients for stem cell transplants or aftercare, and answering routine queries. The social worker and the hospital staff also mentioned counselling and emotional support for patients and their families: '_They always look to us for answers_' and'_Our patients need a lot of emotional support, because they are very ill_'. '_Talking to the patients??? giving them guidance on the illness_'. '_They like contact with patients_'. (The latter is a translation from Afrikaans from the interview with the oncologist.)</td>

</tr>

<tr>

<td>Daily physical care</td>

<td>The hospital staff must ensure, amongst other things, that patients receive their meals, exercise where necessary, and that their beds are made. They also need to offer advice on dealing with nausea, vomiting, etc.</td>

</tr>

<tr>

<td>Staff training</td>

<td>The hospital group need to offer in-service training for new staff members.</td>

</tr>

<tr>

<td>Support of hospital staff</td>

<td>This is mostly the duty of the care workers who need to help with the   washing and feeding of patients when necessary, as well as helping with other small tasks for patients.</td>

</tr>

<tr>

<td>Working with children</td>

<td>One of the hospital wards specialises in the treatment of children. They stressed the fact that paediatric care has its own unique demands. '_Paediatric oncology??? completely different_'. '_A child is totally different from an adult under circumstances of illness_' (Translated from Afrikaans.) (A participant responsible for paediatric care stressed her absolute reliance on the oncologist specialising in paediatric care for information.)</td>

</tr>

</tbody>

</table>

<table id="tab6" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">  
**Table 6: Strength analysis of settings regarding a need for information and current awareness services**</caption>

<tbody>

<tr>

<th>Facilitating</th>

<th>Constraining</th>

<th>Blocking</th>

</tr>

<tr>

<td>Well-being of patients is central in their work</td>

<td>Very few with formal training in oncology nursing</td>

<td>Lack of time</td>

</tr>

<tr>

<td>Growing interest amongst patients to use the Internet and confront nurses with information found</td>

<td>Do not have a culture of sharing & disseminating information</td>

<td>Lack of privacy</td>

</tr>

<tr>

<td>Care for patients central in their work</td>

<td>Do not have a culture of information use</td>

<td>Too many interruptions</td>

</tr>

<tr>

<td>Interest in using CAS (conditional)</td>

<td>Lack of confidence in finding information and using the Internet</td>

<td>Lack of access to ICT and the Internet</td>

</tr>

<tr>

<td>Strong confidence in ability to find information (unfortunately only a very few)</td>

<td>Task completion seem not to depend on information; tasks seems not to be information-intensive</td>

<td>Little emphasis on academic development</td>

</tr>

<tr>

<td>Strong affective and caring atmosphere and attitude</td>

<td>Work is emotionally tiring</td>

<td>Inadequate computer skills</td>

</tr>

<tr>

<td> </td>

<td>Difficult to find a balance between emotionally laden work and personal life</td>

<td>Inadequate skills in using the Internet</td>

</tr>

<tr>

<td> </td>

<td>Focus is on clinical tasks and not research</td>

<td>Lack of funding and time for continuing education (specifically in the hospital wards)</td>

</tr>

<tr>

<td> </td>

<td>Information considered as'nice to have'</td>

<td>Lack of awareness of information needs</td>

</tr>

<tr>

<td> </td>

<td>Evidence-based nursing practices not strong in the country</td>

<td>Not required to search for information in their task completion</td>

</tr>

<tr>

<td> </td>

<td>Lack of formal information literacy and Internet search training</td>

<td>Do not have a strong professional identity - very heavy reliance on doctors</td>

</tr>

<tr>

<td> </td>

<td>Lack of motivation</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>Routine nature of tasks - main role is caring</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>Lack of time of oncologists to share information</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>Fair to considerable work experience in oncology nursing (_this can be facilitating or constraining, but seems constraining in the contexts studied_)</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>Trust in colleagues</td>

<td> </td>

</tr>

</tbody>

</table>