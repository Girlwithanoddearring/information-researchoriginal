#### vol. 20 no. 2, June, 2015

# Assessing cancer patients' health information needs: a standardized approach

#### [Kalyani Ankem](#author)  
Department of Business Informatics, College of Informatics, Northern Kentucky University, Highland Heights, KY 41076, USA

#### Abstract

> **Introduction.** To reach health care decisions that are satisfactory to both health care professionals and patients, it is important to understand cancer patients' information needs to facilitate delivery of care and to design educational programmes. The objective in the present study is to assess instruments designed to gather data on cancer patients' information needs.  
> **Method.** To conduct the review, the present research identified fifteen instruments for measuring cancer patients' information needs. An outcome measures rating tool developed at McMaster University was used to assess the instruments on scale construction, level of measurement, reliability, validity and overall utility.  
> **Analysis.** Data were extracted on each instrument in accordance with the rating system to ascertain attributes being measured, time to complete the assessment, scale construction, standardisation, reliability, content validity, construct validity and criterion validity. No synthesis of the abstracted data was attempted in the present review as the purpose was to inform health care professionals and researchers about the nature and characteristics of applicable measurement tools.  
> **Results.** Several instruments were rated excellent in content validity; however, construct and criterion validity were poor. Content validity was based on literature, experts, clinical professionals, patients and family members. Different methods of testing reliability indicated adequate to excellent reliability for most instruments. Some instruments could not be assessed due to lack of data on reliability and/or validity testing.  
> **Conclusions.** The Toronto informational needs questionnaire - breast cancer, its adaptation by Dale _et al._, and Thurstone scaling of information needs are rated highest as reliable and valid instruments, which can be used in developing and testing instruments further in different cancer patient populations.

## Introduction

Patients and their families are profoundly affected by a diagnosis of cancer. As they manage to come to terms with the news of a diagnosis and anxiety about the unexpected course of the illness, they begin a search for information to learn about the disease itself and therapeutic options. Access to adequate and accurate information not only affects patients' health outcomes, it also helps patients and their families cope with diagnosis and the disruption of quality of life during diagnosis, treatment and post-treatment phases ([Dale, Jatsch, Hughes, Pearce and Meystre, 2004](#dal04); [Arora, Johnson, Gustafson, McTavish, Hawkins. and Pingree 2002](#aro02); [Rutten, Arora, Bakos, Aziz and Rowland, 2005](#rut05)).

The situation necessitates an inquiry of the information cancer patients need. An understanding of their information needs can help tailor patient education to empower and to support coping. It can also guide health care professionals in delivery of care. Several instruments have been employed to measure the information needs of patients in cancer care. These instruments show that cancer patients differ in the extent and type of information they need ([Ankem, 2005](#ank05)). Instruments exist to measure a need for information on diagnosis, chances of cure, spread of disease, prognosis and signs of recurrence ([Ankem, 2005](#ank05)). Also, a need for information on possible treatments, side effects of treatments and progress during the treatment phase ([Ankem, 2005](#ank05)) has been explored.

A patriarchal scenario in which the physician alone chooses the treatment for the patient is far less accepted today than previously, and is even questioned, especially in situations where a clear treatment plan does not exist ([Pinquart and Duberstein, 2004](#pin04)). Information on course of treatment, side effects of treatment options and impact on quality of life would be crucial for both patients and their families if they are to participate in decision-making. Patients' active involvement in health care results in heightened feelings of control and empowerment ([Pinquart and Duberstein, 2004](#pin04)), leading to not only informed decision-making in treatment choice, but also satisfactory interactions with the health care system.

As such, treatment-related information would be especially important in the care context. An analysis of treatment-related information can offer insights into this much needed information to facilitate care. Different types of cancer affect and necessitate different levels of information. Competing treatment plans exist and information on side effects, which is especially important, is not always clear. Treatment decision-making is often a complex process. Incomplete and unconfirmed information about the benefits and side-effects of different treatment options complicates choice of a treatment plan ([Montie, 1994](#mon94)). Hence, the complex process of treatment decision-making entails the physician providing information to patients about treatments and their benefits and risks, patients sharing information about their situation with the physician, and both the physician and patients engaging in a discussion of treatment alternatives and reaching a consensus regarding a treatment plan to follow ([Charles, Whelan, Gafni, Willan and Farrel, 2003](#cha03)). An understanding of what information is needed and how to share can empower patients and improve care.

Several factors influence the need for treatment and other information. Patients and their relatives may be jointly involved in decision-making or patients may delegate decision-making to others completely, because some patients cope with a diagnosis of cancer by avoiding information ([Miles _et al._, 2008](#mil08)). Regardless, the physician must be willing to present all treatment alternatives, and must present the information in a format that is comprehensible to patients and/or their families ([Kim _et al._, 2001](#kim01)). Then, treatment options that are available and those that physicians can present are often affected by patients' type and stage of cancer and their financial situation ([Pinquart and Duberstein, 2004](#pin04); [Cassileth, Zupkis, Sutton-Smith and March, 1980](#cas80); [Degner _et al._, 1997](#deg97)). In the ensuing information exchange, patients' ability to seek information that will facilitate decision-making depends on several other factors. Patients' fear and apprehension after a cancer diagnosis can discourage them from asking specific questions in interactions with health care ([Leydon, _et al._, 2000](#ley00)). Successful information exchange in these overwhelming situations also depends on patients' communication style, aptitude and literacy, which will enable sifting through and evaluating information; on patients' assertiveness and perceptions of the importance of taking an active role in decision-making in health care; and on patients' psychological well-being ([Pinquart and Duberstein, 2004](#pin04); [Mastaglia and Kristjanson, 2001](#mas01); [Pierce, 1996](#pie96)). Patients' age influences information needs as well. For example, younger patients tend to seek more information in cancer care ([Ankem, 2005](#ank05)).

An _a priori_ understanding of the impact of the preceding factors on patients' information needs along the care continuum (diagnosis, treatment, post-treatment and survivorship, relapse or recurrence and end-of-life) would help to tailor patient education intervention and/or to facilitate communication and decisions that are satisfactory to both health care professionals and patients. A systematic and standardized approach to understanding patient information needs is achieved through the use of instruments designed to gather data on information needs in cancer care. As several instruments have been employed to measure patient information needs in cancer care, an assessment of the viability of these instruments would show the availability, or lack thereof, of a systematic approach to measuring and analyzing patient information needs. As such, the present study assesses all characteristics of these instruments, including reliability and validity, to not only encourage use of these instruments in health care education, but also to facilitate use of standardized instruments in assessing information needs of cancer patients in health care research.

## Objectives

The purpose was a) to identify instruments designed to measure the information needs of cancer patients, b) to evaluate the identified instruments, c) to facilitate application of instruments in the development of cancer education programmes for specific populations and d) to facilitate application of instruments in research on cancer patient education.

## Method

The study is an extension of the author's previous research on information needs. In this prior research, the author identified instruments to facilitate a synthesis of cancer patients' information needs ([Ankem, 2005](#ank05)).

In the initial phase, we conducted searches of online databases [Medline](http://www.nlm.nih.gov/bsd/pmresources.html) and CINAHL ([Cumulative Index to Nursing & Allied Health Literature](https://health.ebsco.com/products/the-cinahl-database)) to find studies published during the ten-year period 1993-2003 regarding the information needs of cancer patients. A detailed account of search strategies and inclusion and exclusion criteria can be found in the author's previous publications ([Ankem, 2005](#ank05)). The author expanded the search to include instruments used in research published from 2003-2012.

The first two searches used the heading neoplasm (with the explode function) and the term cancer patients in the title, abstract, medical subject heading (MeSH), and Chemical Abstracts Service registry and European Community number. This study combined these searches with the Boolean _or_ to retrieve studies dealing with cancer patients. Then, the author combined the resulting search with other searches using the following terms: information need(s), information seeking, information seeking behaviour, information source(s) and information resource(s), all in the title, abstract, MeSH subject heading, and Computer Abstracts registry and EC number, to retrieve studies only on the information needs of cancer patients. The final search in Medline and CINAHL was limited to studies published in English, in a certain time period, and including only adult participants. The final search in CINAHL was also limited to research studies.

First, the author reviewed the titles and abstracts of these studies. In the next step, the author perused the remaining studies for inclusion based on these predetermined criteria: (1) analysis of information needs of patients diagnosed with cancer and/or information sources used by cancer patients, (2) inclusion of adults as subjects in the research study, and (3) application of quantitative research methods and relevant statistics.

In selecting studies that dealt with specific information needs only, the author removed those that presented an insufficient number of information needs and did not provide a clear picture of what was studied. The author also excluded those studies that presented too many information needs and those that were too broad and less focused on information needs, making a comparison with information needs presented in other studies unwieldy.

The author evaluated the articles for quality. During this process, more articles were eliminated due to unclear logical progression and/or inadequate explanation and presentation of statistical results that include reports of statistical tests conducted, test statistics derived and probabilities reached. The author did not make a distinction between types of cancer. The publications that were selected are all peer-reviewed journal articles published by researchers in oncology, nursing, public health, psychology or psychiatry. Upon discovering instruments, the author searched databases further to find any studies conducted solely for the purposes of establishing reliability and validity.

### Instruments

Fifteen instruments were found as a result of the literature. search Table 1 lists these instruments. Notable instruments the studies employed for measuring various information needs included Toronto informational needs questionnaire - breast cancer (hereafter, _the Toronto questionnaire_) ([Galloway _et al._, 1997](#gal97)), patient learning need scale ([Galloway _et al._, 1993](#gal97)), information preference cards ([Hack, Degner and Dyck, 1994](#hac94)), and Thurstone scaling of information needs developed by Degner and colleagues ([Bilodeau and Degner, 1996](#bil96); [Degner _et al._, 1997](#deg97)).

The Toronto questionnaire, which has as its precursor the _information needs questionnaire - breast cancer_, measures the importance that patients attribute to different types of information on a five-point Likert scale. The five sub-scales of Toronto questionnaire; disease, investigative tests, treatment, physical and psychosocial, measure five types of information. In seven comparable sub-scales, the patient learning need scale similarly measures the importance of having information. The Hack _et al._ ([1994](#hac94)) information preference cards are a precursor to the Thurstone scaling of information needs developed by Degner and colleagues. The information preference cards involve ranking of seven types of information related to diagnosis: illness, treatment alternatives, treatment procedures, side effects and prognosis. The Thurstone scaling of information needs is a more complicated ranking method that uses nine categories of information: stage of disease, likelihood of cure, effect of treatment on social activity, effect of disease on family and friends, self-care needs, effect of treatment on sexual activity, types of treatment available, risk to a family member and side effects of treatment. According to Bilodeau and Degner ([1996, p. 693](#bil96)), '_In Thurstone scaling, each item produces a single number that reflects the degree of preference that a group of people have for that item_'.

<table class="center" style="width:90%;"><caption>  
Table 1: Instruments measuring cancer patients' information needs  
* Jatsch ([2004](#jat04)) instrument is separate but addressed in Dale _et al_. ([2004](#dal04)). Hence, the total number of instruments is fifteen.</caption>

<tbody>

<tr>

<td>**1\. Toronto informational needs questionnaire**  
Galloway, S., Graydon, J., Harrison, D., Evans-Boyden, B., Palmer-Wickham, S., Burlein-Hall, S. … Blair, A. (1997). Informational needs of women with a recent diagnosis of breast cancer: development and initial testing of a tool. _Journal of Advanced Nursing, 25_(6), 1175-1183.  
**2\. Patient learning needs scale**  
Galloway, S.C., Bubela, N., McKibbon, A., McCay, E. & Ross, E. (1993). Perceived information needs and effect of symptoms on activities after surgery for lung cancer. _Canadian Oncology Nursing Journal, 3_(3), 116-119.  
**3\. Information preference cards**  
Hack, T.F., Degner. L.F. & Dyck, D.G. (1994). Relationship between preferences for decisional control and illness information among women with breast cancer: a quantitative and qualitative analysis. _Social Science & Medicine, 39_(2), 279-289.  
**4\. Thurstone scaling of information needs**  
Bilodeau, B.A. & Degner, L.F. (1996). Information needs, sources of information, and decisional roles in women with breast cancer. _Oncology Nursing Forum, 23_(4), 691-696.  
**5\. Patient information questionnaire**  
Meredith, C., Symonds, P., Webster. L., Lamont, D., Pyper, E., Gillis, C.R. & Fallowfield, L. (1996). Information needs of cancer patients in west Scotland: cross sectional survey of patients' views. _BMJ, 313_(7059), 724-726.  
**6\. Knowledge, attitudes, and beliefs questionnaire**  
Cohn, W.F., Ropka, M.E., Jones, S.M. & Miesfeldt, S. (2003). Information needs about hereditary breast cancer among women with early-onset breast cancer. _Cancer Detection and Prevention, 27_(5), 345-352.  
**7\. Problems and needs in palliative care questionnaire**  
Voogt, E., van Leeuwen, A.F., Visser, A.P., van der Heide, A. & van der Maas, P.J. (2005). Information needs of patients with incurable cancer. _Supportive Care in Cancer, 13_(11), 943-948.  

**Other instruments**  
8\. Stewart, D.E., Wong, F., Cheung, A.M., Dancey, J., Meana, M., Cameron, J.I., … Rosen, B. (2000). Information needs and decisional preferences among women with ovarian cancer. _Gynecologic Oncology, 77_(3), 357-361.  
9\. Jenkins, V., Fallowfield, L. & Saul, J. (2001). Information needs of patients with cancer: results from a large study in UK cancer centres. _British Journal of Cancer, 84_(1), 48-51.  
10\. Iconomou, G., Viha, A., Koutras, A., Vagenakis, A.G. & Kalofonos, H.P. (2002). Information needs and awareness of diagnosis in patients with cancer receiving chemotherapy: a report from Greece. _Palliative Medicine, 16_(4), 315-321.  
11\. Raupach, J.C. & Hiller, J.E. (2002). Information and support for women following the primary treatment of breast cancer. _Health Expectations, 5_(4), 289-301.  
12\. Dale, J., Jatsch, W., Hughes, N., Pearce, A. & Meystre C. (2004). Information needs and prostate cancer: the development of a systematic means of identification. _BJU International, 94_(1), 63-69.  
13\. Andreassen, S., Randers, I., Näslund, E., Stockeld, D. & Mattiasson, A.C. (2007). Information needs following a diagnosis of oesophageal cancer; self-perceived information needs of patients and family members compared with the perceptions of health care professionals: a pilot study. _European Journal of Cancer Care_ (English ed.), _16_(3), 277-285.  
14\. Feldman-Stewart, D., Capirci, C., Brennenstuhl. S., Tong, C., Abacioglu. U., Gawkowska-Suwinska, M., … Wördehoff, H. (2010) Information needs of early-stage prostate cancer patients: a comparison of nine countries. _Radiotherapy and Oncology, 94_(3), 328-333.  
</td>

</tr>

</tbody>

</table>

Other questionnaires administered in studies also measured patients' needs for comparable types of information on a Likert scale. Among these other instruments, the Stewart _et al._ ([2000](#ste00)) questionnaire is based on work they conducted previously in developing information needs measures, including work by Cassileth _et al._ ([1980](#cas80)) on the information styles questionnaire and the more recent extensive work of Degner _et al._ ([1997](#deg97)) on the Thurstone scaling of information needs. Jenkins _et al._ ([2001](#jen01)) indicate that the instrument they employed to measure information needs has been used widely in their studies in the UK and USA. Jenkins _et al._ state that their instrument is based on earlier work that includes the study by Meredith _et al._ ([1996](#mer96)). Among those employed more recently, notable instruments include Dale _et al._'s ([2004](#dal04)) adaptation of the Toronto questionnaire to measure the information needs of prostate cancer patients leading to a shorter, more precise instrument, and the longer, more substantive instruments to measure information needs among oesophageal and prostate cancer patients by Andreassen, Randers, Näslund, Stockeld and Mattiasson ([2007](#and07)) and Feldman-Stewart _et al._ ([2010](#fel10)). Although Andreassen _et al._ developed the instrument based partly on the Toronto questionnaire. Feldman-Stewart _et al._, do not appear to have based their instrument's development on any specific previous instrument. Nevertheless, both are significant because Andreassen _et al._ compare patient perceptions with those of health care professionals while Feldman-Stewart _et al._ compare information needs of prostate cancer patients in nine countries.

### Instrument evaluation

To assess these instruments the present study used an instrument evaluation system designed by Law ([1997](#law97)), which is a rating system to measure outcomes. Law's ([1997](#law97)) analytical framework is designed to rate attributes being measured, time to complete the assessment, scale construction, standardization, reliability, content validity, construct validity and criterion validity. It has been applied in measuring occupational therapy outcomes previously. Research in occupational therapy applied Law's rating system to measure outcomes such as activities of daily living ([Law and Letts, 1989](#law89)). These activities of daily living outcomes can indicate clinical, as well as psychological and social participation, aspects. Measuring the need for cancer-related information includes a need for clinical, psychological and social information. No assessment system exists to evaluate patient information needs. Thus, Law's rating system is deemed appropriate for assessment purposes in this review. Also, Law's rating system is a comprehensive, systematic tool to assess outcome measures. The author acknowledges that a rating of need for information is not a rating of an occupational therapy outcome. However, the criteria in Law's evaluation system do appear to be relevant to assessing the measures in this study.

Table 2 presents an overview of the rating system that one can use to assess any instrument designed to measure outcomes. Scale construction of an instrument is rated on level of measurement, whether items are selected based on literature and/or opinion of experts, and whether or not sub-scale scores can be computed.

Reliability is assessed through internal consistency, inter-observer reliability and intra-observer reliability in terms of number of studies and statistical values.

<table class="center" style="width:95%;"><caption>  
Table 2: Outcome measures rating system  
* Source for the outcome measures rating system is Law [(1997)](#law97).  
* E = Excellent; A = Adequate; P = Poor. Incomplete information was rated as I.</caption>

<tbody>

<tr>

<th colspan="6">Attribute(s) being measured</th>

</tr>

<tr>

<th colspan="6">Time to complete the assessment</th>

</tr>

<tr>

<td style="vertical-align:top;">Scale construction</td>

<td style="vertical-align:top;">**Item selection**  
Literature review and/or survey of experts (E)  
Most relevant characteristics (A)  
Convenient (P)</td>

<td style="vertical-align:top;">**Level of measurement**  
Nominal, ordinal, interval/ratio  
Sub-scales - scores (Y/N)  
Number of items</td>

<td colspan="3"> </td>

</tr>

<tr>

<td style="vertical-align:top;">Standardization</td>

<td style="vertical-align:top;">**Norms**  
Ages, populations, size of sample</td>

<td colspan="4"> </td>

</tr>

<tr>

<td style="vertical-align:top;">Reliability</td>

<td style="vertical-align:top;">**Reliability studies**  
Internal consistency  
Observer (intra-observer and/or inter-observer)  
More than two studies (E)  
One to two studies (A)  
No studies (P)  
</td>

<td style="vertical-align:top;">**Value Rating**  
Statistic (Cronbach's alpha, Kappa coefficient,  
Pearson correlation, intra-class correlation, intercorrelation)  
>0.80 (E)  
0.60-0.79 (A)  
<0.60 (P)</td>

<td colspan="3"> </td>

</tr>

<tr>

<td style="vertical-align:top;">Validity</td>

<td style="vertical-align:top;">More than two studies (E)  
One to two studies (A)  
No studies (P)  
</td>

<td style="vertical-align:top;">**Content validity**  
Judgmental or statistical (factor analysis) (E)  
Comprehensive but no method used (A)  
Not comprehensive (P)  
</td>

<td style="vertical-align:top;">**Construct validity**  
Convergent  
Discriminant  
More than two studies (E)  
One or two studies (A)  
No studies (P)  
</td>

<td style="vertical-align:top;">**Criterion validity**  
Concurrent  
Predictive  
More than two studies (E)  
One or two studies (A)  
No studies (P)  
</td>

<td style="vertical-align:top;">**Value Rating**</td>

</tr>

<tr>

<td style="vertical-align:top;">Overall utility</td>

<td style="vertical-align:top;">Adequate to excellent utility, excellent reliability  
and validity (E)  
Adequate to excellent utility, adequate to  
excellent reliability and validity (A)  
Poor utility, poor reliability and validity (P)</td>

<td colspan="4"> </td>

</tr>

</tbody>

</table>

The present study determined content validity based on whether instrument development used judgmental or statistical methods. It assessed convergent and discriminant validity of the instrument in terms of number of studies conducted and statistical values to establish construct validity. Lastly, to establish criterion validity, the study evaluated concurrent and predictive validity in terms of number of studies and statistical values. An overall rating conveys the value of an instrument.

## Data abstraction and synthesis

This study selected fifteen instruments for data abstraction and analysis. Table 3 summarizes the characteristics of these instruments. The author, accompanied by a research assistant, extracted data on each instrument in accordance with Law's ([1997](#law97)) evaluation system to ascertain attributes being measured, time to complete the assessment, scale construction, standardization, reliability, content validity, construct validity and criterion validity. The results of the abstraction are presented in Table 3\. No synthesis of such abstracted data was attempted in the present review as the purpose was to inform health care professionals and researchers on the nature and characteristics of applicable measurement tools.

## Analysis

Fifteen instruments were identified in an extensive search for literature on the information needs of cancer patients, as described in the method section above. A list of these instruments, including the source article reporting applied research in which the instrument was used or the published evaluation of the instrument, is provided in Table 1\. Full titles of instruments are listed in Table 1 for instruments 1-7\. For the other instruments, only the publication in which they appear are listed in Table 1 as these instruments did not have titles.

### Dimensions

Table 3 ([Appendix](#app)) shows an abstraction of the fifteen instruments rated on Law's ([1997](#law97)) attributes based on the analytical framework. Instruments were designed to measure the information needs of patients diagnosed with breast, prostate, lung and/or oesophageal cancers. However, some measured the information needs in specific cancers while others covered information needs in general.

The Toronto questionnaire ([Galloway _et al._, 1997](#gal97)), a widely applied instrument, has five sub-scales: disease, investigative tests, treatments, physical and psychosocial. Thurstone scaling of information needs in breast cancer ([Bilodeau and Degner, 1996](#bil96)), also a significant measurement tool, includes stage of disease, likelihood of cure, treatment options, side effects, risk to relatives, social activities, self-care issues and sexuality. Both instruments illustrate the importance of patients learning about the disease, including diagnosis and prognosis, and finding information on possible treatments. Dales _et al._'s ([2004](#dal04)) instrument, which is based on the Toronto questionnaire, collapses the five dimensions into four: basics of prostate cancer care, disease management, psycho-social and physical well-being, and self-help.

Other instruments follow the same emphasis. Andreassen _et al._ ([2007](#and07)) add two other dimensions, information methods and health care professionals' competence, in their instrument to measure the information needs in oesophageal cancer. Feldman-Stewart _et al._ ([2010](#fel10)) also add experience of the physician and the treatment centre as a dimension in their instrument to measure the information needs of early-stage prostate cancer patients. Disease and treatment dimensions are included as well; however, psychosocial dimensions are omitted. Meredith _et al._ ([1996](#mer96)) focus on disease and treatment whereas psychosocial aspects are glaringly omitted. Treatment information remains important in instruments such as the _patient learning needs scale_ ([Galloway _et al._, 1993](#gal93)), designed to measure post-discharge information needs in lung cancer patients and the _problems and needs in palliative care questionnaire_ ([Voogt, van Leeuwen, Visser, van der Heide and van der Maas 2005](#voo05)) designed to gather information needs in palliative care, which additionally makes nutrition, care settings and organizations that provide help important in long-term care and alternative options to life. As shown in Table 3 ([Appendix](#app)), physical and psychosocial well-being is a concern throughout cancer care.

### Structure

Many instruments were around twenty to sixty items in length and measured information needs, at least, at the interval level. The shortest instrument, the Thurstone scaling of information needs ([Bilodeau and Degner, 1996](#bil96)), has nine items and the longest, Feldman-Stewart _et al._'s ([2010](#fel10)) instrument to measure the information needs of early-stage prostate cancer patients, has ninety-three items. Three instruments were around thirteen items in length: Iconomou, Viha, Koutras, Vagenakis and Kalofonos ([2002](#ico02)), Raupach and Hiller ([2002](#rau02)) and the _patient learning needs scale_ ([Voogt _et al._, 2005](#voo05)). The Toronto questionnaire ([Galloway _et al._, 1997](#gal97)) was fifty-one items, but Dale _et al._'s ([2004](#dal04)) adaptation had only twenty items. Content and construct validity testing through factor analysis, discussed below, most likely resulted in a more precise adaptation of the instrument. Another instrument with twenty items is Jatsch _et al._'s ([2004](#jat04)). Jatsch _et al._ measured the information needs of prostate cancer patients. The _patient learning needs scale_ ([Galloway _et al._1993](#gal93)) and Stewart _et al._ ([2000](#ste00)) list forty to fifty items. Andreassen _et al._ ([2007](#and07)) have sixty-four items. Generally, longer instruments are considered more reliable in psychometrics.

For three of fifteen instruments, the number of items could not be determined or information was not available. These three instruments were Jenkins, Fallowfield and Saul ([2001](#jen01)), Meredith _et al._ ([1996](#mer96)) and _knowledge, attitudes, and beliefs questionnaire_ ([Cohn, Ropka, Jones and Miesfeldt, 2003](#coh03)). The _knowledge, attitudes, and beliefs questionnaire_ is designed to measure the information needs in early-onset hereditary breast cancer.

The most commonly used measurement scale is the Likert scale, either a four or five-point scale. The items were considered to be interval level. There always is a discussion in the literature as to whether Likert scale items are interval level. Certain criteria, such as number of points in the Likert scales, lend themselves toward acceptance of this type of scale being at interval level. For example, in the Toronto questionnaire, the importance of a type of information within five sub-scales: disease, investigative tests, treatments, physical and psychosocial, was rated over a five-point scale of _not important_ to _extremely important_. Raupach and Hiller's ([2002](#rau02)) items are considered nominal because their four-point Likert scale of _not important_ to _extremely important_ is collapsed into only two categories: _not/slightly important_ and _moderately/extremely important_. Feldman-Stewart _et al._'s ([2010](#fel10)) items are considered ordinal because importance is rated on a four-point scale of _essential_ to _avoid_. In addition to the scale's four points, the statistics that they employ are also indicators of the level of measurement. For instance, the Cronbach's alpha is calculated by Stewart _et al._ ([2000](#ste00)) to assess reliability of responses to items on a five-point scale of _not important_ to _essential_ (Table 3). The Cronbach's alpha indicates that the authors consider the scale interval level. Andreassen _et al._ ([2007](#and07)) calculate mean score of importance, which is another example of items considered interval scale based on which statistics a study employed. Andreassen _et al._, however, use a four-point scale of _not at all applicable_ to _very important_, instead of a five-point scale, which researchers often use to accept items at the interval level ([Table 3](#app)).

The Thurstone scaling of information needs ([Bilodeau and Degner, 1996](#bil96)) is a more complicated ordinal ranking method that used nine categories of information: stage of disease, likelihood of cure, effect of treatment on social activity, effect of disease on family and friends, self-care needs, effect of treatment on sexual activity, types of treatment available, risk to a family member, and side effects of treatment. Items are presented in all possible pairs to the participant. Participants indicated preference for one item over the other item in the pair. Bilodeau and Degner ([1996](#bil96)) used the percentage of times that participants preferred an item to calculate scale scores and to calculate the preferred proportions which were then converted to z scores. (A formula is readily available for the conversion to z scores or can be produced in a statistical package.) These z scores for each information need were ordered along a scale to develop the rank-ordered Thurstone scale, which is the profile of information needs ([Deane and Degner, 1998](#dea98)). According to Bilodeau and Degner ([1996](#bil96)), in essence, '_In Thurstone scaling, each item produces a single number that reflects the degree of preference that a group of people have for that item_' (p. 693).

The number of items could not be determined or information was not available, nor could details of scale of measurement be ascertained for three instruments: the knowledge, attitude, and beliefs questionnaire ([Cohn _et al._, 2003](#coh03)), Jenkins _et al._ ([2001](#jen01)), and Meredith _et al._ ([1996](#mer96)).

### Validity

#### Content validity

According to Law ([1997](#law97)), an instrument has content validity when it is '_comprehensive and fully represents the domain of the characteristics it claims to measure_' (p. 4). Consensus methods based on expert judgment and/or statistical methods can be applied to establish content validity of an instrument.

Regarding content validity, the most common methods used in constructing scales for instruments were based on a variety of sources: experts, literature, clinicians, patients and family members. Not all sources were utilized in developing each instrument. Toronto questionnaire ([Galloway _et al._, 1997](#gal97)) is based on experts and literature. Thurstone scaling of information needs ([Bilodeau and Degner, 1996](#bil96)) is based on literature only. Several instruments used experts to construct the scale. Most instruments had two or more sources - literature, experts and/or patients - as a basis. Stewart _et al._ ([2000](#ste00)), Jatsch _et al._ ([2004](#jat04)) and Dale _et al._ ([2004](#dal04)) used clinicians rather than broader experts. PNPC ([Voogt _et al._, 2005](#voo05)) only used patients and relatives, and these are desired sources in establishing content validity for such instruments. Feldman-Stewart _et al._ ([2010](#fel10)), Knowledge, attitudes, and beliefs questionnaire ([Cohn _et al._, 2003](#coh03)) and Andreassen _et al._ ([2007](#and07)) are comprehensive in using different perspectives, with the addition of clinicians. Feldman-Stewart _et al._ ([2010](#fel10)), however, used lay persons instead of literature, while knowledge, attitudes, and beliefs questionnaire ([Cohn _et al._, 2003](#coh03)), and Andreassen _et al._ ([2007](#and07)) used all four: literature, experts, clinicians and patients. For the patient learning needs scale ([Galloway _et al._, 1993](#gal93)), Iconomou _et al._ ([2002](#ico02)), Jenkins _et al._ ([2001](#jen01)) and Meredith _et al._ ([1996](#mer96)), the source for scale construction could not be determined or was not available.

Statistical testing for validity, however, is sparse. Content validity was established through consensus and was either excellent or adequate for most instruments, as a majority of the instruments had included experts in scale construction. The patient learning needs scale ([Galloway _et al._, 1993](#gal93)) described no source for scale construction; however, the authors stated excellent content validity based on literature. Stewart _et al._ ([2000](#ste00)) state their instrument has content validity. However, experts were not used; literature and clinicians were the basis for scale construction. Only Dale _et al._'s ([2004](#dal04)) adaptation of the Toronto questionnaire ([Galloway _et al._, 1997](#gal97)) used factor analysis for content validity resulting in a shorter instrument of twenty items (from the original fifty-one items). In this case, the factor analysis is used to establish factorial validity. Factorial analysis is a statistical method in which researchers cluster correlations of responses to form groupings of items to represent concepts that must make intuitive sense to be valid. The correlations for Dale _et al._'s ([2004](#dal04)) instrument were 0.25; as such, factor analysis to establish content validity was adequate ([Table 3](#app)).

Iconomou _et al._ ([2002](#ico02)) was rated poor in content validity as there was no evidence of the basis for scale construction or any form of statement to establish, at least, a consensus for content validity. For Jenkins _et al._ ([2001](#jen01)) and Meredith _et al._ ([1996](#mer96)) neither details on scale construction or content validity could be obtained.

#### Construct and criterion validity

Researchers rarely tested construct and criterion validity. Law ([1997](#law97)) describes criterion validity as validity established when '_the measurements obtained by the instrument agree with another more accurate measure of the same characteristic, that is, a criterion or gold standard measure_' (p. 4). In essence, criterion validity is the measurement of agreement with, or relationship to, another more accurate measure of similar characteristics. Criterion validity was consistently poor across instruments.

Construct validity was also lacking across instruments. Construct validity tests whether the measure conforms to prior theoretical constructs established in frameworks and in relationships between characteristics or individuals ([Law, 1997](#law97)). The two instruments that did test construct validity were the patient learning needs scale ([Galloway _et al._, 1993](#gal93)) and Dale _et al._'s ([2004](#dal04)) adaptation of the Toronto questionnaire ([Galloway _et al._, 1997](#gal97)). This scale does not provide factor loadings. The original Toronto questionnaire ([Galloway _et al._, 1997](#gal97)) is a fifty-one-item instrument with excellent scale construction and adequate reliability. Another seminal instrument, the Thurstone scaling of information needs ([Bilodeau and Degner, 1996](#bil96)), also a widely applied instrument, demonstrates excellent scale construction and adequate reliability. Dale _et al._ ([2004](#dal04)) chose the Toronto questionnaire to test the information needs of prostate cancer patients and developed the instrument further in its adaptation by testing for both content and construct validity. The result is a shorter instrument having only items with excellent factor loadings =0.5 (Table 3). <a id="factorloadings">As described earlier</a>, in discussing factor loadings to establish content validity of the same instrument, factor analysis is a statistical method in which correlations of responses are clustered to form groupings of items to represent concepts that must make intuitive sense to be valid. Selection of groupings of items based on only high factor loadings establishes construct validity ([Table 3](#app)).

Construct validity could not be determined, or information was not available, for Raupach and Hiller ([2002](#rau02)), Jenkins _et al._ ([2001](#jen01)) and Meredith _et al._ ([1996](#mer96)).

### Reliability

Researchers assess reliability of an instrument to see if repeatedly applying a particular technique to the same object would yield the same result each time. In essence, the tests gauge the repeatability of a technique. Test-retest is a technique applied to check reliability. Internal consistency, the type of reliability test applied most often in surveys, examines how questionnaire items relate to each other. Cronbach's alpha is a widely used measure to assess internal consistency.

Most instruments were tested for some form of reliability. Internal consistency was evaluated by Cronbach's alpha and intercorrelations. <a id="intercorrelations">Intercorrelations</a> can be observed through a simple correlation matrix. Cronbach's alpha is the average inter-item correlation adjusted for the length of the test, where the longer the test is, the more reliable it is. Excellent global Cronbach's alpha were reported for Stewart _et al._ ([2000](#ste00)) and Iconomou _et al._ ([2002](#ico02)), 0.93 and 0.95, respectively. Excellent sub-scale values are reported for the Toronto questionnaire ([Galloway _et al._, 1997](#gal97)), ranging from 0.81 to 0.96\. Excellent intercorrelations are also reported for the Toronto questionnaire sub-scales, ranging from 0.78 to 0.88\. Dale _et al._ ([2004](#dal04)) report excellent Cronbach's alpha for both global, 0.91, and sub-scales: basics of prostate cancer, disease management, psychosocial and physical wellbeing, and self-help, ranging from 0.67 to 0.87 ([Table 3](#app)).

The present study used reliability and agreement to evaluate reproducibility. Researchers reported the following statistics: test-retest correlations, Kendall's coefficient for inter-observer reliability and intra-observer reliability, Kappa coefficient and percentage agreements (Table 3).

Test-retest correlations are reported only for the patient learning needs scale ([Galloway _et al._, 1993](#gal93)). <a id="test-retest">Test-retest</a> is applied to assess consistency of a measure from one time to another. In this test, correlations are used to assess the consistency over time. For the scale, Galloway _et al._ ([1993](#gal93)) report values for both the total scale, 0.97, and sub-scales, ranging from 0.80 to 0.90\. Intra-observer reliability, a form of test-retest, is reported for Feldman-Stewart _et al._'s ([2010](#fel10)) instrument and Thurstone scaling of information needs ([Bilodeau and Degner, 1996](#bil96)). Additionally, Kendall's coefficients are reported for Thurstone scaling of information needs. <a id="kendallcoef">Kendall's</a> coefficient is a correlation of ordinal scale items. <a id="kappa">Kappa</a> and weighted Kappa coefficients are reported for the Feldman-Stewart _et al._ instrument. The Kappa coefficient is the measure of inter-observer variability in ratings on a nominal or ordinal scale. For weighted Kappa, less weight is assigned to agreements for categories that are further apart. Both Kendall's coefficients and Kappa coefficients for the instruments are rated poor ([Table 3](#app)).

Feldman-Stewart _et al._'s study is the only one to report percentage agreement for its instrument. Percentage agreement is also a measure of reliability to assess inter-observer reliability, that is, consistency in ratings between different raters or observers. The difference between Kappa coefficient and percentage agreement is that the first adjusts for variability due to chance alone. Feldman-Stewart _et al._ report agreement on individual items, ranging from 26% to 75% and agreement across all items to be 43% and when weighted to be 56% ([Table 3](#app)).

The Kendall's zeta in Thurstone scaling of information needs demonstrates more complex reliability testing. It is 0.95, which is excellent. Kendall's zeta '_was calculated to evaluate if the number of triads resulted from chance or inconsistent logic_' ([Deane and Degner, 1998, p. 120](#dea98)). A triad occurred when a participant chose three as more important than one in possible pairs of information needs ranked one, two and three ([Table 3](#app)).

Reliability could not be determined, or information was not available, for seven of fifteen instruments: Raupach and Hiller ([2002](#rau02)), Jenkins _et al._ ([2001](#jen01)), Meredith _et al._ ([1996](#mer96)), knowledge, attitudes, and beliefs questionnaire ([Cohn _et al._, 2003](#coh03)), Andreassen _et al._ ([2007](#and07)), Jatsch _et al._ ([2004](#jat04)) and the problems and needs in palliative care questionnaire ([Voogt _et al._, 2005](#voo05)).

## Overall assessment

Overall reliability of instruments when reported was often adequate (Table 3). Dale _et al._ ([2004](#dal04)) has excellent reliability. Feldman-Stewart _et al._ ([2010](#fel10)) is rated poor. Also, overall reliability could not be determined, or information was not available, for seven of fifteen instruments. Criterion validity is non-existent. Hence, it is rated poor across the board. Construct validity is poor for most instruments, except the patient learning needs scale ([Galloway _et al._, 1993](#gal93)), which has adequate construct validity, and Dale _et al._ ([2004](#dal04)), which has excellent construct validity. Construct validity could not be determined, or information was not available, for Raupach and Hiller ([2002](#rau02)), Jenkins _et al._ ([2001](#jen01)) and Meredith _et al._ ([1996](#mer96)). Content validity is adequate or excellent for most instruments, except Iconomou _et al._ ([2002](#ico02)). Content validity could not be determined, or information was not available, for Jenkins _et al._ ([2001](#jen01)) and Meredith _et al._ ([1996](#mer96)). Content validity contributes to the adequate overall validity rating for several instruments. Overall validity for all instruments, except Jenkins _et al._ ([2001](#jen01)) and Meredith _et al._ ([1996](#mer96)), is rated only adequate as these instruments generally failed to meet the requirement of having more than two well-designed studies to establish excellent validity rating. In summary, overall rating of the instrument itself (last column in Table 3) is adequate for only half of the instruments and indeterminable for the other half of evaluated instruments.

## Discussion and limitations

Most dimensions are relevant to the development of different types of cancer with the exception of a few specific items, such as skin care in lung cancer or melanoma, which did not have an information needs assessment tool. Also, an advanced stage of disease demands increased emphasis on physical and psychosocial care. Clinical information - information on diagnosis, prognosis, treatment and side effects - is certainly more important in the development of all measures. As discussed in the literature ([Pinquart and Duberstein, 2004](#pin04)) and the dimensions of instruments, treatment information and awareness of side effects take precedence. However, some differences in need for information in different countries may exist. For instance, palliative care in the Netherlands ([Voogt _et al._, 2005](#voo05)) may offer more information on alternative therapies or alternative living options due to a higher level of acceptance among citizens, and also on care settings due to a different social care structure. Most instruments evaluated in this study have been designed and applied in the Western world, which includes more economically advanced countries; hence, these instruments encompass more similarities than differences. The situation also points to education that undeveloped or developing countries may lack.

Content validity, found in the present review, is mostly established through consensus of experts or through literature review for instruments to measure the information needs of cancer patients. Factor analysis was utilized in only two of fifteen instruments to establish content and/or construct validity. Content and construct validity are critical in establishing a theoretical foundation. Also, there is no evidence of agreement with another measure, a standard, to establish criterion validity in any of the fifteen instruments. Reliability and agreement in the way of internal consistency and test-retest, when reported, is excellent or adequate which can be easily accomplished in testing for other instruments.

The present study is the first to evaluate measures of information needs in cancer patients. While no measure meets all of Law's ([1997](#law97)) requirements, the Toronto questionnaire ([Galloway _et al._, 1997](#gal97)), Thurstone scaling of information needs ([Bilodeau and Degner, 1996](#bil96)), and the adaptation by Dale _et al._ ([2004](#dal04)) of the Toronto questionnaire are instruments that meet more of Law's criteria compared with other instruments. These instruments may be viewed as the models to build upon in developing further instrumentation to benefit information delivery for cancer patients in direct care or educational programmes to improve patient outcomes, as well as to help them cope.

A limitation of the present study is that data were often missing or difficult to ascertain in studies that applied the instruments. Only a few instruments have reports specifically on their testing.

## Conclusion

The present study provides useful information for clinicians and researchers about different instruments available to measure the information needs of cancer patients, and the strengths and limitations of those instruments. There is no instrument that meets all of Law's ([1997](#law97)) criteria for excellence or even adequacy. The literature on cancer patients' information needs revealed substantive measures. However, more reliability and rigorous validity testing is required. The Toronto questionnaire ([Galloway _et al._, 1997](#gal97)) and Thurstone scaling of information needs ([Bilodeau and Degner, 1996](#bil96)) are substantive, reliable and valid measures of information needs among cancer patients. Although criterion validity is lacking for all three instruments, Dale _et al._ ([2004](#dal04)), in their adaptation of the Toronto questionnaire, illustrate further development in content and construct validity through factor analysis. Dale _et al._'s instrument is a shorter twenty-item instrument that can be more easily administered, while longer instruments are considered more reliable in psychometrics. Dale _et al._ also included patients, a significant group for feedback, as a source in scale construction. the Toronto questionnaire and Thurstone scaling of information needs and their adaptations, such as those of Dale _et al._, can be applied in different cancer patient populations to deliver information, to design educational programmes or to conduct further examination of the information that facilitates improved patient outcomes and coping in cancer.

The gaps identified in current instruments call for more reliability tests that are required across the board. Construct and criterion validity must also be established for measures to understand the information needs of cancer patients. Existing testing for measures of cancer patients' information needs, however, do suggest methods that can be used to establish more rigorous and comprehensive measures.

## About the author

**Kalyani Ankem** is an Assistant Professor of Health Informatics in the College of Informatics, Northern Kentucky University, USA. Her current research interests include the impact of clinical informatics applications on patient outcomes and effective implementation of SNOMED CT in clinical information systems. She can be contacted at [ankemk1@nku.edu](mailto:ankemk1@nku.edu)




#### References

*   Andreassen, S., Randers, I., Näslund, E., Stockeld, D. & Mattiasson, A.C. (2007). Information needs following a diagnosis of oesophageal cancer; self-perceived information needs of patients and family members compared with the perceptions of health care professionals: a pilot study. _European Journal of Cancer Care, 16_(3), 277-285.
*   Arora, N.K., Johnson, P., Gustafson, D.H., McTavish, F., Hawkins. R.P. & Pingree, S. (2002). Barriers to information access, perceived health competence, and psychosocial health outcomes: test of a mediation model in a breast cancer sample. _Patient Education and Counseling, 47_(1), 37-46.
*   Ankem, K. (2005). [Types of information needs among cancer patients: a systematic review.](http://www.webcitation.org/6WnzPCMtW) _LIBRES, 15_(2). Retrieved from http://libres-ejournal.info/858/ (Archived by WebCite® at http://www.webcitation.org/6WnzPCMtW)
*   Bilodeau, B.A. & Degner, L.F. (1996). Information needs, sources of information, and decisional roles in women with breast cancer. _Oncology Nursing Forum, 23_(4), 691-696.
*   Charles, C.A., Whelan, T., Gafni, A., Willan, A. & Farrel, S. (2003). Shared treatment decision making: what does it mean to physicians? _Journal of Clinical Oncology, 21_(5), 932-936.
*   Cassileth, B.R., Zupkis, R.V., Sutton-Smith, K. & March, V. (1980). Information and participation preferences among cancer patients. _Annals of Internal Medicine, 92_(6), 832-836.
*   Cohn, W.F., Ropka, M.E., Jones, S.M. & Miesfeldt, S. (2003). Information needs about hereditary breast cancer among women with early-onset breast cancer. _Cancer Detection and Prevention, 27_(5), 345-352.
*   Dale, J., Jatsch, W., Hughes, N., Pearce, A. & Meystre C. (2004). Information needs and prostate cancer: the development of a systematic means of identification. _BJU International, 94_(1), 63-69.
*   Deane, K.A. & Degner, L.F. (1998). Information needs, uncertainty, and anxiety in women who had a breast biopsy with benign outcome. _Cancer Nursing, 21_(2), 117-126.
*   Degner, L.F., Kristjanson, L.J., Bowman, D., Sloan, J.A., Carriere, K.C., O'Neil. J., … Mueller, B. (1997). Information needs and decisional preferences in women with breast cancer. _JAMA, 277_(18), 1485-1492.
*   Feldman-Stewart, D., Capirci, C., Brennenstuhl. S., Tong, C., Abacioglu. U., Gawkowska-Suwinska, M., … Wördehoff, H. (2010). Information needs of early-stage prostate cancer patients: a comparison of nine countries. _Radiotherapy and Oncology, 94_(3), 328-333.
*   Galloway, S.C., Bubela, N., McKibbon, A., McCay, E. & Ross, E. (1993). Perceived information needs and effect of symptoms on activities after surgery for lung cancer. _Canadian Oncology Nursing Journal, 3_(3), 116-119.
*   Galloway, S., Graydon, J., Harrison, D., Evans-Boyden, B., Palmer-Wickham, S., Burlein-Hall, S., … Blair, A. (1997). Informational needs of women with a recent diagnosis of breast cancer: development and initial testing of a tool. _Journal of Advanced Nursing, 25_(6), 1175-1183.
*   Hack, T.F., Degner. L.F. & Dyck, D.G. (1994). Relationship between preferences for decisional control and illness information among women with breast cancer: a quantitative and qualitative analysis. _Social Science & Medicine, 39_(2), 279-289.
*   Iconomou, G., Viha, A., Koutras, A., Vagenakis, A.G. & Kalofonos, H.P. (2002). Information needs and awareness of diagnosis in patients with cancer receiving chemotherapy: a report from Greece. _Palliative Medicine, 16_(4), 315-321.
*   Jenkins, V., Fallowfield, L. & Saul, J. (2001). Information needs of patients with cancer: results from a large study in UK cancer centres. _British Journal of Cancer, 84_(1), 48-51.
*   Kim, S.P., Knight, S.J., Tomori, C., Colella, K.M., Schoor, R.A., Shih, L., … Bennett, C.L. (2001). Health literacy and shared decision making for prostate cancer patients with low socioeconomic status. _Cancer Investigation, 19_(7), 684-691.
*   Law, M. & Letts, L. (1989). A critical review of scales of activities of daily living. _The American Journal of Occupational Therapy, 43_(8), 522-528.
*   Law, M. (1997). _Outcome measures rating form guidelines._ Unpublished paper, McMaster University, Hamilton, CA.
*   Leydon, G.M., Boulton, M., Moynihan, C., Jones, A., Mossman, J., Boudioni, M. & McPherson, K. (2000). Cancer patients' information needs and information seeking behaviour: in depth interview study. _BMJ, 320_(7239), 909-913.
*   Mastaglia, B. & Kristjanson, L.J. (2001). Factors influencing women's decisions for choice of surgery for Stage I and Stage II breast cancer in Western Australia. _Journal of Advanced Nursing, 35_(6), 836-847.
*   Meredith, C., Symonds, P., Webster. L., Lamont, D., Pyper, E., Gillis, C.R. & Fallowfield, L. (1996). Information needs of cancer patients in west Scotland: cross sectional survey of patients' views. _BMJ, 313_(7059), 724-726.
*   Miles, A., Voorwinden, S., Chapman, S. & Wardle, J. (2008). Psychologic predictors of cancer information avoidance among older adults: the role of cancer fear and fatalism. _Cancer Epidemiology, Biomarkers & Prevention, 17_(8), 1872-1879.
*   Montie, J.E. (1994). Counseling the patient with localized prostate cancer. _Urology, 43_(2 Suppl), 36-40.
*   Pierce, P.F. (1996). When the patient chooses: describing unaided decisions in health care. _Human Factors, 38_(2), 278-287.
*   Pinquart, M. & Duberstein, P.R. (2004). Information needs and decision-making processes in older cancer patients. _Critical Reviews in Oncology/Hematology, 51_(1), 69-80.
*   Raupach, J.C. & Hiller, J.E. (2002). Information and support for women following the primary treatment of breast cancer. _Health Expectations, 5_(4), 289-301.
*   Rutten, L.J., Arora, N.K., Bakos, A.D., Aziz, N. & Rowland, J. (2005). Information needs and sources of information among cancer patients: a systematic review of research (1980-2003). _Patient Education and Counseling, 57_(3), 250-261.
*   Stewart, D.E., Wong, F., Cheung, A.M., Dancey, J., Meana, M., Cameron, J.I., … Rosen, B. (2000). Information needs and decisional preferences among women with ovarian cancer. _Gynecologic Oncology, 77_(3), 357-361.
*   Voogt, E., van Leeuwen, A.F., Visser, A.P., van der Heide, A. & van der Maas, P.J. (2005). Information needs of patients with incurable cancer. _Supportive Care in Cancer, 13_(11), 943-948.


# Appendix

<table class="center" style="width:95%;" id="table3"><caption>  
Table 3: Evaluation of instruments measuring cancer patients' information needs  
Evaluation rating: E = Excellent; A = Adequate; P = Poor</caption>

<tbody>

<tr>

<th>Instrument</th>

<th>Scale</th>

<th>Level</th>

<th>Reliability</th>

<th>Validity</th>

<th>Overall</th>

</tr>

<tr>

<td>**Toronto questionnaire: Breast cancer patients**  
_Subscales_: Disease  
Investigative tests  
Treatments  
Physical  
Psychosocial  
_Scoring_: 1 (not important) - 5 (extremely important)</td>

<td>Experts  
Literature (E)</td>

<td>I/R  
51 items</td>

<td>Internal consistency  
[_Cronbach's alpha_](#cronbach)  
0.81-0.96 (E)  
[_Intercorrelations_](#intercorrelations)  
0.78-0.88 (E)  
Reliability Overall (A)</td>

<td>_Content_  
Consensus (E)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(A)</td>

</tr>

<tr>

<td>**PLNS: Self-management of health care after hospital discharge  
among lung cancer patients**  
_Subscales_: Medication  
Activities of living  
Community and follow-up  
Feelings and condition  
Treatment and complications  
Enhancing quality of life  
Skin care  
_Scoring_: 1 (not important) - 5 (extremely important)</td>

<td>No method (P)</td>

<td>I/R  
50 items</td>

<td>[Test-retest](#test-retest)  
_Total_  
0.97 (E)  
_Subscales_  
0.80-0.90 (E)  
Reliability Overall (A)</td>

<td>_Content_  
Literature (E)  

_Construct_  
[_Factor analysis_](#factoranalysis) (A)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(A)</td>

</tr>

<tr>

<td>**Thurstone scaling of information needs: Breast or prostate  
cancer patients**  
_Items_: Stage of disease  
Likelihood of cure  
Treatment options  
Side effects  
Risk to relatives  
Social activities  
Self-care issues  
Sexuality</td>

<td>Literature (E)</td>

<td>O  
9 items</td>

<td>Observer  
Intra-observer  
[_Kendall's zeta_](#kendallzeta)  
0.99 (E)  
[_Kendall's coefficient_](#kendallcoef)  
0.35 (P)  
Reliability Overall (A)</td>

<td>_Content_  
Consensus (E)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(A)</td>

</tr>

<tr>

<td>**Questionnaire (Stewart _et al._)**  
_Scoring_: 1 (not important) - 5 (essential)</td>

<td>Literature  
Clinical (E)</td>

<td>I/R  
43 items</td>

<td>Internal consistency  
[_Cronbach's alpha_](#cronbach)  
0.95 (E)  
Reliability Overall (A)</td>

<td>_Content_ (A)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(A)</td>

</tr>

<tr>

<td>**Questionnaire (Iconomou _et al._)**  
_Scoring_: 1 (not at all) - 4 (very much)</td>

<td>No method (P)</td>

<td>I/R  
13 items</td>

<td>Internal consistency  
[_Cronbach's alpha_](#cronbach)  
0.93 (E)  
Reliability Overall (A)</td>

<td>_Content_ (P)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (P)</td>

<td>(I)</td>

</tr>

<tr>

<td>**Questionnaire (Raupach & Hiller)**  
_Scoring_: essential, desired, no opinion, avoid</td>

<td>Experts  
Literature (E)</td>

<td>[N](#levelslikert)  
13 items</td>

<td>Reliability Overall (P)</td>

<td>_Content_  
Consensus (E)  

_Construct_ (I)  

_Criterion_ (I)  

Validity Overall (A)</td>

<td>(I)</td>

</tr>

<tr>

<td>**Questionnaire (Jenkins _et al._)**</td>

<td colspan="4">No information</td>

<td>(I)</td>

</tr>

<tr>

<td>**Questionnaire (Meredith _et al._)**  
Patients preference for information in general  
Specific questions about certain aspects of their illness and  
treatment  
_Subscales_: Whether it was cancer  
Specific name of the disease  
Progress of treatment  
Chances of cure  
Details on possible side effects  
How does treatment work  
Knowledge about other types of treatment  
_Scoring_: absolute need to know, would like to know, did not  
want to know about particular details of their illness</td>

<td colspan="4">No information</td>

<td>(I)</td>

</tr>

<tr>

<td>**Questionnaire (Feldman-Stewart _et al._): Early-stage prostate  
cancer patients**  
_Subscales_: Epidemiology and natural history of disease  
Effectiveness and side effects of treatments  
Experience of the physician and the treatment centre  
_Scoring_: essential, desired, no opinion, avoid  
Retrospective survey of importance of information in treatment  
decision</td>

<td>Experts  
Clinical  
Cancer patients  
Lay persons (A)</td>

<td>[O](#levelslikert)  
93 items</td>

<td>Observer  
Inter-observer  
[_Kappa_](#kappa)  
0.17 (P)  
[_Weighted Kappa_](#kappa)  
0.3 (P)  
[_Percentage agreements_](#percentagreement) _on individual items_  
ranged from 26%-75%  
[_Percentage agreement_](#percentagreement) _across all items_  
43%  
[_Weighted percent agreement_](#percentagreement) _across all items_  
56%  
Note: 88 items were deemed essential or desired  
by at least 50%  
Reliability Overall (P)</td>

<td>_Content_  
Consensus (E)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(A)</td>

</tr>

<tr>

<td>**Knowledge, attitudes and beliefs questionnaire: Early-onset  
hereditary breast cancer**</td>

<td>Experts  
Literature  
Clinical  
Patients (E)</td>

<td>(I)</td>

<td>Reliability Overall (P)</td>

<td>_Content_  
Consensus (E)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(I)</td>

</tr>

<tr>

<td>**Questionnaire (Andreassen _et al._): Oesophageal cancer patients**  
_Subscales_: Anatomy/physiology  
The illness  
Tests/treatment  
Information methods  
Self-care  
Psychosocial aspects  
Healthcare professionals competence  
_Scoring_: 0 (not at all applicable, 1 (not important),  
2 (somewhat important), 3 (important), 4 (very important)</td>

<td>Experts  
Literature  
Clinical  
Patients and family members (E)</td>

<td>I/R  
64 items</td>

<td>Reliability Overall (A)</td>

<td>_Content_  
Consensus (E)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(A)</td>

</tr>

<tr>

<td>**Questionnaire (Jatsch _et al._): Prostate cancer patients**  
_Scoring_: 1 (very important) - 4 (completely unimportant)</td>

<td>Clinical  
Patients (E)</td>

<td>I/R  
20 items</td>

<td>Reliability Overall (P)</td>

<td>_Content_  
Consensus (A)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(I)</td>

</tr>

<tr>

<td>**Problems and Needs in Palliative Care Questionnaire (PNPC):  
Incurable cancer patients**  
_Subscales_: Helpful devices  
Organizations that provide help and devices  
Cause of cancer  
Treatment options and side effects  
Physical symptoms that may be expected  
Alternative medicine  
Euthanasia  
Food and diet  
Sexuality  
Psychosocial care  
Complementary care  
Care settings  
_Scoring_: informed sufficiently, informed to some extent, not  
informed about these topics</td>

<td>Patients  
Relatives (A)</td>

<td>O  
12 items</td>

<td>Reliability Overall (P)</td>

<td>_Content_  
Consensus (A)  

_Construct_ (P)  

_Criterion_ (P)  

Validity Overall (A)</td>

<td>(I)</td>

</tr>

<tr>

<td>**Adaptation of the Toronto questionnaire (Dale _et al._): Information needs of  
patients with prostate cancer at the earliest stages**  
_Subscales_: Basics of prostate cancer care  
Disease management  
Psychosocial & physical well being  
Self-help  
_Scoring_: 1 (very important) - 4 (completely unimportant)</td>

<td>Literature  
Clinical  
Patients (E)</td>

<td>I/R  
20 items</td>

<td>Internal consistency  
[_Cronbach's alpha_](#cronbach) _combined_  
0.91 (E)  
Basics of prostate cancer care  
0.87  
Disease management  
0.80  
Psychosocial & physical well being  
0.73  
Self-help  
0.67  
Reliability Overall (E)</td>

<td>_Content_  
Consensus (E)  
[_Factor analysis_](#factoranalysis)  
_Correlations_  
=0.25 (A)  

_Construct_  
[_Factor analysis_](#factoranalysis)  
[_Factor loadings_](#factorloadings)  
=0.5 (E)  

_Criterion_ (I)  

Validity Overall (A)</td>

<td>(A)</td>

</tr>

</tbody>

</table>

