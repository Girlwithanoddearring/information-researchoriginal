#### vol. 20 no. 2, June, 2015

# Spirituality and everyday information behaviour in a non-Western context: sense-making in Buddhist Laos

#### [Nicole M. Gaston](#author)  
Open Polytechnic of New Zealand, 3 Cleary Street, Waterloo, Lower Hutt, New Zealand  
[Dan G. Dorner](#author) and [David Johnstone](#author)  
Victoria Univeristy of Wellington, 23 Lambton Quay, Wellington, New Zealand

#### Abstract

> **Introduction.** We report an investigation into everyday information behaviour in Laos, a non-Western context, that revealed religious and spiritual beliefs played a significant role in information activities. This paper explores the significance of this finding and its implications.  
> **Method.** Qualitative data were collected in the form of semi-structured interviews in using the critical incident technique and Dervin's micro-moment timeline interview instrument with thirty participants in Laos. Interviews ranged from thirty to ninety minutes and were audio recorded and transcribed to provide data for analysis.  
> **Analysis.** Interview data were analysed using a three-phase analysis process based on Dervin's sense-making metaphor and Gee's discourse analysis tools.  
> **Results.** Analysis revealed that the primary contextual factors affecting information behaviour among the participants were their social and cultural environments, with the religious context embedded within the social and cultural values playing a significant role.  
> **Conclusions.** Our study supports Kari's that information can be acquired by spiritual means, and that the conventional conception of information behaviour as happening through the five physical senses is deficient. Rather, these metaphysical ways of knowing should be taken into consideration in the conceptualisation of information behaviour.

## Introduction

This paper explores one key finding from a doctoral research project into the everyday information behaviour of individuals in Laos, a country located in Southeast Asia. The research followed the tradition of user-centred approaches to investigating information behaviour that aim to understand the cognitive, sociological, and affective aspects of human information behaviour (e.g., [Kuhlthau, 1991](#kuh91); [Nahl, 1997](#nah97), [2005](#nah05), [2007](#nah07); [Pettigrew, 1999](#pet99)).  There have been few studies of information behaviour and spirituality in the non-Western context, therefore the strong presence of spirituality as a contextual factor affecting how individuals needed, sought and used information in their everyday lives and the implications of this finding are explored in this paper.

Kari ([2007](#kar07), p. 958) called for '_more empirically and methodologically-oriented research_' relating to spirituality and its effects on information behaviour which he suggested was '_needed on all fronts in order to build up a systematic knowledge base_'. This paper attempts to address that gap by providing empirical evidence gathered through the development of a robust methodology to illustrate some important findings regarding spirituality in information behaviour. Therefore, our study also tests as well as extends the existing information behaviour models, leading to further development of information behaviour theory.

The two broad research questions asked in the doctoral study related to the role of context in information behaviour in a non-Western society using the example of Laos. These research questions were:

*   What are the contextual factors that affect information behaviour in a non-Western, developing country?
*   How do these contextual factors affect information behaviour in a non-Western, developing country?

Analysis revealed the social, cultural, and spiritual contexts of individuals had a strong influence on their information behaviour. The emergence of social and cultural factors as playing the primary role in influencing individuals' information behaviour was not surprising. Research by Paisley ([1968](#pai68)), Wilson (([1997](#wil97)), [1981](#wil81)), Cox ([2012](#cox12)), Tuominen and Savolainen ([1997](#tuo97)) among others suggests that social and cultural values play a significant role in information behaviour. However, the emergence of religious beliefs, or spirituality, as playing a central role in information behaviour was not expected to the extent that was demonstrated in the data.

## Justification for the research

Religion has not played a very significant role in most broad studies of information behaviour emanating from Western contexts. While spirituality has emerged as an important dimension of information behaviour in previous information behaviour studies specifically exploring groups such as church goers ([Freeburg, 2013](#fre13)), members of the clergy ([Wicks, 1999](#wic99)) and cancer patients ([Fourie, 2008](#fou08)), our study was a general investigation of everyday information behaviour in a non-Western society within a sample of participants intended to be representative of the demographic of Laos. Remarkably, even amongst this general Lao population's everyday information activities, spirituality emerged as an important contextual factor affecting information needs, seeking and use. This finding is generally not seen to the same extent amongst most Western investigations of information behaviour examining general populations or in everyday information behaviour studies. Therefore we believe this finding merits some further investigation.

Research into culture and behaviour by Nisbett ([2003](#nis03)), Peters ([2007](#pet07)), and Kityama ([Kitayama and Park, 2010](#kit10); [Kitayama and Uskul, 2011](#kit11); [Markus and Kitayama, 1991](#mar91)) provides evidence that cultural and social factors play a significant role in cognitive processes including information needs, seeking and use. These authors' findings provided additional motivation for undertaking the doctoral research and support the results presented in this paper.

## Background

Laos, or the Lao People's Democratic Republic, a country located in South-East Asia within the Greater Mekong Subregion, is home to six and a half million inhabitants ([Lafont, 2013.](#laf13)). Laos and the Lao people was the setting for an investigation into everyday information behaviour in a non-Western society explored in a doctoral thesis ([Gaston, 2014](#gas14)). Laos is ranked 139 out of 186 countries in the United Nations Human Development Index ([United Nations Development Programme, 2015](#uni15)), at the lower end of the United Nation's ranking system that measures international development.

Laos is a country with a rich cultural history that has long been tied to its official religion, Buddhism. Of particular significance is the designation and implementation of Buddhism as the state religion ([Holt, 2009](#hol09)). This understanding of the historical and religious contexts of Laos, informed by time spent living in Laos by the lead researcher, discussions with Lao people and number of recent monographs, including _Spirits of the place_ by John Holt ([2009](#hol09)) and _Post-war Laos_ by Vatthana Pholsena ([2006](#pho06)), framed the development of the research.

Laos was selected as the setting for our research because of the unique social and historical context mentioned above and for the reason that the lead researcher has extensive experience living and working in Laos. From 2008-2009 the lead researcher worked as a technical advisor in information literacy to the National University of Laos's Central Library where she was immersed in Lao culture. She also lived and worked in Africa from 2003-2005, during which time she underwent extensive cross-cultural training as a Peace Corps volunteer in Benin, West Africa.  The aim of this training was to facilitate international development through the cultivation of cultural sensitivity and awareness widely considered fundamental to effective collaboration amongst people of different cultures. Laos was therefore ideal for investigation into the contextual factors affecting information behaviour as the lead researcher had a thorough understanding of the culture and language, as well as personal contacts willing to facilitate participant recruitment. This research was therefore undertaken from a holistic perspective of culture that is appreciative rather than deficit. All of these factors influenced the development of the research project and are reflected in the data collection, analysis, and interpretation of finding described in this paper.

The terms _developing country_ and _non-Western society_ are both used throughout this paper with regard to Laos, yet require some clarification. These terms have quite different meanings, and have not been used interchangeably. For lack of an acceptable alternative, _developing country_ has been used to describe countries that have not developed economically to the same extent as more developed countries such as nations with OECD (organization for Economic Co-operation and Development) membership. However, use of this term does not imply inferiority to more developed countries. There is neither a standard definition of a developing country, nor any specific statistical indicator threshold or rankings to determine if a country is developed or developing. Nevertheless, the term developing country has entered common usage to describe countries with lower economic development levels. Developing country has no explicit geographical or cultural connotations, but refers merely to economic development. Designation as such is often based on economic indicators including gross domestic product and/or per capita income levels.

On the other hand, _non-Western society_ can refer to any society that does not possess a dominant core value system originating in any of the Western European cultures. This term can include countries that are highly developed economically, such as South Korea, Singapore, and Japan, as well as many developing countries throughout the world. The context of this research, Laos, is considered to be both a non-Western society and a developing country.

The study was intentionally limited to investigatinging the role of context in information behaviour in Vientiane, Laos. This delimitation was imposed due to the magnitude of the research objectives, so that they could be manageably addressed.

## Overview of information behaviour

Information can be conceptualised as one level of understanding on a continuum of human interaction with the universe. Raw facts, or data, become information once they are perceived by an individual. This information may then go on to become knowledge, and finally, wisdom. It is with this conceptualisation of data, information, knowledge and wisdom that an investigation into everyday information needs, seeking and use of a generally representative sample of wider society in the non-Western context of Laos was undertaken.

Pettigrew, Fidel, and Bruce ([2001](#pet01)) define information behaviour as '_how people need, seek, manage, give and use information in different contexts'_ (p.44). The research discussed in this paper was primarily informed by Dervin's ([2008](#der08)) sense-making metaphor theory of information behaviour, in which information seeking is conceptualised as a gap-bridging activity aimed at making sense of confusions or questions encountered in one's daily life. The sense-making metaphor as illustrated by Dervin is shown in Figure 1 below.

<figure class="centre">![](p665fig1.jpg)

<figcaption>Figure 1: Dervin's sense-making metaphor ([Dervin, 2008](#der08), p. 17)</figcaption>

</figure>

The sense-making metaphor is summarised by Naumer, Fisher and Dervin ([2008](#nau08)) as follows:

> A person is seen as embedded in a context-laden situation, bounded in time-space. The person pictured as crossing a bridge is used to metaphorically describe the way that humans are mandated by the human condition to bridge gaps in an always evolving and ever-gappy reality. The person is seen facing a gap (i.e., a sense-making need) that arises out of a situation. Through the process of gap bridging, people seek inputs (sometimes the stuff systems call information) and engage in other activities through the time-space continuum that lead to outcomes. (p. 2)

Dervin's sense-making metaphor and sense-making methodology therefore became central to the undertaking of the research due to their emphasis on the context of an individual's information behaviour. The investigation into the everyday information behaviour of individuals in Laos was also influenced by complementary theories of information behaviour including Wilson's ([1997](#wil97)) model of information behaviour and Chatman's ([Hersberger, 2005](#her05) ) theory of 'information poverty', among others (e.g., [Fisher and Naumer, 2006](#fis06); [Kuhlthau, 2005](#kuh05); [Miwa, 2005](#miw05)). An in-depth investigation into established conceptualisations of information behaviour provided a solid framework and background from which a suitable and informed methodology could be developed.

Research into information behaviour in developing countries thus far appears to have been limited to specific types of individuals such as academic faculty ([Patitungkho and Deshpande, 2005](#pat05)), fishermen ([Ikoja-Odongo and Ocholla, 2003](#iko03); [Njoku, 2004](#njo04)), or specific aspects of information behaviour such as electronic information behaviour ([Borzekowski, Fobil, and Asante, 2006](#bor06)), information needs or information seeking ([Momodu, 2002](#mom02)), rather than a comprehensive approach, as summarised in Dutta's ([2009](#dut09)) review of literature on information needs and information seeking in developing countries. These studies include investigating specific types of professional related information needs and seeking, such as '_The information needs and information-seeking behaviour of ?shermen in Lagos state, Nigeria_' ([Njoku, 2004](#njo04)) , or specific types of information needs, such as '_Responding to information needs of the citizens through e-government portals and online services in India_' ([Hirwade, 2010](#hir10)). Exploration of everyday information behaviour amongst the general population of a developing, non-Western society has not been extensive, and even less discussion of the role of religion and spirituality in such behaviour in a non-Western context is widely available. This paper also discusses that perceived gap in our understanding of information behaviour.

## Religion and information behaviour

This paper reports on an investigation into information behaviour in Laos, a country in which Buddhism is the predominant religion. The study explored the connections between religion and how individuals need, seek, and use information in their daily lives.  While the role of religion and/or spirituality in information studies research has been explored in different contexts, the majority relates to providing information services in religious organizations. The role of religious beliefs or spirituality in information behaviour has not been widely explored thus far, though Kari's 2007 review of the literature related to spirituality in information studies did include some descriptions of information processing and use, critical aspects of information behaviour.

Kari ([2007](#kar07)) conceptualised the spiritual in information behaviour similarly to the Oxford English Dictionary's definition of spiritual: '_of or relating to, affecting or concerning, the spirit or higher moral qualities, esp. as regarded in a religious aspect, frequently in express or implied distinction to bodily, corporal, or temporal_', a definition that also informs the current research. Kari ([2007](#kar07)) also describes spiritual as '_sacred matters_', '_higher powers_', or '_transcendent reality'_ (p. 945).

Religion can be defined as

> a set of beliefs concerning the cause, nature, and purpose of the universe, especially when considered as the creation of a superhuman agency or agencies, usually involving devotional and ritual observances, and often containing a moral code governing the conduct of human affairs….a specific fundamental set of beliefs and practices generally agreed upon by a number of persons or sects. ([Religion, 2012](#rel12))

Therefore these two terms, _religion_ and _spiritual_, are concerned with the metaphysical aspects of an individual's context, and have considerable overlap. Buddhism can be conceptualised as a religion, and the beliefs associated with it to be both religious and spiritual. However, in the case of Laos, the spiritual extends to beliefs beyond what is formally part of the organized religious system, though as these spiritual beliefs are still metaphysical in nature, they may be informed by the same underlying values that comprise the organized religion of Buddhism. In addition, Buddhism and the values associated with it are intertwined into many other aspects of everyday life for people in Laos, and therefore the religious and the spiritual in the Lao context are even more closely linked than in a more secular society with separation of church and state such as the United States or New Zealand.

Kari's ([2007](#kar07)) review identified discussions of the spiritual or religious and its relationship to information behaviour in a number of publications including Bates's ([2002](#bat02)) description of '_layers of understanding_' with '_spiritual_' at the topmost layer. However, as mentioned by Kari ([2007](#kar07)), '_Bates does not explain the potential roles of spirituality in information seeking, nor what the whole typology is founded on_' (p. 945). Nevertheless, it is one of the rare mentions of spiritual beliefs playing a role in information behaviour.

Chatman ([1987](#cha87)) reported on research into information behaviour of low-skilled workers, in which the Bible was reported as a source of information by over half the participants. Chatman ([1987](#cha87)) further suggested that understanding of biblical content was often mediated by personal interpretation or counsel from a member of the clergy (p. 946).

Kari's ([2007](#kar07)) survey of the spiritual in information studies literature led to, among others, conclusions relevant to information behaviour research, including:

1.  Information can be supposedly acquired by spiritual means (e.g., trance channelling);
2.  Information can originate from a source considered as a spiritual entity (e.g., God);
3.  Information processes can be allegedly affected by a spiritual agent (e.g., providence). (p. 957)

These conclusions regarding the spiritual or metaphysical in information behaviour can be contrasted with the logical in information behaviour, which can be conceptualised as traditional sources of information such as oral communication, written text, environmental cues, and electronic sources amongst other types of information.

Kari's ([2007](#kar07)) conclusions resulted from investigations into information behaviour conducted in Western contexts by Kari and others. However, the relevance of these conclusions in a non-Western context, where spiritual paradigms like Buddhism are dominant, remains unclear. The current research applies the connections between religion and information behaviour in a non-Western context.

## Context in information behaviour

According to Dervin ([1997](#der97)) there '_is no term that is more often used, less often defined, and when defined, defined so variously as context_' (p. 14). The _Oxford English Dictionary Online's_ definition of context has been slightly adapted to develop the following working definition used in this research:

> The totality of circumstances that form the setting for an event, statement, or idea, and through which understanding is constructed.

The understanding mentioned in this definition is conceptualised as being constructed amongst individuals, in line with a social constructionist perspective, described in more detail in the following section. Context, therefore, forms the background in which the individual, as well as his or her thoughts and behaviours, is embedded, and how he or she perceives this context affects how he or she interacts with information.

There are numerous dimensions of context that affect how individuals interact with and perceive the world around them. While these numerous dimensions of context, such as the physical environment, political system, or information resource base available play a significant role in how individuals interact with information, for this research we chose to emphasise how socio-cultural and cognitive-emotional dimensions of context affect information behaviour, as this has been largely unexplored in a non-Western context.

Ingwersen and Järvelin's Cognitive Framework in Information Seeking and Retrieval is one of the few frameworks of information behaviour to explicitly articulate the role of social and cultural context in information activities. While their framework limits itself to information seeking and retrieval, it provides a robust tool for conceptualising socio-cultural context and cognitive-emotional context in information behaviour that is very valuable for our investigation.

The cognitive framework of information seeking and retrieval model shown in Figure 2 is one of a number of variations on a visual representation of the framework developed by Ingwersen and Järvelin ([2005](#ing05)) illustrating how context is conceptualised as socially constructed and filtered by the information user's perceptions.

<figure class="centre">![Ingwersen and Järvelin's cognitive framework for information seeking and retrieval](p665fig2.jpg)

<figcaption>Figure 2: Ingwersen and Järvelin's cognitive framework for information seeking and retrieval ([2005](#ing05), p. 279)</figcaption>

</figure>

This model illustrates the central components of the cognitive framework for information seeking and retrieval centred around the information seeker who is located in the physical world constituted of '_directly observable entities_', as well as the role of contextual factors '_as when perceived by the information seeker at his/her cognitive-emotional level_' ([Ingwersen and Järvelin, 2005](#ing05), p. 278).

Parallels exist between Ingwersen and Järvelin's conceptualisation of information behaviour in their cognitive framework and Dervin's sense-making metaphor, including how Dervin's sense-maker is seen as '_embedded in a context-laden situation, bounded in time-space_' ([Naumer _et al._, 2008](#nau08), p. 2). Dervin describes how the situation component of sense-making includes an individual's '_experiences, horizons, history, habits and skills_', visualised as occurring under an umbrella of context, which includes '_power stuctures/dynamics, organizational systems/procedures, domain knowledge systems, and cultures/communities_' ([Naumer _et al._, 2008](#nau08), p. 2), not unlike the dimensions of context described in Ingwersen and Järvelin's model shown in Figure 2.

While Ingwersen and Järvelin's model particularly emphasises information technology in information seeking and retrieval rather than a more holistic conceptualisation of information behaviour encompassing information needs, seeking, and use, such as Dervin's sense-making metaphor, this model nonetheless articulates the important role of the individual's cognitive and emotional framework as well as the social and cultural contexts in his or her information activities. According to Ingwersen and Järvelin ([2005](#ing05)):

> The model emphasises the information processes that are executed during information seeking and retrieval over time: First, processes of social interaction are found between the actors and their past and present socio-cultural or organizational context. Social interaction may instigate information seeking and retrieval activities, but may also form part of their fulfilment. (p. 261)

Ingwersen and Järvelin go on to describe how information interaction takes place between the actors' cognitive processes and cognitive manifestations embedded in IT and information objects via interfaces. In the context of the research in Laos, where the majority of information behaviour incidents reported by participants did not involve IT, we can conceptualise the information objects as knowledge or cognitive manifestations (understanding) embedded within other individuals that the actor interacts with via the interface of oral communication.

As shown in Figure 2, an individual's socio-cultural framework influences his or her perception of his or her context. Markus and Kitayama ([1991](#mar91)) proposed 'that for many cultures of the world, the Western notion of the self as an entity containing significant dispositional attributes, and as detached from context, is simply not an adequate description of selfhood' (p. 225). This evidence of variations in conceptualisations of the self illustrates not only the diverse ways in which individuals may perceive themselves, but also the dynamic nature of how context is perceived, a consideration which is also illustrated in Ingwersen and Järvelin's model above.

The socio-cultural and cognitive-emotional dimensions of context potentially play a significant role in how individuals need, seek and use information, yet have thus far received little investigation with regard to information behaviour in non-Western contexts. Ingwersen and Järvelin's model was, therefore, a very useful tool for visualising how cognitive factors affect information behaviour, which provided additional evidence for the need to further investigate these contextual factors in information behaviour.

## Methodology

A social constructionist perspective providing 'a dialogic viewpoint to study the assumptions and implicit theories that people draw on when they engage in information practices and produce accounts of them' ([Tuominen, Savolainen, and Talja, 2005](#tuo05), p. 328) informed the development the research methodology. The research was undertaken based on the belief that accounts of information behaviour were representative of a socially constructed reality for the individual participant, and therefore also representative of the broader social and cultural context in which the information behaviour occurred. In addition, the social constructionist perspective informed the interpretivist epistemology in which data were interpreted based on the understanding of reality as socially constructed.

Data collection was undertaken during field work over a nine-week period in Vientiane, Laos in 2011\. An initial convenience sample of participants was recruited from the lead researcher's personal network, and further participants were recruited through snowballing methods relying on the personal networks of participants and the interpreter. While random sampling methods may provide data with less likelihood of bias or more representational data, research undertaken by an outsider is limited by the social, cultural, political, and linguistic context of Laos. However, the use of convenience sampling does not necessarily imply the data is unrepresentative or biased. According to Bryman ([2008](#bry08)) social research is often based on convenience sampling (p. 183).

While the initial sample was based on convenience, a purposive selection criterion was applied to potential participants. The purposive criterion was the selection of potentially information-rich cases. According to Patton ([1990](#pat90)), 'information-rich cases are those from which one can learn a great deal about issues of central importance to the purpose of research; thus the term purposeful sampling' (p. 169). For our study, information-rich cases were deemed to be those individuals who were able provide a great deal of data illustrative of their information behaviour. The interpreter helped to recruit individuals who had either unique or highly developed information behaviour, individuals who engaged in a wide variety of information behaviour on a regular basis, or who may have been able to richly articulate their information behaviour, which were all potentially information-rich cases.

These methods successfully yielded a sample of 30 participants from diverse educational, professional and socio-economic backgrounds with the aim of identifying a generally representative sample of individuals who identified as Lao Loum (the ethnic majority in Laos). Interviews ranged from thirty minutes to ninety minutes and averaged approximately one hour. When possible, interviews were conducted in English with participants who had good fluency in the English language. However, individuals with little English language fluency were not excluded from participation in order to avoid biasing the sample, and therefore ten of the thirty interviews were conducted in the Lao language with the aid of an interpreter.

Thirteen participants were male (44%), and seventeen female (56%), with an average age of 32.9 years, making the sample generally representative of overall Lao population demographics excluding those under 16 years old ([Central Intelligence Agency, 2013](#cen13)). Informed consent was obtained prior to interviewing and the interpreter also signed a confidentiality agreement, ensuring anonymity for participants and minimising potential ethical issues, and the relevant ethics committees' approval were obtained, from both the researchers' home institution, and the National University of Laos which oversees research projects conducted in the country.

Dervin's 'micro-moment timeline interview' ([2008](#der08)) instrument has been specifically designed to explore the context of an information behaviour incident, and therefore was ideally suited to the research questions. Dervin developed a semi-structured interview technique to capture sense-making situations, called sense-making methodology questioning. Dervin's questions were designed to be flexible and the following selected sense-making methodology questions were used as the basis for understanding the information behaviour of the study participants:

> What was your situation?  
> What stood in the way?  
> Did you have any questions in mind? What are they?  
> How do you try find the answer to these questions? Any help?  
> What conclusions/ideas/ did you come to?  
> Did you finally get an answer to your questions? Does it help? How?  
> How do you handle/deal with these useful answers?  
> Did you have any difficulties to get the answer? What are they?  
> How did you decide that the information you found was helpful (or not helpful)  
> What led you to that assessment?  
> What did that allow you to do/achieve/think?([Dervin, 2008](#der08), p. 19)

Additional prompts such as 'Tell me more...' and 'What happened then?' were also useful in eliciting rich accounts of information behaviour in the semi-structured interview style employed in this study. Data yielding from Dervin's roster of sense-making interview questions were analysed using a three phase analysis process combining two primary analytic tools described in Table 1\. Dervin's sense-making metaphor, and its five components (situation, gap, bridge, sense-making, outcome) became a framework for analysis, and descriptions of information incidents were coded according to which component of sense-making they described. The same interview data was then analysed using Gee's ([2011b](#gee11b)) 'seven building tasks of language' discourse analysis framework, in which accounts were examined for evidence of the underlying contextual factors present in the interview. Finally, the sense-making behaviours identified during the first phase of analysis were mapped to the contextual factors identified during the second phase, revealing how those contextual factors affected the information behaviour captured during data collection. Table 1 summarises the phases of data analysis.

<table class="center"><caption>  
Table 1: Phases of data analysis  
</caption>

<tbody>

<tr>

<th>Phase</th>

<th>Objective</th>

<th>Framework</th>

<th>Overview</th>

</tr>

<tr>

<td>Phase one</td>

<td>Identify everyday information behaviour of the participants</td>

<td>Sense-making methodology</td>

<td>Identification of information behaviour was accomplished by using the critical incident technique concomitantly with Dervin's sense-making methodology as analytical tools. The critical incident technique was used primarily in the identification of distinct information behaviour incidents, which formed the individual units of analysis for further examination.</td>

</tr>

<tr>

<td>Phase two</td>

<td>Identify the contextual factors influencing the information behaviour identified in phase one</td>

<td>Discourse analysis</td>

<td>Once common information behaviour patterns of the participants were identified, contextual factors were identified using Dervin's sensemaking methodology concomitantly with discourse analysis as analytical tools.</td>

</tr>

<tr>

<td>Phase three</td>

<td>Articulate the role of context in the information behaviour of participants.</td>

<td>Sense-making methodology plus discourse analysis</td>

<td>The influence of the contextual factors emerging from phase two of analysis was examined in relation to the information behaviour identified during phase one using discourse analysis as the primary analytical tool with additional investigation and interpretation informed by Dervin's sense-making methodology.</td>

</tr>

</tbody>

</table>

Analytic tools were selected based on their ability to be used within a social constructionist paradigm to interpret varying accounts of information activities as experienced within each individual's perceived reality. According to Dervin ([1999](#der99)), in sense-making, 'the real is always potentially subject to multiple interpretations, due to changes in reality across space, changes across time, differences in how humans see reality arising from their differing anchorings in time-space; and differences in how humans construct interpretive bridges over a gappy reality' (p. 731). These core assumptions regarding the nature of reality and knowledge that underpin sense-making research have similar parallels within the philosophical foundations of social constructionism and are therefore complementary frameworks from which to explore the contextual nature of information behaviour.

Dervin ([1999](#der99)) argued that the practices of researching information needs and seeking are also communicative practices, and produced collaboratively amongst researchers and participants. In this sense, both the researchers and participants can claim ownership over the research and its outcomes. Conceptualising the researcher and the participants as such supports a relativist ontology and interpretive epistemology, as the real is always subject to interpretation and the known is conceptualised as relative to the individual within this paradigm.

In the first phase of analysis information behaviour incidents were mapped to the five components of the sense-making metaphor based on Dervin's descriptions of these components and the language used by participants to describe their behaviour. Use of the sense-making metaphor as a framework for data collection and analysis permitted in-depth investigation of all stages and aspects of an information behaviour incident, and evidence of these behaviours was identified though the use of Dervin's description's for those components of sense-making. These components of sense-making, and examples from the data used to develop and understanding of sense-making activities in Laos during Phase 1 of analysis are described in Table 2 below.

<table class="center"><caption>  
Table 2: Sense-making in interview data  
</caption>

<tbody>

<tr>

<th>Code/component of sense-making</th>

<th>Description (after [Dervin, 2008](#der08))</th>

<th>Examples from interview data</th>

</tr>

<tr>

<td>Situation</td>

<td>History, experience, horizons, constraints, barriers, habits/skills</td>

<td>'I was a/at/in…'  
'At that time…'  
'I had done…'</td>

</tr>

<tr>

<td>Gap</td>

<td>Questions, confusions, muddles, riddles, angst</td>

<td>'I wanted to know…'  
'My question was…'  
'Wanted more information about…'</td>

</tr>

<tr>

<td>Sense-making</td>

<td>Verbings, procedurings, strategies and tactics</td>

<td>'Asking my friends'  
'Going to the shop'  
'Searching the internet'</td>

</tr>

<tr>

<td>Bridge</td>

<td>Ideas, cognitions and thoughts; attitudes, beliefs and values; feelings, emotions and intuitions; memories, stories and narratives</td>

<td>'I felt…'  
'I thought…'  
'I found…'  
'I used…'  
'I liked…'</td>

</tr>

<tr>

<td>Outcome</td>

<td>Help and facilitations; hurts and hindrances; consequences, impacts and effects</td>

<td>'I made the decision to…'  
'I found out…'  
'I was satisfied…'</td>

</tr>

</tbody>

</table>

The discourse analytic method used in phase two of this study is based on the one developed by Gee ([2011b](#gee11b)) for interpreting how an individual constructs his or her reality through language, a concept that also parallels the central principles of social constructionism.

Gee describes seven '_areas of reality_' that are constructed through the use of language ([Gee, 2011a](#gee11a)). According to Gee, '_whenever we speak or write, we always (often simultaneously) construct or build seven things or seven areas of reality_' ([2011a](#gee11a), p. 17). Gee termed these seven areas the '_seven building tasks_' of language ([Gee, 2011a](#gee11a), p. 17). Interviews with participants produced accounts of information behaviour that provided evidence of these building tasks of language, in turn providing indications as to how an individual constructed and perceived his or her reality.

The seven building tasks identified by Gee ([2011a](#gee11a), p. 17-20) are:

*   Significance
*   Practices (Activities)
*   Identities
*   Relationships
*   Politics (the distribution of social goods)
*   Connections
*   Sign systems and knowledge.

In phase three of analysis the information behaviours mapped to the five components of the sense-making metaphor identified in phase one were then examined alongside the seven building tasks of language identified in phase two in order to establish how the information behaviours and contextual factors were influenced by one another.

The participants reported on critical incidents involving information behaviour that occurred in their daily life. Chell and Pittaway ([1998](#che98)) concisely defined Critical Incident Technique as 'a qualitative interview procedure which facilitates the investigation of significant occurrences (events, incidents, processes or issues) identi?ed by the respondent, the way they are managed, and the outcomes in terms of perceived effects' (p. 25).

Fifty-two incidents of information behaviour were collected using the chosen interviewing technique. The individual incident was therefore used as the unit of analysis for the purposes of this study. The interview recordings were transcribed and incidents collated into five broad clusters of information-related activities:

*   Consumer related information behaviour; for example, making purchases such as a car, a laptop, etc.
*   Professional related information behaviour; for example, job seeking, professional development, etc.
*   Education-related information behaviour; for example, scholarship inquiries, study pathway decision making etc.
*   Social or recreation related information behaviour; for example, organizing recreational sports activities, hobby-related information behaviour etc.
*   Health related information behaviour; for example, sense-making related to illness, medical information behaviour etc.

## Findings regarding spirituality in information activities among individuals in Laos

Evidence of the important role of the spiritual in information activities among individuals in Laos was present in nearly every interview analysed. Often, individuals described visiting temples or monks when they needed advice, consulting fortune tellers, or obtaining good luck blessings after making significant purchases. Other individuals described the role of religious beliefs in activities such as job seeking, such as in the example below:

> Interviewer: Was there anything else that could have helped you?  
> Si: Maybe my fortune I think.  
> Interviewer: Ok just luck, Buddha?  
> Si: Yeah just pray to Buddha to help me find a job. Yeah because for the job interview day a fortune teller came to my house, and I ask to do, like they check my fortune

This interviewee went on to describe how the fortune teller told him he would be successful in his job interview. Similar interactions with spiritual advisors and fortune tellers were reported in a number of incidents with participants describing their belief in the influence of Buddha or fortune on other events in different ways. While on the surface these activities do not initially appear to be 'information seeking' or 'information gathering' behaviour, they are sense-making activities the participants used to make sense of their situation and bridge a gap in their knowledge, using the information communicated to them by the fortune teller or monk to help them come to a conclusion. While as Westerners we might not view ideas communicated by a monk or fortune teller as being information objects in the sense conveyed by Ingwersen and Järvelin's ([2005](#ing05)) cognitive framework shown in Figure 2, those ideas were perceived by the particiapnts in this study as information objects and the monks and fortune tellers they consulted as interfaces, and therefore no less a part of their information seeking behaviour as searching Google would be to most readers of this paper.

The following extract from an interview with Siphok, a 31-old female, provides an example of how one participant viewed her success to be related to metaphysical occurrences, and how activities such as praying were a form of information behaviour in her efforts to make sense of a situation.

> Siphok: Someone said that 'You should believe God'. I think God is just one part. 'God can make you succeed, you can go to the temple and pray' they suggest to me.  
> Interviewer: So you don't agree with that?  
> Siphok: I did, you know, I follow their suggestion.  
> Interviewer: But it was not successful?  
> Siphok: I don't know. I think it's a part. Now I think I believe it about 10%. You know, it's hard for me to get 5.5 [International English Language Testing System score]. The day that I took the IELTS [International English Language Testing System] test for the NZ scholarship, I got up early, and then I went to Si Muang temple, and I prayed and prayed and it made me feel you know, feel warm, confident and more relaxed, and I took the exam maybe around 9 or 9:30, and finally, when I knew the IELTS [score], that I got 5.5, I thought, 'Oh I can't believe it!' I think that during the test I felt I couldn't do well, but I got 5.5, maybe from Buddha. Yeah, I thought that.

Siphok related advice she received from other people, her own beliefs, and her experience at the temple with her English language proficiency test score. While she admitted to believing somewhat that God or Buddha plays a role in the outcomes she experiences, she also described some scepticism, and later she described how her successful outcome was due to these metaphysical forces more than she had initially believed. In this situation, her visit to the temple, the advice from friends, and her own belief system were all part of her sense-making behaviour, in addition to the formal and informal study she had done to prepare herself for the test.  

Later, Siphok describes visiting a fortune teller during the same period.

> Siphok: She told me, 'You can, I predict that you can', and she said that 'You will travel far away from your hometown, and soon', at that time you know I got training scholarship from Singapore, and then I asked her again, 'What about New Zealand scholarship? Do you think that I can? I will take IELTS test', she said 'You can'.  
> Interviewer: So that helped you to be confident?  
> Siphok: Yeah I think so.  
> Interviewer: So for the fortune teller, would you say you also believe in it 10% or…?  
> Siphok: Yes, 10%, because I have to come back to consider that not all fortune tellers can predict the right thing. You know, it's not only her that I went to, but I went to other fortune tellers, many times. And many different fortune tellers, but it was only her that predicted correctly.

This excerpt provides an example of how the participant viewed the connection between a fortune teller's prediction and reality. Despite claiming to only believe only about 10% in relation to the validity of supernatural abilities, the participant indicated that she had repeatedly and often visited fortune tellers.

Saobet, a 31-year old male, also provided evidence of how participants tried to rationalise their beliefs in the power of spirituality, describing the psychological affect of praying or visiting fortune tellers as being a possible influence on the outcomes they experienced. In some cases praying or visiting fortune tellers supplemented more active information seeking, such as finding IELTS study materials online or asking colleagues for advice, however in other instances praying or getting advice from fortune tellers was the only sense-making activity in which an individual engaged.

This kind of contradiction and uncertainty regarding the validity or value of this type of supernatural information was echoed by other participants, as in the following example, where a participant was asked if he ever sought advice at a temple or from a fortune teller when he was making sense of a complicated family situation:

> Saobet: I respect about this, but don't believe.  
> Interviewer: Do you believe in karma?  
> Saobet: Yeah believe it, but sometimes the karma, baap (bad luck), now it turns on me sometimes. I know that. Sometimes my fiancée wants me to take her to the temple, and get the advice from the monk, because we want to know, if we live together (without being married), the situation will improve or not. Because now get trouble.  
> Interviewer: What kind of trouble?  
> Saobet: Like financial trouble, and my mother-in-law, got thyroid cancer, and everything difficult come to me this year. So sometimes I believe in baap (bad luck) and sok (good luck).

This interviewee reported respecting religious beliefs and emphasised the value of respecting tradition, yet he indicated he personally did not believe in Buddhism, and conveyed uncertainty regarding the effect of luck or karma on reality. Despite his professed scepticism, religion, karma and luck all influenced him in various ways over the course of the three information behaviour incidents he described. Again, this uncertainty and what seemed to be a somewhat conflicting belief system was found throughout the interview data.

The following excerpt provides another example of an individual who found his understanding of reality at odds with the cultural and religious values common in Laos. The interviewee expressed the same feelings of disbelief, but respect for spirituality.

> Interviewer: So, you don't go to the temple?  
> Saoet: Yeah I don't go, but I respect.  
> Interviewer: So what do you think about going to get a blessing for good luck from the monk or something?  
> Saoet: It's quite superstitious, but for me, it depends.  
> Interviewer: Do you believe what the fortune tellers tell you? Trust them?  
> Saoet: Yeah, umm it depends. Just before I don't know for sure whether it's the truth or not, I went to fortune teller because my friend wants me to go with him. I never tried before. I asked would I be able to win the scholarship to go to the US and they said 'You will, you will get the scholarship to study overseas' and after that, I got the scholarship.  
> Interviewer: So you trust the fortune teller now?  
> Saoet: Even though it's true, I'm not convinced.

Saoet, a 20 year-old male, described how even though he had a fortune teller accurately predict his future, he was '_not convinced_' that this person was a reliable source of information. Similarly to the previous interviewees, Saoet's understanding of reality conflicted with his core cultural assumptions. Cutler ([2005](#cut05)) described these core cultural assumptions as '_acting below conscious levels_' in his model of the '_cultural onion_' (p. 75).

The conflict between religious belief and reality was found throughout the data but was more apparent in accounts by participants who were young or more well-educated, as opposed to interviewees over the age of 40 or those with less education. Young, educated interviewees justified what they knew to be superstitious beliefs by suggesting that religious activities had implications on the outcomes of the related activities, as shown in Siphok's interview above. Sao, a 19 year-old male, also rationalised religious activities, as in the excerpt below.

> Interviewer: At that time, did you ever go to fortune teller or go to temple for blessing?  
> Sao: Blessing of course, for good luck.  
> Interviewer: Do you think if you had gone to the temple you would have had better luck with that job?  
> Sao: Yes, because if I go to the temple and ask God's blessing me, that means in my heart I ask, I am ambitious. I want the job! I want the job! Maybe, if I don't go the temple, or do anything, that means I don't care about the job.  
> Interviewer: So for your next interview, you plan to go to the temple?  
> Sao: Yes. Yes of course!

These quotations provide examples of how participants' accounts include descriptions of karma and luck, and how these beliefs affected the information behaviour in which participants engaged. As well as engaging in active information seeking, such as gathering information about a prospective employer to prepare for a job interview, participants would also visit a temple and receive a blessing from a monk to help make sense of their situation. The advice or information provided by the monk, or even less tangibly the feelings experienced or awareness obtained as a result of visiting a temple or fortune teller, can be considered 'bridges' in the sense-making metaphor that individuals sought in order to address a gap in their knowledge. Eschewing the spiritual impact of karma, luck, and fortune tellers, it seems there was significant psychological impact on the behaviours of those who believed in them.

The interviewees' reverence for religious or spiritual information appeared to affect the intensity of the information seeking in which they were engaged. In addition to seeking information to resolve issues, individuals relied on deeply ingrained concepts closely linked to Buddhism such as karma and luck in response to their information needs. Their religious beliefs resulted in what appeared to be a lower intensity of active information seeking that at times influenced the resolution of important information needs in some cases, such as serious health related concerns. A significant part of the following interviewee's health related information seeking behaviour included going to the temple and praying to help make sense of their situation.

> Phet: I always go to temple and pray, hope we have someone help us like magic, someone to help.

In Phet's situation, her village had very few health related information resources available, and the temple was the logical place for her seek a bridge to the gap in her knowledge. Another individual dealing with a health related problem also visited the temple and prayed in lieu of seeking other alternative information resources.

> Saobet: I went to temple at that time, until now.  
> Interviewer: A lot?  
> Saobet: A lot, and I prayed, 'I hope I will see some day', or my eyes getting better.

Saobet and Phet's descriptions of going to the temple conform to Dervin's conceptualisation of sense-making behaviour. While praying may not be traditionally considered information seeking, it is a sense-making activity, where the individual seeks some kind of understanding, resolution, or comfort to bridge a gap in their lives. The participants' accounts indicated that they believed the power of their faith was a sense-making behaviour they used to address the incident they described, often after trying to engage in other kinds of information seeking behaviour to help resolve their gap in knowledge. This reliance on prayer and spiritual sense-making activities may have been a result of simply having no other resources available. In Phet's case, she had access to neither books nor newspapers, nor was there a television or even a radio in her community. While she did seek information from those people around her, none of them were knowledgeable about her specific health related information need. Prayer was a way for her to try to make sense of her situation when no other information seeking behaviour were successful. While Saobet did have more health related information sources present in her community, barriers to her accessing them included cultural factors such as the importance of saving face, socio-economic factors, and a lack of awareness of the availability of resources. The implications of these factors, and some discussion of their impact, are provided below.

This research does not intend to discount the validity of religious beliefs nor make any judgements regarding the legitimacy of prayer as an information seeking activity. Nevertheless, the data showed that in the incidents where prayer was cited as a sense-making activity, additional sense-making activities, such as the use of interpersonal networks for information seeking, were also present in participants' accounts of the resolution of the information needs.

## Discussion

This study addressed the need for further research concerning spirituality in information behaviour recognised by Kari ([2007](#kar07)), who suggested further empirical evidence was needed to draw general conclusions about specific information phenomena, such as information seeking, and 'larger information processes or correlations' (p. 958) concerning spirituality.

The first phase of analysis used the sense-making metaphor to reveal all of the information behaviour present in the interview data. The second phase used discourse analysis to identify the contextual factors also present in the interview data. The third phase synthesised the two analytical tools, mapping the contextual factors to the information behaviours, resulting in the identification of a range of contextual factors that affected information behaviour among the participants. The study therefore can be considered to both test existing information behaviour theory, and extend our current information behaviour theory by developing new insights.

Some findings that broadly confirm existing information behaviour theory are the identification of several different factors, also called 'intervening variables' by Wilson ([1999](#wil99)).

They can be grouped into two broad categories, primary factors and secondary factors, based on the degree of influence they exerted on how an individual engaged with information. Primary factors were present in every incident investigated, while secondary factors were present in some, but not all, of the incidents.

*   Primary factors
    *   Social context
        *   Religious context
    *   Cultural context
        *   Religious context
*   Secondary factors
    *   Physical context
    *   Socio-economic context
        *   Educational context
    *   Political context
    *   Personal context
    *   Situational context

Religious context emerged as being a contextual factor related to both the social and cultural contexts, whereas Educational context emerged as being directly related to socio-economic context, amongst other factors including political context. These contextual factors affected information behaviour among the participants, but also affected and influenced each other. This finding is consistent with Bandura's social cognition theory, in which '_triadic reciprocal causation posits that behavioural, cognitive, and other environmental influences all operate interactively as determinants of each other_' ([Miwa, 2005](#miw05), p. 54).

This complex, interdependent web of social, cultural and religious factors made it difficult to conduct a reductionist analysis of the factors into separate, distinct effects. For example, initially Buddhism or religious values appeared to be a secondary contextual factor. However, upon closer consideration, it became apparent that the religious, social and cultural dimensions were so closely inter-related that it would be difficult to consider them as distinct, separate factors. In addition, participants' educational context was revealed to be the result of a number of different factors including the socio-economic context, political context, and personal context. Nevertheless, the educational context did play a role in information behaviour. The contextual factors identified during the analysis and their interdependent nature conform to findings and theory put forth in Wilson's ([1997](#wil97)) model of information behaviour includes _'intervening variables_' such as environment, and Ingwersen and Järvelin's ([2005](#ing05)) framework, shown in Figure 2.

Discourse analysis revealed that social and cultural contextual factors affected the information behaviour of participants in every information behaviour incident reported. In every incident collected, the social and cultural values of the participant had either an explicit or implicit influence on how the interviewee interacted with information. Examples include one participant's description of her decision to begin engaging in consumer related information behaviour.

> Interviewer: So, at that time, when you first started talking to people asking for advice about how to select a car how to buy it, what was your number one question?  
> Gao: They asked me, 'Why not buy it?', you know, 'Buy it, it's convenient for you', something like this, 'If you have money why do you ride a motorbike? You have to buy a car.' So that's the questions people asked me, and they impressed me to buy it.

While this example does not specifically use the word 'culture' or refer to 'Lao people' as a society, its reference to the social and cultural dimensions of context affecting her activities are implicit. Within Gao's social context, people with sufficient financial resources are expected to own a car, rather than ride a motorbike. Car ownership is an important status symbol in Gao's social context, and this has affected her information needs, thus impacting her information behaviour. A further explicit example of cultural values influencing information behaviour is:

> Interviewer: So, if someone is older than you, you have to respect them and be polite to them?  
> Siphok: Yes I think because of culture. One more thing. Not only older people, but older people with high position. We… not only me, all Lao people have to respect them.

Interviewees also made general statements regarding 'Lao people', indicating the attitudes and behaviour common in Lao society, shown below.

> *   Lao people if they have a competition it's like jealousy: Saobet on why she did not ask the neighbour for business advice
> *   Lao people now we don't really trust to buy things on internet: Sipsi on why he would not look for consumer information online

These examples provide additional evidence of how social and cultural values affected information behaviour.

Nahl ([1997](#nah97), [2005](#nah05)) provides us with a lens from which we can understand information behaviour as operating within three levels or domains called affective, cognitive, and sensorimotor (or _psychomotor_). Nahl ([2005](#nah05)) suggests that '_information searching behavior can be defined within this triune classification system as a form of goal-directed behavior in which people are motivated (affective) to formulate a plan (cognitive) and perform it (sensorimotor)'_ (p. 191). Nahl ([2005](#nah05)) also suggests that because of the importance of the affective and cognitive processes in information behaviour we must extend our investigation into the social setting, and to '_the mental norms that each person develops in the form of habits of feeling and interpreting that are inherent to cultural group membership_' (p. 191). Nahl ([2005](#nah05)) confirms that '_motivational states and goal-directed thinking are internally ordered by both the social values (affective) and the structure (cognitive) of the information environment, therefore, information science overlaps with a social-behavioral psychology of development and habit formation_' (p. 191).

Work in psychology by authors such as Nisbett ([2003](#nis03)) and Kitayama ([Kitayama and Park, 2010](#kit10); [Kitayama and Uskul, 2011](#kit11)) support Nahl's assertions, describing how social and cultural contexts affect cognition, citing numerous studies showing evidence that individual's cognitive processes vary according to their cultural background. Kitayama's work in particular articulates the effects of cultural values on cognition accessible to readers from outside of the psychology or neuroscience areas by providing an account of how '_people carry out the same tasks by recruiting varying component neural operations depending on their social or cultural backgrounds_' ([Kitayama and Park, 2010](#kit10), p. 112)

These studies provide further evidence that the way individuals engage with information in a non-Western context may be different from those in Western societies, and support the finding that social and cultural values play a very important role in information behaviour.

## The spiritual and religious in everyday information behaviour

Distinguishing the social context from the cultural or religious context within the interview data was not feasible, as these three dimensions of context are very closely related and interdependent. According to Hofstede and Hofstede ([2005](#hof05)) culture is a collective phenomenon that is '_shared with people who live or lived within the same social environment, which is where it was learned_' (p. 4). They also suggested that individuals develop basic beliefs and values early in life through socialisation and education, and share these values and perceptions with other members of their society.

They state that the power distance dimension relates to the extent to which individuals assume or tolerate an unequal distribution of power. It reflects the degree to which a community maintains inequality among its members by the stratification of individuals and groups with respect to power, authority, prestige, status, wealth, and material possessions ([Hofstede and Hofstede, 2005](#hof05)). It is important to note that, for Hofstede and Hofstede ([2005](#hof05)) the religious belief system is one of the primary indicators for the power distance dimension of culture. Laos has been described as a high power distance society ([Dorner and Gorman, 2011](#dor11)), indicating that inequality stratification of individuals and groups with respect to power, authority, prestige, status, wealth, and material possessions is tolerated more than in a lower power distance society, such as New Zealand ([Hofstede, 2001](#hof01)).

Laos lags behind many Western nations in availability of access to the Internet ([The World Bank, 2015](#thend)), consistent with findings suggesting high power distance societies adopt new information and communication technologies more slowly than those with lower power distance values (eg. [Khalil and Seleim, 2010](#kha10)). The strong religious belief system present in Laos exists in parallel with its high power distance, which Khalil and Seleim ([2010](#kha10)) found to impact information communication technology adoption, and therefore also has implications for information behaviour, as many participants were unlikely to use the Internet to resolve their information needs.

The data analysis revealed a number of secondary contextual factors including the physical, socio-economic, political, personal and situational context which also influenced information behaviour, though to a lesser extent. These factors and their implications are discussed in greater detail by Gaston ([2014](#gas14)) in her PhD thesis.

Kari's ([2007](#kar07)) review of the spiritual in information behaviour found that '_information can be supposedly acquired by spiritual means_' (p. 958), which according to Kari suggests that the conventional '_conception of information seeking as happening through man's five physical senses can be deficient, and that there may be more ways of knowing than we usually realize_' (p. 959).

We suggest that inclusion of the term '_supposedly_' in Kari's findings implies some scepticism in the ability of individuals to acquire information by spiritual means, which this paper demonstrates is indeed the case for the participants in this study.

Kari ([2007](#kar07), p. 957) also developed a typology of '_eleven major findings_' relating to the spiritual in information behaviour, which are

> 1.  information can be about the spiritual (e.g., mysticism);
> 2.  information itself can be reckoned holy (e.g., Bible);
> 3.  information can be supposedly acquired by spiritual means (e.g., trance channelling);
> 4.  information can originate from a source considered as a spiritual entity (e.g., God);
> 5.  an information actor can be an expert in spiritual matters, or a spiritual community (e.g., spiritual advisor);
> 6.  an information actor may claim to possess spiritual abilities (e.g., medium);
> 7.  an information actor can feel him/herself develop spiritually (e.g., spiritual horizons expanding);
> 8.  an information process can be simultaneously experienced as a spiritual process (e.g., library work as ministry);
> 9.  information systems can be believed to parallel or even replace asserted spiritual methods (e.g., internet as omnipotent tool);
> 10.  information phenomena can be influenced by a spiritually oriented environment (e.g., spiritual happening); and
> 11.  information processes can be allegedly affected by a spiritual agent (e.g., providence).

Kari ([2007](#kar07)) went on to suggest that following the in-depth investigation spiritual information can be defined as '_information that complies with criterion (1), (2), (3) or (4) above_' (p. 957). The evidence to emerge from our investigation into everyday information behaviour in Laos therefore supports Kari's finding that information can be acquired by spiritual means, and that the conception of information behaviour as happening through man's five physical senses is deficient, and these metaphysical ways of knowing should be taken into consideration in our conceptualisation of information behaviour. In particular, in a non-Western context such as Laos with a strong religious culture woven throughout everyday life and daily activities, information behaviour may often reflect the influence of spirituality and other metaphysical elements embedded within information activities considered more conventional by Western standards.

## Limitations and considerations

While this study presents some useful insights about the role of culture and the metaphysical in everyday information behaviour in Laos, it was nonetheless limited by a number of factors. As noted by Dervin ([1997](#der97)), the study of context presents multiple challenges, as it is neither static nor independent. While the research aimed to explore the influence of context, the context of the researchers also may affect the data. The ambiguous nature of context presents additional challenges for the study of context. Courtright ([2007](#cou07)) suggests that independent investigations of contextual factors from an information science perspective may test initial assumptions about contextual boundaries, as the researcher is challenged to remain objective, as well as aware of their own implicit assumptions. Courtright ([2007](#cou07)) also suggests that this test of initial assumptions may actually stimulate new questions, which may be posed to information actors to reveal further insights about their information behaviour. Awareness of these assumptions from an early stage yielded a reflexive approach to all aspects of the research undertaking and minimised the potential for a limiting influence.

The undertaking of this research tested the contextual boundaries and initial assumptions of the research team in particular when it came to data collection and analysis. The assistance and insight provided by the interpreter during data collection proved invaluable in understanding some contextual nuances that were difficult to perceive as an outsider, despite having extensive experience in Lao culture. In addition, the initial assumption that specific recommendations for the Lao context would arise out of this research proved unrealistic, as an insider's understanding of the cultural nuances as well as the political, economic, and educational context of Laos beyond the grasp of an outsider is required. Gaston ([2014](#gas14)) describes some strategies for developing specific recommendations for the Lao context including the identification of a collaborator and key stakeholder with insider knowledge of the cultural, political, economic, and educational context of Laos.

Despite an understanding of context as dynamic and responsive to human behaviour, for the purposes of the research context was conceptualized as static at the time of investigation. In the application of social constructionism in information science research one conceptual issue to arise was the understanding the relationship between the individual and society, and whether the former exerts an influence on the latter, or vice versa ([Burr, 2003](#bur03)). According to Burr ([2003](#bur03)), research within the social constructionism framework perceives the relationship between the individual and society as operating in both top-down and bottom-down directions. In Burr's words '_human beings continually construct their social world, which then becomes a reality to which the individual must respond_' (p. 185). She elaborates further:

> Although human beings construct their social world, they cannot construct it in any way they choose. At birth they enter a world already constructed by their predecessors, and this world assumes the status of an objective reality for them and later generations. (p. 185)

Indeed, this mutually-informing relationship is a circular process, involving subtle shifts in society that occur over generations, built upon previous constructions. Conceptualising the relationship between the individual and society in such a manner reiterates many of the ideas that became the impetus for undertaking this research, namely the suggestion that individual thinking and reasoning, and hence information behaviour, do not occur in a historical or cultural void. Instead, these behaviours are built upon the historical and social frameworks of the culture into which one is born, and demonstrate the effects of all aspects of those social, cultural, and historical frameworks, such as religious beliefs.

The dynamic conceptualisation of culture and its relationship to society is reflected in the interpretation of the findings. Awareness of this relationship and the shifts that occur overtime resulted in the identification of changes or trends in information behaviour apparent when participants' ages and education levels were taken into consideration. This research did not intend to examine contextual change and is not a longitudinal study as the influence of the individual upon context occurs slowly and is only perceptible over time. Nevertheless the investigation did identify some changes in information behaviour arising as a result of rapid contextual shifts occurring at the time of the data collection. Those changes, their implications, and the continual construction and re-construction of the social world and the resulting reality are the focus of a publication entitled '_Information behaviour in transition: a developing country perspective_' ([Gaston, Dorner, and Johnstone, 2013](#gas13)).

The use of a vetted interpreter, and the lead author's understanding of the Lao language, permitted reliable collection and analysis of data, though the inability of data collection to be carried out in a native language common to both the researchers and participants may be perceived as a limitation. To further validate the data collection, a trial interview was audio recorded, and the translation validated by a third party. The translation was found to be of a high quality standard by a senior official at the National University of Laos and therefore this limitation was minimised.

Despite our best attempts at neutrality, we are aware that inherent cultural bias towards Western cognitive patterns may be present in this study. Eliminating this bias would be almost impossible, as all research must take place within a social, cultural, and historical context. However, an awareness of the possible preference for Western cognitive patterns permitted us to quickly recognise and respond to potential issues arising from the researcher's social, cultural and historical context. The lead researcher's extensive targeted training in cultural awareness and extended immersions in non-Western cultures also provided a useful perspective from which cultural biases could be better perceived and addressed.

## Implications

The following broad themes emerged as the primary features of information behaviour affected by contextual factors in Laos, all of which have some evidence of the role of religious and spiritual values among interviewees:

*   A strong preference for interpersonal information;
*   Limited active information seeking
*   Avoidance of certain types of information and information sources;
*   Implicit trust in most interpersonal sources of information
*   Respect for tradition influencing information behaviour;
*   Decision making based on incomplete information.

The above list is not exhaustive, but represents the most commonly found features of information behaviour present in the interview data. Each of the above broad features was influenced by a complex system of interconnected contextual factors. Spirituality and Buddhism represents just one of these interconnected contextual factors that played a significant role in information behaviour in Laos, yet its substantial influence is evident in all of the above findings to result from this research.

A number of previous studies support the above findings, including Harris and Dewdney's ([1994](#har94)) findings regarding preference for interpersonal information in North America before wide-spread internet adoption, and Markwei's ([2013](#mar13)) findings regarding everyday information behaviour in a developing country and what can considered less active information seeking. Both sets of findings were explored in greater depth in the main researcher's doctoral thesis ([Gaston, 2014](#gas14)).

Our findings regarding avoidance of information, implicit trust in close personal sources, and a respect for tradition influencing information behaviour are all strongly linked to the cultural identity of the participants in this research. These findings support behaviour been identified with regards to personal uncertainty, and research into how culture and worldviews affect the ways in which people make sense of their world and resolve personal uncertainties ([Van den Bos and Poortvliet, 2005](#van05); [Van den Bos, Uwema, Poortvliet, and Maas, 2007](#van07); [Van den Bos, 2009](#van09)). Van den Bos ([2009](#van09)) found that

> Uncertainty leads people to start processing information they subsequently receive in experiential-intuitive ways, making them react in strong positive affective terms to people and events that bolster their cultural worldviews and in strong negative affective terms to things, individuals or experiences that violate these worldviews. (p. 199)

Van den Bos's findings therefore support our conclusion that resources and behaviours providing information consistent with individuals' existing worldviews was preferred over sources that conflict with these subconscious belief systems, as conflicting information or activities would result in 'strong negative' reactions, even if these information sources offered potentially more efficient or effective resolution to their information needs. Therefore, it is not surprising that decision making was sometimes based on incomplete information, as these affective responses to information play a strong role in the resolution of personal uncertainty and the resolution of information needs ([Van den Bos, 2009](#van09)). These findings can therefore provide some insight into the relationship between an individual's culture, his or her worldviews, and how he or she engages in information behaviour, demonstrating that it is a complex and multi-dimensional one that merits yet further research.

## Conclusion and future research

In this research we found that the primary and secondary contextual factors combined to form a complex and interrelated system that influences information behaviour in different ways among participants in Laos. Our findings support Wilson ([1999](#wil99)) who contends that certain factors act as 'intervening variables' enabling or hindering information behaviour, and others influence broader decisions, choices, and preferences, combining to influence every aspect of information behaviour.

In particular, we found that religious beliefs, closely tied to the cultural and social aspects of the individual's context, affected how he or she engaged with information in parallel with a variety of other factors, in a dynamic and multifaceted manner. By examining information behaviour in Laos, and by explicitly articulating the role of context in information behaviour in this setting, our study further extends the theories regarding the role of context in information behaviour into non-Western developing countries.

Our findings show that existing models of information behaviour are relevant in a non-Western developing country when the role of context is very broadly articulated. Nonetheless, it is worthwhile to note that in a non-Western country such as Laos, Buddhism and spirituality play a very strong role in information behaviour, yet the spiritual context is rarely given explicit consideration in information behaviour research because it is predominantly Western focussed. Therefore, one must consider how spirituality will affect the way an individual needs, seeks, and uses information in the Lao context for any information related activity or for the purposes of designing any information resources. Resources range from oral to print to electronic, and activities may include interacting with international information systems, engaging with training or education materials, obtaining healthcare, making a purchase, staff recruitment, or even organizing a football match; all of which require some interaction with information. The findings described above provide some insights into how religious beliefs affected information behaviour amongst the participants in this study. Namely:

*   A strong preference for interpersonal information suggests individual are less likely to turn to electronic or impersonal information sources
*   Less active information seeking suggesting individual may prefer more passive information seeking methods; indicating that information resources may be more useful if very widely, easily available, or obvious.
*   Avoiding certain types of information and information sources because of social values including the need to '_save face'_ may limit some information sources.
*   Implicit trust in most interpersonal sources of information may result in unverified or inaccurate information being widely in use.
*   A respect for tradition influencing information behaviour may result in activities or resources requiring individuals to behave in a manner not aligning with his or her traditional values meeting significant resistance.
*   Decision making based on incomplete information may be done so to '_save face_'.
*   The information poor perceive themselves to be devoid of any sources that might help them (See [Chatman, 1996](#cha96), p. 196).
*   Information poverty is determined by self-protective behaviours, which are used in response to social norms (See [Chatman, 1996](#cha96), p. 196).

A comprehensive and specific understanding of information behaviour within a given local context or society requires consideration for the contextual factors present within the given community, and how they affect information behaviour. An in-depth investigation, such as the one conducted over the course of this research, has the potential to provide such an accurate and specific understanding of information behaviour.

This research has also permitted the future investigation of information behaviour in non-Western contexts, with further research currently underway in Samoa, a non-Western society with Christianity as the dominant religious paradigm, and additional sites for study under consideration. It is hoped that the investigation of diverse settings and societies will help further our understanding of the role of context in information behaviour. Ultimately, as more societies are investigated, we may be able to further extend information behaviour theory with regards to the role of the metaphysical in everyday information behaviour.

## Acknowledgements

The authors wish to thank the reviewers for their time and feedback on this paper and also to thank Amanda Cossham for her guidance and assistance with preparation of the final draft of this paper. Finally, the authors would like to thank the interpreter and interviewees for their time and assistance with this research, without whom none of this would have been possible.

## About the authors

**Nicole Gaston** is a Lecturer in information and library studies in the School of Social Sciences at the Open Polytechnic of New Zealand. She received her Bachelor's degree from the University of Illinois at Chicago, her Master of Library and Information Science at McGill University, Montreal, Canada, and her PhD in Information Studies from Victoria University of Wellington, New Zealand. She can be contact at [nicole.gaston@openpolytechnic.ac.nz](mailto:nicole.gaston@openpolytechnic.ac.nz.).  
**Daniel G Dorner**, FLIANZA, Ph.D., is a Senior Lecturer in the School of Information Management at Victoria University of Wellington in New Zealand. He has considerable experience in library education and information literacy education in Southeast Asia, particularly Vietnam, Laos, Cambodia and Thailand. His current research interests include the areas of information behaviour in non-Western contexts, digitization and related issues, and advocacy strategies by major library associations and institutions. Dr Dorner is Chair of Division V, IFLA. He can be contact at [dan.dorner@vuw.ac.nz](mailto:dan.dorner@vuw.ac.nz).  
**David Johnstone** is a Senior Lecturer in information systems in the Business School at Victoria University of Wellington in New Zealand. He received his Bachelors (with Honours) and Masters of Science degrees from Massey University in Palmerston North and PhD in Information Systems from Victoria University of Wellington, all in New Zealand. He can be contacted at [david.johnstone@vuw.ac.nz](mailto:dan.dorner@vuw.ac.nz).

#### References

*   Bates, M. (2002). [Toward an integrated model of information seeking and searching](http://www.webcitation.org/6WTO8soWy). _The New Review of Information Behaviour, 3_, 1&dash;15\. Retrieved from http://pages.gseis.ucla.edu/faculty/bates/articles/info_SeekSearch-i-030329.html (Archived by WebCite® at http://www.webcitation.org/6WTO8soWy)
*   Borzekowski, D. L. G., Fobil, J. N. & Asante, K. O. (2006). Online access by adolescents in Accra: Ghanaian teens' use of the internet for health information. _Developmental Psychology, 42_(3), 450&dash;8.
*   Bryman, A. (2008). _Social research methods._ (3rd ed.). Oxford: Oxford University Press.
*   Burr, V. (2003). _Social constructionism._ (2nd ed.). London: Routledge.
*   Central Intelligence Agency. (2013). [CIA - The World Factbook.](http://www.webcitation.org/6WTOAyySm) Retrieved from https://www.cia.gov/library/publications/the-world-factbook/geos/la.html by WebCite® at http://www.webcitation.org/6WTOAyySm}
*   Chatman, E. A. (1987). The information world of low-skilled workers. _Library and Information Science Research, 9_(4), 265&dash;283.
*   Chatman, E. A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science, 47_(3), 193&dash;206.
*   Chell, E. & Pittaway, L. (1998). A study of entrepreneurship in the restaurant and café industry: exploratory work using the critical incident technique as a methodology. _International Journal of Hospitality Management, 17_(1), 23&dash;32.
*   Courtright, C. (2007). Context in information behavior research. _Annual Review of Information Science and Technology, 41_(1), 273&dash;306.
*   Cox, A. M. (2012). An exploration of the practice approach and its place in information science. _Journal of Information Science, 38_(2), 176&dash;188.
*   Cutler, J. (2005). _The cross-cultural communication trainer's manual_. Aldershot: Gower.
*   Dervin, B. (1999). On studying information seeking methodologically: the implications of connecting metatheory to method. _Information Processing & Management, 35_(6), 727&dash;750.
*   Dervin, B. (2008). _Interviewing as dialectical practice: Sense-Making Methodology as exemplar._ Paper presented at the International Association for Media and Communication Research Annual Meeting, Sweden.
*   Dervin, B. (1997). Given a context by any other name: methodological tools for taming the unruly beast. In Pertti Vakkari, Reijo Savolainen & Brenda Dervin (Eds.). _Information seeking in context. Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts, 14-16 August 1996, Tampere, Finland_ (pp. 13-38). London, UK: Taylor Graham.
*   Dorner, D. G. & Gorman, G. E. (2011). [Contextual factors affecting learning in Laos and the implications for information literacy education](http://www.webcitation.org/6WTOVPRtv). _Information Research, 16_(2). Retrieved from http://www.informationr.net/ir/16-2/paper479.html (Archived by WebCite® at http://www.webcitation.org/6WTOVPRtv)
*   Dutta, R. (2009). Information needs and information-seeking behavior in developing countries: a review of the research. _The International Information & Library Review, 41_(1), 44&dash;51.
*   Fisher, K. E. & Naumer, C. M. (2006). Information grounds: theoretical basis and empirical findings on information flow in social settings. In A. Spink & C. Cole (Eds.), _New directions in human information behavior_. Berlin, Heidelberg: Springer-Verlag.
*   Fourie, I. (2008). [Information needs and information behaviour of patients and their family members in a cancer palliative care setting: an exploratory study of an existential context from different perspectives](http://www.webcitation.org/6WTOaGNK4). _Information Research_, _13_(4). Retrieved from http://www.informationr.net/ir/13-4/paper360.html (Archived by WebCite® at http://www.webcitation.org/6WTOaGNK4)
*   Freeburg, D. S. (2013). [_Information culture and belief formation in religious congregations_.](http://www.webcitation.org/6WTOdwTGz) Unpublished doctoral disseration, Kent State University, Kent, Ohio, USA. Retrieved from http://rave.ohiolink.edu/etdc/view?acc_num=kent1383573397 (Archived by WebCite® at http://www.webcitation.org/6WTOdwTGz)
*   Gaston, N. M. (2014). [_Contextualising information behaviour: the example of Laos_.](http://www.webcitation.org/6WTOi4963) Unpublished doctoral dissertation, Victoria University of Wellington, Wellington, New Zealand. Retrieved from http://researcharchive.vuw.ac.nz/handle/10063/3196 (Archived by WebCite® at http://www.webcitation.org/6WTOi4963)
*   Gaston, N. M., Dorner, D. & Johnstone, D. (2013). Information behaviour in transition: a developing country perspective. _Proceedings of the American Society for Information Science and Technology,_ , _50_(1), 1&dash;16./li>
*   Gee, J. P. (2011a). _An introduction to discourse analysis: theory and method_ (3rd ed.). New York, NY: Routledge.
*   Gee, J. P. (2011b). _How to do discourse analysis: a toolkit_ (3rd ed.). New York, NY: Routledge.
*   Harris, R. M. & Dewdney, P. (1994). _Barriers to information: how formal help systems fail battered women_. Westport, CT: Greenwood Press.
*   Hersberger, J. (2005). Chatman's information poverty. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), (pp. 75&dash;78). Medford, N.J: Published for the American Society for Information Science and Technology by Information Today.
*   Hirwade, M. A. (2010). Responding to information needs of the citizens through e-government portals and online services in india. _The International Information & Library Review, 42_(3), 154&dash;163.
*   Hofstede, G. H. (2001). _Culture's consequences: comparing values, behaviors, institutions, and organizations across nations_ (2nd ed.). Thousand Oaks, CA: Sage Publications.
*   Hofstede, G. H. & Hofstede, G. J. (2005). _Cultures and organizations: software of the mind_. New York, NY: McGraw-Hill.
*   Holt, J. (2009). _Spirits of the place: Buddhism and Lao religious culture_. Honolulu, HI: University of Hawai'i Press.
*   Ikoja-Odongo, R. & Ocholla, D. N. (2003). Information needs and information-seeking behavior of artisan fisher folk of Uganda. _Library & Information Science Research, 25_(1), 89&dash;105.
*   Ingwersen, P. & Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context_. Dordrecht, The Netherlands: Springer.
*   Kari, J. (2007). A review of the spiritual in information studies. _Journal of Documentation, 63_(6), 935&dash;962.
*   Khalil, O. E. M. & Seleim, A. (2010). National culture practices and cocietal information dissemination capacity. _Journal of Information & Knowledge Management, 9_(2), 127&dash;144.
*   Kitayama, S. & Park, J. (2010). Cultural neuroscience of the self: understanding the social grounding of the brain. _Social Cognitive and Affective Neuroscience, 5_(2-3), 111&dash;29.
*   Kitayama, S. & Uskul, A. K. (2011). Culture, mind, and the brain: current evidence and future directions. _Annual Review of Psychology,_ , 419&dash;49.
*   Kuhlthau, C. C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science, 42_(5), 361&dash;371.
*   Kuhlthau, C. C. (2005). Kuhlthau's information search process. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of infomation behaviour_ (pp. 230&dash;234). Medford, NJ: Published for the American Society for Information Science and Technology by Information Today.
*   Lafont, P-B. (2013). [Laos.](http://www.webcitation.org/6WTRDboX4) In _Encyclopedia Britannica_. Retrieved July 12, 2013, from http://www.britannica.com/EBchecked/topic/330219/Laos (Archived by WebCite at http://www.webcitation.org/6WTRDboX4)
*   Markus, H. R. & Kitayama, S. (1991). Culture and the self: implications for cognition, emotion, and motivation. _Psychological Review, 98_(2), 224&dash;253.
*   Markwei, E. D. (2013). _Everyday life information seeking behaviour of urban homeless youth_. Unpublished doctoral dissertation, University of British Columbia, Vancouver, BC, Canada.
*   Miwa, M. (2005). Bandura's social cognition. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior_ (pp. 54&dash;57). Medford, NJ: Published for the American Society for Information Science and Technology by Information Today.
*   Momodu, M. O. (2002). Information needs and information seeking behaviour of rural dwellers in Nigeria: a case study of Ekpoma in Esan West local government area of Edo State, Nigeria. _Library Review,_ (8), 406&dash;410.
*   Nahl, D. (1997). Ethnography of novices' first use of web search engines: affective control in cognitive processing. _Internet Reference Services Quarterly,_ (2), 51&dash;72.
*   Nahl, D. (2005). Measuring the affective information environment of web searchers. _Proceedings of the American Society for Information Science and Technology, 41_(1), 191&dash;197.
*   Nahl, D. (2007). A discourse analysis technique for charting the flow of micro-information behavior. _Journal of Documentation, 63_(3), 323&dash;339.
*   Naumer, C. M., Fisher, K. E. & Dervin, B. (2008). _Sense-making: a methodological perspective._ Paper presented at the Computer Human Interaction annual meeting CHI'08, Florence, Italy.
*   Nisbett, R. E. (2003). _The geography of thought: how Asians and Westerners think differently—and why_. New York, NY: Free Press.
*   Njoku, I. F. (2004). The information needs and information-seeking behaviour of fishermen in Lagos State, Nigeria. _The International Information & Library Review, 36_(4), 297&dash;307.
*   Paisley, W. J. (1968). Information needs and uses. _Annual Review of Information Science and Technology, 3_, 1&dash;30.
*   Patitungkho, K. & Deshpande, N. J. (2005). Information seeking behaviour of faculty members of Rajabhat universities in Bangkok. _Webology, 2_(4), 25&dash;33.
*   Patton, M. Q. (1990). _Qualitative evaluation and research methods_ (2nd ed.). Newbury Park, CA: Sage Publications.
*   Peters, M. A. (2007). Kinds of thinking, styles of reasoning. _Educational Philosophy and Theory,_ (4), 350&dash;363.
*   Pettigrew, K. E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behaviour among attendees at community clinics. _Information Processing & Management, 35_(6), 801&dash;817.
*   Pettigrew, K. E., Fidel, R. & Bruce, H. (2001). Conceptual frameworks in information behavior. _Annual Review of Information Science and Technology, 35_, 43&dash;78.
*   Pholsena, V. (2006). _Post-war Laos: the politics of culture, history, and identity_. Ithaca NY: Cornell University Press.
*   [Religion.](http://www.webcitation.org/6WTOrfFNa) (2012). In _Collins English Dictionary - Complete & Unabridged 10th Edition._ Retrieved from http://dictionary.reference.com/browse/religion?s=t (Archived by WebCite® at http://www.webcitation.org/6WTOrfFNa)
*   The World Bank. (2015). [Internet users (per 100 people)](http://www.webcitation.org/6WTP5EBxY) Retrieved from http://data.worldbank.org/indicator/IT.NET.USER.P2 (Archived by WebCite® at http://www.webcitation.org/6WTP5EBxY)
*   Tuominen, K. & Savolainen, R. (1997). A social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information seeking in context. Proceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp. 81-96). London: Graham Taylor..
*   Tuominen, K., Savolainen, R. & Talja, S. (2005). The social constructionist viewpoint on information practices. In K. E. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of infomation behavior_ (pp. 328&dash;333). Medford, N.J: Published for the American Society for Information Science and Technology by Information Today.
*   United Nations Development Programme. (2015). [Table 1: Human Development Index and its components.](http://www.webcitation.org/6WTPDGCI4) Retrieved from http://hdr.undp.org/en/content/table-1-human-development-index-and-its-components (Archived by WebCite® at http://www.webcitation.org/6WTPDGCI4)
*   Van den Bos, K. (2009). Making sense of life: the existential self trying to deal with personal uncertainty. _Psychological Inquiry,_ (4), 197&dash;217.
*   Van den Bos, K. & Poortvliet, P. (2005). An enquiry concerning the principles of cultural norms and values: the impact of uncertainty and mortality salience on reactions to violations and bolstering of cultural. _Journal of Experimental Social Psychology, 41_(2), 91&dash;113.
*   Van den Bos, K., Uwema, M. C., Poortvliet, P. M. & Maas, M. (2007). Uncertainty management and social issues: uncertainty as an important determinant of reactions to socially deviating people. _Journal of Applied Social Psychology, 37_(8), 1726&dash;1756.
*   Wicks, D. A. (1999). The information-seeking behavior of pastoral clergy: a study of the interaction of their work worlds and work roles. _Library & Information Science Research, 21_(2), 205&dash;226.
*   Wilson, T. D. (1981). [On user studies and information needs.](http://www.webcitation.org/6XRiEMhoT) _Journal of Documentation, 37_(1), 3-15 Retrieved from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite® at http://www.webcitation.org/6XRiEMhoT)
*   Wilson, T. D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing & Management, 33_(4), 551&dash;572.
*   Wilson, T. D. (1999). Models in information behaviour research. _Journal of Documentation, 55_(3), 249&dash;270.