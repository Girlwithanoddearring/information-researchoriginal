#### vol. 20 no. 2, June, 2015

# Blogging motivations of women suffering from infertility

#### [Jenny Bronstein](#author) and Maria Knoll  
Department of Information Science, Bar-Ilan University, Ramat-Gan 52900, Israel

#### Abstract

> **Purpose**. The study reports the content analysis of twenty infertility blogs that examined the intrinsic and extrinsic motivations of the women writing these blogs. Intrinsic motivation results in doing something because it is inherently interesting or enjoyable and extrinsic motivation results in doing something for its instrumental value.  
> **Method**. The sample of 400 posts was analysed using the thematic analysis approach.This is a process for encoding qualitative information that facilitates identifying, analyzing and reporting patterns or themes within data.  
> **Findings**. Infertility bloggers were motivated to blog by intrinsic and extrinsic motivations. The main intrinsic motivation revealed was self-expression. Bloggers detailed the various ways infertility affects their identities, careers and marriages. The two extrinsic motivations identified were the coping mechanisms bloggers use to manage their struggle with infertility and the social elements of blogging.  
> **Conclusion**. Understanding the motivations behind blogging is important as earlier studies have found that blogging has a positive impact on the bloggers' well-being. This study presents an example of the use of social media as a coping mechanism to deal with a life crisis by creating a secure and protected personal space on the Internet and as a medium to change societal views on meaning of family and marriage.

## Introduction

Blogs have become one of the most popular forms of online communication since their advent in the mid 1990s ([Nardi, Schiano and Gumbrecht, 2004](#nar04b)). At their beginnings, these online diaries were posted on individual home pages mostly by programmers or computer scientists since these early diaries required minimal skills in Web design. Writing a blog became an option for all Internet users when several companies offered a ready-made blank frame on which one could type whatever one wanted. The frame was then uploaded by the company, which also offered to create a site for free if needed. The technological knowledge requirement was thus completely bypassed ([Serfaty, 2004](#ser04)).

Weblogs started out as lists of annotated links to other sites, progressively expanding to include the author’s thoughts on a variety of topics as well as commentary about current events. ([Blood, 2002](#blo02)). Since then, blogs have become an important venue for self-expression ([Bronstein, 2012](#bro12); [Yao, 2009](#yao09)), relationship-building and management ([Miura and Yamashita, 2007](#miu07)), community identification ([Shen and Chiou, 2009](#she09)), documenting one's life ([Hollenbaugh, 2010](#hol10); [Nardi, _et al.,_ 2004](#nar04b)), for the communication of one’s identity ([Hollenbaugh, 2010](#hol10)) and as storytelling tools for marketing purposes ([Hsiao, Lu and Lan, 2013](#hsi13)). Studies also have found that blogging enhances physical health and subjective well-being ([Ko and Chen, 2009](#ko09); [Miura and Yamashita, 2007](#miu07)), establishes and develops intimacy ([Ellison, Heino and Gibbs, 2006](#ell06)) and helps manage the blogger's image online ([Omarzu, 2000](#oma00)).

The motivations behind individually authored blogs are an important area of study since these social media sites provide their authors with a personal space on the Web in which to publish their views, feelings and life experiences easily and for free. It is also a platform for socialisation that enables the creation and maintenance of social relations and the creation of communities of shared interests.

## Literature review

The most popular blogs are personal blogs through which people share their experiences, thoughts and feelings ([Jones and Fox, 2009](#jon09)). Serfaty ([2004](#ser04)) explains that personal blogs are a form of self-representational writing and are essentially online diaries. These diary-like, personal blogs are sites of self-disclosure where individuals share observations and thoughts about their online and offline lives ([Blood, 2002](#blo02)). Gumbrecht ([2005](#gum05)) posited that self-expression is possible because blogs are perceived by bloggers as _protected spaces_ that allow them to share their feelings and tell their stories or relate their thoughts to an invisible audience without interruption. However, blogs also act as _public spaces_ ([Baoill 2004](#bao04); [Bronstein, 2013a](#bro13a); [Osell 2007](#ose07); [Ratliff 2009](#rat09); [Roberts-Miller 2004](#rob04); [Youngs 2007](#you07)) that facilitate interaction with other bloggers and readers, thus allowing for the emergence and sustenance of communities of shared interests and sub-cultural identifications ([Hodkinson, 2006](#hod06); [Wei, 2004](#wei04)). In addition, blogs can have an empowering effect since they provide a greater voice to those who are not often heard in the public sphere, such as women ([Wilson, 2005](#wil05)). Hence, personal blogs written by women are the central interest of the present study that examines the motivations that drive women suffering from infertility to blog.

### Infertility blogs

Infertility is commonly defined as failure to conceive after at least one year of attempting to achieve a pregnancy with unprotected intercourse ([Schmidt, Christensen and Holstein, 2005](#sch05)). The diagnosis and treatment of infertility is frequently described as one of the greatest sources of life stress, comparable to other major life stressors such as divorce, bereavement and chronic illness ([Benyamini, Gozlan and Kokia, 2009](#ben09)). While each individual experiences infertility differently, research shows that the experiences have lasting emotional effects ([Verhaak, Smeenk, Nahuis, Kremer and Braat, 2007](#ver07)). Unsuccessful treatment can leave women feeling sad, anxious and depressed ([Hammarberg, Astbury and Baker, 2001](#ham01)), with a sense of loss and bereavement ([Lechner, Bolman and Van Dalen, 2007](#lec07)).

The literature on infertility blogs is limited and lacking empirical studies but there are a few essays that touch upon various aspects of infertility blogging. Wells ([2011](#wel11)) examined the rhetorical strategies used in an online community based on experiences of infertility. Ratliff ([2009](#rat09)) dealt with a case of infertility bloggers who facilitated political transformations through their online writing by engaging in feminist activism. Strif ([2005](#str05)) analysed the narrative of infertility blogs and observed that many women choose to remain anonymous and use their blogs as a public forum to discuss their own infertility and treatment, often in explicit detail.

### Motivations for blogging

Brady ([2006](#bra06), p. 4) describes the concept of motivation as ‘_a directing force over behaviour and that motivation can act to begin the behaviour as well as influence its continuation_’. In their study of political blogs Ekdale, Namkoong and Fung ([2010](#ekd10)) asserted that blogging is influenced by both intrinsic and extrinsic motivations. Building on the work of Ryan and Deci ([2000](#rya00), p. 55), they further claimed that intrinsic motivations relate to the nexus between a person and a task, that is,the reward was in the activity itself. Contrarily, extrinsic motivations relate to the performance of an activity or task to attain some separable outcome.

One of the strongest motivations behind personal blogs is the bloggers' need for socialization. '_By identifying, formulating and discussing problems and interests, a socially shared view can evolve through the interaction of others_'. ([Mosel, 2005](#mos05), p. 4). Socialisation can either be an intrinsic motivationto fulfill a personal need of the blogger to contact people on the Internet or an extrinsic one, that drives bloggers to maintain relationships formed elsewhere ([Walker, 2000](#wal00)).

The reasons and motivations for authoring a blog have been at the center of blog research in recent years. In one of the first studies about blogging motivations Nardi _et al.,_ found that blogs support both intrinsic and extrinsic motivations since ‘_blogging is an unusually versatile medium, employed for everything from spontaneous release of emotion to archivable support of group collaboration and com¬munity_’ (Nardi _et al.,_ [2004b](#nar04b), p. 46). A number of studies have identified different intrinsic motivations. One of the more salient is the motivation bloggers have to express intimate feelings and thoughts. Nardi, Schiano, Gumbrecht and Swartz ([2004a](#nar04a)) describe blogging as a way of '_working out issues_' and achieving emotional release. Lenhart and Fox ([2006](#len06)) found that three out of four bloggers do so to express themselves creatively. Bronstein ([2012](#bro12)) posited that self-expression through blogging can help bloggers achieve a deeper understanding of themselves while expressing and disclosing personal information on their blog. Hollenbaugh and Everett ([2013](#hol13)) found that women bloggers feel the need to disclose more breadth of subjects. Other intrinsic motivations revealed in the literature describe blogging as a means of improving the blogger's writing skills and as a pastime ([Armstrong and McAdams, 2011](#arm11); [Bronstein, 2012](#bro12); [Efimova, 2009](#efi09); [Hong-min, 2011](#hon11); [Li, 2007](#li07); [Miller and Shepherd, 2004](#mil04); [Nardi, _et al.,_ 2004a](#nar04a); [Yao, 2009](#yao09)). The need to document one's life is an additional intrinsic motivation related to self-expression. Hollenbaugh ([2010](#hol10)) asserted that women whoblog to document their lives,do soprimarily for themselves and disclose the most private information.

Extrinsic motivations also have been identified in previous studies. Writing a blog can be motivated by an extrinsic need to share information with others as a channel of communication and socialization. This extrinsic motivation is reflected in the interaction with others by sharing information and by receiving feedback and comments from an audience ([Bronstein, 2012](#bro12); [Liu, Liao and Zeng, 2007](#liu07); [Lu and Hsiao, 2007](#lu07); [Nardi, _et al._, 2004a](#nar04a); [Xiaohui, 2010](#xia10)). Bronstein ([2012](#bro12)) found that for some bloggers, the need to influence others was the driving force behind the motivation to share information. Participants in her study talked about their desire to have an impact on their readers and to educate them on different subjects. Nardi, _et al._ ([2004a](#nar04a), p. 226) presented a similar finding when participants in their study '_expressed opinions and advice with a clear statement of particular actions they wished their readers to take_'. Hollenbaugh ([2011](#hol11), p. 18) found that a salient motivation to blog was the desire to help, motivate and encourage others by sharing information and asserted that this fact '_might communicate a sense of caring and support for their audiences_'. The present study investigated the intrinsic and extrinsic motivations that drive women suffering from infertility to blog.

### Value of the study

This study presents the results of the content analysis of blog posts written by women dealing with infertility. The current research presents an interesting view of the motivations that drive women undergoing a critical period in their lives to share their experiences and to look for support on their blogs. Understanding why people blog will add to the knowledge of blogs as a communication medium enhancing knowledge of development of patterns of information behaviour and contributing to innovations in information and communication technology.

## Methods

### Research questions

The following research questions can be drawn from the main purpose of the study:

1.  Which intrinsic motivations drive women suffering from infertility to blog?
2.  Which extrinsic motivations drive women suffering from infertility to blog?

### Sampling

Twenty bloggers were identified through Google search engine, using _infertility blog_ as a search phrase. This initial search phrase led to extensive lists of infertility blogs such as: [Stirrup Queens](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.stirrup-queens.com&date=2015-05-25http://www.webcitation.org/query?url=http%3A%2F%2Fwww.stirrup-queens.com&date=2015-05-25), [Cyclesista](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.cyclesista.org&date=2015-05-25), [Lost and Found](http://www.webcitation.org/query?url=http%3A%2F%2Flostandfound-bailee.blogspot.co.uk&date=2015-05-25), [Glow in the Woods](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.glowinthewoods.com&date=2015-05-25). All the blogs selected are written in English. Although men create infertility blogs, this analysis chose to focus on those authored by women. Four hundred posts were included in the sample that was collected over a period of two months. The frequency and length of blog posts vary greatly.

Criterion sampling, a purposive technique in which all the cases selected for analysis must meet some predetermined criterion or set of criteria ([Creswell, 2007](#cre07)), was determined to be the most appropriate blog-selection method. Criterion sampling can add key elements to qualitative research and helps ensure more accurate in-depth analysis because the selected cases meet the pre-set criteria.

The following list of criteria was used to choose the blogs included in the sample the blogs:

*   Must be written in English.
*   Must be identified as related to the infertility efforts of the creator/primary author.
*   Must be created by a woman or a group of women who together own the blog.
*   Must have been maintained for at least the two months prior to the beginning of the research process.
*   Entries must contain dates in reverse chronological order, with at least four entries made a month, preferably with at least one posting a week.
*   Entries for the two selected months must be accessible on the blog site through archives or continuous postings on the blog main page or other means.
*   The creator or primary author must be contactable via e-mail or through the ability to post comments to which she can respond. The e-mail addresses of the bloggers were usually created exclusively as a contact detail of the blog, having the name of the blog in it and do not reveal any personal information about the blogger.

The above list of criteria was adapted from Rausch (2006).

### Thematic content analysis

Qualitative research was deemed appropriate for the purposes of this research project because the aim was to understand the bloggers’ accounts of meanings, experiences or perceptions related to infertility. The purpose here was not to seek an objective scientific truth, but rather to create a better understanding of the lived experience as reflected in the bloggers' writings. Based on a qualitative paradigm, this study used an inductive approach that involves immersion in the data to discover categories, dimensions and interrelationships ([Durrheim, 2006](#dur06)).

Data collected were analysed using a thematic analysis approach, a process for encoding qualitative information. More specifically, thematic analysis facilitates identifying, analysing and reporting patterns or themes within data. A theme is a pattern found in the information that describes and organizes the possible observations ([Boyatzis, 1998](#boy98)). According to Braun and Clarke ([2006](#bra06)), the _keyness_ of a theme is not necessarily dependent on quantifiable measures but rather on whether it captures something important in relation to the overall research question.

The sample of 400 postings collected over a two month period was printed out, read several times, during which ideas, points, details, incidents were identified. Phrases were identified as the minimal unit of analysis. As the coding took place, items were compared with previous elements coded in the same category to simultaneously develop its features, conditions and other properties, as well as its relation to other categories. The above process was carried out until saturation was reached, that is, when no new information seems to emerge during coding, when no new properties, dimensions, conditions, actions and interactions, or consequences are seen in the data ([Strauss and Corbin, 1998](#str98), p. 136). The two researchers analysed the data at the initial phase of the content analysis for validation purposes. The initial coding scheme was developed at this stage and included about 20% (N= 80) of data. As the coding scheme developed the two researchers revised the scheme in two additional occasions until it was clear that no new categories were appearing in the data so the final category scheme was completed. The final percentage of agreement for all coding decisions was 90%, which suggests that the coding classification used was reliable. The rest of the data were analysed and coded by one of the researchers.

## Results

The purpose of the study was to explore the intrinsic and extrinsic motivations that brought participants to blog about infertility. The following section presents examples of the textual data selected under each category.

### Intrinsic motivations

The main intrinsic motivation revealed in the study was self-expression. Data in this theme relate to the subjects that bloggers disclose on their blogs as a venue for self-expression in which bloggers write about their feelings and experiences making blogging a helpful or engaging activity that intrinsically motivates participants to blog. Through blogging, bloggers find satisfaction by the fact of purging out, expressing, venting their everyday struggle with infertility ([Liu _et al._, 2007](#liu07)). The content analysis revealed different categories of self-expression as well as the bloggers views and perceptions of the role of anonymity in blogging.

#### Emotional effect of infertility treatments

The emotional effects that accompany infertility treatments represent a major category. Bloggers were motivated to blog to vent their feelings and emotions describing the repeated cycle of hope, anticipation and devastation that go with the treatments.

> I know that sounds strange, but I am bracing myself for both a positive and negative outcome of this cycle. A BFN [Big Fat Negative] would be royally disappointing. And a BFP [Big Fat Positive] would be perfectly terrifying… I think I am fearing that time of unknown, or in-between. Being pregnant, but not having that pregnancy firmly established. Having it hang by a thread for weeks on end. Preparing for the best; preparing for the worst; and being invisible.

The emotions described by the bloggers cover a wide spectrum of feelings such as fears, anxiety and worry, anger, depression, grief and hope. Fear is highly present in the blog posts analysed. One of the bloggers describes this emotion as overwhelming.

> I am afraid of going through another fresh IVF [in vitro fertilisation] cycle, knowing how hard one is and that we will max out our insurance benefits by doing so. I know I *can* get through a fresh cycle, but I also know how hard it is. I am afraid that whatever isn't working can't be fixed. I am afraid we'll hit our limit before our family is complete.

The data analysis revealed that participants suffered anxiety attacks as an emotional outcome of their infertility. The following is a detailed description of an anxiety attack that overcomes the blogger.

> I first started experiencing what I must assume was anxiety on Monday evening. I couldn’t fall asleep Monday night and maybe got four hours of sleep total. The anxiety was full-fledged by Tuesday (yesterday) and it never fully went away until I woke up this morning feeling better, feeling like I could breathe again. I don’t know if it was cycle related or not, but it was horrible. My heart was racing, it felt like something had become lodged in my throat and I felt like I couldn’t breathe deeply enough to catch my breath. At times I felt nauseous and I just had this general feeling of uneasiness, like something was just wrong.

Anger is another strong emotion revealed in the content analysis relatedto the bloggers' inability to conceive. One of the bloggers diagnosed with polycystic ovary syndrome (PCOS) described her anger and frustration when confronted with vast amounts of information on how to solve her infertility.

> My gut reaction is to get angry - angry at the fact that I can't seem to find the magic bullet that is keeping me from getting pregnant. Then, my anger turns to self-hatred and disgust when I read about women successfully making some of these very dramatic changes, ending up with babies. How do I know which one? Do I do all of them and never eat a comfort food again? Do I force KG to make these changes too?

Failed cycles and miscarriages trigger a deep sense of loss and grief described in the bloggers' posts. The anonymity and friendliness of the blogs facilitates self-expression which motivates bloggers to express their intimate thoughts and feelings.

> How does one find the strength to go on in the midst of so much pain? When life is so hard?... Diagnosis is complete miscarriage. My body has killed my babies. We're both in shock from the destruction from this storm. But the numbness will wear off soon and we will once again be left with our grief.

Next to all the negative emotions listed above, there is also hope in the blog posts analysed, which constitutes the fuel that drives the bloggers to proceed with treatments with the goal of becoming mothers.

> I am excited to get this show on the road. It is good to start to feel some hope growing like a little seedling inside my heart. Like I said before the last cycle, this could happen. This procedure could lead to pregnancy and to having a baby. It could.

#### The effect of infertility on the bloggers' identity as women

One of the motivations for blogging is the communication of one's identity ([Bronstein, 2013a](#bro13a); [Qian and Scott, 2007](#qia07)). The following category presents the blog posts describing the effect that infertility has on the bloggers' identity and self-image. The impact that infertility has on the bloggers' view of womanhood was revealed as a major topic in the content analysis. One of the participants recounts finding out that she cannot have a child without IVF as a loss of her womanhood.

> Being told that you can’t have a child without the help of science is devastating. Not only does it mean that building a family will be costly, painful and difficult, but also that your body is incapable of doing the one thing that really sets women apart from men. For me, this was more painful than the injections, the retrieval and the heavy price-tag. The day the doctor said I-V-F I felt a portion of my womanhood die.

Another participant voices her emotion about the changes she has experienced during her infertility journey concerning her identity as a woman and concerning the meaning of motherhood for her.

> I used to think that being a mom was everything. It wasn't just something I wanted to do. It was something I had to do to fit in with society's expectations. And I had to do it a certain way (by drinking wine on vacation and getting knocked up by accident, of course). Now I know that neither this journey nor the goal at the end of it should define what kind of woman I am. Do I want to be a mother? Yes. But if my fate is not to be one, I'm ready to accept that, too - and the grief and anguish that may come with it.

Findings show that infertility affects women’s body image. The following is a recount of one of the bloggers feeling unattractive and empty.

> I look in the mirror and feel barren and unattractive. I run from a camera. I edit my self-portraits obsessively,trying to photo shop in the part of me that has been cast out. Gradually new beauty products have crept into my life. I read several fashion blogs and I started to look at these beautiful and fertile women, trying to figure out how I could look like them. How can I paint fertility on my body? I still feel infertile. I still feel empty. I still do not want Mr. Husband to touch me. To look at me. I wish I knew how to fix this. How can I repair the hole that infertility has eaten into my womanly being? I desperately want to enjoy the fact that I am still young, I am pretty and by gosh, Mr. Husband does find me attractive even though my ovaries don’t work. But I don’t know how.

#### Blogging and anonymity

The effect that the anonymity inherent in online communications can have on blogging patterns has been researched in prior studies ([Bronstein, 2013b](#bro13b); [Hollebaugh and Everett, 2013](#hol13); [Qian andScott, 2007](#qia07)). Findings in this study show that bloggers struggle with the idea of disclosing their true identity and exposing themselves publicly and this struggle affects their motivation to blog about their condition. The following category presents the bloggers' views on anonymity and blogging. One of the bloggers who uses a pseudonym for signing her posts expressed her need to be less anonymous to share her experience:

> Even though I still blog anonymously, almost all of my friends (at least the close ones) and my immediate family know I am in IF treatment and many read this blog. I also told my boss and a few select co-workers (most of whom are IFers as well. ) Little by little, I have been honest with more and more people. I even came out to a few friends of friends, because they were also IFers and I wanted to reach out. It's almost like I am trying to swim in the ocean. I started by dipping my toes in. Then I waded to my knees, then my thighs, then my waist. But I can't quite seem to dive into a wave.

On the other hand, being a teacher, she also worries about the reaction that her co-workers and her students' parents could have if they were to find out about her infertility:

> This is the deal: I have not gotten to the point where I am comfortable having all my co-workers finding out. The biggest factor holding me back is that I am a teacher. One thing you should know about teachers is that they are chatty and love to gossip. I hope I'm not throwing my fellow teachers under the bus here, but if you have ever been in a teacher's lounge, you know how it goes. They also tend to let things slip to parents on occasion. I am not friends with any parents on FB, but you know the way the Internet is: you have to be ready for everything to become public, even without your consent. You just have to own it. Am I really ready for the families or students at my school finding out? Not so much.

The fear of being hurt is another reason for blogging anonymously revealed in the data analysis.

> The Internet provides a cloak of anonymity and sometimes a sense that the people who we write about will never see what's been written. Other times, though, the Internet becomes host to words that are meant to hurt. They are meant to be read by the people they target.

#### Marriage and infertility

Another subject that bloggers were motivated to express through blogging was the effects that infertility can have on marriage. The content analysis showed that infertility can affect a couple's relationship both negatively and positively.

Infertility can put a strain on a relationship as revealed from the posts in the sample. One of the bloggers described her disappointment on her partner who is consuming alcohol while on medication owing to male inf

ertility.

> I’m feeling disappointed and hurt by my man. It may sound completely stupid to an outsider, but he broke a promise to me last night. Last night he went over our agreed upon two-drink limit and consumed five beers while I was watching TV in the bedroom, savoring my one glass of hard cider. We just had this talk the other day. He promised to me that while he’s on new meds and while we are actively preparing for more IUIs or IVF, he would cut back on the drinking.

For another blogger the unsupportive behaviour of her spouse is a cause of conflict in the relationship that leaves her feeling alone in her struggle with infertility.

> I know we’re in this together but every now and then I feel like I’m pulling all the weight or like I care about it more. He acts like I am completely ridiculous in my expectations and maybe there are those who would think I am, but I would do anything to have a baby if I thought it would help. I have had dye pushed through my tubes, cysts scraped out of me, needles poked into me, washed sperm injected into me and I take a daily medicine that gives me nausea and sporadic diarrhea. Yeah, I know. Gross. But it happens and I deal with it. Because I want this baby. I know he does too, so why can’t he make this small sacrifice?

Contrary to the statements presented above, some bloggers write about the support they have gotten from their partners.

> That man, he just takes a licking and keeps on ticking. I wish I could be more like him. I feel like all the fight has gone out of me but he’s ready to jump in the ring and go again. I’m glad I married a fighter. I need him in my corner now more than ever.

#### The aftermath of infertility

Bloggers were also motivated to share their experiences related to the aftermath of infertility. This theme starts during treatments with thoughts about their duration followed by expressions about coming to terms with their infertility after deciding to stop the treatments.

> How many more cycles can I do? Is it the actual cycle itself that I resent so much?... or the disappointment when it doesn't work ? Can I just keep putting up with the medications, appointments, procedures to keep striding for our end goal?

Moving on after infertility is compared to rebuilding after a destructive tornado:

> Having spent the bulk of my life in Alabama, I am all too familiar with tornadoes and the destruction they can leave behind. It never ceases to amaze me, though, how the landscape always rebuilds and heals. People rebuild homes and businesses, animals rebuild their nests and mother nature repairs the landscape. It is awe inspiring. Entire communities can rebuild in the wake of a storm, surely we can rebuild in the wake of infertility.

Another participant describes the decision to move on as a new phase in the journey:

> TTC [Trying To Conceive] and infertility have been a huge part of my life for the past three years. THREE YEARS. Infertility defined me for the past two years. (I hate to say that, but it’s true. ) I put everything I had into understanding what was wrong and how we could treat it. I put literally everything I had into trying to make a baby. This is a new phase. A new adventure. I accept this. I am ready for it. This is absolutely the right thing. But, I’m still saying goodbye to that life I envisioned. Goodbye to the person I’ve been for the past three years. Goodbye to a part of myself.

The emotional limbo is replaced by a sense of relief after stopping treatment.

> Since we’ve decided to pursue adoption, I feel a sense of relief and calm about family building. The underlying terror that treatments wouldn’t work is gone. I know that I will have a family someday. It’s made it easier to interact with my friends and family who have children.

One participant who stopped fertility treatments consideredherself an ‘_infertility survivor_’ and described her motivation for blogging about life without children.

> I felt I needed a space for thoughts on my no kidding lifestyle, the good and the bad, remembering what was lost and celebrating what I have.

For another blogger living child-free doesn’t equal living life as a failure.

> But I resented the implication that we were failures. And so I also talked about success stories. Those of us who – through choice or not – were living life with no kids and were living it well. We were – I felt – the more courageous success stories. It is easier to do what everyone else is doing and do what you had planned on doing, be who you had planned to be, even if you don't always feel you fully belong amongst the fertile parents and pregnant women. It isn't as easy to feel you're on the edges of society. And so I felt it was important to say, “Hey, even if the worst happens, it’s still okay".

### Extrinsic motivations

The following excerpts present the data revealed in the analysis relating to extrinsic motivations that reflect the willingness of the bloggers to engage in an activity as a means to an end. The content analysis revealed two main extrinsic motivations. One motivation is to blog as a coping mechanism for dealing with infertility. The second is the motivation bloggers have for socialization that looks for the support and understanding of their readers and creates a community of bloggers sharing a common interest or problem.

#### Coping with infertility

The analysis shows that participants developed certain coping mechanisms that help them deal with their infertility. One of the coping strategies consists of finding distractions and keeping busy.

> Some weekends, when I’m feeling particularly helpless in my infertility, sewing is almost therapeutic - within several hours time I can turn a pile of cloth into something with purpose. While my ovaries might not be able to produce an egg on their own, my hands can produce garments, luggage, slip covers, pillows, cat beds and, of course, drapes with proper hems.

Another way of coping is getting as much information as one can about the process in hope of gaining some control of the situation.

> I struggle to understand our fertility treatments. I want to know how everything works and what I can do to make things successful. I want to understand what's wrong. I want to learn how to fix it. I am frustrated by our "unexplained" diagnosis. I don't want to just hope for the best or think positively. For me, that's not enough.

A blogger wrote about her searching for signs, analysing bodily symptoms similar to those described by other bloggers that could predict a positive outcome of the treatment.

> Searching the blogosphere or Internet, looking for cases similar to our own or stories from others who have traveled a similar path and now have a happy ending. We search for information, hoping for something that will help us turn the corner and find our happy ending.

A different coping technique is self-preservation, caring for oneself and protecting oneself from the disappointment of another negative cycle.

> What I realised this morning, following a particularly tough meditation session, is that all of this comes down to one thing: self-preservation. I don't know what the outcome of this cycle will be, but I'm determined not to jinx it by spending the next week in a pit of despair. But I also can't allow my hopes to be high either. I learned that lesson a long time ago about how damaging disappointment can be. I've crawled back into my cave, waiting with my back to the wall for the storm.

Finding professional help through therapy is yet another way of coping with infertility, as one of the participants describes:

> About a year into the TTC journey, it became clear that I needed to find a therapist. The continual monthly reminders that things were not working was a source of stress that was greatly affecting daily life. Add in the fact that it seemed everyone around me (be it in real life or on Internet support groups) was able to achieve pregnancy and I was one big ball of anxiety.

#### Socialization through blogging

The second extrinsic motivation found in the data analysis relates to the social role that blogging plays in the bloggers' lives. Echoing the findings of Nardi, _et al._ ([2004a](#nar04a)), we found blogging to be a social activity, a form of social communication in which the blogger perceives her audience as a support system in her struggle with infertility. Findings show that creating a platform for getting feedback, advice, encouragement and support is an extrinsic motivation for blogging. An ongoing monthly project is IComLeavWe which stands for International Comment Leaving Week. Each month the participants in this project write a post about their infertility status and they read and comment on every blog that participated in the project.

> Blogging is a conversation and comments should be honoured and encouraged. I like to say that comments are the new hug–a way of saying hello, giving comfort, leaving congratulations.

Many bloggers are motivated to blog because through blogging they share a sense of togetherness with the blogging community that empowers them when coping with infertility.

> When I was going through my grief – both for my two ectopic pregnancies and then for the loss of my fertility/chance of becoming a parent – I found tremendous support on-line. I was able to share my feelings and know I was heard. The support I received, the warmth, understanding and love was healing in itself. Yes, we talked about our losses, but we also talked about recovery, about what we found helped us and together we emerged out of our grief, helping each other as we inevitably slipped from time to time. We shared our wisdoms, debunked myths and gradually also became beacons of hope for those mired in those early dark days of their loss.

#### Infertility and in vitro fertilisation advocacy

As a way to socialise with others suffering from the same condition, bloggers used their blogs as a platform to educate or change society's view on infertility and marriage. The following excerpts present the bloggers' efforts to change society’s pronatalist view on marriage (marriage as a union for reproduction).

> Society recognises marriage as an important institution for these same reasons: to enhance stability in society and to respect and support parents in the crucial task of having children and bringing them up as well as possible.

Many bloggers decided to take a more militant approach and speak up publicly about their infertilityto raiseawareness about the condition and to educate people.

> I’ve designed a “#hope” t-shirt that represents the community of infertile women who support each other and share their stories through blogs and Twitter. I’ve found incredible support from this community. When you purchase a tee, $5 will go to Resolve. org - a non-profit organization that supports and advocates for men and women who are struggling with infertility. I will not make ANY money from the sales of these t-shirts and I will personally match your $5 donation! Resolve. org has been an amazing resource for me, helping me to understand my disease and that I am not alone. Throughout this hellish journey through infertility, I’ve discovered and embraced my inner advocate.

Another instance in which bloggers took a stand to help educate those who do not understand the experience of infertility was National Infertility Awareness Week (NIAW); a week dedicated to raising public awareness about infertility,and to supporting other women going through the same experience.

> NIAW is an opportunity to do exactly what I want most: to help educate those who don't understand the experience of infertility and to support other women who know that experience all too well. I am feeling more and more of a call to get involved with IF advocacy and I have made some small steps in that direction lately.

## Discussion

The current study presents the results of a content analysis of blog posts written by women suffering from infertility that identifies their motivations for blogging. Echoing Chen's ([2012](#che12)) study about women bloggers, the current research found that women blog to meet their needs for self-expression, socialization and for coping with a crisis. Chen further stated that women bloggers who feel the greatest need to self-disclose will be more inclined to fill this need through blogging. These women are likely to see blogging as a mean to express their own voice. The concept of voice draws on a metaphor that Mitra ([2001](#mit01), p. 459) used to show that the Internet could give marginalised groups a place to be heard. She explained that when a person creates a blog '_a voice has been placed in the cybernetic space where the traditional limits of real life could be irrelevant_'. Blogging is particularly suitable for expressing one's voice because bloggers have been found to see blogging as a way to express themselves creatively and share experiences ([Lenhart and Fox, 2006](#len06)). In the present study, self-expression was revealed as the only intrinsic motivation that drives women suffering from infertility to blog. Similarly to others studies about blogging motivations ([Bronstein, 2012](#bro12); [Chen, 2012](#che12); [Hollenbaugh, 2010](#hol10); [Lenhart and Fox, 2006](#len06); [Liu, Liao and Zeng, 2007](#liu07); [Mitra, 2001](#mit01); Nardi _et al.,_ [2004a](#nar04a)) bloggers in this study used their blogs as emotional outlets that allow them to vent freely about the impact that infertility has on their lives. Echoing Hollebaugh and Everett's ([2013](#hol13)) assertion that women bloggers present a deeper and more varied disclosure of personal information, findings show that participants write about a wide variety of subjects; they chronicle their emotions, the struggle in their relationships, their ways of coping with their inability to conceive and their thoughts about the aftermath of infertility. However, many bloggers needed to feel protected by the cloak of anonymity as one blogger described it, to be able to freely vent on their blogs. Qian and Scott ([2007](#qia07)) explained the need for anonymity by claiming that bloggers who are motivated to blog to vent their thoughts and emotions would be likely to seek anonymity. Blogs have been portrayed as protected spacesthat allow bloggers to share the feelings they would not share otherwise and tell their stories or relate their thoughts without interruption ([Gumbrecht, 2005](#gum05)). Bronstein ([2013b](#bro13b)), in her study of the information disclosure of Israeli bloggers, suggested that '_the anonymity possible in blogging facilitated the creation of a protected space in which bloggers felt free to disclose personal or embarrassing information, to show their sensitive side and to let their defences down thus allowing the creation of a personal space_'.

Depicting the emotional effect ofinfertility is central to the data related to the intrinsic motivation for self-expression. The phases of the infertility treatments discussed in the posts are accompanied by detailed descriptions of the bloggers’ emotional reactions. Studies have found that putting emotions into words through writing can be beneficial, both physically and psychologically ([Pennebaker and Seagal, 1999](#pen99) [Smith, Anderson-Hanley, Langrock and Compas, 2005](#smi05)) and blogs have been described as a medium for processing emotionally charged situations (Nardi, 2004b; Yao, 2009) while providing participants with a therapeutic release and a way of reflecting on the events in their lives (Yao, 2009). Furthermore, Pennebaker and Seagal (1999) stated that keeping a journal helped people translate their experiences into stories or narratives, which helped them make sense of these events and resulted in improved mental and physical health. They posited that a journal _'is a constructed story, then, is a type of knowledge that helps to organize the emotional effects of an experience as well as the experience itself_' ([Pennebaker andSeagal, 1999](#pen99), p. 1249).

Bloggers were also motivated to express the significant influence that infertility has on their identity and self-image. Regardless of the outcome of the infertility treatment, these women are permanently marked by the experience. One participant metaphorically names infertility her ‘permanent home’, suggesting that infertility would always be part of her life. Strif ([2005](#str05), p. 191) voiced a similar position in her paper, stating that ‘_even if the authors of these blogs do become pregnant, most insist upon continuing to identify themselves as infertile, or voice their perceived likelihood that their pregnancy will not continue and they will therefore be infertile again_’. Other studies have discussed the role of identity in blogging. Hevern ([2004](#hev04)) stated that a blog provides a picture of its author as he or she has chosen to construct it and that represents the blogger's journey though time. Lopes ([2009](#lop09)), in her study of _mommy blogging_, argued that the uniquely fragmented format of blogs has the potential to capture this multifaceted portrait of motherhood in a way that no other medium has been able to accomplish thus far. Our analysis of the data presents the different ways in which bloggers suffering from infertility perceive their identity and role as women; some bloggers are left feeling barren and unattractive, others question their role and identity as women when failing to conceive. Leggatt-Cook and Chamberlain ([2012](#leg12)) who studied blogs dealing with weight-loss, argued that when writing a blog about one's life, the blogger is constructing a self. They claimed that the blog's archive is a record of the fragmented and segmented self that has the potential to be continuously reinterpreted. Echoing participants in this study whose identity was defined by their infertility, Leggatt-Cook and Chamberlain found that weight-loss bloggers constructed a self who is losing or intends to lose weight.

The content analysis of the data revealed two categories related to extrinsic motivations, coping mechanisms and the social role of blogging. Coping mechanisms were categorised as extrinsic motivations because they described activities undertaken by the bloggers that lead to a separable outcome that is, managing the effects of infertility. Similarly, Ressler, Bradshaw, Gualtieri and Chui's ([2012](#res12)) study on blogs of cancer patients, found that the bloggers' overall ability to cope with chronic illness and pain was impacted positively by blogging. Bloggers in this study were motivated to look for distractions, to get informed about treatments and to seek therapy as a way to deal with their inability to conceive. An additional coping mechanism found was seeking support from an audience and from other infertility bloggers. This finding supports Baker and Moore's ([2008](#bak08)) study, which investigated coping mechanisms of bloggers, and found that blogging might be used to increase social networking and improve existing social-support structures.

The social aspect of blogging was revealed as the second extrinsic motivation. Bloggers in our study often took an active stand and decided to talk publicly offline and online about their infertility as a way to share information and educate their audiences and to connect and socialise with people with whom the share similar interests. One of the subjects that bloggers blogged about was the impact that the pronatalist view that society has on marriage affected their lives. The message they get from their surroundings is that they do not fulfill the purpose of marriage without procreating and often they encounter negative reactions regarding infertility treatments being catalogued as selfish. Instead of accepting society’s definition of family as rooted in biological kinship, participants suggested that family can be based in practices of care and connection and it can be achieved through alternative paths such as adoption or surrogacy. This finding concurs with several studies that indicated that the need to share information and to help others are strong motivations for blogging ([Lenhart and Fox, 2006](#len06); [Trammell _et al.,_ 2006](#tra06); [Walker, 2000](#wal00)). Bronstein ([2012](#bro12)), in her study about blogging motivations of Latin American bloggers, voiced a similar opinion claiming that the need to interact with others by sharing information and to educate and change people's minds is related to the need for socialisation that many bloggers describe as a motivation for blogging. Similarly, Hsu and Lin's ([2008](#hsu08)) showed that people actively engage in blogging to increase the welfare of others. Brady (2006) explained that the interactive nature of blogging makes sharing information and opinions easy and that part of this socialization occurs when readers comment or provide feedback to the blogger's posts. An example of the need of infertility bloggers to share their knowledge with others is the blogs posted during the National Infertility Awareness Week. The two month period while the posts were selected and analysed overlapped with this event. The large majority of the bloggers posted during fertility awareness week, taking an active role in raising awareness about their condition and confronting a wide array of subjects related to infertility. Findings show that participants view the National Infertility Awareness Week as an opportunity to help educate the publicabout infertility and to support other women who also suffer from the condition. This finding concurs with Hollenbaugh's ([2011](#hol11)) study that found that the need to share information to help and encourage others is a strong motivation for blogging.

A different element of socialisation as a motivation for blogging is the blogger's connection to an audience and the audience's role as a supportive community forum ([Liu _et al.,_ 2007](#liu07); [Nardi, _et al.,_ 2004a)](#nar04a). Supporting Chen's ([2012](#che12)) study which argued that women with a high need for affiliation will be more likely to report that they see blogging as a means to connect with other people with similar interests, our study found that women bloggers are motivated to blog to gain influence in the blogosphere and to form connections with other people as a form of support. Similarly, Leggartt-Cook and Chamberlain ([2012](#leg12)) showed that in the weight-loss blogosphere, supportive communities have largely emerged informally. Peers who have been through similar weight-loss challenges are held to provide understanding and support in ways that others cannot.

## Conclusion

The blogosphere offers women suffering from infertility a place to chronicle their personal stories, create a community, seek support, and raise awareness about their condition. This study shows that the public nature of the Internet enables women dealing with a difficult issue in their lives, to openly discuss their experiences and struggles with infertility, defying the inherent privacy of these treatments. The textual data collected from the blogs shows the value of this medium as a venue for self-expression, but also offers some evidence that blogging is not just about speaking out, but italso plays a key role in the creation of women's online identity, and of support communities.

Further research is needed to understand the different motivations behind the existence of blogs dealing with critical or difficult issues and a more extensive categorisation of the subjects disclosed in these blogs might help in the understanding of blogs as a communication medium and of the information behaviour of bloggers.

## About the author

**Jenny Bronstein** is a lecturer at the Information Science department at Bar-Ilan University. Her research interests are in library and information science education and professional development, self presentation and self disclosure on different social platforms and information behaviour in online platforms. She holds a PhD and a M. S. in Information Science from Bar-Ilan University and a B. A. in History and Linguistics from Tel-Aviv University She can be contacted at: [jenny.bronstein@biu.ac.il](mailto:jenny.bronstein@biu.ac.il)  
**Maria Knoll** is a graduate of the M.A programme in Information Science from Bar-Ilan University, she also holds a BA in English Literature and Linguistics from Petru Maior University in Romania. She currently works as a taxonomist and content specialist at Realmatch company.

#### References

*   Armstrong, C.L. & McAdams, M. L. (2011). Blogging the time away? Young adults' motivations for blog use. _Atlantic Journal of Communication, 19_(2), 113-128.
*   Baker, J. R. & Moore, S. M. (2008). Distress, coping, and blogging: comparing new Myspace users by their intention to blog. _CyberPsychology & Behavior, 11_(1), 81-85.
*   Baoill, A. (2004). Weblogs and the public sphere. In In L.J. Gurak, S. Antonijevic, L. Johnson, C. Ratliff and J. Reyman, (Eds.), _Into the blogosphere: rhetoric, community, and culture of Weblogs._ Retrieved from http://blog.lib.umn.edu/blogosphere/weblogs_and_the_public_sphere.html (Archived by WebCite® at http://www.webcitation.org/6YRE1SGUA)
*   Benyamini, Y. , Gozlan, M. & Kokia, E. (2009). Women's and men's perceptions of infertility and their associations with psychological adjustment: a dyadic approach. _British Journal of Health Psychology, 14_(1), 1-16.
*   Blood, R. (2002). _The weblog handbook. Practical advice on creating and maintaining your blog._ Cambridge, MA: Perseus Publishing.
*   Boyatzis, R. E. (1998). Transforming qualitative information: thematic analysis and code development. London: Sage Publications.
*   Brady, M. (2006). _Blogs:motivations behind the phenomenon._ Paper presented at the Information Communication and Society Conference, 12-14 September 2006, University of York, York, UK.
*   Braun, V. & Clarke, V. (2006). Using thematic analysis in psychology. _Qualitative Research in Psychology, 3_(2), 77–101.
*   Bronstein, J. (2012). Blogging motivations for Latin American bloggers: A uses and gratifications approach. In T. Dumova and R. Fiordo, (Eds.). _Blogging in the global society: cultural, political and geographical aspects_ (pp. 200-215). Hershey, PA: Information Science Reference
*   Bronstein, J. (2013a). Personal blogs as online presences on the Internet: exploring self-presentation and self-disclosure in blogging. _Aslib Proceedings, 56_(2), 161-181.
*   Bronstein, J. (2013b). Being private in public: information disclosure behaviour of Israeli bloggers. _Information Research, 18(4_) paper 600\. Retrieved from http://InformationR.net/ir/18-4/paper600.html (Archived by WebCite® at http://www.webcitation.org/6YRFhz2Rt)
*   Chen, G. M. (2012). Why do women write personal blogs? Satisfying needs for self-disclosure and affiliation tell part of the story. _Computers in Human Behavior, 28_(1),171-180.
*   Creswell, J. W. (2007). _Qualitative inquiry and research design:choosing among five approaches_. Thousand Oaks, CA: Sage Publications.
*   Durrheim, K. (2006). Basic quantitative analysis. In M. Terre Blanche, K. Durrheim & D. Painter (Eds.), _Research in practice: applied methods for social sciences_ (pp. 187-214). Cape Town, South Africa: University of Cape Town Press.
*   Efimova, L. A. (2009). _Passion at work: blogging practices of knowledge workers_. Enschede, Netherlands: Novay.
*   Ekdale, B., Namkoong, K., Fung, T.K. & Perlmutter, D.D. (2010). Why blog? (then and now): exploring the motivations for blogging by popular American political bloggers. _New Media and Society, 12_(2), 217-234.
*   Ellison, N., Heino, R. & Gibbs, J. (2006). Managing impressions online: self-presentation processes in the online dating environment. _Journal of Computer-Mediated Communication, 11_(2), 415–441.
*   Gumbrecht, M. (2005). _Blogs as "protected space"_. Paper presented at the Workshop on the Weblogging Ecosystem: Aggregation, Analysis and Dynamics, New York. Retrieved from http://www. blogpulse. com/papers/www2004gumbrecht. pdf (Archived by WebCite® at http://www. webcitation. org/6KPQMK3GK).
*   Hammarberg, K., Astbury, J. & Baker, H.W.G. (2001). Women’s experience of IVF: a follow-up study. _Human Reproduction, 16_(2),374–383.
*   Hevern, V. W. (2004). Threaded identity in cyberspace: Weblogs & positioning in the dialogical self. _Identity, 4_(4), 321-335.
*   Ho, S.S. & McLeod, D. M. (2008). Social-psychological influences on opinion expression in face-to-face and computer-mediated communication. _Communication Research, 35_( 2), 190–207.
*   Hodkinson, P. (2006). Subcultural blogging? Online journals and group involvement among UK Goths. In Axel Bruns and Joanne Jacobs, (Eds.). _Uses of blogs_ (pp. 187–198). New York, NY: Peter Lang.
*   Hollenbaugh, E.E. (2010). Personal journal bloggers: profiles of disclosiveness. _Computers in Human Behavior, 26_(6), 1657-166
*   Hollenbaugh, E.E. (2011). Motives for maintaining personal journal blogs. _Cyberpsychology, Behavior, and Social Networking, 14_(1-2), 13-20.
*   Hollenbaugh, E.E. & Everett, M.K. (2013). The effects of anonymity on self-disclosure in blogs: an application of the online disinhibition effect. _Journal of Computer-Mediated Communication, 18_(3), 283-302.
*   Hong-min, L.U. (2011). A survey on the motivations of blog writing for higher vocational college students. _Journal of Guangzhou Panyu Polytechnic_, No. 2011-2\. Retrieved from http://en.cnki.com.cn/Article_en/CJFDTOTAL-YXZY201102012.htm (Archived by WebCite® at http://www.webcitation.org/6YRcTiBGp)
*   Hsiao, K. L., Lu, H. P. & Lan, W. C. (2013). The influence of the components of storytelling blogs on readers' travel intentions. _Internet Research, 23_(2), 160-182.
*   Hsu, C. L. & Lin, J.C.C. (2008). Acceptance of blog usage: t he roles of technology acceptance, social influence and knowledge sharing motivation. Information and Management, 45(1),65-74.
*   Jones, S. & Fox, S., (2009). Pew Internet and data memo. Washington, DC: Pew Research Center. Retrieved from http://www.pewInternet.org/~/media/Files/Reports/2009/PIP_Generations_2009.pdf (Archived by WebCite® at http://www.webcitation.org/6YRcX2stD)
*   Ko, H.C. & Chen, T.K. (2009). Understanding the continuous self-disclosure of bloggers from the cost-benefit perspective. In _2nd Conference on Human System Interactions, 2009\. HSI'09, Cantania, Italy_ (pp. 520-527). New York, NY: IEEE.
*   Lechner, L. , Bolman, C. & Van Dalen, A. (2007). Definite involuntary childlessness: associations between coping, social support and psychological distress. _Human Reproduction, 22_(1),288–294.
*   Leggatt-Cook, C. & Chamberlain, K. (2012). Blogging for weight loss: personal accountability, writing selves, and the weight-loss blogosphere. _Sociology of Health and Illness, 34_(7), 963-977.
*   Lenhart, A. & Fox, S. (2006). _Bloggers. A portrait of the Internet’s new storytellers._ Washington, DC: Pew Research Center.
*   Li, D. (2007). Why do you blog: a uses-and-gratifications inquiry into bloggers’ motivations. Paper presented at the Annual Meeting of the International Communication Association. Retrieved from http://citation.allacademic.com/meta/p_mla_apa_research_citation/1/7/1/4/9/pages171490/p171490-1.php (Archived by WebCite® at http://www.webcitation.org/6YnMz3MV6)
*   Liu, S.H., Liao, H.L. & Zeng, Y.T. (2007). Why people blog: an Expectancy theory analysis. _Issues in Information Systems, 8_(2), 232-237.
*   Lopez, L. K. (2009). The radical act of'mommy blogging': redefining motherhood through the blogosphere. _New Media & Society, 11_(5), 729-747.
*   Lu, H. & Hsiao, L. (2007). Understanding intention to continuously share information on weblogs. _Internet Research, 17_(4), 345-361
*   Miller, C.R. & Shepherd, D. (2004). Blogging as social action: a genre analysis of the weblog. In L. Gurak, L. Antonijevic, S. Johnson, L. Ratliff, C. and J. Reyman, (Eds. ), _Into the blogosphere: rhetoric, community, and culture of weblogs_. Retrieved from http://blog.lib. umn. edu/blogosphere/blogging_as_social_action_a_genre_analysis_of_the_weblog. html (Archived by WebCite® at http://www.webcitation.org/6YnNNlQtK)
*   Mitra, A. (2001). Marginal voices in cyberspace. _New Media and Society, 3_(1),29–48.
*   Miura, A. and Yamashita, K. (2007). Psychological and social influences on blog writing: an online survey of blog authors in Japan. _Journal of Computer-Mediated Communication, 12_(4),1452-1471.
*   Mosel, S. (2005). _Self directed learning with personal publishing and microcontent_. Paper presented at the Microcontent Conference , Innsbruck, Austria. Retrieved from http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.167.4378&rep=rep1&type=pdf (Archived by WebCite® at http://www.webcitation.org/6YoxIkoiu)
*   Nardi, B. A., Schiano, D.J. & Gumbrecht, M. (2004b). Blogging as social activity, or, would you let 900 million people read your diary? In _CSCW '04 Proceedings of the 2004 ACM conference on Computer supported cooperative work_ (pp. 222–231). New York, NY: ACM.
*   Nardi, B. A., Schiano, D.J., Gumbrecht, M. & Swartz, L. (2004a). Why we blog. _Communications of the ACM, 47_(12), 41-46.
*   Omarzu, J. (2000). A disclosure decision model: determining how and when individuals will self-disclose. _Personality and Social Psychological Review, 4_(2), 174–185.
*   Osell, T. (2007). Where are the women? Pseudonymity and the public sphere. Then and now. S&F Online, 5(2). Retrieved from http://sfonline.barnard.edu/blogs/osell_01.htm (Archived by WebCite® at http://www.webcitation.org/6YnNxVGrq)
*   Pennebaker, J.W. & Seagal, J. D. (1999). Forming a story: the health benefits of narrative. _Journal of Clinical Psychology, 55_(10), 1243–1254.
*   Qian, H. & Scott, C. R. (2007). Anonymity and self-disclosure on weblogs. _Journal of Computer-Mediated Communication, 12_(4), article 14, Retrieved from http://www.webcitation.org/6YnOCXxGz
*   Ratliff, C. (2009). Policing miscarriage: infertility blogging, rhetorical enclaves, and the case of House Bill 1677\. _WSQ: Women’s Studies Quarterly, 37_(1-2), 125–145.
*   Rausch, P. A. (2006). _Cyberdieting: blogs as adjuncts to women’s weight loss efforts_. Unpublished doctoral dissertation, University of Florida, Gainesville, Florida, USA.
*   Ressler, P.K., Bradshaw, Y.S., Gualtieri, L. & Chui. W. H. (2012). Communicating the experience of chronic pain and illness through blogging. _Journal of Medical Internet Research, 14_(5), 1-14.
*   Roberts-Miller, T. (2004). Parody blogging and the call of the real. In L. J. Gurak, S. Antonijevic, L. Johnson, C. Ratliff & J. Reyman, (Eds. ), Into the blogosphere: rhetoric, community, and culture of weblogs. Retrieved from http://blog.lib.umn.edu/blogosphere/parody_blogging.html (Archived by WebCite® at http://www.webcitation.org/6YnOjqgaf)
*   Ryan, R.M. & Deci, E.L. (2000) Intrinsic and extrinsic motivations: classic definitions and new directions. _Contemporary Educational Psychology, 25_(1), 54-67.
*   Shen, Chung-Chi & Jyh-ShenChiou. (2009). The effect of community identification on attitude and intention toward a blogging community. Internet Research, 19(4), 393-407.
*   Schmidt, L., Christensen, U. & Holstein, B.E. (2005). The social epidemiology of coping with infertility. _Human Reproduction, 20_(4), 1044–1052.
*   Serfaty, V. (2004). Online diaries: towards a structural approach. _Journal of American Studies, 38_(3), 457-471.
*   Smith, S., Anderson-Hanley, C., Langrock, A. & Compas, B. (2005). The effects of journaling for women with newly diagnosed breast cancer. _Psycho-Oncology, 4_(12), 1075–1082.
*   Strauss, A. & Corbin, J. (1998). _Basics of qualitative research_. Thousand Oaks, CA: Sage Publications.
*   Strif, E. (2005). Infertile me". The public performance of fertility treatments in Internet weblogs. _Women and Performance. A Journal of Feminist Theory, 15_(2), 189–206.
*   Trammell, K. Tarkowski,A. Hofmokl J. & Sapp A. (2006). Rzeczpospolitablogów [Republic of blog]. Examining Polish bloggers through content analysis. _Journal of Computer-Mediated Communication, 11_(3), 702-722\. Retrieved from http://onlinelibrary.wiley.com/doi/10.1111/j.1083-6101.2006.00032.x/full (Archived by WebCite® at http://www.webcitation.org/6YnPG4Gtc)
*   Verhaak, C.M., Smeenk, J.M.J., Nahuis, M.J., Kremer, J.A.M. & Braat, D.D.M. (2007). Long-term psychological adjustment to IVF/ICSI treatment in women. _Human Reproduction, 22_(1), 305–308.
*   Viegas, F. B. (2005). Bloggers' expectations of privacy and accountability: an initial survey. _Journal of Computer-Mediated Communication, 10(_3), article 12\. Retrieved from http://jcmc. indiana. edu/vol10/issue3/viegas. html (Archived by WebCite® http://www. webcitation. org/6KPTxv7ux)
*   Walker, K. (2000). "It's difficult to hide it": The presentation of self on Internet home pages. _Qualitative Sociology, 23_(1), 99-120.
*   Wei, C. (2004). Formation of norms in a blog community. In L. J. Gurak, S. Antonijevic, L. Johnson, C. Ratliff & J. Reyman, (Eds. ), _Into the blogosphere: rhetoric, community, and culture of weblogs_, Retrieved from http://blog.lib.umn.edu/blogosphere/formation_of_norms.html (Archived by WebCite® at http://www.webcitation.org/6YnPZGeOW)
*   Wells, C. (2011). The vagina posse: confessional community in online infertility journals. In Suzanne Diamond, (Ed.). _Compelling confessions: the politics of personal disclosure_ (pp. 202-221). Madison, NJ: Fairleigh Dickinson University Press.
*   Wilson, T. (2005). Women in the blogosphere. _Off Our Backs, 35_(5/6), 51–55.
*   Xiaohui, M. & Lei L. (2010). Why do people blog? Exploration of motivations for blogging. In _Web Society (SWS), 2010 IEEE 2nd Symposium_, (pp. 119-122). New York, NY: IEEE.
*   Yao, A. (2009). Enriching the migrant experience: blogging motivations, privacy and offline lives of Filipino women in Britain. _First Monday, 14_(3-2). Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/2163 (Archived by WebCite® at http://www.webcitation.org/6YnQ7OZd8)
*   Youngs, G. (2007). Making the virtual real: feminist challenges in the twenty-first century. _S&F Online, 5_(2). Retrieved from http://sfonline.barnard.edu/blogs/youngs_01.htm (Archived by WebCite® at http://www.webcitation.org/6YnQHrjFd)