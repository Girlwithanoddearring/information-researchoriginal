#### vol. 20 no. 2, June, 2015

# Information behaviour of Slovenian researchers: investigation of activities, preferences and characteristics

#### [Polona Vilar](#author) and [Primoz Juznic](#author)  
Department of Library and Information Science and Book Studies, Faculty of Arts, University of Ljubljana, Slovenia  
[Tomaz Bartol](#author)  
Agronomy Department, Biotechnical Faculty, University of Ljubljana, Slovenia

#### Abstract

> **Introduction.** The paper presents one segment of the first comprehensive national study investigating information behaviour of Slovenian researchers in all research disciplines in relation to selected demographic variables. Research questions addressed various types of information behaviour, format preferences, use of different types of sources, organization of work, collaboration, publishing, and impact of information and communication technologies.  
> **Method.** An online survey was used, containing content and demographic questions. It was based on a random sample of the central registry of all active researchers in Slovenia in all scientific disciplines and all age groups.  
> **Analysis.** Descriptive and bivariate analysis were employed.  
> **Results.** The most noticeable demographic elements of influence are age and discipline, and in a few cases also sex. In certain areas, the information behaviour of researchers under study conforms with general trends, for example in power-browsing, squirreling, skimming, etc. Information technology has a strong impact on research work and collaboration. A fairly large segment of researchers often work alone; this is particularly characteristic of researchers in the humanities. In Slovenia, these researchers also show a strong digital preference and do not differ much from researchers in other disciplines, probably because of the nationwide integrated system of research evaluation and centralized subscription to information resources. Low use of open access documents and weak foreign collaboration in some disciplines are also evidenced.  
> **Conclusions.** Despite some expected outcomes, certain previously unknown and noteworthy patterns have been identified which are characteristic of Slovenian researchers. This information constitutes a useful foundation for further research in this area.

## Introduction

Assessment of user behaviour has become an important element of studies which explore new information technologies. Researchers show some special characteristics because of the nformation-intensive specifics of their work. Information is used in many different formats and structures, as multiple resources are now available online. Information behaviour has thus adapted to digital practices which are a characteristic feature of contemporary information and communication technologies (further on referred to as new technologies). In the process of collaboration, communication and information sharing, researchers now also use more novel tools, associated with Web 2.0 and Web 3.0\. The increasingly evolving ICT apparatus progressively stimulates information- and data-intensive, distributed, collaborative and multi-disciplinary forms of scholarship.

In Slovenia, collaboration and publishing patterns of scientists show a very high level of multilateral collaboration as well as a very high output value regarding publications per researcher, very low costs of publishing and increasing impact (citations per paper), which reveals some specific differences between research fields ([Bartol, Budimir, Dekleva Smrekar, Pusnik and Juznic, 2014](#bar14)). Improved transparency of research outcomes has resulted in a better international integration of Slovenian sciences in all research disciplines. This has put new demands on researchers, thus probably influencing information behaviour. An earlier study of information behaviour was conducted by Vilar and Zumer ([2008](#vil08)), who looked at the behaviour and preferences of doctoral students and identified connections with personality traits. In the present paper we present the first comprehensive national survey of researchers. Its aim was to identify the patterns of information behaviour and respective preferences of all researchers in order to explore how these are influenced by selected demographic variables also investigated in some other studies (for example, age, sex, current work assignments and research discipline). The motivation was to better understand the types of scholarly information behaviour, impact of new technologies, organization and flow of scientific work, and to identify potential special traits of Slovenian scientists.

Some preliminary results of the present broader study, based on incomplete data (the online survey had not yet been completed at the time) and employing a more generalized analysis were published by Vilar, Juznic and Bartol ([2012a](#vil12a), [2012b](#vil12b)) and Vilar, Bartol, Juznic and Pisanski ([2012](#vilbar12)). It has been noticed that Slovenian researchers, in general, behave similarly to researchers in other countries, but have some distinguishing traits. New technologies have had an important impact on formats used, venues of access, etc. Research discipline (major research field), sector of employment, current work tasks, age and even sex have been identified as having influence in different aspects of information behaviour. Slovenian researchers share many traits with the general public: they use Web search engines more than 'formal resources', prefer e-materials, digital tools, require immediate access to full-text, etc. Also, they are dissatisfied with the existing library services and use them only rarely, except the Slovenian national OPAC-based library e-catalogue (COBISS). On the other hand, some issues have been identified which clearly distinguish scientists from the general public, especially the intensive reference judgment (researchers are obviously much more inclined toward judging the quality of information sources). Slovenian scientists differ, to some extent, from more usual trends in scholarly behaviour elsewhere. For example, Web 2.0 tools for research purposes (e.g., Weblogs, digital social networking tools) and open-access materials are used less frequently.

The outcomes of this paper present a more thorough examination and evaluation of the complete set of data and offer a more detailed insight into various aspects of information behaviour of researchers. This is relevant for a few reasons: to find out more about information behaviour of scientists in the current situation of abundant technologies and information overload, as well as provide some basis for comparison in further studies.

## Literature review

Researchers are a widely investigated user group. As Wilson ([1997](#wil97)) and Case ([2012](#cas12)) report, the first studies date back to the end of World War Two. Later studies centred on the uses of documents and systems. In the 1960s, the research focused on real information needs, for example, through 'task-oriented studies' ([Vakkari, 1999](#vak99)). Such studies were directed at well-defined groups and information use in the sense of information artefacts or information settings. In the next decade, researchers' interests expanded towards other aspects (non-task oriented studies) which focused on information behaviour of individuals, such as feelings, thoughts and other cognitive/affective issues. From the 1990s onwards, research puts an increasing emphasis on everyday life information situations and individual characteristics.

Not all scientific disciplines have always received equal attention ([Case, 2012](#cas12); [Wilson, 1997](#wil97)). Research started with natural scientists and technical engineers in the 1960s. In the 1980s, the studies began to include social sciences and humanities. The interest expanded to all other scientific areas and now increasingly also includes students (the so-called research offspring, a predictor of future scientific behaviour).

The studies address many different elements which may contribute to disciplinary differences; for example, cultural factors, as found by Wang (2006) or institutional differences, studied by Palmer ([1991a](#pal91a), [1991b](#pal91b)), King, Tenopir, Montgomery and Aerni ([2003](#kin03)) and Nicholas, Huntington and Jamali ([2007](#nic07)). Many such aspects were systematically overviewed by Palmer and Cragin ([2008](#pal08)). Various demographic variables are included in the studies, such as age, sex and role (for example, research or teaching tasks, and academic position). Relationships between scholarly information behaviour and age, sex and academic position were investigated by Niu et al. ([2010](#niu10)) and Niu and Hemminger ([2012](#niu12)). Nicholas, Huntington and Jamali ([2007](#nic07)) searched for differences between the pure researchers and researcher-teachers.

Contemporary studies in this field consistently report a very strong impact of new technologies on information behaviour of scientists (see for example Talja, Savolainen and Maula ([2005](#tal05))). On the other hand, many studies no longer detect differences between information behaviour of scientific researchers and other information users; information behaviour is governed by general principles such as accessibility, familiarity and circumstances. Apparently, changes in technology have removed some previously existing differences between disciplines or domains. Hemminger, Vaughan and Adams ([2007](#hem07)) thus argue that generalizations of disciplinary behaviour are becoming difficult due to the increasingly interdisciplinary nature of research.

User studies are thus also adopting a multidisciplinary approach. Information-seeking patterns as identified by Ellis ([1989](#ell89), [1993](#ell93), [Ellis, Cox and Hall, 1993](#ellcox93)), and further elaborated more recently by Meho and Tibbo ([2003](#meh03)), involved starting, chaining, browsing, differentiating, monitoring, extracting, verifying, ending, accessing, networking and information managing. Similar models were developed by Palmer, Teffeau and Pirmann ([2009](#pal09)), Brockman, Neumann, Palmer and Tidline ([2001](#bro01)) and University of Minnesota Libraries ([2006](#uni06)). Palmer, Teffeau and Pirmann ([2009](#pal09)), building on Unsworth's ([2000](#uns00)) model, provided a two-layer model of scholarly information activities, each of the activities consisting of two or more primitives. In this case, primitives are defined as basic or initial functions common to scholarly activities across disciplines, whereas activities are broader in scope, and have an explicit role in research and creation of new knowledge. The authors identified five core scholarly information activities: searching, collecting, reading, writing and collaborating. The study by University of Minnesota Libraries ([2006](#uni06)) also employed Unsworth's ([2000](#uns00)) model and organized scholarly primitives into four groups: discovering, gathering, creating and sharing. Brockman, Neumann, Palmer and Tidline ([2001](#bro01)) devised a broadly based conceptual framework for the information nature of scholarly work (mainly looking at the humanities, but the model is in fact broader), embracing the processes of reading, collaborative networking, researching and searching, and ways of writing. Some of the principal elements of these studies are also investigated in this study and are presented in detail in the next paragraph.

The aforementioned studies notice that some types of scholarly behaviour have not changed, such as browsing or berry-picking (evolving and adapting queries as a result of finding new information during searching) (Bates, [1989](#bat89), [2007](#bat07)). Some new patterns of information behaviour, however, have recently become more prominent. Rowlands and Fieldhouse ([2007](#row07)) identified the following new activities: skimming (parallel looking at different documents), navigating (random browsing through one or several resources to see what is available, i.e., '_the electronic sweet shop_'), power-browsing (reading only abstracts, titles and indexing terms, rather than text in full), squirreling (downloading materials to be read at some later occasion), cross-checking (verifying information in different sources), chaining (using links in citations to find new information), bouncing (leaving a resource very quickly, after having concluded that it is not relevant).

Besides explaining information-seeking activities, the newer studies also present findings with respect to other segments of the entire information process. The advent of new technologies has obviously brought about significant changes in academic research. This is reflected in the methods of information retrieval, information management, relevance judgement (as found, for example, by Steinerova, [2008](#ste08)), and respective practical application of information by researchers. Tenopir and associates ([2009](#ten09)) have systematically analysed, using questionnaires, how researchers in general and in specific scientific fields (medicine, physics, social science) use electronic journals, and how the use of electronic journals has changed information seeking as well as reading patterns. Such longitudinal studies provide a good basis for collecting data on researchers' information behaviour.

Fast, almost instant, access to information is becoming essential in a growing number of scientific fields. Digital scholarship has gained prominence both in research and teaching ([Borgman, 2009](#bor09); [JISC, 2012](#jisc12); [Moran, Seaman and Tinti-Kane, 2011](#mor11); [Veletsianos and Kimmons, 2012](#vel12)).

## Research design

The goal of the research presented here was to investigate various aspects of scholarly information behaviour in Slovenia by using the complete set of data collected during the experiment (our preliminary interim investigation was only based on a limited amount of data). We wished to explore and compare various concepts which had been investigated in similar circumstances elsewhere. Thus, we sought to gain a picture of the behaviour of Slovenian researchers in the sense of similarities as well as possible differences with regard to their peers in other surroundings. An insight into these activities is important. On the one hand, Slovenian science has been experiencing a rapid internationalisation. On the other hand, certain local particularities do exist (such as availability of some specific information resources or high teaching load of many researchers). To this end, we explored the respective concepts which are summarised in Table 1.

<table class="center"><caption>  
Table 1\. Summary of research design: concepts explored, elements within each concept and evaluation by respective variables  
</caption>

<tbody>

<tr>

<th>Concepts explored</th>

<th>Elements assessed (within each concept)</th>

<th>Evaluation of elements</th>

</tr>

<tr>

<td>Types of scholarly information behaviour</td>

<td>Skimming, power-browsing, squirreling, cross-checking, chaining, berry-picking, bouncing</td>

<td></td>

</tr>

<tr>

<td>Preferred format of tools and sources</td>

<td>Print, digital or both</td>

<td></td>

</tr>

<tr>

<td>Impact of new technologies on information behaviour and preferences</td>

<td>Searching and collecting of sources, relevance judgment, organizing sources, citation checking, reading, communicating, independent or collaborative writing.</td>

<td>Relationships with age, sex, academic discipline, work tasks (research and/or teaching).</td>

</tr>

<tr>

<td>Use of open access</td>

<td>Citing open access publications.</td>

<td></td>

</tr>

<tr>

<td>Organization of work, collaboration, publishing</td>

<td>Available time for searching, organizing information, quick reviewing, thorough reading, writing, collaborating nationally or wider; publishing patterns</td>

<td></td>

</tr>

</tbody>

</table>

Each concept in Table 1 is explained by a characteristic element of investigation. All elements are evaluated in connection with the respective demographic characteristics: age, sex, academic discipline and work tasks (research and/or teaching). The concepts addressed in the study and presented in Table 1 are summed up as five research questions:

*   RQ1: Which types of contemporary scholarly information behaviour are most strongly expressed by the Slovenian researchers?
*   RQ2: Which is the preferred format of tools and sources?
*   RQ3: In which ways do new technologies impact the work in terms of information behaviour activities, use and preference of sources and tools?
*   RQ4: How much do Slovenian scientists use (i.e. cite) open access documents?
*   RQ5: How do Slovenian researchers organize their activities in terms of time; for example, available time for various research-related activities, collaboration and co-authorship, publishing and dissemination of research outcomes?

## Method

A random sample of all active researchers was used. The sample involved researchers officially registered in Slovenia in 2011 by the Slovenian Research Agency, _active_ status indicating that such researchers were allocated at least 100 publicly funded research hours in the year 2011\. The Agency, which was established by the Government of the Republic of Slovenia in 2003, is a publicly-funded organization which supervises national research and development programmes. For the purposes of evaluation, the research activities in Slovenia are organized into the following research disciplines or research fields: natural sciences, technical sciences (engineering), medical sciences, agricultural sciences, social sciences, and humanities. In 2008 there were 1,128 research groups in Slovenia, respectively by disciplines: 181 natural sciences, 558 engineering, 95 medical sciences, 80 agricultural sciences, 142 social sciences and 72 humanities. One of the utilities of the Agency is the [Slovenian Current Research Information System](http://sicris.izum.si) which monitors research activities of every publicly funded researcher in the country. Within this system, every researcher is registered with a unique identification number. This central database was used to collect data on Slovenian researchers and contained 4,800 active researchers in 2011\. This is not a large number; however, it needs to be taken into consideration that the entire country has only two million inhabitants.

The Agency provided all necessary contact information. The researchers' ID numbers were used for systematic sampling. The sample consisted of every eighth researcher. Thus, 592 researchers received a personal e-mail invitation to participate in the online survey, which was open for two months from 14 September to 14 November 2011\. An e-mail reminder was sent a month before the closing of the polling. The response rate was 35% (207 researchers). As not all questions were answered by all respondents, the analysis focused on 122 researchers (20% of the total sample) who provided a sufficient number of answers to be included in a more detailed further examination. The response rate of 35% is comparable to response rates in many similar online surveys, as has been found in reviews by, for example, Nulty ([2008](#nul08)) and Morton _et al._ ([2012](#mor12)), where such a response rate was estimated to bring sufficient reliability. In the academic context, some authors achieved the results of 29.3% and 27.4% (Dykema et al. ([2013](#dyk13)) and Kwak and Radler ([2002](#kwa02)), respectively).

Women accounted for 46% of the participants. The age distribution was as follows: 27.3% 20-30 years, 37.4% 31-40 years, 17.2% 41-50 years, 12.1% 51-60 years, and 6.1% 61 years or more. 33% of the respondents were involved only in research, 53% were also teaching, and 14% also had additional fixed engagements (clinical practice, different administrative positions, work in industry, etc.). Some individuals were involved in several different activities, as in some other studies (e.g., Niu et al. ([2010](#niu10)) and Niu and Hemminger ([2012](#niu12)). The questionnaire enquired about the years of experience in either research or teaching, which are usually associated with position. In terms of active research time, the two largest groups were researchers with one to five years and over fifteen years experience (both 30%), followed by six to ten years (24%), and 11 to fifteen years (16%) of research experience. In teaching, the largest group had one to five years of experience (32%), followed by fifteen years or more (22%), less than one year (19%), six to ten years (16%) and eleven to fifteen years (11%). Most were employed at higher education institutions, such as universities and colleges (64%), 39% were employed in public research institutions, 8% at private research organizations, 4% were independent researchers, 3% worked in industry, and 5% in other areas (mainly hospitals). The sum of these activities is more than 100% as some respondents work in multiple sectors. Respondents were also asked to state their main research areas (as they were able to select two areas this sum also exceeds 100%). Distribution by research area is thus as follows: natural sciences 31.3%, social sciences 22.2%, technical sciences 20.02%, humanities 15.2%, medicine 12.1% and agriculture 8.1%. Interdisciplinary areas account for 13.3%.

The online questionnaire consisted of twenty-five questions: eighteen content questions (mostly Likert scale type) and seven demographic questions. Demographic questions were related to sex, age, type of current occupation (research, teaching), years of experience (referring to either research or teaching), employment status (independent researcher, employed at a research organization, in higher education, commercial organization), and research area (presented in the preceding section).

The eighteen content questions addressed various aspects of information behaviour. This paper deals with the following:

1.  the most frequent types of information behaviour (on the basis of the reviewed literature it was decided to focus on skimming, power-browsing, squirreling, cross-checking, chaining, berry-picking and bouncing, all of which are explained in the introduction section);
2.  use of (i.e. citing) open-access publications;
3.  preferred formats of information sources (printed, electronic, either);
4.  use of electronic tools to search for sources, but then printing these sources for reading;
5.  amount of time dedicated to various aspects of the information process (searching, organizing information, quick reviewing of sources, thorough reading, writing, collaborating);
6.  degree of collaboration with other researchers;
7.  patterns of publishing (publishing-remuneration, participation as an author on a voluntary or involuntary basis);
8.  opinion on the impact of new technologies on various aspects of research work (searching and gathering of information sources, relevance judgment, organization of acquired sources, citation checking, reading, communicating, independent writing, collaborative writing).

Cross tabulations and bivariate statistics (Pearson Chi-Square tests) were used for the assessment of relationships between demographic and content variables. This comprehensive study expands on the previously published analyses which were established on incomplete preliminary data and only provided some selected summarized results. The figures are mainly presented in numbers, not shares, thus allowing comparison between the groups. Some results are additionally explained by way of descriptive analysis, i.e. certain topics have been elaborated by means of data in percentages, for the purposes of providing some additional information. The group of oldest researchers (over sixty-one years of age) was fairly small, thus it was excluded from the more detailed overall analysis. It is nevertheless briefly elucidated at the end of the discussion section.

## Results

The results are organized in line with the research questions presented in the research design section. The first research question addressed various types of information behaviour. Age (i.e. stronger preference by younger researchers) is statistically significantly related to skimming (simultaneously looking at several sources on the computer screen) (sig. 0.001) and to cross-checking (checking accuracy in multiple sources) (sig. 0.046). Skimming is also preferred by researchers with fewer years experience in research and teaching (sig. 0.012 and 0.047 respectively). As can further be noticed in Figure 1, researchers of up to forty years of age favour new types of behaviour such as power-browsing or bouncing, although this is not firmly confirmed by statistical significance. Older researchers display somewhat more traditional (and thorough or time-consuming) types of behaviour, for example cross-checking or berry-picking. Two types of behaviour are associated with researchers' years of teaching: cross-checking (sig. 0.007), which they perform often or always, and power-browsing (sig. 0.02), which it is performed occasionally or often. The effect of sex differences, although slightly above statistical significance (sig. 0.054), can only be assumed in relation to squirreling.

<figure class="centre">![p670fig1: Relationship between age and types of information behaviour](p670fig1.jpg)

Legend: A - Cross-checking, B - Skimming, C - Power-browsing, D - Bouncing, E - Berry-picking, F - Squirreling, G - Chaining

<figcaption>Figure 1: Relationship between age and types of information behaviour by number of researchers (y axis)</figcaption>

</figure>

Besides the types of information behaviour which are shown in Figure 1, some further aspects of certain types of behaviour were investigated. In connection with squirreling (i.e. downloading documents for later reading), it was checked how often the researchers really read the saved documents in detail, as it was assumed that the ease of access may draw the researchers to pile up more material than they can in fact thoroughly examine. No statistically significant connections were found with any of the demographic variables. Overall results show that the most frequent answer (45%) was: often. Interestingly, only 9% of researchers always inspect what they save, and 36% do it only occasionally.

To shed more light on how researchers cope with information overload, the researchers were further asked to estimate the ratio of texts which are read thoroughly, compared to power-browsed documents. Figure 2 displays the ratios of the two activities and shows that 73% (33) researchers, regardless of age, tend to thoroughly read between 20% and 40% of documents. Nine (7%) read half of the documents and power-browse the other half. Quite a large group (17; i.e. 14%) only read 20% of documents, and a few reported power-browsing 90% of documents.

<figure class="centre">![FIG. 2\. Relationship between age and reading versus power-browsing](p670fig2.jpg)

<figcaption>Figure 2: Relationship between age and reading versus power-browsing by number of researchers (y axis)</figcaption>

</figure>

In the second research question the issues of preferred format of tools and sources (i.e. print or digital, or both) were investigated. According to Figure 3, age seems to hold influence (it is supported with statistical significance; sig. 0.030): younger researchers (aged twenty to forty) more often opt for digital-only, and older researchers (aged forty-one to sixty) prefer to have access to both formats. The scientific discipline does not seem to play a major role, as no connections were found, not even with the humanities.

In the same context it was further investigated whether the researchers use electronic tools to access sources and then print out the materials for further use (e.g. reading). The results show that digital preference for searching and print preference for reading is generally true for all scientists under study.

<figure class="centre">![FIG. 3\. Age groups and format preferences](p670fig3.jpg)

<figcaption>Figure 3: Age groups and format preferences (print, digital or both) by number of researchers (y axis)</figcaption>

</figure>

Additionally, the possible relevance of printed materials was also examined by way of assessing the use of information resources and materials in digital format (for the purposes of citations). Figure 4 shows that digital resources are cited strongly (over 40%) by over half of researchers. A statistically significant link was found with the age of researchers (sig. 0.003), indicating that younger researchers tend to prefer to cite digital materials. A third of researchers do not use digital sources very frequently, i.e. they cite them at a share of less than 20%. Preference for digital materials by younger researchers is further evidenced in the inverse relationship with the years of experience: the more years the researchers have been active, the smaller the share of digital materials they cite (sig. 0.029).

<figure class="centre">![FIG. 4\. Age groups and shares of citations of digital documents](p670fig4.jpg)

<figcaption>Figure 4: Age groups and shares of citations of digital documents (by number of researchers - y axis)</figcaption>

</figure>

The third research question investigated the impact of new technologies on the work of researchers in terms of use and preference of sources and tools. Eight scholarly activities were assessed (search and acquisition, relevance judgement, information organization, citation checking, reading, communicating, independent writing, and collaborative writing) in order to establish whether the researchers perceive them to be easier or more difficult with the new technologies.

<figure class="centre">![FIG. 5\. Age and opinion of the impact of new technologies](p670fig5.jpg)

Legend: A - search and acquisition, B - relevance judgement, C - information organization, D - citation checking, E - reading, F - communicating, G - independent writing, H - collaborative writing

<figcaption>Figure 5: Age and opinion of the impact of new technologies by number of researchers (y axis)</figcaption>

</figure>

In Figure 5 the researchers' perceptions regarding the eight activities are assessed in relation to age. Two activities are especially affected: organization of information (sig. 0.039) and communication (sig. 0.003). These activities are perceived easier (or much easier) by researchers over forty. This is also supported by the observation that the more years the individuals have spent researching or teaching, the easier they perceive certain activities, e.g. independent writing (sig. 0.001 and 0.028 respectively), collaborative writing (sig. 0.001 and 0.027 respectively). The perception of collaborative writing is influenced by sex (sig. 0.046): Male researchers believe that new technologies have not changed the ways of collaborative writing; however, female researchers now find such collaboration easier.

The use of open-access sources was the focus of the fourth research question. The assumption was that the use of documents is best expressed in the shares of citations used by researchers in their writing. Figure 6 shows that the share of open-access documents is rather low: 50 (40%) researchers responded that less than 20% of the documents they cite are open-access. Only two researchers cite between 80 and 100% open access documents. Weak use of open access documents is most evident in the humanities (sig. 0.015).

<figure class="centre">![FIG. 6\. Age and shares of citations of open-access documents](p670fig6.jpg)

<figcaption>Figure 6: Age and shares of citations of open-access documents by number of researchers (y axis)</figcaption>

</figure>

The fifth research question investigated how Slovenian researchers organize their work. The first feature was organization in terms of available time for various research-related activities, collaboration and co-authorship, publishing or communicating the outcomes of their research. It was investigated how often Slovenian researchers have enough time for the following activities: searching, organization of information, power-browsing, thorough reading, writing and communication. No statistically significant associations were found with any of the demographic variables, but it is apparent (Figure 7) that most researchers (regardless of age) only occasionally have enough time for these activities. The most challenging seem to be thorough reading and organization of information, which are undoubtedly also the most time-consuming. Power-browsing and communication are perceived as least demanding.

<figure class="centre">![FIG. 7\. Relationship between age and available time for research-related information activities](p670fig7.jpg)

Legend: A - Searching, B - Organization of information C - Power-browsing, D - Thorough reading, E - Writing, F - Communication

<figcaption>Figure 7: Relationship between age and available time for research-related information activities by number of researchers (y axis)</figcaption>

</figure>

Collaboration of Slovenian scientists at home and abroad was also investigated. Although not statistically significant, it can be observed (Figure 8) that much collaboration is carried out within researchers' own organizations. Collaboration with researchers abroad occurs sometimes or often. It was surprising to find that almost a fifth of researchers, regardless of age, do not (or hardly) collaborate with any researchers outside Slovenia. Cross tabulations with research discipline show that collaboration abroad is most frequent in natural sciences (sig. 0.049), while researchers in medicine (sig. 0.005), social sciences (sig. 0.010) and humanities (sig. 0.001) most often engage in collaboration within their own institutions. However, many researchers do not collaborate at all. This is especially characteristic of humanities researchers (sig. 0.002).

<figure class="centre">![FIG. 8\. Relationship between age and place of collaboration](p670fig8.jpg)

Legend: A - Same organization, B - Other Slovenian organizations, C - Abroad, D - Do not collaborate

<figcaption>Figure 8: Relationship between age and place of collaboration by number of researchers (y axis)</figcaption>

</figure>

It was also investigated whether Slovenian researchers write and publish on a voluntary basis as co-authors, as the likelihood exists that some more dominant leading researchers impose their personal ambitions. Figure 9 thus indicates that voluntary co-authorship is indeed most common; however, involuntary forms of co-authorship are also evidenced. Writing alone is not uncommon (some 30% of researchers often or always write alone). Younger researchers are less likely to write alone (sig. 0.002). Similarly, those individuals with fewer years of research or teaching also write alone less frequently (sig. 0.005 and 0.001). This is logical, since younger researchers with less experience will seldom commence their career as sole authors.

Scientific discipline was again important, confirmed with statistical significance. In three research disciplines voluntary participation is the most frequent form of collaboration: natural sciences (sig. 0.026), social sciences (sig.0.018) and humanities (sig. 0.000). Natural scientists and researchers in the technical sciences most often do not write alone (sig. 0.016 and 0.009 respectively), while researchers in the humanities often do (sig. 0.000). This supports the previous finding that researchers in the humanities do not collaborate very often.

<figure class="centre">![FIG. 9\. Relationship between age and type of collaboration](p670fig9.jpg)

Legend: A - Voluntary collaboration, B - Involuntary collaboration, C - Alone

<figcaption>Figure 9: Relationship between age and type of collaboration by number of researchers (y axis)</figcaption>

</figure>

In addition, some possible financial arrangements in scientific publishing were explored. Here it should be emphasized that only some established publishing practices were investigated, such as paying a fee to have a document published or, on the other hand, receiving some fee for publication. No statistically significant connections were found with age, only discipline had some influence: in the humanities, the authors very rarely pay to have their work published (sig. 0.025), in medicine, however, they occasionally pay to have their research published (sig. 0.006). Some authors in other disciplines occasionally pay to publish their material, but the results are not statistically significant. Similarly, some authors occasionally receive some fee.

## Discussion

This section discusses the results following the structure of research questions. The researchers received an invitation to participate in the survey in a more official way through the channels of the Slovenian Research Agency. The survey thus covers all scientific disciplines with a similar margin of participation. The response rate was similar to response rates in some other studies which are cited in the methodology section. However, as authors almost invariably report, not all questions are always answered in questionnaires. This was also the case in our survey, so our more detailed analysis focused on those researchers who provided the answers to most questions. The response rate still requires some caution, as the number of randomly selected respondents was relatively small, which is also contingent on the size of the country. The results provide an exploratory insight into characteristics, preferences and behaviour of Slovenian researchers. However, to further verify the results more research is needed, requiring additional methods, for example, qualitative methods including more thoroughly selected groups of researchers in specific research disciplines.

### Types of scholarly information behaviour

Various types of scholarly information behaviour had been to some degree tentatively assessed in the preliminary examinations of Vilar, Juznic and Bartol ([2012a](#vil12a), [2012b](#vil12b)) and Vilar, Bartol, Juznic and Pisanski ([2012](#vilbar12)) which were based on incomplete data and thus not yet conclusive. The present study wished to investigate some further characteristics. This assessment confirms that all types under investigation (cross-checking, skimming, power-browsing, bouncing, berry-picking, squirreling, chaining) are strongly expressed, demonstrating that Slovenian researchers show similar traits to researchers in the studies by other authors (e.g., Bates ([1989](#bat89), [2007](#bat07)), Rowlands and Fieldhouse ([2007](#row07)), Brockman, Neumann, Palmer and Tidline ([2001](#bro01)), Maron and Smith ([2008](#mar08)), Borgman ([2009](#bor09)), Veletsianos and Kimmons ([2012](#vel12)), University of Minnesota Libraries ([2006](#uni06)), Meho and Tibbo ([2003](#meh03)), Palmer, Teffeau and Pirmann ([2009](#pal09))), substantiating the strong international character of Slovenian scientific research. When cross-tabulated with demographic variables (age, sex, current work tasks and research discipline), some more particular differences among researchers can be noticed, which is again in line with some preliminary findings by the authors. The researchers' age has the strongest weight. sex shows specifics only in one type of behaviour (squirreling). This could indicate some specific sociological issues, so further investigation is proposed for future study. Scientific discipline has no effect on the behaviour types. When investigating the use of the materials after downloading, it can be observed that only a minority (9%) always inspect what they save, while over a third (36%) only do it occasionally. Obviously, current information tools permit a very casual use of sources once they have been retrieved and downloaded. Relevance is apparently determined only at some later stage.

Reading of scientific materials as a characteristic activity of researchers was investigated in numerous studies, for example by Tenopir et al. ([2009](#ten09)), Niu et al. ([2010](#niu10)), Niu and Hemminger ([2012](#niu12)). The aim in this study, more particularly, was to investigate conventional reading of articles in relation to power-browsing (reading only the more informative parts such as titles, abstracts, keywords). The study confirms preliminary results where all researchers, regardless of age, tend to thoroughly read only between 20% and 40% of documents. This can be related to a finding by Niu and Hemminger ([2012](#niu12)) where only about a third of articles in researchers' personal archives contained annotations, implying that only those documents had been investigated thoroughly. This is a clear indication of how much power-browsing is in fact present. From this we can draw a conclusion that those parts of documents which enable power-browsing (mainly abstracts and subject headings) are indeed important in information retrieval.

### _Preferred format of tools and sources_

Preference for the format of tools and sources (i.e. print or digital or both) is in line with findings of most studies. Digital format is also favoured by Slovenian scientists. Some differences can be observed in relation to demographic variables. Age plays some role. Digital-only is preferred by younger researchers (aged twenty to forty years) while concurrent access to both formats is more often favoured by researchers aged forty-one to sixty years. This is similar to findings by Niu and Hemminger ([2012](#niu12)). Scientific discipline seems to play a lesser role. This is the case even with the humanities, which is contrary to some older studies reporting on print preferences in this particular research discipline (for example Brockman, Neumann, Palmer and Tidline ([2001](#bro01))). This confirms some recent findings (e.g. by Bulger, Meyer, de la Flor, Terras, Wyatt, Jirotka, Eccles and Madsen ([2011](#bul11))) that nowadays humanities scholars are strong users of digital materials as well. This result can perhaps be related to the very well established Slovenian comprehensive and centralized digital library which aggregates in one place access to databases such as Web of Science and also provides respective immediate links to full-text documents through password-controlled remote access.

A further intent was to explore the preference for digital searching and printing out materials for subsequent reading in paper form, similarly to King, Tenopir, Montgomery and Aerni ([2003](#kin03)), Liu ([2006](#liu06)), Niu et al. ([2010](#niu10)) and Niu and Hemminger ([2012](#niu12)). In contrast to the first two studies, and in accordance with the last two studies, as well as with our preliminary studies, the results indicate that search-digital/print-to-read is generally true for most Slovenian scientists. It seems that reading on paper is still preferred over reading on screen by many individuals.

This finding can be linked to the next finding that all researchers intensely use (i.e. cite) digital materials; this is especially typical for younger researchers who are more likely to cite digital materials. The outcomes are in line with Niu and Hemminger ([2012](#niu12)) who found a connection between age and format preference. Frequent citing of digital materials is another indication of the way these had been retrieved.

### _Impact of information and communication technologies_

New technologies, as observed in many similar studies, as well as in our preliminary experiments, have clearly simplified information activities and are thus perceived as beneficial. However, some practices remain unchanged, and some now appear even harder. Interestingly, reading and relevance judgement are now perceived as harder. This is perhaps related to information overload and the subsequent reduction of attention span, and can also be attributed to occasional poor quality of search tools.

Some connections with age, sex and seniority can also be observed. Older researchers and, similarly, those with longer experience in research, feel especially positive about the impact of new technologies on organization of information and communication. This perhaps indicates that older researchers recollect the earlier, more cumbersome ways of information discovery. This experience is unknown to younger researchers. In relation to sex, women believe that collaborative writing has now been eased while men see no difference. As with squirreling, which is to some extent also linked mainly to women, this observation also merits further exploration.

### _Use of open access_

It seems that open-access documents have not yet become a very important source. The share of such documents in citations remains rather low. We infer that Slovenian researchers apparently prefer to obtain information through more traditional channels. Well organized library consortia in Slovenia make this access easy. Many e-journals are fully available through regular institutional subscription, thus somehow blurring the perceived difference between the true open-access and pay-per-view. Limitations of open access have frequently been discussed, for example by Hyldegaard and Seiden ([2004](#hyl04)), Bjoerk ([2004](#bjo04)) and Budd ([2013](#bud13)). Among the disciplines, the researchers in the humanities use the smallest ratio of open access documents. This could be explained by the fact that in the humanities a significant body of literature has not yet been digitized and is thus not available online.

### _Organization of work, collaboration, publishing_

All researchers regardless of age, sex or discipline assert not to have enough time for research-related activities, the most challenging again being thorough reading and organization of information. As regards collaboration, it is interesting to note that as many as 20% of researchers hardly ever collaborate with their peers abroad. Also, a rather large share of researchers (one third) often work alone. International collaboration is most characteristic in natural sciences, while researchers in medicine, social sciences and humanities most frequently collaborate within their own institution. Researchers in the humanities often do not collaborate at all. The researchers usually participate as co-authors of papers on a voluntary basis; however, some involuntary co-authorship can also be detected, perhaps signifying some coercion by more dominant individuals or superiors in research groups.

Niu and Hemminger ([2012](#niu12)) investigated how paying for publication related to academic rank and found correlation with younger researchers, i.e. doctoral students. No such characteristic was found in our research. The results indicate that Slovenian authors generally publish their articles without any financial agreements. Only in medicine do they occasionally pay to get their materials published. Here it should be emphasized that only some established (formal) publishing practices were investigated. Paying for publication as well as receiving a fee need to be investigated further by analysing specific publications and publishers' policies. However, such details were not a topic of this questionnaire at this stage of the research. The informal ways of dissemination of scientific works and through open-access also remain to be investigated in further studies.

### _Researchers over the age of sixty-one years_

This was the smallest group, so no strong conclusions can be drawn from the answers. Interestingly, all respondents in this group expressed strong enthusiasm for digital technologies. Considering that the survey was based on a Web questionnaire, it is quite probable that these selected few senior researchers are digital enthusiasts in their own right and can hardly be typical representatives of this age group. In spite of their obvious digital preference, the shares of citations of digital documents and open-access documents are rather low. In terms of organization of activities, they differ from most researchers in that they feel they do not have enough time for communication with peers. Less emphasis on communication in this group can perhaps again be attributed to their previous practices involving less technology. However, types of information behaviour are similar to those of other researchers, with one exception: bouncing and chaining are the strongest. On the other hand, they show little preference for skimming (associated mostly with younger researchers). Power-browsing is nevertheless prevalent: they only thoroughly read between 10-30% of literature.

## Conclusions

This was the first comprehensive study of information behaviour of Slovenian researchers. It investigated researchers in all disciplines and looked at various demographic characteristics. Research questions addressed various types of information behaviour: format preferences, impact of new technologies on the use of different types of sources, organization of work, collaboration and publishing. The study provides an insight into the characteristics and preferences of researchers. Similarities with other studies can be found in relation to some specific items. For example, more recently investigated types of behaviour, such as skimming and cross-checking, are most strongly associated with younger researchers, while power-browsing is strongly present in all age groups. In terms of impact of new technologies, we also confirmed that researchers in the field of humanities behave similarly to their counterparts in other countries: they show preference for new technologies and are thus strong users of digital materials.

Some particular traits of Slovenian researchers can be observed. All researchers use open-access sources rather weakly. This may be a consequence of the well-established Slovenian national system of subscription to e-journals through consortia and availability from home through password-controlled remote access. Collaboration practices also show some interesting features: as many as a fifth of researchers hardly ever collaborate with their peers abroad and one third (mostly researchers in the humanities) often work alone. Co-authorship is performed mostly on a voluntary basis although some imposed co-authorship can be detected. Paying for publication is rare.

Although the response rate was not very high, it was similar to various other studies. The statistically random sample comprised all scientific disciplines. It represents the population of all active researchers officially registered in Slovenia and has been established on the central registry of the Slovenian Research Agency which collects data for all researchers nationwide. The study thus validates some general traits of researchers which have been investigated in some other environments by testing the relationships in Slovenian circumstances. Despite some expected outcomes, certain hitherto less known and interesting patterns have been identified as characteristic of Slovenian researchers. As such, this study may serve as a reference for future investigation of the subject.

## Acknowledgements

The study was conducted as a part of the project V5-1016, funded by the Slovenian Research Agency (ARRS).

## About the authors

**Polona Vilar** is Associate Professor in the Department of Library and Information Science and Book Studies, Faculty of Arts, University of Ljubljana. She received her Bachelor's degrees in Librianship and English Language and Literature, and her masters degree in Librarianship from the Faculty of Arts, University of Ljubljana. She can be contacted at: [polona.vilar@ff.uni-lj.si](mailto:polona.vilar@ff.uni-lj.si)

**Primoz Juznic** is Professor in the Department of Library and Information Science and Book Studies, Faculty of Arts, University of Ljubljana. He can be contacted at: [primoz.juznic@ff.uni-lj.si](mailto:primoz.juznic@ff.uni-lj.si)

**Tomaz Bartol** is Associate Professor in the Agronomy Department, Biotechnical Faculty, University of Ljubljana. He can be contacted at: [tomaz.bartol@bf.uni-lj.si](mailto:tomaz.bartol@bf.uni-lj.si)

#### References

*   Bartol, T., Budimir, G., Dekleva Smrekar, D., Pusnik, M. & Juznic, P. (2014). Assessment of research fields in Scopus and Web of Science in the view of national research evaluation in Slovenia. _Scientometrics, 98_(2), 31-44
*   Bates, M.J. (1989). The design of browsing and berrypicking techniques for the online search interface. _Online Review, 13_(5), 407-424
*   Bates, M.J. (2007). [What is browsing - really? A model drawing from behavioural science research.](http://www.webcitation.org/6VhNllUAD) _Information Research, 12_ (4), paper 330\. Retrieved from http://InformationR.net/ir/12-4/paper330.html (Archived by WebCite at http://www.webcitation.org/6VhNllUAD)
*   Bjoerk, B.C. (2004). [Open access to scientific publications - an analysis of the barriers to change.](http://www.webcitation.org/6VhOXJZuY) . _Information Research, 9_ ((2), paper 170\. Retrieved from http://InformationR.net/ir/9-2/paper170.html (Archived by WebCite at http://www.webcitation.org/6VhOXJZuY)
*   Borgman, C. L. (2007). _Scholarship in the digital age: information, infrastructure, and the Internet._ Cambridge, MA: MIT Press.
*   Borgman, C.L. (2009). [_Scholarship in the digital age: blurring the boundaries between the sciences and the humanities_ (Keynote Talk).](http://www.webcitation.org/6VhObzklV) Paper presented at the Digital Humanities Conference, College Park, Maryland. Retrieved from http://works.bepress.com/borgman/216 (Archived by WebCite at http://www.webcitation.org/6VhObzklV)
*   Brockman, W.S., Neumann, L., Palmer, C.L. & Tidline, T.J. (2001). [Scholarly work in the humanities and the evolving information environment.](http://www.webcitation.org/6VhOh6r6L) _Washington, DC: Digital Library Federation._ Retrieved from http://old.diglib.org/pubs/dlf095/dlf095.pdf (Archived by WebCite at http://www.webcitation.org/6VhOh6r6L)
*   Budd, J.M. (2013). [Scholarly communication's problems: an analysis.](http://www.webcitation.org/6WGI5oTSd) _Information Research, 18_(3), paper S01\. Retrieved from http://InformationR.net/ir/18-3/colis/paperS01.html (Archived by WebCite at http://www.webcitation.org/6WGI5oTSd)
*   Bulger, M., Meyer, E.T., de la Flor, G., Terras, M., Wyatt, S., Jirotka, M., Eccles, K. & Madsen, C. (2011). [Reinventing research? Information practices in the humanities.](http://www.webcitation.org/6VhOz80K7) _London: Research Information Network._ Retrieved from http://www.rin.ac.uk/our-work/using-and-accessing-information-resources/information-use-case-studies-humanities (Archived by WebCite at http://www.webcitation.org/6VhOz80K7)
*   Case, D. (2012). _Looking for information: a survey of research on information seeking, needs, and behaviour._ (3rd ed.) Amsterdam, The Netherlands: Academic Press.
*   Dykema, J., Stevenson, J., Klein, L., Kim, Y. & Day, B. (2013). Effects of e-mailed versus mailed invitations and incentives on response rates, data quality, and costs in a Web survey of university faculty. _Social Science Computer Review, 31_(3), 359-370.
*   Ellis, D. (1989). A behavioural approach to information retrieval design. _Journal of Documentation, 59_(3), 318-338.
*   Ellis, D. (1993). Modeling the information seeking patterns of academic researchers: the grounded theory approach. _Library Quarterly, 63_(4), 469-486.
*   Ellis, D., Cox, C. & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation, 49_(4), 356-369.
*   Hemminger, B.M., Lu, D., Vaughan, K. & Adams, S.J. (2007). Information seeking behaviour of academic scientists. _Journal of the American Society for Information Science and Technology, 58_(14), 2205-2225.
*   Higher Education Funding Council for England. _Joint Information Systems Committee_ and The British Library. (2012). [Researchers of tomorrow: the research behaviour of Generation Y doctoral students.](http://www.webcitation.org/6VhP80SPe) London: Higher Education Funding Councils. Retrieved from http://webarchive.nationalarchives.gov.uk/20140702233839/http://www.jisc.ac.uk/media/documents/publications/reports/2012/Researchers-of-Tomorrow.pdf (Archived by WebCite at http://www.webcitation.org/6VhP80SPe)
*   Hyldegaard, J. & Seiden, P. (2004). [My e-journal - exploring the usefulness of personalized access to scholarly articles and services.](http://www.webcitation.org/6VhOu0Whf) _Information Research, 9_(3), paper 181\. Retrieved from http://InformationR.net/ir/9-3/paper181.html (Archived by WebCite at http://www.webcitation.org/6VhOu0Whf)
*   King, D.W., Tenopir, C., Montgomery, C.H. & Aerni, S.E. (2003). [Patterns of journal use by faculty at three diverse universities.](http://www.webcitation.org/6VhPKJne5) _D-Lib Magazine, 9_(10), 1-10\. Retrieved from http://www.dlib.org/dlib/october03/king/10king.html (Archived by WebCite at http://www.webcitation.org/6VhPKJne5)
*   Kwak, N. & Radler, B. (2002). A comparison between mail and Web surveys: response pattern, respondent profile, and data quality. _Journal of Official Statistics, 18_(2), 257-274.
*   Maron, N. & Smith, K.K. (2008). [Current models of digital scholarly communication: results of an investigation conducted by Ithaka for the Association of Research Libraries.](http://www.webcitation.org/6VhPDIyGE) Retrieved from http://www.arl.org/storage/documents/publications/digital-sc-models-report-2008.pdf. (Archived by WebCite at http://www.webcitation.org/6VhPDIyGE)
*   Meho, L.I., & Tibbo, H.R. (2003). Modeling the information- seeking behaviour of social scientists: Ellis' study revisited. _Journal of the American Society for Information Science and Technology, 54_(6), 570-587
*   Moran, M., Seaman, J. & Tinti-Kane, H. (2011). _[Teaching, learning, and sharing: how today's higher education faculty use social media.](http://www.webcitation.org/6ZIlQxXC7))_ Boston, MA: Pearson Learning Solutions. Retrieved from http://files.eric.ed.gov/fulltext/ED535130.pdf. (Archived by WebCite at http://www.webcitation.org/6ZIlQxXC7)
*   Morton, S., Bandara, D. K., Robinson, E. M. & Carr, P. E. A. (2012). In the 21st century, what is an acceptable response rate? _Australian and New Zealand Journal of Public Health, 36_(2), 106-108.
*   Nicholas, D., Huntington, P. & Jamali, H.R. (2007). Diversity in the information seeking behaviour of the virtual scholar: institutional comparisons. _The Journal of Academic Librarianship, 33_(6), 629-638.
*   Niu, X. & Hemminger, B.M. (2012). A study of factors that affect the information-seeking behaviour of academic scientists. _Journal of the American Society for Information Science and Technology, 63_(2), 336-353.
*   Niu, X., Hemminger, B.M., Lown, C., Adams, S., Brown, C., Level, A., McLure, M., Powers, A., Tennant, M.R. & Cataldo, T. (2010). National study of information seeking behaviour of academic researchers in the United States. _Journal of the American Society for Information Science and Technology, 61_(5), 869-890.
*   Nulty, D. D. (2008). The adequacy of response rates to online and paper surveys: what can be done? _Assessment & Evaluation in Higher Education, 33_(3), 301-314.
*   Palmer, C.L. & Cragin, M.H. (2008). Scholarship and disciplinary practices. _Annual Review of Information Science and Technology, 42_(1), 163-212.
*   Palmer, C.L., Teffeau, L.C. & Pirmann, C.M. (2009).[Scholarly information practices in the online environment: themes from the literature and implications for library service development.](http://www.webcitation.org/6VhPSUYsi) Dublin, OH: OCLC. Retrieved from http://www.oclc.org/research/publications/library/2009/2009-02.pdf (Archived by WebCite at http://www.webcitation.org/6VhPSUYsi)
*   Palmer, J. (1991a). Scientists and information: I. Using cluster analysis to identify information style. _Journal of Documentation, 47_(2), 105-129.
*   Palmer, J. (1991b). Scientists and information: II. Personal factors in information behaviour. _Journal of Documentation, 47_(3), 254-275.
*   Rowlands, I. & Fieldhouse, M. (2007). [Information behaviour of the researcher of the future, work package I: trends in scholarly information behaviour.](http://www.webcitation.org/6VhPXDPiH) Retrieved from www.jisc.ac.uk/media/documents/programmes/reppres/ggworkpackagei.pdf (Archived by WebCite at http://www.webcitation.org/6VhPXDPiH)
*   Steinerova, J. (2008). [Seeking relevance in academic information use.](http://www.webcitation.org/6VhPbXjaN) _Information Research, 13_(4), paper 380\. Retrieved from http://InformationR.net/ir/13-4/paper380.html (Archived by WebCite at http://www.webcitation.org/6VhPbXjaN)
*   Talja, S., Savolainen, R. & Maula, H. (2005). [Field differences in the use and perceived usefulness of scholarly mailing lists.](http://www.webcitation.org/6VhPhu4dy) _Information Research, 10_(1), paper 200\. Retrieved from http://InformationR.net/ir/10-1/paper200.html (Archived by WebCite at http://www.webcitation.org/6VhPhu4dy)
*   Tenopir, C., King, D. W., Edwards, S. & Wu, L. (2009). Electronic journals and changes in scholarly article seeking and reading patterns. _Aslib Proceedings, 61_(1), 5-32.
*   University of Minnesota Libraries. (2006). [_A multi-dimensional framework for academic support: final report._ Minneapolis, MN: University of Minnesota Libraries.](http://www.webcitation.org/6VhPn4eku) Retrieved from http://www.research.umn.edu/documents/UMN_Multi-dimensional_Framework_Final_Report.pdf (Archived by WebCite at http://www.webcitation.org/6VhPn4eku)
*   Unsworth, J. (2000). [_Scholarly primitives: what methods do humanities researchers have in common, and how might our tools reflect this?_](http://www.webcitation.org/6VhPpmhEC) Paper presented at the Symposium on Humanities Computing: Formal Methods, Experimental Practice, May 13, 2000, King's College, London. Retrieved from http://people.brandeis.edu/~unsworth/Kings.5-00/primitives.html (Archived by WebCite at http://www.webcitation.org/6VhPpmhEC)
*   Vakkari, P. (1999). Task complexity, problem structure and information actions: integrating studies on information seeking and information retrieval. _Information Processing & Management, 35_(6), 819-837.
*   Vilar, P., Bartol, T., Pisanski, J. & Juznic, P. (2012). [Are librarians familiar with information seeking behaviour of teachers and researchers in their respective institutions?](http://www.webcitation.org/6VhPyjz1f) In _Libraries in the digital age (LIDA): Zadar, Croatia, 18 - 22 June 2012: Proceedings._ Zadar, Croatia: University of Zadar. Retrieved from http://ozk.unizd.hr/proceedings/index.php/lida/article/download/51/25 (Archived by WebCite at http://www.webcitation.org/6VhPyjz1f)
*   Veletsianos, G. & Kimmons, R. (2012). Networked participatory scholarship: emergent techno-cultural pressures toward open and digital scholarship in online networks. _Computers & Education 58,_(2), 766-774.
*   Vilar, P., Juznic, P. & Bartol, T. (2012a). Information-seeking behaviour of Slovenian researchers: implications for information services. _The Grey Journal, 8_(1), 43-53.
*   Vilar, P., Juznic, P. & Bartol, T. (2012b). [Slovenian researchers: what influences their information behaviour?](http://www.webcitation.org/6VhPtK7h6) In Serap Kurbanoglu (Ed.), _E-science and Information Management: Third International Symposium on Information Management in a Changing World (IMCW 2012), Ankara, Turkey, September 19-21, 2012\. Proceedings_ (pp. 47-60). Berlin: Springer. (Communications in Computer and Information Science, 317). Retrieved from http://www.springerlink.com/content/m6473658746757l2/ (Archived by WebCite at http://www.webcitation.org/6VhPtK7h6)
*   Vilar, P. & Zumer, M. (2008). Perceptions and importance of user friendliness of IR systems according to users' individual characteristics and academic discipline. _Journal of the American Society for Information Science and Technology, 59_(12), 1995-2007.
*   Wang, P. (2006). _[Information behaviours of academic researchers in the Internet era: an interdisciplinary and cross-cultural study.](http://www.webcitation.org/6ZIlbcnd1)_ Paper presented at the 1st International Scientific Conference eRA: the Information Technology to Science, Economy, Society & Education, (Cultural Center of Tripoli, Greece, 16-17 September 2006). Retrieved from http://arizona.openrepository.com/arizona/bitstream/10150/105492/1/eRA_PeilingWang.pdf (Archived by WebCite at http://www.webcitation.org/6ZIlbcnd1)
*   Wilson, T. D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing & Management, 33_(4), 551-572.