#### vol. 20 no. 2, June, 2015

# Determinants of continued use of social media: the perspectives of uses and gratifications theory and perceived interactivity

#### [Meng-Hsiang Hsu,](#author) Department of Information Management, National Kaohsiung First University of Science and Technology, Taiwan  
[Chun-Ming Chang,](#author) Department of Tourism Information, Aletheia University, Taiwan  
[Hsien-Cheng Lin,](#author) Director, Department of Planning, Fooyin University Hospital, Taiwan  
[Yi-Wan Lin,](#author) Department of Information Management, National Kaohsiung First University of Science and Technology, Taiwan

#### Abstract

> **Introduction**. By integrating the perspectives of uses and gratifications theory and perceived interactivity, this study proposes a theoretical model to test what may affect users’ decision to use social media continuously. Our model argues that motivations (entertainment, socialisation, information seeking and self-presentation) will impact perceived interactivity (human-human interaction, human-message interaction and human-community interaction), which in turn affects satisfaction and continued use.  
> **Method**. Data collected from 296 Facebook users in Taiwan were used to test our proposed model. The partial least squares method was employed to assess the relationships in the model.  
> **Results**. The results show that entertainment, socialisation and self-presentation impact human-human interaction significantly, while entertainment and information-seeking have positive influences on human-message interaction. Entertainment, socialisation, information seeking and self-presentation influence human-community interaction positively. Moreover, the three types of perceived interactivity exert positive effects on satisfaction, while satisfaction, human-human interaction and human-community interaction are the determinants of continued use of social media.  
> **Conclusion**. This is one of the first studies to examine the link between motivations and perceived interactivity. This study is also the first to propose a new construct, human-community interaction, to test its effect its effect on satisfaction and continued use.

## Introduction

The rapid innovation of information communication technologies have helped social media, such as Facebook, Twitter and MySpace, to become important social platforms that attracts numerous users around the globe to carry out computer-mediated communications ([Kim, Sohn and Choi, 2011](#kim11); [Lin and Lu, 2011](#lin11)). According to a recent survey conducted by e-Marketer in 2013, more than 22.8% of people in the globe have used social media ([iThome, 2013](#ith13)). This survey also reports that the penetration rate of social media in some counties (e.g., the Netherlands, Denmark, Norway, Sweden and United States) is greater than 50%, indicating that the popularity of social media has enabled people to use social media to build and maintain their personal relationships.

The popularity of social media has motivated many scholars to investigate why people use social media ([Kim _et al._, 2011](#kim11)). Generally speaking, research for examining motives of social media usage can be classified into two perspectives. Based on the uses and gratifications theory, the first perspective asserts that psychological needs and gratifications are the determinants of social media usage ([Lee and Ma 2012](#lee12); [Lu, Lin, Hsiao and Cheng, 2010](#lu10)). Prior studies from this perspective have found that entertainment, information seeking, socialisation and self-presentation are the key factors affecting social media usage ([Dunne, Lawlor and Rowley, 2010](#dun10); [Li, 2011](#li11); [Park, Kee and Valenzuela, 2009](#par09)). On the other hand, the second perspective posits that social interactions affect social media usage ([Li, 2011](#li11)); [Pempek, Yermolayeva and Calvert, 2009](#pem09)). Research from this perspective has found that perceived interactivity will impact users’ satisfaction, which in turn affects social media usage ([Lu _et al._, 2010](#lu10); [Zhao and Lu, 2012](#zha12)).

Although many studies have examined the motives for using social media, most of them focus on the factors determining information systems adoption, which may limit the understanding about the antecedents of users’ post-adoption behaviour. Since continued usage is considered as a power indicator for measuring the success of an information system, including social media ([Bhattacherjee, 2001](#bha01)), the focus of this study is users’ post-adoption behaviour. In addition, while these two streams of research have provided useful insights about the determinants of social media use, few studies have been done to integrate these two distinct perspectives in order to examine the antecedents of continued use of social media. Uses and gratifications theory suggests a variety of needs will enable users to use an information system to fulfill their needs and that their own use experience will impact satisfaction and choice for continual use of an information system. The authors of this study believe that integrating uses and gratifications theory and perceived interactivity to examine the antecedents of continued usage of social media may provide insight into what can motivate users to continue social media use after their initial acceptance.

The purpose of this study is to answer the following questions: (1) Do motivations (entertainment, socialisation, information-seeking and self-presentation) impact perceived interactivity (human-human interaction, human-message interaction and human-community interaction)? (2) Does perceived interactivity influence satisfaction and continued use of social media? This study is organized as follows. First, we review the existing literature of uses and gratifications theory and perceived interactivity to propose our conceptual model, research and hypotheses. Then research methodology and data analysis are described. Finally, we discuss the implications of research findings for theory and practice.

## Theoretical background

### Uses and gratifications theory

Uses and gratifications theory asserts that users of media are active and goal-oriented (Sangwang, 2005) therefore they will be motivated to select a medium that best gratifies their needs ([Luo, Chea and Chen, 2011](#luo11); [Sangwang, 2005](#san05)). That is, the primary function of uses and gratifications theory is to explain how psychological needs can shape peoples’ reasons to use a medium to fulfill their needs ([Roy, 2009](#roy09); [Rubin, 1994](#rub94)). Gratifications of these needs, on the other hand, lead to continuance of media use ([Sangwang, 2005](#san05)). Thus, based on the uses and gratifications theory, satisfaction is treated as a vital antecedent of continued use of social media.

Uses and gratifications theory has been widely used to examine the users’ motivations for using certain media, including social media ([Luo _et al._, 2011](#luo11); [Nguyen and Western, 2006](#ngu06); [Thelwall, Byrne and Goody, 2007](#the07); [Wang, Woo, Quek, Yang and Lin, 2012](#wan12)). For example, Shao ([2009](#sha09)) reports that users use social media to satisfy their information, entertainment and mood management needs. Chua, Goh and Lee ([2012](#chu12)) found that the motivations that bring people use social media are leisure/entertainment, easy access, information resources/services and obtaining high quality information. Other scholars have found that the motivations for using social media are entertainment, information seeking, socialisation, passing time, escapism and professional advancement ([Chen, Yang and Tang, 2013](#che13); [Papacharissi and Mendelson, 2011](#pap11)). Additionally, researchers have found that a purpose to which users use social media is present themselves online, to develop and maintain their social networks and relationships ([Boyle and Johnson, 2010](#boy10); [Ellison, Steinfeld and Lampe, 2007](#ell07)). Similarly, Lenhart ([2009](#len09)) posits that approximately 90% of social media participants create a profile to get in touch with friends or meet new friends. By synthesizing the above standpoints, we recognize that the motivations for using social media can be divided into four types, namely entertainment, socialisation, information seeking and self-presentation.

Entertainment refers to the pleasure or fun derived from social media use ([Lee and Ma, 2012](#lee12)), while socialisation is the need for an individual to connect with others for building and maintaining relationships ([Andrew, Nicole, Cliff and Donghee, 2011](#and11)). Information seeking is the desire to obtain information through social media ([Lee and Ma, 2012](#lee12)), while self-presentation is the need to disclose personal information to enhance identity ([Schlenker and Wowra, 2003](#sch03)).

Social media have been considered as useful platforms that allow people to interact with others to strengthen relationships ([Ko, Cho and Roberts, 2005](#ko05)). From the viewpoint of uses and gratifications theory, motivations may enable users to interact with others through social media. In this sense, we may propose that motivations will impact upon users’ perceptions about interactivities when using social media. However, few studies on uses and gratifications theory have been done to test the influences of motivations on perceived interactivity, which in turn impact satisfaction and continued use. Therefore, this study aims to examine the effects of motivations on perceived interactivity, satisfaction and continued use.

### Perceived interactivity and its classification

Perceived interactivity is a difficult construct to define because it has different meanings in different contexts ([Lowry, Romano Jr., Jenkins and Guthrie, 2009](#low09)). In general, perceived interactivity can be defined from three different perspectives: as a feature of technology, as a process of message exchange and as a user’s perception after using technology or going through a process ([Lowry _et al._, 2009](#low09); [Zhao and Lu, 2012](#zha12)). In this study, we use the perspective of users’ perception and define it as the extent to which users perceive their experiences as a simulation of interpersonal interaction and sense they are in the presence of a social other ( [Zhao and Lu, 2012f](#zha12)).

Many researchers agree that interactivity is a complex and multidimensional concept ( [Burgoon, Bonito, Bengtsson, Ramirez, Dunbar and Miczo, 2000](#bur00); [Zhao and Lu, 2012](#zha12)). Generally speaking, interactivity can be divided into two types, human-human interaction and human-message interaction ([Ko _et al._, 2005](#ko05)). Human-human interaction refers to the extent to which users believe that they can communicate with others via the functions of social media (Lu _et al._, 2010). That is, human-human interaction denotes the reciprocal communication from senders to receivers and vice versa via social media ([Ko _et al._, 2005](#ko05)). Human-message interaction, on the other hand, refers to participants’ interactions with messages (browsing and sharing messages) through the functions of social media ([Lu _et al._, 2010](#lu10)). In general, a higher level of human-message interaction can improve the effectiveness of information searching and feedback. This kind of interactivity has been considered as an important dimension of the social interaction ([Bettman, Luce and Payne, 1998](#bet98); [Lu _et al._, 2010](#lu10)).

Using social media could enable users to invite others with common interests to build their own communities. That is, users may use social media to conduct social interactions with community, since they tend to connect themselves to the community and obtain a sense of belonging ([Hsu, Chang and Yen, 2011](#hsu11); [Li, Browne and Wetherbe, 2006](#li06)). Thus, in this study, we consider the human-community interaction as an additional type of perceived interactivity in the context of social media and define it as one’s feelings of being close to or connected to a community via the functions of social media ([Zhao and Lu, 2012](#zha12)).

## Research model and hypotheses

Based on the theoretical background presented, the research model is shown in Figure 1\. The rationale of the model is explained below.

<figure class="centre">![Figure 1: Research model](p671fig1.jpg)

<figcaption>Figure 1: Research model</figcaption>

</figure>

### Motivations and perceived interactivity

As mentioned earlier, a key value social media can provide to users is allowing them to escape from reality and obtain enjoyment ([Lee and Ma, 2012](#lee12)). Papacharissi and Mendelson ([2011](#pap11)) also posit that entertainment is an important factor driving users’ decision for the use of social media. Previous research suggests that the key motivations for using social media are social interactions and peer identity ([Luo _et al._, 2011](#luo11)). In addition, recent studies suggest that entertainment needs will motivate users of social media to share information with others within online communities ([Lee and Ma, 2012](#lee12); [Nov, Naaman and Ye, 2010](#nov10)) and to read news in the Internet ([Diddi and LaRose, 2006](#did06); [Lee and Ma, 2012](#lee12)). Taken together, we may propose that entertainment will exert positive influences on human-human interaction, human-message interaction and human-community interaction.

> H1a: Entertainment is positively related to human-human interaction.  
> H1b: Entertainment is positively related to human-message interaction.  
> H1c: Entertainment is positively related to human-community interaction.

Previous research also suggests that users typically use social media to communicate with friends ([Lee and Ma, 2012](#lee12); [Papacharissi and Rubin, 2000](#pap00)) to fulfill their socialisation needs ([Park _et al._, 2009](#par09)). Some researchers have also found that social media could facilitate interpersonal communication ([Cemalcilar, Falbo and Stapleton, 2005](#cem05); [Ellison _et al._, 2007](#ell07); [Viswanath, Mislove, Cha and Gummadi, 2009](#vis09)) and achieve sense of belonging ([Lee and Ma, 2012](#lee12); [Rubin, 1994](#rub94)). By synthesizing their arguments, we may recognize that social media can be considered as interactive mechanisms by which users could exchange information with others to strengthen social ties with others in their communities when they want to fulfill their socialisation needs. This leads to the following hypotheses.

> H2a: Socialisation is positively related to human-human interaction.  
> H2b Socialization is positively related to human-human interaction.

Generally speaking, a major reason for people to use social media is to access and share information. According to Wang _et al._ (2012), users tend to use social media to acquire and share information with others. Similarly, Park _et al._ (2009) suggest that knowledge acquisition and sharing is one of the needs met by students using social media. Lee and Ma (2012), on the other hand, find that information seeking is a significant factor affecting users’ intention to share information. Thus, we may recognize that users are more likely to search and share information by using the social media, if they have information seeking needs. This leads to the following hypothesis.

> H3a: Information seeking is positively related to human-message interaction.

Previous researchers have argued that regular communication will facilitate the development of social relationships among people ([Panteli and Sockalingam, 2005](#pan05)). In the online context, users can use social media to provide others with timely and relevant information to satisfy their information needs ([Lee and Ma, 2012](#lee12)). By seeking information posted by users, shared values and understanding can be enhanced. Thus, users’ sense of belonging to the group can be increased as well ([Panteli and Sockalingam, 2005](#pan05)). In this sense, we propose that information seeking is a vital antecedent that can give rise to human-community interaction.

> H3b: Information seeking is positively related to human-community interaction.

In general, social media provide attractive places for users to present themselves to connect to others ([Cheung and Lee, 2010](#che10); [Lin and Lu, 2011](#lin11)). Similarly, other researchers have found that self-presentation is an important factor that will facilitate interpersonal communications ([Laurenceau, Barrett and Pietromonaco, 1998](#lau98)). Ma and Agarwal ([2007](#ma07)) find that self-presentation will increase perceived identity verification, which in turn affects knowledge contribution. Hence, in this study, we may expect that self-presentation will facilitate information exchange among users. This leads to the following hypothesis.

> H4a: Self-presentation is positively related to human-human interaction.

In addition, previous studies (e.g., [Hew and Hara, 2007](#hew07); [Kaiser and Müller-Seitz, 2008](#kai08)) postulate that people generally present themselves to attain status among peers and to be recognized by members in a social system. Researchers also argue that social media may enable users to present themselves to connect to their personal network and thus strengthen the structure of their social systems ([Ellison _et al._, 2007](#ell07); [Kane, Fichman, Gallaugher and Glaser, 2009](#kan09); [Lin and Lu, 2011](#lin11)). Therefore, we may recognize that self-presentation will impact the development of human-community interaction. This leads to the following hypothesis:

> H4b: Self-presentation is positively related to human-community interaction.

### Perceived interactivity and satisfaction

Satisfaction refers to an individual’s evaluation of and his/her psychological state resulting from his/her overall experience with a product or service ([Kim 2008](#kim08); [Oliver 1980](#oli80); [Zhao and Lu, 2012](#zha12)). From the standpoint of uses and gratifications theory, users are motivated to use social media to meet their needs, resulting in a changed level of gratification and satisfaction ([Lu _et al._, 2010](#lu10)). In general, gratification derived from use of the internet can be divided into three types: process gratification, content gratification and social gratification ([Lu _et al._; 2010](#lu10); [Stafford, Stafford and Schkade, 2004](#sta04)). When a user uses social media to interact with others in the social system, human-human interaction and human-community interaction will increase process gratification and social gratification, since website design will determine users’ attitude (i.e., satisfaction) toward website use. The fulfillment of socialization needs will influence users’ pleasure and flow experience. At the same time, human-message interaction will impact content gratification, similar to the assertion of Kim, Xu and Koh ([2004](#kim04)) that content quality will impact on users’ satisfaction. Previous research has also provided empirical evidence to the link between perceived interactivity and satisfaction (e.g., [Lu _et al._, 2010](#lu10); [Zhao and Lu, 2012](#zha12)). Thus,

> H5: Human-human interaction is positively related to satisfaction.  
> H6: Human-message interaction is positively related to satisfaction.  
> H7: Human-community interaction is positively related to satisfaction.

### Satisfaction and continued use of social media

Satisfaction has been considered an important factor determining continued use of information systems ([Lin and Wang, 2006](#lin06)). If a user revalues his or her use experience of an information system positively, it is likely that his or her willingness to use such information system again will increase. Researchers find that people tend to use information systems more frequently when they have high level of satisfaction (e.g., [Wang _et al._, 2012](#wan12); [Kwon, 2005](#kwo05)). Bhattacherjee ([2001](#bha01)) and Zhao and Lu ([2012](#zha12)) also provide empirical support for the link between satisfaction and continued use of information system. Therefore,

> H8: Satisfaction is positively related to continued use of social media.

### Human-human interaction, human-community interaction and continued use of social media

Social media provide places for users to carry out human-human interactions in the virtual setting. Past literature has found that the interpersonal interactions can facilitate users’ loyalty to social media ([Shen, Cheung, Lee and Chen, 2011](#she11)). From the viewpoint of motivation theory ([Deci, 1975](#dec75)), when users’ needs for interpersonal communications can be fulfilled, their behaviour can be motivated. Therefore, we may argue that human-human interaction will have a significant influence on continued use of social media.

> H9: Human-human interaction is positively related to continued use of social media.

On the other hand, Tamborini and Skalski ([2006](#tam06)) describe that a stable environment of social media will enable users to undertake activities in social media. Likewise, Marlow ([2006](#mar06)) suggests that the group harmony of social media will directly influence their use intention. In addition, previous research also finds that a strong sense of belonging to a group is a key motivator for the use of social media (e.g., [Cheung and Lee, 2010](#che10)). By synthesizing their statements, we may expect that human-community interaction will exert positive influence on continued use of social media.

> H10: Human-community interaction is positively related to continued use of social media.

## Research methods

### Data collection

We collected data from the users of Facebook in Taiwan to examine the proposed research model of this study. Facebook was selected because it is a well-known social medium in use around the globe, including Taiwan. Other researchers have also used the users of Facebook as their respondents (e.g., [Lin and Lu, 2011](#lin11)). In order to target the users of Facebook, a Web-based survey questionnaire was employed. Following Lin and Lu ([2011](#lin11)) and Hsu _et al._ ([2011](#hsu11)), this study posted the messages about questionnaires on the websites of Facebook in March 2013\. Active users of Facebook were invited to participate in this survey. In order to encourage participation, one hundred randomly selected participants were offered an incentive in the form of a gift certificate at a convenient store amounting to USD$3.30, following the recommendation of Hsu _et al._. When the questionnaires were received, respondents’ e-mail addresses were checked to avoid replications ([Lin and Lu, 2011](#lin11)). By the time this survey was concluded, 306 questionnaires were collected. The exclusion of ten invalid questionnaires (incomplete or e-mail with similar address) resulted in a total of 296 complete and valid questionnaires for data analysis. Table 1 lists the demographic characteristics of the respondents.

<table class="center"><caption>  
Table 1: Demographics of the research sample (n=296)</caption>

<tbody>

<tr>

<th>Measure</th>

<th>Category</th>

<th>Number</th>

<th>Percentage</th>

</tr>

<tr>

<td rowspan="2">Sex</td>

<td style="text-align:center;">Male</td>

<td style="text-align:center;">170</td>

<td style="text-align:center;">57.4</td>

</tr>

<tr>

<td style="text-align:center;">Female</td>

<td style="text-align:center;">126</td>

<td style="text-align:center;">42.6</td>

</tr>

<tr>

<td rowspan="5">Age</td>

<td style="text-align:center;">10-19</td>

<td style="text-align:center;">45</td>

<td style="text-align:center;">15.2</td>

</tr>

<tr>

<td style="text-align:center;">20-29</td>

<td style="text-align:center;">194</td>

<td style="text-align:center;">65.5</td>

</tr>

<tr>

<td style="text-align:center;">30-39</td>

<td style="text-align:center;">62</td>

<td style="text-align:center;">10.8</td>

</tr>

<tr>

<td style="text-align:center;">40-49</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">5.1</td>

</tr>

<tr>

<td style="text-align:center;">50-59</td>

<td style="text-align:center;">10</td>

<td style="text-align:center;">3.4</td>

</tr>

<tr>

<td rowspan="6">Hours of Facebook use per week</td>

<td style="text-align:center;">More than 15</td>

<td style="text-align:center;">28</td>

<td style="text-align:center;">9.5</td>

</tr>

<tr>

<td style="text-align:center;">10-15</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">1.7</td>

</tr>

<tr>

<td style="text-align:center;">8-9</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">2.7</td>

</tr>

<tr>

<td style="text-align:center;">5-7</td>

<td style="text-align:center;">42</td>

<td style="text-align:center;">14.2</td>

</tr>

<tr>

<td style="text-align:center;">2-4</td>

<td style="text-align:center;">138</td>

<td style="text-align:center;">46.6</td>

</tr>

<tr>

<td style="text-align:center;">Less than 1</td>

<td style="text-align:center;">75</td>

<td style="text-align:center;">25.3</td>

</tr>

</tbody>

</table>

Generally, the demographics of respondents are in line with the recent surveys that found more people on social media were males than females in Taiwan ([ACI-FIND, 2014](#aci14)), but both samples were nearly equal in number ([Lin and Lu, 2011](#lin11)). The demographics of respondents are also similar to the survey conducted by Pew Research Center which found that the years of age for the greatest percentage of Facebook users ranged from 18 to 29 ([TechHive, 2013](#tec13)). Accordingly, we recognize that the sample in the study represents overall Facebook users in Taiwan. In addition, Table 1 also shows 15.2% of participants are young people between the ages of 10 to 19\. In line with Limayem, Hirt and Cheung ([2007](#lim07)), this indicates that young people (e.g., students) generally use social media (e.g., Facebook) to keep in touch with their friends. Several researchers report that no systematical difference occurred between young users (i.e., student subjects) and adult users (e.g., organizational users) in the previous studies ([Bhattacherjee and Premkumar, 2004](#bha04); [Hsu, Yen, Chiu and Chang, 2006](#hsu06)). Thus, we believe that young users can be the representative of Facebook users in this study.

### Measurement development

The measurement items of the constructs were developed based on the existing literature wherever possible. Items for measuring entertainment and socialization were adapted from Andrew _et al._ ([2011](#and11)) and Wu, Wang and Tsai ([2010](#wu10)), while scales used to measure information seeking were adapted from Andrew _et al._ ([2011](#and11)) and Lee and Ma ([2012](#lee12)). Social presentation measures were adapted from Andrew _et al._ ([2011](#and11)) and Pempek _et al._ ([2009](#pem09)), whereas items for measuring human-human interaction were adapted from Lu _et al._ ([2010](#lu10)). Items used to measure human-message interaction were adapted from Lu _et al._ and Taylor, Cool, Belkin and Amadio ([2007](#tay07)), while human-community interaction were measured using items developed based on Yang and Lai ([2011](#yan11)). Items for measuring satisfaction were adapted from Ko _et al._ ([2005](#ko05)) and Lu _et al._, whereas continued use was measured using items adapted from Wu _et al._ ([2010](#wu10)). The survey questionnaire was measured using a 5-point Likert scale with anchors ranging from strongly agree (5) to strongly disagree (1). The items are presented in Table 1 in the [Appendix](#app).

### Data analysis

In this study, we employed the partial least squares regresson to carry out data analysis. The method was chosen because it places minimum restrictions on sample size, measurement scales and residual distributions ([Chin, Marcolin and Newsted, 2003](#chi03)). This study used a two-step approach recommended by Anderson and Gerbing ([1988](#and88)). The first step was to examine the measurement model and then we investigated the structural relationships among the latent constructs. In this study, we used SmartPLS 2.0 M3 ([Ringle, Wende and Will, 2005](#rin05)) to test our research model.

#### Measurement model test

To test the measurement model, confirmatory factor analysis was conducted. We assessed the reliability, convergent validity and discriminant validity of the constructs, following Kang, Hong and Lee ([2009](#kang09)).

<table class="center" style="width:100%;"><caption>  
Table 2: Construct reliability, convergent validity and discriminate validity</caption>

<tbody>

<tr>

<th rowspan="2">Construct</th>

<th rowspan="2">CR</th>

<th rowspan="2">AVE</th>

<th colspan="9">Correlation</th>

</tr>

<tr>

<th>EN</th>

<th>SO</th>

<th>SI</th>

<th>SP</th>

<th>HH</th>

<th>HM</th>

<th>HC</th>

<th>SA</th>

<th>CU</th>

</tr>

<tr>

<td>Entertainment (EN)</td>

<td style="text-align:center;">0.88</td>

<td style="text-align:center;">0.65</td>

<td style="text-align:center;">**0.80**</td>

<td colspan="8" style="text-align:center;"> </td>

</tr>

<tr>

<td>Socialisation (SO)</td>

<td style="text-align:center;">0.85</td>

<td style="text-align:center;">0.59</td>

<td style="text-align:center;">0.53</td>

<td style="text-align:center;">**0.77**</td>

<td colspan="7" style="text-align:center;"> </td>

</tr>

<tr>

<td>Information seeking (SI)</td>

<td style="text-align:center;">0.88</td>

<td style="text-align:center;">0.60</td>

<td style="text-align:center;">0.55</td>

<td style="text-align:center;">0.56</td>

<td style="text-align:center;">**0.77**</td>

<td colspan="6" style="text-align:center;"> </td>

</tr>

<tr>

<td>Self-presentation (SP)</td>

<td style="text-align:center;">0.91</td>

<td style="text-align:center;">0.67</td>

<td style="text-align:center;">0.62</td>

<td style="text-align:center;">0.55</td>

<td style="text-align:center;">0.53</td>

<td style="text-align:center;">**0.82**</td>

<td colspan="5" style="text-align:center;"> </td>

</tr>

<tr>

<td>Human-human interaction (HH)</td>

<td style="text-align:center;">0.90</td>

<td style="text-align:center;">0.65</td>

<td style="text-align:center;">0.63</td>

<td style="text-align:center;">0.65</td>

<td style="text-align:center;">0.55</td>

<td style="text-align:center;">0.60</td>

<td style="text-align:center;">**0.81**</td>

<td colspan="4" style="text-align:center;"> </td>

</tr>

<tr>

<td>Human-message interaction (HM)</td>

<td style="text-align:center;">0.91</td>

<td style="text-align:center;">0.67</td>

<td style="text-align:center;">0.48</td>

<td style="text-align:center;">0.48</td>

<td style="text-align:center;">0.68</td>

<td style="text-align:center;">0.53</td>

<td style="text-align:center;">0.39</td>

<td style="text-align:center;">**0.82**</td>

<td colspan="3" style="text-align:center;"> </td>

</tr>

<tr>

<td>Human-community interaction (HC)</td>

<td style="text-align:center;">0.90</td>

<td style="text-align:center;">0.61</td>

<td style="text-align:center;">0.63</td>

<td style="text-align:center;">0.56</td>

<td style="text-align:center;">0.66</td>

<td style="text-align:center;">0.64</td>

<td style="text-align:center;">0.59</td>

<td style="text-align:center;">0.60</td>

<td style="text-align:center;">**0.78**</td>

<td colspan="2" style="text-align:center;"> </td>

</tr>

<tr>

<td>Satisfaction (SA)</td>

<td style="text-align:center;">0.87</td>

<td style="text-align:center;">0.69</td>

<td style="text-align:center;">0.57</td>

<td style="text-align:center;">0.48</td>

<td style="text-align:center;">0.61</td>

<td style="text-align:center;">0.51</td>

<td style="text-align:center;">0.55</td>

<td style="text-align:center;">0.61</td>

<td style="text-align:center;">0.56</td>

<td style="text-align:center;">**0.83**</td>

<td style="text-align:center;"> </td>

</tr>

<tr>

<td>Continuous use (CU)</td>

<td style="text-align:center;">0.85</td>

<td style="text-align:center;">0.66</td>

<td style="text-align:center;">0.60</td>

<td style="text-align:center;">0.49</td>

<td style="text-align:center;">0.48</td>

<td style="text-align:center;">0.51</td>

<td style="text-align:center;">0.54</td>

<td style="text-align:center;">0.39</td>

<td style="text-align:center;">0.59</td>

<td style="text-align:center;">0.52</td>

<td style="text-align:center;">**0.81**</td>

</tr>

<tr>

<td colspan="12">Diagonal elements in the correlation of constructs matrix are the square root of the average variance extracted. For adequate discriminant validity, diagonal elements should be greater than corresponding off-diagonal elements.</td>

</tr>

</tbody>

</table>

As shown in Table 2, all the values of composite reliability (CR) are above the threshold of 0.7 and the values of average variance extracted (AVE) are greater than 0.5 as well, indicating the adequate reliability ([Fornell and Larcker, 1981](#for81)). In addition, the results show that loadings of indicators are much higher on their hypothesised factor than on other factor (see [Appendix](#app) Table 2) and the square root of average variance extracted are larger than all other cross correlations, indicating the acceptable convergent validity and discriminant validity.

#### Structure model test

The hypothesized relationships were estimated using 500 interactions of bootstrapping technique in SmartPLS. Figure 2 shows the results of analysis.

<figure class="centre">![](p671fig2.png)

<figcaption>Figure 2: Results of analysis</figcaption>

</figure>

The results demonstrate that all of the hypotheses described earlier are supported. As expected, entertainment has a positive influence on human-human interaction, human-message interaction and human-community interaction (ß=0.295; 0.153; 0.258, t-value=5.055; 2.747; 3.936, respectively). Thus, H1a, H1b and H1c are supported. Socialization has significant impacts on human-human interaction and human-community interaction (ß=0.378; 0.138, t-value=5.821; 2.738, respectively), thus, H2a and H2b are supported. Information seeking has positive influences on human-message and human-community interaction (ß= 0.600; 0.311, t-value= 13.119; 5.495, respectively), so H3a and H3b are supported. Self-presentation also has significant positive impacts on human-human interaction and human-community interaction (ß=0.209; 0.207, t-value= 2.936; 3.342, respectively). Therefore, H4a and H4b are supported.

The results also show that human-human interaction, human-message interaction and human-community interaction have significant influence on satisfaction (ß= 0.247; 0.342; 0.218, t-value= 3.963; 5.229; 4.139, respectively). Thus, H5, H6 and H7 were supported. In addition, satisfaction, human-human interaction and human-community interaction exert significant influences on continued use of social media (ß= 0.218; 0.270; 0.263, t-value= 3.420; 3.696; 4.113, respectively). Therefore, H8, H9 and H10 are supported. The proportions of variances explained were 56% for human-human interaction, 49% for human-message interaction, 58% for human-community interaction, 47% for satisfaction and 40% for continued use of social media.

## Discussion and conclusions

### Major findings

In order to understand the determinants of continued use of social media, this study proposes a theoretical model by integrating uses and gratifications theory and perceived interactivity. Our findings show that entertainment exerts positive influence on human-human, human-message interaction and human-community interaction. Our findings are consistent with the assertions of previous studies that entertainment and social interactions are important and complementary and can influence users’ decision to use social media ([Ko _et al._, 2005](#ko05); [Lee and Ma, 2012](#lee12); [Luo _et al._, 2011](#luo11)). Similar to previous literature ([Dunne _et al._, 2010](#dun10); [Park _et al._, 2009](#par09)), our results also indicate that the socialization will have positive impact on human-human interaction and human-community interaction. This finding reveals that people will use social media to communicate with friends or to interact with their peers when they tend to extend and maintain their interpersonal relationships. On the other hand, users’ motivation of information seeking in positively associated with human-message interaction and human-community interaction. The findings indicate that users will use social media more frequently to find or browse information and to seek the sense of belonging to the online community if they have high levels of information seeking needs. In addition, our results reveal that self-presentation has significant influence on human-human interaction and human-environment interaction. The findings are similar to previous research which found that people will attempt to maintain their self-relevant images to increase social identity through social interactions ([Laurenceau _et al._, 1998](#lau98); [Schlenker and Wowra, 2003](#sch03)).

In the study, we also found that human-human interaction, human-message interaction and human-community interaction exert strong influences on satisfaction, which in turn impacts continued use of social media. These findings provide additional evidence to support the standpoint ofprevious research (e.g., [Lu _et al._, 2010](#lu10); [Zhao and Lu, 2012](#zha12)) that satisfaction is an important antecedent of continued use of an information system and that perceived interactivities will influence the formation of satisfaction. Finally, our results also indicate that human-human interaction and human-community interaction have positive influences on continued use of social media. The findings provide empirical evidence to support the link between perceived interactivity and users’ post-adoptive behaviour.

### Implications

Our findings suggest several important implications for theory. First, although past studies have used uses and gratifications theory to identify the motivations of social media use ([Lee and Ma, 2012](#lee12); [Luo _et al._, 2011](#luo11)) and employed the aspect of perceived interactivity to examine the factors affecting social media use ([Lu _et al._, 2010](#lu10); [Zhao and Lu, 2012](#zha12)). Few studies have been conducted to integrate perceived interactivity to examine the determinants of continued use of social media. To the best of our knowledge, this study is one of the earliest studies to examine the link between motivations and perceived interactivity and is a contribution of this study to the literature. Second, while prior literature has tested the influences of human-human interaction and human-message interaction on an individual behaviour (e.g., [Ko _et al._, 2005](#ko05)), it has been incomplete in that it has only identified two possible types of interaction. In this study, we propose a new construct, human-community interaction. Our study demonstrates the effect of human-community interaction on satisfaction and continued use of social media and its complementary relationship to the two previously identified types of interaction in the literature.

From a practical perspective, our results also provide implications for social media service providers who want to improve their service quality and promote their competitive advantage. Theresults show that motivations will facilitate users’ interactions in social media, which in turn impacts satisfaction and continued use of social media. We suggest that managers of social media should deploy some strategic elements to satisfy the users’ needs such as entertainment, socialization, information seeking and self-presentation to facilitate social interactions via social media. In this way users’ intention to use social media continuously can be increased. First, management of social media are advised to provide some interesting games on social media with which users can invite others to use the games together to increase their perception of enjoyment. In addition, in order to satisfy users’ information-seeking needs, managers of social media can provide a mechanism to highlight users who contribute information actively or award users based on the amount the amount of information sharing ([Lee and Ma, 2012](#lee12)). In order to fulfill users’ needs for self-presentation, management of social media could develop new features to allow users to upload video clips to introduce themselves or generate user profiles automatically by using their past activities ([Ma and Agarwal, 2007](#ma07)). Finally, management of social media should improve some tools or functions (e.g., editing, organizing and uploading) to increase the quality of user interface for direct and indirect interactions among users to meet the needs of socialization ([Lu _et al._, 2010](#lu10)).

### Limitations

Despite the contribution of this study, there are some limitations. First, the data of this research were mainly collected from Facebook users in Taiwan. However, every country or region has its unique cultural background which will influence the thoughts and behaviours of its residents (Fang and Prybutok, 2013). Further studies should be conducted to test the results of this study in different countries. Second, this study may suffer from selection bias because our participants are active online users only. These users who cease to use Facebook may present different perceptions about their motivations, perceived interactivity, satisfaction and continued use behaviour. Finally, this study mainly focuses on the effects of motivations, perceived interactivity and satisfaction. Other factors (e.g., social influence) that may impact continued use of social media should also be tested by future studies.

### Conclusion

As Chen ([2012](#che12)) describes, users’ loyalty is a critical factor related to the survival and development of a firm. Hence, understanding what factors will impact users’ decision to use social media continuously is a key issue for organizations that own and operate social media. In this study, we propose a theoretical model by integrating the perspectives of uses and gratifications theory and perceived interactivity. The results show that entertainment impacts human-human interaction, human-message interaction and human-community interaction significantly, while socialization has positive influence on human-human interaction and human-community interaction. The results also report that information seeking influences human-message interaction and human-community interaction positively, whereas self-presentation exerts significant effects on human-human interaction and human-community interaction. Moreover, the three types of perceived interactivity have positive effects on satisfaction, while satisfaction, human-human interaction and human-community interaction are the powerful predictor of continued use of social media.

## About the authors

**Meng-Hsiang Hsu** holds a PhD degree from National Sun-Yat-Sen University, Taiwan. He is currently a distinguished professor at the Department of Information Management, National Kaohsiung First University of Science and Technology. His research interests include knowledge management, information ethics and electronic commerce.  
**Chun-Ming Chang** is currently an associate professor at the Department of Tourism Information, Aletheia University, Taiwan. He holds a PhD degree in management from National Kaohsiung First University of Science and Technology, Taiwan. His current research interests include electronic commerce and knowledge management.  
**Hsien-Cheng Lin** is currently an assistant professor at the Department of Health-Business and Administration, Fooyin University and the chief at Department of Planning, Fooyin University Hospital, Taiwan. He holds a PhD degree in management from National Kaohsiung First University of Science and Technology, Taiwan. His current research interests include knowledge management and medical informatics. He is the corresponding author for the article and can be contacted at [u0028909@nkfust.edu.tw](mailto:u0028909@nkfust.edu.tw)  
**Yi-Wan Lin** holds a Master degree in information management from National Kaohsiung First University of Science and Technology, Taiwan.

#### References

*   ACI-FIND (2014). _[The survey of users’ behaviour on mobile devices use in Taiwan: the first half of 2014](http://%20http://www.webcitation.org/6Yk0sIgLV)_ (in Chinese). Retrieved from http://www.find.org.tw/market_info.aspx?n_ID=8296 (Archived by WebCite® at http://www.webcitation.org/6Yk0sIgLV )
*   Anderson, J.C. & Gerbing, D.W. (1988). Structural equation modeling in practice: a review and recommended two-step approach. _Psychological Bulletin, 103_(3), 411-423.
*   Andrew D.S., Nicole B.E., Cliff L. & Donghee Y.W. (2011). Facebook as a toolkit: a uses and gratification approach to unbundling feature use. _Computers in Human Behavior, 27_(6), 2322-2329.
*   Bettman, J., Luce, M.F. & Payne, J.W. (1998). Constructive consumer choice processes. _Journal of Consumer Research, 25_(3), 187-217.
*   Bhattacherjee, A. (2001). Understanding information systems continuance: an expectation-confirmation model. _MIS Quarterly, 25_(3), 351-370.
*   Bhattacherjee, A. & Premkumar, G. (2004). Understanding changes in belief and attitude toward information technology use: a theoretical model and longitudinal test. _MIS Quarterly, 28_(2), 229–254.
*   Boyle, K. & Johnson, T.J. (2010). MySpace is your space? Examining self-presentation of MySpace users. _Computers in Human Behavior, 26_(6), 1392-1399.
*   Burgoon, J.K., Bonito., J.A., Bengtsson, B., Ramirez, A., Dunbar, N.E. & Miczo, N. (2000). Testing the interactivity model: communication processes, partner assessments and the quality of collaborative work. _Journal of Management Information Systems, 16_(3), 33–56.
*   Cemalcilar, Z., Falbo, T. & Stapleton, L. (2005). Cyber communication: a new opportunity for international students’ adaptation? _International Journal of Intercultural Relations, 29_(1), 91-110.
*   Chen, S.C. (2012). The customer satisfaction-loyalty relation in an interactive e-service setting: the mediators. _Journal of Retailing and Consumer Services, 19_(2), 202-210.
*   Chen, G.L., Yang, S.C. & Tang, S.M. (2013). Sense of virtual community and knowledge contribution in a P3 virtual community: motivation and experience. _Internet Research, 23_(1), 4-26.
*   Cheung, C.M.K. & Lee, M.K.O. (2010). A theoretical model of intentional social action in online social networks. _Decision Support Systems, 49_(1), 24–30.
*   Chin, W.W., Marcolin, B.L. & Newsted, P.R. (2003). A partial least squares latent variable modeling approach for measuring interaction effects: results from a Monte Carlo simulation study and an electronic-mail emotion/adoption study. _Information Systems Research, 14_(2), 189-217.
*   Chua, A.Y.K., Goh, D.H.L. & Lee, C.S. (2012). Mobile content contribution and retrieval: an exploratory study using the uses and gratifications paradigm. _Information Processing & Management, 48_(1), 13-22.
*   Diddi, A. & LaRose, R. (2006). Getting hooked on news: uses and gratifications and the formation of news habits among college students in an internet environment. _Journal of Broadcasting & Electronic Media, 50_(2), 193–210.
*   Deci, E. L. (1975). _Intrinsic motivation_. New York, NY: Plenum Press
*   Dunne, A., Lawlor, M. A. & Rowley, J. (2010). Young people’s use of online social networking sites: a uses and gratifications perspective. _Journal of Research in Interactive Marketing, 4_(1), 46-58.
*   Ellison, N.B., Steinfield C. & Lampe, C. (2007). The benefits of Facebook friends: social capital and college students’ use of online social network sites. _Journal of Computer-Mediated Communication, 12_(4), 1143-1168.
*   Fang, J. & Prybutok, V. (2013). The equivalence of internet versus paper-based surveys in IT/IS adoption research in collectivistic cultures: the impact of satisfying. _Behavior & Information Technology, 32_(5), 480-490.
*   Fornell, C. & Larcker, D. (1981). Evaluating structural equation models with unobservable variables and measurement error. _Journal of Marketing Research, 18_(1), 39-50.
*   Hew, K.F. &Hara, N. (2007). Knowledge sharing in online environment: a qualitative case study. _Journal of the American Society for Information Science and Technology 58_(4), 2310-2324.
*   Hsu, M.H., Yen, C.H., Chiu, C.M. & Chang, C.M. (2006). A longitudinal investigation of continued online shopping behavior: an extension of the theory of planned behavior. _International Journal of Human-Computer Studies, 9_(64), 889-904
*   Hsu, M. H., Chang, C. M. &Yen, C. H. (2011). Exploring the antecedents of trust in virtual communities. _Behaviour & Information Technology, 30_(5), 587–601.
*   iThome (2013). [The number of social media users in the globe has reached 1.6 billion](http://www.webcitation.org/6Yk1HHAIT) (in Chinese). Retrieved from http://www.ithome.com.tw/node/83922 (Archived by WebCite® http://www.webcitation.org/6Yk1HHAIT)
*   Kane, G. C., Fichman, R. G., Gallaugher, J. & Glaser, J. (2009). Community relations 2.0\. _Harvard Business Review, 87_(11), 45-50.
*   Kang, Y S. Hong, S. & Lee, H. (2009). Exploring continued online service use behavior: the roles of self-image congruity and regret. _Computers in Human Behavior, 25_(1), 111–122.
*   Kaiser, S. & Müller-Seitz, G. (2008). Leveraging lead users knowledge in software development: the case of weblog technology. _Industry & Innovation, 15_(2), 199-221.
*   Kim, H.W., Xu, Y. & Koh, J. (2004). A comparison of online trust building factors between potential customers and repeat customers. _Journal of the Association for Information Systems, 5_(10), 392-420.
*   Kim, J. (2008). [Perceived difficulty as a determinant of Web search performance.](http://www.webcitation.org/6Yk1j6b2S) _Information Research, 13_(4), paper 379\. Retrieved from http://InformationR.net/ir/13-4/paper379.html (Archived by WebCite® at http://www.webcitation.org/6Yk1j6b2S)
*   Kim, Y., Sohn, D. & Choi, S.M. (2011). Cultural difference in motivations for using social network sites: a comparative study of American and Korean college students. _Computers in Human Behavior, 27_(1), 365–372.
*   Ko, H., Cho, C.H. & Roberts, M.S. (2005). Internet uses and gratifications: a structural equation model of interactive advertising. _Journal of Advertising, 34_(2), 57-70.
*   Kwon, N. (2005). [User satisfaction with referrals at a collaborative virtual reference service](http://www.webcitation.org/6Yk1n3zqB). _Information Research, 11_(2), paper 246\. Retrieved from http://InformationR.net/ir/11-2/paper246.html (Archived by WebCite® at http://www.webcitation.org/6Yk1n3zqB)
*   Laurenceau, J.-P., Barrett, L.F. & Pietromonaco, P.R. (1998). Intimacy as an interpersonal process: the importance of self-disclosure, partner disclosure and perceived partner responsiveness in interpersonal exchanges. _Journal of Personality Social Psychology, 74_(5), 1238-1251.
*   Lee, C.S. & Ma, L. (2012). News sharing in social media: the effect of gratifications and prior experience. _Computers in Human Behavior, 28_(2), 331-339.
*   Lenhart, A. (2009). [Adults and social network Web sites](http://www.webcitation.org/6Yk2Yfm5P). Washington, DC: Pew Research Center. Retrieved from http://www.pewinternet.org/2009/01/14/adults-and-social-network-websites/ (Archived by WebCite® at http://www.webcitation.org/6Yk2Yfm5P)
*   Li, D., Browne, G.J. & Wetherbe, J.C. (2006). Why do internet users stick with a specific Web Site? a relationship perspective. _International Journal of Electronic Commerce, 10_(4), 105–141.
*   Li, D.C. (2011). Online social network acceptance: a social perspective. _Internet Research, 21_(5), 562-580.
*   Limayem, M., Hirt, S. G. & Cheung, C. M. K. (2007). How habit limits the predictive power of intention: the case of information systems continuance. _Management Information Systems Quarterly, 31_(4), 705 -737.
*   Lin, K.Y. & Lu, H.P. (2011). Why people use social networking sites: an empirical study integrating network externalities and motivation theory. _Computers in Human Behavior, 27_(3), 1152-1161.
*   Lin, H. H. & Wang, Y. S. (2006). An examination of the determinants of customer loyalty in mobile commerce contexts. _Information & Management, 43_(3), 271–282.
*   Lowry, P.B., Romano Jr., N.C., Jenkins, J.L. & Guthrie, R.W. (2009). The CMC interactivity model: how interactivity enhances communication quality and process satisfaction in lean-media groups. _Journal of Management Information Systems, 26_(1), 155-195.
*   Lu, H.P., Lin, J.C.C., Hsiao, K.L. & Cheng, L.T. (2010). Information sharing behaviour on blogs in Taiwan: effects of interactivities and gender differences. _Journal of Information Science, 36_(3), 401-416.
*   Luo, M. M., Chea, S. & Chen, J. S. (2011). Web-based information service adoption: a comparison of the motivational model and the uses and gratifications theory. _Decision Support Systems, 51_(1), 21-30.
*   Ma, M. & Agarwal, R. (2007). Through a glass darkly: information technology design, identity verification and knowledge contribution in online communities. _Information Systems Research, 18_(1), 42–67.
*   Marlow, C.A. (2006). _[Linking without thinking: weblogs, readership, and online social capital formation](http://www.webcitation.org/6Yk24qs2H)_. Paper presented at the 56th International Communication Association Conference, Dresden, Germany. Retrieved from http://www.cameronmarlow.com/media/marlow-2006-linking.pdf (Archived by WebCite® at http://www.webcitation.org/6Yk24qs2H)
*   Nguyen, A. & Western, M. (2006). [The complementary relationship between the internet and traditional mass media: the case of online news and information](http://www.webcitation.org/6Yk1rbJCo). _Information Research, 11_(3) paper 259\. Retrieved from http://InformationR.net/ir/11-3/paper259.html (Archived by WebCite® at http://www.webcitation.org/6Yk1rbJCo)
*   Nov, O., Naaman M. &Ye, C. (2010). Analysis of participation in an online photo sharing community: a multidimensional perspective. _Journal of the American Society for Information Science and Technology, 61_(3), 555-566.
*   Oliver. R. L. (1980). A cognitive model for the antecedents and consequences of satisfaction. _Journal of Marketing Research, 17_(4), 460-469.
*   Panteli, N. & Sockalingam, S. (2005). Trust and conflict within virtual inter-organizational alliances: a framework for facilitating knowledge sharing. _Decision Support Systems, 39_(4), 599–617.
*   Papacharissi, Z. & Mendelson, A. (2011). Toward a new (er) sociability: uses, gratifications and social capital on Facebook. In S. Papathanassopoulos (Ed.), _Media Perspectives for the 21st Century_ (pp. 212-213) New York, NY: Routledge.
*   Papacharissi, Z. &Rubin, A.M. (2000). Predictors of internet use. _Journal of Broadcasting & Electronic Media 44_(2), 175-196.
*   Park, N., Kee, K. F. &Valenzuela, S. (2009). Being immersed in social networking environment: Facebook groups, uses and gratifications and social outcomes. _CyberPsychology & Behavior, 12_(6), 729-733.
*   Pempek, T.A., Yermolayeva, Y.A. &Calvert, S.L. (2009). College students’ social networking experiences on Facebook. _Journal of Applied Developmental Psychology, 30_(3), 227-238.
*   Ringle, C.M., Wende, S. & Will, A. (2005). _[SmartPLS 2.0 (beta)](http://www.webcitation.org/6Yk1VnEP1)._ Bönningstedt, Germany: SmartPLS Gmbh. Retrieved from http://www.smartpls.de. (Archived by WebCite® http://www.webcitation.org/6Yk1VnEP1)
*   Roy, S. K. (2009). Internet uses and gratifications: a survey in the Indian context. _Computers in Human Behavior, 25_(4), 878-886.
*   Rubin, A.M. (1994). Media uses and effects: a uses and gratifications perspective. In J. Bryant & D. Zillmann (Eds.), _Media effects: advances in theory and research_ (pp. 417-436). Hillsdale, NJ: Lawrence Erlbaum Associates.
*   Sangwan, S. (2005). [Virtual community success: a uses and gratifications perspective](http://www.webcitation.org/6Yk2BUqk7). In _Proceedings of the 38th Hawaii International Conference on System Sciences_ (pp. 193c). Washington, DC: IEEE, Retrieved from http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=1385631 (Archived by WebCite® at http://www.webcitation.org/6Yk2BUqk7)
*   Schlenker, B.R. &Wowra, S.A. (2003). Carryover effects of feeling socially transparent or impenetrable on strategic self-presentation. _Journal of Personality Social Psychology, 85_(5), 871-880.
*   Shao, Guosong. (2009). Understanding the appeal of user-generated media: a uses and gratification perspective. _Internet Research, 19_(1), 7-25.
*   Shen, A.X.L., Cheung, C.M.K., Lee, M.K.O. & Chen, H. (2011). How social influence affects we-intention to use instant messaging: the moderating effect of use experience. _Information Systems Frontiers, 13_(2), 157-169.
*   Stafford, T.F., Stafford, M.R. &Schkade, L.L. (2004), Determining uses and gratifications for the internet. _Decision Sciences, 35_(2), 259-288.
*   Tamborini, R. & Skalski, P. (2006). The role of presence in the experience of electronic games. In P. Vorderer & J. Bryant (Eds.), _Playing video games: motives, responses and consequences_. (pp. 225-240). Mahwah, N.J.: Erlbaum.
*   Taylor, A.R., Cool, C., Belkin, N.J. &Amadio, W.J. (2007). Relationships between categories of relevance criteria and stage in task completion. _Information Processing & Management, 43_(4), 1071-1084.
*   TechHive (2013). [Social media survey looks at race, gender and age of users](http://www.webcitation.org/6Yk1bim2v). Retrieved from http://www.techhive.com/article/2028591/social-media-survey-looks-at-race-gender-and-age-of-users.html (Archived by WebCite® http://www.webcitation.org/6Yk1bim2v)
*   Thelwall, M., Byrne, A. & Goody, M. (2007). [Which types of news story attract bloggers?](http://www.webcitation.org/6Yk1y6fDD) _Information Research, 12_(4), paper 327\. Retrieved from http://InformationR.net/ir/12-4/paper327.html (Archived by WebCite® at http://www.webcitation.org/6Yk1y6fDD)
*   Viswanath, B., Mislove, A., Cha, M. & Gummadi, K.P. (2009). [On the evolution of user interaction in Facebook](http://www.webcitation.org/6Yk2Hp74T)). In _Proceedings of at the 2nd ACM Workshop on Online Social Networks, Barcelona, Spain_ (pp. 37-42). New York, NY: ACM. Retrieved from http://www.mpi-sws.org/~gummadi/papers/wosn23-viswanath.pdf (Archived by WebCite® at http://www.webcitation.org/6Yk2Hp74T)
*   Wang, Q.Y., Woo, H.L., Quek, C.L., Yang, Y.Q. & Lin, M. (2012). Using the Facebook group as a learning management system: an exploratory study. _British Journal of Educational Technology, 43_(3), 428-438.
*   Wu, J.H., Wang, S.C. & Tsai, H.H. (2010). Falling in love with online games: the uses and gratifications perspective. _Computers in Human Behavior, 26_(6), 1862-1871.
*   Yang, H. L. & Lai, C.Y. (2011). Understanding knowledge sharing behaviour in Wikipedia. _Behavior & Information Technology, 30_(1), 131-142.
*   Zhao, L. &Lu, Y. (2012). Enhancing perceived interactivity through network externalities: an empirical study on micro-blogging service satisfaction and continuance intention. _Decision Support Systems, 53_(4), 825–834.

## Appendix

<table class="center"><caption>  
Table 1: Measurement items</caption>

<tbody>

<tr>

<th>Item</th>

<th>Sources</th>

</tr>

<tr>

<td colspan="2">**Entertainment (EN)**</td>

</tr>

<tr>

<td>EN1 Using Facebook gives me pleasure.</td>

<td rowspan="4">Adapted from Andrew _et al._ (2011); Wu _et al._ (2010)</td>

</tr>

<tr>

<td>EN2 Using Facebook makes me feel relaxed.</td>

</tr>

<tr>

<td>EN3 Using Facebook helps me escape life’s unpleasantness.</td>

</tr>

<tr>

<td>EN4 Using Facebook helps me pass time.</td>

</tr>

<tr>

<td colspan="2">**Socialisation (SO)**</td>

</tr>

<tr>

<td>SO1 I have a friends’ community on Facebook.</td>

<td rowspan="4">Adapted from Andrew _et al._ (2011) and Wu _et al._ (2010)</td>

</tr>

<tr>

<td>SO2 Facebook makes it easier for me to keep in touch with my friends.</td>

</tr>

<tr>

<td>SO3 I use Facebook to reconnect with friends I had lost contact with through Facebook.</td>

</tr>

<tr>

<td>SO4 Facebook is a useful communication channel to connect with people.</td>

</tr>

<tr>

<td colspan="2">**Information seeking (IS)**</td>

</tr>

<tr>

<td>IS1 Facebook helps me to find information that is up-to-date.</td>

<td rowspan="5">Adapted from Andrew _et al._ (2011) and Lee and Ma (2012)</td>

</tr>

<tr>

<td>IS2 Facebook helps me to find friends’ messages very easily.</td>

</tr>

<tr>

<td>IS3 I use Facebook to keep up with the latest news and events.</td>

</tr>

<tr>

<td>IS4 Facebook helps me to retrieve helpful information I need.</td>

</tr>

<tr>

<td>IS5 Facebook allows me to effectively seek information which I am interested in</td>

</tr>

<tr>

<td colspan="2">**Self-presentation (SP)**</td>

</tr>

<tr>

<td>SP1 I usually update my profile on Facebook.</td>

<td rowspan="5">Adapted from Andrew _et al._ (2011) and Pempek _et al._ (2009)</td>

</tr>

<tr>

<td>SP2 I share my personal information and living conditions on Facebook frequently.</td>

</tr>

<tr>

<td>SP3 I can express my opinions through publishing articles on Facebook.</td>

</tr>

<tr>

<td>SP4 I usually express myself through Facebook to make other people know me more conveniently.</td>

</tr>

<tr>

<td>SP5 I share my personal information on Facebook to help myself to build up my self-image.</td>

</tr>

<tr>

<td colspan="2">**Human-human interaction (HH)**</td>

</tr>

<tr>

<td>HH1 Facebook makes it easy for me to communicate with other people.</td>

<td rowspan="5">Adapted from Lu _et al._ (2010)</td>

</tr>

<tr>

<td>HH2 I can exchange and share opinions with other people easily by using Facebook.</td>

</tr>

<tr>

<td>HH3 Facebook makes it easy for me to connect with other people.</td>

</tr>

<tr>

<td>HH4 Facebook makes it easy for me to develop interpersonal relationships with other people.</td>

</tr>

<tr>

<td>HH5 I can more easily connect with friends by using Facebook.</td>

</tr>

<tr>

<td colspan="2">**Human-message interaction (HM)**</td>

</tr>

<tr>

<td>HM1 I can use Facebook to search information efficiently.</td>

<td rowspan="5">Adapted from Lu _et al._ (2010) and Taylor _et al._ (2007)</td>

</tr>

<tr>

<td>HM2 I can use Facebook to filter information which is useful to me.</td>

</tr>

<tr>

<td>HM3 I can use Facebook to connectto some information which is interesting to me</td>

</tr>

<tr>

<td>HM4 I can obtain meaningful information by using Facebook.</td>

</tr>

<tr>

<td>HM5 Generally, Facebook provides the great majority of information to me.</td>

</tr>

<tr>

<td colspan="2">**Human-community interaction (HC)**</td>

</tr>

<tr>

<td>HC1 Using Facebook enables me to feel that I am a dispensable member of the community.</td>

<td rowspan="4">Developed based on Yang and Lai (2011)</td>

</tr>

<tr>

<td>HC2 Using Facebook enables me to feel that I am attached to the community.</td>

</tr>

<tr>

<td>HC3 Using Facebook has promoted and extended my social networks in the community.</td>

</tr>

<tr>

<td>HC4 Using Facebook enables me to feel that I am a valuable member of the community.</td>

</tr>

<tr>

<td colspan="2">**Satisfaction (SA)**</td>

</tr>

<tr>

<td>SA1 I find that Facebook is easy to use.</td>

<td rowspan="3">Adapted from Ko _et al._ (2005) and Lu _et al._ (2010)</td>

</tr>

<tr>

<td>SA2 I am satisfied with the functionality of Facebook.</td>

</tr>

<tr>

<td>SA3 Overall, I am satisfied with my experience for using Facebook.</td>

</tr>

<tr>

<td colspan="2">**Continued use (CU)**</td>

</tr>

<tr>

<td>CU1 I intend to use Facebook when I am online.</td>

<td rowspan="3">Adapted from Wu _et al._ (2010)</td>

</tr>

<tr>

<td>CU2 I would like to continue using Facebook.</td>

</tr>

<tr>

<td>CU3 Compared to other social media, I intend to use Facebook continuously.</td>

</tr>

</tbody>

</table>

<figure class="centre">![](TableApp2.png)

<figcaption>Table 2: Factor loadings of the constructs</figcaption>

</figure>