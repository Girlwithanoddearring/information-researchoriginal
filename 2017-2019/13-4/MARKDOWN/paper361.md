#### vol. 13 no. 4, December, 2008

# Trends and approaches in information behaviour research

#### [Pertti Vakkari](mailto:pertti.vakkari@uta.fi)  
Department of Information Studies, University of Tampere, Tampere 33014, Finland

#### Abstract

> **Introduction.** The paper explores theoretical and methodological trends in information behaviour research.  
> **Method.** Content analysis was performed on papers accepted for the ISIC 1996 and ISIC 2008 (Information Seeking in Context) conferences. The distributions of eight variables representing major theoretical and methodological characteristics of papers in both years were compared.  
> **Results.** In information behaviour research there is an observable trend towards descriptive, qualitative studies using individual level variables and loose theoretical frames. There is a declining trend of theoretical, explanatory and quantitative studies. There is a shift of focus from the information behaviour of professionals to every-day-life information seeking. The variety of topics and research techniques within qualitative methods has increased.  
> **Conclusions.** The homogenization of research approaches in information behaviour research challenges scholars to pursue more varied theoretical approaches. Some suggestions for future directions are given.

## Introduction

In my concluding speech at the first ISIC in Tampere, I sketched in bright colours the landscape of information need and use studies seen from the perspective of the conference ([Vakkari 1997](#vakkari1997)). In my mind, the conference contributed both socially and cognitively to the development of the field.

The social contribution consisted of organizing a permanent medium of communication and exchange of ideas for those researchers who were, and are, interested in information behaviour studies. The conference was the first of its kind with this orientation. Participants were inspired by the opportunity to exchange ideas with colleagues around the world with parallel research interests. As a result of its enthusiastic reception, the conference in Tampere generated a continuing series of biannual ISIC conferences, the one held here in Vilnius being the 7th ISIC. The conference has grown as an important means for advancing information behaviour research by regularly bringing together researchers to present and discuss their findings and ideas. It has created a sense of community among researchers.

A sign of the growth in importance of this research field is the recent founding of conferences with tangible interests like IIiX (Information interaction in context) and i3 (information: interaction and impact). The former aims at connecting studies on interactive IR including web searching with broader studies of information seeking. The latter focuses more on broader issues of impact of and interaction with information. Thus, we are acting in the field of research, which is attracting a growing number of scholars with varying research interests from information retrieval and digital libraries to the impact and use of information.

I assessed the cognitive contribution of the first ISIC conference by relating the trends emerging from the papers to some of the key reviews of the field such as those by Dervin and Nilan ([1986](#dervin1986)) or Wilson and Walsh ([1996](#wilson1996)). The cognitive contribution of the conference in Tampere consisted of nine trends, which, to my mind, brightened the horizon of information behaviour studies ([Vakkari 1997](#vakkari1997)):

*   a shift in meta-theory from person-centred approach to person in context or situation oriented approach;
*   more varied and holistic theoretical approaches;
*   growing embedding of information needs and seeking in the phenomena of which they are part;
*   more process orientation;
*   more longitudinal studies;
*   intensive use of theoretical and methodological ideas from other disciplines;
*   increased variety of methods and use of multiple methods;
*   qualitative methods have gained a leading position; and
*   research has professionalized in terms of academic norms (maturity)

My impression was that many of the meta-theoretical ideas for information behaviour research put forward in the article by Dervin and Nilan ([1986](#dervin1986)) published ten years before the conference in Tampere, were to a great extent adopted by the members of our research community. This conclusion was based on the presentations and discussions at the conference.

I saw also some dark clouds in the sky of our research field. They consisted of six shortcomings, which were ([Vakkari 1997](#vakkari1997)):

*   lack of theoretical growth;
*   weak specification of meta-theories to unit (substantive) theories;
*   unspecific definitions of basic concepts and their relations;
*   weak conceptual relationship to earlier studies;
*   concentration on individual level variables, group or organizational or societal level usually missing in the studies; and
*   information use is a seldom studied area.

It seems to me now that the way our object of research was typically conceptualized and observed had changed compared to studies in the 1970s and 1980s. People were conceptualized more as actors with tasks and interests, information being one means for coping with them. Human information behaviour was explored through more versatile research approaches and techniques than earlier. Although the new paradigmatic approach had established itself, the way it was applied did not always appear theoretically sound to me. The conceptual frames used were unspecific, some times unexplained and without proper connections to conceptualizations or findings in earlier studies. This implied that the central concepts used were often insufficiently defined. This was to my mind the major reason for sparse theoretical growth. ([Wagner and Berger 1985](#wagner1985); [Vakkari 1998](#vakkari1998))

Theoretical growth refers here to the growth of unit (substantial) theories. A unit theory consists of a set of concepts and a set of relations interconnecting those concepts in an account of some phenomenon. Growth can be assessed by observing the empirical support gained by the theories and by analysing the conceptual growth of the theories. Conceptual growth means that the new theory is conceptually more specific, more general or more comprehensive than its earlier variants. This implies a similar conceptual frame. Conceptual growth can also occur by introducing in the problem area a differing, competing conceptual machinery, which is then supported empirically. Thus, in addition to empirical support, theoretical growth consists of differentiating the concepts of current theory or introducing new concepts in the existing frame or introducing a competing conceptualization.

Although I noticed problems in the theoretical progress in our field, there were also fruitful examples like studies by Byström ([1999](#bystrom1999)) or Kuhlthau ([2004](#kuhlthau2004)). Both had a clear conceptual structure with sufficient definitions. My later studies have shown that frames in both studies have been enriched by new concepts and increasing empirical support.

I concluded my summing up in Tampere in the following way ([Vakkari 1997](#vakkari1997)):

‘A person-in-situation meta-theory has replaced the old system-centred one. Information behaviour is more and more understood as a process of which it is a part. Information is seen as a social construct created in the interaction of people and messages. The research object is conceptualized more frequently as a social phenomenon leading to the use of variables from the organizational and group level. An encouraging feature is also the importing of ideas from other disciplines to information behaviour studies. The results of the ISIC conference are opening a promising and challenging horizon of expectation for our field of research.’

The continuing series of ISIC conferences has indeed advanced the social institutionalization of our field. One would also expect that the growing interaction and the exchange of ideas among researchers would enhance the cognitive growth of this field. To analyse the cognitive trends of ISIC conferences and perhaps more generally in the field of information behaviour studies, I compared the accepted papers in ISIC 1996 and 2008 in terms of their theoretical and methodical approaches. What kind of changes can one observe in the central elements of research from the first to the current conference.

I selected eight central variables indicating theoretical and methodological trends in the articles. I categorized the variables and coded each of the articles in the appropriate category of all the selected variables. Only one value was coded for each variable. Categorizations were a mix of data-driven and theory-driven approaches. In 1996 there were twenty-five accepted presentations and in 2008, thirty-four.

The emerging trends surprised me somewhat. They were the following:

*   increasing variety of topics – from the information behaviour of professionals to every-day-life information seeking;
*   concentration of research approaches – more empirical, fewer theoretical and methodological studies;
*   decrease in explanatory research and increase in descriptive research;
*   fewer analytical and more qualitative studies;
*   individual level research at the expense of group level research;
*   focus on information seeking instead of the whole information behaviour process;
*   looser theoretical framing;
*   decrease in explanation of the contribution of results to research and practice, and
*   the import of ideas from other fields continues.

The overall conclusion from all these trends is rather alarming. Research is becoming more descriptive, not aiming at model building or explanations. The versatility of approaches and methods is decreasing. This all is a threat to the quality of research in our field. On the positive side is the versatility of topics and many-sided research techniques within qualitative methods.

Before the final conclusion, let us analyse the results in more detail, variable by variable.

## Versatility of topics

It has been a common claim for a long time that studies on the information behaviour of professionals are over-represented compared to studies of ordinary citizens’ information seeking. That was also the case in the first ISIC conference (Table 1). Although professionals still interest researchers in our field, studies on information behaviour in the every-day life of citizens have gained more footing.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 1: Topics of papers in ISIC 1996 and ISIC 2008**  
</caption>

<tbody>

<tr>

<th valign="middle">Topics</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Professionals</td>

<td align="center">12</td>

<td align="center">11</td>

</tr>

<tr>

<td>Everyday life</td>

<td align="center">4</td>

<td align="center">10</td>

</tr>

<tr>

<td>Students</td>

<td align="center">3</td>

<td align="center">3</td>

</tr>

<tr>

<td>Relevance</td>

<td align="center">0</td>

<td align="center">2</td>

</tr>

<tr>

<td>Web searching</td>

<td align="center">0</td>

<td align="center">4</td>

</tr>

<tr>

<td>Self-analysis</td>

<td align="center">0</td>

<td align="center">2</td>

</tr>

<tr>

<td>Other</td>

<td align="center">6</td>

<td align="center">2</td>

</tr>

<tr>

<td>Total</td>

<td align="center">25</td>

<td align="center">34</td>

</tr>

</tbody>

</table>

Studies related to information retrieval, e.g., web searching and relevance, have also become slightly more common. Information is increasingly sought through the Internet, extranets and a myriad of information systems. Their role in acquiring all kinds of information for use in various tasks and for various interests has grown tremendously. Therefore, ignoring these central means of information seeking in studies on information behaviour may marginalize our field of research and a marginalised field neither lures new generations of researchers nor new financing sources. These images of threats and opportunities were also discussed in the keynote addresses by Järvelin ([2004](#jarvelin2004)) and Kuhlthau ([2005](#kuhlthau2005)) in ISIC 2004 in Dublin.

There is genuine need to understand information retrieval and searching processes from a more general context than computer science typically presents. Our research tradition includes valuable concepts and tools for better understanding information searching in digital environments. This is an opportunity for our research community. However, the opportunity is not as great as it was some years ago due to the new competing conferences providing a platform for those who wish to study information retrieval in a broader context of information seeking.

The class ‘other’ includes in 1996 mostly theoretical and methodological studies, which do not fit with other categories.

## Concentration on empirical studies

There is a trend to focus on empirical research problems (Table 2). Almost all the papers in this year’s conference deal with empirical research questions. The number of theoretical and methodological studies has decreased. Theoretical papers were typically written from a meta-theoretical perspective. They compared various approaches and proposed fresh theoretical approaches to conceptualize major problems in our field of research. The theoretical approaches suggested in 1996 were social constructivism and phenomenology, grounded theory, naturalistic paradigm and discourse analysis ([Vakkari 1997](#vakkari1997)). Does the current theoretical silence mean that the proposed approaches have established themselves in our field of research? That is at least in part the reason as the further analysis confirms. This would explain that there is not so much need for meta-theoretical debating.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 2: Research approaches of papers in ISIC 1996 and ISIC 2008**</caption>

<tbody>

<tr>

<th valign="middle">Approaches</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Empirical</td>

<td align="center">18</td>

<td align="center">31</td>

</tr>

<tr>

<td>Theoretical</td>

<td align="center">5</td>

<td align="center">2</td>

</tr>

<tr>

<td>Methodological</td>

<td align="center">2</td>

<td align="center">1</td>

</tr>

<tr>

<td>Total</td>

<td align="center">25</td>

<td align="center">34</td>

</tr>

</tbody>

</table>

## Focus on description

The descriptive approach aims at describing the central features or factors of a phenomenon. Comparative research design aims at comparing the object of study in its subgroups or look for connections between various factors. The idea is to observe whether the phenomenon of interest varies between the subgroups. Comparative studies resemble to a certain extent explanatory ones. They may seek to explain variation between the subgroups. Explanatory design seeks to explain the variation of a dependent factor by the variation of certain independent factors. There are several more specific research designs and approaches to apply for realizing these basic designs. Both qualitative and quantitative research methods can be utilized in all of the three designs. However, quantitative approaches are more common in the explanatory end of designs, and qualitative ones in the descriptive end of the continuum. I have also classified some qualitative studies as explanatory when they have attempted to show an association between two or more factors and tried to exclude some factors that could bias the association.

Descriptive research design has increased in popularity from about a half to two thirds (Table 3). Descriptive studies typically thematize or categorize the research object into its major components, not relating the components emerged in the data to each other. These studies do not seek to understand the phenomenon of interest in the sense of co-variation of the factors found. Please note that it is possible to analyse how factors or categories are related to each other in qualitative research as in quantitative, i.e. to analyse co-variation of the elements under study.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 3\. Research designs of papers in ISIC 1996 and ISIC 2008**</caption>

<tbody>

<tr>

<th valign="middle">Type of research</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Descriptive</td>

<td align="center">12</td>

<td align="center">23</td>

</tr>

<tr>

<td>Comparative</td>

<td align="center">3</td>

<td align="center">1</td>

</tr>

<tr>

<td>Exploratory</td>

<td align="center">8</td>

<td align="center">10</td>

</tr>

<tr>

<td>Not applicable</td>

<td align="center">2</td>

<td align="center">0</td>

</tr>

<tr>

<td>Total</td>

<td align="center">25</td>

<td align="center">34</td>

</tr>

</tbody>

</table>

The proportion of explanatory and comparative studies has decreased somewhat. I have to say that I applied the criteria for explanatory studies rather loosely. Some of them could have also been placed in the category of ‘comparative studies’. I also got the impression that there is theoretical and explanatory potential in some studies, but for some reason the potential was not realized in the empirical analysis. Anyway, the finding suggests that our interest in understanding information behaviour more deeply has decreased. This trend also contradicts with one of the core aims of science, which is to try to understand phenomena in the sense of seeking their source of variation.

Comparative and explanatory studies aim at revealing connections between certain factors. Without knowledge of those kinds of associations, it is difficult, for example, to say anything about how information behaviour is related to some features of systems or services aimed at supporting information seeking and use. Those kind of practical recommendations for action require that we have created results, which connect certain features of information behaviour to certain features of systems or services.

The proportional decrease of explanatory studies seems to hint that we are becoming less able to understand human information behaviour more deeply and therefore less able to suggest practical application of our results. The latter threat depends naturally also to which degree we include system or service features in our research designs. Without information about those features, it is difficult to make suggestions about practical actions based on our findings.

## Trend towards qualitative methods

Qualitative methods have continued their triumphal march ([cf. McKehnie et al. 2002](#mckehnie2002)). Their proportion has increased from about one third to little over half of all studies (Table 4). The interest in using quantitative methods has somewhat decreased. Also the number of analytical studies has decreased, reflecting the diminished number of theoretical studies.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 4: Methods used in papers in ISIC 1996 and ISIC 2008**</caption>

<tbody>

<tr>

<th valign="middle">Type of research</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Qualitative</td>

<td align="center">9</td>

<td align="center">18</td>

</tr>

<tr>

<td>Quantitative</td>

<td align="center">3</td>

<td align="center">7</td>

</tr>

<tr>

<td>Mixed Qual & Quant</td>

<td align="center">6</td>

<td align="center">7</td>

</tr>

<tr>

<td>Analytical</td>

<td align="center">7</td>

<td align="center">2</td>

</tr>

<tr>

<td>Total</td>

<td align="center">25</td>

<td align="center">34</td>

</tr>

</tbody>

</table>

A general trend in the studies was to combine several research techniques. It was typical in qualitative studies to apply two or three different data gathering and analysis techniques. This naturally makes it possible for a researcher to create a more accurate and valid account of the research object. It is paradoxical, that at the same time as researchers with more varied methodological tools aim at more accurate and valid descriptive accounts of their research object, they are not interested in explanatory research designs. My impression is that equipped with such tools researchers have a great potential for a deeper understanding of the mechanisms of information behaviour.

Explanatory design was relatively more common in studies combining qualitative and quantitative research methods. It was typical that factors and their categories were created by using qualitative analysis. These variables were then quantified, which enabled the use of statistical analysis.

## Focus on individual level variables

In my speech at ISIC in Tampere I felt that one of the weaknesses in the studies on information behaviour was a too strong concentration on individual level variables ([Vakkari 1997](#vakkari1997)). Individuals’ behaviour and actions were typically described and explained by individual level characteristics. Therefore, I called for more studies combining individual level variables with variables from organizational levels. The latter describe features of organizations or groups, thus bringing organizational context to the studies. One of the major presuppositions of the organizers of the first ISIC was that in studies of information behaviour it is necessary to take into account the social context of individuals. We believed and still believe that these factors centrally frame the information behaviour of individuals.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 5: Social level of variables in papers in ISIC 1996 and ISIC 2008**</caption>

<tbody>

<tr>

<th valign="middle">Level of variables</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Individual</td>

<td align="center">10</td>

<td align="center">28</td>

</tr>

<tr>

<td>Group (Organizational)</td>

<td align="center">9</td>

<td align="center">4</td>

</tr>

<tr>

<td>Societal</td>

<td align="center">1</td>

<td align="center">0</td>

</tr>

<tr>

<td>Not applicable</td>

<td align="center">5</td>

<td align="center">2</td>

</tr>

<tr>

<td>Total</td>

<td align="center">25</td>

<td align="center">34</td>

</tr>

</tbody>

</table>

In the current ISIC papers there are notably few studies that take group or organization level variables into account. In the first ISIC the share of individual level variables compared to group level variables was about even (Table 5). Currently, the researchers seem to focus almost obsessively on individual level variables. This refers to information seeking out of context, at least out of social context. In some studies the organizational or group dimension was mentioned and discussed somewhat, but it was not elaborated enough so that it would have brought other than individual level factors into analysis. This indirect interest in the broader context hints that the situation may not be so homogenous than my analysis indicates.

## Focus on seeking rather than on the whole process

Another core premise of the organizers of the first ISIC was that information seeking is a process. It was reflected in the name of the conference, which differentiated between information needs, seeking and use. Our aim was to encourage submission of papers dealing with this whole spectrum of information behaviour. Naturally, this premise was rooted in the conception of information behaviour as an activity generated by tasks or interest, for which information was used. It was theoretically motivated to cover the whole process from needs to use.

Table 6 indicates which phases of the information behaviour process the studies cover. In general there is a clear shift of focus from a process approach to studying only one phase of the process, especially information seeking. In 1996 nine out of twenty-five studies (36%) focused on only one of the phases of the process, whereas the corresponding figure in 2008 was eighteen out of thirty-four (53%) papers. Especially the number of studies dealing with the whole process has more than halved, from eight to three. This is reflected also in the decrease of longitudinal studies from five to two. Thus, it seems that information behaviour is not conceptualized as strongly as earlier as a process.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 6: Phase of information behaviour process in papers in ISIC 1996 and ISIC 2008**</caption>

<tbody>

<tr>

<th valign="middle">Phase of the process</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Needs</td>

<td align="center">2</td>

<td align="center">0</td>

</tr>

<tr>

<td>Seeking</td>

<td align="center">3</td>

<td align="center">14</td>

</tr>

<tr>

<td>Needs - Seeking</td>

<td align="center">5</td>

<td align="center">6</td>

</tr>

<tr>

<td>Use</td>

<td align="center">4</td>

<td align="center">4</td>

</tr>

<tr>

<td>Seeking - Use</td>

<td align="center">1</td>

<td align="center">5</td>

</tr>

<tr>

<td>Needs – Seeking - Use</td>

<td align="center">8</td>

<td align="center">3</td>

</tr>

<tr>

<td>Not applicable</td>

<td align="center">1</td>

<td align="center">2</td>

</tr>

<tr>

<td>Total</td>

<td align="center">25</td>

<td align="center">34</td>

</tr>

</tbody>

</table>

In my speech in Tampere, I felt that there was a shortage of studies on information use. I suggested that we should devote more interest to those studies ([Vakkari 1997](#vakkari1997)). The figures show that there is a clearly declining trend in studying information use. In 1996 thirteen out of twenty-five studies dealt with use (52%), whereas in 2008 the figure was twelve out of thirty-four (35%). Thus, there is still room for studies with more focus on the use of information.

## Loosening of conceptual frames

I was also curious to know how the theoretical frameworks of the studies were connected to earlier studies and how their contribution, i.e., theoretical or empirical findings, were related to the existing body of knowledge. I analysed to what extent the theoretical framework and the findings of the study were explicated and related to the empirical and theoretical results of earlier relevant studies. I developed a classification consisting of three categories. ‘Loose connection’ refers to general remarks concerning earlier empirical or theoretical findings in relation to the actual study. No specific results or models were clearly explicated. ‘Strong connection’ refers to versatile, detailed and specified discussion of earlier results in relation of the study. ‘Medium connection’ refers to relatively specific, but brief discussion of the study in relation of related findings.

It seems that studies in the first ISIC represent a greater theoretical and empirical continuity than studies in the current conference (Table 7). The studies in 1996 have more explicated relations to earlier research than in the latter one. None of the articles in 1996 has a loose theoretical framework. Most of them are built on a solid frame connecting the study to the existing body of knowledge. Naturally, the differences in the connections of the framework to earlier studies are reflected in the way the results are related to the research literature in both years. If one has built a frame connecting the study firmly to related studies, it is easy to indicate the contribution of one’s work by just applying the frame. Surprisingly many studies in 2008 are left without explicated connections to earlier studies or have only loose relations to the existing research tradition. I would like to emphasize that vague connections to related studies do not reflect the uniqueness of those studies, which would reduce the number of possible related studies.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 7\. Explication of theoretical frame and contribution in ISIC 1996 and 2008**</caption>

<tbody>

<tr>

<th valign="middle">Theoretical frame</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Loose connections</td>

<td align="center">0</td>

<td align="center">10</td>

</tr>

<tr>

<td>Medium connections</td>

<td align="center">10</td>

<td align="center">14</td>

</tr>

<tr>

<td>Strong connections</td>

<td align="center">15</td>

<td align="center">10</td>

</tr>

<tr>

<td>

<div align="center">**Contribution**</div>

</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>No connections</td>

<td align="center">2</td>

<td align="center">9</td>

</tr>

<tr>

<td>Loose connections</td>

<td align="center">9</td>

<td align="center">12</td>

</tr>

<tr>

<td>Medium connections</td>

<td align="center">10</td>

<td align="center">7</td>

</tr>

<tr>

<td>Strong connections</td>

<td align="center">4</td>

<td align="center">6</td>

</tr>

</tbody>

</table>

I was also interested to know in which way the studies contributed to the existing body of knowledge. I classified the contribution explicated in the papers into five categories. ‘Empirical support’ means that the findings of the study typically confirm or in some cases refute earlier empirical findings or theories. ‘New categories or concepts’ means that the study introduced new categories within a known concept or introduced a new concept either as a result of empirical analysis or more seldom as a theoretical construct to be tested empirically. ‘Revision of model’ refers to changing model in some respect. ‘New methodological approach’ typically means introducing a fresh meta-theoretical stand with some methodological recommendations. ‘Nothing special’ means that the contribution is expressed in such general terms, that it is not possible to classify it in the previous categories.

What strikes most is the proportionally great share of papers not explicating special contribution. Their proportion has grown during the years from 40% to 56% (Table 8). It is evident that this is an implication of the loose theoretical framing. Without an explicated frame it is difficult to show how one contributes to the existing knowledge.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
**Table 8: Type of contribution in papers in ISIC 1996 and ISIC 2008**</caption>

<tbody>

<tr>

<th valign="middle">Contribution</th>

<th valign="middle">1996</th>

<th valign="middle">2008</th>

</tr>

<tr>

<td>Empirical support</td>

<td align="center">9</td>

<td align="center">11</td>

</tr>

<tr>

<td>New categories or concepts</td>

<td align="center">3</td>

<td align="center">3</td>

</tr>

<tr>

<td>Revision of a model</td>

<td align="center">0</td>

<td align="center">1</td>

</tr>

<tr>

<td>New methodological approach</td>

<td align="center">3</td>

<td align="center">0</td>

</tr>

<tr>

<td>Nothing special</td>

<td align="center">10</td>

<td align="center">19</td>

</tr>

<tr>

<td>Total</td>

<td align="center">25</td>

<td align="center">34</td>

</tr>

</tbody>

</table>

In other respects there are not many differences. Empirical support has been the most typical contribution in both years. Only about one study out of ten has created new concepts or categorizations. One could have expected that the studies providing new concepts or categories would have increased in the pace of descriptive qualitative studies applying multiple research techniques. A typical aim of a qualitative study especially in the tradition of grounded theory is to reveal and create new categorizations. Moreover, it seems that the calming meta-theoretical discussion has as its natural consequence reduced the number of new methodological approaches.

In all, it seems that research in information behaviour is moving towards the future on a shaking bridge with weak ties to research tradition. Also the somewhat declining trend to import ideas from other fields seems to reflect that. The proportion of studies borrowing ideas from other fields has decreased from 60% to 40%. Loosening connections to research tradition is a threat in many respects. It implies most of the shortcomings mentioned earlier: vague conceptualization of research object, unclear idea of one’s own contribution and therefore, lack of theoretical or even empirical growth. In the long run it means that the quality of research is decreasing.

## Limitations

My analysis is based on final papers of the first ISIC, and on accepted but not revised papers of this ISIC. The revision of papers according to reviewers’ comments typically improves papers. Therefore, the accepted, but not revised papers may give a somewhat biased and gloomier picture of the research landscape than the finished versions presented at the conference. If we suppose, that the authors have modified their papers, how may this influence my findings and conclusions?

It seems that the changes in the papers do not apply to their topics, research approaches and designs, research methods, level of variables and the phase of information behaviour process. The revisions would not change these basic solutions. Thus, changes may concern only the explication of theoretical frame and the type of contribution. It may be that in the revised versions the authors have explained more in detail how their study is related to the relevant literature. Therefore, it may be that the explicated theoretical and empirical continuity between research tradition and the presented studies is stronger than the accepted papers indicate. Perhaps the bridge between the existing body of knowledge and the current studies is more firmly founded that the papers lead us to suppose. In other respects, I would claim, the revisions do not change my results and conclusions.

## Conclusions

My analysis has revealed more gloom than bright lights to our research community. On the bright side there is the growing versatility of research topics. Not only professionals’ information behaviour but also citizens’ everyday life information seeking is interesting researchers more. Web searching and relevance is gaining footing among the research topics. Also, the increasing interest in studying information behaviour in the digital environment is a positive sign.

Another positive sign is an increasing conceptualization of information behaviour as a part of a larger activity that generates it. Information behaviour is not seen as a separate phenomenon on its own, but as an activity serving various tasks and interests.

On the darker side are some features that seem to reflect the homogenization of our field of research. Researchers focus increasingly and almost solely on empirical studies. Their interest in descriptive designs instead of explanatory designs has grown. There is a growing trend towards qualitative studies. However, the use of several research techniques within general research designs has become more popular, which potentially increases the validity of the findings. There is strong concentration on individual level variables, which leaves the social context of the information behaviour untouched. Another restriction of the studies is the relatively weak interest in information behaviour as a process. Studies on one phase of this process, especially information seeking, have gained more footing. Also the interest in information use has decreased somewhat.

Compared to studies in 1996 the studies in 2008 seem to construct weaker ties to the earlier relevant research in the sense of building a conceptual frame or explicating properly how the results contribute to the existing body of knowledge. There seems to be a trend of loosening conceptual frames in our field of research.

All these features may narrow the theoretical and methodological tools of our research and consequently, the richness and versatility of our findings, especially theoretical growth.

Several of these problematic features of research are intertwined. In some way loose theoretical conceptualization is associated with the descriptive research design, the use of qualitative methods and perhaps also the focus on individuals.

Compared to an explanatory or a comparative research design, a descriptive one does not require as coherent a conceptual framing. To be able to compare or explain, one has to relate concepts, i.e., factors in some way. This makes some kind of conceptual framing necessary. In the same way, if one includes factors from other than individual level in the research design, one is typically analysing the relations of variables from individual and, for example, group level. It also requires explication of conceptual relations between the central factors.

Although qualitative methods could be applied both for descriptions and explanations, they are mostly used for descriptions. Most of the studies also including a quantitative component aimed at explanations had a decent conceptual frame. It seems that the trend in using qualitative methods for descriptions has intensified the tendency of building loose conceptual frames and of focusing on individual behaviour ignoring social context. Could it be that this development reflects biased application of grounded theory approach? One of its basic premises is that researchers have to approach research object without presuppositions letting its conceptualizations steer the research process and interpretations (Strauss and Corbin 1998). Sometimes this may be understood as a call for not familiarizing oneself with earlier research findings. This may explain at least to a certain extent the loose framing of research.

## Towards future studies

It is difficult to forecast the future of our field of research. The trends we are able to distil from the earlier research may not necessarily continue in the future. Some of the trends I uncovered in the studies twelve years ago do not continue today. This does not encourage the making of future predictions. Some prospects, however, seem evident. If we do not connect our studies more strongly with the existing body of knowledge and show clearly the contributions they make, we cannot expect progress in our field. Almost by definition ties with the existing tradition is a necessary condition for progress. If we do not know the tradition, we do not know against what we rebel and how we can change it. Moreover, if we focus mainly on descriptive studies instead of explanatory ones, we cannot expect increasing understanding of information behaviour in relation to factors that generate it or in relation to its context. My guess is that if we continue the trends my analysis has uncovered, the significance of our field of research is likely to diminish.

Instead of additional predictions I will now describe what kind of studies I would like to see in our field:

*   more explanatory studies
*   case studies on mechanisms that connect information behaviour with the activities generating it and with the use of tools for supporting that behaviour;
*   inclusion of features of systems and services into research designs

Without an understanding of the associations between central factors that our research object consists of, we represent it only partially. If our aim is to study information needs, seeking and use in context, it is not enough that we categorize various types of information needs, seeking and use in addition of categorizing types of contexts or their elements. This naturally adds to our knowledge what kind of elements the phenomenon of interest consists of, but it does not tell us how these various elements are related. Categorizations are a natural point of departure for further research, but not the sole object of research. Studies explaining how information behaviour is related to varying actions and contexts generating it, or how the use of various tools or services is related to information behaviour are necessary and can build on the categorizations mentioned. Both types of studies are needed.

It is commonplace to state that information behaviour is rooted in the broader activities and interests of which it is part. This conceptualization has also gained footing in the studies. It is typical that the activities that generate information behaviour are studied as a whole. For example, studies on the information behaviour of professionals frequently conceptualize a profession as a black box. By this I mean that they do not analyse in more detail what kind of tasks the work of the profession consists of. They just study the information behaviour of that profession in general, their information needs, the types of channels and sources utilized. This applies mutatis mutandis also to every-day-life information seeking. These kinds of studies are not very fertile, because there is convincing evidence that the information needed varies according to the tasks and even according to the phase of task performance, as the consequent use of the information channels and sources do as well ([Byström 1999](#bystrom1999); [Garvey 1978](#garvey1978); [Kuhlthau 2004](#kuhlthau2004); [Vakkari 2003](#vakkari2003)). There are also frameworks that provide a sound point of departure for analysing professional work and interests in more detail and connecting them to information behaviour ([Byström 1999](#bystrom1999); [Ingwersen and Järvelin 2005](#ingwersen2005); [Kuhlthau 2004](#kuhlthau2004); [Leckie et al. 1996](#leckie1996); [Vakkari 2001](#vakkari2001); [Xie 2008](#xie2008)).

Thus, I would like to see more case studies of professionals or citizens taking their work or broader everyday activities as a point of departure. Work, or the broader everyday activity, should be differentiated into the major tasks it consists of. These tasks should be linked to the type of information needed and used for fulfilling them and to the information channels and sources used. Some studies should focus only on some of the major work tasks, analysing their performance process and how it is connected to information behaviour and use of tools for supporting task performance, information tools, in particular. These kinds of studies would provide us with a more specific account of the phenomena we are interested in. Opening the black box of work or everyday life would also generate progress in our field by introducing new concepts and categories, i.e., enriched theories calling for empirical testing.

In the foreword of the proceedings of the first ISIC Brenda Dervin, Reijo Savolainen and I argued that, without understanding human information behaviour, meaningful design of information systems or reasonable utilization of information in our society is not possible. However, our belief has not kindled much research in our community. Studies dealing with features of information systems or services are rare. However, there is a growing interest in relating the use of information systems to information behaviour and activities generating it in the field of information retrieval and digital libraries. I believe, that by introducing factors reflecting the features of information systems and services to our research designs, we could create results, which would also be of interest outside our field of research. They could also provide designers with information on how to improve systems and services to better match with human information behaviour and related tasks. It would increase the applicability of our research results.

As I mentioned earlier, predicting the future by projecting current trends is problematic. It is likely that the predictions will fail. In addition to some encouraging trends, I found some gloomier tendencies in our field of research. However, those tendencies are not determined. They can be changed like all human activities. The way we do research is not predestined. By changing some features of research practices we are able to break the existing trends. I believe, that our collective actions towards more coherent framing of research, towards more explanatory studies, which connect human activities with information behaviour and with information tools will brighten the future of our field.

## References

Byström, K. (1999). _Task complexity, information types and information sources. Examination of relationships._ Tampere, Finland: University of Tampere, Department of Information Studies. (Acta Universitatis Tamperensis 688). Doctoral dissertation.

Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 3-33.

Garvey, W.D. (1979). _Communication: the essence of science_. Oxford: Pergamon Press.

Ingwersen, P. & Järvelin, K. (2005). _The turn. integration of information seeking and retrieval in context_. Dordrecht, The Netherlands: Springer.

Järvelin, K. (2004). [Information seeking research needs extension toward tasks and technology](http://InformationR.net/ir/10-1/paper212.html). _Information Research_, **10**(1) paper 212\. Retrieved 8 November, 2008 from http://InformationR.net/ir/10-1/paper212.html. (Archived by WebCite® at http://www.webcitation.org/5cP19kBYv)

Kuhlthau, C. (2004). _Seeking meaning_. 2nd ed. Westport, CT: Libraries Unlimited

Kuhlthau, C. (2005). [Towards collaboration between information seeking and information retrieval](http://InformationR.net/ir/10-2/paper225.html). _Information Research_, **10**(2) paper 225 Retrieved 8 November, 2008 from http://InformationR.net/ir/10-2/paper225.html (Archived by WebCite® at http://www.webcitation.org/5cP1J15al)

Leckie, G., Pettigrew, K. & Sylvain, C. (1996). Modelling the information seeking of professionals. _Library Quarterly_, **66**(2): 161-193\.

McKehnie, L., Baker, L., Grenwood, M. & Julien, H. (2002). Research method trends in human information literature. _The New Review of Information Behaviour Research_, **3**, 113-126.

Strauss, A. & Corbin, J. (1998). _Basics of qualitative research_. 2<sup>nd</sup> ed. Thousand Oaks, CA: Sage Publications.

Vakkari, P. (1997), Information seeking in context: a challenging meta-theory. In: P. Vakkari, R. Savolainen & B. Dervin, (Eds.), _Information seeking in context: proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts_, (pp. 451-646). London & Los Angeles, CA: Taylor Graham. 451-646.

Vakkari, P. (1998). Growth of theories on information seeking. an analysis of growth of a theoretical research program on relation between task complexity and information seeking. _Information Processing & Management_ **34**(3/4): 361-382.

Vakkari, P. (2001). A theory of the task based information retrieval process: a summary and generalisation of a longitudinal study. _Journal of Documentation_, **57**(1), 44-60.

Vakkari, P. (2003). Task-based information searching. _Annual Review of Information Science and Technology_, **37**, 413-464.

Wagner, D.& Berger, J. (1985). Do sociological theories grow? _American Journal of Sociology_ **90**(4), 697-728.

Wilson, T.D. & Walsh, C. (1996). _[Information behaviour: an interdisciplinary perspective. A report to the British Library R & D department on a review of the literature](http://informationr.net/tdw/publ/infbehav/index.html)_. London: British Library Research Board. (British Library Research and Innovation Report 10) Retrieved 8 November, 2008 from http://informationr.net/tdw/publ/infbehav/index.html (Archived by WebCite® at http://www.webcitation.org/5cP1Omwgw)

Xie, I. (2008). _Interactive information retrieval in digital environments_. Hershey, PA: IGI Publishing.