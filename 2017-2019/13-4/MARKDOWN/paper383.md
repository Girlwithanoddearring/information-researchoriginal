####  vol. 13 no. 4, December, 2008

# A grounded theory study of the relationship between e-mail and burnout

#### [Marta Rocha Camargo](mailto:martacamargo04@yahoo.com)  
Faculdade Prudente de Moraes, Itu, São Paulo, Brazil
#### Abstract

> **Introduction**. This study consisted of a qualitative investigation into the role of e-mail in work-related burnout among high technology employees working full time and on-site for Internet, hardware, and software companies.  
> **Method**. Grounded theory methodology was used to provide a systemic approach in categorising, sorting, and analysing data gathered in semi-structured interviews with seventeen high technology workers.  
> **Analysis**. Data were analysed following the grounded theory principles: open coding and memos for conceptual labelling, axial coding and memos for category building, and selective coding for model building.  
> **Results**. The end result of the grounded theory process was a theoretical model showing that e-mail misuse and overuse in the context of a fluid, fast-paced, and constantly changing environment, such as the high technology industry, start a chain of events that contributes to prolonged work-related stress or burnout.  
> **Conclusion**. This study is intended to create awareness of the risks of e-mail becoming an obstacle to productivity and workers' well-being. This awareness allows organisations and individuals to engage in more constructive practices regarding e-mail to the benefit of their social actions and interactions both inside and outside their workplaces.

## Introduction

E-mail is a popular medium through which people communicate and share information. This is particularly true in the corporate world, where e-mail has been widely adopted and has even created some controversy. Proponents of e-mail use in corporations claim that e-mail improves employee productivity, increases collaboration, and expands access to information ([Whelan 2000](#whe00)). Critics of e-mail argue that it also has disadvantages, such as e-mail overload that negatively affects production ([Balter & Sidner 2002](#bal02); [Jackson _et al._ 2003](#jac03); [Kimble _et al._ 1998](#kim98); [Whittaker & Sidner 1996](#whit96)).

In a study on the effect of e-mail in the workplace, Ducheneaut and Bellotti ([2001](#duche01)) found that e-mail is where workers spend most of their time communicating with each other: receiving and sending out tasks, reading and storing documentation and making decisions. The authors also found that e-mail became the preferred way to communicate with co-workers in lieu of other methods of communication, such as telephone or face-to-face interaction.

By replacing other means of communication, e-mail introduced new challenges to the workplace such as managing the high volume of written messages. In that regard, Whittaker and Sidner ([1996](#whit96)) explained that when the role of e-mail was expanded from a communication tool to other functions such as task management and personal archiving, it increased the number of messages a person had to manage on a given day, creating problems such as cluttered inboxes and inadequate management of work information.

In a study with corporate e-mail users, Cavanagh ([2003](#cav03)) found that twenty-five e-mails a day is an accurate approximation of the amount of relevant and manageable content. However, participants of her study reported an average reception rate of forty-eight a day. In view of that, the author inferred that people receive more information than they need which requires them to work longer hours to read and process their e-mails. By the same token, Jackson and Culjak ([2006](#jack06))found that 13% of e-mails a person received on a given day were irrelevant to their job, while 16% contained unnecessary information and 41% were merely informational.

Seeley and Hargreaves ([2003](#see03)) found that, on average, twenty-five e-mails a day contained relevant messages; anything over that was more likely to consist of communication noise, rather than actual work. According to the authors, on average, most people receive approximately 40% to 50% more e-mail than they need to perform their jobs.

Based on the above discussions, e-mail is a presence in the corporate work environment whose influence on the workers needs to be investigated. The current study contributea to this research interest by taking a closer look at how e-mail is impacting upon people in one industry. For this study, the industry is high technology and the impact which will be examined is prolonged job-related stress or job burnout.

## Multiple perspectives on e-mail

The literature on e-mail is vast and diverse (e.g., [Barnes 2002](#bar02); [Miller 2001](#mil01); [Flynn & Kahn 2003](#flyn03); [Flynn & Flynn 2003](#fly03); [Lewis 2002](#lew02); [Kinnard 2002](#kin02)). It seems that almost every day, it is possible to find an article or study about e-mail.

Different areas of knowledge also seem to have different perspectives on e-mail. What follows is by no means an exhaustive discussion on the current literature on e-mail, but rather an illustration on the diverse views of this technology and its role in organisations and our society.

When e-mail is discussed with a focus on communication, it is described as a form of written communication that is similar to a memorandum. As such, to make the best use of this form of communication, users need specific writing rules and etiquette to be effective ([Miller 2001](#mil01)). Included among these rules are how-to lists that vary from guidelines for grammar and style ([Booher 2001](#boo01); [Smith 2002](#smi02)) to message samples for specific office situations ([Meyer _et al._ 2005](#meyer05)).

A management view of e-mail considers this mode of communication in relation to its effect on a business's bottom line ([Flynn & Kann 2003](#flyn03)). The main focus of this perspective is to point out that e-mail introduced a new dimension to the corporate world; not only in the way employees talk to each other, but also in the way they do business ([Chase & Trupp 2000](#cha00)).

Within the discipline of psychology, the effects of e-mail are usually discussed together with the effects of the Internet as a whole. In this sense, e-mail researchers explain e-mail in view of its interpersonal and intrapersonal effects on people ([Gackenbach 1998](#gack98)). The emphasis in this type of literature varies from discussing the interpersonal and intrapersonal effects of the Internet on people through compilations of meta-studies ([Jones 1998](#jon98)) to comparisons with similar effects of other such technologies as telephone and radio ([Joinson 2003](#joi03)).

When it comes to sociology, studies take many different routes. Similar to psychology, sociology connects any discussion of e-mail with the advent of the Internet. Studies can be so generic as to include a compilation of essays, as with Dutton _et al._ ([1999](#dut99)), or Tyler ([2002](#tyl02)); specific enough to describe a particular point of view, as with Ducheneaut ([2002](#duche02)); or broad enough to undertake a nation-wide survey on the social consequences of the Internet, as with Katz and Rice ([2002](#katz02)).

Katz and Rice ([2002](#katz02)) provided a meta-analysis based on a series of recent studies about the social aspects of the Internet. The author confirmed that although earlier studies reported a negative effect on social interactions, more recent studies showed that the Internet enhances and facilitates relationships. In this sense, in both social and professional settings, e-mail can help people feel more comfortable with one another before they engage in face-to-face interactions. However, as Tyler ([2002](#tyl02)) added, face-to-face relationships in the workplace still show the best results when it comes to closing important negotiations and dealing with sensitive matters. As a result, the study found that e-mail facilitates communication, but does not change or replace traditional methods in all communication situations.

Ducheneaut's ([2002](#duche02)) study, entitled _The social impacts of electronic mail in organizations_, is similar to Tyler's ([2002](#tyl02)) in that he suggested that e-mail did not change organizations. Rather, e-mail could be used to reinforce pre-existing social structures. This study was not conducted in a corporate setting, but rather in a university, and was further limited to the one-year e-mail archive of one professor. The ability of this study's findings to be replicated and validated is therefore compromised.

Finally, Katz and Rice ([2002](#katz02)) conducted a national random telephone survey from 1995 to 2000 on the impact of the Internet on society, from three perspectives:

*   demographic differences in access to and use of the Internet;
*   involvement with groups and communities through the Internet; and
*   use of the Internet in social interaction and expression.

Katz and Rice ([2002](#katz02)) contextualised e-mail as a means of communication and social interaction on the Internet. Although close to 10,000 people over a 5-year period were surveyed through telephone interviews, this study lacks specific contextualisation which affects its applicability to a corporate environment and its players.

One empirical study, by Sproull and Kiesler ([1986](#spro86)), stands out from the others in the existing literature for the purposes of this study. Although their study, on the theory of social cues, is over 20 years old, its analyses and findings are still relevant, as the authors researched a group of employees in their work environment. According to Sproull and Kiesler, senders and receivers of messages experience variables from their social context through perception, cognitive interpretation and communication-based behaviour. In that regard, the authors claimed that people use static and dynamic cues to communicate with each other. For the authors, static cues are those related to people's appearance and their artefacts, such as computers, desks and answering machines. Dynamic cues, on the other hand, are those nonverbal behaviours that change while people are engaged in communication, such as nodding or frowning.

Sproull and Kiesler ([1986](#spro86)) explained that as people perceive social cues, they create or extract cognitive interpretations and related emotional states. As a result, the authors pointed out that people adjust their gestures or tone of voice to respond to the cues coming from the person to whom they are talking. According to the authors, in contexts where social cues are clear and strong such as in face-to-face communication, people's behaviour becomes controlled, differentiated and focused on the other person. In contexts where cues are weak and sometimes ambiguous, such as in computer-mediated communication, people's behaviour reflects a sense of anonymity that leads to impulsive, self-centred and unregulated actions.

Before Sproull and Kiesler's ([1986](#spro86)) work, Daft and Lengel's ([1984](#daft84)) _media richness theory_ had already addressed the relationship between cues such as body language or tone of voice and their effect on the quality of communication. According to the authors, communication is richer or more effective in traditional ways such as face-to-face meetings that allow for instant feedback (synchronicity) and clarification of issues on the spot. In that sense, the authors claim that communication is less effective in written media such as memos or letters that do not allow for instant clarification of the meaning of the message.

When it comes specifically to e-mail, Sevinc and D'Ambra ([2004](#sev04)) posited that Daft and Lengel's theory did not account for computer-mediated communication such as e-mail. In a quantitative study with virtual teams, Sevinc and D'Ambra found that communication with a medium such as e-mail did not compromise the quality of the communication among the members, as they shared common interests and history regarding their work. The authors concluded that effective communication was more an issue of sharing the same social environment than in the communication medium itself.

Similarly, in a case study on e-mail involving a company in the risk management industry, Markus ([1994](#mar94)) found that some negative social effects were not connected to the characteristics of e-mail as an electronic technology. The conclusions of this study indicated that it was not possible to remove the negative effects of e-mail on social interactions, because of the unpredictability of e-mail use. In this respect, Markus ([1994](#mar94))suggested that e-mail etiquette, training and policies could alleviate, but not eradicate the problem, as users' intentions can be perceived differently, depending on the sender or recipient of a message.

The studies and views explored above are just a few examples of the multiple views on the uses and applications of e-mail in organizations and society as a whole. With such a vast array of possibilities in focus and theme surrounding e-mail, a fundamental question needs to be answered for the purpose of this study: Is e-mail a means of communication similar to telephone, or is it actually a new environment in which people do business and establish relationships? For the purpose of this study, e-mail, as a concept, was viewed as an application that allows people to interact with one another and conduct business. As such, e-mail is an integral part of the corporate world and, consequently, this study reflects the researcher's position that e-mail has a significant influence on employees' lives within this corporate context.

## Burnout

Burnout is a phenomenon studied under the discipline of psychology. As the intention of this study is to add value to the body of knowledge in the applied management and decision sciences, burnout is discussed in generic, as opposed to clinical terms.

According to Schaufeli _et al._ ([1993)](#scha93), the core of developing a better understanding of the concept of burnout lies in the prolonged nature of a stressful element in the workplace. The authors explained that burnout can be seen as a process that starts with prolonged levels of job tension that escalate to emotional exhaustion, depression and job dissatisfaction. This process culminates with employees feeling detached from their work environment, with subsequent effects on their mental and physical health.

Potter ([2005](#pot05)) explained that classic symptoms of burnout include feeling constantly tired or emotionally drained with work. The author posited that burned-out employees interact less with their co-workers and tend to withdraw from relationships in the job. Potter added that burned-out employees feel constantly frustrated and depressed with their job and work environment which leads to a decline in their job performance. The author also explained that after some time, the physical resilience of these employees diminishes and they start to develop health issues such as insomnia, colds, or gastrointestinal problems.

Maslach ([2003](#mar94)) explained that job settings create conditions for burnout when employees feel that their work and environment are out of control. The author posited that this leads to feelings of entrapment, since they cannot take breaks from work, nor influence their environment to improve it. The author also affirmed that when employees are in the process of getting burned out, they will trust their co-workers less and will be more prone to engage in situations of conflict. Maslach ([2003](#mar94))added that these conflicts lead to a breakdown in relationships, with people preferring to work in isolation.

From an organizational standpoint, Maslach ([2003](#mar94)) further explained that job settings enable a process of burnout when managers do not define clear roles and responsibilities for their employees or fail to have clear plans, policies and procedures that define work and performance goals and standards. Additionally, the author affirmed that one common source of work-related burnout is information overload.

## Study population

Although Creswell ([1998](#cres98)) suggested that twenty to thirty participants should be interviewed to achieve a sufficiently high level of detail to develop a theoretical model or theory, Strauss and Corbin ([1998](#stra98)) posited that there is no specific number a researcher should consider as a prerequisite to grounded theory. They further explained that instead of an interview quota, the most important aspect of sampling in grounded theory is to include participants that represent different perspectives regarding the phenomenon; in this case, the use of e-mail as an application that allows people to interact in the workplace, with the aim of conducting business.

For this study, twenty-five workers in high technology companies in the area known as Silicon Valley, a region between San Jose and Paolo Alto in the state of California, United States of America, were invited to participate, but only seventeen accepted the invitation. Of these seventeen, ten had technical roles (e.g., programmer, technical writer or technical support engineer) and seven represented management roles (e.g., functional or project managers). For each interview, the researcher assigned an interview code formed by a letter _I_ (which stands for _interview_), as well as the number representative of the order in which the interview took place (e.g., I-01 for the first interview, I-02 for the second, and so forth).

## Study methodology: grounded theory

According to Glenn ([2003](#glen03)), the high technology environment is constantly changing as new technologies are introduced to the market and new teams and projects are immediately organized to design and deliver products or services to support those technologies. Therefore, grounded theory was chosen to study the relationship between e-mail and burnout, because it allows a researcher to explore a phenomenon and build theory or theoretical models for contexts going through change processes and transitions ([Creswell, 1998](#cres98)) such as the high technology work environment.

For the sake of consistency and objectivity, this study followed the systematic approach developed by Strauss and Corbin ([1998](#stra98)). According to this methodology, an interview protocol was prepared and used in interviews with high technology workers. The interview questions were open and designed to gather information regarding the interviewees' views on the advantages and disadvantages of e-mail in their workplace, their habits regarding their use of e-mail both inside and outside their organization and the volume and type of messages they received and sent on a given day.

Literature was useful as a source of initial ideas regarding common issues associated with the phenomenon being studied. However, as required by the grounded theory approach used in this study, the researcher was careful not to be contaminated and excessively influenced by previous studies. Scholarly, technical and non-technical literature was also used only for the purpose of comparisons and as sources of ideas.

The researcher created a log with the interview transcripts and started the grounded theory process with a line-by-line analysis to extract concepts behind what people were saying. This first analysis is called _open coding_.

From a list of concepts generated in the open coding, the researcher looked for connections between them in order to group similar items that shared common properties or characteristics. This analysis is called _axial coding_ and it allowed the researcher to create initial categories, which are concepts that stand for phenomena. The open and axial coding is illustrated in Table 1 below.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Open and Axial Coding**  
</caption>

<tbody>

<tr>

<th valign="middle">Question</th>

<th valign="middle">Interviewee Code</th>

<th valign="middle">Transcript Excerpt</th>

<th valign="middle">Open Coding</th>

<th valign="middle">Axial Coding</th>

</tr>

<tr>

<td rowspan="2">In your opinion, what are the advantages of using e-mail in your workplace?</td>

<td colspan="1">I-01</td>

<td>Written communication can be tracked, filed and used to keep someone to their word. This is especially useful for working on large projects with huge unwieldy amounts of data that needs to be tracked.</td>

<td>Information tracking, documentation history, project management, data management</td>

<td rowspan="2">Different applications and uses of e-mail</td>

</tr>

<tr>

<td>I-04</td>

<td>a. You can keep records of things to go back when searching for project history  
b. It's a fast way to communicate with multiple users at the same time  
c. It helps me as an archive - a place I can go to find information about what happened with old projects, how issues were resolved and who worked on those projects.  
d. It helps me as a to-do list.</td>

<td>record keeping, message distribution tool, archiving, task list</td>

</tr>

</tbody>

</table>

Each coding phase was supported by memos that are basically _notes to self_. These memos are important to document ideas and connections as they occurred in the analysis process. An example of a memo is below:

> This interviewee shares the same view as I-02, in that e-mail is essential for documenting and tracking everything that happens in the workplace. **(I-04)**

After developing categories in the axial coding phase, it was possible to look at the relational connections between these categories in view of the paradigm of this study. This process is called _selective coding_, which allows for the creation of the main categories of the theoretical model. For example, from the axial coding 'Different applications and uses of e-mail' shown in Table 1 above, a main category emerged: 'Different perceptions on e-mail role in the workplace'.

Although small, the sample of seventeen high technology workers proved to be sufficient for this study as the researcher reached category saturation at interviewee number 12\. In grounded theory, category saturation is when no new information can be found in the data provided by the participants.

The final requirement of the grounded theory approach involved a definition of a process to explain the actions and interactions among the elements of a theoretical model. This process and the theoretical model is explained in the next section.

## Theoretical model

It is important to note here that one of the basic assumptions of the grounded theory methodology is that it does not give rise to findings, but rather provides a tentative explanation of the studied phenomenon through the creation of a theory or theoretical model ([Strauss & Corbin 1998:25](#stra98)). The theoretical model that resulted from this study recognises that the evolution of a social phenomenon requires constant verification and updating as the context evolves and changes with its subjects.

After careful consideration, the central category that emerged and became the title of the theoretical model was: _Misuse and overuse of e-mail as precursors to job burnout_, as shown in Figure 1 below.

<div align="center">

<table width="60%" border="0" cellspacing="2" cellpadding="2" style="font-size: large; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">

<tbody>

<tr valign="top">

<td>Different perceptions of e-mail's role in the workplace</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>E-mail-centric job setting</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>Breakdown in relationships and trust</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>Vicious cycle of communication (high quantity with low quality e-mails)</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>24/7 connectivity and availability</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>Effect on people's personal time</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>Demographics</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>Stress</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>Time</td>

</tr>

<tr valign="top">

<td style="font-size: large; font-family: 'Wingdings 3';">s</td>

</tr>

<tr valign="top">

<td>Burnout</td>

</tr>

</tbody>

</table>

</div>

<div align="center">  
**Figure 1: Misuse and overuse of e-mail as precursors to job burnout**</div>

The theoretical model starts the explanation of the relationship between e-mail and burnout by showing how e-mail is perceived within the high technology industry. Data from the interviewees show that people have different interpretations of what e-mail represents in their environments and the best way to use it for communication and productivity purposes. As emerged during the open coding processes, people use e-mail as they see fit or as appropriate to meet their goals, such as for documenting, tracking, record keeping, information sharing, distribution lists, broadcasting, project management, data management, task management, archiving, data transfer, file swapping, troubleshooting and reminders. The interview excerpt below illustrates this point.

> You can keep records of things to go back [to] when searching for project history. It's a fast way to communicate with multiple users at the same time. It helps me as an archive; a place I can go to find information about what happened with old projects, how issues were resolved and who worked on those projects. It helps me as a to-do list. **(I-04)**

One problem emerges, however. What is a benefit to one person might not be a benefit for another. The interview excerpt below illustrates this point.

> I spend too much time typing/replying to people who work twenty feet from me. People don't ask me in person; they have to create a _formal_ document when they send an e-mail for fear of having it used against them later (negative accountability). I also do not like when people transfer ownership to me by replying to my e-mail without actually replying and making me figure out. **(I-09)**

Uncertain of what to do to perform their jobs and interact with their co-workers, employees search for signals from their work environment, only to find a job setting in constant change. When people are unsure of what is happening around them in terms of roles and responsibilities, they will often use e-mail to communicate with one another and to keep track of what is happening on a daily basis. The interview excerpt below illustrates this point.

> I prefer to document everything that happens in the workplace to 'cover my butt' if something goes wrong. Sometimes I use phone calls and then send an e-mail to confirm what was discussed. It is also a good tool to keep a history of everything that happens in each project. **(I-07)**

There is also a perceived need to broadcast messages to a larger audience, precisely because people are not sure who is doing what, when or how. By using distribution lists, workers not only eventually get an answer from someone, but they also make sure that whatever they discover is shared with others. The interview excerpt below illustrates this point.

> I would like to see logical evaluations to determine if the topic needs to be communicated in e-mail and who really needs to get the messages. One third of my e-mail is based on a distribution list or the _reply to all_ function of a _one-shot_ approach to communications. **(I-13)**

The only common thread that keeps people together in such a work environment is the fact that everything revolves around e-mail. However, as communication needs to happen both quickly and in writing, there is a dependence on e-mail in getting anything done. As a result, the work environment becomes e-mail-centric.

> People often rely on e-mail to communicate, rather than on in-person discussions or phone calls. We [have] created too much of a dependence [on] e-mail to do business. Without e-mail, we can't work. **(I-07)**

At this point in the process, e-mail starts to get misused and overused while breaking down relationships in the workplace. People are constantly documenting what each other is saying; not only as evidence of roles, responsibilities and decision-making, but also as protection against accusations of mistakes or miscommunication. However, instead of solving problems, this constant documentation of events adds more conflicts, misunderstandings and confusion, because reality becomes dependent on people's writing skills. As not everyone can write well, the dependence becomes a vicious cycle that can lead, for example, to more e-mails being generated to request clarification on unclear messages. A vicious cycle starts with a high volume of e-mail with low-quality content.

> Someone who lacks writing skills can easily misrepresent their tone or message, thereby accidentally offending or confusing others. **(I-01)**  
>   
> I currently have about 1,458 e-mails in my mailbox that I need to sort and archive in folders. Some of them I read, others I just left them there to check later. As I'm busy answering new e-mails coming every day, I might just delete most of them without spending any more time with them. The interesting thing is that I get about four messages a day saying my mailbox is too full. **(I-03)**  
>   
> If someone misreads the e-mail (or only reads a portion) it can cause a lot of work to undo the miscommunication. Also, if someone is behind in responding to their e-mail, even if it's just a short yes/no response [that is] needed, it can cause a backup in the workflow. **(I-05)**

This vicious cycle of high quantity and low-quality messages makes high technology employees feel they are never done with their work, as e-mails keep coming. The incessant nature of e-mail gives people the impression that they need to be on the lookout at all times, as a major crisis could emerge at any minute and jeopardize their accomplishments if not resolved immediately. In an e-mail-centric environment, chances are that emergencies will be communicated by e-mail, rather than by phone or face-to-face.

What happens next is that employees become constantly worried about missing _that important message_ if they go home or spend weekends with unopened messages in their inbox. This leads to a sense of 24/7 connectivity and availability, to make sure the work gets done. The interview excerpt below illustrates this point.

> I check my e-mail before going to bed and before coming to work. I'm afraid there might be fires happening without me being aware of [them] and in many cases, I can defuse [a] situation before it becomes a big crisis. It would take more time and effort [for] me to do damage control generated by e-mails, especially when sent to different people who will have different perceptions of what's going on, so, I'm always on the look-out to clarify or solve an issue as soon as it appears in my e-mail, to avoid spending much more time later, once it's widespread. I also do this when I'm on vacation. Nobody [has] officially asked me to do this; I just do [it] to avoid more headaches later. **(I-03)**

At this point in the model, it is possible to notice that the lines between business hours and personal hours are not as clear as in the past. With e-mail, people can work from home or from remote sites; consequently, organizational pressures and ambiguities follow workers wherever they go. Therefore, there is an effect on people's personal time, as they feel they have to check their work-related e-mails to make sure they are not missing an important message or emergency.

While some people see trade-offs and benefits from the flexibility of working remotely, others see it as a burden and source of stress. For example, married people reported that although working with e-mail from home can be stressful at times, it gives them the flexibility of managing their time. Single people reported a different view, as they would much rather pursue other activities in their time away from work than checking their e-mail. Some differences on other demographics such as age and time with the company also emerged in the analysis and coding processes. Theinterview excerpts below illustrate this point.

> I don't check e-mail at home unless there is an urgent issue that I'm waiting for a response to, or if there's something I didn't finish during the day that needs to be communicated by the following morning. I have to say I don't mind reading my work e-mails from home. For me it's more important to have the flexibility of knowing I can catch up with work when I get home than staying overtime and not being able to pick up my son [from] school. As I mentioned earlier, it's a trade-off and for me, it's working well as I can plan to leave every day at 5 PM and work from home after then, if necessary. **(I-08 (Marital status: married))**  
>   
> I have to check e-mail over the weekends because of the nature of my company and projects. We are a 24/7 operation, so it is expected from the employees to remain connected 24/7 as well... I don't like this, as I'm single and I would like to have a life! I miss personal appointments sometimes because I have to stay late at work and deal with my e-mails, or stay at home during weekends instead of going out with friends because I need to respond to messages from people working on the weekend shifts or overseas. So, I can never plan ahead and I am never disconnected from work. I sometimes feel depressed on Sunday night, knowing a weekend had gone by and I could not do all the activities I wanted to and that on Monday it will start all over again. It never stops. **(I-17 (Marital status: single))**

At this point in the theoretical model, it is possible to see that certain elements and conditions that lead to burnout such as information overload, breakdown in relationships, lack of clear policies and guidelines, frustration and continuous stress are present and connected to e-mail. Therefore, with time and for some demographics, one can infer that burnout can be an outcome of an inadequate and excessive use of work-related e-mail.

## Discussion

E-mail cannot be blamed for all the stressful situations that take place in a high technology workplace. Likewise, the issues covered in this study are not unique to the technology industry environment. What is clear, however, is that as most communication and work tasks in this industry happen through e-mail, employees do not have the choice of not using e-mail to do their work. This situation is exacerbated due to the implicit or explicit expectation that employees will be available to respond to e-mails 24 hours a day, 7 days a week.

It was interesting to observe that most participants in this study expressed a sense of the dual nature of the worth of e-mail where advantages can become disadvantages, depending on how it is used by the sender and how it is interpreted by the recipient of messages. Therefore, this study showed that e-mail as an application or technology is neither good nor bad; it is how people use it that makes it a value-added feature or burden in the workplace.

When it comes to specific elements of e-mail that lead to prolonged job-related stress, data confirmed that certain aspects of this stress were indeed connected to the way people use e-mail. For instance, interviewees seemed to be more affected by e-mail volume when the messages did not add value to their work or did not require any action on their part. This is particularly true when people use mailing lists without proper criteria on the need or desire of the recipients to be part of such lists.

Analyses through the grounded theory process also showed that certain demographic factors might play a role on the levels of stress resulting from e-mail use. These demographic factors should be the focus of a separate quantitative study for a more detailed look at specific variables.

Two interviewees mentioned addiction as a concept related to the habit of constantly checking e-mail. Although this item showed some similarities with the concepts that emerged during the open coding process (e.g., 24/7 availability and connectivity), addiction represents a discrete psychological phenomenon that should also be addressed in a separate study.

Some interviewees reported problems with understanding messages from people who speak English as a second language who either worked at their own site or at their company's international offices. Therefore, it could be suggested that the more global a company, the more quality issues related to e-mail those companies are likely to have. Similarly to demographics and addiction, this variation could become a topic of a separate study which could investigate specific aspects of globalisation or diversity in the workplace that influence the way people work and use e-mail.

Additional research opportunities include the following:

*   A study with a larger sample of high technology workers of companies in other areas of the United States or in other countries that have high technology centres similar to Silicon Valley, California (e.g., China or India), comparing the results with those of this study.

*   A study on the financial aspect of e-mail overuse and misuse. One has to wonder if all this unnecessary volume of messages is not affecting bottom lines with costs related to the maintenance of servers, software upgrades, or even the time employees spend managing and cleaning their in-boxes.

### Conclusion and recommendations

The misuse and overuse of e-mail affects people inside and outside their organizations. With this in mind, it is interesting to note that suggestions and recommendations for an effective use of e-mail in the workplace have been available for a while in books and literature on modern office communication (e.g., [Whelan 2000](#whe00); [Booher 2001](#boo01); [Brounstein 2001](#brou01); [Chase and Trupp 2000](#cha00)). Technical approaches have also been available to avoid message overload. These approaches include solutions such as filters and rules that can be predefined by the users according to their informational needs to reduce the volume of e-mail one has to read on a given day ([Bälter and Sidner 2002](#bal02)).

Therefore, the problem of e-mail leading to stress does not seem to be a matter of finding solutions to the inadequate use of e-mail within organizations, but rather of finding creative ways to get people to accept these solutions to their own benefit. For that purpose, leaders and managers of high technology companies need to work as a team with their employees to deliver solutions that can be embraced by all workers. For example, instead of creating a detailed, word-heavy document with a strong focus on policies and procedures to improve e-mail writing skills and sending it to everybody as an attachment to an e-mail (of all things), management teams could instead sponsor a more informal approach to explaining the benefits of an effective use of e-mail in departmental personnel or staff meetings. It is also important to include resources such as fun videos or presentations which are more compatible with the casual organizational culture that predominates in this type of industry ([Glenn 2003](#glen03)).

With regard to burnout, solutions and recommendations are less tangible and, therefore, more difficult to develop and implement. According to Maslach ([2003](#mas03)), burnout is often not detected until it is too late. The author also pointed out that burnout is a process: it takes time to develop and it takes time to recover from it.

As a starting point, Maslach ([2003](#mas03)) suggested that managers should be familiar with the signs of burnout and be on the lookout for its early symptoms in their employees. She also pointed out that burnout is typically detected by someone else and not the person suffering from it. The author explained that managers should try to notice subtle changes in their employees' attitudes toward others, their mood, and their general behaviour on the job. For example, a possible hint could be an employee who used to engage in constant face-to-face communication with his/her co-workers and managers, who changes his behaviour and practices to mostly written communication by e-mail (to avoid contact with others).

Getting companies to be proactive on this issue of employee burnout is difficult, because burnout might not be something that companies look forward to addressing. According to Maslach and Leiter ([1997](#mas97)), most organizations see burnout as a sensitive matter with which they prefer not to deal, as it usually involves employees' complaints about workload or requests for more time off from work.

Ultimately, the problem is that burnout requires the support of higher levels in the organization; even when people try to change their behaviour toward work, they need support from the social environment of the workplace to make those changes permanent. Therefore, as with the recommendations for minimizing the adverse issues associated with e-mail, any solution to burnout in the workplace requires the involvement of all people at all levels of an organization. Based on Maslach and Leiter's ([1997](#mas97)) recommendations to minimize burnout, this researcher suggests the following steps when trying to reduce the levels of stress associated with e-mail:

*   Set realistic goals for specific work tasks for a given day, week, or month. For example, set a reasonable quota for how many e-mails should be read and sent in a day.
*   Develop e-mail rules and filters to sort important messages from spam messages.
*   Encourage diverse communication patterns within organizations. For example, if the first impulse or habit is to send an e-mail to someone, see if e-mail is indeed the best medium for that type of communication. The same applies for distribution lists; check if all people on those lists really need all the messages you are sending and let the owners of distribution lists know when you do not need their messages.
*   Try to keep work and home separate. For example, take a critical look at the task priorities and see if checking e-mail from home over weekends or vacation is actually critical, or merely a habit.

In conclusion, this study has attempted to provide organizations and individuals with specific insights into the relationship between e-mail and prolonged job stress or burnout. As Branden ([1997: 69](#bran97)) pointed out, '_the first step toward change is awareness_'. Therefore, by focusing analyses on specific factors that influence this relationship, for better or worse, this researcher hopes this study can, at a minimum, create awareness in the high technology community that there might be better ways to work with e-mail, to the benefit of the entire industry and its workers.

## Acknowledgements

I would like to express my most sincere appreciation and gratitude to the high technology workers who participated in this study and found time in their busy schedules to share their views with me.

## References

*   <a id="bal02" name="bal02"></a>Bälter O. & Sidner C.L. (2002). [Bifrost inbox organizer: Giving users control over their inbox.](http://www.webcitation.org/5ckWhO8sG) Retrieved December 27, 2007, from http://bit.ly/2sM (Archived by WebCite® at http://www.webcitation.org/5ckWhO8sG)
*   <a id="bar02" name="bar02"></a>Barnes, S.B. (2002). _Computer-mediated communication: human-to-human communication across the Internet._ Boston, MA: Allyn & Bacon.
*   <a id="boo01" name="boo01"></a>Booher, D. (2001). _E-writing: 21st century tools for effective communication._ New York, NY: Pocket Books.
*   <a id="bran97" name="bran97"></a>Branden, N. (1997). _The art of living consciously: the power of awareness to transform everyday life_ New York, NY: Fireside.
*   <a id="brou01" name="brou01"></a>Brounstein, M. (2001). _Communicating effectively for dummies._ Indianapolis, IN: Willey Publications.
*   <a id="cav03" name="cav03"></a>Cavanagh, C. (2003). _Managing your e-mail. Thinking outside the inbox_. Hoboken, NJ: John Wiley & Sons.
*   <a id="cha00" name="cha00"></a>Chase, M. & Trupp, S. (2000). _Office e-mails that really click_. Newport, RI: Aegis Publishing Group.
*   <a id="cres98" name="cres98"></a>Creswell, J. W. (1998). _Qualitative inquiry and research: choosing among five traditions_ Thousand Oaks, CA: Sage Publications.
*   <a id="daft84" name="daft84"></a>Daft, R.L. & Lengel, R.H. (1984). Information richness: a new approach to managerial behavior and organizational design. _Research in organizational behavior_, **6**, 191-233.
*   <a id="duche02" name="duche02"></a>Ducheneaut, N.B. (2002). [The social impacts of electronic mail in organizations](http://www.webcitation.org/5ckXWc2Qr) . _Information, Communication & Society_, **5**(2), 153-188\. Retrieved June 25, 2004, from http://www.parc.xerox.com/research/publications/files/5349.pdf (Archived by WebCite® at http://www.webcitation.org/5ckXWc2Qr)
*   <a id="duche2001" name="duche01"></a>Ducheneaut, N.B. & Bellotti, V. (2001). [E-mail as habitat. An exploration of embedded personal information management](http://www.webcitation.org/5ckXkV6Ae). _ACM Interactions_, **8**(3), 30-38\. Retrieved July 20, 2004, from http://www.parc.com/research/publications/files/4360.pdf (Archived by WebCite® at http://www.webcitation.org/5ckXkV6Ae)
*   <a id="dut99" name="dut99"></a>Dutton, W.H., Peltu M. & Bruce, M. (1999). _Society on the line: information politics in the digital age._ New York, NY: Oxford University Press.
*   <a id="fly03" name="fly03"></a>Flynn, N. & Flynn, T. (2003). _Writing effective e-mail: improving your electronic communication_. (3<sup>rd</sup> ed.). Boston, MA: Course Technology.
*   <a id="flyn03" name="flyn03"></a>Flynn, N. & Kahn R. (2003). _E-mail rules: a business guide to managing policies, security and legal issues for e-mail and digital communication_. New York, NY: Amacom.
*   <a id="gack98" name="gack98"></a>Gackenbach, J. (1998). _Psychology and the Internet: intrapersonal, interpersonal and transpersonal implications_. San Diego, CA: Academic Press.
*   <a id="glen03" name="glen03"></a>Glen, P. (2003). _Leading geeks: how to manage and lead people who delivery technology_. San Francisco, CA: Jossey-Bass.
*   <a id="jac03" name="jac03"></a>Jackson T.W., Dawson R.J. & Wilson D. (2003). Understanding e-mail interaction increases organizational productivity. _Communications of the ACM_, **46**(8), 80-84.
*   <a id="jack06" name="jack06"></a>Jackson, T.W. & Culjak, G. (2006). [Can seminar and computer-based training improve the effectiveness of electronic mail communication within the workplace?](http://www.webcitation.org/5ckYnHYjD). In S. Spencer & A. Jenkins, (Eds.). Proceedings of the 17th Australasian Conference on Information Systems, Adelaide, Australia. North Sydney, NSW, Australia: Australasian Association for Information Systems. Retrieved December 27, 2007, from http://bit.ly/Mwaz (Archived by WebCite® at http://www.webcitation.org/5ckYnHYjD)
*   <a id="joi03" name="joi03"></a>Joinson, A.N. (2003). _Understanding the psychology of Internet behaviour: virtual worlds, real lives_. Basingstoke, UK: Palgrave MacMillan.
*   <a id="jon98" name="jon98"></a>Jones, S. (1998). _Cybersociety 2.0: Revisiting computer-mediated community and technology (new media cultures)._ Thousand Oaks, CA: Sage Publications.
*   <a id="katz02" name="katz02"></a>Katz, J.E. & Rice R.E. (2002). _Consequences of Internet use: access, involvement and interaction_. Cambridge, MA: MIT Press.
*   <a id="ker82" name="ker82"></a>Kerr, E. & Hiltz, S.R. (1982). _Computer-mediated communication systems: status and evaluation._ New York, NY: Academic Press.
*   <a id="kim98" name="kim98"></a>Kimble C., Hildreth P. & Grimsgaw D. (1998). [The role of contextual clues in the creation of information overload, matching technology with organizational needs](http://www.webcitation.org/5ckaNsA0Y). In _Matching Technology with Organisational Needs. Proceedings of the third UKAIS Conference_, (pp. 405-412). London: McGraw-Hill Publishing. Retrieved December 15, 2007, from http://www-users.cs.york.ac.uk/~kimble/research/PAPER200.pdf. (Archived by WebCite® at http://www.webcitation.org/5ckaNsA0Y)
*   <a id="kin02" name="kin02"></a>Kinnard, S.( 2002 ). _Marketing with e-mail: a spam-free guide to increasing sales, building loyalty, and increasing awareness_. Gulf Breeze, FL: Maximum Press.
*   <a id="lew02" name="lew02"></a>Lewis, H.G. (2002). _Effective e-mail marketing: the complete guide to creating successful campaigns._ New York, NY: Amacon.
*   <a id="mar94" name="mar94"></a>Markus, M. L. (1994). Finding a happy medium: explaining the negative effects of electronic communication on social life at work. _ACM Transactions on Information Systems_, **12**(2), 119-149\.
*   <a id="mas97" name="mas97"></a>Maslach, C., & Leiter M. R. (1997). _The truth about burnout._ San Francisco, CA: Jossey-Bass.
*   <a id="mas03" name="mas03"></a>Maslach, C. (2003). _The cost of caring_. Cambridge, MA: Malor.
*   <a id="meyer05" name="meyer05"></a>Meyer V., Sebranek P., & Rys, J.V. (2005). _Writing effective e-mails: practical strategies for strengthening electronic communication_. Burlington, WI: UpWrite Press.
*   <a id="mey86" name="mey86"></a>Meyrowitz, J. (1986). _No sense of place: the impact of electronic media on social behavior._ New York, NY: Oxford University Press.
*   <a id="mil01" name="mil01"></a>Miller, S. (2001). _E-mail etiquette: do's, don'ts and disaster tales from People Magazine's Internet manners expert_. New York, NY: Warner Books.
*   <a id="pot05" name="pot05"></a>Potter, B. (2005) _Overcoming burnout. How to renew enthusiasm for work_. Berkeley, CA: Ronin Publishing.
*   <a id="see03" name="see03"></a>Seeley, M. E., & Hargreaves, G. N. (2003). _Managing in the e-mail office_. Burlington, MA: Butterworth-Heinemann.
*   <a id="sev04" name="sev04"></a>Sevinc, I. & D'Ambra, J. G. (2004). _[Extending media richness theory: the influence of a shared social construction](http://www.webcitation.org/5ckbA9YdT)._ In T. Leino, T. Saarinen & S. Klein, (Eds.) _Proceedings of the Twelfth European Conference on Information Systems, Turku School of Economics and Business Administration, Turku, Finland_. (pp. 1767-1779). Retrieved November 20, 2006, from http://is2.lse.ac.uk/asp/aspecis/20040155.pdf (Archived by WebCite® at http://www.webcitation.org/5ckbA9YdT).
*   <a id="scha93" name="scha93"></a>Schaufeli, W. B., Maslach C., & Marek, T. (1993). _Professional burnout. Recent developments in theory and research._ Philadelphia, PA: Taylor & Francis.
*   <a id="smi02" name="smi02"></a>Smith, L.A. (2002). _Business e-mail: how to make it professional and effective_. San Anselmo, CA: Writing and Editing.
*   <a id="spro86" name="spro86"></a>Sproull, L., & Kiesler S. (1986). Reducing social context cues: electronic mail in organizational communication. _Management Science_, **32**(11), 1492-1512.
*   <a id="stra98" name="stra98"></a>Strauss A., & Corbin, J. (1998). _Basics of qualitative research: techniques and procedures for developing grounded theory_. (2nd ed.). Thousand Oaks, CA: Sage Publications.
*   <a id="sum88" name="sum88"></a>Sumner, M. (1988). The impact of electronic mail on managerial and organizational communications. _ACM SIGOIS Bulletin_, **9**(2-3), 96-109.
*   <a id="tyl02" name="tyl02"></a>Tyler, T. R. (2002). Is the Internet changing social life? It seems the more things change, the more they stay the same. _Journal of Social Issues_, **58**(1), 195-202.
*   <a id="whe00" name="whe00"></a>Whelan, J. (2000). _E-mail @ work_. London, UK: Financial Times Prentice Hall.
*   <a id="whit96" name="whit96"></a>Whittaker S. & Sidner C. (1996). E-mail overload: exploring personal information management of e-mail. In S. Kiesler, (Ed.) _Culture of the Internet_. (pp. 276-295). Mahwah, NJ: Lawrence Erlbaum Associates.

