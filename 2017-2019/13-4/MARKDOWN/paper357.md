#### vol. 13 no. 4, December, 2008

#### [Noa Fink- Shamit](mailto:noaf@savion.huji.ac.il) and [Judit Bar-Ilan](mailto:barilaj@mail.biu.ac.il)

Department of Information Science, Bar-Ilan University, Ramat-Gan, 52900, Israel

#### Abstract

> **Introduction.** The quality of information on the Web is highly variable, thus users should assess the quality of the information by themselves. Our aim was to elucidate the existence of such quality assessment by studying users carrying out specific tasks on the Web.  
> **Method.** Seventy-seven individuals were observed performing three tasks, were then interviewed and also completed questionnaires.  
> **Analysis.** Content analysis and descriptive statistics were used to identify quality assessment components and their attributes. The number of times an attribute was mentioned by the participants was monitored and counted.  
> **Results.** Information quality assessment is composed of four components: credibility of content, credibility of site, predictive relevance and veracity assessment. Each component consists of several attributes. The extent of attribute use, and by extension of component use, represents an actual behaviour.  
> **Conclusions.** Information quality assessment is an integral part of Web search behaviour, as all participants evaluated the information and performed a basic assessment using the authority attributes, allowing us to define these attributes as the core elements for the assessment of quality.

## Introduction

Modern society is an information consuming society and as such the question of information quality is of central importance. Many users consider the Web to be a virtual library and, therefore, when an information need arises they use the Web by default ([Scholz-Crane 1998](#Sch98); [Grimes and Boening 2001](#Gri01)). These users fail to appreciate that the inherent structure of the Web has created a new environment ([Burbules 2001](#Bur01)); that is, they do not completely internalize the difference between the publication process of printed articles and those published on the Internet. In the conventional publishing process the information not only goes through quality assessment, but it is also subject to a publication policy whereas, over the Internet, the publishing process allows almost anyone to quickly and easily publish his or her opinions. Therefore, the information retrieved does not always originate from recognized or qualified sources and possesses varying information qualities ([Rieh and Danielson 2007](#Rieh07); [Metzger 2007](#Met07)) compelling the user to perform an independent assessment of information each time s/he searches for information on the Internet.

Quality is an elusive concept; its definition is situational and has a dual character. It is of a transcendent quality (essence) synonymous with excellence, "an essence that is neither mind nor matter, but a third entity independent of the two ".. and "[e]ven though quality cannot be defined, you know what it is."([Pirsig, 1974: 185, 213](#Pir74)), while, at the same time quality is a precise and appraised quality that may be looked upon as a product ([Garvin 1988: 40-46](#Gar88)).

In the context of this paper the quality of information is solely dependent on whether the information is true, or in other words, whether quality equals veracity. As truth can be subjective ([Jacoby 2002](#Jac02)), here information can be accepted as true only when it conforms to an accepted standard or a pattern ([Merriam-Webster Online Dictionary 2007](#Tru07)), defined by the scientific community or by the searcher's community at large. As the appraisal of facts and determining what is true are based on personal prior knowledge, a state that is seldom present ([Kahneman and Tversky 1982](#Kah82)); therefore to assess the quality of the information, one has to rely on a surrogate mechanism, namely that of credibility. Credibility is "the quality or power of inspiring belief" ([Merriam-Webster Online Dictionary 2007](#Cre)) and therefore conveys the impression that it is more a perception ([Fogg _et al._ 2001](#Fogg01); [Freeman and Spyridakis 2004](#Fre04); [Liu 2004](#Liu04)) than an actual appraisal of facts. By contrast, in the context of the current study, credibility is not a perception nor it is a criterion for relevance judgment ([Barry 1994](#bar94); [Bateman 1999](#Bat99)), it is a direct measure of actual quality ([Garvin 1988](#Gar88) : 40-46), re-created each time the question of truth arises. Hence, assigning credibility to information is not the end; rather, it is the means of accepting the veracity of information.

Patrick Wilson's ([1983](#Wil83)) epistemic authority is another theoretical concept that is closely linked to credibility, which was discussed by both Rieh ([2002](#Rieh02)) and Savolainen ([2007](#Sav07)) in the context of Web searching. Cognitive authority is a quantitative concept that consists of two components, competence and trustworthiness, and is viewed as a relationship between two entities, where there has to be an acknowledgement of the authority of one entity over the other. Acceptance of the information conveyed by the authoritative party implies that cognitive authority can be found in various sources and not only in individuals. It isimportant, however, to note that one can be an expert without being regarded as an authority or be regarded as authority in one area, but not is another area ([Wilson 1983: 13-15](#Wil83)).

The concept of credibility was discussed in two extensive literature reviews ([Metzger _et al._ 2003](#Met03); [Rieh and Danielson 2007](#Rieh07)). Based on surveyed populations, several studies tried to answer the questions how people evaluate Websites and their content and, if they do, what evaluation criteria are employed, how and to what extent these criteria are used ([Flanagin and Metzger 2000](#Fla00); [Liu 2004](#Liu04); [Liu and Huang 2005](#Liu05); [Fogg _et al._ 2001](#Fogg01); [Barnes _et al._ 2003](#Bar03)).It seems that, when assessing credibility, attributes that pertain to the site are used more readily ([Fogg _et al._ 2003](#Fogg03)) than attributes relating to content, implying that sites whose looks are more appealing are perceived as holding more credible content ([Robins and Holmes 2008](#Rob08)). However, there exist other studies ([Rieh 2002](#Rie02); [Stanford _et al._ 2002](#Sta02); [Eysenbach and Köhler 2002](#Eys02)), which consider attributes like author and source as key elements for evaluating the quality information, thus disassociating themselves from appearance and converging to the conventional pattern of assessing information.

People regard the Web as an important source of information and turn to it by default ([Grimes and Boening 2001](#Gri01)). Although users acknowledge the need to evaluate the quality of the information retrieved ([The UCLA Internet Report - Year Three, 2003: 39](#Ucl03)), they do not always do so ([Scholz-Crane 1998](#Sch98); [Graham and Metaxes 2003](#Gra03)), nor do they corroborate the retrieved information with other sources ([Flanagin and Metzger 2000](#Fla00); [Fallis 2004](#Fal04)); therefore, studying methods or mechanisms whereby information quality assessment can be performed is of utmost importance.

Several methods are used by librarians and other information professionals that can be seen as substitutes for a quality control system:

*   The _guideline_ or the checklist method ( [Alexander and Tate 1999](#Ale99)).
*   The _fact checking_ method presupposes that credibility and quality of any information obtained is dubious until corroborated with other sources ([Burbules 2001](#Bur01)).
*   The _contextual approach_ is based on using external information to the current site. That is, examining it by using three basic techniques; teaching where to locate quality sources on the Web, then comparing and corroborating the information with other sources([Meola 2004](#Meo04)).
*   The _common sense approach_ can be considered a learning process triggered by the specific information need. The knowledge accumulated during this learning process allows the user to assess the quality of information ([Haas and Wearden 2003](#Haas03)).

Previous studies were inconclusive on the question whether users voluntarily assess the quality of information accessible on the Web. We hypothesize that as an integral part of Web information behaviour there exist what we may call _information quality assessment_ which represents a true quality control measure that utilizes specific tools and procedures to assist the assessment the quality of information. In contrast, however, to any quality control programme, which is performed by a specially designated individual, in the case of information retrieval, quality control is performed by the user him/herself as a _reflex_ reaction to the diverse and inconsistent quality of information.

### Purpose of the study

The purpose of this study is to investigate information quality assessment of information retrieved on the Web by directly observing users' information seeking behaviour and by asking the following questions:

*   Does Web search behaviour include information quality assessment?
*   To what extent do people engage in quality assessment?
*   What are the main attributes used for assigning quality to information?

## Methods

To answer our research questions we used several instruments; direct observation, interviews, open and closed ended questionnaires and the _think aloud_ technique. Actions taken during tasks were monitored by using the surveillance software [Spybuddy.](http://www.webcitation.org/5d8ailvvn)

### The study population

The study population was recruited between November 2005 and February 2006 by advertising on university bulletin boards using the purposeful sampling method as described by Patton ([1990: 182](#Pat90)). When a person was recruited, s/he signed an informed consent, participated in a full research session and was then offered coupons for the university bookstore. The recruited study population was composed of seventy-seven individuals recruited from the general student (undergraduates and graduates) population at Bar-Ilan University. The majority were females (72.7% out of 77 participants), native Hebrew speakers (89.6%), who prefer to search the Web in Hebrew (79.2%) and are very frequent Web users (as 86.1% of them indicated that they use the Internet every day).

### Sessions

Each session started with briefly explaining that we are conducting a study about Web search behaviour, without disclosing that we are actually looking for behaviour related to information quality assessment. Then the think aloud technique ([Rieh 2002](#Rie02)) was demonstrated, an informed consent form was signed and a background questionnaire was completed by the participants. Thereafter, the session started with each participant completing two search and one evaluation tasks.

Here we only discuss the results of one of the search tasks denoted as scenario task, as this task simulates best the natural Web information behaviour (searching for information on a general topic). One of the additional tasks, the domain task, intended to disclose how domain knowledge influenced information quality assessment. The evaluation task was devised to observe isolated information quality assessment behaviour.

The topic for the scenario task was selected from the site [about](http://www.about.com) as suggested by Toms and Taves ([2004](#Toms04)) and the topic was social phobia. The scenario read as follows:

> You have a close friend who is shy, has problems with adjusting to new situations (like new employment, dating the opposite sex) and expresses anxiety and fear when s/he has to leave the safety of home. You, as his/her best friend encourage him/her to seek professional help. The diagnosis provided was that your friend suffers from Social Phobia or Social Anxiety. Overwhelmed by this information you try to find more information on the subject using the Web.

After completing each search task participants were interviewed for three to five minutes. They were asked for their comments, the reasons for choosing or not choosing specific sites and how they usually selected information from the Web. The responses were recorded and later transcribed([Patton 1990: 348-351](#Pat90)). Then after performing all three tasks, the participants were asked to complete a final questionnaire. It included open and closed questions relating to their views about the quality of information on the Web and the methods employed by them to ascertain it.

### Categorization

Based on ten randomly chosen transcripts and using content analysis ([Strauss and Corbin 1990](#Str90)), a hierarchical tree of categories (later to be referred as components) with their relevant attributes was constructed. Using a Microsoft Excel table, each participant was identified by an ordinal number, with the results of each task for each participant (scenario, domain, and evaluation) recorded in a separate row, while attributes and sub-attributes were placed under a different column. Each time a participant noted an attribute or a sub-attribute the value one (1) was marked in the appropriate cell ( [Miles and Huberman, 1994: 43-45, 253](#Mil94)). After accepting the emerging tree of categories, transcripts of the remaining sixty-seven participants were similarly analyzed and the categorization tree was expanded and altered as the data unfolded ( [Strauss and Corbin 1990: 61-76](#Str90) ).

#### Categorization reliability

On a randomly selected 10% of the study population, categorization was corroborated by two other coders ( [Merrick 1999](#Mer99)). Only after verifying that there was an agreement between the external coders and the categorization performed by us, was the overall categorization accepted.

#### Scoring method

The scoring method expresses numerically the use of the information quality assessment components and its attributes, where an attribute is a feature of an item mentioned by participants. Each component consisted of a different number of attributes, and in order to be able to compare them the components were normalized, i.e., divided by the number of attributes of each component. There were two types of scores:

*   Normalized attribute score: is the number of participants who mentioned the attribute (called attribute score) divided by the number of participants (seventy-seven) and presented as percentage.
*   Normalized group component score: is the sum all the attribute scores for a given component divided by the number of participants and normalized by dividing it by the number of attributes of the component, and then presented as percentage.

## Results

### The information quality assessment components and attributes

The attributes mentioned and used by the participants, defined four different evaluative components (Figure 1): _credibility of site_, _credibility of content_, _predictive relevance_ and _veracity assessment_. In the following subsections we describe the attributes of each of these components. These four components comprise the core category information quality assessment.

<div align="center">![Figure 1 The information quality assessment components](p357fig1.jpg)</div>

<div align="center">**Figure 1: The information quality assessment components**</div>

### Attributes that comprise the component _credibility of content_

There is consensus that, for assessing credibility (referred to here as _credibility of content_ (Figure 2)), some attributes are considered as core elements and, therefore, one expects that every credibility assessment will take them into account ( [Rieh and Danielson 2007](#Rie07)). These are: scope, accuracy, objectivity, currency and authority. However, in reality not all attributes are used ( [Fogg _et al._ 2003](#Fogg03)) and users usually mention only one or two of them ( [Scholz-Crane 1998](#Sch98)). In our study, users mentioned and used extensively only the authority attributes.

Previous studies found that attributes that relate to authority are the main attributes people use when evaluating information quality ( [Rieh 2002](#Rie02); [Freeman and Spyridakis 2004](#Fre04); [Eysenbach and Köhler 2002](#Eys02); [Liu and Huang 2005](#Liu05)) and, therefore, it is not surprising that in the _scenario task_ more than 60% of the study population mentioned the attributes _source_ and _author_ where the first (mentioned by 62% of the participants) is expressed by three sub-attributes: public, private and commercial as nicely verbalized by three participants '_I see that it is Geocities._' '_It is exactly what I looked for, a non profit organization._' '_Although I see it's a .com site, it can be useful_'.

The attribute _author_ (mentioned by 70% of the participants) refers to the author's credentials, qualifications ([Metzger 2007](#Met07)), as was stated by two participants: '_.the person who wrote it is a doctor. I don't know if this doctor really exists, yet the information is helpful_'. '_I am interested knowing who the author is... whether he's an expert or someone who decided to publish his thoughts_'. However, the fact that participants noticed the author's name or credentials does not mean that they actually verified it.

Similarly to the findings of Flanagin and Metzger ([2000](#Fla00)), under the normative Web search behaviour (_scenario task_) we find that the attributes; _accuracy_, _currency_, _objectivity_ were mentioned by very few participants. The low usage of the attribute _accuracy_ (In the context of this study, accuracy, translated from the Hebrew, exists only in the sense of exactness and not that of truth) is explained by the participants themselves when stating that '_.I really can't say whether the information is accurate or complete as I didn't study 'academically' social phobia .if I were a psychologist then I would know_'.

The attributes _accuracy and currency_ as well as the sub-attribute _links and references_ - refers to whether the links or the references themselves are current: '_the site presents how to deal with social phobia but the links that the site refers to are not from recent years._' were also not used by many participants, as reflected in Metzger's ([2007: 2083](#Met07)) conclusion that '_people do not seem to take the currency of the information they find online into account when making credibility judgments_'.

Other frequently used attributes were: _prior acquaintance with site_ (25%), _writing style_ (26%), _type of reference_ (35%) and _scope and completeness_ (27%). The use of the attribute _prior acquaintance with site_ is interesting as it indicates that a site has been already assessed for quality, found to be of good quality, implying that those sites have an advantage compared to other (unknown sites) as there is less need to go though the information quality assessment process and as one of the participants stated: '_I am familiar with [InfoMed](http://www.infomed.co.il) it is a good source_'.

When evaluating an article or a book, one turns to examine whether the author included current references and what types of references were used. This behaviour was found by Eysenbach and Köhler ([2002](#Eys02)) and by the current research as well. The participants used the attribute _writing style_ and its sub attributes _scientific_ or _popular_ as a _sign_ of quality (similar to [Eysenbach and Köhler 2002](#Eys02)) as one of our participants stated: '_It's a serious article; the language is more scientific_'.

The literature ( [Metzger 2007](#Met07); [Rieh and Danielson 2007](#Rie07)) presents scope as one of the five core attributes and, indeed, Scholz-Crane's ([1998](#Sch98)) findings indicated this criterion should be considered as one of the most important criteria for evaluation information quality. However, in contrast to the extensive use of the authority attributes (author and source) by the majority of participants (over 60% of them) only 27% of our participants used the attribute _scope_ and _completeness_ preventing us from ascribing this attribute as one of the core attributes.

<div align="center">![Figure 2: The attributes that make up the component credibility of content](p357fig2.jpg)</div>

<div align="center">**Figure 2: The attributes that make up the component _credibility of content_**</div>

### Attributes that comprise the _component credibility_ of site

Previous studies suggested that the appearance of a site and its design are pivotal when evaluating the overall credibility of that site. However, when prior knowledge about the subject exists ([Eastin 2001](#Eas01); [Rieh 2002](#Rie02); [Stanford _et al._ 2002](#Sta02)) people emphasize the evaluation of the quality of the information ([Liu and Huang 2005](#Liu05); [Stanford _et al._ 2002](#Sta02); [Jenkins _et al._ 2003](#Jen03)) more than the appearance of the site.

Attributes that were mentioned and used are presented in Figure 3, where the main attributes (coloured magenta) of credibility of site are _design_ and _language_. The attribute _design_ is expressed by several sub attributes:

*   <span style="color: maroon;">picture and figures</span>: '_The picture is very appealing . the pictures add a lot_.'
*   <span style="color: maroon;">contact</span> : _There is an option to contact the psychologist._
_*   <span style="color: maroon;">advertisement</span>: _There are no ads so it increases the positive opinion about the site.__*   <span style="color: maroon;">number of links</span> : _It is worth reading; there are a lot of external links.__*   <span style="color: maroon;">layout of page</span>: _The page is divided into subjects and titles.__*   <span style="color: maroon;">tables and numbers</span> : _There is a table with a test about social phobia.nice that helps.__*   <span style="color: maroon;">ease of navigation</span>: _...it's a better site than the last one, it is more organized as it is divided into categories...__*   <span style="color: maroon;">search engine on site</span> : _.there is a search engine..shows that the a Web designer planed the site.__*   <span style="color: maroon;">The colour here is problematic - too much.</span>*   <span style="color: maroon;">The site was updated a few months ago.</span>*   <span style="color: maroon;">The font is too small.</span>_______

___

The attribute _language_ depicted herein denotes a preconceived idea that sites written in English are more academic that those written in Hebrew as expressed by one of the participants '_Because the terminology is in English I will choose this site. it looks more scientific_'.

Barnes _et al._ ( [2003](#Bar03)) found that when participants evaluate Websites they relied more on the design and aesthetics criterion, than on the authority attributes while the present study indicates that only 19% and 23% of the participants used the attributes _tables and numbers_ and _layout_ respectively. Freeman and Spyridakis ( [2004](#Fre04)) concluded that the presence or absence of links did not significantly affect credibility ratings; which may explain the fact that only 31% of our participants used the attribute _number of links_.

<div align="center">![Figure 3: The attributes that make up the component credibility of site](p357fig3.jpg)</div>

<div align="center">**Figure 3: The attributes that make up the component _credibility of site_**</div>

### Attributes that comprise the component _predictive relevance_

When looking for information, on one hand, one looks for the most relevant information to a specific query and topic ([Bateman 1999](#Bat99); [Wang and Soergel 1998](#Wan98)) while on the other, one needs to reduce the amount of information retrieved. Although predictive relevance is conceptually related to relevance, it is also akin to Hogarth's ( [1987](#Hog87)) predictive judgment as the users judge the relevance of the search results before accessing the site.

The use of this component (Figure 4) suggests that the users predict the relevance of the search results based on the _ranking_ of the results by the search engines, the _snippets_ and other characteristics of the search results like the title of the retrieved document and the occurrence of the _query terms_ in the title, in the snippet or in the _url_ of the search result, as explained by two participants: '_the first results are most relevant so I usually look at them_', '_I look at the title and the abstract (snippet) to see if it matches what I asked._' , '_I usually look at the URL, and then I can see that it has no relevancy to social anxiety [because the query terms do not appear in the url]._'

People prefer to use their mother tongue when searching the Web ( [Eysenbach and Köhler 2002](#Eys02)) as well as when assigning relevance ([Hansen and Karlgren 2005](#Han05)). Therefore, _language_, an attribute of predictive relevance, serves here as a _filter_ to information as participants not only searched exclusively in Hebrew but if additional information was linked to English sites they automatically exited the site, as expressed by one of the participants '_I wouldn't enter this site, because of the language - English_'. As can be seen this attribute is different from the attribute _language_ of the component in _credibility of site_, where there the presence of the attribute _language_ (such as English) increases the credibility and the reputation in the eyes of the users, whereas in the case of the component _predictive relevance_ the attribute _language_ serves as a filter to reduce the amount of relevant sites. Although _language_ has an impact on _(predictive) relevance_ as 18% of our participants used this attribute, the attributes _snippet_ and _ranking_ were the most frequently used attributes of this component, mentioned by 32% and 30% of the participants respectively.

<div align="center">![Figure 4: The component predictive relevance and its attributes](p357fig4.jpg)</div>

<div align="center">**Figure 4: The component _predictive relevance_ and its attributes**</div>

### Attributes that comprise the component _veracity assessment_

It was Descartes who said '_if one wishes to know the truth, then one should not believe an assertion until one finds evidence to justify of doing so_' ( [Gilbert _et al._ 1993: 221](#Gil93) ), thus positioning truth as pivotal for accepting information. Hence, to ascertain the veracity of information one should 1) compare and corroborate the information retrieved on the Web with other sources ([Meola 2004](#Meo04)); 2) ascertain authority; and 3) compare the information retrieved on the Web with personal knowledge ([Haas and Waerden 2003](#Haas03)). Therefore, the attributes defining the component veracity assessment (see Figure 5) were _previous knowledge_ (as expressed by a participant '_from my knowledge, I know that socio is society._') and _corroboration_. The latter seems to be the default, whereby one will try to expand the pre-existing knowledge by corroborating the newly acquired information with other sources. Several sub-attributes of _corroboration_ were identified by us:

*   <span style="color: maroon;">other sites</span> : '_I think I found a credible site but to be sure .I prefer to find two more._
*   <span style="color: maroon;">forums</span> - The Web had opened a new and immediate channel of exchanging information while reducing anxieties and uncertainties known as forum as was acknowledged by a participant: '_I use forums a lot, like the one on doctors.co.il [an Israeli health site] so I am sure my friend can find useful information there, also the site is managed by known doctors_. The use of this attributes is further emphasized by the fact that 45% of the participants used the sub-attribute _forums maintained by experts_ suggesting that in this case too, preference is given to authoritative sources.
*   <span style="color: maroon;">printed sources</span>: '_.not enough. needs corroboration with books_.
*   <span style="color: maroon;">consultation with experts</span> - This attribute was not utilized by the users during the experiment, but was mentioned by them as a mean for assuring the quality of information in the final questionnaire.

<div align="center">![Figure 5: The attributes that make up the component veracity assessment](p357fig5.jpg)</div>

<div align="center">**Figure 5: The attributes that make up the component _veracity assessment_**</div>

### Attribute and component use defines behaviour

Attribute scores are the reflection of the number of times an attribute is mentioned by the participants; therefore, the extent and rate of the attribute use, and by extension of component use, defines behaviour. Hence, attributes mentioned by participants during observation are actually used and, therefore, each component and attribute of the information quality assessment process is not a perception it is rather a tool to be utilized for direct appraisal of the quality of information. We envisage the use of information quality assessment as if the user has at his disposal an array of attributes integrated into four different components and utilizes them as the need arises. However, it seems that there is a small preference in usage of certain components as is shown in Table 1 where participants used more readily the components (and therefore its attributes) _credibility of content_ and _predictive relevance_, more often than the components _veracity assessment_ and _credibility of site_, suggesting that for our participants, like for participants in other studies, ( [Stanford _et al._ 2002](#Sta02); [Rieh 2002](#Rie02)) the information has to be both credible and relevant, before being assessed for its veracity or presentation. Note that the _normalized group score_ measures the extent of the use of a specific component on average, i.e., the participants in this experiment used on average 16.6% of the attributes available for the component _credibility of content._.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"><caption align="bottom">  
****Table 1: Participants use more readily the components _credibility of content_ and _predictive relevance_****</caption>

<tbody>

<tr>

<th>Component</th>

<th>Normalized group component score (N=77)</th>

</tr>

<tr>

<td>Credibility of content</td>

<td align="center">16.6%</td>

</tr>

<tr>

<td>Predictive relevance</td>

<td align="center">13.7%</td>

</tr>

<tr>

<td>Veracity assessment</td>

<td align="center">9.2%</td>

</tr>

<tr>

<td>Credibility of site</td>

<td align="center">7.2%</td>

</tr>

</tbody>

</table>

## Conclusions

Based on our data, we suggest that information quality assessment is an integral part of the overall Web search behaviour, as all participants performed to some extent a kind of information quality assessment. Information quality assessment is intimately linked to the basic need of people to ascertain the truth; it is composed of components and attributes that represent an actual behaviour. Hence, attributes mentioned by participants during sessions are actually used and, therefore, each component and attribute of the information quality assessment process is not a perception but rather a tool to be used for direct appraisal of the quality of information.

Similar to other reports ( [Metzger 2007](#Met07)), we also found that, when evaluating quality, participants tend to use only few of the conventional evaluation criteria. Previous studies showed that users link credibility to authority ( [Rieh 2002](#Rie02); [Fogg _et al._ 2003](#Fogg03); [Eysenbach and Köhler 2002](#Eys02); [Liu 2004](#Liu04)). Similarly, the main finding of this study is that the participants, regardless of their previous knowledge on the subject or experience in Web use, use the attributes _author_ and _source_ as primary attributes to assess quality. It seems as if in order to evaluate quality the use of these attributes not only lessens the need to use other attributes, but it constitutes a natural behaviour were one tries to assess truth by asking questions like: Who said it? Who wrote it? What is the source of this information? We have further shown, like Eysenbach and Köhler ( [2002](#Eys02)), that, for non native English speakers, language may be an obstacle. We have noticed that _language_ has become an important attribute of predictive relevance together with the attributes _ranking_ and _snippets_.

The study shows that Web users always, although not always knowingly, perform some rudimentary information quality assessment, where newly introduced Web features such as ranking and snippets are integrated with the classical attributes of authority to generate a new mechanism to ascertain quality.

## Acknowledgements

Results presented in this paper are part of the Ph.D dissertation of Noa Fink-Shamit, carried out under the supervision of Judit Bar-Ilan at the Department of Information Science, Bar-Ilan University.

## References

Alexander, J.E. & Tate, M.A. (1999). _Web wisdom: how to evaluate and create information quality on the Web_. Hillsdale, NJ: Erlbaum.

Barnes, M.D., Penrod, C., Brad, L., Neiger, R.M., Marrill, R.T., Eggett, D.L. _et al._ (2003). Measuring the relevance of evaluation criteria among health information seekers on the Internet. _Journal of Health Psychology_, **8** (1), 71-82.

Barry, C.L. (1994).User-defined relevance criteria: an exploratory study . _Journal of American Society for Information Science_, **45**(3), 149-159.

Bateman, J. (1999). Modeling the importance of end-user relevance criteria. _Proceedings of the ASIS Annual Meeting_ ,**36**, 396-406.

Burbules, N. C. (2001). Paradoxes of the Web: the ethical dimensions of credibility. _Library Trends_, **49**(3), 441-453.

<a id="Cred" name="Cre"></a>[Credibility](http://www.merriam-webster.com/dictionary/credibility) (2007). In _Merriam-Webster Online Dictionary_. Retrieved April 1, 2008 from: http://www.merriam-webster.com/dictionary/credibility. (Archived by WebCite® at http://www.webcitation.org/5d8f0iptR)

Eysenbach, G. & Köhler, C. (2002). [How do consumers search for and appraise health information on the World Wide Web? Qualitative study using focus groups, usability tests, and in-depth interviews.](http://www.bmj.com/cgi/content/full/324/7337/573) _British Medical Journal_, **324**, 573-577\. Retrieved 15 December, 2008 from http://www.bmj.com/cgi/content/full/324/7337/573 (Archived by WebCite® at http://www.webcitation.org/5d8fNA8GC)

Eastin, M.S. (2001). [Credibility assessments of online health information: The effects of source expertise and knowledge of content.](http://jcmc.indiana.edu/vol6/issue4/eastin.html) _Journal of Computer-Mediated Communication_, **6** (4). Retrieved April, 11 2008 from: http://jcmc.indiana.edu/vol6/issue4/eastin.html (Archived by WebCite® at http://www.webcitation.org/5d8fTjnt0)

Fallis, D. (2004). On verifying the accuracy of information: philosophical perspectives. _Library Trends_, **52**(3), 463-487.

Flanigan, A. & Metzger, M. (2000). Perceptions of Internet information credibility. _Journalism and Mass Communication Quarterly_, **77**(3), 515-540.

Fogg, B. J., Marshall, J., Laraki,O., Osipovich, A. Varma, C., Fang, N., Paul, J.,Rangnekar, A ., Shon, J., Swani, P. & Marissa Treinen. (2001). [What makes a Web site credible? A report on a large quantitative study](http://captology.stanford.edu/pdf/p61-fogg.pdf). In _Proceedings of ACM CHI 2001 Conference on Human Factors in Computing Systems_, vol. 1, (pp. 61-68). New York, NY: ACM Press.

Retrieved 26 April, 2008 from: http://captology.stanford.edu/pdf/p61-fogg.pdf (Archived by WebCite® at http://www.webcitation.org/5d8ffszaE)

Fogg, B. J., Soohoo, C. & Danielson, D.(2002). [How do people evaluate a Web site's credibility: results from a large study](http://www.consumerwebwatch.org/dynamic/web-credibility-reports-evaluate-abstract.cfm) . Published by Consumer WebWatch. Retrieved 19 April, 2008 from http://bit.ly/KaO4 (Archived by WebCite® at http://www.webcitation.org/5d8fqA0RX)

Fogg, B. J., Soohoo, C., Danielson, D. R., Marable, L., Stanford, J. & Trauber, E. R. (2003). How do users evaluate the credibility of Web sites? A study with over 2,500 participants. In _Designing For User Experiences: Proceedings of the 2003 Conference on Designing for User Experiences_, San Francisco, California June 06 - 07, 2003, (pp. 1-15), New York, NY: ACM Press.

Freeman, K. S. & Spyridakis, J.H. (2004). An examination of factors that affect the credibility of online health Information. _Technical Communication_, **51**(2), 239-263.

Garvin, D.A (1988). _Managing quality: the strategic and competitive edge_. New York, NY: Free Press.

Gilbert, D.T., Tafarodi, R.W. & Malone, P.S.(1993). [You can't not believe everything you read](http://www.wjh.harvard.edu/~dtg/Gilbert%20et%20al%20(EVERYTHING%20YOU%20READ).pdf). _Journal of Personality and Social Psychology_, **65**(2), 221-233.Retrieved April 19, 2008 from: http://bit.ly/1SFBq7 (Archived by WebCite® at http://www.webcitation.org/5d8fzaZFi)

Graham, L. & Metaxes, P.T. (2003). Of course it's true; I saw it on the Internet!: critical thinking in the Internet era. _Communications of the ACM_, **46**(5), 70-75.

Grimes, D.J. & Boening, C.H. (2001). Worries with the Web: a look at student use of Web resources. _College and Research Libraries_, **62**(1), 11-23.

Haas, C. & Wearden, S.T. (2003). E-credibility: building common ground in Web environments. _Educational Studies in Language and Literature_, **3**(1/2), 169-184.

Hansen, P. & Karlgren, J. (2005). Effects of foreign language and task scenario on relevance assessment, _Journal of Documentation_, **61**(5), 623-639.

Hogarth, R. (1987). _Judgement and choice._ 2nd ed. New York, NY: Wiley.

Jenkins, C., Corritore, C. L. & Wiedenbeck, W. (2003). Patterns of information seeking on the Web: a qualitative study of domain expertise and web expertise. _IT and Society_, **1**(3), 64-89.

Liu, Z. (2004). Perceptions of credibility of scholarly information on the Web. _Information Processing and Management_. **40**(6), 1027-1038.

Liu, Z. & Huang, X. (2005). Evaluating the credibility of scholarly information on the Web: a cross cultural study. _The International Information and Library Review_, **37**(2), 99-106.

Jacoby, M.G. (2002). Kierkegaard on truth. _Religious studies_, **38**(1), 27-44.

Kahneman. D. & Tversky, A. (1982). Variants of uncertainty. _Cognition_, **11**(2), 143-157.

Meola, M. (2004). [Chucking the checklist: a contextual approach to teaching undergradutes Web evaluation.](http://www.tcnj.edu/~meolam/documents/Chucking_003.pdf) _Portal: Libraries and the Academy_, **4**(3), 331-344\. Retrieved 19 April, 2008 from http://www.tcnj.edu/~meolam/documents/Chucking_003.pdf (Archived by WebCite® at http://www.webcitation.org/5d8gDA3IH)

Metzger, M. J. (2007). Making sense of credibility on the Web: models for evaluating online information and recommendations for future research. _Journal of the American Society for Information Science and Technology_, **58**(13), 2078-2091.

Metzger, M. J., Flanagin, A. J., Eyal, K., Lemus, D. & McCann, R. (2003). Credibility for the 21st century: integrating perspectives on source, message, and media credibility in the contemporary media environment. _Communication yearbook_, **27**, 293-335.

Merrick, E (1999).An exploration of quality in qualitative research. In M. Kopala & L.A. Suzuki, (eds.) _Using qualitative methods in psychology_ (pp. 25-36). Thousand Oaks, CA: Sage Publications.

Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis: an expanded sourcebook_. 2nd ed. Thousand Oaks, CA: Sage Publications.

Patton, M.Q. (1990). _Qualitative evaluation and research methods_. 2nd ed. Newbury Park, CA: Sage Publications.

Pirsig, R.M. (1974). _Zen and the art of motorcycle maintenance: an inquiry into values_. New York, NY: Bantam Books.

Rieh, S.Y. & Danielson, D.R. (2007). Credibility: a multidisciplinary framework. _Annual Review of Information Science and Technology_, **41**, 307-364.

Rieh, S.Y. (2002). Judgment of information quality and cognitive authority in the Web. _Journal of the American Society for Information Science and Technology_, **53**(2), 145-161.

Rieh, S.Y. & Belkin, N.J. (2000). Interaction on the Web: scholars' judgment of information quality and cognitive authority. _Proceedings of the ASIS Annual Meeting_, **37**, 25-38.

Robins, D & Holmes, J. (2008). Aesthetics and credibility in web site design. _Information Processing and Management_, **44**(1), 386-399.

Savolainen, R. (2007). [Media credibility and cognitive authority. The case of seeking orienting information](http://InformationR.net/ir/12-3/paper319.html). _Information Research_, **12**(3), paper 319\. Retrieved 19 April, 2008 from http://InformationR.net/ir/12-3/paper319.html (Archived by WebCite® at http://www.webcitation.org/5d8h3ddMQ)

Scholz-Crane, A. (1998). Evaluating the future: a preliminary study of the process of how undergraduate students evaluate Web sources. _Reference Services Review_, **26**(3/4), 53-60.

Stanford, J., Tauber, E.R., Fogg, B.J. & Marable, L. (2002). [Experts vs. online consumers: A comparative credibility study of health and finance Web sites](http://www.consumerwebwatch.org/dynamic/web-credibility-reports-experts-vs-online.cfm). _Consumer WebWatch Research Report_. Retrieved May 8, 2008 from: http://bit.ly/VDWT (Archived by WebCite® at http://www.webcitation.org/5d8h90cZ5)

Strauss, A.L & Corbin, J. (1990). _Basics of qualitative research: grounded theory procedures and techniques_. London: Sage Publications.

Toms, E.G. & Taves, A.R. (2004). Measuring user perceptions of Web site reputation. _Information Processing and Management_, **40**(2), 291-317.

[Truth.](http://www.merriam-webster.com/dictionary/truth) (2007). In _Merriam-Webster Online Dictionary_. Retrieved 15 December, 2008 from http://www.merriam-webster.com/dictionary/truth (Archived by WebCite® at http://www.webcitation.org/5d8hIHiip)

University of California, Los Angeles. _Center for Communication Policy_. (2003). _[The UCLA Internet report. Surveying the digital future. Year three](http://www.digitalcenter.org/pdf/InternetReportYearThree.pdf)_. Retrieved 1 April, 2008 from http://www.digitalcenter.org/pdf/InternetReportYearThree.pdf (Archived by WebCite® at http://www.webcitation.org/5d8grxUZD)

Wang, P. & Soergel, D. (1998). A cognitive model of document use during a research project. Study I: Document selection. _Journal of the American Society for Information Science_, **49**(2), 115-133.

Wilson, P (1983). _Second-hand knowledge: an inquiry into cognitive authority_. Westport, CT: Greenwood Press.

