#### Information Research, Vol. 1 No. 2, December 1995

* * *

[](#paper6)

# Business Use of the World-Wide Web

#### Claire Cockburn and [T.D. Wilson](mailto:t.d.wilson@shef.ac.uk)  
Department of Information Studies  
University of Sheffield, Sheffield, UK

* * *

## Introduction: the Internet and World-Wide Web

Commercial activity on the Internet has only recently been possible. When conceived in 1969, the then ARPAnet was purely the domain of US government research agencies and academic institutions. Business use was prohibited by the National Science Foundation's (NSF) appropriate use policy. ARPAnet was developed at the height of the cold war as an indestructible computer communications network. Based on a vast number of links between many 'host' computers, the theory was that, if any of the links were to be broken the network would not collapse and communication would be preserved.

While ARPAnet grew, similar networks were developing with a more business-orientated slant, although they did not have access to ARPAnet. In the late 1980s ARPAnet was divided into Milnet (the US government military network) and NSFnet (for research and academic purposes). In 1991 NSFnet and the commercial nets were finally connected to form what is today called the Internet ([Fielding](#fie), 1994; [The Internet Group](#tig), 1994; [CommerceNet](#com), 1995; and [Dern](#der), 1994). Of the 25,000 different networks, over 60% are run by businesses ([Tetzeli](#tet), 1994) and it is due to the presence of more and more of these businesses that the Internet is growing so rapidly.

The WWW is a sub-network which was developed at CERN in Switzerland (the European Laboratory for Particle Physics) and is the fastest growing part of the Internet. Based on the Hypertext Transport Protocol (HTTP), it uses hypertext mark-up language (HTML) to create 'web pages' which can be easily navigated through numerous hypertext links contained within them ([Patch](#pat), 1994; [Cortese](#cor), 1995).

Web browsers based on the 'point and click' principal which is so popular in today's software, are freely available over the Internet and allow access to all the Web has to offer. The first of these browsers to be developed was Mosaic, of which 2.5 million copies were downloaded during its first year of release ([Taylor](#tay), 1994) and of which 20,000 copies continue to be downloaded every month.

It is not only the order that the Web has brought to the Internet that has made it so popular. A great deal of its attraction stems from its excellent user- friendly front-end. Web pages can contain graphics, photographs, sound and even video clips in addition to plain text - they truly are multi-media documents. In his book devoted to the Web, [Winder](#win) (1995) states that there are at least 5 million Web pages in existence today, with more and more being added all the time. The Web is believed to be growing at twice the rate of the Internet as a whole. Web traffic increased by 300,000% in the year 1994.

## Possible uses of the Internet and World Wide Web for business

The Internet and, more particularly, the WWW are attracting businesses in their thousands, with the following appearing to be the main application areas:

### Publicity, Marketing and Advertising

The WWW appears to be an ideal medium for businesses attempting to promote themselves and their wares. Setting up a site on the WWW, and thus gaining instant access to millions of people all over the globe, can be achieved at a small fraction of the cost using more conventional methods ([Watson](#wat), 1994).

### Direct On-line Selling

It is already possible to visit 'virtual malls' full of 'virtual shops', browse through catalogues and examine various products in vast detail, all courtesy of the Web. This has all been made possible by the multi-media capabilities that the Web provides ([Minio](#min), 1994).

### Research and Development

Companies, especially those involved in research and development, can use the Internet as an additional resource for collecting information. [Tetzeli](#tet) (1994) explains how it is possible to post a query on a bulletin board or join a discussion group and receive advice on how to solve the problem. Alternatively, there are millions of Web pages, some of which contain access to searchable databases of information relating to particular subjects.

### Communication

The use of low-cost electronic mail (e-mail) is the Internet service used most extensively by businesses ([Rosen](#ros), 1994). [Kehoe](#keh) (1994) illustrates the strength of e-mail with the example of 'Digital Equipment' which has over 31,000 computers linked up to the Internet and exchanges about 1.7 million e-mail messages each month with people external to the company.

### Collaboration

When links are formed between companies, it can be easy for them to communicate through the Internet. One example of this is the collaboration between IBM and Bellcore who use Internet links to share a workstation ([Tetzeli](#tet), 1994).

Although the WWW has only existed for the last two years, there are already over 20,000 business corporations with Web sites ([Yahoo!](#yah), 1995), the figure having doubled in the last three months alone. There has been continued speculation, from a wide range of sources, that the Internet and more specifically the WWW will be the business tool of the future and that companies which do not expand in this direction will be left by the side of the information superhighway.

## The Study

Two methods were employed in the study: first, a sample of 300 businesses with Web sites, across a wide range of industry types, was examined, by selecting (rather than sampling) companies from the [Yahoo! directory](#yah) (1995). The sites were investigated in relation to several areas - the purpose of the Web site, the use being made of electronic mail and the extent to which multi-media was being utilised. In addition, any other aspects of the site which were designed to make it more interesting to potential customers were also noted.

Secondly, an electronic-mail questionnaire was sent to 222 of the 300 companies surveyed: that is, those that provided an e-mail address for contact. 14 were returned immediately due to unknown addresses or technical problems. Of the remaining 208, 102 replies were received, five of which were of no relevance, leaving 97 completed questionnaires to examine; a response rate of 47%, which is surprisingly good for a survey of this kind.

### Industrial Classification of the Sample Companies

The Standard Industrial Classification ([Central Statistics Office](#cso), 1992) was used to classify the companies chosen. The breakdown is illustrated in Table 1:

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">Table 1: Percentages of sample companies in SIC groups (* = rounding error)</caption>

<tbody>

<tr>

<td>**SIC Classification Category**</td>

<td>**% of sample**</td>

</tr>

<tr>

<td>Agriculture, hunting & forestry</td>

<td align="center">1.0</td>

</tr>

<tr>

<td>Fishing</td>

<td align="center">0.0</td>

</tr>

<tr>

<td>Mining & quarrying</td>

<td align="center">0.7</td>

</tr>

<tr>

<td>Manufacturing</td>

<td align="center">31.0</td>

</tr>

<tr>

<td>Utilities</td>

<td align="center">1.0</td>

</tr>

<tr>

<td>Construction</td>

<td align="center">0.7</td>

</tr>

<tr>

<td>Wholesale & retail</td>

<td align="center">22.0</td>

</tr>

<tr>

<td>Hotels & restaurants</td>

<td align="center">2.0</td>

</tr>

<tr>

<td>Tranport & communication</td>

<td align="center">9.3</td>

</tr>

<tr>

<td>Financial services</td>

<td align="center">2.7</td>

</tr>

<tr>

<td>Real estate & business activities</td>

<td align="center">26.3</td>

</tr>

<tr>

<td>Public administration</td>

<td align="center">0.0</td>

</tr>

<tr>

<td>Education</td>

<td align="center">0.7</td>

</tr>

<tr>

<td>Health</td>

<td align="center">0.7</td>

</tr>

<tr>

<td>Other community & public services</td>

<td align="center">2.0</td>

</tr>

<tr>

<td>Private households</td>

<td align="center">0.0</td>

</tr>

<tr>

<td>Extra-territorial organizations</td>

<td align="center">0.0</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**100.1***</td>

</tr>

</tbody>

</table>

In order to investigate the actual distribution business types across the WWW a separate study was undertaken. The Yahoo! directory was used as a basis for the creation of the typology as it already breaks down over 20,000 business entries into specific business types. It also gives the number of businesses in each break-down. An alternative classification scheme was used to create the typology as the Standard Industrial Classification was not appropriate for use with the Yahoo! directory. The results are shown in Table 2.

<table align="center" bgcolor="#FBFFDF" border="1"><caption align="bottom">Table 2: Classification of _Yahoo!_ companies (* = rounding error)</caption>

<tbody>

<tr>

<td>**Industry classification**</td>

<td>**% of sample**</td>

</tr>

<tr>

<td>Financial services</td>

<td align="center">3.87</td>

</tr>

<tr>

<td>Real estate, renting & business activities</td>

<td align="center">14.21</td>

</tr>

<tr>

<td>Computer related</td>

<td align="center">22.50</td>

</tr>

<tr>

<td>Internet related</td>

<td align="center">13.93</td>

</tr>

<tr>

<td>Transport, travel, storage & communication</td>

<td align="center">10.41</td>

</tr>

<tr>

<td>Utilities</td>

<td align="center">0.39</td>

</tr>

<tr>

<td>Construction</td>

<td align="center">0.38</td>

</tr>

<tr>

<td>Education</td>

<td align="center">1.01</td>

</tr>

<tr>

<td>Retailing</td>

<td align="center">6.38</td>

</tr>

<tr>

<td>Mining & quarrying</td>

<td align="center">0.18</td>

</tr>

<tr>

<td>Agriculture</td>

<td align="center">0.07</td>

</tr>

<tr>

<td>Publishing</td>

<td align="center">6.57</td>

</tr>

<tr>

<td>Entertainment</td>

<td align="center">9.84</td>

</tr>

<tr>

<td>Food</td>

<td align="center">1.58</td>

</tr>

<tr>

<td>Engineering & manufacturing</td>

<td align="center">3.39</td>

</tr>

<tr>

<td>Scientific & environmental</td>

<td align="center">1.18</td>

</tr>

<tr>

<td>Miscellaneous</td>

<td align="center">2.64</td>

</tr>

<tr>

<td>Shopping centres</td>

<td align="center">1.44</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**99.97***</td>

</tr>

</tbody>

</table>

Compared to the individual Web sites examined, the percentages of Retailing and Manufacturing companies are much lower here (6.38% and 3.39% respectively). This is due to the nature of the alternative classification scheme used in the whole Yahoo! directory. It should be noted that many of the companies in other classes are also engaged in retail or manufacturing activities. The rest of the classes contain small percentages of companies, but the wide variation of business types on the Web can easily be seen.

During the course of this study the number of companies cited in the Yahoo! directory rose from 11,704 to 20,612, almost doubling. Table 3 shows how the distribution of business types has changed in the three months from May 1995 to August 1995.

Most importantly Table 3 appears to show that the Real estate and business activity sector of businesses on the Web is growing much faster than any other sector. Conversely, the Computer and Internet-related businesses do not seem to be growing as fast. Basically, this means that some of the other industry types are starting to catch up with them, although it must be said that they have a long way to go.

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">Table 3: Change in distribution of industries in Yahoo! directory (*=rounding error)</caption>

<tbody>

<tr>

<td align="center">**Industry classification**</td>

<td align="center">**May 95 %**</td>

<td align="center">**August 95 %**</td>

<td align="center">**% change**</td>

</tr>

<tr>

<td>Financial services</td>

<td align="center">3.51</td>

<td align="center">3.87</td>

<td align="center">+0.36</td>

</tr>

<tr>

<td>Real estate, renting & business activities</td>

<td align="center">9.18</td>

<td align="center">14.21</td>

<td align="center">+5.03</td>

</tr>

<tr>

<td>Computer related</td>

<td align="center">24.90</td>

<td align="center">22.50</td>

<td align="center">-2.4</td>

</tr>

<tr>

<td>Internet related</td>

<td align="center">16.3</td>

<td align="center">13.93</td>

<td align="center">-2.37</td>

</tr>

<tr>

<td>Transport, travel, storage & communication</td>

<td align="center">9.27</td>

<td align="center">10.41</td>

<td align="center">+1.14</td>

</tr>

<tr>

<td>Utilities</td>

<td align="center">0.36</td>

<td align="center">0.39</td>

<td align="center">+0.03</td>

</tr>

<tr>

<td>Construction</td>

<td align="center">0.23</td>

<td align="center">0.38</td>

<td align="center">+0.15</td>

</tr>

<tr>

<td>Education</td>

<td align="center">0.90</td>

<td align="center">1.01</td>

<td align="center">+0.11</td>

</tr>

<tr>

<td>Retailing</td>

<td align="center">6.25</td>

<td align="center">6.38</td>

<td align="center">+0.13</td>

</tr>

<tr>

<td>Mining & quarrying</td>

<td align="center">0.16</td>

<td align="center">0.18</td>

<td align="center">+0.02</td>

</tr>

<tr>

<td>Agriculture</td>

<td align="center">0.08</td>

<td align="center">0.07</td>

<td align="center">-0.01</td>

</tr>

<tr>

<td>Publishing</td>

<td align="center">8.66</td>

<td align="center">6.57</td>

<td align="center">-2.09</td>

</tr>

<tr>

<td>Entertainment</td>

<td align="center">8.74</td>

<td align="center">9.84</td>

<td align="center">+1.10</td>

</tr>

<tr>

<td>Food</td>

<td align="center">1.91</td>

<td align="center">1.58</td>

<td align="center">-0.33</td>

</tr>

<tr>

<td>Engineering & manufacturing</td>

<td align="center">2.88</td>

<td align="center">3.39</td>

<td align="center">+0.51</td>

</tr>

<tr>

<td>Scientific & environmental</td>

<td align="center">0.93</td>

<td align="center">1.18</td>

<td align="center">+0.25</td>

</tr>

<tr>

<td>Miscellaneous</td>

<td align="center">4.45</td>

<td align="center">2.64</td>

<td align="center">-1.81</td>

</tr>

<tr>

<td>Shopping centres</td>

<td align="center">1.26</td>

<td align="center">1.44</td>

<td align="center">+0.18</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**99.97***</td>

<td align="center">**99.97***</td>

<td align="center">**Not summed**</td>

</tr>

</tbody>

</table>

### Use of WWW Sites

The 300 companies making up the sample were classified as to the use of their Web site using the following scheme (the percentage of the sample in each class is shown in parentheses):

1.  A basic Web presence with basic information about the company but no further details on specific products or services. (3.0%)
2.  A Web presence with company information including some information about products or services. (59.3%)
3.  A Web presence with company information and product or service information together with some price details but with facilities for conventional purchasing only. (12.0%)
4.  A Web presence with company information and product or service information with price details and the ability to order the products or services by electronic mail (but with billing occurring conventionally). (10.7%)
5.  A Web presence with company information and product or service information (including price details) and with on-line ordering and payment. (11.7%)
6.  A Web presence with company information and product or service information (including price details) with pre-registration of credit card details by conventional means to gain account number which may be used to order goods on-line. (0.3%)
7.  A Web presence with company information and providing free products or services. (3.0%)

The results show that the majority of businesses in the sample use their WWW sites as a vehicle for displaying information about themselves and their products or services (class 2). In some cases there will be detailed information relating to products but the 'hard sell' is definitely not used at these sites. Typical examples of this class are [Sanders and Mock](http://sarek.netis.com/auctions/Sanders+Mock/) and [Oracle](http://www.oracle.com). .

The next largest proportion of use is class 3 (12.0%). This is similar to class 2 but encourages the potential customer to spend money by including price information alongside product or service details. The customer, however, has to order the goods or services in a conventional manner. Of the sites surveyed [Arkatents](http://www.homepage.com/mall/arkatent/tents.html) is fairly typical of smaller businesses. It contains product and price information and advice on ordering by telephone, fax or mail. The majority of the larger businesses surveyed tend not to use the Web for publishing price information; they are more likely to use it for general marketing purposes (class 2). One exception is [Fisher Scientific](http://www.fisher1.com/) which is a large retailer of chemical products.

Bearing in mind the many problems currently involved in true electronic commerce, such as security and payment, it is not altogether surprising that the majority of companies are electing not to sell on-line at present. In fact, only 11.7% of companies surveyed are currently engaging in on-line selling with credit card details being sent over the Internet and only a quarter of these have secure servers. Examples are the sites owned by [Doggie Diamonds](http://www.primenet.com/~gillet/%20ddhome.html) who specialise in pet products and [CD World](http://www.cdworld.%20com/), a company set up specifically to sell goods on-line over the Internet. A user of the Netscape browser will find that the key at the bottom of the home page is complete, indicating that the site is secure. The order form used by [Flags of the World](http://www.mallmart.com:80%20/cgi-bin/flags) is typical of most sites offering on-line ordering.

0.3% of companies use a registration scheme to give their customers account numbers which are used to purchase directly over the Internet. The credit card details are stored safely by the company and do not need to be transmitted over the Internet. The [Infocaf?](http://www.infocafe.com) operates in this manner. A further 10.7% allow the customer to send the order by e-mail but the payment transaction occurs conventionally, with the company telephoning the customer to confirm the sale and get credit card details, or sending the bill with the goods. Both [The Value International Warehouse](http://www.barint.on.ca/cybermall/viw/value.html) and [Micropatent](http://www.micropatent.com/) use this method.

As on-line shopping becomes more accepted by the general public and technology develops solutions to the associated problems, the percentage of companies offering facilities for ordering and paying for goods and services over the Internet will undoubtedly increase. It is also important to remember that the majority of companies have had WWW sites for less than one year and are still coming to terms with many of the concepts involved in electronic commerce.

### Use of Multi-Media

The sample companies were examined to find out how many of them are incorporating multi-media into their Web pages. The results were classified as shown in Table 4:

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">Table 4: Media used in sample Web sites (*=rounding error)</caption>

<tbody>

<tr>

<td alilgn="center">**Media used**</td>

<td align="center">**% of sites**</td>

</tr>

<tr>

<td>Text only</td>

<td align="center">4.0</td>

</tr>

<tr>

<td>Text & graphics</td>

<td align="center">39.3</td>

</tr>

<tr>

<td>Text, graphics & photos.</td>

<td align="center">51.0</td>

</tr>

<tr>

<td>Text & graphics or photos, + sound or video</td>

<td align="center">5.3</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**99.6***</td>

</tr>

</tbody>

</table>

One particular company making good use of the Web's multi-media capabilities is [CJ Sports Interactive Hat Gui](http://www.texel.com/home/cjsports)de. The user can select a 'hello audio file' upon entry to the site. Extensive use is made of product photographs, including all-sides views. The site owned by [MCA/Universal](http://www.mca.com) contains huge amounts of information relating to various movies and recordings. Short video clips of some of the films are available. [CD World](http://www.cdworld.%20com) offer visitors to their site the opportunity to listen to excerpts from recordings they sell. The [Virtual Advertising](http://www.halcyon.com/zz/top.html) site contains some interesting photos and offers the user the chance to watch a short film.

### Use of electronic mail

Electronic mail is the most widely used facility on the Internet. Business traffic is exceedingly heavy. The sample companies were investigated in respect of how they were making use of e-mail on their Web pages. The results were classified as shown in Table 5:

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">Table 5: Use of e-mail by sample businesses</caption>

<tbody>

<tr>

<td alilgn="center">**E-mail use at site**</td>

<td align="center">**% of sample**</td>

</tr>

<tr>

<td>No e-mail address advertised on the Web site</td>

<td align="center">9.7</td>

</tr>

<tr>

<td>E-mail enquiries encouraged only about the Web pages themselves</td>

<td align="center">6.7</td>

</tr>

<tr>

<td>E-mail enquiries encouraged in relation to information about the company or its products or services</td>

<td align="center">12.3</td>

</tr>

<tr>

<td>E-mail enquiries encouraged in relation to information about the company or its products or services</td>

<td align="center">5.3</td>

</tr>

<tr>

<td>Credit card information sent over the Internet via e-mail</td>

<td align="center">11.7</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**100.1***</td>

</tr>

</tbody>

</table>

### Other Features

A question of relevance to business is, 'What will attract potential customers to your site and what will make them come back?' Certainly the factors discussed above are of importance in this issue. A visually attractive site will impress users more than one which is mainly textual in format with a couple of basic graphics added as an afterthought. The addition of sound and video will become more important as technology progresses. A site which contains a regularly updated list of links to really interesting sites could also draw users back for regular visits. A competition is also likely to increase interest in a site. Of the 300 sample companies surveyed just under 4% of them were currently running competitions at their sites.

In the real world, a traditional method of attracting potential customers is to give them something that they do not have to pay for. This technique has also found its way onto the WWW. Just over 10% of the companies surveyed were offering visitors to their site free demonstrations of software, the chance to download free 'shareware' or the chance to send off for free products.

[Intel](http://www.intel.com) offer visitors to their site the opportunity to download free screensavers and wallpapers for use on their PCs. Several of the publishing companies surveyed make back issues of their publications or certain articles available to users free of charge. Examples are [The Economist](http://www.economist.com/) and [The Washington Telecom Newswire](http://wtn.com/wtn/wtn.html).

The inclusion of a searchable product database helps customers quickly locate something specific rather than having to search through pages and pages of product descriptions to find what they are looking for. Within the companies surveyed, examples of sites possessing such databases are those owned by [Best Video](http://www.tagsys.com/Ads/BestVideo/) which has a database of all its video titles in stock and [Intel](http://www.intel.com) which has a database of all its components.

The inclusion of company histories, press releases and staff information were fairly commonplace amongst the sites surveyed. The site owned by [Coca-Cola](http://cocacola.com/) is a good example. Contained within the site is an extensive, very graphical history of Coca-Cola. A great deal of additional information can also be found there. There is no 'hard sell' of products at this site, it is basically used as a marketing vehicle for the company. As the site is visually very interesting, it is likely that users will be attracted back to the site for several visits, each time being reminded that the owner of this site is'Coca-Cola. The [Apple Computer](http://www.apple.com) site is large compared to many on the Web and contains a vast amount of information relating to the company and its products. In addition the site contains research details, white papers and technical reports of interest to Apple's customers. The site is seen very much as a customer support tool, with much emphasis put on the client.

Several businesses are using their Web sites to advertise job opportunities within their organisations. [Applied Innovation](http://www.aiinet.com) and [Andersen Consulting](http://www.ac.com/) are currently using the Web in this way. Applicants send their CVs to the personnel departments via electronic mail.

Some of the more innovative sites surveyed contain features which are in no way related to the business. These features are designed to entertain the user and bring him or her back to the site regularly. [Planet Reebok](http://www.planetreebok.com/) is one such site. In addition to product information it contains a bulletin board for users to communicate with one another, sport and fitness tips and a large amount of sports related information. The [Best Video](http://www.tagsys.com/Ads/BestVideo/) site mentioned above also contains a 'chat board'. A number of sites contain detailed information concerning subjects completely unrelated to their business. The [Matsushita](http://www.mei.co.jp/) site is one such example with its 1996 Olympic Games pages. The [Infovantage](http://www.infovantage.com) site contains a guide to the local region. Of all those visited, the [Molson Ice Beer](http://www.%20molsonice.com) site takes this idea to the extreme. The site is entitled The Molson Ice Polar Beach and contains very little direct product advertising. The site has music reviews, competitions and detailed information relating to the local region, all sited in the 'information igloo'. The site is aimed to attract their target audience (21 to 30 year olds) and keep them coming back. The [Guinness](http://www.business.co%20.uk/guinness/) site contrasts strongly with Molson's. It has only a few pages with pictures of the product and nothing else; rather similar to a magazine advertisement.

### Examination of Shopping Centres

There are currently over 250 'shopping centres' or 'malls' sited on the WWW. They contain a number of shops gathered together, which can all be accessed from the same server. The way in which they are run varies from centre to centre. In some, all the shops are controlled by an overall managing party and it is possible to make a combined order, selecting goods from more than one shop. Others are more like directories, each shop controlling its own orders. A selection of twenty shopping centres were surveyed as part of this study.

#### Number of Individual Shops in Each Centre

The number of individual outlets sited within each shopping centre tends to vary widely from one centre to another. The results are represented by Table 6, which shows that most shopping centres surveyed contained fewer than ten individual shops and that, in total, only 25% of them contain more than 50\. However, the number of products available for purchase within each shop differs hugely. It is not uncommon to find fewer than ten products sited at an individual shop. On the other hand, one of the sites surveyed was classified as having just five shops, but with over 20,000 products available is obviously a very extensive site. Several of the sites which contained only a small number of shops stated that many more were planned for the future, although this statement might well be simply a marketing device for the mall services.

#### Types of shops sited at shopping centres

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">Table 6: Number of shops in the shopping centres</caption>

<tbody>

<tr>

<td alilgn="center">**Number of shops**</td>

<td align="center">**% of sample**</td>

</tr>

<tr>

<td align="center">1-10</td>

<td align="center">50</td>

</tr>

<tr>

<td align="center">11-20</td>

<td align="center">20</td>

</tr>

<tr>

<td align="center">21-50</td>

<td align="center">5</td>

</tr>

<tr>

<td align="center">51-100</td>

<td align="center">20</td>

</tr>

<tr>

<td align="center">>100</td>

<td align="center">5</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**100**</td>

</tr>

</tbody>

</table>

The majority of shopping centres on the WWW tend to be filled with shops a) from a particular geographical region, b) containing similar products or c) of various types. Of the shopping centres surveyed, 20% contained shops from a specific geographical area. Examples of these include [The Alaskan Mall](http://alaskan.com/mall.html) and [Mexplaza.](http://mexplaza.udg.mx) The remaining 80% contained a variety of business types, similar to that found on the WWW as a whole. None of the shopping centres surveyed specialised in a particular type of product or service, but these do exist, an example being [ConsciousNet](http://www.consciousnet.com) which contains spiritual and new age products and services.

### Payment Methods

It can be assumed that the majority of shopping centres on the WWW have been set up because the proprietors believe the Web is going to be a good place to make money. It is likely that various forms of on-line selling will be used more within the shopping centres than they are in individual business sites. The shopping centres surveyed were classified according to the following scheme:

1.  Conventional methods of ordering and payment used - 16%.
2.  Ordering on-line but conventional payment - 10%
3.  Submission of credit card details to obtain membership ID which is used to order on-line - 16%
4.  Credit card details sent over the Internet to order on-line - 58%

Interestingly, only just under half of these had secure sites. The rest either recommended the use of Pretty Good Privacy (PGP) to encrypt electronic mail or advertised no security at their sites whatsoever.

The use of on-line ordering is far more common inshopping centres than is the case for individual companies. Only 16% of the shopping centres surveyed made no attempt to encourage on-line ordering or payment, preferring conventional methods instead.

### Design of Sites

The majority of shopping centres surveyed make fairly extensive use of multi-media within their Web pages. The sites were classified using the same scheme as the sample companies. The results are shown in Table 7, below.

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">Table 7: Use of multi-media at shopping centre sites</caption>

<tbody>

<tr>

<td alilgn="center">**Media used**</td>

<td align="center">**% of sites**</td>

</tr>

<tr>

<td>Text only</td>

<td align="center">5.0</td>

</tr>

<tr>

<td>Text & graphics</td>

<td align="center">15.0</td>

</tr>

<tr>

<td>Text, graphics & phots.</td>

<td align="center">65</td>

</tr>

<tr>

<td>Text & graphics or phots, + sound or video</td>

<td align="center">15.0</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**100.0**</td>

</tr>

</tbody>

</table>

The percentage of sites using more than just text and graphics is somewhat higher than that of the sample companies, shown in table 4\. The shopping centres appear to spending more time and effort in designing their Web pages. The majority of the shopping centres investigated used good menu layouts to make it easy for the user to navigate the sometimes extensive sites. Several contained search engines to help the user find what he or she wants as quickly as possible. An example of this is the [Virtual Lynx Mall](http://www.virtualynx.com). Many of the sites investigated also contained features designed to make them more attractive to the potential customer. Most sites contain 'hot links' and sometimes relevant links to other centres. Three of the sites run 'classified ads' sections for users wishing to buy and sell items from each other. Fun and entertainment pages are common. By using multi-media and the features described above, the shopping centres are attempting to make a visit to their site an interesting experience, somewhat similar to a real shopping expedition. Attractive and entertaining sites will attract customers back, possibly to spend more money.

## The Electronic Mail Questionnaire

Information from the companies whose Web sites were examined was collected by sending out a brief questionnaire by e-mail to either contact names or offices identified on the company's pages. The questions covered:

*   internet access;
*   length of lime with WWW presence;
*   purpose of WWW site;
*   effect of Web Site on turnover;
*   problems facing electronic commerce; and
*   the future of Web sites.

### Internet access

The companies were asked to state how long they had had access to the Internet in general. The results were:

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">Table 8: Time span of access to the Internet (*=rounding error)</caption>

<tbody>

<tr>

<td alilgn="center">**Time span**</td>

<td align="center">**% of respondents**</td>

</tr>

<tr>

<td>less than three months</td>

<td align="center">9</td>

</tr>

<tr>

<td>three months to six months</td>

<td align="center">21</td>

</tr>

<tr>

<td>over six months to one year</td>

<td align="center">25</td>

</tr>

<tr>

<td>more than one year to two years</td>

<td align="center">24</td>

</tr>

<tr>

<td>more than two years</td>

<td align="center">22</td>

</tr>

<tr>

<td align="center">**Total**</td>

<td align="center">**101***</td>

</tr>

</tbody>

</table>

### Length of time with WWW Presence

Like the Internet, business use of the WWW has grown massively since its launch two years ago. The sample companies were questioned about how long they have had a Web presence. The results were that 51.5% of the respondent companies had had their Web sites for less than six months (21% less than three months), with a further 28.9% having had theirs for less than a year. Only the remaining 19.6% have used this facility of the Internet for more than a year. This was the expected distribution of results, since the WWW is a recent development and has grown particularly quickly in the last year.

### Purpose of WWW site

The companies were asked what was the purpose of their Web sites. The classes shown in Table 9 were presented in the questionnaire and respondents were allowed to select more than one category.

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">**Table 9: Business use of Web sites**</caption>

<tbody>

<tr>

<td alilgn="center">**Purpose of site**</td>

<td align="center">**% of respondents**</td>

</tr>

<tr>

<td>General publicity</td>

<td align="center">73.2</td>

</tr>

<tr>

<td>Advertising specific products and services</td>

<td align="center">73.2</td>

</tr>

<tr>

<td>On-line selling of products and services</td>

<td align="center">41.2</td>

</tr>

<tr>

<td>Customer support and liaison</td>

<td align="center">43.3</td>

</tr>

</tbody>

</table>

### Effect of Web site on turnover

The number of companies setting up site on the WWW has grown exponentially in the last two years. Electronic commerce appears to have a great deal to offer the innovative businessman or woman. However, many of the sites surveyed in the last chapter are purely vehicles for basic publicity and advertising. To find out if owning a Web site is currently making any difference to business, the companies were asked if there had been any effect on turnover as a result of the Web site.

Just over a third (36%) of the companies stated that their WWW sites had had no effect on turnover whatsoever. This is slightly higher than the 31% who stated that the Web site did make a difference. The remaining third (33%) of companies stated that they did not know whether the site had had any effect. Several of these respondents stated that it was far too soon to tell. Considering that many of the companies have had their sites only a few months this is hardly surprising. In some cases it would be impossible to tell if the presence on the WWW had had any effect at all. This might be the case for very large corporations who use the Web simply for publicity purposes. It does appear that there is money to be made on the Web if the right approach is taken, but many companies have yet to find the right strategy. The companies who stated that their Web sites had had a positive effect on turnover were from many different industry sectors and using their site for several of the purposes earlier. The vast majority of these involved basic publicity and marketing, advertising with price information (but with conventional ordering) and on-line ordering of goods and services.

### Problems facing electronic commerce

A number of problems must be overcome before electronic becomes a dominant form of business. The companies were asked what they believed to be the main hurdles to the success of electronic commerce. The following categories were presented for selection:

*   Development of secure sites
*   Development of suitable payment systems (e.g. digital cash)
*   Faster connection times
*   Wider access
*   Information overload for potential customers
*   The unattractiveness of on-line shopping
*   Other

The companies were asked to select one or more of the categories. The results are shown in figure 1:

![](diagr.gif)

Figure 1: Problems facing electronic commerce

It is clear that many respondents believe that the most important problems facing the future of electronic commerce are security (53.6%) and payment issues (47.4%). This confirms the authors' own opinion and agrees with many others in the field. These businessmen and women know that the general public will not be comfortable with purchasing products over the Internet until they can do so safely. The problem of payment is more of a long term issue, with all its associated legal and social problems.

The issue of access is viewed as the next most important problem (43.3%). Many businesses feel that it is necessary for more potential customers to gain access to the Web in general. One respondent commented that his target audience were not even on the Web yet.

The technical difficulties that users are currently experiencing are seen as a problem by 38.1% of respondents. Until connection times become faster, the Web will remain frustrating to many users. They may lose interest while waiting for a page with high multi-media content to download. As technology progresses this problem will undoubtedly disappear. Information overload is not seen as a great problem, with only 21.6% of respondents citing it as such. Most appear to believe that you cannot have too much of a good thing. The businesses questioned do not believe that on-line shopping is inherently unattractive to the general public, with only 16.5% stating that this is likely to hold back development of electronic commerce. However, as these companies already have Web sites it is not surprising to learn that the majority of them feel on-line shopping is something which people will enjoy.

22.7% of respondents argued that there are other important problems facing electronic commerce. Several respondents were concerned that the business world as a whole will not accept the Internet and WWW as a valid commercial medium. The following comments illustrate this point:

"The Web is often viewed by the business community as not being useful" and "Some companies are slow to change".

Another concern was that users need to get used to the concept of on-line shopping and the Web in general, before they are comfortable purchasing goods on-line. Several companies complained that their site was not being accessed often enough to make it profitable. One respondent stated:

"I find people just stumble across my Webpage while surfing, look at it and then go on without putting it on their hotlist for future reference"

This comment also illustrates the need for better directories and organisation of information. Another respondent wrote:

"Currently there are no real attempts at analysing and targeting specific audiences on the net. Therefore the information is scattered, incomplete and sometimes totally incoherent."

A few respondents felt that more Web tools and applications would have to be developed before electronic commerce really starts to make an impact.

### Future of Web sites

The companies were asked to describe how they see their Web sites developing in the future. The ideas given varied widely. The classification scheme given below was used to group the comments. The majority fell into the classes given, but it was necessary to include an 'other' class for a few ideas which did not appear to fit anywhere. The classes and number of respondents (out of 90 replies to this question) are given in the table below:

<table align="center" bgcolor="#FBFFDF" border="1" cellpadding="5"><caption align="bottom">**Table 10: Future enhancements to Web sites**</caption>

<tbody>

<tr>

<td alilgn="center">**Enhancement**</td>

<td align="center">**% of respondents**</td>

</tr>

<tr>

<td>More interaction with users</td>

<td align="center"></td>

</tr>

<tr>

<td>More general content to be added</td>

<td align="center">13.6</td>

</tr>

<tr>

<td>The addition of more products and services</td>

<td align="center">11.0</td>

</tr>

<tr>

<td>The increased use of multi-media</td>

<td align="center">11.0</td>

</tr>

<tr>

<td>More business</td>

<td align="center">8.5</td>

</tr>

<tr>

<td>The inclusion of new technologies</td>

<td align="center">6.8</td>

</tr>

<tr>

<td>The addition of 'fun pages'</td>

<td align="center">5.9</td>

</tr>

<tr>

<td>The addition of links to and from the page</td>

<td align="center">5.1</td>

</tr>

<tr>

<td>Sharing of information and building a community</td>

<td align="center">5.1</td>

</tr>

<tr>

<td>On-line ordering and shopping</td>

<td align="center">4.2</td>

</tr>

<tr>

<td>Uncertain of future direction</td>

<td align="center">4.2</td>

</tr>

<tr>

<td>More advertising</td>

<td align="center">3.4</td>

</tr>

<tr>

<td>The inclusion of forms</td>

<td align="center">2.5</td>

</tr>

<tr>

<td>The site will remain as it is</td>

<td align="center">2.5</td>

</tr>

<tr>

<td>Others</td>

<td align="center">3.4</td>

</tr>

</tbody>

</table>

It can be seen from table one that there is a wide distribution of ideas for future improvement of the respondents' Web sites. It seems that becoming more interactive with other users of the WWW is something that several of companies are interested in. One respondent envisages the following scenario:

"Increased use of live applications to interact with customers and suppliers and distributors"

Another states:

"We hope to move ahead from merely providing information to becoming a more interactive location with greater use of transactions"

The addition of more product and service information in a more exciting format will occur at a number of the sites:

"We see our complete product line being showcased in detail, including photos, complete specs and current prices" Some respondents were not particularly helpful, just stating that their sites would produce more business. How this was to be achieved was not discussed. Other companies noted that their sites will only develop as fast as technology will allow and that more links to their site are required so more potential customers can find their way there. It is also clear from a number of comments that businesses do see the need for their sites to become more entertaining. They know that they must become part of the virtual community by adopting an attitude of 'information-sharing'. One Internet consulting group sees itself as "a centre for a virtual community".

In addition to specific comments about their Web sites, a number of respondents gave more philosophical statements about electronic commerce as a whole. A selection of these are given below:

"We are here to secure a place or domain. The rate at which the Internet is growing could mean that there are no more domains left in two years. We feel it will take about two years until we see any real business as a result of the Web. We wanted to be in place when the big rush starts."

"At the moment, the commercial application of the Internet is probably 85% hype and 15% substance. We feel this will change with wider use and awareness of its potential."

"Sales are dismal and barely cover the cost of maintaining the Web page site through my Internet provider ... I do not see the Internet being a viable marketing tool for products other than computer products for at least 20 years."

## Conclusions

This study has illustrated clearly that electronic commerce is still very much in an embryonic stage. Of the 300 companies surveyed, only 11.7% were engaged in on-line selling of goods or services with the transmission of credit card details over the Internet. Before electronic commerce becomes a dominant form of business it is clear that the issue of security must be addressed. Customers do not feel comfortable sending sensitive information over the Internet and companies are not generally asking them to do so. The number of secure sites currently in place on the Web is relatively low (although the number is growing all the time). Shopping centres appear to be have adopted the technology currently available for secure transmission of data more quickly than individual businesses on the Web. Once the security problem has been overcome the issue of alternative payment systems must be addressed.

The multi-media capabilities of the WWW are not yet being fully exploited, the majority of sites using text, photos and graphics but not sound or video. As technology progresses and the design of Web sites becomes more of an art form this will surely change. Electronic mail is widely used at most of the sites investigated and will remain one of the most important aspects of the Internet, especially in relation to business use.

The creation of a typology of business use of the WWW (based on the Yahoo! directory) confirmed the view that many of the businesses currently owning Web sites are from Computer and Internet-related sectors. However, the Business activities and services sector appears to increasing its share of the overall total, which has doubled in the last threee months alone.

It will be extremely interesting to watch how businesses develop their Web sites in the future and how technological developments are received and incrporated. However, it is clear that the business as a whole has some way to go before true electronic commerce is achieved.

The results obtained from the electronic mail questionnaire made a useful supplement to the findings of the main surve. Most of the respondent companies have had access to the Internet for less than two years and been on the Web for under a year. The majority of the companies responding to the questionnaire are currently using their sites for publicity and advertising purposes. This is in direct agreement with the findings of the main study. Of the two-thirds of companies who knew the difference their Web sites had made to turnover, half of them stated that the effect had been positive. It appears, therefore, that there is profit to made on the Web, but this depends very much on what is being sold and in what manner.

Note: The descriptions of Web sites were correct at the time of the survey, but many are currently developing. Their content, layout, and even address may change.

## References

<a name="cso">Central Statistics Office (1992)</a> Standard Industrial Classification of Economic Activities. London: HMSO

<a name="com">CommerceNet (1995)</a> ['CommerceNet backgrounder'](http://www.commerce.net/information/background.html)

<a name="cor">Cortese, R. (1995)</a> 'Cyberspace - crafting software that will let you build a business out there' Business Week, 27 February, 34-40

<a name="der">Dern, D. P. (1994)</a> ['Myth or menace, a history of commerce on the Net'](http://www.mecklerweb.com:80/biz/biztoc.html) Internet World

<a name="fie">Fielding, H. (1994)</a> 'A non-anorak's guide to the Internet' The Independent, 29 May.

<a name="keh">Kehoe, L. (1994)</a> 'Doing cyber business' Financial Times, 6 June.

<a name="min">Minio, R. (1994)</a> 'Businesses and the Internet; some interesting experiments' Management and Technology, 2 (1), 11-13.

<a name="pat">Patch, K. (1994)</a> 'Businesses spin Web for distribution' PC Week, 11 (25), 73-75

<a name="ros">Rosen, N. (1994)</a> 'Internet's global growth' Sunday Times 13 March.

<a name="tay">Taylor, F. (1994)</a> ['Business and the Internet'](http://www.trinet.com/tonc/inetbusiness.html)

<a name="tet">Tetzeli, R. (1994)</a> 'The Internet and your business' Fortune 5 (129), 86-91

<a name="tig"></a>[The Internet Group (1994)](http://www.tig.com)

<a name="wat">Watson, I. (1994)</a> 'Commercial uses of the Internet' Managing Information, 4 (1), 24-25

<a name="wel">Welz, G. (1995)</a> ['New Deals'](http://www.mecklerweb.com/plweb-cgi/idoc.pl?188+unix+_free_user_+www.iworld.com..80+iWORLD+iWORLD+Internet_World+Internet_World++Welz) Internet World

<a name="win">Winder, D. (1995)</a> All you need to know about the World Wide Web. Bath: Future Publishing.

<a name="yah"></a>[_Yahoo!_ Directory (1995)](http://www.yahoo.com)

* * *