#### vol. 15 no. 1, March, 2010



# The digital press archives of the leading Spanish online newspapers



#### [Javier Guallar](#authors) and [Ernest Abadal](#authors)  
Universitat de Barcelona, Facultat de Biblioteconomia i Documentació,  
C/ Melcior de Palau, 140, 08014 Barcelona, Spain.

#### Abstract

> **Introduction.** This paper analyses the level of development of the press archives of the thirteen Spanish newspapers with the largest digital circulations.  
> **Method.** This is evaluative research based on twenty indicators divided into four main sections: general features, the query system, presentation of results and other features. The benchmarks for all the indicators were the _Lexis-Nexis_, _Factiva_ and _My News_ databases.  
> **Analysis.** The press archives of online newspapers were analysed to determine their level of development and the search and browsing facilities they offer according to a set of quality indicators satisfied by professional press databases. The sample consisted of the thirteen newspapers with the largest circulations according to the figures provided by the organisations that audit the Internet press audience. The analysis was carried out from June to August 2007\.  
> **Results.** The level of development of digital press archives is not homogeneous. Of the newspapers studied, five showed a high or medium-high level of development, though not as high as that of the best commercial products. These were _La Vanguardia_, _ABC_, _El País_, _El Mundo_ and _El Periódico_, all of which are online versions of prestigious general information newspapers published in print format. Seven newspapers showed a low or medium-low level of development. These were three daily sports papers (_As_, _Sport_, _Mundo Deportivo_), three exclusively digital newspapers (_Periodista Digital_, _20 Minutos_ and _Libertad Digital_) and one regional newspaper (_La Verdad_). Finally, one sports paper (_Marca_) had no search system by keywords.  
> **Conclusions.** Fewer than half of the major Spanish newspapers show high or medium levels of development of their digital press archives, though not as high as that of the best press databases, and the rest are still far from satisfying the requirements of a professional search system. The indicators established may also be useful for analysing the situation in other countries.


## Introduction

In their short history online newspapers have shown their strong, dynamic nature through constant innovation. Whereas the first online editions of newspapers in the mid-1990s mostly consisted of the print edition with very few changes, the present-day ones may include immediate updating of information, multimedia content and a variety of feedback services.

Access to the content of digital archives is one of the specific new services offered by the digital press. A variety of systems and options are now offered for consulting and viewing back issues. However, the digital press shows a similar attitude to that of the printed press: it considers press archives to be important but not fundamental. Since the 1990s many studies have been made on the way in which online newspapers use the Web to disseminate their contents but few focusing specifically on digital press archives. We will now review several works that have dealt with the subject using a variety of approaches and geographic perspectives.

Cowen described the current situation of British newspapers on the World Wide Web in 2001 and evaluated their future prospects on the basis of interviews with journalists and specialists. She studied the Web sites of five newspapers, and applied sixteen evaluation criteria, including one for digital archives (Cowen [2001](#cow01): 193-194). At that time, all the newspapers analysed allowed their archives (going back one to six years) to be searched free of charge and without registering. Cowen highlighted particularly the content and search facilities of the _[Financial Times](http://www.ft.com/home/uk)_ Web site.

Longo ([2006](#lon06)) reviewed the presence of Italian press archives on the Internet from the viewpoint of a specialised documentalist or researcher. Her non-exhaustive study presents the situation of two main areas: international databases and the media's own press archives. She first analysed the Italian newspapers whose content is marketed by the main international distributors (_Factiva_, _Lexis-Nexis_ and _Dialog_). She then described the facilities of the press archives of the main national and regional online newspapers, and reported that digital press archives have started to charge, or at least require registration, for access to back issues.

Among the most important studies of the Spanish press are those by Fuentes, González Quesada and Jiménez López. Their line of research analyses the value-added services of Spanish newspapers that have an online edition, including the consultation of press archives. In two of their first works ([Fuentes and González Quesada 1998](#fue98); [Jiménez López _et al_.1999](#jim99)), they carried out an exhaustive study of Spanish newspapers with online versions, analysing indicators such as the coverage of the search system and the operators that it offered. Jiménez López _et al_. ([2000](#jim00)) compared a selection of Spanish press archives with others in the rest of the world and described more than fifty Spanish online newspapers. Just over thirty of them had a digital press archive, but none was comparable to the best of other countries (particularly in the USA), analysed in terms of temporal coverage and search features. The authors also pointed out major differences between the leading Spanish general information newspapers and suggested that these differences could be due to what they considered to be a transitional stage in the formation of digital press archives. Jiménez López ([2003](#jim03)) described the coverage and conditions of subscription to press archives at that time. Garcia Gómez and González Olivares ([2001](#gar01)) studied the search systems of four Spanish online newspapers by making ten searches on current issues and analysing the exhaustiveness and precision of the results. López Carreño ([2004](#lop04)) considered press archives within her categorisation of digital press services into three groups: information products, document products and value-added services such as access to information adapted to the needs of users. Based on the analysis of online newspapers, she established a newspaper portal model with three levels of service development: basic, intermediate and advanced. She included digital press archives in the basic level of services that should be offered.

Since these studies were carried out, the digital press has obviously continued to evolve, but no research has been done in the last five years to analyse the current or recent situation of Spanish press archives. Only in the annual descriptions of the state of the digital press by Guallar ([2007](#gua07), [2008](#gua08), [2009](#gua09)) is mentioned the perception (from professional use rather than a scientific study) that the search systems of digital newspapers were clearly inferior to those of professional international press archive databases such as _Lexis-Nexis_ and _Factiva_, and the Spanish _My News_. For example, searches in the archives of _El País_ offered worse results than searches in the _My News_ press database. The study stated that '_it would be interesting to have up-to-date scientific data on this (i.e. a comparative study of the real features of the leading Spanish digital media)_' ([Guallar 2007](#gua07): 116). This request was addressed in our study.

## Objectives

We set out to determine the level of development and facilities of the digital archives of the leading Spanish online newspapers. We took as a benchmark the most highly developed professional databases in the sector (the international _Lexis-Nexis_ and _Factiva_, and the Spanish _My News_), which provide a wide coverage of newspapers from around the world or from Spain. and which also offer among the best search features available in the market, capable of satisfying the most demanding professional needs.

This study can be useful for users (for example, journalists, librarians, researchers and students) who use digital press archives for their work and also for developers of this kind of portal. Our analysis shows the level of development of leading Spanish newspapers and this can be useful in helping users decide which press archive would be better for their needs, as well as helping developers improve the features of their product.

## Methods

The content of digital press archives was analysed to determine their degree of compliance with a set of indicators. Our study is thus different from those carried out by Lin and Jeffres ([2001](#lin01)), who performed a content analysis of 422 Web sites of newspapers, radio stations and television stations in the largest metropolitan areas of the USA, or Stryker _et al_. ([2006](#str06)), who analysed the treatment of cancer in the press through searches of press databases. These studies required mechanisms to ensure a high degree of reliability among the analysts, to achieve good rates of coincidence, and so on. In our case, on the other hand, it was only necessary to determine the presence or absence of indicators on which both authors performed the analysis and agreed the results.

The sample consisted of the thirteen newspapers with the largest circulations according to the figures provided by the Spanish organisations that audit Internet press audience. The analysis was carried out from June to August 2007.

### Choice of the samples

The two most important systems for measuring press circulation and audience in Spain are the [_Estudio General de Medios_](http://bit.ly/clZoeU) (General Media Study) and the [_Oficina de Justificación de la Difusión_](http://bit.ly/a6zD00) (Circulation Justification Office).

The _Estudio General de Medios_ is an audience-measurement system based on surveys of users. The twenty-five most visited Spanish Web sites in February-March 2007 included eleven online newspapers: _Marca_, _El País_, _As_, _Mundo Deportivo_, _Sport_, _La Vanguardia_, _ABC_, _El Periódico_, _El Correo_, _Expansión_ and _Norte de Castilla_. However, this list does not include some online newspapers, such as the newspaper _El Mundo_, because they do not accept its measurement system.

The _Oficina de Justificación de la Difusión_ measurement system is based on the visits to the Web sites of online newspapers. According to the figures it published for March 2007, the following are the twenty Spanish online newspapers with the largest circulations: _El Mundo_, _Marca_, _20 Minutos_, _ABC_, _Periodista Digital_, _Libertad Digital_, _Sport_, _La Verdad_, _Ideal_, _El Correo_, _El Periódico de Catalunya_, _Las Provincias_, _El Confidencial_, _La Voz de Galicia_, _El Comercio_, _La Razón_, _La Nueva España_, _Expansión_, _Hoy-Diario de Extremadura_ and _Europa Press_. In this case, the media that do not accept its control are the newspapers _El País_, _As_, _La Vanguardia_ and _Mundo Deportivo_.

To obtain a sample that was as representative as possible and did not leave out any of the most important newspapers (other than those that exclude themselves voluntarily from both lists), it was decided to combine the eight first results of each list. This gave a sample of thirteen digital media: _Marca_, _ABC_ and _Sport_ are common to both lists; _El País_, _As_, _Mundo Deportivo_, _La Vanguardia_ and _El Periódico_ are only in the _Estudio General de Medios_ list; and _El Mundo_, _20 Minutos_, _Periodista Digital_, _Libertad Digital_ and _La Verdad_ are only in the _Oficina de Justificación de la Difusión_ list.

The sample is sufficiently varied because it includes general and sports press, national and regional press, paid and free press, and exclusively digital and digital-print press.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: The sample of newspapers analysed**  
</caption>

<tbody>

<tr>

<th align="center" width="20%">Name</th>

<th align="center" width="20%">Subject coverage</th>

<th align="center" width="20%">Geographic coverage</th>

<th align="center" width="20%">Paid/Free press</th>

<th align="center" width="20%">Format</th>

</tr>

<tr>

<td>[20 Minutos](http://www.20minutos.es/archivo)</td>

<td>general</td>

<td>national</td>

<td>free</td>

<td>digital-print</td>

</tr>

<tr>

<td>[ABC](http://www.abc.es/hemeroteca)</td>

<td>general</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[As](http://www.as.com/busca/buscando.html)</td>

<td>sport</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[Libertad Digital](http://www.libertaddigital.com/servicios/buscador)</td>

<td>general</td>

<td>national</td>

<td>free</td>

<td>digital</td>

</tr>

<tr>

<td>[Marca](http://www.marca.com)</td>

<td>sport</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[El Mundo](http://www.elmundo.es/hemeroteca)</td>

<td>general</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[Mundo Deportivo](http://www.elmundodeportivo.es)</td>

<td>sport</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[El Pais](http://www.elpais.com/archivo/buscador.html)</td>

<td>general</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[El Periodico](http://www.elperiodico.com/info/archivo)</td>

<td>general</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[Periodista Digital](http://www.periodistadigital.com)</td>

<td>general</td>

<td>national</td>

<td>free</td>

<td>digital</td>

</tr>

<tr>

<td>[Sport](http://www.sport.es/buscador.asp)</td>

<td>sport</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[La Vanguardia](http://www.lavanguardia.es/hemeroteca)</td>

<td>general</td>

<td>national</td>

<td>paid</td>

<td>digital-print</td>

</tr>

<tr>

<td>[La Verdad](http://www.laverdad.es/murcia/hemeroteca)</td>

<td>general</td>

<td>regional</td>

<td>paid</td>

<td>digital-print</td>

</tr>

</tbody>

</table>

### Indicators for the evaluation

Digital press archives are databases. In this field, we can highlight four texts: two dealt with the process of consulting databases from the viewpoint of the process followed by the user ([Marchionini 1995](#mar95) and [Shneiderman _et al._ 1997](#shn97)), and another two presented the characteristics of a good query interface ([Nielsen and Loranger 2006](#nie06) and [Morville and Rosenfeld 2006: 145-192](#mor06)). Based on these studies, an original proposal was made of the fundamental elements for database query interfaces ([Abadal 2002](#aba02)), which has been taken into account for the present study.

Our evaluation proposal is based on twenty indicators divided into four main sections: general features, the query system, presentation of results, and other features. Table 2 includes a brief description of each indicator and the way it is evaluated.

The two most important features for users in a database are the search facilities and the presentation of results. Because of that, we have established a higher number of indicators for these. Although we have considered these features as critical points for the evaluation of databases, we have also included other indicators to complete the evaluation: for example, temporal coverage (another critical issue) and others that describe press archives without evaluating them (indicators 1.1, 1.2, 4.1 and 4.3 in Table 2).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Indicators for analysing digital press files**  
</caption>

<tbody>

<tr>

<th width="20%">Section</th>

<th width="30%">Indicator</th>

<th width="50%">Description</th>

</tr>

<tr>

<td>General features</td>

<td>1.1\. Name and location</td>

<td>The name that the newspaper gives to its search system or systems, and its location on the home page. This indicator was not used for the evaluation.</td>

</tr>

<tr>

<td> </td>

<td>1.2\. Information on the technology used</td>

<td>Information and explanations on the program used and its accountability (whether it is an in-house or external system). This indicator was not used for the evaluation.</td>

</tr>

<tr>

<td> </td>

<td>1.3\. Coverage</td>

<td>The temporal coverage of the press archive. The proportion of the archive that is accessible online and whether it had a coverage of at least five years.</td>

</tr>

<tr>

<td>Query system</td>

<td>2.1\. Types of search</td>

<td>Types of query by keyword available. Whether the press archive had more than one search option, such as simple and advanced.</td>

</tr>

<tr>

<td> </td>

<td>2.2\. Combination of search terms</td>

<td>Advanced features for search terms: Boolean operators, search for literal expressions, proximity operators, use of parentheses to increase combinations, and adjusting the relevance percentage of the query. In order to fully satisfy this indicator, the system must offer at least searches with Boolean operators and literal expressions.</td>

</tr>

<tr>

<td> </td>

<td>2.3\. Search by date</td>

<td>Facilities for search by date. It considers whether the system allows the exact dates of the search to be defined.</td>

</tr>

<tr>

<td> </td>

<td>2.4\. Search by collection</td>

<td>Ability to differentiate between global and partial searches of the collections in the different sections of the newspaper, supplements, etc.</td>

</tr>

<tr>

<td> </td>

<td>2.5\. Search by document field</td>

<td>Ability to differentiate between global or partial searches in fields such as title, author and section.</td>

</tr>

<tr>

<td> </td>

<td>2.6\. Reusing search strategies</td>

<td>Facilities for saving previous queries, if the system has a search history option.</td>

</tr>

<tr>

<td> </td>

<td>2.7\. Search by browsing</td>

<td>Possibility of access to the document by browsing issues of the newspapers.</td>

</tr>

<tr>

<td> </td>

<td>2.8\. Help</td>

<td>Existence of explanatory texts on the use of the search system.</td>

</tr>

<tr>

<td>Presentation of results</td>

<td>3.1\. Management of the results lists</td>

<td>Options for managing the results, such as ordering them by relevance and date or limiting the number of results per page.</td>

</tr>

<tr>

<td> </td>

<td>3.2 Document fields</td>

<td>Number and type of fields shown in each result (e.g. author, title, date, section, etc.). A search engine is considered to satisfy this indicator if it offers at least four fields.</td>

</tr>

<tr>

<td> </td>

<td>3.3\. Identification of the search terms in the document</td>

<td>Highlighting of the search terms in the results or in the document.</td>

</tr>

<tr>

<td> </td>

<td>3.4\. Choice of formats for viewing the document</td>

<td>Different document formats (html, pdf, etc.).</td>

</tr>

<tr>

<td> </td>

<td>3.5\. Options for managing the documents obtained</td>

<td>Different ways of managing the documents obtained: sending by email, printing, saving, obtain use statistics, evaluating or commenting on the news item, sharing it (sending it to social Web sites), etc. A search engine is considered to satisfy this indicator if it offers at least four options.</td>

</tr>

<tr>

<td> </td>

<td>3.6\. Presentation of related documents</td>

<td>Obtaining documents related to the results in the same press archive or external sources.</td>

</tr>

<tr>

<td>Other features</td>

<td>4.1\. Accessibility</td>

<td>Degree of compliance with accessibility standards that allow disabled persons to consult Web sites. For the quantitative analysis the _Tawdis_ program was used. This indicator was not used for the evaluation.</td>

</tr>

<tr>

<td> </td>

<td>4.2\. Visibility</td>

<td>The impact of the press archive page is evaluated on the basis of the number of links to it from Web pages on other domains. The _Yahoo Site Explorer_ service was used to count them.</td>

</tr>

<tr>

<td> </td>

<td>4.3\. Cost</td>

<td>Free or paid access to the press archive. This indicator was not used for the evaluation.</td>

</tr>

</tbody>

</table>

The benchmarks for all the indicators were the _Lexis-Nexis_, _Factiva_ and _My News_ databases (see above). The unit of analysis was the set of pages of the press archive section of the newspaper.

## Analysis

### General features

#### Name and location

The terms _archivo_ (archive) and _hemeroteca_ (newspaper library) are both used (in four and six cases, respectively). In fact, _ABC_ and _El Periódico_ use both names to differentiate the two different search systems that they offer on the same Website. In cases in which there is no specific section, the term _search_ or _search engine_ is normally used to inform users that they can consult back issues.

#### Information on the technology used

The newspapers offer little or no information on the technology used to manage their digital press archives. _ABC_, _El Periódico_ and _Sport_ all offer two search systems on their Web sites, one of their own, on which they provide no information, and one contracted to the well-known Spanish press database _My New_s. _El Mundo_ uses the _Autonomy_ search engine, version beta v2, and _La Verdad_ uses the _Sarenet_ search system. _Libertad Digital_, _Periodista Digital_ and _20 Minutos_ use the _Google_ and _Yahoo!_ search engines, linking to themselves. They state, sometimes very explicitly, that their Web sites decline all responsibility for the results of the search system.

#### Coverage

Spanish newspapers have increased the scope of their digital archives, but still do not offer the whole of their collections, with the important exceptions of _El País_ and _La Vanguardia_, which have placed online the whole of their print archive since their foundation in 1976 and 1881, respectively. _ABC_ offers its collection from 1996 and _El Mundo_ from 1994\. _El País_ presents its whole print archive in html, whereas _La Vanguardia_ presents it in pdf files. The newspapers mostly offer a shorter period of coverage for the archives of their digital versions (Table 3), which go back between one and seven years.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Temporal coverage of Spanish online newspapers**  
</caption>

<tbody>

<tr>

<th width="30%">Newspaper</th>

<th width="35%">Print edition</th>

<th width="35%">Digital edition</th>

</tr>

<tr>

<td>_20 Minutos_</td>

<td>January 2005 (pdf)</td>

<td>January 2005</td>

</tr>

<tr>

<td>_ABC_</td>

<td>June 1996 (html) / Last 15 days (pdf)</td>

<td>January 2002</td>

</tr>

<tr>

<td>_As_</td>

<td>July 2004 (html) / January 2004 (pdf)</td>

<td>July 2004</td>

</tr>

<tr>

<td>_Libertad Digital_</td>

<td>n/a</td>

<td>No information available</td>

</tr>

<tr>

<td>_Marca_</td>

<td>No information available</td>

<td>No information available</td>

</tr>

<tr>

<td>_El Mundo_</td>

<td>January 1994 (html) / October 2002 (pdf)</td>

<td>January 2000</td>

</tr>

<tr>

<td>_Mundo Deportivo_</td>

<td>January 2004 (html) / Last 15 days (front page) (pdf)</td>

<td>January 2004</td>

</tr>

<tr>

<td>_El País_</td>

<td>4 May 1976 (html) / 25 July 2001 (pdf)</td>

<td>No information available</td>

</tr>

<tr>

<td>_El Periódico_</td>

<td>January 2000 (html, pdf)</td>

<td>January 2006</td>

</tr>

<tr>

<td>_Periodista Digital_</td>

<td>n/a</td>

<td>September 2006</td>

</tr>

<tr>

<td>_Sport_</td>

<td>January 2005 (pdf)</td>

<td>January 2006</td>

</tr>

<tr>

<td>_La Vanguardia_</td>

<td>January 1999 (html) / February 1881 (pdf)</td>

<td>November 2000</td>

</tr>

<tr>

<td>_La Verdad_</td>

<td>January 2006 (html)</td>

<td>January 2006</td>

</tr>

</tbody>

</table>

### The query system

#### Types of search

Nine of the digital press archives offer two types of search, simple and advanced, though the names used to refer to them vary. Three others offer a single option, in _La Vanguardia_ an advanced search and _20 Minutos_ and _Libertad Digital_ a simple _Google_ and _Yahoo!_ search, respectively. _ABC_, _El Periódico_ and _Sport_ each have two search systems on their Web sites: a _My News_ search and a proprietary one. Finally, _Marca_ is the only digital press archive that has no search engine: users must browse instead.

#### Combination of search terms

All the retrieval systems use Boolean operators, either shown directly (AND, OR, NOT) or masked through expressions (search for any words, all words, exact phrase, etc.). However, they lack some of the more sophisticated functions offered by professional press archives, such as using parentheses to increase the combination of terms or adjusting the relevance of the results. The only exception to this is _El Mundo_, which offers both of these options.

#### Search by date

Six of the digital press archives allow users to choose the search dates through a drop-down menu with closed options (today, last week, last month, etc.) or between two exact dates. Two of them only offer the possibility of searching between two dates (_La Vanguardia_ and _Mundo Deportivo_), one offers a drop-down menu (_El Mundo_), and two do not have this option at all (_20 Minutos_ and _Libertad Digital_).

#### Search by collection

Ten of the digital press archives offer the possibility of restricting the search to a given collection (print, digital, supplements, etc.), though the scope of this function varies. _El País_, _La Vanguardia_ and _ABC_ offer a large number of collections. For example, _El País_ offers: print edition with its sections, supplements and regional editions (Madrid, Cataluña, etc.), digital edition with its sections, other media from _Prisa_ group (_Cadena Ser_, _Cinco Días_, etc.), and search by multimedia collections (photographs, videos, etc.). _El Mundo_, _La Verdad_ and _El Periódico_, offer a medium number; and the rest offer few collections or do not have this option at all (_20 Minutos_ and _Sport_).

#### Search by document field

This option, which is common in professional databases, is only found in four of the thirteen systems analysed: _As_, _Mundo Deportivo_, _El País_ and _La Vanguardia_. It is noteworthy that some of the best query systems, such as those of _El Mundo_ and _ABC_, do not have this function. This is a very important shortcoming in which most of the newspapers show room for improvement.

#### Reusing search strategies

None of the digital press archives has search history options for reusing previous search strategies.

#### Search by browsing

All the digital press archives offer access through browsing, generally by means of monthly calendars (including _Marca_, for which it is the only system). Originally the only system in many cases, this form of access is now complementary to searches by keywords.

#### Help

Only six digital press archives provide instructions for the search function, and in some cases these are very simple. It is surprising that such a basic element for users of a query system has been neglected by half the newspapers studied, particularly _El País_, which is among the leaders in most indicators. This is one of the main shortcomings of the systems analysed.

#### General evaluation

The query systems of the digital press archives analysed are at an intermediate level of development in comparison with the best commercial databases. They include the main Boolean operators but lack the more sophisticated options such as the use of parentheses to increase the combination of terms and, with the sole exception of _El Mundo_, they do not allow the relevance to be adjusted. They offer good facilities for searching by date and by collections. However, features such as searching by document field and reusing search strategies through search histories, which are common in professional bibliographic databases, are almost non-existent in the main online newspapers. Finally, one of the main shortcomings is that half the archives in the sample lack help services, which are basic for users of query systems.

### Presentation of results

#### Management of the results lists

Seven of the online newspapers offer two functions that can be considered as basic in a professional query system: ordering of the results by relevance or by date, and selection of the number of hits displayed on each page. These are: _ABC_, _As_, _El Mundo_, _El País_, _El Periódico_, _Sport_ and _La Verdad_ (though, as stated above, _El Periódico_ and _Sport_ have two different search systems, and one of them does not offer this option). The other five, _20 Minutos_, _Libertad Digital_, _Mundo Deportivo_, _Periodista Digital_ and _La Vanguardia_, offer no results management system. This group includes the two digital newspapers and the free one, which in general show lower values in other indicators.

#### Document fields

Most of the digital press archives analysed show between four and six document fields, though _ABC_ shows eight fields in the Archivo de _ABC_ system, and _El Periódico_ shows only two in one of its query systems (but six in the other one). Except in one case, one of the fields is the headline, with a hyperlink to the news item.

#### Identification of the search terms in the document

This indicator shows low values overall, because half the sample do not have this feature. The other half highlight the search term in the lines of text displayed, and in one case also in the headline. The effectiveness of this feature depends on the amount of text shown in the result: four newspapers show only one or two lines of text and only two (_El País_ and _La Vanguardia_) show four and six lines, thus taking full advantage of this feature.

#### Choice of formats for viewing the document

Eight of the online newspapers with independent print and digital editions show their information in both the html format of the Web site and the pdf format of the print edition. However, two media with print and digital editions offer only html: _Mundo Deportivo_ (which offers only the front page of the print edition as a jpg image) and _La Verdad_. The two exclusively digital newspapers only present the information in html. The pdf format, therefore, is the standard for presentation of the print edition of the newspapers. Furthermore, _El Mundo_ and _El País_ offer specific versions in accessible text (or plain text) and for PDAs and mobile phones.

#### Options for managing the documents obtained

Two elementary document management options are very common: printing the news item and sending it by e-mail, which are found in eleven and ten of the digital press archives, respectively. The participatory services of a) voting and/or commenting on the news item, and b) sending it to social Web sites (_Digg_, _Meneame_, _Delicious_, _My Yahoo_, _Technorati_) are offered by about half the newspapers analysed (six and five, respectively). In this case, the most advanced media in Web 2.0 services are the free newspaper _20 Minutos_, the exclusively digital _Periodista Digital_ (both have shown a concern for this type of service for some time) and the traditional newspapers _ABC_, _El País_ and _La Vanguardia_. However, none of them allow part of the result to be selected for further operations (print, send, etc.), which must be performed individually, record by record.

#### Presentation of related documents

Eight press archives give access to a service of great interest: showing news related to the results. In six cases they offer only news from their own archives, but _El País_ and _Libertad digital_ go one step further by linking to related news in other media. Four do not offer this feature (_As_, _El Periódico_, _Sport_ and _La Verdad_). Linking to external Web sites, which is a common practice in other contexts such as blogs, is not common in the main online newspapers, due to their competing position in the market

#### General evaluation of the presentation of results

Only just over half the media consulted allow users to order the results by relevance and date, and to determine the number of results presented on each page. The press archives show on average between four and six fields for the document record. The fields offered in all cases were the date, the headline with a link to the news item, and a few lines of text (mostly short). Half of them highlight the search terms in the document record, and of these only two (_El País_ and _La Vanguardia_) take full advantage of it. All offer the texts in the html format of the Web and eight of them (the newspapers with a print version) also offer texts in pdf, which is thus consolidated as the standard for disseminating printed texts on the Internet. Two options for managing the documents obtained were offered by the vast majority: printing and sending by e-mail. The recently introduced service of sending them to social Web sites like _Digg_, _Technorati_ and _Delicious_ is becoming increasingly widespread, but is still offered by only half the newspapers. Finally, the presentation of news related to the results is widespread, but only _El País_ and _Libertad digital_ offer links to other newspapers outside their own press archive.

### Other features

#### Accessibility

The degree of accessibility of a Web page is measured by the ease with which any type of user can access its contents. The World Wide Web Consortium (W3C) has published a series of guidelines that allow persons with disabilities to access Web page content. These guidelines establish the requirements that must be fulfilled, for example, by animations and images (which are described with the ALT attribute) or hypertext links (the linked expression should have meaning outside the context: 'Click on the W3C report', rather than 'Click here'). Furthermore, they ensure accessibility regardless of the users' computer, screen, browser and type of connection. For the study we used the [TAW (Web Accessibility Test)](http://bit.ly/cS3epF), a tool for the analysis of Web sites based on the W3C - Web Content Accessibility Guidelines 1.0 (WCAG 1.0), which detects three types of accessibility errors for Web pages. Failure to comply with priority 1 means that one or more groups of users will find it impossible to access information in the document; failure to comply with priority 2 means that they will find it difficult to access information in the document; and failure to comply with priority 3 means that they will find it somewhat difficult to access information in the document.

The following table shows the results of the test. They are in general fairly poor, showing a lack of concern for this subject by the developers of the Web sites of online newspapers. The two values indicated for each priority refer to the number of manual and automatic errors.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Compliance with W3C accessibility guidelines**  
</caption>

<tbody>

<tr>

<th width="40%">Newspaper</th>

<th width="20%">P1</th>

<th width="20%">P2</th>

<th width="20%">P3</th>

</tr>

<tr>

<td>20 Minutos</td>

<td align="center">15-155</td>

<td align="center">10-354</td>

<td align="center">10-75</td>

</tr>

<tr>

<td>ABC</td>

<td align="center">0-159</td>

<td align="center">564-373</td>

<td align="center">12-37</td>

</tr>

<tr>

<td>As</td>

<td align="center">67-243</td>

<td align="center">235-455</td>

<td align="center">16-97</td>

</tr>

<tr>

<td>Libertad Digital</td>

<td align="center">23-118</td>

<td align="center">35-197</td>

<td align="center">2-17</td>

</tr>

<tr>

<td>Marca</td>

<td align="center">0-74</td>

<td align="center">43-326</td>

<td align="center">4-30</td>

</tr>

<tr>

<td>El Mundo</td>

<td align="center">10-90</td>

<td align="center">67-59</td>

<td align="center">18-36</td>

</tr>

<tr>

<td>Mundo Deportivo</td>

<td align="center">9-214</td>

<td align="center">55-311</td>

<td align="center">10-26</td>

</tr>

<tr>

<td>El Pais</td>

<td align="center">3-98</td>

<td align="center">230-129</td>

<td align="center">19-28</td>

</tr>

<tr>

<td>El Periódico</td>

<td align="center">6-320</td>

<td align="center">89-621</td>

<td align="center">1-39</td>

</tr>

<tr>

<td>Periodista Digital</td>

<td align="center">14-223</td>

<td align="center">54-305</td>

<td align="center">4-67</td>

</tr>

<tr>

<td>Sport</td>

<td align="center">3-151</td>

<td align="center">44-242</td>

<td align="center">1-24</td>

</tr>

<tr>

<td>La Vanguardia</td>

<td align="center">2-96</td>

<td align="center">27-107</td>

<td align="center">5-37</td>

</tr>

<tr>

<td>La Verdad</td>

<td align="center">6-79</td>

<td align="center">30-150</td>

<td align="center">17-59</td>

</tr>

</tbody>

</table>

#### Visibility

The aim of this indicator is to analyse the presence of the press archives on the World Wide Web based on the number of specific links to their Web sites.

Gao and Vaughan ([2005](#gao05)) analysed the visibility of four newspapers of the USA, Canada, China and Hong Kong from a study of a sample of links to their Web sites. As has been done in similar studies, they differentiated between internal links (from the same Web site) and external ones. They tested different search engines (_Google_, _MSN_ and _Yahoo!_) and chose _Yahoo!_ because it had a greater number of links and allowed the external links to be distinguished from the total.

We decided to follow the same methodology and used the new _[Yahoo Site Explorer](https://siteexplorer.search.yahoo.com/mysites)_ service, which offers many facilities for finding only internal links. In two cases, however, the number of links was increased considerably by links from the same publishing group.

The following table shows the number of direct links to the Web page of the newspaper and to the Web page of the press archive.

As can be seen, the number of links to the press archive pages is low, particularly compared with the number of links to the home page of the newspaper. _El Mundo_ stands out with a very high number of links, followed by _ABC_. The extremely low number of links of _El País_ is related to the change of domain from _elpais.es_ to _elpais.com_.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Number of links to the press archive and to the newspaper**  
</caption>

<tbody>

<tr>

<th width="40%">Newspaper</th>

<th width="30%">Links to the archive</th>

<th width="30%">Links to the newspaper</th>

</tr>

<tr>

<td>20 Minutos</td>

<td align="center">1,705</td>

<td align="center">262,615</td>

</tr>

<tr>

<td>ABC</td>

<td align="center">5,620</td>

<td align="center">323,685</td>

</tr>

<tr>

<td>As</td>

<td align="center">-</td>

<td align="center">825,359</td>

</tr>

<tr>

<td>Libertad Digital</td>

<td align="center">-</td>

<td align="center">128,645</td>

</tr>

<tr>

<td>Marca</td>

<td align="center">-</td>

<td align="center">188,199</td>

</tr>

<tr>

<td>El Mundo</td>

<td align="center">14,307</td>

<td align="center">747,534</td>

</tr>

<tr>

<td>Mundo Deportivo</td>

<td align="center">-</td>

<td align="center">161,852</td>

</tr>

<tr>

<td>El País</td>

<td align="center">734</td>

<td align="center">518,984</td>

</tr>

<tr>

<td>El Periódico</td>

<td align="center">1,290</td>

<td align="center">1,377,886</td>

</tr>

<tr>

<td>Periodista Digital</td>

<td align="center">-</td>

<td align="center">139,591</td>

</tr>

<tr>

<td>Sport</td>

<td align="center">-</td>

<td align="center">1,306,049</td>

</tr>

<tr>

<td>La Vanguardia</td>

<td align="center">396</td>

<td align="center">134,630</td>

</tr>

<tr>

<td>La Verdad</td>

<td align="center">14</td>

<td align="center">590</td>

</tr>

</tbody>

</table>

#### Cost

Though _El País_ charged for all its content between November 2002 and June 2005 ([Guallar 2007](#gua07)), there are currently no newspapers in Spain that do so. Eight of the newspapers follow the mixed model (free and paid content), which consists in offering the online edition free and charging for part or all of the print edition. The normal systems of payment include annual and half-year subscription and the possibility of purchasing news and back issues separately, generally with discounts for packages. The price of the subscription ranges between €75 and €95 a year and €50 and €60 for a half-year, with the exception of _As_ (€235 and €130, respectively) and _Marca_ (€20 per month). There are a variety of package options.

Five online newspapers are completely free: _Periodista Digital_, _Libertad Digital_, _20 Minutos_, _La Verdad_ and _Mundo Deportivo_. All of these have two characteristics: they do not have content in pdf format, and they generally have lower scores than the pay newspapers in the quality indicators of their search systems.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: Cost and form of payment**  
</caption>

<tbody>

<tr>

<th width="26%">Newspaper</th>

<th width="37%">Annual subscription</th>

<th width="37%">Purchase of single issues</th>

</tr>

<tr>

<td>20 Minutos</td>

<td>Free</td>

<td> </td>

</tr>

<tr>

<td>ABC</td>

<td>Annual €80, half-year €50</td>

<td>€0.75 news item, packages</td>

</tr>

<tr>

<td>As</td>

<td>Annual €235, half-year €130</td>

<td>€1.90, day €0.90</td>

</tr>

<tr>

<td>Libertad Digital</td>

<td>Free</td>

<td> </td>

</tr>

<tr>

<td>Marca</td>

<td>Monthly €20</td>

<td>€0.60 per issue</td>

</tr>

<tr>

<td>El Mundo</td>

<td>Annual €75 (only pdf), €97 (html and pdf)</td>

<td>Packages of articles and issues; e.g. 100 articles, €9</td>

</tr>

<tr>

<td>Mundo Deportivo</td>

<td>Free</td>

<td> </td>

</tr>

<tr>

<td>El País</td>

<td>€80</td>

<td>€0.50 per issue</td>

</tr>

<tr>

<td>El Periódico</td>

<td>No</td>

<td>€0.50 article (html), €0.75 (html and pdf), with packages</td>

</tr>

<tr>

<td>Periodista Digital</td>

<td>Free</td>

<td> </td>

</tr>

<tr>

<td>Sport</td>

<td>Annual €80, half-year €50</td>

<td>pdf news item €2 with packages</td>

</tr>

<tr>

<td>La Vanguardia</td>

<td>Annual €95, half-year €60</td>

<td>€0.9 per issue, €3 per issue from the historic archive</td>

</tr>

<tr>

<td>La Verdad</td>

<td>Free</td>

<td> </td>

</tr>

</tbody>

</table>

## Results

The level of development of the press archives in the Spanish newspapers with the widest circulations is not homogeneous. Table 7 presents a summary of the results of the press archives.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 7: Compliance of press archives with the indicators (see Table 2 for indicators)**  
</caption>

<tbody>

<tr>

<th rowspan="2">Newspaper</th>

<th colspan="17">Indicators</th>

</tr>

<tr>

<th width="4%">1.3</th>

<th width="4%">2.1</th>

<th width="4%">2.2</th>

<th width="4%">2.3</th>

<th width="4%">2.4</th>

<th width="4%">2.5</th>

<th width="4%">2.6</th>

<th width="4%">2.7</th>

<th width="4%">2.8</th>

<th width="4%">3.1</th>

<th width="4%">3.2</th>

<th width="4%">3.3</th>

<th width="4%">3.4</th>

<th width="4%">3.5</th>

<th width="4%">3.6</th>

<th width="4%">4.2</th>

<th width="11%">Total</th>

</tr>

<tr>

<td>20 Minutos</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">6</td>

</tr>

<tr>

<td>ABC</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">13</td>

</tr>

<tr>

<td>As</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">8</td>

</tr>

<tr>

<td>Libertad Digital</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">6</td>

</tr>

<tr>

<td>Marca</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">1</td>

</tr>

<tr>

<td>El Mundo</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">12</td>

</tr>

<tr>

<td>Mundo Deportivo</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">7</td>

</tr>

<tr>

<td>El Pais</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">13</td>

</tr>

<tr>

<td>El Periódico</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">12</td>

</tr>

<tr>

<td>Periodista Digital</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">8</td>

</tr>

<tr>

<td>Sport</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">8</td>

</tr>

<tr>

<td>La Vanguardia</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">14</td>

</tr>

<tr>

<td>La Verdad</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td align="center">X</td>

<td> </td>

<td align="center">X</td>

<td align="center">X</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">7</td>

</tr>

<tr>

<td>Total</td>

<td align="center">5</td>

<td align="center">10</td>

<td align="center">8</td>

<td align="center">8</td>

<td align="center">9</td>

<td align="center">4</td>

<td align="center">0</td>

<td align="center">11</td>

<td align="center">6</td>

<td align="center">7</td>

<td align="center">12</td>

<td align="center">5</td>

<td align="center">7</td>

<td align="center">9</td>

<td align="center">8</td>

<td align="center">6</td>

<td> </td>

</tr>

</tbody>

</table>

A quantitative analysis of the results was used to draw up the following ranking of newspapers (from a total of sixteen):

*   _La Vanguardia_: 14
*   _ABC_, _El Pais_: 13
*   _El Mundo_, _El Periódico_: 12
*   _As_, _Periodista Digital_, _Sport_: 8
*   _Mundo Deportivo_, _La Verdad_: 7
*   _20 Minutos_, _Libertad Digital_: 6
*   _Marca_: 1

Three main groups of newspapers can be distinguished.

1. Medium-high to high  
Five newspapers comply with twelve to fourteen of the sixteen indicators evaluated. They are all digital versions of prestigious general newspapers published in print format: _La Vanguardia_, _Abc_, _El País_, _El Mundo_ and _El Periódico_. First and second place are occupied by _La Vanguardia_ and _El País_, the only newspapers that offer their whole archive digitally (over a hundred years for the former). _El Mundo_ has the best features for combining search terms, which is the most important indicator for a query system. Despite their high score, _ABC_ and _El Periódico_ have press archives with two unequal search systems, as stated above.

2. Medium-low  
Seven newspapers comply with six to eight of the indicators. These are three daily sports papers (_As_, _Sport_ and _Mundo Deportivo_), three exclusively digital newspapers (_Periodista Digital_, _20 Minutos_ and _Libertad Digital_) and a regional newspaper (_La Verdad_). This group is far behind the first group. Though they show high values in some points, on the whole the level of development of their press archives is limited and they show much room for improvement.

3. Low  
The sports paper _Marca_ only complies with one indicator (that of searching by browsing) because it has no search engine. Despite the wide audience of this publication, it has not yet decided to offer a keyword search system.

## Conclusions

In summary, fewer than half of the major Spanish newspapers show high or medium levels of development of their digital press archives, though not as high as that of the best press databases, and the rest are still far from satisfying the requirements of a professional search system. By types of media, the sports papers lag behind the general information newspapers in the quality of their search systems, exclusively digital newspapers with more recent press archives fail to give them sufficient importance, and regional newspapers lag behind the major dailies.

The indicators that obtained the highest scores were search by browsing (twelve newspapers) and document fields (eleven newspapers). Those that obtained the lowest scores were repeat search functions (offered by no newspapers), partial searches in results fields (four newspapers), highlighting of search terms (five newspapers), temporal coverage (still low except in five newspapers), visibility (six newspapers) and help texts (six newspapers). Thus, these are the areas in which Spanish digital press archives should improve their services in the future.

As a result of this analysis, several recommendations can be made for developers and publishers:

_Enhance contents_

The most desirable thing is to make the entire newspaper available digitally, including supplements. And, of course, the original digital version should be available too, since it is sometimes different to the print one. Nevertheless, at the time of our study, very few newspapers actually make the entire publication available. Newspapers that offer only a reduced portion of their content on the Web are missing out on the enormous potential of a large newspaper archive because this is a good way to attract new visitors and to mantain the interest of subscribers.

_More search capacity_

Our study clearly identifies the existence of two groups of newspapers. Those that are more advanced would have only to slightly improve their search services, whereas the less advanced group would need to introduce major improvements. Regardless of the group, newspapers in each should pay special attention to those indicators that received the lowest scores. The existence of a professional-level information retrieval system could substantially improve access to contents of high interest for a wide variety of users.

We have not focussed any attention on user studies in this text, but they are certainly an important tool that could be applied here. The best way for improving the quality and utility of newspaper archives is to analyse their use, through methods such as log analyses, eye-tracking studies and surveys.

Finally, our study can be useful for both professional and non-professional users by allowing them to identify the best newspaper archives and by helping them to choose the best options for consulting retrospective news.

## Note

This paper bears some relationship to a text by the same authors ([Guallar and Abadal 2009](#gua09b) ) published in Spanish in _El profesional de la información_. The objective of this earlier article was to establish a list of indicators for the evaluation of digital press archives and to present examples of good practices. The current text applies these same indicators to the thirteen Spanish newspapers with the largest digital circulation, presents the quantitative analysis of the results and ranks the newspapers according to the level of development of their press archives.

## About the authors

Javier Guallar is Professor in the Department of Library and Information Science, University of Barcelona. He is vice-director of _[El Profesional de la Información](http://www.elprofesionaldelainformacion.com)_. He can be contacted at: [jguallar@gmail.com](mailto:jguallar@gmail.com)

Ernest Abadal is Senior Lecturer in the Department of Library and Information Science, University of Barcelona. He is director of the scientific journal _[BiD: textos universitaris de biblioteconomia i documentació](http://www.ub.edu/bid)_. He can be contacted at: [abadal@ub.edu](mailto:abadal@ub.edu)

## References

*   <a id="aba02" name="aba02"></a>Abadal, E. (2002). [Elementos para la evaluación de interfaces de consulta de bases de datos](http://www.webcitation.org/5jFMhhmbm) [Elements for an evaluation of database interfaces]. _El Profesional de la Información_, **11**(5), 349-360\. Retrieved 2 September, 2009 from http://www.elprofesionaldelainformacion.com/contenidos/2002/septiembre/3.pdf (Archived by WebCite® at http://www.webcitation.org/5jFMhhmbm)
*   <a id="cow01" name="cow01"></a>Cowen, N. (2001). The future of the British broadsheet newspaper on the World Wide Web. _Aslib Proceedings_, **53**(5), 189-200.
*   <a id="fue98" name="fue98"></a>Fuentes, M.E. & González Quesada, A. (1998). La prensa española en Internet: análisis de los servicios de valor añadido [The Spanish press on the Internet: analysis of value added services]. In _VI Jornadas Españolas de Documentación Automatizada, Valencia, 1998\._ (pp. 281-292). Valencia: Federación Española de Sociedades de Archivistica, Biblioteconomia y Documentación.
*   <a id="gao05" name="gao05"></a>Gao, Y. & Vaughan, L. (2005). Web hyperlink profiles of news sites: a comparison of newspapers of USA, Canada, and China. _Aslib Proceedings_, **57**(5), 398-411.
*   <a id="gar01" name="gar01"></a>Garcia Gómez, J.C. & González Olivares, J.L. (2001). Aproximación a la evaluación cuantitativa de los sistemas de recuperación de información de la prensa en Internet: exhaustividad y precisión [Approach to quantitative evaluation of information retrieval systems of the press on the Internet: completeness and accuracy]. _Scire_, **7**(1), 143-152\.
*   <a id="gra04" name="gra04"></a>Grau, J. & Guallar, J. (2004). [My News, la hemeroteca digital de la prensa española](http://www.webcitation.org/5k9FHdsFR) [My News, the digital press archive of the Spanish press]. _El Profesional de la Información_, **13**(6), 466-476\. Retrieved 2 September, 2009 from http://eprints.rclis.org/archive/00008532/01/My-News.pdf (Archived by WebCite® at http://www.webcitation.org/5k9FHdsFR)
*   <a id="gua03" name="gua03"></a>Guallar, Javier (2003). [Mètodes i tècniques de recerca en els articles de documentació periodistica a Espanya](http://www.webcitation.org/5l9lv1QKS). _BiD: Textos Universitaris de Biblioteconomia i Documentació_, No. 11\. Retrieved 2 September, 2009 from http://www2.ub.es/bid/consulta_articulos.php?fichero=11gualla.htm (Archived by WebCite® at http://www.webcitation.org/5l9lv1QKS)
*   <a id="gua07" name="gua07"></a>Guallar, J. (2007). [Prensa digital en 2006](http://www.webcitation.org/5k9FNaZAw). [Digital journalism in 2006]. In _Anuario ThinkEpi 2007_ (pp. 106-117). Barcelona: EPI SCP. Retrieved 2 September, 2009 from http://eprints.rclis.org/archive/00008388/01/AnuarioThinkEPI2007-Guallar-Prensa-digital-en-2006.pdf (Archived by WebCite® at http://www.webcitation.org/5k9FNaZAw)
*   <a id="gua08" name="gua08"></a>Guallar, J. (2008). Prensa digital en 2007\. [Digital journalism in 2007]. In _Anuario ThinkEpi 2008_ (pp. 102-108). Barcelona: EPI SCP.
*   <a id="gua09" name="gua09"></a>Guallar, J. (2009). Prensa digital en 2008\. [Digital journalism in 2008]. In _Anuario ThinkEpi 2009_ (pp. 88-94). Barcelona: EPI SCP.
*   <a id="gua09b" name="gua09b"></a>Guallar, J. & Abadal, E. (2009). [Evaluación de hemerotecas de prensa digital: indicadores y ejemplos de buenas prácticas](http://www.webcitation.org/5l9dqKvqH) [Evaluation of digital press archives: indicators and examples of good practice]. _El Profesional de la Información_, **18**(3), 255-269\. Retrieved 2 September, 2009 from http://eprints.rclis.org/16899/1/epi09_guallar-abadal_evaluacion_hemerotecas.pdf (Archived by WebCite® at http://www.webcitation.org/5l9dqKvqH)
*   <a id="jim03" name="jim03"></a>Jiménez López, Á. (2003). [Hemerotecas de pago en la prensa digital española](http://www.webcitation.org/5k9Hr4VhI) [Payment press archives in the Spanish digital press]. _El Profesional de la Información_, **12**(6), 473-474\. Retrieved 2 September, 2009 from http://www.elprofesionaldelainformacion.com/contenidos/2003/noviembre/9.pdf (Archived by WebCite® at http://www.webcitation.org/5k9Hr4VhI)
*   <a id="jim02" name="jim02"></a>Jiménez López, Á. (2002). [Estudi de la gestió documental de la informació en els serveis de valor afegit dels mitjans de comunicació a Internet: el cas de la premsa diària a l'Estat espanyol](http://www.webcitation.org/5k9HtaUCs) [Study of information management on value added services from Internet media: the case of newspapers in Spain]. Bellaterra: Universitat Autónoma de Barcelona. Retrieved 2 September, 2009 from http://www.tesisenxarxa.net/TDX-1008102-143437/index.html (Archived by WebCite® at http://www.webcitation.org/5k9HtaUCs)
*   <a id="jim00" name="jim00"></a>Jiménez López, Á., González Quesada, A. & Fuentes, M. E. (2000). [Las hemerotecas digitales de la prensa en Internet](http://www.webcitation.org/5k9Hwa5HI) [Digital newspapers libraries on the Internet]. _El Profesional de la Información_, **9**(5), 15-24\. Retrieved 2 September, 2009 from http://www.elprofesionaldelainformacion.com/contenidos/2000/mayo/2.pdf (Archived by WebCite® at http://www.webcitation.org/5k9Hwa5HI)
*   <a id="jim99" name="jim99"></a>Jiménez López, Á., González Quesada, A. & Fuentes, M.E. (1999). [Gestió documental de la informació en els serveis de valor afegit de la premsa espanyola a internet](http://www.webcitation.org/5k9HygEmq) [Information management on value added services from Spanish press on Internet]. In _7es Jornades Catalanes de Documentació_ (pp. 405-417). Barcelona: Col·legi  Oficial de Bibliotecaris-Documentalistes de Catalunya. Retrieved 2 September, 2009 from http://www.cobdc.org/jornades/7JCD/18.pdf (Archived by WebCite® at http://www.webcitation.org/5k9HygEmq)
*   <a id="lin01" name="lin01"></a>Lin, C.A. & ]effres, L.W. (2001). Comparing distinctions and similarities across Web sites of newspapers, radio stations,and television stations. _J&MC Quarterly_, **7**(3), 555-573.
*   <a id="lon06" name="lon06"></a>Longo, B. (2006). Gli archivi dei giornali online [The archives of online newspapers]. _Biblioteche Oggi_, **24**(1), 9-21.
*   <a id="lop04" name="lop04"></a>López Carreño, R. (2004). [Análisis taxonómico de los portales periodísticos españoles](http://www.webcitation.org/5k9I1SJei) [Taxonomic analysis of Spanish newspaper Web sites]. _Anales de Documentación_, **7**, 123-140\. Retrieved 2 September, 2009 from http://www.um.es/fccd/anales/ad07/ad0708.pdf (Archived by WebCite® at http://www.webcitation.org/5k9I1SJei)
*   <a id="mar95" name="mar95"></a>Marchionini, G. (1995). _Information seeking in electronic environments_. Cambridge: Cambridge University Press.
*   <a id="mor06" name="mor06"></a>Morville, P. & Rosenfeld, L. (2006). _Information architecture for the World Wide Web_ (3rd. ed.). Sebastopol, CA: O'Reilly.
*   <a id="nie06" name="nie06"></a>Nielsen, J. & Loranger, H. (2006). _Prioritizing web usability_. Berkeley, CA: New Riders.
*   <a id="nei99" name="nei99"></a>Neilson, K & Willett, P. (1999). United Kingdom regional newspapers on the World Wide Web. _Aslib Proceedings_, **51**(3), 78-90
*   <a id="shn97" name="shn97"></a>Shneiderman, B., Byrd, D. & Croft, W.B. (1997, January). [Clarifying search: a user-interface framework for text searches](http://www.webcitation.org/5k9I40g6V). _D-Lib Magazine_. Retrieved 2 September, 2009 from http://www.dlib.org/dlib/january97/retrieval/01shneiderman.html (Archived by WebCite® at http://www.webcitation.org/5k9I40g6V)
*   <a id="str06" name="str06"></a>Stryker, J.E., Wray, R., Hornik, R.C. & Yanovitzky, I. (2006). Validation of database search terms for content analysis: the case of cancer news coverage. _J&MC Quarterly_, **83**(2), 413-430.

