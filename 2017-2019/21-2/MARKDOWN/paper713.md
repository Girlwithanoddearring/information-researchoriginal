#### vol. 21 no. 2, June 2016

# 'If we just knew who should do it', or the social organization of the archiving of archaeology in Sweden

[Isto Huvila](#author)

#### Abstract

**Introduction.** This paper analyses the work practices and perspectives of professionals working with archaeological archives and the social organization of archaeological archiving and information management in Sweden.  
**Method.** The paper is based on an interview study of Swedish actors in the field of archaeological archiving (N=16).  
**Analysis.** The interviews were recorded and transcribed, then analysed using close reading.  
**Results.** We identified eight major work roles of archiving and managing archaeological information. Analysis of the recorded interviews show that there are multiple technical, legislative, conceptual and structural factors that influence and complicate the building, management and use of archaeological archives.  
**Conclusions.** Results show that the central challenge of archiving archaeology is the lack of efforts to influence and control the process by the involved actors. A mutual effort to be more explicit about concerns, needs and wishes of all participating organizations would help them to prioritise their work, take other actors’ concerns into account and develop their work practices to support more effectively the preservation of archaeological information.

## Introduction

Despite the considerable investments in the infrastructures for the preservation of archaeological heritage, there is little in-depth empirical research on their users and especially on the infrastructures and their developers, managers and contributors. The importance of understanding the practices and needs of all involved in the making and use of the ‘archaeological archive’ (understood in this text as consisting of all digital and non-digital records and outputs of archaeological activities) is underpinned by the fact that during the past two decades, archaeological information production has shifted from a predominantly analogue to a digital mode. At the same time, with some (partial) exceptions (e.g., the work of Archeology Data Service in UK and DANS in the Netherlands), the archival and preservation practices are still largely dominated by paper-based procedures and mindsets.

There are on going national and international initiatives to standardise archiving practices in archaeology (e.g., ARCHES-project and the archives workgroup of European Archaeologiae Consilium and the ARIADNE infrastructure project). There is also a long but somewhat slender line of theoretical and practice oriented research on the topic (e.g., [Merriman and Swain, 1999;](#mer99) [Swain, 2006;](#swa06) [Brown, 2011;](#bro11) [Lucas, 2010](#luc10)). What is lacking is a broader understanding of how archaeological archives are managed in practice and how archiving is and is not related to the development of archaeological information systems, databases and management practices.

The aim of this article is to explicate the work practices and perspectives of the professionals working with archaeological archives and the premises of their work and opinions in the social organization of archaeological archiving. What are the roles of the different primary actors in the process of archiving archaeology? What are their opinions on the priorities, problems and challenges of archiving and preserving archaeology? Finally, we ask if and how the work roles and their relationships can be used to explain the organization of archaeological archiving. This article is based on an interview study of Swedish professionals working with archaeological archives and archiving (N=16) representing primary groups of interested parties in the field in Sweden. The study uses a combination CATWOE analysis borrowed from Checkland’s soft systems methodology ([Checkland, 1981](#che81)) and the systems theoretical framework adapted from White ([2008](#whi08)) to explicate the emergence of the positions and world views of the different actors in the field of archaeological information.

## Literature review

The present study builds on earlier work in archaeological archiving. This review covers earlier technical and theoretical literature on archaeological archives and repositories and rationales for establishing centralised repositories for archaeological information. Secondly, the review looks into earlier research on the actors in the area of archaeological archives including users and archives professionals, and the influence of these groups on archiving and preservation.

Even if the management and archiving of archaeological information has never become a mainstream research topic in archival studies, information management or archaeology, there has been a long on-going discussion on the premises and best practices of preserving archaeology (e.g., [Swain, 2006;](#swa06) [Reilly and Rahtz, 1992;](#rei92) [Dallas, 2009](#dal09)). There is a broad consensus on the principled importance of preserving archaeology, but the recurring emphasis on the need to improve archival practices (e.g., [Richards, 2002;](#ric02) [Degraeve, 2012](#deg12)) and surveys of the actual state of affairs (e.g., [Edwards, 2011;](#edw11) [Stoll-Tucker, 2011](#sto11)) both signal that there is room for improvement. The established archival procedures need to be adapted to new types of records. There is also a need to discuss the resourcing and priorities of preserving archaeological information particularly in the context of contract archaeology ([Backhouse, 2006](#bac06)). Even if it would be possible to keep all, the problem is how to make the information digestible ([Richards, 2006](#ric06)).

A greater emphasis of the need for the preservation of digital archaeological records has served as an impetus for national initiatives, including: in the UK, the Archaeology Data Service (ADS) ([Richards, 2002](#ric02)) and Archaeological Archives Forum (AAF) ([Aitchison, 2009](#ait09)); in the Netherlands, EDNA/DANS (E-depot voor de Nederlandse archeologie, Data Archiving and Networked Services) ([Doorn and Tjalsma, 2007](#doo07)); in Germany, IANUS (Forschungsdatenzentrum Archäologie & Altertumswissenschaften), and in Sweden, the National Data Service (SNDS) and the National Heritage Board. The USA lacks similar national projects with an equally broad mandate. The major initiatives of archiving (e.g., tDAR (the Digital Archaeological Record), see [McManamon, Kintigh and Brin, 2010](#mcm10)), and publishing archaeological data (e.g., Open Context, see [Whitcher, Kansa and Schultz , 2007](#whi07)) are private, albeit partially publicly funded, consortia. The founding of specialised bodies such as the Archaeology Data Service, IANUS and the Archaeological Archives Forum (UK) for the management of archaeological information and broadening of the mandate of earlier data archives such as SNDS and DANS have pushed forward the development of policies and practices for archiving archaeology.

In addition, specific projects such as the [Digital Archaeological Workflow](http://www.raa.se/dap) have contributed to the development of models and strategies for that work (e.g., [Balkestein and Tjalsma, 2007](#bal07); [Mitcham, Niven and Richards, 2010](#mit10); [Brown, 2011](#bro11); [Riksantikvarieämbetet, 2012a](#rik12a); [Palaiologk, Economides, Tjalsma, and Sesink, 2012](#pal12)). At the same time, large international endeavours like the [ARIADNE infrastructure](http://ariadne-infrastructure.eu) for the management of archaeological research data and earlier projects including ARENA ([Kenny, Kilbride and Richards, 2003](#ken03)) for the networked access to archaeological records in Europe have brought together national and regional actors to develop sustainable technical solutions and strategies for the long-term digital preservation of archaeological information.

The earlier discussion on archaeological archiving has been characterised by two divergent approaches. First, the aim of a large body of research has been to develop practical data management systems for the purposes of individual institutions and projects. Local, project and institution-specific documentation has been developed to manage specific types of information, such as geographic information system data (e.g., [Shaw, Corns and McAuley, 2009](#sha09); [Gattiglia, 2009](#gat09)) and data on specific types of finds and areas of archaeology (e.g., maritime archaeology, [Drap and Long, 2001](#dra01)). In addition, earlier research has addressed the consequences of working within different legislative and regional contexts (e.g., [Degraeve, 2011;](#deg11) [Braccini and Federici, 2010;](#bra10) [Franceschini, 2011;](#fra11) [Bibby, 2011](#bib11); [Swain, 2006](#swa06)), and for managing information from different (often somewhat implicit) epistemological perspectives (e.g., [Braccini and Federici, 2010](#bra10); [Cantone, 2005](#can05); [Esteva, Trelogan, Rabinowitz, Walling and Pipkin, 2010](#est10); [Stenzer, Woller and Freitag, 2011](#ste11)).

The second approach has been to focus on theoretical research and the development of generic systems for representing all types of archaeological data. Digital library and e-Science oriented research (e.g., [Raghavan, Vemuri, Gonçalves, Fan and Fox, 2005](#rag05); [Fox, 2005](#fox05); [Kintigh, 2006](#kin06); [Esteva, _et al._, 2010](#est10)) has tended to focus on the ’globality’ (roughly as envisioned by [Chowdhury, 1999, p. 431-432](#cho99)) of digital repositories and information retrieval systems. As Kintigh ([2006](#kin06)) notes, a comprehensive integration of archaeological data holds a major promise for working with new types of research questions.

In spite of the universal aspirations, the challenges of universal knowledge organization systems and repositories (e.g., [Fox, Akscyn, Furuta and Leggett, 1995](#fox95); [Fox and Marchionini, 1998](#fox98); [Blandford, Stelmaszewska and Bryan-Kinns, 2001](#bla01)) have been acknowledged for a long time and the currently operational data archives have tended to adapt hybrid models for coping with heterogeneous data. The archaeological data services in the UK and the Netherlands have focused on project level descriptions whereas the US archaeological data repository the Digital Archaeological Record has chosen to enforce a more detailed level of description ([Archaeology Data Service & Digital Antiquity, 2011](#ads11)). In spite of the apparent difficulties, recent studies on semantic repositories in specific branches of archaeological research (e.g., [Desjardin, Roo, Maeyer and Bourgeois, 2014](#des14); [Isaksen, Martinez, Gibbins, Earl and Keay, 2009](#isa09)), have demonstrated the potential of integrated datasets that bring to mind the e-Science infrastructures envisioned in the earlier literature (e.g., [Borgman, 2003](#bor03); [DRIVER Project, 2015](#dri15); [Dunn, 2006](#dun06); [Kintigh, 2006](#kin06)).

In contrast to the proliferation of technical literature on digital repositories (e.g., [Austin and Mitcham, 2007](#aus07); [Richards, 2002](#ric02)), there is relatively little research on the various actors involved in and influencing the development, management and use of archaeological archives. Faniel and Yakel (project directors of the project DIPIR, [www.dipir.org](http://www.dipir.org)) have studied the data reuse of researchers and students in collaboration with the data publishing platform Open Context ([Faniel, Kansa, Kansa, Barrera-Gomez and Yakel, 2013](#fan13); [Kriesberg, Frank, Faniel and Yakel, 2013](#kri13)). Their findings emphasise the importance and typical lack of detailed contextual information on material artefacts and data. Another significant observation was that the interviewees highlighted the importance of the reputation of data repository (e.g., archive or museum) in trusting the provided data ([Faniel _et al._, 2013](#fan13)). Atici, Kansa, Lev-Tov and Kansa ([2013](#ati13)) have discussed the premises and requirements of data-use from the perspective of their own research.

Beyond studies of individual groups of users, both Archaeology Data Service ([Beagrie and Houghton, 2013](#bea13)) and IANUS ([Schäfer, Heinrich and Jahn, 2014](#sch14)) have collected and analysed user feedback. Both studies show positive responses from the depositors and users of the data. According to Beagrie and Houghton ([2013](#bea13)), the Archaeology Data Service is considered an authority in the long-term digital preservation of archaeological data. Research funders and government organizations are content with how the Archaeology Data Service helps contractors to manage the legal requirements of depositing their data. Easy online access to datasets saves researchers’ and students’ time and helps them to succeed. Contractors see the one-off payment model of the Service well aligned with their commercial interests and use the repository to benchmark their own processes.

Other repositories and their funders have commissioned similar economic and qualitative impact reports (e.g., [Bott, 2003](#bot03)). Even if these types of evaluations do provide quantitative and general qualitative insights into the impact of digital repositories, their focus is on estimating the (positive) societal significance of the repositories rather than critically explicating how the repositories are linked to the everyday practices of the different groups that influence or are affected by the repositories. Passley’s ([2013](#pas13)) study on the training preferences of archivists and (archival) restoration workers is an example of parallel work on the diverging professional priorities outside the context of archaeology. Even if the general aim of both professional groups was to ensure the preservation of archival materials, the differences in how they prioritised diverse aspects of their everyday work lead to significant differences in how they esteemed the value of different competences and activities.

The significance of understanding the practices of using and producing archaeological archives relates to their impact on how archaeology is conducted. They are infrastructures ([Huvila, 2009a](#huv09a)) that influence the way archaeologists are working and in the end, shape that what we know of the past ([Guimier-Sorbets, 1996](#gui96); [Huvila, 2006](#huv06)). In the worst case, the suboptimal archiving of archaeological information may lead to a “Digital Dark Age” ([Jeffrey, 2012](#jef12)). On the basis of a study of information work of archaeology professionals, Huvila ([2008b](#huv08b); [2006](#huv06)) recommends that the critical success factors of archaeological information management are the fit (for purpose) and (long-term) sustainability of information. He also notes that special emphasis should be placed on exploiting the generally positive attitude towards digital repositories, relevance and not only comprehensiveness of preserved documentation, the significance of physical artefacts as information objects in archaeology and the contextual and emergent nature of archaeological information ([Huvila, 2008b](#huv08b)). Like Huvila, Faniel _et al._ ([2013](#fan13)) emphasised the importance of contextuality and argued for the need to include more information on contextual dimensions such as the methodological procedures of archaeological investigations, information on the archaeologists who conducted the work (training, previous work) and on the repository.

## Theoretical framework

This study is based on a conceptual framework adapted from White’s theory of social life discussed in the second edition of _Identity and control_ ([2008](#whi08)). White’s theory is combined with a role-theory based approach of information work analysis ([Huvila, 2008a](#huv08a)) emanating from the soft-systems methodology and CATWOE approach (described in detail in the Methods section of this text) of Checkland ([1981](#che81)). Instead of emphasising rules and exceptions or why something happens, White’s theory focuses on how social forms emerge and how normality and anomalies can be accounted for in a single framework. According to White ([2008](#whi08)), the social world, including organizations and persons, emerge from relationships instead of attributes. An organization exists because individuals maintain a particular pattern of mutual relationships. White argues further that identities (defined as a generic source of action, e.g., organizations, crowds, communities or individuals in different situations) are triggered by '_efforts at control amid contingencies and contentions in interaction_' ([White and Godart, 2007](#whit07)). The efforts of control form social realities for other identities. Control has its foundation in physical and social milieus and this foundation or footing brings orientation in relation to other identities. In practice, '_an identity can be perceived by others as having an unproblematic continuity in social footing, even though it is adding through its contentions with others to the contingencies they face_' ([White and Godart, 2007](#whit07)).

The present study builds on White and asks whether the relationships and identities (as discussed by White) can explain the organization of archaeological archiving. In contrast to White’s broader definition of identity ([White 2008, p. 2](#whi08)), this study looks into a specific subset of possible identities related to work roles and posits that a work role is endowed with a form of collective identity, which is similar to individuals or collective entities, capable of engaging in control efforts. The purpose of the approach is to explicate the emergence of the positions of different groups of actors that work with archaeological information, and to explain how archaeological archiving is exercised in Sweden and to discuss its implications.

## Methods and material

The empirical material consists of sixteen qualitative interviews of Swedish archaeology professionals with special interest in issues pertaining to the archiving and preservation of archaeology. The interview approach was chosen to let a group of interviewees working at different positions in different organizations to articulate their understanding of the archiving and preservation of archaeology. In contrast to a survey, interviews gave the interviewees an opportunity to articulate their views in depth and in detail, and the interviewer a comparable opportunity to ask for clarifications and additional information.

The design and conducting of the interviews was based on the semi-structured thematic interview approach of Hirsjärvi and Hurme ([1995](#hir95)). All interviews were conducted by the author, taped and transcribed by a professional transcriber. The interviews lasted an average of 60 minutes. They focused on the interviewees’ professional work, their views on the current state and future prospects of archaeological archiving. After a small number of initial questions about education, work experience, the informants were asked to describe their current work (daily routines, positive and negative challenges, organization). Next the interviewees were asked about the current state and future prospects of the “preservation, archiving and keeping of archaeology”. The questions about archiving were left open to allow interviewees to define the priorities of keeping various types of assets, documents and objects and to choose their preferred term for archiving (or e.g., keeping, preserving, saving) archaeology. Interviewees were also asked to describe their own use of archaeological archives and their information work in general terms. Finally, the interviewees were asked to describe an ideal archaeological archive and from their perspective, how it would differ from the present facilities and processes.

The choice of interviewees was conducted using a mixed-methods approach, which according to the classification of sampling types of Teddlie and Yu ([2007](#ted07)) combined criterion and snowballing-based purposive sampling and convenience sampling. The selection was based on the interviewees’ interest in archaeological archiving, representation of key groups of actors relating to archaeological archiving and convenience (voluntary participation). The initial group was formed by contacting professionals who participated in a workshop on archeological archiving organized by a third party in 2013 in Sweden. Invitations were sent to participants at the event. Interviewees were also asked to suggest other professionals for possible inclusion in the project. The size of the complete population of relevant interviewees, the overlap of categories and corroboration of evidence imply that the sample is not representative of a larger population. It is still useful for considering the qualitative approach and the conceptual and exploratory rather confirmatory aims of the present study. For reporting purposes, the interviewees were assigned false names (Table 1).

<table class="center" style="width:90%;"><caption>  
Table 1: Interviewees and their workroles. (X) refers to earlier and/or current indirect workroles.</caption>

<tbody>

<tr>

<th rowspan="2">Interviewee</th>

<th rowspan="2">Description</th>

<th colspan="8">Workrole</th>

</tr>

<tr>

<th>A</th>

<th>B</th>

<th>C</th>

<th>D</th>

<th>E</th>

<th>F</th>

<th>G</th>

<th>H</th>

</tr>

<tr>

<td>Clavdia</td>

<td>Finds information administrator at a national institution</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Mynheer</td>

<td>Administrative director of a contract financed archaeological department a regional museum</td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Peeperkorn</td>

<td>Archivist, information manager at a national institution</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Chauchat</td>

<td>Administrative director of a contract financed archaeological department a regional museum</td>

<td style="text-align:center">X</td>

<td style="text-align:center">(X)</td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Naptha</td>

<td>Finds administrator at a regional/national institution</td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Settembrini</td>

<td>Coordinator at a private archaeology consultancy</td>

<td style="text-align:center">X</td>

<td style="text-align:center">(X)</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Hans</td>

<td>Researcher</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

</tr>

<tr>

<td>Joachim</td>

<td>Field archaeologist and administrator at a private archaeology consultancy</td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

</tr>

<tr>

<td>Ziemssen</td>

<td>Archivist at a national institution</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Behrens</td>

<td>Coordinator at a contract archaeology department, regional museum</td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Marushja</td>

<td>Archivist at a national institution</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Lodovico</td>

<td>Data archivist</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Leo</td>

<td>Administrator at a county administrative board</td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Castorp</td>

<td>Researcher, data archiving duties</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

</tr>

<tr>

<td>James</td>

<td>Information manager at a national institution</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Tienappel</td>

<td>Researcher, data archivist</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

<td style="text-align:center"></td>

<td style="text-align:center">X</td>

</tr>

<tr>

<td colspan="10">Key to abbreviations: A=Archaeological contract work; B=Administration (CAB); C=Archival management; D=Finds administration; E=Finds allocation; F=Data archiving; G=Information management; H=Archaeological research</td>

</tr>

</tbody>

</table>

The empirical approach has some obvious limitations. Even if the author has done his best to avoid taking '_researcher degrees of freedom_' ([Simmons, Nelson and Simonsohn, 2011](#sim11)), additional studies are needed to confirm the exploratory results of this study. Findings are based on a relatively small number of interviews from a single country, limiting the validity of any generalisation. To control for the overexpression of individual opinions, the analysis places a special emphasis on views expressed by multiple interviewees. Secondly, considering the exploratory aim of the present study to provide evidence for the existence of a phenomenon in mind, the possible inability to generalise is not considered to be a major issue. Thirdly, even if an ethnographical approach could have provided more in-depth information on individuals and their work, the interview approach was considered to be a reasonable compromise to get depth and breadth on work practices within and between individual organizations.

The analysis of the interview transcripts was based on close reading ([DuBois, 2003](#dub03)) of interview transcripts and information work analysis ([Huvila, 2006](#huv06)). The approach builds on explicating work roles ([Huvila, 2008c](#huv08b)) and describing their positions using root definitions and CATWOE analysis. The root definitions and the CATWOE (stands for the six criteria: client, actor, transformation, weltanschauung, owner, environment) criteria are derived from the soft systems method (SSM) developed by Checkland ([1981](#che81)) in a series of publications from the mid- seventies onwards. The technique is used in its original context as a tool for analysing the problems and ’systems’ that are involved in the information systems development, and expressing a hypothesis of a relevant ’system’. The root definitions are particularly useful in clarifying the studied situation by exposing the different views and opinions held by different actors (including e.g., the users, contractors, customers and the management). The root definitions are analytical compositions of attitudes, opinions and world-views, based on several individual responses. The definitions are written in the first-person, but they do not correspond with any actual responses word-to-word. CATWOE is a method for expressing a root definition in a formalised manner (ref. Table 2).

<table class="center"><caption>  
Table 2: The CATWOE mnemonic Smyth and Checkland ([1976](#smy76)), Checkland ([1981, 224-227](#che81)).  
</caption>

<tbody>

<tr>

<th colspan="2">CATWOE</th>

<th>Example of the Archaeological contractor work role from the contractor point of view</th>

</tr>

<tr>

<td>Client</td>

<td>refers to the beneficiary or victim, who is affected by the described activities</td>

<td>Developer (on an abstract level Society); (County administrative board)</td>

</tr>

<tr>

<td>Actor</td>

<td>is the agent who is responsible for carrying out the operations</td>

<td>Me (as a contract archaeologist).</td>

</tr>

<tr>

<td>Transformation</td>

<td>is the core of the root definition, which describes what is happening</td>

<td>Conducting an archaeological investigation of a site according to the contract.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>(world view, ’all that you take for granted’) are the assumptions and the mental ’landscape’ where the root definition functions</td>

<td>My role as a private contractor is in contradiction with the altruistic archaeological ethos of documenting and preserving the past material culture.</td>

</tr>

<tr>

<td>Owner</td>

<td>is the sponsor or the controller of the activity</td>

<td>Developer; Country Administrative Board; (National Heritage Board); (Society)</td>

</tr>

<tr>

<td>Environment</td>

<td>is the space or environment where the activity takes place</td>

<td>Country (Sweden); Archaeological investigation site; (Region for regional contractors)</td>

</tr>

</tbody>

</table>

Root definitions and the CATWOE-criteria are fundamentally a goal-oriented technique intended to bring about the change in an organization. Here the technique is used as an analytic instrument for articulating the boundaries and the constituent operations embodied in the systems, which are determined by the identified work roles.

The results were revisited and revised after one month of the original categorisation for assessing its validity, and again one month later, reanalysed using negative case analysis ([Lincoln and Guba, 1985](#lin85)) with a specific purpose of finding contradictory evidence that would decrease the reliability of the drawn conclusions.

## Archaeological information process in Sweden

Nearly all archaeological fieldwork in Sweden is conducted in contracted rescue archaeology projects. A typical Swedish contract archaeology process is complicated and involves several private and public actors including the developer (in practice state agencies, large construction companies, landowners and similar organizations), county administrative board (CAB), the National Heritage Board, contractor and museums ([Lönn, 2006](#lon06)). All public bodies involved in the process including museums, National Heritage Board and universities are required to archive all related administrative records. In Sweden, the general requirement is that a developer files an environmental impact assessment (MKB) of a planned intervention ([Schibbye, Frisk, Sander and Westerlind, 2007](#sch07)). It is common that the Environmental Impact Assessment is preceded by a pre-study that forms a basis for the comprehensive assessment of environmental impact. From the point of view of archaeology, the problem is that many environmental impact assessment processes lack specific archaeological competence ([Björckebaum and Mossberg, 2009](#bjo09)) and the archaeological assessment is contracted first during the final stages of a planning process.

The compiled Environmental Impact Assessment is delivered to the local county administrative board for a statement and negotiation. If the conclusion is that archaeological sites need to be either fully or partially removed, the county administrative board puts an investigation project out to tender. Contractors are asked to submit an investigation plan and budget for the project. The tenders are processed by the county administrative board and discussed with the developer before a final decision.

After the decision of the winning tender has been made, the chosen contractor conducts an archaeological investigation on the site. The contractor is required to file a report with the county administrative board (with copies submitted to the National Heritage Board, museums involved in the process and the developer) and submit information about the site to the National Sites and Monuments Record (FMIS) held by the National Heritage Board. Recently, the National Heritage Board has started to archive digital archaeological reports in a DSpace-based repository called SAMLA (http://samla.raa.se).

At the end of the investigation, the National Heritage Board makes decision on the allocation of the investigation’s finds. There are specific rules for the depositing of particular groups of finds (e.g., coins that are deposited with the Royal Coin Cabinet), but the general policy is to see that finds are preserved in the region (regional museum) and/or at the museum with earlier finds from the same site.

The county administrative board is also responsible for making decisions about the archiving of other documentation material (including maps, drawings, notes and other data) at an archival institution. Documentation material is kept at regional museums and universities with extensive archives of archaeological documentation. By the time of conducting the study in 2014, material from the investigations conducted by the Contract Archaeological Service of the National Heritage Board were archived with the National Heritage Board. From January 1, 2015 the unit was moved from the National Heritage Board to become a part of the Swedish History Museum. In addition to the statutory allocation of finds and archiving of official records, specialised research data archives including the Swedish National Data Archive (SNDS) and the Strategic Environmental Archaeological Database (SEAD) have started to provide access to selected digital sets of archaeological data.

## Analysis

In the analysis of the empirical material it was possible to distinguish eight major work roles (Table 1) at the level of the national process of archiving and managing archaeological information. The eight roles discussed here overlap with each other and individual informants were often engaged in several different roles as a part of their duties. In addition, other interviewees were able to elaborate each others’ roles from different perspectives. There is also considerable variation in how specific institutions have organized their work and therefore the distribution of the different roles vary from one institution to another. Both in spite and because of this variation, the work roles function as generic and representative units for analysing the archiving of archaeology in Sweden.

### Archaeological contract work

The role archaeological contract work encompasses the practical rescue archaeological work. Contractors undertake archaeological investigations (surveys and excavations) commissioned by developers (in practice state agencies, large construction companies, landowners and other developers) under the supervision of the county administrative board ([Ottander, 2012](#ott12)). Contractors are selected on the basis of a tender or in small-scale projects directly by the county administrative board after discussions with the developer. The responsibility of the contractor is to conduct the investigation according to the current regulations of the National Heritage Board and to submit a report that documents the work procedures and results of the investigation. Similarly to the principles of archaeological work, the form and contents of the report are regulated by national guidelines (including [Kulturrådet, 2013](#kul13); Riksantikvarieämbetet, [2012a](#rik12a); [2012b](#rik12b)), which do, however, leave room for considerable local adjustments.

The priorities of the contractors lie in winning tenders, conducting cost-effective investigations that conform to the regulations and submitting a report. Some Swedish contractors are private companies, but a large number of them are affiliated with regional and national institutions. The largest actor is the field archaeology unit (from January 2015, a part of the Swedish History Museum) of the National Heritage Board. The field archaeology units of regional museums form another large group of contractors. Even if the contract archaeology operations of the public bodies are funded similarly by external contracts than the private companies, the marriage of private interests and public ethos is often problematic. As Chauchat argues, the current administrative system '_should have better readiness to deal with_' contractors and to take into account their economic realities and their need for clients and constant income: '_we need to hunt for work and money, [take into consideration] what customers want and so on_'. The contract archaeology units of the public bodies may have duties that essentially stretch beyond their role as contractors. Even the interviewees representing private archaeology operators reflected upon their problematic feelings of abstract public responsibility that went well beyond their contractual liabilities. For instance, Chauchat and Settembrini, noted that their organizations are keeping at the moment complete datasets from their past investigations even if it is costly and beyond their legal obligations. All interviewed contract archeologists were similarly explicit about their awareness of the destructive nature of archaeological interventions (when an archaeological site is excavated, the documentation is all that remains) and responsibility of documenting their work in detail.

The uncertainties of the ownership and responsibility for the data and reports also hinder the contractors’ possibilities to acquire and share necessary information. Most of the informants were largely favourable to sharing other data than their internal models for calculating costs and tenders. At the same time, however, no procedures for sharing and accessing digital or analogue data exist. The paper-based archival procedures of investigation reports made the documents difficult to access for the contractors even if the unofficial custom of many contractors to publish their finalised reports on their webpages helped in this respect.

<table class="center"><caption>  
Table 3: Archaeological contract work.  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th> </th>

</tr>

<tr>

<td>Client</td>

<td>Developer; (on an abstract level Society); (County administrative board)</td>

</tr>

<tr>

<td>Actor</td>

<td>Me</td>

</tr>

<tr>

<td>Transformation</td>

<td>Conducting an archaeological investigation of a site according to the contract.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>My role as a private contractor is in contradiction with the altruistic archaeological ethos of documenting and preserving the past material culture.</td>

</tr>

<tr>

<td>Owner</td>

<td>Developer; Country Administrative Board; (National Heritage Board); (Society)</td>

</tr>

<tr>

<td>Environment</td>

<td>Country (Sweden); Archaeological investigation site; (Region for regional contractors)</td>

</tr>

</tbody>

</table>

### Administration (County Administrative Board)

In Sweden, the county administrative boards are the authorities that grant permission for land use in their respective counties. Its role is to ensure that archaeological sites and monuments will be left intact as far as possible and if impossible, to make decisions of conducting archaeological investigations on sites that cannot be preserved (Leo, Naphta). The first priority of administrators is to influence the plans in a way that archaeological sites can be left intact and only secondarily, to guarantee that an exhaustive archaeological investigation is conducted and comprehensive documentation of the site produced within reasonable financial constraints and that a report and documentation of the administrative process will be archived properly. The most significant document (as stated also in the guidelines for contract archaeology, [Riksantikvarieämbetet, 2012a](#rik12a)) is the eventual decision of an intervention on an archaeological site made by the county administrative board. The interviewees working in the administrator work role including Leo and Naphta noted that they often work with tight schedules and the work burden tends to be high to a degree that there are seldom possibilities to supervise contractors or even to read all incoming investigation reports. In practice, he considered, however, that there are mechanisms that ensure the quality of the process. The mere possibility that an administrator reviews the contractors’ work and reporting processes and the rule that contractors may be declined tenders if their fail to complete their projects, process finds and submit required documentation puts pressure on the contractors to conduct their work properly. Also the the high level of professionalism of contractors compensates for the scarcity of direct control. Napthta noted also that according to his experience, even if there are differences, the general quality of contractors is high and the smaller and sometimes weaker actors are catching up the larger ones. Another problem is related to the difficulties of accessing information. One of the interviewees employed by a county administrative board noted that it tends to be easier to find a particular report from the web site of the contractor than to contact the archive of the own institution: '_[i]t’s not rare that we go to archaeology companies’ websites and look and search for reports and such things_'. (Naphta).

<table class="center"><caption>  
Table 4: Administration (County Administrative Board).  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th> </th>

</tr>

<tr>

<td>Client</td>

<td>Society; The Law</td>

</tr>

<tr>

<td>Actor</td>

<td>Me</td>

</tr>

<tr>

<td>Transformation</td>

<td>Making a decision whether an archaeological investigation should be conducted or not, and if yes, that it will be conducted.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>Cultural heritage legislation need to be followed. The best alternative is if a site/monument need not to be touched and everyone sees that worthwhile; If the preservation of a site is not possible, a thorough investigation needs to be conducted and properly reported.</td>

</tr>

<tr>

<td>Owner</td>

<td>Country Administrative Board; Legislator; (Society)</td>

</tr>

<tr>

<td>Environment</td>

<td>County; Country (Sweden); Archaeological investigation site</td>

</tr>

</tbody>

</table>

### Archival management

Professional archivists are employed both by the national and regional authorities (including museums, National Heritage Board, county administrative boards, National Historical Museum). All of the interviewed professionals tended to have a background in archaeology combined with a degree or diploma studies in archival science. The archival work in the archaeology sector is focused on conventional archival duties including the management and preservation of administrative records and investigation reports (at county administrative boards and the National Heritage Board archive) and reference service with the users of the archives. It is problematic that archival materials tend to be dispersed and it may be difficult to find which institution is keeping particular records. It is also common that, for instance, museums and the National Heritage Board do not always get their copies of documents (e.g., archaeological reports) even if they would entitled to have them according to the current guidelines.

In spite of these shortcomings, from the perspective of the archivists, the current largely paper-based work flow is well-defined with few apparent problems (Ziemssen, Peeperkorn, Marushja). The major future challenges noted by Ziemssen and Peeperkorn are the implementation of the process based model of describing of archival records included in the new directives of the Swedish National Archives and a transition from a current paper-based management of archival records to a digital archive. In addition to the technical questions of managing digital archival documents, the digitisation of archaeological information work also raises questions of what types of digital information should be archived or not. Even if from an archival management point of view, for instance, material from exhibitions relating to a particular project, video clips, blog posts and other social media content should be archived together with other documents, '_it is very difficult to get people [others than archivists] to understand why it is so important_' (Peeperkorn). As Peeperkorn and Ziemssen emphasise, the problem is less in the lack of technical possibilities to archive relevant digital data, but to convince colleagues of its relevance. At the museums with less established archival procedures, an additional challenge relates to sometimes relatively weak records management culture and the development of the management of administrative archives and document management.

A significant aspect of the archivist work role is that the priorities and requirements of (administrative) archiving are very different from those of data archivists who are working with the preservation and publishing of research data. The archival function focuses on the management of administrative documents (e.g., a decision of an archaeological investigation) and documentation of administrative process by archiving related public documents. The contents of these documents (including archaeological reports) are not directly relevant for archivist work role similarly to the management of other documentation material that is currently black-boxed as an appendix to the archival record. The appraisal of other documentation material is independently conducted by the field archaeologist. The practices vary according to Naphta, who notes that sometimes the documentation material includes all economic details and hand-written notes even if it might be reasonable to reduce, according to him, at least the amount of double documentation.

<table class="center"><caption>  
Table 5: Archival management.  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th></th>

</tr>

<tr>

<td>Client</td>

<td>Society; Employer organization</td>

</tr>

<tr>

<td>Actor</td>

<td>Me</td>

</tr>

<tr>

<td>Transformation</td>

<td>Preservation of records (as information, evidence) as evidence of the activities of my organization.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>Records need to be preserved even if in my organization it can be difficult because of the lack of understanding of what ids and should be an archival record. Archaeological reports and data are a part of the record, but rather an appendix than the main thing.</td>

</tr>

<tr>

<td>Owner</td>

<td>Society</td>

</tr>

<tr>

<td>Environment</td>

<td>Office of origin of the records</td>

</tr>

</tbody>

</table>

### Finds administration

The finds administration role at regional and national museums (primarily National Historical Museum) comprises the management of physical objects originating from archaeological excavations. Roles can be mixed, there are full-time administrators financed by the county administrative boards and the national bodies, and both partly and fully contract funded positions. In the group of interviewees, Naphta, Mynheer, Clavdia and Chauchat worked most explicitly in the finds administrative work role.

The finds management have several complications. The work is largely directed by the incoming finds and how they can be accommodated both physically and documented as a part of the collections. A constant problem for antiquarians is that even if the finds are deposited in a museum, the related archaeological reports and investigation data are deposited elsewhere. CABs archive the reports and at the present the investigation data is not necessarily archived at all. Therefore, the finds administrators need to use a lot of time for retyping and reconstructing necessary contextual data in their collection management systems, as Clavdia noted, instead of focusing on enriching the earlier data with details that would be relevant from collections and museal perspectives.

The priority of the work role is to use the existing material in the museum collections. Partly the work is directed by the explicit needs and wants of (mostly) researchers who need to access the collections. Another dimension of the work is the actual and assumed interests of the general public. In both cases, the success of the work is highly dependent on the personal knowledge of the collections and the quality of documentation. Because of the diverse information needs and want, the search for useful objects can be difficult.

<table class="center"><caption>  
Table 6: Finds administration.  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th></th>

</tr>

<tr>

<td>Client</td>

<td>Museum (National Historical Museum or regional museum)</td>

</tr>

<tr>

<td>Actor</td>

<td>Me</td>

</tr>

<tr>

<td>Transformation</td>

<td>Preservation and management of finds.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>The management and preservation of finds is difficult. Find and archival materials are seldom deposited to same institutions. There are also practical problems in keeping the finds intact. At the same time, I should also engage more in promoting the use of finds for research and presentation, but because of the difficulties of managing finds and their related information I might not have enough time and opportunities to do so.</td>

</tr>

<tr>

<td>Owner</td>

<td>Museum</td>

</tr>

<tr>

<td>Environment</td>

<td>Museum</td>

</tr>

</tbody>

</table>

### Finds allocation

The National Heritage Board appoints an institution (most often a local or regional museum) to take responsibility for the management of finds coming from a particular investigation. Contractor and the appointed institution participate in the decision-making process.

The finds allocation decision is made first after an investigation, a procedure based on the legislation (the Swedish state cannot deposit its property without first knowing what it is depositing) that was criticised by several interviewees. If the receiving institution could be decided before the investigation starts the process could be faster and the contractors could negotiate directly with museums about the management and documentation of finds and it would be easier to see that all relevant parties get the necessary information in time. Further, because the final report is finished before the decision on the deposit of the finds has been made, the report does not contain information on the final location of the finds. Finally, in spite of the specific efforts to keep in pace with incoming allocation requests, the finds allocation decisions tend to be late.

A challenge with the current procedure is that necessary information for making the finds allocation decisions is not always easily available. In addition to information on the investigation and the finds, a finds allocation officer needs information about eventual earlier investigations on the same site and area, and whether there has been a praxis to deposit some specific types of finds at specific institutions. The information tends to be scattered in different places and especially the retrieval of relevant historical decisions (e.g., relating to finds from the same site or area) can be extremely difficult. Documents on decisions made prior to 1994 need to be ordered from the archives. According to the interviewees, this tends to take time and because of the lack of the possibility to browse records, it can be difficult to know which documents and decisions are the relevant ones.

<table class="center"><caption>  
Table 7: Finds allocation.  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th></th>

</tr>

<tr>

<td>Client</td>

<td>Museum (National Heritage Board or regional museum)</td>

</tr>

<tr>

<td>Actor</td>

<td>Me</td>

</tr>

<tr>

<td>Transformation</td>

<td>Preservation and management of finds.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>It is important to preserve finds and keep their related information together with the material artefacts in a safe repository designated by the current regulations and policies.</td>

</tr>

<tr>

<td>Owner</td>

<td>Swedish state</td>

</tr>

<tr>

<td>Environment</td>

<td>Museum</td>

</tr>

</tbody>

</table>

### Data archiving

The role of data archivist relates to the management, preservation and publishing of primary research data for scholarly research. In the group of interviewees Tienappel and Lodovico worked directly with data archiving. In addition, both Hans and Castorp had data archiving related duties and indirect priorities. Currently this role lies outside of the established administrative workflow and as James and Lodovico emphasised, the role and responsibilities of archaeological research data archives in the archaeological information process need to be more clearly defined. The work role relates directly to the requirements of the reuse of archaeological research data for multisite data intensive research and other types of research within and outside archaeology. Also the recent national and international policy initiatives to complement traditional forms of research output (reports, journal articles and e.g., monographs) by opening primary research data resources for peer-review and future research have increased the demand of data archiving expertise. Similarly to the researchers, the data archivists are interested in data, but unlike researchers data archivists see the relevance of the particular pieces of data in terms of how users use the data, not by the data itself. From an archivist’s perspective the data is essentially a black box that needs to be described, managed and made available according to relevant standards and the needs of the users of the data. The usability and adequate documentation of data is a matter of concern for a data archivist (as articulated e.g., by Castorp and Lodovico), but their preoccupation tends to remain on a relatively abstract level.

<table class="center"><caption>  
Table 8: Data archiving.  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th></th>

</tr>

<tr>

<td>Client</td>

<td>Scientific community</td>

</tr>

<tr>

<td>Actor</td>

<td>Me (data archivist who curates data); Researchers (who deposit data)</td>

</tr>

<tr>

<td>Transformation</td>

<td>Making uniform and well-documented sets of archaeological data available enables researchers to address new research questions.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>Data is the major outcome of research and it should be (made) available.</td>

</tr>

<tr>

<td>Owner</td>

<td>Researchers; (Research data archive)</td>

</tr>

<tr>

<td>Environment</td>

<td>Archive; Country (Sweden); Universities; World (research data is not national)</td>

</tr>

</tbody>

</table>

### Information management

Besides technical responsibility for the maintenance of administrative and information systems, the information management role incorporates responsibility for the preservation of an organization’s information so that it may be available to users. Many information management experts tend to have background in information technology or archaeological computing (James, Peeperkorn). Different information management functions can be found in most of the organizations represented by the interviewees but depending on the type and size of the organization, information management can be a primary work role or small part of the work of a project manager or an administrator.

Currently the information management suffers from a lack of standardisation of data formats and of the large number of different local approaches and applications used in the management of documentation data. The most widely used system Intrasis is developed by the largest archaeology contractor, the Contract Archaeological Service (UV), until 2015, a part of the National Heritage Board. Many contractors use their own proprietary solutions, often self-contained systems or applications based on commercial database management systems and GIS-packages. In practice Intrasis has become a de facto standard that is difficult to bypass. Three of the interviewees commented explicitly that this status and the ownership of Intrasis by the largest of the contractor is problematic. One of the problems is that the license of Intrasis is expensive for small actors and the possibilities of moving complete datasets to other environments are limited. Another issue is that county administrative boards and other Intrasis users (including smaller contractors) lack ownership of the system and feel that they have less influence on how the system is developed.

The general perspective to information and data held by information managers is is broadly similar to that of researchers working on data intensive research (compare Castorp and James), but differs by its administrative rather than research-oriented focus. Unlike researchers, the practical interest of information management is to keep and make available information as it is (as far as it is leveraged and documented in a manageable and technically uniform manner) whereas researchers (and according to two informants, large developers) tend to be concerned with the specific sites and research questions.

<table class="center"><caption>  
Table 9: Information management.  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th></th>

</tr>

<tr>

<td>Client</td>

<td>Museum, National Heritage Board, Contractor</td>

</tr>

<tr>

<td>Actor</td>

<td>Me</td>

</tr>

<tr>

<td>Transformation</td>

<td>Management of archaeological information and information systems</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>The management of (especially) digital information is a mess and there are no routines or regulations how things should be done. We have worked out our own procedures and systems to cope with the daily operations, but otherwise we are waiting that a national system would be available.</td>

</tr>

<tr>

<td>Owner</td>

<td>Society (National Historical Museum, National Heritage Board, my own organization)</td>

</tr>

<tr>

<td>Environment</td>

<td>My own organization</td>

</tr>

</tbody>

</table>

### Archaeological research

The large-scale reuse of archaeological research data can be traced back to the emergence of affordable technologies for managing and processing large amounts of data. In spite of the improved opportunities, especially the reuse data is not very common and much of the archaeological research is still focused on the generation and collection of new data, analysis of individual sites and smaller geographical areas. The difficulty of accessing and using dispersed and heterogeneous research data in general and digital data in particular is currently the most critical obstacle for conducting this type of research (Castorp, Tienappel, Hans). There are no comprehensive registers, and the existing documents and data lack uniform structures that would facilitate their retrieval and use (Hans). At the same time, the interviewees acknowledge that '_archaeological information is very complex, very difficult to describe. And it can be described in such a many ways. There are so many different levels of detail_' (Hans).

<table class="center"><caption>  
Table 10: Archaeological research.  
</caption>

<tbody>

<tr>

<th>CATWOE</th>

<th></th>

</tr>

<tr>

<td>Client</td>

<td>Scientific community; Members of the society (that can benefit of research results)</td>

</tr>

<tr>

<td>Actor</td>

<td>Me (researcher)</td>

</tr>

<tr>

<td>Transformation</td>

<td>Reusing and combining sets of earlier archaeological data (e.g., GIS, scientific data) enables me to address new types of research questions.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>Primary research data is the major outcome of archaeological investigations and it should be (made) available.</td>

</tr>

<tr>

<td>Owner</td>

<td>Me (university); National Heritage Board; Research data archive</td>

</tr>

<tr>

<td>Environment</td>

<td>University; Country (Sweden); World (research data is not national)</td>

</tr>

</tbody>

</table>

### Work roles and relationships

Even if the analysis shows that the different work roles have widely different priorities concerning the archiving and preservation of archaeology, it was apparent that all of the interviewees shared a certain abstract idea of the significance of everything that originates from an archaeological investigation and an equal premisory relevance of keeping all data in its entirety. This general scholarly ethos was in apparent conflict with the practical realities of the work roles and their related priorities. The question of appraising and deaccessioning data and finds was generally seen as a difficult question (e.g., Leo). All interviewees agreed that everything cannot be kept, but opinions remained divided on the questions of who, when, what and on what premises finds and information could be deaccessioned.

In spite of the shared idea of archaeological work and its purpose, the CATWOE-analysis revealed several disconnects between the work roles. The major issues experienced by the informants could be categorised in four groups: 1) lack of shared understanding of the division of responsibilities; 2) complexity and lack of coordination of the information process; 3) standardisation and 4) absence of infrastructure for archiving archaeological information.

The first issue relates to the _lack of shared understanding of the division of responsibilities_. The different actors do not necessarily know or follow the existing guidelines and regulations related to the distribution of documentation and division of responsibilities, or they interpret the instructions differently. Even if the complicated paper-based process is reasonably well documented and the interviews revealed that many of the informants were not knowledgeable of where certain decisions were made (e.g., finds allocation) and which authorities had responsibility for archiving particular documents (e.g., Clavdia, Behrens, Settembrini, Joachim). This was the case even if the interviewees were aware of the current legislation and the guidelines published by the National Heritage Board. Interviewees also remarked that it is difficult to communicate to others the relevance, perspective, information needs and priorities of particular work roles. The interviewed archivists considered that the archival management work role and the role of archive as a keeper of administrative documentation rather than research data was not generally understood (Tienappel, Ziemssen). Also finds administrators found that their need of field documentation in general and some specific types of information in particular was not properly catered (Clavdia, Naphta).

Secondly, several interviewees made critical remarks on the general _complexity and lack of coordination of the information process_. The large number of actors involved in the archaeological information process impedes effective coordination (e.g., James, Joachim). The independence of county administrative boards make it difficult to introduce uniform and integrated work flows (Hans, Chauchat) and the often heavy workload of regional archaeological heritage administrators (e.g., Leo) means that there are limited possibilities to follow-up decisions and to ensure that guidelines are being implemented. The small size of the regional organizations responsible for the management of local heritage means also that there are limited resources to develop comprehensive and robust long-term solutions. The problem of the lack of centralised systems and procedures was acknowledged especially in the context of digital information (e.g., Ziemssen, Behrens, Tienappel) but it was apparent that the smallness affected the entire management process. For instance, Behrens noted that the small institutions have a tendency to focus on solving individual problems in hand such as the preservation of particular types of documents or finds instead of focusing on the management of the entire process. '_It is a bit scary that nobody has really a good idea how things are done_' (Behrens). The fact that responsible archival institutions and finds deposits (each with their specific local guidelines) are selected on case by case basis does also complicate the process. Contractors have difficulties to plan their work (Chauchat), finds administrators do not get information they need and information from a single excavation ends up to be kept in several different places and in the end, none of the actors are in a position to take the onus. As Marushja aptly remarked, it is impossible to take responsibility for materials you have never received.

At the same time, however, even if a relative consensus exists that there is room for improvement in the coordination of the current information process, multiple interviewees emphasised that there has to be room for local adjustments (e.g., Settembrini, Ziemssen). Interviewees were also very empathetic about the significance of their local knowledge. Every investigation, regional museum and county administrative board was regarded as to be unique and it was considered to be difficult to establish uniform procedures that would be applicable in all conceivable cases.

Even if the interviewees occasionally saw blemish in their own contribution, they were generally satisfied with their own role in the information process and had a tendency to see the most significant problems in other organizations. A common desire of the interviewees was that initiatives like the DAP project, (unspecific) increased cooperation and the on-going national and international data-archiving initiatives would solve the incumbent problems (e.g., James, Tienappel, Hans, Ziemssen).

The third observed issue is that similarly to the ambiguity of the critique of the complexity and lack of coordination of archaeological information work, the interviewees were highly ambivalent about increased _standardisation_. Especially international standards were seen as problematic. The most eager supporters of standardisation were interviewees who worked with information management and data archiving (James, Lodovico) and to a certain extent, researchers like Hans and Castorp who suffered from the heterogeneity of documentation data even if also they all stressed the challenges and drawbacks of standards. The current situation of archaeological work and documentation was considered to be better in Sweden than in many other countries in Europe and the rest of the world. Leo assumed that it might be difficult to create standards that would be useful both in countries with less developed routines and “in countries like Sweden” and suggested that standardisation might be easier on national than on international level. Similarly to the variance of the quality of archaeological work and administration of archaeological heritage, also the differences in find types, terminology, concepts and working methods were regarded as being problematic from the standardisation point of view (Mynheer, Lodovico). Clavdia noted also that even it is relatively easy to define technical standards, for instance, for preservation of archaeological information, the standards do not guarantee that relevant documents and information would be generated or preserved. Naphta express a worry of overstandardisation and a possible loss of information because of a too close uniformity of documentation. However, even if the direct benefits of specific standards were questioned, in general, the establishing of guidelines was considered to be useful. According to Behrens, establishing a baseline minimum standard for routines and documentation would help archaeologists to conduct their work better and also to specify the minimum criteria of what is expected of documentation, documents and, for instance, their preservation.

Finally, the fourth issue relates to the _absence of an infrastructure for archaeological information_ and the consequent complexity of retrieving documents and information from myriad institutions and repositories (e.g., Hans, Castorp, Settembrini, Naphta). Even if some of the problems of the interviewees to find necessary information relate to technical problems (Hans), the majority of challenges could be traced back to the organizational issues. Naphta, Hans and Settembrini considered that the need to consult archivists at the Antiquarian-Topographical Archive (ATA) of the National Heritage Board to retrieve documents was time consuming and impractical. According to the interviewed archivists, the retrieval of older material was challenging even for professionals (e.g., Peeperkorn). The interviewees pointed also that there are limitations in the available registers and search aids. In practice, it tends to be necessary to know register (diarium) identifiers or at least the municipality of the origin of the documents (Behrens, Chauchat) to retrieve them. It is not uncommon that an information-seeker needs to consult libraries, sites and monument register (FMIS), to rely on verbal information from colleagues and serendipity, and to personally seek documents in multiple archives and organizations (Hans, Chauchat) to find all documentation concerning a particular project or site. In some cases it might be even difficult to assess whether a particular document cannot be found or if does not exist (Naphta). In comparison to reports and paper-based records, getting access to digital data was considered to be even more difficult. According to the informants there are no routines for dealing with data requests (Hans, Castorp). Hans had experienced that some actors did not even consider that digital data is a public document that they would be legally obliged to release for the public.

## Discussion

The analysis shows that the management and archiving of archaeological information is a complex issue with multiple dependencies. The CATWOE analysis highlights the social rather than directly technical or methodological underpinnings of many of the current problems experienced by the interviewees. From the perspective of the organizational theorizing of White ([2008](#whi08)), the dynamics of how archaeology is archived and archaeological information is managed in Sweden can be explained in terms of an assemblage of struggles of how different participants (or using the vocabulary of White, “identities”) are attempting to orient themselves in relation to others. Many of these problems have been documented on a general level in the literature. The lack of shared understanding of the division of responsibilities is a common problem in interdisciplinary teams and complex organizations (e.g., [Mabery, Gibbs-Scharf and Bara, 2013](#mab13); [Weller, 2008](#wel08); [Macpherson and Clark, 2009](#mac09)). The complexity and lack of coordination of the information process is a another well-known problem both in archaeology (e.g., [Huvila, 2006](#huv06); [Thomas, 2006](#tho06)) and beyond (e.g., [Alavi and Leidner, 2001;](#ala01) [Foscarini, 2010](#fos10) similarly to the the paradoxical nature and consequences of standardisation (e.g., [Huvila, 2012](#huv12); [Duff and Harris, 2002](#duf02); [Ketelaar, 1996](#ket96)). The relation between commercial and scholarly interests in archaeology has been noted to be uneasy also outside of Sweden (e.g., [Henson, 2009](#hen09)). The absence and need of infrastructures for archiving archaeological information has been similarly noted in earlier studies from (data) archiving (e.g., [Merriman and Swain, 1999](#mer99); [Richards, 2002](#ric02); [Huvila, 2009b](#huv09b); [Mitcham, Niven and Richards, 2010](#mit10)) and use perspectives (e.g., [Faniel _et al._, 2013](#fan13); [Atici _et al._, 2013](#ati13)).

A major problem with the preservation and curation of archaeological information is that many of the priorities of the users and other actors involved in the process remain tacit. In terms of White ([2008](#whi08)), the relationships that could constitute an organization remain too vague. In addition to the generally accepted consensus of the importance of preserving archaeological information, the different actors tend to have an imprecise idea of the needs and wants of other actors and as, for instance, Passley ([2013](#pas13)) and Huvila ([2008b](#huv08b)) have shown earlier, the priorities of different actors can have significant differences. The shared idea of archaeology can be seen as a manifestation of effective control but in practice, it seems that this common opinion is not used as a strong enough social footing to form an unproblematic social reality for the different stakeholders. The lack of social footing ([White, 2008](#whi08)) is visible in the incomplete understanding of the stakeholder priorities. It is a factor that contributes to the experience of that archival practices are not good enough and the heterogeneity of documentation is a problem. Consequently, it has led to the current rarity of the use of archaeological archives and reuse of data and the continuing emphasis of the archaeological research on the generation of new research data. As Merriman and Swain ([1999](#mer99)) remark, these priorities undermine the claims of the importance of keeping such archives even if both the interviews of researchers in this study and the literature ([Faniel _et al._, 2013](#fan13); [Kriesberg, Frank, Faniel and Yakel, 2013](#kri13); [Huvila, 2006](#huv06)) show that data can be effectively reused to conduct research ([Huvila, 2006](#huv06)), educate archaeologists ([Kriesberg _et al._, 2013](#kri13); [Huvila, 2006](#huv06)) and to address entirely new types of research questions. Similarly to how organizations, even such document centric institutions as museums ([Huvila, 2013](#huv13)), in general tend to be supported by social rather than inscribed forms of information, the most effective approach to find relevant information from the repositories is to use knowledgeable colleagues as a reference.

An apparent solution to the problem of the lack of a proper social footing and the non-existence of an organization is to yield control by investigating, communicating and enforcing the mutual compliance with these requirements and by systematising archaeological information and the ways of how it is produced. Several interviewees did call for a better standardisation (e.g., Behrens, James, Mynheer, Lodovico, Clavdia) i.e., control that would form a (relatively) concise social footing for their work even if the support was seldom entirely unconditional. The challenge of exercising control in the context of archaeological information work is the heterogeneity of archaeological information, the diversity of the explicit and implicit theoretical underpinnings of the different branches of archaeological scholarship and the variety and situatedness of archaeological work practices (e.g., [Huvila, 2006](#huv06); [Davidovic, 2009](#dav09); [Edgeworth, 2006](#edg06)).

The efforts at control that manifest as a high degree of standardisation of the information process, regulations of how all data should be gathered, what should be documented and what interpretations should be done, have a direct impact on the nature of archaeological fieldwork and scholarship (e.g., [Guimier-Sorbets, 1996](#gui96); [Huvila, 2006](#huv06)). The impossibility of anticipating the future needs of archaeological information does not mean that certain key needs of the current users cannot be catered to. For instance, both the present findings and earlier studies (e.g., [Faniel _et al._, 2013](#fan13)) highlight the significance of rich contextual information. The impossibility to predict the future does not imply either that more determined control efforts would not be useful in creating a regime or a set of stories in social reality that would support the emergence of a shared footing and an opportunity for concerted action. The approaches based on the standardization of technical formats and metadata-level documentation assumed by the pioneering data archives in the UK and the Netherlands (e.g., [Richards, 2002;](#ric02) [Doorn and Tjalsma, 2007](#doo07)) are attempts to seize control and find footing among other identities (i.e., work roles, users and other actors) similarly to more stringent efforts to standardise documentation practices. If the identity of data archives and central repositories as control efforts is properly acknowledged, the presence of control itself does not need to be a problem. As White ([2008](#whi08)) notes, the tension between identity and control is a necessary ingredient of further control efforts through which an identity can generate new associations and be creative. Dissolving the tension by rigid expectations of conformity could be counterproductive for the generation of new knowledge and as such, an action that would delimit the relevance of keeping archaeological archives altogether. If conformity takes over creativity, identities might lose their potential to interact in different realms of how things are known and knowable in archaeology.

A related issue to the control efforts is the question of evaluation and quality. The interviews showed that at present the information process incorporates very little follow-up. Administrators were confident that contractors produce high quality reports and make informed decisions on the deaccessioning of finds and information. The main reason given by the interviewees was the lack of resources to properly follow on-going projects. At the same time, however, the interviews indicated the presence of a high level of trust on colleagues and the process that seemed to compensate for an immediate need to scrutinise the process in a greater detail. It seems that a certain generic archaeological identity (see [White, 2008, pp. 1-2](#whi08)) has acquired enough social footing to function as a stable standpoint for the identities to relate to instead of compelling them to orient themselves by seeking control. Instead of making remarks on the recurrent problems with the quality of the contemporary documentation, the interviewees tended to complain that some relevant pieces of information might be missing and especially that reports and documents did not always reach all relevant users.

Even if some of the interviewees (e.g., James, Lodovico, Castorp) saw the absence of information as a control-related matter, the issue can be explained by the previously discussed lack of knowledge of the network i.e. of other actors and their needs. It seems that in the light of the framework of White ([2008](#whi08)), much of the current challenges of archaeological archiving in Sweden can be traced back to this particular issue. The existing shared understanding of the premises of archaeological work appears to provide a relatively firm social footing for a functioning infrastructure. That what seemed to be missing was a common effort to make the relationships and identities to come into being in interactions and endeavours to seize control in the network of relationships. A strong centralised control effort can undoubtedly solve many of the technical issues described by the interviewees (e.g., where to submit specific documents and which institution has the legal responsibility to do what), but it is unclear whether an externally given rather than internally emerging control venture would address the informational and practical needs of the users and other actors. The studies of the existing archaeological data archives show the usefulness of centralised services as a part of a nationwide archaeological information infrastructure, but underline also the need of a close involvement and qualitative understanding of the perspectives and needs of all actors (e.g., [Beagrie and Houghton, 2013](#bea13); [Doorn and Tjalsma, 2007](#doo07)). A Whitean ([2008](#whi08)) reading of the organization (or in terms of classical organizational theory, the lack of organization) of the management of archaeological information in Sweden suggests strongly that infrastructure-wide problems are not solved by waiting for external standards or regulations or by focusing on local nuisances. They require attention and initiative from all involved parties, not merely their participation as informants or advisors.

## Conclusions

Analysis of the interview record shows that there are multiple technical, legislative, conceptual and structural factors that influence and complicate the building, management and use of archaeological archives. Privatisation of archaeological fieldwork, the diversity of involved participants, and often diverging practical and statutory requirements and responsibilities of preserving different types of materials. Further, the digitization and growth of the amount documentation material has brought demands for effective means of capturing and preserving new forms of data, but also a need to reconsider the concepts of “archaeological archive” and ”archaeological data”, and their functions in archaeology and the society as a whole.

It is tempting to see the problems in the process as technical issues relating to a lack of central repositories, unclear regulations, poor follow-up and undefined responsibilities. But a closer look at the users, contributors, managers and developers of archaeological archiving and their work roles and relations suggest that the problems of relating to the division of responsibilities, coordination of the information process, lack of standardisation and the non-infrastructure like nature of the current repositories can be traced back to the organization of the information work. In the light of the organizational theory of White ([2008](#whi08)), the central challenge of archiving archaeology is not the lack of standardisation of information or information processes but the lack of mutual control efforts by the involved actors (identities) that would provide them with orientation in relation to other identities. Instead of speculating who should take responsibility or trying to develop a perfect standard, the different actors could benefit of being more proactive in their efforts to seize control of the process. Archaeology as a practice and a body of theory does undoubtedly constitute a solid enough social footing for exercising control and articulating identities but what is missing is a strong enough _effort_ from the part of individual actors that would lay foundations to the emergence of concerted organization of archaeological archiving. The infrastructure cannot be given by a single actor alone. It has to be established as a network of relationships between all parties.

## Acknowledgements

This study has been conducted under the auspices of the Archaeological Information in the Digital Society project, funded by the Swedish Research Council Grant (VR) grant 340-2012-5751.

## About the author

**Isto Huvila** holds the chair in Library and Information Science at the Department of ALM (Archives, Library & Information, Museum & Cultural Heritage Studies), Uppsala University, Sweden. He received his Master's degree in Cultural History from University of Turku, Finland and his PhD in Information Studies from Åbo Akademi University, Finland. He can be contacted at [isto.huvila@abm.uu.se](mailto:isto.huvila@abm.uu.se)

#### References

*   Aitchison, K. (2009). Standards and guidance in archaeological archiving: the work of the Archaeological Archives Forum and the Institute For Archaeologists. _Grey Journal, 5_(2), 67–71.
*   Alavi, M. & Leidner, D. E. (2001). Review. Knowledge management and knowledge management systems: conceptual foundations and research issues. _MIS Quarterly, 25_(1), 107–136.
*   Archaeology Data Service & Digital Antiquity. (2011). [Project metadata](http://www.webcitation.org/6akc8hcfH). In K. Niven & J. Watts (Eds.) _Guides to good practice_. York. Retrieved from http://guides.archaeologydataservice.ac.uk/g2gp (Archived by WebCite® at http://www.webcitation.org/6akc8hcfH)
*   Atici, L., Kansa, S., Lev-Tov, J. & Kansa, E. (2013). Other people´s data: a demonstration of the imperative of publishing primary data. _Journal of Archaeological Method and Theory, 20_(4), 663–681.
*   Austin, T. & Mitcham, J. (2007). _Preservation and management strategies for exceptionally large data formats: ’big data’_. Swindon, UK: English Heritage and ADS.
*   Backhouse, P. (2006). Drowning in data? Digital data in a British contracting unit. In T. L. Evans & P. T. Daly (Eds.) _Digital archaeology: bridging method and theory_, (pp. 43–49). London; New York, NY: Routledge.
*   Balkestein, M. & Tjalsma, H. (2007). The ADA approach: retro-archiving data in an academic environment. _Archival Science, 7_(1), 89–105.
*   Beagrie, N. & Houghton, J. (2013). _The value and impact of the Archaeology Data Service: a study and methods for enhancing sustainability_. Salisbury, UK: Charles Beagrie Ltd.
*   Bibby, D. (2011). [Collection and preservation of digital excavation data – securing the digital past for the future. A case study from Baden-Württemberg.](http://www.webcitation.org/6akcW54D3) In _Proceedings of the 14th International Congress Cultural Heritage and New Technologies Vienna, 2009_, (pp. 89–96). Vienna: Stadtarchäologie Wien. Retrieved from http://www.stadtarchaeologie.at/wp-content/uploads/eBook_WS14_Part3_AT_1.pdf (Archived by WebCite® at http://www.webcitation.org/6akcW54D3)
*   Björckebaum, M. & Mossberg, M. (2009). _MKB i praktiken: aktörers inställning till och agerande kring kulturmiljö i transportinfrastrukturens MKB-arbete_. [EIA in practice: actors' attitudes and behaviour surrounding the cultural environment in transport infrastructure EIA work] Stockholm: KMV Forum.
*   Blandford, A., Stelmaszewska, H. & Bryan-Kinns, N. (2001). Use of multiple digital libraries: a case study. In _JCDL ´01: Proceedings of the 1st ACM/IEEE-CS joint conference on Digital libraries_, (pp. 179–188). New York, NY: ACM Press.
*   Borgman, C. (2003). _From Gutenberg to the global information infrastructure: access to information in the networked world_. Cambridge, MA: The MIT Press.
*   Bott, V. (2003). _Access to archaeological archives – a study for Resource and the Archaeological Archives Forum_. London: Resource, The Council for Museums, Archives and Libraries, London.
*   Braccini, A. M. & Federici, T. (2010). An IS for archaeological finds management as a platform for knowledge management: the ArcheoTRAC case. _VINE, 40_(2), 136–152.
*   Brown, D. H. (2011). _Archaeological archives: a guide to best practice in creation, compilation, transfer and curation_. Reading, UK: Archaeological Archives Forum.
*   Cantone, F. (2005). _Sistemi di gestione informatizzata integrata dei dati archeologici. protocolli operativi, quality management, processi di trasferimento tecnologico. linee di intervento presso l'heraion alla foce del sele e il sito di Cuma_. [Integrated computerized management systems of archaeological data. operational protocols, quality management, technology transfer processes. lines of action at the Heraion at the mouth of the Sele and the Cuma site] (Unpublished doctoral dissertation, Universitá degli Studi di Napoli Federico II, Naples, Italy).
*   Checkland, P. (1981). _Systems thinking, systems practice_. Chichester, UK: Wiley.
*   Chowdhury, G. G. (1999). _Introduction to modern information retrieval_. London: Library Association.
*   Dallas, C. (2009). [From artefact typologies to cultural heritage ontologies: or, an account of the lasting impact of archaeological computing.](http://www.webcitation.org/6hj8EgLiE) _Archeologia e Calcolatori, 20_, 205–222\. Retrieved from http://soi.cnr.it/archcalc/indice/PDF20/17_Dallas.pdf (Archived by WebCite® at http://www.webcitation.org/6hj8EgLiE)
*   Davidovic, A. (2009). _Praktiken archäologischer Wissensproduktion – eine kulturanthropologische Wissenschaftsforschung. [Practices of archaeological knowledge production - a cultural anthropological study of science]._. Münster, Germany: Ugarit-Verlag.
*   Degraeve, A. (2011). [Archiving the archaeological heritage in the Brussels Capital Region, Belgium - a challenge for the future.](http://www.webcitation.org/6akcfYuxi) In _Proceedings of the 14th International Congress Cultural Heritage and New Technologies Vienna, 2009_, (pp. 592–601). Vienna: Stadtarchäologie Wien. Retrieved from http://www.stadtarchaeologie.at/wp-content/uploads/eBook_WS14_Part5_Poster.pdf (Archived by WebCite® at http://www.webcitation.org/6akcfYuxi).
*   Degraeve, A. (2012). L’accessibilité de nos collections patrimoniales et l’Union européenne [The accessibility of our heritage collections and the European Union]. In _Documentation du patrimoine, vol. 2 of Thema & Collecta_, (pp. 36–41). Brussels: ICOMOS Wallonie-Bruxelles.
*   Desjardin, E., Roo, B. D., Maeyer, P. D. & Bourgeois, J. (2014). [_From the state of the art on 4D data modeling to the archaeologists’ dream for the future_.](http://www.webcitation.org/6altiuzod) Paper presented at the CAA 2014 conference, April 22-25, Paris. Retrieved from http://caa2014.sciencesconf.org/29720 (Archived by WebCite® at http://www.webcitation.org/6altiuzod).
*   Doorn, P. & Tjalsma, H. (2007). Introduction: archiving research data. _Archival Science, 7_(1), 1–20.
*   Drap, P. & Long, L. (2001). Towards a digital excavation data management system: the "Grand Ribaud F" Estruscan deep-water wreck. In D. A. A. Chalmers & D. Fellner (Eds.) _Proceedings of the 2001 conference on Virtual reality, archeology, and cultural heritage, VAST ’01_, (pp. 17–26). New York, NY: ACM.
*   DRIVER Project (2015). _[DRIVER Digital Repository Infrastructure Vision for European Research](http://www.webcitation.org/6aluBr9kI)_. Retrieved from http://www.driver-community.eu. Archived by WebCite® at http://www.webcitation.org/6aluBr9kI).
*   DuBois, A. (2003). Close reading: an introduction. In F. Lentricchia & A. DuBois (Eds.) _Close reading: a reader_, (pp. 1–40). Durham, NC: Duke University Press.
*   Duff, W. M. & Harris, V. (2002). Stories and names: archival description as narrating records and constructing meanings. _Archival Science, 2_(3), 263–285.
*   Dunn, S. (2006). ECAI – E-science methods in archaeology: development, support and infrastructure in the UK. Abstract of a paper presented in the _34th Annual Meeting and Conference of Computer Applications and Quantitative Methods in Archaeology CAA2006_, Fargo, April 18-21, 2006.
*   Edgeworth, M. (2006). _Ethnographies of archaeological practice: cultural encounters, material transformations_. Lanham, MD: Altamira Press.
*   Edwards, R. (2011). _Archaeological archives and museums 2012_. London: Society of Museum Archaeologists.
*   Esteva, M., Trelogan, J., Rabinowitz, A., Walling, D. & Pipkin, S. (2010). From the site to long-term preservation: a reflexive system to manage and archive digital archaeological data. In _Archiving 2010—Preservation Strategies and Imaging Technologies for Cultural Heritage Institutions and Memory Organizations, Final Program and Proceedings_, (pp. 1-6). Washington, DC:Society for Imaging Science and Technology.
*   Faniel, I., Kansa, E., Kansa, S.W., Barrera-Gomez, J. & Yakel, E. (2013). The challenges of digging data: a study of context in archaeological data reuse. In _Proceedings of the 13th ACM/IEEE-CS joint conference on Digital libraries, JCDL ’13_, (pp. 295–304). New York, NY: ACM.
*   Foscarini, F. (2010). Understanding the context of records creation and use: hard versus soft approaches to records management. _Archival Science, 10_(4), 389–407.
*   Fox, E. A. (2005). Digital libraries: archaeology, automation, and enhancements. Paper presented at the _International Advanced Digital Library Conference (IADLC)_, Nagoya University, Japan, August 25-26.
*   Fox, E. A., Akscyn, R. M., Furuta, R. K. & Leggett, J. J. (1995). Digital libraries. _Communications of the ACM_, 38(4), 22–28.
*   Fox, E. A. & Marchionini, G. (1998). Toward a worldwide digital library. _Communications of the ACM, 41_(4), 29–32.
*   Franceschini, M. D. (2011). [The Accademia pilot project in Hadrian´s villa near Tivoli (Rome, Italy). Problems in archiving ancient and modern data.](http://www.webcitation.org/6aluiIRa3) In _Proceedings of the 14th International Congress Cultural Heritage and New Technologies Vienna, 2009_, (pp. 142–152). Vienna: Stadtarchäologie Wien. Retrieved from http://www.stadtarchaeologie.at/wp-content/uploads/eBook_WS14_Part3_AT_1.pdf (Archived by WebCite® at http://www.webcitation.org/6aluiIRa3)
*   Gattiglia, G. (2009). Open digital archives in archeologia. Good practice. _Archeologia e Calcolatori, 20_, 49–64.
*   Guimier-Sorbets, A.-M. (1996). Le traitement de l’information en archéologie: archivage, publication et diffusion [Management of information in archaeology : archiving, publishing and dissemination]. _Archeologia e Calcolatori, 7_, 985–996.
*   Henson, D. (2009). What on earth is archaeology? In E. Waterton & L. Smith (Eds.) _Taking archaeology out of heritage_, (pp. 117–135). Newcastle upon Tyne, UK: Cambridge Scholars Publishing.
*   Hirsjärvi, S. & Hurme, H. (1995). _Teemahaastattelu. [Thematic interview]_. Helsinki: Yliopistopaino.
*   Huvila, I. (2006). _[The ecology of information work – a case study of bridging archaeological work and virtual reality based knowledge organisation](http://www.webcitation.org/5ZzyLARUP)_. Åbo (Turku), Finland: Åbo Akademi University Press. Retrieved from http://doria32-kk.lib.helsinki.fi/bitstream/handle/10024/4153/TMP.objres.83.pdf?sequence=2 (Archived by WebCite® at http://www.webcitation.org/5ZzyLARUP)
*   Huvila, I. (2008a). [Information work analysis: an approach to research on information interactions and information behaviour in context](http://www.webcitation.org/6aluldnMd). _Information Research, 13_(3). Retrieved from http://informationr.net/ir/13-3/paper349.html (Archived by WebCite® at http://www.webcitation.org/6aluldnMd)
*   Huvila, I. (2008b). To whom it may concern? The users and uses of digital archaeological information. In A. Posluschny, K. Lambers & I. Herzog (Eds.) _CAA 2007\. Computer Applications and Quantitative methods in Archaeology_. Bonn, Germany: Dr. Rudolph Habelt Gmbh.
*   Huvila, I. (2008c). Work and work roles: a context of tasks. _Journal of Documentation, 64_(6), 797–815.
*   Huvila, I. (2009a). Ecological framework of information interactions and information infrastructures . _Journal of Information Science, 35_(6), 695–708.
*   Huvila, I. (2009b). Steps towards a participatory digital library and data archive for archaeological information. In _Proceedings of the 10th Libraries in the Digital Age (LIDA) 2009 Conference. Dubrovnik and Zadar, Croatia_, (pp. 149–159). Zadar, Croatia: University of Zadar.
*   Huvila, I. (2012). [Being formal and flexible: semantic wiki as an archaeological e-science infrastructure](http://www.webcitation.org/6alusBAD9). In M. Zhou, I. Romanowska, Z. Wu, P. Xu & P. Verhagen (Eds.) _Revive the Past: Proceeding of the 39th Conference on Computer Applications and Quantitative Methods in Archaeology, Beijing, 12-16 April 2011_, (pp. 186–197). Amsterdam, The Netherlands: Amsterdam University Press. Retrieved from http://dare.uva.nl/aup/nl/record/412958 (Archived by WebCite® athttp://www.webcitation.org/6alusBAD9)
*   Huvila, I. (2013). How a museum knows? Structures, work roles, and infrastructures of information work. _Journal of the American Society for Information Science and Technology, 64_(7), 1375–1387.
*   Isaksen, L., Martinez, K., Gibbins, N., Earl, G. & Keay, S. (2009). Linking archaeological data. In Frischer, B.; Crawford, J. W. & Koller, D. (Eds.) _Making History Interactive. Computer Applications and Quantitative Methods in Archaeology (CAA). Proceedings of the 37th International Conference, Williamsburg, Virginia, United States of America, March 22-26_, (pp. 130-136). Oxford: Archaeopress.
*   Jeffrey, S. (2012). A new digital dark age? Collaborative web tools, social media and long-term preservation. _World Archaeology, 44_(4), 553–570.
*   Kenny, J., Kilbride, W. & Richards, J. D. (2003). Enter the ARENA: Preservation and access for Europe’s archaeological archives. In M. Dörr & A. Sarris (Eds.) _Digital heritage of archaeology CAA 2002 : Computer applications of the quantitative methods in archaeology : Proceedings of the 30th conference, Heraklion, Crete, April 2002_, (pp. 349–354). Athens: Archive of Monuments and Publications.
*   Ketelaar, E. (1996). Archival theory and the Dutch manual. _Archivaria, 41_(1), 31–40.
*   Kintigh, K. (2006). The promise and challenge of archaeological data integration. _American Antiquity, 71_(3), 567–578.
*   Kriesberg, A., Frank, R. D., Faniel, I. M. & Yakel, E. (2013). The role of data reuse in the apprenticeship process. _Proceedings of the American Society for Information Science and Technology, 50_(1), 1–10.
*   Lincoln, Y. S. & Guba, E. G. (1985). _Naturalistic inquiry_. Beverly Hills, CA: Sage.
*   Lönn, M. (2006). _Uppdragsarkeologi och forskning. [Contract archaeology and research]._ Gothenburg, Sweden: Gothenburg University.
*   Lucas, G. (2010). Time and the archaeological archive. _Rethinking History, 14_(3), 343–359.
*   Mabery, M. J., Gibbs-Scharf, L. & Bara, D. (2013). Communities of practice foster collaboration across public health. _Journal of Knowledge Management, 17_(2), 226–236.
*   Macpherson, A. & Clark, B. (2009). Islands of practice: conflict and a lack of community in situated learning. _Management Learning, 40_(5), 551–568.
*   McManamon, F. P., Kintigh, K. W. & Brin, A. (2010). [Digital antiquity and the digital archaeological record (tDAR): broadening access and ensuring long-term preservation for digital archaeological data.](http://www.webcitation.org/6alvIH4dX) _CSA Newsletter, 23_(2). Retrieved from http://csanet.org/newsletter/fall10/nlf1002.html (Archived by WebCite® at http://www.webcitation.org/6alvIH4dX)
*   Merriman, N. & Swain, H. (1999). Archaeological archives: serving the public interest? _European Journal of Archaeology, 2_(2), 249–267.
*   Mitcham, J., Niven, K. & Richards, J. (2010). [Archiving archaeology: introducing the guides to good practice](http://www.webcitation.org/6alvNzZyS). In _Proceedings of iPRES 2010 7th International Conference on Preservation of Digital Objects_, (pp. 1-5). Vienna: Technische Universität Wien. Retrieved from http://www.ifs.tuwien.ac.at/dp/ipres2010/papers/mitcham-54.pdf (Archived by WebCite® athttp://www.webcitation.org/6alvNzZyS)
*   Ottander, J. (2012). _Bedömnings- och utvärderingsmetoder inom uppdragsarkeologin : en studie av anbudsförfaranden genomförda under åren 2008-2009_. [Assessment and evaluation methods in contract archaeology : a study of tender processes in 2008-2009] Stockholm: Riksantikvarieämbetet.
*   Palaiologk, A. S., Economides, A. A., Tjalsma, H. D. & Sesink, L. B. (2012). An activity-based costing model for long-term preservation and dissemination of digital research data: the case of DANS. _International Journal on Digital Libraries, 12_(4), 195–214.
*   Passley, C. E. (2013). Determining differences between archival staff and restorers ranking of training topics for disaster restoration projects. _Collection Management, 38_(4), 267–300.
*   Raghavan, A., Vemuri, N., Shen, R., Gonçalves, M., Fan, W. & Fox, E. (2005). Incremental, semi-automatic, mapping-based integration of heterogeneous collections into archaeological digitallibraries: Megiddo case study. In _Research and Advanced Technology for Digital Libraries: Proceedings of the 9th European Conference, ECDL 2005, Vienna, Austria, September 18-23, 2005._ , (pp. 139-150). Berlin: Springer. (Lecture Notes in Computer Science, Vol. 3652).
*   Reilly, P. & Rahtz, S. (1992). _Archaeology and the information age_. London and New York: Routledge.
*   Richards, J. D. (2002). Digital preservation and access. _European Journal of Archaeology, 5_(3), 343–366.
*   Richards, J. D. (2006). Electronic publication in archaeology. In T. L. Evans & P. T. Daly (Eds.) _Digital archaeology: bridging method and theory_, (pp. 191–201). London: Routledge.
*   Rubin, I. & Rubin, H. J. (2005). _Qualitative interviewing: the art of hearing data_. Thousand Oaks, CA: Sage Publications.
*   Schäfer, F., Heinrich, M. & Jahn, S. (2014). _Stakeholderanalyse 2013: zu Forschungsdaten in den Altertumswissenschaften. Teil 1: Ergebnisse. [Stakeholder analysis 2013: on research data in the study of the antiquity]_. Berlin: IANUS - DAI.
*   Schibbye, K., Frisk, M., Sander, B. & Westerlind, A. (2007). _Kulturmiljön som resurs - hur kulturmiljöaspekterna på ett ändamålsenligt sätt kan behandlas i miljöbedömningar och miljökonsekvensbeskrivningar. [Cultural landscape as a resource - how the aspects of cultural landscape can be managed in appropriately in environmental assessments and descriptions of environmental consequences]_. Stockholm: Riksantikvarieämbetet.
*   Shaw, R., Corns, A. & McAuley, J. (2009). [_Archiving archaeological spatial data: standards and metadata._](http://www.webcitation.org/6alvq4Dpb) Paper presented at _37th Annual Computer Applications and Quantitative Methods in Archaeology (CAA) Conference, Williamsburg, Virginia_. Retrieved from https://www.researchgate.net/publication/242708039_Archiving_Archaeological_Spatial_Data_Standards_and_Metadata. (Archived by WebCite® at http://www.webcitation.org/6alvq4Dpb)
*   Simmons, J. P., Nelson, L. D. & Simonsohn, U. (2011). False-positive psychology: undisclosed flexibility in data collection and analysis allows presenting anything as significant. _Psychological Science, 22_(11), 1359–1366.
*   Smyth, D. S. & Checkland, P. (1976). Using a systems approach: the structure of root definitions. _Journal of Applied Systems Analysis, 5_(1), 75–83.
*   Stenzer, A., Woller, C. & Freitag, B. (2011). MonArch: digital archives for cultural heritage. In _Proceedings of the 13th International Conference on Information Integration and Web-based Applications and Services, iiWAS ’11_, (pp. 144–151). New York, NY: ACM.
*   Stoll-Tucker, B. (2011). [The current situation of digital archiving in Germany’s state archaeology](http://www.webcitation.org/6akcW54D3). In _Proceedings of the 14th International Congress Cultural Heritage and New Technologies Vienna, 2009_, (pp. 191–195). Vienna: Stadtarchäologie Wien. Retrieved from http://www.stadtarchaeologie.at/wp-content/uploads/eBook_WS14_Part3_AT_1.pdf (Archived by WebCite® at http://www.webcitation.org/6akcW54D3)
*   Swain, H. (2006). Archaeological archives in britain and the development of the London Archaeological Archive and Research Centre. In N. Agnew & J. Bridgland (Eds.) _Of the Past, for the Future: Integrating Archaeology and Conservation: Proceedings of the Conservation Theme at the 5th World Archaeological Congress, Washington, D.C., 22-26 June 2003_. Washington, DC: Getty Publications.
*   Sweden. _National Heritage Board._ (2012). _[Arkivering av arkeologisk dokumentation.](http://www.webcitation.org/6alvR2G1V)_ [Archiving of archaeological documentation]. Stockholm: Riksantikvarieämbetet. Retrieved from http://www.raa.se/publicerat/varia2012_32.pdf (Archived by WebCite® at http://www.webcitation.org/6alvR2G1V)
*   Sweden. _National Heritage Board._ (2012a). _Vägledning för tillämpning av kulturminneslagen uppdragsarkeologi (2 kap, 10-13) Tillämpning av Riksantikvarieämbetets föreskrifter och allmänna råd avseende verkställigheten av 2 kap. 10-13 lagen (1988:950) om kulturminnen m.m. [Guidelines for the application of the Cultural Heritage Act in contract archaeology (2 Chapter, 10-13) Application of the regulations and general advice of the National Heritage Board on the execution of the Chapter 2 10-13 of the Act (1988:950) on cultural heritage etc. ]_. Stockholm: Riksantikvarieämbetet.
*   Sweden. _National Heritage Board._ (2012b). _Uppdragsarkeologi (2 kap, 10 - 13 §§). Rapportering och dokumentationsmaterial. [Contract archaeology (Chapter 2, §10-13) Reporting and documentation material]_. Stockholm: Riksantikvarieämbetet.
*   Sweden. _Swedish Arts Council_ (2013). _Föreskrifter om ändring i Riksantikvarieämbetets föreskrifter (KRFS 2007:2) och allmänna råd avseende verkställigheten av 2 kap. kulturmiljölagen (1988:950);. KRFS 2013:3\. [Regulations on the changing of the regulations for the National Heritage Board (KRFS 2007:2) and general advice on the application of the Chapter 2 of Cultural Landscapes Act (1988:950)]_. Stockholm: Kulturrådet.
*   Teddlie, C. & Yu, F. (2007). Mixed methods sampling. _Journal of Mixed Methods Research, 1_(1), 77–100.
*   Thomas, J. (2006). The great dark book: archaeology, experience, and interpretation. In J. Bintliff (Ed.) _A Companion to Archaeology_, (pp. 21–36). Malden: Blackwell Publishing.
*   Weller, T. (2008). _Information history_. Oxford: Chandos Publishing.
*   Whitcher, S., Kansa, E. C. & Schultz, J. M. (2007). Open context, data sharing and archaeology. _Near Eastern Archaeology, 70_(4), 188–201.
*   White, H. C. (2008). _Identity and control: how social formations emerge_. Princeton, NJ: Princeton University Press.
*   White, H. C. & Godart, F. (2007). Stories from identity and control. _Sociologica, 3_(1), 1-17.