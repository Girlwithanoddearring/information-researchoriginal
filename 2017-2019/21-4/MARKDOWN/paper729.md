#### vol. 21 no. 4, December, 2016

# Topical network of breast cancer information in a Korean American online community: a semantic network analysis

#### [Min Sook Park](#author) and [Hyejin Park](#author)

#### Abstract

**Introduction.** Health information-seeking and sharing online has become immensely intertwined with day-to-day information-seeking of US immigrants with health concerns. Despite the consist recognition of unique health needs among different US immigrant communities, little is known about the distinctive patterns and extent of health information behaviour of Korean Americans, who are one of the most rapidly growing Asian immigrant populations in the USA.  
**Method.** The study uses online discussion forums whose participants are Korean Americans living in the USA. A mixed methodology of both content and semantic network analyses was used for data analysis.  
**Results.** It was revealed that forum participants discuss two dominant topics: breast cancer tests and treatment. Although breast cancer tests was the central topic, the emphasis was on diagnosis rather than prevention. Additionally, the data indicates that participants derive psychological or emotional support from breast cancer information-seeking and sharing. Moreover, concerns stemming from insurance and financial difficulties are the biggest causes of distress for those affected by breast cancer.  
**Conclusions.** The findings from this study demonstrate the complexity of health information-seeking behaviour and the subsequent vast spectrum of concerns. Online health information behaviour involving breast cancer is often copnnected not only with medical practices but also with social, financial and psychological challenges as well.

## Introduction

Health-related information, including that about breast cancer, is actively sought and shared through the participatory World Wide Web (the Web) or social media. Indeed, the Web has become immensely intertwined, particularly when it comes to day-to-day information-seeking among immigrants with health concerns ([Kim and Yoon, 2012](#ky12); [Suarez _et al._, 2000](#srv00)). The benefits that social media provides for health information seekers should not be underestimated, particularly for underrepresented groups such as immigrants. As it is for general Internet users, the Web is a useful resource for immigrants with health concerns ([Caidi, Allard and Quirke, 2010](#caq10); [Kim and Yoon, 2012](#ky12); [Suarez _et al._, 2000](#srv00)). Yet, the health information behaviour of different ethnic groups has been understudied, despite the sheer diversity and growth of US immigrant communities. Previous studies have consistently recognised that different ethnic groups have distinctive patterns and extent of health information behaviour, which are impacted by various factors including cultural and social frameworks ([Caidi _et al._, 2010](#caq10); [Kim, Kreps and Shin, 2015](#kks15)). Also, immigrant groups were often reported to experience unique health needs yet disparity in information access ([Caidi _et al._, 2010](#caq10); [Coronges, Sykes and Valente, 2007](#csv07); [Marchionini, 1995](#m95); [Ruiz and Barnett, 2015](#rb15)).

The current study, therefore, intends to enhance the understanding of health information behaviour of Korean Americans, which is one of the most rapidly growing Asian immigrant populations in the USA. These individuals present distinct socio-cultural values and lifestyles. Particular consideration was given to identifying their unique health information needs and shared information regarding breast cancer, as well as the behavioural patterns emerging from the identified needs and shared information. This study focuses exclusively on information relating to breast cancer in order to understand their online health information behaviour patterns, particularly since breast cancer is a leading cause of death for women ([Center for Disease Control and Prevention, 2013](#cdc13); [National Center for Health Statistics, 2011](#nch11); [Choi _et al._, 2010](#clp10); [Oh, Jun, Zhao, Kreps and Lee, 2015](#ojz15)).

In order to probe the patterns of major topics within questions and replies in an online community specialised for Korean Americans, we employed semantic network analysis along with content analysis. Breast cancer may lead to a wide range of medical and other non-medical concerns, such as physical, psychological, social and financial changes, since it is a serious and complex disease. Previous studies regarding Korean Americans and breast cancer provide valuable information about the diverse dimensions of breast cancer among this study group, yet they fell short in addressing relationships between the various aspects of the disease. Semantic network analysis enabled the researchers to capture the latent relationships between topics about breast cancer so as to observe the unique information-seeking and sharing behavioural patterns of this community.

## Related literature

For those with health concerns, the Internet offers new modes of health information-seeking and sharing, including e-mail counselling, bulletin boards and self-help ([Gooden and Winefield, 2007](#gw07); [Liberman and Goldstein, 2005](#lg05); [Winefield, Coventry, Pradhan, Harvey and Lambert, 2000](#wcp00)). Online information exchange affords users multiple benefits including immediate access to useful information, searchable content, easy tracking of health information, the opportunity to share information, emotional support and help with health-related decision-making ([Oh _et al._, 2015](#ojz15)). Through online communication with other patients, many people also receive critical psychological support, a need that is often unfulfilled by doctors ([Lee and Hawkins, 2010](#lh10)). Online discussion is one of the most positive activities for those with chronic disease ([Fox and Purcell, 2010](#fp10)). This medium offers opportunities for those with health concerns to engage in asynchronous written interactions with others interested in the designated topic ([Gooden and Winefield, 2007](#gw07); [Winefield _et al._, 2000](#wcp00)). Especially when it comes to marginalised communities such as immigrants, these online interactions often become an important alternative to traditional caregiving or support, particularly for those who feel dissatisfied with their current circumstances, such as face-to-face peer, family and professional care ([Winefield _et al._, 2000](#wcp00); [Caidi _et al._, 2010](#caq10)). Thus, their sense of health and wellbeing is enhanced. For immigrants who are often isolated due to barriers like language and cultural differences, the Internet offers culturally and linguistically supported information ([Caidi _et al._, 2010](#caq10); [Napier _et al._, 2014](#nab14); [Savolainen, 2008](#s08)), along with other an array of other benefits. As a result, many patients and caregivers actively seek and share health information online to cope with serious diseases, such as breast cancer.

Immigrants, including Korean Americans, however, often have difficulty in locating and accessing information in mediums that are understandable and usable to them due to many challenges such as language, and cultural and economic challenges, along with limited interpersonal networks in the United States ([Caidi _et al._, 2010](#caq10)). The most prevalent limitations include language ([Oh, Kreps, Jun and Ramsey, 2011](#okj11); [Park and Park, 2014](#pp14); [Thomson and Hoffman, 2009](#th09)) and lack of health insurance ([Choi _et al._, 2010](#clp10); [Oh _et al._, 2015](#ojz15)). When Korean Americans experience hardship in terms of accessing information resources, they tend to use Korean American online communities. Providing health information in the users' primary language is identified as critical for the immigrants ([Ahmad _et al._, 2004](#asv04); [Thomson and Hoffman, 2009](#th09)). The ability to exchange information in their native language improves communication while reducing misunderstanding ([Ahmad _et al._, 2004](#asv04); [Thomson and Hoffman, 2009](#th09)).

Individual socioeconomic and cultural realities along with distinctive health needs influence immigrants' patterns of health information behaviour ([Caidi _et al._, 2010](#caq10); [Kim _et al._, 2015](#kks15)). information-seeking and sharing reflect their knowledge, skills, attitudes and preferences within the problem domain ([Marchionini, 1995](#m95); [Savolainen, 2008](#s08)). Furthermore, information-seeking is shaped by group characteristics and internalised behaviour which, in turn, is influenced by traits, attitudes or cognitive styles ([Marchionini, 1995](#m95); [Coronges, Stacy and Valente, 2007](#csv07); [Doerfel and Barnett, 1999](#db99)). Among newcomers to the USA, the Asian population is a rapidly growing segment, which increased by 46 per cent from 11.9 million to 17.3 million from 2000 to 2014 ([U.S. Census Bureau, 2015](#usc15)). Without question, this immigrant sector is multifaceted and enormous. Despite the typical research practice of aggregating national health data of Asians into one category, the US-based Asian population is in fact heterogeneous. Asian groups in the USA originate from fifty-two different countries and vary in culture, traditions, native languages and disease incidence ([Asian American Health Initiative, 2005](#aah05)). Within the Asian population, the Korean population is the fourth fastest growing as well as the fifth largest population and has grown by 41 per cent since 2000 ([U.S. Census Bureau, 2015](#usc15)).

When it comes to Korean Americans, however, there are some distinct trends. They are homogenous in race as well as some cultural traits. They also demonstrate the highest rate of non-English language usage among Asian American subgroups ([Han, Kang, Kim, Ryu and Kim, 2007](#hkk07); [Shin, Song, Kim and Probst, 2005](#ssk05)), strong association primarily with their own ethnic social networks ([Min, 1995](#min95)), and a high rate of not being insured ([Asian American policy review, 2013](#aap13); [Kao, 2010;](#k10) [Shin _et al._, 2005](#ssk05)). These important attributes have great bearing on their health information behaviour.

Korean Americans' general health characteristics also affect their information behaviour. Breast cancer is a leading cause of death and one of the most commonly diagnosed cancers among women ([Centers for Disease Control and Prevention, 2013](#cdc13); [Choi _et al._, 2010](#clp10)). The average annual per centage increase of breast cancer incidence was the highest for Korean women of all Asian groups in the USA from 1972 to 2007 ([Liu, Zhang, Wu, Pike and Deapen, 2012](#lzw12)). Although early detection of the incidence through breast cancer screening tests (e.g., mammography) improves the chances of survival ([Smith, Cokkinides and Brawley, 2009](#scb09)), Asian women, including Koreans, in the United States are less likely to be screened than white women ([American Cancer Society, 2013](#acs13)). For Korean Americans health and lifestyle have profound impacts on their health information-seeking and sharing.

The current body of research has shed light on the distinct health-related landscape pertaining to Korean Americans. Notwithstanding, research on their online health information behaviour regarding cancer is limited. Only a few studies focused on understanding information needs. [Kim and Yoon (2012)](#ky12) examined the use of an online health form by Korean American women through content analysis of questions. The findings indicate that users' health information needs consisted mainly of seeking recommendations for hospitals and doctors, causes of symptoms experienced or treatment methods. [Kim (2015)](#kks15) and his colleagues examined Korean Americans' information-seeking behaviour in the context of social support in social media. Along with social ties such as family, friends and religious organizations, Kim's study identified major topics in their information-seeking: specifically, recommendations regarding hospitals or doctors, preventive care, diagnosis, diet and exercise and medications, as well as comfort with language and sense of belonging.

Several studies about Korean Americans' behaviour regarding cancer information involve users' offline health information behaviour. [Oh _et al._ (2011)](#okj11) examined the types of cancer information searches, cancer information-seeking experiences and the awareness of major cancer information sources among 254 Korean Americans through a cross-sectional, community-based survey. The results suggest that they had a lower rate of cancer information-seeking and limited awareness of cancer information sources. Additionally, these users' experiences in seeking cancer information were somewhat negative. A separate study by [Oh _et al._ (2015)](#okj11) identified the most trustworthy cancer information sources according to Korean American women. The researchers examined perceived usefulness and limitations of cancer information sources in seven focus group interviews with thirty-four women and a survey of 152 women. The data pinpoint that these women viewed health care professionals as the most trusted cancer information source. The Internet and Korean ethnic media were also popular cancer information sources in terms of language, cultural and economic significance.

Regarding studies pertaining to non-disease-specific, health information-seeking behaviour, [Oh _et al._ (2011)](#okj11) explored the relationship between exposure to mass media and health information-seeking behaviour for Korean Americans. The study focused on demographic characteristics, variations in exposure to different health information, as well as trust in health information sources among 254 respondents. The researchers found that those with lower language proficiency, income and education were more likely to seek health information in Korean ethnic magazines and newspapers, while those with higher English proficiency and income were more likely to seek information online. Similarly, a 2013 cross-sectional study by [Oh et al](#okj11). explored the influence of immigration status on Korean Americans' trust in health information sources and health information-seeking behaviour compared to native Koreans. Her results showed that Korean immigrants living in the USA were more likely to trust health information from newspapers or magazines than native Koreans.

The aforementioned studies centred on a common seeking behaviour among Korean Americans, focusing on questioning activities cancer-related information-seeking in general. Questions are often conceptualised as a representation of health information needs to fill gaps in a state of knowledge ([Belkin, Oddy and Brooks, 1982](#bob82); [Dervin, 1983](#d83)). To this end, an important benefit of using the Internet for health-related information lies in the ability to interact with many others who share same or similar interests and to pose questions with minimal risk or consequences. Although it is important to identify whether seekers received replies which adequately addressed the area they wanted to understand, no studies identified relationships between questions (information needs) and replies (shared information). The research milieu has not addressed the link between online health-related questions and answers, specifically as it pertains to Korean Americans. An important concern of cancer information-seeking is unmet needs as a result of inconsistencies between desired and obtained information (queries and replies). This disparity may lead to negative outcomes, such as increased levels of anxiety ([Garvin _et al._, 2003](#gmr03)). In online settings, answers are viewed as a form of information sharing behaviour ([Adamic, Zhang, Bakshy and Ackerman, 2008](#azb08); [Wang, Wang, Abrahams and Fan, 2015](#wwa15)). In this respect, the current study encompasses topics in shared information, which are manifested as answers in online forums. Furthermore, cancer information-seeking behaviour is a much more nuanced and multidimensional behaviour ([Germeni, Bianchi, Valcarenghi and Schulz, 2015](#gbv15)), where one factor impacts another.

Most studies have examined health-related information behaviour using quantitative methods, such as a survey, or a qualitative method, such as focus groups or interviews. None of the examined studies used an in-depth analysis method, such as semantic network analysis, that enables one to understand the relationships among the major needs and shared information. Nor have previous studies used the semantic network analysis technique, a relatively new relational content analysis method, which is promoted as a dynamic methodological approach that enables researchers to capture the pattern of major topics in messages, including questions and replies in online forums. To fill the gaps, this study focuses on understanding the patterns and relationships between Korean Americans' health information-seeking and sharing in the social media setting, focusing on breast cancer.

## Method

### Research questions

With the purpose of identifying Korean Americans' main information needs and information they share online as well as their behavioural patterns, the following research questions guided the current study:

*   RQ1: What topics are shared among Korean Americans who seek and share breast cancer information through online forum questions and correlative replies?
*   RQ2: What are the most prominent topics among Korean Americans who seek and share breast cancer information through online forum questions and correlative replies?
*   RQ3: What are the semantic associations between breast cancer questions and correlative replies?

### Data collection

Data were obtained from MissyUSA (missyusa.com), one of the largest online communities for Korean immigrants living in the United States of America. Comprised of nearly 320,000 members, this Website has multiple forum sections that allow users to post messages and replies on a variety of topics, including health.

Among posted messages, a total of 1,394 replies associated with 246 questions related to breast cancer posted in 2013 and 2014 were collected. Some 1,199 replies (86.01%) associated with 239 questions (97.15%) remained after deleting irrelevant posts. For example, 7 advertisements (2.85%) posted in questions were excluded from analysis. Another 195 (13.99%) reply messages were also excluded because they were non-health related replies (e.g., 'Thank you' and 'Replies here are funny'), sexual jokes, insulting remarks or advertisements.

### Data analysis

The collected dataset was analysed first using content analysis to answer research question one; then semantic network analysis was used to trace the semantic structures of the identified breast cancer related topics for research questions two and three.

First, topics were identified through content analysis by two human coders who manually analysed the collected questions and associated replies using the coding scheme adapted by Oh, Zhang and Park ( [2013](#ozp13)). The coding system was modified through the manual coding process in order to reflect the uniqueness of information sharing regarding breast-cancer among Korean Americans. After 10% (25 questions, 119 replies) of the randomly selected coding results were collected, inter-coder reliability was computed, employing Cohen's Kappa (?) ([Cohen, 1960](#c60)). The computed results in questions and replies were 0.83 and 0.77, respectively, which indicate 'substantial agreement' to 'almost perfect agreement' between the two coders ([Landis and Koch, 1977](#lk77)). The identified topics were recorded in two-mode matrices for both questions and replies (i.e., questions by topics and replies by topics) for further semantic network analyses.

Secondly, semantic network analysis was conducted to examine structural features in terms of behavioural tendencies of Korean Americans who seek and share breast cancer information. Semantic network analysis is a set of techniques with which to analyse and interpret associations within communication content and often treats textual entities (e.g., words) as proxies for cognition ([Coronges _et al._, 2007](#csv07); [Doerfel, 1998](#d98); [Doerfel and Barnet, 1999](#db99)). Semantic network analysis employs network models that allow us to capture the relational structures based on the shared and connected meanings of textual data by illustrating relatedness or associations between pairs of entities ([Coronges _et al._, 2007](#csv07); [Doerfel and Barnett, 1999](#db99); [Doerfel, 1998](#d98)). Associations between textual entities are thought to reflect structural features of people's concepts and are useful for understanding their propensities for certain modes of behaviour, assessing their information processing and discerning their cognitive features ([Coronges _et al._, 2007](#csv07)).

In this study, semantic network analysis includes a) centrality measures of degree, closeness and betweenness and b) the similarity measures. Together, these measures were used to capture the relational properties of the topical networks found in questions and correlative replies and then create a representation through visualization. The centrality measures are the indices of signification in a network ([Henderson, Iacobucci and Calder, 1998](#hic98); [Iacobucci, Henderson, Marcati and Chang, 1996](#ihm96); [Nelson and McEvoy, 2004](#nm04)). In this case, they reveal the most salient cognitive activities that greatly impact information behaviour. The centrality measures quantify not only the volume or strength of links flowing from and to each concept ([Borgatti, 1995](#b95); [Everett and Borgatti, 2005](#eb05); [Grebitus and Bruhn, 2008](#gb08); [Iacobucci _et al._, 1996](#ihm96)), but also the integration with other concepts. This is accomplished through links to neighbouring concepts identified through the process of activating or becoming activated via ongoing communication ([Lim, Berry and Lee, 2015](#lbl15)). In the current study, the centrality measurement indices allow us to examine the extent to which a breast-cancer-related topic impacts upon other health topics. Three different centrality indices were used: degree, closeness and betweenness. The degree centrality (C<sub>D</sub>) is the number of ties that a given a node has ([Borgatti, 2005](#b05); [Hanneman and Riddle, 2005](#hr05)). The closeness centrality (C<sub>C</sub>) calculates the total distance from a given concept to all other concepts in a network, where the distance indicates the length of the shortest path from one to the other ([Borgatti, 2005](#b05); [Hanneman and Riddle, 2005](#hr05)). A high C<sub>C</sub> indicates a greater distance or independence from the flow of activations from other topics, whereas a low C<sub>C</sub> value means close reliance on others to spread activation ([Grebitus and Bruhn, 2008](#gb08)). Betweenness centrality (C<sub>B</sub>) calculates the share of times which an actor (topic) falls on the shortest paths between other pairs of actors in a network ([Borgatti, 2005](#b05); [Hanneman and Riddle, 2005](#hr05); [Kadushin, 2012](#k12)). C<sub>B</sub> is an indicator of control within a network. Higher betweenness centrality pinpoints higher probability of becoming activated or activating other concepts ([Grebitus and Bruhn, 2008, p. 90](#gb08)).

The similarities indices for each pair of topics were also measured in order to understand associations among the topics. Similarity measures refer to the degree to which actors have the same type of ties with others, representing patterns of ties between actors in a network ([Michelson and Contractor, 1992](#mc92)). Semantic associations of topics or concepts are thought to measure the proximity of one idea to others involved in behavioural tendencies ([Coronges _et al._, 2007](#csv07)) and thought processes ([Darkes and Goldman, 1998](#dg98)). The associative relationships in a pair of concepts can be measured by assessing co-occurrences ([Darkes and Goldman, 1998](#dg98)), which depict the similarities in perceptions shared within a group of people ([Coronges _et al._, 2007](#csv07)).

The analyses were conducted using [UCINET version 6.606](https://sites.google.com/site/ucinetsoftware/downloads), a network analysis program ([Borgatti, Everett and Freeman, 2002](#bef02)). The 2-mode matrices for questions and for replies (i.e., question-by-topic and reply-by-topic) developed through content analysis were converted into 1-mode (i.e., topic-by-topic for questions and topic-by-topic for replies) matrices. By doing so, the cell in a matrix would tell us the number of topics in which either two questions or two replies occur. That is, it demonstrates how strongly two topics are associated with one another.

## Results

### Breast cancer topics in questions and associated replies

A total of fourteen topics was identified within two major categories, namely: a) seven topics regarding disease-specific information, _prevention and cause, symptom, breast cancer test_ (includes any type of breast cancer test. e.g., diagnostic test, screening test and monitoring test), _treatment, diagnosis, prognosis, medical history_; and b) seven major topics regarding non-disease specific information, which are _insurance, financial concerns, language barrier, diet or exercises, clinical service, emotional support_ and _social support_. Note that questions often contain more than one sub-question (e.g., '_Where do you think would be better for breast cancer surgery, Korea or the USA? And please, recommend me hospitals and doctors_') or include multiple pieces of information that may help readers to understand their situation (e.g., symptoms they felt, clinical experiences in the past). Regarding the 246 collected questions, users provided multiple answers. The average number of answers per question was 5.6\. Like questions, more than one topic was also often covered in one reply. In such cases, the questions and answers were coded into more than one category. Therefore, a total of 663 pieces of information was identified in the collected questions and 2,163 pieces of information were identified in the collected replies. An overview of the identified topics in questions and replies are described in Table 1.

When posting breast cancer related questions, it appears that participants most frequently sought information about _breast cancer tests_(n=145, 21.87%), followed by _treatment_ (n=75, 11.31%), _symptoms_ (n=70, 10.56%) and _emotional support_ (n=69, 10.41%). The least sought information was _prevention and cause_ (n=5, 0.75%) and _social support_ (n=5, 0.75%). Regarding the replies, information was provided most frequently about treatment (n=373, 17.24%), followed by _breast cancer tests_ (n=331, 15.3%), _symptoms_ (n=253, 11.7%) and _clinical service_ (n=195, 9.02%). The least shared information was about the _language barrier_ (n=13, 0.6%).

<table class="center" style="width:95%;"><caption>  
Table 1\. The distribution of questions and replies regarding breast cancer</caption>

<tbody>

<tr>

<th colspan="3">Questions</th>

<th colspan="3">Replies</th>

</tr>

<tr>

<th>Topics</th>

<th>Frequency</th>

<th>per centage</th>

<th>Topics</th>

<th>Frequency</th>

<th>per centage</th>

</tr>

<tr>

<td>Breast cancer test</td>

<td style="text-align:center">145</td>

<td style="text-align:center">21.87%</td>

<td>Treatment</td>

<td style="text-align:center">373</td>

<td style="text-align:center">17.24%</td>

</tr>

<tr>

<td>Treatment</td>

<td style="text-align:center">75</td>

<td style="text-align:center">11.31%</td>

<td>Breast cancer test</td>

<td style="text-align:center">331</td>

<td style="text-align:center">15.30%</td>

</tr>

<tr>

<td>Symptom</td>

<td style="text-align:center">70</td>

<td style="text-align:center">10.56%</td>

<td>Symptom</td>

<td style="text-align:center">253</td>

<td style="text-align:center">11.70%</td>

</tr>

<tr>

<td>Emotional support</td>

<td style="text-align:center">69</td>

<td style="text-align:center">10.41%</td>

<td>Clinical service</td>

<td style="text-align:center">195</td>

<td style="text-align:center">9.02%</td>

</tr>

<tr>

<td>Diagnosis</td>

<td style="text-align:center">64</td>

<td style="text-align:center">9.65%</td>

<td>Diagnosis</td>

<td style="text-align:center">189</td>

<td style="text-align:center">8.74%</td>

</tr>

<tr>

<td>Clinical service</td>

<td style="text-align:center">64</td>

<td style="text-align:center">9.65%</td>

<td>Dietary or exercise</td>

<td style="text-align:center">179</td>

<td style="text-align:center">8.28%</td>

</tr>

<tr>

<td>Insurance</td>

<td style="text-align:center">51</td>

<td style="text-align:center">7.69%</td>

<td>Financial concern</td>

<td style="text-align:center">126</td>

<td style="text-align:center">5.83%</td>

</tr>

<tr>

<td>Financial concern</td>

<td style="text-align:center">40</td>

<td style="text-align:center">6.03%</td>

<td>Prognosis</td>

<td style="text-align:center">123</td>

<td style="text-align:center">5.69%</td>

</tr>

<tr>

<td>Prognosis</td>

<td style="text-align:center">29</td>

<td style="text-align:center">4.37%</td>

<td>Emotional support</td>

<td style="text-align:center">101</td>

<td style="text-align:center">4.67%</td>

</tr>

<tr>

<td>Dietary or exercise</td>

<td style="text-align:center">18</td>

<td style="text-align:center">2.71%</td>

<td>Insurance</td>

<td style="text-align:center">85</td>

<td style="text-align:center">3.93%</td>

</tr>

<tr>

<td>Language barrier</td>

<td style="text-align:center">16</td>

<td style="text-align:center">2.41%</td>

<td>Prevention and cause</td>

<td style="text-align:center">33</td>

<td style="text-align:center">1.53%</td>

</tr>

<tr>

<td>Medical history</td>

<td style="text-align:center">12</td>

<td style="text-align:center">1.81%</td>

<td>Social support</td>

<td style="text-align:center">20</td>

<td style="text-align:center">0.92%</td>

</tr>

<tr>

<td>Prevention and cause</td>

<td style="text-align:center">5</td>

<td style="text-align:center">0.75%</td>

<td>Medical history</td>

<td style="text-align:center">21</td>

<td style="text-align:center">0.97%</td>

</tr>

<tr>

<td>Social support</td>

<td style="text-align:center">5</td>

<td style="text-align:center">0.75%</td>

<td>Language barrier</td>

<td style="text-align:center">13</td>

<td style="text-align:center">0.60%</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0.00%</td>

<td>Others</td>

<td style="text-align:center">121</td>

<td style="text-align:center">5.59%</td>

</tr>

<tr style="background-color: #C6C5C4;">

<td>Total</td>

<td style="text-align:center">663</td>

<td style="text-align:center">100.00%</td>

<td>Total</td>

<td style="text-align:center">2,163</td>

<td style="text-align:center">100.00%</td>

</tr>

</tbody>

</table>

*   When seeking information about _breast cancer test_, participants were slightly more interested in diagnostic tests (64 out of 145, 44.1%) over screening tests (50 out of 145, 34.5%) or monitoring tests (31 out of 145, 21.4%). It seems that an examination (unspecified) is considered only after an abnormality (e.g., calcification, dense breast or lump) was noticed.
*   Regarding _treatment_, experiences, suggestions and opinions were sought mainly about treatment methods, medicines (e.g., Tamoxifen, chemotherapy) and their effects and side-effects (e.g., cause of uterine cancer). They sought information about treatment methods (e.g., mastectomy, radiation treatment, chemotherapy) in order to figure out which method would have the best results for them. Surgical treatments were often considered and questions about choices between unilateral and bilateral mastectomy were often asked.
*   _Symptoms_ were often described in questions to enable others to understand their concerns and seek others' experiences and opinions about whether their symptoms would be related to breast cancer. The symptoms were often described as pain in a breast, lump in a breast, nausea and abnormally long periods.

The two least frequently asked topics in questions are _prevention/cause_ and _social support_. Only the top three most frequently discussed topics and two least sought or shared topics are presented here. Summaries for other topics are available [upon request](mailto:mp11j@my.fsu.edu).

*   For the _prevention/cause_ topic, participants were often concerned about the possibility of breast cancer in relationship with other medical treatment: for example, about whether therapies or medical procedures (e.g., in-vitro fertilization attempts, plastic surgery, breast massage) or cancer treatment (e.g., X-rays) might increase the chances of breast cancer.
*   Regarding _social support_ systems, social welfare services for low-income single parent households or support groups for breast cancer patients were sought.
*   Regarding the topic of _treatment_, experiences were often shared, for example, on simple or total mastectomy, radiation therapy, chemotherapy, anti-cancer treatments and hormone therapy. Surgical treatment was the most often described procedure (162 out of 373, 43.4%), followed by radiotherapy (82 out of 373, 22%), anti-cancer treatments (76 out of 373, 20.4%) and chemotherapy (48 out of 373, 12.9%). Often the descriptions involved a combination of multiple processes of surgery, chemotherapy, radiotherapy and medicine. Medicines (e.g., antibiotics, Tamoxifen, Arimidex and pain killers) taken after or for treatments and their side effects (e.g., Tamoxifen causes absence of periods and may cause uterine cancer) were also described.
*   When information about breast cancer related tests was provided, repliers often described information based on their direct or indirect experience with the following: a) types of tests (e.g., mammograms, ultrasounds, biopsies, pap smears, MRIs, BRACA testing, Oncotype DX testing, gene tests, blood testing, PET scans, self-testing); and b) types of testing that would be more effective for breast cancer detection (e.g., some insist ultrasounds would be more effective in detecting breast cancer whereas others argue biopsies would have more exact diagnosis results); and c) procedures, appropriate time spans for regular check-ups (e.g., six months, one year, three years, etc.).
*   Repliers discuss _symptoms_ they had when suspecting breast cancer and side effects of treatment, specifically: a) whether breast cancer shows symptoms or not. When they conferred about breast cancer symptoms, they often described pain in a breast, fatigue, mucus discharge, bleeding, and shape (rough or smooth), texture (hard or soft) and the size of a clot or lump in a breast and b) symptoms that were suspected side-effects of breast cancer treatment were also discussed (e.g., loss of hair, insomnia, numbness, oedema in an arm, absence of periods, skin reactions (hardened scars /keloid scarring), weight loss, nausea).

The two topics information was least frequently provided about were the _language barrier_ and _social support_. There are some notable replies.

*   Direct and indirect language issues shared were: a) translation of a specific expression into English (e.g., '_You can say you have a shooting pain in English_') for askers who requested help communicating with clinicians; and b) translation suggestions (one patient who had visited a hospital in Korea was advised to bring the translated documents when returning to the USA whereas another replier suggested bringing a copy of initial test results without translation). This type of information appeared to be provided for those who had or were considering having clinical services in Korea.
*   Regarding social support systems, both online and offline social support groups' information were provided or suggested, but the majority of information (19 out of 20, 95%) was about Korean communities (e.g., online cafes, blogs, online counselling) for Korean breast cancer patients operated by reputable doctors or hospitals in Korea rather than those in the USA. Some offered help resources (e.g., driving) in person.

Other concerns are small in comparison, but include factors that may influence breast cancer patients:

*   Repliers shared direct or indirect experiences with immigration and explained that having illegal status would not affect the reception of medical services for breast cancer patients.
*   Repliers provided advice regarding daily activities after or during cancer treatment. For example, a) patients can drive during a course of chemotherapy but not the day of surgery; and b) do not do house chores at least for a few days, and probably not for a couple of weeks after surgery.
*   Tips for appearance issues after surgery were provided. Examples are to wear scarves rather than wigs, and pros and cons for breast reconstruction surgery. According to users, a benefit of this procedure is that it helps to recover self-esteem as a female, while a drawback is that it is an unnecessary process that causes more pain and risk. In addition, breast reconstructive surgery can possibly interfere with breast cancer recurrence detection.
*   Age (users' often mentioned forty or fifty years old as benchmarks for breast cancer tests) and income were also raised as influencing factors when the participants considered taking breast cancer tests, particularly by free clinical services.

### Relative salience of breast cancer topics

The topics identified here are often interrelated with other topics. This section describes the relative importance of the topics for participants by examining relevance structures of topics in questions and replies. Breast cancer specific topics, which are breast cancer tests, treatment, symptoms and emotional support, were identified as the four most prominent topics in the flow of information both in questions and in replies, although the ranks slightly vary. Thus, with regard to information-seeking and sharing, the high scores for these topics confirm their role as a key stimulus. In contrast, it appeared that _prevention and cause_ tends to be more peripheral in the network with weaker connections, farther location and lower control power both in questions and in replies. Table 2 shows the centrality measures of topics identified in questions and in replies, which indicate the relative importance of topics within the semantic network.

#### Relevance structure of topics in questions

The topic breast cancer test appeared to have the largest number of direct connections with other topics with the highest degree centrality index of 15\. This was followed by three topics, which were _symptom, treatment_ and _emotional support_ (C<sub>D</sub>=14). This indicates that the topic _breast cancer test_ was relatively salient and strongly impacts the flow of information, serving as a major channel to information-seeking when compared with other topics. Thus the probability that these topics with high C<sub>D</sub> activate the network is higher than those of other topics in the network ([Freeman, 1979](#f79); [Collins and Liftus, 1975](#cl75)). By contrast, _prevention/cause_ has the lowest C<sub>D</sub> of 3, indicating this topic is more peripheral to the network based on the fact that there are fewer connections to other topics. Moreover, non-medical related topics (C<sub>D</sub>=0) did not have any direct connections with other topics in questions.

<table class="center" style="font-size:8pt; width:100%;"><caption>  
Table 2: Centrality measures for the network of questions</caption>

<tbody>

<tr>

<th colspan="4">Questions</th>

<th colspan="4">Replies</th>

</tr>

<tr>

<th>Topics</th>

<th>Degree(C<sub>D</sub>)</th>

<th>Closeness(C<sub>C</sub>)</th>

<th>Between-ness(C<sub>B</sub>)</th>

<th>Topics</th>

<th>Degree(C<sub>D</sub>)</th>

<th>Closeness(C<sub>C</sub>)</th>

<th>Between-ness(C<sub>B</sub>)</th>

</tr>

<tr>

<td>Breast cancer test</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">17.000</td>

<td style="text-align:center">9.274</td>

<td>Symptom</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Symptom</td>

<td style="text-align:center">14.000</td>

<td style="text-align:center">18.000</td>

<td style="text-align:center">1.774</td>

<td>Breast cancer test</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Treatment</td>

<td style="text-align:center">14.000</td>

<td style="text-align:center">18.000</td>

<td style="text-align:center">1.774</td>

<td>Treatment</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Emotional support</td>

<td style="text-align:center">14.000</td>

<td style="text-align:center">18.000</td>

<td style="text-align:center">1.774</td>

<td>Emotional support</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Prognosis</td>

<td style="text-align:center">13.000</td>

<td style="text-align:center">19.000</td>

<td style="text-align:center">1.242</td>

<td>Diagnosis</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Diagnosis</td>

<td style="text-align:center">13.000</td>

<td style="text-align:center">19.000</td>

<td style="text-align:center">0.812</td>

<td>Social support</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Clinical service</td>

<td style="text-align:center">13.000</td>

<td style="text-align:center">19.000</td>

<td style="text-align:center">0.812</td>

<td>Others(non-medical)</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Medical history</td>

<td style="text-align:center">12.000</td>

<td style="text-align:center">20.000</td>

<td style="text-align:center">5.317</td>

<td>Insurance</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">0.400</td>

</tr>

<tr>

<td>Insurance</td>

<td style="text-align:center">12.000</td>

<td style="text-align:center">20.000</td>

<td style="text-align:center">0.984</td>

<td>Financial concern</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">0.400</td>

</tr>

<tr>

<td>Financial concern</td>

<td style="text-align:center">12.000</td>

<td style="text-align:center">20.000</td>

<td style="text-align:center">0.476</td>

<td>Clinical service</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">0.400</td>

</tr>

<tr>

<td>Language barrier</td>

<td style="text-align:center">12.000</td>

<td style="text-align:center">20.000</td>

<td style="text-align:center">0.447</td>

<td>Prognosis</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">0.273</td>

</tr>

<tr>

<td>Social support</td>

<td style="text-align:center">10.000</td>

<td style="text-align:center">22.000</td>

<td style="text-align:center">0.211</td>

<td>Medical history</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">0.273</td>

</tr>

<tr>

<td>Dietary/exercise</td>

<td style="text-align:center">10.000</td>

<td style="text-align:center">22.000</td>

<td style="text-align:center">0.000</td>

<td>Others(medical-related)</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">0.273</td>

</tr>

<tr>

<td>Others(medical-related)</td>

<td style="text-align:center">8.000</td>

<td style="text-align:center">24.000</td>

<td style="text-align:center">0.100</td>

<td>Dietary/exercise</td>

<td style="text-align:center">15.000</td>

<td style="text-align:center">16.000</td>

<td style="text-align:center">0.273</td>

</tr>

<tr>

<td>Cause/prevention</td>

<td style="text-align:center">3.000</td>

<td style="text-align:center">29.000</td>

<td style="text-align:center">0.000</td>

<td>Cause/prevention</td>

<td style="text-align:center">12.000</td>

<td style="text-align:center">19.000</td>

<td style="text-align:center">0.000</td>

</tr>

<tr>

<td>Others (non-medical)</td>

<td style="text-align:center">0.000</td>

<td style="text-align:center">45.000</td>

<td style="text-align:center">0.000</td>

<td>Language barrier</td>

<td style="text-align:center">11.000</td>

<td style="text-align:center">20.000</td>

<td style="text-align:center">0.000</td>

</tr>

</tbody>

</table>

_Breast cancer test_ also had the biggest closeness centrality index, indicating this topic was located the shortest distance from other topics in questions. In other words, information-seeking originating from the topic of _breast cancer test_ would flow through the rest of the topics in breast cancer questions. Similar to degree centrality, this was followed by three other topics (_symptom, treatment_ and _emotional support_). By contrast, _prevention/cause_ (C<sub>C</sub>=29) and _non-medical related others_ (C<sub>C</sub>=45) had the largest scores, indicating these topics were the two farthest and independent topics. The _non-medical related others_ topics was particularly far and independent from the control potential of other topics in the network.

_Breast cancer test_ had the highest betweenness centrality index of 9.274\. This result means that slightly over nine associations occurred throughout the topic _breast cancer test_ in the network. This was followed by _medical history_ (C<sub>B</sub>=5.317) and three other topics, _treatments, symptoms_ and _emotional support_, but the index values were decreased to 1.774\. Although the degree and closeness centralities of _medical history_ were not as significant as those of _treatments, symptoms_ and _emotional support_, the topic _medical history_ had higher betweenness centrality, indicating this topic has stronger mediative power than the latter three topics. By contrast, three topics, _dietary and exercise, cause and prevention_ and _non-medical others_, appeared to have no mediative power to other topics in questions.

Figure 1 shows the semantic networks of breast cancer related topics that emerged in Korean Americans' questions. This visual representation demonstrates the overall patterns and holistic properties of the topic networks, bringing the measures of degree centralities and co-occurrence provided in Tables 2 and 3 to life. Each node represents the relative size based on the degree centrality of individual topics and so does the line thickness between the two topic nodes. In addition, two nodes located near each other correspond roughly to the length of the geodesic distance between them. To illustrate, _breast cancer test_ has the largest and thickest lines when compared with other topics including _symptoms, treatment, emotional support, insurance_ and _financial concerns_. These relationships demonstrate strong associations among them and their impact on one another in the network. In contrast, _prevention/cause_ is located in the outskirts of the graph with weak connections with other topics such as _breast cancer test_ and _medical history_.

<figure class="centre">![Figure 1: Structure of breast cancer topics in questions](p729fig1.png)

<figcaption>  
Figure 1\. Structure of breast cancer topics in questions.</figcaption>

</figure>

#### Relevance structure of topics in replies

In replies or provided information, four medical related topics, _symptom, breast cancer test, treatment, diagnosis_, and three non-medical related topics, _emotional support, social support, others_, appeared to be prominent across all three centrality measures. Accordingly, these topics had the strongest impact within the network in that they had the largest number of topical relationships or were proximal to other topics, thus operating as a key channel for information sharing. In contrast, _language barriers_has the lowest degree centrality index of the eleven topics, which infers that the probability for the topic to play a role as the key channel of information sharing is the lowest in the network.

The closeness centrality index also indicates that the seven topics with the highest degree of centrality serve as the foci of other topics. Stated differently, they play pivotal roles as key stimuli in Korean Americans provided information or replies regarding breast cancer. _Language barrier_ (C<sub>C</sub>=20) had the lowest closeness centrality index, denoting that this topic tends to locate far from other topics and, therefore, can be regarded as independent from other topics in the network which activate communication among Korean Americans regarding breast cancer.

For the betweenness centrality, the seven topics with the highest degree centrality also had the highest index score of 0.816\. This result indicates that the probability that these seven topics would be actively involved in the flow of information in replies is relatively higher than the other topics. However, when compared to indices of betweenness centrality of the first four topics in questions, _symptom, breast cancer test, treatment_ and _emotional support_, to those for the same topics in replies, the scores decreased, meaning that the mediative power of the four topics were reduced when Korean Americans share information. In contrast, both _prevention/cause_ and _language barriers_ appeared to have no mediative power (C<sub>B</sub>=0), indicating the probability for these two topics functioning as active paths between other pairs of topics is none. Thus, it is less likely for these topics to be communicated throughout other topics in breast cancer replies unless stimulated separately.

Figure 2 represents the network of breast cancer topics that emerged in Korean Americans' replies. Like Figure 1, the visual representation below depicts the measures of degree centralities and similarities provided in Tables 2 and 3 demonstrating the overall patterns and holistic properties of the topic networks. Some topics such as _breast cancer test, symptom, treatment_ and _emotional support_ share a large number and the thickest of lines and are situated closely to each other. The relationships among them demonstrate that when Korean Americans share information these topics tend to occur together with other correlative topics which have strong impact on other topics. In contrast, _prevention/cause_ and _language barrier_ are located on the outskirts of the graph, thus representing weak connections with other topics such as _insurance, clinical service_ and _medical history_. These outlier topics appeared to be rather independent from other topics. It can therefore be said that they have low impact when Korean Americans share information about breast cancer.

<figure class="centre">![Figure2: Structure of breast cancer topics in replies](p729fig2.png)

<figcaption>  
Figure 2: Structure of breast cancer topics in replies.</figcaption>

</figure>

### Associations among breast cancer topics

The similarity coefficients were measured to determine the semantic associations of the breast cancer topics in questions and in replies by measuring the co-occurrence ties among topics, which indicate cognitive associations among topics. Table 3 shows the correlation coefficients between topics. We used a coefficient greater than 0.71 as a benchmark to verify the strong associations between topics.

#### Association structure in questions

When binary relationships between topics in questions were examined, two pairs of topics, _financial concern_ and _insurance_ (r=0.978), and _financial concern-clinical service_ (r=0.978) showed the strongest relationships (see Table 3). These results affirm the content analysis results which indicated the forum participants tended to consider their insured or uninsured status in relation to accessing clinical services for breast cancer tests or treatments. Essentially, they are often concerned about costs for medical services. Figure 1 shows the relatively stronger and closer relationships among the topics of _insurance, financial concern_ and _clinical service_. Other pairs of topics which have coefficients greater than 0.90 are the following: _language barrier-symptom_ (r=0.962), _clinical service-insurance_ (r=0.958), _emotional support-symptom_ (r=0.951), _breast cancer test-symptom_ (r=0.948), _clinical service-symptom_ (r=0.926), _emotional support-medical history_ (r=0.905). These strong associations between topics implied that participants tend to be concerned about language barriers when they suspect symptoms to be breast cancer. In addition, having or not having insurance influences their access to clinical services. Finally, when suspicious of symptoms, participants are interested in having a breast cancer test and receiving emotional support from others.

In contrast, _prevention and cause - breast cancer test_ (r=-0.199) has the weakest association, followed by _diet or exercise-prevention and cause_ (r=-0.145), _financial concern-diet or exercise_ (r=0.085), and _prevention and cause-treatment_ (r=0.097) (see Table 3). When considering that there is a weak relation between _prevention and cause - breast cancer test_ whereas _breast cancer test-symptom_ have a strong relation, it also affirms the content analysis results that showed participants tended not to pay attention to tests for prevention of breast cancer, but rather to seek a breast cancer test for diagnosis only after they have symptoms. Figure 1 shows that _prevention and cause_ does not have many connections with other topics and is situated on the fringe of the network graph. Also, there is a weak relationship between _diet or exercise-prevention and cause_ whereas _diet or exercise-treatment_ has a strong relation (r=0.774). This result implies that participants showed stronger interest in healthy diet and regular exercise only after being diagnosed with breast cancer.

#### Association structure in replies

The topic pair of _financial concern_ and _insurance_ (r=0.979) shows the strongest relationship, followed by _emotional support-diagnosis_ (r=0.966), _emotional support-prognosis_(r=0.956), _clinical service-insurance_, (r=0.949), and _financial concern-clinical service_ (r=0.913). These patterns demonstrate that the topics of _insurance, financial concern_ and _clinical service_ often co-occurred in replies and were discussed as co-related issues as they were in questions. Figure 2 verifies the closer relationships of these three topics. However, unlike in the questions, participants tend to provide emotional support to others regarding diagnosis and prognosis issues in their discussions. Other pairs of topics that show greater than 0.90 coefficients include _medical history-symptom_ (r=0.953), _diet or exercise-treatment_ (r=0.945), _diagnosis-symptom_ (r=0.944), and _diet or exercise-prognosis_ (r=0.933). An interpretation of these relationships can be that participants tend to consider their medical history when they suspect breast cancer symptoms, and that diet and exercise may aid their treatment and prognosis of the disease as the findings in content analysis revealed in the first phase of this study.

In contrast, _prevention and cause-insurance_ (-0.246) has the weakest association among relationships between topics, followed by _treatment-insurance_ (=-0.030), _language barrier-diet or exercise_ (r=-0.026), and _insurance-diet or exercise_ (r=-0.002). The weak relationships between prevention and cause factors and insurance issues again confirm that Korean Americans take their insured status into account, but not for prevention.

<table class="center" style="width:80%; font-size:8pt;"><caption>  
Table 3: Similarities matrix in breast cancer questions and replies</caption>

<tbody>

<tr>

<th></th>

<th>Prevention and cause</th>

<th>Symptom</th>

<th>Breast cancer test</th>

<th>Treatment</th>

<th>Diagnosis</th>

<th>Prognosis</th>

<th>Medical history</th>

<th>Insurance</th>

<th>Financial concern</th>

<th>Language barrier</th>

<th>Dietary and exercise</th>

<th>Clinical service</th>

<th>Emotional support</th>

</tr>

<tr>

<td>Symptom</td>

<td style="text-align:center">0.492 **(0.798)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Breast cancer test</td>

<td style="text-align:center">-0.199 (0.459)</td>

<td style="text-align:center">**0.948 (0.769)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Treatment</td>

<td style="text-align:center">0.097 **(0.834)**</td>

<td style="text-align:center">**0.701 (0.798)**</td>

<td style="text-align:center">0.514 **(0.784)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Diagnosis</td>

<td style="text-align:center">0.239 **(0.823)**</td>

<td style="text-align:center">**0.851 (0.944)**</td>

<td style="text-align:center">**0.775 (0.786)**</td>

<td style="text-align:center">**0.772 (0.899)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Prognosis</td>

<td style="text-align:center">0.115 **(0.866)**</td>

<td style="text-align:center">0.529 **(0.888)**</td>

<td style="text-align:center">0.379 (0.639)</td>

<td style="text-align:center">**0.815 (0.906)**</td>

<td style="text-align:center">**0.825 (0.917)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Medical history</td>

<td style="text-align:center">**0.806 (0.762)**</td>

<td style="text-align:center">**0.791 (0.953)**</td>

<td style="text-align:center">0.306 (0.635)</td>

<td style="text-align:center">0.464 **(0.707)**</td>

<td style="text-align:center">**0.756 (0.910)**</td>

<td style="text-align:center">0.658 **(0.819)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Insurance</td>

<td style="text-align:center">0.307 (-0.246)</td>

<td style="text-align:center">**0.864** (0.280)</td>

<td style="text-align:center">**0.786** (0.382)</td>

<td style="text-align:center">0.550 (-0.030)</td>

<td style="text-align:center">**0.713** (0.200)</td>

<td style="text-align:center">0.375 (0.050)</td>

<td style="text-align:center">0.547 (0.190)</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Financial concern</td>

<td style="text-align:center">0.333 (0.241)</td>

<td style="text-align:center">**0.844** (0.253)</td>

<td style="text-align:center">**0.783** (0.344)</td>

<td style="text-align:center">0.505 (0.029)</td>

<td style="text-align:center">**0.784** (0.177)</td>

<td style="text-align:center">0.371 (0.015)</td>

<td style="text-align:center">0.552 (0.215)</td>

<td style="text-align:center">**0.978 (0.979)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Language barrier</td>

<td style="text-align:center">0.582 (0.015)</td>

<td style="text-align:center">**0.962** (0.464)</td>

<td style="text-align:center">**0.861** (0.530)</td>

<td style="text-align:center">0.576 (0.222)</td>

<td style="text-align:center">**0.782** (0.316)</td>

<td style="text-align:center">0.445 (0.166)</td>

<td style="text-align:center">**0.781** (0.395)</td>

<td style="text-align:center">**0.804** (0.653)</td>

<td style="text-align:center">**0.818 (0.793)**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Dietary and exercise</td>

<td style="text-align:center">-0.145 **(0.821)**</td>

<td style="text-align:center">0.171 **(0.799)**</td>

<td style="text-align:center">0.157 (0.695)</td>

<td style="text-align:center">**0.774 (0.945)**</td>

<td style="text-align:center">0.558 **(0.888)**</td>

<td style="text-align:center">**0.870 (0.933)**</td>

<td style="text-align:center">0.340 **(0.779)**</td>

<td style="text-align:center">0.179 (-0.002)</td>

<td style="text-align:center">0.085 (0.067)</td>

<td style="text-align:center">0.099 (-0.026)</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Clinical service</td>

<td style="text-align:center">0.438 (0.036)</td>

<td style="text-align:center">**0.926** (0.554)</td>

<td style="text-align:center">**0.864** (0.404)</td>

<td style="text-align:center">0.549 (0.102)</td>

<td style="text-align:center">**0.809** (0.423)</td>

<td style="text-align:center">0.459 (0.249)</td>

<td style="text-align:center">0.677 (0.505)</td>

<td style="text-align:center">**0.958 (0.949)**</td>

<td style="text-align:center">**0.978 (0.913)**</td>

<td style="text-align:center">**0.887** (0.677)</td>

<td style="text-align:center">0.139 (0.173)</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Emotional support</td>

<td style="text-align:center">0.489 **(0.869)**</td>

<td style="text-align:center">**0.951 (0.925)**</td>

<td style="text-align:center">**0.721** (0.686)</td>

<td style="text-align:center">0.685 **(0.928)**</td>

<td style="text-align:center">**0.877 (0.966)**</td>

<td style="text-align:center">0.653 **(0.956)**</td>

<td style="text-align:center">**0.905 (0.878)**</td>

<td style="text-align:center">**0.772** (0.134)</td>

<td style="text-align:center">**0.767** (0.127)</td>

<td style="text-align:center">**0.866** (0.315)</td>

<td style="text-align:center">0.290 **(0.903)**</td>

<td style="text-align:center">**0.860** (0.333)</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Social support</td>

<td style="text-align:center">0.456 (0.578)</td>

<td style="text-align:center">**0.752 (0.761)**</td>

<td style="text-align:center">0.483 **(0.700)**</td>

<td style="text-align:center">0.568 **(0.742)**</td>

<td style="text-align:center">**0.724 (0.859)**</td>

<td style="text-align:center">0.645 **(0.726)**</td>

<td style="text-align:center">**0.727** (0.695)</td>

<td style="text-align:center">0.504 (0.460)</td>

<td style="text-align:center">0.417 (0.351)</td>

<td style="text-align:center">0.684 (0.257)</td>

<td style="text-align:center">0.276 **(0.767)**</td>

<td style="text-align:center">0.568 (0.572)</td>

<td style="text-align:center">**0.778 (0.701)**</td>

</tr>

<tr>

<td colspan="14">Note: The similarities measures were computed using the Pearson product-moment correlation coefficient using topic-by-topic (i.e., one-mode) matrices for questions and replies. Numbers in parenthesis represent the correlation coefficient for replies. A coefficient great than 0.71 was bolded.</td>

</tr>

</tbody>

</table>

<section>

## Discussion

This study took an exploratory approach to understand the patterns of Korean Americans' information behaviour by examining their topics of interest regarding breast cancer and the patterns of associations among the identified topics. Content and semantic network analyses were used to understand how the diverse topics regarding breast cancer interrelated to each other while also measuring the relative salience of individual topics. Analyses confirmed previous observations that the topics of breast cancer test, treatment and emotional support frequently appear in discussions and often act as the central topics of their breast cancer questions and replies in activating their information-seeking and sharing. This finding is in line with study results of previous literature ([Kim and Yoon, 2012](#ky12); [Kim, Shah, Namkoong, McTavish and Gustafson, 2013](#ksn13); [Shaw _et al._, 2008](#sdh08)). However, these breast cancer topics are not isolated but serve to activate other topics impacting on breast cancer information-seeking and sharing in totality.

Although the forum participants showed acute information needs regarding breast cancer tests, interestingly they are more likely to get tested for diagnosis rather than for prevention. Relatively low frequencies and weak associations with other breast cancer topics with the _prevention and cause_ topic verify that breast cancer tests were considered as methods for diagnoses or checking prognoses after treatment rather than prevention. Many studies confirm the findings of the present study, including previous research done by [Choi _et al._, 2010](#clp10); [Maxwell, Bastani and Warda, 1998](#mbw98); [Sadler _et al._, 2001](#srk01) and data from the [Center for Disease and Prevention (CDC) (2014a)](#cdc14a). Generally speaking, Asian Americans have low rates of mammography for breast cancer screening (66.6 % in 2013) compared to all other ethnic groups in the USA. Breast cancer test concerns are often influenced by many other social and cultural factors. The US Preventive Service Tasks Force recommends mammography every two years for women aged fifty to seventy-four years old and possibly for women aged forty to forty-nine years old, depending on doctor recommendation and medical history ([CDC, 2014b](#cdc14b)). Despite this recommendation Korean Americans showed low rates of screening and health service use as a result of sociocultural, language, economic and geographic barriers in accessing healthcare ([Wang and Kwak, 2015](#wk15); [Ahmad _et al._, 2004](#asv04); [Thomson and Hoffman, 2009](#th09); [Changrani _et al._, 2008](#clg08)). The findings of this study demonstrate strong relationships between questions about the topics of _insurance, financial concern, language barrier, clinical service_ and _breast cancer tests_. Therefore, it further pinpoints that there is a need to consider the impacts of social, economic and cultural factors to effectively support breast cancer information needs and clinical interventions. Interestingly, these socio-economic and cultural factors were not salient in Korean Americans' discussion showing relatively weak impact in initiating in their breast cancer information sharing and seeking network. Although it appeared the social, economic and cultural factors were not considered as serious topics, it became clear through critical and systematic analysis that these factors influence the likelihood of receiving breast cancer mammograms.

_Treatment_ was identified as another area of high interest or a central topic in breast cancer discussions, as seen in previous studies such as that of [Kim and Yoon (2012)](#ky12). In similar fashion, our study captured some features through the similarity analyses; diet and exercise were often considered as recovery methods after breast cancer treatment rather than as prevention factors. This finding confirms the [National Cancer Institute's (2015)](#nci15) assertion that these subjects are not highly aware that a healthy diet and regular exercise can be protective factors for breast cancer. A possible reason is that people seek information when they are concerned about potential health problems rather than prevention. The strong associations between treatment and the two topics of diagnosis and prognosis also support the assumption that the forum participants sought and shared information about treatment methods not for prevention, but after there is a threat of breast cancer diagnosis or prognosis.

_Symptoms_ also appeared to be a significant breast cancer topic: when examining the content of questions and replies, _symptom_ is a key starting point leading them to initiate a range of topics related to breast cancer. When they suspect they have cancer or detect an abnormality, they tend to raise questions about other related issues such as taking a test, decision-making about treatment methods and looking for proper clinical services. Strong relationships identified in similarity assessment and in figures between _symptom_ and other topics including _breast cancer test, treatment, diagnosis, prognosis, emotional support_ and _clinical service_ confirmed the findings.

_Emotional support_ also serves as a main topic for breast cancer-related information-seeking and sharing. Although _emotional support_ was often associated with medical issues such as _diagnoses, prognoses_ and _symptoms_. These results imply that _emotional support_ is one of the major communication paths for Korean Americans when others have concerns regarding breast cancer. Similarly, other study groups have demonstrated receipt of emotional support from online communities ([Malik and Coulson, 2010](#mc10); [Nambisan, 2011](#n11); [Taylor _et al._, 2011](#thc11)). The comments related to emotional support have the potential to directly influence the success of the treatment and the healing process ([Kim, Kaplowitz and Johnston, 2004](#kkj04)). Interestingly, our study shows that, when considering that comments related to emotional support were rather simple, short encouraging messages that accompanied other information, emotional support acted as a supplementary entry or as closing remarks in communication rather than offering profound psychological benefits to others.

Topics that were not salient in the networks but showed strong associations were _financial concern, insurance_ and _clinical service_. Strong similarities among the three topics indicate that _insurance_ often led to financial concerns and, as a result, insured/uninsured status acts as a barrier against access to clinical services as previous literature indicated ([Asian American Policy Review, 2013](#aap13); [Kao, 2010](#k10); [Shin _et al._, 2005](#ssk05)). When examining the content of discussions regarding the three topics, it was confirmed that an uninsured status or a coverage with insurance status often led to concerns about costs for clinical services. As aforementioned, these social-economic issues negatively impacted Korean Americans' likelihood of obtaining a breast cancer screening/diagnosis test. Frequent inquiries regarding free breast cancer tests also validated that financial concerns are a barrier for Korean Americans who seek clinical services when they suspect breast cancer. Consistent with our findings, [Kim and Yoon (2012)](#ky12) found that financial concerns were often asked and discussed among Korean Americans in online health forums. [Oh _et al._ (2015)](#ojz15) also indicated that recent immigrant Koreans are facing obstacles in accessing health services because of a lack of health insurance. In Oh _et al._'s ([2015](#ojz15)) study, uninsured Korean Americans heavily depend on online health information to seek information and advice about health care services.

Moreover, concepts which were rarely mentioned but were captured in content analysis are, specifically, immigration status and heavy reliance on the Korean community for social support. Immigration status caused Korean Americans to hesitate to access clinical services and led to concerns about costs; those who are illegal immigrants lack jobs that offer health insurance or government-sponsored insurance ([Kim and Keefe, 2010](#kk10)). For these individuals, being uninsured or illegal poses major barriers in accessing health care services in the USA. Consequently, they may seek information about less costly clinics or alternative medicine. Another unique concern was appearance after breast cancer treatment, particularly mastectomies. Korean Americans often consider having breast reconstruction surgery to improve their appearance.

_Social support_ was another peripheral topic, but it was found that the participants heavily incorporated it into their replies to Korean communities in the USA or often referred to online breast cancer support communities in Korea. Korean immigrants are faced with living under different cultural norms, social conditions and health care systems as indicated in earlier literature (e.g., [Caidi _et al._, 2010](#caq10); [Napier _et al._, 2014](#nab14); [Savolainen, 2008](#s08)). Social support is critical when dealing with health challenges as well as reduced loneliness. This was identified as a major dimension of online breast cancer related information-seeking and sharing ([Fogel, Albert, Schnabel, Ditkoff and Neugut, 2002](#fas02); [Pitt, 2004](#p04)). Consistent with our findings, a study about the pattern of messages among patients with breast cancer found that the most frequent interaction theme related to social support, such as building friendships with peers ([Wen _et al._, 2011](#wmk11)). Another study found that social exchanges were tightly connected with informational or emotional resources; social support is difficult to separate from these interactions ([Rubenstein, 2015](#r15)). For that reason, Korean Americans frequently discussed a topic related to social support along with other topics in the online communities.

Findings from the current study could be useful for health care providers to better understand the uniqueness of Korean Americans' needs and major concerns regarding breast cancer. This understanding would lead to evidence-based, patient-centred and effective communications between practitioners and this community in the USA. Also, findings from this study can help in establishing viable health education and public health promotion strategies to provide better breast cancer information to the community.

This study also has methodological implications, namely, semantic network analysis could be an effective method to unobtrusively observe online health information behaviour made in a natural setting, taking the influence of situational factors into account. [Marton and Choo (2012)](#mc12) identified naturalistic observation on Web applications, such as online communities, was one of the frequently used methods to understand information behaviour online. Semantic network analysis is a relatively new observation methodology, which few studies in information-seeking had employed. This method, however, is well-proven and has been widely used in other fields, such as communication and public relations. In this study, the method allowed us to systematically observe the relationships and associations of diverse topics found in breast cancer-related information behaviour online. This approach could be expanded to information behaviour in different settings and different contexts.

### Limitations

This study has a few limitations. Although MissyUSA is one of the most popular online communities for Korean Americans, it may not represent health information behaviour of all Koreans in the USA. Secondly, data could have been collected through direct interactions with the study population. Although social media is considered as a traditional live resource for observing people's actual needs and behaviour in an unobtrusive manner, it does not allow a researcher to directly question participants about their needs and reasons for their behaviour. Thirdly, data used in our study used questions and answers posted between 2013 and 2014\. This specific time period may have an impact on the generalizability of study results. Finally, this study focused on exploring relationships among main topics in questions and in replies respectively in order to understand how diverse topics regarding breast cancer are associated with each other. The latent relationships between topics in questions and the associated sets of replies are beyond the scope of the current study.

## Conclusion

This study identified major discussion topics of Korean Americans seeking breast cancer information online, and structures among those topics. The researchers aimed to understand Korean Americans' behavioural patterns in relation to breast cancer information-seeking and sharing. Based on the data, both medical and non-medical topics were found to be substantially linked to breast cancer information-seeking. The following are the important findings:

*   Medical topics, such as breast cancer testing and treatment, were central to the breast cancer discussions. However, these topics were more frequently discussed in the context of diagnosis based on suspected breast cancer symptoms as opposed to preventative measures.
*   Along with medical related information, forum participants derived physiological or emotional benefits often through simple, encouraging remarks.
*   Socioeconomic factors, such as insurance and financial concerns, appeared to negatively impact access to clinical services.

In future studies, we will further probe the needs and reasons for the behavioural patterns of Korean Americans through more direct interactions with them. Furthermore, the associations between information needs expressed in questions and provided information in associated replies will be analysed to examine how well the needs are satisfied from replies from many others online.

## About the author

**Min Sook Park** is a PhD gradaute of the School of Information at the Florida State University. Her research focuses on understanding social-behavioral aspects and semantic structure of health consumers in social media. Her research has concerned exploring and mining large-scale, unstructured textual data on online communities in context of chronic health conditions and health issues of immigrant communities. She can be contacted at [mp11j@my.fsu.edu.](mailto:mp11j@my.fsu.edu)  
**Hyejin Park** is an associate professor at Florida State University College of Nursing, 600 W. College Avenue Tallahassee, FL 32306\. Her research interest focuses on implanting and evaluating standardised nursing language-based electronic nursing documentation systems across health care settings. Dr. Park also conducts researches on eHealth literacy to decrease disparities in accessing online health information. She can be contacted at [hpark5@fsu.edu.](mailto:hpark5@fsu.edu)

#### References

*   Adamic, L., Zhang, J., Bakshy, E. & Ackerman, M. (2008). Knowledge sharing and yahoo answers: everyone knows something. In _Proceedings of the 17th International Conference on World Wide Web_ (pp. 665-674). New York, NY: ACM.
*   Ahmad, F., Shik, A., Vanza, R., Cheung, A., George, U. & Stewart, D.E. (2004). Popular health promotion strategies among Chinese and East Indian immigrant women. _Women & Health, 40_(1), 21-40.
*   _Asian American Health Initiative._ (2005). _[Who are Asian Americans?](http://www.aahiinfo.org/english/asianAmericans.php)_ Rockville, MD: Montgomery County Department of Health and Human Services. Retrieved from http://www.aahiinfo.org/english/asianAmericans.php [Unable to archive.]
*   American Cancer Society. (2013). _[Cancer prevention and early detection facts and figures 2013.](http://www.cancer.org/acs/groups/content/@epidemiologysurveilance/documents/document/acspc-037535.pdf)_ Atlanta, GA: American Cancer Society. Retrieved from http://www.cancer.org/acs/groups/content/@epidemiologysurveilance/documents/document/acspc-037535.pdf [Unable to archive.]
*   Belkin, N. J., Oddy, R. N., & Brooks, H. M. (1982). ASK for information retrieval: Part I. background and theory_Journal of Documentation, 38_(2), 61-71.
*   Borgatti, S. P. (1995). Centrality and AIDS. _Connections, 18_(1), 112-114.
*   Borgatti, S. P. (2005). Centrality and networkflow. _Social Networks, 27_(1), 55-71.
*   Borgatti, S. P., Everett, M. G. & Freeman, L. C. (2002). _Ucinet for Windows: software for social network analysis_. Harvard, MA: Analytic Technologies.
*   Caidi, N., Allard, D. & Quirke, L. (2010). Information practices of immigrants. _Annual Review of Information Science and Technology, 44_, 493-531.
*   Changrani, J., Lieberman, M., Golant, M., Rios, P., Damman, J. & Gany, F. (2008). Online cancer support groups: experiences with underserved immigrant Latinas. _Primary Psychiatry 15_(10), 55-62.
*   Choi, K. S., Lee, S., Park, E-C., Kwak, M-S., Spring, B. J. & Juon, H-S. (2010). Comparison of breast cancer screening rates between Korean women in America versus Korea. _Journal of Women's Health, 19_(6), 1089-1096.
*   Cohen, J. (1960). A coefficient of agreement for nominal scales. _Educational & Psychological Measurement, 20_(1), 36-46.
*   Coronges, K. A., Stacy, A. W. & Valente, T. W. (2007). Structural comparison of cognitive associative networks in two populations. _Journal of Applied Social Psychology, 37_(9), 2097-2129.
*   Darkes, J., & Goldman, M. S. (1998). Expectancy challenge and drinking reduction: process and structure in the alcohol expectancy network. _Experimental and Clinical Psychopharmacology, 6_(1), 64-76.
*   Doerfel, M. L. (1998). What constitutes semantic network analysis? A comparison of research and methodologies. _Connections, 21_(2), 16-26.
*   Doerfel, M. L. & Barnett, G. A. (1999). A semantic network analysis of the international communication association. _Human Communication Research, 25_(4), 589-603.
*   Dervin, B. (1983). _[An overview of sense-making research: concepts, methods and results.](http://www.webcitation.org/6lp1JoQz9)_ Paper presented at the International Communication Association Annual Meeting, Dallas, Texas, USA, May l983\. Retrieved from http://faculty.washington.edu/wpratt/MEBI598/Methods/An%20Overview%20of%20Sense-Making%20Research%201983a.htm (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp1JoQz9)
*   Everett, M. G. & Borgatti, S. P. (2005). Extending centrality. _Models and Methods in Social Network Analysis, 35_(1), 57-76.
*   Fogel, J., Albert, S. M., Schnabel, F., Ditkoff, B. A. & Neugut, A. I. (2002). Internet use and social support in women with breast cancer. _Health Psychology, 21_(4), 398-404.
*   Fox S. & Purcell, K. (2010). _[Chronic disease and the Internet.](http://www.webcitation.org/6lp1PQT8m)_ Washington, DC: Pew Research Center. Retrieved from http://www.pewinternet.org/2010/03/24/chronic-disease-and-the-internet/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp1PQT8m)
*   Freeman, L. C. (1979). Centrality in social networks conceptual clarification. _Social Networks, 1_(3), 215-239.
*   Garvin, B. J., Moser, D. K., Riegel, B., McKinley, S., Doering, L. & An, K. (2003). Effects of gender and preference for information and control on anxiety early after myocardial infarction. _Nursing research, 52_(6), 386-392.
*   Germeni, E., Bianchi, M., Valcarenghi, D. & Schulz, P. J. (2015). [Longitudinal qualitative exploration of cancer information-seeking experiences across the disease trajectory: the INFO-SEEK protocol](http://www.webcitation.org/6lp1XbF6h). _BMJ Open, 5_(10), e008933\. Retrieved from http://bmjopen.bmj.com/content/5/10/e008933.full (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp1XbF6h)
*   Gooden, R. & Winefield, H. (2007). Breast and prostate cancer online discussion boards: a thematic analysis of gender differences and similarities. _Journal of Health Psychology_, 12, 103-114.
*   Grebitus, C. & Bruhn, M. (2008). Analyzing semantic networks of pork quality by means of concept mapping. _Food Quality and Preference, 19_(1), 86-96.
*   Han, H. R., Kang, J., Kim, B. K., Ryu, J. P. & Kim, M. T. (2007). Barriers to and strategies for recruiting Korean Americans for community-partnered health promotion research. _Journal of Immigrant Health, 9_(2), 137-146.
*   Hanneman, R. & Riddle, M. (2005). _Introduction to social network methods_. Riverside, CA: University of California, Riverside.
*   Henderson, G. R., Iacobucci, D. & Calder, B. J. (1998). Brand diagnosis: mapping branding effects using consumer associative networks. _European Journal of Operational Research, 111_(2), 306-327.
*   Huang, A. (2013, November 12). [Disparities in health insurance coverage among Asian Americans.](http://www.webcitation.org/6lp054poc) _Asian American Policy Review_ Retrieved from http://hksaapr.com/disparities-in-health-insurance-coverage-among-asian-americans/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp054poc)
*   Iacobucci, D., Henderson, G., Marcati, A. & Chang, J. (1996). Network analyses of brand switching behavior. _International Journal of Research in Marketing, 13_(5), 415-429.
*   Kadushin, C. (2012). _Understanding social networks_. New York, NY: Oxford University Press.
*   Kao, D. (2010). Factors associated with ethnic differences in health insurance coverage and type among Asian Americans. _Journal of Community Health, 35_(2), 142-155.
*   Kim, S. S., Kaplowitz, S. & Johnston, M. V. (2004). The effects of physician empathy on patient satisfaction and compliance. _Evaluation & the Health Professions, 27_(3), 237-251.
*   Kim, W. & Keefe, R. H. (2010). Barriers to healthcare among Asian Americans. _Social Work in Public Health, 25_(3-4), 286-295.
*   Kim, S. C., Shah, D. V., Namkoong, K., McTavish, F. M. & Gustafson, D. H. (2013). Predictors of online health information-seeking among women with breast cancer: the role of social support perception and emotional well-being. _Journal of Computer-Mediated Communication, 18_(2), 212-232.
*   Kim, W., Kreps, G. & Shin, C. N. (2015). The role of social support and social networks in health information-seeking behavior among Korean Americans: a qualitative study. _International Journal for Equity in Health, 14_(1), 40.
*   Kim, S. & Yoon, J. (2012). [The use of an online forum for health information by married Korean women in the United States.](http://www.webcitation.org/6lp4r904T) _Information Research, 11_(2), paper 514\. Retrieved from http://www.informationr.net/ir/17-2/paper514.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp4r904T)
*   Landis, L. & Koch, G. (1977). The measurement of observer agreement for categorical data. _Biometrics, 33_(1), 159-174.
*   Lee, S. Y. & Hawkins, R. (2010). Why do patients seek an alternative channel? The effects of unmet needs on patients' health-related Internet use. _Journal of Health Communication, 15_(2), 152-166.
*   Liberman, M. & Goldstein, B. (2005). Self-help on-line: an outcome evaluation of breast cancer bulletin boards. _Journal of Health Psychology, 10_(6), 855-862.
*   Liu, L., Zhang, J., Wu, A. H., Pike, M. C. & Deapen, D. (2012). Invasive breast cancer incidence trends by detailed race/ethnicity and age. _International Journal of Cancer, 132_(2), 395-404.
*   Lim, S., Berry, F. S. & Lee, K-H. (2015). Stakeholders in the same bed with different dreams: semantic network analysis of issue interpretation in risk policy related to mad cow disease. _Journal of Public Administration Research and Theory, 25_(3), muu052.
*   Malik, S. H. & Coulson, N. S. (2010). Coping with infertility online: an examination of self-help mechanisms in an online infertility support group. _Patient education and counseling, 81_(2), 315-318.
*   Marchionini, G. (1995). _information-seeking in electronic environments._ Cambridge: Cambridge University Press.
*   Marton, C. & Choo, C. W. (2012). A review of theoretical models of health information-seeking on the Web. _Journal of Documentation, 68_(3), 330-352.
*   Maxwell, A. E., Bastani, R. & Warda, U. S. (1998). Mammography utilization and related attitudes among Korean-American women. _Women & Health, 27_(3), 89-107.
*   Michelson, A. & Contractor, N.S. (1992). Structural position and perceived similarity. _Social Psychology Quarterly, 55_(3), 300-310.
*   Min, P. G. (1995). _Asian Americans: contemporary trends and issues_. Newbury Park, CA: Sage Publications.
*   Nambisan, P. (2011). information-seeking and social support in online health communities: impact on patients' perceived empathy. _Journal of the American Medical Informatics Association, 18_(3), 298-304.
*   Napier, A. D., Ancarno, C., Butler, B., Calabrese, J., Chater, A., Chatterjee, H., and Woolf, K. (2014). Culture and health. _The Lancet, 384_(9954),1607-1639.
*   National Cancer Institute. (2015) _[Breast cancer prevention: what is prevention?](http://www.webcitation.org/6lp5ZDsnD)_ Bethesda, MD: National Cancer Institute. Retrieved from http://www.cancer.gov/types/breast/patient/breast-prevention-pdq#section/_1 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp5ZDsnD)
*   Nelson, D.L. & McEvoy, C.L. (2004). Implicitly activated memories: the missing links of remembering. In C. Izawa & N. Ohta (Eds.), _Learning and memory: advances in theory and applications_ (pp. 177-198). Mahwah, NJ: Erlbaum.
*   Oh, K.M., Jun, J., Zhao, X., Kreps, G.L. & Lee, E. E. (2015) Cancer information-seeking behaviors of Korean American women: a mixed-methods study using surveys and focus group interviews. _Journal of Health Communication, 20_(10). 1143-1154.
*   Oh, K.M., Kreps, G., Jun, J. & Ramsey, L. (2011). Cancer information-seeking and awareness of cancer information sources among Korean Americans. _Journal of Cancer Education, 26_(2), 355-364.
*   Oh, S., Zhang, Y. & Park, M. (2013). Health information needs on diseases: a coding schema development for analyzing health questions in social Q&A. _Proceedings of the American Society for Information Science and Technology, 49_(1), 1-4.
*   Park, H. & Park, M. (2014). Cancer information-seeking behaviors and information needs among Korean Americans in the online community. _Journal of Community Health, 39_(2), 213-220.
*   Pitts, V. (2004). Illness and Internet empowerment: writing and reading breast cancer in cyberspace. _Health, 8_(1), 33-59.
*   Rubenstein, E. L. (2015). They are always there for me: the convergence of social support and information in an online breast cancer community. _Journal of the Association for Information Science and Technology, 66_(7), 1418-1430.
*   Ruiz, J. & Barnett, G. (2015). Exploring the presentation of HPV information online: a semantic network analysis of Websites. _Vaccine, 33_(29), 3354-3359.
*   Sadler, G. R., Ryujin, L. T., Ko, C. M. & Nguyen, E. (2001). Korean women: breast cancer knowledge, attitudes and behaviors. _BMC Public Health, 1_(1), 7.
*   Savolainen, R. (2008). _Everyday information practices: a social phenomenological perspective._ Lanham, MD: Scarecrow Press.
*   Shaw, B. R., DuBenske, L. L., Han, J. Y., Cofta-Woerpel, L., Bush, N., Gustafson, D. H. & McTavish, F. (2008). Antecedent characteristics of online cancer information-seeking among rural breast cancer patients: an application of the cognitive-social health information processing (C-SHIP) model. _Journal of Health Communication, 13_(4), 389-408.
*   Shin, H., Song, H., Kim, J. & Probst, J. C. (2005). Insurance, acculturation, and health service utilization among Korean-Americans. _Journal of Immigrant Health, 7_(2), 65-74.
*   Smith, R. A., Cokkinides, V. & Brawley, O. W. (2009). Cancer screening in the United States, 2009: a review of current American cancer society guidelines and issues in cancer screening. _Cancer Journal for Clinicians, 59_(1), 27-41.
*   Suarez, L., Ramirez, A. G., Villarreal, R., Marti, J., McAlister, A., Talavera, G. A. & Perez-Stable, E. J. (2000). Social networks and cancer screening in four US Hispanic groups. _American Journal of Preventive Medicine, 19_(1), 47-52.
*   Taylor, S., Harley, C., Campbell, L. J., Bingham, L., Podmore, E. J., Newsham, A. C., ... & Velikova, G. (2011). Discussion of emotional and social impact of cancer during outpatient oncology consultations. _Psycho-Oncology, 20_(3), 242-251.
*   Thomson, M. D. & Hoffman, G. L. (2009). Acculturation and cancer information preferences of Spanish-speaking immigrant women to Canada: a qualitative study. _Health Care for Women International, 30_(12), 1131-1151.
*   U.S. _Census Bureau_. (2015). _[Asian/Pacific American heritage month: May 2015.](http://www.census.gov/newsroom/facts-for-features/2015/cb15-ff07.html)_ Washington, DC: Census Bureau. Retrieved from http://www.census.gov/newsroom/facts-for-features/2015/cb15-ff07.html [Unable to archive.]
*   US. _Centers for Disease Control and Prevention_. _National Center for Health Statistics._ (2013). _[Health of Asian or Pacific islander population.](http://www.webcitation.org/6lp0Sr36z)_ Atlanta, GA: Centers for Disease Control and Prevention. Retrieved from http://www.cdc.gov/nchs/fastats/asian-health.htm (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp0Sr36z)
*   US. _Centers for Disease Control and Prevention_. _National Center for Health Statistics._ (2011). _[National health interview survey.](http://www.webcitation.org/6lp74g3df)_ Atlanta, GA: Centers for Disease Control and Prevention. Retrieved from http://www.cdc.gov/nchs/nhis.htm (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lp74g3df)
*   Wang, L. & Kwak, M. J. (2015). Immigration, barriers to healthcare and transnational ties: a case study of South Korean immigrants in Toronto, Canada. _Social Science & Medicine, 133_, 340-348.
*   Wang, A., Wang, H. J., Abrahams, A. & Fan, W. (2015). An analytical framework for understanding knowledge-sharing processes in online Q&A communities. _ACM Transactions on Management Information Systems, 5_(4), 1-31.
*   Wen, K.-Y., McTavish, F., Kreps, G., Wise, M. & Gustafson, D. (2011). From diagnosis to death: a case study of coping with breast cancer as seen through online discussion group messages. _Journal of Computer-Mediated Communication, 16_(2), 331-361.
*   Winefield, H., Coventry, B., Pradhan, M., Harvey, E. & Lambert, V. (2000). A comparison of women with breast cancer who do and do not seek support from the Internet. _Australian Journal of Psychology, 55_(1), 30-34.

