###  Vol. 3 No. 2, September 1997



# Towards adaptive information systems: individual differences and hypermedia

#### Sherry Yu-Hua Chen and [Nigel J. Ford](mailto:n.j.ford@shef.ac.uk)  
Department of Information Studies  
University of Sheffield

## 1\. Introduction

Hypermedia, which is a combination of multimedia and hypertext, is distinguishable from traditional computer assisted learning (CAL) by the node-and-link structure inherent to the organisation of information and the fact that it is also based on integrated media ([Heller, 1992](#hell)). Compared with alternative tools, hypermedia in the learning environment has been associated with a number of potential benefits.

## 2\. Potential Benefits For Learning

Hypermedia systems possess a number of characteristics which advocates have claimed are beneficial to the learning process:

*   Hypermedia provides a highly interactive environment.
*   Hypermedia allows the integration of different media, such as text, video, audio and graphics.
*   Hypermedia has a non-linear organisation in the form of a network of nodes and links.

Each of these factors is considered below.

### 2.1 Hypermedia Provides A Highly Interactive Environment

It is generally accepted that interaction is a 'Good Thing', assuming that learners can get prompt feedback from the system. [Bork](#bork) (1987) highlighted two main advantages of interaction - individualisation and increased motivation. Hypermedia has the potential to provide more freedom of choice of what information users see, and how they see it. Many would argue that using a computer can be an intrinsically motivating activity. Bork has also argued that students learn more when they spend more quality time on a task, and described observations of students indicating that they stopped using learning materials at those points at which the level of interaction was low.

### 2.2 Hypermedia Allows The Integration Of Different Media

[Popham](#poph) (1961) demonstrated that students taught using primarily audio material performed equally as well as students taught by conventional methods. Similarly, [Chu & Schramm](#chu) (1974) reported no significant differences between the performance of students using TV or video material and that of students taught by conventional methods.

Computer-Aided Learning (CAL), Computer-Based Training (CBT), and Computer-Aided Instruction (CAI) are all synonyms for the same basic strategy of using a computer to give instruction. Many reviews of this medium exist. These cover a great number of studies using systems which incorporate a wide range of subject matter, instructional strategy, and student ability. However, as with the audio and video studies, the general conclusion reached by most has been that students using CAI materials performed neither better nor worse than those taught conventionally.

All of these studies reached the conclusion that it is not the selection of a particular medium which matters, but instead the carefully considered use of a combination of media which provides the best results. [Schraman](#schr) (1985) has made the following comprehensive statement on the effect of various media on the learning process:

> "Is one medium any more effective than others? For some purposes, probably yes, but overall there is no superlative medium of instruction, any more than there is one simple algorithm for selecting one medium over others. We have come to realise in recent decades that learning from the media is not an area that lends itself to simple answers. It is an extremely complex multivariate process that challenges us, if we are to understand it, not to ask the simple questions, but rather to concern ourselves with the conditions for selecting one medium over another, for combining and for using media." ([Schraman, 1985: 45](#schr))

### 2.3 Hypermedia Has A Non-Linear Organisation

Current theories of learning are based upon principles of cognitive psychology, which emphasise the importance of considering learning as a reorganisation of the knowledge structures which constitute semantic memory ([Eysenck & Keane, 1990](#eyse)). Hypermedia links may be considered to be analogous in some ways to the relationships of knowledge, while nodes are analogous to the attributes of semantic memory ([Jonassen, 1989](#jona2)). This analogy is supported by [Donald](#dona) (1989) who claims that meaning arises as a function of structure.

The relationship between hypermedia structures and knowledge structures is not universally accepted (e.g. [McKnight et al., 1990](#mckn)), but the associative linking of information nodes paralleling human knowledge structures is probably the argument most frequently used as justification by advocates of hypermedia as an educational strategy.

Many educators have believed that learning in higher education is linked to a change not only in the knowledge that is acquired, but also in the way in which it is acquired: there is a movement towards pluralistic, or non-linear thinking. Hypermedia is therefore seen as ideally suited to match this change, as information can be represented in a non-linear manner. [Nielsen](#niel) (1990) has argued that hypermedia is well suited for open learning applications where the students are allowed freedom of action and encouraged to take the initiative, and [Ess](#ess) (1991) describes how following of hypertext links resulted in greater use of cross-references and other textual equivalents in non-hypermedia materials.

## 3\. Potential Problems For Learning

However, hypertext has also been associated with a number of potential problems. [Hammond and Allinson](#hamm2) (1989), for example, have outlined four problems in using hypertext or hypermedia for learning:

*   Disorientation
*   Lack of comprehension
*   Problems with access
*   Inefficient learning strategies

Each of these problems is discussed below.

### 3.1 Disorientation

One of the problems that research in hypermedia quickly encountered was how users are able to find information in a hypermedia information base, when the goal state is not specified as an option on the currently viewed screen. Obviously a default top-level access screen such as a table of contents screen could be made easily accessible, but this can only list a finite number of topics which may have a potentially infinite number of descriptors. Owing to the strong spatial metaphor that has become associated with hypermedia this problem became known as one of navigation. With hypermedia it is often extremely difficult to acquire such maps due to the lack of such cues and disorientation may occur ([Fiderio, 1988](#fide)). As [Elm and Woods](#elm) (1985) have claimed, navigation problems occur because of:

> "...the user not having a clear conception of the relationships within the system, or knowing his present location in the system relative to the display structure, and thus finding it difficult to decide where to look in the system." ([Elm and Woods, 1985: 67](#elm))

Disorientation manifests itself as confusion and frustration and leads to time being wasted wandering round the information base, reducing the effectiveness of the system.

Thus the great strength of hypermedia, _flexibility_, leads to problems of disorientation. ([Conklin, 1987](#conk)). Attempts to overcome the problem of disorientation have tended to either give users more cues, such as maps, in order for them to orientate themselves; ([Folz, 1991](#folz); [Hammond & Allinson, 1989](#hamm2)), or other spatial cues ([Fiderio, 1988](#fide)) or attempt to 'linearize the non-linear' by providing paths through the information relating to a specific goal. This not only aids the access of information, it may aid learners to develop some type of mental map ([Edward & Hardman,1989](#edwa); [Zellweger, 1989](#zell)), which can enable them to find their way around more easily.

Disorientation to varying degrees is bound to have an effect on learning, and most generally this is taken to be a negative effect, disorientation prevents learners from getting to where they want to go; it slows down the access of meaningful and relevant material; it confuses the learners reducing their patience and diminishing motivation to learn. Additionally, the demands that the system makes on the learner to continually choose where to go next, may result in 'cognitive overheads' ([Jonassen and Grabinger, 1990](#jona1)). In the other words, learners spend so much time choosing and navigating that they have little time to devote to reading and thinking about the information.

[Kibby, et al.](#kibb) (1990) have taken different line on disorientation. They have suggested that the process whereby a learner actively seeks recovery from being lost can actually be a positive learning experience. They have given the example that an efficient way to learn one's way around a strange city is to get lost and then have to find one's way again. Therefore, getting lost in hypermedia may force learners to consider the organisation of the hypermedia system more closely, which may give them more of an idea as to the way in which the domain information is organised. Such a learning experience may only arise if the structure of the hypermedia system corresponds to the structure of the domain closely. Without this mapping the learner may be misled into making false inferences about the relationships in the domain itself. In addition, [Kibby](#kibb) (1990) and [Mayes](#maye) (1994) draw a distinction between hyperspace and conceptual space, i.e. the general framework of the hypermedia system and the actual conceptual interrelations. Only getting lost in conceptual space would have any educational benefit, so it is suggested that hyperspace follows the structure of conceptual space as closely as possible.

### 3.2 Lack Of Comprehension

Cognitive interpretations of learning and memory emphasise the interconnected nature of knowledge represented as a 'tangled hierarchy' ([Anderson and Reiser, 1985](#ande)). This has often led to the assumption that, because hypermedia explicitly represents information in a network of conceptual relationships, it enables learners to more readily understand and integrate information. Such assumptions may well be true, but there is some doubt as to whether novices can make sense of all the interconnections and therefore construct some form of coherent overview. Text typically has a wide variety of structural patterns (i.e. path-based links and content-based links) that provide information such as where the text is going, providing integration between what is currently being read and information that the reader has encountered before, or information that they will encounter later on. It is suggested that when these structures are removed, comprehension will become problematic. [Gordon](#gord) (1990) has compared the effectiveness of hypertext presentations of technical and non-technical topics with conventional linear tutorials. They found that when subjects were later asked a series of questions which required them to recall information from the texts, recall was significantly higher in the linear conditions both for basic factual information and for information requiring them to integrate the material. Subjects also expressed a preference for the linear presentation. Indeed in studies where text and hypertext have been directly compared no study shows that hypermedia has any advantage over text for comprehension or learning.

Studies such as the ones above should not be seen as indicating that hypermedia can never be more effective for the delivery of instructional materials than standard text. There may be an interaction between comprehension and the level of domain specific expertise. For example Kintsch's Construction Integration text comprehension model predicts that non-linear representations may be useful when the learners have some understanding of the domain ([Folz, 1991](#folz)).

### 3.3 Problems With Access

Folz's point mentioned above also has bearing on the ability of learners to access known material. Generally hypermedia systems tend to be structured with respect to the conceptual interrelationships of a particular domain. While access may appear logical to someone who understands the subject, it may afford little help to novices. This tends to contradict the stance taken by [Jonassen and Grabinger](#jona1) (1990) who have referred to hypermedia as the ultimate accretion medium. They have argued that the structure of hypermedia is tailor-made for novices to build up a workable understanding of topic areas. In contrast it appears that hypermedia may be more of a medium for tuning, whereby learners with well-developed domain schemata can use the non-linear structure effectively to access relevant information.

### 3.4 Inefficient Learning Strategies

The connections in hypermedia mean that learners can access material with only minimal knowledge of what it is they are looking for. This is achieved by browsing, following the various links that are present within the information itself. Users can follow links, perhaps denoted by keywords, that seem to indicate the sort of information that they are interested in finding out about. It is a heuristic strategy to locate relevant information by following the links. On the other hand, it may be inefficient in terms of the time taken; many blind alleys may be followed, and there is no guarantee of the goals being reached. However, it is useful in the sense that it will often result in success, when all other attempts have failed.

[Marchionini](#marc) (1988) has stated three reasons as to why hypermedia users browse:

> "Firstly, they browse because they cannot, or have not defined their search objective...Second, people browse because it takes less cognitive load to browse than it does to plan and conduct an analytical, optimised search...Third, people browse because the information system supports and encourages browsing." ([Marchionini, 1988: 234](#marc))

Marchionini's third point illustrates that not only does hypermedia permit browsing, but the various on-screen facilities positively encourage the learner to browse, irrespective of whether or not it is the most sensible strategy to adopt ([Gaver, 1991](#gave)). Additionally, [Hammond](#hamm1) (1993) points out that very often hypermedia can offer misdirection in that buttons and active text tend to remain on-screen irrespective of whether the learner has seen the information before; thus causing looping. [McAleese](#mcal) (1989) has stated that browsing should be purposeful, and that the design of the system can, in some respect, support users in their task of finding information by providing meaningful structure to enable successful navigation. However, hypermedia often consists of a mass of interconnections that have little contextual sensitivity. A learner who is uninformed with respect to the domain may find information seeking difficult. Often such learners may find that browsing is the only realistic option open to them. This may have severe effects on the quality of the learning experience, as it may side-track learners, resulting in their covering irrelevant material.

## 4\. The Current Research

Research currently being undertaken by the present authors seeks to examine the effectiveness of hypermedia as a learning tool, taking into account the competing claims described above. Central to the research is the notion of individual differences. It may be that a number of the benefits and problems described above may affect different individuals differentially. Such effects may indeed have masked significant findings in previous studies. Also central to the research are differences in navigational patterns, and different levels of learning outcome. There is evidence to suggest that learners may often adopt differing, but equally valid, effective, and navigational approaches to learning. The effects and effectiveness of such strategies can only fully be examined if learning is analysed in terms of different types and levels.

### 4.1 Individual Differences

#### User characteristics

In an effort to increase the effectiveness of the use of hypermedia systems, the focus of recent research has shifted from systems to users. Users characteristics and cognitive models have played important roles when designing or evaluating hypermedia systems. [Allinson](#alli) (1991) has categorised and analysed the knowledge of mental models as world knowledge, system knowledge, task knowledge, and domain knowledge. These types of knowledge may influence the ways learners search for information, and the nature of the information they need. Many previous studies fail to take account of these four kinds of knowledge, and neglect the interaction between users characteristics and learning styles (e.g. [Shasaani, 1993](#shas); [Francis, 1993](#fran), [Linard & Zeillger, 1995](#lina))._Learning styles_

A number of previous studies have explored the appropriateness of different learning styles to hypermedia learning systems (e.g. [Tompsett, 1990](#tomp)) and tools ([Shnell, 1992](#shne); [Scott, 1993](#scot)). They used learning styles to predict learning performance. However, not many studies have directly addressed the problem of developing systems which adapt to learners' individual differences. [Borgman](#borg) (1995) has claimed that individual differences research will "allow us both to make the transition to new and innovative interfaces and to bring the systems to the users rather than the other way around".

### 4.2 Navigation Patterns

Much research has examined the problem of 'getting lost in hyperspace' and the relationships between styles and navigation path data in hypermedia learning environments (e.g. [Mayers, 1994](#maye); [Simpson, 1994](#simp); [Dillon, 1994](#dill); [Edward & Hardman, 1989](#edwa)). In hypermedia learning environment, a crucial navigation consideration is concept space ([Kent, 1995](#kent)). However, concept space is ignored in many current hypermedia learning environments ([Hammond, 1993](#hamm1)). Concept space is more complex than hyperspace because it is based in implicit knowledge. Hyperspace has to do with searching and locating information; concept space is about comprehension of information. Clearly, students need to know where they are in the system, but they also need information about where they are in the program, i.e. how their current understanding relates to the whole or parts of the subject. The phenomenon of becoming 'lost in concept space' may often happen in relatively passive learning activity in which learners are not required to make intellectual decisions about the choices of hyperlinks.

### 4.3 Learning Outcomes

Hypermedia systems have been generally accepted as an educational support tool ([Jonassen, 1992](#jona2)). Consequently, many researchers have investigated the effects of hypermedia on learning outcomes. However, many studies have failed to employ any subtle or complex definitions of learning outcomes ([Egan et al., 1991](#egan); [Bailey & Hall, 1995](#bail); [Welsh, 1995](#wels); [Aero & Catenazzi, 1996](#aero)). The present research seeks to investigate learning in terms of deep and surface level processing, and the scope of exploration from relatively broad to narrow in the subject hierarchy. The study will explore both quantitative and qualitative aspects of learning.

## 5\. Concluding Remarks

Hypermedia use is premised on learner control ([Jonassen & Grabinger, 1990](#jona1)). Traditional CAL systems have often been authored with a distinct pedagogical strategy underlying the sequence of the learning material. In hypermedia learning environments learners have relatively high levels of control over the sequence of their learning activities, because of their flexibility. In many hypermedia learning environments, learners take the responsibility for the learning process and make decisions relating to choice of routes through the learning material. For these reasons, CAL systems based on hypermedia can be thought of as focusing more on 'learning' than 'teaching'. Research on individual differences may help us create systems that can adapt to learner according to their needs. It may help the development of hypermedia learning environments that are useful to a wide range of users with differing abilities, styles and preferences.

The research is in its early stages. However, it is hoped that by taking into account a number of individual differences, relationships between navigational patterns and both quantitative and qualitative aspects of learning will be identified. The goal is to develop building blocks for a model of learning which will support the development of adaptive information accessing and presentation systems to support learning.

## References

*   <a name="aero"></a>Aero, I. and Catenazzi, N. (1996) The Evaluation of Hypermedia Learning Environemnt: The CESAR Experience. _Journal of Educational Multimedia and Hypermedia_. 5 (1), 3-22.
*   <a name="alli"></a>Allinson, B. (1991) Cognitive research in information science. _Annual Review of Information Science and Technology_. 26, 3-37.
*   <a name="ande"></a>Anderson, J. R. & Reiser, B. L. (1985) The LISP tutor. _Byte_. 10 (4), 159-175.
*   <a name="bail"></a>Bailey, J. D. and Hall, J. L. (1995) CBL in Engineering: students' use of a learning resource on phase diagrams. _Computers & Education_. 25 (1/2), 75-80.
*   <a name="borg"></a>Borgman, C. L. Hirsch, J.G., et al. (1995) Children's Searching Behaviours on Browsing and Keyword Online Catalogs: The Science Library Catalog Project _Journal of the American Society for Information Science_. 46 (9), 663-684.
*   <a name="bork"></a>Bork, E. (1987) An investigation of Hypermedia. _Reading Research and Instruction_ 27 (4), 5-24.
*   <a name="chu"></a>Chu, G. & Schramm, W. (1974) Learning from television: what research says. California: Institute for Communication Research, Stanford University.
*   <a name="cohe"></a>Cohen, K., Husaim, L. et al., (1981) Current Technology for Education. _American Libraries_ 20 (2), 158-162\.
*   <a name="conk"></a>Conklin, J. (1987) Hypertext: An Introduction and Survey. _IEEE Computer_ (Sept.), 17-41.
*   <a name="dill"></a>Dillon, A. C. (1994_. Interfaces for Information Retrieval and Online Systems_. Westport, CT: Greenwood.
*   <a name="dona"></a>Donald, K. (1989) Matching learner preference to amount of instruction. _Education Technology Reseach and Development_ 23 (2), 5-14.
*   <a name="edwa"></a>Edward, D. & Hardman, L. (1989) Lost in hyperspace: A faceted approach to hypertext. In: R. McAleese (Ed.). _Hypertext : Theory into Practice_. Norwood, NJ:Ablex.
*   <a name="egan"></a>Egan, D., Lesk, M. E., et al. (1991) Turning ideas to hypertext. _Hypermedia._ 7(4): 33-40.
*   <a name="elm"></a>Elm, W. C. & Woods, D. D. (1985) Getting lost: A Case Study in Interface Design. _Proceeding of the Human Factors Society_, 927-931.
*   <a name="ess"></a>Ess, H. (1991) Hypermedia: functional features and research issues. _Educational Technology_ 31 (8), 24-31.
*   <a name="eyse"></a>Eysenck, M. W. & Keane, M. T (1990) _Cognitive Psychology: a student's hadbook_. Hillsdale, NJ: LEA.
*   <a name="fide"></a>Fiderio, J. (1988) A Grand Vision. _Byte_. October. 237-247.
*   <a name="folz"></a>Folz, P. W. (1991) A text comprehension model of hypertext. In: S. P. Robertson, G. Ohlson & J. S. Ohlson (Eds). _Reaching Through Technology. Proceedings of CHI_ _1991_, 489-490\. New York: ACM.
*   <a name="fran"></a>Francis, L. (1993). The affective domain. _Computers in Education_. 20 (3), 251-255.
*   <a name="gave"></a>Gaver, W. W. (1991) Technology Affordances. In: S. P. Robertson, G. Ohlson & J. S. Ohlson (Eds). _Reaching Through Technology. Proceedings of CHI 1991_, 79-84\. New York: ACM.
*   <a name="gord"></a>Gordon, S. (1990) The effects of hypertext on reader knowledge representation. In: Vicky Lewis (Ed.). _Proceedings of the Human Factors Society, 34th Annual Meeting_. Anahelm, CA. 112-134.
*   <a name="hamm1"></a>Hammond, N. (1993) Learning with hypertext: problems, principles and prospects. In: C. McKnight, Dillon, A. and Richardson, J. (Eds). _Hypertext Psychological Perspective_. New York: Ellis Horwood.
*   <a name="hamm2"></a>Hammond, N. & Allinson, L. (1989) _Extending hypertext for learning_. In: Alistair Suitcliffe & Linda Macaulay (Eds.). _People and Computers_ V. Cambridge: Cambridge University Press. 114-125.
*   <a name="hell"></a>Heller, L. (1992) Hypermedia _Computer-based instruction: Methods and Development_. Englewood Cliffs, NJ: Prentice Hall.
*   <a name="jona1"></a>Jonassen, D. and Grabinger, R. (1990) Problems and issues in designing hypertext/hypermedia for learning. In: Jonassen, D. and H. Mandel (Eds.). _Designing hypermedia for learning_. Berlin, London: Springer-Verlag. 3-26.
*   <a name="jona2"></a>Jonassen, D. H. (1989) Designing Structured Hypertext, and Structuring Access to Hypertext. _Educational Technology_ 28 (11)13-19.
*   <a name="kibb"></a>Kibby, M., Mayes, T., & Anderson, T. (1990) Learning about learning from hypertext. In: Jonassen, D. H. and Mandel, H. (Eds.). _Designing hypermedia for learning_. Berlin, London: Springer-Verlag. 227-250.
*   <a name="kint"></a>Kintsch, W. (1987) The role of knowledge in discourse comprehension: a construction-integration model. _Psychology Review_. 95 (2),163-182.
*   <a name="lina"></a>Linard, M. & Zeillger, G, (1995) Designing navigational support for educational software_. Proceedings of Human Computer Interactions_. 63-78.
*   <a name="mcal"></a>McAleese, R. C. (1989) Navigation and Browsing in Hypertext. In: R. C. McAleese (Ed.). _Hypertext: Theory into Practice_. Norwood, NJ: Ablex.
*   <a name="mckn"></a>McKnight, C., Dillon, J. and Richardson, J. (1990) _Hypertext Psychological Perspective_. New York: Ellis Horwood.
*   <a name="marc"></a>Marchionini, G. (1988) Hypermedia and Learning: Freedom and Chaos. _Educational Technology._ 28 (11), 8-12.
*   <a name="maye"></a>Mayes, M. (1994) A method for evaluating the efficiency of presenting information in a hypermedia environment. _Computer in Education_. 18 (1), 179-182.
*   <a name="niel"></a>Nielsen, J. (1990) _Hypermedia and Hypertext_. London: Academic Press.
*   <a name="poph"></a>Popham, L. (1961) Using AV materials to provide instruction. _Journal of Educational Professional_ 3 (2), 155-172.
*   <a name="schr"></a>Schraman, G. (1985) New Training Tool for Teaching? _Education Technology_ 28 (8), 7-11.
*   <a name="scot"></a>Scott, P. (1993) Generating a hypermedia tutorial from a concept network. _Presented at CAL 93._ York: University of York. April, 1993.
*   <a name="shas"></a>Shasaani, L. (1993) Gender-based differeces in attitudes in attitudes toward computers. _Computer Education._ 20 (2), 169-181.
*   <a name="shne"></a>Shnell, T. J. (1992) Designing instructional computing for meaningful learning. In: P. Winne & M. Jones (Eds) _Foundations and Frontier in Instrctional Computing Systems._. Berlin: Springer-Verlag. 95-113.
*   <a name="simp"></a>Simpson, M.(1994) Navigation in the hypertext. _Hypermedia._ 16 (2), 113-134\.
*   <a name="tomp"></a>Tompsett, C. P. (1990) _Contextual browsing within a hypermedia environment_ . Kingston Upon Thames: Kingston Polytechnic.
*   <a name="wels"></a>Welsh, T. M. (1995). Simplifying Hypermedia Usage for Learners: The effect of Visual and Manual Filtering Capabilities on Efficiency, Perception of Usability, and Performance. _Journal of Educational Multimedia and Hypermedia._ 4 (4), 275-304.
*   <a name="zell"></a>Zellweger, P. T. (1989) Scripted documents. In: Rob Akyscyn (Ed.). _Hypertext '89_. New York: ACM Press. 1-14