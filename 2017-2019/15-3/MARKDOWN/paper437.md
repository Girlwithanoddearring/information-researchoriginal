#### vol. 15 no. 3, September, 2010



# Chinese cultural and power perspective on information systems: technological frames and re-frames


#### [Christina Ling-hsing Chang](#author)  
Department of Information Management, National Pingtung Institute of Commerce, 51 Min-Sheng E. Road, Pingtung, Taiwan 900, Republic of China.

#### Abstract

> **Introduction**. This paper investigates the power operation in information system development processes. Because key actors in different departments possess different professional knowledge, their diverse contexts lead to some employees supporting the information system, while others resist it. This study uses the perspectives of technological frames theory and Chinese power game theory to explore this problem.  
> **Method.** Qualitative data were collected from a port authority in Taiwan. Interviews were carried out with a total of twenty-five persons in five departments of the authority and five customer organizations.  
> **Analysis.** Qualitative, interpretative analysis was carried out on the interview records by three analysts using four technological frames: motivation adoption, strategies and goals relating to the quay management information system (referred to internally as the DHS-IS) and control of the system's functions.  
> **Results.** Key actors employed Chinese cultural perspectives to affect information systems and achieve self-interest.  
> **Conclusions.** The results demonstrate the significance in information system development of Chinese cultural perspectives and power relationships.



## Introduction

### Motivation

Since the 1970s, there have been many academic studies on the effects of information technology innovation on different organizations from the perspectives of power and politics ([Chang 2006a](#cha06a), [2006b](#cha06b), [2007](#cha07); [Chang and Lin 2005](#cha05); [Lawrence _et al._ 2005](#law05); [Silva 2003](#sil03); [Jasperson _et al._ 2002](#jas02); [Lin and Chang 2000](#lin00); [Sillince and Mouakket 1997](#sil97); [Markus 1983](#mar83); [Keen 1981](#kee81)). Because of the high levels of authority enjoyed by information system professionals and managers, using information technology as an important resource in the negotiation process in both information system implementation and innovation in technology are intensely political, power issues ([Markus and Robey 1995](#mar95); [Keen 1981](#kee81)). Information systems and information technology quickly facilitate the changes in relationships, communication methods, influences, authorities and controls among different departments of an organization. At the same time, it raises the level of political behaviour. As information systems enable information to be distributed among top managers, they can be used as managers' source of power to control staff. Information systems and information technology also reduce the bargaining power of the staff, prompting strong resistance to managerial power, potentially to the point of failure ([Zuboff 1988](#zub88); [Kling 1987](#kli87)).

Davidson ([2006](#dav06)) suggested moving beyond considering frame incongruence and its implications for organizational change to include wider organizational and institutional elements. Boland ([2001](#bol01)) went even further by doubting the very existence of frames as cognitive structures that exist in the minds of organizational actors and therefore, the notion of congruent frames or shared meanings. As technology constitutes a core element in organizations, aspects of the frames of organizational members will involve technology. These can serve as the focus for particular interpretations of technology and its role in organizations. For this reason, Orlikowski and Gash ([1994](#orl94): 178) defined technological frames as a '_a subset of members' organizational frames that involve the assumptions, expectations, and knowledge they use to understand technology in organizations. This includes not only the nature and role of the technology itself, but also the specific conditions, applications, and consequences of that technology in particular contexts_'.

At the same time, Lin and Silva ([2005](#lin05)), McGovern and Hicks ([2004](#mcg04)), Davidson ([2002](#dav02)), Mcloughlin _et al._ ([2000](#mcl00)) and Barrett ([1999](#bar99)) adopted the political processes standpoint to interpret technological frames in information systems. In the light of the above literature, it is apparent that individuals in different departments and positions have different perceptions of information systems, and that they coexist, either with the support of or against employees, in the information system development process.

Martinsons and Westwood ([1997](#mar97)) analysed the diversity between Anglo-American and Chinese culture based on the collectivism and Hofstede's ([1991](#hof91), [1980](#hof80)) power-distance index to interpret the hurdles to information system development in Chinese culture. They reminded us that the power issue in the information system development process is closely associated with cultures ([Dhillon 2004](#dhi04); [Foucault 1982](#fou82)). Avgerou ([2001](#avg01)) asserted that some of the Western literature on information systems in developing countries, whilst recognizing the importance of local cultural understanding, views culture as a barrier to information system adoption. It tends to assign or imply a low worth to indigenous culture and a correspondingly high worth to Western culture. He suggested the need for greater respect than this towards the culture of others ([Avgerou 2001](#avg01)).

Meanwhile, Redding ([1990](#red90), [1980](#red80)), Tricker ([1988](#tri88)) and Wong ([1985](#won85)) believed that Chinese leaders are widely perceived to have a natural right to determine organizational objectives because Chinese culture is a high power-distance society characterized by centralization. It is a culture that places emphasis on tradition, authority and social class ([Hofstede 1991](#hof91)). The apparent acceptance of authority could alternatively conceal an insidious exercise of power through manipulation. In the Chinese business culture, although information is a key source of power, it is fundamentally a personal asset rather than an organizational resource ([Redding 1990](#red90)). Power is maintained by carefully controlling key information; most management information really is information exclusively for top managers. Much of it remains in a soft form in the mind of the manager and is orally communicated ([Ko 1995](#ko95)). Key details, ideas and knowledge are selectively passed on to chosen individuals. This promotes a divide-and-rule strategy, since no other individual is privy to the full information set ([Ko 1995](#ko95); [Redding 1990](#red90)).

Therefore, the abilty to provide or withhold information is closely related to power and control ([Coombs _et al._ 1992](#coo92)). Thus, information technology investment in the Chinese business culture has typically stemmed from a desire to monitor and control the basic operations in an expanding business ([Soh _et al._ 1993](#soh93); [Wong 1992](#won92)). This is a concept contrary to the sharing of information characteristic of Western society ([Avgerou 2002](#avg02)), because information systems are based on the principle that organizational information can be shared and used easily by all employees ([Martinsons and Westwood 1997](#mar97)). However, this ideal may not make sense in the Chinese cultural organization ([Redding 1990](#red90)).

### Research purpose

Technological frames, power (and political) issues and cultural perspectives have been used to understand the cognitive structure of organizations in technology development ([Lin and Silva 2005](#lin05); [McGovern and Hicks 2004](#mcg04); [Davidson 2002](#dav02); [McLoughlin _et al._ 2000](#mcl00); [Barret 1999](#bar99)). For instance, Barrett ([1999](#bar99)) develops a theory based on the cultural assumptions about technology which draws on technological frames and structural culture. McLoughlin _et al._ ([2000](#mcl00)) highlight the significance of intervention in the political and cultural systems of organizations to acomplish change. Davidson ([2002](#dav02)) develops a socio-cognitive process model (based on power in information system development) of how frames, and shifts in frame salience, influence sense-making during the determining of requirements. Lin and Silva ([2005](#lin05)) find that the management of information system adoption is a social and political process in which stakeholders frame and re-frame their perceptions of an information system. They highlight the critical importance of the power and political aspects of the analysed processes and illustrate how the influence of powerful organizational individuals can affect the content and direction of a framing process. In addition, they show how various groups' frames change in content or structure and how the evolution of frame structure may help change managers to guide or redirect these processes.

Lin and Silva ([2005](#lin05)) also suggest that social phenomena such as language, sympolic power and communication processes are fundamental in understanding how technological interpretations are framed. Moreover, organization is permeated by the culture of its social environment ([Hofstede 1991](#hof91); [Lin and Silva 2005](#lin05)). Thus, researchers may need to look outside the organization's boundaries to consider the societal and cultural origins of frames (Davidson [2006](#dav06)). In the light of this, it is impossible to understand how individuals make sense of technology and information systems without examining their social interactions and cultural contexts ([Lin and Silva 2005](#lin05); [Davidson 2006](#dav062)). For this reason, it is apparent that both the technological frames of individuals and the culture in which those individuals work have an effect on information systems.

Although the interwining of interpretation and power is evident in several studies of technological frames, to date no scholar has adopted both the national cultural and technological frames perspective from which to analyse how power is wielded in the information system development process. In view of this, it is necessary to understand how power was been manipulated among different actors in Chinese cultural organizations (such as Taiwan) and what kind of technological frames will be found in this paper. This is the first research question to be resolved in the present study. Consequently, the present paper not only adopts the technological frames, but also enhances the interpretation of power by utilizing the Chinese power game theory to contextualize information technology in Chinese culture.

Moreover, to investigate turbulence and change in problem-setting activities, it can be instructive to consider socio-cognitive effects such as frame shifting (Ovaska _et al._ [2005](#ova05); [Boland 2001](#bol01); [El Sawy and Pauchant 1988](#ei88); [Gioia 1986](#gio86)). Although frames, once formed, are resistant to change ([Walsh 1995](#wal95)), contextual changes can trigger shifts that bring new knowledge to the forefront of sense-making ([Davidson 2002](#dav02); [Barr 1998](#bar98); [El Sawy and Pauchant 1988](#ei88); [Gioia 1986](#gio86); [Bartunek 1984](#bar84)). Contextual changes can trigger shifts in technological frames through time and this will lead people to have new understanding of information systems ([Ovaska _et al._ 2005](#ova05); [Boland 2001](#bol01); [El Sawy and Pauchant 1988](#ei88); [Gioia 1986](#gio86)). Unfortunately, those academics promoting this view do not describe the effect of national culture on the technological re-frame. For this reason, how technological frames can shift through time in a Chinese context is the second research question to be resolved in this study.

Using qualitative research methods, the aim of the present study is twofold. The first aim is to interpret power through the lenses of the _technological frames_ and the _Chinese power game: face and favour theory_ ([Hwang1987](#hwa87)). This is done to facilitate an understanding of how five kinds of actor at _KaoKang_ use their resources to wield power in the quay mangement information system (computerization of ship quay arrangement, referred to internally as the DHS-IS) development processes and achieve their goals of self-interest. The consequences for the system of such power-wielding behaviour are also ascertained through the use of these lenses. The second aim is to understand why and how these actors reframe their perceptions of the system. The discussion of Confucianism in much of Western academic and business literature has been oversimplified ([Chung _et al._ 2008](#chu08)). For this reason, this paper has two aims: 1) to resolve the shortcomings in previous research with respect to the kind of technological frames found in the information system development processes in the Chinese cultural context and to explain how national (Chinese) culture influences individuals' power operations within the different technological frames; and 2) to clarify how the national culture triggers a technological reframe through time.

## Theories

### Technological frames

Bostrom and Heinen ([1977](#bos77)) suggested that a large number of the social problems associated with the implementation of information systems were due to the frames of reference held by system designers. Building on this work, the influence of designers' values and the views of users was on information systems, while the conceptual framework of the designer influenced the kind of system designed (Kumar and Bjorn-Andersen [1990](#kum90)). Since these studies were carried out, researchers have considered the perceptions and values of designers and users as part of their investigations into the social aspects of information technology. Gioia ([1986](#gio86)) noted that frames are helpful when they structure organizational experience, allow interpretation of ambiguous situations, reduce uncertainty in conditions of complexity and change, and provide a basis for taking action.

Orlikowski and Gash ([1994](#orl94)) believed that individuals have different interpretations of the same things and thus make sense of things differently. They asserted that an understanding of people's interpretations of technology is critical to understanding their interaction with it. People have to make sense of information technology and in this sense-making process, they develop particular assumptions, expectations and knowledge of the technology, which then serve to shape subsequent actions towards information technology (Orlikowski and Gash [1994](#orl94)). Asserting that different groups within an organization may have different technological frames, they introduced the notion of congruence to describe the nature and extent of differences among frames.

From sociological studies of technology innovation ([Bijker _et al._ 1987](#bij87)), Orlikowski and Gash ([1994](#orl94)) drew the concept of relevant social groups and shared frames. Relevant social groups include individuals whose interactions and experiences with a technology are similar. Group members tend to develop similar frames of reference that guide their understanding and uses of technology in similar ways. To the extent that frames differ among relevant social groups (that is, are incongruent in structure or content) problems such as misaligned expectations, contradictory actions, resistance, skepticism and poor appropriation of information technology may occur ([Orlikowski and Gash 1994](#orl94)). Interventions aimed at overcoming incongruence ideally result in frame alignment and improve organization outcomes.

Therefore, the nature of technological change ([Barrett 1999](#bar99)) and the business value of information technology ([Davidson 2002](#dav02)) frame domains are concerned with organization members' understanding of how information technology could be used to alter business process and relationships. Organization members' expections of work practices change with information technology use ([Barrett 1999](#bar99); [Davidson 2002](#dav02); [Lin and Silva 2005](#lin05)). Knowledge and expectations about how change occurs is an important component of organizational discourse ([Grant _et al._ 2005](#gra05)). Frames related to the technical change process are evident when the scope of a research study includes information technology development or implementation and how the frames of technology development influence decisions ([Ovaska _et al._ 2005](#ova05)). For this reason, people's technological frames influence their actions toward technology and different groups may have incongruent technological frames, which could lead to difficulties around technological use and change. Therefore, a technological frames framework has to reach information technologys potential as a theoretic perspective on information system-related organizational change (Davidson [2006](#dav06)).

In sum, the theory of frames asserts that people's behaviour inhibits their cognitive frame. However, through socialization, interaction, or negotiation, individuals develop common and shared frames in each department ([Ovaska _et al._ 2005](#ova05)). The subculture of each department is capable of influencing the sharing of a particular frame and impeding members' capacity to make sense of problems outside their department ([Barrett 1999](#bar99)). In view of this, we should consider that problems may be induced from the different cognitive frame of employees, derived from their belonging to different departments ([Davidson 2006](#dav06); [Ovaska _et al._ 2005](#ova05); [Barrett 1999](#bar99)).

Orlikowski and Gash ([1994](#orl94)) positioned the technological frames as a complementary perspective to those adopted in studies of power and politics; however, resolving incongruence to align frames implies that some groups' frames have to change or be changed. Davidson ([2002](#dav02)) observed that interpretative power is brought to bear when dominant frames form the basis for others' understanding of technology, for example, when prominent individuals such as executive champions, project leaders, or lead designers ([Heng _et al._ 1999](#hen99); [Newman and Sabherwal 1996](#new96)) take a prominent role in information technology-related ogranizational change programs. In these situations, information technology becomes difficult to isolate interpretative processes from power and political processes ([Markus and Bjorn-Andersen 1987](#mar87)). Therefore, the concept of technological frames forms a powerful and useful complement to other forms of social analyses, such as power, control and resource dependency ([Grudin 1989](#gru89); [Gash 1987](#gas87); [Kling and Iacono 1984](#kli84); [Markus 1983](#mar83)). In an overall investigation of technology implementation and use in organizations, both interpretative analysis of technological frames and institutional analyses of structural, cultural and power issues are valuable ([Orlikowski and Gash 1994](#orl94)).

In addition, Davidson ([2002](#dav02)) suggested that structural properties of frames such as rigidity and flexibility can influence the outcomes of interpretative processes during information technology change. Overly rigid frames could hinder recognition of important environmental cues ([Davidson 2006](#dav06)). Stabilization occurs within a relevant social group when members begin to talk and think about the technology in increasingly uniform and certain terms. This could lead to increased certainty among group members about the meaning, strategy and implications for practice of the technology ([Davidson 2006](#dav06)). However, extremely stable frames might contribute to escalation of commitment to a failing project ([Newman and Sabherwal 1996](#new96)) if contextual cues inconsistent with the frame are filtered out of consideration. Alternatively, unstable frames could contribute to reduction of commitment to a project ([Keil and Robey 1999](#kei99)) if group members are unable to maintain their focus on intended outcomes. In the light of this, changes that trigger a shift in salient technology frames could lead to reinterpretation of information as well as to new understandings of information system requirements ([Ovaska _et al._ 2005](#ova05); [Boland 2001](#bol01); [El Sawy and Pauchant 1988](#ei88); [Gioia 1986](#gio86)). Therefore, the management of information system adoption is a social and political process in which stakeholders frame and re-frame their perceptions of an information system ([Lin and Silva 2005](#lin05); [McLoughlin _et al._ 2000](#mcl00)).

While the concept of technological frames is rooted in social cognitive research, some studies have also drawn on the sociological literature that examines collective cognition and social constructions of technology ([Henderson 1991](#han91); [Bijker 1987](#bij87); [Bijker _et al._ 1987](#bij87)). From this perspective, technology frames are the understanding that members of a social group come to have of particular technological artefacts and they include not only knowledge about the particular technology, but also local understanding of specific uses in a given setting (such as a Chinese cultural organizational case; _KaoKang_ in this study). The meaning of a technology '_can only be described and information technologys significance appreciated in the context of IT's uses and IT's users_' (Bloomberg [1986](#blo96)). Therefore, the Chinese cultural contextual dimension is an area of this study which is emphasized in this paper as it is particularly significant.

Because frame content is always context-specific, investigating the power and culture foundations of technological frames might serve to provide a broader contextual basis for organizational studies of information system-related organization change ([Davidson 2006](#dav06); [Chiasson and Davidson 2005](#chi05)). information technology is necessary to use Chinese power theory to analyse a case that is set in a Chinese cultural context.

### Chinese power game: face and favour

A large number of studies have dealt with the topic of East and West philosophies. Francois ([2004](#fra04)) argued that the different ways Eastern and Western thinkers have dealt with diplomacy reflect important differences in how the two cultures understand human action in the world. For instance, whilst the Hellenic-Judaeo-Christian tradition of the West places great value on each human life, in the East, the life of the community, the corporation and the family are of greater importance ([Suen _et al._ 2007](#sue07)). Those from the East respect social harmony and consensus and perceive individual initiative and creativity to be of less importance than the willingness to merge one's personal identity in the life of the company or of the family. This is especially evident in Chinese societies ([Low 1996](#low96)).

In addition, Ho ([1993](#ho93)) pointed out that Chinese culture has a relational orientation, a concept that describes ordinary behaviour in Chinese culture. For this reason, the self is not an independent entity in Chinese culture and so the transfer of the individualism characteristic of Western culture to the Chinese relationalist culture will cause conflict. In Chinese culture, individuals' social behaviour depends on their social relationship ([Jacobs 1979](#jac79)). The Chinese tend to apply different rules of social exchange when dealing with people of different relations. Due to the deep impact of _guanxi_ (which, in the Chinese business world, is understood as the network of relationships among various parties who cooperate together and support one another) on Chinese culture, an individual's social relations are a very important source of power. One's power is determined by the extent of control exercised by an individual over resources and his or her particular relationship network. The more powerful network an individual has, the greater the advantages s/he will enjoy ([Hwang 1987](#hwa87)).

Hwang ([1987](#hwa87)) developed the conceptual framework of _face and favour theory_, based on social exchange theory, to fathom the dynamic relationships among these concepts. He divided interpersonal relationships into two parts:

1.  _Expressive component_: in order to satisfy affective feelings, one will manipulate others to procure some desired material resource from family, close friends and other congenial groups. They can also generate an individual's feelings of affection, warmth, safety and attachment.
2.  _Instrumental component_: human beings have strong desires to enlist help from outside resources; thus, they will try very hard to manipulate others to attain their personal goals.

These two components are always mixed.

Adopting this framework, _face and favour theory_ subdivides relationships further into three parts (three types of _guanxi_), represented by a dotted line and a solid line, to show the degrees of permeability between the psychological boundaries used by a Chinese:

1.  _Expressive ties_: this is generally a relatively permanent and stable social relationship within families. If people have family relationships (expressive ties), they will handle them with the _need rule_ to satisfy the request of the petitioner.
2.  _Mixed ties_: this kind of relationship, which has been termed a particularistic tie, occurs mainly among relatives, neighbors, classmates, colleagues, teachers and students who share a common area. If people have mixed ties, they will handle their relationship with the _renqing rule_ (which requires people (as the architects of network-building) to adhere to the reciprocal exchange of affective elements such as gifts and condolences). The members of the mixed tie relationship know each other and keep a certain expressive component in their relationship, but it is never so strong that all participants in this tie are able to express their authentic behaviour as freely as the members in the expressive tie.
3.  _Instrumental ties_: an individual must establish instrumental ties with other people outside his or her family to attain material goals. If people are strangers (instrumental tie), they will handle their relationship with the _equity rule_. It is easier for an instrumental tie relationship to become one of mixed tie through _la guanxi_ (seeking relation).

It is possible for both leaders and workers to be the _resource allocators_ and _petitioners_ in an organization. When an employee needs the support of his or her superior, the superior will judge their relationship on the basis of these three ties: (1) If they have family relationships (expressive tie), the superior will handle information technology with the _need rule_ to satisfy the request of the petitioner. (2) If they are strangers (instrumental tie), the superior will handle it with the _equity rule_. The superior will judge the employee's contribution first and then decide whether to accept or reject the request. (3) If they have mixed ties, the superior will handle it with the _renqing rule_. The superior will first judge the cost of the request for the petitioners and his or her payback to the company in the future. Then he/she will decide whether to accept or reject the petitioners' requests.

While agreeing that these three rules of behaviour are near universals, they fall short of capturing the full richness of the behavioural quandaries faced by participants in some cultures ([Hwang 1987](#hwa87)). According to The Five Cardinal Rules of Confucianism, between father and son, there should be affection; between sovereign and subordinate, righteousness; between husband and wife, attention to their separate functions; between elder brother and younger, a proper order; and between friends, friendship ([Hwang 2000](#hwa00)). Chinese societies emphasize hierarchical relationships ([Su and Littlefield 2001](#su01)), have a high power-distance score (Taiwan: 58; China: 80) and recognize that people are unequal; therefore, these inequalities grow over time into inequalities in power ([Hofstede 1991](#hof91)). In organizations, the level of power-distance is related to the degree of centralization of authority and the degree of autocratic leadership. This relationship shows that centralization and autocratic leadership are rooted in the _mental programming_ of the members of a society, not only of those in power, but also of those at the bottom of the power hierarchy ([Hofstede 1991](#hof91)). Societies in which power tends to be distributed unequally can remain so because this situation satisfies the psychological need for dependence of the people without power. Societies and organizations will be led as autocratically as their members will permit ([Hofstede 1991](#hof91)). For this reason, the traditional Chinese cherish hierarchical status in social relations; they tend to adopt multiple standards of behaviour for interacting with different individuals around them. Western society places extreme emphasis on _equity rules_, while Chinese society emphasizes the special needs of significant others ([Messick and Cook 1983](#mes83); [Greenberg and Cohen 1982](#gre82)). When the resource allocator is asked to mete out a social resource to the benefit of the petitioner, the potential allocator will first carefully consider: 'What is the _guanxi_ between us? How strong is our _guanxi_?'

In view of this, _guanxi_ in Chinese culture is based on factors that promote shared social experience between and among individuals ([Chiao 1982](#chi82); [King 1991](#kin91)). Moreover, the crucial difference is that these norms of reciprocity are much more socially situated than they are in the Western context ([Chung and Hamilton 2001](#chu01)). On the other hand, even in modern contexts, many Chinese have lived in closed communities that are hierarchically organized, with major economic and other resources controlled by a few power figures who could arbitrarily allocate resources ([Hwang 1987](#hwa87)).

Although Gouldner ([1960](#gou60)) has argued that the norm of reciprocity is universal, the chief differences among the three types of interpersonal ties in Chinese cultural society are their different repayment methods and the varying time periods permitted between giving and repaying ([Hwang 1987](#hwa87)). Some cross-cultural research has found that Chinese are more susceptible to the influence of powerful others than Americans ([Lao 1977](#lao77); [Huang 1974](#hua74)). The sharp contrast between Chinese and American society is revealed in their responses to situations in which they are subjected to social demands from group pressure or power figures ([Chu 1979](#chu79)). In the light of the above findings, it is necessary to pay attention to both cultural universals and culturally specific patterns of social interaction.

Implied in the _renqing rule_ is the maintenance of interpersonal harmony within a group, when two or more individuals are equal. It seems that maintaining group harmony and integrity is much more important to a Chinese than is insisting on distributive equity. In order to maintain the affective component in the mixed tie, the participants have to remember the principle that _etiquette requires reciprocity_. To sum up, what motivates the Chinese to offer _renqing_ to another is their anticipation of repayment. It is largely owing to this anticipation of reciprocity that the resource allocator is willing to offer a petitioner _renqing_.

Hwang ([1987](#hwa87)) asserted that an individual's awareness of his or her public image formed in others' minds is called _face_. When an individual is interacting with other parties in the network, all members of the in-group may evaluate his or her _face_ in terms of either performance or morality to _maintain interpersonal harmony_. By the same token, when an individual represents his or her group in interaction with members of another group, others may also evaluate their _face_. This principle finds expression in such Chinese proverbial phrases as: _winning glory for one's fatherland_.

## Research method

### Case selection and data collection

_KaoKang_, a case set in a Chinese cultural context, has 2,000 employees and has had a management information system department for more than twenty years. It is information-rich and has had many occurrences of power-wielding events in the context of the information system. This information system was selected because of the presence in the organization of five kinds of actor who had personal, incongruent assumptions, expectations and knowledge of the system's development. At the same time, their values and belief systems were deeply influenced by the Chinese culture. The snowball sampling method was used to select the key individuals involved with the system in the organization as our subjects.

To understand the cognitive differences of the five kinds of actor (top managers, employees of the Berthing Division (known as DP-1), management information system employees, consultants of _KaoKang_ and customers), and the way they used their resources to wield power in system development processes, we had two interviewing phases. The first phase from December 2001 to December 2004 explored the detailed processes of antecedence, process and failure outcomes of the system. The second phase from January 2005 to December 2008 sought to understand the development processes of the second attempt to develop the quay management information system. A total of twenty-five subjects at _KaoKang_ were interviewed, with the duration of each interview ranging from fifteen minutes to four hours, with the typical interview lasting between thirty minutes and one hour. The selected case subjects are shown in Table 1.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Selected subjects**  
</caption>

<tbody>

<tr>

<th>Kinds of actor</th>

<th>_KaoKang_</th>

<th>Departments' responsibilities</th>

<th>Selected subjects</th>

</tr>

<tr>

<th>Top managers</th>

<td>Data processing department  
(harbour management department)</td>

<td>Management of the harbour facilities, ship quay arrangements, maintenance and control of ships in harbour</td>

<td align="center">2</td>

</tr>

<tr>

<th>Employees of Berthing Division</th>

<td>Berthing division (DP-1)</td>

<td>Ship quay arrangement</td>

<td align="center">4</td>

</tr>

<tr>

<th rowspan="2" valign="middle">Management information system professionals</th>

<td>Management information system department</td>

<td>Planning, design, implementation of the harbour information systems, management hardware and harbour network</td>

<td align="center">6</td>

</tr>

<tr>

<td>Secretariat department (DSE) (which worked for the management information system department)</td>

<td>Public relations and administrative works of harbour</td>

<td align="center">2</td>

</tr>

<tr>

<th rowspan="2" valign="middle">Consultants of _KaoKang_</th>

<td>Berthing division</td>

<td>Ship quay arrangement</td>

<td align="center">1</td>

</tr>

<tr>

<td>Stevedoring & warehousing department (DW)</td>

<td>Harbour dredging, design, maintenance, etc.</td>

<td align="center">2</td>

</tr>

<tr>

<th>Customers</th>

<td>Customer</td>

<td>Shipping companies</td>

<td align="center">5</td>

</tr>

<tr>

<th rowspan="2" valign="top">Others</th>

<td>Research and Development department (DRD)</td>

<td>Research and development of harbour</td>

<td align="center">1</td>

</tr>

<tr>

<td>Secretariat department (DSE)</td>

<td>Public relations and administrative works of harbour</td>

<td align="center">2</td>

</tr>

<tr>

<th colspan="3">Total</th>

<td align="center">25</td>

</tr>

</tbody>

</table>

The data were collected from five different departments and five customers. The taped interviews were transcribed verbatim into text files. Any questions or incomplete information emerging during this stage were dealt with in a second contact either by e-mail or telephone. The following represents a sample of the questions that guided the interview process:

1.  Why was the quay management information system initiated? At what time? Please describe the development processes.
2.  Does this system need the cooperation of a number of departments (divisions)? Did it encounter any problems in the developmental process? Why or why not?
3.  Does the system influence employees' work? Why or why not?
4.  Do those employees' feel anxious about the system (for example, anxiety about job security, etc)? Why or why not?
5.  Do they use their power to resist the system? If so, why and in what ways do they do that? Do you think those reasons and their behaviour are related to Chinese culture? Why or why not?
6.  How successfully does system function now?

### Data analysis

This study adopted an interpretative approach to data analysis, focusing on the aspects of _technological frames_ and _Chinese power games_. Data collected from the field were first read to gain background knowledge of _KaoKang_ and of the quay management information system that was generated and then interpretations were found through continual reading and re-examination of the materials. A large number of events were analysed utilizing an interpretation method. Investigations were conducted into the cognition and action of these different actors and their wielded power processes in information system design. During the data analysis, we returned to the interviewees for further information or clarification of the statements made in the initial interviews either by arranging another interview or by phone call. Sometimes, follow-up interviews or phone calls were made to clarify ambiguities revealed through our initial interpretation of the data.

This study analysed data using thematic units elucidated from the respondents' description of the relevant events of the actors' cognition and power in the information system. Upon selection of the relevant sentences, each theme was analysed, a task achieved by the collecting of themes and their codification. Since coding is subjective, it was necessary that all coders should agree on the coding criteria ([Pare 2004](#par04)). The events were ranked based on a time axis and the data related to actors' cognition and power were filtered and analysed. The correspondents either confirmed or rejected our interpretations, thus providing us with scope for further elaboration of these issues.

Each of the three analysts (two Ph.D. candidates and one management information system professor) analysed these data independently, after which pictures and a matrix were used to present and discuss the results. The final results were accepted when two or more analysts agreed. Those sections that could not be agreed upon in this way were examined in greater detail by the professor. If agreement still could not be reached, the findings of that particular section were discarded. By means of this procedure, the themes selected and the quality of the analysis unit satisfied the requirements of triangulation through multiple analyses ([Klein and Myers 1999](#kle99)). Continuous engagement was pursued through a dialogue with the materials in order to re-examine and revise emergent arguments and assumptions ([Introna 1997](#int97)).

Two examples of the analysis undertaken are provided in the following examples:

<table width="95%" border="1" cellspacing="0" cellpadding="5" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th>Example 1:</th>

<th>Example 2:</th>

</tr>

<tr>

<td>One of the customers stated: The greatest worry for us is _whether DHS-IS is fair and transparent_ with respect to quay arranging for ships. it became _easier to use DHS-IS as a black box to satisfy the requests of privileged clients to have priority with regard to the arranging of good quays for our ships_.  
(Interpretation: The **ship quay arrangement power of _KaoKang_** [**resource allocator** role] was **stronger than customers' power** [**petitioner** role], therefore, **customers had to maintain good _guanxi_** and **interpersonal harmony with _KaoKang_** [on the assumption that they had "**mixed tie**" **_guanxi_** with _KaoKang_], to ensure that their ships would gain access to good quays).</td>

<td>A Berthing Division employee complained: In this affair, _we had the responsibility of checking and signing, making ourselves powerful_. Although _some senior employees of DP-1 division could create problems in their attempts to protect their position, power and self-interests_, it was still necessary _for us to obey top managers' direction and be seen to accommodate the DHS-IS_.  
(Interpretation: the **power position of the employees of the Berthing Division** [**petitioner** role] was **weaker than that of top managers** [**resource allocator** role]. Therefore, they supported DHS information system only superficially, were compelled to **maintain _guanxi_ with top managers** and sought to **enhance their own image to protect their current position** [on the assumption that they had "**mixed tie**" **_guanxi_** with top managers]).</td>

</tr>

</tbody>

</table>

Finally, the data were interwoven with the technological frames and Chinese culture to interpret the power manipulations in the system, which produced a number of exciting new insights and provided a more in-depth understanding of the organizational power in the information system development processes operated in the case of _KaoKang_ and its Chinese cultural background.

## Case description

_KaoKang_, a port authority in Taiwan, was established in 1945\. Employees are government officials and _KaoKang_ has a bureaucratic culture. In the 1960s, the industry, business and economy were thriving and _KaoKang_'s operations increased significantly. Computerization was introduced to some aspects of the business in 1971 and gradually increased, leading to the establishment of an MIS department in 1974\. The structure of the hierarchy of _KaoKang_ is depicted below in Figure 1.

<div align="center">![Figure 1: Hierarchical structure of KaoKang](p437fig1.jpg)</div>

<div align="center">  
**Figure 1: Hierarchical structure of _KaoKang_**</div>

Simon, the new director of _KaoKang_, inspected each department in 1997\. He found that all but the very noisy Berthing Division had been computerized. He then assigned the management information department to the task of computerizing ship quay arrangement processes, called the DHS-IS. Because Simon had bragged about _KaoKang_ having an information system capable of arranging ships' berths when entering foreign ports, it was necessary to computerize this task. Ship arrangement processes were extremely complicated and employees of the division had huge resistance to the changes. One employee recalled:

> With respect to the task of ship quay arrangement, we had the responsibility of checking and signing. This rendered our position powerful. You could sense an atmosphere; some senior employees of DP-1 division would create problems for the MIS professional who did not know the ins-and-outs of the ship quay arrangement processes.

Furthermore, the management information department had no time to develop the system. One of the departmental staff stated:

> We were even expected to help develop the DHS-IS for the DP-1 division, but we had no intention of doing it. There were many systems we needed to develop at that time and so the DHS information system could wait.

Then, _KaoKang_ outsourced the information system. In the management information department, the responsibility for system had been transferred from Paul to Hugo, a less responsible individual than Paul. The leader of DM-2 division stated:

> The sister of Hugo was a senior employee in the human resources department. Therefore, Hugo had better _guanxi_ with the top managers than me and he could work in his own way; even my authority could not stop his irresponsible behaviour.

Hugo used the strategy of _the less he did, the fewer errors he would make_ in relation to the information system and, as a result, the system had no chance of success because Hugo did not do anything.

Meanwhile, customers of _KaoKang_ were afraid that they would lose their jobs if the information system succeeded. One of them stated:

> Our bosses were misled by their employees and believed that these employees were necessary and could not be fired. They were related to their bosses, so the bosses could not pressure their employees to use the DHS-IS.

Six years later, in 2002, James, the leader of the DM-2 division, wrote a Web home page displaying the results of the ship arrangement up to 2004, accessible to anyone with Internet facilities. The first quay management information system development process was a total failure.

From then on, the global competitive advantage of _KaoKang_ diminished rapidly each year, causing the organization to lose its ranking from third to eighth in the world. In 2004, the director heard that even LongKang and ChungKang in Taiwan had information systems capable of arranging a berth for a ship when entering a port. As _KaoKang_ could not lag behind these two smaller ports, it began planning the quay management information system once again in 2005, expecting it to be more successful the second time around. Meanwhile, the director assigned some employees with deep understanding of ship arrangement processes as the consultants for the system. At this stage, the director emphasized the importance of the modified manually function that should not be deleted; otherwise they would not be able to respond to privileged clients' requests for good berths to be assigned to their ships.

Employees of the Berthing Division followed top managers' directives and consultants' suggestions to modify and simplify the complicated rules and passed these modifications on to the management information department to write the proposal request for the information system. They also co-opted the manager of the department, expecting him to share the responsibility. The management information professionals supported the technical, outsourcing to a third-party and supported a contract for the Berthing Division, but neither became involved in other things, nor made any suggestions.

The biggest concern of the customers with respect to the system was the danger that it might not be fair and transparent in terms of ship quay arrangements. They emphasized that if _KaoKang_ guaranteed that the system could achieve the two goals of fairness and transparency, they would accommodate it whole-heartedly. One customer stated:

> The day of the second meeting, many customers came to _KaoKang_. Yes, we were fault-finding; it was our job to be fault-finding. We were, in fact, waiting for their failure.

Consultants for the system asserted that making permanent the function of manual modification never happened in the new system. This became a loophole that interfered with _renqing_. A consultant recalled:

> The mechanism by which individuals could work and the conditions under which they worked must be very clear. Now, everyone was unclear about this critical issue and the new DHS-IS remained the same as the old one. Finally, we had to handle all of the problems. The most difficult thing that happened was the issuing of wrong directives from top managers, which led to our being misdirected. They only paid lip service.

Despite this action, by the end of 2008, the second round of system development had still not been successful. One consultant stated:

> You see, from 1997 to 2008, even though so many resources and so much time had been consumed in the DHS-IS, it still did not work.

## Data analysis and discussion

The study analysed the technological frame differences among five kinds of actor at _KaoKang_: (1) top manager: including director and DP department manager; (2) employees of the Berthing Division: including the leader of the division and its employees; (3) management information professionals: including the manager of the department and the staff concerned with the information system; (4) consultants of _KaoKang_: those employees of the Berthing Division and other departments with deep understanding of the ship arrangement processes; (5) customers (outside users): those who needed _KaoKang_ to arrange ships on the quay for them. This study then analysed the different cognition, conflicts and contradictions among the five kinds of actor in the system development processes. The first stage of the process (1997-2004) was explained and the reasons for their negative effect on the system in the second stage (2005-2008) were given.

During the analysis, researchers observed four categories of technological frames that could be used to explain the attitudes, assumptions, expectations and knowledge affecting the understanding of the two-staged system development process. The identified categories were:

1.  Motivation adoption: based on the unequal power in Chinese cultural society, the assumptions about the information system of the five actors are incongruent. This leads to different levels of willingness to use the system among each of the five actors.
2.  Strategies and goals relating to the system: the five actors necessarily wield their position or expertise, based on the Chinese cultural characteristics maintaining interpersonal harmony, keeping the _face_ of powerful actors and having good _guanxi_ with others, to deal with conflicting expectations and perceptions of the system and to achieve their own expectations of and goals for the system.
3.  Ship-quay (berth) arrangement solutions: irrespective of the perception of the system of the five actors, having demonstrated different assumptions and expectations of the ship-quay arrangement solutions, the actors must comply to the Chinese social harmony and _guanxi_ culture to achieve their respective goals.
4.  Control of the functions of the information system: because of the inequality among the five actors with respect to their respective positions and levels of expertise, the actors have conflicting expectations and understanding of the system. This conflict extends not only to the ways of controlling the functions of the system to achieve their personal goals, but also to the behaviour necessary not violate the Chinese social harmony and _guanxi_ culture.

As the director of _KaoKang_ did not assign the consultants in the first stage of the quay management information system (1997-2004), there were only four kinds of actor at this stage (top managers, employees of the Berthing Division, management information professionals and customers). However, in the second stage (2005 onwards), the director of _KaoKang_ assigned consultants to _KaoKang_. it was found that different actors had different perceptions of technological frames at the same stage, which will be discussed in the following sections.

### The different technological frames of four kinds of actor in the first stage (1997-2004)

These frames can be seen in Table 2 below.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Summary of the technological frames for each actor in the first stage**  
</caption>

<tbody>

<tr>

<th>Issues/actors</th>

<th>Motivation adoption</th>

<th>Strategies and goals relating to the quay management information system</th>

<th>Ship quay arrangement solutions</th>

<th>Control of the functions of the system</th>

</tr>

<tr>

<th rowspan="2" valign="middle">Top managers</th>

<td valign="top">Director told foreign port that _KaoKang_ has the quay management information system.</td>

<td valign="top">For the global competitive advantage of _KaoKang_ and to fully support the budget of the system.</td>

<td valign="top">Accepted customers delegated some powerful individuals to resolve their problems.</td>

<td valign="top">Unconcerned about how the system was produced.</td>

</tr>

<tr>

<td colspan="4">The system was related to the "**face**" of the director [**petitioner** role]. _La guanxi_ with powerful individuals [**resource allocator** role] was sought to **enhance their power**.</td>

</tr>

<tr>

<th rowspan="2" valign="middle">Employees of the Berthing Division</th>

<td valign="top">They had to comply with top managers' demands.</td>

<td valign="top">They used a hit-or-miss and make-shift approach to computerization, hoping the system would ultimately fail.</td>

<td valign="top">They doubted the ability of the system, being certain that the job was too complicated.</td>

<td valign="top">The system was capable of threatening their power, self-interests and jobs because critical information controllability would be transparent after computerization.</td>

</tr>

<tr>

<td colspan="4">They [**petitioner** role] used **_la guanxi_** and **maintenance of interpersonal harmony** with top managers [**resource allocator** role] to **improve their existent power** [on the assumption that they would have "**mixed tie**" **_guanxi_** with top managers].</td>

</tr>

<tr>

<th rowspan="2" valign="middle">Management information Professionals</th>

<td valign="top">Did not want to take responsibility for the system.</td>

<td valign="top">Hugo used the strategy of "the less he did, the fewer errors he would make" to introduce the system.</td>

<td valign="top">It had not come to maturity at that time (1997). Meanwhile, the system had to be associated with a geographical information system.</td>

<td valign="top">Used a web homepage showing the ship quay results only.</td>

</tr>

<tr>

<td colspan="4">They [**petitioner** role] used **maintain interpersonal harmony** with top managers [**resource allocator** role] and employees of the Berthing Division [on the assumption that they would have "**mixed tie**" **_guanxi_** with top managers].</td>

</tr>

<tr>

<th rowspan="2" valign="middle">Customers</th>

<td valign="top">Resisted the information system totally.</td>

<td valign="top">Requested some privileges, pressing _KaoKang_ and resisting the system.</td>

<td valign="top">The system could not modify the function to accommodate day-to-day problems. For this reason, they distrusted the system.</td>

<td valign="top">The system was threatening their job security.</td>

</tr>

<tr>

<td colspan="4">They [**petitioner** role] used **_la guanxi_** with top managers and employees of the Berthing Division [**resource allocator** role] for their ships to obtain good quays and to show that **they were more powerful** than other customers [on the assumption that they would have "**mixed tie**" **_guanxi_** with _KaoKang_].</td>

</tr>

</tbody>

</table>

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Differences in the perceptions of the DHS-information system among the four actors in the first stage**  
</caption>

<tbody>

<tr>

<th>Actors</th>

<th>Data</th>

<th>Interpretation</th>

</tr>

<tr>

<th>Top managers</th>

<td>"The greatest worry for top managers was customers' petitions and complaints to powerful and privileged people, which might affect their chances for future promotion. They also worried that the director had told foreign ports that _KaoKang_ had the quay management information system already."</td>

<td>A manager [**resource allocator** role] stated that the system must be developed to save the director's **_face_** and they had the responsibility and **power** to do it.</td>

</tr>

<tr>

<th>Employees of the Berthing Division</th>

<td>"In the task of quay ship arrangement, many conditions needed to be taken into consideration, which could not all be controlled by the DHS-IS."  
"Some senior employees of DP-1 division created problems to protect their position, power and self-interests, ignoring the need to carry out top managers' directives and to accommodate the transition to the DHS-IS. For the sake of the face of the director and our own future, it was important that we did so."</td>

<td>The system indeed affected their current power, job security and self-interests. However, their **position of power** [**petitioner** role] **was weaker than that of top managers** [**resource allocator** role]. Therefore, they supported the system only superficially. For this reason, they **maintained good _guanxi_** with top managers [on the assumption they would have _**mixed tie**_ **_guanxi_** with top managers] and enhanced their own image to protect their current position.</td>

</tr>

<tr>

<th>Management information Professionals</th>

<td>"At the time, it was not mature enough and the Internet was only just beginning to be developed. The bandwidth was not adequate to run the system. On the one hand, we had to carry out the director's directives. On the other hand, we did not push the employees of DP-1 division to develop the system. No matter what, we needed their assistance in many ways, therefore, it was a good approach to maintain good _guanxi_ and harmony with them."</td>

<td>They did not force the employees of the Berthing Division [**resource allocator** role] to develop the system because their **expertise in arranging ship quays was weaker than that of the employees of the division** and also because they needed their help in the system development processes. Therefore, they needed to **maintain interpersonal harmony** with top managers and employees of the division to enhance their own image [on the assumption that they would have "**mixed tie**" **_guanxi_** with them].</td>

</tr>

<tr>

<th>Customers</th>

<td>"Most of us were small companies and we did not want to pay money to buy a computer to run the DHS-IS. Some employees told their bosses that they would not use a computer if their bosses bought one."  
"We had to have good _guanxi_ with the employees of DP-1 division and maintain interpersonal harmony as far as possible because they controlled the task of ship arrangement."</td>

<td>They [**petitioner** role] would lose their jobs if the system was successful. Therefore, they used their good _guanxi_ with their bosses [**expressive tie**]. At the same time, they **used their _guanxi_** with top managers and employees of the Berthing Division to obtain good quays for their ships because _KaoKang_'s p**ower in arranging ship quays** [**resource allocator** role] **was stronger than that of the customers**.</td>

</tr>

</tbody>

</table>

#### Motivation adoption

With the exception of top managers, the actors believed that the quay management information system was an impossible mission. However, how these three types of actor made sense of the system were totally different.

First, the layman's level of computer knowledge of top managers was perceived as the reason why they had positive attitudes towards the system. Secondly, only management information professionals were able to assess the system from a reasonable perspective and it was clear to them that it did not have the capacity to be as flexible as the original method. Thirdly, both employees of the Berthing Division and customers resisted the system out of fear that it would influence their job security, power and self-interests. Thus, they had an emotional reaction and asserted that the system was an unsuitable instrument for carrying out the task of ship arrangement.

As top managers had the _resource allocator_ role, their power was greater than that of the employees. As a consequence, they regarded their employees as _instrumental ties_ and applied the equity rule in their dealing with them. Seen in this light, the computerization of the ship arrangement processes was an objective decision. On the other hand, in view of the fact that management information professionals, employees of the Berthing Division and customers occupied different positions, they had the _petitioner's role_ as their power was weaker than that of top managers. As a consequence, they had to comply with top managers' directives to enhance the image of the organization. Thus, they were compelled to use _la guanxi_ with top managers, expecting those managers to regard them as _mixed ties_ and to apply the _renqing rule_ in the the information system development processes.

#### Strategies and goals relating to the the quay management information system

The director expected the system to increase the global competitive advantage of _KaoKang_. Although employees of the Berthing Division had expectations of and goals for, the system, which differed from their superiors, they were the subordinates to top managers and had ostensibly to carry out the directives of their superiors ([Redding 1990](#red90), [1980](#red80); [Tricker 1988](#tri88)) to enhance their personal images in the eyes of the top managers. For this reason, these employees (petitioners), whose power was weaker than that of top managers, sought _la guanxi_ with superiors (resource allocator), to move them from _instrumental tie_ to _mixed tie_ and applied the _renqing rule_ when interacting with their superiors to maintain interpersonal harmony.

In contrast, only employees of the Berthing Division (resource allocator) had the knowledge to arrange ship quays, their power derived from expertise being greater than that of the management information professionals. Therefore, they did not share with those professionals (petitioner) the secrets of ship-quay arranging in the the system development processes in order to protect their current position, power and interests. Meanwhile, in the management informaiton department, the responsibility for the system had been transferred from Paul to Hugo, a less responsible individual.

As a result, the system had no chance of success. However, to save the _face_ of top managers (resource allocator), employees of the Berthing Division and management information professionals (petitioners), their power being weaker than that of top managers, ostensibly accommodated superiors' directives and made the results of ship arrangement accessible to everyone. This made it seem as if the information system would not fail.

Customers had formed resistance to the system, requesting privileges, or contesting the system. Thus, customer employees reported to their bosses that the system really was not usable and their bosses believed that these employees were indispensable and could not be fired. In view of the fact that most of the customers were relatives of their bosses, it was very difficult for the bosses (resource allocator) to pressure their employees (petitioner) to use the system. This was because those bosses had regarded them as being _expressive ties_ and had applied the _need rule_ to satisfy unreservedly the request of their employees, in accord with the requirements of Chinese cultural society. Moreover, customers requested a number of privileges, pressing _KaoKang_ and asking the organization's top managers to accommodate their requests, thus enhancing the powerful image of the customers. As a result, top managers were forced to make concessions to the customers and to retain the original ship arrangement processes. it was essential for top managers to seek good _guanxi_ with those powerful individuals in order to retain their opportunities for further promotion in _KaoKang_. This is a specific phenomenon in Chinese cultural society, as illustrated by the Chinese proverb: 'if you have _guanxi_ (relation), then everything is all right; but if you have no _guanxi_ (relation), then everything is not all right'.

Employees at the operational end of the business needed to seek good _guanxi_ with the top managers of _KaoKang_ to increase their chances of promotion. it was also important, for the same reason, for top managers to seek good _guanxi_ with other powerful individuals.

#### Ship quay arrangement solutions

With the exception of top managers, the other three actors believed the information system had no chance of success with respect to ship quay arrangements because of the complexity of the task and the rigidity of the system itself. Employees of the Berthing Division doubted the capability of the system because the job was too complicated. Management information professionals asserted that the system had to be associated with a geographic information system and that had not come to maturity at the time (1997). Customers distrusted the system totally as it could not be used to address day-to-day problems.

#### Control of the functions of the system

As management information professionals believed that a successfully working system was an impossible mission, they accommodated it only passively and refused to coerce employees of the Berthing Division and customers to use it, so as to apply the _renqing rule_ to them and maintain interpersonal harmony. This would lead to future reciprocation in kind from customers to the professionals. This is a characteristic type of behaviour in Chinese culture and of extreme importance for harmonious relations. For this reason, management information professionals in _KaoKang_ considered their _guanxi_ with the division as person-in-relation. For them to coerce employees of the division and customers to use the system would be unwise, as such behaviour would break their harmonious relations.

Both Berthing Division employees and customers were afraid that the system would threaten their jobs, power and self-interests; therefore, they resisted it to the point of its ultimate failure as a system. The relations among employees of the division and customers were very complex. The employees of the division were the _resource allocator_ when they arranged quays for ships. They regarded their ship arrangement knowledge as a personal asset rather than an organizational resource. In the light of this, customers played the petitioner role and therefore, sought good _guanxi_ and exchanged benefits with employees of the Berthing Division as best they could in order to secure a good quay for their ships. In brief, the employees of the division joined customer resistance to the system.

Generally, the first stage of the system failed as a result of these circumstances and nobody took responsibility for the failure.

### The different technological frames of five kinds of actor in the second stage (2005 onwards)

These frames can be seen in table 4 below.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Summary of the technological frames for each actor in the second stage**  
</caption>

<tbody>

<tr>

<th>Issues/actors</th>

<th>Motivation adoption</th>

<th>Strategies and goals relating to the information system</th>

<th>Ship quay arrangement solutions</th>

<th>Control of the functions of the system</th>

</tr>

<tr>

<th rowspan="2" valign="top">Top managers</th>

<td valign="top">LongKang and ChungKang in Taiwan had the information system.</td>

<td valign="top">Assignment of the manager of DP department to pilot the proceedings and totally support the budget for the system.</td>

<td valign="top">The system could help top managers maintain good _guanxi_ with privileged clients.</td>

<td valign="top">Asserted that the system should retain its manual function.</td>

</tr>

<tr>

<td colspan="4">The system was related to the "face" of the director. Top managers [**petitioner** role] could maintain good _guanxi_ with privileges [**resource allocator** role] to **enhance their power** [on the assumption that they would have "**mixed tie**" **_guanxi_** with privileges].</td>

</tr>

<tr>

<th rowspan="2" valign="top">Employees of Berthing Division</th>

<td valign="top">There was a global computerization trend. The manager of the management information department was expected to share the responsibility with them</td>

<td valign="top">Simplified the complicated rules and avoided involvement in customer conflict in order to protect themselves.</td>

<td valign="top">Computerization was possible at this time.</td>

<td valign="top">Wrote request for proposal according to top managers' requirements.</td>

</tr>

<tr>

<td colspan="4">They used **_guanxi_** with the manager of the management information department and customers and "**maintained interpersonal harmony**" with them and top managers to **improve their existing power** [on the assumption that they would have "**mixed ties**" **_guanxi_** with the management information department, customers and top managers].</td>

</tr>

<tr>

<th rowspan="2" valign="top">Management information professionals</th>

<td valign="top">Avoided responsibility for the system's failure.</td>

<td valign="top">From promoter to consultant role.</td>

<td valign="top">The system should achieve its goal.</td>

<td valign="top">Supported the technical side of the project only.</td>

</tr>

<tr>

<td colspan="4">They [**petitioner** role] complied with top managers' [**resource allocator/strong> role] demands, sought **_la guanxi_** and **applied the _renqing rule_** to them [on the ssumption that they would have "**mixed tie**" **_guanxi_** with top managers].**</td>

</tr>

<tr>

<th rowspan="2" valign="top">Customers</th>

<td valign="top">Conditionally accepted the system.</td>

<td valign="top">Raised as many questions as they could without explicitly standing against the system.</td>

<td valign="top">Waited for the second failure of the system.</td>

<td valign="top">_KaoKang_ would have to resolve these flaws first before discussing the system.</td>

</tr>

<tr>

<td colspan="4">They [**petitioner** role] used **_guanxi_** with top managers and employees of the Berthing Division, utilizing powerful individuals and **maintenance of interpersonal harmony** with them in order for their ships to have good quays and to show that **they were more powerful** than other customers [on the assumption that they would have "**mixed tie**" **_guanxi_** with _KaoKang_].</td>

</tr>

<tr>

<th rowspan="2" valign="top">Consultants to _KaoKang_</th>

<td valign="top">This was a good chance to simplify the complicated rules of ship arrangement.</td>

<td valign="top">Consultants to the DP-1 division revised the request for proposals and persuaded top mangers in each meeting.</td>

<td valign="top">Computerization could arrange the quay for a ship.</td>

<td valign="top">The function of manual modification should never be replaced by the new the information system.</td>

</tr>

<tr>

<td colspan="4">They [**petitioner** role] had to respect top managers' [**resource allocator** role] decision to **maintain interpersonal harmony**</td>

</tr>

</tbody>

</table>

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Differences in the perception of the the quay management information system among the five actors in the second stage**  
</caption>

<tbody>

<tr>

<th>Actors</th>

<th>Data</th>

<th>Interpretation</th>

</tr>

<tr>

<th valign="top">Top managers</th>

<td valign="top">"I was so worried that if the DHS-IS could not retain the manual function, I would not be able to respond to the request for privileges from some clients. How could I face them if I lost my control of the the DHS-IS?"</td>

<td valign="top">It was asserted that the system should retain its manual function for them to be able to maintain good _guanxi_ with privileges because the **director** [**resource allocator** role] **was the most powerful person** in _KaoKang_.</td>

</tr>

<tr>

<th valign="top">Employees of the Berthing Division</th>

<td valign="top">"We accommodated the request of top managers, simplified the rules of the task, wrote RFP and made no suggestions because we needed to maintain interpersonal harmony with them. But we avoided involvement in customers' conflicts."</td>

<td valign="top">They believed the system had the possibility of success at this time and so simplified rules and accommodated top managers' requests. At the same time, they did not want involvement in customers' conflicts to maintain interpersonal harmony with them as **they were afraid some powerful customers would use privileges** [moving **from "resource allocator" to "petitioner"** role in this situation] to influence their position in _KaoKang_.</td>

</tr>

<tr>

<th valign="top">Management information professionals</th>

<td valign="top">"I supported the technical outsourcing of the DHS-IS to a third-party and authorized a contract for DP-1 division only; nothing more. This was the smallest risk which I could take. Meanwhile, top managers maintained their image with the management information systems department."</td>

<td valign="top">They [**petitioner** role] were afraid of the system failure once more and so changed their role from promoter to consultant. Therefore, they would be able to preserve their **good image** with top managers [**resource allocator** role].</td>

</tr>

<tr>

<th valign="top">Customers</th>

<td valign="top">"_KaoKang_ needed to replace its make-shift methods to eradicate the flaws in the ship arrangement processes."  
"The greatest worry for us was whether the DHS-IS was fair and transparent with respect to quay arranging for ships. it became easier to use the DHS-IS as a black box to satisfy the requests of privileged clients to have priority with regard to the arranging of good quays for our ships. Therefore, we had to maintain good _guanxi_ and interpersonal harmony with the employees of DP-1 division and the top manager of _KaoKang_ to ensure our company's ships had good quays in the future."</td>

<td valign="top">On the one hand, they did not believe in _KaoKang_ due to the many flaws in their ship-arranging function. On the other hand, they had to **maintain good _guanxi_** and **interpersonal harmony** with _KaoKang_, to ensure that their ships would get access to good quays because _KaoKang_'s **power to arrange ships' quays was stronger than theirs**</td>

</tr>

<tr>

<th valign="top">Consultants to _KaoKang_</th>

<td valign="top">"The most important thing is 'public interest,' fairness, equity and transparency, am I right? Public rights are also important. The most dangerous thing that could happen would be the top managers giving the wrong directive as the result of our misguidance."  
"Top managers always ask my opinion. They cannot avoid the "_renqing_" from some powerful and privileged persons, therefore, they want to retain the manual function."</td>

<td valign="top">The quay management information system had a chance of success. However, if top managers did not support it, success would be threatened as they knew that there were many flaws in the ship quay arrangement function.</td>

</tr>

</tbody>

</table>

The director anticipated another, more advanced information system, in order to restore the global competitive advantage of _KaoKang_. To this end, the building of a new one was planned in 2004.

#### Motivation adoption

The quay management information system was able to save the _face_ of the director by providing _LongKang_ and _ChungKang_ with the same facility. Because of technological developments, four kinds of actor at _KaoKang_ ameliorated the system policies before building and had confidence to do it. However, customers (petitioner) still doubted that the system could deal with these issues on the basis of fairness and transparency. However, they did not reject _KaoKang_'s proposal in order to maintain interpersonal harmony. As a result, the power of ship arrangement processes was still controlled by _KaoKang_ (resource allocator).

At the same time, the employees of the Berthing Division necessarily sought good _guanxi_ with the management information department manager, leading to his being co-opted, with the expectation that he would share the responsibility with employees of the division. However, both employees of the division and management information professionals could not guarantee the success of the system, although it could work theoretically. For this reason, they would not take the responsibility for the system. Therefore, the employees of the division needed _la guanxi_ with the manager of the management information department and customers and to maintain interpersonal harmony with them because in Chinese cultural society, this is necessary and polite behaviour.

#### Strategies and goals relating to the system

With the exception of top managers (resource allocator), who totally supported the quay management information system, consultants for _KaoKang_ proposed a new view of the ship arrangement function, fully supporting the building of a new the system and consultantcy for the DP-1 division. Consultants also recognized that this was a good opportunity to avoid interfering with _renqing_ and to achieve the two goals of fairness and transparency and preservation of public rights. In contrast, the other three actors (petitioner) expressed no enthusiasm for the system and provided minimal effort with respect to the computerization of it. Management information professionals shifted their role from promoter to consultant at this stage.

Because of the customers' distrust, _KaoKang_ had to resolve the long-standing flaws in the system. Unless _KaoKang_ could resolve these flaws first, the information system could not be discussed and it would fail once again. For this reason, customers (petitioner) participated in each system meeting, recognizing the need to retain good _guanxi_ with _KaoKang_ (resource allocator) and to help them to save _face_ by not disagreeing with the project. In this way, they ensured that their business would not be affected by _KaoKang_. In addition, customers (petitioner) still needed to have good _guanxi_ with the employees of the Berthing Division (resource allocator) in order to secure a good quay for their ships, to enhance their power and to benefit from other privileges.

#### Ship quay arrangement solutions

Neither employees of the Berthing Division nor management information professionals could guarantee the success of the DHS system, although it could work theoretically. However, as the manager of the management information department (petitioner) still needed to comply with the directives (resource allocator) coming from senior management, he sought good _guanxi_ with them to enhance his image. At the same time, customers (petitioner) did not explicitly stand against the system in order to retain good _guanxi_ and maintain interpersonal harmony with top managers and employees of the division (resource allocator) for their future work. Finally, consultants alone recognized that computerization could arrange the quays for ships and so supported the system whole-heartedly.

#### Control of the functions of the system

Top managers insisted that the system should retain the manual function, so that they could respond to the requests for privileges from particular clients, who would, in return, help them gain promotion in the future. In this situation, top managers (resource allocator) applied the _renqing rule_ to powerful _petitioners_ and gave them special concessions (_la guanxi_, seeking relation). In the _renqing rule_ in Chinese culture, people offer _renqing_ to another in their anticipation of repayment. For this reason, they would not accept the suggestions of consultants to eradicate the flaws in the ship arrangement processes. In this way, the image of top managers would be enhanced in the eyes of privileged clients, rendering the clients willing to repay the managers' actions by helping them achieve their promotion aspirations.

The employees of both the Berthing Division and the management information department complied with top managers' directives designed to enhance their good image due to their having weaker power than top managers. Customers understood that ship arrangement processes were long-standing: no matter what changes were made, it would have to remain in the future. They believed that _KaoKang_ should learn from the successful experiences of Hong Kong and Singapore; resolve the problems of _KaoKang_ first, then computerize. At the same time, customers (petitioner) needed to have good _guanxi_ with employees of the Berthing Division (resource allocator) to ensure that their ships would be allocated good quays and to benefit from other privileges.

Finally, consultants at _KaoKang_ suggested eradication of the manual function. However, top managers totally rejected their ideas because the top managers' wielded power over consultants. This result is consistent with that of Ko ([1995](#ko95)), Soh _et al._ ([1993](#soh93)) and Wong ([1992](#won92)), namely that the primary aim of an information system is to monitor capabilities in Chinese business. Consultants disagreed with the top managers' decision with respect to the system's functions but their suggestions were rejected by those top managers. Nevertheless, from the perspective of a relational-oriented culture such as the Chinese ([Ho 1993](#ho93); [Hwang 1987](#hwa87); [Jacob 1979](#jac79)), it was still important for the consultants to maintain interpersonal harmony with the top managers.

As a result of the assertiveness of top managers, the confidence of the employees of the Berthing Division, management information professionals and consultants was totally undermined, leading them not to persist in their suggestions. This illustrates how different actors make sense of the system in totally different ways. Because of the above incongruence of the five actors' attitudes, assumptions, expectations and knowledge of the system, it was impossible to make it workable until December 2008.

### The technological re-frames of four actors from the first to second stage

These frames can be seen in Table 6 below.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 6: Technological re-frames of four actors**  
</caption>

<tbody>

<tr>

<th>Actors: Technological Frames</th>

<th>First Stage</th>

<th>Second Stage</th>

</tr>

<tr>

<th>_Top managers: control of the functions of the system_</th>

<td valign="top">Not concerned about the function of the system.</td>

<td valign="top">Asserted the system should retain the manual function.</td>

</tr>

<tr>

<th>_Employees of the Berthing Division: strategy and goal relating to the system_</th>

<td valign="top">The system was capable of threatening their power, self-interests and jobs. They doubted the capacity of the system. They adopted a hit-or-miss and make-shift approach and made only superficial effort to avoid punishment.</td>

<td valign="top">Computerization was possible at this time. They modified and simplified the complicated rules. Therefore, they wrote a request for proposals according to top managers' requirements.</td>

</tr>

<tr>

<th>_Management information professionals: strategy and goal relating to the system_</th>

<td valign="top">it had not come to maturity. Finally, they used a web homepage to show the ship quay results only.</td>

<td valign="top">Shifted their role from that of promoter to consultant. They believed that the system should achieve its goal.</td>

</tr>

<tr>

<th>Customers:_strategy and goal relating to the system_ and _control of its functions._</th>

<td valign="top">The system was threatening their job security. Therefore, they totally resisted the system.</td>

<td valign="top">They conditionally accepted the system but doubted its operation as a black box.</td>

</tr>

</tbody>

</table>

In accord with the perspective of Ovaska _et al._ ([2005](#ova05)), Boland ([2001](#bol01)), El Sawy and Pauchant ([1988](#ei88)), Gioia ([1986](#gio86)), all of whom made longitudinal observations, this study finds that the cognitive technological frames of some actors changed due to time and technological developments.

_Control of the functions of the quay management information system_ of top managers changed in the course of the the implementation process. Top managers changed their perspective when they discovered that '_maintaining good relations with privileged clients_' was their power resource ([Hwang 1987](#hwa87)). This became their top priority, superseding the three goals of fairness, transparency and preservation of public rights and the practical effectiveness of the system.

The _strategy and goals relating to the system_ of employees in the Berthing Division changed in order that they might preserve their power and self-interests after the system had been developed, as well as retain their official power in the ship arranging function. This result demonstrates the officials' desire utterly to shirk their responsibilities.

The _strategy and goals relating to the system_ of management information professionals changed from their understanding that there were many reasons influencing the success of the system development processes and that the chances of success were very low. They changed their strategy to retain their professional power. This result betrayed the principle these officers were adhering to: the less they did, the fewer errors they would make.

The _strategy and goals relating to the system_ and _control of the functions of the system_ of customers changed because of their constant concern about their job rights and the fairness of ship arranging. The ship arranging outcome of the system would also affect their companies' interests if it was to operate as a black box (such as: the system has the capacity to compromise a powerful persons' request).

These technological frame changes may help management to understand how to employ utilizable factors as well as provide an appropriate method for changing the perceptions of information systems of employees in their different positions within the organization. In this way, information systems may be accepted more easily in the future.

## Implications

This section presents discussion of the contribution to both theory and practice of this study. To this end, two theoretical propositions are formulated and their practical implications detailed.

This study adopted four domains of technological frames (motivation adoption, strategies and goals relating to the information system, ship quay arrangement solutions and control of the functions of the information system) and interpreted them through the lens of the Chinese power game theory. For example: (1) Top managers asserted that the information system should retain the manual function to respond to the requests for privileges and expected those privileges to lead to a debt of _renqing_ to the top managers. Those privileges would lead to repayment of the debt of gratitude when the top managers required it. (2) According to traditional Chinese culture, where hierarchical status in social relations is highly valued (a high power-distance society), the line between top manager and employees cannot be crossed. For this reason, even the objective and unselfish consultants could not violate the directives of the top mangers. In fact, the technological frames between top managers and consultants were totally incongruent. The study, therefore, has produced a number of unusual findings. Different methods of sense-making are used with the five kinds of actor present in _KaoKang_ in the the quay management information system development processes.

The above discussion leads us to formulate the following propositions:

**Proposition 1:** The information system is a tool not only to use, but also to be interpreted by different actors who have incongruent technological frames throughout the entire information system development process. The incongruence of the technological frames is due to the high power-distance and the Chinese cultural characteristics that influence both the implementation processes and consequences of information systems in Chinese society.

Although the norm of reciprocity is a universal one, Gouldner ([1960](#gou60)) has argued that the chief differences among three types of interpersonal ties (instrument, mixed and expressive tie) in Chinese cultural society lie in their different repayment methods and in the varying time periods permitted between giving and repaying. In addition, while Western society places considerable emphasis on equity rules, Chinese society emphasizes the special needs of significant others (_need rule_ and _renqing rule_) ([Greenberg and Cohen 1982](#gre82); [Messick and Cook 1983](#mes83)). Therefore, Chinese ethics values positively the obligation for reciprocation and places heavy emphasis on the practice of such maxims as: '_do not forget what other people have done for you_', '_do not forget the good deeds done to you, even if they are small_', '_if you have received one drop of good will from other people, you should return to them a fountain of goodwill_' and '_if one gives you a peach, you should return his favour with a plum_'. The benefactor can rightly look forward to a return, a reciprocal action, not to be neglected by the receiver, in the future, when he, himself, is in great need. it is largely owing to this anticipation of reciprocity that the resource allocator is willing to grant the petitioner _renqing_. Therefore, it is posited that:

**Proposition 1-1:** The incongruent technological frames throughout the entire information system development process are the consequence of the need to maintain interpersonal harmony, _guanxi_ and _renqing_, which are characteristic of Chinese culture.

Based on Confucian values, Chinese business practices emphasize the importance of personal relationships (_guanxi_), _renqing_ and _face_ ([Ang and Leong 2000](#ang00); [Chung _et al._ 2008](#chu08)). Confucian societies place greater emphasis on hierarchical relationships (The five Cardinal Rules of Confucianism), observance of standardized rituals and social harmony ([2008](#chu08)Chung _et al._ ). Thus it is apparent that _guanxi_, _renqing_ and maintenance of interpersonal harmony are important elements in Chinese culture, all of which are intricately linked to power. In the light of the fact that information systems can change an organizationASs power structure and allocation of benefits, some resource allocators will use their _guanxi_ to influence the direction of information systems in order to protect their power and self-interests, as illustrated by the employee actors of the Berthing Division and customers in this study. Moreover, some resource allocators will not consider the difficulties of implementing their ideas, leading them to shape the development of information systems in a way that will ensure the saving of his or her _face_, as illustrated by the actor of top managers in this study. Thirdly, some actors (management information professionals, customers) must satisfy the requirement of Chinese culture to maintain interpersonal harmony and to give _face_ to others (the employees of the Berthing Division) rather than spiting them, to help them to do their best. In so doing, they expect others to repay this _renqing_ in the future. Thus, it is posited that:

**Proposition 1-2:** The need to maintain interpersonal harmony, _guanxi_ and _renqing_ results from the status hierarchy (unequal power, high power-distance) in Chinese cultural society, which triggers incongruent technological frames throughout the information system development process.

This study reminds us that information systems can fail through the wielding of power and self-interests in different organizational structures and cultures, even if they are good and meaningful systems. However, failure in a Western context could occur for different reasons from that in a Chinese context. This paper also finds that even an actor who is just, objective and unselfish (such as consultants in this study) cannot direct information systems successfully if they are living in the context of Chinese culture due to their lower position of power compared to top managers. These phenomena are characteristic of Chinese cultural society. Thus, it is proposed that:

**Proposition 1-3:** The equal power perspective of Western society is not appropriate in the hierarchical power context of Chinese society, which requires reference to the Chinese cultural characteristics of unequal power, maintainenance of interpersonal harmony, _guanxi_ and _renqing_ to interpret the technological frames. Only through such reference will information system development in Chinese cultural society be successful.

Not only top managers, but also employees of the Berthing Division, management information professionals and customers shifted their technological frames from the first to second stage of the information system. Top managers wished to maintain good relations with privileged clients and repay _renqing_ to those powerful individuals at a later date. Employees of the Berthing Division sought to retain their official power and utterly to shirk their responsibilities. The hope of management information professionals was to maintain harmony with the other four actors. The expectation of customers was to gain greater benefit from the information system and to have good _guanxi_ with _KaoKang_. The conflicts of interest among the five actors are notable because they created potential difficulties for their decision-making and uncertainty about how those difficulties would be resolved (Provis [2007](#pro07)). In order to overcome those difficulties, the actors shifted their frames to conform with the Chinese _guanxi_, _renqing_ protocol and to maintain interpersonal harmony. Thus, it is proposed that:

**Proposition 2:** it is necessary for different actors to adapt their respective perceptions throughout the information system development process to re-frame their technological frames through time in order to maintain interpersonal harmony, enjoy good _guanxi_ with powerful individuals, repay the _renqing_ to resource allocators and respect the Chinese cultural power hierarchy.

For this reason, management should understand not only the different kinds of cognition among actors (technological frames), but also the Chinese culture in information system development processes. Therefore, in-depth understanding from these perspectives will promote smoother information system development with the avoidance of unnecessary manipulated power events in Chinese culture. In light of this, there are five suggestions to management.

First, managers should consider adopting a team-work approach in which team members take collective responsibility to avoid the danger of employees serving self-interests in different situations. Secondly, before the information system development process, the plan, direction, content and goal of the system should be announced and explained in considerable detail. This will help to avoid different actors wielding power in the development processes and overshadowing the future direction of is the information system by actions driven by self-interest and power. Thirdly, managers should explain the extent to which the formal rules of the organization are flexible to avoid employees from different departments using these rules to control the information system. This is a particularly important strategy in the public department (government).

Next, enterprises should have clear reward and punishment standards before the information system development process, in order to avoid employees from different departments exploiting the system and shirking responsibility for decisions. In the light of this, management should understand the different kinds of cognition among actors (technological frames). Finally, managers should also recognize how these key actors will use their status and expert knowledge to wield power in the information system development processes. In-depth understanding from this perspective will promote smoother development and will avoid unnecessary manipulated power events. In addition, managers would benefit from interacting informally with employees in accordance with the _guanxi_ culture and use it to bring about positive outcomes with employees in information system development processes. For example, if managers maintain good _guanxi_ with their employees in ordinary life, employees will help their managers whole-heartedly, as the consequence of _guanxi_. In brief, enterprises seeking to implement policy relating to the adoption of a new information system should take into serious consideration the national culture in order to increase the chances of success for the new system.

## Conclusion

Based on The Five Cardinal Rules of Confucianism, Chinese societies emphasize hierarchical relationships in which power tends to be distributed unequally and greater emphasis is placed on the importance of personal relationships (_guanxi_), _renqing_, _face_ and social harmony. Thus, this paper has investigated the power operation of information systems and their development, exploring the psychological mechanism of individuals from the micro perspective. It has analysed the gap in technological frames among different actors and used Chinese cultural perspectives to ascertain how these actors wield power to achieve their goals of self-interest and impede information systems' effectiveness. The study collected data from _KaoKang_, a port authority and using the Chinese power game theory of Hwang ([1987](#hwa87)), attempted to understand different key actors in a particular context with technological frame gaps in development processes.

This paper has provided insights into how the need to maintain interpersonal harmony, _guanxi_, _renqing_ and status hierarchy (unequal power distribution) characteristic of Chinese culture can lead to the incongruence of technological frames in the information system development processes. how the different actors in the case examined in this study not only wielded power, but also dealt with and reflected the incongruent technological frames accorded with the actors' need to uphold these Chinese cultural characteristics. The study revealed that so as not to violate these important cultural characteristics, the actors shifted their technological frames through time.

Finally, the paper has provided evidence that the incongruence of the techological frames as well as the re-frames are closely related to power manipulation and national (Chinese) culture. This finding reveals a difference between Chinese and Western society and provides a fresh perspective for management on how best to develop information systems in Chinese cultural organizations in the future.

In view of the important role played by China in the 21st century, particularly by mainland China, which represents a hugely significant market to all global enterprises, it is hoped that this study will assist the business world in understanding the culture in which Chinese business is conducted. Therefore, this study has provided a good reference source for global enterprises, as well as insight into how to avoid different actors' use of _guanxi_ to wield power in information system development processes in Chinese cultural organizations. It is hoped that it will serve to further reduce the number of negative outcomes of information systems in organizations.

## Acknowledgements

The author would like to thank the National Science Council of the Republic of China for its financial support of this study (NSC93-2416-H-251-008). I would also like to acknowledge the constructive comments of the anonymous reviewers.

## References

*   Ang, S.H. & Leong, S.M. (2000). Out of the mouths of babes: business ethics and youths in Asia. _Journal of Business Ethics_, **28**(2), 129-144\.
*   Avgerou, C. (2001). The signification of context in information systems and organizational change. _Information Systems Journal_, **11**(1), 43-63.
*   Avgerou, C. (2002). _Information systems and global diversity_. Oxford: Oxford University Press.
*   Barr, P. (1998). Adapting to unfamiliar environmental events: a look at the evolution of interpretation and its role in strategic change. _Organization Science_, **9**(6), 644-670.
*   Barrett, M. (1999). Challenges of EDI adoption for electronic trading in the London insurance market. _European Journal of Information Systems_, **8**(1), 1-15.
*   Bartunek, J. (1984). Changing interpretative schemes and organizational restructuring: the example of a religious order. _Administrative Science Quarterly_, **29**(3), 355-372.
*   Bijker, W. (1987). The social construction of Bakelite: toward a theory of invention. In Wiebe T. Bijker, Thomas P. Hughes & Trevor J. Pinch, (Eds.). _The social construction of technological systems_ (pp. 159-187). Cambridge, Mass: MIT Press.
*   Bijker, W.T., Hughes, T.P. & Pinch, T.J. (Eds). (1987). _The social construction of technological systems_. Cambridge, MA: MIT Press.
*   Bloomberg, J.L. (1986). The variable impact of computer technologies on the organization of work activities. In _Proceedings of the Conference on Computer Supported Cooperative Work_ (pp. 35-42). New York, NY: ACM.
*   Boland, R. (2001). The tryanny of space in organizational analysis. _Information and Organization_, **11**(1), 2-23.
*   Bostrom, R.P. & Heinen, J.S. (1977). MIS problems and failures: a socio-technical perspective: Part I, The causes. _MIS Quarterly_, **1**(1), 17-32.
*   Chang, C.L.H. & Lin, T.C. (2005). Interpretation political behaviors in information system implementation from multi-perspectives. _Journal of Information Management_ [in Chinese], **12**(4), 185-210.
*   Chang, C.L.H. (2006a). The political behavior intention in information system development: a study based on the theory of planned behavior: the gap between MIS professionals and users. _Journal of Management_ [in Chinese], **23**(4), 347-365.
*   Chang, C.L.H. (2006b). The political behavior intention in information system development from MIS professionals perspective: a study based on the theory of planned behavior. _International Journal of Innovation and Technology Management_, **3**(4), 341-360.
*   Chang, C.L.H. (2007). The political behavior intention of users in information system development. _Human Systems Management_, **26**(2), 123-137.
*   Chiao, C. (1982). Guanxi: a preliminary conceptualization. In K.S. Yang & C.I. Wen (Eds.), _The Sincization of social and behavioral science research in China_ [in Chinese] (pp. 345-360). Taipei, Republic of China: Academia Sinica.
*   Chiasson, M. & Davidson, E. (2005). Taking industry seriously in IS research. _MIS Quarterly_, **29**(1), pp. 2-16.
*   Chu, L. (1979). The sensitivity of Chinese and American children to social influence. _Journal of Social Psychology_, **109**(2), 175-186.
*   Chung, W.K. & Hamilton, G.G. (2001). Social logic as business logic: guanxi, trustworthiness and the embeddedness of Chinese business practices. In R. Appelbaum, W. Felstiner & V. Gessner (Eds.), _Rules and networks: the legal culture of global business transactions_ (pp. 325-346). Oxford: Hart Publishing.
*   Chung K.Y., Eichenseher, J.W. & Taniguchi, T. (2008). Ethical perceptions of business students: differences between East Asia and the USA and among 'Confucian' cultures. _Journal of Business Ethics_, **79**(1 & 2), 121-132.
*   Coombs, R., Knights, D. & Willmott, H.C. (1992). Culture, control and competition: towards a conceptual framework for the study of it in Organizations. _Organization Studies_, **13**(1), 51-72.
*   Davidson, E. (2002). Technology frames and framing: a socio-cognitive investigation of requirements determination. _MIS Quarterly_, **26**(4), 329-358.
*   Davidson, E. (2006). A technological frames perspective on it and organizational change. _The Journal of Applied Behavioral Science_, **42**(1), 23-39.
*   Dhillon, G. (2004). Dimensions of power and IS implementation. _Information & Management_, **41**(5), 635-644.
*   El Sawy, O. & Pauchant, T. (1988). Triggers, templates and twitches in the tracking of emerging strategic issues. _Strategic Management Journal_, **9**(5), 445-473.
*   Foucault, M. (1982). The aubject and power. In: H.L. Dreyfus & P. Rainbow (Eds.), _Michel Foucault: beyond structuralism and hermeneutics_ (pp. 208-226). Brighton, England: Harvester Press.
*   Francois, J. (2004). _A treatise on efficacy: between Western and Chinese thinking_ (translated by Janet Lloyd). Honolulu, HI: University of Hawaii Press.
*   Gash, D.C. (1987). _The Effects of microcomputers on organizational roles and the distribution of power_. Unpublished doctoral dissertation. Cornell University, Ithaca, N.Y., U.S.A.
*   Gioia, D. (1986). Symbols, scripts and sensemaking: creating meaning in the organizational experience. In H. Sims Jr., D. Gioia & Associates (Eds.), _The thinking organization_ (pp. 49-74). San Francisco, CA: Jossey-Bass.
*   Gouldner, A. (1960). The norm of reciprocity: a preliminary statement. _American Sociological Review_, **25**(2), 161-178.
*   Grant, D., Michelson, G., Oswick, C. & Wailes, N. (2005). Guest editorial: Discourse and organizational change. _Journal of Organizational Change_, **18**(1), 6-15.
*   Greenberg, J. & Cohen, R.L. (1982). Why justice? Normative and instrumental interpretations. In J. Greenberg and R.L. Cohen (Eds), _Equity and justice in social behavior_ (pp. 437-466). New York, NY: Academic Press.
*   Grudin, J., (1989). Why groupware applications fail: problems in design and evaluation. _It and People_, **4**(3), 245-254.
*   Henderson, K. (1991). Flexible sketches and inflexible data bases: visual communication, conscription devices and boundary objects in design engineering. Science, _Technology & Human Values_, **16**(4), 448-473.
*   Heng, M., Trauth, E. & Fischer, S. (1999). Organizational champions of IT innovation. Accounting, _Management and It_, **9**(3), 193-222.
*   Ho. D.Y.F. (1993). Relational orientation in Asian social psychology. In U. Kim & J. W. Berry (Eds.), _Indigenous psychologies: research and experience in cultural context_ (pp. 240-259). Newbury Park, CA: Sage Publications.
*   Hofstede, G. (1980). _Culture's consequences: international differences in work-related values_. London: Sage Publications.
*   Hofstede, G. (1991). _Cultures and organizations: software of the mind_. New York, NY: McGraw-Hill.
*   Huang, L.C. (1974). A cross-cultural study of conformity in Americans and Chinese. _Dissertation Abstracts International_, **34**, 571b.
*   Hwang, K.K. (1987). Face and favor: the Chinese power game. _American Journal of Sociology_, **92**(4), 944-974.
*   Hwang, K.K. (2000). Chinese relationism: theoretical construction and methodological considerations. _Journal for the Theory of Social Behavior_, **30**(2), 155-178.
*   Introna, L.D. (1997). _Management, information and power_. London: Macmillan.
*   Jacobs, B.J. (1979). A preliminary model of particularistic ties in Chinese political alliances: 'renqing' and 'guanxi' in a rural taiwanese township. _China Quarterly_, No. 78, 237-273.
*   Jasperson, J., Carte, T.A., Saunders, C.S., Butler, B.S., Croes, H.J.P. & Zheng, W. J. (2002). Review: Power and it research: a metatriangulation review. _MIS Quarterly_, **26**(4), 397-459.
*   Keen, P.G.W. (1981). Information systems and organizational change. _Communications of the ACM_, **24**(1), 24-33.
*   Keil, M. & Robey, D. (1999). Turning around troubled software projects: an exploratory study of the de-escalation of commitment to failing courses of action, _Journal of Management Information Systems_, **15**(4), 63-88.
*   King, A.Y. (1991). Kuan-his and network building: a socialogical interpretation. _Deadalus_, **120**(2), 63-84.
*   Klein, H.K. & Myers M.D. (1999). A set of principles for conducting and evaluating interpretative field studies in information systems. _MIS Quarterly_, **23**(1), 67-94.
*   Kling, R. (1987). Defining the boundaries of complex systems across complex organizations. In R. L. Boland and R. A. Hirschheim (Eds.), _Critical issues in information systems research_. New York, NY: Wiley.
*   Kling, R. & Iacono, S. (1984). The control of information systems after implementation. _Communications of the ACM_, **27**(12), 1218-1226.
*   Ko, A.C.K. (1995). Towards an understanding of overseas Chinese management. _Journal of Management Systems_, **7**(1), 13-28.
*   Kumar, K. & Bjorn-Andersen, N. (1990). A cross-cultural comparison of IS designer values. _Communications of the ACM_, **33**(5), 528-538.
*   Lao, R.C. (1977). Levenson's IPC (internal-external control) scale: a comparison of Chinese and American students. _Journal of Cross-Cultural Psychology_, **9**(1), 113-124.
*   Lawrence, T.B., Mauws, M.K. & Dyck, B. (2005). The politics of organizational learning: integrating power into the 4I framework. _Academy of Management Review_, **30**(1), 180-191.
*   Lin, T.C. & Chang, C.L.H. (2000). The political cames of user and MIS personnel in information system development process: an exploratory research. _Sun Yat-Sen Management Review_ [in Chinese], **8**(3), 479-510.
*   Lin, A. & Silva, L. (2005). The social and political construction of technological frames. _European Journal of Information Systems_, **14**(1), 49-59.
*   Low, S.P. (1996). The influence of Chinese philosophies on mediation and conciliation in the Far East. _Arbitration: Journal of the Chartered Institute of Arbitrators_, **62**(1), 16-20.
*   Markus, M.L. (1983). Power, politics and MIS implementation. _Communications of the ACM_, **26**(6), 430-444.
*   Markus, M.L. & Bjorn-Andersen, N. (1987). Power over users: its exercise by system professionals. _Communications of the ACM_, **30**(6), 498-504.
*   Markus, M.L. & Robey, D. (1995). It and organizational change: causal structure in theory and research. _Management Science_, **34**(5), 583-598.
*   Martinsons, M.G. & Westwood, R.I. (1997). Management information systems in the Chinese business culture: an explanatory theory. _Information & Management_, **32**(6), 215-228.
*   McGovern, T. & Hicks, C. (2004). How political processes shaped the IT adoption by a small make-to-order company: a case study in the insulated wire and cable industry. _Information & Management_, **42**(1), 243-257.
*   McLoughlin, I., Badham, R. & Couchman, P. (2000). Rethinking a political process in technological change: socio-technical configurations and frames. _Technology Analysis & Strategic Management_, **12**(1), 17-37.
*   Messick, D.M. & Cook, K.S. (eds.) (1983). _Equity theory: psychological and sociological perspectives_. New York, NY: Praeger.
*   Newman, M. & Sabherwal, R. (1996). Escalation of commitment to information systems development: a longitudinal investigation. _MIS Quarterly_, **20**(1), 23-54.
*   Orlikowski, W.J. & Gash D.C. (1994). Technological frames: making sense of it in arganizations. _ACM Transactions on Information System_, **12**(2), 174-207.
*   Ovaska, P., Rossi, M. & Smolander, K. (2005). Filtering, negotiating and shifting in the understianding of information system requirements. _Scandinavian Journal of Information Systems_, **17**(1), 31-66.
*   Pare, G. (2004). Investigating information systems with positivist case study research. _Communications of the Association for Information Systems_, **13**(1), 233-264.
*   Provis, C. (2007). Guanxi and conflicts of interest. _Journal of Business Ethics_, **79**(1), 57-68.
*   Redding, S.G. (1980). Cognition as an aspect of culture and its relation to management process: an exploratory view of the Chinese case. _Journal of Management Studies_, **17**(2), 127-148.
*   Redding, S.G. (1990). _The spirit of Chinese capitalism_. New York, NY: DeGruyter.
*   Sillince, J.A.A. & Mouakket, S. (1997). Varieties of political process during systems development. _Information Systems Research_, **8**(4), 368-397.
*   Silva, L. & Backhouse, J. (2003). The circuits-of-power framework for studying power in institutionalization of information systems. _Journal of the Association for Information Systems_, **4**(6), 294-336.
*   Soh, C., Neo, B.S. & Markus, M.L. (1993). IT 2000: a critical appraisal of Singapore's state-wide strategic planning process for it. _Journal of Strategic Information Systems_, **2**(4), 351-372.
*   Su, C. & Littlefield, J.E. (2001). Entering guanxi: a business ethical dilemma in mainland China? _Journal of Business Ethics_, **33**(3), 190-210.
*   Suen, H., Cheung, S.O. & Mondejar, R. (2007). Managing ethical behaviour in construction organizations in Asia: how do the teachings of Confucianism, Taoism and Buddhism and globalization influence ethics management? _International Journal of Project Management_, **25**(3), 257-265.
*   Tricker, R.I. (1988). Information resource management: a cross-cultural perspective. _Information & Management_, **15**(1), 37-46.
*   Walsh, J. (1995). Managerial and organizational cognition: notes from a trip down memory lane. _Organization Science_, **8**(5), 280-321.
*   Wong, S.H. (1985). The Chinese family firm: a model. _British Journal of Sociology_, **36**(1), 58-72.
*   Wong, S.H. (1992). Exploiting it: a case study of Singapore. _World Development_, **28**(2), 1817-1828.
*   Zuboff, S. (1988). _In the age of the smart machine_. New York, NY: Basic Books.

