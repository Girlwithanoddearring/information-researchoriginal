#### vol. 15 no. 3, September, 2010

## Proceedings of the Seventh International Conference on Conceptions of Library and Information Science - "Unity in diversity"

# Identifying factors that may impact the adoption and use of a social science collaboratory: a synthesis of previous research

[Monica Lassi](#authors)
Swedish School of Library & Information Science, 
University of Gothenburg and University of Borås, 
Allégatan 1, SE-501 90 Borås, Sweden 

 and 
 
 [Diane H. Sonnenwald](#authors) 
 School of Information & Library Studies, University College Dublin. Belfield, Dublin 4, Ireland.

#### Abstract

> **Introduction.** This paper examines and synthesizes previous research in scientific collaboration, scholarly communication, scientific collaboratories, scientific disciplines, invisible colleges and virtual communities to identify factors that may impact the design, adoption and use of a collaboratory within librarianship and information science.  
> **Results.** A taxonomy of factors that appear to impact the design, adoption and use of a collaboratory emerged from the synthesis. Six types of factors were identified: factors that impact a researcher's career advancements; factors concerning aspects of doing science which affect researchers on a personal level, other than their career; factors focusing on whether the benefits of submitting to and using a collaboratory is worth the cost for the individual; disciplinary and factors focusing on science and disciplines in general, such as the development of new methodology within a discipline; factors that affect the community of researchers; and factors that are the costs of developing and sustaining a collaboratory for a community or discipline.  
> **Conclusions.** The taxonomy provides a concise overview of explanatory factors concerning the adoption and use of collaboratories. The taxonomy provides a theoretical framework to guide future research which explores the adoption and use of a collaboratory in disciplines not yet studied, e.g., library and information science.

## Introduction

Collaboration increasingly facilitates advances in research and development. Collaboration brings together expertise and resources otherwise not available to an individual researcher, allowing complex and multi-disciplinary problems to be addressed ([Sonnenwald 2007](#sonn07)). It helps nations develop or maintain their scientific excellence which in turn can improve their national economies ([Berman and Brady 2005](#berm05); [Pothen 2007](#poth07)). Studies have shown that researchers who collaborate tend to produce more publications and that these publications get cited more and for longer periods of time than single-authored publications ([Beaver 2004](#beav04); [Sonnenwald 2007](#sonn07)). Despite these advantages collaboration is not very common in some social science disciplines when compared to other disciplines. For example in 1999, the number of co-authors per article in library and information science was lower than the number of co-authors per article in the natural sciences in 1955 ([Cronin 2005](#cron05)).

Collaboratories have emerged as a new way of facilitating and conducting research across geographical distances. A collaboratory is defined as,

> a network-based facility and organizational entity that spans distance, supports rich and recurring human interaction oriented to a common research area, fosters contact between researchers who are both known and unknown to each other, and provides access to data sources, artifacts and tools required to accomplish research tasks." ([Science of Collaboratories 2003](#scie03)).

In contrast to repositories, collaboratories add a social feature, connecting shared resources with research and social practices prevalent in its research community. Collaboratories, also referred to as virtual research environments, have been used in the natural sciences since the early 1990s and their success has been mixed ([Finholt 2002](#finh02)).

Could a collaboratory facilitate research within a discipline, such as library and information science, which does not have a strong tradition of collaboration? As in other disciplines, researchers in this field are pressured to keep up with rapid changes in cognate disciplines, quickly and effectively contribute to identifying and finding solutions to emerging problems, and disseminate knowledge in new ways. However in academia library and information science departments typically have a small number of full time faculty and are often geographically dispersed. For example, in some U.S. states and European countries there may only be one such academic department. Furthermore their faculty are often required to do twice as much teaching as colleagues in natural science and engineering departments because there are typically not as many opportunities (as in the natural sciences) to obtain large amounts of research funding. A collaboratory which spans geographic distances and facilitates the sharing of valuable resources could perhaps facilitate and promote library and information science research. We could learn from the successes and failures of collaboratories and collaboration among researchers in other disciplines to inform and evaluate the potential of a collaboratory within the field.

Thus, this paper examines and synthesizes literature in scientific collaboration, scholarly communication, scientific collaboratories, scientific disciplines, invisible colleges and virtual communities to identify factors that may impact the design, adoption and use of a collaboratory within a discipline such as library and information science. Six types of factors were identified: career factors that impact a researcher's career advancements; personal factors concerning aspects of doing science which affect researchers on a personal level, other than their career; cost of use factors focusing on whether the benefits of submitting to and using a collaboratory is worth the cost for the individual; disciplinary and scientific advancement factors focusing on science and disciplines in general, such as the development of new methodology within a discipline; community factors which affect the community of researchers; and cost to develop and sustain factors that are the costs of developing and sustaining a collaboratory for a community or discipline.

In this paper we first discuss the relevant research literatures that contribute to our understanding of collaboratories. We next synthesize results reported in the literatures, proposing a taxonomy, or classification, of factors that appear to impact the design and adoption of collaboratories. As noted by Gregor ([2006](#greg06)) taxonomies, also referred to as typologies and classification schemes, are theories of analysis. They contribute to our understanding by describing what is, synthesizing '_salient attributes of phenomena_' ([Gregor 2006](#greg06): 623). Taxonomies, such as that presented in this paper, provide a comprehensive view of a phenomenon and provide a foundation for predictive theories.

## Relevant research literature

To develop a broad understanding of factors that may impact the design and adoption of a collaboratory in library and information science we sought out literature that reported on the collaborative scientific processes in both traditional science and e-science contexts. To complement this literature, we also sought out literature discussing organizational, or structural, aspects of traditional science and e-science. Thus we examined literature on scientific collaboration, scholarly communication, scientific collaboratories, scientific disciplines, invisible colleges and virtual communities in science (see Table 1).

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Relevant literature.**</caption>

<tbody>

<tr>

<th rowspan="2" valign="middle">_Perspective_</th>

<th colspan="4">_Context_</th>

</tr>

<tr>

<th colspan="1">Traditional science</th>

<th colspan="1">e-Science</th>

</tr>

<tr>

<td align="center">_Process_</td>

<td align="center">Scientific collaboration  
Scholarly communication</td>

<td align="center">Scientific collaboratories</td>

</tr>

<tr>

<td align="center">_Organization_</td>

<td align="center">Scientific disciplines  
Invisible colleges</td>

<td align="center">Virtual communities</td>

</tr>

</tbody>

</table>

_Scientific collaboration_ is defined as '_human behavior among two or more scientists that facilitates the sharing of meaning and completion of tasks with respect to a mutually-shared superordinate goal and which takes place in social context'_ ([Sonnenwald 2007](#sonn07): 645). Collaboration can encompass a range of activities, from loosely-coupled to tightly-coupled activities. Coupling concerns the amount of work a person can conduct individually before needing to interact with another person to reach their goal. Neale _et al._ ([2004](#neal04): 115) propose the following range:

*   light-weight interaction: communication about work may be mixed with social talk;
*   information sharing: exchanging information related to work;
*   coordination: coordinating activities and communication;
*   collaboration: working toward a common goal;
*   cooperation: working more tightly together; the common goals are prioritized before individual goals.

In this paper we consider the entire range when we write about collaboration unless otherwise noted.

Scientific collaboration often differs from collaboration in other settings in that there is a high degree of uncertainty with respect to whether the research goal will be achieved, and how to achieve it in the best possible way. A number of factors have been shown to impact scientific collaboration: scientific; political; socio-economic; resource accessibility; and social networks and personal factors ([Sonnenwald 2007](#sonn07)).

_Scholarly communication_ investigates how researchers share their findings traditionally through publishing research papers and articles and, more recently, through Web pages. Scholarly communication is a vital part of researchers' work. As explained by Montgomery ([2003](#mont03): '_There are no boundaries, no walls, between the doing of science and the communication of it; communicating is the doing of science_'. Borgman and Furner ([2002](#borg02)) see the transformation of scholarly communication in how researchers '_discuss, write, share, and seek information through networked information systems_' ([Borgman and Furner 2002](#borg02): 4).

_Scientific disciplines_ have different characteristics that influence patterns of collaboration and information sharing among researchers in that discipline. Examples of successful large scale collaborations within specific disciplines include the [Inter-university Consortium for Political and Social Research](http://www.icpsr.umich.edu/) , [GenBank](http://www.ncbi.nlm.nih.gov/Genbank/) , and the [Protein Data Bank](http://www.rcsb.org/). Birnholtz and Bietz ([2003](#birn03)) suggest that disciplines that display low task-uncertainty and high mutual dependence are more prone to succeeding in information sharing and collaboration. The task-uncertainty of a discipline concerns the degree to which the researchers within the discipline agree on what the important problems are and which are the preferred methods to solve the problems. Mutual dependence concerns the magnitude of the problems studied within a discipline. A high mutual dependence is when many research groups have to study a problem in order to find a solution, as in the case of AIDS research ([Birnholtz and Bietz 2003](#birn03)). Library and information science could be described as having high task uncertainty and low mutual dependence, which may influence collaboration within the discipline.

_Invisible colleges_ have traditionally been described as secret societies within which the members collaborate and cite each other, which can be studied with bibliometric methods ([Price 1963](#pric63); [Crane 1972](#cran72)). An invisible college is a tightly knit group of scholars who promote each other's work. Colleges have embraced new communications media; these new tools have created electronic invisible colleges ([Finholt 2002](#finh02); [Crawford _et al._ 1996](#craw96)). Ideally the tools would bring allow researchers who do not work at elite universities or cannot attend conferences to participate in invisible colleges. However this has not happened; the same invisible college structures are merely replicated in the virtual realm ([Finholt 2002](#finh02)).

_Scientific collaboratories_, which depend on cyberinfrastructure, enable novel approaches to doing science, e.g., providing real-time use of instruments at remote locations, facilitating sharing digital research data across geographic distances, and synchronous manipulation of texts, video and other digital media. Scientific collaboratories are also referred to virtual research environments, and today include many types of social media and Web 2.0 features such as social networking tools and interactive information sharing tools. Scientific collaboratories were originally been developed for the natural sciences, and not for the social sciences and humanities; however, this is changing. Most recently collaboratories bringing together researchers from natural science, engineering, social sciences and humanities have emerged (e.g., [HASTAC](http://www.hastac.org/) ). Lessons learned from the development of previous collaboratories can help inform future collaboratory design with the caveat that the characteristics of the disciplines for which they have been developed might be quite different from the characteristics of the target discipline.

_Virtual communities_ are '_social aggregations that emerge from the Net when enough people carry on those public discussions long enough, with sufficient human feeling, to form Webs of personal relationships in cyberspace_' ([Rheingold, 1995](#rhei95): 5). Associated terms include online communities ([Preece 2000](#pree00)); and virtual community networks ([Ellis _et al._ 2004](#elli04)). Preece ([2000](#pree00)) has identified four constituent elements of virtual communities: people, a shared purpose, policies to guide interactions, and computer systems to support the community. A typical phenomenon that occurs in virtual communities is the sharing of information. It is believed that general reciprocity motivates people to share information with other community members - people they have in many cases never met in real life ([Ellis _et al._ 2004](#elli04); [Lakhani and von Hippel 2003](#lakh03); [Preece 2000](#pree00); [Rheingold 1995](#rhei95); [von Krogh _et al._ 2003](#vonk03); [Wasko and Faraj 2000](#wask00)). This means that a person sharing information can expect to get information in return when needed, but not necessarily from the same person who one helped to begin with.

Knowledge about what motivates members of open source software development communities to actively contribute to communities without monetary rewards (e.g. [Lakhani and von Hippel 2003](#lakh03)) may help inform how we can motivate contributions to a scientific collaboratory. For example the motivations for contributing time, resources and knowledge to the community to an open source software development project could be similar to contributing to a scientific collaboratory. Virtual communities can thrive and survive long-term though members never meet face to face, and a sense of belonging to a community can be established and maintained only through online communication. How this occurs is an active area of research ([Ellis _et al._ 2004](#elli04); [Preece 2000](#pree00)) which could have implications for the design of a collaboratory.

Literature on the above topics emerges in several disciplines, including library and information science, computer science (especially in the computer supported cooperative work subfield), communication (e.g. in computer-mediated communication subfield), psychology, sociology and social studies of science. Journals that publish articles in several of the topic areas include (listed in alphabetical order): _American Sociological Review_, _Communications of the ACM_, _Information Research_, _Journal of the American Society for Information Science and Technology_, _Journal of Documentation_, _Journal of Engineering and Technology Management_, and _Social Studies of Science_. Journals that publish articles on one of the topics include: _Scientometrics_ and _Journal of Informetrics_ (scholarly communication), _Human-Computer Interaction_ (scientific collaboratories), _Information Systems Journal_, _Research Policy_, and _Strategic Information Systems_ (virtual communities). Research conferences that cover the six topics include: the _ACM GROUP conference_, the _ACM Conference on Computer Supported Cooperative Work (CSCW)_, the _European Conference on Computer Supported Cooperative Work (ECSCW)_, the _International Conference on Intercultural Collaboration (ICIC)_, and the _Science of Team Science Conference_.

Of course there are also relevant monographs and books covering the six topics. These include a number of national reports on collaboratories, primarily from the United States and United Kingdom (e.g., [Berman and Brady 2005](#berm05), [Pothen 2007](#poth07), [Our cultural commonwealth 2006](#our06)). The _Annual Review of Information Science and Technology_ contains several excellent review articles on scientific collaboration, collaboratories, scholarly communication, invisible colleges and virtual communities. Other notable books include: _Scientific collaboration on the Internet_ ([Olson _et al._ 2008](#olso08b)), _The hand of science: academic writing and its rewards_ ([Cronin 2005](#cron05 )), _Invisible colleges: diffusion of knowledge in scientific communities_ ([Crane 1972](#cran72)), _Little science, big science - and beyond_ ([Price 1963](#pric63)), _Scholarship in the digital age: information, infrastructure, and the Internet_ ([Borgman 2007](#borg07)), and _Online communities: designing usability, supporting sociability_ ([Preece 2000](#pree00)).

As can be seen in the overview presented above, there are overlaps in topics and questions addressed in the literatures that examine scientific processes and organization of science in both traditional and e-Science contexts. We propose it is useful to examine and synthesize these literatures to identify different types of factors that impact the design and use of scientific collaboratories.

## Factors

From an in-depth review of the literature of the topics presented above, six types of factors appear to impact the design and use of a scientific collaboratory (see Table 2). The factors are categorized according three aspects of science, i.e., progress, social and economic. Each aspect is considered from an individual and group focus or perspective.

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Six types of factors which may impact the adoption and use of a collaboratory**</caption>

<tbody>

<tr>

<th rowspan="2" valign="middle">_Level of focus_</th>

<th colspan="4">_Aspects of Science_</th>

</tr>

<tr>

<th colspan="1" valign="middle">Progress</th>

<th colspan="1">Social</th>

<th colspan="1">Economic</th>

</tr>

<tr>

<td align="center">_Individual_</td>

<td align="center">Career</td>

<td align="center">Personal</td>

<td align="center">Cost of participation</td>

</tr>

<tr>

<td align="center">_Group_</td>

<td align="center">Disciplinary and  
scientific advancement</td>

<td align="center">Community</td>

<td align="center">Cost to develop and sustain</td>

</tr>

</tbody>

</table>

Progress in science is increasingly important for individuals and groups. If progress cannot be shown individuals and groups may not receive resources required to conduct research. For individuals _career factors_ emanate from scientific reward systems, including measures of success such as publication counts, citations, and acknowledgments, which can impact a researcher's career. Can a collaboratory have a positive influence on a scientist's career progress? For groups _disciplinary and scientific advancement factors_ concern how progress is achieved and perceived within a group or discipline through the use of a collaboratory.

Social factors play a key role in science because science is conducted by humans. On an individual level _personal factors_ such as the desire to have fun and learn new things influence an individual's willingness to participate in a collaboratory. At the group level _community factors_ focus on the importance of a sense of community among researchers and how that sense may be influenced by a collaboratory.

Economic factors play a key role in the adoption and use of many innovations, including collaboratories. For individual scientists _cost of participation factors_ concern whether the benefits of submitting to and using a collaboratory is worth the cost for the individual. For groups the _cost to develop and sustain factors_ concern the costs of developing and sustaining a collaboratory for a community or discipline.

Each type of factor may positively and/or negatively influence a researchers' willingness to contribute to a collaboratory, use a collaboratory, or help maintain a collaboratory. Each factor and the previous research it emanates from are discussed below.

### Career factors

Scientists may use and contribute to a collaboratory when doing so will advance their careers. The reward system of academia traditionally gives credit to publications and citations in scholarly publications. By making research resources available in a collaboratory possibilities increase of getting more citations and acknowledgements. Apart from the traditional rewards, information and communication tools for research enable other types of rewards, such as statistics regarding times accessed, number of downloads, and use of resources. Although these types of statistics do not always count in formal career assessments today, they add a value in making possible other types of rewards, such as invitations to speak at conferences.

Many research areas use co-authorship to reward colleagues for various kinds of contributions to a publication, including use of a colleague's data ([Birnholtz and Bietz 2003](#birn03)). Co-authored publications are generally cited more and for longer periods of time ([Beaver 2004](#beav04); [Sonnenwald 2007](#sonn07)), which makes collaboration a good strategy for career advancements. In the natural sciences, co-authorship has been common for a long time. The same cannot be said for library and information science: a comparison of co-authorship presented by Cronin ([2005](#cron05)), drawing on two separate studies of roughly the same time period, showed that the number of authors per article in the Science Citation Index was 1.83 in 1955 and 3.9 in 1999 ([Cronin 2005](#cron05): 47), whereas the corresponding numbers for the _Journal of the American Society for Information Science_ (starting from the time the journal was called _American Documentation_) were ~1.2 in the 1950s and 1.8 in the 1990s ([Koehler _et al_. 2000](#koeh00)). Thus, the field still did not come up to the average number of co-authors in the 1990s as the sciences had in the 1950s. It should be noted, however, that the studies used different sets of data; the sciences study was based on a citation index, and the ibrary and information study using only one journal. The journal is however one of the core journals in the field, thus possibly making it a relatively fair representation. A reward system built upon publication and citation counts needs to support co-authorship in order to encourage researchers to pursue co-authorship. Meho and Spurgin ([2005](#meho05)) conducted a study of current practices of rankings of information schools and faculty, including an analysis of databases which index their research publications. They found that some '_citation databases provide credit only to the first author_' ([Meho and Spurgin 2005](#meho05): 1316). Unfortunately, Meho and Spurgin's article does not specify which databases used this strategy, but it can nevertheless have an impact on the perceived benefits of collaboration and co-authorship.

As library and information science is interdisciplinary, there are research areas within the discipline that are more prone to co-authorship, e.g., within the information retrieval research. Information retrieval would fit the description of characteristics discussed by Moody ([2004](#mood04)), in his investigation of a sociology collaboration network as reflected by citations: '_coauthorship is more likely in specialties that admit to an easier division of labor. Research methods seem particularly important, showing that quantitative work is more likely to be coauthored than non-qualitative work_.' ([Moody 2004](#mood04): 235)

Complementing citations and co-authorship, another way of acknowledging a contributor of a research project is to mention them in the acknowledgement section of a publication. The status of acknowledgements as a career factor is unclear: Cronin ([2005](#cron05)) posed the open question in his book _The hand of science: academic writing and its rewards_ why acknowledgements are treated differently from citations in how they can influence career advancements (such as the tenure review process common in the USA). Cronin ([1991](#cron91), [2005](#cron05)) found that about 50% of the articles in the _Journal of the American Society for Information Science and Technology_ over a period of twenty-five years included acknowledgements with thanks to colleagues for inspiration, feedback and other contributions.

Building upon colleagues' work, e.g., by using an existing data collection instrument instead of constructing one, can mean increased efficiency in the research process. The saved time and effort can be devoted to other parts of the process, such as data analysis, or could enable quicker publishing of results contributing to one's career progress. For example, a collaboratory for AIDS research provided a collaborative environment that allowed the collaboratory members to construct a clinical protocol in hours, instead of days that the process typically would take outside of the collaboratory framework ([Finholt 2002](#finh02)). Members of collaboratories also have a chance to learn from more experienced researchers ([Sonnenwald 2007](#sonn07); [Finholt 2002](#finh02)), for example by observing how the clinical protocol of the AIDS research collaboratory was constructed; or by observing how an experiment is conducted. Researchers can move ahead quicker by building on others' work, which may lead to a situation of competition between the researcher who has spent much time to collect a data set, and the researcher who can skip steps in the research process and start analyzing data immediately.

Scientists can find that the prestige that comes with belonging to a particular community can have a positive effect on career advancements, as the case of SCIENCEnet shows ([Finholt 2002](#finh02)). SCIENCEnet was created in the 1980s to provide access to data and create a network for oceanographers in different geographic locations ([Finholt 2002](#finh02)). Results of a study of SCIENCEnet showed that scientists who frequently used the collaboratory were more active and productive in their research; they worked at more prestigious institutions; received more professional recognition; published more;, and had a bigger network ([Hesse _et al_., 1993](#hess93)). In the case of the space physics research collaboratories the Upper Atmospheric Research Collaboratory (UARC) and the [Space Physics and Astronomy Research Collaboratory](http://www.si.umich.edu/sparc/) (SPARC), which was its successor, researchers who had access to particular data sources had high status. The high status members could choose whether or not they wanted to co-author publications with colleagues who wished to use their data, as well as other terms of use of the data ([Birnholtz and Bietz 2003](#birn03)). However using data collected by a colleague may have a negative influence on a researcher's career. In earthquake engineering, which was described by Birnholtz and Bietz ([2003](#birn03)), a researcher who used someone else's data would experience negative effects on their status in the research community and their chances of getting published lessened. Birnholtz and Bietz suggest that disciplines with low task uncertainty do not have this problem since there is much agreement on what the problems to be studied are, and how to study them. With high task uncertainty comes less agreement among the community of researchers, and the methods of data collection differ more, making it more difficult to use data collected for other projects. Library and information science could best be described as a discipline with high task-uncertainty, though some sub-disciplines, such as information retrieval, might have a low task-uncertainty.

Academic integrity may not always be present, and is a threat to sharing resources because others might use a researcher's work without citing it or otherwise giving appropriate credit to the researcher. For example, in the [Worm Community System](http://www.canis.uiuc.edu/projects/wcs/index.html) collaboratory the postdoctoral researchers were worried about having their research data and results scooped, and so they did not contribute their work to the community. This has been identified as one of a series of factors leading to the Worm Community System collaboratory not becoming the success that was anticipated ([Star and Ruhleder 1994](#star94)). Other research fields have learned from this example. For example in the brain research collaboratory new members were required to sign a 'covenant' that stipulated how the data within the collaboratory could be used ([Finholt 2002](#finh02)). In the UARC and SPARC collaboratories there were a set of _rules of the road_, which stipulated the use of data, such as '_rights to first publication and mechanisms for sharing credit, such as to instrument owners_' ([Finholt 2002](#finh02): 94). In the high energy physics community, the concern of not getting credited for one's data or data analysis has been solved by listing all people involved in a project as authors ([Birnholtz 2006](#birn06)). There are still open questions on how to solve issues of assigning appropriate credit. In a report the development of a national e-infrastructure for science and innovation in the UK, the importance of a credit system that covers more than publications is stressed, though no solutions were proposed ([Pothen 2007](#poth07)).

In sum an effect of contributing to a collaboratory is that members can find prospective collaborators, access relevant resources, or find published work that they end up citing. In the same way as _success breeds success_, it is possible that resources which are frequently downloaded and cited lead to more downloads and citations. Potential threats to scientists wanting to use and contribute to a collaboratory include: whether collaboration as reflected by co-authorship is recognized and condoned by the discipline and community; concerns of competition with colleagues who might use one of their resources and gain time advantages; concerns of not getting credit when a colleague has used one's resources; and that participating in a collaboratory might not guarantee inclusion in the invisible college.

### Personal factors

Scientists may use and support a collaboratory for personal reasons, with few or no expectations regarding subsequent rewards or benefits from their interaction with the collaboratory. Personal reasons reported in previous literature for participating in virtual communities and scientific collaboration include: the opportunity to learn and share expertise; enjoyment from interacting with others; satisfaction from a sense of belonging to a community; and, prestige from membership.

Learning and sharing expertise can be important from a personal perspective. For example when studying the Apache Web server software support community, an open source community, Lakhani and von Hippel ([2003](#lakh03)) found that members who provided technical support to the community felt that 98% of their effort provided learning benefits for themselves. Members answered the questions they knew the answers to, and so the cost of answering was very low, and provided many opportunities to gain new knowledge ([Lakhani and von Hippel 2003](#lakh03)). Similarly a study of Usenet newsgroups showed that discussions within newsgroups often provided added value for members, that is, community members reported that original ideas were often augmented and enhanced through discussions, providing learning opportunities and added value for members ([Wasko and Faraj 2000](#wask00)). Scientists have also reported that one of the benefits from interdisciplinary collaborations is the opportunity to learn from colleagues in other disciplines ([Maglaughlin and Sonnenwald 2005](#magl05)).

People often enjoy working with others. The opportunity to have fun interacting with others has been found to motivate people to join open source software development communities ([Lakhani and von Hippel 2003](#lakh03)) and scientific collaborations ([Kraut _et al._ 1988](#krau88); [Maglaughlin and Sonnenwald 2005](#magl05)). Bates ([1999](#bates99)) reports that humour is an outstanding feature of the library and information science community so perhaps its researchers may enjoy participating in a collaboratory more than those in other disciplines.

It can be personally satisfying to have a sense of belonging to a community, and this has been found to be a reason why individuals may join and participate in a virtual community ([Preece 2000](#pree00)). This is connected to people's willingness to support the community, e.g., by sharing information. The stronger a person's sense of belonging to a virtual community, the more likely they are to actively participate and support that community.

Prestige from belonging to a community emanates from within the community and from people outside the community who hold the community in high regard. Within a virtual community or collaboratory, prestige may emerge in several different ways. Statistics regarding access and downloads of an individual's contributions such as data files or workflows that are highlighted within the community can provide prestige. For example [myExperiment](http://www.myexperiment.org/) ([De Roure _et al._ 2009](#dero09)) provide an example of this, showing statistics of how many times a shared workflow has been viewed and download. Prestige may also come from the quality and quantity of an individual's contributions ([Ellis _et al._ 2004](#elli04)), such as the number of questions an individual has answered in 'Frequently Asked Questions' ([Donath, 1998](#dona98)). A third source includes rating points given by other members, for example in the Eureka system at the Xerox Corporation ([Dutta _et al._ 2000](#dutt00)). Such data provide personal satisfaction. In addition prestige from within a community may lead to activities that are more widely recognized and rewarded in academia. For example, a prestigious member in a collaboratory may be invited by other members to discuss their work at prestigious academic conferences.

### Cost of participation factors

Costs of participating in a collaboratory can be measured by: monetary costs; the time required to participate; and the effort needed. Costs are, of course, relative to the perceived benefits of use. The perceived benefits should exceed the time and effort needed to use the collaboratory.

In a review of previous collaboratory initiatives, Finholt ([2002](#finh02)) suggests that a key feature of a successful collaboratory is that it is _as useful and invisible as possible, in order to allow users to maximize their focus on conducting research_ ([Finholt 2002](#finh02): 81). Among the lessons learned from the review of collaboratories, Finholt found that difficult installation procedures and a collaboratory based on a computing environment that most researchers in the community do not use have prevented collaboratories from becoming widely used, even though scientists have been positive to the collaboratory initiative beforehand. ([Finholt 2002](#finh02))

The concept _contribution barrier_ proposed by von Krogh, Spaeth and Lakhani ([2003](#vonk03)) refers to barriers newcomers in open source software development projects encounter when contributing and becoming active members. Transferred to the context of sharing research resources in a scientific collaboratory, potential contribution barriers could include: complex submission processes; requirements for submitter to thoroughly describe their submission with complex and exhaustive metadata; and providing support for a limited type of resource such as file or program type.

Furthermore a collaboratory that supports the members' language will be more likely to provide members with the benefits of using a collaboratory ([Olson _et al_. 2008](#olso08a)). A collaboratory should ideally support members' preferred means of communication and support for variations in the vocabulary used by the members. If there are gaps in the meta-data or thesauri used within a collaboratory, so that some resources cannot be described in full from the perspective of the members, members might be reluctant to submit their resources.

### Disciplinary and scientific advancement factors

Scientists may collaborate and share research resources to advance their disciplines and science in general. In some disciplines collaboration and sharing research resources are integrated into research practices in the small, by members of research groups collaborating on projects, and in the large, by researchers sharing their research data and publications in repositories and collaboratories. Today collaboration within and between research areas is strongly encouraged by funding agencies ([Pothen 2007](#poth07); [Berman and Brady 2005](#berm05)). The social sciences ([Berman and Brady 2005](#berm05); [Our cultural commonwealth 2006](#our06)) and the humanities ([Our cultural commonwealth 2006](#our06)) are particularly encouraged now to use cyberinfrastructure (such as a collaboratory) to collaborate. It has been suggested that there are particular characteristics of disciplines that make them prone to collaboration: a high degree of mutual dependence among the researchers in the area ([Birnholtz and Bietz 2003](#birn03)); a low degree of task-uncertainty within the area ([Birnholtz and Bietz 2003](#birn03)); and collaboration readiness, collaboration infrastructure readiness, and collaboration technology readiness ([Olson _et al_. 2008](#olso08b); [Olson _et al_. 2002](#olso02)).

Large scale benefits of scientific collaboration include: development of new branches of science; increased reliability of research; and increased credibility for individual researchers ([Sonnenwald 2007](#sonn07); [Sonnenwald 2003](#sonn03); [Olson _et al_. 2008](#olso08b)). Increased reliability of research and increased credibility for individuals may aid in gaining recognition for a research area, group or paradigm ([Beaver 2001](#beav01); [Sonnenwald 2007](#sonn07)). Diversity, such as cultural, national, and interdisciplinary diversity, may lead to bigger discoveries, conceptual revolutions and new models of science ([Olson _et al_. 2008](#olso08b)). A collaboratory may provide a platform for scientific collaboration and may aid researchers in finding new collaborators, thus overcoming obstacles geographical distance typically entail ([Sonnenwald 2007](#sonn07); [Olson et al. 2008](#olso08b)).

As collaboration leads to results being published quicker (see e.g. [Beaver 2004](#beav04); [Frenken _et al._ 2005](#fren05); [Olson _et al_. 2008](#olso08b); [Sonnenwald 2007](#sonn07)), the scientific community can make progress more rapidly. Areas focusing on solving urgent health problems and environmental problems are particularly dependent on rapid progress. In general collaboration can be driven by researchers' visions of a better world ([Sonnenwald 2003](#sonn03)).

Students and junior researchers can learn from using collaboratories. There are opportunities to learn how to collaborate with peers on the same level of education, and with senior researchers, as well as to study the research process in research performed by senior researchers ([Finholt 2002](#finh02); [Sonnenwald 2007](#sonn07)).

In sum motivations to collaborate with regards to advancing science and disciplines reported in the literature include: solving important problems; a vision of a better world; increased credibility and reliability through collaboration; new results can be published quicker; advancements in science can be made quicker; learning and education can be facilitated; and development of science and disciplines can be promoted by researchers using new theories, methods and approaches.

### Community factors

Motivations for contributing to a community has been studied in several contexts (see e.g. [Preece 2000](#pree00)). A sense of belonging to a community has been identified as a motivator for contributing and being active in a Usenet newsgroup ([Wasko and Faraj 2000](#wask00)), and similarly for members wanting to support the community ([Lakhani and von Hippel 2003](#lakh03)). The Xerox company with employees in several countries used the Eureka system to facilitate information sharing, and the system included consideration of the different languages and cultures of the employees in order to facilitate information sharing across national, linguistic and cultural boundaries ([Dutta _et al._ 2000](#dutt00); [Ellis _et al._ 2004](#elli04)). In an academic context, it is important that members' research areas are supported in the community, and that the community caters to the members' academic cultures and languages. For example the vocabulary used for communication should reflect the vocabulary used by academic community not the system developer's vocabulary.

Motivations to contribute in the open source software development community has been studied with particular interest (e.g., [Lakhani and von Hippel 2003](#lakh03)) to understand why members of open source software development projects contribute to projects without receiving monetary compensation. The motivations for being active in a community through sharing information, answering questions, and developing code have been found to be a mixture of altruism and self-interest. Altruism is manifested through members sharing information and their knowledge to other members. Self-interest is manifested through an expectation to get something back from other members of the community ([Ellis _et al._ 2004](#elli04)). A concept that captures both altruism and self-interest is 'generalized reciprocity' which means that a reciprocal action can come from someone else other than the person you have helped in the past ([Lakhani and von Hippel 2003](#lakh03)). For example, you might provide an answer to a question posed by a community member in a forum. At a later time, you pose a question in the forum, and receive a reply from a third community member. You have helped a community member and get paid back by help from another community member. The importance of reciprocity to motivate information sharing has also been reported in virtual community research ([Preece 2000](#pree00); [Rheingold 1995](#rhei95)) and computer supported collaborative work research ([Clement, 1990](#clem90); [Nardi and Harris 2006](#nard06)). Ellis _et al._ call the reciprocity occurring in information sharing '_the currency in the community economy_' ([Ellis _et al._ 2004](#elli04): 147).

A related type of motivation is _gift-giving_ which has been studied thoroughly in open source software development communities ([Bergquist and Ljungberg 2001](#berg01); [von Krogh _et al._ 2003](#vonk03); [Lakhani and von Hippel 2003](#lakh03)). Gifts that have been identified in these communities include software code, ideas, and proposals to solve problems in a development project. Eckstein ([2001](#ecks01)) argue that gift-giving is common in communities in which sharing is a norm. In scientific communities, gifts could include co-authorship, data collection instruments, data sets, citations; and acknowledgements.

For communities to be successful they must establish a critical mass of community members ([Markus 1994](#mar94)) who preferably sharing a strong common belief, e.g., the Linux community's belief in open source software code ([Preece 2000](#pree00)) which, in turn, can attract new members. The size and structure needed to establish a critical mass for a community varies for different types of communities and the purposes of the communities ([Preece 2000](#pree00)). While some members are more active than the typical community member, some members will be lurkers, who observe but do not participate actively. In some communities up to ninety percent of the members are lurkers ([Nonnecke and Preece, 1999](#nonn99)). Lurking is not necessarily a problem: in a collaborative project, junior researchers can observe the work conducted by senior researchers, thus getting valuable learning experiences ([Finholt 2002](#finh02); [Sonnenwald 2007](#sonn07)).

The level of trust among the members of a community may affect the degree to which members are active and what type of information they share amongst each other ([Preece 2000](#pree00); [van House _et al_. 1998](#vanh98)). Kraut, Galegher and Egido ([1988](#krau88)) found that having met in person before deciding to collaborate would make scientists more confident in their decision to collaborate, as they could anticipate the compatibility between the collaborators before the project started. Other ways to establish trust in scholarly communities include reading, acknowledging and building upon other researchers' work.

In sum factors that have been reported to influence community members' will to contribute to the community include: whether members feel they are part of the community identity; members' rate of contributing to the community; whether the community has a critical mass of members that keeps the community going; the level of trust among community members; and ease of understanding and mastering the culture and language of the community.

### Cost to develop and sustain factors

Issues concerning the costs to develop and sustain infrastructures for sharing resources still need to be investigated. Development costs typically include: conducting requirement analysis; hardware and software costs; salaries for programmers and other staff involved in the development process; and costs to recruit a critical mass of members for the community. Sustainment costs include: telecommunications network costs; software maintenance costs; salaries to staff involved in day to day operations; and keeping a critical mass of members of the community, as well as recruiting new members.

Open questions concerning fiscal and other costs are summarized by Borgman: "Issues of who pays, for what, and at what stage of the life cycle of data have deep roots in the infrastructure for research funding and in university and governmental policies and practices. Many of these differ by country and discipline." ([Borgman 2007](#borg07): 140). Borgman concludes that the economic aspects are very complex and need to be further investigated. This issue was one of the pressing issues identified in the National Science Foundation's Workshop on Cyber-infrastructure and the Social Sciences ([Berman and Brady 2005](#berm05)). The workshop summary suggested that although a cyber-infrastructure to facilitate collaboration across distances can reduce costs for research and advance science, there is a need for social scientists to investigate: '_What are the fiscal costs and social, technical, and organizational benefits of candidate governance structures for Cyberinfrastructure?_' ([Berman and Brady 2005](#berm05): 29).

## Conclusions

Experiences from collaboratories in other disciplines can help inform and inspire collaboration in disciplines such as library and information science which do not have long traditions in collaboration across distances. Table 3 summarizes facilitators and benefits with respect to the adoption and use of a scientific collaboratory.

<table width="70%" border="1" cellspacing="0" cellpadding="5" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Summary of facilitators and benefits with respect to the adoption and use of scientific collaboratories.**</caption>

<tbody>

<tr>

<th>Types of factors</th>

<th>Facilitators and benefits</th>

</tr>

<tr>

<td align="center">_Career_</td>

<td>* Prestige of belonging to a community  
* Understanding and mastering the culture and language  
* The academic reward system: citations, acknowledgements, usage statistics; co-authorship leads to more citations for longer periods of time  
* Increased efficiency by using existing resources  
* Learning from colleagues' work</td>

</tr>

<tr>

<td align="center">_Personal_</td>

<td>* Belonging to, and supporting a community  
* Prestige of being a member of a community  
* Learning opportunities  
* Getting recognition through usage statistics, download statistics etc  
* Having fun</td>

</tr>

<tr>

<td align="center">_Cost of participation_</td>

<td>* Understanding and mastering the culture and language</td>

</tr>

<tr>

<td align="center">_Disciplinary and scientific advancement_</td>

<td>* Discipline characteristics: collaboration readiness; collaboration infrastructure readiness; collaboration technology readiness; high degree of mutual dependence; low degree of task-uncertainty  
* Vision of a better world, solving important problems  
* Increased quality of research: reliability, validity, credibility  
* Faster advancements - new results published faster  
* Diversity among scientists can lead to new developments: new branches of science; new models of science; conceptual revolutions  
* Funding agencies encouraging collaboration  
* Learning opportunities for students and junior researchers</td>

</tr>

<tr>

<td align="center">_Community_</td>

<td>* Members feeling a sense of belonging to the community, and wanting to support it: reciprocity, gift-giving  
* Members understanding and mastering the culture and language  
* Establishing a critical mass of active users  
* Trust amongst community members</td>

</tr>

<tr>

<td align="center">_Cost to develop and sustain_</td>

<td>* Economic issues solved, such as who provides for development and maintenance</td>

</tr>

</tbody>

</table>

The synthesis of previous research presented in this paper suggests that there can be many benefits to a collaboratory that facilitates sharing digital resources and supporting interaction among researchers across geographic distances. There are benefits for researchers' careers as well as for researchers personally, for the advancement of science and disciplines, and for scholarly and professional communities. The perceived and expected benefits will have to be greater than the concerns and potential barriers that could make a researcher or community hesitant to share resources. Table 4 summarizes the concerns with respect to the adoption and use of a scientific collaboratory.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Summary of concerns with respect to the adoption and use of scientific collaboratories.**</caption>

<tbody>

<tr>

<th>Types of Factors</th>

<th>Concerns</th>

</tr>

<tr>

<td align="center">_Career_</td>

<td>* The academic reward system: low prestige in using other researchers' resources; recognition only to first author in citation analyses  
* Not getting credit when others use your resources  
* Competition - others may use your resources and publish before you do</td>

</tr>

<tr>

<td align="center">_Cost of participation_</td>

<td>* Economic costs, such as: equipment, operating systems, software, Internet connection)  
* Contribution barriers, such as: complex procedures for sharing; few supported file types; complex and exhaustive metadata schemes</td>

</tr>

<tr>

<td align="center">_Community_</td>

<td>* Too many passive members (lurkers) compared to active members</td>

</tr>

<tr>

<td align="center">_Cost to develop and sustain_</td>

<td>* Development costs, such as requirement analysis  
* Hardware and software costs  
* Salaries for staff and developers  
* Recruiting a critical mass of members</td>

</tr>

</tbody>

</table>

Encouraging researchers to share resources may require a mix of new and traditional ways to acknowledge research contributions. The traditional academic reward system is based on publications and citations, and a change towards broadening the types of rewards that matter for researchers' careers may take time. However, steps in this direction have been taken by for example myExperiment.com and the [ACM Digital Library](http://portal.acm.org/) that report the number of downloads and other reuse statistics. In addition, as virtual community research has shown, having a sense of belonging to a collaboratory, and thus wanting to contribute by gift-giving and reciprocity can facilitate the adoption and use of a collaboratory.

It is vital for a collaboratory to support members' culture, styles of communication, and languages. This helps to create a critical mass of researchers who share resources, and to establish trust amongst them. Trust among members enables them to learn from each other, openly share resources and increase the efficiency of the research process. When trust is lacking, especially in disciplines where time to submit for publication matters very much, the competition among researchers to be the first to publish could make them reluctant to share their resources. It is also vital that the procedures for participating in a collaboratory, e.g., uploading a digital resource into a collaboratory, require little effort since free time is scarce in research.

The taxonomy presented in this paper provides a theoretical framework to guide future research which explores the adoption and use of a collaboratory in disciplines not yet studied, e.g., library and information science. As disciplines have different characteristics, including their reward systems, ways of communicating, and ways of conducting research, a collaboratory which is successful in a natural science discipline might or might not be successful in a social science discipline, such as library and information science. To explore this we are currently conducting interviews with researchers, students and professionals in that field to learn about their current practices of sharing resources, and the potential benefits and concerns that they see for a collaboratory within the area. Analysis of the interviews will contribute to the reliability and generalizability of the taxonomy.

The taxonomy also provides the foundation for a predictive theory of collaboratory design. Theories for predicting describe what will be, predicting "outcomes from a set of explanatory factors" ([Gregor 2006](#greg06): 625). The taxonomy presented here provides a concise overview of explanatory factors concerning the adoption and use of collaboratories. This knowledge is crucial in creating a predictive theory that explicates the design of scientific collaboratories to facilitate their adoption and use.

Collaboratories may not produce changes in the research we conduct ([Finholt 2002](#finh02)). However they may produce changes in the way we conduct research, introducing a new rhythm to our research. This new rhythm could include reuse of research materials to avoid duplication of effort; new and sustained relationships among researchers that increase researchers' personal satisfaction, leading to the creation of new knowledge enhancing progress in scientific disciplines ([Sonnenwald 2003](#sonn03)).

## Acknowledgements

This work is funded by the Swedish National Graduate School of Language Technology and the Swedish School of Library and Information Science. Our thanks to the anonymous CoLIS conference referees.

## About the authors

Monica Lassi is a lecturer and a Ph.D. candidate at the Swedish School of Library and Information Science. She teaches Web communication, social media and social interaction, knowledge organization, and content management. She is conducting a Ph.D. research project on collaboration in library and information science. She is also a member of the Swedish National Graduate School of Language Technology, University of Gothenburg. She can be contacted at: [monica.lassi@hb.se](mailto:monica.lassi@hb.se)

Diane H. Sonnenwald is Head of School and Professor at the School of Information and Library Studies at University College, Dublin, Ireland and an Adjunct Professor of computer science at the University of North Carolina at Chapel Hill. She conducts research on collaboration and collaboration technology in a variety of contexts, including inter-disciplinary, inter-organizational and distributed collaboration in emergency healthcare, academia and industry. She can be contacted at: [diane.sonnenwald@ucd.ie](mailto:diane.sonnenwald@ucd.ie)

#### References

*   Bates, M.J. (1999). The invisible substrate of information science. _Journal of the American Society for Information Science_, **50**(12), 1043-1050.
*   Beaver, D.D. (2001). Reflections on scientific collaboration (and its study): Past, present, and future. _Scientometrics_, **52**(3), 365-377.
*   Beaver, D.D. (2004). Does collaborative research have greater epistemic authority? _Scientometrics_, **60**(3), 399-408.
*   Bergquist, M. & Ljungberg, J. (2001). The power of gifts: organizing social relationships in open source communities. _Information Systems Journal_, **11**(4), 305-320.
*   Berman, F. & Brady, H. (2005). [Final Report: NSF SBE-CISE Workshop on Cyberinfrastructure and the Social Sciences.](http://www.webcitation.org/5sjQayTUh) Retrieved 14 September, 2010 from http://ucdata.berkeley.edu/pubs/CyberInfrastructure_FINAL.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5sjQayTUh)
*   Birnholtz, J.P. (2006). What does it mean to be an author? The intersection of credit, contribution, and collaboration in science. _Journal of the American Society for Information Science and Technology_, **57**(13), 1758-1770.
*   Birnholtz, J.P. & Bietz, M.J. (2003). Data at work: Supporting sharing in science and engineering. In _Proceedings of the 2003 international ACM SIGGROUP conference on Supporting group work_, GROUP'03 conference, November 9-12 2003, Sanibel Island, Florida, USA. (pp. 339-348). New York, NY: ACM Press.
*   Borgman, C.L. (2007). _Scholarship in the digital age: information, infrastructure, and the internet_. Cambridge, MA: MIT Press
*   Borgman, C.L. & Furner, J. (2002). Scholarly communication and bibliometrics. _Annual Review of Information Science and Technology_, **36**), 2-72
*   Clement, A. (1990). Cooperative support for computer work: a social perspective on the empowering of end users. In _Proceedings of the 1990 ACM conference on Computer-supported cooperative work_, CSCW'90, Los Angeles, CA, USA. (pp. 223-236) New York, NY: ACM Press.
*   Clement, A. & Halonen, C. (1998). Collaboration and conflict in the development of a computerized dispatched facility. _Journal of the American Society for Information Science_, **49**(12), 1090-1100.
*   Crane, D. (1972). _Invisible colleges: diffusion of knowledge in scientific communities_. Chicago, IL: University of Chicago Press.
*   Crawford, S.Y., Hurd, J.M. & Weller, A.C. (1996). _From print to electronic: the transformation of scientific communication_. Medford, NJ: Information Today.
*   Cronin, B. (1991). Let the credits roll: a preliminary examination of the role played by mentors and trusted assessors in disciplinary formation. _Journal of Documentation_, **47**(3), 227-239.
*   Cronin, B. (2005). _The hand of science: academic writing and its rewards_. Lanham, MD.: Scarecrow Press.
*   De Roure, D., Goble, C. & Stevens, R. (2009). The design and realisation of the virtual research environment for social sharing of workflows. _Future Generation Computer Systems_, **25**(5), 561-567.
*   Donath, J. (1998). Identity and deception in the virtual community. In M. Smith and P. Kollock, (Eds.). _Communities in cyberspace_. (pp. 29-59). London: Routledge.
*   Dutta, S., Biren, B. & van Wassenhove, L. (2000). _Xerox: building a corporate focus on knowledge_. Bedford, UK: European Case Clearing House, Cranfield University.
*   Eckstein, S. (2001). Community as gift-giving: collectivistic roots of volunteerism. _American Sociological Review_, **66**(6), 829-851.
*   Ellis, D., Oldridge, R. & Vasconcelos, A. (2004). Community and virtual community. In B. Cronin (Ed.), _Annual Review of Information Science and Technology_, **38**, 145-186.
*   Finholt, T.A. (2002). Collaboratories. In B. Cronin (Ed.), _Annual Review of Information Science and Technology_, **36**, 73-107.
*   Frenken, K., Hölzl, W. & de Vor, F. (2005). The citation impact of research collaborations: the case of European biotechnology and applied microbiology (1988-2002). _Journal of Engineering and Technology Management_, **22**(1-2), 9-30.
*   Gregor, S. (2006). The nature of theory in information systems. _MIS Quarterly_, **30**(3), 611-642.
*   Hesse, B. W., Sproull, L. S., Kiesler, S. B. & Walsh, J. P. (1993). Returns to science: computer networks in oceanography. _Communications of the ACM_, **36**(8), 90-101.
*   Kimble, C., Hildreth, P. & Wright, P. (2001). Communities of practice: going virtual. In Yogesh Malhotra (Ed.). _Knowledge management and business model innovation_ (pp. 220-234). London: Idea Group Publishing.
*   Koehler, W., Aguilar, P., Finarelli, S., Gaunce, C., Hatchette, S., Heydon, R., _et al_. (2000). [A bibliometric analysis of select information science print and electronic journals in the 1990s.](http://informationr.net/ir/6-1/paper88.html) _Information Research_, **6**(1). Retrieved 12 July 2010 from http://informationr.net/ir/6-1/paper88.html
*   Kraut, R. E., Galegher, J. & Egido, C. (1988). Relationships and tasks in scientific research collaboration. _Human-Computer Interaction_, **3**(1), 31-58.
*   Lakhani, K. R. & von Hippel, E. (2003). How open source software works: "free" user-to-user assistance. _Research Policy_, **32**(6), 923-943.
*   Maglaughlin, K.L. & Sonnenwald, D.H. (2005). Factors that impact interdisciplinary scientific research collaboration: Focus on the natural sciences in academia. _Proceedings of the 10th International Conference of the International Society for Scientometrics and Informetrics_.
*   Markus, L. M. (1994). Electronic mail as the medium of managerial choice. _Organization Science_, **5**(4), 502-527.
*   Meho, L. I. & Spurgin, K.M. (2005). Ranking the research productivity of library and information science faculty and schools: an evaluation of data sources and research methods. _Journal of the American Society for Information Science and Technology_, **56**(12), 1314-1331.
*   Montgomery, S.L. (2003). _The Chicago guide to communicating science_. Chicago, IL: University of Chicago Press.
*   Moody, J. (2004). The structure of a social science collaboration network: disciplinary cohesion from 1963 to 1999\. _American Sociological Review_, **69**(April) 213-238.
*   Nardi, B. & Harris, J. (2006). Supporting social play: strangers and friends. Collaborative play in World of Warcraft. In _Proceedings of the 2006 20th anniversary conference on Computer supported cooperative work_, CSCW'06\. Banff, Alberta, Canada. (pp. 149-158). New York, NY: ACM Press.
*   Neale, D.C., Carroll, J.M. & Rosson, M.B. (2004). Evaluating computer-supported cooperative work: models and frameworks. In _Proceedings of the 2004 ACM conference on Computer supported cooperative work_, CSCW'04\. Chicago, Illinois, USA. (pp. 112-121). New York, NY: ACM Press.
*   Nonnecke, B. & Preece, J. (1999). _Shedding light on lurkers in online communities._ Paper presented at the Esprit i3 Workshop on Ethnographic Studies in Real and Virtual Environments: Inhabited Information Spaces and Connected Communities, 24-26 January, Edinburgh, UK.
*   Olson, J.S., Hofer, E., Bos, N., Zimmerman, A., Olson, G.M., Cooney, D., _et al_. (2008). A theory of remote scientific collaboration (TORSC). In Gary M. Olson, Ann Zimmerman & Nathan Bos, (Eds.) (2008). _Scientific collaboration on the Internet_. Cambridge, MA: The MIT Press.
*   Olson, G.M., Teasley, S., Bietz, M. J. & Cogburn, D. L. (2002). [Collaboratories to support distributed science: the example of international HIV/AIDS research](http://www.webcitation.org/5sjTTVZZM). In _Proceedings of the 2002 annual research conference of the South African institute of computer scientists and information technologists on enablement through technology_. (pp. 44-51). New York, NY: ACM Press. Retrieved 14 September, 2010 from https://www.cs.usask.ca/faculty/sal426/SensorGrid/docs/GENERAL%20_E%20HEALTH%20PAPER/p44-olson.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5sjTTVZZM)
*   Olson, G. M., Zimmerman, A. & Bos, N. (Eds.) (2008). _Scientific Collaboration on the Internet_. Cambridge, MA: The MIT Press.
*   American Council of Learned Societies. _Commission on Cyberinfrastructure for the Humanities and Social Sciences_. (2006). _[Our cultural commonwealth.](http://www.webcitation.org/5sjTqUwXe)_. New York, NY: American Council of Learned Societies. Retrieved 12 July 2010 from http://www.acls.org/uploadedFiles/Publications/Programs/Our_Cultural_Commonwealth.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5sjTqUwXe)
*   Pothen, P. (2007). _[Developing the UK's e-infrastructure for science and innovation.](http://www.nesc.ac.uk/documents/OSI/report.pdf)_ London: OSI e-infrastructure Working Group. Retrieved 12 July 2010 from http://www.nesc.ac.uk/documents/OSI/report.pdf
*   Preece, J. (2000). _Online communities: designing usability, supporting sociability_. Chichester, UK: Wiley.
*   Price, S.D. (1963). _Little science, big science_. New York, NY: Columbia University Press.
*   Rheingold, H. (1995). _The virtual community: finding connection in a computerized world_ (Minerva ed.): Mandarin Paperbacks.
*   Science of Collaboratories Project. _Workshop on the Social Underpinnings of Collaboration_. (2003). _[Final summary](http://www.webcitation.org/5sjUuZBHZ)_. Retrieved 12 July 2010 from http://www.scienceofcollaboratories.org/Workshops/WorkshopJune42001/index.php?FinalSummary (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5sjUuZBHZ)
*   Shrum, W., Chompalov, I. & Genuth, J. (2001). Trust, conflict and performance in scientific collaborations. _Social Studies of Science_, **31**(4), 681-730.
*   Sonnenwald, D.H. (2003). Expectations for a scientific collaboratory: a case study. In _Proceedings of the 2003 international ACM SIGGROUP conference on supporting group work_. (pp. 68-74). New York, NY: ACM Press.
*   Sonnenwald, D. H. (2007). Scientific collaboration. In B. Cronin (Ed.), _Annual Review of Information Science and Technology_, **40**, 643-681.
*   Star, S.L. & Ruhleder, K. (1994). Steps toward an ecology of infrastructure: complex problems in design and access for large-scale collaborative systems. In _Proceedings of the 1994 ACM conference on Computer supported cooperative work_, CSCW 94\. Chapel Hill, NC, USA. (pp. 253-264). New York, NY: ACM Press.
*   van House, N., Butler, M. H. & Schiff, L. R. (1998). Cooperative knowledge work and practices of trust: sharing environmental planning data sets. In _Proceedings of the 1998 ACM conference on Computer supported cooperative work_, CSCW '98\. Seattle, Washington. (pp. 335-343). New York, NY: ACM Press.
*   von Krogh, G., Spaeth, S. & Lakhani, K. R. (2003). Community, joining, and specialization in open source software innovation: a case study. _Research Policy_, **32**(7), 1217-1241.
*   Wasko, M.M. & Faraj, S. (2000). "It is what one does": Why people participate and help others in electronic communities of practice. _Strategic Information Systems_, **9**(2-3), 155-173.
*   Wellman, B. & Gulia, M. (1999). Net surfers don't ride alone. In M. A. Smith & P. Kollock (Eds.), _Communities in cyberspace_. London: Routledge.