##### Volume 15 No 3 September, 2010

#### Proceedings of the Seventh International Conference on Conceptions of Library and Information Science, London 21-24 June, 2010

### [Guest editorial](edcolis7.html)

##### Melanie Feinberg  
[Designing collections for storytelling: purpose, pathos, and poetry](colis701.html)

##### Helena Francke and Olof Sundin  
[An inside view: credibility in Wikipedia from the perspective of editors](colis702.html)

##### Deborah Turner and Warren Allen  
[Investigating oral information](colis703.html)

##### Annemaree Lloyd  
[Corporeality and practice theory: exploring emerging research agendas for information literacy](colis704.html)

##### Jens-Erik Mai  
[Trusting tags, terms, and recommendations](colis705.html)

##### Birger Hjørland and Jeppe Nicolaisen  
[The social psychology of information use: seeking "friends", avoiding "enemies"](colis706.html)

##### Kerstin Rydbeck  
[The research circle as a meeting place for researchers and practitioners](colis707.html)

##### Christine Urquhart  
[Systematic reviewing, meta-analysis and meta-synthesis for evidence-based library and information science](colis708.html)

##### Jarkko Kari  
[Diversity in the conceptions of information use](colis709.html)

##### Monica Lassi and Diane H. Sonnenwald  
[Identifying factors that may impact the adoption and use of a social science collaboratory: a synthesis of previous research](colis710.html)

##### Brendan Luyt and Intan Azura  
[The sigh of the information literate: an examination of the potential for oppression in information literacy](colis711.html)

##### Jan Nolin and Katriina Byström  
[From HTML to XML and more? A case study of language games within portal server technology implementation](colis712.html)

