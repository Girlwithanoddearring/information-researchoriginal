<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Uncertainty in reference and information service

#### [Amy VanScoy](#author)  
Department of Library & Information Studies, University at Buffalo

#### Abstract

> **Introduction.** Uncertainty is understood as an important component of the information seeking process, but it has not been explored as a component of reference and information service.  
> **Method.** Interpretative phenomenological analysis was used to examine the practitioner perspective of reference and information service for eight academic research librarians in the United States.  
> **Analysis.** Data were analyzed thematically according to interpretative phenomenological analysis procedures.  
> **Results.** _Variety and uncertainty_ emerged as one of the themes of experience for these practitioners. Based on the results of this study, a conceptual model is proposed for uncertainty in reference and information service.  
> **Conclusion.** The conceptual model of uncertainty in reference and information service can be used for further study of the phenomenon and to contribute to a comparative sociology of uncertainty across professions. In addition, it can be used to support practitioners and to better prepare students for the uncertainties of practice.

## Introduction

Uncertainty is understood as an important component of the information seeking process (for example, Kuhlthau, [1993](#kuh93), [2003](#kuh03)) and has been studied as an issue in information retrieval and systems design (for example, [Brashers and Hogan, 2013](#bra13)). However, it has not been explored as a component of reference and information service.

In a recent study of the practitioner perspective, uncertainty, ambiguity and variability emerged as a major theme in the experience of reference and information service for practitioners. This theme has also been found in research on professional work for other client-centered professions, such as medicine and social work. This short paper explores this theme in relation to the existing literature from other professions and proposes future work to more fully understand the concept of uncertainty in reference and information service.

## Related research

The most relevant previous research in this area is Bouthillier's ([2000](#bou00)) study of ambiguity for public library service providers. In this ethnography, Bouthillier identified ambiguities at various levels: '_ideological, structural and operational_' (p. 260), and argued that these ambiguities must be incorporated into conceptualizations of service.

Beyond Bouthillier's study, however, uncertainty has not been explored in reference and information service. It has, however, been explored extensively outside library and information science. Schön ([1983](#sch83)) describes professional work as occurring in ambiguous situations where problems are not clear or easily understood. Medicine is the profession that has most thoroughly studied uncertainty in professional work. Some notable research includes Fox's ([1957](#fox57)) study of areas of uncertainty in the training of medical professionals; Light's ([1979](#lig79)) discussion of uncertainty in psychiatry; Gerrity, Earp and DeVellis' ([1992](#ger92)) development of an instrument to measure affective response to uncertainty for physicians; and Cranley 's ([2012](#cra12)) model of uncertainty in nursing.

## Uncertainty in the experience of reference and information service

In a recent study of the practitioner perspective of reference and information service, uncertainty emerged as a theme in the experience of reference and information service. This exploratory study used interpretative phenomenological analysis to study the phenomenon of reference and information service from the practitioner perspective. The aim of the study was to complement existing behavioral studies by examining the thoughts and feelings that motivate behaviors in reference and information service.

Interpretative phenomenological analysis was chosen as an approach for examining the experience of reference and information service, as it examines both commonality and diversity of experience, combining aspects of phenomenology and phenomenography ([VanScoy and Evenstad, in press](#van15)). The eight participants in this study were academic research librarians working in the United States. The semi-structured interviews focused on the experience of reference and information service for the participants. Interview data was analyzed thematically following interpretative phenomenological analysis procedures. Each participant's data was analyzed individually, then master themes for the group were developed. Five themes of the experience of reference and information service emerged from the analysis: _importance of the user_, _variety and uncertainty_, _fully engaged practice_, _emotional connection_, and _sense of self as reference professional_ ([VanScoy, 2013](#van13)).

The theme variety and uncertainty was an important theme for these participants. Variety was expressed explicitly with phrases such as '_a lot of variety_','_change of pace and variety at all levels_'and '_it's a mixed bag_'. Coupled with variety was a sense of uncertainty or ambiguity. Participants stated that they never knew what questions they might be asked, what path a reference interaction might take, or what reaction a user might give, using phrases such as '_you never know what you're gonna get or where it goes_', '_when you work at a reference desk, it's like anyone can ask you any conceivable question at any time_', and '_[you] really, truly never know what's gonna come at you'_. Other phrases hint at a lack of control for the direction of the interaction: '_wherever it takes you_' and '_you're not in control of what's going on'_.

The variety of users and user needs, along with the variety of locations and modes of delivery, demands nimbleness and diverse skills, In addition, the variety of purposes of the interaction, coupled with the fact that the ultimate purpose for any given interaction is not known at the outset, creates even more uncertainty for the librarian to cope with.

This finding supports Bouthillier's ([2000](#bou00)) findings about ambiguity for library service providers. It also supports a similar finding by Burns and Bossaller ([2012](#bur12)), who studied communication in reference and information service. Although they did not find uncertainty as a theme, they were surprised by the complexity of the work environment that their participants described: '_a confusing and unstable landscape_' (p. 613) featuring '_an experience much more varied and intricate than imagined before the study_' (p. 612).

## Future directions for uncertainty in reference and information service

These findings support the claim that the experience of reference and information service is uncertain, varied and ambiguous. However, the nature of this uncertainty and how it affects practice is unclear.

A first step toward understanding this uncertainty is development of a conceptual model of uncertainty in reference and information service. Based on the results of the interpretative phenomenological analysis study of the experience of reference and information service for academic research librarians, Bouthillier's study of ambiguity for public library service providers, and models developed for physicians ([Gerrity, Earp and DeVellis, 1992](#ger92)) and nurses ([Cranley , 2012](#cra12)), a model is proposed that identifies areas of uncertainty in reference and information service (See Figure 1). Through the center of the model are the stages of the reference interaction where uncertainty may occur. Contributing to uncertainty in the interaction are user, professional and organizational characteristics. This model may be used as tool for further study, refining the elements presented and examining relationships between them.

<figure class="centre">![Figure1: A model for uncertainty in reference and information service](isicsp9fig1.jpg)

<figcaption>Figure 1: A model for uncertainty in reference and information service</figcaption>

</figure>

Understanding uncertainty in reference and information service could contribute to the larger dialogue of professional uncertainty, responding to Gerrity, Earp and DeVellis' ([1992](#ger92)) call for a '_comparative sociology of uncertainty_' (p. 1044). On a more practical level, articulation of the uncertainty involved in reference and information service would provide support to practitioners and help avoid the temptation to over control uncertain situations by ignoring complexity and refusing to question one's decisions. Library and information science educators could use such a model to prepare students for the uncertainties of practice.

## About the author

**Amy VanScoy** is an Assistant Professor in the Department of Library and Information Studies, University at Buffalo, Buffalo, NY. She received her PhD from the University of North Carolina at Chapel Hill and her MLIS from the University of Alabama. She can be contacted at: vanscoy@buffalo.edu.

#### >References

*   Bouthillier, F. (2000). The meaning of service: ambiguities and dilemmas for public library service providers. _Library & Information Science Research_, 22(3), 243-272.
*   Brashers, D. E. & Hogan, T. P. (2013). The appraisal and management of uncertainty: implications for information-retrieval systems. _Information Processing & Management_, 49(6), 1241-1249.
*   Burns, S. & Bossaller, J. (2012). Communication overload: a phenomenological inquiry into academic reference librarianship. _Journal of Documentation_, 68(5), 597-617.
*   Cranley, L.A., Doran, D.M., Tourangeau, A.E, Kushniruk, A. & Nagle, L. (2012). Recognizing and responding to uncertainty: a grounded theory of nurses' uncertainty. _Worldviews on Evidence-Based Nursing_ 9(3), 149-158.
*   Fox, R. C. (1957). Training for uncertainty. In R.K. Merton, G. Reader & P.L. Kendall (Eds.), _The student physician: introductory studies in the sociology of medical education_ (pp. 207-241). Cambridge, MA: Harvard University Press.
*   Gerrity, M.S., Earp, J.A., DeVellis, R.F. & Light, D.W. (1992). Uncertainty and professional work: perceptions of physicians in clinical practice. _American Journal of Sociology_, 97(4), 1022-1051.
*   Kuhlthau, C. C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, 49(4), 339-355.
*   Kuhlthau, C. C. (2003). _Seeking meaning: a process approach to library and information services._ Englewood, CO: Libraries Unlimited.
*   Light, D. (1979). Uncertainty and control in professional training. _Journal of Health and Social Behavior_, 20, 310-322.
*   Schön, D. A. (1983). _The reflective practitioner : how professionals think in action_. New York: Basic Books.
*   VanScoy, A. (2013). Fully engaged practice and emotional connection: aspects of the practitioner perspective of reference and information service. _Library & Information Science Research_, 35(4), 272-278.
*   VanScoy, A. & Evenstad, S.B. (in press). Interpretative phenomenological analysis for LIS research. _Journal of Documentation_.

</article>