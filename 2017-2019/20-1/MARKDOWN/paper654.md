#### vol. 20 no. 1, March, 2015

# Why principal investigators funded by the U.S. National Institutes of Health publish in the Public Library of Science journals

#### [Nancy Pontika](#author)  
COnnecting REpositories (CORE) project, Knowledge Media Institute, Open University, Milton Keynes, Buckinghamshire, MK7 6AA, U.K.

#### Abstract

> **Introduction**. The National Institutes of Health public access policy requires the principal investigators of any Institutes-funded research to submit their manuscript to PubMed Central, and the open access publisher Public Library of Science submits all articles to PubMed Central, irrespective of funder. Whether the investigators, who made the decision to publish in one of the seven Public Library of Science journals were motivated by the National Institutes' public access policy or by the journals' quality standards is unknown.  
> **Method**. Forty-two Institutes-funded investigators who had published in one of the seven journals between 2005 and 2009 were interviewed, using a semi-structured, open-ended interview schedule.  
> **Analysis**. Qualitative analysis was conducted, dividing the participants into those who published in the journals before the mandatory policy (pre-mandate) and those who published after the policy (post-mandate).  
> **Results**. The Institutes-funded investigators submitted to the Public Library of Science journals because they favour the high impact factor, fast publication speed, fair peer-review system and the articles/ immediate open access availability.  
> Conclusions. The requirements of the National Institutes' public access policy do not influence the investigators' decision to submit to one of the Public Library of Science journals and do not increase their familiarity with open access publishing options.

## Introduction

In the current scholarly communications system there are two kinds of journals; the open access, which are freely distributed through the Internet ([Suber, 2010](#sub10)), and the toll- access, or subscription-based, which charge a fee to anyone who wants to access their content. In the past, open access journals were not considered as reputable as the toll access journals, mainly because of their electronic form. When the [Public Library of Science](http://www.plos.org) (PLOS) journals were compared with the journal _Nature_ it was found that _Nature_ is considered to be more prestigious because it has a longer publishing history and is published in a printed form ([Anderson, 2004](#and04)). The study by Nicholas and colleagues ([2005](#nic05)) study supports similar conclusions, suggesting that promotion committees prefer journals that are published in print and not online.

With regard to the journal impact factor, the quality of the PLOS journals is determined by its ranking in the Thomson Reuters Web of Science™ (formerly ISI Web of Knowledge), a database that indexes journal articles ([Garfield, 1990](#gar90)). Although the PLOS journals are only recently established publications, they have been receiving outstanding impact factors ([Tschider, 2006](#tsc06); [Atsuko, 2006](#ats06)) compared with other highly ranked toll-access journals with a longer existence. For instance, from the moment _PLOS Biology_ was created it ranked first in the Thomson Reuters list among the biology journals. A similar position is held by the journal _PLOS Pathogens_, while the journal _PLOS Medicine_ ranked in the fifth and seventh position during the period 2005 to 2009.

The United States National Institutes of Health (hereafter, Institutes) public access policy ([2014](#nih14)) mandates that the results of all research funded by them should be submitted immediately upon publication to [PubMed Central](http://www.ncbi.nlm.nih.gov/pubmed), a subject repository in health science. The PLOS journals submit all published research to PubMed Central immediately upon publication of all research articles, irrespective of their funder. So, why do Institutes-funded authors publish in one of the PLOS open access journals? Is it the fact that it has high quality standards or that authors are driven by the Institutes' public access policy? The study presented in this article is only a section of a wider research on the Institutes' public access policy and their funded authors' publication habits, which attempted to investigate if open access policies that mandate self-archiving (_green open access_) influence the authors' decision to publish in open access journals (Gold open access). This article relates to the reasons behind the authors' decisions to publish in one of the PLOS open access journals, and, to provide these answers, two propositions were tested. First, the publishing choices of the authors, who were self-characterised as open access advocates, would be affected by the Institutes' public access policy. Secondly the authors who were not self-characterised as open access advocates would improve their knowledge on open access when they would become familiar with the public access policy.

## Literature review

### Characteristics of the open access journals according to users

Suber ([2010](#sub10)) describes the open access journals as peer-reviewed publications that provide their content for free to anyone wants to access it through the Internet. When 4,000 authors were asked to assign the open access journals' characteristics, they described them as publications that offer their content free of cost, publish high quality articles and that they are indexed ([Nicholas, Huntington and Rowlands, 2004](#nic04)). In another study ([Morris and Thorn, 2009](#mor09)) it was discovered that the participants, who were members of the learned societies of the BioSciences Federation, had a fuzzy idea of the open access journals' characteristics. Although two thirds responded that they have read articles in open access journals, only one third named journals that were actually accessible for free. In this list, from the top eight journals five of them belonged to the PLOS publishing house, while _PLOS Biology_, _PLOS Medicine_ and _PLOS ONE_ ranked in the first three positions. In the largest study so far (N=40,000) regarding open access delivered through journals ([Dallmeier-Tiessen _et al._, 2010](#dal10)), the open access journals are considered to host prestigious publications with a higher citation impact than the toll access journals.

### Fee-based, open access journals

Article processing charges constitute a journal business model, followed by both open access and hybrid journals, which covers an article's publication expenses. Laakso and Björk (2012) discovered that currently only a 49% of the fully openly accessible articles levied article processing charges. Shieber ([2009](#shi09)) looked at that the open access journals that do charge and found that only 18% of these types of journals actually charge this fee. The most extended study up to today, the SOAP study ([Dallmeier-Tiessen _et al._, 2010](#dal10)) surveyed 40,000 participants and found that only 12% of the charges were covered from the authors' pockets.

The publication fees that researchers are asked to pay varies widely, with regard to both the type of the publisher and the subject field. Solomon and Björk ([2012](#sol12)) found that international journals had higher prices than the journals from developing countries, while commercial publishers had considerably higher fees than journals published by learned societies and universities. The range of price varies mostly among subject fields. For example, journals in the field of history usually do not charge this fee, while in biology and medicine we can find the most expensive fees, up to $3,000\. In the middle of the price range are journals in computer science and economics.

### Peer review

The quality of a journal's peer-review process is an important factor that adds value. The reported results of a peer-reviewed article are highly regarded and when researchers search for published literature there is a preference towards peer-reviewed publications ([Nicholas _et al._, 2005](#nic05)). When the quality of the peer-review process of the open access and the toll access journals was compared ([Swan and Brown, 2004](#swa04)) not only it was discovered that the peer-review process of both journals had no differences, but a small percentage (13%) mentioned that the reviewers' comments they received from open access journals were greater in quantity. A study ([Key Perspectives..., 2004](#key04)) conducted for the UK funding body, JISC, focused on the authors' satisfaction level concerning the peer-review process of the open access articles and showed that almost half of the respondents were satisfied, 45% were extremely satisfied, and a small percentage (5%) of the authors felt that they were not satisfied.

### The Public Librqry of Science journals

Swan and Brown ([2004](#swa04)) discovered that authors publish their manuscripts in open access journals because they wish to disseminate them for free. This public access availability makes the open access journals competitive with the toll access journals, even though the latter have a longer tradition in the scientific publishing industry (Tschider, 2006). The open access supporters consider that the prestige of the open access journals is high ([Swan and Brown, 2004](#swa04); [Dallmeier-Tiessen _et al._, 2010](#dal10); [Warlick, 2006](#war06)) and that they attract a wide readership ([Zivkovic, 2009](#ziv09); [Binfield, 2008](#bin08); [Hawxhurst, 2009](#haw09)). With regards to PLOS, the argument regarding prestige is supported by four facts; a) each of the PLOS journals focuses on a specific topic, b) the journals are characterised by publication speed, c) the open accessibility allows a wide dissemination of the articles attracting a high number of readers ([Schonfeld and Housewright, 2010](#sch10); [Dallmeier-Tiessen _et al._, 2010](#dal10)), and d) the articles are highly regarded by peers ( [Frank, 1994](#fra94); [Regazzi and Aytac, 2008](#reg08)). A Public Library of Science study ([Allen, 2011](#all11)), conducted for two consecutive years, surveyed all the authors who had submitted a manuscript to one of the seven journals and discovered that the journals are highly recommended among peers and that authors who had published with PLOS chose this publishing house because at some point they had read quality articles.

### PubMed Central

PubMed Central is a subject repository in health sciences, which is maintained by the United States National Library of Medicine and the National Institutes of Health. It was established in 1999 by Dr Harold Varmus, whose vision was to create a repository with articles related to health sciences ([Pope, 2001](#pop01)). According to the Institutes' public access policy, all research articles must be deposited to PubMed Central immediately upon publication, which would then become available after the expiration of the embargo period ([U.S. National..., 2008](#nih08)).

### Awareness of open access journals

Although in the past authors' awareness towards open access journals was relatively low (32%) ([Rowlands, Nicholas and Huntington, 2004](#row04)), it was discovered that within a year this proportion changed; the percentage of authors who knew nothing about the open access journals decreased by 15% ([Rowlands and Nicholas, 2005](#row05)). When researchers from a variety of fields were studied ([Swan and Brown, 2004](#swa04)) it was found that all who had published in open access journals were aware of them before submitting, while 60% of the authors who had published in toll access journals also knew about open access journals. A possible reason for authors not submitting their manuscripts to open access journals, even though they are aware of their existence, could be that they are not familiar with any open access journals in their field ([Key Perspectives..., 2004](#key04)).

When subject fields were compared, the authors in the health sciences were most aware of the open access journals ([Over, Maiworm and Schelewsky, 2005](#ove05)). Xia ([2010](#xia10)) compared the level of the authors' awareness of open access journals between 1990 and 2008, and showed that there has been an increase in the awareness of open access journals during recent years. Although the level of awareness has increased and authors are in favour of open accessibility to articles ([Xia, 2010](#xia10); [Kenneway, 2011](#ken11)), they still do not publish widely in open access journals ([Hess, Wigand, Mann and von Walter, 2007](#hes07)).

### Publishing motivations

Researchers publish mainly for three reasons: they want to disseminate their research findings, gain promotion and increase their professional prestige ([Peek and Newby, 1996](#pee96)). The vast majority of the authors do not expect a payment for their articles; only a small percentage expects to gain a direct income ([Dallmeier-Tiessen _et al._, 2010](#dal10); [Key Perspectives..., 2004](#key04); [Swan, 1999](#swa99)). When authors are about to publish their research articles, they usually choose a journal title with high quality standards, such as the impact factor, publication speed, editorial board prestige, readership and indexing ([Rowlands _et al._, 2004](#row04); [Rowlands and Nicholas, 2005](#row05); [Nicholas _et al._, 2004](#nic04); [Swan, 1999](#swa99); [Warlick and Vaughan, 2007](#war07)). The argument is that publications with high standards will attract future research funding and create more chances for career advancement.

Those who choose to publish in open access journals explained that they do so because they support the '_principle of free access to all readers_' ([Swan and Brown, 2004](#swa04), p. 220). In addition, it is believed that the open access journals have a high publication speed and can reach a wide readership ([Morris and Thorn, 2009](#mor09); [Dallmeier-Tiessen _et al._, 2010](#dal10); [Warlick, 2006](#war06)). Other reasons are that these journal titles were recommended by their peers or that they had recognised familiar names either in the authors' list or editorial boards ([Nariani and Fernande, 2011](#nar11)).

On the other hand, there are authors who prefer not to publish in open access journals mainly for five reasons. These authors believe that the open access journals do not have a high publication speed and do not attract more citations. Based on the fact that the journal quality is an important decision-making factor ([Hess, _et al._, 2007](#hes07); [Morris and Thorn, 2009](#mor09); [Dallmeier-Tiessen _et al._, 2010](#dal10)) these authors abstain from publishing in open access journals because these journals are perceived as having low prestige compared with toll access journals, and this can influence negatively their promotion and career ([Hurrell and Meijer-Kline, 2011](#hur11)). Another factor is the article processing charge; authors consider that the existing charges are high and it is difficult to ensure funds to cover them ([Dallmeier-Tiessen _et al._, 2010](#dal10); [Warlick and Vaughan, 2007](#war07)). Finally, authors do not publish in open access journals because they are not aware of any open access journals titles in their fields ([Swan and Brown, 2004](#swa04); [Warlick, 2006](#war06); [Xia, 2010](#xia10)).

### Copyright

Every time an author publishes an article in a toll access journal s/he has to sign a licensing agreement, transferring the copyright to the journal publisher. Copyright issues can affect the future use of a publication, such as its further manipulation or use for teaching purposes. There have been cases where authors would sign the publishers' agreements without having a clear understanding of their rights. As a result, when they would wish to re-use their publications for other purposes, they would infringe copyright ([Warlick and Vaughan, 2007](#war07)). Previous research ([Rowlands _et al._, 2004](#row04);[Rowlands and Nicholas, 2005](#row05); [Swan, 1999](#swa99); [Warlick and Vaughan, 2007](#war07)) indicates that the open access advocates are highly interested in retaining the copyright of their articles and their decision on which journal to publish in is shaped by the journal's licensing agreement terms and whether they are allowed to retain the rights to their publications. These authors choose to submit their manuscripts mainly to open access journals that use liberal licenses. With regards to PLOS, all seven PLOS journals publish their articles with a Creative Commons license, which not only allows access to the published articles, but also unrestricted use, reproduction and distribution.

## Method

The findings of this research are based on only a section of a wider research conducted during the period March- May 2011 on the Institutes' public access policy and their funded authors' publication habits. The research interview schedule was composed of thirteen questions and only the first referred to the PLOS journals: '_Tell me, why did you choose to publish with PLOS?_'. During the interview, the participants spent an considerable time on this first question and rich responses were collected. As a result, it was decided that it would worth presenting the findings of this question in a separate paper.

### Participant selection

The participants of the study were Institutes-funded principal investigators (hereafter, investigators) who had published in one of the seven PLOS journals during the period 2005- 2009\. These investigators were considered as the most appropriate group to participate in this research because according to the policy they are responsible for the manuscript submission to PubMed Central (Revised Policy on Enhancing Public Access to Archived Publications Resulting from NIH-Funded Research, 2008).

A list of Institues-funded investigators who had published in the PLOS journals was extracted from [RePORT](http://www.webcitation.org/6CjQuDV93), a database maintained by the US Department of Health and Human Services. The publications data were extracted from the [RePORTER](http://www.webcitation.org/6CjQrfTkD), the RePORT Expenditures and Results database, which contains information on Institues-funded research and provides details such as the awardee organization and a full bibliographic information of the published articles that emerged from each grant. All the publications' files were extracted from the [ExPORTER](http://www.webcitation.org/6CjQSPoAY), the service that contains downloadable CSV and XML files with full publication details. The researcher used these files the track the investigators' names and e-mail addresses.

A random sample of one thousand investigators was drawn using the [Randomizer](http://www.webcitation.org/6CjR4qvMJ) Website and approximately 900 different PubMed unique identifiers were extracted for the seven PLOS journals. Each week 100 investigators were contacted and asked to participate in the research during the period March-May 2011; thus, the sample was self-selected. This was done because it was impossible to create a list of individuals who would meet a specific profile for this study, since the number of authors who have published in one of the PLOS journals and received funding from the Institutes is extremely large.

In the end, forty-two (N=42) participants were interviewed. According to Marshall ([1996](#mar96)) the purpose of a qualitative study is not to theorise the results of the research to the wider population, as in quantitative studies, but to present a story based on the analysis and interpretation of the collected data. For that reason, the size of the population was considered to be adequate for a qualitative research study, as the researcher observed that the participants' responses revealed repeating themes after the completion of the first thirty five interviews.

Participants' characteristics

The participants were asked to state their affiliation at the beginning of the interview; all of them were affiliated either with academic institutions, or research centres and hospitals in the United States. The majority of the participants were working in academic institutions (n=24) and approximately one third in research centres (n=10). A small number had more than one affiliation, which was a combination of an academic institution and a research centre or an academic institution and a hospital (n=8).

<table class="center" style="width:60%;"><caption>  
Table 1: Participants' affiliation</caption>

<tbody>

<tr>

<th>Participants' affiliation</th>

<th>Number</th>

</tr>

<tr>

<td>Research centre</td>

<td style="text-align:center">10</td>

</tr>

<tr>

<td>Academic institution</td>

<td style="text-align:center">24</td>

</tr>

<tr>

<td>Combination of research centre or hospital and academic institution</td>

<td style="text-align:center">8</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">42</td>

</tr>

</tbody>

</table>

### Interviews

The interview schedule of the wider research study was designed to study the investigators' awareness on the open access movement and their preference to publish in one of the PLOS journals. The question related specifically this article and to PLOS was, '_Tell me, why did you publish with PLOS?_'

## Analysis

According to Morse ([1994](#mor94)) the investigation of the behaviour of people from different cultures is better depicted when there is a comparison between two populations. Therefore, each participant was given a code; the first part of the code related to whether the participant belonged to the pre-mandate, those who had published in one of the PLOS journals before the mandatory public access policy, or to the post-mandate group, those who had published in one of the PLOS journals after the mandatory policy. If the participant belonged to the pre-mandate group s/he was given the code PRE and then the count of the interview, for example 7, if this interview was the seventh interview among the pre-mandate participants. If the participant belonged in the post-mandate group s/he was given the code POST and then the count for that interview.

### Data validity and analysis

For the data validity the researcher followed the Leedy and Ormrod ([2005](#lee05)) guidelines; in a qualitative method it is suggested that the researcher eliminates personal biases during the data accumulation and interprets the findings by demonstrating the arguments and ranking them based on their importance. For the researcher to accomplish that, both descriptive and interpretive coding is used ([Creswell, 2003](#cre03)). During the first step of the coding, the brief coding ([Kvale, 2007](#kva07)), the researcher established specific codes that characterised part of the text and tried to identify their meaning and summarise the storyline to convey the current situation. In the second step, the main points were highlighted and explanatory key words were added, which were used for the interpretative coding. During the interpretative coding the researcher attempted to interpret the significance of the participants' ideas as they emerged from the interviews ([King and Horrocks, 2010](#kin10)). For that reason, the ideas with a similar meaning were placed into categories. During the third step, these ideas were incorporated together to create the story that would answer the research questions.

## Results

This section discusses the major findings of this study and addresses the relevant topics that emerged from the interviews. The most pertinent themes were online and public access, the impact factor, publication speed, and citation advantage of the PLOS journals. The participants' references regarding copyright and licensing were limited.

### Online and public access availability

In their search for scientific articles the investigators visit the PubMed Central database where they can retrieve information quickly by adding in the search field keywords or authors' names. Afterwards they can download the article immediately as they do not face a username and password barrier. A participant affiliated with an academic institution mentioned that,

> [t]here is an additional concern and that is if you want to do a survey, you know, a review on a particular topic and you use PubMed for example to get their sources, then obviously you will, for practical reasons, you will focus on what is most easily available for expediency (ID: PRE.7).

The online availability of PLOS articles is favourable to the investigators not only because they can conveniently retrieve and access the articles, but also because they can manipulate and re-use them. A tenured professor affiliated with an academic institution described the easiness of retrieving articles from PubMed Central,

> having them available on people's desktops from a public library it makes it so much easier to do the work, you can do anything you want with them. You don't have to go over to the library, find a journal, look it up, xerox it. It is just wonderful (ID: POST.17).

All PLOS journals submit the final publisher PDF file of the article to PubMed Central immediately after publication regardless of the agency that funded the research, an important detail that only one third of participants knew (33%). The participants noted that they do not prefer reading the author's accepted manuscript version, but they would rather prefer to have access to the publisher's final PDF version of the article. An oncologist affiliated with an academic institution said,

> I like to get the actual format of the article, it is a beautiful thing, it is like a house that has been painted (ID: POST.24).

### Impact factor

The journal impact factor is a highly appreciated measurement of the journal's quality, and all seven PLOS journals have demonstrated an admirable impact factor in the Thomson Reuters list, where _PLOS Biology_ and _PLOS Pathogens_ hold the first position in the list. Half of the participants stated that they chose to publish in one of the PLOS journals because of their high impact factor. A pre-mandate professor in immunology, who was also self-characterised as an open access advocate mentioned about the PLOS journals' quality,

> I used the PLOS system because these journals are most highly rated among the open access journals (ID: PRE.8).

A post-mandate PI, who is not an open access advocate and is affiliated with an academic institution said,

> PLOS Pathogens is a very prestigious journal in my field (ID: POST.9)

Concerns were expressed about one of the PLOS journals, _PLOS ONE_. This journal publishes articles in a variety of topics relating to health sciences and a small proportion (16%) of investigators mentioned that they do not consider _PLOS ONE_ to be a prestigious publication. Almost half of the investigators indicated that they have mixed feelings about the journal and that they have read both low and high quality articles there. A non-tenured professor conducting research in evolution explained why he chose to publish one of his articles in _PLOS ONE_,

> [t]he article in PLOS ONE did not have very important results and this is why we chose this journal (ID: POST.19).

However, about 10% of the investigators finds that PLOS ONE is a journal with the capability of communicating messages effectively. An example comes from a post-mandate participant who used the PLOS ONE journal to send a message to the community;

> the article was about the spread of flu, influenza, internationally via airplanes. And it was a topic that was being discussed broadly and actively and we wanted to get an article written and out to the research domain fast (ID: POST.13).

### Speed of publication

Previous studies support the proposition that in the open access journals the time between the article's submission and publication is low (Swan and Brown, 2004; Morris and Thorn, 2009). In this study, almost 40% of the participants (n=16) mentioned that they chose to publish with one of the PLOS journals because of their publication speed. An investigator conducting research in radiology and affiliated with a research institution noted,

> [t]he time from submission to publication online is short. You do want your research to be known by other people as soon as possible (ID: POST.11).

With the term _publication speed_ the investigators of this study referred to the whole publication process; the submission, peer-review, copy editing and the article's publication on the publisher's Website.

The PLOS journals were also characterised by expediency. When an article was not accepted in one of the PLOS journals, the investigators were advised by the journal editors to re-submit to another PLOS journal, something that could be done easily, since all PLOS journals follow the same article layout and referencing style. Six investigators mentioned that they had re-submitted their articles to another PLOS journal thanks to that. A post-mandate researcher said,

> PLOS Pathogens is a prestigious journal and sometimes for expediency we submitted papers to PLOS Pathogens, but they were not accepted and they recommended that we submit to PLOS ONE, so that's part of the expediency as well (ID: POST.8).

### Quality of publication

During the research process the researcher asked questions only about the PLOS journals and never mentioned any other journal title in health sciences. During the discussions on the PLOS journals' publication speed, the investigators compared PLOS with the prestigious journals Cell, Nature and Science. They mainly expressed a criticism on the publication delays of these prestigious journals, and commented on their irritation at the peer review delays, insisting that research results must become available to the readers with no major delays. An investigator affiliated with an academic institution mentioned about publishing delays,

> Nature, Neuroscience and Neuron, for my field, which is neuroscience, are the top journals. Those can go on for years. The review process is notoriously long (ID: POST.18).

Nine participants compared the peer-review process of the PLOS journals to the peer-review process of the journals _Cell_, _Nature_ and _Science_. They felt that these journals in their attempt to increase their profits are being extremely selective about the articles they publish. It was believed that their rigid peer-review process aims mainly to commercialise scientific research and impose a level of control over authors. A post-mandate researcher conducting research in oncology explains:

> Nature's income depends on advertising and subscriptions and the number of subscriptions and the number of pages of advertising they get are going to be directly related to the impact factor. So if they publish a lot of papers that nobody refers to, then the impact factor is going to go down. You know, it is like the Nielsen ratings for television, but it is who actually is reading the journal. I think that the open access journals are a great alternative to a system that just got too much power (ID: POST.24).

Although the journals _Cell_, _Nature_ and _Science_ received criticism for publication delays and their peer-review system, almost half of the researchers characterised them as highly prestigious publications, capable of advancing a researcher's career more certainly than the open access journals. A post-mandate researcher in oncology affiliated with an academic institution commented,

> There are three journals that are targeted and these are Cell, Nature and Science. These are the top three. The open access journals are more in the third category (ID: POST.16).

The other half of the investigators believed though that the open access PLOS journals are equally prestigious and can promote someone's career. A post-mandate tenured professor thinks that promotion committees are consisted of,

> pretty smart [people] and they are subject to the same type of pressure that the people going up for tenure are, and they understand the pressure, so I don't think that there is a bias [against open access journals]. The fact is that if you publish in Nature it is awesome. There is no way around that. But the fact is that if you publish in PLOS Medicine that is darn good (ID: POST.24).

### Citation impact

Almost one fourth of the investigators mentioned that they chose to publish their articles in the PLOS journals because of their citation advantage. The investigators are familiar with the current literature ([Brody, 2004](#bro04); [Hajjen, Harnad and Gingras, 2005](#haj05); [Harnad and Brody, 2004](#har04)), which indicates an increase in citations for open access articles. A pre-mandate investigator conducting research on environmental medicine affiliated both with a prestigious academic institution and research centre commented,

> there is some indication that research freely available by open access is more often cited than research that is not as easily available (ID: PRE.7).

When the researcher coded the term citation advantage in the interviews, she screened the other concepts and words that appeared with that term and the terms that were repeated are quality, impact factor and dissemination. Therefore, it could be suggested that when the participants consider that the PLOS journals receive high citations, they also have in mind that there is some positive level of quality and impact factor in the articles.

### Copyright

The PLOS articles are distributed under a Creative Commons Attribution License (CC-BY), which allows an article's reuse and manipulation ([Sedwick, 2005](#sed05)). In this research a limited proportion of investigators (17%) mentioned copyright issues and even when they were mentioned, the researcher concluded that the investigators had limited interest and possibly limited knowledge on the subject matter. A post-mandate participant in neurology said of copyright,

> I don't worry about copyright issues, I just don't think about it (ID: POST.23).

Two participants mentioned that they would prefer to retain the copyright for their articles, but they understand this cannot apply to all journals. A post-mandate investigator affiliated with a large academic institution conducting research in molecular genetics mentioned,

> I think from our perspective, as researchers, it is obviously to our benefit to hold the copyright, but this meets some resistance (ID: POST.7).

<table class="center"><caption>  
Table 2: Reasons participants publish with the PLoS journals</caption>

<tbody>

<tr>

<th>Impact factor</th>

<th>Publication speed</th>

<th>Peer-review</th>

<th>Open access</th>

</tr>

<tr>

<td>Competes with prestigious toll access journals  
All PLoS journals are the best among open access journals</td>

<td>Fast article submission and acceptance process  
Easy article re-submission to another PLoS journal</td>

<td>Fast  
Fair  
Impartial</td>

<td>Wide article dissemination  
Increased article citations</td>

</tr>

</tbody>

</table>

## Concluding discussion

Over the past decade there has been a shift in the scientific journal publishing towards online content, which is easy to search, retrieve, manipulate and re-use. The PLOS journals, which are available online for free with a liberal license, have four quality characteristics; (1) impact factor, (2) speed of publication, (3) peer-review system and (4) article's citation advantage. This finding agrees with other previous research that investigated quality standards for journals ([Rowlands, _et al._, 2004](#row04); [Rowlands and Nicholas, 2005](#row05); [Nicholas _et al._, 2004](#nic04); [Swan, 1999](#swa99); [Warlick and Vaughan, 2007](#war07)).

Although not all participants position the PLOS publications in the top three journals, their high impact factor impels them to submit their articles to this open access publisher. The publication-speed of the PLOS journals articles was positively described, which consists of the following two components; the fast peer-review process and the ease of article submission. The quality peer review process of the PLOS journals received also a positive feedback, as the investigators believed that their articles were being judged by their significance and not by the popularity of the explored topic. As happens with all open access publications, a citation advantage appears with articles published in the PLOS journals, an aspect of great interest for the investigators, since not only do they aim at a wide readership, but they also seek the opportunity to raise their profiles and attract future funding.

The participants in this study were divided into two categories, the pre-mandate and the post-mandate group, as an effort to distinguish whether there are specific patterns between these two groups. The results do not indicate any differences in the publishing influences of the two groups, who mainly decide on the basis of the four aforementioned publication standards.

The two propositions of this study were that the participants who were self-characterized as open access advocates would be affected in their choice about the journal in which they will publish their papers by the NIH public access policy and would choose the open access journals. The second was that the authors who were not self-characterized as open access advocates would advance their knowledge about available open access options after they became familiar with the NIH open access policy. The results indicate that the NIH public access policy does not have an important role in both groups. Although there seemed to be a tendency for publicly-funded research to be disseminated for free it could not be determined if this tendency was a shift towards supporting the funder's decision or if it was just a new popular trend.

This study was qualitative and had a self-selected population. The interviews were conducted with open-ended questions and forty-two investigators funded by the National Institutes of Health were interviewed. With respect to this research study, the readers should be reminded that the results are determined by this population only, and the extent to which the findings can be generalised certainly requires further investigation. Future research could be conducted using a quantitative method to collect data from a larger number of authors, which would allow the results to be generalised. Finally, an extended investigation in another qualitative study with a non-self-selected population could show whether there is a tendency for a shift in the investigators' publication habits in this group.

## Acknowledgements

This article is part of the research I conducted for my doctoral dissertation. I wish here to present my special thanks to my dissertation committee, Dr. Peter Suber (Harvard University) and Professors Robin Peek and Lisa Hussey (Simmons College). I would also like to thank the anonymous reviewers for their constructive comments.

## About the author

Nancy Pontika is an Open Access Aggregation Officer at the COnnecting REpositories (CORE) project, Knowledge Media Institute, Open University. She holds a PhD from the School of Library and Information Science, Simmons College, Boston, Massachusetts, USA. Her ORCiD id is 0000-0002-2091-0403 and she can be contacted at pontika.nancy@gmail.com

#### References

*   Allen, L. (2011). _[Author research, 2010: summary of results and conclusions](http://www.webcitation.org/6CjR33DWI)_. San Francisco, CA: Public Library of Science. Retrieved from http://www.slideshare.net/MarkPatterson/p-lo-s-author-research-2010-6638756 (Archived by WebCite® at http://www.webcitation.org/6CjR33DWI)
*   Anderson, R. (2004). Author disincentives and open access. _Serials Review, 30_(4), 288-291
*   Antelman, K. (2004). Do open access articles have a greater research impact? _College & Research Libraries, 65_(5), 372-382.
*   Atsuko, M. (2006). Situation and spread of open access publishing in the Journal Citation Reports. _Journal of the Japan Medical Library Association, 53_(1), 41-47
*   Binfield, P. (2008, October 20). [Interview with a PLOS ONE author](http://www.webcitation.org/6CjQfPCIu) [Web log message]. Retrieved from http://blogs.plos.org/plos/2008/10/interview-with-a-plos-one-author/ (Archived by WebCite® at http://www.webcitation.org/6CjQfPCIu)
*   Brody, T. (2004). [Citation analysis in the open access world](http://www.webcitation.org/6CjQFHakc). Southampton, UK: University of Southampton, School of Electronics and Computer Science. Retrieved from http://eprints.ecs.soton.ac.uk/10000/ (Archived by WebCite® at http://www.webcitation.org/6CjQFHakc)
*   Carr, L. & Harnad, S. (2005). [Keystroke economy: a study of the time and effort involved in self-archiving](http://www.webcitation.org/6CjQJWfNO). Southampton, UK: University of Southampton, School of Electronics and Computer Science. Retrieved from http://eprints.ecs.soton.ac.uk/10688/ (Archived by WebCite® at http://www.webcitation.org/6CjQJWfNO)
*   Creswell, J.W. (2003). _Research design: qualitative, quantitative and mixed methods approaches_. (2nd ed.). London: Sage Publications
*   Cronin, B. & Overfelt, K. (1995). E-journals and tenure. _Journal of the American Society of Information Science, 46_(9), 700-703
*   Dallmeier-Tiessen, S., Darby, R., Goerner, B., Hyppoelae, J., Igo-Kemenes, P., Kahn, D. & Van der Stelt, W. (2010). [Highlights from the SOAP project survey: what scientists think about open access publishing](http://arxiv.org/ftp/arxiv/papers/1101/1101.5260.pdf). Retrieved from http://arxiv.org/ftp/arxiv/papers/1101/1101.5260.pdf
*   Frank, E. (1994). [Authors' criteria for selecting journals](http://www.webcitation.org/6CjQW2jgL). _Journal of the American Medical Association , 272_(2), 163-164\. Retrieved from http://dx.doi.org/10.1001/jama.272.2.163 (Archived by WebCite® at http://www.webcitation.org/6CjQW2jgL)
*   Garfield, E. (1990). How ISI selects journals for coverage: quantitative and qualitative considerations. _Essays of an Information Scientist, 13_(22), 185-193
*   Hajjen, C., Harnad, S. & Gingras, Y. (2005). Ten-year cross-disciplinary comparison of the growth of open access and how it increases research citation impact. Bulletin of the Technical Committee on Data Engineering (IEEE Computer Society), 28(4), 39-46.
*   Harnad, S. (2004). [Comparing the impact of open access (oa) vs. non-oa articles in the same journals](http://www.webcitation.org/6CjQZC0IE). _D-Lib Magazine, 10_(6). Retrieved from http://www.dlib.org/dlib/june04/harnad/06harnad.html (Archived by WebCite® at http://www.webcitation.org/6CjQZC0IE)
*   Hawxhurst, A. (2009, September 8). [Ant research in PLoS ONE](http://www.webcitation.org/6VZwvkeEe) [Web log message]. Retrieved on January, 20011 from http://blogs.plos.org/everyone/2009/09/08/ant-research-in-plos-one/ (Archived by WebCite® at http://www.webcitation.org/6VZwvkeEe)
*   Hess, T., Wigand, R., Mann, F. & von Walter, B. (2007). _[Open access and science publishing](http://www.webcitation.org/6CjQJWfNO)_. Munich, Germany: Ludwig-Maximilians-Universität. (Management Report 1/2007) Retrieved from http://openaccess-study.com/Hess_Wigand_Mann_Walter_2007_Open_Access_Management_Report.pdf (Archived by WebCite® at http://www.webcitation.org/6CjQJWfNO)
*   Hurrell, C. & Meijer-Kline, K. (2011). [Open access up for review: academic attitudes towards open access publishing in relation to tenure and promotion](http://www.webcitation.org/6VZxC5BMw). Retrieved on June 25, 2011 from http://tsc.library.ubc.ca/index.php/journal4/article/viewFile/104/pdf_10 (Archived by WebCite® at http://www.webcitation.org/6VZxC5BMw)
*   Kenneway, M. (2011). _[Author attitudes towards open access publishing](http://www.webcitation.org/6CjQlsIBz))_. Wheatley, UK: TBI Communications Ltd. Retrieved on June 25, 2011, from http://www.intechweb.org/public_files/Intech_OA_Apr11.pdf (Archived by WebCite® at http://www.webcitation.org/6CjQlsIBz)
*   Key Perspectives, Ltd. (2004). _[JISC/OSI journal authors survey: report](http://www.webcitation.org/6CjQiAgGd)_. Truro, UK: Key Perspectives Ltd. Retrieved from http://www.jisc.ac.uk/uploaded_documents/JISCOAreport1.pdf (Archived by WebCite® at http://www.webcitation.org/6CjQiAgGd)
*   King, N. & Horrocks, C. (2010). _Interviews in qualitative research_. Los Angeles, CA: Sage Publications.
*   Kvale, S. (2007). _Doing interviews_. Los Angeles, CA: Sage Publications.
*   Laakso, M. & Björk, B.C. (2012). [Anatomy of open access publishing: a study of longitudinal development and internal structure](http://www.webcitation.org/6JhducNeV). _BMC Medicine, 10_(124). http://www.biomedcentral.com/1741-7015/10/124 (Archived by WebCite® http://www.webcitation.org/6JhducNeV)
*   Leedy, P.D. & Ormrod, J.E. (2005). _Practice research: planning and design_. (8th ed.) Columbus, OH: Pearson Education Inc.
*   Marshall, M.N. (1996). [Sampling for qualitative research](http://www.webcitation.org/6SrBiuBEo). _Family Practice, 13_(6), 522-525 Retrieved from http://fampra.oxfordjournals.org/content/13/6/522.full.pdf+html (Link to paper archived by WebCite® http://www.webcitation.org/6SrBiuBEo)
*   Morris, S. & Thorn, S. (2009). Learned society members and open access. _Learned Publishing, 22_(3), 221- 239.
*   Morse, J. M. (1994). Designing funded qualitative research. In N.K. Denzin & Y.S. Lincoln (Eds.), _Handbook of qualitative research_, (pp.220- 235). London: Sage Publications.
*   Nariani, R. & Fernandez, L. (2011). [Open access publishing: what authors want](http://www.webcitation.org/6CjQovj9q). _College and Research Libraries, 73_(2), 182-195\. Retrieved from http://crl.acrl.org/content/early/2011/06/10/crl-203.abstract (Archived by WebCite® at http://www.webcitation.org/6CjQovj9q
*   Nicholas, D., Huntington, P., Rowlands, I. (2004). Open access journal publishing: the views of some of the world's senior authors. _Journal of Documentation, 61_(4), 497- 519.
*   Nicholas, D., Jamali, H., Huntington, P. & Rowlands, I. (2005). In their very own words: authors and scholarly publishing journals. _Learned Publishing, 18_(3), 212-220.
*   Over, A., Maiworm, F. and Schelewsky, A. (2005). _[Publishing strategies in transformation? Results of a study on publishing habits and information acquisition with regard to open access](http://www.webcitation.org/6CjQwD0aB)_. Bonn, Germany: Deutsche Forschungsgemeinschaft Information Management (IM). Retrieved on February 2011, from http://www.dfg.de/download/pdf/dfg_im_profil/evaluation_statistik/programm_evaluation/studie_publikationsstrategien_bericht_en.pdf (Archived by WebCite® at http://www.webcitation.org/6CjQwD0aB)
*   Pharmboy, A. (2008, December 2). [Chris Patil (ouroboros) on the Campisi lab's new PLOS Biology paper: cellular senescence, protein secretion, and the aging/cancer paradox](http://www.webcitation.org/6CjR14W090) [Web log message]. Retrieved from http://scienceblogs.com/terrasig/2008/12/chris_patil_ourboros_on_the_ca.php (Archived by WebCite® at http://www.webcitation.org/6CjR14W090)
*   Peek, R. and Newby, G. (1996). _Scholarly publishing: the electronic frontier_. Cambridge, MA: MIT Press.
*   Pope, L. (2001). PubMed Central: a barrier-free repository for health sciences. _Series Librarian, 40_(1/2), 183-190 (Archived by WebCite® at http://www.webcitation.org/6JheANox1)
*   Regazzi, J.J. & Aytac, S. (2008). Author perceptions of journal quality. _Learned Publishing, 21_(3), 225- 235.
*   Rowlands, I. & Nicholas, D. (2005). Scholarly communication in the digital environment: The 2005 survey of journal author behaviour and attitudes. _Aslib Proceedings, 57_(6), 481-497.
*   Rowlands, I., Nicholas, D., Huntington, P. (2004). _Scholarly communication in the digital environment: what do authors want? Findings from an international survey on author opinion: project report_. London: City University, Department of Information Science, Centre for Information Behaviour and Evaluation of Research. Retrieved from http://www.ucl.ac.uk/ciber/ciber-pa-report.pdf (Archived by WebCite® at [http://www.webcitation.org/6CjR9BirZ](http://www.webcitation.org/6CjR9BirZ))
*   Schonfeld, R.C. & Housewright, R. (2010). _Faculty survey 2009: key strategic insights for libraries, publishers and societies_. New York, NY: Ithaka S+R. Retrieved from http://www.sr.ithaka.org/sites/default/files/reports/Faculty_Study_2009.pdf (Unable to archive)
*   Sedwick, C. (2005). [Opening access to cell biology](http://www.webcitation.org/6CjRGEtwm). _PLOS Biology, 3_(12), e426\. Retrieved from http://www.plosbiology.org/article/info:doi%2F10.1371%2Fjournal.pbio.0030426 (Archived by WebCite® at http://www.webcitation.org/6CjRGEtwm)
*   Shieber, S. (2009, May 29). [What percentage of open access journals charge publication fees](http://www.webcitation.org/6JheKBlaH) [Web log post]. Retrieved from http://blogs.law.harvard.edu/pamphlet/2009/05/29/what-percentage-of-open access-journals-charge-publication-fees/ (Archived by WebCite® at http://www.webcitation.org/6JheKBlaH)
*   Solomon, D. & Björk, B.C. (2012). [A study of open access journals using article processing charges](http://www.webcitation.org/6JheUtZIw). _Journal of the American Society for Information Science and Technology, 63_(8), 1485-1495\. (Archived by WebCite® at http://www.webcitation.org/6JheUtZIw)
*   Suber, P. (2010). Thoughts on prestige, quality and open access. _Logos, 21_(1/2), 115-128.
*   Swan, A. (1999). [What authors want: the ALPSP research study on the motivations and concerns of contributors to learned journals.](http://www.webcitation.org/6VZkgE36X) _Learned Publishing, 12_(3), 170-172\. Retrieved from http://image.sciencenet.cn/olddata/kexue.com.cn/upload/blog/file/2009/12/2009122714187128494.pdf (Archived by WebCite® at http://www.webcitation.org/6VZkgE36X)
*   Swan, A. & Brown, S. (2004). [Authors and open access publishing](http://www.webcitation.org/6VZkp7Wp6). _Learned Publishing, 17_(3), 219- 224\. Retrieved from http://eprints.soton.ac.uk/261003/1/Authors_and_open_access_publishing.pdf (Archived by WebCite® at http://www.webcitation.org/6VZkp7Wp6)
*   Tschider, C. (2006). [Investigating the "public" in the Public Library of Science: gifting economics in the Internet community](http://www.webcitation.org/6CjRKSFT9). _First Monday, 11_(6). Retrieved February, 2011 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1340/1260 (Archived by WebCite® at http://www.webcitation.org/6CjRKSFT9)
*   U.S. _National Institutes of Health_. (2014). _[NIH public access policy details.](http://www.webcitation.org/6VYf28bdK)_ Bethesda, MD: National Institutes of Health. Retrieved from http://publicaccess.nih.gov/policy.htm (Archived by WebCite® at http://www.webcitation.org/6VYf28bdK)
*   U.S. _National Institutes of Health_. (2008). _[Revised policy on enhancing public access to archived publications resulting from NIH-funded research.](http://www.webcitation.org/6VZo5TOKM)_ Bethesda, MD: National Institutes of Health. Retrieved from http://grants.nih.gov/grants/guide/notice-files/NOT-OD-08-033.html (Archived by WebCite® at http://www.webcitation.org/6VZo5TOKM)
*   Warlick, S. E. (2006). _[Publication transformation: why authors choose to publish in openaccess/free full-text journals](http://www.webcitation.org/6CjSWQ1Dw)_. Unpublished Master's course-work, University of North Carolina at Chapel Hill, Chapel Hill, North Carolina, USA. Retrieved from http://www.ils.unc.edu/MSpapers/3195.pdf (Archived by WebCite® at http://www.webcitation.org/6CjSWQ1Dw)
*   Warlick, S.E. & Vaughan, KTL. (2007). [Factors influencing publication choice: why faculty choose open access](http://www.webcitation.org/6CjRS81U5). _Biomedical Digital Libraries, 4_(1). Retrieved from http://www.bio-diglib.com/content/4/1/1 (Archived by WebCite® at http://www.webcitation.org/6CjRS81U5)
*   Xia, J. (2010). Longitudinal study of scholars attitudes and behaviors toward open access journal publishing. _Journal of the American Society for Information Science and Technology, 61_(3), 615- 624.
*   Zivkovic, B. (2009, March 11). [Interview with a PLOS ONE frequent author: Seyed Hasnain.](http://www.webcitation.org/6CjRVMqJZ) [Web log message]. Retrieved from http://blogs.plos.org/plos/2009/03/interview-with-a-plos-one-frequent-author-seyed-hasnain/ (Archived by WebCite® at http://www.webcitation.org/6CjRVMqJZ)