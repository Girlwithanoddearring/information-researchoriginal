#### vol. 20 no. 1, March, 2015

# Expressing emotions in information sharing: a study of online discussion about immigration

#### [Reijo Savolainen](#author)  
School of Information Sciences, University of Tampere, FIN-33014, Finland

#### Abstract

> **Introduction**. The main task of this paper is to assess how emotions are expressed in sharing of information about a controversial topic, immigration, and how the emotional expressions vary by comment type.  
> **Method**. A sample of 50 threads of conversation from a Finnish online discussion group focusing on issues of immigration was selected for the study. The dataset contains 2,312 messages submitted to the discussion group in 2009–2013\. A descriptive statistics approach was used to calculate the percentage distributions for positive and negative emotions in the context of six comment types: agreement, disagreement, neutral statement, sarcasm, provocation, and invective. In addition, qualitative content analysis was employed to characterise the nature of the comments.  
> **Results**. Of the messages, 21.5% incorporated explicit expressions of emotions. Reflective of the sensitive topic of discussion, the majority of emotions were negative. The most frequent emotions seen were contempt, envy, fear, and irritation. Of the positive emotions, sympathy was expressed most frequently. The positive emotions were articulated in the context of agreement and neutral statements, while other comment types – disagreement, sarcasm, provocation, and invective –incorporated negative emotions only.  
> **Conclusions**. Emotions are contextual factors that affect information sharing by setting the tone of online discussion. The emphasis on positive or negative emotions is dependent on the nature of the topic at hand. In addition, the intent of the communication affects the ways in which emotions are incorporated into provision of information to others.

## Introduction

Information sharing may be understood as a set of activities by which information is provided to others, either proactively or upon request, such that the information has an impact on one or more other people's image of the world ([Sonnenwald, 2006](#son06)). In the broadest sense, information sharing includes providing information, receiving information supplied by other people, confirming receipt of information, and confirming that the information is jointly understood. Online discussion groups provide opportunities to examine information sharing from the standpoints of both providing and receiving information because the process of information sharing becomes documented in the messages. Therefore, it is possible to examine how the participants offer information for potential readers by writing messages and how the readers comment on them.

In studies of information sharing or of information behaviour more generally, insufficient attention has been paid to the nature of affective factors such as emotions and feelings ([Fulton, 2009](#ful09); [Lopatovska and Arapakis, 2011](#lop11); [Nahl, 2007](#nah07)). The paucity of studies reflects the dominance of the cognitive approach to information behaviour: only a residual role is assigned to affective factors. However, emotions and feelings are important, because they – together with cognitive and situational factors – influence the ways in which information is provided to others and how the recipients interpret it. In information sharing, affective factors also affect the tone of the discussion; in addition, these factors may strengthen the power of arguments. The present study fills gaps in the research by examining the ways in which emotions are expressed in information sharing. To this end, an empirical case study was conducted to examine how emotions are expressed in the sharing of information in a Finnish online discussion group focusing on a controversial topic: immigration.

This topic was chosen because it is apparent that the nature of affective elements can be discerned most clearly in discursive contexts giving rise to strong feelings and emotions, both positive and negative. Immigration is a complex phenomenon with social, cultural, economic, political, and emotional aspects (see, for example, [Berry, 2001](#ber01); [Fell and Hayes, 2007](#fel07)). In general, immigration can be defined as 'the movement of people into another country or region to which they are not native in order to settle there' ([Immigration, 2013](#imm13)). Motives for immigration include escape from poverty and the avoidance of risks to civilians during war. Immigration often leads to a variety of problems, among them ethnic separation, increasing crime rates, and conflicts in values between established residents and immigrants. Thus, it is no wonder that immigration has become an emotion-laden social and political issue all over the world. As the present study demonstrates, emotional argumentation about controversial topics such as immigration is also characteristic of information sharing taking place in online discussion forums.

Another contextual factor shaping the affective elements of information sharing is the intent of the communication, which can be specified through identification of comment types, of diverse kinds. For example, comments agreeing with an opinion are likely to be accompanied by positive emotions, while disagreements tend to incorporate negative rather than positive emotions. Drawing on these assumptions, the main goal of the present study is to elaborate the picture of emotional factors in information sharing in the context of online discussion. To achieve this, the present study seeks answers to two main questions: (i) what kinds of emotions are incorporated into information sharing online, and (ii) how do the emotions expressed by the online discussion contributors vary relative to the various comment types? Although the study concentrates on emotional expressions in the particular context of online discussion about immigration, it nevertheless contributes more generally to the body of research on affective factors in information sharing.

## Earlier research

### The nature of emotions

A number of competing research approaches exist to affective factors such as emotion, feeling, and mood. No consensus has been reached among researchers as to, for example, the nature of emotions ([Mulligan and Scherer, 2012](#mul12)). Discrete emotion theorists suggest the existence of six or more basic emotions such as anger, disgust, fear, happiness, sadness, and surprise, which are universally recognised ([Lopatovska and Arapakis, 2011](#lop11)). Appraisal theories suggest that emotions result from the evaluation (i.e., appraisal) of events occurring in the everyday environment ([Ellsworth and Scherer, 2003](#ell03)). The main idea is that as soon as the initial appraisal is made, the organism is in a sense 'emotional' in comparison to its previous state, even if not experiencing any of the full-fledged basic emotions described by discrete emotion theorists. Accordingly, the nature of this emotionality is fluid and constantly changing as appraisals are added and revised ([Ellsworth and Scherer, 2003](#ell03)).

There are a number of alternative categorisations of basic emotions. For example, Robinson ([2008](#rob08)) differentiates among eleven pairs of positive and negative emotions. The former include, for instance, gratitude, hope, and sympathy, while the latter involve emotions such as anger, disgust, and fear. For example, Robinson denotes gratitude as a warm and pleasing experience that occurs when one receives some benefit from others, and it motivates behaviour of attempts to reciprocate the benefit received. Gratitude is set in opposition to anger, which can be defined as a state wherein a subjectively unpleasant mental experience is evoked by real or imagined harm done to an individual or to what an individual values. Another typology of basic emotions is provided by Izard, Libero, Putnam and Haynes, ([1993](#iza93)), who identified three positive emotions: interest, joy, and surprise. In addition, nine negative emotions were identified: anger, contempt, disgust, fear, guilt, inward hostility, sadness, shame, and shyness. The above schemes are used in the building of the conceptual framework specified below.

### The nature of emotions in online information sharing

The use of online discussion groups has been examined in numerous studies since the 1980s in work-related and non-work contexts (see, for example, [Matzat, 2004](#mat04); [Savolainen, 2011](#sav11)). Many of the non-work-related studies review sharing of health information in a variety of subject areas, such as binge-eating disorders, depression, and diabetes (e.g., [Owen, Boxley, Goldstein, Lee, Breen and Rowland, 2010](#owe10)). Many of these studies examine the provision of informational, emotional, and social support (e.g., [Attard and Coulson, 2012](#att12)).

Dervin and Reinhard ([2007](#der07)) reviewed the ways in which emotions are approached in library and information science, along with communication research literature. Eight major viewpoints were identified. Of these, the most relevant for the present study is the approach assuming that emotions can be encoded into formalised information _packages_, for example, messages posted to online discussion forums. From this perspective, emotions can be defined as attributes of those packages with potential for impact on users, such as framing how they will respond to the information shared by others. This approach comes close to Godbold's ([2013](#god13)) idea that emotions are informational. However, the assumption can also be interpreted the other way round: a piece of information, such as a fact about the number of immigrants allowed into the United States each year, may incorporate emotional attributes affecting how this information is offered to others. For example, adding ten exclamation marks at the end of the message may indicate a strong emotional reaction against the current immigration policy. Furthermore, it is evident that the recipients would receive and interpret information incorporating certain emotional elements differently from information that is emotionally neutral.

The tone of online discussion is affected by several factors, such as the nature of the topic and the participants' beliefs and opinions surrounding it. The comments made by the participants can be facts and emotional judgements implying positive or negative preferences related to the issue at hand. In facing of opinions, preferences, and beliefs different from a participant's own, emotional reactions may be triggered ([Derks, Fischer, and Bos, 2008](#der08)). In this case, explicit communication involves references to discrete emotions through verbal emotion labels (e.g., '_I am angry_'), appraisals (e.g., '_this is scary_'), and sometimes even expressions of action tendencies (e.g., '_I'd like to hit you_'). The emotional payload of implicit emotion communication may be delivered through the emotional style of the message, as can be inferred from the degree of personal involvement and self-disclosure, for example. Further, the reduced visibility of emotions that is characteristic of computer-mediated communication strengthens the emotional style and content, and it affords the expression of emotions. In particular, expression of negative emotions tends to be more commonplace in anonymous computer-mediated communication settings than in face-to-face situations ([Derks, _et al._, 2008](#der08)).

Chmiel and colleagues ([2011](#chm11)) examined the expression of emotions in BBC discussion forums; for example, the '_Religion and ethics_' and '_World/UK news_' message boards. The findings indicate that the emotion a participant expresses depends on the emotions in previous posts. Positive emotions tended to facilitate affiliative responses, while negative emotions appeared to be more complicated in this respect. For example, anger, whether targeted at other participants directly or at the topic of the discussion, might elicit anger, while sadness, in contrast, might elicit empathic responses that also express sadness. The study concluded, however, that it does not matter which discrete negative emotions are expressed: negative statements tend to follow negative statements.

Finally, Laflen and Fiorenza ([2012](#laf12)) examined the language of emotion in computer-mediated communication. They identified linguistic features that writers used to communicate emotion in computer-mediated communication to substitute for the nonverbal emotional cues that speakers and listeners rely upon in face-to-face conversation. The analysis focused on a course of study titled '_Presidential election rhetoric_'. It appeared that the writers used evaluation devices in computer-mediated communication quite differently than they did in face-to-face conversation or _news English_. In particular, computer-mediated communication was characterised by the frequent use of critical evaluations: no fewer than 62% of the comments presented online were negative.

## The research process

### The conceptual framework

A literature review provided useful background for the development of the conceptual framework. The findings of earlier studies suggest that messages posted to online forums can incorporate a variety of emotions and that they are contextually sensitive with regard to the discussion topics. The conceptual framework was developed further through drawing on the categorisation of comment types identified by Sobkowicz and Sobkowicz ([2012](#sob12)). The two researchers examined emotion and communication patterns in a Polish political discussion group wherein debate tends to be polarised and characterised by high levels of emotion. On the basis of the intent of a post, Sobkowicz and Sobkowicz ([2012](#sob12)) identified seven comment types: agreement, disagreement, neutral statement, invective, provocation, off-topic, and switch of opinion. Except for the last of these, the above types appeared to be directly relevant for the present study. Because of their general nature, it is evident that these categories are relevant for analysis of information sharing in cases in which the discussion topic is controversial.

The above-mentioned typology was complemented by drawing on a study conducted by Langlotz and Locher ([2012](#lan12)). They approached the affective elements of online discussion by identifying three categories that indicate how emotions are incorporated into messages: i) verbal expression and visual intensification through, for example, use of emoticons; ii) implied indexing of emotional stance by means of implicature, as with the use of irony, sarcasm, and metaphor; and iii) description of emotional states with words such as horrible or disgusting. From the standpoint of the present study, the category of implicature, more specifically its sarcasm sub-category, appeared the most relevant for the analysis of the empirical data because sarcasm represents a comment type in its own right. Other categories identified by Langlotz and Locher were excluded because they are not directly relevant from the perspective of comment types. For example, visual intensification mainly indicates the graphical means by which the emotional experiences are communicated in discussion.

The comment types considered in the present study are specified below (the illustrative examples are taken from the empirical data):

*   **Agreement**. The comment agrees with an individual idea or viewpoint presented in the discussion. Example: _Yes, a reasonable suggestion. I support it._
*   **Disagreement**. The comment disagrees with an idea or viewpoint presented in the discussion. Example: _Not by any means. Governments and people may do such things, but religion does not have power of that kind._
*   **Neutral statement**. The comment is neither obvious agreement nor a disagreement, in that it communicates a fact or an individual's opinion. Example: _It has been determined that 5–10% of the French population are Muslims. In Finland, they amount to 0.5% of the population._
*   **Sarcasm**. The comment is a sharp, bitter, or cutting expression or remark on an issue, with the intent of ridiculing or putting down someone. Example: _Who is that Danish researcher? H.C. Andersen?_
*   **Provocation**. The comment is aimed at causing dissent and usually is related only weakly to the topic of discussion. Example: _Our duty is TO RESCUE AFRICA!_
*   **Invective**. The comment uses abusive, reproachful, or venomous language intended to offend or hurt the previous commentator. Example: _You are not only a racist but also a chauvinist._
*   **Off-topic**. The comment is unrelated to the topic of ongoing discussion but is without malicious intent. Example: _About 0.6% of Finnish people are autistic_.

As comments of various types indicate the diverse intents of communication, it is logical to assume that emotions given form in comments may impact on how positively or negatively an individual appraises the qualities of an object, person, or event under discussion ([Lazarus, 1991](#laz91)). The emotions encompassed by comments were classified through reference to the typologies developed by Izard and associates ([1993](#iza93)) and by Robinson ([2008](#rob08)), as referred to above. However, some of the categories identified in these studies were excluded because a detailed reading of the discussion threads revealed that the messages did not incorporate explicit expressions of emotions such as anger, disgust, inward hostility, shame, and shyness. Therefore, the following categories were used in the empirical analysis.

#### Positive emotions

*   **Gratitude**. A feeling of thankfulness and appreciation for what one has. Example: _Thank you for your reasonable and impartial comment. Comments like that are rare diamonds in this forum._
*   **Hope**. Expectation of a positive outcome: a feeling that what is wanted can be had or that events will turn out for the best. Example: _Really good that not all hope is gone – young people are the future._
*   **Joy**. A feeling experienced at the moment when hopes are realised and success achieved. Example: _I love Somali restaurants. They provide excellent food._
*   **Relief**. A feeling of success when expected failure does not arise or confirmation that an aversive event is not going to occur. Example: _Fortunately, Green-leftist brainwashing has not made any impact on young people._
*   **Sympathy**. A feeling of caring about and being sad about someone else's troubles, grief, or misfortune. Example: _Our society lives in abundance and can bestow a little bit of it on people coming here from the poorest country in the world._

#### Negative emotions

*   **Contempt**. A feeling of regarding someone or something as inferior or worthless. Example: _Skinheads are just spitting in the corners here and there, causing disturbance and not obeying Finnish law and order._
*   **Envy**. A feeling of resenting another's success or coveting another's possessions. Example: _Getting social benefits is the real motive for many immigrants and asylum-seekers coming to Finland. Finland is a heaven for asylum-seekers._
*   **Fear**. A distressing feeling aroused by impending danger, whether the threat is real or imagined. Example: _The fact is that a man with a bomb can kill hundreds of people in a second._
*   **Irritation**. A feeling of being stimulated by an event or idea of an uncomfortable kind. Example: _No need to mention it once again, because you mention it in every single thread._
*   **Guilt**. A feeling that occurs when someone believes that he or she has violated a moral standard that he or she holds dear. Example: _You may wonder about my obscure message. I was writing several messages at the same time and got mixed up._
*   **Powerlessness**. A feeling of being unable to do something positive or to stop a negative process from moving forward. Example: _Finland will be destroyed according to the Kalergi plan because Finns are so bloody stupid and no-one will protest. But what could be changed by one individual alone? Nothing._
*   **Sadness**. A feeling of disadvantage, loss, despair, and sorrow. Example: _Haters gonna hate. You just hate; it is the only thing you can do – really sad._

The conceptual framework is depicted in Figure 1, below.

<figure class="centre">![Figure1: The conceptual framework: emotions and comment types in information sharing](p662fig1.png)

<figcaption>Figure 1: The conceptual framework</figcaption>

</figure>

For the sake of simplicity, Figure 1 illustrates a case in which three participants post messages to an online discussion group. A message generally contains one or more comments on an issue at hand, and these can represent many of the comment types defined above. In addition, the comments may incorporate expressions of positive and/or negative emotions.

To illustrate the ways in which emotions are expressed in information sharing, let us assume that Participant 1 initiates the discussion thread by posting Message 1, which communicates a neutral statement (Comment A) about the employment rate among immigrants. Comment A incorporates a positive emotion – for example, sympathy with immigrants who are actively seeking jobs regardless of the economic downturn. Participant 2 reacts to Comment A by indicating agreement (Comment B), but no emotions are explicated. Participant 3 writes Message 3, containing comments C and D. Comment C communicates disagreement with Comment A and incorporates a negative emotion, such as contempt for passive job-seekers among immigrants. Message 3 also contains a neutral statement (Comment D) describing the allocation of social benefits to unemployed immigrants; this comment incorporates an expression of envy. As the discussion continues, further replies, representing diverse comment types, may be made. These can be emotionally neutral or incorporate one or more emotions, positive or negative.

As Figure 1 suggests, agreement and neutral statement are likely to be associated with positive emotions while disagreements and negatively coloured statements such as provocation are more likely associated with negative affect. However, negative affect is not always undesirable if it is used to strengthen a critical argument in a constructive way.

### The research questions

Situating itself in the above framework, the present study addresses the following research questions:

*   Research question 1: What kinds of emotions are incorporated in messages while information is being shared about immigration in online discussion?
*   Research question 2: How do the emotions expressed by the online discussion contributors vary relative to the various types of comment relevant to information sharing?

To sharpen the study's focus, no attention is given to the sequences of emotional expressions indicating the development of the dialogue among the participants. Therefore, for example, the question of whether negative emotions fuel further negative emotions is excluded from analysis (cf. [Chmiel, _et al._, 2011](#chm11)). In addition, the strength of individual emotions expressed in online discussion is not examined here. Reviewing issues such as these would require a separate study.

### The empirical data

The present study focuses on messages posted to [Suomi24](http://www.suomi24.fi/) (meaning 'Finland24'), Finland's largest online discussion platform. It is divided into twenty-three main sections, under headings such as Health, Hobbies, and Society. The Society section addresses a number of specific sub-topics – e.g., Globalisation, and Poverty. The present study concentrates on the Immigration subsection, which in November 2013 contained 216 discussion threads, with about 2,200 messages in all. The Suomi24 discussion about immigration has moderators, and racist messages are prohibited.

For the needs of the present study, fifty consecutive threads with ten or more messages, starting with those that had been most recently updated, were downloaded. Shorter threads were excluded because they seldom featured dialogue elements indicating how the participants reacted to ideas communicated in previous messages. The messages analysed in this study had been posted to the discussion group between 1 March 2009 and 11 September 2013\. The time spans of individual threads varied considerably. The longest thread covered four years and five months (1 March 2009 – 23 July 2013), while the shortest contained messages posted within one day only (7 September 2013). The 50 threads contained, in total, 1,878 messages. On average, there were 37 messages per thread. The number of messages per thread ranged from 10 to 139\. All told, 670 individual contributors posted messages, with 74% of the participants writing only one message while the most active contributor posted no fewer than 256 messages. that is, about 14% of all messages. All in all, a small number of highly active participants produced a disproportionate quantity of messages. The 1,878 messages included, in all, 2,382 comments, or 1.2 comments per message on the average.

### Analysis of the data

For an overview, the downloaded threads were read carefully several times. Then, the relevant parts of the messages (i.e., a sentence or a few related sentences featuring one or more comments) on a given topic were coded in terms of the categories specified in Figure 1, above. An individual issue (for example, the financial cost of immigration) commented upon in a message formed the unit of analysis. First, the comment type was identified from the categorisation presented above. Importantly, the same message might feature one or more comments on issues discussed in the thread. In the coding, information shared by the participants covered both facts and opinions; however, these categories are not reviewed separately in the empirical analysis, because the present study focuses on the expression of emotions in information sharing, not the types of information. A comment was counted only once for any given comment type present (for example, agreement or provocation); further instances of the same comment type were simply ignored in the coding. The coding continued with identification of the emotions evidenced in the comments. The emotions – positive or negative – were classified in accordance with the framework specified above. A code was assigned to each emotion (for example, sympathy or irritation) when it occurred for the first time in a comment. Again, multiple instances of the same emotion were ignored. Only explicit expressions of emotions were assigned a code; if a comment incorporated no explicit indications of emotions, it was classified as non-emotional.

To strengthen the validity of the coding, the initial coding was checked iteratively by this author. Because the study is exploratory and not aimed at statistically representative generalisations about online discussion groups, the requirement for consensus on coding decisions based on inter rater reliability can be compromised without endangering the reliability of the study. According to Miles and Huberman ([1994](#mil94)), check-coding the same data is useful for a lone researcher, provided that code-recode consistency is at least 90%. In line with this idea, check-coding was repeated, and the initial coding was carefully refined until there were no anomalies.

For answers to the research questions, the data were scrutinised by means of descriptive statistics. Most importantly, the percentage distributions were calculated for the comment types and emotions expressed. Secondly, the quantitative analysis was complemented by qualitative analysis that drew from the constant comparative approach ([Lincoln and Guba, 1985](#lin85)). An attempt was made to capture the variety of articulations of emotions appearing in the comments. Importantly, the sample of fifty threads appeared to be large enough for the production of a good qualitative and indicative quantitative picture of the nature of emotional elements of information sharing. It became evident that inclusion of additional threads would not have fundamentally changed the quantitative picture of information sharing in this particular context. As to the qualitative analysis, the number of comments appeared to be sufficient because the data became saturated.

In the analysis of the data, particular attention was devoted to the ethics issues. It can be argued that the need for informed consent hinges on whether messages posted to online discussion forums are private or public communications. Researchers agree that informed consent is not required if the setting of data collection is regarded as public (e.g., [Eysenbach and Till, 2001](#eys01)). On this basis, it can be claimed that messages posted to such discussion groups are intended for the public and that the contributors to online forums are aware of the possibility of their messages being read by a wide audience. On account of their public nature, messages posted to online forums may be utilised for research purposes, provided that the anonymity of an individual writer is sufficiently protected when illustrative extracts of messages are used. To this end, the participants' nicknames were replaced with neutral identifiers such as: Thread 36, Participant 12\. Given the relatively large number of threads discussing immigration, it is unlikely that such extracts could be associated with individual contributors.

## Findings

### Quantitative overview

The discussion about immigration focused on a variety of issues. The main topics of the fifty threads are specified in Table 1, below.

<table class="center" style="width:50%;"><caption>  
Table 1: Percentage distribution of the main topics of the discussion threads (n = 50)</caption>

<tbody>

<tr>

<th>Topic</th>

<th>%</th>

</tr>

<tr>

<td>Economic issues of immigration</td>

<td style="text-align:center;">24</td>

</tr>

<tr>

<td>Islam, and Muslim immigrants</td>

<td style="text-align:center;">30</td>

</tr>

<tr>

<td>Social problems of immigration</td>

<td style="text-align:center;">18</td>

</tr>

<tr>

<td>Immigration as a political question</td>

<td style="text-align:center;">12</td>

</tr>

<tr>

<td>Crime and public safety</td>

<td style="text-align:center;">10</td>

</tr>

<tr>

<td>Cultural habits and values</td>

<td style="text-align:center;">6</td>

</tr>

<tr>

<td>Employment among immigrants</td>

<td style="text-align:center;">6</td>

</tr>

<tr>

<td>Multiculturalism</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>     Total</td>

<td style="text-align:center;">100</td>

</tr>

</tbody>

</table>

As Table 1 indicates, the participants were particularly interested in economic issues of immigration, such as social benefits allocated for immigrants. In addition, topics related to Islam, and Muslim immigrants appeared to be popular. Quite frequently, the participants discussed social and political problems of immigration, such as racism and the control of immigration flows. Similarly, issues related to crime and public safety were debated fairly often, while topics such as cultural habits among immigrants, employment, and multiculturalism appeared to be less popular. On the whole, the discussions focused on immigration from African countries, particularly Somalia. This emphasis probably stems from the fact that Somalis are Finland's third-largest immigrant group by country of origin.

The 1,878 messages posted to the fifty discussion threads contained 2,382 individual comments, on diverse topics related to immigration. The distribution by comment type is presented in Table 2.

<table class="center" style="width:50%;"><caption>  
Table 2: Percentage distribution of the comment types in information sharing (n = 2,382)</caption>

<tbody>

<tr>

<th>Statement type</th>

<th>%</th>

</tr>

<tr>

<td>Neutral statement</td>

<td style="text-align:center;">64.8</td>

</tr>

<tr>

<td>Disagreement</td>

<td style="text-align:center;">11.5</td>

</tr>

<tr>

<td>Sarcasm</td>

<td style="text-align:center;">7.8</td>

</tr>

<tr>

<td>Off-topic</td>

<td style="text-align:center;">7.3</td>

</tr>

<tr>

<td>Invective</td>

<td style="text-align:center;">4.5</td>

</tr>

<tr>

<td>Agreement</td>

<td style="text-align:center;">3.0</td>

</tr>

<tr>

<td>Provocation</td>

<td style="text-align:center;">1.0</td>

</tr>

<tr>

<td>     Total (Note: due to rounding)</td>

<td style="text-align:center;">99.9</td>

</tr>

</tbody>

</table>

Neutral statements, that is, comments indicating neither obvious agreement nor disagreement, were the most frequent: two in three comments could be placed in this category. The high proportion of neutral state¬ments suggests that, no matter the inherently controversial discussion topic, most replies were not dialogic, as they did not elicit responses from other participants; most comments remained unrelated indications of opinion. As to comments of other types, disagreement and sarcastic expressions were fairly common, while comments indicating agreement with fellow participants were seen less frequently. This is probably because once an opinion has received support with no alternative views having been presented, other participants are no longer motivated to explicate agreement by saying, in effect, '_me too_'. Finally, invectives were presented fairly seldom and the amount of explicit provocation remained low.

Of all comments, 78.5% appeared emotionally neutral, while 21.5% contained explicit expressions of emotions. Table 3 specifies how the comments incorporating emotional and non emotional elements were distributed by comment type.

<table class="center"><caption>  
Table 3: Percentage distribution of the emotional and non-emotional elements by comment type</caption>

<tbody>

<tr>

<th>Comment type</th>

<th>Emotional  
(n = 513)</th>

<th>Non-emotional  
(n = 1,869)</th>

<th>Total</th>

</tr>

<tr>

<td>Invective (n = 108)</td>

<td style="text-align:center;">54.6</td>

<td style="text-align:center;">45.4</td>

<td style="text-align:center;">100.0</td>

</tr>

<tr>

<td>Provocation (n = 23)</td>

<td style="text-align:center;">30.4</td>

<td style="text-align:center;">69.6</td>

<td style="text-align:center;">100.0</td>

</tr>

<tr>

<td>Neutral statement (n = 1,544)</td>

<td style="text-align:center;">23.5</td>

<td style="text-align:center;">76.5</td>

<td style="text-align:center;">100.0</td>

</tr>

<tr>

<td>Sarcasm (n = 186)</td>

<td style="text-align:center;">18.8</td>

<td style="text-align:center;">81.2</td>

<td style="text-align:center;">100.0</td>

</tr>

<tr>

<td>Disagreement (n = 275)</td>

<td style="text-align:center;">14.5</td>

<td style="text-align:center;">85.5</td>

<td style="text-align:center;">100.0</td>

</tr>

<tr>

<td>Agreement (n = 71)</td>

<td style="text-align:center;">12.7</td>

<td style="text-align:center;">87.4</td>

<td style="text-align:center;">100.0</td>

</tr>

</tbody>

</table>

Emotions were incorporated relatively often in invective and provocation. At the other extreme, comments indicating agreement and disagreement were the least emotional, while emotional expressions were more frequent in the context of neutral statements and sarcastic comments. Since the present study focuses on the emotions in information sharing, comments without emotional elements are not discussed in detail here. Of the emotions expressed by the participants, 9.4% were positive and 90.6% negative. Table 4 specifies the distribution of individual emotions.

<table class="center" style="width:50%;"><caption>  
Table 4: Percentage distribution of emotions expressed in information sharing (n = 513)</caption>

<tbody>

<tr>

<th>Type of emotion</th>

<th>%</th>

</tr>

<tr>

<th colspan="2">Positive emotions</th>

</tr>

<tr>

<td>Sympathy</td>

<td style="text-align:center;">6.4</td>

</tr>

<tr>

<td>Hope</td>

<td style="text-align:center;">1.6</td>

</tr>

<tr>

<td>Relief</td>

<td style="text-align:center;">0.6</td>

</tr>

<tr>

<td>Gratitude</td>

<td style="text-align:center;">0.4</td>

</tr>

<tr>

<td>Joy</td>

<td style="text-align:center;">0.4</td>

</tr>

<tr>

<td>     Total</td>

<td style="text-align:center;">9.4</td>

</tr>

<tr>

<th colspan="2">Negative emotions</th>

</tr>

<tr>

<td>Contempt</td>

<td style="text-align:center;">38.0</td>

</tr>

<tr>

<td>Irritation</td>

<td style="text-align:center;">20.3</td>

</tr>

<tr>

<td>Fear</td>

<td style="text-align:center;">14.6</td>

</tr>

<tr>

<td>Envy</td>

<td style="text-align:center;">11.9</td>

</tr>

<tr>

<td>Powerlessness</td>

<td style="text-align:center;">5.4</td>

</tr>

<tr>

<td>Guilt</td>

<td style="text-align:center;">0.2</td>

</tr>

<tr>

<td>Sadness</td>

<td style="text-align:center;">0.2</td>

</tr>

<tr>

<td>     Total</td>

<td style="text-align:center;">90.6</td>

</tr>

<tr>

<td>     _**Overall total**_</td>

<td style="text-align:center;">100.0</td>

</tr>

</tbody>

</table>

Reflecting the controversial nature of the immigration issues, negative emotions were predominant. In particular, contempt and irritation were commonplace in the participants' comments. In addition, expressions of fear and envy were quite frequent. Of the positive emotions, sympathy appeared to be relatively uncommon, while positive emotions of other types remained very marginal.

To flesh out the picture, we can look at the emotional expressions in the context of comment types (see Table 5, below). To tighten the focus, the work excluded off-topic comments, because they did not incorporate emotional expressions relevant to the issues of immigration.

<table class="center" style="width:90%;"><caption>  
Table 5: Percentage distribution of the emotions by comment type</caption>

<tbody>

<tr>

<th rowspan="2">Emotion</th>

<th colspan="6">Comment type</th>

</tr>

<tr>

<th>Agreement (n=9)</th>

<th>Disagreement (n=40)</th>

<th>Neutral statement (n=363)</th>

<th>Sarcasm (n=35)</th>

<th>Provocation (n=7)</th>

<th>Invective (n=59)</th>

</tr>

<tr>

<td>_**Positive emotions**_</td>

<td style="text-align:center;">44.4</td>

<td style="text-align:center;">10.0</td>

<td style="text-align:center;">11.1</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Hope</td>

<td style="text-align:center;">33.3</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">1.4</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Sympathy</td>

<td style="text-align:center;">11.1</td>

<td style="text-align:center;">10.0</td>

<td style="text-align:center;">7.7</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Relief</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.8</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Gratitude</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.6</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Joy</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.6</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>**_Negative emotions_**</td>

<td style="text-align:center;">56.5</td>

<td style="text-align:center;">90.0</td>

<td style="text-align:center;">88.9</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">100.0</td>

</tr>

<tr>

<td>Contempt</td>

<td style="text-align:center;">22.2</td>

<td style="text-align:center;">27.5</td>

<td style="text-align:center;">29.4</td>

<td style="text-align:center;">48.6</td>

<td style="text-align:center;">42.9</td>

<td style="text-align:center;">93.2</td>

</tr>

<tr>

<td>Envy</td>

<td style="text-align:center;">22.2</td>

<td style="text-align:center;">10.0</td>

<td style="text-align:center;">10.5</td>

<td style="text-align:center;">40.0</td>

<td style="text-align:center;">42.9</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Fear</td>

<td style="text-align:center;">11.1</td>

<td style="text-align:center;">7.5</td>

<td style="text-align:center;">18.7</td>

<td style="text-align:center;">5.6</td>

<td style="text-align:center;">14.2</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Irritation</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">42.5</td>

<td style="text-align:center;">22.6</td>

<td style="text-align:center;">2.8</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">6.8</td>

</tr>

<tr>

<td>Powerlessness</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">7.4</td>

<td style="text-align:center;">2.8</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Guilt</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.3</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>Sadness</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">2.5</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

<td style="text-align:center;">0.0</td>

</tr>

<tr>

<td>In total</td>

<td style="text-align:center;">99.9*</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">100.0</td>

<td style="text-align:center;">100.0</td>

</tr>

<tr>

<td colspan="7">* rounding error</td>

</tr>

</tbody>

</table>

As is indicated by Table 5, negative emotions strongly dominate across comment types, except for the Agreement category, wherein positive emotions gained some significance. In contrast, sarcasm, provocation, and invective are comment types incorporating negative emotions only. Overall, sharing of information about immigration was most frequently accompanied by emotions such as contempt, envy, irritation, and fear. The repertoire of emotions was broadest in the context of neutral statements: all emotions apart from sadness were represented. On the other extreme, the emotional repertoire associated with invective was narrow, because one particular emotion, contempt, was predominant.

### Qualitative findings: emotions in the context of comment types

The overall picture presented in Table 5 can be honed through qualitative examination of how emotions of diverse kinds were expressed in the context of comment type. The discussion of the findings begins with the comment type that is most strongly associated with positive emotions – i.e., agreement – and ends with the comment type most clearly characterised by negative emotions: invective. Most importantly, the qualitative findings demonstrate how the same emotions can be carried in diverse comment types. For example, contempt may be directed at the cultural habits of immigrants in the contexts of agreement, disagreement, and provocation. This suggests that the emotions are flexible factors that can be employed for multiple purposes in information sharing.

#### Agreement

The participants expressed positive emotions most actively when agreeing with ideas presented by others. In this context, about 44% of the emotions were positive. In most cases, the participants expressed hope for the development of more positive attitudes toward immigration, among young people in particular.

> Really good that not all hope is gone – young people are the future. (Thread 3, Participant 8)

However, negative emotions were more commonplace when the commentators were supporting views presented by others. In this context, expressions of contempt were associated with opinions about the 'inferior' characteristics of certain immigrant groups.

> Yes, this holds true, and the reason is clear. People coming from regions south of the Sahara are unable to comprehend social units that are wider than family or clan. That's why the nation-states established by these people are chaotic. (Thread 9, Participant 3)

Agreement was accompanied also by expressions of envy. Typically, it originated from the perceived unfairness of allocating taxpayer money to the needs of immigrants.

> Yes, exactly. I find it outrageous that money taken from ordinary workers is spent for the luxurious life of immigrants. (Thread 19, Participant 12)

Fear too was found in the context of agreement. This emotion was grounded mainly in the belief that immigration increases the risk of unrest if borders are opened to people with a terrorist background.

> I agree with you. I really don't want '**war veterans**' with battle and killing experience to come here, loitering in the streets and alleys. (Thread 42, Participant 3)

#### Disagreement

The positive emotions were expressed less frequently in the context of disagreement. Nevertheless, even though a participant might have disagreed with an idea presented by a fellow contributor, the comment may have been accompanied by a positive emotion. For example, sympathy can be directed toward immigrants who are criticised without sound reasons.

> Many immigrants work in the service business. However, you are never satisfied and just invent new ways to bash them. Shame on you!!!! (Thread 8, Participant 6)

However, it was negative emotions that predominated in the context of disagreements too. The participants expressed irritation more often than in the agreeing comments discussed above (42.5% of all emotions articulated in disagreements with others). Another difference was that the irritation was primarily directed not at immigration or immigrants but at the ways in which the comments of disagreement were grounded in the debate. In particular, insufficient or unclear evidence gave rise to irritation.

> Here we go again! Your first claim was that things like that have happened throughout history, but now you switch to talking about one 'period' only. (Thread 30, Participant 32)

Disagreement was often accompanied by expressions of contempt also. Similarly to that in the context of agreement, contempt here was expressed mainly in the appraisal of personal and social traits of immigrants. This points to emotional expressions as not strictly dependent on comment type: similar emotions may be articulated in the context of agreement and disagreement. The same conclusion applies to expressions of envy. As in the discussion of agreement above, the study found that participants were envious of the abundant social benefits they believed are being allocated to immigrants. The expressions of fear resembled articulations presented in the context of agreement: it was feared that the growing number of immigrants will heighten the risk of social unrest.

In a few cases, disagreements were accompanied by expressions of sadness. This emotion is closely related to sympathy, in that both emotions indicate feeling sorrow about someone's disadvantage. However, sadness differs from sympathy in the former's indication of a negative appraisal of, for example, fellow participants' support for racism.

> Haters gonna hate. You just hate; it is the only thing you can do – really sad. (Thread 8, Participant 28)

#### Neutral statements

The majority of the comments were neutral in that they communicated neither obvious agreement nor disagreement. Again, negative emotions were predominant, even though there appeared to be a few indications of positive emotions, with a share of about 11% of all emotional expressions. In the context of neutral statements, the most frequent positive emotion was sympathy.

> Finland is a rich country. It is a moral duty of Finnish people to help minorities coming from poor countries. (Thread 31, Participant 1)

In a few cases, neutral statements also incorporated expressions of hope, gratitude, joy, and relief. However, about 89% of all emotions articulated in the context of neutral statements were negative. The participants were particularly active in indicating contempt (about 29% of all emotional expressions). Contempt was directed to the traits, personal and social, of immigrants who were classified as inferior with regard to their habits and behaviour patterns, for example. In these cases, immigrants were labelled with epithets such as social-benefit tourist. Envy appeared to be a frequent emotion that is closely related to contempt; envy appeared in cases wherein negative features such as living as '_a parasite_' in a welfare state were associated with the belief that immigrants are favoured over Finnish citizens in the allocation of social benefits.

> 3,562 euros NET per month! (Thread 24, Participant 6)

Neutral statements also incorporated expressions of powerlessness, pointing to pessimism about the possibilities for improving the unfair rules by which social benefits are allocated. It was also stated that ordinary people are unable to change the current immigration policy by, for example, voting.

> Where are the politicians now who decided against the will of ordinary people? Are they still responsible for their decisions? Let me laugh. The decision-makers can simply leave their posts, but ordinary citizens have to shoulder the costs. (Thread 17, Participant 59)

The strongest negative emotion embedded in neutral statements, however, was fear. It was mainly expressed in Islamophobic comments in the threats stemming from increasing support for Sharia law and Jihadism among fundamentalist Muslim immigrants. Of the negative emotions expressed in the context of neutral statements, irritation too was fairly frequent. As in the context of disagreement, irritation was most often associated with the problematic ways in which individual opinions were grounded. For example, intentional disregard for factual evidence provided by others was found to be irritating.

> In this forum, we have repeatedly offered you links to statistical classifications. However, it has turned out that you don't like the truth. (Thread 12, Participant 32)

#### Sarcastic comments

In comparison to emotions expressed in the context of disagreement and neutral statements, sarcastic comments appeared to be less hostile, even though they presented biting remarks on ideas offered by others. Nevertheless, no positive emotions were explicitly expressed when participants provided sarcastic comments. Contempt appeared to be the most frequent emotion in this context (accounting for about 49% of all emotions expressed). Again, contempt was directed mainly at perceived personal and cultural features of immigrants.

> It's strange that they first come to Finland to escape the war and then voluntarily leave our country to participate in battles elsewhere. Really strange people. (Thread 42, Participant 10)

Sarcasm also appeared in the context of ridicule for the seemingly insufficient cognitive capacities of fellow participants who had failed to understand ideas presented by others.

> Obviously, you face difficulties in trying to comprehend large numbers (>10). Buy an abacus. It matches well with your multiculturalist ideology. (Thread 14, Participant 86)

Almost equally, expressions of envy were characteristic of sarcastic comments. Similarly to other comment types reviewed above, envy was directed at the ways in which social benefits are allocated to immigrants.

> Immigrants do not feel at home in Finland unless you pay them well. (Thread 24, Participant 85)

Sometimes, sarcastic comments were associated with expressions of fear. Again, most of these comments were directed toward radical ideologies or religious commitments such as Jihadism that were seen as threatening to peaceful living. Unlike with disagreement and neutral statements, the participants seldom expressed irritation when communicating their opinions by means of sarcastic comments.

#### Provocation

Provocative comments were quite rare in the discussion about immigration. Again, all emotions expressed in this context were negative. The repertoire of emotions appeared to be quite similar to that seen with the sarcastic comments discussed above. Contempt and envy were articulated equally often; each of them accounted for about 43% of all emotional expressions. Distinct from other comment types, provocation was almost exclusively directed toward economic issues of immigration.

> I demand that in the capital region, refugees and all poor immigrants be provided with detached houses, free of charge! (Thread 46, Participant 1)

In a few cases, provocative comments incorporated appeals to fear.

> Muslims will take power in Finland. Lutheran people should be driven into the Baltic Sea! (Thread 14, Participant 8)

Even though the category of provocation may be associated with strong emotional reactions, about 70% of the statements of these kinds did not contain explicit emotional elements. In these cases, the provocative statements usually aimed at causing dissent by grossly exaggerating the cost of immigration.

> There are about one million refugees in Finland. The cost per refugee is 74 000 euros a year. Therefore, the total cost amounts to 74 billion euros, a exceeding the national budget of Finland. (Thread 17, Participant 67)

#### Invective

Finally, emotional expressions appeared in the context of invective. Different from provocation, invective was targeted at fellow participants. Almost always, invective was accompanied by expressions of contempt (about 93% of all indications of emotions). Often, the invective dealt with questionable ways of communicating – for example, trolling and spamming.

> Are you and [other participant] the same person – a schizophrenic with a number of multiple personalities? (Thread 1, Participant 18)

Further, criticism for vagueness of arguments was strengthened by drawing on invective. Sometimes these were based on the questioning of the opponent's cognitive abilities or on denouncing the opponent's ideological assumptions.

> Your caste classification is really weird: Indians, Mexican-origin people, and ordinary Americans. I think you are a super-racist. (Thread 37, Participant 14)

However, all invectives did not incorporate explicit emotional elements. As noted in Table 3 above, about 45% of invectives appeared to be non-emotional. In these cases, the fellow participants were not insulted directly: instead, they were characterized by drawing on oblique hints about their inability to develop credible arguments.

> Oh really, you had such a perfect dream! (Thread 18, Participant 15)

Often, the indirect strategy was also used by associating the fellow participant with a particular group of people with morally dubious ideas such as racism.

> More generally, these '**critics of immigration**' are not particularly known for their intelligence. (Thread 20, Participant 33)

## Discussion

The present study sought to two major questions. First, it was asked 'What kinds of emotions are incorporated in messages while information is being shared about immigration in online discussion?' The empirical analysis showed that 21.5% of the comments presented in the online discussion contained explicit emotional expressions and that about 91% of the emotions seen in the comments were negative. Clearly, the emotional tone of the discussion about immigration was quite antagonistic. Overall, contempt, irritation, and fear were the predominant emotions. Of the positive emotions, sympathy was expressed most frequently. Though not examined in great detail in the present work, it is obvious that the features specific to computer-mediated communication function as contextual factors shaping emotions in information sharing. The reduced visibility of emotions that is characteristic of anonymous computer-mediated communication tends to strengthen the emotional style and content, making it easier to express negative emotions in particular. This finding finds support from the research of Laflen and Fiorenza ([2012](#laf12)), who examined the language of emotion in computer-mediated communication in an educational context. They concluded that computer-mediated communication is characterised by frequent use of critical evaluations; no fewer than 62% of the comments presented in their online dataset were negative.

The second research question focused on 'How do the emotions expressed by the online discussion contributors vary relative to the various types of comment relevant to information sharing?' The empirical findings demonstrated that the types of comment indicating the intent of the communication affects the ways in which emotions are expressed when one is sharing facts and opinions with others. As the analysis of the comment types demonstrated, positive emotions are more likely to be embedded in comments agreeing with ideas presented before, while negative emotions are particularly characteristics of invectives and provocation. However, positive and negative emotions were emphasised differently from the context of one comment type to the next. Positive emotions such as hope and sympathy were expressed when one was agreeing with others or presenting neutral statements. In the context of comments of other types, only negative emotions were articulated. The repertoire of negative comments was broadest in the context of neutral statements and narrowest in presentation of invective. Importantly, emotions appeared to be flexible elements of information sharing in that the same emotion could find form in diverse comment types.

The contribution of the findings is difficult to evaluate in greater detail, on account of the paucity of comparable studies. The findings partially support the results of Sobkowicz and Sobkowicz ([2012](#sob12)), who focused on political discussion groups. They found that, while there is a rough correspondence between the intent of the comments (e.g., agreement and provocation) and associated emotions, the mapping is not perfect. A comment categorised as agreement may be expressed in highly emotional fashion while disagreement can be stated in emotionally neutral ways ([Sobkowicz and Sobkowicz, 2012](#sob12)). Therefore, the relationship between the intent of communication and the emotions expressed in discussion is not straightforward. The present study comes to a similar conclusion: emotions can appear differently in various discursive contexts, because the same emotion can be given form in diverse comment types, indicating various intents of communication.

On the other hand, the findings are not directly comparable, because Sobkowicz and Sobkowicz ([2012](#sob12)) approached emotions on a very general level by identifying five broad categories: positive emotions, neutral emotions, light negative emotions, strong negative emotions, and excessive negative emotions. Because of the general nature of these categories, the picture the study offered of the nature of emotions in Polish discussion groups remained vague. The findings of the present study suggest that such problems can be avoided through the use of a more detailed typology that specifies the qualitative variation of positive and negative emotions. In fact, the present study is unique in that earlier investigations of information sharing have not paid attention to such variation.

The empirical findings of the present study are limited in that the focus was placed on the explicit expressions of emotions. The study revealed that 21.5% of the comments contained such expressions while the remaining 78.5% were emotionally neutral. We may think, however, that all statements are by definition motivated by feeling to some degree; all messages commenting on the issues at hand are written with a motivating force incorporating some affective element. From this perspective, classifying comments as emotionally neutral does not mean that such comments have no emotional backing. One of the challenges for future research is to develop a more fine-grained classification of emotions in order to capture better the variation emotions implicit in online discourse.

## Conclusion

The present study demonstrated that emotions expressed in online information sharing are contextually sensitive in two major ways. First, the topic of discussion affects the emotional tone. It appeared that information sharing about controversial issues such as immigration is strongly marked by negative emotions: they accounted for at least 91% of all emotional expressions. Second, the emotional expressions are also contextually sensitive in that the intent of communication indicated by the comment type affects the ways in which positive and negative emotions are expressed. Positive emotions are more likely to be embedded in comments agreeing with ideas presented before, while negative emotions are particularly characteristics of invectives and provocation.

The major implication of the present study is that emotions are flexible elements of information sharing in that the same emotion could find form in diverse comment types. Since prior studies have paid insufficient attention to the nature and functions of affective factors in information sharing, it is important to develop research frameworks that conceptualize the relationships between emotions, and information giving as well as information reception, i.e., the main aspects of information sharing ([Sonnenwald, 2006](#son06)). It is obvious that conceptualizations such as these would also be relevant for the study of information use because information reception is closely related to the issues of information utilization.

Since the present study concentrated on the particular context of computer-mediated communication, further research is needed to examine these issues in other contexts, such as face-to-face discussion. In addition, the picture of the nature of emotions can be refined via examination of sharing of information about less controversial topics. Finally, there is a need to investigate how the cognitive and situational factors are related to affective factors of information sharing. Particularly in work-related studies, among the relevant cognitive factors may be perceived benefits from sharing information with others, while situational factors might involve, for instance, organisational proximity ([Wilson, 2010](#wil10)). Such studies could hone the complex picture of information sharing by showing how emotions moderate the relationships between cognitive and situational factors.

## About the author

Reijo Savolainen (PhD, 1989, University of Tampere, Finland) is currently Professor at the School of Information Sciences, University of Tampere, Finland. He may be reached at: [Reijo.Savolainen@uta.fi](mailto:Reijo.Savolainen@uta.fi)

#### References

*   Attard, A. & Coulson, N. (2012). A thematic analysis of patient communication in Parkinson's disease online support group discussion forums. _Computers in Human Behavior, 28_(2), 500-506.
*   Berry, J.W. (2001). A psychology of immigration. _Journal of Social Issues, 57_(3), 615-631.
*   Chmiel, A., Sienkiewicz, J., Thelwall, M., Paltoglou, G., Buckley, K., Kappas, A., & Holyst, J.A. (2011). [Collective emotions online and their influence on community life.](http://www.webcitation.org/6Uv17SrvO) _PLoS ONE, 6_(7). Retrieved from http://www.plosone.org/article/info%3Adoi/10.1371/journal.pone.0022207 (Archived by WebCite® at http://www.webcitation.org/6Uv17SrvO)
*   Derks, D., Fischer, A.H., & Bos, A.E.R. (2008). The role of emotion in computer-mediated communication: a review. _Computers in Human Behavior, 24_(3), 766-785.
*   Dervin, B. & Reinhard, C.D. (2007). How emotional dimensions of situated information seeking relate to user evaluations of help from sources: an exemplar study informed by sense-making methodology. In D. Nahl & D. Bilal (Eds.), _Information and emotion: the emergent affective paradigm in information behavior research and theory_, (pp. 51-84). Medford, NJ: Information Today, Inc.
*   Ellsworth, P.C. & Scherer, K.R. (2003). Appraisal processes in emotion. In R.J. Davidson, K.R. Scherer, & H. Goldsmith (Eds.), _Handbook of affective sciences_, (pp. 572-594). Oxford: Oxford University Press.
*   Eysenbach, G. & Till, J. (2001). Ethical issues in qualitative research on internet communities. _British Medical Journal, 323_(7321), 1103-1105.
*   Fell, P. & Hayes, D. (2007). _What are they doing here? A critical guide to asylum and immigration_. Birmingham, UK: Venture Press.
*   Fulton, C. (2009). The pleasure principle: the power of positive affect in information seeking. _Aslib Proceedings: New Information Perspectives, 61_(3), 245-261.
*   Godbold, N. (2013). [An information need for emotional cues: unpacking the role of emotions in sense making.](http://www.webcitation.org/6Uvk9KiMP) _Information Research, 18_(1), paper561\. Retrieved from http://InformationR.net/ir/18-1/paper561.html (Archived by WebCite® at http://www.webcitation.org/6Uvk9KiMP)
*   [Immigration](http://www.webcitation.org/6Uvjyc6dS). (2013). In _The Free Dictionary_. Retrieved from http://www.thefreedictionary.com/immigration (Archived by WebCite® at http://www.webcitation.org/6Uvjyc6dS)
*   Izard, C.E., Libero, D.Z., Putnam, P., & Haynes, O.M. (1993). Stability of emotion experiences and their relations to traits of personality. _Journal of Personality and Social Psychology, 64_(5), 847-860.
*   Laflen, A. & Fiorenza, B. (2012). 'Okay, my rant is over': the language of emotion in computer mediated communication. _Computers & Composition, 29_(4), 296-308.
*   Langlotz, A. & Locher, M.A. (2012). Ways of communicating emotional stance in online disagreements. _Journal of Pragmatics, 44_(12), 1591-1606.
*   Lazarus, R.S. (1991). Progress on a cognitive-motivational-relational theory of emotion. _American Psychologist, 46_(8), 819–834.
*   Lincoln, Y.S. & Guba, E.G. (1985). _Naturalistic inquiry_. Newbury Park, CA: Sage Publications.
*   Lopatovska, I. & Arapakis, I. (2011). Theories, methods and current research on emotions in library and information science, information retrieval and human–computer interaction. _Information Processing & Management, 47_(4), 575–592.
*   Matzat, U. (2004). Academic communication and internet discussion groups: transfer of information or creation of social contacts? _Social Networks, 26_(3), 221-255.
*   Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis: an expanded sourcebook_. (2nd ed.). London: Sage Publications.
*   Mulligan, K. & Scherer, K.R. (2012). Toward a working definition of emotion. _Emotion Review, 4_(4), 345-357.
*   Nahl, D. (2007). The centrality of the affective in information behavior. In D. Nahl & D. Bilal (Eds.), _Information and emotion: the emergent affective paradigm in information behavior research and theory_, (pp. 3-37). Medford, NJ: Information Today, Inc.
*   Owen, J.E., Boxley, L., Goldstein, M.S., Lee, J.H., Breen, N., & Rowland, J.H. (2010). Use of health-related online support groups: population data from the California Health Interview Survey Complementary and Alternative Medicine Study. _Journal of Computer-Mediated Communication, 15_(3), 427-446.
*   Robinson, D.L. (2008). Brain function, mental experience and personality. _Netherlands Journal of Psychology, 64_(4), 152-167.
*   Savolainen, R. (2011). Requesting and providing information in blogs and internet discussion forums. _Journal of Documentation, 67_(5), 863-886.
*   Sobkowicz, P. & Sobkowicz, A. (2012). Two-year study of emotion and communication patterns in a highly polarized political discussion forum. _Social Science Computer Review, 30_(4), 448-469.
*   Sonnenwald, D.H. (2006). [Challenges in sharing information effectively: examples from command and control](http://www.webcitation.org/6UvkGx2wi)). _Information Research, 11_(4), paper 251\. Retrieved from http://InformationR.net/ir/11-4/paper251.html (Archived by WebCite® at http://www.webcitation.org/6UvkGx2wi)
*   Wilson, T.D. (2010). [Information sharing: an exploration of the literature and some propositions](http://www.webcitation.org/6UvkLNBhE). _Information Research, 15_(4), paper440\. Retrieved from http://InformationR.net/ir/15 4/paper440.html (Archived by WebCite® at http://www.webcitation.org/6UvkLNBhE)