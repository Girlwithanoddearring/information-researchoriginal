<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Use of information and communication technologies in the everyday lives of Indian women: a normative behaviour perspective

#### Anindita Paul  
Indian Institute of Management Kozhikode, IIMK Campus P. O., Kozhikode, Kerala, India

#### Abstract

> **Introduction.** Information and communication technologies (information and communication technologys) have touched the lives of middle-class Indian women and enabled them to open up their lives in ways that were previously restricted. The theory of normative behaviour was used to examine data collected about the everyday life use of information and communication technology by middle class Indian women. Social and cultural aspects that affect information and communication technology use are investigated along with its influence in the women's lives.  
> **Method.** Semi-structured interviews were conducted with five participants from the Kerala region of India with follow-up telephone conversations.  
> **Analysis.** Extensive coding of the interview responses was done using Nvivo10\. The interview responses were coded and analyzed in light of the concepts of the theory of normative behaviour.  
> **Results.** information and communication technology is available to women in more ways ever; however, social norms still affect information and communication technology use. Though the women unanimously identified benefits of the internet, it seemed the women were conscious to conform their information and communication technology use to help them fulfil their traditional social roles of wife, mother, relative, neighbour, and community member. Both external and internal factors affected their information behaviour on information and communication technology devices including the models of their devices, schedules, the support received from the environment, usability issues and attitude towards technology.  
> **Conclusions.** Change is happening in the Indian society brought about by the use of information and communication technology, although it is strongly influenced by social practices and traditional roles. Future studies should investigate larger samples for specific issues highlighted in this study.

## Introduction

The information behaviour of women in India has lacked attention in the literature. Women's information needs can be diverse, ranging from household affairs and family needs to health issues and other. Studies that consider women at the centre have usually addressed a specific domain such as their profession or their use of online community- based forums, etc. There is a need for research that studies women in their natural settings and provides greater understanding of their everyday-life information behaviour as shaped by their _small worlds_. Elfreda A. Chatman ([1996](#Cha96)) defines small worlds as social worldviews individuals build to insulate themselves from those they do not trust (i.e., outsiders), which tends to have the unfortunate side effect of also keeping individuals from information that could help '_better their present condition_' ([Chatman, 1991](#Cha91), p. 443). Thus, the case of Indian woman is of interest not only because of the dearth of studies about this population, but also because the social worldview of the Indian woman has been affected by changes in environment (i.e., technology and the information infrastructure), the Indian society and the attitude of the woman herself.

The digital revolution has significantly affected Indian society by providing Indian women with a greater awareness of and access to information than ever before. It has enabled them to open up their lives beyond their previously restricted, much smaller worlds. The digital revolution has enabled women to have easier access to information and communication technology not only within the household, such as with personal computers and laptops, but also through mobile handheld devices. The dynamic changes in technology with frequent new releases of inexpensive technologies and added features have enabled women convenient access to tools that help them to meet their information and other needs. It remains to be seen how such a ubiquity of technology will affect women who have relied on their small world information sources.

This study investigates the aspects of society and culture that are reflected in the middle-class Indian woman's daily use of information and communication technology. Further, the influence of information and communication technology in their lives is also examined and factors that determine the use of information and communication technology for their everyday information seeking are explored. Five women in the Kerala region of South India were interviewed about their adoption and use of information and communication technology in their daily lives. The participants were chosen for this study because of some similarities, i.e., they are all women who are identifiable as belonging to India's growing middle class, all are in their early- to mid-30s, all live in the same region, all are married and are mothers, all are employed and all have tertiary credentials. They were not chosen to represent all of Indian women, but rather they were selected as a means to explore themes related to middle class Indian women's use information and communication technology, as little is known about digital inclusion among this demographic. Although the five women share some similarities, they vary in some ways as well, such as their family background, upbringing, religion and current family set up. The data were coded qualitatively using Nvivo10\. The data were analyzed and coded using Chatman's ([2000](#Cha00)) theory of normative behaviour as framework.

#### Theory of normative behaviour

Chatman's ([2000](#Cha00)) theory of normative behaviour contributes to our understanding of the small world lives of women in India. Traditionally, women in India were confined to their homes and their exposure was limited to their nuclear and extended family. The women's domain of responsibility was the inside and related - family, children, cooking and home duties. According to Chatman's theory, it is '_the common or routine events that characterize the everyday reality of people who share a similar cultural space_' ([Chatman, 2000](#Cha00), p. 10). She continues:

> The small world of their lives lacks sweeping surprises. One conducts the business of living in such an uneventful way that few aspects are worth important discussion. Most occurrences in this world are predictable. Much of the information that holds this world together is appropriate, legitimate, and has a rightful place in the general scheme of things. Even the activity of information seeking can be viewed as normative. That is, one looks at the world with some degree of interest... [but]... Most of the information produced from the large world has little lasting value. ([Chatman, 2000](#Cha00), p. 10)

The small world is constructed by _social norms_, _worldviews_ and _social types_ that shape _information behaviour_ ([Chatman 2000](#Cha00)). These concepts of the framework will be discussed in more detail in the findings.

Chatman suggests this life in a small world contributes to an information disadvantage or even information poverty. Britz ([2004](#Bri04)) defines information poverty as

> that situation in which individuals and communities, within a given context, do not have the requisite skills, abilities or material means to obtain efficient access to information, interpret it and apply it appropriately. It is further characterized by a lack of essential information and a poorly developed information infrastructure. ([Britz, 2004](#Bri04), p. 194)

Researchers in information sciences have emphasized the importance of examining information poverty and factors leading to it (e.g., [Childers and Post, 1975](#Chi75); [Britz, 2004](#Bri04); [Burnett, Jaeger and Thompson, 2008](#Bur08); [Thompson, 2006](#Tho06)). Britz ([2004](#Bri04)) cautions that one needs to be careful about not overlooking aspects of culture, language, education, or ability or inability to access or benefit from information. He stresses the complexity of information poverty by indicating divides between countries, societies and boundaries and even sections of the same physical space and culture. Burnett, Jaeger and Thompson ([2008](#Bur08)) have also emphasized the need to look at normative problems within the small world of marginalized communities, particularly in terms of how physical (technology, infrastructure), intellectual (literacy, language), and socio-cultural factors (social inclusion) can either create barriers to or supports for information access and use.

Though Indian women in their small worlds might be considered as marginalized in many ways in Indian society, and might even be considered information poor, as they are restricted from the outside world in many ways, the author feels that information and communication technology enables women to meet more of their information needs than was possible previously, as they are increasingly able to access external information from within their small worlds. The extent of such an influence is to be determined.

Various socio-cultural factors in women's use of information and communication technology in their daily lives can be understood by applying the theory of normative behaviour. The theory has been applied in the past to communities and groups such as virtual communities and feminist booksellers ([Burnett, Besant and Chatman, 2001](#Bur01)) and has recently been expanded by Jaeger and Burnett ([2010](#Jae10)) to create a theory of information worlds, which blends Chatman's normative behaviour theory with the Habermasian concepts of the _lifeworld_ and _social sphere_. The current study, however, seeks only to examine the small worlds of the women interviewed and the ways in which information and communication technology may or may not affect these normative structures. Thus, Chatman's theory of normative behaviour is applied to a data collected from married, middle-class, employed Indian mothers who are heterogeneous in terms of profession, neighbourhood, family make-up, and interests. Their homogeneity allows us to see commonalities across the group in terms of social position in society, and their heterogeneity allows us to view a range of factors that influence the information behaviour of these five Indian women.

#### Women and their use of information and communication technology

information and communication technology has contributed to social change in India and sex roles within and outside the family are also changing. Hilbert ([2011](#Hil11)) recognizes how in developing countries women are using information and communication technology differently now than in the past. He notes women's positive attitudes, good communication skills and media capacities as positive qualities that will allow them to overcome any inequalities in information and communication technology usage. In Iran, women have found their freedom of expression materialize through information and communication technology channels such as blogs, allowing them to discuss women's issues in the public sphere and organize rallies, online petitions and street protests ([Shirazi, 2012](#Shi12)). According to Shirazi, women's blogs have been among the most visited and reader-responsive blogs within the Iranian blogosphere, whereas other media channels (TV, radio, print) would not have enabled such freedom of expression, as they are controlled by the government.

Women tend to be considered more reticent users of technology ([Howcroft and Trauth, 2008](#How08); [Jain, 2006](#Jai06)); however, they are finding use for information and communication technology in their daily lives. Though information and communication technology can empower women, a lot depends on the access and actual usage of it ([Hilbert, 2011](#Hil11)). There are cultural and traditional expectations for women that influence their information and communication technology use. Castano and Webster ([2011](#Cas11)) suggest that various contextual elements should be considered when examining women and their information and communication technology use. They point out that different life- course events affect women's information and communication technology use. Choices are also influenced by domestic as well as workplace components and other social contexts such as sex regimes in the society, family structures and employment cultures. National cultures affect information and communication technology use attitudes and usage as well ([Li and Kirkup, 2007](#Lin07)).

Maghferat and Stock ([2010](#Mag10)) found certain dissimilarities in women's information search processes on the web as compared to those of men. They found that women behaved cautiously in choosing search sources. They decided either on very familiar sources they knew skilfully or sources that were assigned to them. Further, Lim and Kwon ([2010](#Lim10)) too found women used Wikipedia, with '_more cautious or conservative attitudes, emotions, and behaviour than their male counterparts_' ([Lim and Kwon, 2010](#Lim10), p. 2019). Halder, Ray, and Chakrabarty ([2010](#Hal10)) found in a study of 600 university students in West Bengal, India that the female students were as adept with information seeking as their male counterparts when seeking, using primary, secondary and tertiary resources found both inside and outside of their institution, and in using both formal and informal sources. The only area in which the female students scored lower than their male counterparts was in the diversity of their information searches.

In an American study of female health information seeking, Warner and Procaccino ([2004](#War04)) found that women have a low awareness of health and medical information resources and employ weak search strategies. Family and friends were typically instrumental in health information seeking. The study indicates the uncertainty in relying on internet repositories for crucial health information. With increasing penetration and use of information and communication technology over the ensuing decade, however, technology use habits have possibly changed and women may have developed strategies for better use of information and communication technology.

Australian researchers Ghobadi and Smith ([2011](#Gho11)) used activity theory to understand what influence information and communication technology has had in transforming eastern women's feminist related attitudes to feminist related behaviour. They identify factors affecting women's use of information and communication technology to include fear of technology, cyber-security concerns, as well as strategic use of technology. Despite these barriers, women are often themselves seen as strong intermediaries of information dissemination because of the positive stereotypes they hold such as attributes of being caring, approachable and social. information and communication technologys enable women to help others with their information needs. Urquhart and Yeoman ([2010](#Urq10)) call for an inquiry into women's use of information and communication technology in terms of whether use can strengthen social connections or make new connections as well as affect past connections.

This study is an attempt to look at the affect of the information age on Indian women's information behaviour. It further seeks to identify the changing lives of Indian women from their restrictive past with regards to information access and its repercussions with respect to the four concepts of the theory of normative behaviour.

## Methodology

The current study takes an interpretivist approach to look at a particular Indian social context. Within the purview of the interpretivist paradigm, each and every method used for observing the same phenomenon will potentially result in different interpretations of what is observed, and so acknowledge that my interpretations of the data collected are influenced by my own knowledge, experience, and understanding of the data and the context from within which the data were collected, and my own Indian background and knowledge of the context was instrumental in data interpretation. I have tried to increase objectivity by looking for broad themes that pose interesting questions rather than try to prove anything to be fact. I acknowledge that the sample size is limited and the findings are not generalisable; however, qualitative research is not intended to result in generalisable findings, but may result in findings that may apply to other similar groups (transferability). This transferability will be increased through the careful provision of context and the central research assumptions and limitations, so the reader can know all the limitations and thus make well-informed decisions whether or not to transfer.

India is a vast country with multiple cultures. This study is situated in the Southern India region of Kerala which has a high sex ratio (1084 female for each 1000 male; [Government of Kerala, 2011b](#Gov11b)) as well as a relatively high literacy rate for women (92.98 percent; [Government of Kerala, 2011a](#Gov11a)) and the status of women is reportedly high compared to other states of India ([Government of Kerala, 2001](#Gov01)). Even so, women in India play an important role in the household, and there is still a wide gap in information and communication technology use by men and women. Indian women, owing to various factors of culture, behaviour, habit and environment, have not adopted information and communication technology to the optimum in their daily lives.

The participants of the study were identified through snowball sampling based on the demographics mentioned earlier (tertiary graduates, aged early- to mid-30s, married, mothers, employed, users of information and communication technology in the state of Kerala). Each participant was asked if she could recommend someone else from her social network who uses information and communication technology and might be willing to be interviewed. Table 1 summarizes the profiles of the five participants. All the participants were interviewed using semi-structured interviews of at least an hour and then engaged in follow-up phone calls and participant diaries. Data were collected from October 2012 to January 2013 and again from June to August 2013\. NVivo10 was used to code the interview responses for the four concepts of the theory of normative behaviour: _social norms_, _worldview_, _social type_ and _information behaviour_.

Clearly, not all social norms, worldviews, social types or information behaviour will be limited to a single sex, class, or culture. Men in Indian society may face many of the same issues identified here, as may children, teens, the elderly, and other demographics. I do not claim that the information activities identified here are exclusive to Indian women. The objective is not to be exclusive, but rather to explore social norms, worldviews, social types, and information behaviour that the women expressed and that have bearing on their information access.

<table class="center" style="width:100%;"><caption>  
**Table 1: Participant profiles**</caption>

<tbody>

<tr>

<th>Participants</th>

<th>Age</th>

<th>Members in family - by sex (M/F) and by age group (m-minor)</th>

<th>Family structure</th>

<th>Profession/industry</th>

<th>information and communication technology ownership</th>

<th>Lifestyle</th>

</tr>

<tr>

<td style="vertical-align:top;">Participant 1 (P1)</td>

<td style="vertical-align:top;">32</td>

<td style="vertical-align:top;">2M, 2F, 2m</td>

<td style="vertical-align:top;">Joint</td>

<td style="vertical-align:top;">Entrepreneur/hotel</td>

<td style="vertical-align:top;">iPod, iPad, Samsung Duo smartphone,</td>

<td style="vertical-align:top;">Upper middle class, fairly busy with work, not much domestic responsibility.</td>

</tr>

<tr>

<td style="vertical-align:top;">Participant 1 (P1)</td>

<td style="vertical-align:top;">32</td>

<td style="vertical-align:top;">2M, 2F, 2m</td>

<td style="vertical-align:top;">Joint</td>

<td style="vertical-align:top;">Entrepreneur/hotel</td>

<td style="vertical-align:top;">iPod, iPad, Samsung Duo smartphone,</td>

<td style="vertical-align:top;">Upper middle class, fairly busy with work, not much domestic responsibility.</td>

</tr>

<tr>

<td style="vertical-align:top;">Participant 2(P2)</td>

<td style="vertical-align:top;">35</td>

<td style="vertical-align:top;">1M, 4F, 2m</td>

<td style="vertical-align:top;">Joint</td>

<td style="vertical-align:top;">Anaesthetist and Faculty/medicine</td>

<td style="vertical-align:top;">iPod touch, Samsung smartphone, laptop, personal computer</td>

<td style="vertical-align:top;">Middle class, busy lifestyle where she manages household responsibilities, family and work.</td>

</tr>

<tr>

<td style="vertical-align:top;">Participant 3 (P3)</td>

<td style="vertical-align:top;">34</td>

<td style="vertical-align:top;">1M, 1F, 1m</td>

<td style="vertical-align:top;">Nuclear</td>

<td style="vertical-align:top;">Dentist and entrepreneur/medicine</td>

<td style="vertical-align:top;">Smartphone, laptop, personal computer</td>

<td style="vertical-align:top;">Middle class; busy mostly with work, not too busy with domestic responsibilities; does not have large social circle; home-centred lifestyle.</td>

</tr>

<tr>

<td style="vertical-align:top;">Participant 4 (P4)</td>

<td style="vertical-align:top;">35</td>

<td style="vertical-align:top;">1M, 1F, 1m</td>

<td style="vertical-align:top;">Nuclear</td>

<td style="vertical-align:top;">Bookseller and radio-announcer/media and publishing</td>

<td style="vertical-align:top;">Feature phone, PC</td>

<td style="vertical-align:top;">Lower middle class, busy during standard office hours; busy social life; spends time in hobbies (such as frequent tours, watching plays, learning and writing about dance), does not have many domestic responsibilities</td>

</tr>

<tr>

<td style="vertical-align:top;">Participant 5 (P5)</td>

<td style="vertical-align:top;">33</td>

<td style="vertical-align:top;">1M, 1F, 2m</td>

<td style="vertical-align:top;">Nuclear</td>

<td style="vertical-align:top;">IT professional/education</td>

<td style="vertical-align:top;">Smartphone, laptop</td>

<td style="vertical-align:top;">Middle class; busy with household and family responsibilities; hired domestic help to care for the minors.</td>

</tr>

</tbody>

</table>

<section>

## Findings and discussion

The everyday lives of Indian women are governed by expectations from society, environment, culture, family beliefs, and so forth. information and communication technology and culture can give rise to complexities that may confound interpretations owing to its bidirectional nature of cause and effect ([Fichman and Sanfilippo, 2013](#Fic13)). Using Chatman's ([2000](#Cha00)) theory of normative behaviour, the data were analyzed in light of the four concepts: _social norms_, _worldview_, _social typing_ and _information behaviour_.

_Social norms_  
Social norms allow for rightness and wrongness in social appearances. The concept provides a way to gauge what is normal in a specific context at a specific time. It points the way to assess acceptable standards and codes of behaviour in a social context ([Chatman, 2000](#Cha00)). For the participants, what they considered normal is reflected in their daily life information seeking. All five of the participants have used information and communication technology (including telephony, television, radio, computers) for most of their lives, and the internet for five years or longer, although they ranged in their feelings of personal technological acceptance. P1, for example, said she feels very technologically savvy with a range of information and communication technologys and is an individual who attracts questions related to technology from her friends and relatives. She is an avid user of her iPad and smartphone and is updated about the latest apps. Over time, she has switched over to more frequent use of the latest technology on handheld devices rather than rely on the bulkier personal computer. The devices enable her to fulfil her personal and professional roles such as checking emails, browsing the web, making printouts, scanning documents, taking pictures, using apps for leisure and for official purposes, etc., all things for which she had some responsibility before her iPad and smartphone were adopted, these tools simply make her normal roles easier to enact.

P2, a medical professional married to a neurosurgeon, sees her role as a family member as predominant in her every-day life. She cares for her family not only in terms of health related information needs (particularly pertaining to her mother-in-law and son) but also other information needs. When asked how she seeks information for daily needs such as clothing or school-related information, her response was a ready: '_Almost [always,] first is internet only. Maybe medical issues I look up in my text books, but the rest, usually I go to [the] internet only_'. Her role as information provider extends beyond the family and into the workplace as well. It is quite evident from her interview that she is close to her colleagues. When at home, she helps them with their information needs such as drug doses, lab investigations and normal values since the organization did not have good connectivity.

She also supports her husband's information needs, as does P3\. Social norms in India dictate that women should support their husband's needs first when he is the primary bread-winner of the family. Though times have changed and women working outside the home provide income for the family as well as the husband, the traditional model of the woman remains largely the same. The data indicate that technology has helped the participants provide access to information despite the many external responsibilities these women have shouldered. P2, P3, and P5 indicated how their husbands were even busier than they, and hence they, the women, had to take upon themselves more of the family's information responsibilities. Both P2 and P5 also said they extensively utilised the internet for health information seeking, primarily in their roles as carers for others, and benefitted from it, enabling them to be satisfied mothers.

This was true even for P3 who indicated she is not as information and communication technology-savvy as the others. P3 was responsible to access to her husband's email and Facebook account for regular checking, and it has been a normal practice for her to do so. Interestingly, although P3 was a regular user of technology, she did not feel it was essential for her own information needs:

> I don't know much about [information and communication technology] and I am not sure how much it will be helpful for me, that's a fact [and] for the same reason I don't think I'll be in a trouble if anything like [information and communication technology] is not there. [P3]

She seemed to have decided technology falls within the males' domain, and she said she expressed her discomfort with technology to her male colleagues and sought their support. She manifested some traditional dependencies on her husband; however, she also stated that she is unable to relate to the stereotypical image of Indian housewives. She uses technology daily to Skype with her sister who lives in a distant city, but seemed reluctant to consider herself a dedicated user of information and communication technology in her daily life. This may have been due to her belief that technology skills are not intrinsic to traditional Indian social norms for women. This idea requires further exploration.

P4 only began using the internet in 2009, and her information behaviour has not been as richly affected by information and communication technology as the others interviewed, although she does regularly use email, Facebook, and the internet for searches related to her work as a journalist and her dance hobby. She has a PC and a Nokia X202 mobile phone, but she said she is not comfortable using the phone for internet searches. A frequent traveller, she said that although she knows there are hotel reviews and other travel information online, she prefers to ask her friends and colleagues for information such as which hotels to use and other details. Although she describes herself as a scanty user of information and communication technology, nevertheless she identifies with the importance of information and communication technology as much as the other four participants.

P5, who belongs to a conservative Muslim community, has done much to break the stereotypical image of women in her community. She has pursued higher studies in computer engineering and maintained her interest and inclination towards new technology. She is an IT professional and as such, has found her skills to be beneficial for her social community. She had been entrusted with account details and credit card information by her community members to check their email and transact online on behalf of them. She has influenced her friends, neighbours and acquaintances to accept information and communication technology more in their own lives and has promoted information and communication technology as benefitting their children's education and improving business. She has also helped her parents in their information needs online. Her typical social role is based on multiple aspects of being an IT professional, a mother and a community member of close bonded community. From the above it can be inferred that each of the women have made choices about information and communication technology adoption based on social norms. Some of these choices have helped them enhance their traditional roles, while their role of information provider may be expanding.

_Worldview_  
Worldview concerns the collective perception of members of a social world about things that are important or trivial. It

> provides a collective approach to the overall importance of things, and ensures that details do not all have the same value as they enter an individual's awareness. Rather, through the collective worldview, the learning of perception in concert with others alerts members of a small world to become conscious of those things that they ought to know. ([Burnett, Besant and Chatman, 2001](#Bur01), p. 537)

Worldview with respect to information and communication technology was found to be somewhat bidirectional in the case of the women interviewed. The women presented a worldview of information and communication technology based on social norms, and information and communication technology, through access to a wider array of information resources, shaped the women's worldview and social norms. With the information age, women have access to an overflow of information. The unrestricted flow of information and its influence on the worldview of women is obvious among the participants. The participants shared a positive view of online information, albeit with reservations. It was evident that they counted on the online medium unhesitatingly for some needs. It was interesting to see how the women have enthusiastically warmed to online information to help them meet their own and their families' different needs. The online forums that they could easily connect to further influenced their acceptance of information and communication technology. They could now seek book reviews, health and other information. In one instance, P1 mentioned how she regretted when she bought a book even knowing about its unfavourable reviews. Data collected from the other participants-P2, P3 and P5-indicate that they too felt the influence of reviews in shaping their opinion. P5, for example, said she has sought reviews from top academic institution sites or Google searches to find student reviews of university courses.

P2 and P5 reported that they have used the internet to help them seek and understand health information beyond that which was provided by their health professionals. P2 searched for information related to her son's allergy that she could not get from the paediatricians at the town's hospitals. P5 had an issue deciphering some medical jargon that she was unable to understand during her consultation with the doctor regarding her child's urinary tract infection that was causing fits, so she looked online for additional information. She reminisced:

> I found [a doctor's opinion online]. I ... understand most of the doctors.have other patients [they have to attend to] and they don't have time, so, [I found from a] Google search . that [the infection] came from the Pampers, the [nappy] that we're using. We buy it from shops, but [from the internet I found] that it will have [bacteria] that may have caused the fits. [P5]

Here, it is important to consider the role of information and communication technology in shaping these women's notions of the world, their worldview. One can postulate that the online mode is becoming a trusted part of their lives, and that the line that separates the outside from the inside is blurring. This especially holds significance in understanding the small worlds of women that have extended to the online worlds they can access with the help of information and communication technology. Women are now able to access more information with less effort, and this is helping to shape their worldviews.

_Social types_  
Social typing is a way for individuals to determine their expectations of conduct in the world around them.

> Social types allow for sharing or hindrance of information. That is, if a specific individual's type is viewed as desirable within their small world, resources (including information) offered by that individual to that world would be readily accepted and disseminated. However, if the individual is an undesirable type, he or she will have difficulties in overcoming this classification, and information coming from this person may not be easily accepted or believed by others. ([Burnett, Besant and Chatman, 2001](#Bur01), p. 537)

Social typing is a way to help reduce the need to continually evaluate and re-evaluate the trustworthiness of information sources. Once a resource is typed as trustworthy, one does not have to evaluate the source over and over again each time it is used. It is trustworthy until it proves otherwise. Because social typing saves time, it is likely that the ever-busier lives of the middle class wife and mother will rely more and more on social typing to help her navigate her information world.

Social typification in this study's context extended beyond the physical and into the virtual worlds of these women. Although information and communication technology provides what might be considered external information (i.e., information generated outside one's small world), the devices allow access to external information without the need to necessarily create external social networks and hence can be integrated into the domain of the female responsibilities and social norms. The online communities with which these women engage for information seeking mostly consists of strangers unknown to the woman, but the online information, particularly reviews, were evaluated with a certain level of trust. The online community consisted of sellers, buyers and other users generating reviews, mothers sharing and seeking health solutions or knowledge workers sharing their knowhow. The following experience illustrates how one participant's trust in her online information source was vindicated when shopping for an iPad cover. She recollects:

> [In the] store and while I was talking to the sales person there, I figured out he didn't know much about [the iPad cover]. I told him that I wanted a cover for my old iPad but instead he gave me the new one and said this would fit. I wasn't carrying my device but I knew [the new iPad cover] won't fit... In all the reviews [online, I knew] this was lighter and the dimension was smaller. But the sales person said I was wrong and that he was dealing with Apple products every day.

The salesperson insisted that he was right and went on to argue with P1, assuming her ignorant of information she might not have had had she not typed the information gathered online prior to the interaction as trustworthy. Women's use of information and communication technology for product reviews and price comparisons may contribute, or at the very least allow Indian women to feel more confident in their power to negotiate in the marketplace and perhaps elsewhere.

_Information behaviour_  
The concept of information behaviour has been used to portray the broader context in which information seeking happens. Burnett, Besant and Chatman ([2001](#Bur01)) define information behaviour as a state in which one may or may not act on available or offered information. They assert:

> the concept of information behaviour allows a way of understanding why intelligent, reasonable persons, who have definable information needs, may never even begin the process of searching for information, may give up a search process before finding the information they need, or may decide simply to wait passively until the information comes to them. ([Burnett, Besant and Chatman, 2001](#Bur01), p. 537)

Various factors may affect the information behaviour of an individual as was evident in the case of the participants. There were external and internal factors that decide their information seeking over the internet. A considerable variation in information behaviour among the five participants explains the significance of all such factors. P1 attributes motivations for her information behaviour to depend on the availability of time and the necessary devices and connectivity for accessing information. She has downloaded and organised different apps on her iPad to help her save time. P2 and P3 reportedly use information and reviews online to help them save time while shopping. However, P1 and P5 used the review information for a different motive: to get a better understanding about the product to be purchased that sometimes lead to an online purchase.

P4 and P3 were the least of information seekers using information and communication technology. P4 was a rare user of information and communication technology. This was true despite the fact that, P4, a journalist and bookseller, indicated that she had more job-related information needs than P3, a dentist. P4 had a strong friend circle to rely on and hence she felt she could manage without deep involvement with information and communication technology herself. She certainly uses information and communication technology regularly, only with more caution than others, including her friends. Her friends have supported her information seeking to the extent that they have entrusted her with their own passwords to e-commerce sites so that she can perform transactions over the web, such as booking train tickets for herself online. When e-payments are required, her friends become intermediaries for such transactions and she pays them in turn with cash.

For P4, access to a computer was constrained. Though her family owns a computer at home, her writer-husband is mostly engaged with it. P4 manages with her office computer or a cybercafé. In P4's case she was unable to use her Nokia feature phone for internet access due to lack of knowledge and usability issues. In her words-

> I think I want to use internet in my phone because after [the book shop started] I have no computer [on my desk]. I don't know how can I use internet smoothly [on my mobile], [I have used my mobile for internet]. One or two times I have taken a [broadband plan too], but I am not so satisfied with that. One time I searched for something and I couldn't see it, after downloading it. [P4]

Because of difficulties while using her cell phone for internet access, eventually she gave up.

Although P3, had higher quality technology than P4 in terms of technology access both at office and home, she was still a low user of information and communication technology. Her smartphone was designed for internet access with a touchscreen, but because she has ready internet access through her office laptop at work and her home computer at home, she said she has never used her smartphone for accessing the internet, although she has used her husband's phone to access internet information and Facebook for him. P3 discussed her unwillingness to give technology a place in her life. She said one strong barrier to her engagement with information and communication technology was difficulties of maintaining unique passwords. She stated

> [I face a] problem with the passwords. I have tried with [the] music download I do once in a week maybe after two, three weeks or after a month I try again, but other time I would [forget] my password. I can't [reset the password with the same word], so, I leave it there. Even for the [Indian railways website], I've [set my password once but] I lost the password paper. So I cannot use [many] names.[P3]

P3 said she considers herself as technologically incapable because she did not learn to use a computer through a formal process. She has taken steps such as hiring a tutor and urging her son's friend to teach her to use the internet, but due to unavailability of time these attempts fell through. Another constraint is the availability of a credit card to perform online transactions, as hers is an add-on credit card on her husband's card. She cannot independently use the card without checking with her husband about the minimum balance. She stated: '_When I am free he'll be working in the evening, so I can't call him and ask which card should I use? I am not sure if it is activated, so I just leave it like that_'.

As for P2, the anaesthetist, her information behaviour is affected by network issues that disable connectivity. She requires her smartphone to refer to lab values but due to lack of connectivity at her work she accesses it from home to help her colleagues at work. Further P2's information seeking is mostly dependent on the needs of her family. She is the information centre for her family and some friends too. She says her busy schedule as well as connectivity issues do not allow her to browse for leisure. Due to bad connectivity she had to discontinue reading the news from her phone while on breaks at her workplace.

It can hence be said that information and communication technology has influenced the information behaviour of women, not only its ownership but attitude towards it. The time spent on an information and communication technology device contributes to the level of confidence in handling it, but time is also noted as one of the factors that reduced use, as learning how to navigate e-commerce sites, adjust passwords, and other technical adjustments seemed overwhelming for some of the women interviewed. Clearly this is not an issue only for women; they are issues that need consideration for all users. As newer models of smartphone and operating system updates focus more on increasing usability, users may recognize the benefits of additional features in newer models over the older ones and hence adapt faster to any change in technology.

## Conclusion

Based on the four concepts of social norms, worldview, social type and information behaviour, it was found that women's use of information and communication technology is affecting their lives and their roles in the family and community. There seems to be a place for information and communication technology within the realm of traditional social norms, and the women seemed intent on keeping transformation of these norms to a minimum. Family, friends and community members, on the other hand seem to have welcomed the added information support the woman can now provide. While some of the participants were enthusiastic about the benefits of information and communication technology in their small worlds, others were less engaged, reflecting attitudinal differences that may influence information and communication technology adoption among Indian middle class women. The benefit of information and communication technology in the lives of all five participants, however, is evident. They are able to sustain relationships over distances using Facebook and Skype, inform their shopping decisions using online reviews and engage in e- commerce, and locate information about health and business concerns that benefit not only themselves but also augment their role as information intermediary in the home, community, and workplace. The participants reflected a happiness to conform to the social types of wife, mother, daughter-in-law, neighbour, caring relative in spite of being professionally busy, and found that, while sometimes too busy to use information and communication technology as well as they thought they might, they all felt it had a added value to their everyday lives. Further study is needed to expand upon these findings within the specific contexts of everyday life information seeking and its impact on the perceptions of women in the larger society. Specific communities across India also need to be studied to reflect the cultural variation across the subcontinent.

## Acknowledgements

I would like to acknowledge gratefully the support of the Indian Institute of Management Kozhikode for the grant monies the Institute provided for this project. I would also like to thank Professor Anupama Saxena and Assistant Professor Deepa Ray for their insights, and Dr Kim M. Thompson for her help with edits and revisions both before and after the review process. Finally, I greatly appreciate the helpful input provided by the two ISIC 2014 reviews received, as they helped shape the final version of this article.

</section>

#### References

*   Britz, J. J. (2004). To know or not to know: A moral reflection on information poverty. _Journal of Information Science, 30_(3), 193-204.
*   Burnett, G., Besant, M. & Chatman, E. A. (2001). Small worlds: Normative behavior in virtual communities and feminist bookselling. _Journal of the American Society for Information Science and Technology, 52_(7), 536-547.
*   Burnett, G., Jaeger, P. T. & Thompson, K. M. (2008). Normative behavior and the multiple layers of information access: Physical, intellectual, and social. _Library & Information Science Research, 30_(1), 56-66.
*   Castano, C. & Webster, J. (2011). Understanding women's presence in information and communication technology: The life course perspective. _International Journal of Gender, Science and Technology, 3_(2), 364-386.
*   Chatman, E. A. (1991). Life in a small world: Applicability of gratification theory to information-seeking behavior. _Journal of the American Society for Information Science, 42_(6), 438-449.
*   Chatman, E. A. (1996). The impoverished life-world of outsiders. JASIS, 47(3), 193-206.
*   Chatman, E. A. (2000). Framing social life in theory and research. _The New Review of Information Behaviour Research, 1_, 3-17.
*   Childers, T. & Post, J. (1975). _The information-poor in America_. Metuchen, NJ: Scarecrow Press.
*   Fichman, P. & Sanfilippo, M. R. (2013). Multiculturalism and information and communication technology. In G. M. (ed) (Ed.), _Synthesis Lectures on Information Concepts, Retrieval, and Services_. (pp. 5-29).
*   Ghobadi, S. & Smith, S. (2011). The employment of online communities of practice for manifesting feminist behaviors among eastern women. _PACIS 2011 Proceedings_.
*   Government of Kerala. (2001). Status of Women. Retrieved 13.11.2013\. from http://www.old.kerala.gov.in/education/status.htm.
*   Government of Kerala. (2011a). Literacy rate 2011\. http://www.kerala.gov.in/index.php?option=com_content&id=4007&Itemid=3187
*   Government of Kerala. (2011b). Sex-Ratio 2011\. Retrieved 13.11.2013, from http://kerala.gov.in/index.php?option=com_content&view=article&id=4006&Itemi d=3186
*   Halder, S., Ray, A. & Chakrabarty, P. K. (2010). Gender differences in information seeking behavior in three universities in West Bengal, India. _The International Information & Library Review, 42_, 242-251.
*   Hersberger, J. (2002/2003). Are the economically poor information poor? Does the digital divide affect the homeless and access to information? _The Canadian Journal of Information and Library Science, 27_(3), 45-63.
*   Hilbert, M. (2011). Digital gender divide or technologically empowered women in developing countries? A typical case of lies, damned lies, and statistics. _Women's Studies International Forum, 34_(6), 479-489.
*   Howcroft, D. & Trauth, E. M. (2008). The implications of a critical agenda in gender and IS research. _Information Systems Journal, 18_(2), 185-202.
*   Jaeger, P. T. & Burnett, G. (2010). Information worlds: Social context, technology, and information behavior in the age of the internet. New York: Routledge.
*   Jain, S. (2006). _information and communication technologys and women's empowerment: Some case studies from India_: Department of Economics at LakshmiBai College, Delhi University.
*   Li, N. & Kirkup, G. (2007). Gender and cultural differences in Internet use: A study of China and the UK. _Computers & Education, 48_(2), 301-317.
*   Lim, S. & Kwon, N. (2010). Gender differences in information behavior concerning Wikipedia, an unorthodox information source? _Library & Information Science Research, 32_, 212-220.
*   Maghferat, P. & Stock, W. G. (2010). Gender-specific information search behavior. _Webology, 7_(2).
*   Shirazi, F. (2012). Information and communication technology and women empowerment in Iran. _Telematics and Informatics, 29_(1), 45-55.
*   Thompson, K. M. (2006). Multidisciplinary approaches to information poverty and their implications for information access. Doctoral dissertation. Tallahassee, FL: Florida State University.
*   Urquhart, C. & Yeoman, A. (2010). Information behaviour of women: Theoretical perspectives on gender. _Journal of Documentation, 66_(1), 113 - 139.
*   Warner, D. & Procaccino, J. D. (2004). Toward wellness: Women seeking health information. _Journal of the American Society for Information Science and Technology, 55_(8), 709-730.

</article>