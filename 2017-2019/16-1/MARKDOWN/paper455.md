#### vol. 16 no. 1, March 2011

# Meta-synthesis of research on information seeking behaviour

#### [Christine Urquhart](#author)  
Aberystwyth University, Department of Information Studies, Aberystwyth SY23 3AS, UK

#### Abstract

> **Introduction.** Meta-synthesis methods may help to make more sense of information behaviour research evidence.  
> **Aims and objectives**. The objectives are to: 1) identify and examine the theoretical research strategies commonly used in information behaviour research; 2) discuss meta-synthesis methods that might be appropriate to the type of research studies normally encountered in information behaviour research; and 3) propose some approaches.  
> **Method.** The methods are literature-review based, with reflection and discussion of an existing meta-synthesis of research on women's information behaviour.  
> **Analysis.** Information behaviour reviews are categorised according to prevailing paradigm and these paradigms mapped to those used in reviews of meta-synthesis in social science.  
> **Results.** The range of inquiry paradigms used in information behaviour research is varied, with a strong emphasis on psychological (positivistic) approaches and constructivism. There are many approaches to meta-synthesis and the choice depends on the desired outcomes, as the example meta-synthesis illustrates.  
> **Conclusions.** Meta-synthesis approaches, such as meta-ethnography and critical interpretative synthesis, may be applied to research in information behaviour.

## Introduction

Systematic reviewing and meta-analysis of the quantitative research evidence has extended from the health disciplines into other disciplines. In the social sciences, synthesis is a better descriptor of the process for qualitative research and the term meta-synthesis is used to distinguish this from quantitative meta-analysis. Meta-synthesis may also be used to integrate the findings from quantitative and qualitative studies.

There have been large reviews of the literature on information seeking, but these, like the successive reviews in the _Annual Review of Information Science and Technology_, or the book by Donald Case ([2007](#cas07)) are generally narrative literature reviews, with some elements of mapping reviews (as defined in a typology of reviews by Grant and Booth ([2009](#gra09)). Compared to systematic reviews, such reviews often lack details about the search process and quality assessment.

Unfortunately, systematic reviews, meta-analysis and meta-synthesis require much time and effort to produce a conclusion and the needs of the audience have to be considered. Academic researchers wish to learn about research questions and research strategy. Practising professionals need to learn about common themes and discordances in studies of the information behaviour of particular user groups. Unfortunately, interactions between practitioners and researchers have been limited ([Limberg and Sundin 2006](#lim06) discuss the case of information literacy).

An example of the requirements of policymaker support is provided by the Research Information Network in the UK (for support of research). The Network has funded studies on information use and information activities (information flows) by researchers in different disciplines. The report on the life sciences ([RIN 2009](#res09)) mentions information flows, a knowledge transfer cycle, affordance theory, stresses the heterogeneity of the case studies and the implications of the division of labour in research work. There are messages for various interested parties. Amazingly, the report cites very little previous literature on the use of information by researchers. Theories appear to be plucked from the air, with little awareness, for example, of activity theory, that might have been used as a framework. The apparent need for funding such research seems to be driven, partly, by a lack of suitable meta-syntheses of the existing evidence.

The results of systematic reviews of the clinical evidence may, or may not, provide a definitive answer and for social sciences, where research designs can rarely be as neat as a randomised controlled trial of a drug, clear answers may not be possible. But asking questions, querying assumptions, and comparisons should help dialogue between researchers and practitioners, in all areas of social science. For information behaviour we need to attend to '_making user and audience studies matter_' ([Dervin _et al._ 2006](#der06b) ).

## Aims and objectives

The aim of the paper is to examine and recommend meta-synthesis methodologies that researchers in information behaviour might use. The term methodology is used as the social science research strategies that have been adopted are important in determining how the results of research studies may be synthesised. In the paper, an existing meta-synthesis ([Urquhart and Yeoman 2010](#urq10)) is discussed, to reflect critically on the steps taken and consider how meta-synthesis could be used to answer questions that researchers, practitioners or policymakers might ask.

Accordingly, the objectives of the paper are to: 1) identify and examine the theoretical research strategies commonly used in information behaviour research; 2) discuss meta-synthesis methods that might be appropriate to the type of research studies normally encountered in information behaviour research; and 3) propose some useful meta-synthesis methods for information behaviour research.

## Methods

The first stage was to locate recent overviews of theoretical research strategies used in information behaviour research. This required searches of _Library and Information Science Abastracts_ and _Library, Information Science & Technology Abstracts_, using a combination of terms such as information seeking behaviour, information behaviour, theory, review, philosophical, epistemological, for the years 1999-2009\. The books by Donald Case reviewing information seeking studies (Case 2007) and Rice _et al._ (2001) were also used as examples of overviews and reviews in the _Annual Review of Information Science and Technology_ were also scanned (1999-2008). Google Scholar and Web of Knowledge were used to check the wider literature. Items were included if there was an extensive review or overview of theories of relevance to information behaviour research. One issue of the _Journal of Documentation_ (volume 63, number 1, 2007) is devoted to studies of human information behaviour. Some of these studies reviewed several models or theories. Bates ([2005](#bat05)) reviewed meta-theories, theories and models in library and information science and Spink and Cole ([2006a](#spi06a)) ([2006b](#spi06b)) categorise frameworks in human information behaviour.

The second stage required identification of reviews of meta-synthesis methods in social science. Web of Knowledge and Google Scholar were used for this, along with a check of document alerts on the ASQUS ([Advice and Support in QUalitative evidence Synthesis](https://www.jiscmail.ac.uk/cgi-bin/webadmin?A0=asqus)) mailing list (time period for database searching was 2002 to late 2009, as most of this literature is recent). Documents that reflected on a framework for review, or critiqued one or more meta-synthesis methods were included.

For each element of the search, titles and abstracts were checked for relevance and reference made to the full text document if necessary, to check the scope and depth of any review work indicated in the abstract. This was a difficult judgement to make for many of the information behaviour papers, where the literature review within the document might not be a full and reasoned critique of alternative approaches and the abstract did not help distinguish between a broad or narrow literature review.

For the analysis of reviews of information behaviour, extracted data were tabulated (Table 1) to provide details of the research strategies, the scope of the review and alongside each review, the appropriate inquiry paradigms were entered. For classification of the inquiry paradigm, the familiar Guba and Lincoln ([2008: 257](#gub08)) categories were used. These are positivism, post-positivism, critical theory and related, constructivism and participatory. An additional category was used for psychological theories that are, at present, exploratory ways of thinking, rather than conventional cognitive psychology, which usually operates in a positivist or postpositivist paradigm. Several of the _Annual Review of Information Science and Technology_ reviews did not explicitly state or discuss the inquiry paradigms. One of the reviews of meta-synthesis methods ([Barnett-Page and Thomas 2009](#bar09)) in social science research uses a similar paradigm categorisation, a spectrum of epistemological positions, from na?ve realism, through scientific realism, critical realism, objective idealism and subjective idealism. These have been mapped, approximately, to the Guba and Lincoln categories (Table 1). The participatory paradigm of Guba and Lincoln, not included in the other scheme, seems for information science researchers to be associated with the community of practice principles; that is, action controlled by members of a 'community' is the aim of inquiry. Subjective idealism (not included in Guba and Lincoln) has different connotations, the emphasis for Spencer ([2003])) being the celebration of individual diversity, which is of more interest to researchers than to policymakers and practitioners, who are usually seeking a consensus.

<table width="100%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Comparisons of reviews of information behaviour**</caption>

<tbody>

<tr>

<th>Citation identifier</th>

<th>Scope</th>

<th>Social research strategies</th>

<th>Inquiry paradigm</th>

<th>Justification, other notes</th>

</tr>

<tr>

<td valign="top">Bates ([2005])</td>

<td valign="top">Theories, metatheories and models in library and information science</td>

<td valign="top">Comprehensive list</td>

<td valign="top">Contrasts the nomothetic (scientific) with the idiographic (mainly humanities)</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Bates (2007)</td>

<td valign="top">Browsing (theoretical proposal)</td>

<td valign="top">(mostly commenting on contribution of psychology research)</td>

<td valign="top">Psychological, behavioural ecology</td>

<td valign="top">Defines browsing as a series of glimpses</td>

</tr>

<tr>

<td valign="top">Case et al. (2005)</td>

<td valign="top">Information avoiding versus information seeking</td>

<td valign="top">Mostly refers to cognitive aspects, individual information seeking, psychological constructs</td>

<td valign="top">Mostly post-positivist</td>

<td valign="top">Refers to Johnson's model of information seeking</td>

</tr>

<tr>

<td valign="top">Case (2006)</td>

<td valign="top">Review of information behaviour (by demographic, professional groups</td>

<td valign="top">Not discussed in detail, but lists theories applied</td>

<td valign="top">Cites Bates (2002) on need for meta-theory diversity</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Case (2007)</td>

<td valign="top">Book on information seeking, covers concepts of information, information needs</td>

<td valign="top">Compares models of information behaviour</td>

<td valign="top">Postpositivist and constructivist approaches predominate</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Courtright (2006)</td>

<td valign="top">Examines models of context in information behaviour research</td>

<td valign="top">Compares models of information behaviour. Context as container ? objectivist? Person in context, social actor, embeddedness</td>

<td valign="top">Postpositivist, constructivist</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Dresang (2005)</td>

<td valign="top">Meta-analysis or review of youth information seeking behaviour (digital)</td>

<td valign="top">Radical change</td>

<td valign="top">Constructivist, also learning towards participatory?</td>

<td valign="top">Key ideas: interactivity, connectivity, access</td>

</tr>

<tr>

<td valign="top">Hepworth (2007)</td>

<td valign="top">Critiques contribution of information behaviour studies to people-centred design</td>

<td valign="top">Overview of cognitive approaches (mainly), some connative ideas, notes lack of social, cultural power perspectives</td>

<td valign="top">Critical theory - constructivist in aim</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top"> Hjørland (2000)</td>

<td valign="top">Development of a general theory of information seeking behaviour</td>

<td valign="top">Relates to psychology: cognitive, activity theory, socio-linguistic (for cross disciplinary information seeking</td>

<td valign="top">Realism ? postpositivist</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Johnson (2009)</td>

<td valign="top">Mapping of information behaviour studies</td>

<td valign="top">Dimensions of acquisition-avoidance and accidental-purposive</td>

<td valign="top">Mostly postpositivist in orientation</td>

<td valign="top">Critiques assumed rationality</td>

</tr>

<tr>

<td valign="top">McKechnie (2001)</td>

<td valign="top">Content analysis of the use of the theory for information behaviour in six library and information science journals</td>

<td valign="top">Lists library and information science theories and social sciences theories mentioned</td>

<td valign="top">Various - spectrum of standpoints</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Mutshewa(2007)</td>

<td valign="top">Power perspective on information behaviour</td>

<td valign="top">Overview of information behaviour research trends: cognitive, social, review of sociological theory around power</td>

<td valign="top">Critical theory</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Pettigrew et al. (2001)</td>

<td valign="top">Conceptual frameworks</td>

<td valign="top">Cognitive, social, multifaceted</td>

<td valign="top">Various ? post-positivist, also constructivist</td>

<td valign="top">Categorises sensemaking and cognitive work analysis as general approaches</td>

</tr>

<tr>

<td valign="top">Prabha et al.(2007))</td>

<td valign="top">Satisficing and search stopping</td>

<td valign="top">Role theory; rational choice theory</td>

<td valign="top">Critical constructivist? Post-positivist (satisficing)</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Rice et al. (2001)</td>

<td valign="top">Development of interdisciplinary frameworks for accessing and browsing</td>

<td valign="top">Briefly covers theories associated with information society, mass communications</td>

<td valign="top">Integration of research perspectives: critical theory; constructivist in desired orientation?</td>

<td valign="top">Communication as well as information seeking</td>

</tr>

<tr>

<td valign="top">Sadler and Given (2007)</td>

<td valign="top">Ecological approaches</td>

<td valign="top">Ecological psychology</td>

<td valign="top">Mostly psychological</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Schulz-Jones (2009)</td>

<td valign="top">Literature review of five databases - interdisciplinary</td>

<td valign="top">Social network analysis</td>

<td valign="top">Post-positivism (although social network analysis may be positivist)</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Spink and Cole (2006a)</td>

<td valign="top">Evolutionary psychology ? application to information behaviour</td>

<td valign="top">Considers problem solving approaches, everyday life information seeking and sense-making, information foraging, modular thinking</td>

<td valign="top">Cognitive and evolutionary psychology</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Spink and Cole (2006b)</td>

<td valign="top">Integrated framework for human information behaviour</td>

<td valign="top">Evolutionary and social, Spatial and collaborative, Multi-tasking, nonlinear and digital frameworks</td>

<td valign="top">Stresses evolutionary psychology contribution</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Spink et al. (2008)</td>

<td valign="top">Review of multi-tasking and task switching. Integrated framework for human information behaviour</td>

<td valign="top">Cognitive approach, communication sciences, human-computer interaction, organizational behaviour</td>

<td valign="top">Psychological (cognitive sciences) mainly post-positivist</td>

<td valign="top">Considers time urgency</td>

</tr>

<tr>

<td valign="top">Vakkari (2003)</td>

<td valign="top">Task performance and information searching</td>

<td valign="top">Considers several approaches, presents extended Kuhlthau's information search process model.</td>

<td valign="top">Post-positivism, but with some constructivist elements?</td>

<td valign="top"> </td>

</tr>

<tr>

<td valign="top">Wilson (2006) (2008)</td>

<td valign="top">Relevance and applicability of activity theory</td>

<td valign="top">Activity theory as conceptual framework</td>

<td valign="top">Can vary ? mostly post-positivist</td>

<td valign="top">Suggests activity theory applicable to information literacy</td>

</tr>

</tbody>

</table>

For the overview of reviews of meta-synthesis, Table 2 indicates whether the review covered qualitative, or mixed qualitative-quantitative research synthesis, as well as indicating the scope of the review.

<table width="100%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">**Table 2: Reviews of meta-synthesis methods**</caption>

<tbody>

<tr>

<th>Citation identifier</th>

<th>Qualitative</th>

<th>Qualitative and quantitative</th>

<th>Scope of review</th>

<th>Other notes</th>

</tr>

<tr>

<td valign="top">Barnett-Page and Thomas (2009)</td>

<td valign="top">Yes</td>

<td valign="top">No</td>

<td valign="top">Comprehensive review of existing methods: meta-ethnography, grounded theory, thematic synthesis, textual narrative synthesis, meta-study, meta-narrative, critical interpretive synthesis, ecological triangulation, framework synthesis.</td>

<td valign="top">Realist synthesis not included</td>

</tr>

<tr>

<td valign="top">Boaz and Pawson (2006)</td>

<td valign="top">Yes</td>

<td valign="top">Yes</td>

<td valign="top">Critique of five reviews on mentoring programmes: nominal meta-analysis, phenomenological review, evidence 'nugget', literature review, synthesis</td>

<td valign="top">Suggests more focus on explanation and clearer idea of questions policymakers ask. Subjective idealism accepted.</td>

</tr>

<tr>

<td valign="top">Dixon-Woods et al. (2006)</td>

<td valign="top">Yes</td>

<td valign="top">Yes, more focus on this</td>

<td valign="top">Explanation and justification of critical interpretive synthesis</td>

<td valign="top">Contrasts aggregative with interpretive review, comments on refutational synthesis</td>

</tr>

<tr>

<td valign="top">Mays et al. (2005)</td>

<td valign="top">Considers qualitative to quantitative conversion (and reverse process)</td>

<td valign="top">Yes</td>

<td valign="top">Proposes: narrative approaches (including thematic analysis), meta-ethnography, cross-case analysis, quantitative cross case analysis, content analysis, Bayesian meta-analysis</td>

<td valign="top">Emphasis on illuminating Cochrane-type systematic reviews. Includes table relating approach to intended review aim</td>

</tr>

<tr>

<td valign="top">Pawson (2002) (2004) (2005)</td>

<td valign="top">More important as generative mechanisms for processes to work</td>

<td valign="top">Yes</td>

<td valign="top">Critically reviews evidence-based practice philosophy, developing realist synthesis ? generative approach to causation. Pawson _et al._ 2004 explain and justify mechanism of realist synthesis; provide a template. Pawson _et al._ 2005 discuss realist review</td>

<td valign="top">Develops programme theory of change, do programmes work for X in Y circumstances? Emphasis on policymaking, different viewpoints acknowledged</td>

</tr>

<tr>

<td valign="top">Walsh and Downe (2005)</td>

<td valign="top">Yes</td>

<td valign="top">Yes, to a limited extent</td>

<td valign="top">Overview of existing methods</td>

<td valign="top">Stresses need for good reflexive skills</td>

</tr>

</tbody>

</table>

## Findings: information behaviour: examining the philosophical assumptions

For the information behaviour documents, categorisation of the inquiry paradigms covered in the document was difficult. The following definitions provided by Guba and Lincoln ([2008: 260-261](#gub08)) were helpful in deciding the probable orientations. The closest approximation to the position in the spectrum of Barnett-Page and Thomas ([2009](#bar09)) is indicated. As Spencer _et al._ ([2003: 47](#spe03)) note, the division of quantitative from qualitative paradigms is unhelpful and the different views of knowledge among qualitative researchers are not easy to reconcile.

*   Positivism
    *   Ontology: naive realism; real reality but apprehendible (equates to naive realism)
    *   Nature of knowledge: verified hypotheses established as facts or laws
    *   Knowledge accumulation: accretion; generalizations and cause-effect linkages
    *   Goodness or quality criteria: conventional benchmarks of rigour
*   Post-positivism
    *   Ontology: critical realism, i.e., _real_ reality but only imperfectly and probabilistically apprehensible (probably nearer scientific realism)
    *   Nature of knowledge: non-falsified hypotheses that are probable facts or laws
    *   Knowledge accumulation: accretion; generalizations and cause-effect linkages
    *   Goodness or quality criteria: conventional benchmarks of rigour
*   Critical theory (including action research philosophies)
    *   Ontology: historical realism ? virtual reality shaped by social, political, cultural, economic, ethnic and gender values; crystallised over time (critical realism?)
    *   Nature of knowledge: structural and historical insights
    *   Knowledge accumulation: historical revisionism; generalization by similarity
    *   Goodness or quality criteria: historical situatedness; erosion of ignorance and misapprehensions; action stimuli
*   Subjective idealism: there is no shared reality independent of multiple alternative human constructions (this is based on the ideas of Berkeley and closely related to phenomenalism: reality can be reduced to mind and mental objects.)
*   Constructivism
    *   Ontology: relativism, i.e., local and specific co-constructed realities (objective idealism, world of collectively shared understanding).
    *   Nature of knowledge: individual and collective reconstructions sometimes coalescing around consensus
    *   Knowledge accumulation: more informed and sophisticated reconstructions; vicarious experience
    *   Goodness or quality criteria: trustworthiness and authenticity including catalyst for action
*   Participatory
    *   Ontology: participative reality ? subjective-objective reality, co-created by mind and the given cosmos
    *   Nature of knowledge: extended epistemology, primacy of practical knowing, critical subjectivity, living knowledge
    *   Knowledge accumulation: in communities of inquiry embedded in communities of practice
    *   Goodness or quality criteria: congruence of experiential, presentational, propositional and practical knowing lead to action to transform the world in the service of human flourishing.

Conflating two typologies for social science research strategies is risky. Constructivism has been subdivided into cognitive constructivism, collectivism-social constructivism, and constructionism to help understand applications within information science ([Talja _et al._ 2005](#tal05)), but there are other ways of dividing up constructivism. Different philosophers see critical theory in different ways Delanty ([2005: 87](#del05)) but most stress that social science should transform and emancipate and that ideologies should be avoided. Critical realism comes from a different direction, but it defends the possibility of a causal explanation, accepts that social reality is communicatively constructed, and most proponents stress the emancipatory function of science (Delanty [2005: 146](#del05)) (Wikgren [2005](#wik05)). In fact, many philosophers of social science are now trying to integrate realism and constructivism into a new critical theory of science (Delanty [2005: 148](#del05)), which stresses reflexivity.

Several of the reviews attempted to cover a wide range of information behaviour research studies and, inevitably, a range of inquiry paradigms, often not explicitly stated. The assignment in Table 1 of the relevant inquiry paradigms is tentative and intended to identify the range of paradigms that have been used. This scoping is important as the inquiry paradigms used within information behaviour research will influence the way meta-synthesis might proceed and who might benefit from the conclusions of meta-synthesis.

Table 1 illustrates that interest in psychological contributions to information behaviour research is still strong, although extended from cognitive psychology (traditional positivist and postpositivist) to evolutionary psychology and ecological psychology. Spink and Cole ([2006b](#spi06b)) propose evolutionary and social, spatial and collaborative and multi-tasking frameworks, which may be integrated. The other strong theme in Table 1 is constructivism. A few reviews (those of Mutshewa or Hepworth, for example) suggest that critical theory approaches are rare in information behaviour research and the table confirms this. Some researchers propose participatory approaches, but these seem relatively rare. Table 1 indicates little evidence of subjective idealism. Diversity in meta-theory is necessary, but rarely practised (Dervin and Reinhard [2006](#der06a)) and there is a lack of dialogue (Bates [2005](#bat05); Dervin _et al._ [2006](#der06b)) for reflection on the inherent problems present with any inquiry paradigm. Fuller ([2003: 430](#ful03)) contrasts two strategies for generating philosophically interesting problems of knowledge: 1) generalising from the individual case (adding insight) (the scientific approach), or 2) fully realising the universal or redistributing something already present, such as knowledge or power (perhaps associated more with the constructivist and critical realism approaches). Observing has a price in any strategy as the act of observing distorts the knowledge obtained.

## Lessons from meta-synthesis reviews

One major comprehensive review (Barnett-Page and Thomas [2009](#bar09)) covered meta-synthesis of qualitative research in social science. Some of the reviews that deal with the integration of qualitative and quantitative research data emphasise the importance of the questions asked by policymakers. Realist synthesis (Pawson, e.g. [2004](#paw04)) embraces a programme theory of change, asking questions such as, which interventions work for whom, in what circumstances, but keeps open the idea that there may not be a consensus result. Realist synthesis operates within a realist paradigm that accepts the possibility of causal explanations, but there is an emphasis on explanation.

With meta-analysis of quantitative evidence, it is assumed that, with the same body of evidence and looking at the same outcomes, meta-analysis should come up with the same, or very similar, quantitative conclusions even if slightly different statistical methods are used. This may not hold for qualitative, or quantitative-qualitative meta-syntheses, for several reasons. First, there are different ways of integrating quantitative and qualitative evidence (Dixon-Woods _et al._ [2006](#dix06)) and the method used may affect the results obtained. Secondly, qualitative meta-synthesis is much fuzzier on the benefits of appraising research studies prior to synthesis. Approaches that are based loosely or firmly on grounded theory may prefer a more iterative approach to study selection, similar to theoretical sampling. Studies, that are otherwise _unworthy_ may provide useful evidence in later stages of the synthesis. (Dixon-Woods _et al._ [2006](#dix06)). Thirdly, ambiguous evidence is difficult to handle; the concept of confidence levels in quantitative analysis has to be translated into a different language of risk. Boaz and Pawson ([2006](#boa06)) note that five reviews or syntheses of a contentious issue came to different conclusions.

Barnett-Page and Thomas ([2009](#bar09)) stress that proponents of different methods do not necessarily cite each other, even if there are clear conceptual links. This makes it difficult for novices to meta-synthesis to understand the similarities and differences (a strong justification for their review, which does try to map the linkages). Their message is that philosophical assumptions matter. The idealist may seek and celebrate diversity (for a researcher audience), the realist may accept diversity, but have to come to a working compromise for policymakers.

## Discussion: reflections on a meta-synthesis

The meta-synthesis in information behaviour under discussion was prepared by a doctoral student and myself ([Urquhart and Yeoman 2010](#urq10)). The traditional narrative review was prepared for the thesis, but we were aware that a traditional narrative review might not be able to answer some other questions (less relevant to the doctoral research design itself) about the effect of sex on information seeking behaviour and the influence of feminist research paradigms on research strategies, analysis and interpretation.

Our approach was directed by reading about meta-synthesis methods and meta-ethnography in particular. Meta-ethnography requires three methods of synthesis ([Barnett-Page and Thomas 2009](#bar09)): 1) the translation of concepts from individual studies into one another (reciprocal translational analysis); 2) refutational synthesis, which explores and explains contradictions between individual studies; and 3) lines of argument synthesis, building up a picture of the whole. The approach is similar to grounded theory development. The difficulty was that meta-ethnography requires some homogeneity in the studies and we did not have a neat, small pile of qualitative research on sex and information behaviour. We had a large and untidy pile of research that was partly quantitative, partly qualitative, or a mixture of both, with visible and less visible feminist influences. At the time we did the meta-synthesis (2008) a confusing variety of meta-synthesis approaches seemed to be available (Table 2).

It seemed sensible to us to identify the main feminist research themes first, as an organizing framework. The themes that we identified were 1) sexual asymmetry, power relations (and assumed sexual differences as well as similarities); 2) way of knowing, the knowledge(s); 3) the rejection of the hierarchy in the researcher-subject relationship and questioning of assumptions; and 4) the goal of research as emancipation and social justice. We then tried to translate these to information behaviour research, to develop a framework for categorising our studies, according to the intent of the authors and the main messages of the research. The resulting framework of questions, reflecting the feminist themes above, was

*   Sexual differences (how and why are sex differences in aptitudes and attitudes examined?)
*   Methodological variations (which studies have set out to use different techniques in studying women, which have not? Are some methodologies more appropriate for particular groups of women and if so, why?)
*   Fairness (how is the context of information seeking of women considered and how are power relations examined?)
*   Emancipation (how is learning, or seeking meaning and purpose by women in information seeking considered?)

Reflecting on the process of critical interpretive synthesis, Dixon-Woods _et al._ ([2006](#dix06)) explain that purposive sampling was used to select papers on the topic for meta-synthesis, with later theoretical sampling used to refine and test the emerging analysis. Our approach was also purposive in the sense of having a framework to use to help guide how to allocate papers collected and we worked with a set of documents that had been collected, with some later literature consulted to help with the analysis. However, we felt that we needed to deal directly with the feminist undercurrent and that is why we developed the framework first.

The next step was to categorise the research studies and the studies that did not appear to fit into one or more of the four themes were set aside for later consideration. Most of the studies did fit into one of the four categories and the remainder in fact formed a coherent group on the topic of communicating risks and decision making. The initial framework was expanded. Within each framework theme (now with five themes) the studies within each category theme were compared to identify emerging similarities, but also emerging differences and queries. These were then tabulated to enable further comparison across category themes. These processes are similar in principle to reciprocal translation analysis and refutational analysis, but, like Dixon-Woods _et al._ ([2006](#dix06)), we did not have time to do a full meta-ethnography as such work, at the level of individual studies, could only be attempted for a small number of research studies and we had far too many studies to consider.

For the next stage of synthesis, meta-ethnography proposes a lines-of-argument synthesis. For critical interpretative synthesis (Table 2), Dixon-Woods _et al._ ([2006](#dix06)) suggest that the appropriate way of conceptualising such synthesis is as a synthesising argument. This argument integrates evidence from across the studies in the review into a coherent theoretical framework, comprising a network of constructs and the relationships between them. What we did in our meta-synthesis was to examine the table that was produced, with columns for organizing or categorising theme, emerging similarities, emerging differences and queries. The next stage looked carefully across the columns on emerging similarities and emerging differences. For example, for one of the cells within the column on emerging similarities 'Influences of the social environment on information seeking ? positioning theory', we could identify some mapping themes in the cells of the 'emerging differences and queries' column:

*   Inviting space for personal development required?
*   Expectations of a support network?
*   Does situation make a difference: the settings in which women are more likely to be found?
*   What defines a group of relevance to women?
*   Group characteristics: how is the sphere of influence defined?

It was not a conscious decision to phrase the themes in the emerging differences and queries column as questions but it is probably helpful in such meta-synthesis to be reminded that such reflection is necessary. Our approach may be different to some types of critical interpretative synthesis but it worked for our purposes and we did include some aggregation of quantitative research as well. For synthesis of quantitative and qualitative research findings, for the fifth theme in particular (communicating risks and decision making) we could have converted some of the qualitative to quantitative data (as described by Mays _et al._ [2005](#may05)) (Table 2), but this was beyond the scope of the doctoral research.

Our conclusions were for researchers and the synthesising argument was intended to assist, ultimately, in the discussion of the doctoral research findings. However, the themes that emerged in the final stage of analysis (situation as mesh, intermediary as node with connections and connecting behaviour) may affect the practical design of information services and systems for women making decisions about hormone replacement therapy. For policymakers wishing to fund information services for women making such decisions, a realist synthesis approach could be used (Pawson, Table 2), but we have no way of knowing whether this would produce the same themes that emerged in our analysis.

In the findings of the review on information behaviour research, most research seems to fall into the constructivist or the postpositivist categories. Constructivism and critical realism may be merging, but there could be more honest discussions among information behaviour researchers about the assumptions that have to be made when using particular research strategies (as Fuller [2003](#ful03), suggests). The meta-synthesis we attempted on sex and information behaviour partly addressed these problems, but only vicariously. The framework based on feminist research themes provided a convenient short-cut to exploring some of the value systems present in the literature reviewed. The value systems may be less obvious when synthesising other types of information behaviour research, but we believe that these assumptions may need to be made explicit before trying to integrate findings from different research paradigms.

## Conclusion

Meta-analysis and meta-synthesis methods are developing rapidly and offer opportunities for researchers in information behaviour to provide syntheses of the literature for policymakers and providers of information systems and services. Meta-synthesis can also help move knowledge forward for researchers by a more systematic approach to identification of similarities and differences among research studies, one step towards suggesting different questions to research.

Information behaviour research is conducted under various inquiry paradigms, with perhaps two main groupings under post-positivist or psychological approaches and constructivist approaches. The new developments in psychology research stress the social and evolutionary aspects. The overview (a review of reviews) of information behaviour research concluded that there was and probably will be, considerable diversity in research approaches used in information behaviour research and different views on organizing frameworks are inevitable. The wealth of studies in information behaviour means that there are opportunities for meta-synthesis, although many challenges as well, as those doing the meta-synthesis need to reflect carefully on the standpoints taken by the authors of the research studies.

The meta-synthesis of studies on women's information behaviour suggests that approaches such as critical interpretative synthesis, which can deal with quantitative and qualitative research data, may be useful. For providing syntheses to policymakers and practitioners, realist synthesis methods might be useful in providing guidance, but this requires further trials of the approach.

## Acknowledgements

I am very grateful to Brenda Dervin for her very constructive comments on an earlier version of this pape, to Alison Yeoman for discussions on the doctoral research and to the referees for their helpful comments.

## About the author

Christine Urquhart has directed several studies of information seeking and use in the health sector and also co-directed a longitudinal study of the impact of electronic information services on the information behaviour of students and staff in UK higher and further education. She also prepares systematic reviews for the Cochrane Collaboration, principally the Effective Practice and organization of Care group and is a co-author of reviews on nursing record systems and telemedicine. She was Director of Research in the Department at Aberystwyth for several years and established the training programme for doctoral students. She can be contacted at [cju@aber.ac.uk](mailto:cju@aber.ac.uk)

#### References

*   Barnett-Page, E. & Thomas, J. (2009). [Methods for the synthesis of qualitative research: a critical review.](http://www.webcitation.org/5vJhDz6rR) _BMC Medical Research Methodology Research_, **9**, 59\. Retrieved 28 December, 2010, from http://www.biomedcentral.com/1471-2288/9/59 (Archived by WebCite? at http://www.webcitation.org/5vJhDz6rR)
*   Bates, M. J. (2002). Toward an integrated model of information seeking and searching. _The New Review of Information Behaviour Research_, **3**, 1-16.
*   Bates, M.J. (2005). An introduction to metatheories, theories and models. In K.E. Fisher, S. Erdelez & L. McKechnie (eds.) _Theories of information behavior_, (pp. 1-24). Medford, N.J: Information Today.
*   Bates, M.J. (2007). [What is browsing?really? A model drawing from behavioural science research.](http://www.webcitation.org/5vJhRJciD) _Information Research_, **12**(4), paper 330\. Retrieved 28 December, 2010 from http://InformationR.net/ir/12-4/paper330.html (Archived by WebCite? at http://www.webcitation.org/5vJhRJciD)
*   Boaz, A. & Pawson, R. (2006). The perilous road from evidence to policy: five journeys compared. _Journal of Social Policy_, **34**(2), 175-194.
*   Case, D.O., Andrews, J.E., Johnson, J.D. & Allard, S.L. (2005). Avoiding versus seeking: the relationship of information seeking to avoidance, blunting, coping, dissonance and related concepts. _Journal of the Medical Library Association_, **93**(3), 353-362.
*   Case, D.O. (2006). Information behavior. _Annual Review of Information Science and Technology_, **40**, 293-328.
*   Case, D.O. (2007). _Looking for information: a survey of research on information seeking, needs and behavior_. London: Academic Press/Elsevier.
*   Courtright, C. (2006). Context in information behavior research. _Annual Review of Information Science and Technology_, **41**. 273-306.
*   Delanty, G. (2005). _Social science_. 2nd ed. Maidenhead, UK: Open University Press.
*   Dervin, B. & Reinhard, C.D. (2006). [Researchers and practitioners talk about users and each other. Making user and audience studies matter ? paper 1.](http://www.webcitation.org/5vJhfajdH) _Information Research_ **12**(1), paper 286\. Retrieved 28 December, 2010 from http://InformationR.net/ir/12-1/paper286.html (Archived by WebCite? at http://www.webcitation.org/5vJhfajdH)
*   Dervin, B., Reinhard, C.D. & Shen, F.C. (2006). [Beyond communication: research as communicating. Making user and audience studies matter- paper 2.](http://www.webcitation.org/5vJhpNERP) _Information Research_, **12**(1), paper 287\. Retrieved 28 December, 2010 from http://InformationR.net/ir/12-1/paper287.html (Archived by WebCite? at http://www.webcitation.org/5vJhpNERP)
*   Dixon-Woods, M, Cavers, D., Agarwal, S., Annandale, E., Arthur, A., Harvey, J., Hsu, R., Katbamna, S., Olsen, R., Smith, L., Riley, R. & Sutton, A.J. (2006). [Conducting a critical interpretive synthesis of the literature on access to healthcare by vulnerable groups.](http://www.webcitation.org/5vJhupFnJ) _BMC Medical Research Methodology_, **6**, 35\. Retrieved 28 December, 2010 from http://www.biomedcentral.com/1471-2288/6/35 (Archived by WebCite? at http://www.webcitation.org/5vJhupFnJ)
*   Dresang, E.T. (2005).The information-seeking behavior of youth in the digital environment. _Library Trends_, **54**(2), 154-178.
*   Foster, J. (2005). Collaborative information seeking and retrieval. _Annual Review of Information Science and Technology_, **39**, 329-356.
*   Fuller, S. (2003). The project of social epistemology and the elusive problem of knowledge in contemporary society, In G. Delanty & P. Strydom (Eds.) _Philosophies of social science: the classic and contemporary readings_, (pp.428-435). Maidenhead, UK: Open University Press.
*   Grant, M.J. & Booth, A. (2009). A typology of reviews: an analysis of 14 review types and associated methodologies. _Health Information and Libraries Journal_, **26**(2), 91-108.
*   Guba, E.G. & Lincoln, Y.S. (2008). Paradigmatic controversies, contradictions and emerging confluences. In N.K. Denzin & Y.S. Lincoln, (Eds.) _The landscape of qualitative research_. (pp. 255-286). London: Sage.
*   Hepworth, M. (2007). Knowledge of information behaviour and its relevance to the design of people-centred information products and services. _Journal of Documentation_, **63**(1), 33-56.
*   Hj?rland, B. (2000). Information seeking behaviour: what should a general theory look like? _New Review of Information Behaviour Research_, **1**, 19-33.
*   Johnson, J.D. (2009). An impressionistic mapping of information behavior with special attention to contexts, rationality and ignorance. _Information Processing and Management_, **45**(5), 593-604.
*   Limberg, L., & Sundin, O. (2006). [Teaching information seeking: relating information literacy education to theories of information behaviour](http://www.webcitation.org/5vJiF9x25). _Information Research_, **12**(1), paper 280\. Retrieved 28 December, 2010 from http://InformationR.net/ir/12-1/paper280.html (Archived by WebCite? at http://www.webcitation.org/5vJiF9x25)
*   Mays, N., Pope, C. & Popay, J. (2005). Systematically reviewing qualitative and quantitative evidence to inform management and policy-making in the health field. _Journal of Health Services Research and Policy_, **10**(Supplement 1), 6-20.
*   McKechnie, L., Pettigrew, K.E. & Joyce, S.L. (2001). The origins and contextual use of theory in human information behaviour research. _New Review of Information Behaviour Research_, **2**, 47-63.
*   Mutshewa, A. (2007). A theoretical exploration of information behaviour: a power perspective. _Aslib Proceedings_, **59**(3) 249-263.
*   Pawson, R. (2002). _Evidence-based policy: the promise of 'realist synthesis'_. Evaluation, **8**(3), 340-358.
*   Pawson, R., Greenhalgh, T., Harvey, G. & Walshe, K. (2004). _[Realist synthesis: an introduction](http://www.webcitation.org/5vJiVkJ8A)_. Manchester: University of Manchester, Faculty of Social Sciences. (ESRC Research Methods Programme Paper 2/2004). Retrieved 28 December, 2010 from http://www.ccsr.ac.uk/methods/publications/RMPmethods2.pdf (Archived by WebCite? at http://www.webcitation.org/5vJiVkJ8A)
*   Pawson, R., Greenhalgh, T., Harvey, G. & Walshe, K. (2005). Realist review ? a new method designed for complex policy interventions. _Journal of Health Services Research and Policy_, **10**(Supplement 1) 21-34.
*   Pettigrew, K.E., Fidel, R. & Bruce, H. (2001). Conceptual frameworks in information behavior. _Annual Review of Information Science and Technology_, **35**, 43-78.
*   Prabha, C., Connaway, L.S., Olszewski, L. & Jenkins, L.R. (2007). What is enough? Satisficing information needs. _Journal of Documentation_, **63**(1), 74-89.
*   Research Information Network and British Library. (2009). _[Patterns of information use and exchange: case studies of researchers in the life sciences](http://www.webcitation.org/5vJiwQ4Ka)_. London: Research Information Network. Retrieved 28 December, 2010 from http://www.rin.ac.uk/our-work/using-and-accessing-information-resources/patterns-information-use-and-exchange-case-studie (Archived by WebCite? at http://www.webcitation.org/5vJiwQ4Ka)
*   Rice, R.E., McCreadie, M. & Chang, S-J.L. (2001). _Accessing and browsing: information and communication_. Cambridge, MA: MIT Press.
*   Sadler, E. & Given, L.M. (2007). Affordance theory: a framework for graduate students' information behavior. _Journal of Documentation_, **63**(1), 115-141.
*   Schulz-Jones, B. (2009). Examining information behavior through social networks: an interdisciplinary review. _Journal of Documentation_, **65**(4), 592-631.
*   Spencer, L., Ritchie, J. Lewis, J. & Dillon, L. (2003). _[Quality in qualitative evaluation: a framework for assessing research evidence. A quality framework](http://www.webcitation.org/5vJjHYu75)_. London: Government Chief Social Researcher's Office. Retrieved 28 December, 2009 from http://www.civilservice.gov.uk/Assets/a_quality_framework_tcm6-7314.pdf (Archived by WebCite? at http://www.webcitation.org/5vJjHYu75)
*   Spink, A. & Cole, C. (2006a). Human information behavior: integrating diverse approaches and information use. _Journal of the American Society for Information Science and Technology_, **57**(1) 25-35.
*   Spink, A. & Cole, C. (2006b). Integrations and further research, In A.Spink and C. Cole, (Eds.) _New directions in human information behaviour_, (pp. 231-237). Dordrecht, The Netherlands: Springer.
*   Spink, A., Cole, C. & Waller, M. (2008). Multitasking behavior. _Annual Review of Information Science and Technology_, **42**, 93-118.
*   Talja, S., Tuominen, K. & Savolainen, R. (2005) 'Isms' in information science: constructivism, collectivism and constructionism. _Journal of Documentation_, **61**(1), 79-101.
*   Urquhart, C. & Yeoman, A. (2010). Information behaviour of women: theoretical perspectives on gender. _Journal of Documentation_, **66**(1), 113-139.
*   Vakkari, P. (2003). Task-based information searching. _Annual Review of Information Science and Technology_, **37**, 413-464.
*   Walsh, D. & Downe, S. (2005). Metasynthesis method for qualitative research: a literature review. _Journal of Advanced Nursing_, **50**(2) 204-211.
*   Wikgren, M. (2005). Critical realism as a philosophy and social theory in information science. _Journal of Documentation_, **61**(1), 11-22.
*   Wilson, T.D. (2006). [A re-examination of information seeking behaviour in the context of activity theory.](http://www.webcitation.org/5vJjTIx4L) _Information Research_, **11**(4), paper 260\. Retrieved November 25 2009 from http://InformationR.net/ir/11-4/paper260.html (Archived by WebCite? at http://www.webcitation.org/5vJjTIx4L)
*   Wilson, T.D. (2008). Activity theory and information seeking. _Annual Review of Information Science and Technology_, **42**, 119-161.