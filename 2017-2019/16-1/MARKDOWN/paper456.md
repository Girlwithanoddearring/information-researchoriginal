#### vol. 16 no. 1, March 2011

# Weblog publishing behaviour of librarianship and information science students: a case study.

#### [Jesus Tramullas](#authors)  
Deparment of Librarianship & Information Science, University of Zaragoza, Spain

#### [Piedad Garrido](#authors)  
Deparment of Computer Science & Systems Engineering, University of Zaragoza, Spain

#### Abstract

> **Introduction.** The 'blogosphere' is a space with digital information in which social networks form that offer countless application possibilities. In this technology-mediated context, it is feasible to study the performance and approaches of production, diffusion, relationship and use of information from different perspectives..  
> **Method.** Quantitative data were obtained through the regular examination of the blogs maintained by students and qualitative data were obtained from reports by the students and self-assessment questionnaires.  
> **Analysis.** Simple counts of quantitative data were obtained, without further statistical analysis. The qualitative data were reviewed for insights into the motivations of students.  
> **Results.** Given a free choice, most students adopted the Blogger platform for their blogs. Most blogs consisted of content reported from elsewhere and were not continued by the students following the end of the exercise.  
> **Conclusions.** Students adopted an instrumental approach to the exercise, doing enough to complete the course requirements but not being sufficiently engaged to continue their blogs. Preliminary work based on basic competences is necessary in both collaboration processes and Web 2.0 technology to obtain satisfactory results in the use of Weblogs as teaching and learning tools.

## Introduction

The _blogosphere_ is a space consisting of digital information formed by series of Weblogs (or _blogs_), their contents and the relationships established among them, which give rise to social network models in a digital setting that are based on relationships and participation ([Agarwal and Liu 2008](#aga); [Kumar _et al._ 2004](#kum)). The possibilities offered by such tools to generate and re-use contents with minimum technical requirements have enabled end users to enter the world of the Internet as active participants. The world of information, which is currently developing, is a place where information research and social science research converge and have a social setting with enriched information that offers new possibilities ([Thewall and Wouters 2005](#the)).

The potential and capacity of Weblogs as tools to be used for teaching and learning processes has been stressed time and time again in recent research (e.g., [Minocha 2009](#min); [Churchill 2009](#chu); [Gomes 2008](#gom); [Agostini _et al._ 2009](#ago)). The possibility of converting a student into an active participant within a technology-mediated framework of social process has been viewed as a valuable factor as opposed to other systems such as individual virtual classrooms. Moreover, Weblogs not only have the capacity to complement face-to-face teaching in a blended-learning setting, but also offer relationship and interaction possibilities among the students themselves.

The combination of educational and information management processes is an interesting field in which multiple issues may be studied, ranging from users' performance to information organization structures, and even classification and labelling outlines, etc. And this interest grows if we focus on the use of the tools and services offered through them, that is, real-time informational performance carried out precisely by those who specialise in them in a technology-mediated setting in which innovation opportunities are abundant. The immediate nature of this setting enables methods and techniques to be checked, assessed and corrected almost in real time.

## Methods

The objective of this work is to analyse Weblog usage and creation approaches as an information tool for librarianship and information science students at the University of Zaragoza. The previously-posed hypothesis to be confirmed or ruled out is that such students are capable of developing a Weblog by applying the specialised information management techniques and principles that students have acquired during their academic training.

There are related works covering aspects similar to those discussed in this paper. Beale ([2007](#bea)) examines the use of blogs as learning tools among human-computer interaction students. His paper uses a quantitative approach based on the count of entries in the blogs, the size of the post, the comments received and the use of external links. In addition, it uses a qualitative approach based on a review of post categories. An approach based on the analysis of use patterns combined with the study of reports or interviews has been used by Ducate and Lomicka ([2008](#duc)) among language and literature students. Aharony ([2009a](#aha9)) uses forms that the students complete asking them to consider how they value the use of Web 2.0 technology. Hall and Davison ([2007](#hal)) have conducted research in the specific domain of librarianship and information science, studying the role of blogs in reflective learning. Students must create and maintain a blog, with a required number of entries and comments, for fifteen weeks. With obtained data, researchers studied the interaction among them through content analysis techniques.

As mentioned earlier, there is a growing body of studies on Weblog users' performance. After reviewing the works mentioned above, we decided to apply an approach based on a combination of quantitative and qualitative analysis. The data required for the quantitative analysis were obtained by directly observing the Weblogs designed by students and by the qualitative analysis of the project reports and self-assessment reports handed in. No statistical analysis was carried out, because this research was not to obtain data on networks of blogs. Data sources were as follows:

1.  Blogs were examined every week to get the figures for the number and type of post, categories and tags applied, links and comments.
2.  The personal report and self-ssessment questionnaire were the sources for obtaining qualitative information.

This research is of particular interest because, given their planning, execution and follow-up, it is possible to obtain homogeneous results over a two-year period in accordance with:

1.  the information creation and use project approach being consistent for all students and reflected in the project guide;
2.  the assessment of the technical aspects of publishing, managing and diffusing the information used in each case;
3.  the results obtained from the self-assessment report that each student presents;
4.  the follow-up process carried out for all the Weblogs over a three-year period (2007-2009).

## The student Weblog project

This project was proposed to students by means of a working paper on the project, which included information about the approach, competences, objectives, work materials, schedule, requirements and assessment criteria. The main educational objective was that students should carry out a content management project using a Weblog tool. They were provided with all the support material they needed to learn certain aspects such as the planning, setting up, publishing and maintenance of a digital information product by using a platform for Weblogs. Special emphasis was placed on the technical aspects of its implementation as certain technique requirements had to be compulsorily completed for writing contents, organizing, publishing, diffusing and classifying or tagging. For these purposes, students were offered a list of free Weblog servers so they could assess the different options available and choose that which best matched their own expectations and ways of working.

Students were free to choose the theme and content for their Weblog, with the only condition that it should have nothing to do with libraries, archives or information centres. The intention was twofold: students had to concentrate on a theme of personal interest to them; and to give them the chance to include their Weblog in a digital information management process. The project lasted three months and publishing a weekly post was compulsory. After the minimum set time had passed, students had to write a report about their project and present this in class. To finish the project, they had to present an individual self-assessment of their performance to the teacher in charge the content of which would go towards the mark they obtained for undertaking the project. The data provided in the sections that follow were obtained from reviewing the Weblogs and their contents, from the various reports, and from the individual follow-ups done for each student during academic courses 2007-2008 and 2008-2009.

## The results obtained

First, it is necessary to point out that the number of Weblogs studied derives directly from the number of students who were studying the course subject. Contrary to belief, and despite the fact that the participants were in the final year of the former Diploma in Library Science and Documentation (which is now in the process of being replaced with the Information and Documentation Degree in accordance with European Higher Education Area requirements) the number of students who had previously maintained a blog or who continued a blog with minimum activity after the end of the compulsory period established in the project guide, was practically insignificant. Table 1 summarises the corresponding data.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Number of pre-existing, project and subsequently continued Weblogs.**</caption>

<tbody>

<tr>

<th> </th>

<th>Pre-existing Weblogs</th>

<th>Weblogs projects</th>

<th>Weblogs continued</th>

<th>Weblogs abandoned</th>

<th>Weblogs eliminated</th>

</tr>

<tr>

<td>Academic year 2007-2008</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">22</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">13</td>

<td style="text-align: center;">8</td>

</tr>

<tr>

<td>Academic year 2008-2009</td>

<td style="text-align: center;">2</td>

<td style="text-align: center;">20</td>

<td style="text-align: center;">3</td>

<td style="text-align: center;">11</td>

<td style="text-align: center;">6</td>

</tr>

</tbody>

</table>

The almost insignificant continuance of the Weblogs clashes with the intentions set out in the self-assessment reports as between 40% and 60% of students indicated their intention to maintain their Weblog insofar as they were able to. Regularly published contents in the four Weblogs which continued were not observed, although they tended to offer a mode of one or two monthly posts. It is interesting to note that between 32% and 36% of students had actually taken the trouble to eliminate their Weblog rather than simply abandoning it. The volume and frequency of the posts made in the projects have not been assessed because these values were established according to the frequency set out in the project guide. Consequently, they were regularly distributed in October, November, December and January; that is, the months that correspond to the four-month period that the course subject is taught.

The project was carried out using an implemented Weblog on a free blogs server. The volume of students who used the Blogger platform (60%) is emphasised. In this sense, it has to be stated that Blogger is a widely used platform in Spain and is currently more used than WordPress. Indeed, a large number of libraries and professionals use Blogger as a support for their Weblogs. The review done of the project reports that students presented, in which they had to mention the tools assessed and their reasons for selecting them, allows us to state that the main reasons why this platform was selected by the vast majority of students was because it is simple and easy to use, a factor which had a greater impact than the technical features or functions offered. This finding contradicts the conclusions obtained in other studies. For example Du and Wagner ([2006](#du)) concluded that a blog's success depended largely on the level of technical capabilities offered to manage content and create community. In the case study students have preferred the simplicity rather than technical excellence of the tool.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: The platforms used by students.**</caption>

<tbody>

<tr>

<td> </td>

<th>Academic year 2007-2008</th>

<th>Academic year 2008-2009</th>

</tr>

<tr>

<td>Blogger</td>

<td style="text-align: center;">12</td>

<td style="text-align: center;">13</td>

</tr>

<tr>

<td>Wordpress</td>

<td style="text-align: center;">5</td>

<td style="text-align: center;">4</td>

</tr>

<tr>

<td>Blogspot</td>

<td style="text-align: center;">3</td>

<td style="text-align: center;">2</td>

</tr>

<tr>

<td>Blogia</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">0</td>

</tr>

<tr>

<td>Livejournal</td>

<td style="text-align: center;">0</td>

<td style="text-align: center;">0</td>

</tr>

<tr>

<td>Typedpad</td>

<td style="text-align: center;">1</td>

<td style="text-align: center;">0</td>

</tr>

</tbody>

</table>

We reviewed the content of the posts made in order to detect any information use, acquisition and enrichment patterns. A control template was established for this purpose and to also indicate whether the post made fell in any of the following categories:

1.  Type 1: repetition of contents published by other sources.
2.  Type 2: repetition of contents from other sources enriched by the student?s own contributions, e.g., personal opinions or complementary materials.
3.  Type 3: preparing and writing original material.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Type of post in accordance with its level of preparation.**</caption>

<tbody>

<tr>

<th> </th>

<th>Type 1</th>

<th>Type 2</th>

<th>Type 3</th>

<th>Total</th>

</tr>

<tr>

<td>Academic year 2007-2008</td>

<td style="text-align: center;">227</td>

<td style="text-align: center;">158</td>

<td style="text-align: center;">28</td>

<td style="text-align: center;">413</td>

</tr>

<tr>

<td>Academic year 2008-2009</td>

<td style="text-align: center;">230</td>

<td style="text-align: center;">90</td>

<td style="text-align: center;">22</td>

<td style="text-align: center;">342</td>

</tr>

</tbody>

</table>

The three above-mentioned types may be found in all the Weblogs we analysed. This demonstrates a content preparation approach which seems to satisfy the minimum requirements needed to obtain a pass rather than a demonstration of producing and publishing quality information. Weblogs that show a combination of all three types are in fact repetitions of third-party contents that do not contribute added value.

The content of the posts made were also studied in order to assess the inclusion of various types of information within this content. Posts were classified depending on whether text, images, videos and links to external resources were included. Table 4 presents the results obtained where we can see a remarkable change in the students' object of interest which changed from a motionless image in academic year 2007-2008 to videos in 2008-2009\. Almost all the videos included came from YouTube, while the presence of other sources, Vimeo for instance, was merely symbolic. We found no significant relationship between the posting of original material (Type 3 above) and the inclusion of specific types of information.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: The types of information included.**</caption>

<tbody>

<tr>

<th> </th>

<th>Text only</th>

<th>Text and image</th>

<th>Text and video</th>

<th>Links</th>

<th>Total</th>

</tr>

<tr>

<td>Academic year 2007-2008</td>

<td style="text-align: center;">119</td>

<td style="text-align: center;">204</td>

<td style="text-align: center;">90</td>

<td style="text-align: center;">214</td>

<td style="text-align: center;">4138</td>

</tr>

<tr>

<td>Academic year 2008-2009</td>

<td style="text-align: center;">64</td>

<td style="text-align: center;">110</td>

<td style="text-align: center;">168</td>

<td style="text-align: center;">268</td>

<td style="text-align: center;">342</td>

</tr>

</tbody>

</table>

The comments of the students deserve special attention. Students were not concerned about getting post comments from their peers, nor did they display any attempts to attract such comments. One explanation for this is that the project requirements did not specify making such remarks.

Among the requirements set out in the project guide, is the compulsory categorization and/or tagging of the contents published in the posts made. The study of labelling characteristics is a theme that has been covered in the literature from different perspectives, as shown by ([Aharony 2009b](#aha9b)). In our particular case, we first provided details as to whether students opted for a categories system, a tags system or a mixed approach. Secondly, we established the mean number of categories or tags used. Finally, we reviewed the number of categories and tags applied in each post made. The results are provided in Table 5.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Number of blogs using categories and/or tags.**</caption>

<tbody>

<tr>

<th> </th>

<th>Categories</th>

<th>Tags</th>

<th>Categories and tags</th>

<th>Mean number of categories created</th>

<th>Mean number of tags created</th>

<th>Mean number of categories per post</th>

<th>Mean number of tags per post</th>

</tr>

<tr>

<td>Academic year 2007-2008</td>

<td style="text-align: center;">13</td>

<td style="text-align: center;">5</td>

<td style="text-align: center;">4</td>

<td style="text-align: center;">5.2</td>

<td style="text-align: center;">11.3</td>

<td style="text-align: center;">1.4</td>

<td style="text-align: center;">2.8</td>

</tr>

<tr>

<td>Academic year 2008-2009</td>

<td style="text-align: center;">8</td>

<td style="text-align: center;">6</td>

<td style="text-align: center;">6</td>

<td style="text-align: center;">6.3</td>

<td style="text-align: center;">14.8</td>

<td style="text-align: center;">1.7</td>

<td style="text-align: center;">3.2</td>

</tr>

</tbody>

</table>

The results obtained show that students have used a limited combination of categories and tags. There is a slight difference between academic years which may indicate a tendency to progressively use tags as opposed to categories given the impact of the social tagging systems over the last two years. However, the observation period and the volume of data available do not allow us to conclusively confirm this. Regarding the categories and tags mostly used in the respective Weblogs, the distribution shows a quite regular guideline given the low number of published posts and by the fact that students have sought to cover different aspects of the theme they have chosen for their project, which has led to the avoidance of more specific categories or tags, for more general tags on the subject matter of the Weblog.

Nonetheless, the relationship established with other Weblogs by using a _blog-roll_ (i.e., a list of links to other blogs of interest) shows two different patterns between academic years. That of 2007-2008 is characterised by the fact that 90% of the links to other information resources included in the blog-roll were on the theme the student had selected for his or her Weblog. Likewise, the mean number of links included (7.4) is higher than that obtained by the students of academic year 2008-2009 (5.2). However, the use of links in 2008-2009 reveals fewer links to external resources. We also observe that they were replaced with a circular links networks design in which students created links with each other within their respective blog-rolls, even though the themes were unrelated. Therefore, this led to the formation of blog networks whose premise was personal affinity rather than theme-related. Of the 127 links used by the students of academic year 2008-2009, 53 (41.7%) were actually links among the students of this same academic year. Specifically, four Weblogs only contained links to each other and to the Weblogs of fellow students.

In relation to the previous paragraph, RSS syndication was used to include third-party contents. This was one of the compulsory technical requirements of the project guide, and all the Weblogs included this service. As expected, the group of Weblogs with a blog-roll based on personal affinity, as mentioned in the previous paragraph, also syndicated the content of related Weblogs.

## Conclusions

Schmidt ([2007](#sch)) proposed a framework to analyse the use of the blog format which considered the use of Weblogs as episodes marked by the dimensions of rules, relationships and codes. By applying this framework, it was possible to detect communities of users who shared work approaches, usage approaches and personal expectations. Kerawalla _et al._ ([2009](#ker)) identified six factors affecting students? attitudes and activities in educational Weblogs: audience, community, remarks, style of presentation, technological context and pedagogic context. In line with the factors identified in both proposals, the conclusions are presented below.

In this case, by reference to Schmidt's proposal can not be said to have created a community and was assigned to a specific audience. The content creation guidelines show a high level of reuse. The life cycle of Weblogs match the project life cycle for the subject. This excessive reuse of content and lack of subsequent development of the blogs may be related to the lack of application in other project management skills acquired over information. The students show low level of confidence in writing properly documented information. However, self-evaluation reports reveal that there is a direct relationship between confidence, ability and experience in the use of software tools by the student, and the satisfaction gained by developing the project.

In contrast, the technical aspects have been properly learned and applied by students. The blog has been understood as a necessary tool for personal development and the future career. No problems were detected in the learning and application of performance and technical capabilities of the blogs. However, students hardly used more advanced features than those established as mandatory in the working paper of the project: entries, links to other blogs and tagging. Except in some particular case, have not integrated additional advanced features like geotagging widgets, social networks, link-rolls, social bookmarking, external RSS, statistical information about visitors or surveys.

Students neither consider the blog a tool in terms of prestige and social relationships, nor an information diffusion tool, but a technical requirement that has to be overcome to pass a course subject. Although students are aware of the importance of this instrument for their future and for professional applications, their motivations are far removed from those of the professional community. If the training that students received is of a traditional kind, they are more reluctant to admit innovations and, consequently, the possibility of using and applying social Web services like Weblogs, as Aharony pointed out ([2009a](#aha9)). Should also be taken into consideration that students respond to the user profile of youth between the posts which are in decline, a growing trend as observed by the latest report from Pew Internet on _Social media & Mobile Internet use among teens and young adults_ ([Lenhardt _et al._ 2010](#lenh))

The predominant information management approaches were based on undertaking the essential actions to get through the project.Therefore, students carried out certain practices, which they understood would ensure them the required mark. This can be confirmed in other studies on information use behaviour of university students. (e.g., [Tramullas and S?nchez 2010](#tra)). It was a matter of fulfilling basic assessment requirements, and they did not act as future specialists in information, but as students whose objective was to pass the subject. In this particular case, user friendliness factors, knowledge exchange, reputation or other social aspects ([Hsu and Lin 2008](#hsu)) did not seem to have any influence on either the use approaches or the life cycle of Weblogs. Kim ([2008](#kim)) pointed out how the results of studies into applying Weblogs to educational processes are disperse, and that the same could be said of motivation and users? participation. Sim and Hew ([2010](#sim)) indicated that although compulsory participation may have undesirable effects, it assures students' technical training in the use of the tool. We can confirm both statements. The results obtained enable us to state that complete preliminary work based on basic competences is indeed necessary in both collaboration processes and Web 2.0 technology to obtain satisfactory results in the use of Weblogs as teaching and learning tools.

## About the authors

Jess Tramullas Saz is Professor in the Department of Librarianship and Information Sciences, University of Zaragoza, Spain. He received his PhD in Philosophy and Arts, and Master in Computer Science and Management. He has authored works on the application and development of computer tools for the information sciences, including the Semantic Web, user studies and professional education. His blog is located at [http://tramullas.com](http://tramullas.com) and he can be contacted at [tramullas@unizar.es](mailto:tramullas@unizar.es)

Piedad Garrido Picazo is Assistant in the Deparment of Computer Science and Systems Engineering, University of Zaragoza, Spain. She received her PhD in Documentation from the Carlos III University, Madrid. Currently, she is working in the Polytechnic University School of Teruel, teaching analysis, design and implementation of relational databases. She has authored works about database design, computer applications in computer science education, and the Semantic Web. She can be contacted at [piedad@unizar.es](mailto:piedad@unizar.es)

#### References

*   Aharony, N. (2009). Web 2.0 use by Librarians. _Library and Information Science Research_, **31**(1), 29-37.
*   Aharony, N. (2009a). The influence of LIS students' personality characteristics on their perceptions towards Web 2.0 use. _Journal of Librarianship and Information Science_, **41**(4), 227-242.
*   Aharony, N, (2009b). Librarians and information scientists in the blogosphere: an exploratory analysis. _Library & Information Science Research_, **31**(3), 174-181.
*   Agarwal, N. & Liu, H. (2008). [Blogosphere: research issues, tools, and applications.](http://www.webcitation.org/5vLTYNRpU) _SIGKDD Explorations_, **10**(1), 18-31\. Retrieved 29 December, 2010 from http://www.sigkdd.org/explorations/issues/10-1-2008-07/V10N1-Blogosphere.pdf (Archived by WebCite at http://www.webcitation.org/5vLTYNRpU)
*   Agostini, A., De Michelis, G. & Loregian, M. (2009). Using blogs to support participative learning in university courses. _International Journal of Web Based Communities_, **5**(4), 515-527
*   Beale, R. (2007). [Blogs, reflective practice and student-centered learning](http://www.webcitation.org/5vLU1axFH). In Devina Ramduny-Ellis & Dorothy Rachovides, (Eds.). _Proceedings of the 21st BCS HCI Group Conference HCI 2007_, 3-7 September 2007, Lancaster University, (Volume 2, pp. 3-6). London: British Computer Society. Retrieved 29 December, 2010 fromhttp://www.bcs.org/upload/pdf/ewic_hc07_sppaper1.pdf (Archived by WebCite at http://www.webcitation.org/5vLU1axFH)
*   Chan, C. & Cmor, D. (2009). [Blogging toward information literacy: engaging students and facilitating peer learning.](http://www.webcitation.org/5vLUHUYVw) _Reference Services Review_, **37**(4), 395-407\. Retrieved 29 December, 2010 from http://eprints.hkbu.edu.hk/89/1/Blogging_towards_information_literacy.pdf (Archived by WebCite at http://www.webcitation.org/5vLUHUYVw)
*   Churchill, D. (2009). Educational applications of Web 2.0: using blogs to support teaching and learning. _British Journal of Educational Technology_, **40**(1), 179-183.
*   Du, H.S & Wagner, C. (2006). [Weblog success: exploring the role of technology.](http://www.webcitation.org/5vLUcwoAP) _International Journal of Human-Computer Studies_, **64**(9), 789-798\. Retrieved 29 December, 2010 from http://kuo.bm.nsysu.edu.tw/2009/m964012010/references/Weblog%20success.pdf (Archived by WebCite at http://www.webcitation.org/5vLUcwoAP)
*   Ducate, L & Lomicka, L. (2008). [Adventures in the blogosphere: from blog readers to blog writers.](http://www.webcitation.org/5vLUsAS0E) _Computer Assisted Language Learning_, **21**(1), 9-28\. Retrieved 29 December, 2010 from http://oostburgteam.pbworks.com/f/adventures+in+blogging.pdf (Archived by WebCite at http://www.webcitation.org/5vLUsAS0E)
*   Gomes, M.J. (2008). Blogs: a teaching resource and a pedagogical strategy. In A.J. Mendes, I. Pereira and R. Costa (Eds), _Computers and Education_, (pp. 219-228). London: Springer.
*   Hall, H. & Davison, B. (2007). [Social software as support in hybrid learning environments: the value of the blog as a tool for reflective learning and peer support.](http://www.webcitation.org/5vLVEwmvk) _Library & Information Science Research_, **29**(2), 163-187\. Retrieved 29 December, 2010 from http://www.soc.napier.ac.uk/~hazelh/esis/hall_davison_blogs_draft.pdf (Archived by WebCite at http://www.webcitation.org/5vLVEwmvk)
*   Hsu, C-L & Lin, J. C-C. (2008). [Acceptance of blog usage: the roles of technology acceptance, social influence and knowledge sharing motivation.](http://www.webcitation.org/5vLVc4uTT) _Information & Management_, **45**(1), 65-74\. Retrieved 29 December, 2010 from http://tinyurl.com/2cyblfh (Archived by WebCite at http://www.webcitation.org/5vLVc4uTT)
*   Kerawalla, L., Minocha, S., Kirkup, G. & Conole, G. (2009). An empirically grounded framework to guide blogging in higher education. _Journal of Computer Assisted Learning_, **25**(1), 31-42.
*   Kim, H.N. (2008). [The phenomenon of blogs and theoretical model of blog use in educational context.](http://www.webcitation.org/5vLezqipg) _Computers & Education_, **51**(3), 1342-1352\. Retrieved 29 December, 2010 from http://etec.hawaii.edu/otec/classes/645/sdarticle.pdf (Archived by WebCite? at http://www.webcitation.org/5vLezqipg)
*   Kumar, R., Novak, J., Raghavan, P. & Tomkins, A. (2004), [Structure and evolution of blogspace.](http://www.webcitation.org/5vLfOcNij) _Communications of the ACM_, **47**(12), 35-39\. Retrieved 29 December, 2010 from http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.86.2527&rep=rep1&type=pdf (Archived by WebCite? at http://www.webcitation.org/5vLfOcNij)
*   Lenhardt, A., Purcell, K., Smith, A. & Zickuhr, K. (2010), _[Social media & mobile Internet use among teens and young adults](http://www.webcitation.org/5wK2fxFqL)_. Washington, DC: Pew Internet and American Life Project. Retrieved 28 December, 2010 from http://pewinternet.org/Reports/2010/Social-Media-and-Young-Adults.aspx (Archived by WebCite? at http://www.webcitation.org/5wK2fxFqL)
*   Minocha, S. (2009). [Role of social software tools in education: a literature review.](http://www.webcitation.org/5vLfaVCgO) _Education + Training_, **51**(5/6), 353-369\. Retrieved 28 December, 2010 from http://oro.open.ac.uk/18910/2/Shailey_Education%2BTraining_Lit_review_Edited.pdf (Archived by WebCite? at http://www.webcitation.org/5vLfaVCgO)
*   Sim, J.W.S & Hew, K.F. (2010). The use of Weblogs in higher education settings: a review of empirical research. _Educational Research Review_, **5**(2), 151-163.
*   Schmidt, J. (2007). [Blogging practices: an analytical framework.](http://www.webcitation.org/5vLfr34rN) _Journal of Computer-Mediated Communication_ **12**(4). 1409-1427\. Retrieved 29 December, 2010 from http://jcmc.indiana.edu/vol12/issue4/schmidt.html (Archived by WebCite at http://www.webcitation.org/5vLfr34rN)
*   Thelwall, M. & Wouters, P. (2005). What?s the dealwith the Web/blogs/the next big technology: a key role for information science in e-social science research?. In Fabio Crestani and Ian Ruthven, (Eds.). _Information context: nature, impact & role. Proceedings 5th International Conference on Conceptions of Library and Information Sciences, CoLIS 2005, Glasgow, UK, June 4-8, 2005_, (pp. 187-199). Berlin: Springer. (Lecture Notes in Computer Science, Volume 3507).
*   Tramullas, J & S?nchez Casab?n, A.I. (2010). [Scientific information retrieval behaviour: a case study in students of philosophy](http://www.webcitation.org/5vLgwJ1rX). In 1er Congreso Espa?ol de Recuperaci?n de Informaci?n CERI 2010\. (pp. 251-258). Madrid: Univ. Aut?noma de Madrid. Retrieved 29 December, 2010 from http://eprints.rclis.org/bitstream/10760/14663/1/tramullas%26sanchez_CERI2010.pdf (Archived by WebCite? at http://www.webcitation.org/5vLgwJ1rX)