#### vol. 16 no. 1, March 2011

# Communities of practice in the business and organization studies literature

#### [Enrique Murillo](#author)  
Department of Business Management, Instituto Tecnol?gico Aut?nomo de M?xico, Mexico City, Mexico

#### Abstract

> **Introduction.** As it approaches the two decade milestone, the concept of _community of practice_ faces what can be described as a midlife crisis. It has achieved wide diffusion, but users have adapted it to suit their needs, leading to a proliferation of diverging interpretations. Recent critiques lament that the concept is losing its coherence and analytical power.  
> **Method.** This review uses Benders and van Veen's model of a management fashion to account for the popularity of the concept of communities of practice in the business and organization studies literature and for the current crisis.  
> **Results.** The literature displays considerable confusion between communities of practice and other social structures concerned with knowledge and learning, although recent typologies are helping to clarify concepts. Researchers have accepted the concept as an enduring element in the knowledge-based view of the firm, but practitioners have mostly used it in fashionable management discourse, specifically as a knowledge management tool, resulting in numerous publications based on pragmatic interpretations of the concept. By now, the fashion is fading in the practitioner literature, but the researcher community displays renewed interest in the form of several in-depth critiques and a resurgence of theory-grounded studies.  
> **Conclusions.** The review predicts that the concept will successfully mature out of its current crisis through a new period, already started, of theory development grounded in rigorous studies conducted in organizations.

## Introduction

The term _community of practice_ was originally introduced by Lave and Wenger ([1991](#lav91)) and further developed by Wenger ([1998](#wen98)) and has been described as '_one of the most influential concepts to have emerged within the social sciences during recent years_' ([Hughes _et al._ 2007: 1](#hug07b)). However, as it approaches the two-decade milestone, it faces a _midlife crisis_ in the form of mounting conceptual critiques and a recent downturn in hitherto robust publication trends. As shown in Figure 1, a search for <span style="font-family: monospace;">'community of practice' OR 'communities of practice'</span> in the _EBSCO Business Source Complete_ database revealed, from 2005 onwards, a decrease in publications in the practitioner literature (which in _EBSCO_ includes magazines, newspapers and trade publications, but also book reviews published in academic journals), followed by a more recent decline in academic journals, although it is too early to know if it constitutes a trend (the search printout is available from the author).

<div align="center">![Figure 1: Academic and practitioner publications on communities of practice in EBSCO Business Source Complete](p464fig1.gif)</div>

<div align="center">  
**Figure 1: Academic and practitioner publications on communities of practice in EBSCO Business Source Complete**</div>

Communities of practice have been described as '_groups of people who share a concern, a set of problems, or a passion about a topic and who deepen their knowledge and expertise in this area by interacting on an ongoing basis_' ([Wenger _et al._ 2002](#wms02): 4). Examples might include a group of nurses who discuss patient cases over their daily lunch meeting ([Wenger 1996](#wen96)), petrophysicists involved in deep-sea petroleum exploration at Shell who meet weekly to explore issues and real problems they face in their formal teams ([McDermott and Kendrick 2000](#mck00)), Chief Information Officers from various companies in the San Francisco area who meet monthly for a technical presentation and discussion followed by dinner ([Moran and Weimer 2004](#mor04)); or public defence attorneys sharing a county office who learn from each other how to improve their court performance and thereby develop their professional identities ([Hara and Schwen 2006](#har06)).

The concept has led to far-reaching insights about workplace learning. Researchers found that employees are constantly learning as they go about their daily work and much of this learning bears little relation to, and is often at odds with, formal training and canonical work procedures ([Brown and Duguid 1991](#bro91); [Wenger 1998](#wen98)). Rather, learning is a matter of becoming competent practitioners of informal communities, which, through the shared practices of their members, provide a living repository for knowledge ([Orr 1990](#orr90); [Brown and Gray 1995](#bro95)). Thus communities of practice are not confined to formal apprenticeships, but are natural social structures existing wherever people work and accomplish things together ([Wenger 1998](#wen98)).

Theorists now see communities of practice as an essential component of the knowledge-based view of the firm ([Kogut and Zander 1996](#kog96); [Brown and Duguid 1998](#bro98b); [Cook and Brown 1999](#coo99); [Tsoukas and Vladimirou 2001](#tso01); [Grover and Davenport 2001](#gro01)). For their part, managers increasingly view communities of practice as privileged sites of knowledge-sharing and innovation ([Prokesch 1997](#pro97); [Swan _et al._ 1999](#swa99); [Lesser and Everest 2001](#lese01)). Moreover, after disappointing results from first-generation knowledge management projects, with their heavy emphasis on technological solutions for knowledge-sharing ([Scarbrough 2003](#sca03); [Thompson and Walsham 2004](#tho04); [McDermott 1999](#mcd99)), communities of practice have been recognised as an indispensible element of organizational knowledge management programmes. In a field that is criticised for its increasing fragmentation ([Alvesson and K?rreman 2001](#alv01); [Wilson 2002](#wil02); [Gray and Meister 2003](#gra03)), communities of practice have emerged as one of the few common denominators in existing typologies of knowledge management ([Hansen _et al._ 1999](#han99); [Despres and Chauvel 2000](#des00); [Binney 2001](#bin01); [Earl 2001](#ear01); [Alvesson and K?rreman 2001](#alv01); [Kakabadse _et al._ 2003](#kak03); [Lloria 2008](#llo08)).

Even as the concept reaches the two decade milestone, it still lacks a widely accepted definition. The literature displays considerable confusion, failing to distinguish communities of practice from other social structures concerned with knowledge and learning, such as occupational communities, organizational subcultures, networks of practice and epistemic cultures. Moreover, both academics and practitioners have interpreted and adapted the concept in many different ways, for which the ambiguity of the seminal studies is mostly responsible.

Widespread diffusion, non-agreement on a definition and diversity of adaptations are all symptoms identified by Benders and van Veen ([2001](#ben01)) in their conceptualization of a _management fashion_, which they define as '_the patterns of production and consumption of temporarily intensive management discourse and the organizational changes induced by and associated with this discourse_' ([2001: 40](#ben01)).

These authors critique Abrahamson's ([1996](#abr96)) seminal conceptualization of a fashion for failing to include what they consider a key characteristic, namely, interpretative viability. This refers to a certain degree of ambiguity in a concept, which endows it with greater appeal to a broader set of potential users. A concept that is loosely specified leaves room for a manager to see it as the solution for a vexing problem, or to selectively adopt from the concept those elements which s/he finds most appealing. Such concepts can be operationalized in a number of different ways, can be deployed to achieve different purposes and can simultaneously appeal to different constituencies since each can interpret the concept in their own way.

Benders and van Veen ([2001](#ben01)) also view Abrahamson's conceptualization as simplistic in classifying fashion users as either setters (academics and consultants) or followers (_docile_ managers). Instead they argue that, to a certain extent, all actors develop their own interpretation of the concept, adapt it to create fashionable discourse and use this to promote organizational change.

This review found that Benders and van Veen's model provides a plausible explanation for the rapid diffusion of the community of practice concept and the proliferation of diverging interpretations, which has led to the current crisis. In the historical evolution of the literature, three events are singled out as deserving special attention:

1.  Seminal works on the concept ([Lave and Wenger 1991](#lav91); [Brown and Duguid 1991](#bro91)) failed to establish a clear definition, leaving it '_largely as an intuitive notion_' in need of further development ([Lave and Wenger 1991](#lav91): 42). This development was provided much later by Wenger ([1998](#wen98)), but even then a _closed_ definition was not put forward. From its origins, then, the concept was endowed with a large degree of interpretative viability.
2.  This ambiguity of an undeniably appealing concept enabled academics and practitioners alike to interpret and operationalize communities of practice in many different ways. As a result, there was a rapid growth of publications, but also a proliferation of different interpretations.
3.  The concept satisfied an urgent theoretical need by providing a rationale for the early failures of a previous fashion, specifically KM. Despite widespread agreement that knowledge has become a key source of competitive advantage and superior performance ([Spender and Grant, 1996](#spe96); [Davenport and Prusak, 1998](#dav98); [Hansen _et al._ 1999](#han99); [Von Krogh _et al._ 2001](#von01)), first generation IT-intensive knowledge management projects ended mostly with disappointing results. The cause, theorists convincingly argued, lay in the failure of these projects to consider intra-organizational communities of practice ([Brown and Duguid 1998](#bro98b); [McDermott 1999](#mcd99); [Swan _et al._ 1999](#swa99b); [Wenger 2000a](#wen00a); [Tsoukas and Vladimirou 2001](#tso01)). The concept thus afforded a timely boost to knowledge management, which quickly embraced it as part of its toolkit (e.g., [Hansen _et al._ 1999](#han99); [Binney 2001](#bin01); [Earl 2001](#ear01)).

Since Benders and van Veen's fashion model addresses management discourses and their effects on organizations, this review will limit its purview to business and organizational studies. Nevertheless, the concept has also been very influential in the fields of education, sociology and anthropology. For extensive reviews of those literatures, the reader is referred to Davenport and Hall ([2002](#dav02)) and Koliba and Gajda ([2009](#kol09)).

Beyond this introduction, the review is organized into seven sections. The first presents early studies and the evolution of the concept through the seminal works of Lave, Wenger, Orr, and Brown and Duguid. Section two describes Wenger's ([1998](#wen98)) theoretical framework of communities of practice, starting with his broader social theory of learning, where communities of practice are an embedded element, followed by a focus on communities of practice and their defining dimensions. The third section reviews related social groups and their differences with respect to communities of practice. Section four examines alternative or competing designations that have been proposed for communities of practice. Section five discusses direct challenges and critiques to Wenger's 1998 framework. The major part of the review is contained in section six, which examines theoretically grounded contributions to CoP research published in the last decade. The final section presents detected trends and conclusions.

## Seminal studies and rapid diffusion

The concept of community of practice was originally proposed by Lave and Wenger ([Lave and Wenger 1991](#lav91)). The focus of their research was social learning as a critique of the then dominant cognitive approach. They grounded their theory on five ethnographic studies of traditional apprenticeship institutions: Yucatec midwives in Mexico, Vai and Gola tailors in Liberia, U.S. Navy quartermasters, U.S. supermarket meat cutters and U.S. nondrinking alcoholics. They proposed a theory of learning whereby people learn by becoming acknowledged, but peripheral, members of social communities where knowledge resides, not as abstract ideas, but as embodied and shared practices. They view learning as the process of joining a community and actually taking part in its practices, beginning with the most basic and gradually mastering the most complex, while working alongside established members. In this way, newcomers gradually change their identity to that of an insider. They named this progression from peripheral membership to full insider status _legitimate peripheral participation_ and it is their main intended contribution and the title of their book. They coined the term _communities of practice_, without giving a formal definition, to designate the communities that apprentices joined. Thus, by design, their monograph focuses on the apprentices and the process of legitimate peripheral participation, while paying less attention to the community itself.

The communities of practice described by Lave and Wenger were characterized by legitimate peripheral participation, learning (equated to the construction of a practitioner identity) and a practice. Though sketchy, this model is still currently in use, especially in studies focusing on the inbound trajectories of newcomers into established communities of practice (e.g., [Harris _et al._ 2004](#har04); [Handley _et al._ 2007](#han07); [Campbell 2009](#cam09)).

Another seminal study is Orr's ([1990](#orr90)) ethnography of Xerox photocopier field technicians, which famously revealed the extent to which conventional job descriptions failed to capture the intricacies of practice. The company assumed the _tech. reps._ had an individual job which could be accomplished by simply following the repair procedures specified in the official service manual. In practice, though, Orr discovered the representatives had developed a strong informal community that met daily for breakfast to exchange problem-solving tips. Specifically, they crafted and told each other stories about the machines they fixed. This narration served the dual purpose of holding contextualized actionable knowledge about individual machines and enacting their professional identities as (heroic) representatives. Because they already shared a great deal of common ground ([Bechky 2003](#bec03)), this narration was an effective way of communicating complex tacit knowledge about the machines. In fact, becoming a member of the community was as much about learning to tell good stories as it was about learning mechanical repair skills.

At the time he wrote his ethnography, Orr was not unaware of the community of practice concept ([Duguid 2006](#dug06)); nevertheless, he used van Maanen and Barley's ([1984](#van84)) construct of occupational community to describe the community of technicians and reaffirmed this choice in his later book ([Orr 1996](#orr96)). Contu and Willmott ([2003: 289](#con03)) further note Orr's debt is '_principally to the work of Suchman rather than Lave and Wenger_'. Other researchers nevertheless acknowledge Orr's ground-breaking study as the earliest ethnography of a community of practice ([Raelin 1997](#rae97); [Brown and Duguid 2001](#bro01); [Teigland 2003](#tei03)).

The third seminal study by Brown and Duguid ([1991](#bro91)) articulated the relevance for business organizations of what initially appeared like an esoteric concept developed by anthropologists. These authors were first to argue that, despite their near-invisibility, communities of practice were the key to effective workplace learning and innovation and therefore constituted an important concern for managers, especially in knowledge-based organizations.

They base their theorizing on Orr's ([1990](#orr90)) account and were thus the first to propose a reinterpretation of Orr's thick description in terms of the positive contribution communities of practice make to the organization. Others would later propose more conflictual reinterpretations ([Fox 2000](#fox00); [Contu and Willmott 2003](#con03); [Cox 2005](#cox05); [Contu and Willmott 2006](#con06)). Specifically, Brown and Duguid ([1991](#bro91)) propose three overlapping categories as an explanatory model that fits the representatives' practice: narration, collaboration and social construction. This first model goes well beyond Lave and Wenger's _intuitive notion_ and probably explains why their article is cited by many as _the_ seminal reference.

Narration is the crafting and exchanging of war stories about the repair of specific machines. Telling a story about a faulty machine was the representatives' heuristic for building a causal map that gave a coherent account of the problem. Because the story included and took into account the social and material context in which the machine operated, it provided a much more actionable account than the decision tree prescribed by the official service manual.

Collaboration refers to the fact that the reps spontaneously organized themselves as an informal team in order to collaborate with each other, trading stories and helping each other to make sense of the idiosyncrasies of different machines. This occurred despite the fact that the corporation viewed the job as individual and asocial.

Finally, the category of social construction manifests itself in two dimensions. First, that the reps build through their interactions a shared understanding, in effect a representatives's model of the machines. Second, that by becoming proficient in the telling of stories, the representative simultaneously builds his own identity and contributes to the collectively-held knowledge base of the community of practice.

Brown and Duguid next adapt Lave and Wenger's ([1991](#lav91)) concept to characterize the representatives' actions as learning to function, or developing insider identities, in an organizational community of practice. '_Workplace learning is best understood, then, in terms of communities being formed or joined and personal identities being changed_' ([1991](#lav91): 48). This organizational community is slightly different from the communities described by Lave and Wenger and not just by the fact that it is embedded in a large corporation, whereas the former communities were largely autonomous. Brown and Duguid also prefer the egalitarian community described by Orr ([1990](#orr90): 33), '_the only real status is that of member_'; whereas Lave and Wenger's community of practice displays wide status differentials between masters and apprentices.

A final adaptation is their call for organizations to reconceive themselves as communities of communities of practice and thereby release the innovative potential of these continuously learning groups. They develop a vision (further developed in [Brown and Duguid 1998](#bro98b)) of multiple communities of practice acting in a loosely coordinated fashion, whereas Lave and Wenger's communities were isolated and self-sufficient.

Thus, in developing their conceptualization of an organizational community of practice, Brown and Duguid have used the concept's interpretative viability to perform the necessary adaptations to transplant it into organizations and thereby present the business community with a novel organizational group that held the key to continuous learning, knowledge sharing and innovation. Over the following years, various articles and interviews, mostly by Brown, marketed the concept to various audiences ([Brown and Grey 1995](#bro95); [LaPlante 1996](#lap96); [Brown and Duguid 1996](#bro96); [Stucky and Brown 1996](#stu96); [Brown 1998](#bro98); [Brown and Duguid 2000a](#bro00a)) and fairly soon the business media picked up on the trend.

A final point worth stressing is that at no point in Brown and Duguid's article are communities of practice defined, just as they were not in Lave and Wenger ([1991](#lav91)), nor, of course, in Orr's ([1990](#orr90)) ethnography. The conclusion from this historical overview, using Benders and van Veen's terminology, is that the seminal studies introduced an appealing concept with substantial interpretative viability. In subsequent years, the resulting ambiguity allowed the concept to be used and interpreted in increasingly divergent ways.

The mid- and late-nineties can be characterized as a period of increasing excitement about communities of practice, with enthusiastic accounts appearing in business magazines (e.g., [Brown and Gray 1995](#bro95); [Manville and Foote 1996](#man96); [Roth 1996](#rot96); [Stucky and Brown 1996](#stu96); [Stewart 1996](#ste96); [Prokesch 1997](#pro97); [Stamps 1997](#sta97); [Graham _et al._ 1998](#gra98); [Wright 1999](#wri99); [Stewart 2000](#ste00), [Ward 2000](#war00); [Brown and Duguid 2000a](#bro00a)). This growing interest was partly due to the fact that the concept gives a name to the familiar human need to belong and take part in a group of like-minded peers. Indeed, Wenger ([1999](#wen99)) argues communities of practice are natural social structures, citing prehistoric tribes and medieval guilds as historical examples. Furthermore, the same years witnessed an explosion of interest in knowledge management. Ponzi and Koenig ([2002](#pon02)) report that publications on the subject had very rapid growth after 1996 and hit a peak of almost 600 articles in 1999\. Clearly, some of these studies were written using the new perspective afforded by communities of practice.

In 1998, Wenger published his now famous ethnography of insurance claims processors ([Wenger 1998](#wen98)). After his research with Lave, Wenger shifted his attention from the process of induction of new members to the community of practice itself. He based his new theorizing on ethnographic fieldwork conducted in 1989-1990 in a medical claims processing centre operated by a large US insurance company. The result is a novel framework that constitutes a very substantial development of Lave and Wenger's intuitive notion. The book remains, to this day, the most detailed and comprehensive treatise on communities of practice ([Schwen and Hara 2003](#sch03); [Plaskoff 2003](#pla03); [Meeuwesen and Berends 2007](#mee07); [Zhang and Watts 2008](#zha08)), arguably making Wenger's theory the _de facto_ benchmark ([Lindkvist 2005](#lin05); [Iverson and McPhee 2008](#ive08)). This is also suggested by its becoming the target of a growing number of critiques (e.g., [Fox 2000](#fox00); [Contu and Willmot 2003](#con03); [Marshall and Rollinson 2004](#mar04); [Cox 2005](#cox05); [Roberts 2006](#rob06)).

However, as already mentioned, the book fails to provide an explicit definition of community of practice and the proposed framework is fairly complex and difficult to operationalize. Hence, subsequently, very few studies applied this more developed model (e.g., [Smeds and Alvesalo 2003](#sme03)), choosing instead to interpret and adapt Lave and Wenger's ([1991](#lav91)) intuitive notion or, to a lesser extent, Brown and Duguid's ([1991](#bro91)) model (e.g., [Lee and Cole 2003](#lee03); [Pan and Leidner 2003](#pan03); [Holmqvist 2003](#hol03)). In-depth engagement with Wenger's framework did not come until later (e.g., [Thompson 2005](#tho05); [Goodwin _et al._ 2005](#goo05)).

By the time Wenger's ethnography appeared, a trend was already visible in the literature which would continue after the turn of the century. Studies were roughly aligned along two distinct camps which might be labelled the _organizational studies interpretation_ and the _knowledge management interpretation_. The first group emphasizes theory development by describing emergent, informal organizational communities of practice. The second group emphasizes the business value of communities of practice and aims to identify, support and/or launch _strategic_ communities of practice in order to manage organizational knowledge. The two perspectives are displayed in Table 1 which highlights their contrasting interpretations of various characteristics and capabilities as reflected in representative studies. Thus, as the concept approached its tenth birthday, signs of a management fashion were in evidence, with rising publication trends and interpretative viability leading to increasingly divergent interpretations.

Wenger's ([1998](#wen98)) book is arguably the key reference in the organizational studies perspective, although in later publications he would move toward the knowledge management view. However, Wenger argues that he uses the concept mainly as an entry point into a broader social theory of learning, or as a way to bring together social theory and learning theory ([Wenger 1998](#wen98): 5). The concept of is tightly embedded within this framework which is described in some detail in the following section.

<table width="95%" border="1" cellspacing="0" cellpadding="5" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Contrasting organizational and knowledge management interpretations of communities of practice**</caption>

<tbody>

<tr>

<th>Organizational studies interpretation</th>

<th>Knowledge management interpretation</th>

</tr>

<tr>

<td>These studies interpret communities of practice as emergent, informal, self-organizing groups who set their own learning agenda and operate beyond management control. Therefore, the positions they emphasize are:</td>

<td>These studies interpret communities of practice as hidden resources that should be identified, supported by management and charged with pursuing knowledge initiatives that have strategic value for the organization. Therefore, the positions they emphasize are:</td>

</tr>

<tr>

<td valign="top">**'Communities of practice are informal emergent structures'**  
Orr (1990); Brown and Duguid (1991); Hendry (1996); Wenger (1998); Gherardi and Nicolini (2000)</td>

<td>**'Communities of practice are organizational knowledge assets'**  
Prokesch (1997); Wenger and Snyder (2000); Lesser and Everest (2001); Lesser and Storck (2001); Kimble and Bourdon (2008)</td>

</tr>

<tr>

<td>**'Because communities of practice are informal, they are not under management's control'**  
Brown and Duguid (1991); Wenger (1998); Gongla and Rizzuto (2004); Thompson (2005); Duguid (2006; Pastoors (2007); Raz (2007)</td>

<td valign="top">**'Because communities of practice are knowledge assets, they should be managed'**  
Prokesch (1997); Hanley (1998)); Lesser and Everest (2001); Cross et al. (2006); Probst and Borzillo (2008)</td>

</tr>

<tr>

<td valign="top">**'All competencies of the organization reside in communities of practice'**  
Brown and Duguid (1991); Wenger (1998); Tsoukas and Vladimirou (2001)</td>

<td>**'Core competencies of the organization reside in communities of practice'**  
Brown and Gray (1995); Manville and Foote (1996); Roth (1996); Wenger (1999); McDermott and Kendrick (2000); Barrow (2001); Saint-Onge and Wallace (2003)</td>

</tr>

<tr>

<td valign="top">**'Communities of practice emerge to solve routine problems'**  
Orr (1990); Wenger (1998); Gherardi and Nicolini (2000); Tsoukas and Vladimirou (2001); Hara and Schwen (2006)</td>

<td>**'Communities of practice should focus on strategically important problems'**  
Brown and Gray (1995); Wenger (1999); (2004); McDermott and Kendrick (2000); Barrow (2001); Saint-Onge and Wallace (2003); Anand et al. (2007)</td>

</tr>

<tr>

<td>**'The knowledge of the community of practice is situated and held communally, hence it cannot be extracted'**  
McDermott (1999); Newell et al. (2002); Duguid (2006); Cox (2007b)</td>

<td>**'Knowledge management systems should build on and leverage the natural knowledge-sharing practices of a community of practice**  
Brown (1998), Wright (1999), Brown and Duguid (2000); Bobrow and Whalen (2002)</td>

</tr>

<tr>

<td valign="top">**'Communities of practice emerge of their own accord'**  
Orr (1990); Wenger (1998); Gherardi and Nicolini (2000); (2002); Hara and Schwen (2006)</td>

<td>**'Communities of practice can be designed and launched'**  
McDermott and Kendrick (2000); Barrow (2001); Wenger et al. (2002); Plaskoff (2003); Saint-Onge and Wallace (2003); Thompson (2005); Anand et al. (2007]); McDermott (2000); 2007); Meeuwesen and Berends (2007)</td>

</tr>

<tr>

<td>**'Communities of practice subvert management authority'**  
Orr (1990); Korczynski (2003); Cox (2005); Duguid (2006); Raz (2007)</td>

<td>**'Communities of practice are the heroes of the organization'**  
Brown and Grey (1995); Prokesch (1997); Brown (1998); Stewart (2000); Brown and Duguid (2000a); Barrow (2001)</td>

</tr>

<tr>

<td valign="top">**Communities of practice are (just) an analytical category'**  
Contu and Willmott (2003); (2006); Gherardi (2006)</td>

<td>**'Communities of practice are a new organizational group, the key to managing knowledge and innovation'**  
Brown and Duguid (1991); (2000a); Brown and Grey (1995; Wenger and Snyder (2000); Kimble and Bourdon (2008)</td>

</tr>

<tr>

<td valign="top">**'Communities of practice benefit mostly their own members'**  
Orr (1990); Ibarra (2003); Moran and Weimer (2004)</td>

<td>**'Organizations can harvest the knowledge of communities of practice'**  
Manville and Foote (1996); Prokesch (1997); McDermott and Kendrick (2000); Bobrow and Whalen (2002); Probst and Borzillo (2008); Kimble and Bourdon (2008)</td>

</tr>

</tbody>

</table>

## Wenger's social theory of learning and the role played by the community of practice concept

Wenger ([1998](#wen98)) breaks with cognitive learning theories by proposing to focus on _meaningfulness_, which he views as the ultimate aim of learning. This focus, together with the premise that meanings are negotiated in social communities, implies that the social nature of human beings is an essential enabler of learning. This is not to deny the possibility or the value of individual learning, but to make the important assertion that meanings cannot be determined in isolation.

Wenger defines the negotiation of meaning as 'the process by which we experience the world and our engagement in it as meaningful' ([1998](#wen98): 53). This process is embedded in the practices of communities of practice. Moreover, it is constituted by the interaction of two further processes termed _participation_ and _reification_. Participation is '_the process of being active participants in the **practices** of social communities and constructing **identities** in relation to these communities_' ([1998](#wen98): 4, emphasis in the original). Reification is '_the process of giving form to our experience by producing objects that congeal this experience into "thingness" [in order to] create points of focus around which the negotiation of meaning becomes organized_' ([1998](#wen98): 58).

Human learning is chiefly about the negotiation of new meanings rather than the acquisition of new skills or information ([Wenger 2005](#wen05)). The negotiation of meaning takes place in communities of practice, social groups that organize themselves to pursue enterprises deemed valuable to their members. In so doing, these communities define what it means to be a competent practitioner with respect to their enterprise, be it fixing photocopiers ([Orr 1990](#orr90)), building flutes ([Cook and Yanow 1993](#coo93)), or processing insurance claims ([Wenger 1998](#wen98)). '_Learning is therefore a social becoming, the ongoing negotiation of an identity that we develop in the context of participation (and non-participation) in communities and their practices_' ([Wenger 2005](#wen05): 15).

Thus, although the communities of practice concept has taken centre stage, it plays a subordinate, instrumental role in Wenger's social theory of learning. It enables the theory to focus on meaningfulness by locating learning within a social structure where its meaning can be collectively negotiated. Specifically, the framework brings together four interconnected and mutually defining elements:

> *   Meaning: a way of talking about our (changing) ability -individually and collectively - to experience our life and the world as meaningful.
> *   Practice: a way of talking about the shared historical and social resources, frameworks and perspectives that can sustain mutual engagement in action.
> *   Community [of practice]: a way of talking about the social configurations in which our enterprises are defined as worth pursuing and our participation is recognizable as competence.
> *   Identity: a way of talking about how learning changes who we are and creates personal histories of becoming in the context of our communities.
> 
> ([Wenger 1998](#wen98): 5)

To illustrate the connections between learning and these four elements and to highlight the distinct processes that result in learning, Wenger ([1998](#wen98): 5) proposed a diagram, which is reproduced with slight changes in Figure 2.

<div align="center">![Figure 2: Components of Wenger's social theory of learning](p464fig2.jpg)</div>

<div align="center">  
**Figure 2: Components of Wenger's social theory of learning**  
Source: Adapted from Wenger ([1998: 5](#wen98))</div>

The diagram makes clear that the community of practice is just one element (and not even the core) of Wenger's learning theory, albeit an indispensible one because it analytically ties together all the elements into a coherent framework. As mentioned before, it is the concept that has taken centre stage, probably because it evokes the familiar human experience of participating in a group of like-minded peers, as hinted in the paragraph where Wenger ([1998](#wen98): 45) formally introduces the concept:

> 'Being alive as human beings means that we are constantly engaged in the pursuit of enterprises of all kinds, from ensuring our physical survival to seeking the most lofty pleasures. As we define these enterprises and engage in their pursuit together, we interact with each other and with the world and we tune our relations with each other and with the world accordingly. In other words, we learn. Over time, this collective learning results in practices that reflect both the pursuit of our enterprises and the attendant social relations. These practices are thus the property of a kind of community created over time by the pursuit of a shared enterprise. It makes sense, therefore, to call these kinds of communities _communities of practice_' ([Wenger 1998: 45](#wen98)).

Within this lengthy description, which is not strictly a definition, the three elements of engagement, enterprise and practices deserve a particular emphasis because Wenger uses them to join the concepts of community and practice into a unitary construct. He does this by describing three dimensions of practice as the source of coherence of a community (of practice), i.e., as what makes that particular kind of community cohere. He thus describes them as constitutive or defining dimensions of communities of practice ([Wenger 1998](#wen98)):

*   _Mutual engagement:_ members build the community and the practice by conducting practice-related interactions with each other on a regular basis.
*   a _Joint enterprise:_ members collectively negotiate what their community is all about and hold each other accountable to this understanding.
*   a _Shared repertoire:_ over time, members develop a set of shared resources that allow them to engage more effectively.

The presence of these three dimensions in a group is a necessary and sufficient condition for the existence of a community of practice and they also provide a more straightforward way of operationalizing Wenger's model than the earlier description. Also helpful is a list that Wenger provides of empirical indicators of the existence of a community of practice. These indicators, whose ethnographic origin is readily apparent, can be classified as specific manifestations of the defining dimensions, as shown in Table 2.

<table id="tab2" width="80%" border="1" cellspacing="0" cellpadding="8" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Indicators that a community of practice has formed**  
Source: Adapted from Wenger ([1998](#wen98): 125-126)</caption>

<tbody>

<tr>

<th>Dimension</th>

<th>Indicators of a community of practice</th>

</tr>

<tr>

<td>**Mutual engagement**</td>

<td>1) Sustained mutual relationships - harmonious or conflictual.  
2) Shared ways of engaging in doing things together.  
3) The rapid flow of information and propagation of innovation.  
4) Absence of introductory preambles, as if conversations and interactions were merely the continuation of an ongoing process.  
5) Very quick setup of a problem to be discussed.</td>

</tr>

<tr>

<td>**Joint  
enterprise**</td>

<td>6) Substantial overlap in participants' descriptions of who belongs.  
7) Knowing what others know, what they can do and how they can contribute to an enterprise.  
8) Mutually defining identities.  
9) The ability to assess the appropriateness of actions and products.</td>

</tr>

<tr>

<td>**Shared repertoire**</td>

<td>10) Specific tools, representations and other artefacts.  
11) Local lore, shared stories, inside jokes, knowing laughter.  
12) Jargon and shortcuts to communication as well as the ease of producing new ones.  
13) Certain styles recognised as displaying membership.  
14) A shared discourse reflecting a certain perspective on the world.</td>

</tr>

</tbody>

</table>

It should be noted that the operationalization of Wenger's model requires just the three defining dimensions mentioned above and not the more fundamental processes of negotiation of meaning, participation and reification. These concepts, although central to Wenger's social theory of learning, are not especially helpful in defining communities of practice or to describe their empirical attributes.

Wenger defines _participation_ as the social experience of living in the world in terms of membership in social communities and active involvement in social enterprises. Moreover, he explicitly qualifies participation as broader than mutual engagement ([1998](#wen98): 55). Hence, not all participation involves communities of practice; it may involve various types of social structures and do so without engagement in practice.

Similarly, Wenger ([1998](#wen98): 58) defines _reification_ as the process of giving form to our experience by producing objects that congeal this experience into '_thingness_' in order to create points of focus around which the negotiation of meaning becomes organized. So defined, the process of reification becomes a basic building block of practically _any human discussion_ and certainly not limited to interactions taking place within an established community of practice. Even something as transient as a conversation on an aeroplane would very likely make use of reification. Reification is a constitutive process in the development and use of a shared repertoire, but again, it is broader and more basic than this Wenger concept

In sum, communities of practice display processes of participation and reification, but so do other types of groups and social structures, which is why these processes are not useful for the purpose of empirically identifying communities of practice.

Wenger's ([1998](#wen98)) framework is a very substantial theoretical development of Lave and Wenger's ([1991](#lav91)) '_intuitive notion_', grounded on an organizational ethnography of a single community of practice. Even though a _closed_ definition of community of practice is not proposed, the detailed framework could have set bounds to the interpretative viability of the concept if it had been promptly adopted in relevant studies. Still, it is not an easy model to operationalize, which probably explains the paucity of studies that use this model instead of the more adaptable notion of Lave and Wenger ([1991](#lav91)) or Brown and Duguid ([1991](#bro91)).

In a later theoretical essay, Wenger ([2000b](#wen00b)) argues that organizations should design themselves as social learning systems, constituted by communities of practice, boundary processes between them and the identities participants develop as they participate in these systems. The paper constitutes a fairly compact summary of the 1998 book, but again fails to provide an explicit definition of community of practice.

Other writings by Wenger are aimed at practitioners ([1999](#wen99); [2000a](#wen00a); [2004](#wen04)). Most particularly, his book co-authored with McDermott and Snyder ([Wenger _et al._ 2002](#wms02)) is a practical guide for organizations wishing to launch and nurture communities of practice. Several authors have critiqued Wenger for this popularization of a highly complex concept and for his shift from what is interpreted as an emancipatory discourse in Lave and Wenger ([1991](#lav91)), to a managerialist discourse in his practitioner writings ([Fox 2000](#fox00); [Contu and Willmott 2003](#con03); [Cox 2005](#cox05); [Hughes 2007](#hug07a)).

This review considers Wenger's ([1998](#wen98)) framework as a critical and lasting contribution to the literature and can only deplore the lack of subsequent empirical studies by the author, which surely would have contributed to a more focused and nuanced understanding of this complex notion. As things turned out, the reviewer feels compelled to trace some of the conceptual confusions in the literature to the introduction of a simplified model by Wenger _et al._ ([2002](#wms02)), now consisting of Community, Domain and Practice, as well as some liberties taken with respect to the 1998 framework, such as the possibility of '_distributed communities of practice_' counting thousands of members (in effect abandoning the 1998 criterion of direct mutual engagement). This relaxation of the carefully balanced 1998 framework reinforced interpretative viability, contributed to the proliferation of studies built on shallow theoretical foundations and blurred the differences between communities of practice and other social phenomena concerned with knowledge or learning. The resulting confusions are examined in the following section.

## Conceptual confusion from similar phenomena

Some confusions in the literature can be traced to the similarity of the concept of community of practice to other social structures that are related to knowledge and learning; the most prominent are listed in Table 3\. The first is Constant's ([1980](#con80)) concept of communities of technological practitioners, which Brown and Duguid ([2001](#bro01): 210) consider an earlier and independent derivation of the community of practice concept. However, Constant's communities are actually closer to Brown and Duguid's own concept of a _network of practice_ (defined simply by a shared practice), than to Wenger's concept (which is defined by direct engagement between participants). Constant's aim is to build a Kuhnian model to explain a technological revolution, specifically the advent of turbojets. Yet his communities are defined as people sharing a narrow technical specialty, rather than people sustaining direct engagement.

<table width="70%" border="1" cellspacing="0" cellpadding="8" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Social structures that bear some resemblance to communities of practice**</caption>

<tbody>

<tr>

<td>**Community of technological practitioners**</td>

<td>'Utilization of a community of practitioners as a primary unit of historical analysis nevertheless does promise to generate basic insights for the history of technology. A community of technological practitioners, moreover, may be analyzed in turn as an aggregation of individuals or of firms, just as a scientific specialty may be analyzed as an aggregation of individuals or of labs and departments' ([Constant 1980: 9](#con80)).</td>

</tr>

<tr>

<td>**Occupational  
community**</td>

<td>'[A] group of people who consider themselves to be engaged in the same sort of work; who identify (more or less positively) with their work; who share a set of values, norms and perspectives that apply to, but extend beyond work related matters; and whose social relationships meld the realms of work and leisure' ([van Maanen and Barley 1984](#van84): 295).</td>

</tr>

<tr>

<td>**Occupational  
subculture**</td>

<td>Occupational subcultures comprise unique clusters of ideologies, beliefs, cultural forms and practices that arise from shared educational, personal and work experiences of individuals who pursue the same profession within the overarching organizational culture of a single workplace ([Trice 1993](#tri93)).</td>

</tr>

<tr>

<td>**Epistemic  
culture**</td>

<td>'[T]hose amalgams of arrangements and mechanisms ? bonded through affinity, necessity and historical coincidence ? which, in a given field, make up how we know what we know' ([Knorr Cetina 1999](#kno99): 1, italics in original).</td>

</tr>

<tr>

<td>**Network of  
practice**</td>

<td>'Networks of practice are made up of people that engage in the same or very similar practice, but unlike in a community of practice, these people don't necessarily work together [yet] such a network shares a great deal of common practice. Consequently, its members share a great deal of insight and implicit understanding. And in these conditions, new ideas can circulate. These do not circulate as in a community of practice, through collaborative, coordinated practice and direct communication. Instead, they circulate on the back of similar practice (people doing similar things but independently) and indirect communications (professional newsletters, listservs, journals and conferences, for example)' ([Brown and Duguid 2000b](#bro00b): 28).</td>

</tr>

</tbody>

</table>

Next is van Maanen and Barley's ([1984](#van84)) notion of occupational community, whose definition is shown in Table 3\. Crucially absent from this definition is any mention of mutual engagement, which for Wenger ([1998](#wen98)) is the only necessary condition for the existence of a CoP, irrespective of members' occupations. Moreover, since communities of practice can have interdisciplinary membership (e.g., [Goodwin _et al._ 2005](#goo05)), they cannot be considered local portions of a broader occupational community.

As mentioned earlier, the concept of occupational community is famously used in Orr's ([1990](#orr90)) ethnography of tech reps, even though they formed a local group who regularly engaged in sharing stories, i.e., a CoP, while simultaneously belonging, as Brown and Duguid ([2001](#bro01): 206) point out, to a much larger occupational community.

Trice's ([1993](#tri93)) concept of occupational subculture is another that superficially bears some resemblance to the concept of community of practice. However, Trice's concept, like Constant's and van Maanen and Barley's, does not require or guarantee that members of these groups actually engage with each other. Therefore, though they may share a profession, they cannot cohere into a community without regular engagement.

Brown and Duguid's ([2000b](#bro00b); [2000c](#bro00c)) concept of network of practice has attracted much attention, possibly because it provides a theoretically legitimate way of talking about Internet-based structures that could be easily taken for virtual communities of practice ([Wasko and Faraj 2000](#was00); [Wasko and Teigland 2004](#was04); [Wasko _et al._ 2009](#was09)). However, this is not the original intent of the concept, as the authors themselves explain. They acknowledge their concept is close to van Maanen and Barley's ([1984](#van84)) occupational community, using at one time the metaphor of a 'virtual guild' ([Brown and Duguid 2000b](#bro00b): 29), but they wish to re-direct attention from the 'community' aspect of such groups to the shared practice. They see networks of practice as extended epistemic networks where practice provides a common substrate which makes them capable of effectively sharing a great deal of knowledge, even if most of their members '_will never know, know of, or come across one another_' ([Brown and Duguid 2001](#bro01): 205). Thus, a key strength of the concept is spatial extension. Two haematologists who have never met would be part of the same network of practice because of the highly specialized practice they both belong to ([Brown and Duguid 2000b](#bro00b)). Indeed, Duguid ([2005](#dug05): 113) defines a network of practice as '_the collective of all practitioners of a particular practice_'. This is distinctly different from a community of practice, where the membership criterion is direct and sustained engagement.

However, by definition, networks of practice contain embedded communities of practice: high density sections of the network formed by practitioners who actually engage with each other regularly and thus develop much stronger ties than those prevalent over the network ([Brown and Duguid 2000c](#bro00c)). This suggests that a viable search strategy for communities of practice is to examine the social network structure of a known network of practice for areas of high-density. Fleming and Marx ([2006](#fle06)) have tried this by examining the social networks of co-authored patent-holders in the US for the 25-year period starting in 1975\. This has allowed them to actually sketch existing networks of practice and their embedded communities of practice and to link them to increasing innovation, particularly in Silicon Valley and Boston.

Lastly there is Knorr Cetina's ([1999](#kno99)) concept of epistemic culture, which Brown and Duguid ([2001: 205](#bro01)) assess as equivalent to a network of practice by pointing out it makes no distinction between groups of scientists working closely together on a regular basis and same-discipline scientists who rarely meet or know each other. Hence, a local portion of an epistemic culture might qualify as a community of practice, but not the complete culture. As before, the crucial distinction is that of direct engagement, while both network of practice and epistemic culture require only a common practice, however specialized.

The concepts reviewed in this section have caused some confusion in the literature, but in the end they designate different realities (see [Cox 2008](#cox08) for a useful critique). Greater confusion has been caused by alternative or competing designations for communities of practice, which are reviewed next.

## Conceptual confusion from competing designations

Another manifestation of interpretative viability can be discerned in the appearance of a number of alternative designations for communities of practice. Some authors have introduced new names for various learning groups in organizations, which upon close examination appear to differ very little from the earlier concept of CoP, thereby contributing to the conceptual confusion in the literature.

Boland and Tenkasi ([1995](#bol95)) acknowledge borrowing Lave and Wenger's ([1991](#lav91)) concept of community of practice, to which they added their own nuances. They propose the term community of knowing to describe communities of specialised or expert knowledge workers in knowledge-intensive firms. Arguably, they could have referred to a '_community of practice of experts_' to avoid the introduction of a new designation.

Other authors have differentiated themselves from the Wenger framework by making absence of management direction or support a necessary condition for a true community of practice and proposing new designations for groups that display all the properties of communities of practice, yet develop with management's blessing and support.

For instance, B?chel and Raub ([2002](#buc02)) introduced the concept of knowledge networks which they argue extends beyond the traditional concept of community of practice. The authors propose four variants of this concept: '_hobby_' networks, '_professional learning_' network, '_business opportunity_' network and '_best practices_' network. They contend only the first two conform to the traditional concept of community of practice ([2002](#buc02): 589), but it is the last two that can bring about organizational benefits. Their position is that once management support is received the community ceases to be '_informal_' and '_voluntary_' and therefore ceases to be a community of practice.

Similarly, Barret _et al._ ([2004](#bar04): 1) establish a distinction between communities '_which are... voluntary in terms of participation and those with a more managed membership_', with only the former being considered communities of practice. They use the umbrella term '_knowledge communities'_ to cover voluntary and managed communities and they also include constellations of communities of practice ([Wenger 1998](#wen98)).

Stork and Hill ([2000](#sto00)) also present informality as a necessary condition of a community of practice. They describe a community of information technology managers at Xerox, which began with an initial roundtable two-day meeting, decided to meet again in two months and thereafter met every six weeks. They took the name Transition Alliance and agreed their domain was to orchestrate a major transition from Xerox's proprietary technology to more open industry standards. Senior management fostered the launch of the Alliance and was supportive of it, but did not try to control it or request deliverables. For instance, attendance to meetings was not mandatory. Still, Stork and Hill argue that because the group was deliberately established by senior management, it did not qualify as a community of practice ([2000: 65](#sto00)), but as a new organizational group they labelled a '_strategic community_'. However, the large amount of freedom this group enjoyed from the start, the fact that members all shared the same practice and were all stakeholders in the technological transition and the crucial fact that they interacted regularly both in and between meetings would suggest that, from Wenger's perspective, the group did evolve into a true community of practice. (In fact, both Wenger and Thomas Davenport wrote letters to the Editor to suggest this.)

These various authors' insistence on informality as a defining condition of a true community of practice is contradicted by Wenger's ([1998](#wen98)) study of claims processors, which showed mutual engagement and not informality, to be the essential condition. Any workgroup engaged in a specific domain of knowledge will over time evolve into a community of practice; that is, it will develop an indigenous practice that allows it to get the job done, even if the workgroup is formally established by management, as was the claims processors unit, which boasted a supervisor and an assistant supervisor ([1998](#wen98): 75). Still, because a community of practice defines itself through engagement, its boundaries will not necessarily match institutional boundaries, because membership is not defined by institutional categories. It is in this sense that Wenger describes communities of practice as essentially informal, but he explicitly rejects the view that communities of practice can never have a formal status.

Further evidence against informality as a defining condition is provided by several recent studies of strategically-important communities of practice supported or even intentionally launched by management ([Swan _et al._ 2002](#swa02); [Thompson 2005](#tho05); [Anand _et al._ 2007](#ana07)). These studies, which are reviewed in below, support Wenger's position that communities of practice can assume '_knowledge stewarding_' responsibilities in organizations if management is socially sensitive and is careful not to stifle their self-organizing drive ([Wenger 1999](#wen99); [2000a](#wen00a)). Intra-organizational communities of practice are ubiquitous, a consequence of engagement being their root cause ([Wenger 1998](#wen98)). The problem is that not all communities of practice are equally relevant to managers; most are only important to members, helping them to cope with a particular class of problems at work. Much more exceptional are communities of practice with the potential to have a strategic impact on the business, whose main interest is aligned with managers' priorities and who are actually recognised and supported. The knowledge management agenda of detecting or launching such communities of practice has resulted in a large number of publications displaying minimal theoretical support.

A final example to close this section is Korczynski's ([2003](#kor03)) concept of communities of coping. These are informal groups that emerge among service workers to support each other in dealing with the stress and discomfort caused by having to deal with irate customers, for instance in call centres. In choosing this label, the author acknowledges adopting Brown and Duguid's ([1991](#bro91)) language, but still considers these groups as a different structural form without clearly articulating the difference. In a recent study, Raz ([2007](#raz07)) uses participant observation and interviews to examine work in three Israeli call centres where employees not only support each other but teach new members how to _work the system_. This study indistinctly uses both labels, communities of practice and communities of coping. It finds that the key driver for the emergence of the community is to help its members deal with the contradictions of their work, such as the tension between time spent on each call and quality of customer service. However, Wenger's ([1998](#wen98)) ethnography of claims processors gives ample space to describing how that co-located community of practice helped employees to cope with management demands, including dealing with irate customer calls ([1998](#wen98): 24). There thus seems to be little reason for creating a new label to highlight this previously identified aspect of the enterprise of a community of practice.

## Challenges to Wenger's (1998) framework

Some authors argue that when Lave and Wenger ([1991](#lav91)) refer to a community of practice they use this designation just as an analytic viewpoint or a conceptual lens for examining social learning processes, but it was not their intention to name a stable group ([Contu and Willmott 2003](#con03); [Cox 2005](#cox05)). However, taking at face value Lave and Wenger's ethnographic studies, as well as Orr's ([1990](#orr90)) minutely detailed ethnography, it seems implausible to argue the concept does not identify a particular type of social structure. The potential difficulties in clearly establishing their membership, the relations between them, or the exact contents of their practice should not be exacerbated into denying their reality as stable social structures with identifiable characteristics that members are aware of belonging to.

Gherardi, Nicolini and Odella ([1998](#ghe98)) similarly argue from a constructivist perspective that there is a danger of reifying communities of practice. They reject the view of a community of practice as a community with defined boundaries, established behavioural rules and canons. They argue that communities of practice are just one of the forms of organizing, specifically, organizing for the execution and perpetuation of a practice.

> 'In other words, referring to a community of practice is not a way to postulate the existence of a new informal grouping or social system within the organization, but is a way to emphasise that every practice is dependent on social processes through which it is sustained and perpetuated and that learning takes place through the engagement in that practice' ([1998: 279](#ghe98)).

By declining to reify the community, Gherardi _et al._ seem to reify the practice instead, as if the practice had a life of its own, independent from this or that specific group of practitioners. Cook and Yanow ([1993: 378](#coo93)) provide a good counterargument by noting how the Concertgebouw Orchestra and the New York Philharmonic perform the same Mahler symphony differently (as any Mahler fan knows, this is the case even under the same conductor).

Wenger's ([2002: 2340](#wen02)) position is that a community of practice is an analytical category but also a real social structure:

> 'Yet, you can go into the world and actually see communities of practice at work. Moreover, these communities are not beyond the awareness of those who belong to them, even though participants may not use this language to describe their experience. Members can usually discuss what their communities of practice are about, who else belongs and what competence is required to qualify as a member.'

Another frequent critique concerns Wenger's ([1998](#wen98)) treatment of power issues within communities of practice and what is regarded as a shift from an _emancipatory_ discourse in his seminal work with Lave, to a managerialist discourse of _performance_ in his later publications.

Specifically, Fox ([2000](#fox00)) critiques Wenger ([1998](#wen98)) for insufficiently addressing unequal relations of power and for explaining power only as an aspect of identity formation and not as an aspect of practice _per se_. Marshall and Rollins ([2004](#mar04)) also underscore the importance of power and politics in the process of negotiating meaning. They critique Wenger ([1998](#wen98)) for mentioning without further elaboration the potential struggles for the appropriation and fixing of meaning within communities of practice. Cox ([2005](#cox05)) argues that mundane workplaces, such as those chronicled by Wenger ([1998](#wen98)), trigger alienation and suggests communities of practice are informal groups of employees with an agenda of active opposition to management's control. However, this is a somewhat strained extrapolation of Wenger's observation that by inventing a practice, the claims processors community of practice aimed to get the job done in a manner satisfying to themselves.

For their part, Contu and Willmott ([2000: 271](#con00)) critique the latter Wenger for his support for a managerialist agenda:

> The account of learning presented in _Communities of Practice and Social Learning Systems_ [[Wenger 2000b](#wen00b)] can be interpreted as a shift from earlier participation in an analytic community engaged in practices that aspire to enhance mutual understanding for purposes of emancipation ([Lave and Wenger 1991](#lav91)) to participation in a community that is primarily preoccupied with improving prediction and control for purposes of improving performance.

In point of fact, Wenger argues communities of practice cannot be _managed_ in the usual sense of the word; they can be manipulated or coerced into submission, but _managing_ the practice of a community of practice, in the narrow sense of exercising control over it, is not possible:

> '[T]he power - benevolent or malevolent - that institutions, prescriptions or individuals have over the practice of a community is always mediated by the community's production of its practice. External forces have no direct power over this production, because in the last analysis (i.e., in the doing through mutual engagement in practice), it is the community that negotiates its enterprise' ([Wenger 1998: 80](#wen98)).

Indeed, because a community of practice is essentially an informal group, it always has the option of removing itself from management's control if it feels its enterprise is threatened. Studies by Gongla and Rizzuto ([2004](#gon04)) and Pastoors ([2007](#pas07)), reviewed in the next section, give evidence of employees joining bootlegged or underground communities of practice to evade management and freely pursue their own interests. Nevertheless, Wenger also qualifies that arguing communities of practice produce their own practices is not to assert that they are an emancipatory force ([1998: 85](#wen98)).

Wenger's treatment of power issues is, in fact, consistent with his broader theoretical framework. He sees communities of practice as wielding power because they socially define competence and identities ([Wenger 2000b](#wen00b)). Thus, a person may decide whether he or she wants to belong to a particular community (i.e. learn its practice), but has a limited capacity as an outsider (or even as full member) to change the practice of the community. On the other hand, a community of practice is powerless before an individual who does not recognize its authority and is not interested in joining.

It ought to be noted that this competence-defining role of communities of practice is also the source of their greatest weakness: the danger of becoming insular ([Wenger 2000b](#wen00b)) and losing touch with the broader organization and market environment ([Thompson 2005](#tho05)).

Wenger's ([1998](#wen98)) framework thus seems to give coherent replies to the principal critiques that have been levelled at it. Moreover, the framework seems to be enjoying a renaissance, as a growing number of studies are attempting to operationalize it. In the meanwhile, other researchers have gradually made specific contributions to theory, either by confirming insights already mentioned in Wenger ([1998](#wen98)), or by extending that framework in particular respects. These contributions are reviewed in the next section.

## Recent theoretical contributions to the literature

This section reviews a selection of theoretically-grounded studies, published during the second decade of community of practice literature, that have made significant contributions to our understanding of communities of practice. They are broadly grouped into six areas of current interest: launching communities of practice, managing and controlling communities of practice, boundaries and innovation, identity construction, virtual communities of practice and the concept of community of practice. An overview of these contributions is provided in Table 4.

<table width="70%" border="1" cellspacing="0" cellpadding="8" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: An overview of recent contributions to the theory of communities of practice**</caption>

<tbody>

<tr>

<td>**Launching communities of practice**</td>

<td>Swan _et al._ ([2002](#swa02)): communities of practice used as a rhetoric device to promote change and innovation.  
Thompson ([2005](#tho05)): distinguishes between structural components, which organizations can furnish and epistemic behaviours, which depend on members alone.  
Anand _et al._ ([2007](#ana07)): identify success factors for launching new _practice areas_ in consulting firms.</td>

</tr>

<tr>

<td>**Managing or controlling communities of practice**</td>

<td>Gongla and Rizzuto ([2004](#gon04)); Pastoors ([2007](#pas07)): communities of practice can and do evade management control.  
Cross _et al._ ([2006](#cro06)): use social network analysis to measure knowledge transactions and perform targeted interventions in community of practice membership and structure.  
Meeuwesen and Berends ([2007](#mee07)): develop measures of performance of intentionally launched communities of practice.  
Schenkel and Teigland ([2008](#sch08)): use _learning curves_ to measure performance of identified communities of practice.</td>

</tr>

<tr>

<td>**Boundaries and innovation**</td>

<td>Hislop ([2003](#his03)): study of seven companies where a technological innovation was promoted by management; found that some communities of practice supported and others hindered the project.  
Bechky ([2003](#bec03)): ethnographic study of difficulties in transferring knowledge across community boundaries.  
Carlile ([2004](#car04)): identifies three types of boundaries between communities of practice, syntactic, semantic and pragmatic and three processes for spanning each type of boundary: transfer, translation, transformation.  
Swan _et al._ ([2007](#swa07)): use Carlile typology to examine the role of objects in spanning different boundaries in a study of innovation diffusion in the UK health system.  
Ferlie _et al._ ([2005](#fer05)): found that epistemic boundaries between discipline-bound health care communities of practice can retard the spread of innovations.  
M?rk _et al._ ([2008](#mor08)): study of cross-disciplinary R&D medical centre, where new knowledge challenging an established community of practice was marginalized.</td>

</tr>

<tr>

<td>**Identity construction**</td>

<td>Ibarra ([2003](#iba03)): mid-career transitions require severing ties from former communities of practice and joining new ones.  
Hara and Schwen ([2006](#har06)): ethnographic study of a public defenders' office developed new community of practice framework consisting of six dimensions.  
Handley _et al._ ([2007](#han07)): ethnographic study of identity construction by two junior consultants working at a leading firm.  
Campbell _et al._ ([2009](#cam09)): case study of the learning trajectory into a community of practice of a middle-aged nurse who made a career change to police officer.  
Goodwin _et al._ ([2005](#goo05)): ethnographic study of internal boundaries in multi-disciplinary communities of practice in anaesthesia.  
Faraj and Xiao ([2006](#far06)): emergency boundary suspension in multidisciplinary medical teams.</td>

</tr>

<tr>

<td>**Virtual communities of practice**</td>

<td>Bryant _et al._ ([2005](#bry05)): case study of increasing involvement in Wikipedia as an induction into an online community of practice and development of _Wikipedian_ identity.  
Hara and Hew ([2007](#har07)): case study of community of advanced practice nurses based on a listserv.  
Zhang and Watts ([2008](#zha08)): apply Wenger framework to online community focused on back-packing.  
Murillo ([2008](#mur08)): systematic Usenet search and detection of four virtual communities of practice displaying Wenger dimensions.  
Silva _et al._ ([2008](#sil08)): apply the legitimate peripheral participation model to a blog, operationalized as old-timers who enforce local norms.  
Fang and Neufeld ([2009](#fan09)): apply the legitimate peripheral participation model to an open-source software community, finding evidence of strong identity construction/enactment.</td>

</tr>

<tr>

<td>**The concept of community of practice**</td>

<td>Cox ([2005](#cox05)): critical review and comparison of four seminal studies.  
Roberts ([2006](#rob06)): literature review that highlights the limits of communities of practice as a knowledge management tool and identifies issues that have been insufficiently addressed in research.  
Hughes ([2007](#hug07a)): critique of Lave and Wenger (1991) that questions whether their model extends beyond the cases they examined.  
Amin and Roberts ([2008](#ami08)): critique the status of communities of practice as an umbrella concept and propose a typology of four modes of _knowing in action_.  
Gherardi ([2006](#ghe06)): proposes new definition and theoretical framework for communities of practice based on a full-length ethnography of three interacting communities of practice in a construction site.  
Hara ([2009](#har09)): proposes a new definition and theoretical framework of communities of practice based on ethnographic study of a public defenders' county office.</td>

</tr>

</tbody>

</table>

### Launching communities of practice

Several recent studies examine whether communities of practice can be launched, or more precisely, whether a group initiated by management has a reasonable chance of developing into a true community of practice. The answer appears to be a cautious yes, if the necessary structural elements are provided ([McDermott 2000](#mcd00); [Wenger _et al._ 2002](#wms02); [Thompson 2005](#tho05)). However, management attempts to control communities of practice, for instance by demanding certain deliverables, can simply transform them into organizational units (teams or task forces), make them go underground ([Gongla and Rizzuto 2004](#gon04)) or make them conform to the official line with little real learning ([McDermott 2007](#mcd07)).

Swan, Scarbrough and Robertson ([2002](#swa02)) provide an example of highly nuanced managerial intervention, a case study of a medical community of practice convened or promoted by administrators of a health organization as a vehicle for a radical innovation in the treatment of prostate cancer known as brachytherapy. These managers envisioned their role not quite as _launching_ but as _facilitating_ the construction of a new multidisciplinary community engaged in brachytherapy practice. They explicitly addressed constitutive dimensions of communities of practice such as a well-defined domain of knowledge, identity enhancement, networking and knowledge brokering. Thus, even though managers were relatively powerless before established medical professionals, they were able to deploy the theory and the discourse of communities of practice to promote the adoption of an innovative procedure. The study also provides a textbook example of the use of fashionable management discourse to promote change ([Benders and van Veen 2001](#ben01)).

Thompson ([2005](#tho05)) makes a contribution to the community-of-practice-launching debate by distinguishing between structural parameters and epistemic behaviour adopted from Wenger's ([1998](#wen98)) indicators (see [Table 2](#tab2)) and proposing that management can manipulate them to launch a community of practice. The study relies on participant observation and interviews to examine a co-located community of practice at a large, information technology hardware and services firm. The forty-member group was formally established as a creative Web-design agency, exempt from the commercial and procedural restrictions of the parent organization. It enjoyed heavy corporate sponsorship of information technology infrastructure and culturally symbolic artefacts (pool tables, video games, bean bags, etc.) conducive to a relaxed, informal and creative work environment. The author reports strong group identification and epistemic interaction, relying on Wenger's ([1998](#wen98)) framework to assess the emergent tight-knit group as a community of practice. However, the organization tried to capitalise on the group's success with the addition of 140 new in-training participants, which required formal documentation of procedures (hitherto unnecessary because of the group's small size) and other prescriptive measures such as controls on billable versus non-billable activities. This brought about the demise of the original community of practice, as members quickly withdrew identification and commitment from the new, more formalised structure. These findings are in line with Wenger's ([1998](#wen98)) position that communities of practice can be supported or _nurtured_ but not controlled. Furthermore, they refine Wenger's framework by distinguishing between two dimensions which managers must pay attention to when launching communities of practice. The first, are seeding structures, including shared symbols, artefacts, monuments, tools and boundary objects, that can be comprised under the Wenger concept of shared repertoire and which organizations can subsidize and make available to potential community of practice members. Second and more difficult, is encouraging members to interact within these structures and among themselves, i.e., to engage in practice or perform the epistemic behaviours which over time will give rise to a community of practice. The article also maps these dimensions onto Wenger's indicators, with epistemic behaviours corresponding to [Indicators 1-9](#tab2) and structural components to [Indicators 10-14](#tab2).

Anand _et al._ ([2007](#ana07)) investigate the success factors for launching new practice areas within management consultancies. They characterize these as innovative knowledge-based structures and they expressly identify them with communities of practice as portrayed in a vignette of a consulting company in the book by Wenger _et al._ ([2002](#wms02)). Their multiple case study was conducted at four consulting firms and included a total of twenty-nine cases of practice areas, including both successful and unsuccessful efforts. This led them to identify four critical generative elements: socialised agency (a consultant's drive to create a new practice area as a key career-progression move), differentiated expertise (a new and distinctive body of professional knowledge), defensible turf (persuading others of the market relevance of the new practice area) and organizational support (resources, personnel and sponsorship to nourish the new practice area). All instances of successful practice area creation displayed socialised agency as the process catalyst. In what the authors called emergence step, agency combines with one of the other three critical elements, which gives the new practice area visibility within the firm. But the new structure only becomes viable if the other two elements are also added, in what is called the embedding step. The study identified three equally robust pathways whereby a practice area can be born depending on which of the three elements initially combines with socialised agency: the expertise-based pathway (when a consultant develops new expertise), the turf-based pathway (when a client opportunity provides a consultant with enough market power) and the support-based pathway (when firm leadership nominates a consultant to create a new practice top-down). The study found as many instances of successful management-launched communities of practice, through the support-based pathway, as of bottom-up emergence through the other two pathways. Study findings thus complement Thompson's ([2005](#tho05)) single firm case study and contrasts with previous literature about the immunity of communities of practice to management control ([Gongla and Rizzuto 2004](#gon04); [Pastoors 2007](#pas07)).

In sum, there is empirical support to claims that communities of practice can be intentionally designed and launched and there is certainly no shortage of step-by-step guides (e.g., [McDermott and Kendrick 2000](#mck00); [Wenger _et al._ 2002](#wms02); [Plaskoff 2003](#pla03); [Saint-Onge and Wallace 2003](#sai03); [Moran and Weimer 2004](#mor04)).

### Managing or controlling communities of practice

On the other hand, several studies document the reluctance of community of practice members to maintain their commitment when management attempts to control the learning agenda of the community or request specific deliverables. An example is the demise of Thompson's ([2005](#tho05)) community, described earlier. In addition, there is a systematic study by Gongla and Rizzuto ([2004](#gon04)) who tracked the _disappearance_, over a six year period, of twenty-five organizational communities of practice at IBM Global Services. In many cases, the demise of communities of practice can be attributed to _natural_ causes, as members' interests and commitments shift. However, they also found that management intervention can cause the transformation or demise of a community of practice in two ways: first, management interventions can transform a community of practice into an official organizational unit, like a programme, a project or a practice. Henceforth, decisions about (former) community objectives, agenda, deliverables and membership are made by management, not by members. Second, a community that faces increasing management control may decide to '_remove itself completely from the organizational radar screen_' ([2004: 299](#gon04)) and continue to function off-site or outside work hours in order to preserve its independence and avoid management-imposed assignments.

In a similar vein, Pastoors ([2007](#pas07)) provides a case study of a large information technology consultancy that ran a strong internal programme of institutionalised communities of practice. Management assigns consultants to different communities of practice without paying much attention to their preferences. communities of practice are highly formalised and operate according to strict guidelines with respect to roles, communication and performance evaluation. Moreover, consultants' career advancement is contingent on their performance in their assigned community of practice. The study found consultants were not motivated to spend extra time or effort in their assigned communities of practice. Instead, they joined and spent time on bootlegged, unofficial communities of practice where they were free to pursue their passion.

An unusual angle, with respect to management control, is provided by Cross _et al.'s_ ([2006](#cro06)) study of targeted interventions to improve performance of communities of practice. They use social network analysis to map the existing relationships between community members and the volume of knowledge transactions flowing through these social ties. Coupled with background information of member expertise, social network analysis can reveal community of practice members who are excessively connected and thus bear a disproportionate burden of consultations, usually repetitive. Social network analysis can also detect functional and geographical silos where good practices are not being effectively communicated due to lack of connections between some members. Similarly, such analysis can locate peripheral individuals in the community who have high experience and expertise but are relatively isolated and hence unable to fulfil their potential. Specific interventions that companies can use include revising the formal roles of certain community of practice members, using electronic profiling systems to communicate member expertise more broadly and transferring or rotating specific experts to particular geographical areas. Post intervention member surveys, again interpreted through social network analysis, can then quantify the knowledge-transfer improvements and thus validate the interventions.

Another dimension of management control is the attempt to measure the performance of intentionally launched communities of practice. Various indicators have been tried, including levels of community of practice activity, development of new products and processes, knowledge sharing behaviours, messages posted in discussion boards, etc. A related concern has been to measure the benefits of community of practice activity to the launching organization ([Lesser and Storck 2001](#lese01); [Fontaine and Millen 2004](#fon04)).

Meeuwesen and Berends ([2007](#mee07)) provide a case study of four intentionally launched communities of practice focused on advanced manufacturing technologies at Rolls Royce. The communities were formed by management-designated experts, about ten in each. All of them went through a day long workshop where they learned about the characteristics and benefits of a community of practice and a dedicated facilitator was assigned to each. An important caveat is that the communities of practice were not all launched simultaneously: at the time of the evaluation the youngest was a month old and the oldest (and most successful) over three months old. The company evaluated the performance of the communities of practice using member surveys with scales for measuring community of practice activities such as internal and external knowledge sharing, contributions to the online bulletin board and meeting frequency. Other scales measured outcomes, such as number of products, procedures and processes adopted, that were originally mentioned in the community of practice.

The study found that intentionally designed communities of practice indeed began to function as such and provided some benefits to its members. However, the results were uneven across the four communities of practice; in particular, no correlation was found between the level of community of practice activity and the outcome variables. Moreover, the study found that the structural elements of communities of practice (the Wenger dualities of participation-reification, identification-negotiability, local-global and designed-emergent) take time to develop and to become balanced. A limitation of the study is lack of information about ground rules, deliverables, or the time members were allowed to commit to the community of practice. Hence, even though the study is grounded upon Wenger's (1998) framework, it is difficult to decide whether these communities are true communities of practice, cross-disciplinary teams or committees, a failing displayed by similar other studies of community of practice performance (e.g., [Chua 2006](#chu06); [Verburg and Andriessen 2006](#ver06); [Probst and Borzillo 2008](#pro08)).

Schenkel and Teigland ([2008](#sch08)) used learning curves to measure the performance of four co-located communities of practice at a large construction site. To identify them, they relied on the Wenger dimensions of mutual engagement, joint enterprise and shared repertoire. They developed a performance measure using the learning curves associated with the number of recorded deviations from defined standards. They found all learning curves had negative slopes, indicative of a decreasing number of deviations, which in turn spoke of improving community of practice performance. One community broke the pattern, though, displaying positively sloped learning curves for a time and then plateauing. The authors traced this anomaly to a disruption in communicative processes caused by a physical move of the group to a new location that additionally split the group between two separate locations, making face-to-face exchanges a rare occurrence.

Reviewed studies thus seem to confirm the ability of communities of practice to evade management control when they feel their jointly negotiated enterprise is threatened. Management can, of course, take over a community of practice but Gongla and Rizzuto ([2004](#gon04)) are correct in pointing out this will just turn it into a committee or task force, which is unlikely to draw the same level of enthusiasm from members.

### Boundaries and innovation

Recent studies have also examined the role communities of practice sometimes play in retarding or inhibiting innovation. This is a relatively new angle given the many studies that present innovation as a defining feature of communities of practice ([Orr 1990](#orr90); [Brown and Duguid 1991](#bro91), [2000a](#bro00a); [Brown and Grey 1995](#bro95); [Prokesch 1997](#pro97); [Swan _et al._ 1999](#swa99); [Wenger 2000b](#wen00b); [Lesser and Everest 2001](#lese01); [Fontaine and Millen 2004](#fon04)). Indeed, the studies by Anand _et al._ ([2007](#ana07)), [Meeuwesen and Berends (2007](#mee07)) and Schenkel and Teigland ([2008](#sch08)) all provide evidence of innovation taking place inside communities of practice and specific measures of innovation are often used in studies of communities of practice performance. Yet there are also studies showing more mixed results.

For instance, Hislop ([2003](#his03)) reports longitudinal case study evidence from seven companies implementing technological innovation projects, specifically multi-site, cross-functional management information systems. He uses Brown and Duguid's ([2001](#bro01)) definition of communities of practice as groups possessing common knowledge/practices, shared identity and common work-related values, which results in his identifying communities of practice with local business units and/or business functions. The study found communities of practice that strongly supported the innovation project, specifically those that had a strong information technology identity which valued information systems. Other communities of practice hindered the innovation because they valued their local autonomy, were resistant to management's centralising agenda and were reluctant to share knowledge with other units. These results are congruent with Wenger's ([2000b](#wen00b)) views on boundaries and identities, but are limited by the condensed definition of community of practice and the study's generalised characterization of each of the company's business units and functions as communities of practice.

Wenger ([2000b](#wen00b)) argues that community of practice boundaries deserve special attention because they connect different communities of practice and because they offer distinct learning opportunities. Radical insights often arise at the intersection of multiple practices. Yet the process is not without tension and conflict. Researchers have chronicled instances of successful innovation at the boundaries of different communities of practice, but also instances where such boundaries have retarded the spread of innovations. In talking about moving knowledge across communities of practice or networks of practice, Duguid ([2005](#dug05)) introduces a useful distinction by talking about the epistemic and ethical entailments of practice: the former refers to the challenge of translating knowledge held within one practice into a different one; the latter refers to the political barriers that may make exporting such knowledge (or even importing it) unacceptable to one of the communities.

In fact, boundaries (specifically boundary spanning) have recently become an important topic in their own right, to the point that the communities of practice that generate them are scarcely examined (e.g., [Carlile 2004](#car04); [Swan _et al._ 2007](#swa07)). Moreover, some danger of conceptual confusion arises from the fact that since boundaries are created by different practices, they are created not only in communities of practice, but also in larger networks, such as professions, occupational communities and networks of practice ([Brown and Duguid 2001](#bro01)).

The study by Bechky ([2003](#bec03)) illuminates boundary processes by providing a detailed ethnographic analysis of the interactions between different communities of practice at a semiconductor equipment manufacturing company. The manufacturing process included three phases assigned to three highly distinct communities of practice. In the design phase, teams of design engineers developed the engineering drawings of new products. These would be turned over to the technicians of the prototyping phase, whose job was to verify and correct the drawings by building a physical prototype of the machine. After several prototypes had been built and technicians and engineers had agreed on a final set of drawings, the assemblers of the assembly phase were brought in to learn from the technicians and the prototypes how to build a finished product. These three communities of practice had different practices, languages, repertoires and perspectives, but needed to coordinate and work together, especially during product 'handoffs'. The study uses the term decontextualisation to describe the misunderstandings and communication difficulties between the three communities of practice. It occurred when people from different communities of practice met to discuss a problem bringing different understandings of the same problem. The misunderstandings were resolved through a process named transformation (borrowed from [Carlile 2004](#car04)) which occurred when members of one community of practice came to understand how knowledge from another community of practice fit within the context of their own work, thus enriching and altering what they knew ([Wenger 2000b](#wen00b)). It was not just the introduction of new knowledge, but the placing of it within their own locus of practice that allowed the practitioners to see the world in a new light.

Carlile ([2004](#car04)) undertakes a similar ethnographic study of exchanges between different functional groups involved in new product development at a car manufacturer. The contribution of this study is a classification of boundaries between communities of practice into three progressively more complex types: syntactic or information-processing boundaries, semantic or interpretive boundaries and pragmatic or political boundaries. Furthermore, the study describes three processes for moving knowledge across each type of boundary: transfer, translation and transformation.

Swan _et al._ ([2007](#swa07)) apply this framework to explain the role played by objects in exchanging knowledge across boundaries in a longitudinal study of ten innovations in the UK's National Health Service. Each of Carlile's boundaries was spanned by an object with different epistemic attributes. The syntactic boundary was spanned by developing a common project database to serve the different professional groups involved. The semantic boundary required not just a physical object but the collaborative development by participating groups of data collection instruments, such as a patient questionnaire, in order that they might align their different interpretations in a concrete instrument. The pragmatic boundary is the most complex because the involved actors have different vested interests and incentives which make them unwilling to change their practice and which must be reconciled in order to successfully span the boundary. This was accomplished by two members of the project who prepared and delivered a presentation on the benefits of the research, along with an information pack, to each of the recruiting centres and clinician groups whose commitment was essential to the project.

In a study more directly overtly concerned with communities of practice, Ferlie _et al._ ([2005](#fer05)) provide an interview-based longitudinal analysis of the spread of eight innovations in the UK health system. Half of the innovations had strong scientific evidence supporting their medical value, while the other half had more contestable evidence. Moreover, half of the innovations involved just one focal stakeholder, being relatively easy to implement, while the other half involved multiple stakeholders and greater difficulty to achieve consensus. Results showed unexpectedly slow spread of innovations with strong scientific support, one of them spread widely, two had some spread and one stayed at the pilot stage. Innovations involving one stakeholder spread more widely than those involving multiple stakeholders, but not overwhelmingly so. In interpreting their results, the authors theorize that the spread of innovations was retarded by social and epistemic boundaries between uniprofessional communities of practice of the various health care professionals involved in the system. They report three characteristics of these groups which differentiated them from Wenger's ([1998](#wen98)) claims processors community: they are unidisciplinary, they seal themselves off from neighbouring communities defending jurisdiction and group identity and they are highly institutionalised. These features erected barriers to learning and knowledge sharing between communities of practice, for instance between the communities of orthopaedic and vascular surgeons, whose lack of consensus led to the slow spread of one of the strongly supported innovations. However, a closer examination of the communities involved is warranted, to discard the alternative explanation that these were territorial disputes between members of competing professional associations ([Swan _et al._ 1999](#swa99); [Cox 2007a](#cox07a)).

Finally, M?rk _et al._ ([2008](#mor08)) develop an ethnographic account of a medical R&D centre whose mission is to develop new practices for patient diagnostics and treatment using advanced technologies contributed by a number of different professional groups, such as surgeons, anaesthetics, engineers, nurses, radiologists and radiographers (imaging technicians). At the time of the study, the centre had operated for twelve years, developed over twenty new procedures, filed twelve patent applications and published over 200 scientific papers. Thus it has been highly successful in developing new cross-disciplinary practices. However, the study found that collaboration between different communities of practice was a source of constant tension. New practices had to build upon the knowledge of different co-located communities of practice, each one with its own epistemological foundations. Informants reported that less collaboration took place than was desirable and that many opportunities went unrealised. In particular, new knowledge that challenged current practice belonging to any of the involved communities of practice was more likely to be marginalised.

### Identity construction

Both Lave and Wenger ([1991](#lav91)) and Wenger ([1998](#wen98); [2000b](#wen00b)) underscore the importance of identity construction in the individual decision to join a community of practice, since learning the practice implies acquiring the identity of a competent practitioner. People have an intrinsic and powerful motivation to join some communities of practice and keep their distance from others:

> In the landscape of communities and boundaries in which we live, we identify with some communities strongly and not at all with others. We define who we are by what is familiar and what is foreign, by what we need to know and what we can safely ignore ([Wenger 2000b](#wen00b): 239).

For example, people undergoing career change, even as they withdraw personal and psychological commitment from outdated professional identities and related communities of practice, try to connect to new communities of practice in order to perform low-risk practice/identity experiments to bring into sharper focus the new professional identity they are trying on ([Ibarra 2003](#iba03)).

Studies of people joining communities of practice to develop practitioner identities are often grounded on Lave and Wenger's ([1991](#lav91)) theoretical framework of legitimate peripheral participation (e.g., [Harris _et al._ 2004](#har04); [Taber _et al._ 2008](#tab08); [Campbell 2009](#cam09)). This framework has recently benefited from insights regarding apprenticeships at modern workplaces which are very different from the ethnographies grounding Lave and Wenger's model of legitimate peripheral participation. Specifically, Fuller and Unwin ([2004](#ful04)) studied the relationships between apprentices and experienced workers at four private companies from the steel industry in the UK, where the government funded a programme called Modern Apprenticeships. Using interviews, structured learning-logs, surveys and observations, they found that learning was not a one-way relationship from experienced workers to apprentices as predicted by the legitimate peripheral participation model. In all companies, _novices_ reported helping others, both novices and experienced workers, to learn new skills in spontaneous problem-solving sessions at work. For instance, many young apprentices were more familiar with information technology than their older, more experienced colleagues. Thus the study warns that neither novices nor experts are stable or uniform concepts and that modern novices bring a wealth of previous learning experiences to the workplace.

Hara and Schwen ([2006](#har06)) provide an ethnographic study of a public defenders' office, where attorneys formed a strong co-located community of practice to share knowledge and information and support each other in the difficult task of representing clients who did not have means to pay for private counsel. Because the job was not high status, even in the eyes of the clients, attorneys banded together to provide emotional support. The community had several members with considerable experience and willingness to share knowledge and mentor others. Moreover, the performance of new attorneys was closely monitored, since their failure or successes in court reflected on the prestige of the public defender's office. The authors introduce a new theoretical framework for communities of practice; with more detail provided in a later monograph of the full ethnography ([Hara 2009](#har09)).

Handley _et al._ ([2007](#han07)) provide an ethnographic study of identity construction by two junior consultants working at a leading firm of strategy consultants. Their opportunities for participation and identity-construction were closely regulated by senior managers who assigned them to less visible data crunching activities and allowed them only limited contact with clients. Junior consultants attended internal review meetings where they would hear senior consultants speak about project progress, client responses and ways of _handling_ the client. They also accompanied senior consultants to work meetings with clients where they could silently observe interactions of their older colleagues with clients. At one point, the two junior consultants were assigned to play a much more active role at the site of a different and relatively minor client, where their existential confidence in being 'good consultants' increased sharply. Apprentices thus learned what it meant to _think like_ and to _behave like_ a consultant from carefully orchestrated opportunities for legitimate participation and thereby developed their professional identities as consultants and through different project assignments and interaction with senior colleagues became conscious of possible career paths, i.e., '_the proposal of an identity_' ([Wenger 1998](#wen98): 156).

Campbell ([2009](#cam09)) uses Lave and Wenger's ([1991](#lav91)) model of legitimate peripheral participation to examine the learning trajectory into a community of practice of a nurse unit manager who at the age of fifty made a career change to police officer. Her previous managing experience gave her people skills, respect for hierarchical authority and medical expertise which were all valued by the policing community, where she was accorded respect much sooner than younger trainees. While her training progressed, she continued to work part time as a nurse, but over time she shifted her identity from a nurse becoming a police officer to a police officer who used to be a nurse. After gaining her constable stripes, she quit nursing altogether and started seeing herself first and foremost as a police officer. The study contributes the insight that learners do not shed their former identity when striving to acquire a new one; rather the new identity is a composition of previous histories enriched with new experiences.

One of the manifestations of identity enactment is a strong awareness of the occupational and institutional boundaries that separate members of the community of practice from non-members. These boundaries are less about the epistemic barriers between communities of practice, discussed before, than about establishing who has legitimate access to practice in the community.

The ethnography of anaesthetic teams by Goodwin _et al._ ([2005](#goo05)) uses Wenger's ([1998](#wen98)) framework to examine multidisciplinary communities of practice that stand in contrast to the unidisciplinary communities analysed in seminal studies ([Lave and Wenger 1991](#lav91); [Brown and Duguid 1991](#bro91); [Wenger 1998](#wen98)). An anaesthetic team is composed of an anaesthetist, an operating department practitioner and a recovery nurse. The ethnography brings out how boundaries inside the community of practice are drawn and regularly enforced by the enactment of the different professional practices and identities of each practitioner. The study also shows how in this community legitimacy is stratified, i.e., access and participation is contingent upon each member's professional identity and learning trajectories do not lead to all-encompassing mastery, as they did in Lave and Wenger's ([1991](#lav91)) examples, but are constrained by the explicit rules and responsibilities governing each position in the anaesthetic team.

In a related vein, Faraj and Xiao's ([2006](#far06)) study of emergency medical coordination procedures identifies a unique instance of boundary suspension. The authors examine coordination between different communities of practice in a medical trauma centre, a fast-paced setting where diverse communities must collaboratively build an accurate diagnosis and treatment for patients. Disciplinary and epistemic boundaries between communities of practice are regularly enforced; the study describes how communities of practice assume coordinating responsibilities across disciplines, as well as scheduling and legitimate participation (learning) responsibilities within each discipline. Furthermore, the authors identified a practice labelled expertise coordination process, which relies heavily on established protocols and facilitates the management of the diverse and interdependent skills and knowledge required to diagnose and treat patients. This is normal practice, accounting for 90% of patients. The remaining 10% are patients whose condition unexpectedly deteriorates, demanding immediate improvisational intervention. The authors labelled these dialogic coordination practices; they involve a suspension of disciplinary boundaries, a process of joint sense making between participants, drawing in additional experts, judicious breaking of protocols and deliberate boundary-crossing interventions in the name of patient safety.

### Virtual communities of practice

No review of community of practice literature can fail to mention the current popularity of the topic of virtual or Internet-based communities of practice. The increase in the number of such studies is partly explained by the relentless pace of technological innovation (e.g., blogs, wikis and social networking sites are all recent developments) and strong interest among knowledge management practitioners who view community of practice development as a key offering in their portfolios. Workshops on launching and supporting virtual communities of practice are a staple of professional knowledge management conferences.

Two important clarifications should be made with respect to the virtual community of practice literature. First is the fact that many studies label as virtual communities of practice what are really virtual teams or workgroups convened on a temporary basis to accomplish specific projects (e.g., [Rogers 2000](#rog00); [Davenport 2004](#dav04)). The compulsory character, professional or academic, of such projects leads to impressive online collaboration, but for a limited time. The disbandment of the team at the end of the project clearly distinguishes them from communities of practice which are usually characterized as emergent persistent communities ([Barab _et al._ 2003](#bar03)).

The second and more serious problem, just as in the co-located community of practice literature, is that a large majority of published studies have relied on a condensed definition of community of practice, that either lacks a formal model, or sidesteps Wenger's ([1998](#wen98)) theoretical framework without providing a developed alternative, thus casting doubt on the characterization as a community of practice of examined communities (e.g., [Baym 2000](#bay00); [Robey _et al._ 2000](#rob00); [Johnson 2001](#joh01); [Schlager _et al._ 2002](#sch02); [Pan and Leidner 2003](#pan03); [Ardichvili _et al._ 2003](#ard03); [Dub? _et al._ 2005](#dub05); [Ardichvili _et al._ 2006](#ard06); [Fahey _et al._ 2007](#fah07); [Usoro _et al._ 2007](#uso07)).

Given these two caveats, all of the virtual community of practice studies which follow involve a persistent online community and are based on a theoretically grounded model.

Bryant, Forte and Bruckman ([2005](#bry05)) provide a case study of participation in Wikipedia as an induction into an online community of practice, _becoming Wikipedian_, as it were. They apply Lave and Wenger's ([1991](#lav91)) concept of legitimate peripheral participation to describe the induction of novices into the Wikipedia community. Furthermore, they report the presence of Wenger's ([1998](#wen98)) traits of _mutual engagement_, _shared repertoire_ and _joint enterprise_. Although the question remains of whether this is truly a single community or rather a constellation ([Wenger 1998](#wen98)), the study makes a theory-grounded case for a virtual community of practice.

Hara and Hew ([2007](#har07)) use content analysis of messages and twenty-seven member interviews to build an in-depth case study of an online community of advanced practice professional nurses based on a listserv. In their depiction of the group as an online community of practice the authors apply Wenger's ([1998](#wen98)) theory, but take as defining characteristics _practice, community, meaning_ and _identity_. However, as described before, these are the elements of Wenger's social theory of learning, in which communities of practice constitute a single element ([Wenger 1998](#wen98): 5). Notwithstanding this difference of interpretation, reported evidence of Wenger's three constitutive dimensions is sufficient to support the authors' characterization of the community as a successful virtual community of practice.

Zhang and Watts ([2008](#zha08)) provide a study of a very active Chinese online community focused on backpacking. They relied on Wenger's ([1998](#wen98)) framework, specifically the dimensions of _mutual engagement_, _joint enterprise_, _shared repertoire_, _practice_ and _identity construction_. The study performed qualitative data analysis of messages downloaded from the bulletin board and complemented this with interviews of the group's moderators. From the presence of all Wenger dimensions, the authors conclude the group is indeed a virtual community of practice.

Murillo ([2008](#mur08)) conducted a systematic search of the mainstream hierarchies in the Usenet discussion network, using quantitative and qualitative filtering criteria developed from Wenger's dimensions. This resulted in a selection of eleven high-potential newsgroups, which were further examined using social network analysis (core-periphery structure), an online survey of participants and content analysis of discussions. By triangulating results from these instruments, the study identified four professionally oriented newsgroups that displayed the complete set of Wenger dimensions and were thus rigorously assessed as Usenet-based communities of practice.

Silva _et al._ ([2008](#sil08)) provide a theory-grounded study of an Internet community of practice based on the relatively new medium of blogs. They conducted an interpretative study of MetaFilter, an online community blog which they describe as a community of practice. They performed hermeneutic interpretation of thirty-eight threads containing about 1,300 comments, plus the posting guidelines and policies for new users. The data were coded using four constructs derived from Lave and Wenger ([1991](#lav91)) and Wenger ([1998](#wen98)): identity, knowledge sharing, warrants (community procedures for evaluating the relevance of posts) and legitimate peripheral participation. However, legitimate peripheral participation was operationalized as old-timers enforcing community rules for good posts, either through praise or ridicule, which does not consider things from the viewpoint of novices and, more fundamentally, does not document inbound trajectories into the community of practice. With respect to the constitutive Wenger dimensions, the existence of a Joint enterprise is questionable since the blog is unmoderated and is not centred on any particular topic other than discussing the Web. Members argue over what constitutes a good post or a bad post, but this has more to do with style and language, than online engagement on a particular topic. Hence questions remain about the characterization of this blog as a virtual community of practice.

Finally, Fang and Neufeld ([2009](#fan09)) used qualitative analysis of online documents and e-mail messages to predict sustained participation in an open-source software development online community. The study is grounded on Lave and Wenger's legitimate peripheral participation framework; specifically focused on two dimensions: situated learning (operationalized as conceptual and practical contributions to code development) and identity construction (operationalized as identity regulation and identity work). The study found both of these dimensions were related to sustained participation, but initial motivation and initial access to the community were not. The authors note this community of practice differed from the traditional, which were apprenticeship-oriented ([Lave and Wenger 1991](#lav91)) or collegial-oriented ([Brown and Duguid 1991](#bro91); [Wenger 1998](#wen98)). In the community, conceptual or practical contributions were required for sustained core membership. Thus, although initial access was easily granted, it was not a community of practice for novices to learn how to code, but rather a serious project for experts willing to commit time and work. An example of this is how the project leader grants different levels of code-modifying privileges to participants, according to their expertise and previous contributions, thus echoing Goodwin _et al.'s_ ([2005](#goo05)) concept of stratified legitimacy. Where Lave and Wenger ([1991](#lav91)) pay little attention to the learning of established members, Fang and Neufeld ([2009](#fan09)) put the greatest emphasis on the situated learning achieved by expert and involved core members. In this they reveal a very different interpretation of legitimate peripheral participation than Lave and Wenger and leave us with the impression that Wenger's ([1998](#wen98)) construct of _mutual engagement_ would have provided a better conceptual fit. Nevertheless, the article makes strong contributions to the study of identity construction and enactment in virtual communities of practice and clearly illustrates the social definition of competence that takes place within communities of practice, even when they are Internet-based.

### The concept of community of practice

In recent years the confusion in the literature and the perceived dilution of the concept to little more than a fashionable label has resulted in the publication of several conceptual critiques and/or proposed typologies.

Cox ([2005](#cox05)) provides a comparative review of the three seminal studies ([Lave and Wenger 1991](#lav91); [Brown and Duguid 1991](#bro91); [Wenger 1998](#wen98)) and the Wenger _et al._ ([2002](#wms02)) practitioner book. Across these works the author finds substantial differences in the treatment of key issues: community, learning, power, change, formality and diversity. The article provides a useful comparative table of these concepts across the four studies ([2005: 537](#wen05)). The author traces the popularity of the concept to the ambiguity of both terms, _community_ and _practice_, which has enabled academic and practitioner audiences to appropriate the notion in different ways, which fits Benders and van Veen's notion of interpretive viability. The same author provides a remarkable account of the appropriation and adaptation of Orr's ([1996](#orr96)) ethnographic findings about the Xerox technical representatives, to develop an MBA case study for a top business school on the topic of knowledge management ([Cox 2007b](#cox07b)).

Lindkvist ([2005](#lin05)) proposes a two-part typology of knowledge work performed in groups. On one side he locates communities of practice, characterised as tightly knit, with a high degree of shared understandings and repertoire, operating with a significant amount of face-to-face encounters and requiring an extended time period of local interaction to develop fully. On the other side he locates a frequent work unit in today's economy, which he terms a collectivity of practice. This is a temporary group or team, assembled to carry out a specified task involving knowledge creation and exchange and to do so within cost and time limits. Moreover, members of the collectivity have typically not met or worked together before and possess highly specialised competences, not conducive to shared understandings or a common knowledge base.

Roberts ([2006](#rob06)) provides an in-depth critique of the community of practice concept, identifying several issues that have been insufficiently examined in the literature and which limit the usefulness of the approach as a knowledge management tool. First, since trust is a precondition for knowledge-sharing within a community of practice ([Wenger 2000b: 230](#wen00b)), the approach will be less effective in organizational environments characterised by adversarial relations between workers and management. Secondly, societies with strongly individualistic cultures (such as the UK or the USA), which have experienced a decline of community in the social context, will experience greater difficulty in deploying a community structure in business organizations. Thirdly, the author rejects using the concept for the large (over 1,500 members), distributed communities described in Wenger _et al._ ([2002](#wms02)), arguing that size and spatial reach impose limits to member participation. Finally, the current acceleration of change in business organizations (e.g., restructurings, downsizings and outsourcing) threatens to disrupt the sustained engagement communities of practice need to develop and endure.

Hughes ([2007](#hug07a)) points out that Lave and Wenger ([1991](#lav91)) are somewhat equivocal on whether their theory of learning emerges from their empirical research, or is projected on to concrete cases of apprenticeship. In effect, the critique questions '_whether the theory can be said to speak beyond the cases examined_' ([2007](#hug07a): 39). This also translates into an ambiguity with respect to the status and purpose of Lave and Wenger's model; whether it is a descriptive theory of learning or a prescriptive model of learning. The author suggests that it is the second version, the _ideal model_ of how learning should be that has been eagerly adopted by consultants and human resource development practitioners, in turn leading to numerous _translations_ of the concept and an increasing divergence from the original theory.

Amin and Roberts ([2008](#ami08)) argue that the concept has become an umbrella term that does not contemplate all the social varieties of _knowing in action_. They conducted an extensive review of the literature describing situated social practice, learning and knowing. From this they developed a four-part typology of specific modes of knowing in action: task or craft-based, professional, epistemic or creative and virtual. Each group is then examined along four proposed dimensions of the character and dynamic of knowledge production: the type of knowledge used and produced, the nature of the within-group social interaction, the kind of innovation achieved and the within-group organizational dynamic. In turn, three further traits characterise the within-group social interaction: proximity, longevity and strength of the social ties. The authors acknowledge their typology is not exhaustive, nor are the four groups mutually exclusive, but their intent is to highlight that differences in their proposed dimensions result in significant differences between the groups. With respect to communities of practice, each of the four proposed groups contains literature examples of communities of practice and non-communities of practice. The key dimension in separating one from the other appears to be the proximity, longevity and (resulting) strength of social ties; which is coherent with Wenger's ([1998](#wen98)) admonition regarding sustained mutual engagement.

Lastly, two authors deserve particular attention for conducting and publishing full-length ethnographic accounts of communities of practice in organizational settings and for proposing comprehensive theoretical frameworks for communities of practice that for the first time offer a developed alternative to Wenger ([1998](#wen98)).

Gherardi ([2006](#ghe06)) conducted ethnographic fieldwork of three distinct communities of practice in a construction site, in order to study the situated learning of safety. The book goes beyond Wenger's ([1998](#wen98)) single case ethnography by closely examining how different communities of practice interact discursively. The author proposes a theoretical-methodological framework to account for the phenomenon of _knowing-in-practice_ and includes a radically new conceptualization of communities of practice, deliberately relabelled as '_communities of practitioners_' in order to shift the emphasis to the pre-existing practice. Gherardi does not view communities of practice as a social object or as a collective subject, but more as a fluid social process whose _existence_ is a construction of the researcher's gaze. Lave and Wenger's ([1991](#lav91)) realist ontology is rejected in favour of postmodern constructivist assumptions, thereby proposing a novel way of looking at communities of practice:

> I argue that it is practice, with its materiality, its technological knowledge and its transorganizational character, that organises a community. I maintain that practice 'performs' the community ([Gherardi and Nicolini 2002](#ghe02)) in order to emphasise that the terms of the causal relation have been reversed: it is not the community as the acting subject that somehow precedes the action and has ontological primacy over it; rather, it is the process of doing, the course of action, which aggregates an incipient community in a process of reciprocal definition. ([Gherardi 2006](#ghe06): 108)

Gherardi's theoretical-methodological framework for _knowing-in-practice_ is proposed as a third way between a mentalistic vision of knowledge in organizations, identified with the discourse of organizational learning and a commodification and reification of knowledge, identified with the discourse of knowledge management. The aspiration to avoid these two discourses through a reliance on the concept of practice has resulted in a new and growing field called _practice-based studies_, where the author has emerged as a leading figure (e.g., [Gherardi 2009a](#ghe09a), [2009b](#ghe09b)).

Hara's ([2009](#har09)) monograph is a full-length ethnography of a public defenders' county office where attorneys have developed a strong community of practice to share information, provide emotional support and learn from each other. The book proposes a specific definition of communities of practice as '_collaborative, informal networks that support professional practitioners in their efforts to develop shared understandings and engage in work-relevant knowledge building_' ([2009](#har09): 3). In addition, the author proposes a novel theoretical framework for communities of practice comprised of six distinct attributes:

1.  a group of professional practitioners;
2.  development of a shared meaning;
3.  informal social networks;
4.  a supportive culture involving trust;
5.  engagement in knowledge building; and
6.  members' negotiation and development of professional identities

Thus, although not as fully developed as Gherardi's ([2006](#ghe06)) theory, Hara makes a welcome contribution to the field by proposing an alternative to Wenger's framework that is grounded on a full-length organizational ethnography. More such studies are urgently needed.

## Detected trends and conclusions

This review of the first two decades of the literature has found both cause for concern and hopeful developments. This section summarizes detected trends and concludes with an optimistic forecast.

### Misuse of the communities of practice concept

As described in sections three and four, the literature reveals a large number of studies that rely on condensed or abridged definitions, lack a theory-grounded model, or mistake communities of practice with other social structures that somehow feature knowledge sharing, such as occupational communities, professional associations, epistemic cultures, or networks of practice. Although less often, the confusion also goes the other way, as some exemplary studies using the later concepts are clearly about communities of practice in the strictest sense (e.g., [Orr 1990](#orr90); [Bechky 2003](#bec03)).

Further confusion has been introduced into the literature by a number of competing designations for essentially the same social phenomenon or some aspect of it, including communities of knowing ([Boland and Tenkasi 1995](#bol95)), strategic communities ([Stork and Hill 2000](#sto00)), knowledge networks ([B?chel and Raub 2002](#buc02)), communities of coping ([Korczynski 2003](#kor03)) and knowledge communities ([Barret _et al._ 2004](#bar04)).

### Interpretations and adaptations of the communities of practice concept

The review encountered numerous studies that casually applied the community of practice label to a learning group without a formal assessment of the traits defined by existing theoretical frameworks ([Lave and Wenger 1991](#lav91); [Brown and Duguid 1991](#bro91); [Wenger 1998](#wen98)) and without proposing an alternative framework. A clear example are the many studies that apply the label to short-lived organizational workgroups or task forces and to semester-long academic team projects, virtual or co-located. The review sees this as evidence of communities of practice becoming fashionable and of the concept being loosely interpreted without being theoretically amended or challenged. This is cause for concern to the extent that good research should be grounded on theory.

On the other hand, extant theory ([Brown and Duguid 1991](#bro91); [Wenger 1998](#wen98)) describes communities of practice as sufficiently versatile to accommodate a wide range of successful organizational deployments. The studies outlined earlier, which are all theoretically grounded, provide examples of communities of practice used as fashionable managerial discourse to drive organizational change ([Swan _et al._ 2002](#swa02)); as a support group to successfully navigate a mid-life career transition ([Ibarra 2003](#iba03)); or as a task force focused on product innovation ([Thompson 2005](#tho05)). communities of practice have been purposefully deployed as a formal organizational structure for discipline-based personnel management ([Faraj and Xiao 2006](#far06)); as knowledge-based practice areas in consultancy firms ([Anand _et al._ 2007](#ana07)); and as online peer support groups for highly specialized professionals ([Hara and Hew 2007](#har07)). Success in such diverse endeavours clearly adds to the concept's appeal and supports those who defend the organizational contribution of communities of practice ([Wenger _et al._ 2002](#wms02); [McDermott 2007](#mcd07)).

On balance, the most serious consequence of the interpretative viability that has marked the evolution of the literature is the fact that researchers currently defend divergent conceptualizations of communities of practice, to the point where academic gatherings focused on the topic may end up speaking past each other ([Duguid 2008](#dug08)). It is this state of affairs that the review has characterized as a _mid-life crisis_ of the concept.

### Assessing Wenger's 1998 framework

This review considers Wenger's ([1998](#wen98)) theoretical framework to be the most developed model, even as it welcomes the recent appearance of competing models ([Gherardi 2006](#ghe06); [Hara 2009](#har09)). As mentioned in section four, several scholars have critiqued various aspects of the framework, such as the cursory treatment of power issues ([Fox 2000](#fox00); [Contu and Willmott 2003](#con03)), the reification of communities of practice ([Gherardi _et al._ 1998](#ghe98); [Contu and Willmott 2003](#con03); [Cox 2005](#cox05)) and Wenger's shift to a managerialist discourse between his 1991 work with Lave and his 2002 work with McDermott and Snyder ([Contu and Willmott 2003](#con03); [Cox 2005](#cox05); [Hughes 2007](#hug07a)). Still, we would argue that Wenger's framework does give coherent replies to most critiques and that the way forward lies in rigorously testing and refining the framework through fresh empirical studies, especially organizational ethnographies. In this respect, Gherardi's ([2006](#ghe06)) extensive ethnography of a construction site is deemed exemplary for revealing the interaction and discursive practices between three organizational communities of practice, thereby extending Wenger's ([1998](#wen98)) single community study.

While Wenger's framework provides substantial detail, it is not easily operationalized, even with the indicators the author proposed (see [Table 2](#tab2)). Each of the constitutive dimensions of _mutual engagement_, _joint enterprise_ and _shared repertoire_ comprises four or five indicators, which may prove difficult to measure given their ethnographic origin (e.g., Indicators 2, 8 or 13). Moreover, while providing useful guidelines for empirical research, the framework is not exempt from interpretative viability. Indeed, different operationalizations have been proposed (e.g., [Thompson 2005](#tho05); [Hara and Hew 2007](#har07); [Murillo 2008](#mur08)). Still, the 1998 framework does provide coherent criteria for evaluating communities of practice studies and detecting lack of rigour or overenthusiastic claims about showcased organizational communities of practice. In fact, the review used the framework to critique Wenger _et al.'s_ ([2002](#wms02)) characterization as true communities of practice of very large distributed networks of specialists which violate the direct engagement criterion. These should have been coherently described as constellations of practices, with due acknowledgement of the trade-offs that size and lack of face-to-face interaction inflict on engagement ([Wenger 1998: 131](#wen98)).

### Recent theory-grounded studies striving for conceptual clarity

We have detected a welcome trend in the appearance of new concepts to designate deviations from the community of practice model with respect to size, spatial distribution and temporal limits. Brown and Duguid's ([2000b](#bro00b)) concept of network of practice is an important and useful addition to the literature with the specific theoretical justification of explaining knowledge _leakiness_ and with a clear rationale as to how these large networks are different from communities of practice. Several empirical studies have relied on the network of practice concept to examine knowledge sharing in networks which, unlike communities of practice, are characterized by weak links (e.g., [Vaast 2004](#vaa04); [Fleming and Marx 2006](#fle06); [Tagliaventi and Mattarelli 2006](#tag06); [Cox 2007a](#cox07a); [Teigland and Wasko 2009](#tei09); [Wasko _et al._ 2009](#was09)).

Another useful clarification is provided by Lindkvist's ([2005](#lin05)) model of collectivities of practice. Specifically, existing community of practice studies that describe temporary teams and task forces, virtual or not, that display impressive short-term collaboration, without becoming a permanent community, can now be reclassified under the new concept, thereby extracting from the literature one major source of confusion.

More broadly, there is a nascent clarifying trend comprised by authors who have used the concept as a point of departure to build broader typologies of situated learning or situated practice theories. These include Lindkvist's ([2005](#lin05)) typology of knowledge work in groups, Gherardi's ([2006](#ghe06)) theoretical framework of _knowing in practice_ and the typology of modes of _knowing in action_ developed by Amin and Roberts ([2008](#ami08)) .

Finally, a growing number of studies are returning to the Wenger framework and explicitly considering the constitutive dimensions of _mutual engagement_, _joint enterprise_ and _shared repertoire_ (e.g., [Thompson 2005](#tho05); [Goodwin _et al._ 2005](#goo05); [Bryant _et al._ 2005](#bry05); [Iverson and McPhee 2008](#ive08); [Murillo 2008](#mur08); [Wan _et al._ 2008](#wan08); [Schenkel and Teigland 2008](#sch08); [Zhang and Watts 2008](#zha08). Other studies have operationalized different constructs from the 1998 framework, such as _practice, meaning, community_ and _identity_ ([Hara and Hew 2007](#har07)), or _participation, identity_ and _practice_ ([Handley _et al._ 2007](#han07)). These authors display a new willingness to engage with what most acknowledge as the most detailed and coherent framework and use it to study both co-located and virtual communities of practice. These studies mark a new period of theoretically grounded studies and provide some justification for an optimistic outlook regarding future research.

### Conclusion: undiminished academic interest

This review began with the suggestion that the community of practice concept faces a mid-life crisis, based on three telltale signs. First, the increasing ambiguity of the concept, which has resulted in wide diffusion but diluted its meaning. Second, recent critiques and reviews have highlighted the erosion in the concept's coherence and analytical power ([Cox 2005](#cox05); [Roberts 2006](#rob06); [Hughes 2007](#hug07a); [Amin and Roberts 2008](#ami08)). Third, a possible decline in publication trends, which appears fairly established in the practitioner literature, but still incipient in academic journals. A more anecdotal sign of this crisis is recent calls to look '_beyond communities of practice_' ([Handley _et al._ 2006](#han06); [Amin and Roberts 2008](#ami08)).

This review does not join those calls, but it does argue that the time is overdue for developing what could be called _community of practice version 2.0_. Notwithstanding the critiques levelled at Lave and Wenger ([1991](#lav91)), Orr ([1990](#orr90)), [Brown and Duguid (1991](#bro91)) and Wenger ([1998](#wen98)), there are few who seriously doubt the existence of organizational communities of practice. Yet few have embarked on the time-consuming project of conducting rigorous ethnographies within and increasingly across organizations ([Bechky 2006](#bec06)), in order to develop a refined theoretical framework for communities of practice. For a topic that has demonstrated such strong appeal, the dearth of theory-developing empirical studies is remarkable. Hence this review joins others ([Bechky 2006](#bec06); [Roberts 2006](#rob06); [Hughes 2007](#hug07a); [Hara 2009](#har09)) in making a call for theory-grounded research focused on organizational communities of practice that contributes to rigorously test, update and extend Wenger's ([1998](#wen98)) framework, or to propose entirely new frameworks as others have done already.

We have argued that the growth of the community of practice literature during two decades and the appearance of multiple interpretations of the concept, is consistent with Benders and van Veen's ([2001](#ben01)) model of a management fashion. It is in the nature of fashions to fade away, either by becoming outmoded or by becoming mainstream. In the world of practitioner publications, it is the latter course that communities of practice appear to be taking.

Specifically, the observed decline is a symptom of the CoP concept becoming mainstream, an accepted addition to the Management vernacular. Some evidence of this comes from the fact that communities of practice are currently given a cursory mention in the training chapter of mainstream human resource management textbooks (e.g., [Noe _et al._ 2007](#noe07); [Jackson _et al._ 2008](#jac08); [Bohlander and Snell 2009](#boh09)). By this account, communities of practice have become a commodity and no longer newsworthy. Practitioner journals currently seem caught up on the topic of collaboration through Web 2.0 technologies (e.g., [Lynch 2008](#lyn08); [Pace 2009](#pac09); [Lamont 2009](#lam09); [Rosenheck 2010](#ros10)).

Though no longer news, the concept is an important contribution to managerial knowledge. It gives a name to the familiar human need to participate in a group of like-minded peers; many professionals are deliberately seeking connection with competence and identity-defining communities of practice ([Ibarra 2003](#iba03)), for which the new social media offer a promising avenue. Moreover, even as the knowledge management fashion fades, communities of practice have achieved lasting recognition of their role in managing knowledge work ([Newell _et al._ 2002](#new02)).

By contrast, in the more rigorous world of academic journals, the community of practice fashion has turned into a heated theoretical debate, with many researchers, including some of the original proponents ([Duguid 2008](#dug08); [Lave 2008](#lav08)), lamenting the proliferation of instrumentalist managerial interpretations of a concept that in its origin '_was specifically not intended as a normative or prescriptive model for what to do differently or how to create better classrooms or businesses_' ([Lave 2008: 283](#lav08)).

This review assesses the current crisis as a healthy one and predicts publications in academic journals will resume the rising trend of previous years, as a result of strong researcher interest in the topic. This assessment is based on several hopeful signs in the recent literature. First, the appearance of several typologies and in-depth critiques of community of practice theory signals increased researcher interest, coupled with concern about the current state of confusion in the literature. These critiques converge on specific shortcomings of extant theory (e.g., the power differentials between community members and their implications for participation, learning and identity formation), thus setting the stage for developing improved theory which, as previously argued, should be grounded in well-designed organizational studies.

Second, the appearance, for the first time, of developed alternatives to Wenger's ([1998](#wen98)) framework is also regarded as a hopeful sign. Studies based on these new frameworks are just beginning (e.g., [Kaiser _et al._ 2007](#kai07)), but, in time, will contribute to academic publication numbers and drive further theoretical development.

A third positive sign is the growing number of studies that are recovering Wenger's ([1998](#wen98)) framework by attempting various operationalizations of the constitutive dimensions of _mutual engagement_, _joint enterprise_ and _shared repertoire_. Furthermore, these authors display an increased sensibility to the demands of methodological rigour, which is a welcome step forward.

In sum, we draw an optimistic conclusion about the future of community of practice research. This is not to mean the current crisis will soon be left behind, only that researchers' interest and indeed passion about this remarkable concept will remain unabated in the coming years.

## Acknowledgements

This study was made possible through the generous support of Asociacion Mexicana de Cultura, A.C. The author gratefully acknowledges the help of Associate Editor, Dr. Terrence Brooks and the thoughtful suggestions of two anonymous reviewers.


#### References

*   Abrahamson, E. (1996). Management fashion. _Academy of Management Review_, **21**(1), 254-285.
*   Alvesson, M. & Kärreman, D. (2001). Odd couple: making sense of the curious concept of Knowledge Management. _Journal of Management Studies_, **38**(7), 995-1018.
*   Amin, A. & Roberts, J. (2008). Knowing in action: beyond communities of practice. _Research Policy_, **37**(2), 353-369.
*   Anand, N., Gardner, H. K. & Morris, T. (2007). Knowledge-based innovation: emergence and embedding of new practice areas in management consulting firms. _Academy of Management Journal_, **50**(2), 406-428.
*   Ardichvili, A., Page, V. and Wentling, T. (2003). Motivation and barriers to participation in virtual knowledge-sharing communities of practice. _Journal of Knowledge Management_, **7**(1), 64-77.
*   Ardichvili, A., Maurer, M., Li, W., Wentling, T. & Stuedemann, R. (2006). Cultural influences on knowledge sharing through online communities of practice. _Journal of Knowledge Management,_ **10**(1), 94-107.
*   Barab, S. A., MaKinster, J. G. & Scheckler, R. (2003). Designing system dualities: characterizing a web-supported professional development community. _The Information Society_, **19**(3), 237-256.
*   Barrett, M., Cappleman, S., Shoib, G. & Walsham, G. (2004). Learning in knowledge communities: managing technology and context. _European Management Journa_l, **22**(1), 1-11.
*   Barrow, D. C. (2001). Sharing know-how at BP Amoco. _Research Technology Management_, **44**(3), 18-25.
*   <a id="bay00" name="bay00"></a>Baym, N. (2000). _Tune in, log on: soaps, fandom and online community._ Thousand Oaks, CA: Sage Publications.
*   Bechky, B. (2003). Sharing meaning across occupational communities: the transformation of understanding on a production floor. _Organization Science_, **14**(3), 312-330.
*   Bechky, B. (2006). _Talking about machines,_ thick description and knowledge work. _Organization Studies_, **27**(12), 1757-1768.
*   Benders, J. & van Veen, K. (2001). What's in a fashion? Interpretative viability and management fashions. _Organization_, **8**(1), 33-53.
*   Binney, D. (2001). The knowledge management spectrum: understanding the KM landscape. _Journal of Knowledge Management_, **5**(1), 33-42.
*   Bohlander, G. W. & Snell, S. A. (2009). _Managing human resources_, 15th ed. Mason, OH: South-Western.
*   Boland, R. & Tenkasi, R. (1995). Perspective making and perspective taking in communities of knowing. _Organization Science_, **6**(4), 350-372.
*   Brown, J.S. (1998). Internet technology in support of the concept of "communities-of-practice": the case of Xerox. _Accounting, Management and Information Technologies_, **8**(4), 227-236.
*   Brown, J.S. & Duguid, P. (1991). Organizational learning and communities-of-practice: toward a unified view of working, learning and innovation. _Organization Science_, **2**(1), 40-57.
*   Brown, J.S. & Duguid, P. (1996). Universities in the digital age. _Change_, **28**(4), 10-19.
*   Brown, J.S. & Duguid, P. (1998). Organizing knowledge. _California Management Review_, **40**(3), 90-111.
*   Brown, J.S. & Duguid, P. (2000a). Balancing act: how to capture knowledge without killing it. _Harvard Business Review_, **78**(3), 73-80.
*   Brown, J.S. & Duguid, P. (2000b). Mysteries of the region. In W. F. Miller, et al (Eds.), _The Silicon Valley edge_ (pp. 16-39). Palo Alto, CA: Stanford University.
*   Brown, J.S. & Duguid, P. (2000c). _The social life of information_. Boston, MA: Harvard Business School.
*   Brown, J.S. & Duguid, P. (2001). Knowledge and organization: a social-practice perspective. _Organization Science_, **12**(2), 198-213.
*   Brown, J.S. & Gray, E. S. (1995). The people are the company. _Fast Company_, **1**(1), 78-82.
*   Bryant, S. L., Forte, A. & Bruckman, A. (2005). Becoming Wikipedian: transformation of participation in a collaborative online encyclopaedia. In K. Schmidt, M. Pendergast, M. Ackerman & G. Mark (Eds.), _Proceedings of GROUP International Conference on Supporting Group Work_ (pp. 11-20). New York: ACM Press.
*   Bobrow, D. & Whalen, J. (2002). Community knowledge sharing in practice: the Eureka story. _Reflections_, **4**(2), 47-59.
*   B?chel, B. & Raub, S. (2002). Building knowledge-creating value networks. _European Management Journal_, **20**(6), 587-596.
*   Campbell, M. (2009). Intersection of trajectories: a newcomer in a community of practice. _Journal of Workplace Learning_, **21**(8), 647-657.
*   Carlile, P. R. (2004). Transferring, translating and transforming: an integrative framework for managing knowledge across boundaries. _Organization Science_, **15**(5), 555-568.
*   Chua, A. Y. K. (2006). The rise and fall of a community of practice: a descriptive case study. _Knowledge and Process Management_, **13**(2), 120-128.
*   Constant, E. W. (1980). _The origins of the turbojet revolution_. Baltimore, MD: John Hopkins University.
*   Contu, A. and Willmot, H. (2000). Comment on Wenger and Yanow. Knowing in practice: a 'delicate flower' in the organizational learning field. _Organization_, **7**(2), 269-276.
*   Contu, A. & Willmot, H. (2003). Re-embedding situatedness: the importance of power relations in learning theory. _Organization Science_, **14**(3), 283-296.
*   Contu, A. & Willmot, H. (2006). Studying practice: situating _Talking about machines._ _Organization Studies_, **27**(12), 1769-1782.
*   Cook, S. & Brown, J. S. (1999). Bridging epistemologies: the generative dance between organizational knowledge and organizational knowing. _Organization Science_, **10**(4), 381-400.
*   Cook, S. & Yanow, D. (1993). Culture and organizational learning. _Journal of Management Inquiry_, **2**(4), 373-390.
*   Cox, A.M. (2005). What are communities of practice? A comparative review of four seminal works. _Journal of Information Science_, **31**(6), 527-540.
*   Cox, A.M. (2007a). Beyond information: factors in participation in networks of practice, a case study of web management in UK higher education. _Journal of Documentation_, **63**(5), 765-787.
*   Cox, A.M. (2007b). Reproducing knowledge: Xerox and the story of knowledge management. _Knowledge Management Research & Practice_, **5**(1), 3-12.
*   Cox, A.M. (2008). An exploration of concepts of community through a case study of UK university Web production. _Journal of Information Science_, **34**(3), 327-345.
*   Cross, R., Laseter, T., Parker, A. & Velasquez, G. (2006). Using social network analysis to improve communities of practice. _California Management Review_, **49**(1), 32-60.
*   Davenport, E. (2004). Double agents: visible and invisible work in an online community of practice. In P. Hildreth & C. Kimble (Eds.), _Knowledge networks: innovation through communities of practice_ (pp. 256-266). London: Idea Group Publishing.
*   Davenport, E. & Hall, H. (2002). Organizational knowledge and communities of practice. _Annual Review of Information Science and Technology_, **36**, 145-186.
*   Davenport, T. & Prusak, L. (1998). _Working knowledge_. Boston: Harvard Business School.
*   Despres, C. & Chauvel, D. (2000). A thematic analysis of the thinking in knowledge management. In C. Despres & D. Chauvel (Eds.), _Knowledge horizons: the present and promise of knowledge management_ (pp. 55-86). Woburn, MA: Butterworth-Heinneman.
*   Dub?, L., Bourhis, A. & Jacob, R. (2005). The impact of structuring characteristics on the launching of virtual communities of practice. _Journal of Organizational Change Management_, **18**(2), 145-166.
*   Duguid, P. (2005). 'The art of knowing': social and tacit dimensions of knowledge and the limits of the community of practice. _The Information Society_, **21**(2), 109-118.
*   Duguid, P. (2006). What _Talking about machines_ tells us. _Organization Studies_, **27**(12), 1794-1804.
*   Duguid, P. (2008) Prologue: community of practice then and now. In A. Amin & J. Roberts (Eds.), _Community, economic creativity and organization_, (pp. 1-10). Oxford: Oxford University Press.
*   Earl, M. (2001). Knowledge management strategies: toward a taxonomy. _Journal of Management Information Systems_, **18**(1), 215-233.
*   Fahey, R., Vasconcelos, A.C. & Ellis, D. (2007). The impact of rewards within communities of practice: a study of the SAP online global community. _Knowledge Management Research & Practice_, **5**(3), 186-198.
*   Fang, Y. & Neufeld, D. (2009). Understanding sustained participation in open source software projects. _Journal of Management Information Systems_, **25**(4), 9-50.
*   Faraj, S. & Xiao, Y. (2006). Coordination in fast-response organizations. _Management Science_, **52**(8), 1155-1169.
*   Ferlie, E., Fitzgerald, L., Wood, M. & Hawkins, C. (2005). The nonspread of innovations: the mediating role of professionals. _Academy of Management Journal_, **48**(1), 117-134.
*   Fleming, L. & Marx, M. (2006). Managing creativity in small worlds. _California Management Review_, **48**(4), 6-27.
*   Fox, S. (2000). Communities of practice: Foucault and actor-network theory. _Journal of Management Studies_, **37**(6), 853-867.
*   Fontaine, M.A. & Millen, D. R. (2004). Understanding the benefits and impact of communities of practice. In P. Hildreth & C. Kimble (Eds.), _Knowledge networks: innovation through communities of practice_ (pp. 1-13). London: Idea Group Publishing.
*   Fuller, A. & Unwin, L. (2004). Young people as teachers and learners in the workplace: challenging the novice-expert dichotomy. _International Journal of Training and Development_, **8**(1), 32-42.
*   Gherardi, S. (2006). _Organizational knowledge: the texture of workplace learning._ Oxford: Blackwell.
*   Gherardi, S. (2009a). Introduction to special issue: the critical power of the 'Practice Lens'. _Management Learning_, **40**(2), 115-128.
*   Gherardi, S. (2009b). Knowing and learning in practice-based studies: an introduction. _The Learning Organization_, **16**(5), 352-359.
*   Gherardi, S. & Nicolini, D. (2000). The organizational learning of safety in communities of practice. _Journal of Management Inquiry_, **9**(1), 7-18.
*   Gherardi, S. & Nicolini, D. (2002). Learning the trade: a culture of safety in practice. _Organization_, **9**(2), 191-223.
*   Gherardi, S., Nicolini, D. & Odella, F. (1998). Toward a social understanding of how people learn in organizations. _Management Learning_, **29**(3), 273-297.
*   Gongla, P. & Rizzuto, C.R. (2004). Where did that community go? Communities of practice that "disappear". In P. Hildreth & C. Kimble (Eds.), _Knowledge networks: innovation through communities of practice_ (pp. 295-307). London: Idea Group Publishing.
*   Goodwin, D., Pope, C., Mort, M. & Smith, A. (2005). Access, boundaries and their effects: legitimate participation in anaesthesia. _Sociology of Health & Illness_, **27**(6), 855-871.
*   Graham, W., Osgood, D. & Karren, J. (1998). A real-life community of practice. _Training & Development_, **52**(5), 34-38.
*   Gray, P. H. & Meister, D. B. (2003). Introduction: fragmentation and integration in knowledge management research. _Information Technology & People_, **16**(3), 259-265.
*   Grover, V. & Davenport, T. H. (2001). General perspectives on knowledge management: fostering a research agenda. _Journal of Management Information Systems_, **18**(1), 5-21.
*   Handley, K., Clark, T., Finchman, R. & Sturdy, A. (2007). Researching situated learning: participation, identity and practices in client-consultant relationships. _Management Learning_, **38**(2), 173-191.
*   Hanley, S. (1998). Knowledge-based communities of practice at AMS. _Knowledge Management Review_, **1**(3), 8-9.
*   Hansen, M.T., Nohria, N. and Tierney, T. (1999). What's your strategy for managing knowledge? _Harvard Business Review_, **77**(2), 106-116.
*   Hara, N. (2009). _Communities of practice: fostering peer-to-peer learning and informal knowledge sharing in the workplace._ Berlin: Springer-Verlag.
*   Hara, N. & Hew, K. F. (2007). Knowledge-sharing in an online community of health-care professionals. _Information Technology & People_, **20**(3), 235-261.
*   Hara, N. & Schwen, T. M. (2006). Communities of practice in workplaces: learning as a naturally occurring event. _Performance Improvement Quarterly_, **19**(2), 93-114.
*   Harris, R., Simons, M. & Carden, P. (2004). Peripheral journeys: learning and acceptance of probationary constables. _Journal of Workplace Learning_, **16**(4), 205-218.
*   Hendry, C. (1996). Understanding and creating whole organizational change through learning theory. _Human relations_, **49**(5), 621-641.
*   Hislop, D. (2003). The complex relations between communities of practice and the implementation of technological innovations. _International Journal of Innovation Management_, **7**(2), 163-188.
*   Holmqvist, M. (2003). A dynamic model of intra- and interorganizational learning. _Organization Studies_, **24**(1), 95-123.
*   Hughes, J. (2007). Lost in translation: communities of practice: the journey from academic model to practitioner tool. In J. Hughes, N. Jewson & L. Unwin (Eds.), _Communities of practice: critical perspectives_ (pp. 30-40). London: Routledge.
*   Hughes, J., Jewson, N. & Unwin, L. (2007). Communities of practice: a contested concept in flux. In J. Hughes, N. Jewson & L. Unwin (Eds.), _Communities of practice: critical perspectives_ . (pp. 1-16). London: Routledge.
*   Ibarra, H. (2003). _Working identity: unconventional strategies for reinventing your career_. Boston, MA: Harvard Business School.
*   Iverson, J.O. & McPhee, R.D. (2008). Communicating knowing through communities of practice: exploring internal communicative processes and differences among communities of practice. _Journal of Applied Communication Research_, **36**(2), 176-199.
*   Jackson, S. E., Schuler, R. S. & Werner, S. (2008). _Managing human resources_. 10th ed. Mason, OH: South-Western.
*   Johnson, C. (2001). A survey of current research on online communities of practice. _The Internet and Higher Education_, **4**(1), 45-60.
*   Kakabadse, N., Kakabadse, A. & Kouzmin, A. (2003). Reviewing the knowledge management literature: towards a taxonomy. _Journal of Knowledge Management_, **7**(4), 75-91.
*   Kaiser, S., M?ller-Seitz, G., Pereira Lopes, M. & Pina e Cunha, M. (2007). Weblog-technology as a trigger to elicit passion for knowledge. _Organization_, **14**(3), 391-412.
*   Kimble, C. & Bourdon, I. (2008). Some success factors for the communal management of knowledge. _International Journal of Information Management_, **28**(6), 461-467.
*   Knorr Cetina, K. (1999). _Epistemic cultures: how the sciences make knowledge_. Boston, MA: Harvard Business School.
*   Kogut, B. & Zander, U. (1996). What firms do? Coordination, identity and learning. _Organization Science_, **7**(5), 502-518.
*   Koliba, C. & Gajda, R. (2009). 'Communities of practice' as an analytical construct: implications for theory and practice. _International Journal of Public Administration_, **32**(1), 97-135.
*   Korczynski, M. (2003). Communities of coping: collective emotional labour in service work. _Organization_, **10**(1), 55-79.
*   Lamont, J. (2009). Managing the Web 2.0 life cycle. _KM World_, **18**(7), 14-22.
*   LaPlante, A. (1996). John Seely Brown: of mice and zen. _Computerworld_, **30**(46), S11-S12.
*   Lave, J. & Wenger, E. (1991). _Situated learning: legitimate peripheral participation_. Cambridge: Cambridge University Press.
*   Lave, J. (2008). Epilogue: situated learning and changing practice. In A. Amin & J. Roberts (Eds.), _Community, economic creativity and organization_ (pp. 283-296). Oxford: Oxford University Press.
*   Lee, G. K. & Cole, R. E. (2003). From a firm-based to a community-based model of knowledge creation: the case of the Linux kernel development. _Organization Science_, **14**(6), 633-649.
*   Lesser, E. & Everest, K. (2001). Using communities of practice to manage intellectual capital. _Ivey Business Journal_, **65**(4), 37-41.
*   Lesser, E. L. & Storck, J. (2001). Communities of practice and organizational performance. _IBM Systems Journal_, **40**(4), 831-841.
*   Lindkvist, L. (2005). Knowledge communities and knowledge collectivities: a typology of knowledge work in groups. _Journal of Management Studies_, **42**(6), 1189-1210.
*   Lloria, M. B. (2008). A review of the main approaches to knowledge management. _Knowledge Management Research & Practice_, **6**(1), 77-89.
*   Lynch, C. G. (2008). Enterprise Web 2.0 adoption still growing. _CIO_, **21**(23), 17.
*   Manville, B. & Foote, N. (1996). Harvest your workers' knowledge. _Datamation_, **42**(13), 78-81.
*   Marshall, N. & Rollinson, J. (2004). Maybe Bacon had a point: the politics of interpretation in collective sensemaking. _British Journal of Management_, **15**(S1), S71-S86.
*   McDermott, R. (1999). Why information technology inspired but cannot deliver knowledge management. _California Management Review_, **41**(4), 103-117.
*   McDermott, R. (2000). Knowing in community: ten critical success factors in building communities of practice. _IHRIM Journal_, **4**(1), 19-26.
*   McDermott, R. (2007). Building healthy communities. _Inside Knowledge_, **10**(9), 14-19.
*   McDermott, R. & Kendrick, J. (2000). How learning communities steward knowledge: Shell Oil Company. In P. Harkins, L.L. Carter & A.J. Timmins (Eds.), _Best practices in knowledge management and organizational learning handbook_ (pp. 194-226). Lexington, MA: Linkage Incorporated.
*   Meeuwesen, B. & Berends, H. (2007). Creating communities of practices to manage technological knowledge: an evaluation study at Rolls-Royce. _European Journal of Innovation Management_, **10**(3), 333-347.
*   Moran, J. & Weimer, L. (2004). Creating a multi-company community of practice for Chief Information Officers. In P. Hildreth & C. Kimble (Eds.), _Knowledge networks: innovation through communities of practice_ (pp. 125-132). London: Idea Group Publishing.
*   Murillo, E. (2008). [Searching Usenet for virtual communities of practice: using mixed methods to identify the constructs of Wenger's theory](http://www.webcitation.org/5wprX82C8). _Information Research_, **13**(4), paper 386\. Retrieved28 February, 2011 from http://InformationR.net/ir/13-4/paper386.html (Archived by WebCite at http://www.webcitation.org/5wprX82C8)
*   M?rk, B.E., Aanestad, M., Hanseth, O. & Grisot, M. (2008). Conflicting epistemic cultures and obstacles for learning across communities of practice. _Knowledge and Process Management_, **15**(1), 12-23.
*   Newell, S., Robertson, M., Scarbrough, H. & Swan, J. (2002). _Managing knowledge work_. Basingstoke, UK: Palgrave McMillan.
*   Noe, R.A., Hollenbeck, J.R., Gerhart, B. & Wright, P.M. (2007) _Human resource management_. 6th ed. New York, NY: McGraw-Hill.
*   Orr, J.E. (1990). Sharing knowledge, celebrating identity: community memory in a service culture. In R. Middleton & D. Edwards (Eds.), _Collective remembering: memory in society_. (pp. 169-189). London: Sage Publications.
*   Orr, J. E. (1996). _Talking about machines: an ethnography of a modern job_. Ithaca, NY: Cornell University Press.
*   Pace, A. (2009). Web 2.0 in the driver's seat. _T+D_, **63**(11), 20-20.
*   Pan, S.L. & Leidner, D.E. (2003). Bridging communities of practice with information technology in pursuit of global knowledge sharing. _Journal of Strategic Information Systems_, **12**(1), 71-88.
*   Pastoors, K. (2007). Consultants: love-hate relationships with communities of practice. _The Learning Organization_, **14**(1), 21-33.
*   Plaskoff, J. (2003). Intersubjectivity and community building: learning to learn organizationally. In M. Easterby-Smith & M.A. Lyles (Eds.), _The Blackwell handbook of organizational learning and knowledge management_, (pp.161-184). Oxford: Blackwell.
*   Ponzi, L. & Koenig, M. (2002). [Knowledge management: another management fad?](http://www.webcitation.org/5wpro56QJ) _Information Research_, **8**(1), paper no. 145\. Retrieved 28 February, 2011 from http://InformationR.net/ir/8-1/paper145.html (Archived by WebCite at http://www.webcitation.org/5wpro56QJ)
*   Probst, G. & Borzillo, S. (2008). Why communities of practice succeed and why they fail. _European Management Journal_, **26**(5), 335-347.
*   Prokesch, S. E. (1997). Unleashing the power of learning: an interview with British Petroleum's John Browne. _Harvard Business Review_, **75**(5), 146-168.
*   Raz, A. E. (2007). Communities of practice or communities of coping? Employee compliance among CSRs in Israeli call centres. _The Learning Organization_, **14**(4), 375-387.
*   Raelin, J. (1997). A model of work-based learning. _Organization Science_, **8**(6), 563-578.
*   Roberts, J. (2006). Limits to communities of practice. _Journal of Management Studies_, **43**(3), 623-639.
*   Robey, D., Khoo, H.M. & Powers, C. (2000). Situated learning in cross-functional virtual teams. _Technical Communication_, **47**(1), 51-66.
*   Rogers, J. (2000). Communities of practice: a framework for fostering coherence in virtual learning communities. _Educational Technology & Society_, **3**(3), 384-392.
*   Rosenheck, M. (2010). Navigating the interactive workplace. _Chief Learning Officer_, **9**(5), 18-21.
*   Roth, A.V. (1996). Achieving strategic agility through economies of knowledge. _Strategy & Leadership_, **24**(2), 30-37
*   Saint-Onge, H. & Wallace, D. (2003). _Leveraging communities of practice for strategic advantage_. Boston, MA: Butterworth-Heinemann.
*   Scarbrough, H. (2003). Knowledge management, HRM and the innovation process. _International Journal of Manpower_, **24**(5), 501-516.
*   Schenkel, A. & Teigland, R. (2008). Improved organizational performance through communities of practice. _Journal of Knowledge Management_, **12**(1), 106-118.
*   Schlager, M., Fusco, J. & Schank, P. (2002). Evolution of an on-line education community of practice. In K. A. Renninger & W. Shumar (Eds.), _Building virtual communities: learning and change in cyberspace_ (pp. 129-158). New York, NY: Cambridge University Press.
*   Schwen, T.M. & Hara, N. (2003). Community of practice: a metaphor for online design? _The Information Society_, **19**(3), 257-270.
*   Silva, L., Goel, L. & Mousavidin, E. (2008). Exploring the dynamics of blog communities: the case of MetaFilter. _Information Systems Journal_, **19**(1), 55-81.
*   Smeds, R. & Alvesalo, J. (2003). Global business process development in a virtual community of practice. _Production Planning & Control_, **14**(4), 361-371.
*   Spender, J.-C. & Grant, R. (1996). Knowledge and the firm: overview. _Strategic Management Journal_, **17**(1), 5-9.
*   Stucky, S. U. & Brown, J. S. (1996). Leveraging learning. _Across the Board - The Conference Board Magazine_, **33**(3), 22-24.
*   Stamps, D. (1997). Communities of practice: learning is social, training is irrelevant? _Training_, **34**(2), 34-42.
*   Stewart, T.A. (1996). The invisible key to success. _Fortune_, **134**(3), 173-176.
*   Stewart, T.A. (2000). Knowledge worth $1.25 billion. _Fortune_, **142**(13), 302-304.
*   Stork, J. & Hill, P. (2000). Knowledge diffusion through "strategic communities". _MIT Sloan Management Review_, **41**(2), 63-74.
*   Stucky, S. & Brown, J. S. (1996). Leveraging learning. _Across the Board - The Conference Board Magazine_. **33**(3), 22-24.
*   Swan, J., Bresnen, M., Newell, S. & Robertson, M. (2007). The object of knowledge: the role of objects in biomedical innovation. _Human Relations_, **60**(12), 1809-1837.
*   Swan, J., Newell, S. & Robertson, M. (1999). National differences in the diffusion and design of technological innovation: the role of inter-organizational networks. _British Journal of Management_, **10**(Supplement S1), S45-S59
*   Swan, J., Newell, S., Scarbrough, H. & Hislop, D. (1999). Knowledge management and innovation: networks and networking. _Journal of Knowledge Management_, **3**(4), 262-275.
*   Swan, J., Scarbrough, H. & Robertson, M. (2002). The construction of 'communities of practice' in the management of innovation. _Management Learning_, **33**(4), 477-496.
*   Taber, N., Plumb, D. & Jolemore, S. (2008). "Grey" areas and "organized chaos" in emergency response. _Journal of Workplace Learning_, **20**(4), 272-285.
*   Teigland, R. (2003). _[Knowledge networking: structure and performance in networks of practice](http://www.webcitation.org/5wptXdrb1)_. Stockholm: Institute of International Business. [Stockholm School of Economics Ph.D. dissertation] Retrieved 28 February, 2011 from http://www.hhs.se/CSC/Publications/Documents/Teigland%20thesis-Knowledge%20Networking.pdf (Archived by WebCite? at http://www.webcitation.org/5wptXdrb1)
*   Teigland, R. & Wasko, M. (2009). Knowledge transfer in MNCs: examining how intrinsic motivations and knowledge sourcing impact individual centrality and performance. _Journal of International Management_, **15**, 15-31.
*   Thompson, M. (2005). Structural and epistemic parameters in communities of practice. _Organization Science_, **16**(2), 151-164.
*   Thompson, M. & Walsham, G. (2004). Placing knowledge management in context. _Journal of Management Studies_, **41**(5), 725-747.
*   Tagliaventi, M.R. & Mattarelli, E. (2006). The role of networks of practice, value sharing and operational proximity in knowledge flows between professional groups. _Human Relations_, **59**(3), 291-319.
*   Trice, H.M., (1993). _Occupational subcultures in the workplace_. Ithaca, NY: ILR Press.
*   Tsoukas, H. & Vladimirou, E. (2001). What is organizational knowledge? _Journal of Management Studies_, **38**(7), 973-993.
*   Usoro, A., Sharratt, M.W., Tsui, E. & Shekhar, S. (2007). Trust as an antecedent to knowledge sharing in virtual communities of practice. _Knowledge Management Research & Practice_, **5**(3), 199-212.
*   Vaast, E. (2004). O brother, where are thou? From communities to networks of practice through intranet use. _Management Communication Quarterly_, **18**(1), 5-44.
*   van Maanen, J. and Barley, S.R. (1984). Occupational communities: culture and control in organizations. _Research in organizational behavior_, **6**, 287-365.
*   von Krogh, G., Nonaka, I. & Aben, M. (2001). Making the most of your company's knowledge: a strategic framework. _Long Range Planning_, **34**(4), 421-439.
*   Verburg, R. M. & Andriessen, J. H. E. (2006). The assessment of communities of practice. _Knowledge and Process Management_, **13**(1), 13-25.
*   Wan, Z., Fang, Y. & Neufeld, D. J. (2008). Individual learning and performance in communities of practice. In _HICSS '08 Proceedings of the Proceedings of the 41st Annual Hawaii International Conference on System Sciences_, (p. 338). Washington, DC: IEEE Computer Society
*   Ward, A. (2000). Getting strategic value from constellations of communities. _Strategy & Leadership_, **28**(2), 4-9.
*   Wasko, M. & Faraj, S. (2000). It is what one does: why people participate and help others in electronic communities of practice. _Journal of Strategic Information Systems_, **9**(2-3), 155-173.
*   Wasko, M. & Teigland, R. (2004). Public goods or virtual commons? Applying theories of public goods, social dilemmas and collective action to electronic networks of practice. _Journal of Information Technology Theory and Applications_, **6**(1), 25-41.
*   Wasko, M., Teigland, R. & Faraj, S. (2009). The provision of online public goods: examining social structure in an electronic network of practice. _Decision Support Systems_, **47**(3), 254-265.
*   Wenger, E. (1996). Communities of practice: the social fabric of a learning organization. _Healthcare Forum Journal_, **39**(4), 20-26.
*   Wenger, E. (1998). _Communities of practice: learning, meaning and identity_. Cambridge: Cambridge University Press.
*   Wenger, E. (1999). Communities of practice: the key to knowledge strategy. _Knowledge Directions_, **1**(2), 48-63.
*   Wenger, E. (2000a). Communities of practice: the structure of knowledge stewarding. In C. Despres & D. Chauvel (Eds.), _Knowledge horizons: the present and promise of knowledge management_, (pp. 205-225). Woburn, MA: Butterworth-Heinneman.
*   Wenger, E. (2000b). Communities of practice and social learning systems. _Organization_, **7**(2), 225-246.
*   Wenger, E. (2002). Communities of practice. In _Encyclopedia of the social sciences_. (Vol. 1.5, Article 5). Amsterdam: Elsevier Science.
*   Wenger, E. (2004). Knowledge management as a doughnut: shaping your knowledge strategy through communities of practice. _Ivey Business Journal_, **68**(3), 1-8.
*   Wenger, E. (2005). _[Learning for a small planet: a research agenda](http://www.webcitation.org/5wpsSSCOs)_. Retrieved 28 February, 2011 from http://ewenger.com/research/LSPfoundingdoc.doc (Archived by WebCite at http://www.webcitation.org/5wpsSSCOs)
*   Wenger, E. & Snyder, W. M. (2000). Communities of practice: the organizational frontier. _Harvard Business Review_, **78**(1), 139-145.
*   Wenger, E., McDermott, R. & Snyder, W. M. (2002). _Cultivating communities of practice: a guide to managing knowledge_. Boston, MA: Harvard Business School.
*   Wilson, T.D. (2002). [The nonsense of 'knowledge management'](http://www.webcitation.org/5wpsIkQqA). _Information Research_, **8**(1), paper no. 144\. Retrieved 28 February, 2011 from http://InformationR.net/ir/8-1/paper144.html (Archived by WebCite at http://www.webcitation.org/5wpsIkQqA)
*   Wright, P. (1999). Leveraging frontline knowledge for smarter service. _Knowledge Management Review_, **1**(6), 4-5.
*   Zhang, W. & Watts, S. (2008). Online communities as communities of practice: a case study. _Journal of Knowledge Management_, **12**(4), 55-71.