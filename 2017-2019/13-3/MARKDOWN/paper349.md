#### vol. 13 no. 3, September, 2008

# Information work analysis: an approach to research on information interactions and information behaviour in context

#### [Isto Huvila](mailto:isto.huvila(a)abo.fi)  
Information Studies, Abo Akademi University, Tavastgatan 13, FI-20500 Abo, Finland

#### Abstract

> **Introduction.** A work roles and role theory-based approach to conceptualise human information activity, denoted information work analysis is discussed. The present article explicates the approach and its special characteristics and benefits in comparison to earlier methods of analysing human information work.  
> **Method.** The approach is discussed in the light of the results of an empirically-based qualitative investigation of the information work of Finnish and Swedish archaeology professionals. The material was gathered in a series of twenty-five thematic interviews, which comprised a variety of different interview approaches.  
> **Analysis.** The data were analysed by using a grounded theory approach, which comprised iterative writing, schema analysis and validity checks.  
> **Results.** Information work analysis is an approach that explicitly focuses on information and work, from an information management point of view and perceives the two as social and cultural, instead of merely cognitive, issues. Unlike the information technology-oriented analytical frameworks, information work analysis aims at improving information work as a comprehensive enterprise instead of merely looking at how to develop a computerised information system.  
> **Conclusions.** Information work analysis provides a description and understanding of real world phenomena, but it is also an instrument, which aims at providing necessary premises for changing and developing the present state of affairs.


## Introduction

The number of theories and approaches in information behaviour research ([Fisher _et al._2005](#Fisher2005)) can be seen as a symptom of the fact that one of the major challenges of information seeking and use studies is how to conceptualise human information activity. The most typical approaches have been to look at individual information behaviour or at information activities in occupational groups and task-based studies of information seeking and use (e.g., [Wiberley and Jones 1989](#Wiberley1989); [Ellis 1993](#Ellis1993); [Byström 1997](#Bystrom1997); [Sonnenwald and Lievrouw 1997](#Sonnenwald1997); [Widén-Wulff 2000](#Widenwulff2000); [Hansen and Järvelin 2000](#Hansen2000); [Talja 2002](#Talja2002); [Heinström 2002](#Heinstrom2002); [Vakkari 2003](#Vakkari2003); and [Case 2007](#Case2007)).

Yet another way of explicating the contexts of information activities is to scrutinise work and work roles. This approach has been more common in information systems studies and organization science ([Clifford 1996](#Clifford1996)), though it is also occasionally cited in information science research (e.g., [Leckie 1996](#Leckie1996) [Sonnenwald and Lievrouw 1997](#Sonnenwald1997); [Wilkinson 2001](#Wilkinson2001); [Chaplan and Hertenstein 2005](#Chaplan2005); [Fidel _et al._ 2004](#Fidel2004); [Fidel and Pejtersen 2004](#Fidel2004a); and [Landry 2006](#Landry2006)).

The present article discusses a work roles and role theory-based approach to conceptualise human information activity originally proposed by Huvila in 2006 ([Huvila 2006](#Huvila2006)). The approach, which is compounded of four individual analytical steps, is denoted _information work analysis_. Information work analysis focuses on scrutinising integral top-level processes, which pertain to work in a domain ([Hjørland and Albrechtsen 1995](#Hjorland1995)), instead of individual work tasks and their functioning, or activity of individuals or confined occupational groups.

## Theoretical background

The theoretical framework of the analytical viewpoint referred to as _information work analysis_ is based on interactional role theory ([Turner 2001](#Turner2001); as opposed to the classical role theory of [Biddle and Thomas 1966](#Biddle1966); [Biddle 1986](#Biddle1986); [Goffman 1959](#Goffman1959); [Goffman 1972](#Goffman1972)), systemic ([Checkland 2000](#Checkland2000)) and ecological approaches ([Gibson 1979](#Gibson1979)), faceted classification of information interactions ([Cool and Belkin 2002](#Cool2002)) and the concept of information horizons ( [Sonnenwald _et al._ 2001](#Sonnenwald2001)).

The approach acknowledges the relevance of cognitive work analysis as a related framework. The two approaches differ in that information work analysis is _information_ activity and information management-oriented (as opposed to work and information systems development-oriented), it focuses on information activity (instead of on cognition and work), it is emphatically socio-cultural (as opposed to cognitive) and it is less focused on the Gibsonian ecological constraints identified by Gibson ([1979](#Gibson1979)) and adopted by Rasmussen _et al._ ([1994](#Rasmussen1994)) and by Pejtersen and Rasmussen ([2004](#Pejtersen2004)). Information work analysis is concerned with _information work_ and sees _work_ as one of its contexts.

Information work analysis examines human activity from an information management perspective, which brings to the foreground the idea of consciously contriving change in work and its infrastructures by focusing on their related information processes ([Huvila 2006](#Huvila2006)). Management does not necessarily denote a total state of control, but a capability to steer, guide and anticipate synergies and dissonances in the interactions between humans and information. Information is seen as a qualitative cultural concept which manifests as a contextual property and as a practical matter which is a part of the everyday life and work ([Huvila 2006](#Huvila2006); see also [Langlois 1982](#Langlois1982) and [Bates 2005](#Bates2005)).

The centrepiece of the landscape of information and work in information work analysis is a human actor, who has assumed one or several work roles. _Work role_ is a distinct set of activities within _work_, just as _work_ is a distinct set of activities within the entirety of human endeavour ([Star and Strauss 1999](#Star1999)). From the information viewpoint, all work comprises the processing of information, i.e., it is information work. The precise patterns of information work are dependent on the actor, his/her personal _information behaviour_ ([Wilson 2000](#Wilson2000)), the assumed _work role_, the available information (embodied in his/her information horizon and the context and situation of the activity ([Huvila 2006](#Huvila2006)).

Information work analysis emphasises the contextual, situated and evolutionary characteristics of information and human activity. The viewpoint is anthropological and it builds on a sociocultural understanding of the soft systems theory and Gibsonian ecological approach ([Huvila 2006](#Huvila2006)). The rationale of the approach is to be able to focus the analysis of information interactions on a meaningful level of understanding, where the interactions may be perused in a reasonable level of abstraction, yet to retain at same time a picture of the fundamental purposes, meanings and values that guide the activity.

The discussion of the information work analysis approach is based on an earlier study by the author. The investigation is a qualitative study of information work in the archaeology sector. The twenty-five participants from Finland and Sweden were interviewed by using a thematic interview approach ([Hirsjärvi and Hurme 1995](#Hirsjarvi1995)), which combined several interview methods ([Huvila 2006](#Huvila2006): 93-104). The case study has been published in its entirety ([Huvila 2006](#Huvila2006)).

## Work roles and role theory

Role theory has been acknowledged to be a viable instrument for understanding the actions of professionals in their work ([Leckie and Pettigrew 1997](#Leckie1997): 109; [Nurminen and Torvinen 1996](#Nurminen1996)). In the present study the concept of work role is used to refer to a distinct set of activities within work, just as work is a distinct set of activities in a broader scope of the human life-world ([Huvila 2006](#Huvila2006), [Strauss 1985](#Strauss1985) and [Star and Strauss 1999](#Star1999)). Following Clifford, the present study considers a role as a concept with both abstract and tangible properties, but not as a theory ([Clifford 1996](#Clifford1996)). A work role is not a job description and it does not reflect directly any existing organization of work or the manner in which that organization is perceived by workers or their superiors. Work role is an analytical concept like the concept of work.

The perspective of role theory which is assumed in the present study follows the position taken within organizational theory, gender studies and, with a special reference to group roles, cognitive psychology ([Campbell 1999](#Campbell1999)). The assumed view of roles and role theory acknowledges the critique of classical role theory, which carries a tendency to externalise roles of their actors ([Davies and Harré 1998](#Davies1998): 52; [Layder 2006](#Davies1998)) and aligns itself with interactional role theory, which emphasises the dynamics and vagueness of roles ([Turner 2001](#Turner2001)).

Roles are considered to be the results of socialisation and contextual division of duties between abstract archetypal actors, rather than between distinct human individuals. Therefore, it is possible to identify individuals who are related to the different roles, but impossible to make any static assumptions that an individual is acting exclusively in a precise role. Traditional information science research has acknowledged that individuals have multiple memberships in different information behavioural classes or groups, but have generally focused on a single membership (see for example [Taylor 1991](#Taylor1991) or [Case 2007](#Case2007)). In spite of its partial formality, a work role is not a static entity. It incorporates the notions of motivation, meaning and value of the body of the recurring and recognisable activities in addition to the activities themselves (cf. genre in [Spinuzzi 2003](#Spinuzzi2003): 119-120, 222).

The primary implication of referring to work roles as conceptual relations is the possibility of linking together work and workers . In the present study work roles are a conceptual instrument for explicating and identifying different locations where work and more precisely information work, reside within the work processes.

To overcome the limitation of precision and imprecision of the role-based approaches, the roles are deconstructed further in the present study by using the concepts of _information work_, _use cases_ and the _faceted classification of information interactions_ as a framework for scrutinising the work roles on a more detailed level. In spite of closer dissection, the information work analysis consciously retains the focus on the level of work and work roles to maintain the emphasis on complete and meaningful processes instead of individual isolated actions.

## Information work analysis in practice

Information work analysis perceives work as a _soft system_ ([Checkland 1981](#Checkland1981)), which comprises resources, conventions, structures, infrastructures and actors who participate in different work roles. The system, like its participants, has an individual sense of the common and personal purposes, meanings and values. It is ecological in the sense that it is evolving and it is situated and framed within, and by, its surroundings. This system is explored on two levels: the level of work itself and the information component referred to as information work.

In information work analysis the systemic processes are explored using a four step approach, which consists of the following:

1.  actor-context analysis;
2.  process task analysis;
3.  information interactions assessment and classification; and
4.  description of the work-role-specific information landscape.

The four step approach of information work analysis allows a deep scrutiny of work and information work because of its scope, which ranges from a domain level analysis to the explication of very specific activities and motivations. The approach also makes it possible to analyse the primary actors, the qualities and the motivations of the work. The first two steps help to identify the organization and the structures that reside in the work. The next two steps are used to explicate the ways in which an organization comes into the being and how the system functions in the practical information interactions and behaviour of the participating actors.

## Preparatory work

### Data gathering

In the case studies, the empirical data were collected by using an adapted version of a semi-structured approach referred as a thematic interview ([Hirsjärvi and Hurme 1995](#Hirsjarvi1995): 35-37), which is based on the _focused interview_ developed by Merton _et al._ ([1956](#Merton1956)). The discussion on the different interview themes was informed and structured according to the notions of free-form thematic discussion and storytelling in the spirit of creative interviewing ([Douglas 1985](#Douglas1985); [Fontana and Frey 2000](#Fontana2000)), active semi-structured interviewing with the objective of inducing structured reflection to inform the interviewer ([Holstein and Gubrium 1997](#Holstein1997)), reflection ([Boud 1985](#Boud1985): 37 Figure 3), semi-structured interviewing ([Fontana and Frey 2000](#Fontana2000)), and an _imagination exercise_ ([Segar _et al._ 2006](#Segar2006), 177).

Even though information work analysis has been used until now only in conjunction with thematically-collected, narrative interview data, the approach can be presumed to work with any kind of research data that provide a rich description of work and related information activity (e.g., ethnographical observation).

### Processing of research data

A schema analysis ([Ryan and Bernard 2000](#Ryan2000): 783-784) of the empirical data resulted in a tentative set of narratives of the information work in a total of seven archaeological work roles. Following the lines of role theory ([Turner 2001](#Turner2001)), the roles are viewed as clusters of similar or nearly similar work duties, titles, scenes of work, objects interacted with and activities. A role is not a direct representation of a job description of any individual. It is rather a profile which may function as a work description by itself, or by combining several work roles together. Roles do change, but more slowly than work profiles.

## Four steps of information work analysis

The author's 2006 study used root definitions, use cases, faceted classification of information interactions and the concept of information horizons to conduct the four steps of information work analysis. The approach does not, however, assume that any particular methods need to be used.

### Actor-context analysis

Information work analysis describes activities that are associated with the actor-contexts of the work role, using _root definitions_ ([Checkland 1981](#Checkland1981): 166-168) based on rich description of the characteristic work processes of the work role. A root definition is "a concise, tightly constructed description of a human activity system which states what the system is" ([Checkland 1981](#Checkland1981): 317). The root definitions are explicated according to the CATWOE criteria ([Smyth and Checkland 1976](#Smyth1976)) from the viewpoints of all the major actors in each of the work contexts. The mnemonic CATWOE stands for **C**lient, **A**ctor, **T**ransformation, **W**eltanschauung (world view), **O**wner and **E**nvironment. The root definitions are particularly useful in clarifying the studied situation by exposing the different views and opinions held by the different stakeholders (including, for example, the users, contractors, customers and management). The root definitions are analytical compositions of the attitudes, opinions and worldviews, based on several individual responses. The definitions are written in the first-person, but they do not correspond with any actual responses word-for-word.

Root definitions and the CATWOE criteria are fundamentally a goal-oriented technique intended to actuate the change in an organization. In information work analysis this technique is used as an analytic instrument for articulating the boundaries and constituent operations embodied in the systems, which are determined by the identified archaeological work roles. A further purpose of the approach is to use the attained understanding, in conjunction with the stated problems, to enquire into the formation of the information work in each context.

### Process task analysis

The understanding of a work role acquired in the analysis of the principal actors and their motivation by using the root definitions, is elaborated by a closer analysis of the information interactions within the context of each role. The role-related interactions are summarised in top-level _use case diagrams_ drawn loosely according to the notation of the Universal Modelling Language (UML) specification version 1.5 ([Object Management Group Inc. 2003](#OMG2003)), which is a language for specifying, visualising, constructing and documenting elements of software systems. (See, for example, [Figure 1](#f1)). A use case diagram allows the description of the high-level user goals in relation to a set of confined regions of work (i.e., soft systems) and their surroundings. The use of UML-style notation is expected to facilitate communication between researchers and informants, because the framework is used widely (for instance, in information systems design).

The link between the root definitions and the use cases is, in essence, dispositional. A use case is considered to be a disposition defined by the system. At the same time, however, it is acknowledged that the use cases induce completely different dispositions on the problem solving practice from those expected within the system ([Checkland & Tsouvalis 1997](#Checkland1997)). The use cases are based on the root definitions, but in an analytical sense the root definitions are usable also in articulating the use cases in the opposite direction. The goals do not need to be specific tasks or actions. They may represent the general lines of action.

In Huvila ([2006](#Huvila2006)), the analysis of the information interactions focuses on the cases that reside within the system of archaeological work drawn in each of the diagrams (the solid rectangle, for example, in [Figure 1](#f1)). The sub-systems of the archaeological system are respectively drawn with dotted lines. The related customer side use cases are illustrated in the diagrams to provide a context for understanding the core information work. The cases that fall outside the system boundaries are omitted in the further analysis of the information processes.

### Information interactions assessment and classification

The next step is to place the relevant information activities in each use case under direct scrutiny. In Huvila ([2006](#Huvila2006)) interactions are explicated and classified according to the faceted classification of interactions with information, introduced by Cool and Belkin ([2002](#Cool2002)). The chosen approach to the scheme places a special emphasis on accommodating the entire spectrum of information interactions, in contrast to Cool and Belkin's approach ([2002](#Cool2002)), where the scrutiny is focused on the seeking of information. Similarly, the assumed information point of view of the study underlines the significance of the information behaviour facet over communication behaviour.

Besides focusing on information behaviour instead of communication behaviour, the original scheme has been modified by augmenting it to incorporate an additional sub-facet of Obstacles in the Interaction Criteria facet. The purpose of the sub-facet is to add a further analytical precision to the Interaction Criteria to elaborate the investigation of the ecological dimension and of the purposes, meanings and values of information work.

The faceted classification scheme, including the addition of obstacles and removal of the communication criteria is summarised in [Table 1](#t1) using the "Give course" interaction of the academic teaching work role as an example.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**<a name="t1"></a>Table 1: An example of the faceted classification of the "Give course" information interaction related to the academic teaching work role  
**(For a description of the activity, see [Huvila 2006](#Huvila2006): 168.)</caption>

<tbody>

<tr>

<th>Facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

<th>Sub-facet</th>

</tr>

<tr>

<td>Information behaviour  
Disseminate</td>

<td>Method:  
speak, (show, discuss)</td>

<td>Mode:  
instruction</td>

<td> </td>

</tr>

<tr>

<td>Communication behaviour  
(omitted in the present study)</td>

<td>Medium: -</td>

<td>Mode: -</td>

<td>Mapping: -</td>

</tr>

<tr>

<td>Objects interacted with</td>

<td>Level:  
information and meta-information</td>

<td>Medium:  
written text, speech, images</td>

<td>Quantity:  
set of objects</td>

</tr>

<tr>

<td>Common dimensions of information</td>

<td>Information object:  
part</td>

<td>Systematicity:  
systematic</td>

<td>Degree:  
selective</td>

</tr>

<tr>

<td>Interaction criteria:  
topic, authority</td>

<td>Obstacles:  
training, information overflow, time</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

### Description of the work-role-specific information landscape

Finally the complete information landscape is summarised (i.e., sphere of the information resources) of the individual use-case-related spheres of the information resources. This is done using analytically-constructed information horizon maps ([Sonnenwald _et al._ 1999](#Sonnenwald1999)) of each work role to provide an overview of the landscape of the relevant information objects for the information activity of the particular role. The information horizon maps explicate both sources and the patterns of how they are used.

In contrast to the earlier applications of the method, the technique of working with information horizon maps is used in information work analysis to _represent typified information horizons, which relate to the work roles, not to the individual actors._ The aim of the approach is to be able to articulate the shared components of the information source landscapes in the diverse archaeological information processes. The principal benefit of the approach is to move the scope of the study from the individual users to the level of work systems.

The notation of the information horizon maps used in Huvila ([2006](#Huvila2006)) consists of ordinary (dotted squares) and entry-point information sources (those typically used first in information interactions; marked with solid squares), links between the sources (arrowed, if a prevalent sequence of use has been identifiable). The arrow and line indicate the position from which the horizon is approached examined by a representant of the work role.

## Case example: The case of academic teaching work role

As an example of the information work analysis approach in action, the following sections present an overview of the analysis of an individual work role in the context of archaeological work. The example is abridged from a comprehensive study of archaeological information work. A detailed version of this description and descriptions of the remaining six archaeological work roles may be found in Huvila ([2006](#Huvila2006)).

### Work role

The role of academic teaching refers to the full-time and part-time university educators. Archaeology departments employ relatively few people on a permanent basis as full-time or part-time teachers. Most of the teachers holding a permanent position have completed their doctoral degree. The lectures and courses held by permanent staff members are complemented by part-time lecturers who are employed outside the university in museums and other heritage institutions [respondents O and C], or are graduate students at the department. (The informants are cited through out the study by using codes to preserve their anonymity.)

The academic teaching role may involve a great variety of tasks that are relevant to archaeology education and its planning. Depending on the position of the teacher and the division of work in an individual department, the duties may include giving lectures, conducting practical field and classroom exercises, holding examinations, reading essays, planning study programmes and supervising students. Part-time personnel (respondents H, M, S and Z) concentrate mostly on giving lectures

### Work process

The work process of academic teaching work role consists of various tasks related to the education and training of archaeology professionals (see [Figure 1](#f1)). Curriculum management and planning is shared by the entire teaching staff of a department. Individual courses and their precise contents are typically left to the discretion of the individual lecturers, although the old course contents are often made available for the new lecturers to ensure some degree of continuity (respondent Z).

Root definitions are given from two viewpoints: student - an archaeology major (see [Huvila 2006](#Huvila2006): 165) and teacher (see Table 2).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
<a name="t2"></a>**Table 2: CATWOE analysis of academic teaching: teacher viewpoint**</caption>

<tbody>

<tr>

<th>Customers</th>

<th>Student</th>

</tr>

<tr>

<td>Actors</td>

<td>Me</td>

</tr>

<tr>

<td>Transformation</td>

<td>Getting an archaeology student to become concerned and informed of the constituent aspects of the archaeological knowledge so that s/he will be able to work as a professional archaeologist.</td>

</tr>

<tr>

<td>Weltanschauung</td>

<td>It is important to ensure that the students learn the essential content of my teaching and obtain proficiency in the science and craft of archaeology.</td>

</tr>

<tr>

<td>Owners</td>

<td>The community of archaeologists; University; Me</td>

</tr>

<tr>

<td>Environment</td>

<td>University</td>

</tr>

<tr>

<td colspan="2">I am employed at the university to teach new archaeology students, that is, forthcoming archaeology professionals, my future colleagues and successors in the science, art and craft of the archaeological profession. My duty is to make the students to learn the essentials so that they will be able to perform well during their future career. I am responsible for my job to myself, to my colleagues at the department and to my university. In a broader framework, I am responsible for the whole community of archaeologists that the future archaeology professionals share the essence of our profession.</td>

</tr>

</tbody>

</table>

### Interactions with information

The constituent information interactions of the academic teaching work role are interlocked with keeping aware of the latest research and archaeology related discussions and evaluation and processing of that information. The scope of the interactions tends to be broader than in the other work roles.

Academic teaching serves an intermediary function. From the management perspective, the constituent transformation is an act of a focused reorganization. Basically, the process comprises information behaviours of access, comprehension, evaluation, reorganization and dissemination. The most critical phase, considering the task performance, is the reorganization of the existing scientific and scholarly information into a form that could be expected to enable effective communication of the information to the students.

Of the four information interactions that reside within the system under scrutiny, the interaction _Prepare a course_ is described in detail below.

<div align="center">**![Figure 1: Use case diagram of academic teaching](p349fig1.png)**</div>

<div align="center">**<a name="f1"></a>Figure 1: Use case diagram of academic teaching role**</div>

Completely new courses are introduced rather seldom in educational programmes in the Nordic context. The curricula evolve rather than change drastically. When the preparation of a course starts from scratch, its general framework is typically drafted by the entire teaching staff of the department. The appointed teachers often consult subject experts if they are not specialists themselves (A.a, B.a, see classified interactions below). Information seeking starts from a generic monograph on the subject closest to the topic and continues iteratively through lists of references and formal searches to complementary literature until the perceived information needs are satisfied or the available time has run out (A.b-c, B.b-c). The interaction is steered by the criteria of topicality and authority and constrained by appropriateness and the availability of resources (A.d, B.d).

**A. Access**

a. Information behaviour: _access_, Method: _searching_, Mode: _specification_  
b. Objects interacted with: Level: _meta-information_, Medium: _literature, personal communication_, Quantity: _set of objects_  
c. Common dimensions of information: Information object: _part_, Systematicity: _systematic_, Degree: _selective_  
d. Interaction criteria: _authority, topic,_ Obstacles: _appropriateness, time, resources, access, information overflow_

**B. Organize**

a. Information behaviour: _organize,_ Method: _rewrite, restructure,_ Mode: _summarisation, (simplification)_  
b. Objects interacted with: Level: _meta-information,_ Medium: _literature, visual material,_ Quantity: _set of objects_  
c. Common dimensions of information: Information object: _part,_ Systematicity: _exhaustive,_ Degree: _selective_  
d. Interaction criteria: _authority, topic,_ Obstacles: _training, appropriateness, time, information overflow_

### Information horizon

The information horizon in the academic teaching work role ([Figure 2](#f2)) tends to be broad, but relatively shallow in comparison to the research-oriented work roles. The primary material for academic teaching is the established archaeological literature, which is complemented with diverse actual matters drawn from the current research. The use of literature differs in the basic courses from the advanced level instruction.

Even though the research literature is acknowledged to be the most important information source, the academic teaching work role thinks most positively of all of the archaeological work roles about the popular media and makes the best use of popular media sources. even though the popular sources are used more typically for the purposes of visualisation than as a source of information (respondents M and Z). The role of social information seeking is also important for the educators, although it seems to be somewhat less important than in the other work roles.

<div align="center">![Figure 2: Information horizon of the academic teaching work role](p349fig2.png)</div>

<div align="center">  
<a name="f2"></a>**Figure 2: Information horizon of the academic teaching work role**</div>

## Discussion

### Information activity and work roles in earlier studies

Various role-based approaches have attracted occasional interest among information science researchers (e.g., [Leckie1996](#Leckie1996); [Sonnenwald _et al._ 2001](#Sonnenwald2001); [Wilkinson 2001](#Wilkinson2001); [Fidel _et al._ 2004](#Fidel2004); [Fidel & Pejtersen 2004](#Fidel2004a); [Chaplan & Hertenstein 2005](#Chaplan2005); and [Landry 2006](#Landry2006)). Probably the most cited work on the roles-based approach is the general model of the information seeking of professionals proposed by Leckie _et al._ in 1996 ([Leckie _et al._ 1996](#Leckie1996)). The model makes a tripartite hierarchical distinction between work roles, tasks and information needs. The proposition suggests that the tasks and subsequent information needs emerge from the work roles. The general model is based on an observation that five distinct work roles tended to recur in research data gathered in a series of studies of different professional groups. The model has been developed and validated since in further studies (for example, in [Landry 2006](#Landry2006) and [Wilkinson 2001](#Wilkinson2001)).

The model of Leckie _et al._ has definite relevance in explaining the general patterns of information seeking and use of professionals. It is, however, based on assumptions which make it rather abstract. The model explains the information use of professional groups (such as engineers, lawyers, nurses or dentists). The groups are treated as relatively monolithic entities. In practice, the duties and subsequent information activities of two engineers can differ significantly. At the present, the increasing dynamics of job descriptions, work, professions and duties underlines this diversity (in particular, Hudson ([2002](#Hudson2002): 44) and also Gershuny ([2000](#Gershuny2000)) and Burchell _et al._ ([2002](#Burchell2002))). The practical work of a professional is an amalgam of various duties, which are repeated in different combinations in the daily work of their colleagues as, for instance, Taylor ([1991](#Taylor1991)) has underlined. There are similarities between the daily work of different individuals, but it is argued that the similarities do not relate as much to the entirety of their work as to the resemblance or dissimilarity of its individual elements ([Hudson 2002](#Hudson2002): 44). Even if the academic research in different fields is somewhat similar, it has been demonstrated on several occasions that there are disciplinary differences (e.g., [Wiberley and Jones 1989](#Wiberley1989); [Whitmire 2002](#Whitmire2002); [Tibbo 1994](#Tibbo1994)). Therefore it can be suggested that it would be relevant to look at the potential similarity of information interactions in different work settings instead of the recurrence of broader patterns of information use, which inevitably conceal a wealth of differences. Even though it is possible (for example) to pinpoint _service provider nurses_ and _service provider lawyers_ (see [Leckie 1996](#Leckie1996)), the similarity of the two groups would have (in many cases) only minor practical implications for the form of common information services, sources, systems or education.

Assuming a more contextually-anchored viewpoint, information work analysis proposes that the complete system of shared practices of information use, information interactions and information horizons is work-role-specific, but that any of these individual components is likely to occur in a number of work roles. It is suggested that it would be feasible to pinpoint eventual similarities in information horizons and interactions not only between different work roles, but between all who share the relevance of common meanings, purposes and values and (from the information point of view) share the same information resources.

The information work analysis argues that work roles are meaningful contextual units of analysis in their own right. Work roles do not merely form a framework for sets of tasks and subsequent information needs within a profession; they also represent a practicable unit of conceptualising _why_ the work is being done. The work roles relate to work, which is an activity characterised by a common purpose, meaning and value. Simultaneously the work roles relate to broader professional knowledge domains (understood here in the Hjørlandian sense as thought communities with shared patterns of co-operation, relevance criteria, structure and knowledge organization ([Hjørland and Albrechtsen 1995](#Hjorland1995))). Work roles directly affect the use of information resources and, albeit relating to the information behaviour of the involved actors, the work role specific information activity and an activity of an individual are not identical. From a systemic perspective the different work roles assumed by individuals function as sub-systems of their complete work (i.e., all the work in which they are involved).

A work role may function directly as a target group for an information service. The more general similarities are related to the individual information interactions and information horizons, which may be bear resemblance and have synergy far beyond the general purpose of the associated activity. A student and a researcher are likely to have similarities in their information interactions although their work roles (including their purposes, meanings and values) are significantly different. In consequence, it would be possible to develop information services in a manner which takes into account both the fundamental differences of the role and the practical similarities of some of the information interactions.

Besides the possibility of a finer-grain analysis of human information activities, another significant benefit of considering work roles as firmly contextual rather than abstract notions is that it makes it possible to bring work-role-related information activities together with the information behaviour of the individuals acting in the work roles. It provides a framework that allows integration of the extremes of personal and shared characteristics of information work.

### Information work analysis

Information work analysis provides a multiple level analytical framework. First, because a work role is a contextual unit of work, the analysis of information work of an individual work role provides an understanding of that particular sector of information work and the means to develop measures to improve its success. It is possible to develop specific measures to improve and support information work of, for example, field archaeologists or cultural heritage administrators.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**<a name="t3"></a>Table 3: Cross-tabulation of the work roles and information interactions classified according to the information behaviour facet.**  
(The foci of the interactions related to each work role are indicated by using a bold typeface and upper case letters.)</caption>

<tbody>

<tr>

<th>Work role</th>

<th>Create</th>

<th>Modify</th>

<th>organize</th>

<th>Preserve</th>

<th>Disseminate</th>

<th>Access</th>

<th>Evaluate</th>

<th>Use</th>

<th>Comprehend</th>

</tr>

<tr>

<td>Field archaeology</td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Antiquarian</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">**X**</td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Public dissemination</td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

</tr>

<tr>

<td>Academic research</td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

</tr>

<tr>

<td>Academic teaching</td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td align="center">**X**</td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Cultural heritage  
administration</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Infrastructural  
development</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td> </td>

<td align="center">**X**</td>

<td> </td>

</tr>

<tr>

<td>Field archaeology</td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Antiquarian</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">**X**</td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Public dissemination</td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

</tr>

<tr>

<td>Academic research</td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

</tr>

<tr>

<td>Academic teaching</td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td align="center">**X**</td>

<td align="center">x</td>

<td align="center">x</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Cultural heritage  
administration</td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">x</td>

<td align="center">**X**</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Infrastructural  
development</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td align="center">x</td>

<td> </td>

<td align="center">**X**</td>

<td> </td>

</tr>

</tbody>

</table>

Second, the work-roles-based approach of information work analysis provides a set of comparable units of description of different kinds of work activities. Root definitions, use cases, information interactions and information horizons are presented in a form, which allows cross work role comparisons for discerning differences, similarities and convergencies. The Huvila ([2006](#Huvila2006)) study explicated the fundamental difference between the information work of field archaeologists, antiquarians and academic researchers by looking at their information horizon and interactions with common information resources (Tables [3](#t3) and [4](#t4). See also [Huvila 2006](#Huvila2006): 254-259). In a similar manner, the framework may be used to analyse convergences and differences, for instance, between patterns of information use, production, their criteria, resources and the horizon of their use.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
<a name="t4"></a>**Table 4: Aspects of work role-specific information sources**</caption>

<tbody>

<tr>

<th>Work role</th>

<th>Focus of interest</th>

<th>Principal transmitter</th>

<th>Information</th>

<th>Specificity of information</th>

<th>Quantity of objects</th>

<th>Mode of access</th>

</tr>

<tr>

<th>Field  
archaeology</th>

<td>Site</td>

<td>Investigation report</td>

<td>Descriptive</td>

<td>Specific</td>

<td>Set</td>

<td>Search/  
browse</td>

</tr>

<tr>

<th>Antiquarian</th>

<td>Artefact</td>

<td>Collections  
database</td>

<td>Descriptive</td>

<td>Specific</td>

<td>Database</td>

<td>Search/  
browse</td>

</tr>

<tr>

<th>Public  
dissemination</th>

<td>Subject</td>

<td>General  
literature</td>

<td>Affective</td>

<td>General</td>

<td>Set</td>

<td>(General  
level)  
browse</td>

</tr>

<tr>

<th>Academic  
research</th>

<td>(varies)</td>

<td>(varies)</td>

<td>(varies)</td>

<td>Specific</td>

<td>Set</td>

<td>Search/  
browse</td>

</tr>

<tr>

<th>Academic  
teaching</th>

<td>Subject</td>

<td>General  
literature</td>

<td>Summarising</td>

<td>General</td>

<td>Set</td>

<td>Browse</td>

</tr>

<tr>

<th>Cultural heritage  
administration</th>

<td>Site</td>

<td>Investigation report</td>

<td>Evaluative</td>

<td>Specific</td>

<td>Database</td>

<td>Search</td>

</tr>

<tr>

<th>Infrastructural  
development</th>

<td>Method</td>

<td>Technical  
literature</td>

<td>Evaluative</td>

<td>Specific</td>

<td>Set</td>

<td>Browse</td>

</tr>

</tbody>

</table>

The third, broader integrative power of the approach relates to the possibility to scrutinise simultaneously the distribution of work role ([Figure 3](#f3)) and individual information behaviour related information interactions ([Figure 4](#f4)) on the life-cycle of information. When these distributions (Figures 3 and 4) are studied together with the affordances and constraints of information systems and knowledge organization frameworks, it becomes possible make conclusions about their compatibility.

<div align="center">![Figure 3: Work roles situated on the life-cycle](p349fig3.png)</div>

<div align="center">  
**<a name="f3"></a>Figure 3: Work roles situated on the life-cycle of information  
**([Huvila 2006](#Huvila2006), Figure 7.10 p. 235)</div>

<div align="center">**![Figure 4: Information behaviour groups situated](p349fig4.png)**</div>

<div align="center">  
**<a name="f4"></a>Figure 4: Information behaviour groups situated on the life-cycle of information**  
The information behavioural groups and the distiribution of their related characteristic information interactions is based on a qualitative analysis of the information seeking and use of the individual informants of the study ([Huvila 2006](#Huvila2006): 236 Figure 7.11)</div>

As a formality and structure-seeking framework, information work analysis has its limitations. The approach is based on a significant assumption that the analysed activity is "work", i.e., is a distinct evolving set of inter-linked human activities, which has either explicit or implicit purpose, meaning and value ([Huvila 2006](#Huvila2006), 20). Furthermore, as in all research that aims at formality of description, the representation of information provided by the instruments of the analysis simplifies the diversity of human endeavours.

Similarly, as in all qualitative research the analysis provides an understanding from the point of view represented by the analyser. The most significant limitation of the approach is that the framework scrutinises information activity on the level of a domain. In order to attain a comprehensive understanding of the information activity and its motivations, information work analysis needs to be complemented with an investigation of individuals and their information behaviour ([Huvila 2006](#Huvila2006): 201-214). Otherwise the work-related factors and individual preferences of the informants remain indistinguished from each other. On the other hand, the approach from two directions allows us to relate the two together ([Table 5](#t5)).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**<a name="t5"></a>Table 5: Work roles and information behaviour**  
</caption>

<tbody>

<tr>

<th>Work role</th>

<th>**Information behaviour**</th>

</tr>

<tr>

<td>Field archaeology</td>

<td>Extensive (intensive) participants</td>

</tr>

<tr>

<td>Antiquarian</td>

<td>Intensive participants</td>

</tr>

<tr>

<td>Public dissemination</td>

<td>Extensive customers</td>

</tr>

<tr>

<td>Academic research</td>

<td>Intensive (extensive) participants</td>

</tr>

<tr>

<td>Academic teaching</td>

<td>Extensive customers</td>

</tr>

<tr>

<td>Cultural heritage administration</td>

<td>Intensive customers (administration subsystem)</td>

</tr>

<tr>

<td>Cultural heritage administration</td>

<td>Extensive participants (cultural heritage policy subsystem)</td>

</tr>

<tr>

<td>Infrastructural development</td>

<td>Extensive customers</td>

</tr>

</tbody>

</table>

## Conclusion

In comparison to the earlier approaches to the analysis of human information work, the information work analysis approach offers some benefits in form of new insights and emphases. Information work analysis is an approach that focuses explicitly on information and work from an information and management point of view. It perceives the two as social and cultural instead of merely cognitive issues. Unlike the information technology oriented analytical frameworks, information work analysis aims at improving information work as a comprehensive enterprise instead of merely looking at how to develop a computerised information system.

Information work analysis provides a description and understanding of real world phenomena, but it is also an instrument that aims to provide the necessary premises for changing and developing the present state of affairs. The limitations of the approach relate to its inbuilt focus on work (i.e., scale of perusal) and domains (i.e., contextuality versus generality). Finally, an additional benefit of the framework is its ecological approach to the information activity. Because the human information work is conceptualised in comparable systemic terms as the information processing systems, information work analysis provides means to manage and explicate both within a single framework.

## Note

This paper is based on the doctoral thesis of the author ([2006](Huvila)).

## Acknowledgements

The author thanks the anonymous referees whose comments led to an improved version of the original paper and the copy-editor whose changes helped to produce a more readable version.

## References

*   <a id="Bates2005" name="Bates2005"></a>Bates, M. (2005). [Information and knowledge: an evolutionary framework for information science.](http://InformationR.net/ir/10-4/paper239.html) _Information Research_, **10**(4) paper 239\. Retrieved 11 August, 2008 from http://InformationR.net/ir/10-4/paper239.html
*   <a id="Biddle1966" name="Biddle1966"></a>Biddle, B., & Thomas, E.J. (Eds.) (1966). _Role theory: concepts and research_. New York, NY: Wiley.
*   <a id="Biddle1986" name="Biddle1986"></a>Biddle, B.J. (1986). Recent developments in role theory. _Annual Review of Sociology_, **12**, 67-92\.
*   <a id="Boud1985" name="Boud1985"></a>Boud, D., Keogh, R., & Walker, D. (1985). _Reflection: turning experience into learning_. London: Kogan Page.
*   <a id="Burchell2002" name="Burchell2002"></a>Burchell, B., Ladipo, D., & Wilkinson, F. (Eds.). (2002). _Job insecurity and work intensification: flexibility and the changing boundaries of work_. London: Routledge.
*   <a id="Bystrom1997" name="Bystrom1997"></a>Byström, K. (1997). Municipal administrators at work: information needs and seeking [IN&S] in relation to task complexity: a case-study amongst municipal officials. In In P. Vakkari, R. Savolainen & B. Dervin (eds.), _Information seeking in context (ISIC): Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts._ (pp. 125-146). London: Taylor Graham.
*   <a id="Campbell1999" name="Campbell1999"></a>Campbell, S. J. (1999). [Role theory, foreign policy advisors and U.S. foreign policymaking.](http://www.webcitation.org/5ZzjKYb1n) Paper presented at the International Studies Association, 40th Annual Convention, Washington, D.C., February 16-20, 1999 Retrieved 11 August, 2008 from http://www.diligio.com/notes14.htm (Archived by WebCite® at http://www.webcitation.org/5ZzjKYb1n)
*   <a id="Case2007" name="Case2007"></a>Case, D.O. (2007). _Looking for information: a survey of research on information seeking, needs and behavior._ Burlington, MA: Academic Press.
*   <a id="Chaplan2005" name="Chaplan2005"></a>Chaplan, M. A., & Hertenstein, E.J. (2005). Role-related library use by local union officials. Journal of the American Society for Information Science and Technology, **56**(10), 1062-1074\.
*   <a id="Checkland1981" name="Checkland1981"></a>Checkland, P. (1981). _Systems thinking, systems practice_. New York, NY: Wiley.
*   <a id="Checkland2000" name="Checkland2000"></a>Checkland, P. (2000). Soft systems methodology: a thirty year retrospective. _Systems Research and Behavioral Science_, **17**(S1), S11-S58\.
*   <a id="Checkland1997" name="Checkland1997"></a>Checkland, P., & Tsouvalis, C. (1997). Reflecting on SSM: the link between root definitions and conceptual models. _Systems Research and Behavioral Science_, **14**(3), 153-168\.
*   <a id="Clifford1996" name="Clifford1996"></a>Clifford, C. (1996). Role: a concept explored in nursing education. Journal of Advanced Nursing, **23**(6), 1135-1141\.
*   <a id="Cool2002" name="Cool2002"></a>Cool, C., & Belkin, N. (2002). A classification of interactions with information. In H. Bruce, R. Fidel, P. Ingwersen & P. Vakkari (Eds.), Emerging frameworks and methods: Proceedings of the Fourth International Conference on Conceptions of Library and Information Science (CoLIS4) (pp. 1-15). Greenwood Village, CO: Libraries Unlimited.
*   <a name="Davies1998"></a>Davies, B. & Harré, R. (1998) Positioning and personhood In R. Harré & L. van Lagenhove, L. (Eds.) _Positioning theory: moral contexts of intentional action._ (pp. 32-52). Oxford: Blackwell, 32-52.
*   <a id="Douglas1985" name="Douglas1985"></a>Douglas, J.D. (1985). _Creative interviewing_. Beverly Hills, CA: Sage.
*   <a id="Ellis1993" name="Ellis1993"></a>Ellis, D. (1993). Modeling the information-seeking patterns of academic researchers. _Library Quarterly_, **63**(4), 469-486\.
*   <a id="Fidel2004" name="Fidel2004"></a>Fidel, R., Pejtersen, A.M., Cleal, B., & Bruce, H. (2004). A multidimensional approach to the study of human-information interaction: a case study of collaborative information retrieval. _Journal of the American Society for Information Science and Technology_, **55**(11), 939-953.
*   <a id="Fidel2004a" name="Fidel2004a"></a>Fidel, R., & Pejtersen, A. M. (2004). [From information behaviour research to the design of information systems: the Cognitive Work Analysis framework.](http://www.webcitation.org/5ZzxLR8nr) _Information Research_ **10**(1), paper 210\. Retrieved 11 August, 2008 from http://InformationR.net/ir/10-1/paper210.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5ZzxLR8nr)
*   <a id="Fisher2005" name="Fisher2005"></a>Fisher, K.E., Erdelez, S., & McKechnie, L.E. (Eds.). (2005). _Theories of information behavior_. Medford, NJ: Information Today.
*   <a id="Fontana2000" name="Fontana2000"></a>Fontana, A., & Frey, J. H. (2000). The interview. In N. K. Denzin & Y. S. Lincoln (Eds.), _Handbook of qualitative research_. 2nd ed. (pp. 645-672). Thousand Oaks, CA: Sage.
*   <a id="Gershuny2000" name="Gershuny2000"></a>Gershuny, J. (2000). Changing times: work and leisure in post-industrial society. Oxford: Oxford University Press.
*   <a id="Gibson1979" name="Gibson1979"></a>Gibson, J.J. (1979). _The perception of the visual world_. Boston, MA: Houghton Mifflin.
*   <a id="Goffman1959" name="Goffman1959"></a>Goffman, E. (1959). _The presentation of self in everyday life_. New York, NY: Anchor Books.
*   <a id="Goffman1972" name="Goffman1972"></a>Goffman, E. (1972). _Encounters: two studies in the sociology of interaction._ Harmondsworth, UK: Penguin.
*   <a id="Hansen2000" name="Hansen2000"></a>Hansen, P. & Järvelin, K. (2000). The information seeking and retrieval process at the Swedish Patent- and Registration Office. Moving from lab-based to real life work-task environment. In N. Kando & M.-K. Leong (Eds.), _Proceedings of the SIGIR 2000 workshop on patent retrieval_ (pp. 43-53). Athens, Greece: ACM SIGIR.
*   <a id="Heinstrom2002" name="Heinstrom2002"></a>Heinström, J. (2002). _Fast surfers, broad scanners and deep divers: personality and information seeking behaviour._ Unpublished doctoral dissertation, Abo Akademi University, Abo, Finland
*   <a id="Hirsjarvi1995" name="Hirsjarvi1995"></a>Hirsjärvi, S., & Hurme, H. (1995). Teemahaastattelu. (The thematic interview.) Helsinki: Yliopistopaino.
*   <a id="Hjorland1995" name="Hjorland1995"></a>Hjørland, B., & Albrechtsen, H. (1995). Toward a new horizon in information science: domain analysis. _Journal of American Society for Information Science_, **46**(6), 400-425\.
*   <a id="Holstein1997" name="Holstein1997"></a>Holstein, J.A., & Gubrium, J.F. (1997). Active interviewing. In D. Silverman (Ed.), _Qualitative research: theory, method and practice._ (pp. 113-129). London: Sage.
*   <a id="Hudson2002" name="Hudson2002"></a>Hudson, M. (2002). Disappearing pathways and the struggle for a fair day's pay. In B. Burchell, D. Ladipo & F. Wilkinson, (Eds.), _Job insecurity and work intensification: flexibility and the changing boundaries of work._ (pp. 39-60). London and New York, NY: Routledge.
*   <a id="Huvila2006" name="Huvila2006"></a>Huvila, I. (2006). [The ecology of information work: a case study of bridging archaeological work and virtual reality based knowledge organization.](http://www.webcitation.org/5ZzyLARUP) Abo/Turku: Abo Akademi University Press. Retrieved 11 August, 2008 from http://bit.ly/4mbiJV (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5ZzyLARUP)
*   <a id="Landry2005" name="Landry2006"></a>Landry, C.F. (2006). Work roles, tasks and the information behavior of dentists. _Journal of the American Society for Information Science and Technology_, **57**(14), 1896-1908
*   <a id="Langlois1982" name="Langlois1982"></a>Langlois, R.N. (1982). Systems theory and the meaning of information. _Journal of the American Society for Information Science_, **33**(6), 395-399\.
*   <a name="Layder2006"></a>Layder, D. (2006) _Understanding social theory_. Thousand Oaks, CA: Sage.
*   <a id="Leckie1997" name="Leckie1997"></a>Leckie, G.J., & Pettigrew, K.E. (1997). A general model of the information seeking of professionals: role theory through the back door? In In P. Vakkari, R. Savolainen & B. Dervin (eds.), _Information seeking in context (ISIC): Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts._ (pp. 99-110). London: Taylor Graham Publishing.
*   <a id="Leckie1996" name="Leckie1996"></a>Leckie, G.J., Pettigrew, K.E., & Sylvain, C. (1996). Modeling the information seeking of professional: a general model derived from research on engineers, health care professionals, and lawyers. _Library Quarterly_, **66**(2), 161-193\.
*   <a id="Merton1956" name="Merton1956"></a>Merton, R.K., Fiske, M. & Kendall, P.L. (1956). _Focused interview: a manual of problems and procedures._ New York, NY: Simon and Schuster.
*   <a id="Nurminen1996" name="Nurminen1996"></a>Nurminen, M. I., & Torvinen, V. (1996). Role-based interpretation of ISS. Turku, Finland: Turku Centre for Computer Science. (TUCS Technical Report No. TUCS-TR-9).
*   <a id="OMG2003" name="OMG2003"></a>Object Management Group Inc. (2003). _[http://www.webcitation.org/5ZzzvjmUYUnified Modeling Language (UML), version 2.1.2.](http://www.webcitation.org/5ZzzvjmUY)_ Needham, MA: Object Management Group, Inc. Retrieved 11 August, 2008 from http://www.omg.org/technology/documents/formal/uml.htm (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5ZzzvjmUY
*   <a id="Pejtersen2004" name="Pejtersen2004"></a>Pejtersen, A. M., & Rasmussen, J. (2004). Cognitive work analysis of new collaborative work. In _IEEE international conference on systems, man & cybernetics, The Hague, Netherlands, 10-13 october 2004._ (pp. 904-910). Piscataway NJ: IEEE.
*   <a id="Rasmussen1994" name="Rasmussen1994"></a>Rasmussen, J., Pejtersen, A. M., & Goodstein, L. P. (1994). _Cognitive systems engineering_. New York, NY: John Wiley.
*   <a id="Ryan2000" name="Ryan2000"></a>Ryan, G. W., & Bernard, H. R. (2000). Data management and analysis methods. In N. K. Denzin & Y. S. Lincoln (Eds.), _Handbook of qualitative research_. 2nd. ed. (pp. 769-802). Thousand Oaks, CA: Sage.
*   <a id="Segar2006" name="Segar2006"></a>Segar, M., Spruijt-Metz, D., & Nolen-Hoeksema, S. (2006). Go figure? Body-shape motives are associated with decreased physical activity participation among midlife women. _Sex Roles_, **54**(3), 175-187\.
*   <a id="Smyth1976" name="Smyth1976"></a>Smyth, D. S., & Checkland, P> (1976). Using a systems approach: the structure of root definitions. _Journal of Applied Systems Analysis_, **5**(1), 75-83\.
*   <a id="Sonnenwald1999" name="Sonnenwald1999"></a>Sonnenwald, D.H. (1999). Evolving perspectives of human information behavior: Contexts, situations, social networks and information horizons. In T. D. Wilson & D.K. Allen (Eds.), Exploring the contexts of information behaviour: proceedings of the Second International Conference on Research in Information Needs, Seeking and Use in Different Contexts, 13/15 August, 1998, Sheffield, UK (pp. 176-190). London: Taylor Graham.
*   <a id="Sonnenwald1997" name="Sonnenwald1997"></a>Sonnenwald, D. H., & Lievrouw, L. A.(1997). Collaboration during the design process: a case study of communication, information behavior and project performance. In ISIC '96: Proceedings of an international conference on information seeking in context (pp. 179-204). London: Taylor Graham.
*   <a id="Sonnenwald2001" name="Sonnenwald2001"></a>Sonnenwald, D. H., Wildemuth, B.M., & Harmon, G.L. (2001). A research method using the concept of information horizons: an example from a study of lower socio-economic students' information seeking behaviour. _The New Review of Information Behaviour Research_, **2**, 65-86\.
*   <a id="Spinuzzi2003" name="Spinuzzi2003"></a>Spinuzzi, C. (2003). _Tracing genres through organizations: a sociocultural approach to information design._ Cambridge, MA: MIT Press.
*   <a id="Star1999" name="Star1999"></a>Star, S. L., & Strauss, A. (1999). Layers of silence, arenas of voice: the ecology of visible and invisible work. _Computer Supported Cooperative Work_, **8**(1-2), 9-30\.
*   <a id="Strauss1985" name="Strauss1985"></a>Strauss, A. (1985). Work and the division of labor. _The Sociological Quarterly_, **26**(1), 1-19\.
*   <a id="Talja2002" name="Talja2002"></a>Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _The New Review of Information Behaviour Research_, **3**, 132-159\.
*   <a id="Taylor1991" name="Taylor1991"></a>Taylor, R. (1991). Information use environments. _Progress in Communication Sciences_, **10**, 217-255\.
*   <a id="Tibbo1994" name="Tibbo1994"></a>Tibbo, H.R. (1994). Indexing for the humanities. _Journal of the American Society for Information Science_, **45**(8), 607-619\.
*   <a id="Turner2001" name="Turner2001"></a>Turner, R. H. (2001) Role theory. In J. Turner, (Ed.), _Handbook of sociological theory_. (pp. 233-254). Berlin: Springer.
*   <a id="Vakkari2003" name="Vakkari2003"></a>Vakkari: (2003). Task-based information searching. _Annual Review of Information Science and Technology_, **37**, 413-464\.
*   <a id="Whitmire2002" name="Whitmire2002"></a>Whitmire, E. (2002). Disciplinary differences and undergraduates' information-seeking behavior. _Journal of the American Society for Information Science and Technology_, **53**(8), 631-638\.
*   <a id="Wiberley1989" name="Wiberley1989"></a>Wiberley, S.E. & Jones, W.G. (1989). Patterns of information seeking in the humanities. _College & Research Libraries_, **50**(6), 638-645\.
*   <a id="Widenwulff2000" name="Widenwulff2000"></a>Widén-Wulff, G. (2000). [Business information culture: a qualitative study of the information culture in the Finnish insurance industry](http://www.webcitation.org/5a02QWyHt). _Information Research_, **5**(3). Retrieved 11 August, 2008 from http://informationr.net/ir/5-3/paper77.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5a02QWyHt)
*   <a id="Wilkinson2001" name="Wilkinson2001"></a>Wilkinson, M.A. (2001). Information sources used by lawyers in problem solving: an empirical exploration. _Library & Information Science Research_, **23**(3), 257-276\.
*   <a id="Wilson2000" name="Wilson2000"></a>Wilson, T.D. (2000). Human information behavior. Informing Science, **3**(2), 49-55\.



