####  Vol. 6 No. 2, January 2001


# National Information Infrastructure and the realization of Singapore IT2000 initiative

#### Cheryl Marie Cordeiro and [Suliman Al-Hawamdeh](mailto:assuliman@ntu.edu.sg)  
Division of Information Studies  
School of Computer Engineering  
Nanyang Technological University, Singapore

#### Abstract

> Being a small island and without any natural resource, Singapore depends on its human potential and investment in National Information Infrastructure (NII) in order to find its place in the competitive global world economies. From Singapore's first experience with the setting up and accessing of the Internet in 1991, the Singapore Government has expended much creative and financial energy into using information technology to spearhead Singapore's success in terms of enticing and encouraging economic growth and achieving national competitiveness on a global scale. In 1991, the Singapore government, together with the National Computer Board (NCB) currently known as the Infocomm Development Authority (IDA), launched the _IT 2000_, with the objective of converting Singapore into an intelligent island. With many NII projects in place and the various government initiative, this study focus on the role of Singapore Government in the development of the national information infrastructure and the realisation of IT2000 vision. This investigative study delves into the role of the Singapore government in helping Singapore forge its path into the new millennium of the information world.

## Introduction

Around the world, government policy forms the corner stone in steering nations towards a more information-based society and economy. Countries are reviewing and restructuring their regulations, financing operations and ownership to meet the demands of these new telecommunications and multimedia drivers. Plans exist to improve telecommunications channels and to construct new channels that are more advanced and accessible. Education, human resource retraining, health care, productivity, and environmental conservation are but a few of the issues covered that have potentially brighter futures directly related to the developments of national information highways or information societies. Singapore is pursuing the _Intelligent Island,_ Japan the _Info-communications for the 21<sup><font size="-3">st</font></sup> Century_ and the European Union (EU) the _European Information Society_ (_GICC Asia Regional Meeting and International Conference_, November 1995)_._ These various government-led initiatives have become an inter-woven lacing of technologies, economics, politics and potentialities. New corporate alliances across the globe are making traditional geographical boundaries (as well as those between companies and technologies) irrelevant.

In the first annual report to the European Commission from the Information Society Forum, the third working group for the forum, with Jacques Wautrequin as Chairman, did a study on the _[Influences on Public Services](#ref3)_ (1996). The report stressed the importance of the public sector and how government ministries, regional and local authorities should get involved in the intensive introduction of information technologies to the public services. This will bring public administrations nearer each citizen, making information services accessible to everybody, as a concrete application of the 'universal service' concept at Union, national, regional and local level.

One of the key roles in which governments of the various countries can play is to promote electronic links between national and Union administrations within the European Union, with a common goal of accelerating the execution of common policies and the single market. In the third working group's view, a wide dissemination of data held by the public sector would respond to the interest of citizens, the market and society as a whole. An active public sector in the Information Society will provide better services to the citizen as well as give the European equipment and content industries the chance to develop new products and services that could also be of advantage on the global market.

At the Union level, common legal frameworks are implemented to encourage accessibility of public information to every citizen at acceptable and reasonable costs. Focusing projects on functional specifications of systems, harmonised message formatting and user commands will further stimulate and encourage standardisation processes throughout Europe. The Union is also currently revising prevalent Union R&D programmes and other programmes, especially those of informatics, telematics and INFO 2000, with the goal of identifying and promoting appropriate solutions for public services.

In the same Information Society Forum, the first working group, with Joan Majo as Chairman, studied the _[Impact on the Economy and Employment.](#ref1)_ The study emphasised the need for deep structural and institutional changes, fuelled and steered by the governments of each country, for the potentially positive impact of the Information Society on growth to materialise. As echoed by the third working group, the first working group also voiced the need for a regulatory framework, with priority given to the adaptation of the legal and social framework, and the need for lifelong learning and retraining. In Europe, governments promote long-term entrepreneurship, in which the Information Society will restructure company organisations by developing their autonomy and responsibilities. The promotion of entrepreneurship can in turn be achieved by modifying the education system in the European secondary schools and universities. The other goal of European governments would be to promote the 'learning company', which will become the cornerstone of the Information Society tomorrow. The aim is to have the 'learning company' at the cutting edge of innovation, by recognising and endorsing positive (new markets, technological innovations, and the availability of resources) and negative (saturated markets, competition, lack of resources and regulatory changes) shifts in the business environments.

Governmental ideologies permeate the entire society, their regulations and policies reaching out and extending throughout the social fabric of that society and culture. Consequently, one cannot consider only economic and political factors but must also take into account social and democratic values. This study, completed by the second working group to the Information Society Forum, with Aidan White as Chairman, reports on the _[Basic Social and Democratic Values in the Virtual Community.](#ref3)_ This study looked into how the European Commission, at both national and international levels, can do more to assert the moral and social dimension of the Information Society. Public as well as individual interests of setting social and professional standards and of defining objectives that sustain democratic and individual liberties are regarded by the various governments in the European Union in order to foster the conditions for economic exploitation. For instance, all citizens must be guaranteed the right of freedom of expression including the freedom to hold opinions and to receive and impart information without interference. Used properly, the European Commission deems that information technology can be a tool of empowerment for ordinary people, giving them a greater understanding of their rights. This, in turn, encourages greater self-confidence, assertiveness and encourages more involvement of the people in the exercise of democracy. Another dimension that needs to be explored is how governments have helped people with physical disabilities to gain equal access to the Information Society, including the free provision of services funded from public and/or private sources. People need to be made aware of the potential uses of the technologies and to understand that they present new and untried opportunities for the majority. It is difficult for people to know their needs unless they know the possibilities on offer. In other words, an entire education system has to support the idea of an Information Society, teaching people how to work and live and make a living in such a Society.

In the _Japan-U.S. Information Infrastructure Symposium_ of June 1994, ([CSIS..., 1994](#ref1)) keynote speaker, Michael Nelson stated that, politically, the notion of advancing national information superhighways was set in motion when the Clinton Administration announced the establishment of a _National Information Infrastructure_ (NII) initiative as a major policy. The NII is a system to deliver to all Americans the information they need when they want it and where they want it for an affordable price. The goal is to have 250 million channels - a channel for every American, a personalised channel for information and a two-way channel of information that provides interactive video throughout the country. But, while the Clinton Administration, Congress and the Federal Communications Commission have outlined the rules and regulations to the NII, success depends on American industry. This is because the major networks for telephones, broadcasting, cable and satellites launched in the U.S. have been operated and owned by the private sector. The merging of information technologies has significantly increased the potential for electronic communications to facilitate broader social policy issues. Thus, the Clinton Administration put strong emphasis on this area in its NII initiative. While U.S. industry is more advanced than the government in designing and implementing services that address social policy goals, the U.S. government's call for broadband access to every school, hospital, and library in America has raised expectation levels of many interest groups in the U.S. The last role is in the area of government services, where the task would be to put the government on-line, making the U.S. government more approachable and more accessible to the public.

Over the next five years, the U.S will channel millions more into research projects that include ways to speed up the Internet and how to integrate computing and medical research. Similar issues are being addressed in Japan, with a stronger emphasis on government leadership and direct governmental involvement. In areas such as cable television for example, complex government restrictions have prevented alternative networks to be a major dimension of the private sector's ability to build up the information infrastructure. However, Japan has the advantage in escalating the use and applications of multimedia in its information infrastructure. Tanabe (CSIS..., 1994) suggests that the Japanese industry has recognised the tremendous business potential in the information industry which now exists in the U.S and globally. The U.S. government has created positive competitive conditions by systematically removing regulatory structures that exemplified technology and outmoded economic assumptions. Forces transforming communication, information and entertainment industries in the U.S. undermine existing global regulatory and commercial conditions, which in turn stimulate growth in the field of information infrastructure. Tanabe goes as far as to suggest that nations that do not assimilate the new and changing informational environment of global competitiveness and advanced telecommunications to embrace the concept of the 'global village' may well place their national competitiveness at risk.

Due to rapidly changing information technology and increasing international competition, alliances across national and regional borders have become an inevitable trend. According to Lü Xinkui, ([GIIC..., 1995](#ref1)), while many Asian countries have made outstanding achievements in national information technology and information infrastructure. The Asian region as a whole still has a long way to go in achieving the levels of informatisation in Europe and America. Xinkui calls for a common and joint effort of all the Asian countries to work on an equal and symbiotic basis, helping each other out in terms of NII.

Pairash Thajchayapong, Director of the National Electronics and Computer Technology Centre (NECTEC), Thailand ([GIIC..., 1995](#ref1)), called for fuller exploitation of the new opportunities offered by IT. Thajchayapong suggested that the government of Thailand provide efficient and effective social services to all citizens to provide full support for a 'strong and thriving local information industry', of which appropriate policy recommendations include:

*   launching a Nation wide Government Informatisation Programme
*   making IT planning an integral part of the annual government budgeting exercise and IT policy research a continuous effort
*   supporting the development of a strong local information industry and
*   promoting and supporting electronic means for citizens and businesses to interact nationally, regionally and internationally, trading with government or amongst themselves.

The Telecommunications and Informatics Division of The World Bank assists countries in deploying the Information Infrastructure and reviews opportunities, challenges and the role of governments in developing the Information Infrastructure. James Bond, the Division Chief of the Telecommunications and Informatics Division of the World Bank examined the role of the World Bank and other multilateral financing institutions in presenting the new Information and Development Initiative launched by the World Bank ([GIIC..., 1995](#ref1)). The World Bank is a significant financing source for infrastructure, private and public, in the emerging economies. However, its role is not only towards being a direct source of funds but with its financing infrastructure of _lending and equity_ and _guarantees_, it is also a catalyst in attracting financing from other sources. The challenges put forth by Bond in view of obtaining global information infrastructure include the absence of information infrastructure, in which the role of governments here would be to ensure that the infrastructure gets built and not to build it itself. The next challenge is that of the need for a decent regulatory and business environment, which means having a regulatory function that manages sector-specific issues such as frequency allocation, interconnection, tariffs and public service obligations. And ultimately, the greatest challenge as Bond sees it, is that of human capacity and culture, in which economic changes have to be integrated into human terms that respect the individual, and empowers the individual to make the most of the new paradigm.

## Singapore IT Initiatives

Singapore has prepared itself to meet the new challenges of the information age. In the _Vision of the Intelligent Island: the IT2000 Report_. ([National Computer Board, 1992](#ref2)) one of the key roles played by the Singapore government was to invest heavily in R&D to project leadership positions in what is regarded as the 'locomotive' industry of the next millennium. In brief, IT2000 has a ten-year rolling horizon, which looks into long-term issues and key trends such as:

1.  Improving price-performance of microelectronics resulting in faster, cheaper, smaller and user-friendly IT
2.  Dramatic increase in speed and capacity of information communication, seeing the availability of broadband communication and wireless communication networks
3.  Proliferation of multimedia applications, which means cheap, portable and powerful multimedia systems, that reach out to the various domains of public and private spheres, including that of education. This move will also see the convergence of media like TV, radio and print media
4.  Extensive standardisation which involves the international movement to standardise information technology, which is driven largely by market forces and fuelled by buyers' concerns over the protection of the value of their investment
5.  Integration of IT with the transportation system, thereby expanding work and lifestyle options.

The NIIG foresees that information infrastructure planning is likely to be the social planning movement of the early 21<sup>st</sup> century. NCB as the NII masterplanner will take on the new function of overseeing and spearheading the implementation of NII. Forming the NII Group and collaborate with other organisations, mainly that of the Singapore Telecom, TCS, Ministry of Information and the Arts (MITA) and Ministry of Communications to plan and develop different components of the NII. The system architecture will be designed by these organisations and functional specifications for common network services will be drawn. The ultimate goal of the NIIG would be to find a common balance between integration and flexibility of informational infrastructure that enables productive and efficient connectivity.

According to the IT2000 report, there will be a need for multi-agency collaboration as Singapore becomes a more networked society. The implementation of the NII projects also requires the participation of, and has direct impact on, many governmental departments and statutory boards. Singapore's IT visions can only be achieved through synergistic efforts, adopting highly co-ordinated approach at all organisational levels. A high-level steering committee will be formed to forge and implement the NII, charting the directions and development of information infrastructure, addressing policy issues such as data protection, information access, intellectual property and computer security as Singapore becomes a highly-networked society. Non-technical issues such as policy and legal frameworks that take into account the cost of enforcement of the NII to business operations will be drawn and adopted. While experiences from other countries are useful, the government as well as the involving organisations acknowledge that personal choice on the basis of evolving national values and traditions have to be taken into consideration when drawing up policies and legal frameworks. There will be a need for Singapore to define its own set of codes of practice or laws on data protection which reflect local conditions that establish ground rules on the obligations of data users and the rights of data subjects.

Future recommendations by the IDA for IT2000, sustained by the Singapore government include endorsing the IT vision to stimulate national competitiveness and to improve the quality of life in the coming decades. Singapore has to entrust the IDA as master planner of the NII to lead and direct a multi-agency effort to implement the information infrastructure. The government has to provide the NCB with funding for the NIIG, especially projects that are not deemed commercially viable but strategically or beneficial to the country. Commercially viable projects would be open to participation by the private sector, participation by multi-national corporations.

## Study Methodology

The seventy-six participants who volunteered for this study are working professionals. They came from various working backgrounds. From governmental statutory boards such as the National Library Board (NLB), the NCB, the Ministry of Information and the Arts (MITA), the Ministry of Education (MOE), the Economic Development Board (EDB), Telecommunications Authority of Singapore (TAS) and also the private sector such as multi-national corporations and local companies. All participants were part of a graduate study degree in information studies. The participants were asked to discuss and answer the an open question: _identify areas in which the government can play a role in the development of the Information Society in Singapore._ The age range of the participants is between 24 and 55.

The participants were allowed an hour of discussion in working groups of four or five, after being given the question on the role of the government in NII in Singapore. The groups were formed according to the participants' working background such that educational officers, managers, library officers, IT specialists etc, all formed distinct groups for interaction. When the hour was up, each group reported their findings through a group spokesperson. All seventy-six individual full reports were consolidated and collected exactly a week after the group discussion and presentation.

In order to identify the various ideas from the participants' responses, each script was analysed after which a checklist was created from those responses. The checklist enabled comparisons to be made to the IT2000 report, which was used as the main framework to this study. The report as explicated by the NCB acts as a national standard in which the main ideas for IT and IS in Singapore are put forward for the country's future development.

Both quantitative and the qualitative methods of analysis were used so that a fair cross-section of the responses from the participants could be obtained. The quantitative technique was used to collect statistical facts in the responses of the participants in order to be evaluated in relation to the IT2000 report. The qualitative technique was more concerned with the each individual's perception of the Singapore government and their efforts in bringing Singapore into the Information Age. The latter method will serve to seek insight rather than statistics to the situation, lending depth to this investigative study.

## IT2000 as a Benchmark

IT2000 as a government initiative, outlined the role of government in the development of national information infrastructure in Singapore. The report provides the basic criteria for analysing the responses of the participants. When measured and compared to the IT2000 report, this will help determine the extent in which the IT2000 report, conceived in March 1992, was able to address the government role in the development of NII at the time of conception. IT2000 and its five major categories include:

<div style="text-indent: 30;">

1.  **Developing Global Hub**

*   Business Hub
*   Services Hub
*   Transportation Hub

4.  **Boosting the Economic Engine**

*   Manufacturing
*   Commerce
*   Tourism
*   Construction

7.  **Enhancing the Potential of Individuals**

*   Lifelong Learning:
*   Creativity
*   Skills
*   Knowledge
*   Greater investment in education & training
*   Individual learning pace, place & time
*   Simulated & interactive education

1.  Extension of Media & Cultural Institutions
2.  Knowledge Navigation
3.  Extra Help for the Disadvantaged
4.  Interactive Distance Education
5.  Multimedia Learning

10.  **Improving Quality of Life**

*   Easy Commuting
*   Teleshopping
*   One Stop, Non-stop Government & Business Service
*   Cashless Transactions
*   Telecommuting
*   More Options for Leisure
*   Intelligent Buildings
*   Better Healthcare

13.  **Linking Communities Locally and Globally**

*   NII to help create electronic communities of like-minded people.
*   To enable Singaporeans to extend and strengthen personal reach.
*   Encourage development of a global mindset, blurring the lines between local and global communication.
*   Each individual to have access to and actively use electronic mailing, video conferencing, bulletin boards, electronic chats and spin her own communication webs according to her set of affiliations e.g., clans, old school ties, professional society etc.

1.  Community Telecomputing Network
2.  Singapore International Net

</div>

## Findings and Analysis

Five corresponding tables relating to the IT2000 categories were created to provide the statistical results from the participants. More than half of the participants thought that developing Singapore into a global hub is the right move in bringing Singapore into the information age. However, more participants saw the globalization of Singapore in terms of business and services rather than in transportation. From one participant's point of view, the Singapore government had done much to position Singapore as a global business and services hub, as evidenced by the number of international conferences, seminars and events held in Singapore on an annual basis.

<table align="center" border="" cellspacing="1" cellpadding="7" width="660"><caption align="bottom">Table 1: Developing Singapore as a Global Hub</caption>

<tbody>

<tr>

<td width="42%" valign="TOP" bgcolor="#00ffff" height="49">

Category

</td>

<td width="20%" valign="TOP" bgcolor="#00ffff" height="49">

Number of transcripts (of 76)

</td>

<td width="38%" valign="TOP" bgcolor="#00ffff" height="49">

Percentage of total transcripts (to the nearest decimal place)

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#c0c0c0" height="31">

Developing Singapore as a global hub

</td>

<td width="20%" valign="TOP" bgcolor="#c0c0c0" height="31">

40

</td>

<td width="38%" valign="TOP" bgcolor="#c0c0c0" height="31">

52.6%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="27">

a. Business hub

</td>

<td width="20%" valign="TOP" bgcolor="#ffff00" height="27">

18

</td>

<td width="38%" valign="TOP" bgcolor="#ffff00" height="27">

45% (of 40 transcripts)

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="31">

b. Services hub

</td>

<td width="20%" valign="TOP" bgcolor="#ffff00" height="31">

16

</td>

<td width="38%" valign="TOP" bgcolor="#ffff00" height="31">

40%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="28">

c. Transportation hub

</td>

<td width="20%" valign="TOP" bgcolor="#ffff00" height="28">

6

</td>

<td width="38%" valign="TOP" bgcolor="#ffff00" height="28">

15%

</td>

</tr>

</tbody>

</table>

The participants also mentioned that Multi-National Companies (MNCs) are not only encouraged by the government to set up operations in the region but they are also encouraged to use Singapore as their headquarters or main trading zone to the rest of the Asian region. To cite an example of what Singapore is competing with, the participant gave an example of how the Nike Organisation has its manufacturing plants located in China to tap the low cost labour market and it has its research and design division in Silicon Valley, USA. As a result, the participants feel that Singapore has the challenge to invent and present itself to such similar organisations, in the manner most attractive to them such that they will want to root its organisation in the country. Without the advantage of low labour costs, Singapore has to turn to information technology, research and development, its efficiency and transparent governmental system to attract such organisations.

<table align="center" border="" cellspacing="1" cellpadding="7" width="708"><caption align="bottom">Table 2: Boosting the Economic Engine</caption>

<tbody>

<tr>

<td width="32%" valign="TOP" bgcolor="#00ffff" height="49">

Category

</td>

<td width="24%" valign="TOP" bgcolor="#00ffff" height="49">

Number of transcripts (of 76)

</td>

<td width="43%" valign="TOP" bgcolor="#00ffff" height="49">

Percentage of total transcripts (to the nearest decimal place)

</td>

</tr>

<tr>

<td width="32%" valign="TOP" bgcolor="#c0c0c0" height="31">

Boosting the economic engine

</td>

<td width="24%" valign="TOP" bgcolor="#c0c0c0" height="31">

6

</td>

<td width="43%" valign="TOP" bgcolor="#c0c0c0" height="31">

7.9%

</td>

</tr>

<tr>

<td width="32%" valign="TOP" bgcolor="#ffff00" height="27">

a. Manufacturing

</td>

<td width="24%" valign="TOP" bgcolor="#ffff00" height="27">

6

</td>

<td width="43%" valign="TOP" bgcolor="#ffff00" height="27">

100% (of 6 transcripts)

</td>

</tr>

<tr>

<td width="32%" valign="TOP" bgcolor="#ffff00" height="31">

b. Commerce

</td>

<td width="24%" valign="TOP" bgcolor="#ffff00" height="31">

3

</td>

<td width="43%" valign="TOP" bgcolor="#ffff00" height="31">

50%

</td>

</tr>

<tr>

<td width="32%" valign="TOP" bgcolor="#ffff00" height="27">

c. Construction

</td>

<td width="24%" valign="TOP" bgcolor="#ffff00" height="27">

5

</td>

<td width="43%" valign="TOP" bgcolor="#ffff00" height="27">

83.3%

</td>

</tr>

<tr>

<td width="32%" valign="TOP" bgcolor="#ffff00" height="27">

d. Tourism

</td>

<td width="24%" valign="TOP" bgcolor="#ffff00" height="27">

6

</td>

<td width="43%" valign="TOP" bgcolor="#ffff00" height="27">

100%

</td>

</tr>

</tbody>

</table>

In 'boosting the economic engine', only six of the seventy-six participants thought of IT2000 as having any concern with the manufacturing, commerce and construction sectors - these traditionally being non-IT related sectors of the economy. However, all six participants mentioned the government's role in the tourism sector, which they deemed as a good manner in which the government can promote Singapore. If tourism figures increase and if tourists return to their countries with a good impression of Singapore, it can be considered that Singapore has left them with a lasting, positive impression.

IT and NII in Singapore tend to permeate to the core of the Singapore system. For example, the country's local transport systems provide technology-based, up-to-the-minute information on bus arrivals and departures etc, and Singapore has information kiosks situated along roads to give tourists whatever information they need. One cannot underestimate the power of word of mouth or first hand information, as such, tourists will leave the country with the impression of how well connected and organised the country is and other people will come to hear of Singapore's coming to the information age.

All fifty-one scripts of the seventy-six scripts that mentioned the Singapore government's role in enhancing the potential of individuals by bringing Singapore into the information age referred to Singaporeans adopting multimedia learning as a way of life in future. The government's stance towards education in this era is that Singaporeans should learn to adopt a lifelong attitude towards learning by which it means that an individual never stops learning. In a recent spate of debates on the Age of Information Technology in Singapore, it has been said that the difference between IT haves and have-nots will be to society what the difference between the literate and the illiterate meant once ([Sunday Times - Review-Viewpoints, 27 February 2000](#ref2)). In this matter the Infocomm Development Authority of Singapore (IDA) plans to organise events in March 2000 to make people comfortable with using information and communication technology. Their events are planned to reach home-makers, blue-collar workers and the disabled so that nobody is overlooked or neglected.

<table align="center" border="" cellspacing="1" cellpadding="7" width="688"><caption align="bottom">Table 3: Enhancing the Potential of Individuals</caption>

<tbody>

<tr>

<td width="50%" valign="TOP" bgcolor="#00ffff">

Category

</td>

<td width="20%" valign="TOP" bgcolor="#00ffff">

Number of transcripts (of 76)

</td>

<td width="30%" valign="TOP" bgcolor="#00ffff">

Percentage of total transcripts (to the nearest decimal place)

</td>

</tr>

<tr>

<td width="50%" valign="TOP" bgcolor="#c0c0c0" height="26">

Enhancing the Potential of Individuals

</td>

<td width="20%" valign="TOP" bgcolor="#c0c0c0" height="26">

51

</td>

<td width="30%" valign="TOP" bgcolor="#c0c0c0" height="26">

67.1%

</td>

</tr>

<tr>

<td width="50%" valign="TOP" bgcolor="#ffff00">

1.  Multimedia learning

</td>

<td width="20%" valign="TOP" bgcolor="#ffff00">

51

</td>

<td width="30%" valign="TOP" bgcolor="#ffff00">

100% (of 51 transcripts)

</td>

</tr>

<tr>

<td width="50%" valign="TOP" bgcolor="#ffff00" height="14">*   Interactive distance education</td>

<td width="20%" valign="TOP" bgcolor="#ffff00" height="14">

12

</td>

<td width="30%" valign="TOP" bgcolor="#ffff00" height="14">

23.6%

</td>

</tr>

<tr>

<td width="50%" valign="TOP" bgcolor="#ffff00" height="18">*   Extension of media and cultural institutions</td>

<td width="20%" valign="TOP" bgcolor="#ffff00" height="18">

22

</td>

<td width="30%" valign="TOP" bgcolor="#ffff00" height="18">

43.1%

</td>

</tr>

<tr>

<td width="50%" valign="TOP" bgcolor="#ffff00" height="18">*   Extra help for the disadvantaged</td>

<td width="20%" valign="TOP" bgcolor="#ffff00" height="18">

17

</td>

<td width="30%" valign="TOP" bgcolor="#ffff00" height="18">

33.3%

</td>

</tr>

<tr>

<td width="50%" valign="TOP" bgcolor="#ffff00" height="17">*   Knowledge navigation</td>

<td width="20%" valign="TOP" bgcolor="#ffff00" height="17">

14

</td>

<td width="30%" valign="TOP" bgcolor="#ffff00" height="17">

27.5%

</td>

</tr>

</tbody>

</table>

Most participants (67.1%), viewed this category as the most important in bringing Singapore into the information age, which uncovers the underlying assumption that many participants have - that computers are tools of economic empowerment to individuals. Enabling easy access to computers gives the under-privileged and the uninformed not only psychological access to power but it democratises technology in the interests of those who might otherwise be left behind. The IDA's efforts are part of the government's moves to encourage participation in the Internet. For example, it has been given assurance that all students will have access to computers, with a ratio of two students to one PC.

Twenty-two of fifty-one participants thought that the government could enhance and extend media and cultural institutions. In a bid to make this a reality, students of the Nanyang Technological University (NTU) will be able to connect to the Internet anytime and anywhere from their 'wireless' campus by December 2000\. The university plans to spend nearly four million dollars to set up a wireless campus network with five hundred access points. It is currently seeking tenders for the installation of the network, as mentioned by the NTU president Dr. Cham Tao Soon ([Straits Times - Home, Thursday, 17 February 2000](#ref2)). This move was decided upon in a bid to keep abreast with the changing education system where computers and the Internet play vital roles in expanding students' learning horizons. It was also to keep up with the global scene and advances in the communication of information. It is not only the tertiary institutions that are moving ahead in term of info-communications. A project entitled _Curriculum Alive_ is one of the services made available on Singapore One by Ednovation Pte. Ltd. Curriculum Alive is multimedia learning and revision software for primary school students and provides about 1,800 interactive learning activities with animated stories and educational games. The software helps complement traditional teaching methods in primary schools so that students can learn in creative and entertaining ways. More examples of a similar trend can be found in the section on [the realisation of IT2000](#real) below.

<table align="center" border="" cellspacing="1" cellpadding="7" width="708"><caption align="bottom">Table 4: Improving the Quality of Life</caption>

<tbody>

<tr>

<td width="42%" valign="TOP" bgcolor="#00ffff">

Category

</td>

<td width="23%" valign="TOP" bgcolor="#00ffff">

Number of transcripts (of 76)

</td>

<td width="36%" valign="TOP" bgcolor="#00ffff">

Percentage of total transcripts (to the nearest decimal place)

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#c0c0c0" height="26">

Improving the Quality of Life

</td>

<td width="23%" valign="TOP" bgcolor="#c0c0c0" height="26">

27

</td>

<td width="36%" valign="TOP" bgcolor="#c0c0c0" height="26">

35.5%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="42">

a. One stop, non-stop government & business service

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="42">

27

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="42">

100% (of 27 transcripts)

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="18">

b. Teleshopping

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="18">

3

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="18">

11.1%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="22">

c. Easy commuting

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="22">

19

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="22">

70.4%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="26">

d. Better healthcare

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="26">

4

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="26">

14.8%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="21">

e. Intelligent buildings

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="21">

18

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="21">

66.6%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="15">

f. More options for leisure

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="15">

16

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="15">

59.3%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="26">

g. Telecommuting

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="26">

15

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="26">

55.5%

</td>

</tr>

<tr>

<td width="42%" valign="TOP" bgcolor="#ffff00" height="30">

h. Cashless transactions

</td>

<td width="23%" valign="TOP" bgcolor="#ffff00" height="30">

5

</td>

<td width="36%" valign="TOP" bgcolor="#ffff00" height="30">

18.5%

</td>

</tr>

</tbody>

</table>

Although twenty-seven participants regarded coming into the information age as a means of improving the quality of life, all twenty-seven generally thought that a one-stop government business service would be adequate in this aspect. Few participants saw tele-shopping and cashless transactions as a means of improving the quality of life and even fewer conceived having more leisure time in the information era. It is perhaps because tele-shopping is a relatively new phenomenon in Singapore that few participants mentioned it or even trust it. It is an event where individuals can shop at home using the Internet or television, comparing product design and prices at various electronic retail sites. _[NTUC Income Insurance Co-operative Ltd](http://www.income.com.sg)_ is the first insurance company in Singapore to offer its products and services on Singapore One, introducing users to the full array of insurance products. It enables the secure buying of policies using credit or cash cards. _[Singtel Magix](http://hispeed.singtelmagix.com.sg/magix/ethernet/menu.asp)_, another service offered by the Singapore Telecommunications Ltd. sets alternative leisure options for Singaporeans at home, where they can watch movies, play computer games, catch up on the latest news or shop, all in the privacy of their homes. More examples of what the Singapore government has done in this aspect can be found in the following pages in _[the realisation of IT2000](#real)_.

In terms of healthcare standards, where only four participants mentioned the improving of healthcare administration via the NII, the government has in view the creation of an _Electronic Medical Records System_ in which an individual's medical information history and personal particulars are recorded on to a smart card. This card will be used during medical check-ups or during emergencies in a bid to save time and helping to save the individual's life in critical conditions. Time will not be wasted on rummaging through computer data for medical records since all is available on the smart card. The NII can also be used to telecast short video-clips on the harm caused by smoking or drug taking anywhere and anytime on the MRTs and bus interchanges or shopping centres. With videoconferencing and Internet consultations, patients who are housebound or who cannot afford a costly trip to a specialist overseas can use the Internet to their advantage in this aspect.

<table align="center" border="" cellspacing="1" cellpadding="7" width="669"><caption align="bottom">Table 5: Linking Communities Locally and Globally</caption>

<tbody>

<tr>

<td width="46%" valign="TOP" bgcolor="#00ffff">

Category

</td>

<td width="21%" valign="TOP" bgcolor="#00ffff">

Number of transcripts (of 76)

</td>

<td width="32%" valign="TOP" bgcolor="#00ffff">

Percentage of total transcripts (to the nearest decimal place)

</td>

</tr>

<tr>

<td width="46%" valign="TOP" bgcolor="#c0c0c0" height="28">

Linking Communities Locally and Globally

</td>

<td width="21%" valign="TOP" bgcolor="#c0c0c0" height="28">

20

</td>

<td width="32%" valign="TOP" bgcolor="#c0c0c0" height="28">

26.3%

</td>

</tr>

<tr>

<td width="46%" valign="TOP" bgcolor="#ffff00" height="37">

1.  Community telecomputing network

</td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="37">

20

</td>

<td width="32%" valign="TOP" bgcolor="#ffff00" height="37">

100% (of 20 transcripts)

</td>

</tr>

<tr>

<td width="46%" valign="TOP" bgcolor="#ffff00">*   Singapore International Net</td>

<td width="21%" valign="TOP" bgcolor="#ffff00">

12

</td>

<td width="32%" valign="TOP" bgcolor="#ffff00">

60%

</td>

</tr>

</tbody>

</table>

All participants agreed that a community network set up by the government would be useful in helping keep all the communities together. In 1999, 58.9% of households had computers with 42.1% having Internet access (compared with 14% in 1997) ([IDA, 2000](#ref1))

It has been said that poverty is the 'ultimate threat' to man and globalisation is still the best hope for closing the rich-poor gap ([Straits Times - Prime News,14 February 2000](#ref2)). All the participants have indicated that globalisation should be seen in a positive light and not as 'what some have portrayed it to be - a blind, potentially malevolent force that needs to be tamed'. Singapore is one nation which has decided to take on the responsibility to progress towards world unity in its bid to provide international links on the Internet. However, in a bid to keep Singapore's Asian orientation, Singapore's Minister of State for Foreign affairs, Ow Chin Hock in a speech at an Edusave scholarship and bursary awards presentation at Leng Kee Community Centre in January 2000 said that:

> As Singapore becomes more international and cosmopolitan in the 21<sup>st</sup> century, let us not forget that we are also Asians.... As Asians, we should always strive to preserve our cultural heritage...

The Minister of State for Foreign affairs has called for Singaporeans to look beyond the monetary and material aspects of life to show compassion towards their fellow citizens. He also said that a helping hand should be extended to the more needy, sharing success with the less fortunate. By 'heartlanders' was meant people who made a living in Singapore as taxi drivers or provision-store owners as opposed to the new cosmopolitan Singaporean who has international mobility and marketing ability and who speaks English.

A final table will tabulate the responses that, from the participants' points of view, would fall outside of the scope of the IT2000 framework or what the participants have suggested that might improve the IT2000 framework.

### Beyond the scope of the IT2000 Initiative

Table 6 categorises the participants' responses that are perhaps beyond the scope of IT2000 or that the specific issues tabulated are not indicated as clearly in IT2000 on which the participants wish for more governmental attention and emphasis. The issues raised may also be that of those that either affects the participants directly or indirectly for example, they have experienced the issues personally in their working environment or in their personal domain. As indicated in table 6, the top three concerns of the participants would be that of: _standardisation practices_ with 55.2%; _ethical aspect of techno-preneurship or technology_ with 27.6% and a _secure socio-political & economic environment_ and _providing tax incentives & subsidy services_ with 23.7% in agreement each.

<table align="center" border="" cellspacing="1" cellpadding="7" width="696"><caption align="bottom">Table 6: Beyond the scope of IT2000</caption>

<tbody>

<tr>

<td width="6%" valign="TOP" bgcolor="#c0c0c0" height="51">

<font size="2">No.</font>

</td>

<td width="44%" valign="TOP" bgcolor="#c0c0c0" height="51"><font size="2">

Category

</font></td>

<td width="21%" valign="TOP" bgcolor="#c0c0c0" height="51"><font size="2">

Number of transcripts (of 76)

</font></td>

<td width="29%" valign="TOP" bgcolor="#c0c0c0" height="51"><font size="2">

Percentage of total transcripts (to the nearest decimal place)

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="18"><font size="2">

1

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="18"><font size="2">

Aiding & encouraging private sectors

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="18"><font size="2">

16

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="18"><font size="2">

21%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

2

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

Policies & Standardisation practices

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

42

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

55.2%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="35"><font size="2">

3

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="35"><font size="2">

Providing secure socio-political and economic environment

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="35"><font size="2">

18

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="35"><font size="2">

23.7%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="34"><font size="2">

4

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="34"><font size="2">

Providing tax incentives & subsidy services

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="34"><font size="2">

18

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="34"><font size="2">

23.7%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="21"><font size="2">

5

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="21"><font size="2">

Carry out 'human engineering' processes

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="21"><font size="2">

6

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="21"><font size="2">

7.9%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

6

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

Liase with the banking & finance sector

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

1

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

1.3%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

7

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

Discourage Monopolies

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

1

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="23"><font size="2">

1.3%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="13"><font size="2">

8

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="13"><font size="2">

Balance censorship and freedom of expression

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="13"><font size="2">

3

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="13"><font size="2">

3.9%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

9

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

Workshops & seminars for IT and information illiteracy for the elderly

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

5

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

6.6%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="36"><font size="2">

10

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="36"><font size="2">

Provide more IT related scholarships

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="36"><font size="2">

1

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="36"><font size="2">

1.3%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

11

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

Work with the mass media on IT and NII

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

2

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

2.6%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

12

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

Government as consumer of IT and NII

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

2

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="37"><font size="2">

2.6%

</font></td>

</tr>

<tr>

<td width="6%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

13

</font></td>

<td width="44%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

Ethical Aspect of Technopreneurship or Technology

</font></td>

<td width="21%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

21

</font></td>

<td width="29%" valign="TOP" bgcolor="#ffff00" height="46"><font size="2">

27.6%

</font></td>

</tr>

</tbody>

</table>

Table 7 below highlights the participants' responses to the IT2000's 5 major thrusts with 51 participants or 67.1% emphasising the importance of enhancing the potential of individuals.

The participants have spoken strongly on several issues in both their qualitative and quantitative responses. The first of which is that they feel that individuals will benefit most from the Singapore government's efforts at bringing the country into the information age. Singaporeans will grow as individuals, and government efforts will ensure that the majority of individuals will know how to harvest their own potential and fend for themselves in the information age. Some participants have viewed the government as having a variety of roles to play in the information age, from seeing the government as a 'strategist' and 'builder' to the government as 'consumer' of the very IT that they hope to promote and encourage in Singapore. Some of the insecurities felt by the participants do need real attention from the government. 23.7% believed that Singaporean's need to be more open-minded and entrepreneurial and there is a need to re-educate the workforce on IT. Other insecurities revealed by the participants had to do with their general lack of knowledge of the current IT and Infocomm updates in Singapore. 55.2% believed more work needs to be done in terms of Internet security and rules and regulations regarding IT usage and exploitation of IT. However, these issues have been dealt with via the various governmental Ministries.

<table align="center" border="" cellspacing="1" cellpadding="7" width="766"><caption align="bottom">Table 7: Summary of Quantitative Results of Study</caption>

<tbody>

<tr>

<td width="35%" valign="TOP" colspan="2" bgcolor="#c0c0c0" height="41">

Category

</td>

<td width="33%" valign="TOP" colspan="4" bgcolor="#c0c0c0" height="41">

Number of transcripts (of 76)

</td>

<td width="32%" valign="TOP" bgcolor="#c0c0c0" height="41">

Percentage of total transcripts (to the nearest decimal place)

</td>

</tr>

<tr>

<td width="27%" valign="TOP" bgcolor="#ffff00" height="26">

1.  Developing a global hub

</td>

<td width="23%" valign="TOP" colspan="3" bgcolor="#ffff00" height="26">

40

</td>

<td width="50%" valign="TOP" colspan="3" bgcolor="#ffff00" height="26">

52.6%

</td>

</tr>

<tr>

<td width="35%" valign="TOP" colspan="2" bgcolor="#ffff00" height="27">*   Boosting the economic engine</td>

<td width="23%" valign="TOP" colspan="3" bgcolor="#ffff00" height="27">

6

</td>

<td width="42%" valign="TOP" colspan="2" bgcolor="#ffff00" height="27">

7.9%

</td>

</tr>

<tr>

<td width="35%" valign="TOP" colspan="2" bgcolor="#ffff00" height="28">*   Enhancing the Potential of Individuals</td>

<td width="23%" valign="TOP" colspan="3" bgcolor="#ffff00" height="28">

51

</td>

<td width="42%" valign="TOP" colspan="2" bgcolor="#ffff00" height="28">

67.1%

</td>

</tr>

<tr>

<td width="35%" valign="TOP" colspan="2" bgcolor="#ffff00" height="25">*   Improving the Quality of Life</td>

<td width="23%" valign="TOP" colspan="3" bgcolor="#ffff00" height="25">

27

</td>

<td width="42%" valign="TOP" colspan="2" bgcolor="#ffff00" height="25">

35.5%

</td>

</tr>

<tr>

<td width="35%" valign="TOP" colspan="2" bgcolor="#ffff00" height="28">*   Linking Communities Locally and Globally</td>

<td width="23%" valign="TOP" colspan="3" bgcolor="#ffff00" height="28">

20

</td>

<td width="42%" valign="TOP" colspan="2" bgcolor="#ffff00" height="28">

26.3%

</td>

</tr>

</tbody>

</table>

The top three concerns of the participants were standardization practices with 55.2%, the ethical aspect of 'techno-preneurship' or technology with 27.6%, and secure socio-political and economic environment with 23.7%.

## Differing Views and Expectations

All of the participants' viewed the government as the single most influential body to help bring Singapore into the Information Age for the 21<sup>th</sup> Century, with its main responsibility in building and providing the means for either the National Information Infrastructure (NII) or for general information infrastructure in information technology. Not all participants agreed with the IT2000 checklist categories. Four participants that had formed a group derived their own categories on what the role of government should be in bringing Singapore into the Information Age. The six differentiated categories as set forth by this group of participants include the government being in the roles of:

*   a _strategist_ - in which the government is seen as the main body of organisation in developing a vision for the nation, taking on a leadership role in defining Singapore's future direction of growth and allocating appropriate resources in aid of realising the information age for Singapore;
*   a _builder_ - in which the government is seen to provide the physical infrastructure that would provide the means for everyone to access information from around the world. At the same time, the government is also seen as the organisation to develop forward-looking policies that would attract global information services and telecommunication players to invest, develop and promote Singapore as the 'gateway of information', first within the region in view of moving onto a more global scale;
*   a _regulator_ and _facilitator_ - in which the government faces the challenges of creating a nation of enterprise without stifling innovation through excessive censorship. It needs to create a fair business competition ground without allowing fraudulent, undesirable businesses to thrive and it has to create an environment for risk taking in business without encouraging rampant abuse of the system to occur
*   an _investor_ - in which various governmental organisations can help local MNCs, SMEs and individual entrepreneurs to grow in the new information environment. Through tax incentives and special grants, the government can encourage local enterprises to invest in technology so as to exploit the new medium of trading. SMEs can also take the advantage of e-commerce to develop international storefronts without the need to lay out heavy capital investments
*   an _integrator_ - in which the government ensures that the various programmes and projects such as Singapore ONE, Library 2000 etc, that have been assigned towards the realising of the 'Vision of the Intelligent Island' is well integrated, becoming a cohesive strategy in allowing Singapore to thrive in the Information Age. This requires well constructed and forward thinking policies as well as close co-operation between government, businesses and citizens to respond effectively not only to new opportunities but also to threats that the Information Age may carry
*   an _educator_ - in which the government should create an environment that would encourage Singaporeans to adopt the 'lifelong learning concept'. The education curriculum, schoolwork and information rich resources such as libraries and the use of the internet, has been reviewed to reflect the change towards a more dynamic and intellectually stimulating mode of learning

### Ethical Aspects of IT Use

Some concerns that were voiced out by the participants included that of the ethical aspects of 'techno-preneurship' and technology usage. The participants seemed to look to the government in helping maintain a code of IT ethics:

> The government's attitude is essential in maintaining IT ethical conduct. For example, discourag[ing]... software piracy and educat[ing] its people on [the vice of] hacking and virus planting...

Some participants wanted cyber policies to 'avoid moral corruption and other cyber- crimes' (transcript 9) such that users will be able to effectively exploit and narrowly focus on information on government ministries, businesses and education.

> The government could also act as regulators to supervise the use of the information highways so as to avoid any abuse of the system. For instance, laws governing e-commerce could be fine-tuned and enforced so that more companies would feel safe and be forthcoming [in] us[ing] the service.

One participant suggested that an information society is one in which its population exhibited an _information reflex_; an 'unconscious, subtle instinct to use the available technology to source for relevant and timely information to enhance life as a whole' (transcript no.2:1). For such information reflexes to develop, the participant acknowledged that the population in general should be knowledgeable about the technologies available to them, and that they should also be 'supremely confident' of the accuracy of the acquired information. A person should be able to trust all network transactions with even more secured monetary transactions. The participant argues that 'until such time when a person will release his personal information with peace of mind, that it would not be wrongly used, [the achieving of the] information society will be hindered'. While this participant feels that the government has done much in terms of ensuring timely, relevant information is available to all and that the information is reliable, the participant feels that 'more could be done'. Several suggestions for improvement include 'training for all, skills upgrading of the middle aged and the older population, campaigns, exhibitions, seminars, talks and hands-on sessions'. These minor projects could go far in helping dispel and diffuse technological fears that some sections of society might have. This participant also suggested the use of more technologically advanced methods of teaching using computers and integrated networks as educational tools. 'Businesses should be able to access useful information for decision-making, social and cultural events should be built upon available information accessed easily on-line' (transcript no.2:2): this participant feels that the government should lay the infrastructure for all of these to materialize in the near future.

The topic of laws, regulations and information policies regarding technological usage and networking is a recurring one in the transcripts from the participants. Most participants feel that more can be done in that area. For example, points of view such as:

*   government _should_ implement law and regulation
*   the government _needs_ to review existing legal framework
*   a legal and regulatory measure has _to be_ undertaken
*   reviews of laws and policies _need to be_ conducted and _should_ include

are common amongst the participants and the use of the modals _should, needs_ and _has to be_ indicate that participants feel that the government is somehow not effective in this area and that an improvements can or should be made.

### Liberalizing Internet Rules and Regulations

While many participants feel that more can be done in terms of rules and regulations for technology usage and e-commerce, there are others who believe that Singapore might be better off with an opening up and liberalization of impeding rules and regulations. Many have voiced their desire to have not only local internet service providers (ISPs) such as Cyberway, Pacific Net and Singnet, but for the government to relax the laws on Internet service providers and allow foreign players in the field, such as AOL. The greater number of ISPs will spark greater competition and lower costs of access to the Internet and perhaps better services for Singaporeans. Such events may encourage more households to get connected to the web. In moving towards a wired and global community, one participant suggests perhaps for the government to relax the terms and conditions that permits an individual to run his office out of his home in the HDB flats where the majority of the population of Singapore resides. Such a move, according to the participant, would pave the way for nurturing a secondary industry in electronic publishing, somewhat similar to the cottage industries of the industrial age.

Currently, the Monetary Authority of Singapore (MAS) has specified age and income requirements for the issuing of credit cards, such that credit cards are not as easily obtainable as some other countries. In addition to MAS, several participants have highlighted that the NETS payment facility holds a monopoly in Singapore as other companies offer no other similar facilities. In a bid to improve shopping and buying transactions over the Internet, suggestions have been made that perhaps the government should calibrate its policies such that alternative means of cashless payment systems can be devised for efficient cashless transactions over the Internet thus promoting a cashless transaction culture.

### Using the Mass Media to Create Public Awareness

Creating public awareness of information availability and information retrieval is not just for the government and schools but for the mass media as well. As one participant put it, journalism plays a vital role in building up an information society. A complete news coverage of electronic information services must be made available to every user by the library professionals in order to deliver articles on trends in the field, governmental polices and perhaps overall updates on the newest and friendliest technology. One participant suggested that the government provide a scheme that allows the purchasing of computers by household at a more affordable price so that lower income families can also take advantage of technology and the Internet in gaining access to information.

### Obstacles faced by the Singapore Government

Some obstacles for the government that the participants have observed include Singapore having a shrinking and aging population. Ms Lim Soo Hoon, Permanent Secretary of the Ministry of Community Development has spoken on this issue at the _Perspectives: 2000 & beyond_ conference ([Lee Tsao Yuan, 2000](#ref1)). She referred that a declining birth rate and a stressed 'sandwich' class who will have to support both the young and the old will begin to face a brutal economy. As such, in the years to come, Singapore will have a smaller group of young adults working harder to support an older population, thus a greater percentage of national resource and finance will have to be channelled into caring for the needs of the elderly. At present, several participants have voiced that more will have to be done for the elderly in terms of information education. Holding classes and seminars at community centres could be a solution to this issue, thus helping to assuage Internet fears for the elderly and helping them help themselves in the Cyber Age. An issue running parallel to this would be that it is not only the elderly that needs to be technology savvy and have their techno-phobias quelled but that information technology needs to be created for the disabled as well. This would give them a chance to be productive economic agents, making them less alienated from mainstream society.

Another participant warns of the disadvantages of having 'info-elitists', where the segregation of societies into the IT literate and illiterate may have undesirable social consequences of feelings of alienation by the IT illiterate, as mentioned in the discussion on the 'digital divide' above. This issue has surfaced too as the over-riding concern for Singapore as it enters the Cyber Age at the panel session of the _Perspectives: 2000 & beyond_ conference ([Lee Tsao Yuan, 2000](#ref1)). Investigating the future trends of IT in business and society, speakers such as Mr Darke Sani, managing director of Apple Computer South Asia, voiced concern over a growing 'digital divide'. Those with IT knowledge will earn more money through the net being able to buy stocks and shares over the Internet and, perhaps, earning in a month, as much as what others might earn in a year. Those who are technology aware will also know how to get cheaper goods and faster services through the Internet. Although the projected digital divide will cut across all social classes, the trend will become worrying if those who are left behind come from a single community, since the unskilled and unqualified could possibly become an unemployable class, lacking both the ability and aptitude to keep pace with changes. Although the government has assured the ratio of two students to one PC, the participants feel that more needs to be done. If it perhaps becomes unfeasible for the government to take on such a task as to supply all needy homes, where computers are needed the most, with computers, then community groups and community associations can come to aid in the situation. In a small realization of this, two schemes have been put forward. One by the Association of Muslim Professionals to give underprivileged Malay-Muslim families free computers, and the other a co-operative project spearheaded by the Malay community in Singapore to put computers in needy Indian homes - these illustrate the kind of concrete steps that similar community groups can make. Computer companies should not be myopic and support such projects since it will hopefully build clientele in the future. The crux is that no home in Singapore should be unable to face the digital future.

### Impact of the Cyber-Age and Globalisation

A crucial point raised by nominated MP Claire Chiang at the conference ([Lee Tsao Yuan, 2000](#ref1))but not raised by the participants include the challenge of the impact of globalisation and cyber-age on Singapore's social fabric in terms of social cohesion and national identity. Singaporeans today can connect with like-minded cyber communities around the world without the need to travel. This borderless world might have the impact of diluting the concept of national allegiance and loyalty to a nation state, above all other loyalties. While the majority of Singaporeans will not migrate, Ms Chiang noted that the creative minority can and will. The task ahead for the government as Ms Chiang suggested, might be to review its policy on dual citizenship and examine the push factors that cause emigration and tolerate different expectations of the good life. Instead of promoting the pragmatic, market-driven values, the government could turn to promoting heartware values that includes the exploration for moral, spiritual and aesthetic order. These moves might abate the brain drain and counter the potentially divisive element of the Internet among Singaporeans.

## <a name="real"></a>The Realization of the IT2000 Report

The various governmental statutory boards from the Singapore Broadcasting Authority to the NCB have their own rules and regulations for broadcasting and Internet use respectively regarding standardization practices and safe Internet usage. The existence of an Electronic Transaction Bill passed in 1998 have also served to address the ethical aspect of Internet usage and transactions. Common Network Services also ensure that all users perform electronic transactions in a secured and reliable environment. The services provided by Netscape, Secure Socket Layer and Microsoft . CryPOADI, include user verification, network security, billing and payment as well as user and service directories. A new Certification Authority, [ID.Safe](http://www.id-safe.com.sg/ ), launched in June 1999 by Cisco and Singapore Post promote further advancement of e-commerce through its Public Key infrastructure.

The government has acknowledged Singapore's lack of a nurturing environment for future technopreneurs and the need to foster such an environment for future e-commerce generations ([Straits Times - Home, February 16 2000](#ref2)). Despite the fact that larger governmental organizations such as national Computer Board (NCB, the National Science and Technology Board (NSTB) and the Economic Development Board (EDB), do in fact on part of the government, grand subsidies and loans to help start-up businesses in technopreneurship. With regard to the participants' anxiety of government subsidies and tax reductions in helping new technopreneurs, there are numerous online help for smaller scale businesses. One of these is the books and stationary trade making use of Booknet, an Internet trading platform that offers services such as point-of-sale, inventory management, order processing and sales analysis. The Booknet and Electronic Data Interchange Server also enable retailers to send purchase orders through the Internet and place their orders immediately. Shopnet, another electronic commerce platform offers grocery retail shops and suppliers services such as point-of-sales scanning, backend inventory control and Electronic Data Interchange. Many businesses in Singapore are also able to expand operations and scoop for opportunities via the Internet by making use of the Electronic Commerce platform put in place by the government in 1996\. For example, Small and Medium Size Enterprise (SMEs) can make use of Singapore Connect.

Backed with evidence from national projects, the Internet on Singapore and the mass media, this study is convinced that the government is more than just casually involved in bringing the country into the information age. The Infocomm Development Authority of Singapore's (IDA) island-wide events in March 2000 were aimed at making everyone, including home-makers and blue-collared workers and the disabled comfortable with using information and communication technology. The participants' response (67.1%) was that the idea that dgovernment could make the most impact in enhancing the potential of individuals, is accurate. 'Infocomm' technology refers to the family of IT and telecommunication technologies such as computers, computer programs, the Internet, e-mail, e-commerce, broadband Internet access and web-enabled mobile devices and phones. This five week long extravaganza's aim is to demystify infocomm technology for the underprivileged and inexperienced. It will also seek to expedite and enhance the infocomm qualities of those who are already cultured with such technologies such as students and professionals. Ms Yong Ying-I, IDA's chief executive officer suggested that this approach be designed to reach as many segments of society as possible. Since the digital divide is 'very real' and that "the benefits of being connected must be shared with all Singaporeans" ([Straits Times - Home, 19 February 2000](#ref2)). **eCelebrations Singapore** is part of the IDA's new three-year 'infocomm' technology initiative to raise awareness on how such technologies can enhance the quality of life and standard of living in Singapore. The ideology and lifestyle that the IDA is promoting, is a sense of urgency to motivate the inexperienced in Internet usage to be connected and stay connected and be relevant in Singapore's new economy. With S$25 million devoted to bridging the digital divide, the government is serious about initiating, enabling and grooming technophobes to become Internet aware. The funds that are committed from the IDA would pay special attention to senior citizens and special interests groups as well as the above-mentioned groups of home-makers and blue-collared workers. IDA would work with self-help groups and grass-roots groups to offer used computers, together with free Internet access and basic training to about 30,000 low-income households with combined monthly incomes of about S$2,000\. It would also work with industry and community groups to develop locally relevant content in other Asian languages and encourage the development of infocomm applications and services that catered to the different population segments ([Straits Times - Prime News, 2 March 2000](#ref2)).

The government's Infocomm 21 Masterplan launched on 4 March 2000, by Singapore's Minister for Communications and Information Technology, Mr. Yeo Cheow Tong at the Singapore Computer Society gala dinner, plans to increase net-savvy workers ([Sunday Times, March 2000](#ref2)). The key findings of the Infocomm Manpower and Skills Survey in 1999 revealed that for Singapore's economy to grow, three in four workers must be knowledgeable about Web and Internet matters by 2010, by which time information and communication workers should number approximately 250,000 or 10% of the total workforce.

## Conclusion

To achieve the targets mentioned above and more, Singapore needs to set up a world-class information and communication education system, become the regional e-learning hub and recruit foreign talent actively, including world-renowned researchers and teachers for institutes of higher learning. Foreign talent will get their work passes at a much quicker rate than others since a separate queue has been set up at the Manpower Ministry to process their applications. Singapore also has to boost computer-based schooling so that it makes up one-third of the curriculum.

As a gauge to the pace of change in today's world of information technology research, the _[World Times Inc.](#ref3)_ (2000) reported on 14 February 2000 that Sweden has edged USA out of top position in the Information Revolution. Sweden proved that a small initiative such as an Employee Purchase Scheme (EPS) could have a significant impact upon the Information Society in a short time period. The country's programme - which was a corporate initiative as opposed to a government programme - demonstrates the ability of corporations to influence the development of the Information Society. Although the USA has slipped to number two, International Data Group's Global IT Market and Strategies research anticipates that the USA, with its consistent investments in IT will enable it to return to the top. Sweden's movement however, is a proof of the fact that even the strongest Information Society must remain innovative both in terms of developing IT and integrating it into their societies. The dispersion and saturation of PCs in the computer infrastructure segment in a country does not secure the strength of a digital economy. Instead, all infrastructures such as computer, Internet, information and social, must become a stalwartly interwoven fabric that works together to support the Information Society. Sweden has understood this need for a balanced approach, an approach that Singapore has add to its original plans.

The success of bringing Singapore into the information age depends as much on people's attitudes and motivations as on the government. While the government can do so much to promote IT culture and ideology, the people of Singapore have to embrace such events and moves, to want to make IT and infocomm knowledge a part of their lives. Singaporeans have to willingly allow the permeation of such ideologies to the core of their private domains, to have the IT ideology in the crux of their daily lives, affecting the manner in which they do things and in how they think. SM Lee reminded Singaporeans that, if Singapore is to succeed in transforming the nation and economy into an information age country, then it needs its people to be more open minded and embracing of new ideas and new methods of achieving their goals ([Straits Times, Febraury 2000](#ref2)). In the information race, foreign players are competing with Singaporeans and the country needs people who will meet the challenge instantly and without hesitation. Singapore has both Hong Kong and the USA to look to for examples of risk takers. SM Lee Kuan Yew's message to Singaporeans rings clear: dare to be entrepreneurial and dare to compete in the global league brought about by the digital revolution. In the example cited by SM Lee, in the USA small investors have, over the last six months, sold out some US$51 billion worth of bonds and fixed income securities to buy dot.com companies on Nasdaq where they believe the profits are. They did this without thought of failing, abandoning standard rules of prudence to leave some money in less risky investments which will retain their value should there be a sharp stock-market correction (how many have been hurt by the correction when it came is, of course, another issue). While not to encourage hasty and ruthless investing in Internet businesses, SM Lee's view that the choice for Singapore is to be the best in every field. Otherwise Singapore will decline and for this to continue to actualise, Singapore needs its people to be top creative individuals who are enterprising and brilliant.

The Internet has changed the way people live, work and play. It has shaken up businesses worldwide, revising business structures and eliminating irrelevant middlemen parties. Countries such as Japan and Germany are also rethinking old methods of doing business in this new economy. And if exceptionally successful countries such as thesel go through paradigm shifts, then Singapore has to do so too. Singapore cannot simply produce managers and engineers as it has been doing for the last 30 years. Today, it needs a convincing nucleus of inherent and intrinsic entrepreneurial talent.

## References

*   <a name="ref1"></a>Chaudhry, A. S. and Al-Hawamdeh, S. (1998) _National Information Infrastructure development and Internet applications in Singapore._ Proceeding of the Conference on Achieving Excellence in Information Organisation and Delivery: Strategies for the 21<sup>st</sup> Century, 25-27 November 1998, Bahrain, Arabian Gulf Chapter of the Special libraries Association, pp 41-50.
*   CSIS International Communications Studies Program. (1994) _Japan and the United States: revving up for the information superhighway_, including highlights of the Japan-U.S. Information Infrastructure Symposium, June 13 1994, International Conference Hall of the United Nations University, Tokyo, Japan. Washington, D.C.: CSIS International Communications Studies Program.
*   GIIC Asia Regional Meeting and International Conference. (1995) _National Information Infrastructure for Social and Economic Development in Asia._ Organised by: the Global Information Infrastructure Commission (GIIC) National Electronics and Computer Technology Center, Thailand. The World Bank. November 28-30, 1995\. Bangkok Thailand. Ed. By Carol Ann Charles.
*   Infocomm Development Agency. (2000) [Fourth Information Technology (IT) Household Survey, 1999](http://www.ida.gov.sg/Website/IDAhome.nsf/Home?OpenForm).  Singapore: IDA.
*   Lee Tsao Yuan, ed.  (2000)  _Perspectives: 2000 &mp beyond_. Singapore: Singapore: Times Academic Press for IPS.
*   Majo, J. (Chairman). (1996) _The impact on the economy and employment._ Networks for People and their Communities. Making the most of the Information Society in the European Union. First Annual Report to the European Commission from the Information Society Forum; supplement containing working groups reports. June 1996, p5\. at [http://europa.eu.int/ISPO/policy/isf/documents/rep-96/ISF-REPORT-96A.html#chap1](http://europa.eu.int/ISPO/policy/isf/documents/rep-96/ISF-REPORT-96A.html#chap1) Accessed 03.01.200
*   National Computer Board of Singapore, (1992) _A Vision of an Intelligent Island: The IT2000 Report_. SNP Publishers.
*   <a name="ref2"></a>National IT Standards Committee (ITSC) of the Singapore Productivity Standards Board, _NII Standards,_ 24 July 1997 at [http://www.niistds.ncb.gov.sg/](http://www.niistds.ncb.gov.sg/) (12 August 1999) [Page no longer exists at 03.01.2001 - NCB is now IDA and the ITSC is a separate body - [http://www.itsc.org.sg/](http://www.itsc.org.sg/)]
*   The Sunday Times - Home, _Infocomm 21,_ 5 March 2000\. Singapore: Singapore Press Holdings, p 38.
*   The Straits Times - Home, _'Go global but keep Asian values',_ Monday 10 January 2000\. Singapore: Singapore Press Holdings, p 31.
*   The Straits Times - Prime News, _Poverty the 'ultimate threat' to man,_ Monday 14 February 2000\. Singapore: Singapore Press Holdings, p 2.
*   The Straits Times - Home, _What the players say,_ Wednesday 16 February 2000\. Singapore: Singapore Press Holdings, p 48.
*   The Straits Times - Home, _SM Lee's talk at NTU,_ Wednesday 16 February 2000\. Singapore: Singapore Press Holdings, p 44.
*   The Straits Times. _Dare to take on the world, says SM Lee,_ Wednesday 16 February 2000\. Singapore: Singapore Press Holdings, p 1\.
*   The Straits Times - Home, _Soon-surf the Net anywhere in NTU,_ Thursday 17 February 2000\. Singapore: Singapore Press Holdings, p 46.
*   The Straits Times - Home, _Up next: Dot.com-ing each home in Singapore,_ Saturday, 19 February 2000\. Singapore: Singapore Press Holdings, p 49\.
*   The Straits Times - Prime News, _You too, can go online. Don't worry about IT,_ Thursday 2 March 2000\. Singapore: Singapore Press Holdings, p 3
*   The Straits Times - Prime News, _Singapore Techventure 2000 conference,_ Saturday 11 March 2000\. Singapore: Singapore Press Holdings, p 2.
*   The Sunday Times - Review, Viewpoints. 27 February 2000\. Singapore: Singapore Press Holdings, p 42.
*   The Sunday Times - _Virtual institute to promote e-learning,_ 5 March 2000\. Singapore: Singapore Press Holdings, p 1.
*   The Sunday Times - Home. (5 March 2000) _Infocomm 21,_ Singapore: Singapore Press Holdings, p 38
*   <a name="ref3"></a>Wautrequin, J. (Chairman)  (1996)  _Influences on Public Services._ Networks for People and their Communities. Making the most of the Information Society in the European Union. First Annual Report to the European Commission from the Information Society Forum; supplement containing working groups reports. June 1996, p 35\. at [http://europa.eu.int/ISPO/policy/isf/documents/rep-96/ISF-REPORT-96A.html#chap3](http://europa.eu.int/ISPO/policy/isf/documents/rep-96/ISF-REPORT-96A.html#chap3) Accessed 03.01.2001
*   White, A. (Chairman)  (1996)  _Basic Social and Democratic Values in the Virtual Community._ Networks for People and their Communities. Making the most of the Information Society in the European Union. First Annual Report to the European Commission from the Information Society Forum; supplement containing working groups reports. June 1996, p 17\. Available at:[http://europa.eu.int/ISPO/policy/isf/documents/rep-96/ISF-REPORT-96A.html#chap2](http://europa.eu.int/ISPO/policy/isf/documents/rep-96/ISF-REPORT-96A.html#chap2) Accessed 03.01.2001
*   World Times Inc_, 2000 IDC/World Times Information Society Index_ at [http://www.worldpaper.com](http://www.worldpaper.com/) (16 February 2000).


