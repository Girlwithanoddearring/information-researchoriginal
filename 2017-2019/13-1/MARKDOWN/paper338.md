####  vol. 13 no. 1, March 2008



# Case studies in open access publishing. Number Five.  
Taking the plunge: open access at the _Canadian Journal of Sociology_



#### [Kevin D. Haggerty](mailto:kevin.haggerty@ualberta.ca )  
Editor, Canadian Journal of Sociology, Department of Sociology, University of Alberta

#### Abstract

> **Introduction.** Presents a personal account of the transfer to open access of the leading Canadian journal of sociology.  
> **Background** The _Canadian Journal of Sociology_ had established a strong position, internationally, among sociology journals. However, subscriptions were falling as readers increasingly accessed the resource through libraries and a transfer of editorship occasioned an opportunity for a radical re-assessment.  
> **Rationale for open access** Following something of a personal struggle on the part of the Editor, the rationale for open access, that is, increasing the visibility and use of the journal, was accepted. Support from the Canadian Social Sciences and Humanities Research Council becomes more central to economic viability  
> **Conclusion** Converting a scholarly print journal from _toll-based_ to open access cannot be undertaken without serious consideration of all elements of the publishing and editing processes, but the benefits to scholarship are expected to be real.



## The opportunity

I was an unlikely candidate to be the person who would transform Canada's leading generalist sociology journal into an electronic open access format. Indeed, my initial reaction to the entire concept was alarmist. I first learned about open access in a mass e-mail from the Social Sciences and Humanities Research Council (SSHRC) of Canada. If I remember correctly, the message arrived several years ago and solicited input from academics on the prospect that SSHRC would expect funded academics to publish in journals that provided readers with free electronic access to their content. Knowing a bit about the financially precarious nature of academic publishing in Canada I suspected that this requirement would devastate independent journals. Consequently, I forwarded the message to Professor Nico Stehr, then the editor of the _Canadian Journal of Sociology/cahiers canadiens de sociologie_, expressing my concerns and asking if he was aware of this development. He was not.

On the face of it, contacting Professor Stehr was a bit presumptuous as I had no official standing with the journal. That said, by that time a series of factors had conspired to ensure that I had developed a consistent pattern of involvement with the _Canadian Journal of Sociology_. The office for the journal is located in my home department of sociology at the University of Alberta. One of the founding editors was Professor Nico Stehr, who was also by far the most active person in editing and managing the journal. He had essentially served as editor since the journal was established in 1975\. I became acquainted with Professor Stehr in 1994 at the University of British Columbia, where he was a visiting fellow and I was a PhD student and also his research assistant. One of the other co-founders of the journal was Professor Richard Ericson. Richard was my PhD supervisor, co-author and close friend. Beyond those personal connections with the journal, by 2006 I had also published one article, a review essay and a few book reviews in the journal. I was also the journal's most active manuscript reviewer. Hence, despite my not having official standing I had developed an interest in, and persistent series of contacts with, the journal. It was in that context that I sent my unsolicited note to the editor to ask if he was aware of how open access threatened to destroy what he worked so hard to build.

By 2006 Professor Stehr had retired from his Canadian academic positions and was living and working full-time in Germany. He approached me in December of that year to ask if I would be willing to take-over as editor. There was considerable time pressure involved, as he wanted me to start effective January 1st. I was conflicted. My editorial experience at that time consisted of co-editing a book and also a special volume of the journal _Surveillance & Society_, both of which provided me some limited insights into the editing process. The job of editor seemed a nice fit with some of my strengths, in that I am well organized and like to try and strengthen the works of other scholars. I also have eclectic interests and am a fan of clear prose, which is not a bad trait for an editor. Nonetheless, I had no experience with the day-to-day running of a journal. Moreover, I was in the middle of my first sabbatical and was concerned about what would happen to my research and writing plans if I became editor.

After thinking about the offer for a couple of days my first question to Professor Stehr was whether the journal was financially viable. I feared that if I took over as editor and opened the books to discover that the finances were a disaster that my editorial legacy would consist of having to fold-up the publication. Thankfully, I was reassured that the journal's finances were in good shape. Today, those initial concerns about subscription revenues seems quite ironic given that I have subsequently introduced changes which effectively eliminated that revenue stream.

Encouraged by my friend Richard Ericson, I accepted the position. What follows is an abridged account of my first year as editor. It aims to accomplish several things. It provides an opportunity to outline my rationale for making a number of changes, including the decision to make the journal entirely electronic and open access. As such, it is a kind of snapshot of some of the issues currently facing editors in the rapidly evolving field of academic publishing. Hence, I hope that my recollections might prove useful to other editors who are contemplating such a move or individuals interested in becoming an editor.

## The Canadian Journal of Sociology

Thankfully, the journal I inherited was generally in good shape. The _Canadian Journal of Sociology_ is an independent peer reviewed publication, and was originally constituted as a charitable organization in 1975\. It is a generalist sociology journal, which, given the heterogeneous nature of the discipline, means we receive submissions on an extremely diverse range of topics. In addition to peer-reviewed articles we also publish review essays and book reviews. We also have _Notes on society_ and _Notes on the discipline_ sections which are not peer-reviewed but provide a venue for shorter, more topical pieces.

If we have a Canadian 'competitor' it would be the _Canadian Review of Sociology_ (formerly the _Canadian Review of Sociology and Anthropology_) and one of the original justifications for creating the _Canadian Journal of Sociology_ was to counter some of the geographic, substantive and theoretical biases that the original founders of our journal identified in the _Review_. Today, the major distinction between the two journals is that the _Review_ is an association journal and, as such, receives some funding from the Canadian Sociological Association. Members of the Association automatically receive an annual subscription to the _Review_, whereas up until 2008 they had to subscribe to the _Journal_. When I took over, our journal was funded from several sources, most notably a competitive grant from the SSHRC, subscription revenues and reprint fees.

It is a testament to Professor Stehr's hard work that the journal is recognized as a quality publication, having published well-received and often reproduced works by some of the most prominent sociologists in Canada and abroad. I was pleasantly surprised to learn that with an impact factor of .700 the journal ranks 34th of all sociology journals internationally. While this places the journal well below the dominant American venues, it does compare favourably to the _British Journal of Sociology_ (1.00) which is the main generalist sociology journal in the United Kingdom. The journal also ranks higher than a series of venerable American and European sociology journals (see Table 1) and higher than the _Canadian Review of Sociology_. While I am the first person to question the value of such high-level indictors, it is nonetheless a notable achievement that a small independent generalist sociology journal run out of a Canadian prairie province would rank so highly; particularly in light of the enormous number of sociology journals now in publication.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Impact factors for selected sociology journals**  
[Journal Citation Reports 2006]</caption>

<tbody>

<tr>

<th>Journal</th>

<th>Impact factor</th>

</tr>

<tr>

<td>Acta Sociologica</td>

<td align="center">0.256</td>

</tr>

<tr>

<td>American Journal of Sociology</td>

<td align="center">2.581</td>

</tr>

<tr>

<td>American Journal of Economic Sociology</td>

<td align="center">0.205</td>

</tr>

<tr>

<td>British Journal of Sociology</td>

<td align="center">1.000</td>

</tr>

<tr>

<td>British Journal of the Sociology of Education</td>

<td align="center">0.632</td>

</tr>

<tr>

<td>Canadian Journal of Sociology</td>

<td align="center">0.700</td>

</tr>

<tr>

<td>Canadian Review of Sociology</td>

<td align="center">0.316</td>

</tr>

<tr>

<td>Contemporary Sociology</td>

<td align="center">0.464</td>

</tr>

<tr>

<td>Deviant Behaviour</td>

<td align="center">0.691</td>

</tr>

<tr>

<td>European Sociology</td>

<td align="center">0.524</td>

</tr>

<tr>

<td>European Sociological Review</td>

<td align="center">0.607</td>

</tr>

<tr>

<td>International Sociology</td>

<td align="center">0.558</td>

</tr>

<tr>

<td>Journal of Historical Sociology</td>

<td align="center">0.433</td>

</tr>

<tr>

<td>Journal of Sociology</td>

<td align="center">0.419</td>

</tr>

<tr>

<td>Media Culture and Society</td>

<td align="center">0.418</td>

</tr>

<tr>

<td>Political Sociology</td>

<td align="center">0.488</td>

</tr>

<tr>

<td>Social Forces</td>

<td align="center">1.214</td>

</tr>

<tr>

<td>Sociological Quarterly</td>

<td align="center">0.597</td>

</tr>

<tr>

<td>Sociological Review</td>

<td align="center">0.705</td>

</tr>

<tr>

<td>Sociological Theory</td>

<td align="center">0.827</td>

</tr>

<tr>

<td>Symbolic Interactionism</td>

<td align="center">0.559</td>

</tr>

</tbody>

</table>

In 2007 the journal was being published on a contract basis by the University of Toronto Press who also handled subscriptions and mailed the journal to subscribers. Published articles are included in the main scholarly indexing services, and the entire run of manuscripts going back to 1975 is available electronically through aggregator services such as Project MUSE and Ebsco. Somewhat confusingly, when I became editor, there was also an electronic version of the journal which Professor Stehr had the foresight to establish in 1995 called the _Canadian Journal of Sociology Online_—a rarity in those early days of electronic publishing. This publication was edited by Professor Jim Conley at Trent University and was entirely open access, publishing several original contributions and a considerable number of book reviews.

When I commenced, Joanne Milson was working part-time as the editorial assistant out of the journal's main office at the University of Alberta. She managed the submissions, finances and a host of other tasks essential to the smooth operation of the journal. Her process for managing submissions had evolved to the point that we would accept submissions as email attachments and would also coordinate reviews that way. The rest of the process was, however, still done on paper and pencil. Joanne used large index cards to keep track of the details about a submission; who had been asked to review the piece, the date the review was due, and so on. This system worked, but the information was not readily available to the editor. Hence, one of the last messages Professor Stehr sent me before he stepped down included the suggestion that I consider adopting the [Open Journal System](http://pkp.sfu.ca/?q=ojs) which is produced out of Simon Fraser University. I suspect that he was recommending this as a way to handle submissions entirely electronically, but as I explored everything that the Open Journal System was designed to accomplish the seeds were planted of making the journal entirely open access.

## Transformation

In the first few months of 2007 I threw myself into learning more about the journal. One of the more disconcerting things I discovered was that many years ago our charitable status had lapsed. For the purposes of taxation, therefore, it was completely unclear what, if any, legal standing the journal had. I contacted numerous editors of Canadian journals for guidance. After extensive additional consultations with my departmental chair, university lawyers and risk assessors we concluded that the journal was a University of Alberta entity—essentially recognizing the fact that it had been founded here and operated with university support for thirty-two years. Everyone also acknowledged that as editor it was within my purview to take the journal with me if I left for another institution and that I could hand it off to a successor at another university.

The questions about governance structure highlighted the issue of oversight. Over a period of many years the previous editor had become responsible for almost everything pertaining to the journal. Although he undoubtedly sought out advice from colleagues, all decisions effectively flowed through him. While this arrangement appears to have worked fine for him, I wanted to establish a more formal oversight mechanism as I anticipated making several changes to the journal and was well aware of my inexperience. Towards that end I established the Executive Board for the journal at the University of Alberta. It consists of five sociology colleagues who have diverse theoretical and methodological orientations and who generously agreed to serve in this capacity. We met for the first time in March 2007 and I have subsequently asked individual members for advice on a range of issues.

Reassuringly, I learned that our subscription numbers were quite respectable. That said, there had nonetheless been a long-term decline in both individual and institutional subscriptions while the number of people accessing the journal electronically through library-provided subscriptions had continued to increase. Both of these trends would be familiar to many editors.

Adding to the general sense of transition, Joanne Milson informed me early in the year that she would be retiring. With her departure the journal had effectively seen a complete overhaul in its executive structure, extending all the way to the need for a new book review editor. As we interviewed candidates for the editorial assistant position I wanted to ensure that we hired someone with the technical skills that would allow us to make the journal more electronic if I chose to go in that direction. That said, at this stage my understanding of everything entailed by open access was extremely rudimentary. Things came into greater focus in the coming weeks.

In a 'back to the future' move in the spring of 2007 I hired Laura Botsford as the new editorial assistant. Laura had been the editorial assistant for the journal from 1978 to 1990\. In the interim she developed even more experience as an editorial assistant, had worked on open access journals, and become very technologically proficient. Equally importantly, she was a qualified copy editor. When I started in 2007, I learned that the copy editing of manuscripts was essentially left to individual authors, with some minor corrections being done by our publisher. Predictably, there was much to be desired in the published version of some papers. As the previous editorial assistant (Joanne) was not a copy editor I decided to rectify this situation by throwing myself into the task. While I enjoy copy editing and had romantic images of scrupulously scratching-up each article, the time involved proved to be enormous, and work on the journal was already cutting into many other things. It was a relief when Laura was able to assume this responsibility.

As noted, I was also starting to more consciously weigh the pros and cons of making the journal open access—a process I found extremely frustrating. It was a big decision, and I was simply unaware of all of the implications of such a move. To say I vacillated would be an understatement, often changing my mind during the course of a single day. To try and clarify the issues involved in such a move I invited two colleagues (Pam Ryan and Denise Koufogiannakis) from the University of Alberta Libraries to advise Laura and me on the prospect of the journal going open access. Rather fortuitously, the University of Alberta Libraries is a major supporter of open access publishing and offers a journal hosting service free of charge using Open Journal Systems. Laura and I, however, did not give our colleagues an easy time during the meeting, asking them a procession of difficult questions about the implications of such a move. Looking back, it is evident that Pam and Denise could not have answered most of those questions to my satisfaction as the answers were contingent upon their having detailed knowledge about the specifics of the journal's finances and assorted institutional arrangements. I also suspect that what I really wanted from them was an impossible guarantee that the journal could accrue all of the benefits of going open access without also bearing the risks of such a move.

At their suggestion I attended a public lecture, organized by the University of Alberta Libraries, by John Willinsky on open access publishing. Unbeknownst to me at the time, Professor Willinsky is one of the major figures in open access publishing (see [Willinsky 2006](#wil06)) and his talk drew a large audience. During the course of his presentation it dawned on me that open access is not just something one might or might not do. Instead, it is a widespread social movement informed by the normative position that knowledge should be as widely accessible as possible. Notwithstanding that undeniably appealing sentiment, at the end of the talk I was still ambivalent. I was intrigued by many of the arguments, notably that open access could increase readership and the impact of the works published in the _Canadian Journal of Sociology_. At the same time, I was troubled by the flavour of the talk which leaned towards advocacy and as such tended to elide all of the tough questions I was wrestling with, most notably how to ensure the financial viability of an open access journal. After the talk I informed Professor Willinsky that I was contemplating taking the journal in this direction, and he was (predictably) very enthusiastic. I bought a copy of his book [The Access Principle](http://informationr.net/ir/reviews/revs202.html) (2006) and started to read more widely on the topic. Momentum was undeniably building in a particular direction, but I continued to have reservations.

To try and further clarify some of these issues I travelled to Toronto to meet the journals editor for our publisher, the University of Toronto Press (UTP). She accentuated the financial risks of such a move and (also predictably) argued that open access would represent the death knell for our journal. As evidence of my continuing ability to vacillate on this topic, even at this late date the last thing I said to her before we parted was that I expected to keep the journal with the Press and planned to make the journal one of their premier publications. The next time she heard from me was some weeks later when I asked her to immediately stop taking subscription renewals for the journal as we would be leaving the Press, would no longer publish a paper edition, and would be moving to an entirely open access format. She was taken aback and told me this was an uncorrectable mistake.

Notwithstanding a series of lingering anxieties, I was now persuaded of the benefits of open access publishing. It is also the case that making an established hard copy journal entirely electronic and open access is a big move, and the risks of this move are born almost entirely by the editor. The largest anxiety concerned the financial implications of such a move. When I first contemplated assuming the editorship I consulted with my colleague Professor Jo-Anne Wallace who was then editor of the journal _English Studies in Canada_. Being a bit of a pessimist I asked her what was the most unpalatable aspect of being editor, and she replied: '_laying awake at night thinking about the journal's finances_'. I laughed at the time, but the humour started to wane as I subsequently found myself lying awake worrying about our journal's finances.

Journals have survived by charging for access to their contents, which can take the form of subscription fees or increasingly fees from aggregators who then charge libraries for exclusive electronic access to a package of journals. Going open access essentially eliminates most if not all of those revenues. However, it was always the case that most of our subscription revenues went to cover the costs associated with producing a print volume, such as printing, subscription management and postage. When I sat down to try and calculate the bare minimum we needed to keep the journal running, those numbers boiled down to the salary of our editorial assistant and some funds to occasionally purchase new technology. The cost of office space was not an issue as the Department of Sociology very generously provides the journal with an office. A competitive grant from the University of Alberta gives me one course teaching release to work on the journal. Scrutinizing my (admittedly rough) calculations I concluded that we could survive on our SSHRC funding and decided that we would go open access.

Several things now had to be resolved. It was apparent, for example, that we needed to clarify the status of the _Canadian Journal of Sociology Online_ before we could move the print version of the journal towards open access. Hence, I consulted with Professor Jim Conley who was editing that publication and had an obvious interest in these developments. At the most basic level, eliminating the print version of the journal—making it both electronic and completely open access—would mean that we would now have two online journals under the imprimatur of the _Canadian Journal of Sociology_. Hence, I proposed that we integrate the two publications, effectively wrapping-up the existing online version at the end of the year and starting to produce the other publication electronically for the first volume of 2008\. Jim was enthusiastic and also generously agreed to serve as our new book review editor.

The other issue which caused me sleepless nights concerned the aggregators. Those are the agencies such as Ebsco and Project MUSE who, on a contract basis, digitize our printed copy journal and then include those electronic articles in a package they sell to libraries. It is because of such agencies that patrons of subscribing libraries can get electronic access to the entire run of volumes of the journal going back to 1975\. With open access the issue becomes what will happen to our arrangements with the aggregators given that the journal is now available free to anyone with an internet connection? Financial considerations are involved here, as aggregators pay the journal several thousand dollars a year to include us in their holdings. More importantly for me, however, is the fact that the back issues are _only_ available electronically through such services. If they decided to effectively drop us from their holdings I feared that the previous volumes (all thirty-two years worth) would no longer be available electronically. While our journal holds the copyright for each article we publish, the aggregators have incurred the costs involved in digitizing the older works, so they effectively own those digital copies and would therefore be unlikely to share them for free. While we could digitize our back issues ourselves and post them at the journal's new site, doing so would be prohibitively costly and time-consuming.

Ultimately, I decided to move towards open access before those issues were resolved. In the interim I had learned that Google Scholar was wiling to digitize back issues of journals for free and post them on the internet, again on the condition that they would own the electronic copy. Such an arrangement would mimic our existing relationship with the aggregators, with the important proviso that after digitizing the text Google Scholar provides _free_ electronic access to the articles. While this was not an ideal scenario, I approached it as a reassuring fall-back option. If we could not come to an agreement with the existing aggregators I envisioned taking up the Google Scholar offer as a way to ensure the continuing electronic availability of our back issues.

All of these changes now had to be publicized. In drafting an editorial for a forthcoming volume of the journal I carefully tried to outline and justify the changes, cognisant aware that this was a major step in its transformation. Once these developments were announced there would be no opportunity to go back—certainly not without my looking like a fool. The editorial detailed several changes to the journal, including the use of a different academic style, the new online submission management system, enhanced copy editing, new governance structure, and so on. The largest amount of space was dedicated to outlining and justifying the decision to drop the print version and go open access. The positive environmental implications of such a move were noted, as was the ability to publish more book reviews in a more timely fashion. For me, however, the knock-down appeal of this change has always been that it would give works published in the journal a wider global audience and greater impact. When I had a clean draft of this document I send it to my friend and mentor Richard Ericson. I did so with some degree of trepidation, as he was one of the original founders of the journal and I was clearly taking a risk with something he had helped to create. Thankfully, he was wildly supportive: '_You have convinced (even) me that open access is the way to go!_' His encouragement meant a lot to me, but today it is also tinged with considerable sadness as this was the last I ever heard from Richard who died tragically a short while later.

On a more pleasant note, Richard was not the only person who supported this initiative. When the editorial was published ([Haggerty 2007](#hag07)) I also sent a copy by e-mail to all of the sociology department chairs in Canada. The profile of the move was further enhanced when the Canadian academic magazine _University Affairs_ ran a prominent article on the changes to the journal ([Tamburri 2007](#tam07)). Having taken the plunge I waited to see how my colleagues would react. Thankfully (and somewhat surprisingly) I have yet to receive a single missive from anyone who objected but have had a host of messages supporting the change. Two exchanges were perhaps most encouraging. The first was with Professor John Willinsky who praised our example as a model for other journals. It was a sentiment echoed in an email I received from Professor Craig Calhoun who is currently the head of the Social Science Research Council (SSRC) in the United States, who noted that he strongly supported the move and might use it as an example for his colleagues to emulate.

## Looking ahead

As I write this we are taking steps towards publishing our [first exclusively electronic issue](http://ejournals.library.ualberta.ca/index.php/CJS/) (see Figure 1). The content for the first few issues is now set, and we are fortunate that they contain contributions from prominent sociologists which I hope will only add to the profile of the changes to the journal. Publishing the first electronic volume brings one part of this process to a close, but a number of outstanding issues still need to be addressed.

<div align="center">![The electronic version of the Canadian Journal of Sociology](cjs.jpg)</div>

<div align="center">  
Figure 1: The electronic version of the Canadian Journal of Sociology</div>

One matter that I have not yet been able to turn my attention to concerns the journal's associate editors. We are blessed with many excellent and dedicated associate editors who review manuscripts and provide informal input on the journal. However, without completely dropping the existing cohort, it is time to add a group of new associate editors as part of the overall process of renewal and rejuvenation. As I go forward on this front I will try to balance at least five different interests. First, associate editors need to be established scholars who can provide sound judgement and guidance on manuscripts. Second, they have to be willing to actually help out. As any editor will confirm, finding willing (quality) reviewers is the most frustrating part of his or her job. This problem is compounded for a generalist sociology journal where the editor often has to seek out expertise from individuals who are not associate editors and whom they may not know personally. The third criterion is that as a body the associate editors should approximate the various forms of diversity that now characterize our discipline. Fourth, I am eager to include a younger cohort of individuals as associate editors as part of the process of rejuvenation, hopefully bringing a new generation of readers and contributors to the journal. Finally, by convention, some of these individuals will have to be _name_ scholars in order to maintain the journal's profile. This is something that is explicitly mandated by SSHRC's funding criteria, but it can sometimes be in tension with the aim of having associate editors who participate. Many _name_ individuals with prominent journals are just that, names, who are minimally involved in the journal itself.

Another development concerns the fact that the _Canadian Journal of Sociology_ is officially a bilingual journal, publishing works in both English and French. In practice, however, in recent years we have not received many French submissions and published few French articles. I hope to rectify this situation and draw more French language submissions and readers to the journal. Towards that end I recently asked Professor Daniel Béland (University of Saskatchewan) to be the editor responsible for French submissions and Professor Elke Winter (Université d'Ottawa) to be the book review editor for French books. Both have graciously agreed to take on those responsibilities.

At this date our relationship with the aggregators still remains unclear. We are talking with them about how to proceed in light of the the journal's open access status. Happily, they seem interested in keeping the reconfigured journal in their stable of journals and might be willing to compensate us (a bit) to do so. The reason for this appears to be related to their business models. Aggregators want to have as comprehensive a package of journals as possible to present to libraries in order to demonstrate the volume of material that they index. From the library perspective it is this indexing which is key, as it allows patrons to discover specific journal content from everything else in the field. The aggregators' desire to have widespread coverage, combined with the comparatively high profile of the journal, appears to make them amenable to maintaining our journal as part of their holdings and compensating us for this fact.

Now that the online journal will soon start to have real content, we must ensure that people can access this material. Individuals who use an internet search engine should have little difficulty finding such articles, but what about library patrons who use a library system to search for a specific article published in the new journal, or who do a subject search on a topic covered in one of the articles? In order to ensure that they will be automatically directed to the new _Canadian Journal of Sociology_ homepage we now need to contact all of our previous institutional subscribers and let them know about this change. Apparently librarians receive such notices on a regular basis and update their catalogues accordingly. But how do we get the word out more widely? One of the most appealing aspects of open access is that it can provide scholarship with a global reach. In order to ensure that a librarian in Kenya or Argentina who perhaps previously could not afford a subscription to the journal or to JSTOR can add the _Canadian Journal of Sociology_ to their list of offerings we will be adding our journal to the [Directory of Open Access Journals](http://www.DOAJ.org) . My librarian colleagues advise me that this is the best mechanism to inform the global library community about this change.

We must now also start to think about incorporating the works that were published in the previous incarnation of the _Canadian Journal of Sociology Online_ into the new homepage. Our plan is to copy the current content to a server at the University of Alberta and then have them linked to pages on the journal site. Always the optimist, Jim Conely reassures me that this should not be a major undertaking.

Finally, looking ahead, the most consequential thing that I must do this year is apply for SSHRC funding. Our existing grant will soon end, and everything that we have done over the past twelve months has effectively involved a gamble that the journal will continue to receive funding from SSHRC. There are good reasons to believe that the funding application will be successful. The journal has a high profile and publishes prominent works. Moreover, in the past few years SSHRC has emerged as a strong proponent of open access and the _Canadian Journal of Sociology_ has become something of a cause celebre for open access publishing. If, however, I have dramatically misread developments at SSHRC, or if the winds of change push the funding agency itself on to the federal chopping block, then we could find ourselves in a crisis. At this stage there is no going back to a print journal. All of our eggs are effectively in one funding basket and it is not clear how we could generate sufficient revenue to keep the journal running without SSHRC funding.

## Lessons learned

Although I was not aware of it at the time, I suspect that one thing which predisposed me towards open access was my experience with the journal [Surveillance & Society](http://www.surveillance-and-society.org/). One of my academic specializations is in surveillance studies and over the past few years I have been impressed with the work Professor David Murakami Wood (University of Newcastle) has done in establishing and editing this electronic open access journal. It has published excellent pieces and, more than that, emerged as a hub around which a particular intellectual community revolves. The successes of that venture undoubtedly informed my sense that open access was a viable option.

A different experience related to my surveillance studies persona probably ultimately tipped the balance towards making the journal open access. In June of 2007 I helped facilitate the week-long _Summer Seminar in Surveillance Studies_ held at Queen's University (Kingston). I was one of several individuals who provided surveillance-related workshops, seminars and field trips to a group of approximately twenty-five graduate students from around the world. As I became acquainted with these exceptionally bright and accomplished students I was struck by their technological prowess and the informational and mediated worlds they inhabit. They write computer code, have their own blogs, are denizens (and critics) of the Facebook world, read electronic magazines, and are more up-to-date on technological developments than I will ever be. These students will comprise the next generation of scholars, and it dawned on me in a very concrete way that they increasingly expect academic works to be freely available. Barriers that restrict their ability to acquire information will only serve to direct them elsewhere for more immediately available sources. Spending time with them made me realize that in the long term the _Canadian Journal of Sociology_ had to go open access or risk irrelevance.

Many lessons have been learned along the way. One of the first is that while there is an appetite for open access publishing, editors are wrestling with the types of issues I detail above. An unanticipated consequence of publicizing our transformation is that I am often now consulted by colleagues on open access issues. As an advisor I suspect that I am infuriatingly inconclusive, inevitably stressing the benefits of open access, but at the same time noting that whether this is desirable for _your_ particular journal depends on your vision for the future, but also upon the specifics of your contractual arrangements, subscriptions and cash flow. And while the devil is in the details, it is also apparent that all of these things cannot be completely worked out in advance. Moving the journal to open access was a calculated risk, but it was still a plunge into the unknown. I did not tie up every loose end or answer every lingering question before committing to the change, and if I had tried to do so I suspect that the change would never have occurred.

Another lesson I have learned is one that has been learned repeatedly by administrators the world over: how invaluable it is to have dedicated assistance and support. This has come in several different forms. My editorial assistant, Laura Botsford, has been a blessing. While she is technically in my employment, she approaches her tasks with the vigour and commitment of someone who is involved in a collaborative venture. At the institutional level, I have been fortunate that the University of Alberta Libraries support open access as part of their business model. Their programmers and technicians have dedicated the time and energy necessary to help us switch to a new format. As further evidence of this commitment, in the past six months the University of Alberta Libraries hired Leah Vanderjagt. One of her main responsibilities is to assist the open access journals on campus and I have been lucky to have Leah to whom I can direct all manner of idiosyncratic questions. Such support is certainly not the norm at other universities.

Looked at differently, however, I have not been so much a beneficiary of good luck, but have capitalized on the type of infrastructure, priorities and culture that should characterize a prominent research university. Repeatedly, I have heard anecdotes from editor colleagues who lament continually bumping up against an administration that does not recognize the importance of editing academic journals, and is consequently obtuse as to the need to provide even rudimentary support. The exact opposite has been the case at the University of Alberta. Everyone I have encountered recognizes the contribution made by quality journals, and the centrality of editors in ensuring their success. Moreover, they are attuned to the prestige value that can come from having such journals housed at your institution. Wherever reasonably possible, my university has provided the types of support (financial, institutional, moral) required to run a journal. If I was at a lesser university where the value of research and publishing was not as highly regarded I suspect that such assistance would not be as forthcoming.

All of that said, it would still have been tremendously easier to just start an entirely new electronic open access publication than make the transition from an existing print journal. Anyone who establishes a new open access journals does not have to deal with existing subscribers, publishers, aggregators, funding agencies, indexing services and other institutions. At the same time, our situation has undeniable advantages. We have the imprimatur and history of an established publication that will make it easier (hopefully) for us to secure funding, quality submissions and reviewers. History, name recognition, reputation and impact factors are on our side whereas they are not available to an entirely new journal.

Some of the more general lessons I have learned over the past twelve months concern publishing and scholarship. Editing a journal is akin to running a small independent business. Even with informal advisors and a formal Executive Board, it is still the case that every decision relating to the journal essentially still runs through me-from the big picture issues to the minutia of whether we should capitalize the word _Internet_. People continually ask how much time it takes to do this work. In my case the answer is _a tonne_. The more general answer is _it depends_. The time commitment is ultimately contingent on what an editor wants to accomplish. Maintaining the status quo can be time consuming, but with adequate support it should not be overwhelming. It was introducing changes that has eaten up an inordinate amount of time, one of the reasons I suspect that more journals have not yet moved in this direction.

The editorship has also taught me a good deal abut my discipline and my colleagues, both good and bad. I have been reminded of the harried existence of many academics, with many potential reviewers often indicating that they would love to help but are simply overwhelmed. Even in such an environment, however, many colleagues who have their own pressing research, administrative and family priorities have nonetheless been tremendously generous in donating their time and energy.

I have also been exposed to the unbelievably eclectic nature of sociology in a way that mitigates against the academic tendency to become an expert in an ever-more-narrow specialization. It has been fascinating to learn about fields I knew nothing about and to be introduced to colleagues working on innovative projects. Over the past twelve months I have acquired a few more grey hairs and lost a good amount of sleep. I have also met excellent scholars and generous colleagues. And while editing is often characterized as a thankless task, I have been thanked many times by individual contributors, the wider sociological community and advocates for open access. What else could you hope for?

## Acknowledgements

Many people helped make the journal open access and some are mentioned by name in this paper. I would also like to thank the Chair of my department, Professor Harvey Krahn, for all of his support in this process. I would also like to apologize to the many other individuals who have been such a help but who I could not personally recognize due to space restrictions.

## References

*   <a id="hag07" name="hag07"></a>Haggerty, K.D. (2007). Change and continuity at the Canadian Journal of Sociology/Cahiers canadiens de sociologie. _Canadian Journal of Sociology_, **32**(3), v-xii.
*   <a id="tam07" name="tam07"></a>Tamburri, R. (2007, October). [Leading sociology journal abandons print edition](http://www.webcitation.org/5VaLO1yf7). _University Affairs_, No. 31\. Retrieved 13 February, 2008 from http://www.universityaffairs.ca/issues/2007/october/socio_journal_01.html (Archived by WebCite at http://www.webcitation.org/5VaLO1yf7)
*   <a id="wil06" name="wil06"></a>Willinsky, J. (2006). _The access principle: the case for open access to research and scholarship._ Cambridge, MA: MIT Press.

