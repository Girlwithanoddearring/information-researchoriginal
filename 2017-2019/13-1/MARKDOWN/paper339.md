####  vol. 13 no. 1, March 2008


# Household information practices: how and why householders process and manage information



#### [Bryan Kalms](mailto:success@atrax.net.au)  
School of Information Technology and Electrical Engineering, University of New South Wales at the Australian Defence Force Academy, Northcott Drive, Canberra, ACT 2600 Australia

#### Abstract

> **Introduction.** This paper reports the findings of an exploratory study investigating how and why a household takes charge of its information. The household has always been a place of information consumption and management. While much is known about the household as a consumer of media and adopter of technologies, little is known about how the household as a collectivity, as an information system, processes and manages the information it receives each day.  
> **Method.** Twenty-eight householders in eleven Australian households were interviewed about their information practices at home and the information and information-related devices and services used by them were identified. Interviews were recorded and transcribed for analysis.  
> **Analysis.** Data collection and analysis used Schatzman's dimensional analysis methodology. Interviews were analysed graphically to identify concepts and dimensions while four metrics were developed to measure the household information environment.  
> **Results.** Household information practices emerge from the interaction of two enabling processes and nine dimensions of action.  
> **Conclusions.** Household information practices represent a negotiated order for processing and managing information in a household.


## Introduction

A household is,

> ...a group of two or more related or unrelated people who usually reside in the same dwelling, who regard themselves as a household, and who make common provision for food or other essentials for living; or a person living in a dwelling who makes provision for his/her own food and other essentials for living, without combining with any other person'. ([Australian Bureau of Statistics 2002: 115](#abs02))

By this definition a household includes, e.g., group houses, aged care facilities, single parent homes and extended families.

The household, whatever its composition, has been a place of information consumption and management since mankind first began living in caves. In the intervening years, both the technologies that mediate information and the information itself have undergone dramatic and constant change. Cave walls gave way to clay tablets, then papyrus scrolls and paper, and now we have electrons and pdf files. Naturalistic information, i.e., information emanating from the physical environment, including speech, has been supplemented by text messages, e-mails and action thrillers.

While much is known about the modern household as a consumer of media and adopter of technologies, little is known about how the household, as a collectivity, processes and manages the information it receives each day; that is, about how the household operates as an information system.

Indeed, we have been unable to find any research on the household as an information system. An extensive search of academic databases and the internet over several years revealed 26,000 potentially relevant papers but none of these specifically addressed the household as an information system. However, several authors sought this knowledge, primarily as a means of better understanding how a household adopts information and communications technologies. For example, [Venkatesh (1996: 51)](#ven96) observes, '_for a thorough understanding of the household adoption and use of new information technologies, we need a theory of household behaviour and a theory of household interaction_'.

Ten years later Haddon notes:

> ...to understand both adoption and use [of information and communications technologies in the household] we need to appreciate the negotiation and interaction between household members and the politics of the home that lie behind conflicts and tensions on the one hand and the formation of areas of consensus on the other. ([Haddon 2006](#had06) : 197)

In the same vein, where models of household information practices have been proposed, they tend to be directed towards understanding the place of technology in those practices [(Venkatesh 1996)](#ven96) rather than the use and management of information _per se_.

Despite these and similar calls for an understanding of the household as an information system, little effort appears to have been applied to providing that understanding.

Yet one might ask what benefits one would obtain from such an understanding, apart from informing a theory of the adoption of information and communications technologies in households. Understanding household information practices is important, I believe, for several reasons. First, there is currently no theory or model for how or why a household processes and manages its information. Secondly, the household is one of many types of organization. Insights gained from understanding the information practices of a household may be generalisable to other types of organization. Thirdly, a householder spends much of his or her time at home engaged in information-related activities; up to three hours a day according to the [Australian Bureau of Statistics (1998)](#abs98). Anecdotal evidence, not to mention our personal experiences, suggests that much of this time is spent processing and managing information and that these activities are too often frustrating and annoying for all sorts of reasons, which need to be understood if we are to devise better ways of handling information (including through improved information and communications technologies) and minimising the loss of important information. Finally, we may discover that how a household processes and manages information is related to other aspects of a household, such as family dysfunction or the level of social isolation of a householder.

This article reports the findings of an exploratory Australian study (conducted as part of a PhD program of study) into the operation of the household as an information system. It outlines a theory of how and why the household, as a collectivity, processes (filters, uses, keeps and throws out) and manages (maintains and looks after) the information that flows into it each day, irrespective of whether the information relates to the household or an individual householder: its 'household information practices'. In doing so, the research looks at household information practices from the perspective of a householder.

We begin with a brief analysis of the context of household information practices before outlining the methodology and methods used for this study. Then the information management challenge confronting a household is discussed and, finally, there is a statement of the theory of household information practices.

## Context of household information practices

Household information practices occur against a background, or structure, of a constantly changing context of information, information-related devices and services and society. Exploration, and hence our understanding, of each of these elements is well represented in the literature.

Our relation with, and the changing nature and quantity of, information is explored in various terms, for instance:

*   the digital divide (e.g., [Compaine 2001](#com01); [Foster 2000](#fos00); [Loader 1998](#loa98))
*   information overload (e.g., [Edmunds & Morris 2000](#edm00); [Noyes & Thomas 1995](#noy95); [Tidline 1999](#tid99))
*   media consumption (e.g., [Green & Davenport 1998](#gre98); [Schement & Curtis 1995](#sch95); [Silverstone 1994](#sil94))
*   information behaviour (e.g., [Bonner _et al_. 1998](#bon98); [Wilson 2000](#wil00))
*   our information age (e.g., [Feather 2000](#fea00); [Hobart & Schiffman 1998](#hob98))
*   information (and knowledge) management (e.g., [Choo 1998](#cho98); [Davenport & Prusak 1997](#dav97))
*   personal information management (e.g., [Whittaker and Sidner 1996](#whi96); the [Keeping Found Things Found](#kftf) project)

Information-related devices and services have been widely studied in terms of the technologies themselves and their development, adoption, domestication and social impact (e.g., [Avery & Baker 2002](#ave02); [Checkland & Holwell 1998](#che98); [O'Brien _et al_. 1999](#obr99); [Venkatesh 1996](#ven96)).

The construction, maintenance and changing nature of society is the domain of various disciplines like sociology, anthropology and economics, each with its own extensive, related literature (e.g., [Aldous 1977](#ald77); [Australian Bureau of Statistics 2005a](#abs05a); [Bell 1973](#bel73); [Castells 2000](#cas00); [Dalton 1968](#dal68); [Diamond 1998](#dia98); [Douglas & Isherwood 1996](#dou96); [Keesing & Strathern 1998](#kee98); [Narotzky 1997](#nar97); [Weber 1964](#web64)).

This literature paints a picture of a constantly evolving and interacting structure or context that both affects households and is affected by them. The interactive process is akin to what [Venkatesh (2006: 194)](#ven06) calls '_coevolution of the artefacts and the users_'.

## The research

As mentioned in the Introduction, the aim of this research was to conduct an exploratory study into household information practices. In particular, the objective was to develop what ([Gregor 2002](#gre02)) calls a '_theory for understanding_' that would illuminate how and why household information practices occur. (Gregor contends that there are five types of theory based upon their intended purpose: '_(i) theory for analysing and describing, (ii) theory for understanding, (iii) theory for predicting, (iv) theory for explaining and predicting, and (v) theory for design and action._') At the same time, the theory was intended to provide a basis for further research and the development of more comprehensive theory.([Gregor 2002: 2](#gre02))

### Methodology

The dimensional analysis methodology of [Schatzman (1991)](#sch91) was used for two reasons. First, it specifically seeks to identify the _all_ that is involved in a phenomenon: the processes, activities, strategies, considerations and motivations affecting household information practices. As such, it is ideal for exploring an unstudied or poorly understood research area. Secondly, dimensional analysis comes from the interactionist theoretical perspective which places the actor (or householder in our case) firmly at the centre of analysis. This was considered essential if the theory was to encompass the deeper matter of why information was processed and managed. One consequence of this approach is that the terminology of the theory is that used by the householders, rather than that found in the literature.

In exploring a phenomenon, dimensional analysis seeks to identify the _range_ of behaviour and factors involved, not typical or normal behaviour. Data collection and analysis occur concurrently, facilitated by theoretical sampling: selecting subjects or participants because they are likely to inform the developing theory, not on the basis of representativeness or randomness. This allows potentially relevant behaviour and factors identified in one person to be explored in the next and compared with later ones. During analysis, identified behaviour and factors are abstracted into concepts, grouped into integrating sub-dimensions and dimensions, and ultimately formed into a substantive theory. In this way, the theory emerges from the data and is contextualised through an integrating perspective. For this research, the perspective that best accounted for the data was _living with information_.

Explication of the theory is structured by using Schatzman's explanatory matrix:

> An explanation, after all, tells a story about the relations among things or people and events. To tell a complex story, one must designate objects and events, state or imply some of their dimensions and properties. - that is, their attributes - provide some context for these, indicate a condition or two for whatever action or interaction is selected to be central to the story, and point to, or imply, one or more consequences. To do all this, one needs at least one perspective to select items for the story, create their relative salience, and sequence them. Thus, "from" perspective, "in" context, "under" conditions, specified actions, "with" consequences, frame the story in terms of an explanatory logic... (Schatzman 1991: 308)

### Methods

Data were collected in two ways: by questionnaire and interview.

Each householder interviewed (except one) first completed a questionnaire nominating which of the 190 types of information ([Appendix 1](#App1)) and 70 types of information-related devices and services ([Appendix 2](#App2)) commonly found in a household they used and how frequently they did so. (Information-related devices and services were considered important because of their role in mediating interactions between householders and information.) The questionnaire also collected basic demographics about the household and each householder. This was followed by a semi-structured, group interview with (where possible) all members of the household lasting 60-90 minutes. Interviews generally began with the question, 'Tell me what happens to information in your household' and went on to explore the theme for that household (identified through theoretical sampling) as well as issues arising during the interview.

Analysis of the questionnaires resulted in four rather crude, but useful, metrics. 'Information variety' and 'information-related devices and services variety', respectively, measured the range of information and types of information-related devices and services used in a household; 'information use' and 'information-related devices and services use' measured how frequently each type was used in a household. Frequency of use of information was considered to be a proxy indicator for the amount of information processed by a household and its relative preponderance within the household. Use of information-related devices and services was considered a proxy for the relative importance of a technology or service in a household as both a source and mediator of information.

Interviews were transcribed and then analysed graphically using a graphic software application ([OmniGraffle](http://www.omnigroup.com/applications/omnigraffle/)). This gave a rich-picture-like description of each interview, its concepts and the information processes used along with their inter-relationships. Further graphics were used to consolidate concepts into sub-dimensions and dimensions and to explore alternative perspectives.

Dimensional analysis allows data to be collected from any source. Accordingly, the first household was selected because the householders were known to me and had previously told me that they had resolved the problem of information overload. Subsequent households were selected theoretically to explore emergent themes and issues, as well as different types of households, e.g., an aged care facility.

It quickly became clear that each household interviewed possessed a particular combination of factors that affected household information practices, e.g., household composition and/or disability and/or demographics. Each household also had largely similar kinds of information-related behaviour. Because of this, development of concepts (conceptualisation), dimensions and theory was rapid. By the eighth household, theoretical saturation was firmly established; no new concepts were identified. Data collection continued to confirm and densify the emergent theory and ceased after sampling eleven households with twenty-eight householders.

## The household information challenge

If we are to understand how and why a household processes and manages its information, we need first to understand what its members (the householders) consider to be information. Then we need to understand the range and quantities of information held and processed, i.e., the household information environment.

### The nature of information

What is information? There is no simple answer. Machlup & Mansfield ([1983](#mac83)), for instance, identified and reviewed around forty different academic disciplines and fields dealing with information, each essentially claiming it as their own and defining it in their own terms. Holgate ([2002](#hol02)) offered about a dozen approaches to the concept of information and Capurro & Hjørland concluded their review of the status of information with the comment, 'There are many concepts of information, and they are embedded in more or less explicit theoretical structures' ([2003](#cap03): 396).

To overcome this situation, each householder was required to employ his or her own view of what constituted information. Additionally, for the questionnaire, the concepts information (however defined) and information object were conflated. This approach was an acknowledgement that information is usually embedded in some object (an information object) and that this object is what is processed and managed by a householder. (The only exception to this is transient information: information (content) that has not been recorded or captured in an information object, e.g., live speech.) Accordingly, the list of information types used in the questionnaire covered both transient and object-based information.

Analysis of the questionnaires and interviews revealed that, for all householders, information in the form of information objects was the centre of their information processing and management; as one householder commented '_...I have four filing cabinets in this household and they are jammed full cause I keep records of all this stuff_'. Thus, transient information, such as a television programme, might be converted to an object (videotape or DVD) for keeping and later enjoyment; appointments would be noted on a calendar for safekeeping and/or publication to other householders; and, notes of a public lecture would be carried around in a personal digital assistant (PDA) for easy access.

Further, householders overwhelmingly considered information to be something that was useful to them: by definition, if it wasn't useful, it wasn't information: '_Something useful_', noted a female householder. Such a simple and straightforward approach to the concept of information is not readily found in the literature on the nature of information.

Information then, for the purposes of a theory of household information practices, is information-as-object, as favoured by [Buckland (1991)](#buc91) in his discussion of information. But these information objects must be useful to a householder.

### The information environment

If information is useful information objects, what are the objects that constitute a household's information environment and what information-related devices and services are used to interact with that information? No clear picture of the information environment of a (typical) household can be gauged from the literature. What measures are available are largely provided by national statistical agencies, but these are extremely limited and cover such things as time spent in specific information-related activities like watching television or reading ([Australian Bureau of Statistics 1998](#abs98)); the number of books published ([Australian Bureau of Statistics 2005b](#abs05b)); and, the use and penetration of selected information-related devices and services in households ([Australian Bureau of Statistics 2006](#abs06)). There is nothing (at least in Australia) on the quantity of junk mail, letters, newspapers, e-mail (just data transmission ([Australian Bureau of Statistics 2005c](#abs05c))), transaction receipts and so on entering, or found in, a household. In short, there are no data on the majority of the types of information, useful or otherwise, making their way into our homes every day, whether intended for personal or household use. Thus, the questionnaire results provide the best picture of the information environment of Australian households.

Analysis of the questionnaire responses revealed that within the same household, each householder reported different information and information-related devices and services variety. That is, they each had a different understanding of the information, information-related devices and services available in their household and each used a different sub-set of the total found in their household. Similarly, they each reported different levels of information and information-related devices and services use, each interacting with their sub-set according to their own timelines. These results were also repeated at the household level: different households had different information and information-related devices and services variety and different levels of use.

Overall, this meant that the information environment of each household was unique. Some environments were richly varied with extensive use while others were not. The most constrained environment was that found in an aged care facility.

Setting aside the aged care facility, there was no obvious cause for the differences in variety and use, notwithstanding the small sample size. Having young children in a household seems to be one factor; income, education level, employment type and household location seem to be irrelevant. During the interviews, many of the householders offered their own reason for these differences, primarily the interests of a householder and their personality. Further analysis confirmed this to be so. But this is only one dimension of the _all_ that is involved in processing and managing information in a household.

## The theory of household information practices

Analysis of the data reveals that household information practices (processing and managing information in a household) consist of two enabling processes and nine dimensions of action. In combination, these processes and dimensions determine which householder will take charge of what information, how they will process and manage that information and how much the other householders will be involved. Thus, households do not manage information, individual householders do, either on their own behalf or on behalf of another member of the household.

### The enabling processes

Figure 1 illustrates the basic process by which household information practices emerge. Where information is likely to be meaningful to a householder they will offer to take charge of it (process and manage it). This offer is negotiated with the other householders who consider all the dimensions of action in deciding if they will accede to the offer. Once agreed, the responsible householder devises their individual information practices, which combine across all householders to produce household information practices. Responsibilities may be (re-)negotiated after a trigger event or problematic situation occurs that forces a householder to review their individual information practices.

<div align="center">![Figure 1: Emergence of household information practices](p339fig1.jpg)</div>

<div align="center">  
**Figure 1: Emergence of household information practices**</div>

Thus the two enabling processes are _taking charge_ and _negotiating_.

#### Taking charge

An individual householder comes to take charge of information in one of three ways: by assumption, by caring or by gap filling.

In _taking charge by assumption_, a householder assumes responsibility for processing and managing (and often using) selected information because that information is useful to them as an individual, that is, it fulfils a role for them personally. This is most clearly illustrated when householders takes charge of their personal information. Alternatively, the information may be associated with a role occupied by a householder and information processing and management is part of exercising that role, e.g., being 'wife' or 'owner' of the computer. The husband in one household observed, '_Well, [my wife] looks after a lot of social type information, whereas I take care of the financial..._'. To which one of the sons replied, '_It's an ethnic thing._'

_Taking charge by caring_ occurs when a householder processes and manages information on behalf of another householder (or someone outside the household) who is unable to take charge of that information themself, e.g., a child, invalid parent, disabled or aged spouse. Commented one woman:

> ...I would rather [my husband who suffers from Multiple Sclerosis] concentrated on things he enjoyed doing more than worried about silly little you know the day-to-day stuff that causes stress if you find you can't focus on it for very long.

_Taking charge by gap filling_ occurs when a householder, who is not interested in the information, takes charge of it because no-one else will and the consequences of not doing so are unacceptable to them. Thus, one householder may end up paying all the bills, not because they want to, but because it is important for them that the household continue receiving electricity, gas and the phone service. So they take charge of, and pay, the bills rather than miss out on these utilities. One wife observed in relation to her gap filling: '_No, he's just being plain lazy [in not doing day-to-day information management]. It's a bit like he won't do the vacuum cleaning because he doesn't like the noise but does that mean to say that I like the noise? No!_' This way of taking charge may also be associated with a particular role within a household.

In these ways all the information flowing into a household is nominally processed and managed by someone in the household. Each responsible householder develops their own ways of processing and managing the information for which they take charge: their own individual information practices. Often, these practices involve more than one householder so creating _open_ practices, as when the bill payer keeps the paid bill in someone else's filing cabinet. Just as usual though, individual information practices are _closed_, involving only the responsible householder who can do whatever they like with the information.

When contemplating taking charge of information in any way, a householder weighs up the nine dimensions of action to ensure that the value they obtain from taking charge, i.e., a fulfilled role of information, is worth the effort of taking charge.

#### Negotiating

Taking charge in any of these ways is not totally a unilateral action: a grab for power, exercise of authority or imposition of self-interest. Every endeavour to take charge is negotiated between the householders, be the endeavour related to a particular type of information or the chance to watch a favourite movie on television. Not all negotiations, however, are obvious or explicit. Many are tacit, often requiring the other householders to object to the proposed endeavour rather than specifically agree to it. Sometimes, non-negotiation-based techniques are used, e.g., one householder may get tired of processing and managing all the tax-related information and insist that the other householders look after their own:

> So I just took [doing my husband's tax] on by default because I could do it quickly and easily and was used to it. But then it sort of got to a point where I said, "No, I'm jack of doing all of this. You should be doing some of it". And so we had a shift of responsibilities...

Negotiations usually occur whenever a problematic situation arises, circumstances change or a trigger event happens within a household, e.g., a child leaves home, couples separate, a new information-related device or service is acquired, a favourite television show needs to be recorded. Through negotiation, such situations are resolved and routine information practices are established for both the individual householders and the household as a collectivity.

By way of these negotiations, household information practices emerge. In particular, they represent a negotiated order: '_an informal structure ... in which the involved parties develop tacit agreements and unofficial arrangements that enable them to carry out their work_' ([Day & Day 1977: 130](#day77)). The negotiated order of household information practices consists of the individual information practices of each householder, the interactions between those practices and any rules established within a household for the maintenance of those practices. The process of establishing and maintaining household information practices as a negotiated order is illustrated in Figure 2.

<div align="center">![Figure 2: Household information practices as a negotiated order](p339fig2.jpg)</div>

<div align="center">  
**Figure 2: Household information practices as a negotiated order**</div>

Over time, the negotiated order, and hence household information practices, change reflecting solutions to more problematic situations, changes to the rules and changes in the context of household information practices (information, information-related devices and services, and society).

According to [Strauss (1978)](#str78), a negotiation consists of three interacting components as illustrated in Figure 3\. Central is the negotiation itself which includes all the transactions and interactions between the negotiators: doing deals, making compromises and the like. The negotiation structural context is the larger context within which a negotiation occurs; it is generally external to the negotiation. And finally, the negotiation context is the set of factors that directly affect a negotiation.

<div align="center">![Figure 3: Strauss's components of a negotiation](p339fig3.jpg)</div>

<div align="center">  
**Figure 3: Strauss's components of a negotiation**</div>

In negotiating household information practices, the nine dimensions of action contribute to these components as shown in Figure 4\. Dimensions over which a householder generally has little control (influencer dimensions) are part of the negotiation structural context. Dimensions directly affecting a householder (direct affect dimensions) constitute the narrower negotiation context, and individual information practices are the outcome of the negotiation. Sitting behind theses dimensions is the ever-present, constantly changing context of household information practices.

<div align="center">![Figure 4: The dimensions of action and the negotiation model](p339fig4.jpg)</div>

<div align="center">  
**Figure 4: The dimensions of action and the negotiation model**</div>

As in taking charge, each householder engaged in a negotiation considers all the dimensions of action when deciding their position.

Of course, this analysis ignores issues like power within a household, negotiation skills and previous negotiations, all of which affect a negotiation. Such issues must also be considered when analysing and understanding a negotiation. However, they have been omitted here to highlight the place of the dimensions of action in establishing household information practices as a negotiated order.

### The dimensions of action

Table 1 lists the nine dimensions of action for processing and managing information and their sub-dimensions as revealed from analysing the interviews and questionnaires. A dimension, and its sub-dimensions, represent factors, issues, circumstances (considerations) taken into account by, or affecting, a householder when interacting with information. Hence, the designation _dimensions of action_. These dimensions represent the viewpoint of a householder coming to live with information as part of their daily life. Therefore, they include the impact on a householder of dealing with the daily flow of information.

<table width="90%" border="1" cellspacing="0" cellpadding="6" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: The dimensions and sub-dimensions of action**  
</caption>

<tbody>

<tr>

<th>Dimension</th>

<th>Sub-dimension</th>

</tr>

<tr>

<th colspan="2" bgcolor="#FCF972">**Influencer dimensions**</th>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Accessibility of information: the extent to which information provided to a householder can be accessed by the householder</td>

<td style="width: 70%; vertical-align: top; text-align:left">Nature of the information: the characteristics of information that make it easy for a householder to use and understand. Specific characteristics include:  

—for aural information: the speaker's accent, speed of delivery, enunciation and pronunciation  
—for visual information: relative use, and type, of images and words;  
—for textual information: language style and complexity, design and layout;  
—for information collections: their organization.  

Technology of information: the appropriateness, availability and ease of use by a householder of the devices and services needed to use the provided information</td>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Considerateness of information provider: the extent to which an information provider is responsive to the information needs and preferences of a householder</td>

<td style="width: 70%; vertical-align: top; text-align:left">Information withholding practices: the activities of an information provider that limit the information it provides to a householder.  

Respect for a householder: the extent to which an information provider respects the circumstances and information needs and preferences of a householder when providing them information.</td>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Flow of information: the amount of information from all information providers that a household must process each day</td>

<td style="width: 70%; vertical-align: top; text-align:left">Information gathering arrangements: the arrangements made by a householder to obtain information (information _pull_ arrangements).  

Kept information: information received by a household that has not been destroyed, given away or thrown out.  

Open channels for information: the publicly accessible means, or channels, by which a household accepts information (information _push_ mechanisms).</td>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Having a disability: a householder within a household is suffering a disability.</td>

<td style="width: 70%; vertical-align: top; text-align:left">Extent of disability.  

Nature of disability: includes ageing.</td>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Life!: the activities associated with daily living that affect a householder's interactions with information</td>

<td style="width: 70%; vertical-align: top; text-align:left">Daily events: activities that occur from time-to-time and over which a householder has no, or limited, control, that impede a householder from effectively interacting with information.  

Demographics of a household: the number and ages of householders and the relationships between them.  

Household resources and facilities: the physical and financial assets available to a householder to support their interactions with information; they include the design of a house and the household's information-related devices and services.  

Work: the impact of employment outside a household on a householder's interactions with information.</td>

</tr>

<tr>

<th bgcolor="#99FFCC" colspan="2">Direct affect dimensions</th>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Being a householder: the characteristics of a householder as a person that influence their interactions with information</td>

<td style="width: 70%; vertical-align: top; text-align:left">Being interested: the activities, hobbies, concerns and philosophies of a householder that provide a focus for their interactions with information.  

Being skilled with information: the skills, competencies and abilities of a householder in processing and managing information.  

Exercising a role: the roles occupied by a householder within their household.  

Imagining oneself: the way a household sees itself.  

Personality: 'the unique psychological qualities of an individual [householder] that influence a variety of characteristic behaviour patterns (both overt and covert) across different situations and over time ([Gerrig & Zimbardo 2002](#ger02)).  

Preferring selected information: the nature of, and technologies for, information that are preferred by a householder.</td>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Fulfilling a role for information: using information for a meaningful purpose</td>

<td style="width: 70%; vertical-align: top; text-align:left">Connecting generations: using information to establish and maintain a sense of connection with forebears, descendants and a community at large.  

Experiencing life: using information as part of the expression of life.  

Living your values: using information to establish and maintain a sense of identity or self-image and to be true to that identity.  

Managing life: using information to arrange, coordinate or conduct the activities of life.  

Not missing out: keeping or using information just because of its potential usefulness.</td>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Losing information: the act of, and response to, not having information</td>

<td style="width: 70%; vertical-align: top; text-align:left">Bearing the consequences: the impact on a householder of not having information; consequences may be potential, real or perceived.  

Having lost information: not having, or not being able to access, information because it is destroyed, inaccessible, misprocessed or withheld.  

Responding to lost information: taking action to reduce the impact of losing information.</td>

</tr>

<tr>

<th colspan="2" bgcolor="#ACFBFB">Individual information practices</th>

</tr>

<tr>

<td style="width: 30%; vertical-align: top; text-align:left">Managing information (individual information practices): the actions and strategies used by a householder when processing and managing information</td>

<td style="width: 70%; vertical-align: top; text-align:left">Filtering information: reviewing information to decide what should happen to it.  

Going without information: choosing not to fulfil a role for information by not having information.  

Improving the accessibility of information: making it easier to use or retrieve information.  

Protecting information: improving the physical security of information against potential loss by destruction.</td>

</tr>

</tbody>

</table>

Dimensions interact and operate within the broader structural context of household information practices, as shown in Figure 5\. Interactions may be in a direct relationship (an increase/decrease in the level of one dimension produces a comparable increase/decrease in the other (represented by + sign)) or an inverse one (an increase/decrease in one produces a decrease/increase in the other (- sign)). Sometimes the relationship can be direct or inverse depending on the householder (∞ sign); thus some householders do not always learn from their experiences of losing information. Relationships between 'Being a householder' and other dimensions are not shown as this dimension affects all the others except 'Considerateness of information provider'. This latter dimension is outside the control of a household, but householders do attempt to influence information providers to make them more, or less, considerate, e.g., by asking to be removed from mailing lists.

<div align="center">![Figure 5: Interactions between the dimensions of action](p339fig5.jpg)</div>

<div align="center">  
**Figure 5: Interactions between the dimensions of action**</div>

As noted above, these dimensions are considered by each householder when negotiating to take charge of information. Thus, the model portrayed in Figure 5 operates at the level of each householder, not the household. When it operates effectively it allows a householder to take charge of his/her information (personal or household-related) through individual information practices. When the system breaks down, or when the structural context changes too much or too quickly for a householder to respond (for instance, a new computer comes without a floppy disc drive), information is lost and a householder must live without it, at least in the immediate term.

### Individual information practices

Despite the different information environments existing in households, each householder adopts a common process for dealing with information once they have assumed responsibility for it. What differs is the information to which the process is applied by each householder and the timing of activities within the process. This process, shown in Figure 6, incorporates all the dimensions of action and represents individual information practices as an information system.

<div align="center">![](p339fig6.jpg)</div>

<div align="center">  
**Figure 6: Individual information practices**</div>

This system works as follows. Because information is meaningful or has value to a householder, the focus of their individual information practices is to identify, capture and use meaningful information. As information comes into a household it is filtered and sorted, acted upon as necessary, kept, organized and protected from loss. What is not needed is thrown out and relevant information is shared or given away. As appropriate, the accessibility of information is improved, either as part of filtering or during storage and protection. From time to time information is lost, leading to mitigating action, according to the value and nature of the information lost. Where lost information cannot be replaced or compensated for, or should a householder choose to go without information, they live without information and bear the consequences of the loss. All these activities are driven by the need of a householder to fulfil a role for information, to make it meaningful in their life. Further, the activities are affected by the characteristics of a householder ('Being a householder'), any disabilities suffered by a householder or the existence of a disabled householder within a household, the vicissitudes of life and the circumstances of a householder. Finally, changes in the background context are responded to leading to revised individual information practices.

### Characteristic household information practice mode

Because each household has its own dynamics, operation of the two enabling processes (taking charge and negotiating) and the nine dimensions of action occurs differently in each household. Even so, a household has a characteric way in which it approaches its information practices, called its _characteristic household information practice mode_. This mode is based on the allocation of responsibility for processing and managing household-related and personal information.

Four modes have been identified:

*   Caring: household information practices revolve around taking charge by caring. Processing and management of the personal information of a disabled or dependent householder is involved, along with household-related information for the whole household.
*   Co-managed: householders share responsibility for household-related information using a combination of taking charge by assumption and gap filling. Each householder is responsible for their own personal information unless otherwise negotiated.
*   Controlled: one householder, in taking charge by assumption, exercises prime responsibility for processing and managing, at least, household-related information, through taking charge by assumption. This mode is often displayed in a household having a dominant householder. Domination may be due to the householder's personality, their household role or some other aspect of 'Being a householder'. Personal information of other householders may also be controlled.
*   Independent: each householder is largely left to their own information practices and control of household-related information is loosely exercised. This mode is typical of households comprised of independent householders, for instance, group households, single-person households and aged care facilities where the clients are able to process and manage their own information.

## Information practices as meaningful action

The discussion so far indicates that household information practices are an emergent phenomenon representing a self-organizing information system. But why do these practices emerge at all? Why do householders process and manage information?

### Enabling the role of information

Central to this Why? question is the role of information: the purpose to which it is put by a householder. As Brown & Duguid note,

> The ends of information, after all, are human ends. The logic of information must ultimately be the logic of humanity. For all information's independence and extent, it is people, in their communities, organizations, and institutions, who ultimately decide what it all means and why it matters. ([Brown & Duguid 2000: 18)](#bro00).

For each householder a piece of information, as noted in Table 1, is used as a means (has the role) of:

*   connecting generations;
*   experiencing life;
*   living your values;
*   managing life; and
*   not missing out.

Which role the information constitutes depends on the dimension 'Being a householder', that is, it depends on what each householder wants to use the information for. Accordingly, the same information can have different roles for different householders and even different roles for the same householder at different times in their life.

Processing and managing information, in the form of individual information practices, is part and parcel of fulfilling the role of information. For instance, information cannot be used to manage life unless the relevant information is first identified and extracted from the daily inflow of information.

In the same way, meaningful roles for information determine meaningful information which determines individual information practices. So, if a householder values being part of a future history, or has information they believe is historically important, that is, information with a role of 'Connecting generations', say, grandfather's diary, a spouse's collection of photographs, they are likely to filter this information from the daily inflow, keep it, protect it and improve its accessibility by organizing it.

Looked at another way, the individual information practices implemented are a measure of the relative usefulness for a householder of different types of information.

Ultimately, if a householder decides that they can live without information, affording it no role, they will centre their information practices on 'Going without information'. Such householders are rare. More common are those for whom the role 'Not missing out' manifests itself in information hoarding. These people value keeping information just in case it is needed some time in the future. Just knowing it is there is important. One householder observed, '_I suppose I am a background hoarder of things, you know. I am not so good at tossing out things even when there is a good reason for them to be tossed out..._' Most householders are somewhere between these extremes.

### Negotiation as meaningful action

As a negotiated order, household information practices reflect the agreements between householders about who will do what with which information and how that is to be done. Negotiations (or lack thereof) thus mediate within a household shared understandings of the role, and hence meaning and importance, of information for each householder. What is also negotiated, besides individual information practices, are the relative priorities of those roles and meanings amongst all the householders. In short, the meaning of household information practices and individual information practices (the whys?) are socially constructed and maintained.

## Constructing household information practices

Throughout the interviews, householders told stories about how they managed to extract meaning from their interactions with information. Often this required them to overcome their circumstances (represented by the context of household information practices and the influencer dimensions of action), and the competing demands of other householders, as they came to live with information. What follows is two of those stories.

### Living your values

The husband in Household 1 told how a desire to live his values permeates his individual information practices and hence household information practices. (All quotations are from this householder.)

This householder made '_a decision earlier on in my life that you want to be able to leave the world at least as good and, if possible, you want to make a difference, a positive difference_'.

One way in which this decision was put into effect was to limit the flow of information: '_There's two principles: one is that of information density and the other one is that of relevance, to areas of interest_'. Consequently, household information practices included:

*   buying '_only one [news]paper a week_';
*   listening to '_high density radio, information dense radio_' and
*   using '_a newsletter or a magazine [which] gives you a denser information flow, a more comprehensive [coverage] than a newspaper_'.

Because this householder was also politically active (to make a positive difference), he needed '_an information resource space on the specific areas of interest'._ So, '_I create paper files, hanging files and other files, related to particular areas of interest, and then use those when I need them_'. His preference for paper is because '_I was brought up in the paper age_' and, anyway, paper '_is portable, it's portable, it's easily accessible, it's file-able later on_'. Preferring paper means that information-related technologies are not very important: '_We don't have to have the latest [technology]... I always think with new technology you let those who have got the money pay for the experiments_'. Accordingly, '_We don't regularly back up the hard drive_' as most of the important information is paper-based. But this paper is not protected in any way.

Concern for a better world also prompted him to try and preserve history by protecting a collection of photographs and other documents of '_historic value_'. This information, created by a friend, was being offered to the Australian National Library for safe-keeping and community use.

For this householder, living with information means being able to use information to make the world a better place. In turn, this means controlling information overload to maximise the collection of information that supports this goal.

### Using the changing context of household information practices

Household 10 illustrated how changes in the context of household information practices can be deliberately used to change household information practices to the benefit of one particular householder.

The father in this household had, many years ago, assumed control of the family photographs because he was interested in them. He took the photos, put them in an album, '_put dates and everything on the envelopes and all that_' (Daughter). Until recently that is, when the daughter bought a digital camera. Now responsibility is split between father and daughter, on the basis of the technology of the photograph. '_As she said, they are in two formats. And I normally take care of the paper-based ones_' (Father).

Introducing this new device was not negotiated; it resulted from a unilateral action by the daughter. In taking charge of photographs in this way she challenged the established negotiated order of household information practices. Not only did this challenge trigger a reappraisal of, and variation in, household information practices, it also triggered her father to consider a similar move. '_Well, I haven't bought a good camera yet—when I do, maybe I'll switch over totally to the digital format,,, It's still quite expensive_' (Father).

But the daughter does not support any further change to the negotiated order. '_There is no point in buying two digital cameras in the one family!_' (Daughter).

Her decision to buy her own digital camera was taken against the background of male domination of technology in the household: '_[Access to the computer] goes on gender then on age_' commented one of her brothers.

For her, living with information means being able to access and control information on her terms. It means breaking the bonds of sexism which, she believes, constrain her access to technology. Until these bonds were broken, she considered her opportunity to fulfil any role for information to be limited.

## Conclusions

Household information practices are complex, ever-changing, often subtle and socially constructed. They emerge from the interactions of a householder with information, information-related devices and services, other householders and information providers. Interactions with information reflect the importance of that information to the householder. In turn, this importance is reflected in the operation of the enabling processes and the calculus ([Schatzman 1991:309](#sch91)) performed on the dimensions of action.

Household information practices thus constitute a negotiated human information system: an agreed way of ensuring that all the information entering a household is processed and managed according to the needs of each householder.

<form action="">

<fieldset><legend style="color: white; background-color: maroon; font-size: medium; padding: .1ex .5ex; border-right: 1px solid navy; border-bottom: 1px solid navy; font-weight: bold;">References</legend>

*   <a name="ald77"></a>Aldous, J. (1977). Family interaction patterns. _Annual Review of Sociology_, **3**, 105-135.
*   <a name="abs98"></a>Australian Bureau of Statistics. (1998). _How Australians use their time 1997_. Canberra: Australian Bureau of Statistics. (Cat No. 4153.0).
*   <a name="abs02"></a>Australian Bureau of Statistics. (2002). _Census of population and housing: selected social and housing characteristics_. Canberra: Australian Bureau of Statistics. (Cat No. 2015.0).
*   <a name="abs05a"></a>Australian Bureau of Statistics. (2005a). _Australian social trends 2005_. Canberra: Australian Bureau of Statistics. (Cat No. 4102.0).
*   <a name="abs05b"></a>Australian Bureau of Statistics. (2005b). _Book publishers 2003-2004_. Canberra: Australian Bureau of Statistics. (Cat No. 1363.0).
*   <a name="abs05c"></a>Australian Bureau of Statistics. (2005c). _Internet activity, March 2005_. Canberra: Australian Bureau of Statistics. (Cat No. 8153.0).
*   <a name="abs06"></a>Australian Bureau of Statistics. (2006). _Measures of Australia's progress 2006_.Canberra: Australian Bureau of Statistics. (Cat No 1370.0).
*   <a name="aih05"></a>Australian Institute of Health and Welfare. (2005). _Australia's welfare 2005_.Canberra: Australian Institute of Health and Welfare. (Cat. No. AUS 65).
*   <a name="ave02"></a>Avery, G. C. & Baker, E. (2002). Reframing the infomated household-workplace. _Information and Organization_, **12**(2), 109-134.
*   <a name="bel73"></a>Bell, D. (1973). _The coming of post-industrial society: a venture in social forecasting_. New York< NY: Basic Books.
*   <a name="bon98"></a>Bonner, M., Casey, M.-E., Greenwood, J., Johnstone, D., Keane, D. & Huff, S. L. (1998). _Information behaviour: a preliminary investigation_. Paper presented at the 1998 IMRA International Conference: Effective Utilisation and Management of Emerging Information Technologies, Boston, USA, May 17-20, 1998.
*   <a name="bro00"></a>Brown, J. S. & Duguid, P. (2000). _The social life of information_. Boston, MA: Harvard Business School Press.
*   <a name="buc91"></a>Buckland, M. K. (1991). Information as thing. _Journal of the American Society for Information Science_, **42**(5), 351-360.
*   <a name="cap03"></a>Capurro, R. & Hjorland, B. (2003). The concept of information. _Annual Review of Information Science and Technology_, **37**, 343-411.
*   <a name="cas00"></a>Castells, M. (2000). _The information age: economy, society and culture._ (2nd ed.). Vol. 1\. _The rise of the network society_. Oxford and Malden: Blackwell.
*   <a name="che98"></a>Checkland, P. & Holwell, S. (1998). _Information, systems, and information systems: making sense of the field_. Chichester, UK: Wiley
*   <a name="cho98"></a>Choo, C. W. (1998). _The knowing organization: how organizations use information to construct meaning, create knowledge, and make decisions_. New York, NY: Oxford University Press.
*   <a name="com01"></a>Compaine, B.M. (Ed.). (2001). _The digital divide: facing a crisis or creating a myth?_ Cambridge, MA: MIT Press Sourcebooks.
*   <a name="dal68"></a>Dalton, G. (Ed.). (1968). _Primitive, archaic and modern economies: essays of Karl Polanyi_. Garden City, NY: Doubleday.
*   <a name="dav97"></a>Davenport, T.H. & Prusak, L. (1997). _Information ecology: mastering the information and knowledge environment_. New York, NY: Oxford University Press.
*   <a name="day77"></a>Day, R. & Day, J. V. (1977). A review of the current state of negotiated order theory: an appreciation and a critique. _The Sociological Quarterly_, **18**(1), 126-142.
*   <a name="dia98"></a>Diamond, J. M. (1998). _Guns, germs and steel: a short history of everybody for the last 13,000 years_. London: Vintage.
*   <a name="dou96"></a>Douglas, M. & Isherwood, B. (1996). _The world of goods: towards an anthropology of consumption_. London: Routledge.
*   <a name="edm00"></a>Edmunds, A. & Morris, A. (2000). The problem of information overload in business organizations: a review of the literature. _International Journal of Information Management_, **20**(1), 17-28.
*   <a name="fea00"></a>Feather, J.P. (2000). _The information society: a study of continuity and change_. (3rd ed.). London: Library Association.
*   <a name="fos00"></a>Foster, S.P. (2000). The digital divide: some reflections. _International Information and Library Review_. **32**(3-4), 437-451.
*   <a name="ger02"></a>Gerrig, R.J. & Zimbardo, P.G. (2002). [Personality](http://www.psychologymatters.org/glossary.html)

_Psychology and life._

*   <a name="gre98"></a>Green, A.-M. & Davenport, E. (1998). Putting new media in its place: the Edinburgh experience. In T.D. Wilson & D.K. Allen, (Eds.). _Exploring the contexts of information behaviour: proceedings of the second international conference on research in information needs, seeking and use in different contexts_, Sheffield, 13-15 August. London: Taylor Graham.
*   <a name="gre02"></a>Gregor, S.D. (2002). A theory of theories in information systems. In S.D. Gregor & D.N. Hart, (Eds.). _Information systems foundations workshop: building the theoretical base_. Canberra: Australian National University.
*   <a name="had06"></a>Haddon, L. (2006). The contribution of domestication research to in-home computing and media consumption. _The Information Society_, **22**(4), 195-203.
*   <a name="hob98"></a>Hobart, M.E. & Schiffman, Z.S. (1998). _Information ages: literacy, numeracy, and the computer revolution_. Baltimore, MD: Johns Hopkins University Press.
*   <a name="hol02"></a>Holgate, J. (2002). _The phantom of information_. Paper presented at the Electronic Conference on Foundations of Information Science, 6-10 May 2002\.
*   <a name="kftf"></a>[Keeping found things found](http://www.webcitation.org/5W669LFyY). (2007). .

*   <a name="kee98"></a>Keesing, R.M. & Strathern, A.J. (1998). _Cultural anthropology: a contemporary perspective_. (3rd ed.). Fort Worth, TX: Harcourt Brace College Publishers.
*   <a name="loa98"></a>Loader, B. (1998). _Cyberspace divide: equality, agency, and policy in the information society_. London: Routledge.
*   <a name="mac83"></a>Machlup, F. & Mansfield, U. (Eds). (1983). _The study of information: interdisciplinary messages_. New York, NY: John Wiley and Sons.
*   <a name="nar97"></a>Narotzky, S. (1997). _New directions in economic anthropology_. London, Chicago, IL: Pluto Press.
*   <a name="noy95"></a>Noyes, J.M. & Thomas, P.J. (1995). _Information overload: an overview_. Paper presented at the IEE Colloquium on Information Overload, 20th November, 1995\. (Digest No. 95223, 1-3).
*   <a name="obr99"></a>O'Brien, J., Rodden, T., Rouncefield, M. & Hughes, J. (1999). At home with the technology: an ethnographic study of a set-top box trial. _ACM Transactions on Computer-Human Interaction_, **6**(3), 282-308.
*   <a name="sch91"></a>Schatzman, L. (1991). Dimensional analysis: notes on an alternative approach to the grounding of theory in qualitative research. In D.R. Maines, (Ed.). _Social organization and social process: essays in honor of Anselm Strauss_. New York, NY: Aldine De Gruyter.
*   <a name="sch95"></a>Schement, J. R. & Curtis, T. (1995). _Tendencies and tensions of the information age: the production and distribution of information in the United States_. New Brunswick, NJ: Transaction Publishers.
*   <a name="sil94"></a>Silverstone, R. (1994). _Television and everyday life_. London: Routledge.
*   <a name="str78"></a>Strauss, A. L. (1978). _Negotiations: varieties, contexts, processes and social order_. San Francisco, CA: Jossey-Bass.
*   <a name="tid99"></a>Tidline, T. J. (1999). The mythology of information overload. _Library Trends_, **47**(3), 485-506.
*   <a name="ven96"></a>Venkatesh, A. (1996). Computers and other interactive technologies for the home. _Communications of the ACM_, **39**(12), 47-54.
*   <a name="ven06"></a>Venkatesh, A. (2006). Introduction to the special issue of 'ICT in everyday life: home and personal environments'. _The Information Society_, **22**(4), 191-194.
*   <a name="web64"></a>Weber, M. (1964). _The theory of social and economic organization_. (Henderson, A. M. & Parsons, T, Trans.). New York, NY: Collier-Macmillan Ltd.
*   <a name="whi96"></a>Whittaker, S and Sidner, C. (1996). e-mail overload: exploring personal information management of e-mail. Paper presented at the _Conference on Human Factors and Computing Systems_, Vancouver, BC, Canada.
*   <a name="wil00"></a>Wilson, T.D. (2000). Human information behavior. _Informing Science_, **3**(2), 49-56.





## Appendices

**Note**

<a name="appnote"></a>I have been unable to find any agreed nomenclature to describe the types of information and information-related devices and services found in a household. Terms used in both appendices, therefore, are mine. The types, and the types of information-related devices and services, were identified through an audit of the information found in my own household. They generally relate to the subject of the information or its use. That these designations were not challenged by participants indicates that they were meaningful, if not accurate.

Rather than present participants with long lists of different types of information and information-related devices and services, I chose to group the various types into categories. Because I could find no agreed categories I developed my own. To a large extent categorisation is arbitrary. My intent in categorising types was simply to make it easier for participants to complete the questionnaire and identify the types held and used in their household.

Devising suitable designations and categories for the different types of information and information-related devices and services found in a household is a task I leave to other researchers.



### List of information types by category



#### Advertising-related

Advertorials on TV  
Advertorials in newspapers and magazines  
Commercials (TV, radio, Internet)  
Take away menus  
Real estate flyers and letters  
How to vote cards  
Newspaper inserts  
Newspapers  
Letters from politicians  
Price lists (e.g., phone calls, parts)  
Government advertising and public notices  
Product brochures  
Shopping specials flyers  
Charity reports and letters

#### Advice-related

Warranty papers  
Directories (e.g., Big Colour Pages, camping, hotels)  
Instruction/assembly manuals/CD/DVD/videos (e.g., appliances, computers, tools, furniture, handyman brochures)  
Loyalty program records and reports (e.g., FlyBuys)  
Phone books  
User manuals for software  
Emergency phone numbers  
Council and notices (e.g., tree lopping)

#### Around the house-related

Fridge magnets  
Fridge doors, including notices on the door  
Shop-a-Dockets  
Mementoes (e.g., menus, serviettes, book matches, pub coasters)  
Household jobs lists  
Other lists (e.g., birthdays, gifts, guests, shopping)  
House plans  
Ornaments  
Decorations  
Art works  
Children's drawings  
Posters  
Packaging (e.g., clothing, appliances, food)  
Food labels  
Rosters (e.g., school canteen, volunteer work)  
Clothing labels  
Recipe books  
Kitchen notice boards

#### Education and training-related

School reports  
School photos  
School notices (e.g., newsletters, P&C notices)  
School course notes  
Text books  
Student identity cards  
Reference books, paper (e.g., atlases, dictionaries, encyclopaedia)  
Reference CD/DVD (e.g., dictionaries, atlases, encyclopaedia)  
Curriculum papers  
Diplomas, Testamurs, Degree certificates, etc.  
Exam and test results  
Children's school work/note books

#### Entertainment-related

TV shows generally  
Radio shows generally  
Internet radio  
Talkback radio  
Games (e.g., computer, board, card)  
Games rules  
Music CDs and tapes  
Musical scores  
Lyrics (words for songs)  
Music CD/tape sleeves  
Movies on DVD/video tape  
General reading books  
General reading magazines  
Stamp collections  
Photos and home movies (paper or digital)  
Bulletin boards on the Internet  
Betting slips  
Lotto, Tatts, Pools, lottery tickets  
Ticket stubs  
Programs of events (e.g., concerts, school events, church service, award ceremonies, weddings, radio, TV)

#### Finance-related

Bank statements (digital, paper)  
Bank books  
Bank notices  
Cheque book butts  
Bank/ATM access cards  
ATM receipts  
Bills, invoices  
Receipt numbers from bill payments (e.g., via telephone, Internet)  
Electronic receipts (e.g., online shopping, online banking, bill payments)  
Other payment receipts  
Superannuation and pension records, reports, benefit lists, etc  
Credit cards  
Store, charge and credit cards  
Investment prospectuses  
Investment records and statements  
Shareholder reports  
Stock and investment certificates  
Company annual reports  
Social security documents and notices  
Tax Packs  
Tax related documents (other than Tax Pack)  
Donation receipts (for tax deductions)  
Church offering envelopes  
Pawn broker receipts

#### Health-related

Prescriptions (e.g., glasses, medicines)  
X-rays  
Medical reports (e.g., X-ray reports, test results)  
Health insurance cards  
Medicare cards  
Medical condition cards (e.g., MedicAlert)  
Vaccination records  
Health advice and well-being brochures

#### Keeping in touch-related

Phone calls (mobile and house/fixed line)  
Pager/beeper messages  
Phone messages on paper  
Voice mail messages  
Telephone answer machine messages  
SMS/MMS messages (text and picture messages)  
Birthday cards  
Christmas cards  
Other special occasion cards  
Address books (paper, digital)  
Gift vouchers  
Gossip  
Internet chat sessions  
Invitations (written, oral)  
Talks with friends  
Visits from friends and others  
Post cards  
Family conversations  
Thank you notes  
Tone of voice (in conversations)  
Body language (in conversations)  
Arguments  

#### Legal-related

House deeds  
Drivers licences  
Other licences  
Terms and conditions (e.g., insurance policies, credit cards, Frequent Flyer)  
Proxy voting forms  
Passports  
Insurance policies  
Legal notices  
Birth certificates  
Lease documents  
Marriage certificates  
Power of Attorney documents  
Wills  
Death certificates  
Car registration certificates  
Traffic infringement notices  
Postal voting papers (e.g., company, government, club elections)  
Approval certificates (e.g., house occupancy, safety)  
Car registration labels  
Car number plates

#### Personal-related

Diaries (paper, digital)  
Personal notes, jottings, reminders, etc.  
Notes from meetings  
Passwords, PIN  
Password and PIN notices  
Personal Digital Assistants (PDA, e.g., PALM)  
FlyBuy cards  
Discount cards  
Library cards  
Personal Information Management (PIM) software  
Love letters  
Personal letters  
Awards and prizes (e.g., cups, trophies, plaques, certificates)  
Baptismal certificates  
Membership cards (e.g., sports/coffee/health/social/video club, political party, motoring association)  
Membership records and notices  
Renewal notices (e.g., licences, insurance, subscriptions)  
Personal computers  
Travel-related  
Timetables (e.g., bus, train, airline)  
Airline boarding passes  
Holiday brochures  
Maps (e.g., road, walking, bicycle)  
Travel tickets and passes (e.g., plane, bus)  
Frequent Flyer cards

#### Work-related

Employment histories  
CVs/resumes  
Professional journals  
Security passes  
Business cards  
Referee reports for jobs  
Pay slips (paper, digital)  
Training notes and handouts  
Other business/work related documents

#### Miscellaneous

Downloaded computer files  
E-mail  
Computer files created by you  
Computer disks (e.g., floppy, CD-ROM, DVD, tapes, external and removable hard drives)  
Newsgroups on the Internet  
The internet, generally  
Newsletters (if not mentioned above)  
Podcasts

#### Other

As identified by householders

### <a name="App2">Appendix 2 - List of IRDS types by category</a>

[See appendix notes](#appnote)

#### Telephone-related

Fixed telephone lines  
Fixed telephone handsets, including wireless and other extensions  
Telephone answering machine, fixed phone  
Voice mail, fixed phone  
Advanced services for fixed phone (eg, call waiting, group calls)  
Mobile phone  
Voice mail for mobile phone  
SMS/MMS for mobile phone  
Other advanced services

#### Computer-related

Computer  
Broadband connection  
Dial up connection  
Colour laser printer  
Monochrome laser printer  
Ink jet printer  
Other printer, not photo printer  
Household network, cable based  
Household network, wireless based  
Scanner  
All-in-one scanner, printer, photocopier  
E-mail account  
Drawing tablet

#### Entertainment-related

Home entertainment centre  
Television  
Pay TV service  
Stereo system, stand alone  
Stereo system, portable  
VCR player  
DVD player/recorder, stand alone  
DVD player, portable  
CD player/recorder, stand alone  
CD player, portable  
Radio, stand alone  
Radio, portable  
Record player  
Cassette player, stand alone  
Cassette player, portable  
iPod or similar MP3 player, portable  
E-book reader  
LCD projector  
Games console  
Mini-disc player

#### Camera-related

Video camera, digital or analogue  
Digital still camera  
Film camera  
Movie camera, film  
Slide projector  
Movie projector  
Photo printer, for direct printing of photos  
Portable digital photo viewer  
Photo printing service by Internet  
Photo printing service by mail  
Memory card to CD service

#### Other devices

Fax machine, computer based  
Fax machine, not computer based  
Beeper/pager  
Personal Digital Assistant (PDA)  
Other electronic diary (eg Outlook, iCal)  
Paper diary  
Photocopier  
Typewriter  
Writing pad  
Address book, paper  
Bookcase  
Filing cupboard (eg, 2- or 4-drawer)

#### Additional

As identified by householders

